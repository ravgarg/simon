package com.mirakl.hybris.core.constants;

/**
 * Global class for all Miraklservices constants. You can add global constants for your extension into this class.
 */
public final class MiraklservicesConstants extends GeneratedMiraklservicesConstants {
  public static final String EXTENSIONNAME = "miraklservices";

  private MiraklservicesConstants() {
    // empty to avoid instantiating this constant class
  }

  public static String[] getCategoryExportHeaders() {
    return new String[] {CategoryExportHeaders.CATEGORY_CODE, CategoryExportHeaders.CATEGORY_LABEL,
        CategoryExportHeaders.PARENT_CODE};
  }

  public interface CategoryExportHeaders {
    String CATEGORY_CODE = "category-code";
    String CATEGORY_LABEL = "category-label";
    String PARENT_CODE = "parent-code";
  }

  public static final String HYBRIS_DATA_DIR = "HYBRIS_DATA_DIR";
  public static final String ALL_BRANDS_CONTEXT_VARIABLE = "allBrands";
  public static final String ALL_CATEGORIES_CONTEXT_VARIABLE = "allCategories";
  public static final String KEY_VALUE_SEPARATOR = "|";
  public static final char COLLECTION_ITEM_SEPARATOR = ',';
  public static final String DESCRIPTION_MAXLENGTH_CONFIG_KEY = "mirakl.products.export.description.maxlength";
  public static final String OFFER_NEW_STATE_CODE_KEY = "mirakl.offers.state.new.code";
  public static final String UPDATE_RECEIVED_EVENT_NAME = "_UpdateReceived";
  public static final String BOOLEAN_VALUE_LIST_ID = "boolean-values";
  public static final String PRODUCTS_IMPORT_MAX_DURATION = "mirakl.products.import.maximumduration";
  public static final String PRODUCTS_IMPORT_VALUES_SEPARATOR = "mirakl.products.import.valuesseparator";
  public static final String PRODUCTS_IMPORT_LOCALIZED_ATTRIBUTE_REGEX = "mirakl.products.import.localizedattributeregex";
  public static final String PRODUCTS_IMPORT_RESULT_QUEUE_LENGTH = "mirakl.products.import.resultqueuelength";
  public static final String PRODUCTS_IMPORT_ADDITIONAL_MESSAGE_HEADER = "mirakl.products.import.additionalmessageheader";
  public static final String PRODUCTS_IMPORT_ERROR_MESSAGE_HEADER = "mirakl.products.import.errormessageheader";
  public static final String PRODUCTS_IMPORT_ERROR_LINE_HEADER = "mirakl.products.import.errorlineheader";
  public static final String PRODUCTS_IMPORT_ORIGINAL_LINE_NUMBER_HEADER = "mirakl.products.import.originallinenumberheader";
  public static final String PRODUCTS_IMPORT_ERROR_FILENAME_SUFFIX = "mirakl.products.import.errorfilenamesuffix";
  public static final String PRODUCTS_IMPORT_SUCCESS_FILENAME_SUFFIX = "mirakl.products.import.successfilenamesuffix";
  public static final String PRODUCTS_IMPORT_REPORT_DEFAULT_ENCODING = "mirakl.products.import.reportdefaultencoding";
  public static final String PRODUCTS_IMPORT_ALREADY_RECEIVED_MESSAGE = "text.products.import.alreadyreceived";
  public static final String PRODUCTS_IMPORT_MEDIA_DOWNLOAD_FAILURE_MESSAGE = "text.products.import.mediadownloadfailure";
  public static final String CATALOG_EXPORT_DECIMAL_PRECISION = "mirakl.catalog.export.decimal.precision";
  public static final String CATALOG_EXPORT_DATE_FORMAT = "mirakl.catalog.export.date.format";
  public static final String CATALOG_EXPORT_MEDIA_SIZE = "mirakl.catalog.export.media.size";
  public static final String CATALOG_EXPORT_LOCALIZED_ATTRIBUTE_PATTERN = "mirakl.catalog.export.localizedattributepattern";
  public static final String CATALOG_EXPORT_DEFAULT_ENCODING = "UTF-8";
  public static final String CATALOG_EXPORT_FILE_EXTENSION = ".csv";
}
