package com.mirakl.hybris.core.category.services.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.getCategoryExportHeaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.category.services.CommissionCategoryService;
import com.mirakl.hybris.core.util.services.CsvService;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

public class DefaultCommissionCategoryService implements CommissionCategoryService {

  protected CategoryService categoryService;
  protected SessionService sessionService;
  protected I18NService i18NService;
  protected CsvService csvService;
  protected Converter<Pair<CategoryModel, Collection<CategoryModel>>, Map<String, String>> commissionCategoryConverter;

  @Override
  public Collection<CategoryModel> getCategories(CategoryModel rootCategory) {
    List<CategoryModel> subCategoriesForRootCatalogVersion = new ArrayList<>();
    subCategoriesForRootCatalogVersion.add(rootCategory);
    Collection<CategoryModel> allSubcategories = categoryService.getAllSubcategoriesForCategory(rootCategory);

    CatalogVersionModel rootCatalogVersion = rootCategory.getCatalogVersion();
    for (CategoryModel subcategory : allSubcategories) {
      if (rootCatalogVersion.equals(subcategory.getCatalogVersion())) {
        subCategoriesForRootCatalogVersion.add(subcategory);
      }
    }
    return subCategoriesForRootCatalogVersion;
  }

  @Override
  public String getCategoryExportCsvContent(final Locale locale, final Collection<CategoryModel> categories) throws IOException {
    final Collection<Pair<CategoryModel, Collection<CategoryModel>>> categoryPairs = getCategoryPairs(categories);
    List<Map<String, String>> commissionCategories = mapExportCategories(locale, categoryPairs);

    return csvService.createCsvWithHeaders(getCategoryExportHeaders(), commissionCategories);
  }

  protected List<Map<String, String>> mapExportCategories(final Locale locale,
      final Collection<Pair<CategoryModel, Collection<CategoryModel>>> categoryPairs) {
    return sessionService.executeInLocalView(new SessionExecutionBody() {
      @Override
      public Object execute() {
        i18NService.setCurrentLocale(locale);
        return commissionCategoryConverter.convertAllIgnoreExceptions(categoryPairs);
      }
    });
  }

  protected Collection<Pair<CategoryModel, Collection<CategoryModel>>> getCategoryPairs(Collection<CategoryModel> categories) {
    Collection<Pair<CategoryModel, Collection<CategoryModel>>> categoryPairs = new ArrayList<>();

    for (CategoryModel category : categories) {
      categoryPairs.add(Pair.of(category, categories));
    }
    return categoryPairs;
  }

  @Required
  public void setCategoryService(CategoryService categoryService) {
    this.categoryService = categoryService;
  }

  @Required
  public void setSessionService(SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @Required
  public void setI18NService(I18NService i18NService) {
    this.i18NService = i18NService;
  }

  @Required
  public void setCsvService(CsvService csvService) {
    this.csvService = csvService;
  }

  @Required
  public void setCommissionCategoryConverter(
      Converter<Pair<CategoryModel, Collection<CategoryModel>>, Map<String, String>> commissionCategoryConverter) {
    this.commissionCategoryConverter = commissionCategoryConverter;
  }
}
