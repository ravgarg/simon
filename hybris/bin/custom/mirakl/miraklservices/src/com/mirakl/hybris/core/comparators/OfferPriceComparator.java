package com.mirakl.hybris.core.comparators;

import com.mirakl.hybris.core.model.OfferModel;

import java.util.Comparator;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class OfferPriceComparator implements Comparator<OfferModel> {

  @Override
  public int compare(OfferModel offer1, OfferModel offer2) {
    return offer1.getTotalPrice().compareTo(offer2.getTotalPrice());
  }
}
