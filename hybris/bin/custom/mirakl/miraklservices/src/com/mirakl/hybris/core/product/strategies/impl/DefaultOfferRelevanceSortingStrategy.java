package com.mirakl.hybris.core.product.strategies.impl;

import static com.google.common.base.Preconditions.checkState;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.OFFER_NEW_STATE_CODE_KEY;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.comparators.OfferPriceComparator;
import com.mirakl.hybris.core.comparators.OfferStateComparator;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.strategies.OfferRelevanceSortingStrategy;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultOfferRelevanceSortingStrategy implements OfferRelevanceSortingStrategy {

  private ConfigurationService configurationService;

  @SuppressWarnings("unchecked")
  @Override
  public List<OfferModel> sort(List<OfferModel> offerList) {
    ComparatorChain comparatorChain = new ComparatorChain();
    comparatorChain.addComparator(new OfferStateComparator(getNewOfferState()));
    comparatorChain.addComparator(new OfferPriceComparator());
    Collections.sort(offerList, comparatorChain);
    return offerList;
  }

  protected OfferState getNewOfferState() {
    String newOfferStateCode = configurationService.getConfiguration().getString(OFFER_NEW_STATE_CODE_KEY);
    checkState(isNotBlank(newOfferStateCode), "No NEW offer state code configured");

    return OfferState.valueOf(newOfferStateCode);
  }

  @Required
  public void setConfigurationService(ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }
}
