package com.mirakl.hybris.core.catalog.strategies.impl;


import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static com.mirakl.hybris.core.enums.MiraklExportType.ATTRIBUTE_EXPORT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mci.front.core.MiraklCatalogIntegrationFrontApi;
import com.mirakl.client.mci.front.domain.attribute.MiraklAttributeImportResult;
import com.mirakl.client.mci.front.request.attribute.MiraklAttributeImportErrorReportRequest;
import com.mirakl.client.mci.front.request.attribute.MiraklAttributeImportStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.strategies.impl.AbstractExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultAttributeExportReportStrategy extends AbstractExportReportStrategy<MiraklAttributeImportResult> {

  protected MiraklCatalogIntegrationFrontApi mciApi;
  protected Populator<MiraklAttributeImportResult, MiraklJobReportModel> reportPopulator;
  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  protected File getErrorReportFile(String jobId) throws IOException {
    checkArgument(StringUtils.isNotBlank(jobId), "Attribute export job id cannot be blank");

    return mciApi.getAttributeImportErrorReport(new MiraklAttributeImportErrorReportRequest(jobId));
  }

  @Override
  protected MiraklAttributeImportResult getExportResult(String syncJobId) {
    checkArgument(isNotBlank(syncJobId), "Attribute export job id cannot be blank");

    return mciApi.getAttributeImportResult(new MiraklAttributeImportStatusRequest(syncJobId));
  }

  @Override
  protected boolean isExportCompleted(MiraklAttributeImportResult exportResult) {
    return !MiraklExportStatus.PENDING.equals(exportStatuses.get(exportResult.getImportStatus()));
  }

  @Override
  protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
    return miraklJobReportDao.findPendingJobReportsForType(ATTRIBUTE_EXPORT);
  }

  @Override
  protected Populator<MiraklAttributeImportResult, MiraklJobReportModel> getReportPopulator() {
    return reportPopulator;
  }

  @Required
  public void setMciApi(MiraklCatalogIntegrationFrontApi mciApi) {
    this.mciApi = mciApi;
  }

  @Required
  public void setReportPopulator(Populator<MiraklAttributeImportResult, MiraklJobReportModel> reportPopulator) {
    this.reportPopulator = reportPopulator;
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
