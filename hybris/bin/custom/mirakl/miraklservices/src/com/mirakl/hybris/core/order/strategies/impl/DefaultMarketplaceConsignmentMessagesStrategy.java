package com.mirakl.hybris.core.order.strategies.impl;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.order.strategies.MarketplaceConsignmentMessagesStrategy;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

public class DefaultMarketplaceConsignmentMessagesStrategy implements MarketplaceConsignmentMessagesStrategy {
  @Override
  public boolean canWriteMessages(MarketplaceConsignmentModel consignment) {
    return true;
  }
}
