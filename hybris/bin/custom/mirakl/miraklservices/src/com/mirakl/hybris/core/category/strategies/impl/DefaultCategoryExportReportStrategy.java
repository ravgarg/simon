package com.mirakl.hybris.core.category.strategies.impl;

import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static com.mirakl.hybris.core.enums.MiraklExportType.COMMISSION_CATEGORY_EXPORT;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.category.synchro.MiraklCategorySynchroResult;
import com.mirakl.client.mmp.request.catalog.category.MiraklCategorySynchroErrorReportRequest;
import com.mirakl.client.mmp.request.catalog.category.MiraklCategorySynchroStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.strategies.impl.AbstractExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;

public class DefaultCategoryExportReportStrategy extends AbstractExportReportStrategy<MiraklCategorySynchroResult> {

  protected Populator<MiraklCategorySynchroResult, MiraklJobReportModel> reportPopulator;
  protected MiraklMarketplacePlatformFrontApi mmpApi;
  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  protected MiraklCategorySynchroResult getExportResult(String syncJobId) {
    checkArgument(isNotBlank(syncJobId), "Category export job id cannot be blank");

    return mmpApi.getCategorySynchroResult(new MiraklCategorySynchroStatusRequest(syncJobId));
  }

  @Override
  protected boolean isExportCompleted(MiraklCategorySynchroResult exportResult) {
    return !MiraklExportStatus.PENDING.equals(exportStatuses.get(exportResult.getStatus()));
  }

  @Override
  protected File getErrorReportFile(String jobId) {
    checkArgument(isNotBlank(jobId), "Category export job id cannot be blank");

    return mmpApi.getCategorySynchroErrorReport(new MiraklCategorySynchroErrorReportRequest(jobId));
  }

  @Override
  protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
    return miraklJobReportDao.findPendingJobReportsForType(COMMISSION_CATEGORY_EXPORT);
  }

  @Override
  protected Populator<MiraklCategorySynchroResult, MiraklJobReportModel> getReportPopulator() {
    return reportPopulator;
  }

  @Required
  public void setReportPopulator(Populator<MiraklCategorySynchroResult, MiraklJobReportModel> reportPopulator) {
    this.reportPopulator = reportPopulator;
  }

  @Required
  public void setMmpApi(MiraklMarketplacePlatformFrontApi mmpApi) {
    this.mmpApi = mmpApi;
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
