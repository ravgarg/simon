package com.mirakl.hybris.core.shop.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.evaluation.MiraklEvaluations;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.request.shop.MiraklGetShopEvaluationsRequest;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;
import com.mirakl.hybris.core.shop.services.ShopService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultShopService implements ShopService {

  protected ShopDao shopDao;
  protected MiraklMarketplacePlatformFrontApiClient miraklApi;

  @Override
  public ShopModel getShopForId(String id) {
    return shopDao.findShopById(id);
  }

  @Override
  public MiraklEvaluations getEvaluations(String id, PageableData pageableData) {
    validateParameterNotNull(pageableData, "PageableData must not be null");
    if (pageableData.getCurrentPage() < 1) {
      throw new IllegalArgumentException(format("Illegal evaluation page number: %d for shop [%s]", pageableData.getCurrentPage(), id));
    }

    MiraklGetShopEvaluationsRequest request = new MiraklGetShopEvaluationsRequest(id);
    request.setPaginate(true);
    request.setMax(pageableData.getPageSize());
    request.setOffset(pageableData.getPageSize() * (pageableData.getCurrentPage() - 1));
    return miraklApi.getShopEvaluations(request);
  }

  @Required
  public void setShopDao(ShopDao shopDao) {
    this.shopDao = shopDao;
  }

  @Required
  public void setMiraklApi(MiraklMarketplacePlatformFrontApiClient miraklApi) {
    this.miraklApi = miraklApi;
  }
}
