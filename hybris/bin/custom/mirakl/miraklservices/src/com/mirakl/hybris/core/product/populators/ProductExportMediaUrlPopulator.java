package com.mirakl.hybris.core.product.populators;

import static com.mirakl.hybris.core.enums.MiraklProductExportHeader.MEDIA_URL;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class ProductExportMediaUrlPopulator extends AbstractProductExportWithFallbackPopulator {

  protected boolean secure;

  @Override
  protected void populateAttributesIfNotPresent(ProductModel source, Map<String, String> target) throws ConversionException {
    if (source.getPicture() != null && target.get(MEDIA_URL.getCode()) == null) {
      target.put(MEDIA_URL.getCode(), siteBaseUrlResolutionService.getMediaUrlForSite(baseSiteService.getCurrentBaseSite(),
          secure, source.getPicture().getURL()));
    }

  }

  @Required
  public void setSecure(boolean secure) {
    this.secure = secure;
  }

}
