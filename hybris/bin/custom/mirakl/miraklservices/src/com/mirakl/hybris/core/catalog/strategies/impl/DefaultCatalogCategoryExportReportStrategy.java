package com.mirakl.hybris.core.catalog.strategies.impl;


import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static com.mirakl.hybris.core.enums.MiraklExportType.CATALOG_CATEGORY_EXPORT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mci.front.core.MiraklCatalogIntegrationFrontApi;
import com.mirakl.client.mci.front.domain.hierarchy.MiraklHierarchyImportResult;
import com.mirakl.client.mci.front.request.hierarchy.MiraklHierarchyImportErrorReportRequest;
import com.mirakl.client.mci.front.request.hierarchy.MiraklHierarchyImportStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.strategies.impl.AbstractExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultCatalogCategoryExportReportStrategy extends AbstractExportReportStrategy<MiraklHierarchyImportResult> {

  protected MiraklCatalogIntegrationFrontApi mciApi;
  protected Populator<MiraklHierarchyImportResult, MiraklJobReportModel> reportPopulator;
  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  protected File getErrorReportFile(String jobId) throws IOException {
    checkArgument(StringUtils.isNotBlank(jobId), "Hierarchy export job id cannot be blank");

    return mciApi.getHierarchiyImportErrorReport(new MiraklHierarchyImportErrorReportRequest(jobId));
  }

  @Override
  protected MiraklHierarchyImportResult getExportResult(String syncJobId) {
    checkArgument(isNotBlank(syncJobId), "Hierarchy export job id cannot be blank");

    return mciApi.getHierarchyImportResult(new MiraklHierarchyImportStatusRequest(syncJobId));
  }

  @Override
  protected boolean isExportCompleted(MiraklHierarchyImportResult exportResult) {
    return !MiraklExportStatus.PENDING.equals(exportStatuses.get(exportResult.getImportStatus()));
  }

  @Override
  protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
    return miraklJobReportDao.findPendingJobReportsForType(CATALOG_CATEGORY_EXPORT);
  }

  @Override
  protected Populator<MiraklHierarchyImportResult, MiraklJobReportModel> getReportPopulator() {
    return reportPopulator;
  }

  @Required
  public void setMciApi(MiraklCatalogIntegrationFrontApi mciApi) {
    this.mciApi = mciApi;
  }

  @Required
  public void setReportPopulator(Populator<MiraklHierarchyImportResult, MiraklJobReportModel> reportPopulator) {
    this.reportPopulator = reportPopulator;
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
