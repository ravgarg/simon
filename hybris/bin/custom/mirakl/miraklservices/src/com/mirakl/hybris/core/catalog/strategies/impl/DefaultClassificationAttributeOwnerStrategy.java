package com.mirakl.hybris.core.catalog.strategies.impl;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.core.catalog.strategies.ClassificationAttributeOwnerStrategy;
import com.mirakl.hybris.core.catalog.strategies.AttributeVarianceStrategy;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.product.ProductModel;

public class DefaultClassificationAttributeOwnerStrategy implements ClassificationAttributeOwnerStrategy {

  protected AttributeVarianceStrategy attributeVarianceStrategy;

  @Override
  public ProductModel determineOwner(ClassAttributeAssignmentModel attributeAssignment, ProductImportData data,
      ProductImportFileContextData context) {
    if (attributeVarianceStrategy.isVariant(attributeAssignment)) {
      return data.getProductToUpdate();
    }

    return data.getRootBaseProductToUpdate();
  }

  @Required
  public void setAttributeVarianceStrategy(AttributeVarianceStrategy attributeVarianceStrategy) {
    this.attributeVarianceStrategy = attributeVarianceStrategy;
  }

}
