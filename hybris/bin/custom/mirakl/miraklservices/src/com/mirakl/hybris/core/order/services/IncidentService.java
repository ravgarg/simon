package com.mirakl.hybris.core.order.services;

import java.util.List;

import com.mirakl.client.mmp.domain.reason.MiraklReason;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public interface IncidentService {

  /**
   * Gets the reasons defined in Mirakl
   *
   * @return a list of MiraklReason from Mirakl
   */
  List<MiraklReason> getReasons();

  /**
   * Opens an incident for the designated consignment entry
   * 
   * @param consignmentEntryCode The code of the consignment entry
   * @param reasonCode The reason of the incident
   */
  void openIncident(String consignmentEntryCode, String reasonCode);

  /**
   * Closes an incident for the designated consignment entry
   * 
   * @param consignmentEntryCode The code of the consignment entry
   * @param reasonCode The reason for the closing of the incident
   */
  void closeIncident(String consignmentEntryCode, String reasonCode);
}
