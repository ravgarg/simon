package com.mirakl.hybris.core.order.services.impl;

import static com.mirakl.hybris.core.enums.MiraklOrderLineStatus.INCIDENT_CLOSED;
import static com.mirakl.hybris.core.enums.MiraklOrderLineStatus.INCIDENT_OPEN;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.request.order.incident.MiraklCloseIncidentRequest;
import com.mirakl.client.mmp.request.order.incident.MiraklOpenIncidentRequest;
import com.mirakl.hybris.core.order.services.IncidentService;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultIncidentService implements IncidentService {

  private static final Logger LOG = Logger.getLogger(DefaultIncidentService.class);

  protected MiraklMarketplacePlatformFrontApiClient miraklApi;
  protected ModelService modelService;
  protected MarketplaceConsignmentService marketplaceConsignmentService;

  @Override // TODO: Currently Using RE01 (RE02 not available in SDK v3.24.1)
  public List<MiraklReason> getReasons() {
    return miraklApi.getReasons();
  }

  @Override
  public void openIncident(String consignmentEntryCode, String reasonCode) {
    validateParameterNotNullStandardMessage(consignmentEntryCode, "consignmentEntryCode");

    ConsignmentEntryModel consignmentEntry =
        marketplaceConsignmentService.getConsignmentEntryForMiraklLineId(consignmentEntryCode);
    if (!isTrue(consignmentEntry.getCanOpenIncident())) {
      throw new IllegalStateException(format("Impossible to open an incident for consignment entry [%s]", consignmentEntryCode));
    }

    String consignmentCode = consignmentEntry.getConsignment().getCode();
    MiraklOpenIncidentRequest request = new MiraklOpenIncidentRequest(consignmentCode, consignmentEntryCode, reasonCode);
    miraklApi.openIncident(request);

    consignmentEntry.setCanOpenIncident(false);
    consignmentEntry.setMiraklOrderLineStatus(INCIDENT_OPEN);

    modelService.save(consignmentEntry);

    LOG.debug(String.format("Opened incident for consignment entry [%s]", consignmentEntryCode));
  }

  @Override
  public void closeIncident(String consignmentEntryCode, String reasonCode) {
    validateParameterNotNullStandardMessage(consignmentEntryCode, "consignmentEntryCode");

    ConsignmentEntryModel consignmentEntry =
        marketplaceConsignmentService.getConsignmentEntryForMiraklLineId(consignmentEntryCode);
    if (consignmentEntry.getCanOpenIncident() || !INCIDENT_OPEN.equals(consignmentEntry.getMiraklOrderLineStatus())) {
      throw new IllegalStateException(format("Impossible to close an incident for consignment entry [%s]", consignmentEntryCode));
    }

    String consignmentCode = consignmentEntry.getConsignment().getCode();
    MiraklCloseIncidentRequest request = new MiraklCloseIncidentRequest(consignmentCode, consignmentEntryCode, reasonCode);
    miraklApi.closeIncident(request);

    consignmentEntry.setCanOpenIncident(true);
    consignmentEntry.setMiraklOrderLineStatus(INCIDENT_CLOSED);

    modelService.save(consignmentEntry);

    LOG.debug(String.format("Closed incident for consignment entry [%s]", consignmentEntryCode));
  }

  @Required
  public void setMiraklApi(MiraklMarketplacePlatformFrontApiClient miraklApi) {
    this.miraklApi = miraklApi;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setMarketplaceConsignmentService(MarketplaceConsignmentService marketplaceConsignmentService) {
    this.marketplaceConsignmentService = marketplaceConsignmentService;
  }
}
