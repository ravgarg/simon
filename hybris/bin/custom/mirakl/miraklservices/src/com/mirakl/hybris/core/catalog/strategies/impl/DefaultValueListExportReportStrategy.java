package com.mirakl.hybris.core.catalog.strategies.impl;


import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static com.mirakl.hybris.core.enums.MiraklExportType.VALUE_LIST_EXPORT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mci.front.core.MiraklCatalogIntegrationFrontApi;
import com.mirakl.client.mci.front.domain.value.list.MiraklValueListImportResult;
import com.mirakl.client.mci.front.request.value.list.MiraklValueListImportErrorReportRequest;
import com.mirakl.client.mci.front.request.value.list.MiraklValueListImportStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.strategies.impl.AbstractExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultValueListExportReportStrategy extends AbstractExportReportStrategy<MiraklValueListImportResult> {

  protected MiraklCatalogIntegrationFrontApi mciApi;
  protected Populator<MiraklValueListImportResult, MiraklJobReportModel> reportPopulator;
  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  protected File getErrorReportFile(String syncJobId) throws IOException {
    checkArgument(StringUtils.isNotBlank(syncJobId), "Category export job id cannot be blank");

    return mciApi.getValueListImportErrorReport(new MiraklValueListImportErrorReportRequest(syncJobId));
  }

  @Override
  protected MiraklValueListImportResult getExportResult(String syncJobId) {
    checkArgument(isNotBlank(syncJobId), "Value List export job id cannot be blank");

    return mciApi.getValueListImportResult(new MiraklValueListImportStatusRequest(syncJobId));
  }

  @Override
  protected boolean isExportCompleted(MiraklValueListImportResult exportResult) {
    return !MiraklExportStatus.PENDING.equals(exportStatuses.get(exportResult.getImportStatus()));
  }

  @Override
  protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
    return miraklJobReportDao.findPendingJobReportsForType(VALUE_LIST_EXPORT);
  }

  @Override
  protected Populator<MiraklValueListImportResult, MiraklJobReportModel> getReportPopulator() {
    return reportPopulator;
  }

  @Required
  public void setMciApi(MiraklCatalogIntegrationFrontApi mciApi) {
    this.mciApi = mciApi;
  }

  @Required
  public void setReportPopulator(Populator<MiraklValueListImportResult, MiraklJobReportModel> reportPopulator) {
    this.reportPopulator = reportPopulator;
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
