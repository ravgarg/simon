package com.mirakl.hybris.core.product.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.daos.MiraklProductDao;
import com.mirakl.hybris.core.product.services.MiraklProductService;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

public class DefaultMiraklProductService implements MiraklProductService {

  protected BaseStoreService baseStoreService;
  protected CommerceStockService commerceStockService;
  protected CommercePriceService commercePriceService;
  protected MiraklProductDao miraklProductDao;

  @Override
  public ProductModel getProductForShopVariantGroupCode(ShopModel shop, String variantGroupCode,
      CatalogVersionModel catalogVersion) {
    validateParameterNotNullStandardMessage("shop", shop);
    validateParameterNotNullStandardMessage("variantGroupCode", variantGroupCode);
    validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);

    return miraklProductDao.findProductForShopVariantGroupCode(shop, variantGroupCode, catalogVersion);
  }

  @Override
  public boolean isSellableByOperator(ProductModel product) {
    return isPurchasable(product) && hasStock(product) && hasPrice(product);
  }

  protected boolean isPurchasable(ProductModel product) {
    return product.getVariantType() == null && isApproved(product);
  }

  protected boolean isApproved(ProductModel product) {
    return ArticleApprovalStatus.APPROVED.equals(product.getApprovalStatus());
  }

  protected boolean hasStock(ProductModel product) {
    BaseStoreModel baseStore = baseStoreService.getCurrentBaseStore();
    if (baseStore == null) {
      return false;
    }
    StockLevelStatus stockLevelStatus = commerceStockService.getStockLevelStatusForProductAndBaseStore(product, baseStore);

    return !StockLevelStatus.OUTOFSTOCK.equals(stockLevelStatus);
  }

  protected boolean hasPrice(ProductModel product) {
    PriceInformation productPrice = commercePriceService.getWebPriceForProduct(product);

    return productPrice != null;
  }

  @Required
  public void setBaseStoreService(BaseStoreService baseStoreService) {
    this.baseStoreService = baseStoreService;
  }

  @Required
  public void setCommerceStockService(CommerceStockService commerceStockService) {
    this.commerceStockService = commerceStockService;
  }

  @Required
  public void setCommercePriceService(CommercePriceService commercePriceService) {
    this.commercePriceService = commercePriceService;
  }

  @Required
  public void setMiraklProductDao(MiraklProductDao miraklProductDao) {
    this.miraklProductDao = miraklProductDao;
  }

}
