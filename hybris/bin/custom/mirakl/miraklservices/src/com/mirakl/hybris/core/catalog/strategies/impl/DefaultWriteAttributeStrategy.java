package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.BOOLEAN_VALUE_LIST_ID;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CATALOG_EXPORT_DATE_FORMAT;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CATALOG_EXPORT_DECIMAL_PRECISION;
import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.catalog.events.ExportableAttributeEvent;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogContext;
import com.mirakl.hybris.core.catalog.strategies.AttributeDefaultValueStrategy;
import com.mirakl.hybris.core.catalog.strategies.AttributeVarianceStrategy;
import com.mirakl.hybris.core.catalog.strategies.ValueListNamingStrategy;
import com.mirakl.hybris.core.catalog.strategies.WriteAttributeStrategy;
import com.mirakl.hybris.core.constants.MiraklservicesConstants;
import com.mirakl.hybris.core.enums.MiraklAttributeExportHeader;
import com.mirakl.hybris.core.enums.MiraklAttributeType;

import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultWriteAttributeStrategy implements WriteAttributeStrategy {

  private static final Logger LOG = Logger.getLogger(DefaultWriteAttributeStrategy.class);

  protected ConfigurationService configurationService;
  protected ValueListNamingStrategy valueListNamingStrategy;
  protected AttributeVarianceStrategy attributeVarianceStrategy;
  protected AttributeDefaultValueStrategy attributeDefaultValueStrategy;

  @Override
  public void handleEvent(ExportableAttributeEvent event) {
    MiraklExportCatalogContext context = event.getContext();
    ClassAttributeAssignmentModel assignment = event.getAttributeAssignment();
    CategoryModel currentCategory = event.getCurrentCategory();

    if (context.getExportConfig().isExportAttributes() && isExportableAttribute(assignment)) {
      try {
        writeAttribute(assignment, currentCategory, context);
      } catch (IOException e) {
        LOG.error(format("Unable to export attribute [%s]", assignment.getClassificationAttribute().getCode()), e);
      }
    }
  }

  protected boolean isExportableAttribute(ClassAttributeAssignmentModel assignment) {
    if (ClassificationAttributeTypeEnum.ENUM == assignment.getAttributeType() && isEmpty(assignment.getAttributeValues())) {
      return false;
    }
    if (assignment.getOperatorExclusive() == null) {
      return !assignment.getClassificationAttribute().isOperatorExclusive();
    }

    return !assignment.getOperatorExclusive().booleanValue();
  }

  protected void writeAttribute(ClassAttributeAssignmentModel assignment, CategoryModel currentCategory,
      MiraklExportCatalogContext context) throws IOException {
    if (shouldBeLocalized(assignment, context)) {
      for (Locale locale : context.getExportConfig().getTranslatableLocales()) {
        writeLine(assignment, currentCategory, locale, context);
      }
      return;
    }

    writeLine(assignment, currentCategory, null, context);
  }

  protected boolean shouldBeLocalized(ClassAttributeAssignmentModel assignment, MiraklExportCatalogContext context) {
    return isTrue(assignment.getLocalized()) && isNotEmpty(context.getExportConfig().getTranslatableLocales());
  }

  protected void writeLine(ClassAttributeAssignmentModel assignment, CategoryModel currentCategory, Locale locale,
      MiraklExportCatalogContext context) throws IOException {
    context.getWriter().writeAttribute(buildLine(assignment, currentCategory, context, locale));
    context.removeMiraklAttributeCode(
        Pair.of(getLocalized(assignment.getClassificationAttribute().getCode(), locale), currentCategory.getCode()));
  }

  protected Map<String, String> buildLine(ClassAttributeAssignmentModel source, CategoryModel currentCategory,
      MiraklExportCatalogContext context, Locale locale) {
    ClassificationAttributeModel attribute = source.getClassificationAttribute();
    Locale defaultLocale = context.getExportConfig().getDefaultLocale();

    Map<String, String> line = new HashMap<>();
    line.put(MiraklAttributeExportHeader.CODE.getCode(), getLocalized(attribute.getCode(), locale));
    line.put(MiraklAttributeExportHeader.REQUIRED.getCode(), Boolean.toString(source.isRequiredForMarketplace()));
    line.put(MiraklAttributeExportHeader.VARIANT.getCode(), Boolean.toString(attributeVarianceStrategy.isVariant(source)));
    line.put(MiraklAttributeExportHeader.DEFAULT_VALUE.getCode(), attributeDefaultValueStrategy.<String>getDefaultValue(source));
    line.put(MiraklAttributeExportHeader.LABEL.getCode(), getLocalized(attribute.getName(defaultLocale), locale));
    line.put(MiraklAttributeExportHeader.DESCRIPTION.getCode(), source.getDescription(defaultLocale));

    for (Locale additionalLocale : context.getExportConfig().getAdditionalLocales()) {
      line.put(MiraklAttributeExportHeader.LABEL.getCode(additionalLocale),
          getLocalized(attribute.getName(additionalLocale), locale));
      line.put(MiraklAttributeExportHeader.DESCRIPTION.getCode(additionalLocale), source.getDescription(additionalLocale));
    }
    line.put(MiraklAttributeExportHeader.HIERARCHY_CODE.getCode(), currentCategory.getCode());
    populateTypes(source, line);

    return line;
  }

  protected String getLocalized(String value, Locale locale) {
    if (locale == null || value == null) {
      return value;
    }
    String columnFormat =
        configurationService.getConfiguration().getString(MiraklservicesConstants.CATALOG_EXPORT_LOCALIZED_ATTRIBUTE_PATTERN);

    return format(columnFormat, value, locale.getLanguage());
  }

  protected void populateTypes(ClassAttributeAssignmentModel source, Map<String, String> target) {
    switch (source.getAttributeType()) {
      case STRING:
        putType(MiraklAttributeType.TEXT.getCode(), null, target);
        break;

      case BOOLEAN:
        putType(MiraklAttributeType.LIST.getCode(), BOOLEAN_VALUE_LIST_ID, target);
        break;

      case DATE:
        putType(MiraklAttributeType.DATE.getCode(), configurationService.getConfiguration().getString(CATALOG_EXPORT_DATE_FORMAT),
            target);
        break;

      case NUMBER:
        putType(MiraklAttributeType.DECIMAL.getCode(),
            configurationService.getConfiguration().getString(CATALOG_EXPORT_DECIMAL_PRECISION), target);
        break;

      case ENUM:
        putType(isTrue(source.getMultiValued()) ? MiraklAttributeType.LIST_MULTIPLE_VALUES.getCode()
            : MiraklAttributeType.LIST.getCode(), valueListNamingStrategy.getCode(source), target);
        break;
    }
  }

  protected void putType(String type, String typeParameter, Map<String, String> line) {
    line.put(MiraklAttributeExportHeader.TYPE.getCode(), type);
    line.put(MiraklAttributeExportHeader.TYPE_PARAMETER.getCode(), typeParameter);
  }

  @Required
  public void setConfigurationService(ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @Required
  public void setValueListNamingStrategy(ValueListNamingStrategy valueListNamingStrategy) {
    this.valueListNamingStrategy = valueListNamingStrategy;
  }

  @Required
  public void setAttributeVarianceStrategy(AttributeVarianceStrategy attributeVarianceStrategy) {
    this.attributeVarianceStrategy = attributeVarianceStrategy;
  }

  @Required
  public void setAttributeDefaultValueStrategy(AttributeDefaultValueStrategy attributeDefaultValueStrategy) {
    this.attributeDefaultValueStrategy = attributeDefaultValueStrategy;
  }
}
