package com.mirakl.hybris.core.util;

import static de.hybris.platform.util.Config.getParameter;
import static java.lang.Double.parseDouble;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class HybrisVersionUtils {

  private HybrisVersionUtils() {}

  public static class VersionChecker {

    private double buildVersion;
    private Double minimumVersion;
    private Double maximumVersion;

    private VersionChecker(String buildVersion) {
      String[] versionNumbers = buildVersion.split("\\.");
      this.buildVersion = parseDouble(String.format("%s.%s", versionNumbers[0], versionNumbers[1]));
    }

    public VersionChecker maximumVersion(double maximumVersion) {
      this.maximumVersion = maximumVersion;
      return this;
    }

    public VersionChecker minimumVersion(double minimumVersion) {
      this.minimumVersion = minimumVersion;
      return this;
    }

    public boolean isValid() {
      return (minimumVersion == null || buildVersion >= minimumVersion)
          && (maximumVersion == null || buildVersion <= maximumVersion);
    }
  }

  public static VersionChecker versionChecker() {
    return new VersionChecker(getParameter("build.version"));
  }

  public static VersionChecker versionChecker(String buildVersion) {
    return new VersionChecker(buildVersion);
  }

}
