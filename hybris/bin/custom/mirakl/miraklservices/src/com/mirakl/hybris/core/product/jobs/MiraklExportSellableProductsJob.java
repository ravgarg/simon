package com.mirakl.hybris.core.product.jobs;

import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.hybris.core.model.MiraklExportSellableProductsCronJobModel;
import com.mirakl.hybris.core.product.services.ProductExportService;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import java.io.IOException;

import static java.lang.String.format;

public class MiraklExportSellableProductsJob extends AbstractJobPerformable<MiraklExportSellableProductsCronJobModel> {

  private static final Logger LOG = Logger.getLogger(MiraklExportSellableProductsJob.class);

  protected ProductExportService productExportService;

  @Override
  public PerformResult perform(MiraklExportSellableProductsCronJobModel cronJob) {
    LOG.info("Started exporting products..");

    int exportedProductsCount = 0;
    try {
      String synchronizationFileName = cronJob.getSynchronizationFileName();
      if (cronJob.isFullExport() || cronJob.getLastExportDate() == null) {
        exportedProductsCount = productExportService.exportAllProducts(cronJob.getRootCategory(), cronJob.getRootBrandCategory(),
            cronJob.getBaseSite(), synchronizationFileName);
      } else {
        exportedProductsCount = productExportService.exportModifiedProducts(cronJob.getRootCategory(),
            cronJob.getRootBrandCategory(), cronJob.getBaseSite(), cronJob.getLastExportDate(), synchronizationFileName);
      }
    } catch (MiraklApiException | IOException e) {
      LOG.error("Exception occurred while exporting products", e);
      return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
    }

    LOG.info(format("Product export finished successfully. %s products synchronized.", exportedProductsCount));
    cronJob.setLastExportDate(cronJob.getStartTime());
    modelService.save(cronJob);

    return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
  }

  @Required
  public void setExportProductsService(ProductExportService productExportService) {
    this.productExportService = productExportService;
  }


}
