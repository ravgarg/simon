package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.BOOLEAN_VALUE_LIST_ID;
import static java.lang.String.format;

import java.util.*;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.MiraklExportCatalogConfig;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogContext;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandler;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandlerResolver;
import com.mirakl.hybris.core.catalog.strategies.ExportCoreAttributesStrategy;
import com.mirakl.hybris.core.enums.MiraklAttributeExportHeader;
import com.mirakl.hybris.core.enums.MiraklAttributeType;
import com.mirakl.hybris.core.enums.MiraklValueListExportHeader;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

public class DefaultExportCoreAttributesStrategy implements ExportCoreAttributesStrategy {

  private static final Logger LOG = Logger.getLogger(DefaultExportCoreAttributesStrategy.class);

  protected CoreAttributeHandlerResolver coreAttributeHandlerResolver;

  @Override
  public void exportCoreAttributes(MiraklExportCatalogContext context) {
    Set<MiraklCoreAttributeModel> coreAttributes = context.getExportConfig().getCoreAttributes();
    LOG.info(format("Started processing Core Attributes.. (found [%s])", coreAttributes.size()));

    for (MiraklCoreAttributeModel attribute : coreAttributes) {
      exportAttribute(attribute, context);
      exportValues(attribute, context);
    }
  }

  protected void exportAttribute(MiraklCoreAttributeModel attribute, MiraklExportCatalogContext context) {
    if (!context.getExportConfig().isExportAttributes()) {
      return;
    }
    writeAttributeLine(attribute, context);
    context.removeMiraklAttributeCode(
        Pair.of(attribute.getCode(), attribute.getCategory() != null ? attribute.getCategory().getCode() : ""));
  }

  protected void writeAttributeLine(MiraklCoreAttributeModel attribute, MiraklExportCatalogContext context) {
    try {
      context.getWriter().writeAttribute(buildAttributeLine(context, attribute));
    } catch (Exception e) {
      LOG.error(format("Unable to export core attribute [%s]", attribute.getCode()), e);
    }
  }

  protected Map<String, String> buildAttributeLine(MiraklExportCatalogContext context, MiraklCoreAttributeModel attribute) {
    MiraklExportCatalogConfig exportConfig = context.getExportConfig();

    Map<String, String> line = new HashMap<>();
    line.put(MiraklAttributeExportHeader.CODE.getCode(), attribute.getCode());
    line.put(MiraklAttributeExportHeader.LABEL.getCode(), attribute.getLabel(exportConfig.getDefaultLocale()));
    line.put(MiraklAttributeExportHeader.DESCRIPTION.getCode(), attribute.getDescription(exportConfig.getDefaultLocale()));
    line.put(MiraklAttributeExportHeader.REQUIRED.getCode(), Boolean.toString(attribute.isRequired()));
    line.put(MiraklAttributeExportHeader.VARIANT.getCode(), Boolean.toString(attribute.isVariant()));
    line.put(MiraklAttributeExportHeader.DEFAULT_VALUE.getCode(), attribute.getDefaultValue());

    if (attribute.getCategory() != null) {
      line.put(MiraklAttributeExportHeader.HIERARCHY_CODE.getCode(), attribute.getCategory().getCode());
    }

    if (attribute.getType() == MiraklAttributeType.BOOLEAN) {
      line.put(MiraklAttributeExportHeader.TYPE.getCode(), MiraklAttributeType.LIST.getCode());
      line.put(MiraklAttributeExportHeader.TYPE_PARAMETER.getCode(), BOOLEAN_VALUE_LIST_ID);
    } else {
      line.put(MiraklAttributeExportHeader.TYPE.getCode(), attribute.getType().getCode());
      line.put(MiraklAttributeExportHeader.TYPE_PARAMETER.getCode(), attribute.getEffectiveTypeParameter());
    }

    for (Locale additionalLocale : exportConfig.getAdditionalLocales()) {
      line.put(MiraklAttributeExportHeader.LABEL.getCode(additionalLocale), attribute.getLabel(additionalLocale));
      line.put(MiraklAttributeExportHeader.DESCRIPTION.getCode(additionalLocale), attribute.getDescription(additionalLocale));
    }

    return line;
  }

  protected void exportValues(MiraklCoreAttributeModel attribute, MiraklExportCatalogContext context) {
    if (!context.getExportConfig().isExportValueLists()
        || !EnumSet.of(MiraklAttributeType.LIST, MiraklAttributeType.LIST_MULTIPLE_VALUES, MiraklAttributeType.MULTIPLE)
            .contains(attribute.getType())) {
      return;
    }

    CoreAttributeHandler<MiraklCoreAttributeModel> valueHandler = coreAttributeHandlerResolver.determineHandler(attribute);
    for (Map<String, String> line : valueHandler.getValues(attribute, context)) {
      writeValueListLine(context, line);
      context.removeMiraklValueCode(Pair.of(line.get(MiraklValueListExportHeader.VALUE_CODE.getCode()),
          line.get(MiraklValueListExportHeader.LIST_CODE.getCode())));
    }
  }

  protected void writeValueListLine(MiraklExportCatalogContext context, Map<String, String> line) {
    try {
      context.getWriter().writeAttributeValue(line);
    } catch (Exception e) {
      LOG.error(format("Unable to export core attribute value [%s]", line), e);
    }
  }

  @Required
  public void setCoreAttributeHandlerResolver(CoreAttributeHandlerResolver coreAttributeHandlerResolver) {
    this.coreAttributeHandlerResolver = coreAttributeHandlerResolver;
  }
}
