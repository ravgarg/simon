package com.mirakl.hybris.core.product.populators;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mmp.domain.product.synchro.MiraklProductSynchroResult;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.model.MiraklJobReportModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import org.springframework.beans.factory.annotation.Required;

import java.util.Map;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class ProductExportReportPopulator implements Populator<MiraklProductSynchroResult, MiraklJobReportModel> {

  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  public void populate(MiraklProductSynchroResult synchroResult, MiraklJobReportModel miraklJobReport)
      throws ConversionException {
    miraklJobReport.setLinesRead(synchroResult.getLinesRead());
    miraklJobReport.setLinesInError(synchroResult.getLinesInError());
    miraklJobReport.setLinesInSuccess(synchroResult.getLinesInSuccess());
    miraklJobReport.setStatus(exportStatuses.get(synchroResult.getStatus()));
    miraklJobReport.setItemsDeleted(synchroResult.getProductDeleted());
    miraklJobReport.setItemsInserted(synchroResult.getProductInserted());
    miraklJobReport.setItemsUpdated(synchroResult.getProductUpdated());
    miraklJobReport.setHasErrorReport(synchroResult.hasErrorReport());
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatues) {
    this.exportStatuses = exportStatues;
  }
}
