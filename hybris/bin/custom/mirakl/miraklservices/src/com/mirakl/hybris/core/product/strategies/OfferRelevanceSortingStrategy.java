package com.mirakl.hybris.core.product.strategies;

import com.mirakl.hybris.core.model.OfferModel;

import java.util.List;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 *
 * Strategy allowing to sort the offers by relevance for the client. This strategy is used for example in the offers tab
 */

public interface OfferRelevanceSortingStrategy {

  /**
   * Sorts the offers
   * 
   * @param offerList
   * @return The offers sorted
   */
  List<OfferModel> sort(List<OfferModel> offerList);
}
