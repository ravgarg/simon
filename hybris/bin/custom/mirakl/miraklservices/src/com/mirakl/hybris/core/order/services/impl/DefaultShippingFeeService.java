package com.mirakl.hybris.core.order.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.mirakl.client.mmp.domain.shipping.MiraklShippingTypeWithConfiguration;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFee;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeError;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeOffer;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees;
import com.mirakl.client.mmp.front.domain.shipping.MiraklShippingFeeType;
import com.mirakl.hybris.core.order.services.ShippingFeeService;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

public class DefaultShippingFeeService implements ShippingFeeService {

  protected JsonMarshallingService jsonMarshallingService;

  @Override
  public Optional<MiraklOrderShippingFeeOffer> extractShippingFeeOffer(final String offerId,
      MiraklOrderShippingFees shippingFees) {
    validateParameterNotNullStandardMessage("offerId", offerId);
    validateParameterNotNullStandardMessage("shippingFees", shippingFees);

    return Iterables.tryFind(extractAllShippingFeeOffers(shippingFees), new Predicate<MiraklOrderShippingFeeOffer>() {
      @Override
      public boolean apply(MiraklOrderShippingFeeOffer offer) {
        return offerId.equals(offer.getId());
      }
    });
  }

  @Override
  public Optional<MiraklOrderShippingFeeError> extractShippingFeeError(final String offerId,
      MiraklOrderShippingFees shippingFees) {
    validateParameterNotNullStandardMessage("offerId", offerId);
    validateParameterNotNullStandardMessage("shippingFees", shippingFees);

    return Iterables.tryFind(shippingFees.getErrors(), new Predicate<MiraklOrderShippingFeeError>() {
      @Override
      public boolean apply(MiraklOrderShippingFeeError shippingFeeError) {
        return offerId.equals(shippingFeeError.getOfferId());
      }
    });
  }

  @Override
  public MiraklOrderShippingFees getStoredShippingFees(AbstractOrderModel order) {
    validateParameterNotNullStandardMessage("order", order);

    return jsonMarshallingService.fromJson(order.getShippingFeesJSON(), MiraklOrderShippingFees.class);
  }

  @Override
  public Optional<MiraklOrderShippingFee> extractShippingFeeForShop(MiraklOrderShippingFees shippingFees, final String shopId,
      final Integer leadTimeToShip) {
    validateParameterNotNullStandardMessage("shippingFees", shippingFees);
    validateParameterNotNullStandardMessage("shopId", shopId);
    validateParameterNotNullStandardMessage("leadTimeToShip", leadTimeToShip);

    return Iterables.tryFind(shippingFees.getOrders(), new Predicate<MiraklOrderShippingFee>() {
      @Override
      public boolean apply(MiraklOrderShippingFee shippingFee) {
        return shopId.equals(shippingFee.getShopId()) && leadTimeToShip.equals(shippingFee.getLeadtimeToShip());
      }
    });
  }

  @Override
  public List<MiraklOrderShippingFeeOffer> extractAllShippingFeeOffers(MiraklOrderShippingFees shippingFees) {
    validateParameterNotNullStandardMessage("shippingFees", shippingFees);

    List<MiraklOrderShippingFeeOffer> allShippingOffers = Lists.newArrayList();

    for (MiraklOrderShippingFee miraklOrderShippingFee : shippingFees.getOrders()) {
      allShippingOffers.addAll(miraklOrderShippingFee.getOffers());
    }
    return allShippingOffers;
  }

  @Override
  public void updateSelectedShippingOption(MiraklOrderShippingFee shippingFee, final String shippingOptionCode) {
    validateParameterNotNullStandardMessage("shippingFee", shippingFee);
    validateParameterNotNullStandardMessage("shippingOptionCode", shippingOptionCode);

    Optional<MiraklShippingFeeType> miraklShippingFeeType =
        Iterables.tryFind(shippingFee.getShippingTypes(), new Predicate<MiraklShippingFeeType>() {
          @Override
          public boolean apply(MiraklShippingFeeType shipping) {
            return shippingOptionCode.equals(shipping.getCode());
          }
        });
    if (miraklShippingFeeType.isPresent()) {
      shippingFee.setSelectedShippingType(miraklShippingFeeType.get());
    }
  }

  @Override
  public List<AbstractOrderEntryModel> setLineShippingDetails(AbstractOrderModel order, MiraklOrderShippingFees shippingRates) {
    validateParameterNotNullStandardMessage("shippingRates", shippingRates);

    return setLineShippingDetails(order, shippingRates.getOrders());
  }


  @Override
  public List<AbstractOrderEntryModel> setLineShippingDetails(AbstractOrderModel order,
      List<MiraklOrderShippingFee> orderShippingFees) {
    validateParameterNotNullStandardMessage("order", order);
    validateParameterNotNullStandardMessage("orderShippingFees", orderShippingFees);

    List<AbstractOrderEntryModel> updatedEntries = new ArrayList<>();

    for (AbstractOrderEntryModel orderEntry : order.getMarketplaceEntries()) {
      String entryOfferId = orderEntry.getOfferId();
      for (MiraklOrderShippingFee shippingFee : orderShippingFees) {
        Optional<MiraklOrderShippingFeeOffer> miraklOfferOptional = getOfferFromShippingFee(shippingFee, entryOfferId);
        if (miraklOfferOptional.isPresent()) {
          MiraklOrderShippingFeeOffer miraklOffer = miraklOfferOptional.get();
          orderEntry.setLineShippingPrice(miraklOffer.getLineShippingPrice().doubleValue());
          MiraklShippingTypeWithConfiguration selectedShippingType = shippingFee.getSelectedShippingType();
          orderEntry.setLineShippingCode(selectedShippingType.getCode());
          orderEntry.setLineShippingLabel(selectedShippingType.getLabel());
          updatedEntries.add(orderEntry);
        }
      }
    }

    return updatedEntries;
  }

  protected Optional<MiraklOrderShippingFeeOffer> getOfferFromShippingFee(MiraklOrderShippingFee shippingFee,
      final String offerId) {
    return Iterables.tryFind(shippingFee.getOffers(), new Predicate<MiraklOrderShippingFeeOffer>() {
      @Override
      public boolean apply(MiraklOrderShippingFeeOffer miraklOffer) {
        return offerId.equals(miraklOffer.getId());
      }
    });
  }

  @Override
  public String getShippingFeesAsJson(MiraklOrderShippingFees miraklOrderShippingFees) {
    validateParameterNotNullStandardMessage("miraklOrderShippingFees", miraklOrderShippingFees);

    return jsonMarshallingService.toJson(miraklOrderShippingFees, MiraklOrderShippingFees.class);
  }

  @Required
  public void setJsonMarshallingService(JsonMarshallingService jsonMarshallingService) {
    this.jsonMarshallingService = jsonMarshallingService;
  }
}
