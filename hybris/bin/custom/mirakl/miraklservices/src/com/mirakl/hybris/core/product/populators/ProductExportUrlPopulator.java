package com.mirakl.hybris.core.product.populators;

import static com.mirakl.hybris.core.enums.MiraklProductExportHeader.PRODUCT_URL;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.acceleratorservices.urlresolver.SiteBaseUrlResolutionService;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.site.BaseSiteService;

public class ProductExportUrlPopulator implements Populator<ProductModel, Map<String, String>> {

  protected UrlResolver<ProductModel> productModelUrlResolver;
  protected SiteBaseUrlResolutionService siteBaseUrlResolutionService;
  protected BaseSiteService baseSiteService;
  protected boolean secure;

  @Override
  public void populate(ProductModel source, Map<String, String> target) throws ConversionException {
    target.put(PRODUCT_URL.getCode(), siteBaseUrlResolutionService.getWebsiteUrlForSite(baseSiteService.getCurrentBaseSite(),
        secure, productModelUrlResolver.resolve(source)));
  }

  @Required
  public void setProductModelUrlResolver(UrlResolver<ProductModel> productModelUrlResolver) {
    this.productModelUrlResolver = productModelUrlResolver;
  }

  @Required
  public void setBaseSiteService(BaseSiteService baseSiteService) {
    this.baseSiteService = baseSiteService;
  }

  @Required
  public void setSiteBaseUrlResolutionService(SiteBaseUrlResolutionService siteBaseUrlResolutionService) {
    this.siteBaseUrlResolutionService = siteBaseUrlResolutionService;
  }

  @Required
  public void setSecure(boolean secure) {
    this.secure = secure;
  }

}
