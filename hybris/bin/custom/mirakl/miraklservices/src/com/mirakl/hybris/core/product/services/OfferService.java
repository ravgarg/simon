package com.mirakl.hybris.core.product.services;

import java.util.List;

import com.mirakl.hybris.core.model.OfferModel;

import com.mirakl.hybris.core.product.services.impl.DefaultOfferService;
import de.hybris.platform.core.model.c2l.CurrencyModel;

/**
 * Service providing access and control on the Offers imported from Mirakl
 */
public interface OfferService {

  /**
   * Returns the offer with the specified id.
   *
   * @param offerId the id of the offer
   * @return the offer with the specified id.
   * @throws de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException if no Offer with the specified id is found
   *
   */
  OfferModel getOfferForId(String offerId);

  /**
   * Returns the offers for the given product code, sorted using the {@link DefaultOfferService#sortingStrategy}
   *
   * @param productCode The code of the product
   * @return A list of sorted offers
   */
  List<OfferModel> getSortedOffersForProductCode(String productCode);

  /**
   * Checks if there are any offers associated with the product code. Search restrictions apply.
   *
   * @param productCode unique code of the product
   * @return true if any offers found
   */
  boolean hasOffers(String productCode);

  /**
   * Checks if there are any offers associated with the product code. Search restrictions apply.
   *
   * @param productCode unique code of the product
   * @param currency the currency of the searched offers
   * @return true if any offers found
   */
  boolean hasOffersWithCurrency(String productCode, CurrencyModel currency);
}
