package com.mirakl.hybris.core.util.services;


/**
 * Service handling marshalling and unmarshalling object to and from JSON
 * 
 */
public interface JsonMarshallingService {

  /**
   * Transforms JSON to an object of a specific type
   * 
   * @param objectToUnmarshal object to unmarshal
   * @param type tagret Class
   * @return unmarshalled object
   */
  <T> T fromJson(String objectToUnmarshal, Class<T> type);

  /**
   * Transforms an object of a specific type to JSON
   * 
   * @param objectToMarshal object to marshal
   * @param type source Class
   * @return marshalled JSON
   */
  <T> String toJson(T objectToMarshal, Class<T> type);
}
