package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CATALOG_EXPORT_DATE_FORMAT;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.PRODUCTS_IMPORT_VALUES_SEPARATOR;
import static java.lang.String.format;
import static org.apache.commons.lang3.BooleanUtils.isTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.ClassificationAttributeOwnerStrategy;
import com.mirakl.hybris.core.catalog.strategies.ClassificationAttributeUpdateStrategy;
import com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationClassesResolverStrategy;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultClassificationAttributeUpdateStrategy implements ClassificationAttributeUpdateStrategy {

  private static final Logger LOG = Logger.getLogger(DefaultClassificationAttributeUpdateStrategy.class);

  protected ClassificationService classificationService;
  protected ConfigurationService configurationService;
  protected ModelService modelService;
  protected ClassificationClassesResolverStrategy classificationClassesResolverStrategy;
  protected ClassificationAttributeOwnerStrategy classificationAttributeOwnerStrategy;

  @Override
  public void updateAttributes(Collection<AttributeValueData> attributeValues, ProductImportData data,
      ProductImportFileContextData context) throws ProductImportException {

    Map<ProductModel, FeatureList> featuresByProduct = new HashMap<>();
    for (AttributeValueData attributeValue : attributeValues) {
      setFeatureValues(attributeValue, data, featuresByProduct, context);
    }

    for (Entry<ProductModel, FeatureList> entry : featuresByProduct.entrySet()) {
      classificationService.replaceFeatures(entry.getKey(), entry.getValue());
    }
  }

  protected void setFeatureValues(AttributeValueData attributeValue, ProductImportData data,
      Map<ProductModel, FeatureList> featuresByProduct, ProductImportFileContextData context) throws ProductImportException {
    ClassAttributeAssignmentModel assignment = getClassAttributeAssignment(attributeValue.getCode(), data, context);

    if (assignment == null) {
      // Attribute not defined for this product
      return;
    }

    ProductModel ownerProduct = classificationAttributeOwnerStrategy.determineOwner(assignment, data, context);
    FeatureList featureList = featuresByProduct.get(ownerProduct);
    if (featureList == null) {
      featureList = classificationService.getFeatures(ownerProduct);
      featuresByProduct.put(ownerProduct, featureList);
    }
    setValue(ownerProduct, attributeValue, featureList, assignment, data);
    data.getModelsToSave().add(ownerProduct);
  }

  protected void setValue(ProductModel product, AttributeValueData attributeValue, FeatureList features,
      ClassAttributeAssignmentModel assignment, ProductImportData data) throws ProductImportException {
    Feature feature = features.getFeatureByAssignment(assignment);

    if (feature == null) {
      LOG.warn(format("Unable to find feature for attribute [%s] in product [%s]",
          assignment.getClassificationAttribute().getCode(), product.getCode()));
      return;
    }

    try {
      setFeatureValue(feature, attributeValue, assignment);
      classificationService.setFeature(product, feature);
    } catch (ParseException e) {
      throw new ProductImportException(data.getRawProduct(),
          format("Unable to parse date for attribute [%s]. Expected format [%s]",
              assignment.getClassificationAttribute().getCode(), getDateFormat()));
    }
  }

  protected void setFeatureValue(Feature feature, AttributeValueData attributeValue, ClassAttributeAssignmentModel assignment)
      throws ParseException {
    removeFeatureValues(feature, attributeValue.getLocale());
    if (attributeValue.getValue() == null) {
      return;
    }

    List<FeatureValue> featureValues = new ArrayList<>();
    if (isTrue(assignment.getMultiValued())) {
      addFeatureValueMultivalued(featureValues, attributeValue, assignment);
    } else {
      addFeatureValue(featureValues, attributeValue.getValue(), assignment);
    }

    if (feature instanceof LocalizedFeature) {
      ((LocalizedFeature) feature).setValues(featureValues, attributeValue.getLocale());
    } else {
      feature.setValues(featureValues);
    }
  }


  protected void removeFeatureValues(Feature feature, Locale locale) {
    if (feature instanceof LocalizedFeature && locale != null) {
      ((LocalizedFeature) feature).removeAllValues(locale);
    } else {
      feature.removeAllValues();
    }
  }

  protected void addFeatureValueMultivalued(List<FeatureValue> featureValues, AttributeValueData attributeValue,
      ClassAttributeAssignmentModel assignment) throws ParseException {
    String[] values =
        attributeValue.getValue().split(configurationService.getConfiguration().getString(PRODUCTS_IMPORT_VALUES_SEPARATOR));
    for (String value : values) {
      addFeatureValue(featureValues, value.trim(), assignment);
    }
  }

  protected void addFeatureValue(List<FeatureValue> featureValues, String receivedValue, ClassAttributeAssignmentModel assignment)
      throws ParseException {
    Object convertedValue = convertFeatureValue(assignment, receivedValue);
    if (convertedValue == null) {
      handleUnresolvedValue(assignment, receivedValue);
      return;
    }
    featureValues.add(new FeatureValue(convertedValue, null, assignment.getUnit()));
  }

  protected void handleUnresolvedValue(ClassAttributeAssignmentModel assignment, String receivedValue) {
    LOG.warn(String.format("Unable to resolve value [%s] for attribute [%s-%s]", receivedValue,
        assignment.getClassificationAttribute().getCode(), assignment.getClassificationClass().getCode()));
  }

  protected Object convertFeatureValue(final ClassAttributeAssignmentModel assignment, final String stringValue)
      throws ParseException {
    switch (assignment.getAttributeType()) {
      case STRING:
        return stringValue;
      case BOOLEAN:
        return Boolean.valueOf(stringValue);
      case NUMBER:
        return Double.valueOf(stringValue);
      case DATE:
        return new SimpleDateFormat(getDateFormat()).parse(stringValue);
      case ENUM:
        return FluentIterable.from(assignment.getAttributeValues())
            .firstMatch(new Predicate<ClassificationAttributeValueModel>() {

              @Override
              public boolean apply(ClassificationAttributeValueModel attributeValue) {
                return attributeValue.getCode().equals(stringValue);
              }
            }).orNull();
      default:
        LOG.warn(format("Unknown attribute type for attribute [%s] on class [%s]",
            assignment.getClassificationAttribute().getCode(), assignment.getClassificationClass().getCode()));
        return null;
    }
  }

  protected String getDateFormat() {
    return configurationService.getConfiguration().getString(CATALOG_EXPORT_DATE_FORMAT, "dd-MM-yyyy");
  }

  protected ClassAttributeAssignmentModel getClassAttributeAssignment(final String attributeQualifier, ProductImportData data,
      ProductImportFileContextData context) {
    final Set<ClassificationClassModel> classificationClasses =
        classificationClassesResolverStrategy.resolve(getProductCategory(data.getRootBaseProductToUpdate(), context));
    List<ClassAttributeAssignmentModel> allClassAttributeAssignments =
        classificationClassesResolverStrategy.getAllClassAttributeAssignments(classificationClasses);

    return FluentIterable.from(allClassAttributeAssignments).firstMatch(new Predicate<ClassAttributeAssignmentModel>() {

      @Override
      public boolean apply(ClassAttributeAssignmentModel attributeAssignment) {
        return attributeQualifier.equals(attributeAssignment.getClassificationAttribute().getCode());
      }
    }).orNull();
  }

  protected CategoryModel getProductCategory(ProductModel product, ProductImportFileContextData context) {
    final Set<PK> allProductCategories = getAllProductCategories(context);
    return FluentIterable.from(product.getSupercategories()).firstMatch(new Predicate<CategoryModel>() {

      @Override
      public boolean apply(CategoryModel category) {
        return allProductCategories.contains(category.getPk());
      }
    }).orNull();

  }

  protected Set<PK> getAllProductCategories(ProductImportFileContextData context) {
    MiraklCategoryCoreAttributeModel coreCategoryAttribute =
        (MiraklCategoryCoreAttributeModel) modelService.get(context.getGlobalContext().getCategoryRoleAttribute());

    return context.getGlobalContext().getAllCategoryValues().get(coreCategoryAttribute.getUid());
  }

  @Required
  public void setClassificationService(ClassificationService classificationService) {
    this.classificationService = classificationService;
  }

  @Required
  public void setConfigurationService(ConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setClassificationClassesResolverStrategy(
      ClassificationClassesResolverStrategy classificationClassesResolverStrategy) {
    this.classificationClassesResolverStrategy = classificationClassesResolverStrategy;
  }

  @Required
  public void setClassificationAttributeOwnerStrategy(ClassificationAttributeOwnerStrategy classificationAttributeOwnerStrategy) {
    this.classificationAttributeOwnerStrategy = classificationAttributeOwnerStrategy;
  }

}
