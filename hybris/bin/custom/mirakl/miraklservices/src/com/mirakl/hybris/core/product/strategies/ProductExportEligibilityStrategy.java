package com.mirakl.hybris.core.product.strategies;

import java.util.Collection;
import java.util.Date;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

public interface ProductExportEligibilityStrategy {

  Collection<ProductModel> getModifiedProductsEligibleForExport(CatalogVersionModel catalogVersion, Date modifiedAfter);

  Collection<ProductModel> getAllProductsEligibleForExport(CatalogVersionModel catalogVersion);

}
