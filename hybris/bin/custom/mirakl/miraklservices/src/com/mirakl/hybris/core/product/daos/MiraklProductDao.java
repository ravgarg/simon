package com.mirakl.hybris.core.product.daos;

import java.util.Date;
import java.util.List;

import com.mirakl.hybris.core.model.ShopModel;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

public interface MiraklProductDao extends GenericDao<ProductModel> {

  /**
   * Finds products with no variant type modified after a given date (if provided), or all products otherwise.
   * 
   * @param modifiedAfter the earliest date of modification allowed (can be null)
   * @param catalogVersion the catalog version containing the products
   * @return a list a products (can be empty)
   */
  List<ProductModel> findModifiedProductsWithNoVariantType(Date modifiedAfter, CatalogVersionModel catalogVersion);

  /**
   * Finds products sold by the given shop with the requested variant group code
   *
   * @param shop the shop selling the product
   * @param variantGroupCode the variant group code given by the shop to the product
   * @param catalogVersion the catalog version to search in
   * @return a product
   */
  ProductModel findProductForShopVariantGroupCode(ShopModel shop, String variantGroupCode, CatalogVersionModel catalogVersion);

}
