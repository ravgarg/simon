package com.mirakl.hybris.core.shop.services;

import com.mirakl.hybris.core.model.ShopModel;

import java.util.Collection;
import java.util.Date;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 *
 * A Service allowing to import shops from Mirakl, providing incremental or full import. Implementation of Mirakl S20 API.
 */
public interface ShopImportService {

  /**
   * Incremental import of the Mirakl shops
   * 
   * @param updatedSince Date of the last incremental update.
   * @return a <tt>Collection</tt> containing the imported shops
   */
  Collection<ShopModel> importShopsUpdatedSince(Date updatedSince);

  /**
   * Import all the shops from Mirakl
   * 
   * @return a <tt>Collection</tt> containing the imported shops
   */
  Collection<ShopModel> importAllShops();
}
