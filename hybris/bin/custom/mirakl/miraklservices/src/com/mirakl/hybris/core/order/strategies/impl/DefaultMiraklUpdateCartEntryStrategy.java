package com.mirakl.hybris.core.order.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static java.lang.String.format;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.order.strategies.CommonMiraklCartStrategy;
import com.mirakl.hybris.core.product.services.OfferService;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceUpdateCartEntryStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

public class DefaultMiraklUpdateCartEntryStrategy extends DefaultCommerceUpdateCartEntryStrategy {

  private static final Logger LOG = Logger.getLogger(DefaultMiraklUpdateCartEntryStrategy.class);

  protected CommonMiraklCartStrategy commonCartStrategy;

  protected OfferService offerService;

  @Override
  public CommerceCartModification updateQuantityForCartEntry(final CommerceCartParameter parameters)
      throws CommerceCartModificationException {
    final CartModel cartModel = parameters.getCart();
    validateParameterNotNull(cartModel, "Cart model cannot be null");

    final long newQuantity = parameters.getQuantity();
    final long entryNumber = parameters.getEntryNumber();
    final AbstractOrderEntryModel entryToUpdate = getEntryForNumber(cartModel, (int) entryNumber);

    if (entryToUpdate.getOfferId() == null) {
      return super.updateQuantityForCartEntry(parameters);
    }

    beforeUpdateCartEntry(parameters);
    validateEntryBeforeModification(newQuantity, entryToUpdate);

    final Integer maxOrderQuantity = entryToUpdate.getProduct().getMaxOrderQuantity();
    // Work out how many we want to add (could be negative if we are removing items)
    final long quantityToAdd = newQuantity - entryToUpdate.getQuantity().longValue();

    final long actualAllowedQuantityChange = getAllowedAdjustmentForOffer(cartModel, entryToUpdate, quantityToAdd);
    CommerceCartModification modification =
        modifyEntry(cartModel, entryToUpdate, actualAllowedQuantityChange, newQuantity, maxOrderQuantity);
    afterUpdateCartEntry(parameters, modification);
    return modification;
  }

  protected long getAllowedAdjustmentForOffer(final CartModel cartModel, final AbstractOrderEntryModel entryToUpdate,
      final long quantityToAdd) {
    try {
      OfferModel offer = offerService.getOfferForId(entryToUpdate.getOfferId());
      final long actualAllowedQuantityChange =
          commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, entryToUpdate.getProduct(), offer, quantityToAdd);
      return actualAllowedQuantityChange;
    } catch (UnknownIdentifierException e) {
      LOG.error(format("Unable to retrieve offer [%s]. It may have been disabled or deleted", entryToUpdate.getOfferId()), e);
      return 0L;
    }
  }


  @Required
  public void setOfferService(OfferService offerService) {
    this.offerService = offerService;
  }

  @Required
  public void setCommonCartStrategy(CommonMiraklCartStrategy commonCartStrategy) {
    this.commonCartStrategy = commonCartStrategy;
  }

}
