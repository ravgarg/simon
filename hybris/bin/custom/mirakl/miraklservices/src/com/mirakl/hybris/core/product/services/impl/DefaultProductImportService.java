package com.mirakl.hybris.core.product.services.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.PRODUCTS_IMPORT_ALREADY_RECEIVED_MESSAGE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;

import java.util.Collection;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportErrorData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.beans.ProductImportSuccessData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.services.ProductImportService;
import com.mirakl.hybris.core.product.services.ShopSkuService;
import com.mirakl.hybris.core.product.strategies.PostProcessProductLineImportStrategy;
import com.mirakl.hybris.core.product.strategies.ProductCreationStrategy;
import com.mirakl.hybris.core.product.strategies.ProductIdentificationStrategy;
import com.mirakl.hybris.core.product.strategies.ProductImportCredentialCheckStrategy;
import com.mirakl.hybris.core.product.strategies.ProductImportValidationStrategy;
import com.mirakl.hybris.core.product.strategies.ProductUpdateStrategy;
import com.mirakl.hybris.core.shop.services.ShopService;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.L10NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

public class DefaultProductImportService implements ProductImportService {

  private static final Logger LOG = Logger.getLogger(DefaultProductImportService.class);

  protected ModelService modelService;
  protected ShopService shopService;
  protected ShopSkuService shopSkuService;
  protected L10NService l10nService;
  protected ProductIdentificationStrategy productIdentificationStrategy;
  protected ProductImportValidationStrategy productImportValidationStrategy;
  protected ProductCreationStrategy productCreationStrategy;
  protected ProductUpdateStrategy productUpdateStrategy;
  protected ProductImportCredentialCheckStrategy credentialCheckStrategy;
  protected PostProcessProductLineImportStrategy postProcessProductLineImportStrategy;
  protected Converter<Pair<MiraklRawProductModel, ProductImportFileContextData>, ProductImportData> productImportDataConverter;
  protected Converter<ProductImportException, ProductImportErrorData> errorDataConverter;

  @Override
  public void importProducts(Collection<MiraklRawProductModel> variants, ProductImportFileContextData context) {
    validate(variants, context);

    for (MiraklRawProductModel variant : variants) {
      try {
        if (alreadyReceived(variant, context)) {
          writeSuccessToResultQueue(variant, getAlreadyReceivedMessage(context), context);
          continue;
        }
        validateProduct(variant, context);
        ProductImportData data = productImportDataConverter.convert(Pair.of(variant, context));
        identifyProduct(data);

        ProductModel productToUpdate = data.getIdentifiedProduct();
        if (productToUpdate == null) {
          productToUpdate = createProduct(data, context);
        }

        data.setProductToUpdate(productToUpdate);
        data.setRootBaseProductToUpdate(getRootBaseProduct(productToUpdate));

        applyReceivedValues(data, context);
        postProcessProductLineImport(data, variant, context);
        modelService.saveAll(data.getModelsToSave());
        writeSuccessToResultQueue(variant, context);

      } catch (ProductImportException e) {
        LOG.error(format("An error occurred during product import. Raw line: [%s]", variant.getValues()), e);
        writeErrorToResultQueue(e, context);
      } catch (Exception e) {
        LOG.error(format("Unable to import product. Raw line: [%s]", variant.getValues()), e);
        writeErrorToResultQueue(new ProductImportException(variant, e), context);
      }
    }
  }

  protected void validate(Collection<MiraklRawProductModel> variants, ProductImportFileContextData context) {
    validateParameterNotNullStandardMessage("context", context);
    ProductImportGlobalContextData globalContext = context.getGlobalContext();
    validateParameterNotNullStandardMessage("globalContext", globalContext);
    validateParameterNotNullStandardMessage("productCatalogVersion", globalContext.getProductCatalogVersion());
    validateParameterNotNullStandardMessage("variantAttributesPerType", globalContext.getVariantAttributesPerType());
    validateParameterNotNullStandardMessage("variantTypeHierarchyPerType", globalContext.getVariantTypeHierarchyPerType());
  }

  protected ProductModel createProduct(ProductImportData data, ProductImportFileContextData context)
      throws ProductImportException {
    credentialCheckStrategy.checkProductCreationCredentials(data, context);
    return productCreationStrategy.createProduct(data, context);
  }

  protected boolean alreadyReceived(MiraklRawProductModel rawProduct, ProductImportFileContextData context) {
    CatalogVersionModel catalogVersion = modelService.get(context.getGlobalContext().getProductCatalogVersion());
    ShopModel shop = shopService.getShopForId(context.getShopId());
    return shopSkuService.getShopSkuForChecksum(rawProduct.getChecksum(), shop, catalogVersion) != null;
  }

  protected void validateProduct(MiraklRawProductModel miraklRawProduct, ProductImportFileContextData context)
      throws ProductImportException {
    productImportValidationStrategy.validate(miraklRawProduct, context);
  }

  protected void identifyProduct(ProductImportData productImportData) throws ProductImportException {
    productIdentificationStrategy.identifyProduct(productImportData);
  }


  protected void applyReceivedValues(ProductImportData data, ProductImportFileContextData context) throws ProductImportException {
    credentialCheckStrategy.checkProductUpdateCredentials(data, context);
    productUpdateStrategy.applyValues(data, context);
  }

  protected void writeErrorToResultQueue(ProductImportException exception, ProductImportFileContextData context) {
    try {
      context.getImportResultQueue().put(errorDataConverter.convert(exception));
    } catch (InterruptedException e) {
      LOG.warn(format("Unable to write to the error queue. Line value: [%s], Line number: [%s]",
          exception.getRawProduct().getValues(), exception.getRawProduct().getRowNumber()), e);
      Thread.currentThread().interrupt();
    }
  }


  protected void writeSuccessToResultQueue(MiraklRawProductModel rawProduct, ProductImportFileContextData context) {
    writeSuccessToResultQueue(rawProduct, null, context);
  }

  protected void writeSuccessToResultQueue(MiraklRawProductModel rawProduct, String message,
      ProductImportFileContextData context) {
    try {
      ProductImportSuccessData successData = new ProductImportSuccessData();
      successData.setAdditionalMessage(message);
      successData.setLineValues(rawProduct.getValues());
      successData.setRowNumber(rawProduct.getRowNumber());
      context.getImportResultQueue().put(successData);
    } catch (InterruptedException e) {
      LOG.warn(format("Unable to write to the success queue. Line value: [%s], Line number: [%s]", rawProduct.getValues(),
          rawProduct.getRowNumber()), e);
      Thread.currentThread().interrupt();
    }
  }

  protected void postProcessProductLineImport(ProductImportData data, MiraklRawProductModel variant,
      ProductImportFileContextData context) {
    postProcessProductLineImportStrategy.postProcess(data, variant, context);
  }

  protected ProductModel getRootBaseProduct(ProductModel identifiedProduct) {
    ProductModel rootBaseProduct = identifiedProduct;
    while (rootBaseProduct instanceof VariantProductModel) {
      rootBaseProduct = ((VariantProductModel) rootBaseProduct).getBaseProduct();
    }
    return rootBaseProduct;
  }

  protected String getAlreadyReceivedMessage(ProductImportFileContextData context) {
    return l10nService.getLocalizedString(PRODUCTS_IMPORT_ALREADY_RECEIVED_MESSAGE);
  }

  @Required
  public void setShopSkuService(ShopSkuService shopSkuService) {
    this.shopSkuService = shopSkuService;
  }

  @Required
  public void setShopService(ShopService shopService) {
    this.shopService = shopService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setL10nService(L10NService l10nService) {
    this.l10nService = l10nService;
  }

  @Required
  public void setProductCreationStrategy(ProductCreationStrategy productCreationStrategy) {
    this.productCreationStrategy = productCreationStrategy;
  }

  @Required
  public void setProductIdentificationStrategy(ProductIdentificationStrategy productIdentificationStrategy) {
    this.productIdentificationStrategy = productIdentificationStrategy;
  }

  @Required
  public void setProductImportValidationStrategy(ProductImportValidationStrategy productImportValidationStrategy) {
    this.productImportValidationStrategy = productImportValidationStrategy;
  }

  @Required
  public void setProductUpdateStrategy(ProductUpdateStrategy productUpdateStrategy) {
    this.productUpdateStrategy = productUpdateStrategy;
  }

  @Required
  public void setCredentialCheckStrategy(ProductImportCredentialCheckStrategy setCredentialCheckStrategy) {
    this.credentialCheckStrategy = setCredentialCheckStrategy;
  }

  @Required
  public void setPostProcessProductLineImportStrategy(PostProcessProductLineImportStrategy postProcessProductLineImportStrategy) {
    this.postProcessProductLineImportStrategy = postProcessProductLineImportStrategy;
  }

  @Required
  public void setProductImportDataConverter(
      Converter<Pair<MiraklRawProductModel, ProductImportFileContextData>, ProductImportData> productImportDataConverter) {
    this.productImportDataConverter = productImportDataConverter;
  }

  @Required
  public void setErrorDataConverter(Converter<ProductImportException, ProductImportErrorData> errorDataConverter) {
    this.errorDataConverter = errorDataConverter;
  }

}
