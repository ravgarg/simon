package com.mirakl.hybris.core.ordersplitting.daos;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;

public interface MarketplaceConsignmentDao {

  /**
   * Returns the Mirakl consignment with the specified code.
   * 
   * @param code the code of the consignment
   * @return the consignment with the specified code.
   * 
   */
  MarketplaceConsignmentModel findMarketplaceConsignmentByCode(String code);

}
