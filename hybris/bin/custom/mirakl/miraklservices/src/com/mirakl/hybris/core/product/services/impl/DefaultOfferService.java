package com.mirakl.hybris.core.product.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.daos.OfferDao;
import com.mirakl.hybris.core.product.services.OfferService;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.core.product.strategies.OfferRelevanceSortingStrategy;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

public class DefaultOfferService implements OfferService {

  protected OfferDao offerDao;
  protected OfferRelevanceSortingStrategy sortingStrategy;
  protected OfferCodeGenerationStrategy offerCodeGenerationStrategy;
  protected CommonI18NService commonI18NService;

  @Override
  public OfferModel getOfferForId(String offerId) {
    validateParameterNotNullStandardMessage("offerId", offerId);

    OfferModel offer = offerDao.findOfferById(offerId);
    if (offer == null) {
      throw new UnknownIdentifierException(format("No offer having for id [%s] can be found.", offerId));
    }

    return offer;
  }

  @Override
  public List<OfferModel> getSortedOffersForProductCode(String productCode) {
    List<OfferModel> offersModifiableList = new ArrayList<>();
    CurrencyModel currentCurrency = commonI18NService.getCurrentCurrency();
    offersModifiableList.addAll(offerDao.findOffersForProductCodeAndCurrency(productCode, currentCurrency));
    offersModifiableList = sortingStrategy.sort(offersModifiableList);
    return offersModifiableList;
  }

  @Override
  public boolean hasOffers(String productCode) {
    return offerDao.countOffersForProduct(productCode) > 0;
  }

  @Override
  public boolean hasOffersWithCurrency(String productCode, CurrencyModel currency) {
    return offerDao.countOffersForProductAndCurrency(productCode, currency) > 0;
  }

  @Required
  public void setOfferDao(OfferDao offerDao) {
    this.offerDao = offerDao;
  }

  @Required
  public void setSortingStrategy(OfferRelevanceSortingStrategy sortingStrategy) {
    this.sortingStrategy = sortingStrategy;
  }

  @Required
  public void setOfferCodeGenerationStrategy(OfferCodeGenerationStrategy offerCodeGenerationStrategy) {
    this.offerCodeGenerationStrategy = offerCodeGenerationStrategy;
  }

  @Required
  public void setCommonI18NService(CommonI18NService commonI18NService) {
    this.commonI18NService = commonI18NService;
  }


}
