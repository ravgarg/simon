package com.mirakl.hybris.core.catalog.services;

import java.io.IOException;

import com.mirakl.hybris.beans.MiraklExportCatalogResultData;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public interface MiraklExportCatalogService {

  /**
   * Exports the Hybris catalog to Mirakl. Depending on a given configuration, it is able to export Catalog Categories, Attributes
   * or/and Value Lists.
   * 
   * @param context the export context
   * @return the result containing the tracking ids of the different exports
   * @throws IOException
   */
  MiraklExportCatalogResultData export(MiraklExportCatalogContext context) throws IOException;
}
