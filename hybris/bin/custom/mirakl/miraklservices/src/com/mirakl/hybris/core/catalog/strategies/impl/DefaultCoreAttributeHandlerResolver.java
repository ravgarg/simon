package com.mirakl.hybris.core.catalog.strategies.impl;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandler;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandlerResolver;
import com.mirakl.hybris.core.enums.MiraklAttributeRole;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

import de.hybris.platform.core.Registry;

@SuppressWarnings({"rawtypes", "unchecked"})
public class DefaultCoreAttributeHandlerResolver implements CoreAttributeHandlerResolver {

  protected Map<MiraklAttributeRole, CoreAttributeHandler> roleAttributeHandlers;
  protected CoreAttributeHandler defaultHandler;

  @Override
  public <T extends MiraklCoreAttributeModel> CoreAttributeHandler<T> determineHandler(MiraklCoreAttributeModel attribute,
      ProductImportData data, ProductImportFileContextData context) {
    return determineHandler(attribute);
  }


  @Override
  public <T extends MiraklCoreAttributeModel> CoreAttributeHandler<T> determineHandler(MiraklCoreAttributeModel attribute) {
    if (isNotBlank(attribute.getImportExportHandlerStringId())) {
      return getCoreAttributeBeanHandler(attribute);
    }

    if (attribute.getRole() != null && roleAttributeHandlers.containsKey(attribute.getRole())) {
      return roleAttributeHandlers.get(attribute.getRole());
    }

    return defaultHandler;
  }

  protected <T extends MiraklCoreAttributeModel> CoreAttributeHandler<T> getCoreAttributeBeanHandler(
      MiraklCoreAttributeModel attribute) {
    return Registry.getApplicationContext().getBean(attribute.getImportExportHandlerStringId(), CoreAttributeHandler.class);
  }

  @Required
  public void setRoleAttributeHandlers(Map<MiraklAttributeRole, CoreAttributeHandler> roleAttributeHandlers) {
    this.roleAttributeHandlers = roleAttributeHandlers;
  }

  @Required
  public void setDefaultHandler(CoreAttributeHandler defaultHandler) {
    this.defaultHandler = defaultHandler;
  }

}
