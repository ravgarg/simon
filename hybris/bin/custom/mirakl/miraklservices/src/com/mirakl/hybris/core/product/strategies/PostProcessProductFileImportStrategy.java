package com.mirakl.hybris.core.product.strategies;

import java.io.IOException;

import com.mirakl.hybris.beans.ProductImportFileContextData;

public interface PostProcessProductFileImportStrategy {

  /**
   * Called after a product is imported. Can be used to add custom logic after the import
   * 
   * @param context the file import context
   * @param importId ther importId
 * @throws IOException 
   */
  void postProcess(ProductImportFileContextData context, String importId) throws IOException;

}
