package com.mirakl.hybris.core.catalog.strategies;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

public interface CoreAttributeHandlerResolver {

  /**
   * Determines the spring bean handling the import or the export of the given {@link MiraklCoreAttributeModel}
   *
   * @param attribute the {@link MiraklCoreAttributeModel} to get the handler from
   * @param <T> the core attribute model extending {@link MiraklCoreAttributeModel} (ie:
   *        {@link com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel})
   * @return the handler for the given {@link MiraklCoreAttributeModel}
   */
  <T extends MiraklCoreAttributeModel> CoreAttributeHandler<T> determineHandler(MiraklCoreAttributeModel attribute);

  <T extends MiraklCoreAttributeModel> CoreAttributeHandler<T> determineHandler(MiraklCoreAttributeModel attribute,
                                                                                ProductImportData data, ProductImportFileContextData context);
} 
