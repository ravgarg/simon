package com.mirakl.hybris.core.product.services.impl;

import static com.mirakl.hybris.core.enums.MiraklAttributeRole.SHOP_SKU_ATTRIBUTE;
import static com.mirakl.hybris.core.enums.MiraklAttributeRole.VARIANT_GROUP_CODE_ATTRIBUTE;
import static java.lang.String.format;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.services.MiraklRawProductImportService;
import com.mirakl.hybris.core.util.services.CsvService;
import com.mirakl.hybris.core.util.strategies.ChecksumCalculationStrategy;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.model.ModelService;
import shaded.org.supercsv.io.CsvMapReader;
import shaded.org.supercsv.io.ICsvMapReader;

public class DefaultMiraklRawProductImportService implements MiraklRawProductImportService {

  private static final Logger LOG = Logger.getLogger(DefaultMiraklRawProductImportService.class);

  protected CsvService csvService;
  protected ModelService modelService;
  protected ChecksumCalculationStrategy checksumCalculationStrategy;
  protected Populator<String[], ProductImportFileContextData> headerFileContextPopulator;

  @Override
  public String importRawProducts(File inputFile, ProductImportFileContextData context) {
    String importId = UUID.randomUUID().toString();
    try (FileReader fileReader = new FileReader(inputFile);
        ICsvMapReader csvMapReader = new CsvMapReader(fileReader, csvService.getDefaultCsvPreference())) {
      final String[] header = csvMapReader.getHeader(true);
      headerFileContextPopulator.populate(header, context);

      Map<String, String> productValues;
      while ((productValues = csvMapReader.read(header)) != null) {
        MiraklRawProductModel rawProduct = createRawProduct(productValues, csvMapReader, context, importId);
        modelService.save(rawProduct);
      }
      if (LOG.isDebugEnabled()) {
        LOG.debug(format("Parsing of file [%s] done. Import Id [%s]", inputFile, importId));
      }

    } catch (FileNotFoundException e) {
      LOG.error(format("Unable to find file [%s]", inputFile), e);
    } catch (IOException e) {
      LOG.error(format("Error on parsing file [%s]", inputFile), e);
    }

    return importId;
  }

  protected MiraklRawProductModel createRawProduct(Map<String, String> productValues, ICsvMapReader csvMapReader,
      ProductImportFileContextData context, String importUUID) {
    MiraklRawProductModel rawProduct = modelService.create(MiraklRawProductModel.class);
    String rawData = csvMapReader.getUntokenizedRow();
    rawProduct.setRawData(rawData);
    rawProduct.setChecksum(checksumCalculationStrategy.calculateChecksum(rawData));
    rawProduct.setValues(productValues);
    rawProduct.setRowNumber(csvMapReader.getLineNumber());
    rawProduct.setShopId(context.getShopId());
    rawProduct.setImportId(importUUID);
    rawProduct.setSku(productValues.get(getShopSkuAttributeCode(context)));
    if (getVariantGroupAttributeCode(context) != null) {
      rawProduct.setVariantGroupCode(productValues.get(getVariantGroupAttributeCode(context)));
    }
    modelService.save(rawProduct);

    return rawProduct;
  }

  protected String getShopSkuAttributeCode(ProductImportFileContextData context) {
    return context.getGlobalContext().getCoreAttributePerRole().get(SHOP_SKU_ATTRIBUTE);
  }

  protected String getVariantGroupAttributeCode(ProductImportFileContextData context) {
    return context.getGlobalContext().getCoreAttributePerRole().get(VARIANT_GROUP_CODE_ATTRIBUTE);
  }

  @Required
  public void setCsvService(CsvService csvService) {
    this.csvService = csvService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setChecksumCalculationStrategy(ChecksumCalculationStrategy checksumCalculationStrategy) {
    this.checksumCalculationStrategy = checksumCalculationStrategy;
  }

  @Required
  public void setHeaderFileContextPopulator(Populator<String[], ProductImportFileContextData> headerFileContextPopulator) {
    this.headerFileContextPopulator = headerFileContextPopulator;
  }
}
