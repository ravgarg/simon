package com.mirakl.hybris.core.order.services.impl;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Lists.newArrayList;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.isBlank;

import java.util.List;

import com.mirakl.client.mmp.domain.evaluation.MiraklAssessment;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Predicate;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.domain.order.create.MiraklOfferNotShippable;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklCreateOrderRequest;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklValidOrderRequest;
import com.mirakl.hybris.core.order.services.MiraklOrderService;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultMiraklOrderService implements MiraklOrderService {

  private static final Logger LOG = Logger.getLogger(DefaultMiraklOrderService.class);

  protected ModelService modelService;
  protected JsonMarshallingService jsonMarshallingService;
  protected MiraklMarketplacePlatformFrontApiClient miraklApi;
  protected Converter<OrderModel, MiraklCreateOrder> miraklCreateOrderConverter;

  @Override
  public MiraklCreatedOrders createMarketplaceOrders(OrderModel order) {
    validateParameterNotNullStandardMessage("order", order);

    MiraklCreateOrder miraklCreateOrder = miraklCreateOrderConverter.convert(order);
    MiraklCreateOrderRequest request = new MiraklCreateOrderRequest(miraklCreateOrder);

    LOG.info(format("Sending order [%s] to Mirakl..", order.getCode()));

    MiraklCreatedOrders createdOrders = miraklApi.createOrder(request);
    storeCreatedOrders(order, createdOrders);

    return createdOrders;
  }

  @Override
  public List<AbstractOrderEntryModel> extractNotShippableEntries(final List<MiraklOfferNotShippable> notShippableOffers,
      OrderModel order) {
    validateParameterNotNullStandardMessage("notShippableOffers", notShippableOffers);
    validateParameterNotNullStandardMessage("order", order);

    return newArrayList(filter(order.getMarketplaceEntries(), new Predicate<AbstractOrderEntryModel>() {
      @Override
      public boolean apply(AbstractOrderEntryModel orderEntry) {
        for (MiraklOfferNotShippable nonShippableOffer : notShippableOffers) {
          if (nonShippableOffer.getId().equals(orderEntry.getOfferId())) {
            return true;
          }
        }
        return false;
      }
    }));
  }

  @Override
  public String storeCreatedOrders(OrderModel order, MiraklCreatedOrders createdOrders) {
    validateParameterNotNullStandardMessage("createdOrders", createdOrders);
    validateParameterNotNullStandardMessage("order", order);

    String serializedOrders = jsonMarshallingService.toJson(createdOrders, MiraklCreatedOrders.class);
    order.setCreatedOrdersJSON(serializedOrders);
    modelService.save(order);

    return serializedOrders;
  }

  @Override
  public MiraklCreatedOrders loadCreatedOrders(OrderModel order) {
    validateParameterNotNullStandardMessage("order", order);

    if (isBlank(order.getCreatedOrdersJSON())) {
      if (LOG.isDebugEnabled()) {
        LOG.debug(format("No marketplace orders stored for commercial order id [%s].", order.getCode()));
      }
      return null;
    }

    return jsonMarshallingService.fromJson(order.getCreatedOrdersJSON(), MiraklCreatedOrders.class);
  }

  @Override
  public void validateOrder(OrderModel order) {
    validateParameterNotNullStandardMessage("order", order);

    if (LOG.isDebugEnabled()) {
      LOG.debug(format("Sending validation for order [%s].", order.getCode()));
    }
    miraklApi.validOrder(new MiraklValidOrderRequest(order.getCode()));
  }

  @Override
  public List<MiraklAssessment> getAssessments() {
    return miraklApi.getAssessments();
  }

  @Required
  public void setMiraklApi(MiraklMarketplacePlatformFrontApiClient miraklApi) {
    this.miraklApi = miraklApi;
  }

  @Required
  public void setMiraklCreateOrderConverter(Converter<OrderModel, MiraklCreateOrder> miraklCreateOrderConverter) {
    this.miraklCreateOrderConverter = miraklCreateOrderConverter;
  }

  @Required
  public void setJsonMarshallingService(JsonMarshallingService jsonMarshallingService) {
    this.jsonMarshallingService = jsonMarshallingService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }


}
