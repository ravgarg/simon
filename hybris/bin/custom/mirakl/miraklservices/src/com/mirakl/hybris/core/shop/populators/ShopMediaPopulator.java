package com.mirakl.hybris.core.shop.populators;

import static java.lang.String.format;
import static org.apache.commons.io.IOUtils.toByteArray;

import java.io.IOException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.shop.MiraklMediaInformation;
import com.mirakl.client.mmp.domain.shop.MiraklShop;
import com.mirakl.hybris.core.model.ShopModel;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

public class ShopMediaPopulator implements Populator<MiraklShop, ShopModel> {

  private static final Logger LOG = Logger.getLogger(ShopMediaPopulator.class);

  private MediaService mediaService;

  private ModelService modelService;

  @Override
  public void populate(MiraklShop source, ShopModel target) throws ConversionException {

    MiraklMediaInformation shopMediaInformation = source.getMediaInformation();
    if (shopMediaInformation == null) {
      LOG.debug(format("No media information for shop [%s]", source.getId()));
      return;
    }

    if (shopMediaInformation.getLogo() != null) {
      MediaModel logo = downloadMediaForShop(source.getId(), shopMediaInformation.getLogo());
      target.setLogo(logo);
    }
    if (shopMediaInformation.getBanner() != null) {
      MediaModel banner = downloadMediaForShop(source.getId(), shopMediaInformation.getBanner());
      target.setBanner(banner);
    }
  }

  protected MediaModel downloadMediaForShop(String shopId, String mediaUrl) {
    try {
      URL url = new URL(mediaUrl);
      CatalogUnawareMediaModel media = getMediaModel(format("%s-%s", shopId, url.getFile()));
      modelService.save(media);
      downloadMediaFromURL(media, url);
      return media;
    } catch (IOException e) {
      LOG.error(format("Unable to download a media for shop. Shop Id : [%s], Media URL : [%s]", shopId, mediaUrl), e);
      return null;
    }
  }

  protected CatalogUnawareMediaModel getMediaModel(String mediaCode) {
    try {
      return (CatalogUnawareMediaModel) mediaService.getMedia(mediaCode);
    } catch (UnknownIdentifierException e) {
      CatalogUnawareMediaModel newMediaModel = modelService.create(CatalogUnawareMediaModel.class);
      newMediaModel.setCode(mediaCode);
      return newMediaModel;
    }
  }

  protected void downloadMediaFromURL(MediaModel media, URL url) throws IOException {
    mediaService.setDataForMedia(media, toByteArray(url.openStream()));
  }

  @Required
  public void setMediaService(MediaService mediaService) {
    this.mediaService = mediaService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

}
