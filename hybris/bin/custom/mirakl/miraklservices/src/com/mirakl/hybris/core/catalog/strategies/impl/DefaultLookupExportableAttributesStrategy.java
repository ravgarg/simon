package com.mirakl.hybris.core.catalog.strategies.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.catalog.events.ExportableAttributeEvent;
import com.mirakl.hybris.core.catalog.events.ExportableCategoryEvent;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogContext;
import com.mirakl.hybris.core.catalog.strategies.LookupExportableAttributesStrategy;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.event.EventService;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultLookupExportableAttributesStrategy implements LookupExportableAttributesStrategy {

  protected EventService eventService;

  @Override
  public void handleEvent(ExportableCategoryEvent event) {
    lookupExportableAttributes(event.getCategory(), event.getContext());
  }

  protected void lookupExportableAttributes(CategoryModel currentCategory, MiraklExportCatalogContext context) {
    for (CategoryModel superCategory : currentCategory.getSupercategories()) {
      if (superCategory instanceof ClassificationClassModel) {
        ClassificationClassModel currentClassificationClass = (ClassificationClassModel) superCategory;
        if (context.getVisitedClassIds().add(currentClassificationClass.getCode())) {
          publishExportableAttributeEvents(currentClassificationClass, currentCategory, context);
          lookupExportableAttributes(currentClassificationClass, context);
        }
      }
    }
  }

  protected void publishExportableAttributeEvents(ClassificationClassModel currentClassificationClass,
      CategoryModel currentCategory, MiraklExportCatalogContext context) {
    List<ClassAttributeAssignmentModel> attributeAssignments =
        currentClassificationClass.getDeclaredClassificationAttributeAssignments();
    for (ClassAttributeAssignmentModel attributeAssignment : attributeAssignments) {
      eventService.publishEvent(new ExportableAttributeEvent(attributeAssignment, currentCategory, context));
    }
  }

  @Required
  public void setEventService(EventService eventService) {
    this.eventService = eventService;
  }
}
