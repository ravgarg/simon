package com.mirakl.hybris.core.jobs.strategies.impl;

import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.io.Files;
import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.enums.MiraklExportType;
import com.mirakl.hybris.core.jobs.dao.MiraklJobReportDao;
import com.mirakl.hybris.core.jobs.strategies.ExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

public abstract class AbstractExportReportStrategy<T> implements ExportReportStrategy {

  private static final Logger LOG = Logger.getLogger(AbstractExportReportStrategy.class);
  protected static final String ERROR_REPORT_MIME_TYPE = "text/csv";

  protected ModelService modelService;
  protected MiraklJobReportDao miraklJobReportDao;
  protected MediaService mediaService;

  @Override
  public boolean updatePendingExports() {
    List<MiraklJobReportModel> pendingExportReports = getPendingMiraklJobReports();
    List<MiraklJobReportModel> updatedReports = new ArrayList<>();
    boolean allReportsProcessedSuccessfully = true;

    for (MiraklJobReportModel pendingExportReport : pendingExportReports) {
      try {
        T exportResult = getExportResult(pendingExportReport.getJobId());

        if (isExportCompleted(exportResult)) {
          updatePendingReport(pendingExportReport, exportResult);
          updatedReports.add(pendingExportReport);
        }
      } catch (MiraklApiException e) {
        allReportsProcessedSuccessfully &= canHandleMiraklApiException(updatedReports, pendingExportReport, e);
      }
    }
    modelService.saveAll(updatedReports);

    return allReportsProcessedSuccessfully;
  }

  protected void updatePendingReport(MiraklJobReportModel pendingExportReport, T synchroResult) {
    getReportPopulator().populate(synchroResult, pendingExportReport);
    setErrorReport(pendingExportReport);
  }

  protected boolean canHandleMiraklApiException(List<MiraklJobReportModel> updatedReports,
      MiraklJobReportModel pendingExportReport, MiraklApiException exception) {
    if (isExportNotFound(exception.getError())) {
      LOG.info(format("Export with syncJobId [%s] not found", pendingExportReport.getJobId()));
      pendingExportReport.setStatus(MiraklExportStatus.NOT_FOUND);
      updatedReports.add(pendingExportReport);
      return true;
    }
    LOG.error(format("Exception occurred while updating export report [%s]", pendingExportReport.getJobId()), exception);
    return false;
  }

  protected boolean isExportNotFound(MiraklErrorResponseBean error) {
    return NOT_FOUND.getStatusCode() == error.getStatus();
  }

  protected void setErrorReport(MiraklJobReportModel report) {
    if (BooleanUtils.isTrue(report.getHasErrorReport())) {
      String jobId = report.getJobId();
      LOG.warn(format("Export with syncJobId [%s] ended with errors - setting error report", jobId));
      try {
        report.setErrorReport(createErrorReport(getErrorReportFile(jobId), jobId, report.getReportType()));
      } catch (IOException e) {
        LOG.error("Exception occurred while setting error report", e);
      }
    }
  }

  protected abstract File getErrorReportFile(String jobId) throws IOException;

  protected MediaModel createErrorReport(File errorReport, String jobId, MiraklExportType reportType) throws IOException {
    checkArgument(isNotBlank(jobId), "Cannot create error report - missing job ID");
    checkArgument(reportType != null, "Cannot create error report - missing report type");

    CatalogUnawareMediaModel mediaErrorReport = modelService.create(CatalogUnawareMediaModel.class);
    mediaErrorReport.setCode(format("%s-%s", reportType, jobId));
    mediaErrorReport.setRealFileName(errorReport.getName());
    mediaErrorReport.setMime(ERROR_REPORT_MIME_TYPE);

    modelService.save(mediaErrorReport);
    mediaService.setStreamForMedia(mediaErrorReport, Files.asByteSource(errorReport).openStream());
    modelService.save(mediaErrorReport);
    return mediaErrorReport;
  }

  protected abstract T getExportResult(String syncJobId);

  protected abstract boolean isExportCompleted(T exportResult);

  protected abstract List<MiraklJobReportModel> getPendingMiraklJobReports();

  protected abstract Populator<T, MiraklJobReportModel> getReportPopulator();

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setMiraklJobReportDao(MiraklJobReportDao miraklJobReportDao) {
    this.miraklJobReportDao = miraklJobReportDao;
  }

  @Required
  public void setMediaService(MediaService mediaService) {
    this.mediaService = mediaService;
  }
}
