package com.mirakl.hybris.core.catalog.populators.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mci.front.domain.common.AbstractMiraklCatalogImportResult;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class MiraklCatalogExportReportPopulator implements Populator<AbstractMiraklCatalogImportResult, MiraklJobReportModel> {

  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  public void populate(AbstractMiraklCatalogImportResult synchroResult, MiraklJobReportModel miraklJobReport)
      throws ConversionException {
    miraklJobReport.setStatus(exportStatuses.get(synchroResult.getImportStatus()));
    miraklJobReport.setHasErrorReport(synchroResult.hasErrorReport());
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
