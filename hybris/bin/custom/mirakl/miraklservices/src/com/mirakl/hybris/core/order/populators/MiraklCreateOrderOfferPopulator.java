package com.mirakl.hybris.core.order.populators;

import static com.mirakl.client.core.internal.util.Preconditions.checkNotNull;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.order.tax.MiraklOrderTaxAmount;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderOffer;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.services.OfferService;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;

public class MiraklCreateOrderOfferPopulator implements Populator<AbstractOrderEntryModel, MiraklCreateOrderOffer> {

  protected OfferService offerService;
  protected Converter<TaxValue, MiraklOrderTaxAmount> miraklOrderTaxAmountConverter;

  @Override
  public void populate(AbstractOrderEntryModel orderEntry, MiraklCreateOrderOffer miraklCreateOrderOffer)
      throws ConversionException {
    checkNotNull(orderEntry);
    checkNotNull(miraklCreateOrderOffer);

    String offerId = orderEntry.getOfferId();
    checkNotNull(offerId);

    miraklCreateOrderOffer.setId(offerId);
    miraklCreateOrderOffer.setQuantity(orderEntry.getQuantity().intValue());

    setShippingInfo(miraklCreateOrderOffer, orderEntry);
    if (isTrue(orderEntry.getOrder().getNet())) {
      setTaxes(miraklCreateOrderOffer, orderEntry);
    }
    setOfferDetails(orderEntry, miraklCreateOrderOffer, offerId);
  }

  protected void setOfferDetails(AbstractOrderEntryModel orderEntry, MiraklCreateOrderOffer miraklCreateOrderOffer,
      String offerId) {
    OfferModel offer = offerService.getOfferForId(offerId);

    miraklCreateOrderOffer.setPriceUnit(offer.getPrice());
    miraklCreateOrderOffer.setPrice(getLinePrice(orderEntry, offer));
    miraklCreateOrderOffer.setCurrencyIsoCode(MiraklIsoCurrencyCode.valueOf(offer.getCurrency().getIsocode()));
    miraklCreateOrderOffer.setLeadtimeToShip(offer.getLeadTimeToShip());
  }

  protected void setTaxes(MiraklCreateOrderOffer miraklCreateOrderOffer, AbstractOrderEntryModel orderEntry) {
    miraklCreateOrderOffer.setTaxes(miraklOrderTaxAmountConverter.convertAll(orderEntry.getTaxValues()));
  }

  protected void setShippingInfo(MiraklCreateOrderOffer miraklCreateOrderOffer, AbstractOrderEntryModel orderEntry) {
    String lineShippingCode = orderEntry.getLineShippingCode();
    checkNotNull(lineShippingCode);

    miraklCreateOrderOffer.setShippingTypeCode(lineShippingCode);
    miraklCreateOrderOffer.setShippingPrice(BigDecimal.valueOf(orderEntry.getLineShippingPrice()));
  }

  protected BigDecimal getLinePrice(AbstractOrderEntryModel orderEntry, OfferModel offer) {
    return offer.getPrice().multiply(BigDecimal.valueOf(orderEntry.getQuantity()));
  }

  @Required
  public void setOfferService(OfferService offerService) {
    this.offerService = offerService;
  }

  @Required
  public void setMiraklOrderTaxAmountConverter(Converter<TaxValue, MiraklOrderTaxAmount> miraklOrderTaxAmountConverter) {
    this.miraklOrderTaxAmountConverter = miraklOrderTaxAmountConverter;
  }
}
