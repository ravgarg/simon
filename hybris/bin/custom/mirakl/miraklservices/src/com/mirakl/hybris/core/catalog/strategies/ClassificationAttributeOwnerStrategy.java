package com.mirakl.hybris.core.catalog.strategies;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportData;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.product.ProductModel;

public interface ClassificationAttributeOwnerStrategy {

  /**
   * Determines the owner of the given {@link ClassAttributeAssignmentModel}
   *
   * @param attributeAssignment the Attribute Assignment from which the product will be found
   * @param data the product related data
   * @param context the product import file context data
   * @return the {@link ProductModel} owning the {@link ClassAttributeAssignmentModel}
   */
  ProductModel determineOwner(ClassAttributeAssignmentModel attributeAssignment, ProductImportData data,
      ProductImportFileContextData context);

}
