package com.mirakl.hybris.core.setup;

import com.mirakl.hybris.core.constants.MiraklservicesConstants;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.List;

@SystemSetup(extension = MiraklservicesConstants.EXTENSIONNAME)
public class MiraklCoreSystemSetup extends AbstractSystemSetup {
  private static final String IMPEX_IMPORT_FOLDER = "/miraklservices/import";

  @Override
  @SystemSetupParameterMethod
  public List<SystemSetupParameter> getInitializationOptions() {
    final List<SystemSetupParameter> params = new ArrayList<>();
    return params;
  }

  @SystemSetup(type = Type.PROJECT, process = Process.ALL)
  public void createProjectData(final SystemSetupContext context) {
    importImpexFile(context, IMPEX_IMPORT_FOLDER + "/searchrestrictions.impex");
  }

}
