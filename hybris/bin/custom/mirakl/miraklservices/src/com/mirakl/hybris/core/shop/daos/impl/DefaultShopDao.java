package com.mirakl.hybris.core.shop.daos.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.text.MessageFormat.format;
import static java.util.Collections.singletonMap;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

public class DefaultShopDao extends DefaultGenericDao<ShopModel> implements ShopDao {

  public static final String SHOPS_FOR_PRODUCT_CODE_QUERY = "SELECT {s." + ShopModel.PK + "} FROM {" + OfferModel._TYPECODE
      + " AS o JOIN " + ShopModel._TYPECODE + " AS s ON {s." + ShopModel.PK + "} = {o." + OfferModel.SHOP + "} } WHERE {o."
      + OfferModel.PRODUCTCODE + "} = ?" + OfferModel.PRODUCTCODE + " GROUP BY {s." + ShopModel.PK + "}";

  public DefaultShopDao() {
    super(ShopModel._TYPECODE);
  }

  @Override
  public ShopModel findShopById(String shopId) {
    validateParameterNotNullStandardMessage("shopId", shopId);

    List<ShopModel> shopModels = find(singletonMap(ShopModel.ID, shopId));

    if (isNotEmpty(shopModels) && shopModels.size() > 1) {
      throw new AmbiguousIdentifierException(format("Multiple shops found for id [{0}]", shopId));
    }
    return isEmpty(shopModels) ? null : shopModels.get(0);
  }

  @Override
  public Collection<ShopModel> findShopsForProductCode(String productCode) {
    validateParameterNotNull("productCode", productCode);

    FlexibleSearchQuery query =
        new FlexibleSearchQuery(SHOPS_FOR_PRODUCT_CODE_QUERY, Collections.singletonMap(OfferModel.PRODUCTCODE, productCode));
    SearchResult<ShopModel> searchResult = getFlexibleSearchService().search(query);

    return searchResult.getResult();
  }
}
