package com.mirakl.hybris.core.product.strategies.impl;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.strategies.ProductImportCredentialCheckStrategy;

public class DefaultProductImportCredentialCheckStrategy implements ProductImportCredentialCheckStrategy {

  @Override
  public void checkProductCreationCredentials(ProductImportData data, ProductImportFileContextData context)
      throws ProductImportException {
    if (!isProductCreationAllowed(data, context)) {
      throw new ProductImportException(data.getRawProduct(), "Creation is forbidden for this product");
    }
  }

  @Override
  public void checkProductUpdateCredentials(ProductImportData data, ProductImportFileContextData context)
      throws ProductImportException {
    if (!isProductUpdateAllowed(data, context)) {
      throw new ProductImportException(data.getRawProduct(), "Update is forbidden for this product");
    }
  }

  protected boolean isProductCreationAllowed(ProductImportData data, ProductImportFileContextData context) {
    return true;
  }

  protected boolean isProductUpdateAllowed(ProductImportData data, ProductImportFileContextData context) {
    return true;
  }

}
