package com.mirakl.hybris.core.payment.strategies.impl;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.payment.debit.MiraklOrderPayment;
import com.mirakl.hybris.core.enums.MarketplaceConsignmentPaymentStatus;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.mirakl.hybris.core.payment.events.DebitRequestReceivedEvent;
import com.mirakl.hybris.core.payment.strategies.DebitRequestEventHandlingStrategy;

import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultDebitRequestEventHandlingStrategy implements DebitRequestEventHandlingStrategy {

  protected MarketplaceConsignmentService marketplaceConsignmentService;

  protected ModelService modelService;

  @Override
  public void handleEvent(DebitRequestReceivedEvent event) {
    MarketplaceConsignmentModel consignment =
        marketplaceConsignmentService.getMarketplaceConsignmentForCode(event.getDebitRequest().getOrderId());
    storeDebitRequest(consignment, event.getDebitRequest());
  }

  protected void storeDebitRequest(MarketplaceConsignmentModel consignment, MiraklOrderPayment orderPayment) {
    marketplaceConsignmentService.storeDebitRequest(orderPayment);
    consignment.setPaymentStatus(MarketplaceConsignmentPaymentStatus.INITIAL);
    modelService.save(consignment);
  }

  @Required
  public void setMarketplaceConsignmentService(MarketplaceConsignmentService marketplaceConsignmentService) {
    this.marketplaceConsignmentService = marketplaceConsignmentService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

}
