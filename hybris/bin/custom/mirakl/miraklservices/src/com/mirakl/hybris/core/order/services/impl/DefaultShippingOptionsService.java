package com.mirakl.hybris.core.order.services.impl;

import static com.google.common.base.Preconditions.checkState;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFee;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeError;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeOffer;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees;
import com.mirakl.client.mmp.front.request.shipping.MiraklGetShippingRatesRequest;
import com.mirakl.client.mmp.front.request.shipping.MiraklOfferQuantityShippingTypeTuple;
import com.mirakl.hybris.core.order.services.ShippingFeeService;
import com.mirakl.hybris.core.order.services.ShippingOptionsService;
import com.mirakl.hybris.core.order.strategies.ShippingZoneStrategy;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultShippingOptionsService implements ShippingOptionsService {

  protected MiraklMarketplacePlatformFrontApi miraklOperatorApi;
  protected ShippingFeeService shippingFeeService;
  protected ModelService modelService;
  protected ShippingZoneStrategy shippingZoneStrategy;

  @Override
  public void setShippingOptions(AbstractOrderModel order) {
    validateParameterNotNull(order, "AbstractOrder cannot be null for the shipping rates request");

    List<MiraklOfferQuantityShippingTypeTuple> offerTuples = getOfferTuples(order);
    if (isEmpty(offerTuples)) {
      if (isNotBlank(order.getShippingFeesJSON())) {
        overwriteStoredShippingFees(order, null);
      }
      return;
    }
    MiraklGetShippingRatesRequest request =
        new MiraklGetShippingRatesRequest(offerTuples, shippingZoneStrategy.getShippingZoneCode(order));

    MiraklOrderShippingFees shippingRates = miraklOperatorApi.getShippingRates(request);
    shippingFeeService.setLineShippingDetails(order, shippingRates);
    modelService.saveAll(order.getEntries());

    overwriteStoredShippingFees(order, shippingFeeService.getShippingFeesAsJson(shippingRates));
  }

  @Override
  public void setSelectedShippingOption(AbstractOrderModel order, String shippingOptionCode, Integer leadTimeToShip,
      String shopId) {
    validateParameterNotNullStandardMessage("order", order);
    validateParameterNotNullStandardMessage("shippingOptionCode", shippingOptionCode);
    validateParameterNotNullStandardMessage("leadTimeToShip", leadTimeToShip);
    validateParameterNotNullStandardMessage("order", shopId);

    MiraklOrderShippingFees miraklOrderShippingFees = shippingFeeService.getStoredShippingFees(order);
    checkState(miraklOrderShippingFees != null,
        format("Cannot set selected shipping option - no shipping fees found for order [%s]", order.getCode()));

    Optional<MiraklOrderShippingFee> shippingFee =
        shippingFeeService.extractShippingFeeForShop(miraklOrderShippingFees, shopId, leadTimeToShip);
    checkState(shippingFee.isPresent(),
        format("No shipping fee found for shop with id [%s] and lead time to ship [%s]", shopId, leadTimeToShip));

    shippingFeeService.updateSelectedShippingOption(shippingFee.get(), shippingOptionCode);
    order.setShippingFeesJSON(shippingFeeService.getShippingFeesAsJson(miraklOrderShippingFees));
    List<AbstractOrderEntryModel> updatedEntries =
        shippingFeeService.setLineShippingDetails(order, singletonList(shippingFee.get()));

    modelService.saveAll(updatedEntries);
    modelService.save(order);
  }

  @Override
  public void removeOfferEntriesWithError(AbstractOrderModel order, final List<MiraklOrderShippingFeeError> shippingFeeErrors) {

    Collection<AbstractOrderEntryModel> entriesToRemove =
        Collections2.filter(order.getMarketplaceEntries(), new Predicate<AbstractOrderEntryModel>() {
          @Override
          public boolean apply(AbstractOrderEntryModel entry) {
            for (MiraklOrderShippingFeeError shippingFeeError : shippingFeeErrors) {
              if (entry.getOfferId().equals(shippingFeeError.getOfferId())) {
                return true;
              }
            }
            return false;
          }
        });
    modelService.removeAll(entriesToRemove);
    modelService.refresh(order);
  }

  @Override
  public void adjustOfferQuantities(List<AbstractOrderEntryModel> orderEntries,
      List<MiraklOrderShippingFeeOffer> shippingFeeOffers) {
    List<AbstractOrderEntryModel> modifiedEntries = Lists.newArrayList();
    for (AbstractOrderEntryModel orderEntry : orderEntries) {
      for (MiraklOrderShippingFeeOffer shippingFeeOffer : shippingFeeOffers) {
        Optional<AbstractOrderEntryModel> modifiedEntry = processEntryQuantity(orderEntry, shippingFeeOffer);
        if (modifiedEntry.isPresent()) {
          modifiedEntries.add(modifiedEntry.get());
        }
      }
    }
    modelService.saveAll(modifiedEntries);
  }

  protected void overwriteStoredShippingFees(AbstractOrderModel order, String shippingFeesJSON) {
    order.setShippingFeesJSON(shippingFeesJSON);
    order.setCalculated(false);
    modelService.save(order);
  }

  protected Optional<AbstractOrderEntryModel> processEntryQuantity(AbstractOrderEntryModel orderEntry,
      MiraklOrderShippingFeeOffer shippingFeeOffer) {
    if (shippingFeeOffer.getId().equals(orderEntry.getOfferId()) && hasInsufficientQuantity(orderEntry, shippingFeeOffer)) {
      long newQuantity = getAvailableQuantity(shippingFeeOffer.getQuantity(), orderEntry.getQuantity());
      if (newQuantity > 0) {
        orderEntry.setQuantity(newQuantity);
        return Optional.of(orderEntry);
      } else {
        modelService.remove(orderEntry);
      }
    }
    return Optional.absent();
  }

  protected boolean hasInsufficientQuantity(AbstractOrderEntryModel orderEntry, MiraklOrderShippingFeeOffer shippingFeeOffer) {
    return shippingFeeOffer.getQuantity() < orderEntry.getQuantity();
  }

  protected long getAvailableQuantity(Integer offerQuantity, Long entryQuantity) {
    return Math.min(offerQuantity, entryQuantity);
  }

  protected List<MiraklOfferQuantityShippingTypeTuple> getOfferTuples(AbstractOrderModel order) {
    List<MiraklOfferQuantityShippingTypeTuple> offerTuples = new ArrayList<>();

    for (AbstractOrderEntryModel entry : order.getMarketplaceEntries()) {
      offerTuples.add(new MiraklOfferQuantityShippingTypeTuple(entry.getOfferId(), entry.getQuantity().intValue(),
          entry.getLineShippingCode()));
    }
    return offerTuples;
  }

  @Required
  public void setMiraklOperatorApi(MiraklMarketplacePlatformFrontApi miraklOperatorApi) {
    this.miraklOperatorApi = miraklOperatorApi;
  }

  @Required
  public void setShippingFeeService(ShippingFeeService shippingFeeService) {
    this.shippingFeeService = shippingFeeService;
  }

  @Required
  public void setModelService(ModelService modelService) {
    this.modelService = modelService;
  }

  @Required
  public void setShippingZoneStrategy(ShippingZoneStrategy shippingZoneStrategy) {
    this.shippingZoneStrategy = shippingZoneStrategy;
  }
}
