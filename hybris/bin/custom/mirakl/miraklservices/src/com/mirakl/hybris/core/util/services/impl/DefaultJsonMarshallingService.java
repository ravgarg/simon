package com.mirakl.hybris.core.util.services.impl;


import java.io.IOException;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import shaded.org.codehaus.jackson.map.ObjectMapper;

public class DefaultJsonMarshallingService implements JsonMarshallingService {

  protected ObjectMapper mapper;

  @Override
  public <T> T fromJson(String objectToUnmarshal, Class<T> type) {
    try {
      if(objectToUnmarshal != null) {
        return mapper.reader(type).readValue(objectToUnmarshal);
      }
      return null;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public <T> String toJson(T objectToMarshal, Class<T> type) {
    try {
      return mapper.writeValueAsString(objectToMarshal);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Required
  public void setMapper(ObjectMapper mapper) {
    this.mapper = mapper;
  }
}
