package com.mirakl.hybris.core.order.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.strategies.calculation.impl.DefaultFindDeliveryCostStrategy;
import de.hybris.platform.util.PriceValue;

/**
 * Strategy returning total delivery cost of {@link AbstractOrderModel} including Mirakl line shipping options retrieved from
 * {@link com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees}
 */
public class MiraklFindDeliveryCostStrategy extends DefaultFindDeliveryCostStrategy {

  @Override
  public PriceValue getDeliveryCost(AbstractOrderModel order) {
    PriceValue deliveryCost = super.getDeliveryCost(order);
    return new PriceValue(deliveryCost.getCurrencyIso(), getTotalDeliveryCost(order, deliveryCost), deliveryCost.isNet());
  }

  protected double getTotalDeliveryCost(AbstractOrderModel order, PriceValue deliveryCost) {
    double totalDeliveryCost = deliveryCost.getValue();
    for (AbstractOrderEntryModel orderEntry : order.getEntries()) {
      totalDeliveryCost += orderEntry.getLineShippingPrice();
    }
    return totalDeliveryCost;
  }
}
