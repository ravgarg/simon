package com.mirakl.hybris.core.util;

import static java.lang.Math.ceil;
import static java.lang.Math.min;

import java.util.List;

import com.google.common.base.Preconditions;

public class PaginationUtils {

  private PaginationUtils() {
    // No instanciation
  }

  /**
   * Calculates the number of pages, given a total count and a page size
   * 
   * @param totalCount total count
   * @param pageSize page size
   * @return
   */
  public static final int getNumberOfPages(long totalCount, double pageSize) {
    return (int) ceil(totalCount / pageSize);
  }

  /**
   * Extracts a page form a collection. Page numbers are starting from 0.
   * 
   * @param pageNumber the page to return
   * @param pageSize page size
   * @param exportData the list from which the page will be extracted
   * @return
   */
  public static final <T> List<T> getPage(int pageNumber, int pageSize, List<T> exportData) {
    Preconditions.checkArgument(exportData != null);
    return exportData.subList(pageNumber * pageSize, min((pageNumber + 1) * pageSize, exportData.size()));
  }

}
