package com.mirakl.hybris.core.catalog.strategies.impl;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.core.catalog.strategies.ClassificationAttributeOwnerStrategy;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.product.ProductModel;

public class DefaultAcceleratorClassificationAttributeOwnerStrategy implements ClassificationAttributeOwnerStrategy {

  @Override
  public ProductModel determineOwner(ClassAttributeAssignmentModel attributeAssignment, ProductImportData data,
      ProductImportFileContextData context) {
    return data.getProductToUpdate();
  }


}
