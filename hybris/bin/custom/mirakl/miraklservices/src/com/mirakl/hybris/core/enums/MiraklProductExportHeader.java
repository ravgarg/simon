package com.mirakl.hybris.core.enums;

import java.util.List;
import java.util.Locale;

public enum MiraklProductExportHeader implements MiraklHeader {
  PRODUCT_SKU("product-sku"), //
  PRODUCT_DESCRIPTION("product-description"), //
  PRODUCT_TITLE("product-title"), //
  CATEGORY_CODE("category-code"), //
  ACTIVE("active"), //
  PRODUCT_REFERENCES("product-references"), //
  BRAND("brand"), //
  PRODUCT_URL("product-url"), //
  MEDIA_URL("media-url"), //
  VARIANT_GROUP_CODE("variant-group-code"), //
  SHOP_SKUS("shop-skus");

  private String code;
  private boolean localizable;

  private MiraklProductExportHeader(String code) {
    this.code = code;
  }

  private MiraklProductExportHeader(String code, boolean localizable) {
    this.localizable = localizable;
    this.code = code;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getCode(Locale locale) {
    return MiraklHeaderUtils.getCode(this, locale);
  }

  public static String[] codes() {
    return MiraklHeaderUtils.codes(values());
  }

  public static String[] codes(List<Locale> locales) {
    return MiraklHeaderUtils.codes(values(), locales);
  }

  @Override
  public MiraklHeader[] getValues() {
    return values();
  }

  @Override
  public boolean isLocalizable() {
    return localizable;
  }

}
