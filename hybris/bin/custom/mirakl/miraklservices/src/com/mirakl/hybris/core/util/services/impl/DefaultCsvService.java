package com.mirakl.hybris.core.util.services.impl;

import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static java.lang.String.format;
import static org.fest.util.Arrays.isEmpty;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.log4j.Logger;

import com.mirakl.hybris.core.util.services.CsvService;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import shaded.org.supercsv.exception.SuperCsvConstraintViolationException;
import shaded.org.supercsv.io.CsvMapWriter;
import shaded.org.supercsv.io.ICsvMapWriter;
import shaded.org.supercsv.prefs.CsvPreference;
import shaded.org.supercsv.quote.AlwaysQuoteMode;

public class DefaultCsvService implements CsvService {

  private static final Logger LOG = Logger.getLogger(DefaultCsvService.class);

  @Override
  public String createCsvWithHeaders(String[] header, List<Map<String, String>> lines) throws IOException {
    checkArgument(!isEmpty(header), "Header array cannot be empty for CSV file");
    ServicesUtil.validateParameterNotNull(lines, "Value lines cannot be empty for CSV file");

    int fails = 0;
    int successes = 0;
    try (StringBuilderWriter writer = new StringBuilderWriter();
        ICsvMapWriter csvMapWriter = new CsvMapWriter(writer, getDefaultCsvPreference())) {
      csvMapWriter.writeHeader(header);

      for (Map<String, String> line : lines) {
        try {
          csvMapWriter.write(line, header);
          successes++;
        } catch (SuperCsvConstraintViolationException e) {
          LOG.error(format("CSV Constraint Violation occurred on line :[%s]", line), e);
          fails++;
        }
      }
      LOG.info(format("Finished writing CSV content. [%s successful lines] / [%s lines in error]", successes, fails));
      csvMapWriter.flush();

      return writer.toString();

    }
  }

  @Override
  public CsvPreference getDefaultCsvPreference() {
    return new CsvPreference.Builder(CsvPreference.EXCEL_NORTH_EUROPE_PREFERENCE)//
        .useQuoteMode(new AlwaysQuoteMode()) //
        .surroundingSpacesNeedQuotes(true) //
        .build();
  }

  @Override
  public boolean writeLine(ICsvMapWriter mapWriter, String[] header, Map<String, String> line) throws IOException {
    try {
      mapWriter.write(line, header);
    } catch (SuperCsvConstraintViolationException e) {
      LOG.error(format("CSV Constraint Violation occurred on line :[%s]", line), e);
      return false;
    }
    return true;
  }

}
