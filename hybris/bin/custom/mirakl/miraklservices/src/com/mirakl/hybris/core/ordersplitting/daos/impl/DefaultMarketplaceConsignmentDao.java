package com.mirakl.hybris.core.ordersplitting.daos.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static java.util.Collections.singletonMap;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.List;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.daos.MarketplaceConsignmentDao;

import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

public class DefaultMarketplaceConsignmentDao extends DefaultGenericDao<MarketplaceConsignmentModel>
    implements MarketplaceConsignmentDao {

  public DefaultMarketplaceConsignmentDao() {
    super(MarketplaceConsignmentModel._TYPECODE);
  }

  @Override
  public MarketplaceConsignmentModel findMarketplaceConsignmentByCode(String code) {
    validateParameterNotNullStandardMessage("code", code);
    List<MarketplaceConsignmentModel> consignments = find(singletonMap(MarketplaceConsignmentModel.CODE, code));
    if (isEmpty(consignments)) {
      return null;
    }
    if (consignments.size() > 1) {
      throw new AmbiguousIdentifierException(format("Multiple consignments found with code [%s]", code));
    }

    return consignments.get(0);
  }

}
