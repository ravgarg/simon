package com.mirakl.hybris.core.product.daos.impl;

import static com.google.common.collect.Lists.transform;
import static com.mirakl.hybris.core.model.OfferModel.CURRENCY;
import static com.mirakl.hybris.core.model.OfferModel.PRODUCTCODE;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.base.Function;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.daos.OfferDao;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

public class DefaultOfferDao extends DefaultGenericDao<OfferModel> implements OfferDao {

  private static final String COUNT_OFFERS_QUERY = "select count({o." + OfferModel.PK + "}) from {" + OfferModel._TYPECODE
      + " as o} where {o." + PRODUCTCODE + "} = ?" + OfferModel.PRODUCTCODE + "";

  private static final String COUNT_OFFERS_FOR_CURRENCY_QUERY =
      "select count({o." + OfferModel.PK + "}) from {" + OfferModel._TYPECODE + " as o} where {o." + OfferModel.PRODUCTCODE
          + "} = ?" + OfferModel.PRODUCTCODE + " and {o." + OfferModel.CURRENCY + "} = ?" + OfferModel.CURRENCY;

  private static final String OFFERS_FOR_PRODUCT_AND_CURRENCY_QUERY =
      "select {o." + OfferModel.PK + "} from {" + OfferModel._TYPECODE + " as o} where {o." + OfferModel.PRODUCTCODE + "} = ?"
          + OfferModel.PRODUCTCODE + " and {o." + OfferModel.CURRENCY + "} = ?" + OfferModel.CURRENCY;

  private static final String OFFER_STATES_FOR_PRODUCT_QUERY = "SELECT {o." + OfferModel.STATE + "}, {o." + OfferModel.CURRENCY
      + "} FROM {" + OfferModel._TYPECODE + " AS o} WHERE {o." + OfferModel.PRODUCTCODE + "} = ?" + OfferModel.PRODUCTCODE
      + " GROUP BY {o." + OfferModel.STATE + "}, {o." + OfferModel.CURRENCY + "}";

  private static final String UNDELETED_OFFERS_MODIFIED_BEFORE_DATE_QUERY =
      "SELECT {o." + OfferModel.PK + "} FROM {" + OfferModel._TYPECODE + " as o} WHERE {o." + OfferModel.MODIFIEDTIME + "} < ?"
          + OfferModel.MODIFIEDTIME + " and {o." + OfferModel.DELETED + "} = false";


  public DefaultOfferDao() {
    super(OfferModel._TYPECODE);
  }

  @Override
  public OfferModel findOfferById(String offerId) {
    validateParameterNotNullStandardMessage("offerId", offerId);

    List<OfferModel> offerModels = find(singletonMap(OfferModel.ID, offerId));

    if (isNotEmpty(offerModels) && offerModels.size() > 1) {
      throw new AmbiguousIdentifierException(format("Multiple offers found for id [%s]", offerId));
    }
    return isEmpty(offerModels) ? null : offerModels.get(0);
  }

  @Override
  public List<OfferModel> findAllOffers() {
    return find();
  }

  @Override
  public List<OfferModel> findUndeletedOffersModifiedBeforeDate(Date modificationDate) {
    validateParameterNotNullStandardMessage("modificationDate", modificationDate);
    FlexibleSearchQuery query = new FlexibleSearchQuery(UNDELETED_OFFERS_MODIFIED_BEFORE_DATE_QUERY,
        singletonMap(OfferModel.MODIFIEDTIME, modificationDate));
    SearchResult<OfferModel> result = getFlexibleSearchService().search(query);
    return result.getResult();
  }

  @Override
  public List<OfferModel> findOffersForProductCode(String productCode) {
    validateParameterNotNullStandardMessage("product", productCode);
    return find(singletonMap(PRODUCTCODE, productCode));
  }

  @Override
  public List<OfferModel> findOffersForProductCodeAndCurrency(String productCode, CurrencyModel offerCurrency) {
    validateParameterNotNullStandardMessage("product", productCode);
    validateParameterNotNullStandardMessage("currency", offerCurrency);

    Map<String, Object> queryArguments = new HashMap<>();
    queryArguments.put(PRODUCTCODE, productCode);
    queryArguments.put(CURRENCY, offerCurrency);
    FlexibleSearchQuery query = new FlexibleSearchQuery(OFFERS_FOR_PRODUCT_AND_CURRENCY_QUERY, queryArguments);

    SearchResult<OfferModel> result = getFlexibleSearchService().search(query);
    return result.getResult();
  }

  @Override
  public List<Pair<OfferState, CurrencyModel>> findOfferStatesAndCurrencyForProductCode(String productCode) {
    validateParameterNotNullStandardMessage("product", productCode);
    FlexibleSearchQuery query = new FlexibleSearchQuery(OFFER_STATES_FOR_PRODUCT_QUERY, singletonMap(PRODUCTCODE, productCode));
    query.setResultClassList(Arrays.asList(OfferState.class, CurrencyModel.class));
    SearchResult<List<Object>> result = getFlexibleSearchService().search(query);

    return transform(result.getResult(), new Function<List<Object>, Pair<OfferState, CurrencyModel>>() {

      @Override
      public Pair<OfferState, CurrencyModel> apply(List<Object> tuple) {
        return Pair.of((OfferState) tuple.get(0), (CurrencyModel) tuple.get(1));
      }
    });

  }

  @Override
  public int countOffersForProduct(String productCode) {
    FlexibleSearchQuery query = new FlexibleSearchQuery(COUNT_OFFERS_QUERY, singletonMap(PRODUCTCODE, productCode));
    query.setResultClassList(singletonList(Integer.class));
    SearchResult<Integer> result = getFlexibleSearchService().search(query);

    return result.getResult().get(0);
  }

  @Override
  public int countOffersForProductAndCurrency(String productCode, CurrencyModel currency) {
    Map<String, Object> queryArguments = new HashMap<>();
    queryArguments.put(PRODUCTCODE, productCode);
    queryArguments.put(CURRENCY, currency);
    FlexibleSearchQuery query = new FlexibleSearchQuery(COUNT_OFFERS_FOR_CURRENCY_QUERY, queryArguments);

    query.setResultClassList(singletonList(Integer.class));
    SearchResult<Integer> result = getFlexibleSearchService().search(query);

    return result.getResult().get(0);
  }


}
