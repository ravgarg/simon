package com.mirakl.hybris.core.product.strategies.impl;

import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static com.mirakl.hybris.core.enums.MiraklExportType.PRODUCT_EXPORT;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.product.synchro.MiraklProductSynchroResult;
import com.mirakl.client.mmp.request.catalog.product.MiraklProductSynchroErrorReportRequest;
import com.mirakl.client.mmp.request.catalog.product.MiraklProductSynchroStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.strategies.impl.AbstractExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.platform.converters.Populator;

public class DefaultProductExportReportStrategy extends AbstractExportReportStrategy<MiraklProductSynchroResult> {

  protected Populator<MiraklProductSynchroResult, MiraklJobReportModel> reportPopulator;
  protected MiraklMarketplacePlatformFrontApi mmpApi;
  protected Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Override
  protected MiraklProductSynchroResult getExportResult(String syncJobId) {
    checkArgument(isNotBlank(syncJobId), "Product export job id cannot be blank");

    return mmpApi.getProductSynchroResult(new MiraklProductSynchroStatusRequest(syncJobId));
  }

  @Override
  protected boolean isExportCompleted(MiraklProductSynchroResult exportResult) {
    return !MiraklExportStatus.PENDING.equals(exportStatuses.get(exportResult.getStatus()));
  }

  @Override
  protected File getErrorReportFile(String jobId) {
    checkArgument(isNotBlank(jobId), "Product export job id cannot be blank");

    return mmpApi.getProductSynchroErrorReport(new MiraklProductSynchroErrorReportRequest(jobId));
  }

  @Override
  protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
    return miraklJobReportDao.findPendingJobReportsForType(PRODUCT_EXPORT);
  }

  @Override
  protected Populator<MiraklProductSynchroResult, MiraklJobReportModel> getReportPopulator() {
    return reportPopulator;
  }

  @Required
  public void setReportPopulator(Populator<MiraklProductSynchroResult, MiraklJobReportModel> reportPopulator) {
    this.reportPopulator = reportPopulator;
  }

  @Required
  public void setMmpApi(MiraklMarketplacePlatformFrontApi mmpApi) {
    this.mmpApi = mmpApi;
  }

  @Required
  public void setExportStatuses(Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses) {
    this.exportStatuses = exportStatuses;
  }
}
