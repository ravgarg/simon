package com.mirakl.hybris.core.product.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mci.front.core.MiraklCatalogIntegrationFrontApi;
import com.mirakl.client.mci.front.request.product.MiraklUpdateProductImportStatusRequest;
import com.mirakl.client.mci.request.product.AbstractMiraklUpdateProductImportStatusRequest.ProductImportStatus;
import com.mirakl.hybris.beans.ProductImportErrorData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportResultData;
import com.mirakl.hybris.beans.ProductImportSuccessData;
import com.mirakl.hybris.core.product.strategies.ProductImportLineResultHandler;
import com.mirakl.hybris.core.product.strategies.ProductImportResultHandler;

public class DefaultProductImportResultHandler implements ProductImportResultHandler, BeanFactoryAware {

  private static final Logger LOG = Logger.getLogger(DefaultProductImportResultHandler.class);

  protected static final String PRODUCT_IMPORT_SUCCESS_LINE_RESULT_HANDLER = "productImportSuccessLineResultHandler";
  protected static final String PRODUCT_IMPORT_ERROR_LINE_RESULT_HANDLER = "productImportErrorLineResultHandler";

  protected MiraklCatalogIntegrationFrontApi mciApi;
  protected BeanFactory beanFactory;

  @Override
  public void handleImportResults(ProductImportFileContextData context) {
    validateParameterNotNullStandardMessage("context", context);
    validateParameterNotNullStandardMessage("importResultQueue", context.getImportResultQueue());


    try (ProductImportLineResultHandler<ProductImportSuccessData> successHandler = getSuccessHandler(context);
        ProductImportLineResultHandler<ProductImportErrorData> errorHandler = getErrorHandler(context)) {

      while (true) {
        ProductImportResultData resultData = context.getImportResultQueue().take();
        if (resultData.isTerminationSignal()) {
          displayTerminationMessage(context);
          break;
        }

        if (resultData instanceof ProductImportErrorData) {
          handleResult((ProductImportErrorData) resultData, errorHandler, false);
        } else if (resultData instanceof ProductImportSuccessData) {
          handleResult((ProductImportSuccessData) resultData, successHandler, true);
        } else {
          LOG.error(format("No handler defined for result of type [%s]", resultData.getClass()));
        }
      }

      updateProductImportStatus(context, successHandler, errorHandler);

    } catch (InterruptedException e) {
      LOG.warn(format("Error handler thread for file [%s] was interrupted", context.getReceivedFile().getName()), e);
      Thread.currentThread().interrupt();
    } catch (Exception e) {
      LOG.error(format("An error occurred in the error handler thread for file [%s]", context.getReceivedFile().getName()), e);
    }


  }

  protected <T extends ProductImportResultData> void handleResult(T resultData, ProductImportLineResultHandler<T> handler,
      boolean success) {
    if (LOG.isDebugEnabled()) {
      LOG.debug(format("%s line [%s]", success ? "Imported" : "Rejected", resultData.getRowNumber()));
    }
    handler.handleLineResult(resultData);
  }

  protected void updateProductImportStatus(ProductImportFileContextData context, ProductImportLineResultHandler<ProductImportSuccessData> successHandler, ProductImportLineResultHandler<ProductImportErrorData> errorHandler) {
    try {
      mciApi.updateProductImportStatus(buildRequest(successHandler.getFilename(), errorHandler.getFilename(), context));
    } catch (FileNotFoundException e) {
      LOG.error(format("Unable to update product import status in Mirakl for file [%s]", context.getFullFilename()), e);
    }
  }

  protected MiraklUpdateProductImportStatusRequest buildRequest(String successFilename, String errorFilename,
      ProductImportFileContextData context) throws FileNotFoundException {
    MiraklUpdateProductImportStatusRequest request =
        new MiraklUpdateProductImportStatusRequest(context.getMiraklImportId(), ProductImportStatus.COMPLETE);
    if (context.getErrorFile() != null) {
      request.setErrorFilename(errorFilename);
      request.setErrorsStream(new FileInputStream(context.getErrorFile()));
    }
    if (context.getSuccessFile() != null) {
      request.setImporterProductsFilename(successFilename);
      request.setImportedProductsStream(new FileInputStream(context.getSuccessFile()));
    }

    return request;
  }

  @SuppressWarnings("unchecked")
  protected ProductImportLineResultHandler<ProductImportSuccessData> getSuccessHandler(ProductImportFileContextData context) {
    return (ProductImportLineResultHandler<ProductImportSuccessData>) beanFactory
        .getBean(PRODUCT_IMPORT_SUCCESS_LINE_RESULT_HANDLER, context);
  }

  @SuppressWarnings("unchecked")
  protected ProductImportLineResultHandler<ProductImportErrorData> getErrorHandler(ProductImportFileContextData context) {
    return (ProductImportLineResultHandler<ProductImportErrorData>) beanFactory.getBean(PRODUCT_IMPORT_ERROR_LINE_RESULT_HANDLER,
        context);
  }

  protected void displayTerminationMessage(ProductImportFileContextData context) {
    if (LOG.isDebugEnabled()) {
      LOG.debug(format("Received termination signal for file [%s]", context.getFullFilename()));
    }
  }

  @Required
  public void setMciApi(MiraklCatalogIntegrationFrontApi mciApi) {
    this.mciApi = mciApi;
  }


  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = beanFactory;
  }
}
