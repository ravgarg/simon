package com.mirakl.hybris.core.comparators;

import java.util.Comparator;

import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class OfferStateComparator implements Comparator<OfferModel> {

  private OfferState priorityOfferState;

  public OfferStateComparator(OfferState priorityOfferState) {
    this.priorityOfferState = priorityOfferState;
  }

  @Override
  public int compare(OfferModel offer1, OfferModel offer2) {
    if (priorityOfferState.equals(offer1.getState()) && !offer1.getState().equals(offer2.getState())) {
      return -1;
    }
    if (priorityOfferState.equals(offer2.getState()) && !offer1.getState().equals(offer2.getState())) {
      return 1;
    }
    return 0;
  }

}
