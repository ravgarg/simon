package com.mirakl.hybris.core.catalog.jalo;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.jalo.MiraklservicesManager;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.services.OfferService;

import de.hybris.platform.catalog.jalo.CatalogAwareEurope1PriceFactory;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.PriceValue;

public class MiraklEurope1PriceFactory extends CatalogAwareEurope1PriceFactory {

  protected OfferService offerService;
  protected SessionService sessionService;
  protected SearchRestrictionService searchRestrictionService;

  @Override
  public PriceValue getBasePrice(AbstractOrderEntry entry) throws JaloPriceFactoryException {
    final String offerId = MiraklservicesManager.getInstance().getOfferId(entry);
    if (StringUtils.isBlank(offerId)) {
      return super.getBasePrice(entry);
    }

    final AbstractOrder order = entry.getOrder();
    try {
      return sessionService.executeInLocalView(new SessionExecutionBody() {
        @Override
        public Object execute() {
          try {
            searchRestrictionService.disableSearchRestrictions();
            return createPriceValue(offerId, order.isNet().booleanValue());
          } finally {
            searchRestrictionService.enableSearchRestrictions();
          }
        }
      });

    } catch (UnknownIdentifierException e) {
      throw new IllegalStateException(e);
    }
  }

  protected Object createPriceValue(final String offerId, boolean net) {
    OfferModel offer = offerService.getOfferForId(offerId);
    return new PriceValue(offer.getCurrency().getIsocode(), offer.getPrice().doubleValue(), net);
  }

  @Required
  public void setOfferService(OfferService offerService) {
    this.offerService = offerService;
  }

  @Required
  public void setSessionService(SessionService sessionService) {
    this.sessionService = sessionService;
  }

  @Required
  public void setSearchRestrictionService(SearchRestrictionService searchRestrictionService) {
    this.searchRestrictionService = searchRestrictionService;
  }

}
