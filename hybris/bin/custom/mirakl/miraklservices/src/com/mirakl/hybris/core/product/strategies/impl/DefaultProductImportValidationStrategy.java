package com.mirakl.hybris.core.product.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Map;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.enums.MiraklAttributeRole;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.strategies.ProductImportValidationStrategy;

public class DefaultProductImportValidationStrategy implements ProductImportValidationStrategy {

  @Override
  public void validate(MiraklRawProductModel rawProduct, ProductImportFileContextData context) throws ProductImportException {
    validateParameterNotNullStandardMessage("rawProduct", rawProduct);
    validateParameterNotNullStandardMessage("context", context);

    Map<MiraklAttributeRole, String> coreAttributePerRole = context.getGlobalContext().getCoreAttributePerRole();
    checkNotNull(coreAttributePerRole.get(MiraklAttributeRole.SHOP_SKU_ATTRIBUTE), "Shop Sku attribute must be provided",
        rawProduct);
    checkNotNull(coreAttributePerRole.get(MiraklAttributeRole.CATEGORY_ATTRIBUTE), "Category attribute must be provided",
        rawProduct);
  }

  protected void checkNotNull(MiraklCoreAttributeModel coreAttribute, String message, MiraklRawProductModel rawProduct)
      throws ProductImportException {
    validateParameterNotNullStandardMessage("coreAttribute", coreAttribute);

    checkNotNull(coreAttribute.getCode(), message, rawProduct);
  }

  protected void checkNotNull(String attribute, String message, MiraklRawProductModel miraklRawProduct)
      throws ProductImportException {
    if (isBlank(miraklRawProduct.getValues().get(attribute))) {
      throw new ProductImportException(miraklRawProduct, message);
    }
  }


}
