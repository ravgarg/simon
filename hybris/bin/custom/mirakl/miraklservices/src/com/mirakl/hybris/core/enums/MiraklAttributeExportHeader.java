package com.mirakl.hybris.core.enums;

import java.util.List;
import java.util.Locale;

public enum MiraklAttributeExportHeader implements MiraklHeader {
  CODE("code"), //
  LABEL("label", true), //
  HIERARCHY_CODE("hierarchy-code"), //
  DESCRIPTION("description", true), //
  EXAMPLE("example"), //
  REQUIRED("required"), //
  TYPE("type"), //
  TYPE_PARAMETER("type-parameter"), //
  VARIANT("variant"), //
  DEFAULT_VALUE("default-value"), //
  TRANSFORMATIONS("transformations"), //
  VALIDATIONS("validations"), //
  UPDATE_DELETE("update-delete");

  private String code;
  private boolean localizable;

  private MiraklAttributeExportHeader(String code) {
    this.code = code;
  }

  private MiraklAttributeExportHeader(String code, boolean localizable) {
    this.localizable = localizable;
    this.code = code;
  }

  @Override
  public String getCode() {
    return code;
  }

  @Override
  public String getCode(Locale locale) {
    return MiraklHeaderUtils.getCode(this, locale);
  }

  public static String[] codes() {
    return MiraklHeaderUtils.codes(values());
  }

  public static String[] codes(List<Locale> locales) {
    return MiraklHeaderUtils.codes(values(), locales);
  }

  @Override
  public MiraklHeader[] getValues() {
    return values();
  }

  @Override
  public boolean isLocalizable() {
    return localizable;
  }

}
