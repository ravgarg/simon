package com.mirakl.hybris.core.product.daos.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.model.ShopVariantGroupModel;
import com.mirakl.hybris.core.product.daos.MiraklProductDao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

public class DefaultMiraklProductDao extends DefaultGenericDao<ProductModel> implements MiraklProductDao {

  private static final String SHOP_VARIANT_GROUP_QUERY = "SELECT {svg:" + ShopVariantGroupModel.PK + "}"//
      + " FROM {" + ShopVariantGroupModel._TYPECODE + " AS svg"//
      + " JOIN " + ProductModel._TYPECODE + " AS p ON {p:" + ProductModel.PK + "}={svg:" + ShopVariantGroupModel.PRODUCT + "}}" //
      + " WHERE {svg:" + ShopVariantGroupModel.SHOP + "} = ?" + ShopVariantGroupModel.SHOP //
      + " AND {svg:" + ShopVariantGroupModel.CODE + "} = ?" + ShopVariantGroupModel.CODE//
      + " AND {p:" + ProductModel.CATALOGVERSION + "} = ?" + ProductModel.CATALOGVERSION;

  public DefaultMiraklProductDao() {
    super(ProductModel._TYPECODE);
  }

  @Override
  public List<ProductModel> findModifiedProductsWithNoVariantType(Date modifiedAfter, CatalogVersionModel catalogVersion) {
    Map<String, Object> params = new HashMap<>();
    params.put(ProductModel.CATALOGVERSION, catalogVersion);
    params.put(ProductModel.MODIFIEDTIME, modifiedAfter);

    StringBuilder queryString =
        new StringBuilder("SELECT {").append(ProductModel.PK).append("} FROM {").append(ProductModel._TYPECODE).append("}") //
            .append(" WHERE {").append(ProductModel.CATALOGVERSION).append("}=?").append(ProductModel.CATALOGVERSION) //
            .append(" AND {").append(ProductModel.VARIANTTYPE).append("} IS NULL");
    if (modifiedAfter != null) {
      queryString.append(" AND {").append(ProductModel.MODIFIEDTIME).append("} > ?").append(ProductModel.MODIFIEDTIME);
    }

    SearchResult<ProductModel> searchResult =
        getFlexibleSearchService().search(new FlexibleSearchQuery(queryString.toString(), params));
    return searchResult.getResult();
  }

  @Override
  public ProductModel findProductForShopVariantGroupCode(ShopModel shop, String variantGroupCode,
      CatalogVersionModel catalogVersion) {
    validateParameterNotNullStandardMessage("shop", shop);
    validateParameterNotNullStandardMessage("variantGroupCode", variantGroupCode);
    validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);

    Map<String, Object> params = new HashMap<>();
    params.put(ShopVariantGroupModel.SHOP, shop);
    params.put(ShopVariantGroupModel.CODE, variantGroupCode);
    params.put(ProductModel.CATALOGVERSION, catalogVersion);

    SearchResult<ShopVariantGroupModel> searchResult = getFlexibleSearchService().search(SHOP_VARIANT_GROUP_QUERY, params);
    if (searchResult.getCount() > 1) {
      throw new AmbiguousIdentifierException(
          format("Found multiple root base products matching shop [%s] and variant group [%s]", shop.getId(), variantGroupCode));
    }

    if (searchResult.getCount() == 1) {
      return searchResult.getResult().get(0).getProduct();
    }

    return null;
  }


}
