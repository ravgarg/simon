package com.mirakl.hybris.core.order.strategies.impl;

import static java.lang.Math.min;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.order.daos.MiraklAbstractOrderEntryDao;
import com.mirakl.hybris.core.order.strategies.CommonMiraklCartStrategy;

import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.AbstractCommerceCartStrategy;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

public class DefaultCommonMiraklCartStrategy extends AbstractCommerceCartStrategy implements CommonMiraklCartStrategy {

  protected MiraklAbstractOrderEntryDao<CartEntryModel> miraklCartEntryDao;

  @Override
  public long getAllowedCartAdjustmentForOffer(final CartModel cartModel, ProductModel productModel, OfferModel offerModel,
      final long quantityToAdd) {
    final long cartLevelForOffer = checkCartLevel(offerModel, cartModel);
    final long stockLevel = offerModel.getQuantity();

    final long newTotalQuantity = cartLevelForOffer + quantityToAdd;
    final long newTotalQuantityAfterStockLimit = min(newTotalQuantity, stockLevel);
    long allowedAdjustment = newTotalQuantityAfterStockLimit - cartLevelForOffer;

    final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();
    if (isMaxOrderQuantitySet(maxOrderQuantity)) {
      // We should check that we will not exceed the product's max order quantity
      final long cartLevelForProduct = checkCartLevel(productModel, cartModel, null);
      final long newTotalQuantityForProduct = cartLevelForProduct + newTotalQuantityAfterStockLimit;
      if (newTotalQuantityForProduct > maxOrderQuantity.longValue()) {
        final long maxAllowedAdjustment = maxOrderQuantity - (cartLevelForProduct + cartLevelForOffer);
        return min(allowedAdjustment, maxAllowedAdjustment);
      }
    }

    return allowedAdjustment;
  }

  @Override
  public long checkCartLevel(final OfferModel offerModel, final CartModel cartModel) {
    long cartLevel = 0;
    CartEntryModel entryModel = miraklCartEntryDao.findEntryByOffer(cartModel, offerModel);
    if (entryModel != null) {
      cartLevel += entryModel.getQuantity() != null ? entryModel.getQuantity().longValue() : 0;
    }

    return cartLevel;
  }

  @Override
  public String getStatusCodeAllowedQuantityChange(final long actualAllowedQuantityChange, final Integer maxOrderQuantity,
      final long quantityToAdd, final long cartLevelAfterQuantityChange) {
    if (isMaxOrderQuantitySet(maxOrderQuantity) && (quantityToAdd > actualAllowedQuantityChange)
        && (cartLevelAfterQuantityChange == maxOrderQuantity.longValue())) {
      return CommerceCartModificationStatus.MAX_ORDER_QUANTITY_EXCEEDED;
    }
    if (actualAllowedQuantityChange == quantityToAdd) {
      return CommerceCartModificationStatus.SUCCESS;
    }
    return CommerceCartModificationStatus.LOW_STOCK;
  }

  @Override
  public String getStatusCodeForNotAllowedQuantityChange(final Integer maxOrderQuantity,
      final Long cartLevelAfterQuantityChange) {
    if (isMaxOrderQuantitySet(maxOrderQuantity) && (cartLevelAfterQuantityChange == maxOrderQuantity.longValue())) {
      return CommerceCartModificationStatus.MAX_ORDER_QUANTITY_EXCEEDED;
    }
    return CommerceCartModificationStatus.NO_STOCK;
  }

  @Override
  protected long checkCartLevel(ProductModel productModel, CartModel cartModel, PointOfServiceModel pointOfServiceModel) {
    return super.checkCartLevel(productModel, cartModel, pointOfServiceModel);
  }

  @Required
  public void setMiraklCartEntryDao(MiraklAbstractOrderEntryDao<CartEntryModel> miraklCartEntryDao) {
    this.miraklCartEntryDao = miraklCartEntryDao;
  }


}
