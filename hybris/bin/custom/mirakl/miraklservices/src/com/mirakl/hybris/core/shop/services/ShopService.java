package com.mirakl.hybris.core.shop.services;

import com.mirakl.client.mmp.domain.evaluation.MiraklEvaluations;
import com.mirakl.hybris.core.model.ShopModel;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 *
 * Service providing access and control on the Shops imported from Mirakl
 * 
 * @See ShopModel
 */
public interface ShopService {

  /**
   * Returns the model of the shop matching the given identifier
   * 
   * @param id Mirakl identifier of the shop
   * @return a <tt>ShopModel</tt> if any, <tt>null</tt> otherwise
   */
  ShopModel getShopForId(String id);

  /**
   * Synchronous call to Mirakl APIs to get the requested evaluation page for a shop
   *
   * @param id Mirakl identifier of the shop
   * @param pageableData filled with the page size and the current page
   * @return the filled <tt>MiraklEvaluations</tt> if any, empty otherwise
   */
  MiraklEvaluations getEvaluations(String id, PageableData pageableData);
}
