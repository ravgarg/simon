package com.mirakl.hybris.core.order.strategies;

import de.hybris.platform.core.model.order.AbstractOrderModel;

/**
 * Strategy to extract shipping zone from {@link AbstractOrderModel}
 */
public interface ShippingZoneStrategy {

  /**
   * Returns shipping zone code from {@link AbstractOrderModel}
   *
   * @param order {@link AbstractOrderModel}
   * @return shipping zone code
   */
  String getShippingZoneCode(AbstractOrderModel order);
}
