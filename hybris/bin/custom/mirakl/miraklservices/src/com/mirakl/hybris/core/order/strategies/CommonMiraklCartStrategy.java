package com.mirakl.hybris.core.order.strategies;

import com.mirakl.hybris.core.model.OfferModel;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * A convenience class to share cart operations strategies (add to cart, update quantity,..)
 *
 */
public interface CommonMiraklCartStrategy {

  /**
   * Returns the allowed cart adjustment for an offer given a quantity to add
   * 
   * @param cartModel
   * @param productModel
   * @param offerModel
   * @param quantityToAdd
   * @return the allowed quantity to add
   */
  long getAllowedCartAdjustmentForOffer(CartModel cartModel, ProductModel productModel, OfferModel offerModel,
      long quantityToAdd);

  /**
   * Checks the cart level for a given offer
   * 
   * @param offerModel
   * @param cartModel
   * @return the cart level for the offer
   */
  long checkCartLevel(final OfferModel offerModel, final CartModel cartModel);


  /**
   * Returns the status code for an allowed quantity change
   * 
   * @param actualAllowedQuantityChange
   * @param maxOrderQuantity
   * @param quantityToAdd
   * @param cartLevelAfterQuantityChange
   * @return the status code for the allowed change
   */
  String getStatusCodeAllowedQuantityChange(final long actualAllowedQuantityChange, final Integer maxOrderQuantity,
      final long quantityToAdd, final long cartLevelAfterQuantityChange);

  /**
   * Returns the status code for a not allowed quantity change
   * 
   * @param maxOrderQuantity
   * @param cartLevelAfterQuantityChange
   * @return the status code for the not allowed change
   */
  String getStatusCodeForNotAllowedQuantityChange(final Integer maxOrderQuantity, final Long cartLevelAfterQuantityChange);

}
