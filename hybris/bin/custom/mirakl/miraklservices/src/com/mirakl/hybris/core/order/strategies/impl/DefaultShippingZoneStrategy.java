package com.mirakl.hybris.core.order.strategies.impl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;

import static com.google.common.base.Preconditions.checkState;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import com.mirakl.hybris.core.order.strategies.ShippingZoneStrategy;

public class DefaultShippingZoneStrategy implements ShippingZoneStrategy {

  @Override
  public String getShippingZoneCode(AbstractOrderModel order) {
    validateParameterNotNullStandardMessage("order", order);

    AddressModel deliveryAddress = order.getDeliveryAddress();
    checkState(deliveryAddress != null, "Delivery address cannot be null for the shipping rates request");
    return deliveryAddress.getCountry().getIsocode();
  }
}
