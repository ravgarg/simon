package com.mirakl.hybris.core.shop.daos.impl;

import static java.util.Collections.emptyList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.mirakl.hybris.core.model.ShopModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShopDaoTest {

  private static final String SHOP_ID = "shopId";
  public static final String PRODUCT_CODE = "product_code";

  @InjectMocks
  private DefaultShopDao testObj = new DefaultShopDao();

  @Mock
  private FlexibleSearchService flexibleSearchServiceMock;

  @Mock
  private ShopModel firstShopModel, secondShopModel;

  @Test
  public void findsShopById() {
    when(flexibleSearchServiceMock.search(any(FlexibleSearchQuery.class)))
        .thenReturn(new SearchResultImpl<>(ImmutableList.<Object>of(firstShopModel), 1, 0, 0));

    ShopModel result = testObj.findShopById(SHOP_ID);

    assertThat(result).isSameAs(firstShopModel);
  }

  @Test
  public void findShopByIdReturnsNullIfNoShopFound() {
    when(flexibleSearchServiceMock.search(any(FlexibleSearchQuery.class)))
        .thenReturn(new SearchResultImpl<>(emptyList(), 1, 0, 0));

    ShopModel result = testObj.findShopById(SHOP_ID);

    assertThat(result).isNull();
  }

  @Test(expected = AmbiguousIdentifierException.class)
  public void findShopByIdThrowsAmbiguousIdentifierExceptionIfMultipleShopsFound() {
    when(flexibleSearchServiceMock.search(any(FlexibleSearchQuery.class)))
        .thenReturn(new SearchResultImpl<>(ImmutableList.<Object>of(firstShopModel, secondShopModel), 2, 0, 0));

    testObj.findShopById(SHOP_ID);
  }

  @Test
  public void findShopsForProductCode() {
    when(flexibleSearchServiceMock.search(any(FlexibleSearchQuery.class)))
        .thenReturn(new SearchResultImpl<>(ImmutableList.<Object>of(firstShopModel), 1, 0, 0));

    Collection<ShopModel> shops = testObj.findShopsForProductCode(PRODUCT_CODE);

    assertThat(shops).containsOnly(firstShopModel);
  }

  @Test
  public void findShopsForProductCodeReturnsEmptyIdNoShopFound() {
    when(flexibleSearchServiceMock.search(any(FlexibleSearchQuery.class)))
        .thenReturn(new SearchResultImpl<>(emptyList(), 0, 0, 0));

    Collection<ShopModel> shops = testObj.findShopsForProductCode(PRODUCT_CODE);

    assertThat(shops).isEmpty();
  }
}
