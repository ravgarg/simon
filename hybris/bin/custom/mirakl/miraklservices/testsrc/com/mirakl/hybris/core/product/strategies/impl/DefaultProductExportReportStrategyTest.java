package com.mirakl.hybris.core.product.strategies.impl;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.product.synchro.MiraklProductSynchroResult;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.catalog.product.MiraklProductSynchroErrorReportRequest;
import com.mirakl.client.mmp.request.catalog.product.MiraklProductSynchroStatusRequest;
import com.mirakl.hybris.core.jobs.dao.MiraklJobReportDao;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.mirakl.hybris.core.enums.MiraklExportType.PRODUCT_EXPORT;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultProductExportReportStrategyTest {

  private static final String SYNC_JOB_ID = "syncJobId";
  private static final int PRODUCTS_DELETED = 1;
  private static final int PRODUCTS_INSERTED = 2;
  private static final int PRODUCTS_UPDATED = 3;

  @InjectMocks
  private DefaultProductExportReportStrategy testObj = new DefaultProductExportReportStrategy();

  @Mock
  private MiraklJobReportDao miraklJobReportDaoMock;
  @Mock
  private MiraklMarketplacePlatformFrontApi miraklOperatorApiMock;

  @Mock
  private MiraklJobReportModel miraklJobReportMock;
  @Mock
  private MiraklProductSynchroResult miraklProductSynchroResultMock;

  @Captor
  private ArgumentCaptor<MiraklProductSynchroResult> productSynchroResultArgumentCaptor;
  @Captor
  private ArgumentCaptor<MiraklProductSynchroStatusRequest> productSynchroStatusRequestArgumentCaptor;
  @Captor
  private ArgumentCaptor<MiraklProductSynchroErrorReportRequest> productSynchroErrorReportRequestArgumentCaptor;

  @Rule
  public TemporaryFolder errorReportRule = new TemporaryFolder();

  @Before
  public void setUp() {
    when(miraklJobReportDaoMock.findPendingJobReportsForType(PRODUCT_EXPORT)).thenReturn(singletonList(miraklJobReportMock));

    when(miraklProductSynchroResultMock.getProductDeleted()).thenReturn(PRODUCTS_DELETED);
    when(miraklProductSynchroResultMock.getProductInserted()).thenReturn(PRODUCTS_INSERTED);
    when(miraklProductSynchroResultMock.getProductUpdated()).thenReturn(PRODUCTS_UPDATED);

    when(miraklOperatorApiMock.getProductSynchroResult(productSynchroStatusRequestArgumentCaptor.capture()))
        .thenReturn(miraklProductSynchroResultMock);
  }

  @Test
  public void getsPendingMiraklJobReports() {
    List<MiraklJobReportModel> result = testObj.getPendingMiraklJobReports();

    assertThat(result).containsOnly(miraklJobReportMock);

    verify(miraklJobReportDaoMock).findPendingJobReportsForType(PRODUCT_EXPORT);
  }

  @Test
  public void getsProductExportResult() {
    MiraklProductSynchroResult result = testObj.getExportResult(SYNC_JOB_ID);

    assertThat(result).isSameAs(miraklProductSynchroResultMock);

    MiraklProductSynchroStatusRequest productSynchroStatusRequest = productSynchroStatusRequestArgumentCaptor.getValue();
    assertThat(productSynchroStatusRequest.getSynchroId()).isEqualTo(SYNC_JOB_ID);
  }

  @Test
  public void getsProductExportErrorReport() throws IOException {
    File errorReport = errorReportRule.newFile();
    when(miraklOperatorApiMock.getProductSynchroErrorReport(productSynchroErrorReportRequestArgumentCaptor.capture()))
        .thenReturn(errorReport);

    File result = testObj.getErrorReportFile(SYNC_JOB_ID);

    assertThat(result).isSameAs(errorReport);

    MiraklProductSynchroErrorReportRequest productSynchroErrorReportRequest =
        productSynchroErrorReportRequestArgumentCaptor.getValue();
    assertThat(productSynchroErrorReportRequest.getSynchroId()).isEqualTo(SYNC_JOB_ID);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getProductExportResultThrowsIllegalArgumentExceptionISyncJobIdIsNull() throws IOException {
    testObj.getExportResult(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getProductExportResultThrowsIllegalArgumentExceptionISyncJobIdIsEmpty() throws IOException {
    testObj.getExportResult(EMPTY);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getExportErrorReportThrowsIllegalArgumentExceptionISyncJobIdIsNull() throws IOException {
    testObj.getErrorReportFile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getExportErrorReportThrowsIllegalArgumentExceptionISyncJobIdIsEmpty() throws IOException {
    testObj.getErrorReportFile(EMPTY);
  }

  @Test(expected = MiraklApiException.class)
  public void getProductExportResultThrowsMiraklApiException() {
    when(miraklOperatorApiMock.getProductSynchroResult(productSynchroStatusRequestArgumentCaptor.capture()))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    testObj.getExportResult(SYNC_JOB_ID);
  }

  @Test(expected = MiraklApiException.class)
  public void getExportErrorReportThrowsMiraklApiException() throws IOException {
    when(miraklOperatorApiMock.getProductSynchroErrorReport(productSynchroErrorReportRequestArgumentCaptor.capture()))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    testObj.getErrorReportFile(SYNC_JOB_ID);
  }

}
