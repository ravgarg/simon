package com.mirakl.hybris.core.category.populators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mmp.domain.category.synchro.MiraklCategorySynchroResult;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryExportReportPopulatorTest {

  private static final int LINES_READ = 5;
  private static final int LINES_IN_ERROR = 1;
  private static final int LINES_IN_SUCCESS = 4;
  private static final int CATEGORIES_DELETED = 2;
  private static final int CATEGORIES_INSERTED = 1;
  private static final int CATEGORIES_UPDATED = 1;

  @Mock
  private Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Mock
  private MiraklCategorySynchroResult synchroResult;

  @InjectMocks
  private CategoryExportReportPopulator testObj;

  @Test
  public void populate() throws Exception {
    MiraklJobReportModel target = new MiraklJobReportModel();
    when(synchroResult.getStatus()).thenReturn(MiraklProcessTrackingStatus.COMPLETE);
    when(exportStatuses.get(MiraklProcessTrackingStatus.COMPLETE)).thenReturn(MiraklExportStatus.COMPLETE);
    when(synchroResult.hasErrorReport()).thenReturn(true);
    when(synchroResult.getLinesRead()).thenReturn(LINES_READ);
    when(synchroResult.getLinesInError()).thenReturn(LINES_IN_ERROR);
    when(synchroResult.getLinesInSuccess()).thenReturn(LINES_IN_SUCCESS);
    when(synchroResult.getCategoryDeleted()).thenReturn(CATEGORIES_DELETED);
    when(synchroResult.getCategoryInserted()).thenReturn(CATEGORIES_INSERTED);
    when(synchroResult.getCategoryUpdated()).thenReturn(CATEGORIES_UPDATED);

    testObj.populate(synchroResult, target);

    assertThat(target.getHasErrorReport()).isTrue();
    assertThat(target.getStatus()).isEqualTo(MiraklExportStatus.COMPLETE);
    assertThat(target.getLinesRead()).isEqualTo(LINES_READ);
    assertThat(target.getLinesInError()).isEqualTo(LINES_IN_ERROR);
    assertThat(target.getLinesInSuccess()).isEqualTo(LINES_IN_SUCCESS);
    assertThat(target.getItemsDeleted()).isEqualTo(CATEGORIES_DELETED);
    assertThat(target.getItemsInserted()).isEqualTo(CATEGORIES_INSERTED);
    assertThat(target.getItemsUpdated()).isEqualTo(CATEGORIES_UPDATED);
  }

}
