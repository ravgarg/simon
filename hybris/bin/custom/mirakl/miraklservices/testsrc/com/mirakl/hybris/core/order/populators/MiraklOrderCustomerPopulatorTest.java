package com.mirakl.hybris.core.order.populators;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklCustomerShippingAddress;
import com.mirakl.client.mmp.domain.order.MiraklOrderCustomer;
import com.mirakl.hybris.core.order.populators.MiraklOrderCustomerPopulator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklOrderCustomerPopulatorTest {

  private static final String LANGUAGE_ISO_CODE = "en";
  private static final String CUSTOMER_EMAIL_ADDRESS = "customerEmailAddress";
  private static final String FIRST_NAME = "firstName";
  private static final String LAST_NAME = "lastName";
  private static final String CUSTOMER_NAME = FIRST_NAME + " " + LAST_NAME;
  private static final Locale LANGUAGE_LOCALE = Locale.FRENCH;

  @InjectMocks
  private MiraklOrderCustomerPopulator testObj = new MiraklOrderCustomerPopulator();

  @Mock
  private CommonI18NService commonI18NServiceMock;
  @Mock
  private CustomerNameStrategy customerNameStrategyMock;
  @Mock(name = "miraklCustomerAddressConverter")
  private Converter<AddressModel, MiraklCustomerShippingAddress> miraklCustomerAddressConverterMock;
  @Mock
  private ConfigurationService configurationServiceMock;
  @Mock
  private Configuration configurationMock;
  @Mock
  private OrderModel orderMock;
  @Mock
  private CustomerModel customerMock;
  @Mock
  private MiraklCustomerShippingAddress miraklCustomerBillingAddressMock, miraklCustomerShippingAddressMock;
  @Mock
  private AddressModel paymentAddressMock, deliveryAddressMock;
  @Mock
  private LanguageModel languageMock;

  @Before
  public void setUp() {
    when(orderMock.getUser()).thenReturn(customerMock);
    when(orderMock.getDeliveryAddress()).thenReturn(deliveryAddressMock);
    when(orderMock.getPaymentAddress()).thenReturn(paymentAddressMock);
    when(orderMock.getLanguage()).thenReturn(languageMock);

    when(customerMock.getName()).thenReturn(CUSTOMER_NAME);
    when(customerMock.getContactEmail()).thenReturn(CUSTOMER_EMAIL_ADDRESS);

    when(customerNameStrategyMock.splitName(CUSTOMER_NAME)).thenReturn(new String[] {FIRST_NAME, LAST_NAME});
    when(commonI18NServiceMock.getLocaleForLanguage(languageMock)).thenReturn(LANGUAGE_LOCALE);
    when(languageMock.getIsocode()).thenReturn(LANGUAGE_ISO_CODE);
    when(miraklCustomerAddressConverterMock.convert(paymentAddressMock)).thenReturn(miraklCustomerBillingAddressMock);
    when(miraklCustomerAddressConverterMock.convert(deliveryAddressMock)).thenReturn(miraklCustomerShippingAddressMock);

    when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
  }

  @Test
  public void shouldPopulateMiraklOrderCustomer() {
    MiraklOrderCustomer result = new MiraklOrderCustomer();

    testObj.populate(orderMock, result);

    assertThat(result.getEmail()).isEqualTo(CUSTOMER_EMAIL_ADDRESS);
    assertThat(result.getId()).isEqualTo(CUSTOMER_EMAIL_ADDRESS);
    assertThat(result.getFirstname()).isEqualTo(FIRST_NAME);
    assertThat(result.getLastname()).isEqualTo(LAST_NAME);
    assertThat(result.getLocale()).isEqualTo(LANGUAGE_LOCALE);
    assertThat(result.getBillingAddress()).isEqualTo(miraklCustomerBillingAddressMock);
    assertThat(result.getShippingAddress()).isEqualTo(miraklCustomerShippingAddressMock);
  }

  @Test
  public void shouldNotPopulateLocaleIfNoOrderLanguageFound() {
    when(orderMock.getLanguage()).thenReturn(null);

    MiraklOrderCustomer result = new MiraklOrderCustomer();

    testObj.populate(orderMock, result);

    assertThat(result.getLocale()).isNull();

    verify(commonI18NServiceMock, never()).getLocaleForLanguage(any(LanguageModel.class));
  }

  @Test
  public void shouldNotPopulateLocaleIfNoLocaleFoundForOrderLanguage() {
    when(commonI18NServiceMock.getLocaleForLanguage(languageMock)).thenReturn(null);

    MiraklOrderCustomer result = new MiraklOrderCustomer();

    testObj.populate(orderMock, result);

    assertThat(result.getLocale()).isNull();

    verify(commonI18NServiceMock).getLocaleForLanguage(languageMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfFirstNameIsNull() {
    when(customerNameStrategyMock.splitName(CUSTOMER_NAME)).thenReturn(new String[] {null, LAST_NAME});

    testObj.populate(orderMock, new MiraklOrderCustomer());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfLastNameIsNull() {
    when(customerNameStrategyMock.splitName(CUSTOMER_NAME)).thenReturn(new String[] {FIRST_NAME, null});

    testObj.populate(orderMock, new MiraklOrderCustomer());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfOrderNull() {
    testObj.populate(null, new MiraklOrderCustomer());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfMiraklOrderCustomerNull() {
    testObj.populate(orderMock, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfCustomerIsNull() {
    when(orderMock.getUser()).thenReturn(null);

    testObj.populate(orderMock, new MiraklOrderCustomer());
  }
}
