package com.mirakl.hybris.core.catalog.attributes;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.collections.Sets;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.MiraklExportCatalogCronJobModel;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExportCatalogCoreAttributesDynamicHandlerTest extends AbstractCatalogJobCoreAttributesDynamicHandlerTest {

  @Mock
  private MiraklExportCatalogCronJobModel catalogExportJob;

  @InjectMocks
  private ExportCatalogCoreAttributesDynamicHandler testObj;

  @Before
  public void setUp() throws Exception {
    super.setUp(testObj);
    when(catalogExportJob.getCoreAttributeConfiguration()).thenReturn(configuration);
  }

  @Test
  public void get() {
    testObj.get(catalogExportJob);

    verify(catalogExportJob).getCoreAttributeConfiguration();
  }

  @Test
  public void set() {
    testObj.set(catalogExportJob, Sets.newSet(coreAttribute1, coreAttribute2));

    verify(catalogExportJob).getCoreAttributeConfiguration();
  }
}
