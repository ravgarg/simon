package com.mirakl.hybris.core.util.services.impl;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;

import de.hybris.bootstrap.annotations.UnitTest;
import shaded.org.supercsv.cellprocessor.Optional;
import shaded.org.supercsv.cellprocessor.constraint.UniqueHashCode;
import shaded.org.supercsv.cellprocessor.ift.CellProcessor;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCsvServiceTest {

  private static CellProcessor[] getTestProcessors() {
    return new CellProcessor[] {new UniqueHashCode(), new Optional(), new Optional()};
  }

  private static final String HEADER_1 = "header1";
  private static final String HEADER_2 = "header2";
  private static final String HEADER_3 = "header3";

  private static final String LINE_1_VALUE_1 = "line1value1";
  private static final String LINE_1_VALUE_2 = "line1value2";
  private static final String LINE_2_VALUE_1 = "line2;value1";
  private static final String LINE_2_VALUE_3 = "line2 value3";

  private static final String CSV_HEADER = "\"header1\";\"header2\";\"header3\"\n";
  private static final String CSV_LINE_1 = "\"line1value1\";\"line1value2\";\n";
  private static final String CSV_LINE_2 = "\"line2;value1\";;\"line2 value3\"\n";

  private DefaultCsvService testObj = new DefaultCsvService();

  @Test
  public void createsCsvWithHeaders() throws IOException {
    Map<String, String> csvLine1 = ImmutableMap.of(HEADER_1, LINE_1_VALUE_1, HEADER_2, LINE_1_VALUE_2);
    Map<String, String> csvLine2 = ImmutableMap.of(HEADER_1, LINE_2_VALUE_1, HEADER_3, LINE_2_VALUE_3);

    String result = testObj.createCsvWithHeaders(new String[] {HEADER_1, HEADER_2, HEADER_3}, asList(csvLine1, csvLine2));

    assertThat(result).isEqualTo(CSV_HEADER + CSV_LINE_1 + CSV_LINE_2);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createCsvWithHeadersThrowsIllegalArgumentExceptionIfHeaderArrayIsEmpty() throws IOException {
    testObj.createCsvWithHeaders(new String[] {}, Collections.<Map<String, String>>emptyList());
  }

  @Test(expected = IllegalArgumentException.class)
  public void createCsvWithHeadersThrowsIllegalArgumentExceptionIfCellProcessorArrayIsEmpty() throws IOException {
    testObj.createCsvWithHeaders(new String[] {}, Collections.<Map<String, String>>emptyList());
  }

  @Test
  public void createsCsvWithHeadersWithNoValuesIfFieldsListIsEmpty() throws IOException {
    String result =
        testObj.createCsvWithHeaders(new String[] {HEADER_1, HEADER_2, HEADER_3}, Collections.<Map<String, String>>emptyList());

    assertThat(result).isEqualTo(CSV_HEADER);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createCsvWithHeadersThrowsIllegalArgumentExceptionIfFieldsListIsNull() throws IOException {
    testObj.createCsvWithHeaders(new String[] {HEADER_1, HEADER_2, HEADER_3}, null);
  }

  @Test
  public void createsCsvWithHeadersWithoutValuesIfFieldListIsEmpty() throws IOException {
    String result =
        testObj.createCsvWithHeaders(new String[] {HEADER_1, HEADER_2, HEADER_3}, Collections.<Map<String, String>>emptyList());

    assertThat(result).isEqualTo(CSV_HEADER);
  }

}
