package com.mirakl.hybris.core.jobs.strategies.impl;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mmp.domain.common.synchro.AbstractMiraklSynchroResult;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.enums.MiraklExportType;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AbstractExportReportStrategyTest {

  private static final MiraklExportType REPORT_TYPE = MiraklExportType.PRODUCT_EXPORT;
  private static final String SYNC_JOB_ID = "syncJobId";
  private static final String ERROR_SYNC_JOB_ID = "errorSyncJobId";
  private static final String ERROR_REPORT_FILE_NAME = "errorReportFileName";

  @Spy
  @InjectMocks
  private TestExportReportStrategy testObj = new TestExportReportStrategy();

  @Mock
  private ModelService modelServiceMock;
  @Mock
  private MediaService mediaServiceMock;
  @Mock
  private AbstractMiraklSynchroResult abstractMiraklSynchroResultMock;
  @Mock
  private MiraklErrorResponseBean miraklErrorResponseBeanMock, notHandleableErrorMock;
  @Mock
  private MiraklApiException notFoundMiraklApiExceptionMock, notHandleableMiraklApiExceptionMock;
  @Mock
  private MiraklJobReportModel miraklJobReportMock, erroneousJobReportMock;
  @Mock
  private CatalogUnawareMediaModel errorReportMock;
  @Mock
  private Populator reportPopulatorMock;

  private Boolean exportComplete;

  @Rule
  public TemporaryFolder errorReportRule = new TemporaryFolder();

  @Before
  public void setUp() {
    when(miraklJobReportMock.getJobId()).thenReturn(SYNC_JOB_ID);
    when(miraklJobReportMock.getReportType()).thenReturn(REPORT_TYPE);
    when(erroneousJobReportMock.getJobId()).thenReturn(ERROR_SYNC_JOB_ID);
    when(modelServiceMock.create(CatalogUnawareMediaModel.class)).thenReturn(errorReportMock);

    when(notFoundMiraklApiExceptionMock.getError()).thenReturn(miraklErrorResponseBeanMock);
    when(notHandleableMiraklApiExceptionMock.getError()).thenReturn(notHandleableErrorMock);
    when(miraklErrorResponseBeanMock.getStatus()).thenReturn(NOT_FOUND.getStatusCode());
    when(notHandleableErrorMock.getStatus()).thenReturn(BAD_REQUEST.getStatusCode());

    exportComplete = true;
  }

  @Test
  public void updatesPendingExportsWithoutErrorReportIfCompleted() {
    boolean result = testObj.updatePendingExports();

    assertThat(result).isTrue();

    verify(miraklJobReportMock, never()).setErrorReport(any(MediaModel.class));
    verify(modelServiceMock).saveAll(singletonList(miraklJobReportMock));
  }

  @Test
  public void updatePendingExportsReturnsFalseIfOneOfReportsThrowsUnhandleableMiraklApiException() {
    doReturn(ImmutableList.of(miraklJobReportMock, erroneousJobReportMock)).when(testObj).getPendingMiraklJobReports();
    doThrow(notHandleableMiraklApiExceptionMock).when(testObj).getExportResult(ERROR_SYNC_JOB_ID);

    boolean result = testObj.updatePendingExports();

    assertThat(result).isFalse();

    verify(miraklJobReportMock, never()).setErrorReport(any(MediaModel.class));
    verify(modelServiceMock).saveAll(singletonList(miraklJobReportMock));
  }

  @Test
  public void updatePendingExportsDoesNotSaveReportIfStillIncomplete() {
    when(abstractMiraklSynchroResultMock.getStatus()).thenReturn(MiraklProcessTrackingStatus.WAITING);
    exportComplete = false;

    boolean result = testObj.updatePendingExports();

    assertThat(result).isTrue();

    verify(miraklJobReportMock).getJobId();
    verify(testObj).getExportResult(SYNC_JOB_ID);
    verifyNoMoreInteractions(miraklJobReportMock);
    verify(modelServiceMock).saveAll(emptyList());
  }

  @Test
  public void updatesPendingExportsToNotFoundIfMiraklApiExceptionIsThrownWith404Status() {
    doThrow(notFoundMiraklApiExceptionMock).when(testObj).getExportResult(SYNC_JOB_ID);

    boolean result = testObj.updatePendingExports();

    assertThat(result).isTrue();

    verify(miraklJobReportMock).setStatus(MiraklExportStatus.NOT_FOUND);
    verify(modelServiceMock).saveAll(singletonList(miraklJobReportMock));
  }

  @Test
  public void updatePendingExportsSavesErrorReportIfExportCompletedWithErrors() throws IOException {
    when(abstractMiraklSynchroResultMock.getStatus()).thenReturn(MiraklProcessTrackingStatus.COMPLETE);
    when(abstractMiraklSynchroResultMock.hasErrorReport()).thenReturn(true);
    doReturn(errorReportMock).when(testObj).createErrorReport(any(File.class), eq(SYNC_JOB_ID), eq(REPORT_TYPE));

    boolean result = testObj.updatePendingExports();

    assertThat(result).isTrue();

    verify(testObj).getExportResult(SYNC_JOB_ID);

    verify(modelServiceMock).saveAll(singletonList(miraklJobReportMock));
  }

  @Test
  public void updatePendingExportsSavesNoErrorReportIfIOExceptionOccurs() throws IOException {
    when(abstractMiraklSynchroResultMock.getStatus()).thenReturn(MiraklProcessTrackingStatus.COMPLETE);
    when(abstractMiraklSynchroResultMock.hasErrorReport()).thenReturn(true);
    doThrow(new IOException()).when(testObj).getErrorReportFile(SYNC_JOB_ID);

    boolean result = testObj.updatePendingExports();

    assertThat(result).isTrue();

    verify(testObj).getExportResult(SYNC_JOB_ID);

    verify(miraklJobReportMock, never()).setErrorReport(any(MediaModel.class));
    verify(modelServiceMock).saveAll(singletonList(miraklJobReportMock));
  }

  @Test
  public void createsErrorReport() throws IOException {
    File errorReport = errorReportRule.newFile(ERROR_REPORT_FILE_NAME);
    doReturn(errorReport).when(testObj).getErrorReportFile(SYNC_JOB_ID);

    MediaModel result = testObj.createErrorReport(errorReport, SYNC_JOB_ID, miraklJobReportMock.getReportType());

    assertThat(result).isSameAs(errorReportMock);

    InOrder inOrder = inOrder(errorReportMock, modelServiceMock, mediaServiceMock);
    inOrder.verify(errorReportMock).setCode(format("%s-%s", REPORT_TYPE, SYNC_JOB_ID));
    inOrder.verify(errorReportMock).setRealFileName(ERROR_REPORT_FILE_NAME);
    inOrder.verify(errorReportMock).setMime(AbstractExportReportStrategy.ERROR_REPORT_MIME_TYPE);
    inOrder.verify(modelServiceMock).save(errorReportMock);
    inOrder.verify(mediaServiceMock).setStreamForMedia(eq(errorReportMock), any(InputStream.class));
    inOrder.verify(modelServiceMock).save(errorReportMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getErrorReportThrowsIllegalArgumentExceptionIfSyncJobIdIsNull() throws IOException {
    testObj.createErrorReport(errorReportRule.newFile(), null, miraklJobReportMock.getReportType());
  }

  @Test(expected = IllegalArgumentException.class)
  public void createErrorReportThrowsIllegalArgumentExceptionIfSyncJobIdIsEmpty() throws IOException {
    testObj.createErrorReport(errorReportRule.newFile(), (EMPTY), miraklJobReportMock.getReportType());
  }

  @Test(expected = IllegalArgumentException.class)
  public void createErrorReportThrowsIllegalArgumentExceptionIfReportTypeIsNull() throws IOException {
    testObj.createErrorReport(errorReportRule.newFile(), SYNC_JOB_ID, null);
  }

  protected class TestExportReportStrategy extends AbstractExportReportStrategy {

    @Override
    protected File getErrorReportFile(String jobId) throws IOException {
      return null;
    }

    @Override
    protected AbstractMiraklSynchroResult getExportResult(String syncJobId) {
      return abstractMiraklSynchroResultMock;
    }

    @Override
    protected boolean isExportCompleted(Object exportResult) {
      return exportComplete;
    }

    @Override
    protected List<MiraklJobReportModel> getPendingMiraklJobReports() {
      return singletonList(miraklJobReportMock);
    }

    @Override
    protected Populator getReportPopulator() {
      return reportPopulatorMock;
    }
  }
}
