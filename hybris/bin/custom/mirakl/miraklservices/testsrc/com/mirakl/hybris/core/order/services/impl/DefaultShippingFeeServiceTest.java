package com.mirakl.hybris.core.order.services.impl;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.base.Optional;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFee;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeError;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeOffer;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees;
import com.mirakl.client.mmp.front.domain.shipping.MiraklShippingFeeType;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShippingFeeServiceTest {

  private static final String OFFER_ID = "offerId";
  private static final String SHOP_ID = "shopId";
  private static final String SHIPPING_FEES_JSON = "shippingFeesJSON";
  private static final String SELECTED_SHIPPING_OPTION_CODE = "selectedShippingOptionCode";
  private static final String SELECTED_SHIPPING_OPTION_LABEL = "selectedShippingOptionLabel";
  private static final String OFFER_1 = "offer1";
  private static final String OFFER_2 = "offer2";
  private static final double LINE_SHIPPING_PRICE = 10.0;
  private static final int LEAD_TIME_TO_SHIP = 1;

  @InjectMocks
  private DefaultShippingFeeService testObj = new DefaultShippingFeeService();

  @Mock
  private JsonMarshallingService jsonMarshallingServiceMock;

  @Mock
  private MiraklOrderShippingFees shippingFeesMock;
  @Mock
  private MiraklOrderShippingFee shippingFeeMock, firstShippingFeeMock, secondShippingFeeMock;
  @Mock
  private MiraklOrderShippingFeeOffer offerMock, firstOfferMock, secondOfferMock;
  @Mock
  private MiraklOrderShippingFeeError errorMock;
  @Mock
  private MiraklShippingFeeType selectedShippingTypeMock, firstShippingTypeMock, secondShippingTypeMock;
  @Mock
  private AbstractOrderModel orderMock;
  @Mock
  private AbstractOrderEntryModel firstOrderEntryMock, secondOrderEntryMock;

  @Before
  public void setUp() {
    when(shippingFeesMock.getOrders()).thenReturn(singletonList(shippingFeeMock));
    when(shippingFeesMock.getErrors()).thenReturn(singletonList(errorMock));
    when(shippingFeeMock.getOffers()).thenReturn(singletonList(offerMock));
    when(shippingFeeMock.getShippingTypes()).thenReturn(asList(firstShippingTypeMock, secondShippingTypeMock));
    when(offerMock.getId()).thenReturn(OFFER_ID);
    when(errorMock.getOfferId()).thenReturn(OFFER_ID);

    when(jsonMarshallingServiceMock.fromJson(SHIPPING_FEES_JSON, MiraklOrderShippingFees.class)).thenReturn(shippingFeesMock);
    when(jsonMarshallingServiceMock.toJson(shippingFeesMock, MiraklOrderShippingFees.class)).thenReturn(SHIPPING_FEES_JSON);

    when(orderMock.getMarketplaceEntries()).thenReturn(asList(firstOrderEntryMock, secondOrderEntryMock));
    when(firstOrderEntryMock.getOfferId()).thenReturn(OFFER_1);
    when(secondOrderEntryMock.getOfferId()).thenReturn(OFFER_2);
    when(firstOfferMock.getId()).thenReturn(OFFER_1);
    when(secondOfferMock.getId()).thenReturn(OFFER_2);

    when(offerMock.getLineShippingPrice()).thenReturn(BigDecimal.valueOf(LINE_SHIPPING_PRICE));

    when(shippingFeeMock.getSelectedShippingType()).thenReturn(selectedShippingTypeMock);
    when(selectedShippingTypeMock.getCode()).thenReturn(SELECTED_SHIPPING_OPTION_CODE);
    when(selectedShippingTypeMock.getLabel()).thenReturn(SELECTED_SHIPPING_OPTION_LABEL);
  }

  @Test
  public void getsShippingFeeOffer() {
    Optional<MiraklOrderShippingFeeOffer> result = testObj.extractShippingFeeOffer(OFFER_ID, shippingFeesMock);

    assertThat(result.isPresent()).isTrue();
    assertThat(result.get()).isSameAs(offerMock);
  }

  @Test
  public void getShippingFeeOfferReturnsAbsentOptionalIfNoOfferFound() {
    when(shippingFeeMock.getOffers()).thenReturn(Collections.<MiraklOrderShippingFeeOffer>emptyList());

    Optional<MiraklOrderShippingFeeOffer> result = testObj.extractShippingFeeOffer(OFFER_ID, shippingFeesMock);

    assertThat(result.isPresent()).isFalse();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeOfferThrowsIllegalArgumentExceptionIfOfferIdIsNull() {
    testObj.extractShippingFeeOffer(null, shippingFeesMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeOfferThrowsIllegalArgumentExceptionIfShippingRatesIsNull() {
    testObj.extractShippingFeeOffer(OFFER_ID, null);
  }

  @Test
  public void getsShippingFeeError() {
    Optional<MiraklOrderShippingFeeError> result = testObj.extractShippingFeeError(OFFER_ID, shippingFeesMock);

    assertThat(result.isPresent()).isTrue();
    assertThat(result.get()).isSameAs(errorMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeErrorThrowsIllegalArgumentExceptionIfOfferIdIsNull() {
    testObj.extractShippingFeeError(null, shippingFeesMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeErrorThrowsIllegalArgumentExceptionIfShippingRatesIsNull() {
    testObj.extractShippingFeeError(OFFER_ID, null);
  }

  @Test
  public void getsShippingFeesJSON() {
    when(orderMock.getShippingFeesJSON()).thenReturn(SHIPPING_FEES_JSON);

    MiraklOrderShippingFees result = testObj.getStoredShippingFees(orderMock);

    assertThat(result).isSameAs(shippingFeesMock);
  }

  @Test
  public void getShippingFeesJSONReturnsNullIfShippingFeesJSONIsNull() {
    when(orderMock.getShippingFeesJSON()).thenReturn(null);

    MiraklOrderShippingFees result = testObj.getStoredShippingFees(orderMock);

    assertThat(result).isNull();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeesJSONThrowsIllegalArgumentExceptionIfOrderNull() {
    testObj.getStoredShippingFees((AbstractOrderModel) null);
  }

  @Test
  public void getsShippingFees() {
    String result = testObj.getShippingFeesAsJson(shippingFeesMock);

    assertThat(result).isSameAs(SHIPPING_FEES_JSON);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeesThrowsIllegalArgumentExceptionIfShippingFeesNull() {
    testObj.getShippingFeesAsJson((MiraklOrderShippingFees) null);
  }

  @Test
  public void getsShippingFeeForShop() {
    when(shippingFeeMock.getShopId()).thenReturn(SHOP_ID);
    when(shippingFeeMock.getLeadtimeToShip()).thenReturn(LEAD_TIME_TO_SHIP);

    Optional<MiraklOrderShippingFee> result = testObj.extractShippingFeeForShop(shippingFeesMock, SHOP_ID, LEAD_TIME_TO_SHIP);

    assertThat(result.isPresent()).isTrue();
    assertThat(result.get()).isSameAs(shippingFeeMock);
  }

  @Test
  public void getShippingFeeForShopReturnsAbsentOptionalIfNoShopWasFound() {
    Optional<MiraklOrderShippingFee> result = testObj.extractShippingFeeForShop(shippingFeesMock, SHOP_ID, LEAD_TIME_TO_SHIP);

    assertThat(result.isPresent()).isFalse();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeForShopThrowsIllegalArgumentExceptionIfShippingFeesIsNull() {
    testObj.extractShippingFeeForShop(null, SHOP_ID, LEAD_TIME_TO_SHIP);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeForShopThrowsIllegalArgumentExceptionIfShopIdIsNull() {
    testObj.extractShippingFeeForShop(shippingFeesMock, null, LEAD_TIME_TO_SHIP);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getShippingFeeForShopThrowsIllegalArgumentExceptionIfLeadTimeToShipNull() {
    testObj.extractShippingFeeForShop(shippingFeesMock, SHOP_ID, null);
  }

  @Test
  public void getsAllShippingFeeOffers() {
    when(shippingFeesMock.getOrders()).thenReturn(asList(firstShippingFeeMock, secondShippingFeeMock));
    when(firstShippingFeeMock.getOffers()).thenReturn(singletonList(firstOfferMock));
    when(secondShippingFeeMock.getOffers()).thenReturn(singletonList(secondOfferMock));

    List<MiraklOrderShippingFeeOffer> result = testObj.extractAllShippingFeeOffers(shippingFeesMock);

    assertThat(result).containsOnly(firstOfferMock, secondOfferMock);
  }

  @Test
  public void getAllShippingFeeOffersReturnsEmptyListIfNoOffersFound() {
    when(shippingFeesMock.getOrders()).thenReturn(asList(firstShippingFeeMock, secondShippingFeeMock));

    List<MiraklOrderShippingFeeOffer> result = testObj.extractAllShippingFeeOffers(shippingFeesMock);

    assertThat(result).isEmpty();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getAllShippingFeeOffersThrowsIllegalArgumentExceptionIfShippingFeesIsNull() {
    testObj.extractAllShippingFeeOffers(null);
  }

  @Test
  public void updatesSelectedShippingOption() {
    when(secondShippingTypeMock.getCode()).thenReturn(SELECTED_SHIPPING_OPTION_CODE);

    testObj.updateSelectedShippingOption(shippingFeeMock, SELECTED_SHIPPING_OPTION_CODE);

    verify(shippingFeeMock).setSelectedShippingType(secondShippingTypeMock);
  }

  @Test
  public void updateSelectedShippingOptionDoesNotChangeSelectedShippingTypeIfNoShippingTypeFound() {
    testObj.updateSelectedShippingOption(shippingFeeMock, SELECTED_SHIPPING_OPTION_CODE);

    verify(shippingFeeMock, never()).setSelectedShippingType(any(MiraklShippingFeeType.class));
  }

  @Test(expected = IllegalArgumentException.class)
  public void updateSelectedShippingOptionThrowsIllegalArgumentExceptionIfShippingFeeIsNull() {
    testObj.updateSelectedShippingOption(null, SELECTED_SHIPPING_OPTION_CODE);
  }

  @Test(expected = IllegalArgumentException.class)
  public void updateSelectedShippingOptionThrowsIllegalArgumentExceptionIfSelectedShippingOptionIsNull() {
    testObj.updateSelectedShippingOption(shippingFeeMock, null);
  }

  @Test
  public void setsLineShippingDetails() {
    when(firstOrderEntryMock.getOfferId()).thenReturn(OFFER_ID);

    testObj.setLineShippingDetails(orderMock, shippingFeesMock);

    verify(firstOrderEntryMock).setLineShippingPrice(LINE_SHIPPING_PRICE);
    verify(firstOrderEntryMock).setLineShippingCode(SELECTED_SHIPPING_OPTION_CODE);
    verify(firstOrderEntryMock).setLineShippingLabel(SELECTED_SHIPPING_OPTION_LABEL);
  }

  @Test
  public void setLineShippingPricesDoesNotSetShippingLinesIfNoOfferFound() {
    when(shippingFeesMock.getOrders()).thenReturn(asList(firstShippingFeeMock, secondShippingFeeMock));
    when(firstShippingFeeMock.getOffers()).thenReturn(Collections.<MiraklOrderShippingFeeOffer>emptyList());
    when(secondShippingFeeMock.getOffers()).thenReturn(Collections.<MiraklOrderShippingFeeOffer>emptyList());

    testObj.setLineShippingDetails(orderMock, shippingFeesMock);

    verify(firstOrderEntryMock).getOfferId();
    verify(secondOrderEntryMock).getOfferId();
    verifyNoMoreInteractions(firstOrderEntryMock);
    verifyNoMoreInteractions(secondOrderEntryMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void setLineShippingPricesThrowsIllegalArgumentExceptionIfOrderIsNull() {
    testObj.setLineShippingDetails(null, shippingFeesMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void setLineShippingPricesThrowsIllegalArgumentExceptionIfSelectedShippingFeesIsNull() {
    MiraklOrderShippingFees orderShippingFees = null;
    testObj.setLineShippingDetails(orderMock, orderShippingFees);
  }
}
