package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.mirakl.hybris.core.enums.MiraklExportType.CATALOG_CATEGORY_EXPORT;
import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mci.front.core.MiraklCatalogIntegrationFrontApi;
import com.mirakl.client.mci.front.domain.hierarchy.MiraklHierarchyImportResult;
import com.mirakl.client.mci.front.request.hierarchy.MiraklHierarchyImportErrorReportRequest;
import com.mirakl.client.mci.front.request.hierarchy.MiraklHierarchyImportStatusRequest;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.jobs.dao.MiraklJobReportDao;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCatalogCategoryExportReportStrategyTest {

  private static final String SYNC_JOB_ID = "1523";

  @Mock
  private MiraklCatalogIntegrationFrontApi mciApi;

  @Mock
  private Populator<MiraklHierarchyImportResult, MiraklJobReportModel> reportPopulator;

  @Mock
  private MiraklHierarchyImportResult exportResult;

  @Mock
  private MiraklJobReportDao miraklJobReportDao;

  @Mock
  private MiraklJobReportModel pendingReport1, pendingReport2;

  @Mock
  private Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Mock
  private MiraklHierarchyImportResult hierarchiesImportResult;

  @Mock
  private File errorReportFile;

  @InjectMocks
  private DefaultCatalogCategoryExportReportStrategy testObj;

  @Test
  public void getErrorReportFile() throws Exception {
    when(mciApi.getHierarchiyImportErrorReport(any(MiraklHierarchyImportErrorReportRequest.class))).thenReturn(errorReportFile);

    File output = testObj.getErrorReportFile(SYNC_JOB_ID);

    assertThat(output).isEqualTo(errorReportFile);
  }

  @Test
  public void getExportResult() throws Exception {
    when(mciApi.getHierarchyImportResult(any(MiraklHierarchyImportStatusRequest.class))).thenReturn(hierarchiesImportResult);

    MiraklHierarchyImportResult output = testObj.getExportResult(SYNC_JOB_ID);

    assertThat(output).isEqualTo(hierarchiesImportResult);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getErrorReportFileWithNullSyncJobId() throws Exception {
    File output = testObj.getErrorReportFile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getExportResultWithNullSyncJobId() throws Exception {
    MiraklHierarchyImportResult output = testObj.getExportResult(null);
  }

  @Test
  public void isExportPending() throws Exception {
    when(exportStatuses.get(MiraklProcessTrackingStatus.QUEUED)).thenReturn(MiraklExportStatus.PENDING);
    when(exportResult.getImportStatus()).thenReturn(MiraklProcessTrackingStatus.QUEUED);

    boolean output = testObj.isExportCompleted(exportResult);

    assertThat(output).isFalse();
  }

  @Test
  public void isExportCompleted() throws Exception {
    when(exportStatuses.get(MiraklProcessTrackingStatus.COMPLETE)).thenReturn(MiraklExportStatus.COMPLETE);
    when(exportResult.getImportStatus()).thenReturn(MiraklProcessTrackingStatus.COMPLETE);

    boolean output = testObj.isExportCompleted(exportResult);

    assertThat(output).isTrue();
  }

  @Test
  public void getPendingMiraklJobReports() throws Exception {
    List<MiraklJobReportModel> pendingReports = asList(pendingReport1, pendingReport2);
    when(miraklJobReportDao.findPendingJobReportsForType(CATALOG_CATEGORY_EXPORT)).thenReturn(pendingReports);

    List<MiraklJobReportModel> output = testObj.getPendingMiraklJobReports();

    assertThat(output).isEqualTo(pendingReports);
  }

  @Test
  public void getReportPopulator() throws Exception {
    Populator<MiraklHierarchyImportResult, MiraklJobReportModel> output = testObj.getReportPopulator();

    assertThat(output).isEqualTo(reportPopulator);
  }

}
