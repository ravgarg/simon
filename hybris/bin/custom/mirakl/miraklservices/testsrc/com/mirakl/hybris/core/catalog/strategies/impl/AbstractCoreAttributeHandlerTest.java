package com.mirakl.hybris.core.catalog.strategies.impl;

import static org.mockito.Mockito.when;
import static org.mockito.internal.util.collections.Sets.newSet;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.mockito.Mock;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public abstract class AbstractCoreAttributeHandlerTest {

  protected static final String PRODUCT_ITEM_TYPE = "ApparelSizeVariantProduct";
  protected static final String SIZE_ATTRIBUTE_QUALIFIER = "size";
  protected static final String STYLE_ATTRIBUTE_QUALIFIER = "style";
  protected static final String COMPOSED_TYPE_CODE = "ApparelSizeVariantProduct";

  @Mock
  protected TypeService typeService;
  @Mock
  protected ModelService modelService;
  @Mock
  protected CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;
  @Mock
  protected MiraklCoreAttributeModel coreAttribute;
  @Mock
  protected ProductImportData data;
  @Mock
  protected AttributeValueData attributeValue;
  @Mock
  protected ProductImportFileContextData context;
  @Mock
  protected VariantProductModel ownerProduct;
  @Mock
  protected ComposedTypeModel composedType;
  @Mock
  protected ProductImportGlobalContextData globalContext;

  protected Map<String, Set<String>> attributesPerType = new HashMap<>();

  public void setUp() throws Exception {
    attributesPerType.put(COMPOSED_TYPE_CODE, newSet(SIZE_ATTRIBUTE_QUALIFIER, STYLE_ATTRIBUTE_QUALIFIER));
    when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(ownerProduct);
    when(ownerProduct.getItemtype()).thenReturn(PRODUCT_ITEM_TYPE);
    when(typeService.getComposedTypeForCode(PRODUCT_ITEM_TYPE)).thenReturn(composedType);
    when(context.getGlobalContext()).thenReturn(globalContext);
    when(globalContext.getAttributesPerType()).thenReturn(attributesPerType);
    when(attributeValue.getCoreAttribute()).thenReturn(coreAttribute);
  }


}
