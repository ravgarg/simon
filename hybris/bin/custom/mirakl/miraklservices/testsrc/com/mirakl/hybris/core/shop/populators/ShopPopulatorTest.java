package com.mirakl.hybris.core.shop.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import com.mirakl.client.mmp.domain.shop.*;
import com.mirakl.hybris.core.i18n.services.CountryService;
import de.hybris.platform.core.model.c2l.CountryModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.hybris.core.i18n.services.CurrencyService;
import com.mirakl.hybris.core.model.ShopModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShopPopulatorTest {

  private static final String SHOP_ID = "2086";
  private static final String OPERATOR_INTERNAL_ID = "operator-internal-id";
  private static final BigDecimal GRADE = BigDecimal.valueOf(4.567);
  private static final String NAME = "name";
  private static final MiraklPremiumState PREMIUM_STATE = MiraklPremiumState.NOT_PREMIUM;
  private static final Long EVALUATION_COUNT = 10000L;
  private static final MiraklShopState STATE = MiraklShopState.SUSPENDED;
  private static final MiraklIsoCurrencyCode CURRENCY_CODE = MiraklIsoCurrencyCode.USD;
  private static final String SHOP_COUNTRY_ISO_ALPHA_3 = "FRA";


  @InjectMocks
  private ShopPopulator populator = new ShopPopulator();

  @Mock
  private MiraklShop miraklShop;

  @Mock
  private MiraklShippingInformation miraklShippingInfo;

  @Mock
  private MiraklShopStats miraklShopStats;

  @Mock
  private CurrencyService currencyService;

  @Mock
  private CountryService countryService;

  @Mock
  private CurrencyModel currencyModel;

  @Mock
  private CountryModel countryModel;


  @Before
  public void setUp() {
    when(miraklShop.getId()).thenReturn(SHOP_ID);
    when(miraklShop.getOperatorInternalId()).thenReturn(OPERATOR_INTERNAL_ID);
    when(miraklShop.getName()).thenReturn(NAME);
    when(miraklShop.getGrade()).thenReturn(GRADE);
    when(miraklShop.getPremiumState()).thenReturn(PREMIUM_STATE);
    when(miraklShop.getState()).thenReturn(STATE);
    when(miraklShop.getGrade()).thenReturn(GRADE);
    when(miraklShop.getShopStatistic()).thenReturn(miraklShopStats);
    when(miraklShopStats.getEvaluationsCount()).thenReturn(EVALUATION_COUNT);
    when(miraklShop.getCurrencyIsoCode()).thenReturn(CURRENCY_CODE);
    when(currencyService.getCurrencyForCode(CURRENCY_CODE.name())).thenReturn(currencyModel);
    when(currencyModel.getIsocode()).thenReturn(CURRENCY_CODE.name());
    when(miraklShop.getShippingInformation()).thenReturn(miraklShippingInfo);
    when(miraklShippingInfo.getShippingCountry()).thenReturn(SHOP_COUNTRY_ISO_ALPHA_3);
    when(countryService.getCountryForIsoAlpha3Code(SHOP_COUNTRY_ISO_ALPHA_3)).thenReturn(countryModel);
  }

  @Test
  public void populate() {
    ShopModel shopModel = new ShopModel();
    populator.populate(miraklShop, shopModel);

    assertEquals(SHOP_ID, shopModel.getId());
    assertEquals(OPERATOR_INTERNAL_ID, shopModel.getInternalId());
    assertEquals(NAME, shopModel.getName());
    assertEquals((Double) GRADE.doubleValue(), shopModel.getGrade());
    assertEquals(String.valueOf(PREMIUM_STATE), String.valueOf(shopModel.getPremiumState()));
    assertEquals(String.valueOf(STATE), String.valueOf(shopModel.getState()));
    assertEquals(EVALUATION_COUNT, (Long) shopModel.getEvaluationCount().longValue());
    assertEquals(countryModel, shopModel.getShippingCountry());

  }
}
