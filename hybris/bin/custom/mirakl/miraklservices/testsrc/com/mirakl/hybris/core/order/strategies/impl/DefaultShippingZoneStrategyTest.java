package com.mirakl.hybris.core.order.strategies.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.order.strategies.impl.DefaultShippingZoneStrategy;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShippingZoneStrategyTest {

  private static final String SHIPPING_ZONE_CODE = "FR";

  private DefaultShippingZoneStrategy testObj = new DefaultShippingZoneStrategy();

  @Mock
  private AbstractOrderModel orderMock;
  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private AddressModel deliveryAddressMock;

  @Before
  public void setUp() {
    when(orderMock.getDeliveryAddress()).thenReturn(deliveryAddressMock);
    when(deliveryAddressMock.getCountry().getIsocode()).thenReturn(SHIPPING_ZONE_CODE);
  }

  @Test
  public void getsShippingZoneCodeFromCountryIsoCode() {
    String result = testObj.getShippingZoneCode(orderMock);

    assertThat(result).isEqualTo(SHIPPING_ZONE_CODE);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfOrderIsNull() {
    testObj.getShippingZoneCode(null);
  }

  @Test(expected = IllegalStateException.class)
  public void throwsIllegalStateExceptionIfDeliveryAddressIsNull() {
    when(orderMock.getDeliveryAddress()).thenReturn(null);

    testObj.getShippingZoneCode(orderMock);
  }
}
