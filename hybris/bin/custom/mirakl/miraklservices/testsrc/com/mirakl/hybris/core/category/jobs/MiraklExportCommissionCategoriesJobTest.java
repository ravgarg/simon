package com.mirakl.hybris.core.category.jobs;

import static com.mirakl.hybris.core.enums.MiraklExportType.COMMISSION_CATEGORY_EXPORT;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.category.synchro.MiraklCategorySynchroTracking;
import com.mirakl.hybris.core.category.services.CategoryExportService;
import com.mirakl.hybris.core.enums.MiraklExportType;
import com.mirakl.hybris.core.jobs.services.ExportJobReportService;
import com.mirakl.hybris.core.model.MiraklExportCommissionCategoriesCronJobModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.i18n.I18NService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklExportCommissionCategoriesJobTest {

  private static final String SYNC_JOB_ID = "syncJobId";
  private static final String SYNCHRONIZATION_FILE_NAME = "Synchronization file name";

  @InjectMocks
  private MiraklExportCommissionCategoriesJob testObj = new MiraklExportCommissionCategoriesJob();

  @Mock
  private CategoryExportService categoryExportServiceMock;
  @Mock
  private CommonI18NService commonI18NServiceMock;
  @Mock
  private I18NService i18NServiceMock;
  @Mock
  private ExportJobReportService exportJobReportServiceMock;

  @Mock
  private MiraklExportCommissionCategoriesCronJobModel miraklExportCategoriesCronJobMock;
  @Mock
  private CategoryModel rootCategoryMock, firstCommissionCategoryMock, secondCommissionCategoryMock;
  @Mock
  private LanguageModel languageMock;
  @Mock
  private MiraklCategorySynchroTracking miraklCategorySynchroTrackingMock;

  @Before
  public void setUp() throws IOException {
    when(miraklExportCategoriesCronJobMock.getRootCategory()).thenReturn(rootCategoryMock);
    when(miraklExportCategoriesCronJobMock.getSessionLanguage()).thenReturn(languageMock);
    when(miraklExportCategoriesCronJobMock.getSynchronizationFileName()).thenReturn(SYNCHRONIZATION_FILE_NAME);

    when(commonI18NServiceMock.getLocaleForLanguage(languageMock)).thenReturn(Locale.ENGLISH);

    when(categoryExportServiceMock.exportCommissionCategories(eq(rootCategoryMock), any(Locale.class), anyString()))
        .thenReturn(miraklCategorySynchroTrackingMock);
    when(miraklCategorySynchroTrackingMock.getSynchroId()).thenReturn(SYNC_JOB_ID);
  }

  @Test
  public void exportsCategoriesForConfiguredCatalogVersionAndSessionLanguage() throws IOException {
    PerformResult result = testObj.perform(miraklExportCategoriesCronJobMock);

    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    verify(categoryExportServiceMock).exportCommissionCategories(rootCategoryMock, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME);
    verify(exportJobReportServiceMock).createMiraklJobReport(SYNC_JOB_ID, COMMISSION_CATEGORY_EXPORT);
  }

  @Test
  public void exportCategoriesEndsWithErrorIfMiraklApiExceptionOccurs() throws IOException {
    when(categoryExportServiceMock.exportCommissionCategories(rootCategoryMock, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    PerformResult result = testObj.perform(miraklExportCategoriesCronJobMock);

    assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);

    verify(exportJobReportServiceMock, never()).createMiraklJobReport(anyString(), any(MiraklExportType.class));
  }

  @Test
  public void exportCategoriesEndsWithErrorIfIOExceptionOccurs() throws IOException {
    when(categoryExportServiceMock.exportCommissionCategories(eq(rootCategoryMock), any(Locale.class), anyString()))
        .thenThrow(new IOException());

    PerformResult result = testObj.perform(miraklExportCategoriesCronJobMock);

    assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);

    verify(exportJobReportServiceMock, never()).createMiraklJobReport(anyString(), any(MiraklExportType.class));
  }

  @Test
  public void exportsCategoriesForCurrentLanguageIfNoSessionLanguageFound() throws IOException {
    when(miraklExportCategoriesCronJobMock.getSessionLanguage()).thenReturn(null);
    when(i18NServiceMock.getCurrentLocale()).thenReturn(Locale.FRENCH);

    PerformResult result = testObj.perform(miraklExportCategoriesCronJobMock);

    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);

    verify(categoryExportServiceMock).exportCommissionCategories(rootCategoryMock, Locale.FRENCH, SYNCHRONIZATION_FILE_NAME);
    verify(exportJobReportServiceMock).createMiraklJobReport(SYNC_JOB_ID, COMMISSION_CATEGORY_EXPORT);
  }
}
