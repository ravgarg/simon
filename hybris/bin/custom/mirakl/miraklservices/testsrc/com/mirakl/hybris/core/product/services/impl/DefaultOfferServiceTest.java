package com.mirakl.hybris.core.product.services.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.daos.OfferDao;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.core.product.strategies.OfferRelevanceSortingStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOfferServiceTest {

  private static final String PRODUCT_CODE = "test";
  private static final String OFFER_ID = "offerId";

  @InjectMocks
  private DefaultOfferService testObj;

  @Mock
  private OfferDao offerDaoMock;
  @Mock
  private OfferRelevanceSortingStrategy sortingStrategyMock;
  @Mock
  private OfferCodeGenerationStrategy offerCodeGenerationStrategyMock;
  @Mock
  private MiraklExportOffer newMiraklExportOfferMock, existingMiraklExportOfferMock;
  @Mock
  private OfferModel newOfferModelMock, existingOfferModelMock, offerModelMockSample1, offerModelMockSample2;
  @Mock
  private CurrencyModel currencyMock;

  @Mock
  private OfferModel offer;

  @Mock
  private CommonI18NService commonI18NService;

  @Before
  public void setUp() {
    when(offerDaoMock.findOfferById(OFFER_ID)).thenReturn(offer);
  }

  @Test
  public void shouldGetOfferForId() {
    OfferModel result = testObj.getOfferForId(OFFER_ID);

    assertThat(result).isEqualTo(offer);
  }

  @Test(expected = UnknownIdentifierException.class)
  public void shouldThrowUnknownIdentifierExceptionOnNotFoundOffer() {
    when(offerDaoMock.findOfferById(OFFER_ID)).thenReturn(null);

    testObj.getOfferForId(OFFER_ID);
  }

  @Test
  public void getSortedOffersForProductCodeWhenOffersExist() {
    List<OfferModel> sampleOfferList = Arrays.asList(offerModelMockSample1, offerModelMockSample2);
    when(offerDaoMock.findOffersForProductCodeAndCurrency(anyString(), any(CurrencyModel.class))).thenReturn(sampleOfferList);
    when(sortingStrategyMock.sort(sampleOfferList)).thenReturn(sampleOfferList);

    List<OfferModel> resultObj = testObj.getSortedOffersForProductCode(PRODUCT_CODE);

    assertThat(resultObj).contains(offerModelMockSample1, offerModelMockSample2);
  }

  @Test
  public void getSortedOffersForProductCodeWhenNoOffersExist() {
    List<OfferModel> emptyList = new ArrayList<>();
    when(offerDaoMock.findOffersForProductCodeAndCurrency(anyString(), any(CurrencyModel.class))).thenReturn(emptyList);
    when(sortingStrategyMock.sort(emptyList)).thenReturn(emptyList);

    List<OfferModel> resultObj = testObj.getSortedOffersForProductCode(PRODUCT_CODE);

    assertThat(resultObj).hasSize(0);
  }

  @Test
  public void hasOffersReturnsTrueIfAtLeastOneOfferFound() {
    when(offerDaoMock.countOffersForProduct(PRODUCT_CODE)).thenReturn(1);

    boolean result = testObj.hasOffers(PRODUCT_CODE);

    assertThat(result).isTrue();
  }

  @Test
  public void hasOffersReturnsTrueIfMoreThanOneOfferFound() {
    when(offerDaoMock.countOffersForProduct(PRODUCT_CODE)).thenReturn(2);

    boolean result = testObj.hasOffers(PRODUCT_CODE);

    assertThat(result).isTrue();
  }

  @Test
  public void hasOffersReturnsFalseIfNoOfferFound() {
    when(offerDaoMock.countOffersForProduct(PRODUCT_CODE)).thenReturn(0);

    boolean result = testObj.hasOffers(PRODUCT_CODE);

    assertThat(result).isFalse();
  }

  @Test
  public void hasOffersWithCurrencyReturnsTrueIfMoreThanOneOfferFound() {
    when(offerDaoMock.countOffersForProductAndCurrency(PRODUCT_CODE, currencyMock)).thenReturn(2);

    boolean result = testObj.hasOffersWithCurrency(PRODUCT_CODE, currencyMock);

    assertThat(result).isTrue();
  }

  @Test
  public void hasOffersWithCurrencyReturnsFalseIfNoOfferFound() {
    when(offerDaoMock.countOffersForProductAndCurrency(PRODUCT_CODE, currencyMock)).thenReturn(0);

    boolean result = testObj.hasOffersWithCurrency(PRODUCT_CODE, currencyMock);

    assertThat(result).isFalse();
  }
}
