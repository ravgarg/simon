package com.mirakl.hybris.core.services.impl.fulfilment;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.domain.order.create.MiraklOfferNotShippable;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklCreateOrderRequest;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklValidOrderRequest;
import com.mirakl.hybris.core.order.services.impl.DefaultMiraklOrderService;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultMiraklOrderServiceTest {

  private static final String JSON_STRING = "json-string";
  private static final String OFFER_ID = "offerId";
  private static final String ORDER_CODE = "orderCode";
  @InjectMocks
  private DefaultMiraklOrderService testObj;

  @Mock
  private MiraklMarketplacePlatformFrontApiClient miraklApiMock;
  @Mock
  private Converter<OrderModel, MiraklCreateOrder> miraklCreateOrderConverterMock;
  @Mock
  private ModelService modelServiceMock;
  @Mock
  private JsonMarshallingService jsonMarshallingService;
  @Mock
  private OrderModel orderMock;
  @Mock
  private MiraklCreateOrder miraklCreateOrderMock;
  @Mock
  private MiraklCreatedOrders miraklCreateOrdersMock;
  @Mock
  private MiraklOfferNotShippable offerNotShippableMock;
  @Mock
  private AbstractOrderEntryModel shippableOfferEntryMock, notShippableOfferEntryMock;

  @Captor
  private ArgumentCaptor<MiraklCreateOrderRequest> createOrderRequestCaptor;
  @Captor
  private ArgumentCaptor<MiraklValidOrderRequest> validOrderRequestCaptor;

  @Before
  public void setUp() {
    when(miraklCreateOrderConverterMock.convert(orderMock)).thenReturn(miraklCreateOrderMock);
    when(miraklApiMock.createOrder(createOrderRequestCaptor.capture())).thenReturn(miraklCreateOrdersMock);
    when(orderMock.getMarketplaceEntries()).thenReturn(asList(shippableOfferEntryMock, notShippableOfferEntryMock));
    when(orderMock.getCode()).thenReturn(ORDER_CODE);
    when(offerNotShippableMock.getId()).thenReturn(OFFER_ID);
    when(notShippableOfferEntryMock.getOfferId()).thenReturn(OFFER_ID);
    when(jsonMarshallingService.toJson(miraklCreateOrdersMock, MiraklCreatedOrders.class)).thenReturn(JSON_STRING);
    when(jsonMarshallingService.fromJson(JSON_STRING, MiraklCreatedOrders.class)).thenReturn(miraklCreateOrdersMock);
  }

  @Test
  public void createsMarketplaceOrder() {
    MiraklCreatedOrders result = testObj.createMarketplaceOrders(orderMock);

    assertThat(result).isSameAs(miraklCreateOrdersMock);

    verify(miraklCreateOrderConverterMock).convert(orderMock);
    verify(miraklApiMock).createOrder(createOrderRequestCaptor.capture());

    MiraklCreateOrderRequest createOrderRequest = createOrderRequestCaptor.getValue();
    assertThat(createOrderRequest.getCreateOrder()).isSameAs(miraklCreateOrderMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void createMarketplaceOrderThrowsIllegalArgumentExceptionIfOrderIsNull() {
    testObj.createMarketplaceOrders(null);
  }

  @Test
  public void marksNotShippableEntries() {
    List<AbstractOrderEntryModel> result = testObj.extractNotShippableEntries(singletonList(offerNotShippableMock), orderMock);

    assertThat(result).containsOnly(notShippableOfferEntryMock);

    verify(orderMock).getMarketplaceEntries();
  }

  @Test(expected = IllegalArgumentException.class)
  public void markNotShippableEntriesThrowsIllegalArgumentExceptionIfOrderIsNull() {
    testObj.extractNotShippableEntries(Collections.<MiraklOfferNotShippable>emptyList(), null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void markNotShippableEntriesThrowsIllegalArgumentExceptionIfNotShippableOfferListIsNull() {
    testObj.extractNotShippableEntries(null, orderMock);
  }

  @Test
  public void storesCreatedOrders() {
    testObj.createMarketplaceOrders(orderMock);

    verify(orderMock).setCreatedOrdersJSON(JSON_STRING);
    verify(modelServiceMock).save(orderMock);
  }

  @Test
  public void loadsCreatedOrders() {
    when(orderMock.getCreatedOrdersJSON()).thenReturn(JSON_STRING);

    MiraklCreatedOrders createdOrders = testObj.loadCreatedOrders(orderMock);

    assertThat(createdOrders).isEqualTo(miraklCreateOrdersMock);
  }

  @Test
  public void loadsNullForEmptyCreatedOrders() {
    when(orderMock.getCreatedOrdersJSON()).thenReturn(null);

    MiraklCreatedOrders createdOrders = testObj.loadCreatedOrders(orderMock);

    assertThat(createdOrders).isNull();
  }

  @Test
  public void validatesOrder() {
    testObj.validateOrder(orderMock);

    verify(miraklApiMock).validOrder(validOrderRequestCaptor.capture());
    assertThat(validOrderRequestCaptor.getValue().getCommercialId()).isEqualTo(orderMock.getCode());
  }

  @Test(expected = IllegalArgumentException.class)
  public void validateOrderThrowsIllegalArgumentExceptionIfOrderNull() {
    testObj.validateOrder(null);
  }

  @Test
  public void getAssessments() {
    testObj.getAssessments();

    verify(miraklApiMock).getAssessments();
  }

}
