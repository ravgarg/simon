package com.mirakl.hybris.core.product.daos.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.daos.OfferDao;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTest;

@IntegrationTest
public class OfferDaoIntegrationTest extends ServicelayerTest {

  private static final String TEST_OFFER_ID_1 = "testOffer1";
  private static final String TEST_OFFER_ID_2 = "testOffer2";

  @Resource
  private OfferDao offerDao;

  @Before
  public void setUp() throws ImpExException {
    importCsv("/miraklservices/test/testOffers.impex", "utf-8");
  }

  @Test
  public void findsAllOffers() {
    List<OfferModel> result = offerDao.findAllOffers();

    assertThat(result).hasSize(2);
    assertThat(result).onProperty(OfferModel.ID).containsOnly(TEST_OFFER_ID_1, TEST_OFFER_ID_2);
  }

  @Test
  public void findsOfferById() {
    OfferModel result = offerDao.findOfferById(TEST_OFFER_ID_1);

    assertThat(result.getId()).isEqualTo(TEST_OFFER_ID_1);
  }
}
