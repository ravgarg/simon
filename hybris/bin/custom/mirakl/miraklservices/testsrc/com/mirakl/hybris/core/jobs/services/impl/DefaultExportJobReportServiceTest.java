package com.mirakl.hybris.core.jobs.services.impl;

import static com.mirakl.hybris.core.enums.MiraklExportType.COMMISSION_CATEGORY_EXPORT;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.mirakl.hybris.core.jobs.strategies.ExportReportStrategy;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultExportJobReportServiceTest {

  private static final String JOB_ID = "jobId";

  @InjectMocks
  private DefaultExportJobReportService testObj = new DefaultExportJobReportService();

  @Mock
  private ModelService modelServiceMock;
  @Mock
  private ExportReportStrategy categoryExportReportStrategyMock, productExportReportStrategyMock;

  @Mock
  private MiraklJobReportModel miraklJobReportMock;

  @Before
  public void setUp() {
    when(modelServiceMock.create(MiraklJobReportModel.class)).thenReturn(miraklJobReportMock);
    when(categoryExportReportStrategyMock.updatePendingExports()).thenReturn(true);
    when(productExportReportStrategyMock.updatePendingExports()).thenReturn(true);

    testObj.setExportTypeStrategies(ImmutableList.of(categoryExportReportStrategyMock, productExportReportStrategyMock));
  }

  @Test
  public void createsMiraklJobReport() {
    MiraklJobReportModel result = testObj.createMiraklJobReport(JOB_ID, COMMISSION_CATEGORY_EXPORT);

    assertThat(result).isEqualTo(miraklJobReportMock);

    verify(miraklJobReportMock).setJobId(JOB_ID);
    verify(miraklJobReportMock).setReportType(COMMISSION_CATEGORY_EXPORT);
    verify(modelServiceMock).save(miraklJobReportMock);
  }

  @Test
  public void updatesPendingExportReports() {
    boolean result = testObj.updatePendingExportReports();

    assertThat(result).isTrue();

    verify(categoryExportReportStrategyMock).updatePendingExports();
    verify(productExportReportStrategyMock).updatePendingExports();
  }

  @Test
  public void updatePendingExportReportsReturnsFalseIfOneOfTheStrategiesDidNotEndSuccessfully() {
    when(productExportReportStrategyMock.updatePendingExports()).thenReturn(false);

    boolean result = testObj.updatePendingExportReports();

    assertThat(result).isFalse();

    verify(categoryExportReportStrategyMock).updatePendingExports();
    verify(productExportReportStrategyMock).updatePendingExports();
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfJobIdIsNull() {
    testObj.createMiraklJobReport(null, COMMISSION_CATEGORY_EXPORT);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfJobIdIsEmpty() {
    testObj.createMiraklJobReport(EMPTY, COMMISSION_CATEGORY_EXPORT);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfMiraklExportTypeIsNull() {
    testObj.createMiraklJobReport(JOB_ID, null);
  }
}
