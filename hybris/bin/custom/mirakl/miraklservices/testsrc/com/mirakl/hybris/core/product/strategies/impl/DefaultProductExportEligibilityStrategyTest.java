package com.mirakl.hybris.core.product.strategies.impl;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.product.daos.impl.DefaultMiraklProductDao;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductExportEligibilityStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultProductExportEligibilityStrategyTest {

  @InjectMocks
  private DefaultProductExportEligibilityStrategy strategy = new DefaultProductExportEligibilityStrategy();

  @Mock
  private DefaultMiraklProductDao customProductDao;

  @Mock
  private CatalogVersionModel catalogVersion;

  @Test
  public void shouldReturnModifiedProducts() {
    ProductModel product1 = mock(ProductModel.class);
    ProductModel product2 = mock(ProductModel.class);
    Date modificationDate = new Date();
    when(customProductDao.findModifiedProductsWithNoVariantType(modificationDate, catalogVersion))
        .thenReturn(asList(product1, product2));

    Collection<ProductModel> modifiedProducts = strategy.getModifiedProductsEligibleForExport(catalogVersion, modificationDate);

    assertThat(modifiedProducts).containsOnly(product1, product2);
  }

  @Test
  public void shouldReturnAllProducts() {
    ProductModel product1 = mock(ProductModel.class);
    ProductModel product2 = mock(ProductModel.class);
    when(customProductDao.findModifiedProductsWithNoVariantType(null, catalogVersion)).thenReturn(asList(product1, product2));

    Collection<ProductModel> modifiedProducts = strategy.getAllProductsEligibleForExport(catalogVersion);

    assertThat(modifiedProducts).containsOnly(product1, product2);
  }
}
