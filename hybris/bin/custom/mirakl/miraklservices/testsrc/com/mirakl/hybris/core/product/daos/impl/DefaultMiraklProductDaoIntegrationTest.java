package com.mirakl.hybris.core.product.daos.impl;


import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.mirakl.hybris.core.shop.services.ShopService;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTest;

@IntegrationTest
public class DefaultMiraklProductDaoIntegrationTest extends ServicelayerTest {

  private static final String PRODUCT_CATALOG = "productCatalog";
  private static final String STAGED_VERSION = "Staged";
  private static final String ONLINE_VERSION = "Online";

  @Resource
  private DefaultMiraklProductDao miraklProductDao;
  @Resource
  private CatalogVersionService catalogVersionService;
  @Resource
  private ShopService shopService;

  @Before
  public void setUp() throws Exception {
    importCsv("/miraklservices/test/testProducts.impex", "utf-8");
  }

  @Test
  public void shouldFindProductsWithNoVariantType() {
    List<ProductModel> products = miraklProductDao.findModifiedProductsWithNoVariantType(null,
        catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, STAGED_VERSION));

    ImmutableList<String> productCodes = FluentIterable.from(products).transform(toProductCode()).toList();
    assertThat(productCodes).containsOnly("product1", "product3");
  }

  @Test
  public void shouldFindProductForShopVariantGroupCodeInCatalog() {
    ProductModel product = miraklProductDao.findProductForShopVariantGroupCode(shopService.getShopForId("shop1"), "vg1",
        catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, STAGED_VERSION));

    assertThat(product).isNotNull();
    assertThat(product.getCode()).isEqualTo("product1");
  }

  @Test
  public void shouldFindProductForShopVariantGroupCodeForDifferentShop() {
    ProductModel product = miraklProductDao.findProductForShopVariantGroupCode(shopService.getShopForId("shop2"), "vg1",
        catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, STAGED_VERSION));

    assertThat(product).isNotNull();
    assertThat(product.getCode()).isEqualTo("product2");
  }

  @Test
  public void shouldFindProductForShopVariantGroupCodeForDifferentCatalog() {
    ProductModel product = miraklProductDao.findProductForShopVariantGroupCode(shopService.getShopForId("shop1"), "vg1",
        catalogVersionService.getCatalogVersion(PRODUCT_CATALOG, ONLINE_VERSION));

    assertThat(product).isNotNull();
    assertThat(product.getCode()).isEqualTo("product2");
  }


  protected Function<ProductModel, String> toProductCode() {
    return new Function<ProductModel, String>() {

      @Override
      public String apply(ProductModel product) {
        return product.getCode();
      }
    };
  }
}
