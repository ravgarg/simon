package com.mirakl.hybris.core.ordersplitting.services.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.UPDATE_RECEIVED_EVENT_NAME;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySetOf;
import static org.mockito.Mockito.*;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.payment.debit.MiraklOrderPayment;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.request.order.evaluation.MiraklCreateOrderEvaluation;
import com.mirakl.client.mmp.front.request.order.evaluation.MiraklCreateOrderEvaluationRequest;
import com.mirakl.client.mmp.request.order.worflow.MiraklCancelOrderRequest;
import com.mirakl.client.mmp.request.order.worflow.MiraklReceiveOrderRequest;
import com.mirakl.hybris.core.enums.MiraklOrderStatus;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.daos.ConsignmentEntryDao;
import com.mirakl.hybris.core.ordersplitting.daos.MarketplaceConsignmentDao;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultMarketplaceConsignmentServiceTest {

  private static final String CONSIGNMENT_JSON = "json-consignment";
  private static final String DEBIT_REQUEST_JSON = "json-debit-request";
  private static final String LOGISTIC_ORDER_CODE = "logistic-order-code";
  private static final String WRONG_USER_NAME = "wrongName";
  private static final String CONSIGNMENT_PROCESS_CODE = "consignment-process-code";
  private static final String CONSIGNMENT_CODE = "1234567-976543-79840496-A";
  private static final String CONSIGNMENT_ENTRY_CODE = "1234567-976543-79840496-A-1";

  @InjectMocks
  private DefaultMarketplaceConsignmentService marketplaceConsignmentService;

  @Mock
  private ModelService modelService;
  @Mock
  private JsonMarshallingService jsonMarshallingService;
  @Mock
  private BusinessProcessService businessProcessService;
  @Mock
  private MarketplaceConsignmentDao marketplaceConsignmentDao;
  @Mock
  private Converter<Pair<OrderModel, MiraklOrder>, MarketplaceConsignmentModel> miraklCreateConsignmentConverter;
  @Mock
  private OrderModel order;
  @Mock
  private MiraklCreatedOrders miraklCreatedOrders;
  @Mock
  private MiraklOrder miraklOrder;
  @Mock
  private MiraklOrderPayment miraklOrderPayment;
  @Mock
  private MarketplaceConsignmentModel consignment;
  @Mock
  private ConsignmentProcessModel consignmentProcess;
  @Mock
  private MiraklMarketplacePlatformFrontApiClient miraklApi;
  @Mock
  private UserModel user, wrongUser;
  @Mock
  private MiraklCreateOrderEvaluation evaluation;
  @Mock
  private ConsignmentEntryModel consignmentEntry;
  @Mock
  private ConsignmentEntryDao consignmentEntryDao;
  @Mock
  private ProductModel product;
  @Mock
  private OrderEntryModel orderEntry;
  @Captor
  private ArgumentCaptor<MiraklCancelOrderRequest> cancelRequestArgumentCaptor;

  private List<MiraklOrder> miraklOrders = asList(mock(MiraklOrder.class), mock(MiraklOrder.class));


  @Before
  public void setUp() throws Exception {
    when(miraklCreatedOrders.getOrders()).thenReturn(miraklOrders);
    when(miraklOrder.getId()).thenReturn(LOGISTIC_ORDER_CODE);
    when(miraklOrderPayment.getOrderId()).thenReturn(LOGISTIC_ORDER_CODE);
    when(marketplaceConsignmentDao.findMarketplaceConsignmentByCode(miraklOrder.getId())).thenReturn(consignment);
    when(consignment.getMiraklOrderStatus()).thenReturn(MiraklOrderStatus.SHIPPING);
    when(consignment.getOrder()).thenReturn(order);
    when(consignment.getCanEvaluate()).thenReturn(true);
    when(consignment.getCode()).thenReturn(LOGISTIC_ORDER_CODE);
    when(order.getUser()).thenReturn(user);
    when(consignment.getConsignmentProcesses()).thenReturn(singleton(consignmentProcess));
    when(consignmentProcess.getCode()).thenReturn(CONSIGNMENT_PROCESS_CODE);
    when(jsonMarshallingService.toJson(miraklOrder, MiraklOrder.class)).thenReturn(CONSIGNMENT_JSON);
    when(jsonMarshallingService.fromJson(CONSIGNMENT_JSON, MiraklOrder.class)).thenReturn(miraklOrder);
    when(jsonMarshallingService.toJson(miraklOrderPayment, MiraklOrderPayment.class)).thenReturn(DEBIT_REQUEST_JSON);
    when(jsonMarshallingService.fromJson(DEBIT_REQUEST_JSON, MiraklOrderPayment.class)).thenReturn(miraklOrderPayment);
    when(consignmentEntryDao.findConsignmentEntryByMiraklLineId(CONSIGNMENT_ENTRY_CODE)).thenReturn(consignmentEntry);
    when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
    when(consignmentEntry.getCanOpenIncident()).thenReturn(true);
    when(consignmentEntry.getConsignment()).thenReturn(consignment);
    when(orderEntry.getOrder()).thenReturn(order);
    when(orderEntry.getProduct()).thenReturn(product);
    when(order.getUser()).thenReturn(user);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void shouldCreateConsignments() {
    marketplaceConsignmentService.createMarketplaceConsignments(order, miraklCreatedOrders);

    verify(order).setConsignments(anySetOf(ConsignmentModel.class));
    verify(miraklCreateConsignmentConverter, times(miraklOrders.size())).convert(any(Pair.class));
  }

  @Test
  public void shouldLoadConsignmentUpdate() {
    when(consignment.getConsignmentUpdatePayload()).thenReturn(CONSIGNMENT_JSON);

    MiraklOrder update = marketplaceConsignmentService.loadConsignmentUpdate(consignment);

    assertThat(update).isNotNull();
  }

  @Test
  public void shouldLoadConsignmentUpdateReturnNullIfNotPresent() {
    when(consignment.getConsignmentUpdatePayload()).thenReturn(null);

    MiraklOrder update = marketplaceConsignmentService.loadConsignmentUpdate(consignment);

    assertThat(update).isNull();
  }

  @Test
  public void shouldReceiveConsignmentUpdate() {
    MarketplaceConsignmentModel result = marketplaceConsignmentService.receiveConsignmentUpdate(miraklOrder);

    verify(marketplaceConsignmentDao).findMarketplaceConsignmentByCode(LOGISTIC_ORDER_CODE);
    verify(consignment).setConsignmentUpdatePayload(CONSIGNMENT_JSON);
    verify(consignment).setLastUpdateProcessed(false);
    verify(modelService).save(consignment);
    verify(businessProcessService).triggerEvent(CONSIGNMENT_PROCESS_CODE + UPDATE_RECEIVED_EVENT_NAME);
    assertThat(result).isEqualTo(consignment);
  }

  @Test
  public void shouldStoreDebitRequest() {
    MarketplaceConsignmentModel result = marketplaceConsignmentService.storeDebitRequest(miraklOrderPayment);

    verify(marketplaceConsignmentDao).findMarketplaceConsignmentByCode(LOGISTIC_ORDER_CODE);
    verify(consignment).setDebitRequestPayload(DEBIT_REQUEST_JSON);
    verify(modelService).save(consignment);
    assertThat(result).isEqualTo(consignment);
  }

  @Test
  public void shouldLoadDebitRequest() {
    when(consignment.getDebitRequestPayload()).thenReturn(DEBIT_REQUEST_JSON);

    MiraklOrderPayment debitRequest = marketplaceConsignmentService.loadDebitRequest(consignment);

    assertThat(debitRequest).isNotNull();
  }

  @Test
  public void shouldLoadDebitRequestReturnNullIfNotPresent() {
    when(consignment.getDebitRequestPayload()).thenReturn(null);

    MiraklOrderPayment debitRequest = marketplaceConsignmentService.loadDebitRequest(consignment);

    assertThat(debitRequest).isNull();
  }

  @Test
  public void shouldConfirmConsignmentReceptionForCode() {
    marketplaceConsignmentService.confirmConsignmentReceptionForCode(LOGISTIC_ORDER_CODE, user);

    verify(miraklApi).receiveOrder(any(MiraklReceiveOrderRequest.class));
    verify(consignment).setMiraklOrderStatus(MiraklOrderStatus.RECEIVED);
    verify(consignment).setCanEvaluate(true);
    verify(modelService).save(any(MarketplaceConsignmentModel.class));
  }

  @Test(expected = UnknownIdentifierException.class)
  public void consignmentConfirmationShouldThrowUnknownIdentifierExceptionForWrongUser() {
    UserModel wrongUser = new UserModel();
    wrongUser.setName(WRONG_USER_NAME);

    marketplaceConsignmentService.confirmConsignmentReceptionForCode(LOGISTIC_ORDER_CODE, wrongUser);
  }

  @Test(expected = IllegalStateException.class)
  public void consignmentConfirmationShouldThrowIllegalStateExceptionForWrongState() {
    when(consignment.getMiraklOrderStatus()).thenReturn(MiraklOrderStatus.RECEIVED);

    marketplaceConsignmentService.confirmConsignmentReceptionForCode(LOGISTIC_ORDER_CODE, user);
  }

  @Test
  public void shouldPostEvaluation() {
    marketplaceConsignmentService.postEvaluation(LOGISTIC_ORDER_CODE, evaluation, user);

    verify(miraklApi).createOrderEvaluation(any(MiraklCreateOrderEvaluationRequest.class));
    verify(consignment).setCanEvaluate(false);
    verify(modelService).save(consignment);
  }

  @Test(expected = UnknownIdentifierException.class)
  public void evaluationShouldThrowUnknownIdentifierExceptionForWrongUser() {
    UserModel wrongUser = new UserModel();
    wrongUser.setName(WRONG_USER_NAME);

    marketplaceConsignmentService.postEvaluation(LOGISTIC_ORDER_CODE, evaluation, wrongUser);
  }

  @Test(expected = IllegalStateException.class)
  public void evaluationShouldThrowIllegalStateExceptionWhenEvaluationImpossible() {
    when(consignment.getCanEvaluate()).thenReturn(false);

    marketplaceConsignmentService.postEvaluation(LOGISTIC_ORDER_CODE, evaluation, user);
  }

  @Test
  public void shouldCancelMarketplaceConsignment() {
    marketplaceConsignmentService.cancelMarketplaceConsignment(consignment);

    verify(miraklApi).cancelOrder(cancelRequestArgumentCaptor.capture());
    assertThat(cancelRequestArgumentCaptor.getValue().getOrderId()).isEqualTo(LOGISTIC_ORDER_CODE);
  }

  @Test
  public void shouldCancelMarketplaceConsignmentForCode() {
    marketplaceConsignmentService.cancelMarketplaceConsignmentForCode(LOGISTIC_ORDER_CODE);

    verify(miraklApi).cancelOrder(cancelRequestArgumentCaptor.capture());
    assertThat(cancelRequestArgumentCaptor.getValue().getOrderId()).isEqualTo(LOGISTIC_ORDER_CODE);
  }

  @Test
  public void getConsignmentEntry() {
    ConsignmentEntryModel output = marketplaceConsignmentService.getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);

    assertThat(output).isEqualTo(consignmentEntry);
  }

  @Test(expected = UnknownIdentifierException.class)
  public void getConsignmentEntryWhenConsignmentEntryDoesNotExist() {
    when(consignmentEntryDao.findConsignmentEntryByMiraklLineId(CONSIGNMENT_ENTRY_CODE)).thenReturn(null);

    marketplaceConsignmentService.getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
  }

  @Test
  public void getProductFromConsignmentEntry() {
    ProductModel output = marketplaceConsignmentService.getProductForConsignmentEntry(CONSIGNMENT_ENTRY_CODE);

    assertThat(output).isEqualTo(product);
  }

}
