package com.mirakl.hybris.core.catalog.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.AttributeVarianceStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultClassificationAttributeOwnerStrategyTest {

  @InjectMocks
  private DefaultClassificationAttributeOwnerStrategy testObj;

  @Mock
  private AttributeVarianceStrategy attributeVarianceStrategy;
  @Mock
  private ClassAttributeAssignmentModel attributeAssignment;
  @Mock
  private ProductImportData data;
  @Mock
  private ProductImportFileContextData context;
  @Mock
  private ProductModel productToUpdate, rootBaseProductToUpdate;

  @Before
  public void setUp() throws Exception {
    when(data.getRootBaseProductToUpdate()).thenReturn(rootBaseProductToUpdate);
    when(data.getProductToUpdate()).thenReturn(productToUpdate);
  }

  @Test
  public void determineOwnerForVariantProducts() throws Exception {
    when(attributeVarianceStrategy.isVariant(attributeAssignment)).thenReturn(true);

    ProductModel output = testObj.determineOwner(attributeAssignment, data, context);

    assertThat(output).isEqualTo(productToUpdate);
  }

  @Test
  public void determineOwnerForNonVariantProducts() throws Exception {
    when(attributeVarianceStrategy.isVariant(attributeAssignment)).thenReturn(false);

    ProductModel output = testObj.determineOwner(attributeAssignment, data, context);

    assertThat(output).isEqualTo(rootBaseProductToUpdate);
  }

}
