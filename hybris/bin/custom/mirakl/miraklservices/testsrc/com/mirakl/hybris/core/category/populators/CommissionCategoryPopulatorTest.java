package com.mirakl.hybris.core.category.populators;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CategoryExportHeaders.CATEGORY_CODE;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CategoryExportHeaders.CATEGORY_LABEL;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.CategoryExportHeaders.PARENT_CODE;
import static java.util.Arrays.asList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CommissionCategoryPopulatorTest {

  private static final String CATEGORY_CODE_VALUE = "categoryCode";
  private static final String CATEGORY_NAME_VALUE = "categoryName";
  private static final String SUPER_CATEGORY_CODE_2 = "superCategoryCode2";

  @InjectMocks
  private CommissionCategoryPopulator testObj = new CommissionCategoryPopulator();

  @Mock
  private CategoryService categoryServiceMock;

  @Mock
  private CategoryModel categoryMock;
  @Mock
  private CategoryModel superCategoryMock1, superCategoryMock2;
  @Mock
  private CategoryModel rootCategoryMock, otherRootCategoryMock;
  @Mock
  private Pair<CategoryModel, Collection<CategoryModel>> categoryPairMock;

  @Before
  public void setUp() {
    when(categoryPairMock.getKey()).thenReturn(categoryMock);
    when(categoryPairMock.getValue()).thenReturn(asList(categoryMock, rootCategoryMock, superCategoryMock2));

    when(categoryMock.getCode()).thenReturn(CATEGORY_CODE_VALUE);
    when(categoryMock.getName()).thenReturn(CATEGORY_NAME_VALUE);

    when(categoryMock.getSupercategories()).thenReturn(asList(superCategoryMock1, superCategoryMock2));
    when(superCategoryMock2.getCode()).thenReturn(SUPER_CATEGORY_CODE_2);
  }

  @Test
  public void populateSubCategoryWithFirstSuperCategory() {
    Map<String, String> result = new HashMap<>();

    testObj.populate(categoryPairMock, result);

    assertThat(result).isNotEmpty();
    assertThat(result.keySet()).containsOnly(CATEGORY_CODE, CATEGORY_LABEL, PARENT_CODE);

    assertThat(result.get(CATEGORY_CODE)).isEqualTo(CATEGORY_CODE_VALUE);
    assertThat(result.get(CATEGORY_LABEL)).isEqualTo(CATEGORY_NAME_VALUE);
    assertThat(result.get(PARENT_CODE)).isEqualTo(SUPER_CATEGORY_CODE_2);
  }

  @Test
  public void populateRootCategoryWithNoSuperCategories() {
    when(categoryMock.getSupercategories()).thenReturn(Collections.<CategoryModel>emptyList());

    Map<String, String> result = new HashMap<>();

    testObj.populate(categoryPairMock, result);

    assertThat(result).isNotEmpty();
    assertThat(result.keySet()).containsOnly(CATEGORY_CODE, CATEGORY_LABEL, PARENT_CODE);

    assertThat(result.get(CATEGORY_CODE)).isEqualTo(CATEGORY_CODE_VALUE);
    assertThat(result.get(CATEGORY_LABEL)).isEqualTo(CATEGORY_NAME_VALUE);
    assertThat(result.get(PARENT_CODE)).isEqualTo(EMPTY);
  }

  @Test
  public void populateRootCategoryWithNoParentCategoryIfSuperCategoryIsNotInExportedCategories() {
    when(categoryMock.getSupercategories()).thenReturn(Collections.singletonList(superCategoryMock1));

    Map<String, String> result = new HashMap<>();

    testObj.populate(categoryPairMock, result);

    assertThat(result).isNotEmpty();
    assertThat(result.keySet()).containsOnly(CATEGORY_CODE, CATEGORY_LABEL, PARENT_CODE);

    assertThat(result.get(CATEGORY_CODE)).isEqualTo(CATEGORY_CODE_VALUE);
    assertThat(result.get(CATEGORY_LABEL)).isEqualTo(CATEGORY_NAME_VALUE);
    assertThat(result.get(PARENT_CODE)).isEqualTo(EMPTY);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfSourceIsNull() {
    testObj.populate(null, Collections.<String, String>emptyMap());
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfTargetIsNull() {
    testObj.populate(categoryPairMock, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfCategoryPairKeyIsNull() {
    testObj.populate(Pair.<CategoryModel, Collection<CategoryModel>>of(null, Collections.<CategoryModel>emptyList()),
        Collections.<String, String>emptyMap());
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfCategoryPairValueIsNull() {
    testObj.populate(Pair.<CategoryModel, Collection<CategoryModel>>of(rootCategoryMock, null),
        Collections.<String, String>emptyMap());
  }
}
