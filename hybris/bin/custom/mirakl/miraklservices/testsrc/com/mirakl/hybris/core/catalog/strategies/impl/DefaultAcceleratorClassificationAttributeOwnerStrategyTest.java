package com.mirakl.hybris.core.catalog.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultAcceleratorClassificationAttributeOwnerStrategyTest {

  @Mock
  private ProductImportData data;
  @Mock
  private ProductModel productOwner;

  @InjectMocks
  DefaultAcceleratorClassificationAttributeOwnerStrategy testObj;

  @Before
  public void setUp() throws Exception {
    when(data.getProductToUpdate()).thenReturn(productOwner);
  }

  @Test
  public void determineOwner() throws Exception {
    ProductModel result = testObj.determineOwner(null, data, null);

    assertThat(result).isEqualTo(productOwner);
  }

}
