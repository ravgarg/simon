package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.google.common.collect.Sets.newHashSet;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.BOOLEAN_VALUE_LIST_ID;
import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.MiraklExportCatalogConfig;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogContext;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandler;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandlerResolver;
import com.mirakl.hybris.core.catalog.writer.ExportCatalogWriter;
import com.mirakl.hybris.core.enums.MiraklAttributeExportHeader;
import com.mirakl.hybris.core.enums.MiraklAttributeType;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultExportCoreAttributesStrategyTest {

  private static final String TYPE_PARAMETER_1 = "type-parameter-1";
  private static final String TYPE_PARAMETER_2 = "type-parameter-2";

  @InjectMocks
  private DefaultExportCoreAttributesStrategy strategy;

  @Mock
  private MiraklExportCatalogContext context;
  @Mock
  private MiraklExportCatalogConfig config;
  @Mock
  private ExportCatalogWriter writer;
  @Mock
  private MiraklCoreAttributeModel coreAttribute1, coreAttribute2;
  @Mock
  private CoreAttributeHandlerResolver coreAttributeHandlerResolver;
  @Mock
  private CoreAttributeHandler<MiraklCoreAttributeModel> listCoreAttributeHandler;
  @Captor
  private ArgumentCaptor<Map<String, String>> lineArgumentCaptor;

  @Before
  public void setUp() {
    when(context.getWriter()).thenReturn(writer);
    when(context.getExportConfig()).thenReturn(config);
    when(config.isExportAttributes()).thenReturn(true);
    when(config.isExportValueLists()).thenReturn(true);
    when(config.getCoreAttributes()).thenReturn(newHashSet(coreAttribute1, coreAttribute2));
    when(coreAttribute1.getType()).thenReturn(MiraklAttributeType.LIST);
    when(coreAttribute2.getType()).thenReturn(MiraklAttributeType.TEXT);
    when(coreAttribute1.getTypeParameter()).thenReturn(TYPE_PARAMETER_1);
    when(coreAttribute2.getTypeParameter()).thenReturn(TYPE_PARAMETER_2);
    when(coreAttributeHandlerResolver.determineHandler(coreAttribute1)).thenReturn(listCoreAttributeHandler);
  }

  @Test
  public void shouldExportCoreAttributes() throws IOException {
    List<Locale> additionalLocales = asList(Locale.ENGLISH, Locale.GERMAN);
    when(config.getAdditionalLocales()).thenReturn(additionalLocales);

    strategy.exportCoreAttributes(context);

    verify(writer, times(config.getCoreAttributes().size())).writeAttribute(lineArgumentCaptor.capture());
    Map<String, String> line = lineArgumentCaptor.getValue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.CODE.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.DESCRIPTION.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.DESCRIPTION.getCode() + "[en]")).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.DESCRIPTION.getCode() + "[de]")).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.DESCRIPTION.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.LABEL.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.REQUIRED.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.TYPE.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.VARIANT.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.TYPE.getCode())).isTrue();
    assertThat(line.containsKey(MiraklAttributeExportHeader.TYPE_PARAMETER.getCode())).isTrue();
  }

  @Test
  public void shouldTransformBooleansIntoList() throws IOException {
    List<Locale> additionalLocales = asList(Locale.ENGLISH, Locale.GERMAN);
    when(config.getAdditionalLocales()).thenReturn(additionalLocales);
    when(coreAttribute1.getType()).thenReturn(MiraklAttributeType.BOOLEAN);
    when(config.getCoreAttributes()).thenReturn(Collections.singleton(coreAttribute1));

    strategy.exportCoreAttributes(context);

    verify(writer).writeAttribute(lineArgumentCaptor.capture());
    Map<String, String> line = lineArgumentCaptor.getValue();
    assertThat(line.get(MiraklAttributeExportHeader.TYPE.getCode())).isEqualTo(MiraklAttributeType.LIST.getCode());
    assertThat(line.get(MiraklAttributeExportHeader.TYPE_PARAMETER.getCode())).isEqualTo(BOOLEAN_VALUE_LIST_ID);
  }

  @Test
  public void shouldNotExportAttributesWhenNotNecessary() throws Exception {
    when(config.isExportAttributes()).thenReturn(false);

    strategy.exportCoreAttributes(context);

    verify(writer, never()).writeAttribute(anyMapOf(String.class, String.class));
  }

  @Test
  public void shouldNotExportValuesWhenNotNecessary() throws Exception {
    when(config.isExportValueLists()).thenReturn(false);

    strategy.exportCoreAttributes(context);

    verify(writer, never()).writeAttributeValue(anyMapOf(String.class, String.class));
  }


}
