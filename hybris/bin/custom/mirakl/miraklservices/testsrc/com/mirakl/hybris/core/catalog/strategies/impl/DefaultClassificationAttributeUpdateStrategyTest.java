package com.mirakl.hybris.core.catalog.strategies.impl;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

import java.util.*;

import org.apache.commons.configuration.Configuration;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.ClassificationAttributeOwnerStrategy;
import com.mirakl.hybris.core.constants.MiraklservicesConstants;
import com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ClassificationAttributeTypeEnum;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationClassesResolverStrategy;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultClassificationAttributeUpdateStrategyTest {

  private static final PK CATEGORY_ROLE_ATTRIBUTE_PK = PK.fromLong(3L);
  private static final PK ROOT_BASE_PRODUCT_CATEGORY_PK = PK.fromLong(1L);
  private static final PK PRODUCT_CATEGORY_PK_2 = PK.fromLong(2L);
  private static final Set<PK> allProductCategoriesPK = newHashSet(ROOT_BASE_PRODUCT_CATEGORY_PK, PRODUCT_CATEGORY_PK_2);
  private static final String LOCALIZED_ATTRIBUTE_CODE = "dimensions";
  private static final String ATTRIBUTE_VALUE_1 = "20";
  private static final String ATTRIBUTE_VALUE_2 = "30";
  private static final String ATTRIBUTE_VALUE_3 = "2";
  private static final String ATTRIBUTE_MULTI_VALUED = ATTRIBUTE_VALUE_1 + ", " + ATTRIBUTE_VALUE_2 + " ," + ATTRIBUTE_VALUE_3;
  private static final String ATTRIBUTE_WITHOUT_FEATURE_CODE = "focale";
  private static final String MISSING_ATTRIBUTE_CODE = "exposure";
  private static final String CATALOG_EXPORT_DATE_FORMAT = "dd-MM-yyyy";
  private static final String PRODUCTS_IMPORT_VALUES_SEPARATOR = ",";

  private static final String BOOLEAN_ATTRIBUTE_VALUE = "true";
  private static final String NUMBER_ATTRIBUTE_VALUE = "1456.25";
  private static final String DATE_ATTRIBUTE_VALUE = "09-05-1923";

  private static final Boolean BOOLEAN_ATTRIBUTE_ATOMIC_VALUE = true;
  private static final Double NUMBER_ATTRIBUTE_ATOMIC_VALUE = 1456.25;
  private static final Date DATE_ATTRIBUTE_ATOMIC_VALUE = getExpectedAttributeDate();
  private static final String CATEGORY_CORE_ATTRIBUTE_UID = "1";

  @InjectMocks
  private DefaultClassificationAttributeUpdateStrategy testObj;

  @Captor
  private ArgumentCaptor<List<FeatureValue>> featureListCaptor;

  private Collection<AttributeValueData> attributeValues;
  private Collection<CategoryModel> rootBaseProductCategories;
  private Set<ClassificationClassModel> rootBaseProductClassificationClasses;
  private List<ClassAttributeAssignmentModel> rootBaseProductClassAttributeAssignments;
  private Map<String, Set<PK>> allCategoryValues = singletonMap(CATEGORY_CORE_ATTRIBUTE_UID, allProductCategoriesPK);

  @Mock
  private AttributeValueData testingAttribute, attributeWithoutFeature, missingAttribute;
  @Mock
  private ProductImportData data;
  @Mock
  private ProductImportFileContextData context;
  @Mock
  private ClassificationService classificationService;
  @Mock
  private ConfigurationService configurationService;
  @Mock
  private ClassificationClassesResolverStrategy classificationClassesResolverStrategy;
  @Mock
  private ClassificationAttributeOwnerStrategy classificationAttributeOwnerStrategy;
  @Mock
  private ProductImportGlobalContextData globalContext;
  @Mock
  private ProductModel rootBaseProductToUpdate, productToUpdate;
  @Mock
  private CategoryModel rootBaseProductCategory;
  @Mock
  private ClassificationClassModel rootBaseProductClassificationClass1, rootBaseProductClassificationClass2;
  @Mock
  private ClassAttributeAssignmentModel testingAttributeAssignment, attributeAssignmentWithoutFeature;
  @Mock
  private ClassificationAttributeModel testingAttributeClassificationAttribute, AttributeWithoutFeatureClassificationAttribute;
  @Mock
  private FeatureList featureList;
  @Mock
  private Set<ItemModel> modelsToSave;
  @Mock
  private LocalizedFeature localizedFeature;
  @Mock
  private Feature unlocalizedFeature;
  @Mock
  private Configuration configuration;
  @Mock
  private ClassificationAttributeUnitModel centimeters;
  @Mock
  private ModelService modelService;
  @Mock
  private MiraklCategoryCoreAttributeModel categoryRoleCoreAttribute;

  @Before
  public void setUp() throws Exception {
    attributeValues = asList(testingAttribute, attributeWithoutFeature, missingAttribute);
    rootBaseProductCategories = singletonList(rootBaseProductCategory);
    rootBaseProductClassificationClasses = newHashSet(rootBaseProductClassificationClass1, rootBaseProductClassificationClass2);
    rootBaseProductClassAttributeAssignments = asList(testingAttributeAssignment, attributeAssignmentWithoutFeature);

    when(data.getRootBaseProductToUpdate()).thenReturn(rootBaseProductToUpdate);
    when(context.getGlobalContext()).thenReturn(globalContext);
    when(globalContext.getCategoryRoleAttribute()).thenReturn(CATEGORY_ROLE_ATTRIBUTE_PK);
    when(modelService.get(CATEGORY_ROLE_ATTRIBUTE_PK)).thenReturn(categoryRoleCoreAttribute);
    when(categoryRoleCoreAttribute.getUid()).thenReturn(CATEGORY_CORE_ATTRIBUTE_UID);
    when(globalContext.getAllCategoryValues()).thenReturn(allCategoryValues);
    when(rootBaseProductToUpdate.getSupercategories()).thenReturn(rootBaseProductCategories);
    when(rootBaseProductCategory.getPk()).thenReturn(ROOT_BASE_PRODUCT_CATEGORY_PK);
    when(classificationClassesResolverStrategy.resolve(rootBaseProductCategory)).thenReturn(rootBaseProductClassificationClasses);
    when(classificationClassesResolverStrategy.getAllClassAttributeAssignments(rootBaseProductClassificationClasses))
        .thenReturn(rootBaseProductClassAttributeAssignments);
    when(classificationAttributeOwnerStrategy.determineOwner(any(ClassAttributeAssignmentModel.class), eq(data), eq(context)))
        .thenReturn(productToUpdate);
    when(classificationService.getFeatures(productToUpdate)).thenReturn(featureList);
    when(data.getModelsToSave()).thenReturn(modelsToSave);
    when(configurationService.getConfiguration()).thenReturn(configuration);
    when(configuration.getString(MiraklservicesConstants.PRODUCTS_IMPORT_VALUES_SEPARATOR))
        .thenReturn(PRODUCTS_IMPORT_VALUES_SEPARATOR);
    when(configuration.getString(eq(MiraklservicesConstants.CATALOG_EXPORT_DATE_FORMAT), anyString()))
        .thenReturn(CATALOG_EXPORT_DATE_FORMAT);

    when(testingAttribute.getCode()).thenReturn(LOCALIZED_ATTRIBUTE_CODE);
    when(testingAttribute.getLocale()).thenReturn(Locale.FRENCH);
    when(testingAttribute.getValue()).thenReturn(ATTRIBUTE_MULTI_VALUED);
    when(testingAttributeAssignment.getClassificationAttribute()).thenReturn(testingAttributeClassificationAttribute);
    when(testingAttributeAssignment.getMultiValued()).thenReturn(true);
    when(testingAttributeAssignment.getUnit()).thenReturn(centimeters);
    when(testingAttributeAssignment.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.STRING);
    when(testingAttributeClassificationAttribute.getCode()).thenReturn(LOCALIZED_ATTRIBUTE_CODE);
    when(featureList.getFeatureByAssignment(testingAttributeAssignment)).thenReturn(localizedFeature);

    when(attributeWithoutFeature.getCode()).thenReturn(ATTRIBUTE_WITHOUT_FEATURE_CODE);
    when(attributeAssignmentWithoutFeature.getClassificationAttribute())
        .thenReturn(AttributeWithoutFeatureClassificationAttribute);
    when(AttributeWithoutFeatureClassificationAttribute.getCode()).thenReturn(ATTRIBUTE_WITHOUT_FEATURE_CODE);

    when(missingAttribute.getCode()).thenReturn(MISSING_ATTRIBUTE_CODE);
  }

  @Test
  public void updateAttributes() throws Exception {
    testObj.updateAttributes(attributeValues, data, context);

    verify(classificationService).replaceFeatures(productToUpdate, featureList);
    verify(modelsToSave, atLeastOnce()).add(productToUpdate);
    verify(localizedFeature).removeAllValues(Locale.FRENCH);
    verify(localizedFeature).setValues(featureListCaptor.capture(), eq(Locale.FRENCH));
    List<FeatureValue> featureValues = featureListCaptor.getValue();
    assertThat(featureValues.get(0).getValue().toString()).isEqualTo(ATTRIBUTE_VALUE_1);
    assertThat(featureValues.get(1).getValue().toString()).isEqualTo(ATTRIBUTE_VALUE_2);
    assertThat(featureValues.get(2).getValue().toString()).isEqualTo(ATTRIBUTE_VALUE_3);
  }

  @Test
  public void updateMissingAttribute() throws Exception {
    attributeValues = singletonList(missingAttribute);

    testObj.updateAttributes(attributeValues, data, context);

    verify(classificationService, never()).setFeature(any(ProductModel.class), any(Feature.class));
    verifyZeroInteractions(modelsToSave);
  }

  @Test
  public void updateAttributeWithoutFeature() throws Exception {
    attributeValues = singletonList(attributeWithoutFeature);

    testObj.updateAttributes(attributeValues, data, context);

    verify(classificationService, never()).setFeature(any(ProductModel.class), any(Feature.class));
  }

  @Test
  public void updateAttributeWithoutValue() throws Exception {
    attributeValues = singletonList(testingAttribute);
    when(testingAttribute.getValue()).thenReturn("");
    when(featureList.getFeatureByAssignment(testingAttributeAssignment)).thenReturn(unlocalizedFeature);
    when(testingAttribute.getLocale()).thenReturn(null);
    when(testingAttributeAssignment.getMultiValued()).thenReturn(false);

    testObj.updateAttributes(attributeValues, data, context);

    verify(modelsToSave, atLeastOnce()).add(productToUpdate);
    verify(unlocalizedFeature).setValues(featureListCaptor.capture());
    assertThat(featureListCaptor.getValue()).hasSize(1);
    assertThat(featureListCaptor.getValue().iterator().next().getValue()).isEqualTo("");
  }

  @Test
  public void updateBooleanAttribute() throws Exception {
    attributeValues = singletonList(testingAttribute);
    when(testingAttributeAssignment.getMultiValued()).thenReturn(false);
    when(testingAttribute.getValue()).thenReturn(BOOLEAN_ATTRIBUTE_VALUE);
    when(testingAttribute.getLocale()).thenReturn(null);
    when(featureList.getFeatureByAssignment(testingAttributeAssignment)).thenReturn(unlocalizedFeature);
    when(testingAttributeAssignment.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.BOOLEAN);

    testObj.updateAttributes(attributeValues, data, context);

    verify(unlocalizedFeature).setValues(featureListCaptor.capture());
    assertThat(featureListCaptor.getValue()).hasSize(1);
    assertThat(featureListCaptor.getValue().iterator().next().getValue()).isEqualTo(BOOLEAN_ATTRIBUTE_ATOMIC_VALUE);
  }

  @Test
  public void updateNumberAttribute() throws Exception {
    attributeValues = singletonList(testingAttribute);
    when(testingAttributeAssignment.getMultiValued()).thenReturn(false);
    when(testingAttribute.getValue()).thenReturn(NUMBER_ATTRIBUTE_VALUE);
    when(testingAttribute.getLocale()).thenReturn(null);
    when(featureList.getFeatureByAssignment(testingAttributeAssignment)).thenReturn(unlocalizedFeature);
    when(testingAttributeAssignment.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.NUMBER);

    testObj.updateAttributes(attributeValues, data, context);

    verify(unlocalizedFeature).setValues(featureListCaptor.capture());
    assertThat(featureListCaptor.getValue()).hasSize(1);
    assertThat(featureListCaptor.getValue().iterator().next().getValue()).isEqualTo(NUMBER_ATTRIBUTE_ATOMIC_VALUE);
  }

  @Test
  public void updateDateAttribute() throws Exception {
    attributeValues = singletonList(testingAttribute);
    when(testingAttributeAssignment.getMultiValued()).thenReturn(false);
    when(testingAttribute.getValue()).thenReturn(DATE_ATTRIBUTE_VALUE);
    when(testingAttribute.getLocale()).thenReturn(null);
    when(featureList.getFeatureByAssignment(testingAttributeAssignment)).thenReturn(unlocalizedFeature);
    when(testingAttributeAssignment.getAttributeType()).thenReturn(ClassificationAttributeTypeEnum.DATE);

    testObj.updateAttributes(attributeValues, data, context);

    verify(unlocalizedFeature).setValues(featureListCaptor.capture());
    assertThat(featureListCaptor.getValue()).hasSize(1);
    assertThat(featureListCaptor.getValue().iterator().next().getValue()).isEqualTo(DATE_ATTRIBUTE_ATOMIC_VALUE);
  }

  private static Date getExpectedAttributeDate() {
    return new DateTime().withTimeAtStartOfDay().withYear(1923).withMonthOfYear(5).withDayOfMonth(9).toDate();
  }
}
