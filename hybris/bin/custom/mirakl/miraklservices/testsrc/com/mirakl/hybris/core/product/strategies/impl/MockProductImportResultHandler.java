package com.mirakl.hybris.core.product.strategies.impl;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.product.strategies.ProductImportResultHandler;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class MockProductImportResultHandler implements ProductImportResultHandler {
  @Override
  public void handleImportResults(ProductImportFileContextData context) {}
}
