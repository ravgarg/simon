package com.mirakl.hybris.core.category.services.impl;

import static com.mirakl.hybris.core.constants.MiraklservicesConstants.getCategoryExportHeaders;
import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.util.services.CsvService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCommissionCategoryServiceTest {

  private static final String CSV_CONTENT = "csvContent";

  @InjectMocks
  @Spy
  private DefaultCommissionCategoryService testObj = new DefaultCommissionCategoryService();

  @Mock
  private CategoryService categoryServiceMock;
  @Mock
  private CsvService csvServiceMock;
  @Mock
  private SessionService sessionServiceMock;

  @Mock
  private CategoryModel rootCategoryMock;
  @Mock
  private CategoryModel categoryMock1, categoryMock2;
  @Mock
  private Map<String, String> rootCategoryMapMock, subCategoryMapMock;
  @Mock
  private CatalogVersionModel rootCatalogVersionMock, otherCatalogVersionMock;

  @Captor
  private ArgumentCaptor<Collection<Pair<CategoryModel, Collection<CategoryModel>>>> categoryPairsArgumentCaptor;

  @Before
  public void setUp() throws IOException {
    when(rootCategoryMock.getCatalogVersion()).thenReturn(rootCatalogVersionMock);
    when(categoryMock1.getCatalogVersion()).thenReturn(rootCatalogVersionMock);
    when(categoryMock2.getCatalogVersion()).thenReturn(otherCatalogVersionMock);

    when(categoryServiceMock.getAllSubcategoriesForCategory(rootCategoryMock)).thenReturn(asList(categoryMock1, categoryMock2));
    when(sessionServiceMock.executeInLocalView(any(SessionExecutionBody.class)))
        .thenReturn(asList(rootCategoryMapMock, subCategoryMapMock));
    when(csvServiceMock.createCsvWithHeaders(eq(getCategoryExportHeaders()), eq(asList(rootCategoryMapMock, subCategoryMapMock))))
        .thenReturn(CSV_CONTENT);
  }

  @Test
  public void getsCommissionCategories() {
    Collection<CategoryModel> result = testObj.getCategories(rootCategoryMock);

    assertThat(result).containsOnly(rootCategoryMock, categoryMock1);
  }

  @Test
  public void getsCategoryExportCsvContent() throws IOException {
    String result = testObj.getCategoryExportCsvContent(Locale.ENGLISH, asList(rootCategoryMock, categoryMock1));

    assertThat(result).isEqualTo(CSV_CONTENT);

    verify(csvServiceMock).createCsvWithHeaders(eq(getCategoryExportHeaders()),
        eq(asList(rootCategoryMapMock, subCategoryMapMock)));

    verify(testObj).mapExportCategories(eq(Locale.ENGLISH), categoryPairsArgumentCaptor.capture());
    Collection<Pair<CategoryModel, Collection<CategoryModel>>> categoryPairs = categoryPairsArgumentCaptor.getValue();
    assertThat(categoryPairs).isNotEmpty();
    assertThat(categoryPairs).hasSize(2);

    Iterator<Pair<CategoryModel, Collection<CategoryModel>>> categoryPairsIterator = categoryPairs.iterator();
    Pair<CategoryModel, Collection<CategoryModel>> rootCategoryPair = categoryPairsIterator.next();
    assertThat(rootCategoryPair.getKey()).isEqualTo(rootCategoryMock);
    assertThat(rootCategoryPair.getValue()).isEqualTo(asList(rootCategoryMock, categoryMock1));

    Pair<CategoryModel, Collection<CategoryModel>> subCategoryPair = categoryPairsIterator.next();
    assertThat(subCategoryPair.getKey()).isEqualTo(categoryMock1);
    assertThat(subCategoryPair.getValue()).isEqualTo(asList(rootCategoryMock, categoryMock1));
  }

  @Test(expected = IOException.class)
  public void getCategoryExportCsvContentThrowsIOExceptionIfCSVContentCannotBeCreated() throws IOException {
    when(csvServiceMock.createCsvWithHeaders(eq(getCategoryExportHeaders()), eq(asList(rootCategoryMapMock, subCategoryMapMock))))
        .thenThrow(new IOException());

    testObj.getCategoryExportCsvContent(Locale.ENGLISH, asList(rootCategoryMock, categoryMock1));
  }
}
