package com.mirakl.hybris.core.comparators;

import com.mirakl.hybris.core.model.OfferModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;

import java.math.BigDecimal;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
public class OfferPriceComparatorTest {

  private OfferPriceComparator priceComparator = new OfferPriceComparator();

  private OfferModel offer1 = new OfferModel();
  private OfferModel offer2 = new OfferModel();


  @Test
  public void compareOffer1CheaperThanOffer2() {
    offer1.setTotalPrice(BigDecimal.valueOf(150));
    offer2.setTotalPrice(BigDecimal.valueOf(175));

    assertThat(priceComparator.compare(offer1, offer2)).isEqualTo(-1);
  }

  @Test
  public void compareOffer2CheaperThanOffer1() {
    offer1.setTotalPrice(BigDecimal.valueOf(175));
    offer2.setTotalPrice(BigDecimal.valueOf(150));

    assertThat(priceComparator.compare(offer1, offer2)).isEqualTo(1);
  }

  @Test
  public void compareOffer1HasSamePriceThanOffer2() {
    offer1.setTotalPrice(BigDecimal.valueOf(150));
    offer2.setTotalPrice(BigDecimal.valueOf(150));

    assertThat(priceComparator.compare(offer1, offer2)).isEqualTo(0);
  }
}
