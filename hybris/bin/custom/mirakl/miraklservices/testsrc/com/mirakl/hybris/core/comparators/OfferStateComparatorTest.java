package com.mirakl.hybris.core.comparators;

import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
public class OfferStateComparatorTest {

  private static final String OFFER_STATE_1 = "state1";
  private static final String OFFER_STATE_2 = "state2";
  private static final String OFFER_STATE_PRIORITY_CODE = "offerStatePriorityCode";

  private OfferStateComparator testObj = new OfferStateComparator(OfferState.valueOf(OFFER_STATE_PRIORITY_CODE));

  private OfferModel offer1 = new OfferModel();
  private OfferModel offer2 = new OfferModel();

  @Test
  public void compareWhenOffer1IsNew() throws Exception {
    offer1.setState(OfferState.valueOf(OFFER_STATE_PRIORITY_CODE));
    offer2.setState(OfferState.valueOf(OFFER_STATE_1));

    int result = testObj.compare(offer1, offer2);

    assertThat(result).isEqualTo(-1);
  }

  @Test
  public void compareWhenOffer2IsNew() throws Exception {
    offer1.setState(OfferState.valueOf(OFFER_STATE_1));
    offer2.setState(OfferState.valueOf(OFFER_STATE_PRIORITY_CODE));

    int result = testObj.compare(offer1, offer2);

    assertThat(result).isEqualTo(1);
  }

  @Test
  public void compareWhenBothOffersAreNew() throws Exception {
    offer1.setState(OfferState.valueOf(OFFER_STATE_PRIORITY_CODE));
    offer2.setState(OfferState.valueOf(OFFER_STATE_PRIORITY_CODE));

    int result = testObj.compare(offer1, offer2);

    assertThat(result).isEqualTo(0);
  }

  @Test
  public void compareWhenBothOffersAreRandom() throws Exception {
    offer1.setState(OfferState.valueOf(OFFER_STATE_1));
    offer2.setState(OfferState.valueOf(OFFER_STATE_2));

    int result = testObj.compare(offer1, offer2);

    assertThat(result).isEqualTo(0);
  }
}
