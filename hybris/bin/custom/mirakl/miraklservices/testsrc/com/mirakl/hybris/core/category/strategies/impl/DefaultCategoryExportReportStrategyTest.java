package com.mirakl.hybris.core.category.strategies.impl;

import static com.mirakl.hybris.core.enums.MiraklExportType.COMMISSION_CATEGORY_EXPORT;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.category.synchro.MiraklCategorySynchroResult;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.catalog.category.MiraklCategorySynchroErrorReportRequest;
import com.mirakl.client.mmp.request.catalog.category.MiraklCategorySynchroStatusRequest;
import com.mirakl.hybris.core.jobs.dao.MiraklJobReportDao;
import com.mirakl.hybris.core.model.MiraklJobReportModel;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCategoryExportReportStrategyTest {

  private static final String SYNC_JOB_ID = "syncJobId";
  private static final int CATEGORIES_DELETED = 1;
  private static final int CATEGORIES_INSERTED = 2;
  private static final int CATEGORIES_UPDATED = 3;

  @InjectMocks
  private DefaultCategoryExportReportStrategy testObj = new DefaultCategoryExportReportStrategy();

  @Mock
  private MiraklJobReportDao miraklJobReportDaoMock;
  @Mock
  private MiraklMarketplacePlatformFrontApi miraklOperatorApiMock;

  @Mock
  private MiraklJobReportModel miraklJobReportMock;
  @Mock
  private MiraklCategorySynchroResult miraklCategorySynchroResultMock;

  @Captor
  private ArgumentCaptor<MiraklCategorySynchroStatusRequest> categorySynchroStatusRequestCaptor;
  @Captor
  private ArgumentCaptor<MiraklCategorySynchroErrorReportRequest> categorySynchroErrorReportRequestCaptor;

  @Rule
  public TemporaryFolder errorReportRule = new TemporaryFolder();

  @Before
  public void setUp() {
    when(miraklJobReportDaoMock.findPendingJobReportsForType(COMMISSION_CATEGORY_EXPORT)).thenReturn(singletonList(miraklJobReportMock));

    when(miraklCategorySynchroResultMock.getCategoryDeleted()).thenReturn(CATEGORIES_DELETED);
    when(miraklCategorySynchroResultMock.getCategoryInserted()).thenReturn(CATEGORIES_INSERTED);
    when(miraklCategorySynchroResultMock.getCategoryUpdated()).thenReturn(CATEGORIES_UPDATED);


    when(miraklOperatorApiMock.getCategorySynchroResult(categorySynchroStatusRequestCaptor.capture()))
        .thenReturn(miraklCategorySynchroResultMock);
  }

  @Test
  public void getsPendingMiraklJobReports() {
    List<MiraklJobReportModel> result = testObj.getPendingMiraklJobReports();

    assertThat(result).containsOnly(miraklJobReportMock);

    verify(miraklJobReportDaoMock).findPendingJobReportsForType(COMMISSION_CATEGORY_EXPORT);
  }

  @Test
  public void getsCategoryExportResult() {
    MiraklCategorySynchroResult result = testObj.getExportResult(SYNC_JOB_ID);

    assertThat(result).isSameAs(miraklCategorySynchroResultMock);

    MiraklCategorySynchroStatusRequest categorySynchroStatusRequest = categorySynchroStatusRequestCaptor.getValue();
    assertThat(categorySynchroStatusRequest.getSynchroId()).isEqualTo(SYNC_JOB_ID);
  }

  @Test
  public void getsCategoryExportErrorReport() throws IOException {
    File errorReport = errorReportRule.newFile();
    when(miraklOperatorApiMock.getCategorySynchroErrorReport(categorySynchroErrorReportRequestCaptor.capture()))
        .thenReturn(errorReport);

    File result = testObj.getErrorReportFile(SYNC_JOB_ID);

    assertThat(result).isSameAs(errorReport);

    MiraklCategorySynchroErrorReportRequest categorySynchroErrorReportRequest =
        categorySynchroErrorReportRequestCaptor.getValue();
    assertThat(categorySynchroErrorReportRequest.getSynchroId()).isEqualTo(SYNC_JOB_ID);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getCategoryExportResultThrowsIllegalArgumentExceptionISyncJobIdIsNull() throws IOException {
    testObj.getExportResult(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getCategoryExportResultThrowsIllegalArgumentExceptionISyncJobIdIsEmpty() throws IOException {
    testObj.getExportResult(EMPTY);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getErrorReportFileThrowsIllegalArgumentExceptionISyncJobIdIsNull() throws IOException {
    testObj.getErrorReportFile(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getErrorReportFileThrowsIllegalArgumentExceptionISyncJobIdIsEmpty() throws IOException {
    testObj.getErrorReportFile(EMPTY);
  }

  @Test(expected = MiraklApiException.class)
  public void getCategoryExportResultThrowsMiraklApiException() {
    when(miraklOperatorApiMock.getCategorySynchroResult(categorySynchroStatusRequestCaptor.capture()))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    testObj.getExportResult(SYNC_JOB_ID);
  }

  @Test(expected = MiraklApiException.class)
  public void getErrorReportFileThrowsMiraklApiException() throws IOException {
    when(miraklOperatorApiMock.getCategorySynchroErrorReport(categorySynchroErrorReportRequestCaptor.capture()))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    testObj.getErrorReportFile(SYNC_JOB_ID);
  }
}
