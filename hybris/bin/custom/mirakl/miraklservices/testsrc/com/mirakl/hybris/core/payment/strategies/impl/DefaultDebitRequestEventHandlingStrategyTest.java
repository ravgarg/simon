package com.mirakl.hybris.core.payment.strategies.impl;

import static java.util.Collections.singletonList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.order.MiraklOrders;
import com.mirakl.client.mmp.domain.payment.debit.MiraklOrderPayment;
import com.mirakl.hybris.core.enums.MarketplaceConsignmentPaymentStatus;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.mirakl.hybris.core.payment.events.DebitRequestReceivedEvent;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.cluster.ClusterService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.tenant.TenantService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultDebitRequestEventHandlingStrategyTest {
  private static final String CONSIGNMENT_ID = "order-id";

  @InjectMocks
  private DefaultDebitRequestEventHandlingStrategy eventHandler;

  @Mock
  private MarketplaceConsignmentService marketplaceConsignmentService;
  @Mock
  private ModelService modelService;
  @Mock
  private ClusterService clusterService;
  @Mock
  private TenantService tenantService;
  @Mock
  private DebitRequestReceivedEvent event;
  @Mock
  private MiraklOrderPayment debitRequest;
  @Mock
  private MiraklOrders miraklOrders;
  @Mock
  private MiraklOrder miraklOrder;
  @Mock
  private MarketplaceConsignmentModel consignment;

  @Before
  public void setUp() throws Exception {
    when(event.getDebitRequest()).thenReturn(debitRequest);
    when(debitRequest.getOrderId()).thenReturn(CONSIGNMENT_ID);
    when(marketplaceConsignmentService.getMarketplaceConsignmentForCode(CONSIGNMENT_ID)).thenReturn(consignment);
    when(consignment.getCode()).thenReturn(CONSIGNMENT_ID);
    when(miraklOrders.getOrders()).thenReturn(singletonList(miraklOrder));
  }

  @Test
  public void shouldHandleCaptureEvent() {
    eventHandler.handleEvent(event);

    verify(marketplaceConsignmentService).getMarketplaceConsignmentForCode(CONSIGNMENT_ID);
    verify(marketplaceConsignmentService).storeDebitRequest(debitRequest);
    verify(consignment).setPaymentStatus(MarketplaceConsignmentPaymentStatus.INITIAL);
    verify(modelService).save(consignment);
  }

}
