package com.mirakl.hybris.core.order.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableList;
import com.mirakl.hybris.core.order.strategies.impl.MiraklFindDeliveryCostStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.delivery.JaloDeliveryModeException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklFindDeliveryCostStrategyTest {

  private static final double FIRST_LINE_SHIPPING_PRICE = 5.0;
  private static final double SECOND_LINE_SHIPPING_PRICE = 7.50;
  private static final double ORDER_DELIVERY_COST = 10.00;
  private static final String CURRENCY_ISO_CODE = "currencyIsoCode";

  @InjectMocks
  private MiraklFindDeliveryCostStrategy testObj = new MiraklFindDeliveryCostStrategy();

  @Mock
  private ModelService modelServiceMock;

  @Mock
  private AbstractOrderEntryModel firstOrderEntryMock, secondOrderEntryMock;
  @Mock
  private AbstractOrderModel orderMock;
  @Mock
  private PriceValue priceValueMock;
  @Mock(answer = Answers.RETURNS_DEEP_STUBS)
  private AbstractOrder orderItemMock;
  @Mock
  private CurrencyModel currencyMock;
  @Mock
  private DeliveryModeModel deliveryModeModelMock;
  @Mock
  private DeliveryMode deliveryModeMock;

  @Before
  public void setUp() throws JaloDeliveryModeException {
    when(modelServiceMock.getSource(orderMock)).thenReturn(orderItemMock);
    when(orderItemMock.getDeliveryMode().getCost(orderItemMock)).thenReturn(priceValueMock);
    when(modelServiceMock.getSource(deliveryModeModelMock)).thenReturn(deliveryModeMock);

    when(deliveryModeMock.getCost(any(AbstractOrder.class))).thenReturn(priceValueMock);
    when(priceValueMock.getValue()).thenReturn(ORDER_DELIVERY_COST);
    when(priceValueMock.getCurrencyIso()).thenReturn(CURRENCY_ISO_CODE);
    when(currencyMock.getIsocode()).thenReturn(CURRENCY_ISO_CODE);
    when(priceValueMock.isNet()).thenReturn(true);

    when(orderMock.getEntries()).thenReturn(ImmutableList.of(firstOrderEntryMock, secondOrderEntryMock));
    when(orderMock.getCurrency()).thenReturn(currencyMock);
    when(orderMock.getDeliveryMode()).thenReturn(deliveryModeModelMock);

    when(firstOrderEntryMock.getLineShippingPrice()).thenReturn(FIRST_LINE_SHIPPING_PRICE);
    when(secondOrderEntryMock.getLineShippingPrice()).thenReturn(SECOND_LINE_SHIPPING_PRICE);
  }

  @Test
  public void getsCost() {
    PriceValue result = testObj.getDeliveryCost(orderMock);

    assertThat(result.getValue()).isEqualTo(ORDER_DELIVERY_COST + FIRST_LINE_SHIPPING_PRICE + SECOND_LINE_SHIPPING_PRICE);
    assertThat(result.getCurrencyIso()).isEqualTo(CURRENCY_ISO_CODE);
    assertThat(result.isNet()).isTrue();
  }

  @Test(expected = IllegalArgumentException.class)
  public void getCostThrowsIllegalArgumentExceptionIfOrderIsNull() {
    testObj.getDeliveryCost(null);
  }

}
