package com.mirakl.hybris.core.catalog.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.catalog.strategies.CoreAttributeHandler;
import com.mirakl.hybris.core.enums.MiraklAttributeRole;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCoreAttributeHandlerResolverTest {

  private static final String CUSTOM_CORE_ATTRIBUTE_BEAN_HANDLER_ID = "customBeanHandlerId";

  @InjectMocks
  @Spy
  private DefaultCoreAttributeHandlerResolver testObj;

  @Mock
  private Map<MiraklAttributeRole, CoreAttributeHandler> roleAttributeHandlers;
  @Mock
  private MiraklCoreAttributeModel coreAttribute;
  @Mock
  private CoreAttributeHandler defaultHandler, customHandler, titleRoleHandler;

  @Before
  public void setUp() throws Exception {
    doReturn(customHandler).when(testObj).getCoreAttributeBeanHandler(any(MiraklCoreAttributeModel.class));
    when(roleAttributeHandlers.containsKey(MiraklAttributeRole.TITLE_ATTRIBUTE)).thenReturn(true);
    when(roleAttributeHandlers.get(MiraklAttributeRole.TITLE_ATTRIBUTE)).thenReturn(titleRoleHandler);
    when(coreAttribute.getImportExportHandlerStringId()).thenReturn(CUSTOM_CORE_ATTRIBUTE_BEAN_HANDLER_ID);
    when(coreAttribute.getRole()).thenReturn(MiraklAttributeRole.TITLE_ATTRIBUTE);
  }

  @Test
  public void customHandlerHasHighestPriority() throws Exception {
    CoreAttributeHandler<MiraklCoreAttributeModel> output = testObj.determineHandler(coreAttribute);

    assertThat(output).isEqualTo(customHandler);
  }

  @Test
  public void roleHandlersHaveMidPriority() throws Exception {
    when(coreAttribute.getImportExportHandlerStringId()).thenReturn(null);

    CoreAttributeHandler<MiraklCoreAttributeModel> output = testObj.determineHandler(coreAttribute);

    assertThat(output).isEqualTo(titleRoleHandler);
  }

  @Test
  public void defaultHandlerHasLowestPriority() throws Exception {
    when(coreAttribute.getImportExportHandlerStringId()).thenReturn(null);
    when(coreAttribute.getRole()).thenReturn(null);

    CoreAttributeHandler<MiraklCoreAttributeModel> output = testObj.determineHandler(coreAttribute);

    assertThat(output).isEqualTo(defaultHandler);
  }

  @Test
  public void defaultHandlerShouldBeUsedWhenRoleHasNoHandler() throws Exception {
    when(coreAttribute.getImportExportHandlerStringId()).thenReturn(null);
    when(coreAttribute.getRole()).thenReturn(MiraklAttributeRole.VARIANT_GROUP_CODE_ATTRIBUTE);

    CoreAttributeHandler<MiraklCoreAttributeModel> output = testObj.determineHandler(coreAttribute);

    assertThat(output).isEqualTo(defaultHandler);
  }

}
