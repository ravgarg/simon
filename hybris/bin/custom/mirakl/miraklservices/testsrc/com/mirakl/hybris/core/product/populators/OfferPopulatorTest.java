package com.mirakl.hybris.core.product.populators;

import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.common.MiraklDiscount;
import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.mirakl.client.mmp.domain.offer.MiraklOfferMinimumShipping;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.i18n.services.CurrencyService;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.populators.OfferPopulator;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.core.shop.daos.ShopDao;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OfferPopulatorTest {

  private static final String OFFER_ID = "offerId";
  private static final String OFFER_CODE = "offerCode";
  private static final String OFFER_DESCRIPTION = "offerDescription";
  private static final String OFFER_PRICE_ADDITIONAL_INFO = "offerPriceAdditionalInfo";
  private static final String PRODUCT_CODE = "productCode";
  private static final String OFFER_SHOP_ID = "offerShopId";
  private static final String OFFER_STATE_CODE = "stateCode";
  private static final int FAVORITE_RANK = 100;
  private static final int LEAD_TIME_TO_SHIP = 20;
  private static final int OFFER_QUANTITY = 999;
  private static final BigDecimal OFFER_PRICE = new BigDecimal(99.99);
  private static final BigDecimal OFFER_DISCOUNT_PRICE = new BigDecimal(88.88);
  private static final BigDecimal OFFER_ORIGINAL_PRICE = new BigDecimal(20.00);
  private static final BigDecimal OFFER_MIN_SHIPPING_PRICE = new BigDecimal(10.00);
  private static final BigDecimal OFFER_MIN_SHIPPING_PRICE_ADDITIONAL = new BigDecimal(5.00);
  private static final BigDecimal OFFER_TOTAL_PRICE = new BigDecimal(109.99);

  @InjectMocks
  private OfferPopulator testObj = new OfferPopulator();

  @Mock
  private CurrencyService currencyServiceMock;
  @Mock
  private ShopDao shopDaoMock;
  @Mock
  private EnumerationService enumerationServiceMock;
  @Mock
  private OfferCodeGenerationStrategy offerCodeGenerationStrategyMock;

  @Mock
  private MiraklExportOffer miraklExportOfferMock;
  @Mock
  private OfferModel offerModelMock;
  @Mock
  private Date availableStartDateMock, availableEndDateMock, discountStartDateMock, discountEndDateMock;
  @Mock
  private MiraklDiscount miraklDiscountMock;
  @Mock
  private MiraklOfferMinimumShipping miraklOfferMinimumShippingMock;
  @Mock
  private CurrencyModel currencyMock;
  @Mock
  private ShopModel shopMock;
  @Mock
  private OfferState offerStateMock;

  @Before
  public void setUp() {
    when(miraklExportOfferMock.getId()).thenReturn(OFFER_ID);
    when(miraklExportOfferMock.getAvailableEndDate()).thenReturn(availableEndDateMock);
    when(miraklExportOfferMock.getAvailableStartDate()).thenReturn(availableStartDateMock);
    when(miraklExportOfferMock.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.EUR);
    when(miraklExportOfferMock.getDescription()).thenReturn(OFFER_DESCRIPTION);
    when(miraklExportOfferMock.getFavoritRank()).thenReturn(FAVORITE_RANK);
    when(miraklExportOfferMock.getLeadtimeToShip()).thenReturn(LEAD_TIME_TO_SHIP);
    when(miraklExportOfferMock.getPrice()).thenReturn(OFFER_PRICE);
    when(miraklExportOfferMock.getPriceAdditionalInfo()).thenReturn(OFFER_PRICE_ADDITIONAL_INFO);
    when(miraklExportOfferMock.getProductSku()).thenReturn(PRODUCT_CODE);
    when(miraklExportOfferMock.getDiscount()).thenReturn(miraklDiscountMock);
    when(miraklExportOfferMock.getMinShipping()).thenReturn(miraklOfferMinimumShippingMock);
    when(miraklExportOfferMock.getQuantity()).thenReturn(OFFER_QUANTITY);
    when(miraklExportOfferMock.getShopId()).thenReturn(OFFER_SHOP_ID);
    when(miraklExportOfferMock.isDeleted()).thenReturn(false);
    when(miraklExportOfferMock.isActive()).thenReturn(true);
    when(miraklExportOfferMock.getTotalPrice()).thenReturn(OFFER_TOTAL_PRICE);
    when(miraklExportOfferMock.getStateCode()).thenReturn(OFFER_STATE_CODE);

    when(miraklDiscountMock.getDiscountPrice()).thenReturn(OFFER_DISCOUNT_PRICE);
    when(miraklDiscountMock.getOriginPrice()).thenReturn(OFFER_ORIGINAL_PRICE);
    when(miraklDiscountMock.getStartDate()).thenReturn(discountStartDateMock);
    when(miraklDiscountMock.getEndDate()).thenReturn(discountEndDateMock);

    when(miraklOfferMinimumShippingMock.getPrice()).thenReturn(OFFER_MIN_SHIPPING_PRICE);
    when(miraklOfferMinimumShippingMock.getPriceAdditional()).thenReturn(OFFER_MIN_SHIPPING_PRICE_ADDITIONAL);

    when(currencyServiceMock.getCurrencyForCode(MiraklIsoCurrencyCode.EUR.name())).thenReturn(currencyMock);
    when(shopDaoMock.findShopById(OFFER_SHOP_ID)).thenReturn(shopMock);
    when(enumerationServiceMock.getEnumerationValue(OfferState.class, OFFER_STATE_CODE)).thenReturn(offerStateMock);
    when(offerCodeGenerationStrategyMock.generateCode(OFFER_ID)).thenReturn(OFFER_CODE);
  }

  @Test
  public void populatesAllOfferProperties() {
    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferDiscount();
    verifyPopulateOfferMinShipping();

    verify(offerModelMock).setCurrency(currencyMock);
    verify(offerModelMock).setState(offerStateMock);
    verify(offerModelMock).setShop(shopMock);
  }

  @Test
  public void populatesOfferPropertiesWithoutDiscount() {
    when(miraklExportOfferMock.getDiscount()).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferMinShipping();

    verify(offerModelMock).setCurrency(currencyMock);
    verify(offerModelMock).setState(offerStateMock);
    verify(offerModelMock).setShop(shopMock);
  }

  @Test
  public void populatesOfferPropertiesWithoutMinShipping() {
    when(miraklExportOfferMock.getMinShipping()).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferDiscount();

    verify(offerModelMock).setCurrency(currencyMock);
    verify(offerModelMock).setState(offerStateMock);
    verify(offerModelMock).setShop(shopMock);
  }

  @Test
  public void populatesOfferPropertiesWithoutOfferState() {
    when(miraklExportOfferMock.getStateCode()).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferMinShipping();
    verifyPopulateOfferDiscount();

    verify(offerModelMock).setCurrency(currencyMock);
    verify(offerModelMock, never()).setState(any(OfferState.class));
    verify(offerModelMock).setShop(shopMock);
  }

  @Test(expected = ConversionException.class)
  public void populateThrowsConversionExceptionIfNoOfferStateFoundForOffer() {
    when(enumerationServiceMock.getEnumerationValue(OfferState.class, OFFER_STATE_CODE))
        .thenThrow(new UnknownIdentifierException(EMPTY));

    testObj.populate(miraklExportOfferMock, offerModelMock);
  }

  @Test
  public void populatesOfferPropertiesWithoutCurrency() {
    when(miraklExportOfferMock.getCurrencyIsoCode()).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferMinShipping();
    verifyPopulateOfferDiscount();

    verify(offerModelMock, never()).setCurrency(any(CurrencyModel.class));
    verify(offerModelMock).setState(offerStateMock);
    verify(offerModelMock).setShop(shopMock);
  }

  @Test
  public void populatesOfferPropertiesWithoutShop() {
    when(miraklExportOfferMock.getShopId()).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);

    verifyPopulateNullableOfferProperties();
    verifyPopulateOfferMinShipping();
    verifyPopulateOfferDiscount();

    verify(offerModelMock).setCurrency(currencyMock);
    verify(offerModelMock).setState(offerStateMock);
    verify(offerModelMock, never()).setShop(any(ShopModel.class));
  }

  @Test(expected = ConversionException.class)
  public void populateThrowsConversionExceptionIfNoShopFoundForOffer() {
    when(shopDaoMock.findShopById(OFFER_SHOP_ID)).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);
  }

  @Test(expected = ConversionException.class)
  public void populateThrowsConversionExceptionIfCurrencyFoundForOffer() {
    when(currencyServiceMock.getCurrencyForCode(MiraklIsoCurrencyCode.EUR.name())).thenReturn(null);

    testObj.populate(miraklExportOfferMock, offerModelMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfMiraklExportOfferIsNull() {
    testObj.populate(null, offerModelMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsIllegalArgumentExceptionIfOfferModelIsNull() {
    testObj.populate(miraklExportOfferMock, null);
  }

  private void verifyPopulateNullableOfferProperties() {
    verify(offerModelMock).setActive(true);
    verify(offerModelMock).setAvailableEndDate(availableEndDateMock);
    verify(offerModelMock).setAvailableStartDate(availableStartDateMock);
    verify(offerModelMock).setDeleted(false);
    verify(offerModelMock).setDescription(OFFER_DESCRIPTION);
    verify(offerModelMock).setFavoriteRank(FAVORITE_RANK);
    verify(offerModelMock).setId(OFFER_ID);
    verify(offerModelMock).setLeadTimeToShip(LEAD_TIME_TO_SHIP);
    verify(offerModelMock).setPrice(OFFER_PRICE);
    verify(offerModelMock).setPriceAdditionalInfo(OFFER_PRICE_ADDITIONAL_INFO);
    verify(offerModelMock).setProductCode(PRODUCT_CODE);
    verify(offerModelMock).setQuantity(OFFER_QUANTITY);
    verify(offerModelMock).setTotalPrice(OFFER_TOTAL_PRICE);
  }

  private void verifyPopulateOfferDiscount() {
    verify(offerModelMock).setDiscountEndDate(discountEndDateMock);
    verify(offerModelMock).setDiscountStartDate(discountStartDateMock);
    verify(offerModelMock).setDiscountPrice(OFFER_DISCOUNT_PRICE);
    verify(offerModelMock).setOriginPrice(OFFER_ORIGINAL_PRICE);
  }

  private void verifyPopulateOfferMinShipping() {
    verify(offerModelMock).setMinShippingPrice(OFFER_MIN_SHIPPING_PRICE);
    verify(offerModelMock).setMinShippingPriceAdditional(OFFER_MIN_SHIPPING_PRICE_ADDITIONAL);
  }
}
