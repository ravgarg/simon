package com.mirakl.hybris.core.category.services.impl;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.category.synchro.MiraklCategorySynchroTracking;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.catalog.category.MiraklCategorySynchroRequest;
import com.mirakl.hybris.core.category.services.CommissionCategoryService;
import com.mirakl.hybris.core.category.services.impl.DefaultCommissionCategoryExportService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Locale;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultCommissionCategoryExportServiceTest {

  private static final String CSV_CONTENT = "csvContent";
  private static final String SYNCHRONIZATION_FILE_NAME = "Synchronization file name";

  @InjectMocks
  private DefaultCommissionCategoryExportService testObj = new DefaultCommissionCategoryExportService();

  @Mock
  private CommissionCategoryService commissionCategoryServiceMock;
  @Mock
  private MiraklMarketplacePlatformFrontApi miraklOperatorApiMock;

  @Mock
  private CategoryModel rootCategoryMock, commissionCategoryMock;
  @Mock
  private MiraklCategorySynchroTracking categorySynchroTrackingMock;

  @Captor
  private ArgumentCaptor<MiraklCategorySynchroRequest> categorySynchroRequestArgumentCaptor;

  @Before
  public void setUp() throws IOException {
    when(commissionCategoryServiceMock.getCategories(rootCategoryMock))
        .thenReturn(asList(rootCategoryMock, commissionCategoryMock));
    when(commissionCategoryServiceMock.getCategoryExportCsvContent(Locale.ENGLISH,
        asList(rootCategoryMock, commissionCategoryMock))).thenReturn(CSV_CONTENT);

    when(miraklOperatorApiMock.synchronizeCategories(categorySynchroRequestArgumentCaptor.capture()))
        .thenReturn(categorySynchroTrackingMock);
  }

  @Test
  public void exportsCommissionCategories() throws IOException {
    MiraklCategorySynchroTracking result = testObj.exportCommissionCategories(rootCategoryMock, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME);

    assertThat(result).isSameAs(categorySynchroTrackingMock);

    verify(commissionCategoryServiceMock).getCategories(rootCategoryMock);
    verify(commissionCategoryServiceMock).getCategoryExportCsvContent(Locale.ENGLISH,
        asList(rootCategoryMock, commissionCategoryMock));

    verify(miraklOperatorApiMock).synchronizeCategories(categorySynchroRequestArgumentCaptor.capture());

    MiraklCategorySynchroRequest miraklCategorySynchroRequest = categorySynchroRequestArgumentCaptor.getValue();
    assertThat(miraklCategorySynchroRequest).isNotNull();
    assertThat(IOUtils.toString(miraklCategorySynchroRequest.getInputStream())).isEqualTo(CSV_CONTENT);
    assertThat(miraklCategorySynchroRequest.getFilename()).isEqualTo(SYNCHRONIZATION_FILE_NAME);
  }

  @Test(expected = MiraklApiException.class)
  public void exportCommissionCategoriesThrowsMiraklApiException() throws IOException {
    when(miraklOperatorApiMock.synchronizeCategories(categorySynchroRequestArgumentCaptor.capture()))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    testObj.exportCommissionCategories(rootCategoryMock, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME);
  }

  @Test(expected = IOException.class)
  public void exportCommissionCategoriesThrowsIOException() throws IOException {
    when(commissionCategoryServiceMock.getCategoryExportCsvContent(Locale.ENGLISH,
        asList(rootCategoryMock, commissionCategoryMock))).thenThrow(new IOException());

    testObj.exportCommissionCategories(rootCategoryMock, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME);
  }

  @Test(expected = IllegalArgumentException.class)
  public void exportCommissionCategoriesThrowsIllegalArgumentExceptionIfRootCategoryIsNull() throws IOException {
    testObj.exportCommissionCategories(null, Locale.ENGLISH, SYNCHRONIZATION_FILE_NAME);
  }

}
