package com.mirakl.hybris.core.product.jobs;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.hybris.core.model.MiraklExportSellableProductsCronJobModel;
import com.mirakl.hybris.core.product.jobs.MiraklExportSellableProductsJob;
import com.mirakl.hybris.core.product.services.ProductExportService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Date;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class MiraklExportSellableProductsJobTest {

  private static final String SYNCHRONIZATION_FILE_NAME = "Synchronization file name";

  @InjectMocks
  private MiraklExportSellableProductsJob miraklExportSellableProductsJob;

  @Mock
  private ProductExportService productExportService;

  @Mock
  private ModelService modelService;

  @Mock
  private MiraklExportSellableProductsCronJobModel cronJob;

  @Mock
  private CategoryModel rootCategory;

  @Mock
  private CategoryModel rootBrandCategory;

  @Mock
  private BaseSiteModel baseSite;

  @Before
  public void setUp() {
    when(cronJob.getRootCategory()).thenReturn(rootCategory);
    when(cronJob.getRootBrandCategory()).thenReturn(rootBrandCategory);
    when(cronJob.getBaseSite()).thenReturn(baseSite);
    when(cronJob.getSynchronizationFileName()).thenReturn(SYNCHRONIZATION_FILE_NAME);
  }

  @Test
  public void shouldExecuteFullExportSuccessfully() throws IOException {
    when(cronJob.isFullExport()).thenReturn(Boolean.TRUE);

    PerformResult result = miraklExportSellableProductsJob.perform(cronJob);

    verify(modelService).save(cronJob);
    verify(productExportService).exportAllProducts(rootCategory, rootBrandCategory, baseSite, SYNCHRONIZATION_FILE_NAME);
    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
  }

  @Test
  public void shouldExecuteIncrementalExportSuccessfully() throws IOException {
    Date lastExportDate = new Date();
    when(cronJob.getLastExportDate()).thenReturn(lastExportDate);
    when(cronJob.isFullExport()).thenReturn(Boolean.FALSE);

    PerformResult result = miraklExportSellableProductsJob.perform(cronJob);

    verify(modelService).save(cronJob);
    verify(productExportService).exportModifiedProducts(rootCategory, rootBrandCategory, baseSite, lastExportDate, SYNCHRONIZATION_FILE_NAME);
    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
  }

  @Test
  public void shouldExecuteFullExportWhenNoLastExportDate() throws IOException {
    when(cronJob.isFullExport()).thenReturn(Boolean.FALSE);
    when(cronJob.getLastExportDate()).thenReturn(null);

    PerformResult result = miraklExportSellableProductsJob.perform(cronJob);

    verify(modelService).save(cronJob);
    verify(productExportService).exportAllProducts(rootCategory, rootBrandCategory, baseSite, SYNCHRONIZATION_FILE_NAME);
    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
  }

  @Test
  public void shouldEndWithErrorOnException() throws IOException {
    when(productExportService.exportAllProducts(rootCategory, rootBrandCategory, baseSite, SYNCHRONIZATION_FILE_NAME))
        .thenThrow(new MiraklApiException(new MiraklErrorResponseBean()));

    PerformResult result = miraklExportSellableProductsJob.perform(cronJob);

    assertThat(result.getResult()).isEqualTo(CronJobResult.ERROR);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.ABORTED);
  }
}
