package com.mirakl.hybris.core.order.strategies.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.order.daos.MiraklAbstractOrderEntryDao;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultCommonMiraklCartStrategyTest {

  @Spy
  @InjectMocks
  private DefaultCommonMiraklCartStrategy commonCartStrategy;

  @Mock
  private CartService cartService;

  @Mock
  private OfferModel offerModel;

  @Mock
  private ProductModel productModel;

  @Mock
  private CartModel cartModel;


  @Mock
  private CartEntryModel cartEntryModel;

  @Mock
  private MiraklAbstractOrderEntryDao<CartEntryModel> miraklCartEntryDao;

  @Before
  public void setUp() {
    when(miraklCartEntryDao.findEntryByOffer(cartModel, offerModel)).thenReturn(cartEntryModel);
    when(cartService.getEntriesForProduct(cartModel, productModel)).thenReturn(Collections.singletonList(cartEntryModel));
  }

  @Test
  public void shouldReturnRequestedAdjustmentWhenNoMaxOrderQuantity() {
    int quantityToAdd = 2;
    long cartLevel = 1;
    long productCartLevel = 5;
    int stockLevel = (int) cartLevel + quantityToAdd;
    mockQuantities(cartLevel, productCartLevel, null, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(quantityToAdd);
  }

  @Test
  public void shouldReturnRequestedAdjustmentWhenMaxOrderQuantityMoreThanRequested() {
    int quantityToAdd = 2;
    long cartLevel = 3;
    long productCartLevel = 5;
    int maxOrderQuantity = 10;
    int stockLevel = (int) cartLevel + quantityToAdd;
    mockQuantities(cartLevel, productCartLevel, maxOrderQuantity, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(quantityToAdd);
  }

  @Test
  public void shouldReturnPartialRequestedAdjustmentWhenMaxOrderQuantityLessThanRequested() {
    int quantityToAdd = 3;
    long cartLevel = 1;
    long productCartLevel = 2;
    int maxOrderQuantity = 4;
    int stockLevel = (int) cartLevel + quantityToAdd;
    mockQuantities(cartLevel, productCartLevel, maxOrderQuantity, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(1);
  }

  @Test
  public void shouldReturnNotAllowedAdjustmentWhenMaxOrderQuantityLessThanRequested() {
    int quantityToAdd = 2;
    long cartLevel = 1;
    long productCartLevel = 2;
    int maxOrderQuantity = 3;
    int stockLevel = (int) cartLevel + quantityToAdd;
    mockQuantities(cartLevel, productCartLevel, maxOrderQuantity, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(0);
  }

  @Test
  public void shouldReturnNotAllowedAdjustmentWhenNotInStock() {
    int quantityToAdd = 1;
    long cartLevel = 1;
    long productCartLevel = 0;
    int stockLevel = (int) cartLevel;
    mockQuantities(cartLevel, productCartLevel, null, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(0);
  }

  @Test
  public void shouldReturnPartialRequestedAdjustmentWhenWhenLowStock() {
    int quantityToAdd = 2;
    long cartLevel = 1;
    long productCartLevel = 0;
    int stockLevel = 2;
    mockQuantities(cartLevel, productCartLevel, null, stockLevel);

    long allowedAdjustment =
        commonCartStrategy.getAllowedCartAdjustmentForOffer(cartModel, productModel, offerModel, quantityToAdd);

    assertThat(allowedAdjustment).isEqualTo(1);
  }

  @Test
  public void shouldReturnCartLevel() {
    long cartEntryQuantity = 3;
    when(cartEntryModel.getQuantity()).thenReturn(cartEntryQuantity);

    long cartLevel = commonCartStrategy.checkCartLevel(offerModel, cartModel);

    assertThat(cartLevel).isEqualTo(cartEntryQuantity);
  }

  @Test
  public void shouldReturnSuccessStatusWhenQuantityAllowedAndNoMaxOrder() {
    final long actualAllowedQuantityChange = 1;
    final Integer maxOrderQuantity = null;
    final long quantityToAdd = 1;
    final long cartLevelAfterQuantityChange = 2;

    String status = commonCartStrategy.getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
        quantityToAdd, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.SUCCESS);
  }

  @Test
  public void shouldReturnSuccessStatusWhenQuantityAllowedAndLessThanMaxOrder() {
    final long actualAllowedQuantityChange = 1;
    final Integer maxOrderQuantity = 2;
    final long quantityToAdd = 1;
    final long cartLevelAfterQuantityChange = 2;

    String status = commonCartStrategy.getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
        quantityToAdd, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.SUCCESS);
  }

  @Test
  public void shouldReturnExceededStatusWhenQuantityAllowedAndMoreThanMaxOrder() {
    final long actualAllowedQuantityChange = 1;
    final Integer maxOrderQuantity = 2;
    final long quantityToAdd = 2;
    final long cartLevelAfterQuantityChange = 2;

    String status = commonCartStrategy.getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
        quantityToAdd, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.MAX_ORDER_QUANTITY_EXCEEDED);
  }

  @Test
  public void shouldReturnLowStockStatusWhenQuantityAllowedLessThanRequestedAndNosMaxOrder() {
    final long actualAllowedQuantityChange = 1;
    final Integer maxOrderQuantity = null;
    final long quantityToAdd = 2;
    final long cartLevelAfterQuantityChange = 2;

    String status = commonCartStrategy.getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
        quantityToAdd, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.LOW_STOCK);
  }

  @Test
  public void shouldReturnExceededStatusWhenNotAllowedAndMaxOrderHit() {
    final Integer maxOrderQuantity = 2;
    final long cartLevelAfterQuantityChange = 2;

    String status = commonCartStrategy.getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.MAX_ORDER_QUANTITY_EXCEEDED);
  }

  @Test
  public void shouldReturnExceededStatusWhenNotAllowedAndLessThanMaxOrder() {
    final Integer maxOrderQuantity = 2;
    final long cartLevelAfterQuantityChange = 4;

    String status = commonCartStrategy.getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, cartLevelAfterQuantityChange);

    assertThat(status).isEqualTo(CommerceCartModificationStatus.NO_STOCK);
  }

  private void mockQuantities(long cartLevel, long productCartLevel, Integer maxOrderQuantity, int stockLevel) {
    when(productModel.getMaxOrderQuantity()).thenReturn(maxOrderQuantity);
    when(offerModel.getQuantity()).thenReturn(stockLevel);
    doReturn(cartLevel).when(commonCartStrategy).checkCartLevel(offerModel, cartModel);
    doReturn(productCartLevel).when(commonCartStrategy).checkCartLevel(productModel, cartModel, null);
  }

}
