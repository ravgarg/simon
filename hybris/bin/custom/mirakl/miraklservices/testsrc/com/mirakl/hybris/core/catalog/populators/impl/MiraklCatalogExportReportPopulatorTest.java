package com.mirakl.hybris.core.catalog.populators.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Map;

import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.client.domain.common.MiraklProcessTrackingStatus;
import com.mirakl.client.mci.front.domain.common.AbstractMiraklCatalogImportResult;
import com.mirakl.hybris.core.enums.MiraklExportStatus;
import com.mirakl.hybris.core.model.MiraklJobReportModel;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklCatalogExportReportPopulatorTest {

  @Mock
  private Map<MiraklProcessTrackingStatus, MiraklExportStatus> exportStatuses;

  @Mock
  private AbstractMiraklCatalogImportResult miraklCatalogImportResult;

  @InjectMocks
  private MiraklCatalogExportReportPopulator testObj;

  @Test
  public void populate() throws Exception {
    MiraklJobReportModel target = new MiraklJobReportModel();
    when(miraklCatalogImportResult.getImportStatus()).thenReturn(MiraklProcessTrackingStatus.COMPLETE);
    when(exportStatuses.get(MiraklProcessTrackingStatus.COMPLETE)).thenReturn(MiraklExportStatus.COMPLETE);
    when(miraklCatalogImportResult.hasErrorReport()).thenReturn(true);

    testObj.populate(miraklCatalogImportResult, target);

    assertThat(target.getHasErrorReport()).isTrue();
    assertThat(target.getStatus()).isEqualTo(MiraklExportStatus.COMPLETE);
  }

}
