package com.mirakl.hybris.core.shop.daos.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Collection;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTest;

@IntegrationTest
public class ShopDaoIntegrationTest extends ServicelayerTest {

  private static final String TEST_SHOP_ID_1 = "testShop1";
  public static final String TEST_PRODUCT_CODE = "testProductCode";

  @Resource
  private ShopDao shopDao;

  @Before
  public void setUp() throws ImpExException {
    importCsv("/miraklservices/test/testShops.impex", "utf-8");
  }

  @Test
  public void findsShopById() {
    ShopModel result = shopDao.findShopById(TEST_SHOP_ID_1);

    assertThat(result.getId()).isEqualTo(TEST_SHOP_ID_1);
  }

  @Test
  public void findShopsForProductCode() {
    Collection<ShopModel> result = shopDao.findShopsForProductCode(TEST_PRODUCT_CODE);

    assertThat(result).hasSize(2);
  }
}
