package com.mirakl.hybris.core.shop.services.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.request.shop.MiraklGetShopEvaluationsRequest;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShopServiceTest {

  public static final String SHOP_ID = "shop_id";
  public static final int EVALUATION_PAGE = 5;
  public static final int EVALUATION_PAGE_SIZE = 10;
  public static final int WRONG_EVALUATION_PAGE = -3;

  @InjectMocks
  private DefaultShopService defaultShopService = new DefaultShopService();

  @Mock
  private ShopDao shopDao;

  @Mock
  private MiraklMarketplacePlatformFrontApiClient mockedMiraklApi;

  @Mock
  private PageableData pageableData;

  @Before
  public void setUp() {
    when(pageableData.getCurrentPage()).thenReturn(EVALUATION_PAGE);
    when(pageableData.getPageSize()).thenReturn(EVALUATION_PAGE_SIZE);
  }

  @Test
  public void getShopForIdShouldReturnAResult() {
    when(shopDao.findShopById(anyString())).thenReturn(new ShopModel());

    assertNotNull(defaultShopService.getShopForId(SHOP_ID));
  }

  @Test
  public void getShopForIdShouldReturnNullWhenNoResult() {
    when(shopDao.findShopById(anyString())).thenReturn(null);

    assertNull(defaultShopService.getShopForId(SHOP_ID));
  }

  @Test
  public void getEvaluationsInNormalUse() {
    ArgumentCaptor<MiraklGetShopEvaluationsRequest> argument = ArgumentCaptor.forClass(MiraklGetShopEvaluationsRequest.class);

    defaultShopService.getEvaluations(SHOP_ID, pageableData);

    verify(mockedMiraklApi).getShopEvaluations(argument.capture());
    assertThat(argument.getValue().getOffset()).isEqualTo(40);
    assertThat(argument.getValue().getMax()).isEqualTo(EVALUATION_PAGE_SIZE);
    assertThat(argument.getValue().isPaginate()).isTrue();
    assertThat(argument.getValue().getShopId()).isEqualTo(SHOP_ID);
  }

  @Test(expected = IllegalArgumentException.class)
  public void getEvaluationsWhenWrongPageIsRequested() {
    when(pageableData.getCurrentPage()).thenReturn(WRONG_EVALUATION_PAGE);

    defaultShopService.getEvaluations(SHOP_ID, pageableData);
  }

}
