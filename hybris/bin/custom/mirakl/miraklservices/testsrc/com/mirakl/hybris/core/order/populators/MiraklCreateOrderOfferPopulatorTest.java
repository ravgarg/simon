package com.mirakl.hybris.core.order.populators;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.order.tax.MiraklOrderTaxAmount;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderOffer;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.services.OfferService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklCreateOrderOfferPopulatorTest {

  private static final String OFFER_ID = "offerId";
  private static final String OFFER_SHIPPING_TYPE_CODE = "offerShippingTypeCode";
  private static final int QUANTITY = 2;
  private static final int OFFER_LEAD_TIME_TO_SHIP = 3;
  private static final BigDecimal OFFER_UNIT_PRICE = BigDecimal.valueOf(10.0);
  private static final BigDecimal OFFER_LINE_PRICE = OFFER_UNIT_PRICE.multiply(BigDecimal.valueOf(QUANTITY));
  private static final BigDecimal OFFER_SHIPPING_PRICE = BigDecimal.valueOf(5.0);

  @InjectMocks
  private MiraklCreateOrderOfferPopulator testObj = new MiraklCreateOrderOfferPopulator();

  @Mock
  private OfferService offerServiceMock;
  @Mock
  private Converter<TaxValue, MiraklOrderTaxAmount> miraklOrderTaxAmountConverterMock;

  @Mock
  private AbstractOrderEntryModel orderEntryMock;
  @Mock
  private AbstractOrderModel orderMock;
  @Mock
  private MiraklOrderTaxAmount taxAmountMock;
  @Mock
  private OfferModel offerMock;
  @Mock
  private CurrencyModel currencyMock;
  @Mock
  private TaxValue taxValueMock;

  @Before
  public void setUp() {
    when(orderEntryMock.getOfferId()).thenReturn(OFFER_ID);
    when(orderEntryMock.getLineShippingPrice()).thenReturn(OFFER_SHIPPING_PRICE.doubleValue());
    when(orderEntryMock.getLineShippingCode()).thenReturn(OFFER_SHIPPING_TYPE_CODE);
    when(orderEntryMock.getTaxValues()).thenReturn(singletonList(taxValueMock));
    when(orderEntryMock.getQuantity()).thenReturn(Long.valueOf(QUANTITY));
    when(orderEntryMock.getOrder()).thenReturn(orderMock);

    when(offerMock.getPrice()).thenReturn(OFFER_UNIT_PRICE);
    when(offerMock.getLeadTimeToShip()).thenReturn(OFFER_LEAD_TIME_TO_SHIP);
    when(offerMock.getCurrency()).thenReturn(currencyMock);

    when(currencyMock.getIsocode()).thenReturn(MiraklIsoCurrencyCode.EUR.name());

    when(offerServiceMock.getOfferForId(OFFER_ID)).thenReturn(offerMock);
    when(miraklOrderTaxAmountConverterMock.convertAll(singletonList(taxValueMock))).thenReturn(singletonList(taxAmountMock));
  }

  @Test
  public void shouldPopulateMiraklCreateOrderOffer() {
    MiraklCreateOrderOffer result = new MiraklCreateOrderOffer();

    testObj.populate(orderEntryMock, result);

    assertThat(result.getId()).isEqualTo(OFFER_ID);
    assertThat(result.getPriceUnit()).isEqualTo(OFFER_UNIT_PRICE);
    assertThat(result.getQuantity()).isEqualTo(QUANTITY);
    assertThat(result.getPrice()).isEqualTo(OFFER_LINE_PRICE);
    assertThat(result.getShippingPrice()).isEqualTo(OFFER_SHIPPING_PRICE);
    assertThat(result.getShippingTypeCode()).isEqualTo(OFFER_SHIPPING_TYPE_CODE);
    assertThat(result.getCurrencyIsoCode()).isEqualTo(MiraklIsoCurrencyCode.EUR);
    assertThat(result.getLeadtimeToShip()).isEqualTo(OFFER_LEAD_TIME_TO_SHIP);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfOrderEntryIsNull() {
    testObj.populate(null, new MiraklCreateOrderOffer());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfMiraklCreateOrderOfferIsNull() {
    testObj.populate(orderEntryMock, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfOfferIdIsNull() {
    when(orderEntryMock.getOfferId()).thenReturn(null);

    testObj.populate(orderEntryMock, new MiraklCreateOrderOffer());
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldThrowIllegalArgumentExceptionIfLineShippingCodeIsNull() {
    when(orderEntryMock.getLineShippingCode()).thenReturn(null);

    testObj.populate(orderEntryMock, new MiraklCreateOrderOffer());
  }

  @Test
  public void shouldNotSetTaxesOnGrossOrders() {
    when(orderMock.getNet()).thenReturn(false);

    testObj.populate(orderEntryMock, new MiraklCreateOrderOffer());

    verifyZeroInteractions(miraklOrderTaxAmountConverterMock);
  }

  @Test
  public void shouldSetTaxesOnNetOrders() {
    when(orderMock.getNet()).thenReturn(true);

    MiraklCreateOrderOffer result = new MiraklCreateOrderOffer();
    testObj.populate(orderEntryMock, result);

    assertThat(result.getTaxes()).containsOnly(taxAmountMock);
  }
}
