package com.mirakl.hybris.core.order.services.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.request.order.incident.MiraklCloseIncidentRequest;
import com.mirakl.client.mmp.request.order.incident.MiraklOpenIncidentRequest;
import com.mirakl.hybris.core.enums.MiraklOrderLineStatus;
import com.mirakl.hybris.core.ordersplitting.daos.ConsignmentEntryDao;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultIncidentServiceTest {

  private static final String CONSIGNMENT_ENTRY_CODE = "1234567-976543-79840496-A-1";
  private static final String REASON_CODE = "4";
  private static final String CONSIGNMENT_CODE = "1234567-976543-79840496-A";

  @InjectMocks
  private DefaultIncidentService testObj;

  @Mock
  private MiraklMarketplacePlatformFrontApiClient miraklApi;

  @Mock
  private ConsignmentEntryDao consignmentEntryDao;

  @Mock
  private UserModel user, wrongUser;

  @Mock
  private ConsignmentEntryModel consignmentEntry;

  @Mock
  private ConsignmentModel consignment;

  @Mock
  private OrderEntryModel orderEntry;

  @Mock
  private OrderModel order;

  @Mock
  private ProductModel product;

  @Mock
  private ModelService modelService;

  @Mock
  private UserService userService;

  @Mock
  private MarketplaceConsignmentService marketplaceConsignmentService;

  @Before
  public void setUp() {
    when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
    when(consignmentEntry.getCanOpenIncident()).thenReturn(true);
    when(consignmentEntry.getConsignment()).thenReturn(consignment);
    when(consignment.getCode()).thenReturn(CONSIGNMENT_CODE);
    when(orderEntry.getOrder()).thenReturn(order);
    when(marketplaceConsignmentService.getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE)).thenReturn(consignmentEntry);
  }

  @Test
  public void getReasons() {
    when(miraklApi.getReasons()).thenReturn(Collections.singletonList(new MiraklReason()));

    testObj.getReasons();

    verify(miraklApi).getReasons();
  }

  @Test
  public void openIncident() {
    testObj.openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(miraklApi).openIncident(any(MiraklOpenIncidentRequest.class));
    verify(consignmentEntry).setCanOpenIncident(false);
    verify(consignmentEntry).setMiraklOrderLineStatus(MiraklOrderLineStatus.INCIDENT_OPEN);
    verify(modelService).save(consignmentEntry);
  }

  @Test(expected = IllegalStateException.class)
  public void openIncidentWhenWrongState() {
    when(consignmentEntry.getCanOpenIncident()).thenReturn(false);

    testObj.openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(modelService, times(0)).save(consignmentEntry);
  }

  @Test(expected = MiraklApiException.class)
  public void openIncidentShouldNotSaveWhenApiCallFails() {
    doThrow(new MiraklApiException(new MiraklErrorResponseBean())).when(miraklApi)
        .openIncident(any(MiraklOpenIncidentRequest.class));

    testObj.openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(modelService, times(0)).save(consignmentEntry);
  }

  @Test
  public void closeIncident() {
    when(consignmentEntry.getCanOpenIncident()).thenReturn(false);
    when(consignmentEntry.getMiraklOrderLineStatus()).thenReturn(MiraklOrderLineStatus.INCIDENT_OPEN);

    testObj.closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(miraklApi).closeIncident(any(MiraklCloseIncidentRequest.class));
    verify(consignmentEntry).setCanOpenIncident(true);
    verify(consignmentEntry).setMiraklOrderLineStatus(MiraklOrderLineStatus.INCIDENT_CLOSED);
    verify(modelService).save(consignmentEntry);
  }

  @Test(expected = IllegalStateException.class)
  public void closeIncidentWhenOpeningIncidentIsPossible() {
    testObj.closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(modelService, times(0)).save(consignmentEntry);
  }

  @Test(expected = IllegalStateException.class)
  public void closeIncidentWhenWrongState() {
    when(consignmentEntry.getCanOpenIncident()).thenReturn(false);
    when(consignmentEntry.getMiraklOrderLineStatus()).thenReturn(MiraklOrderLineStatus.INCIDENT_CLOSED);

    testObj.closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(modelService, times(0)).save(consignmentEntry);
  }

  @Test(expected = MiraklApiException.class)
  public void closeIncidentShouldNotSaveWhenApiCallFails() {
    when(consignmentEntry.getCanOpenIncident()).thenReturn(false);
    when(consignmentEntry.getMiraklOrderLineStatus()).thenReturn(MiraklOrderLineStatus.INCIDENT_OPEN);

    doThrow(new MiraklApiException(new MiraklErrorResponseBean())).when(miraklApi)
        .closeIncident(any(MiraklCloseIncidentRequest.class));

    testObj.closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(marketplaceConsignmentService).getConsignmentEntryForMiraklLineId(CONSIGNMENT_ENTRY_CODE);
    verify(modelService, times(0)).save(consignmentEntry);
  }

}
