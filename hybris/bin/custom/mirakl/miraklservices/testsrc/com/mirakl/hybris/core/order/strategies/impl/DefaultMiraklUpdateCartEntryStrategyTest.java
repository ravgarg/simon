package com.mirakl.hybris.core.order.strategies.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.order.strategies.CommonMiraklCartStrategy;
import com.mirakl.hybris.core.product.services.OfferService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.ModifiableChecker;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;

@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class DefaultMiraklUpdateCartEntryStrategyTest {

  private static final int ENTRY_NUMBER = 1;
  private static final long REQUESTED_QUANTITY = 3L;
  private static final String OFFER_ID = "offerId";

  @InjectMocks
  private DefaultMiraklUpdateCartEntryStrategy updateStrategy;

  @Mock
  private CommonMiraklCartStrategy commonCartStrategy;

  @Mock
  private CartService cartService;

  @Mock
  private OfferService offerService;

  @Mock
  private ModelService modelService;

  @Mock
  private CommerceCartCalculationStrategy cartCalculationStrategy;

  @Mock
  private ModifiableChecker<AbstractOrderEntryModel> entryOrderChecker;

  @Mock
  private CommerceCartParameter parameter;

  @Mock
  private CartModel cart;

  @Mock
  private CartEntryModel cartEntry, otherCartEntry;

  @Mock
  private OfferModel offer;

  @Mock
  private ProductModel product;


  @Before
  public void setUp() throws Exception {
    when(parameter.getCart()).thenReturn(cart);
    when(parameter.getEntryNumber()).thenReturn((long) ENTRY_NUMBER);
    when(parameter.getQuantity()).thenReturn(REQUESTED_QUANTITY);
    when(cart.getEntries()).thenReturn(Arrays.<AbstractOrderEntryModel>asList(cartEntry, otherCartEntry));
    when(cartEntry.getEntryNumber()).thenReturn(ENTRY_NUMBER);
    when(otherCartEntry.getEntryNumber()).thenReturn(ENTRY_NUMBER + 1);
    when(cartEntry.getProduct()).thenReturn(product);
    when(cartEntry.getOfferId()).thenReturn(OFFER_ID);
    when(entryOrderChecker.canModify(cartEntry)).thenReturn(true);
    when(cartService.getEntriesForProduct(cart, product)).thenReturn(Collections.singletonList(cartEntry));
    when(offerService.getOfferForId(OFFER_ID)).thenReturn(offer);
    when(product.getMaxOrderQuantity()).thenReturn(null);
  }

  @Test
  public void shouldIncrementEntryForOfferWhenAdjustmentAllowed() throws CommerceCartModificationException {
    long actualQuantity = 1L;
    long wantedQuantity = 4L;
    long quantityToAdd = wantedQuantity - actualQuantity;
    long allowedAdjustment = quantityToAdd;
    when(cartEntry.getQuantity()).thenReturn(actualQuantity);
    when(parameter.getQuantity()).thenReturn(wantedQuantity);
    when(commonCartStrategy.getAllowedCartAdjustmentForOffer(cart, product, offer, quantityToAdd)).thenReturn(allowedAdjustment);

    updateStrategy.updateQuantityForCartEntry(parameter);

    verify(cartEntry).setQuantity(wantedQuantity);
  }

  @Test
  public void shouldDecrementEntryForOfferWhenAdjustmentAllowed() throws CommerceCartModificationException {
    long actualQuantity = 3L;
    long wantedQuantity = 2L;
    long quantityToAdd = wantedQuantity - actualQuantity;
    long allowedAdjustment = quantityToAdd;
    when(cartEntry.getQuantity()).thenReturn(actualQuantity);
    when(parameter.getQuantity()).thenReturn(wantedQuantity);
    when(commonCartStrategy.getAllowedCartAdjustmentForOffer(cart, product, offer, quantityToAdd)).thenReturn(allowedAdjustment);

    updateStrategy.updateQuantityForCartEntry(parameter);

    verify(cartEntry).setQuantity(wantedQuantity);
  }

  @Test
  public void shouldUpdateEntryForOfferWhenAdjustmentAllowedLessThanQuantity() throws CommerceCartModificationException {
    long actualQuantity = 1L;
    long wantedQuantity = 4L;
    long allowedAdjustment = 2L;
    long quantityToAdd = wantedQuantity - actualQuantity;
    when(cartEntry.getQuantity()).thenReturn(actualQuantity);
    when(parameter.getQuantity()).thenReturn(wantedQuantity);
    when(commonCartStrategy.getAllowedCartAdjustmentForOffer(cart, product, offer, quantityToAdd)).thenReturn(allowedAdjustment);

    updateStrategy.updateQuantityForCartEntry(parameter);

    verify(cartEntry).setQuantity(actualQuantity + allowedAdjustment);
  }

  @Test
  public void shouldNotUpdateEntryForOfferWhenAdjustmentNotAllowed() throws CommerceCartModificationException {
    long actualQuantity = 1L;
    long wantedQuantity = 4L;
    long allowedAdjustment = 0L;
    long quantityToAdd = wantedQuantity - actualQuantity;
    when(cartEntry.getQuantity()).thenReturn(actualQuantity);
    when(parameter.getQuantity()).thenReturn(wantedQuantity);
    when(commonCartStrategy.getAllowedCartAdjustmentForOffer(cart, product, offer, quantityToAdd)).thenReturn(allowedAdjustment);

    updateStrategy.updateQuantityForCartEntry(parameter);

    verify(cartEntry).setQuantity(actualQuantity);
  }


}
