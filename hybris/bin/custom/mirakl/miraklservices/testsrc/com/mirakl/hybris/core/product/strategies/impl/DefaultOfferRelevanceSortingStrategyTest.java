package com.mirakl.hybris.core.product.strategies.impl;

import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.strategies.OfferRelevanceSortingStrategy;
import com.mirakl.hybris.core.product.strategies.impl.DefaultOfferRelevanceSortingStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import org.apache.commons.configuration.Configuration;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.mirakl.hybris.core.constants.MiraklservicesConstants.OFFER_NEW_STATE_CODE_KEY;
import static org.apache.commons.lang.StringUtils.EMPTY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;


/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOfferRelevanceSortingStrategyTest {

  private static final int LOWEST_PRICE = 11;
  private static final int HIGHEST_PRICE = 18;
  private static final int ALMOST_LOWEST_PRICE = 16;
  private static final int ALMOST_HIGHEST_PRICE = 17;

  private static final String OFFER_STATE_NEW_CODE = "offerStateNewCode";
  private static final OfferState NEW_OFFER_STATE = OfferState.valueOf(OFFER_STATE_NEW_CODE);
  private static final OfferState NOT_NEW_OFFER_STATE = OfferState.valueOf("lower_priority_than_new");

  @InjectMocks
  OfferRelevanceSortingStrategy testObj = new DefaultOfferRelevanceSortingStrategy();

  @Mock
  private ConfigurationService configurationServiceMock;
  @Mock
  private Configuration configurationMock;

  private OfferModel offer1 = new OfferModel();
  private OfferModel offer2 = new OfferModel();
  private OfferModel offer3 = new OfferModel();
  private OfferModel offer4 = new OfferModel();

  @Before
  public void setup() {
    when(configurationServiceMock.getConfiguration()).thenReturn(configurationMock);
    when(configurationMock.getString(OFFER_NEW_STATE_CODE_KEY)).thenReturn(OFFER_STATE_NEW_CODE);

    setupOfferStates();
    setupOfferPrices();
  }

  private void setupOfferStates() {
    offer1.setState(NEW_OFFER_STATE);
    offer2.setState(NOT_NEW_OFFER_STATE);
    offer3.setState(NOT_NEW_OFFER_STATE);
    offer4.setState(NEW_OFFER_STATE);
  }

  private void setupOfferPrices() {
    offer1.setTotalPrice(BigDecimal.valueOf(ALMOST_HIGHEST_PRICE));
    offer2.setTotalPrice(BigDecimal.valueOf(LOWEST_PRICE));
    offer3.setTotalPrice(BigDecimal.valueOf(HIGHEST_PRICE));
    offer4.setTotalPrice(BigDecimal.valueOf(ALMOST_LOWEST_PRICE));
  }

  @Test
  public void sorting() {
    List<OfferModel> toBeSorted = newArrayList(offer1, offer2, offer3, offer4);

    List<OfferModel> result = testObj.sort(toBeSorted);

    assertThat(result, IsIterableContainingInOrder.contains(offer4, offer1, offer2, offer3));
  }

  @Test(expected = IllegalStateException.class)
  public void sortThrowsIllegalStateExceptionIfNoNewOfferStateCodeIsConfigured() {
    when(configurationMock.getString(OFFER_NEW_STATE_CODE_KEY)).thenReturn(EMPTY);

    testObj.sort(newArrayList(offer1, offer2, offer3, offer4));
  }
}
