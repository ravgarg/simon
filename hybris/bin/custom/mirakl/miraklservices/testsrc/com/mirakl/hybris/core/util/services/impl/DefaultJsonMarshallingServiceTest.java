package com.mirakl.hybris.core.util.services.impl;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.mirakl.client.core.internal.mapper.CustomObjectMapper;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees;

import de.hybris.bootstrap.annotations.UnitTest;
import shaded.org.codehaus.jackson.JsonParseException;
import shaded.org.codehaus.jackson.map.JsonMappingException;

@UnitTest
public class DefaultJsonMarshallingServiceTest {

  private final static String JSON_FROM_API =
      "{ \"errors\": [], \"orders\": [ { \"currency_iso_code\": \"USD\", \"leadtime_to_ship\": 0, \"offers\": [ { \"allow_quote_requests\": false, \"allowed_shipping_types\": [ { \"code\": \"STD\", \"label\": \"Standard\", \"shipping_additional_fields\": [] }, { \"code\": \"NXD\", \"label\": \"Next day\", \"shipping_additional_fields\": [] }, { \"code\": \"SMD\", \"label\": \"Same day\", \"shipping_additional_fields\": [] } ], \"line_only_shipping_price\": 5, \"line_only_total_price\": 85, \"line_original_quantity\": 1, \"line_price\": 80, \"line_quantity\": 1, \"line_shipping_price\": 5, \"line_total_price\": 85, \"offer_additional_fields\": [ { \"code\": \"temp2\", \"type\": \"STRING\", \"value\": \"TEmp\" } ], \"offer_discount\": null, \"offer_id\": 2003, \"offer_price\": 80, \"offer_quantity\": 37, \"product_category_code\": \"576\", \"promotions\": [], \"shipping_price_additional_unit\": 2, \"shipping_price_unit\": 5 } ], \"promotions\": { \"applied_promotions\": [], \"total_deduced_amount\": 0 }, \"selected_shipping_type\": { \"code\": \"STD\", \"label\": \"Standard\", \"shipping_additional_fields\": [] }, \"shipping_types\": [ { \"code\": \"STD\", \"label\": \"Standard\", \"shipping_additional_fields\": [], \"total_shipping_price\": 5 }, { \"code\": \"NXD\", \"label\": \"Next day\", \"shipping_additional_fields\": [], \"total_shipping_price\": 8 }, { \"code\": \"SMD\", \"label\": \"Same day\", \"shipping_additional_fields\": [], \"total_shipping_price\": 15 } ], \"shop_id\": 2000, \"shop_name\": \"CAMERA Pro\" } ], \"total_count\": 1 }";

  private DefaultJsonMarshallingService jsonMarshallingService = new DefaultJsonMarshallingService();

  @Before
  public void setUp() {
    jsonMarshallingService.setMapper(new CustomObjectMapper());
  }

  @Test
  public void shouldHandleDeserializeJson() throws JsonParseException, JsonMappingException, IOException {

    MiraklOrderShippingFees shippingFees = jsonMarshallingService.fromJson(JSON_FROM_API, MiraklOrderShippingFees.class);

    assertThat(shippingFees).isNotNull();
    assertThat(shippingFees.getOrders()).isNotEmpty();
    assertThat(shippingFees.getOrders().get(0).getOffers()).isNotEmpty();
    assertThat(shippingFees.getOrders().get(0).getOffers().get(0).getOfferAdditionalFields()).isNotEmpty();
  }

  @Test
  public void shouldHandleNullDeserialization() throws JsonParseException, JsonMappingException, IOException {
    MiraklOrderShippingFees shippingFees = jsonMarshallingService.fromJson(null, MiraklOrderShippingFees.class);

    assertThat(shippingFees).isNull();
  }

  @Test
  public void shouldSerializeToJson() throws JsonParseException, JsonMappingException, IOException {
    MiraklOrderShippingFees shippingFees = jsonMarshallingService.fromJson(JSON_FROM_API, MiraklOrderShippingFees.class);
    String valueAsString = jsonMarshallingService.toJson(shippingFees, MiraklOrderShippingFees.class);
    shippingFees = jsonMarshallingService.fromJson(valueAsString, MiraklOrderShippingFees.class);

    assertThat(shippingFees).isNotNull();
    assertThat(shippingFees.getOrders()).isNotEmpty();
    assertThat(shippingFees.getOrders().get(0).getOffers()).isNotEmpty();
    assertThat(shippingFees.getOrders().get(0).getOffers().get(0).getOfferAdditionalFields()).isNotEmpty();
  }
}
