package com.mirakl.hybris.addon.controllers.cms;

import com.mirakl.hybris.beans.OfferData;
import de.hybris.bootstrap.annotations.UnitTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import java.util.Collections;

import static com.mirakl.hybris.addon.controllers.cms.CMSBuyBoxComponentController.TOP_OFFER_ATTRIBUTE;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.verify;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSBuyBoxComponentControllerTest {

  private CMSBuyBoxComponentController testObj = new CMSBuyBoxComponentController();

  @Mock
  private Model modelMock;
  @Mock
  private OfferData offerDataMock;

  @Test
  public void fillsModelWithTopOffer() {
    testObj.fillModel(modelMock, singletonList(offerDataMock));

    verify(modelMock).addAttribute(TOP_OFFER_ATTRIBUTE, offerDataMock);
  }

  @Test
  public void fillModelSetsNullAsTopOfferIfNoOffersFound() {
    testObj.fillModel(modelMock, Collections.<OfferData>emptyList());

    verify(modelMock).addAttribute(TOP_OFFER_ATTRIBUTE, null);
  }

}
