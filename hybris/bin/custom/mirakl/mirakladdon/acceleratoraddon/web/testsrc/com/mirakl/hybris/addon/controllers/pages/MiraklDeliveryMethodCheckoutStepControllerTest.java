package com.mirakl.hybris.addon.controllers.pages;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.hybris.addon.controllers.MirakladdonControllerConstants;
import com.mirakl.hybris.addon.forms.UpdateShippingOptionForm;
import com.mirakl.hybris.facades.order.ShippingFacade;
import com.mirakl.hybris.facades.shipping.data.ShippingOfferDiscrepancyData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklDeliveryMethodCheckoutStepControllerTest {

  private static final String SHIPPING_OPTION_CODE = "shippingOptionCode";
  private static final String SHOP_ID = "shopId";
  private static final String NEXT_CHECKOUT_STEP = "nextCheckoutStep";
  private static final String REDIRECT_TO_DELIVERY_ADDRESS = "redirectToDeliveryAddress";
  private static final String REDIRECT_TO_DELIVERY_METHOD = "redirectToDeliveryMethod";
  private static final int LEAD_TIME_TO_SHIP = 1;

  @Spy
  @InjectMocks
  private MiraklDeliveryMethodCheckoutStepController testObj = new MiraklDeliveryMethodCheckoutStepController();

  @Mock
  private ShippingFacade shippingFacadeMock;
  @Mock
  private CartFacade cartFacadeMock;
  @Mock
  private AcceleratorCheckoutFacade checkoutFacadeMock;
  @Mock
  private CheckoutStep checkoutStepMock;

  @Mock
  private Model modelMock;
  @Mock
  private RedirectAttributes redirectAttributesMock;
  @Mock
  private ShippingOfferDiscrepancyData shippingOfferDiscrepancyDataMock;
  @Mock
  private UpdateShippingOptionForm updateShippingOptionFormMock;
  @Mock
  private BindingResult bindingResultMock;

  @Before
  public void setUp() throws CMSItemNotFoundException {
    testObj.setRedirectToDeliveryAddress(REDIRECT_TO_DELIVERY_ADDRESS);
    testObj.setRedirectToDeliveryMethod(REDIRECT_TO_DELIVERY_METHOD);
    doNothing().when(testObj).enterStepInternal(modelMock, redirectAttributesMock);
    doNothing().when(testObj).preparePage(modelMock);
    doReturn(checkoutStepMock).when(testObj).getCheckoutStep();

    when(shippingFacadeMock.getOfferDiscrepancies()).thenReturn(singletonList(shippingOfferDiscrepancyDataMock));
    when(updateShippingOptionFormMock.getShopId()).thenReturn(SHOP_ID);
    when(updateShippingOptionFormMock.getLeadTimeToShip()).thenReturn(LEAD_TIME_TO_SHIP);
    when(checkoutStepMock.nextStep()).thenReturn(NEXT_CHECKOUT_STEP);
  }

  @Test
  public void entersStep() throws CMSItemNotFoundException {
    String result = testObj.enterStep(modelMock, redirectAttributesMock);

    assertThat(result).isSameAs(MirakladdonControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryMethodPage);

    verify(shippingFacadeMock).setAvailableShippingOptions();
    verify(modelMock).addAttribute("offerErrors", singletonList(shippingOfferDiscrepancyDataMock));
    verify(testObj).enterStepInternal(modelMock, redirectAttributesMock);
  }

  @Test
  public void redirectsToDeliveryAddressPageIfMiraklApiExceptionIsThrown() throws CMSItemNotFoundException {
    doThrow(MiraklApiException.class).when(shippingFacadeMock).setAvailableShippingOptions();

    String result = testObj.enterStep(modelMock, redirectAttributesMock);

    assertThat(result).isSameAs(REDIRECT_TO_DELIVERY_ADDRESS);

    verify(shippingFacadeMock).setAvailableShippingOptions();
    verify(modelMock, never()).addAttribute(anyString(), anyListOf(ShippingOfferDiscrepancyData.class));
    verify(redirectAttributesMock).addFlashAttribute(eq(GlobalMessages.ERROR_MESSAGES_HOLDER), anyListOf(String.class));
    verify(testObj, never()).enterStepInternal(any(Model.class), any(RedirectAttributes.class));
  }

  @Test
  public void updatesCartShopShippingOptions() throws CMSItemNotFoundException {
    String result =
        testObj.updateCartShippingOptions(SHIPPING_OPTION_CODE, modelMock, updateShippingOptionFormMock, bindingResultMock);

    assertThat(result).isSameAs(REDIRECT_TO_DELIVERY_METHOD);

    verify(shippingFacadeMock).updateShippingOptions(SHIPPING_OPTION_CODE, LEAD_TIME_TO_SHIP, SHOP_ID);
    verify(checkoutFacadeMock, never()).setDeliveryMode(anyString());
    verify(redirectAttributesMock, never()).addFlashAttribute(anyString(), anyListOf(String.class));
  }

  @Test
  public void updatesCartOperatorShippingOptions() throws CMSItemNotFoundException {
    when(updateShippingOptionFormMock.getShopId()).thenReturn(null);

    String result =
        testObj.updateCartShippingOptions(SHIPPING_OPTION_CODE, modelMock, updateShippingOptionFormMock, bindingResultMock);

    assertThat(result).isSameAs(REDIRECT_TO_DELIVERY_METHOD);

    verify(shippingFacadeMock, never()).updateShippingOptions(anyString(), anyInt(), anyString());
    verify(checkoutFacadeMock).setDeliveryMode(SHIPPING_OPTION_CODE);
    verify(redirectAttributesMock, never()).addFlashAttribute(anyString(), anyListOf(String.class));
  }

  @Test
  public void updateCartShippingOptionsReturnsToPageWithoutSettingAnyShippingOptionsIfFormContainsErrors()
      throws CMSItemNotFoundException {
    when(bindingResultMock.hasErrors()).thenReturn(true);

    String result =
        testObj.updateCartShippingOptions(SHIPPING_OPTION_CODE, modelMock, updateShippingOptionFormMock, bindingResultMock);

    assertThat(result).isSameAs(MirakladdonControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryMethodPage);

    verify(shippingFacadeMock, never()).updateShippingOptions(anyString(), anyInt(), anyString());
    verify(checkoutFacadeMock, never()).setDeliveryMode(anyString());
  }

  @Test
  public void entersNextStep() {
    String result = testObj.next(redirectAttributesMock);

    assertThat(result).isSameAs(NEXT_CHECKOUT_STEP);

    verify(shippingFacadeMock).removeInvalidOffers();
    verify(shippingFacadeMock).setAvailableShippingOptions();
  }

  @Test
  public void nextRedirectsToSelectDeliveryMethodPageIfMiraklApiExceptionIsThrown() {
    doThrow(MiraklApiException.class).when(shippingFacadeMock).setAvailableShippingOptions();

    String result = testObj.next(redirectAttributesMock);

    assertThat(result).isSameAs(REDIRECT_TO_DELIVERY_METHOD);

    verify(shippingFacadeMock).removeInvalidOffers();
    verify(redirectAttributesMock).addFlashAttribute(eq(GlobalMessages.ERROR_MESSAGES_HOLDER), anyListOf(String.class));
  }
}
