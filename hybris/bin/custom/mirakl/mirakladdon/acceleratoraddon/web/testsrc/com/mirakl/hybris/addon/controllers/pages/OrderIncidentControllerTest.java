package com.mirakl.hybris.addon.controllers.pages;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;

import com.mirakl.hybris.facades.order.MarketplaceConsignmentFacade;
import org.fest.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.hybris.addon.controllers.MirakladdonControllerConstants;
import com.mirakl.hybris.addon.forms.IncidentForm;
import com.mirakl.hybris.beans.ReasonData;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.mirakl.hybris.facades.order.IncidentFacade;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderIncidentControllerTest {

  private static final String ORDER_CODE = "1234a7984-45687e6541-12354987";
  private static final String CONSIGNMENT_CODE = "1234a7984-45687e6541-12354987-A";
  private static final String CONSIGNMENT_ENTRY_CODE = "1234a7984-45687e6541-12354987-A-1";
  private static final String REASON_CODE = "5";

  @Mock
  private UserService userService;

  @Mock
  private IncidentFacade incidentFacade;

  @Mock
  private MarketplaceConsignmentService marketplaceConsignmentService;

  @Mock
  private ReasonData incidentCloseReason1, incidentCloseReason2, incidentOpenReason1, incidentOpenReason2;

  @Mock
  private ProductData product;

  @Mock
  private Model model;

  @Mock
  private RedirectAttributes redirect;

  @Mock
  private IncidentForm incidentForm;

  @Mock
  private UserModel user;

  @Mock
  private MarketplaceConsignmentModel marketplaceConsignment;

  @Mock
  private OrderModel order;

  @Mock
  private MarketplaceConsignmentFacade marketplaceConsignmentFacade;

  @InjectMocks
  private OrderIncidentController testObj;

  @Before
  public void setUp() {
    when(incidentFacade.getReasons(MiraklReasonType.INCIDENT_CLOSE)).thenReturn(Collections.list(incidentCloseReason1, incidentCloseReason2));
    when(incidentFacade.getReasons(MiraklReasonType.INCIDENT_OPEN)).thenReturn(Collections.list(incidentOpenReason1, incidentOpenReason2));
    when(marketplaceConsignmentFacade.getProductForConsignmentEntry(CONSIGNMENT_ENTRY_CODE)).thenReturn(product);
    when(marketplaceConsignmentService.getMarketplaceConsignmentForCode(CONSIGNMENT_CODE))
        .thenReturn(marketplaceConsignment);
    when(marketplaceConsignment.getOrder()).thenReturn(order);
    when(order.getCode()).thenReturn(ORDER_CODE);
    when(incidentForm.getReasonCode()).thenReturn(REASON_CODE);
  }

  @Test
  public void openIncidentPage() throws UnsupportedEncodingException, CMSItemNotFoundException {
    String output = testObj.openIncidentPage(CONSIGNMENT_ENTRY_CODE, model, null, null);

    verify(model).addAttribute(eq("reasons"), eq(Collections.list(incidentOpenReason1, incidentOpenReason2)));
    verify(model).addAttribute(eq("product"), eq(product));

    assertThat(output).isEqualTo(MirakladdonControllerConstants.Fragments.Order.orderIncidentPopup);
  }

  @Test
  public void closeIncidentPage() throws UnsupportedEncodingException, CMSItemNotFoundException {
    String output = testObj.closeIncidentPage(CONSIGNMENT_ENTRY_CODE, model, null, null);

    verify(model).addAttribute(eq("reasons"), eq(Collections.list(incidentCloseReason1, incidentCloseReason2)));
    verify(model).addAttribute(eq("product"), eq(product));

    assertThat(output).isEqualTo(MirakladdonControllerConstants.Fragments.Order.orderIncidentPopup);
  }

  @Test
  public void postOpenIncident() {
    String output = testObj.postOpenIncident(CONSIGNMENT_CODE, CONSIGNMENT_ENTRY_CODE, model, null, incidentForm, null, redirect);

    verify(marketplaceConsignment).getOrder();
    verify(incidentFacade).openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);
    assertThat(output).isEqualTo(OrderIncidentController.REDIRECT_TO_ORDER_DETAIL_PAGE + ORDER_CODE);
  }

  @Test
  public void postOpenIncidentWhenException() {
    doThrow(new IllegalStateException()).when(incidentFacade).openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    String output = testObj.postOpenIncident(CONSIGNMENT_CODE, CONSIGNMENT_ENTRY_CODE, model, null, incidentForm, null, redirect);
    verify(marketplaceConsignment).getOrder();
    assertThat(output).isEqualTo(OrderIncidentController.REDIRECT_TO_ORDER_DETAIL_PAGE + ORDER_CODE);
  }

  @Test
  public void postCloseIncident() {
    String output = testObj.postCloseIncident(CONSIGNMENT_CODE, CONSIGNMENT_ENTRY_CODE, model, null, incidentForm, null, redirect);

    verify(marketplaceConsignment).getOrder();
    verify(incidentFacade).closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);
    assertThat(output).isEqualTo(OrderIncidentController.REDIRECT_TO_ORDER_DETAIL_PAGE + ORDER_CODE);
  }

  @Test
  public void postCloseIncidentWhenException() {
    doThrow(new IllegalStateException()).when(incidentFacade).closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    String output = testObj.postCloseIncident(CONSIGNMENT_CODE, CONSIGNMENT_ENTRY_CODE, model, null, incidentForm, null, redirect);

    verify(marketplaceConsignment).getOrder();
    assertThat(output).isEqualTo(OrderIncidentController.REDIRECT_TO_ORDER_DETAIL_PAGE + ORDER_CODE);
  }
}
