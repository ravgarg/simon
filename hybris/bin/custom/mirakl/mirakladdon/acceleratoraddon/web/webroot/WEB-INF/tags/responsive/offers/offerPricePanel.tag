<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="offer" required="true" type="com.mirakl.hybris.beans.OfferData" %>

<div class="row">
    <div class="col-sm-4 price">
        <format:fromPrice priceData="${offer.price}"/>
    </div>
    <c:if test="${offer.price.value != offer.originPrice.value}">
        <div class="col-sm-4 price-origin col-sm-pull-1">
            <format:fromPrice priceData="${offer.originPrice}"/>
        </div>
    </c:if>
    <div class="col-sm-4 price-description col-sm-pull-2">
        ${offer.priceAdditionalInfo}
    </div>
    <div class="col-sm-12">
        +<b><format:fromPrice priceData="${offer.minShippingPrice}"/></b>&nbsp;<spring:theme code="product.offer.paid.shipping"/>
    </div>
</div>
