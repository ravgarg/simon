<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="offers" tagdir="/WEB-INF/tags/addons/mirakladdon/responsive/offers" %>


<c:set var="maxQty" value="${topOffer.quantity}"/>

<div class="addtocart-component">
    <c:if test="${empty showAddToCart ? true : showAddToCart}">
        <div class="qty-selector input-group js-qty-selector">
            <span class="input-group-btn">
                <button class="btn btn-default js-qty-selector-minus" type="button"><span
                        class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
            </span>
            <input type="text" maxlength="3" class="form-control js-qty-selector-input" size="1" value='1'
                   data-max="${maxQty}" data-min="1" name="pdpAddtoCartInput" id="pdpAddtoCartInput"/>
            <span class="input-group-btn">
                <button class="btn btn-default js-qty-selector-plus" type="button"><span
                        class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            </span>
        </div>
    </c:if>
    <c:if test="${topOffer.quantity gt 0}">
        <c:set var="offerQuantity">${topOffer.quantity}&nbsp;
            <spring:theme code="product.variants.in.stock"/>
        </c:set>
    </c:if>
    <div class="stock-wrapper clearfix">
        <c:if test="${not empty topOffer.state}">
            ${topOffer.state}&nbsp;-&nbsp;
        </c:if>${offerQuantity}
    </div>
    <spring:theme code="checkout.summary.deliveryMode.soldBy" text="Sold by"/>&nbsp;
    <offers:offerRating offer="${topOffer}"/>
    <div class="actions">
        <action:actions element="div" parentComponent="${component}"/>
    </div>
</div>
