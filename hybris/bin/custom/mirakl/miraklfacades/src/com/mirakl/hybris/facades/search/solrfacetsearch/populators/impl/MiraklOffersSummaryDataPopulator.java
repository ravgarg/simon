package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.collect.ImmutableList;
import com.mirakl.hybris.beans.OfferOverviewData;
import com.mirakl.hybris.beans.OfferStateSummaryData;
import com.mirakl.hybris.beans.OffersSummaryData;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.facades.product.helpers.PriceDataFactoryHelper;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class MiraklOffersSummaryDataPopulator implements Populator<List<OfferModel>, OffersSummaryData> {

  protected Converter<OfferModel, OfferOverviewData> offerOverviewConverter;

  protected PriceDataFactoryHelper priceDataFactoryHelper;

  @Override
  public void populate(List<OfferModel> source, OffersSummaryData target) throws ConversionException {
    validateParameterNotNullStandardMessage("source", source);

    if (CollectionUtils.isEmpty(source)) {
      return;
    }
    target.setBestOffer(offerOverviewConverter.convert(source.get(0)));
    target.setOfferCount(source.size());
    target.setStates(buildOfferStatesSummary(source, target));
  }

  protected List<OfferStateSummaryData> buildOfferStatesSummary(List<OfferModel> source, OffersSummaryData target) {
    Map<OfferState, OfferStateSummaryData> summaries = new HashMap<>();

    for (OfferModel offer : source) {
      if (!summaries.containsKey(offer.getState())) {
        addOfferStateSummary(offer, summaries);
      } else {
        updateOfferStateSummary(offer, summaries);
      }
    }

    return ImmutableList.copyOf(summaries.values());
  }

  protected void addOfferStateSummary(OfferModel offer, Map<OfferState, OfferStateSummaryData> summaries) {
    OfferStateSummaryData summary = new OfferStateSummaryData();
    summary.setStateCode(offer.getState().getCode());
    summary.setOfferCount(1);
    summary.setMinPrice(priceDataFactoryHelper.createPrice(offer.getPrice()));

    summaries.put(offer.getState(), summary);
  }

  protected void updateOfferStateSummary(OfferModel offer, Map<OfferState, OfferStateSummaryData> summaries) {
    OfferStateSummaryData summary = summaries.get(offer.getState());
    summary.setOfferCount(summary.getOfferCount() + 1);
    BigDecimal offerPrice = offer.getPrice();
    if (offerPrice.doubleValue() < summary.getMinPrice().getValue().doubleValue()) {
      summary.setMinPrice(priceDataFactoryHelper.createPrice(offerPrice));
    }

    summaries.put(offer.getState(), summary);
  }

  @Required
  public void setOfferOverviewConverter(Converter<OfferModel, OfferOverviewData> offerOverviewConverter) {
    this.offerOverviewConverter = offerOverviewConverter;
  }

  @Required
  public void setPriceDataFactoryHelper(PriceDataFactoryHelper priceDataFactoryHelper) {
    this.priceDataFactoryHelper = priceDataFactoryHelper;
  }


}
