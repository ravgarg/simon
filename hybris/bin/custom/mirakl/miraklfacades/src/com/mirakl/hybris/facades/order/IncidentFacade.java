package com.mirakl.hybris.facades.order;

import java.util.List;

import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.hybris.beans.ReasonData;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public interface IncidentFacade {

  /**
   * Get the reasons with the given type as defined in Mirakl
   *
   * @param wantedType the type of reason desired (ex: INCIDENT_OPEN, MESSAGING, ...)
   * @return a list of Reasons
   */
  List<ReasonData> getReasons(MiraklReasonType wantedType);

  /**
   * Open an incident for the designated Consignment Entry Code
   *  @param consignmentEntryCode The code of the consignment entry (= Mirakl Order Entry id)
   * @param reasonCode The reason code of the incident
   */
  void openIncident(String consignmentEntryCode, String reasonCode);

  /**
   * Close an incident for the designated Consignment Entry Code
   *  @param consignmentEntryCode The code of the consignment entry (= Mirakl Order Entry id)
   * @param reasonCode The reason code of the incident
   */
  void closeIncident(String consignmentEntryCode, String reasonCode);
}
