package com.mirakl.hybris.facades.product.converters.populator;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Date;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class OfferDataPopulator implements Populator<OfferModel, OfferData> {

  protected EnumerationService enumerationService;

  protected PriceDataFactory priceDataFactory;

  protected OfferCodeGenerationStrategy offerCodeGenerationStrategy;

  @Override
  public void populate(OfferModel offerModel, OfferData offerData) throws ConversionException {

    if (offerModel.getDiscountPrice() != null) {
      offerData
          .setDiscountPrice(priceDataFactory.create(PriceDataType.BUY, offerModel.getDiscountPrice(), offerModel.getCurrency()));
    }
    if (offerModel.getMinShippingPrice() != null) {
      offerData.setMinShippingPrice(
          priceDataFactory.create(PriceDataType.BUY, offerModel.getMinShippingPrice(), offerModel.getCurrency()));
    }
    if (offerModel.getMinShippingPriceAdditional() != null) {
      offerData.setMinShippingPriceAdditional(
          priceDataFactory.create(PriceDataType.BUY, offerModel.getMinShippingPriceAdditional(), offerModel.getCurrency()));
    }
    if (offerModel.getOriginPrice() != null) {
      offerData.setOriginPrice(priceDataFactory.create(PriceDataType.BUY, offerModel.getOriginPrice(), offerModel.getCurrency()));
    }
    offerData.setPrice(priceDataFactory.create(PriceDataType.BUY, offerModel.getPrice(), offerModel.getCurrency()));
    offerData.setTotalPrice(priceDataFactory.create(PriceDataType.BUY, offerModel.getTotalPrice(), offerModel.getCurrency()));
    offerData.setAvailableEndDate(offerModel.getAvailableEndDate());
    offerData.setAvailableStartDate(offerModel.getAvailableStartDate());
    offerData.setCode(offerCodeGenerationStrategy.generateCode(offerModel.getId()));
    offerData.setDescription(offerModel.getDescription());
    offerData.setDiscountEndDate(offerModel.getDiscountEndDate());
    offerData.setDiscountStartDate(offerModel.getDiscountStartDate());
    offerData.setId(offerModel.getId());
    offerData.setLeadTimeToShip(offerModel.getLeadTimeToShip());
    if(isInDiscount(offerModel)) {
      offerData.setPriceAdditionalInfo(offerModel.getPriceAdditionalInfo());
    }
    offerData.setProductCode(offerModel.getProductCode());
    offerData.setQuantity(offerModel.getQuantity());
    offerData.setShopCode(offerModel.getShop().getId());
    offerData.setShopName(offerModel.getShop().getName());
    offerData.setShopGrade(offerModel.getShop().getGrade());
    offerData.setShopEvaluationCount(offerModel.getShop().getEvaluationCount());
    offerData.setState(enumerationService.getEnumerationName(offerModel.getState()));
  }
  
  protected boolean isInDiscount(OfferModel offer) {
    Date currentDate = new Date();
    return (offer.getDiscountStartDate() == null || currentDate.after(offer.getDiscountStartDate()))
        && (offer.getDiscountEndDate() == null || currentDate.before(offer.getDiscountEndDate()));
  }

  @Required
  public void setEnumerationService(EnumerationService enumerationService) {
    this.enumerationService = enumerationService;
  }

  @Required
  public void setPriceDataFactory(PriceDataFactory priceDataFactory) {
    this.priceDataFactory = priceDataFactory;
  }

  @Required
  public void setOfferCodeGenerationStrategy(OfferCodeGenerationStrategy offerCodeGenerationStrategy) {
    this.offerCodeGenerationStrategy = offerCodeGenerationStrategy;
  }
}
