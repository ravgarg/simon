package com.mirakl.hybris.facades.order.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.hybris.beans.ReasonData;
import com.mirakl.hybris.core.order.services.IncidentService;
import com.mirakl.hybris.facades.order.IncidentFacade;

import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public class DefaultIncidentFacade implements IncidentFacade {

  protected Converter<MiraklReason, ReasonData> reasonDataConverter;
  protected IncidentService incidentService;

  @Override
  public List<ReasonData> getReasons(MiraklReasonType wantedType) {
    List<MiraklReason> reasons = incidentService.getReasons();
    return reasonDataConverter.convertAll(Collections2.filter(reasons, isWantedType(wantedType)));
  }

  @Override
  public void openIncident(String consignmentEntryCode, String reasonCode) {
    incidentService.openIncident(consignmentEntryCode, reasonCode);
  }

  @Override
  public void closeIncident(String consignmentEntryCode, String reasonCode) {
    incidentService.closeIncident(consignmentEntryCode, reasonCode);
  }

  protected Predicate<MiraklReason> isWantedType(final MiraklReasonType type) {
    return new Predicate<MiraklReason>() {
      @Override
      public boolean apply(MiraklReason reason) {
        return (reason.getType().equals(type) && reason.isCustomerRight());
      }
    };
  }

  @Required
  public void setReasonDataConverter(Converter<MiraklReason, ReasonData> reasonDataConverter) {
    this.reasonDataConverter = reasonDataConverter;
  }

  @Required
  public void setIncidentService(IncidentService incidentService) {
    this.incidentService = incidentService;
  }
}
