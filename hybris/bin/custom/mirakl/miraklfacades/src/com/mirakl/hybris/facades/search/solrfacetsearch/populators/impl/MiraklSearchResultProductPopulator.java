package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static org.apache.commons.lang.StringUtils.isNotEmpty;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.OfferStateSummaryData;
import com.mirakl.hybris.beans.OffersSummaryData;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class MiraklSearchResultProductPopulator extends SearchResultProductPopulator {

  protected JsonMarshallingService jsonMarshallingService;

  protected EnumerationService enumerationService;

  @Override
  public void populate(SearchResultValueData source, ProductData target) throws ConversionException {
    String offersSummaryJson = this.<String>getValue(source, "offersSummary");

    if (isNotEmpty(offersSummaryJson)) {
      OffersSummaryData offersSummary = jsonMarshallingService.fromJson(offersSummaryJson, OffersSummaryData.class);
      populateOfferStateLabels(offersSummary);
      target.setOffersSummary(offersSummary);
    }
  }

  protected void populateOfferStateLabels(OffersSummaryData offersSummary) {
    for (OfferStateSummaryData offerStateSummaryData : offersSummary.getStates()) {
      OfferState offerState = enumerationService.getEnumerationValue(OfferState.class, offerStateSummaryData.getStateCode());
      offerStateSummaryData.setStateLabel(enumerationService.getEnumerationName(offerState));
    }

  }

  @Required
  public void setJsonMarshallingService(JsonMarshallingService jsonMarshallingService) {
    this.jsonMarshallingService = jsonMarshallingService;
  }

  @Required
  public void setEnumerationService(EnumerationService enumerationService) {
    this.enumerationService = enumerationService;
  }

}
