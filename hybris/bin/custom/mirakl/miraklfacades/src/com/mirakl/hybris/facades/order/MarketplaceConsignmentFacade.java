package com.mirakl.hybris.facades.order;

import com.mirakl.hybris.beans.EvaluationData;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.user.UserModel;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
public interface MarketplaceConsignmentFacade {

  /**
   * Allows to post an evaluation of a consignment to Mirakl
   *
   * @param consignmentCode The code of the consignment
   * @param evaluationData The filled EvaluationData
   * @param user The logged user
   * @return the evaluationData that was sent
   */
  EvaluationData postEvaluation(String consignmentCode, EvaluationData evaluationData, UserModel user);

  /**
   * Returns the product from the consignment entry
   *
   * @param consignmentEntryCode The code of the consignment Entry
   * @return The product data
   */
  ProductData getProductForConsignmentEntry(String consignmentEntryCode);
}
