package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.OfferOverviewData;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.facades.product.helpers.PriceDataFactoryHelper;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class MiraklOfferOverviewDataPopulator implements Populator<OfferModel, OfferOverviewData> {

  protected OfferCodeGenerationStrategy offerCodeGenerationStrategy;

  protected PriceDataFactoryHelper priceDataFactoryHelper;

  @Override
  public void populate(OfferModel source, OfferOverviewData target) throws ConversionException {
    validateParameterNotNullStandardMessage("source", source);

    target.setCode(offerCodeGenerationStrategy.generateCode(source.getId()));
    target.setQuantity(source.getQuantity());
    target.setShopId(source.getShop().getId());
    target.setShopGrade(source.getShop().getGrade());
    target.setShopName(source.getShop().getName());
    populatePrices(source, target);
  }

  protected void populatePrices(OfferModel source, OfferOverviewData target) {
    if (source.getPrice() != null) {
      target.setPrice(priceDataFactoryHelper.createPrice(source.getPrice()));
    }
    if (source.getOriginPrice() != null) {
      target.setOriginPrice(priceDataFactoryHelper.createPrice(source.getOriginPrice()));
    }
    if (source.getMinShippingPrice() != null) {
      target.setMinShippingPrice(priceDataFactoryHelper.createPrice(source.getMinShippingPrice()));
    }
  }

  @Required
  public void setOfferCodeGenerationStrategy(OfferCodeGenerationStrategy offerCodeGenerationStrategy) {
    this.offerCodeGenerationStrategy = offerCodeGenerationStrategy;
  }

  @Required
  public void setPriceDataFactoryHelper(PriceDataFactoryHelper priceDataFactoryHelper) {
    this.priceDataFactoryHelper = priceDataFactoryHelper;
  }

}
