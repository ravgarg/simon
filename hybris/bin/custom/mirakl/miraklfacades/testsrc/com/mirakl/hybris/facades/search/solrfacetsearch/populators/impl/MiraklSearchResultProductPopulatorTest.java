package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.OfferStateSummaryData;
import com.mirakl.hybris.beans.OffersSummaryData;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.enumeration.EnumerationService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklSearchResultProductPopulatorTest {

  private static final String OFFER_STATE_LABEL = "offer-state-label";
  private static final String JSON_CONTENT = "json-string";
  private static final String OFFER_STATE_CODE = "offer-state-code";

  @InjectMocks
  private MiraklSearchResultProductPopulator populator;

  @Mock
  private JsonMarshallingService jsonMarshallingService;
  @Mock
  private EnumerationService enumerationService;
  @Mock
  private SearchResultValueData searchResultValueData;
  @Mock
  private OffersSummaryData offersSummaryData;
  @Mock
  private OfferStateSummaryData offerStateSummaryData;
  @Mock
  private OfferState offerState;

  @Before
  public void setUp() throws Exception {
    when(jsonMarshallingService.fromJson(JSON_CONTENT, OffersSummaryData.class)).thenReturn(offersSummaryData);
    when(searchResultValueData.getValues()).thenReturn(Collections.<String, Object>singletonMap("offersSummary", JSON_CONTENT));
    when(offerStateSummaryData.getStateCode()).thenReturn(OFFER_STATE_CODE);
    when(enumerationService.getEnumerationValue(OfferState.class, OFFER_STATE_CODE)).thenReturn(offerState);
    when(enumerationService.getEnumerationName(offerState)).thenReturn(OFFER_STATE_LABEL);
  }

  @Test
  public void shouldPopulateOffersSummary() {
    ProductData result = new ProductData();
    populator.populate(searchResultValueData, result);

    assertThat(result.getOffersSummary()).isEqualTo(offersSummaryData);
  }

  @Test
  public void shouldPopulateOfferStatesLabels() {
    when(offersSummaryData.getStates()).thenReturn(singletonList(offerStateSummaryData));
    when(offerStateSummaryData.getStateCode()).thenReturn(OFFER_STATE_CODE);

    ProductData result = new ProductData();
    populator.populate(searchResultValueData, result);

    verify(offerStateSummaryData).setStateLabel(OFFER_STATE_LABEL);
  }
}
