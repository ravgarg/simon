package com.mirakl.hybris.facades.order.impl;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.List;

import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import org.fest.util.Collections;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Collections2;
import com.mirakl.client.mmp.domain.reason.MiraklReason;
import com.mirakl.client.mmp.domain.reason.MiraklReasonType;
import com.mirakl.hybris.beans.ReasonData;
import com.mirakl.hybris.core.order.services.IncidentService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultIncidentFacadeTest {

  private static final MiraklReasonType INCIDENT_OPEN_REASON_TYPE = MiraklReasonType.INCIDENT_OPEN;
  private static final String CONSIGNMENT_ENTRY_CODE = "1234a7984-45687e6541-12354987-A-1";
  private static final String REASON_CODE = "5";

  @Mock
  private Converter<MiraklReason, ReasonData> reasonDataConverter;

  @Mock
  private IncidentService incidentService;

  @Mock
  private MiraklReason openIncidentReason, closeIncidentReason;

  @Mock
  private ReasonData openIncidentReasonData;

  @Mock
  private UserModel user;

  @Mock
  private ProductModel product;

  @InjectMocks
  private DefaultIncidentFacade testObj;

  @Before
  public void setUp() {
    when(incidentService.getReasons()).thenReturn(Collections.list(openIncidentReason, closeIncidentReason));
    when(reasonDataConverter.convertAll(anyListOf(MiraklReason.class))).thenReturn(singletonList(openIncidentReasonData));
    when(openIncidentReason.getType()).thenReturn(MiraklReasonType.INCIDENT_OPEN);
    when(closeIncidentReason.getType()).thenReturn(MiraklReasonType.INCIDENT_CLOSE);
    when(closeIncidentReason.isCustomerRight()).thenReturn(true);
    when(openIncidentReason.isCustomerRight()).thenReturn(true);
  }

  @Test
  public void getReasons() {
    List<ReasonData> reasons = testObj.getReasons(INCIDENT_OPEN_REASON_TYPE);

    verify(incidentService).getReasons();
    verify(reasonDataConverter).convertAll(Matchers.anyListOf(MiraklReason.class));
    assertThat(reasons).containsExactly(openIncidentReasonData);
  }

  @Test
  public void openIncident() {
    testObj.openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(incidentService).openIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);
  }

  @Test
  public void closeIncident() {
    testObj.closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);

    verify(incidentService).closeIncident(CONSIGNMENT_ENTRY_CODE, REASON_CODE);
  }

  @Test
  public void isWantedTypeWithDifferentTypes() {
    Collection<MiraklReason> output = Collections2.filter(Collections.list(openIncidentReason, closeIncidentReason),
        testObj.isWantedType(MiraklReasonType.INCIDENT_OPEN));

    assertThat(output).containsOnly(openIncidentReason);
  }

  @Test
  public void isWantedTypeWithDifferentRights() {
    when(openIncidentReason.isCustomerRight()).thenReturn(false);

    Collection<MiraklReason> output = Collections2.filter(Collections.list(openIncidentReason, closeIncidentReason),
        testObj.isWantedType(MiraklReasonType.INCIDENT_OPEN));

    assertThat(output).isEmpty();
  }

}
