package com.mirakl.hybris.facades.product.converters.populator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.storesession.data.CurrencyData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.enumeration.EnumerationService;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OfferDataPopulatorTest {

  private static final String OFFER_CODE = "offer-code";
  private static final String OFFER_STATE = "offerState";
  private static final Date OFFER_AVAILABLE_END_DATE = new Date();
  private static final Date OFFER_AVAILABLE_START_DATE = new Date();
  private static final String OFFER_DESCRIPTION = "Offer description";
  private static final BigDecimal OFFER_DISCOUNT_PRICE = BigDecimal.valueOf(75);
  private static final Date OFFER_DISCOUNT_START_DATE = new DateTime().minusDays(100).toDate();
  private static final Date OFFER_DISCOUNT_END_DATE = new DateTime().plusDays(100).toDate();
  private static final String OFFER_ID = "4567820";
  private static final Integer OFFER_LEAD_TIME_TO_SHIP = 10;
  private static final BigDecimal OFFER_MIN_SHIPPING_PRICE = BigDecimal.valueOf(10);
  private static final BigDecimal OFFER_MIN_SHIPPING_PRICE_ADDITIONAL = BigDecimal.valueOf(25);
  private static final BigDecimal OFFER_ORIGIN_PRICE = BigDecimal.valueOf(120);
  private static final BigDecimal OFFER_PRICE = BigDecimal.valueOf(100);
  private static final String OFFER_PRICE_ADDITIONAL_INFO = "Additional Info";
  private static final String OFFER_PRODUCT_CODE = "product_code";
  private static final Integer OFFER_QUANTITY = 456;
  private static final BigDecimal OFFER_TOTAL_PRICE = BigDecimal.valueOf(147);
  private static final String SHOP_NAME = "test shop";
  private static final String SHOP_CODE = "testshopcode";
  private static final Integer SHOP_EVALUATION_COUNT = 1500;
  private static final Double SHOP_GRADE = 4.5;

  @InjectMocks
  private OfferDataPopulator populator;
  @Mock
  private OfferCodeGenerationStrategy offerCodeGenerationStrategy;
  @Mock
  private OfferModel mockedOffer;
  @Mock
  private ShopModel mockedShop;
  @Mock
  private CurrencyData mockedCurrencyData;
  @Mock
  private PriceDataFactory mockedPriceDataFactory;
  @Mock
  private EnumerationService enumerationServiceMock;
  @Mock
  private EnumerationValueModel mockedEnumValue;
  @Mock
  private PriceData offerDiscountMock;
  @Mock
  private PriceData offerMinShippingPrice;
  @Mock
  private PriceData offerMinShippingPriceAdditional;
  @Mock
  private PriceData offerOriginPrice;
  @Mock
  private PriceData offerPrice;
  @Mock
  private PriceData offerTotalPrice;
  @Mock
  private CurrencyModel mockedCurrencyModel;
  @Mock
  private OfferState offerStateMock;

  @Before
  public void setup() {
    when(enumerationServiceMock.getEnumerationName(offerStateMock)).thenReturn(OFFER_STATE);
    when(mockedOffer.getCurrency()).thenReturn(mockedCurrencyModel);

    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_DISCOUNT_PRICE, mockedCurrencyModel))
        .thenReturn(offerDiscountMock);
    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_MIN_SHIPPING_PRICE, mockedCurrencyModel))
        .thenReturn(offerMinShippingPrice);
    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_MIN_SHIPPING_PRICE_ADDITIONAL, mockedCurrencyModel))
        .thenReturn(offerMinShippingPriceAdditional);
    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_ORIGIN_PRICE, mockedCurrencyModel)).thenReturn(offerOriginPrice);
    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_PRICE, mockedCurrencyModel)).thenReturn(offerPrice);
    when(mockedPriceDataFactory.create(PriceDataType.BUY, OFFER_TOTAL_PRICE, mockedCurrencyModel)).thenReturn(offerTotalPrice);

    when(offerDiscountMock.getValue()).thenReturn(OFFER_DISCOUNT_PRICE);
    when(offerMinShippingPrice.getValue()).thenReturn(OFFER_MIN_SHIPPING_PRICE);
    when(offerMinShippingPriceAdditional.getValue()).thenReturn(OFFER_MIN_SHIPPING_PRICE_ADDITIONAL);
    when(offerOriginPrice.getValue()).thenReturn(OFFER_ORIGIN_PRICE);
    when(offerPrice.getValue()).thenReturn(OFFER_PRICE);
    when(offerTotalPrice.getValue()).thenReturn(OFFER_TOTAL_PRICE);

    when(mockedShop.getName()).thenReturn(SHOP_NAME);
    when(mockedShop.getId()).thenReturn(SHOP_CODE);
    when(mockedShop.getEvaluationCount()).thenReturn(SHOP_EVALUATION_COUNT);
    when(mockedShop.getGrade()).thenReturn(SHOP_GRADE);
    when(mockedOffer.getAvailableEndDate()).thenReturn(OFFER_AVAILABLE_END_DATE);
    when(mockedOffer.getAvailableStartDate()).thenReturn(OFFER_AVAILABLE_START_DATE);
    when(mockedOffer.getDescription()).thenReturn(OFFER_DESCRIPTION);
    when(mockedOffer.getDiscountEndDate()).thenReturn(OFFER_DISCOUNT_END_DATE);
    when(mockedOffer.getDiscountPrice()).thenReturn(OFFER_DISCOUNT_PRICE);
    when(mockedOffer.getDiscountStartDate()).thenReturn(OFFER_DISCOUNT_START_DATE);
    when(mockedOffer.getId()).thenReturn(OFFER_ID);
    when(mockedOffer.getLeadTimeToShip()).thenReturn(OFFER_LEAD_TIME_TO_SHIP);
    when(mockedOffer.getMinShippingPrice()).thenReturn(OFFER_MIN_SHIPPING_PRICE);
    when(mockedOffer.getMinShippingPriceAdditional()).thenReturn(OFFER_MIN_SHIPPING_PRICE_ADDITIONAL);
    when(mockedOffer.getOriginPrice()).thenReturn(OFFER_ORIGIN_PRICE);
    when(mockedOffer.getPrice()).thenReturn(OFFER_PRICE);
    when(mockedOffer.getPriceAdditionalInfo()).thenReturn(OFFER_PRICE_ADDITIONAL_INFO);
    when(mockedOffer.getProductCode()).thenReturn(OFFER_PRODUCT_CODE);
    when(mockedOffer.getQuantity()).thenReturn(OFFER_QUANTITY);
    when(mockedOffer.getShop()).thenReturn(mockedShop);
    when(mockedOffer.getState()).thenReturn(offerStateMock);
    when(mockedOffer.getTotalPrice()).thenReturn(OFFER_TOTAL_PRICE);
    when(offerCodeGenerationStrategy.generateCode(OFFER_ID)).thenReturn(OFFER_CODE);
  }

  @Test
  public void shouldConvertOfferProperly() {

    OfferData result = new OfferData();
    populator.populate(mockedOffer, result);

    assertThat(result.getAvailableEndDate(), equalTo(OFFER_AVAILABLE_END_DATE));
    assertThat(result.getAvailableStartDate(), equalTo(OFFER_AVAILABLE_START_DATE));
    assertThat(result.getDescription(), equalTo(OFFER_DESCRIPTION));
    assertThat(result.getDiscountEndDate(), equalTo(OFFER_DISCOUNT_END_DATE));
    assertThat(result.getDiscountPrice().getValue(), equalTo(OFFER_DISCOUNT_PRICE));
    assertThat(result.getDiscountStartDate(), equalTo(OFFER_DISCOUNT_START_DATE));
    assertThat(result.getId(), equalTo(OFFER_ID));
    assertThat(result.getLeadTimeToShip(), equalTo(OFFER_LEAD_TIME_TO_SHIP));
    assertThat(result.getMinShippingPrice().getValue(), equalTo(OFFER_MIN_SHIPPING_PRICE));
    assertThat(result.getMinShippingPriceAdditional().getValue(), equalTo(OFFER_MIN_SHIPPING_PRICE_ADDITIONAL));
    assertThat(result.getOriginPrice().getValue(), equalTo(OFFER_ORIGIN_PRICE));
    assertThat(result.getPrice().getValue(), equalTo(OFFER_PRICE));
    assertThat(result.getPriceAdditionalInfo(), equalTo(OFFER_PRICE_ADDITIONAL_INFO));
    assertThat(result.getProductCode(), equalTo(OFFER_PRODUCT_CODE));
    assertThat(result.getQuantity(), equalTo(OFFER_QUANTITY));
    assertThat(result.getShopCode(), equalTo(SHOP_CODE));
    assertThat(result.getShopEvaluationCount(), equalTo(SHOP_EVALUATION_COUNT));
    assertThat(result.getShopGrade(), equalTo(SHOP_GRADE));
    assertThat(result.getShopName(), equalTo(SHOP_NAME));
    assertThat(result.getState(), equalTo(OFFER_STATE));
    assertThat(result.getTotalPrice().getValue(), equalTo(OFFER_TOTAL_PRICE));
    assertThat(result.getCode(), equalTo(OFFER_CODE));
  }

  @Test
  public void shouldConvertAdditionalInfoWhenDiscountEndIsNull() {
    when(mockedOffer.getDiscountEndDate()).thenReturn(null);

    OfferData result = new OfferData();
    populator.populate(mockedOffer, result);

    assertThat(result.getPriceAdditionalInfo(), equalTo(OFFER_PRICE_ADDITIONAL_INFO));
  }

  @Test
  public void shouldNotConvertAdditionalInfoWhenDiscountIsOver() {
    when(mockedOffer.getDiscountEndDate()).thenReturn(new DateTime().minusDays(5).toDate());

    OfferData result = new OfferData();
    populator.populate(mockedOffer, result);

    assertNull(result.getPriceAdditionalInfo());
  }

  @Test
  public void shouldConvertAdditionalInfoWhenDiscountStartIsNull() {
    when(mockedOffer.getDiscountStartDate()).thenReturn(null);

    OfferData result = new OfferData();
    populator.populate(mockedOffer, result);

    assertThat(result.getPriceAdditionalInfo(), equalTo(OFFER_PRICE_ADDITIONAL_INFO));
  }

  @Test
  public void shouldNotConvertAdditionalInfoWhenDiscountIsNotStarted() {
    when(mockedOffer.getDiscountStartDate()).thenReturn(new DateTime().plusDays(50).toDate());

    OfferData result = new OfferData();
    populator.populate(mockedOffer, result);

    assertNull(result.getPriceAdditionalInfo());
  }

}
