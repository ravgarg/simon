package com.mirakl.hybris.facades.order.impl;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeError;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFeeOffer;
import com.mirakl.client.mmp.front.domain.shipping.MiraklOrderShippingFees;
import com.mirakl.hybris.core.order.services.ShippingFeeService;
import com.mirakl.hybris.core.order.services.ShippingOptionsService;
import com.mirakl.hybris.facades.shipping.data.ShippingOfferDiscrepancyData;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShippingFacadeTest {

  @InjectMocks
  private DefaultShippingFacade testObj = new DefaultShippingFacade();

  @Mock
  private ShippingOptionsService shippingOptionsServiceMock;
  @Mock
  private ShippingFeeService shippingFeeServiceMock;
  @Mock
  private CartService cartServiceMock;
  @Mock
  private ModelService modelServiceMock;
  @Mock
  private CommerceCheckoutService commerceCheckoutServiceMock;
  @Mock
  private Converter<AbstractOrderEntryModel, ShippingOfferDiscrepancyData> offerDiscrepancyConverterMock;

  @Mock
  private CartModel cartMock;
  @Mock
  private CartData cartDataMock;
  @Mock
  private AbstractOrderEntryModel firstOrderEntryMock, secondOrderEntryMock;
  @Mock
  private ShippingOfferDiscrepancyData offerDiscrepancyMock;
  @Mock
  private MiraklOrderShippingFees shippingFeesMock;
  @Mock
  private MiraklOrderShippingFeeError feeErrorMock;
  @Mock
  private MiraklOrderShippingFeeOffer feeOfferMock;

  @Captor
  private ArgumentCaptor<CommerceCheckoutParameter> checkoutParameterArgumentCaptor;

  @Before
  public void setUp() {
    when(cartServiceMock.hasSessionCart()).thenReturn(true);
    when(cartServiceMock.getSessionCart()).thenReturn(cartMock);
    when(cartMock.getEntries()).thenReturn(asList(firstOrderEntryMock, secondOrderEntryMock));
    when(offerDiscrepancyConverterMock.convertAllIgnoreExceptions(asList(firstOrderEntryMock, secondOrderEntryMock)))
        .thenReturn(asList(offerDiscrepancyMock, null));
  }

  @Test
  public void setsAvailableShippingOptions() throws CalculationException {
    testObj.setAvailableShippingOptions();

    verify(cartServiceMock).hasSessionCart();
    verify(cartServiceMock).getSessionCart();
    verify(shippingOptionsServiceMock).setShippingOptions(cartMock);

    verify(commerceCheckoutServiceMock).calculateCart(checkoutParameterArgumentCaptor.capture());
    CommerceCheckoutParameter checkoutParameter = checkoutParameterArgumentCaptor.getValue();
    assertThat(checkoutParameter.getCart()).isEqualTo(cartMock);
    assertThat(checkoutParameter.isEnableHooks()).isTrue();
  }

  @Test(expected = IllegalArgumentException.class)
  public void setAvailableShippingOptionsThrowsIllegalArgumentExceptionIfNoSessionCartIsFound() {
    when(cartServiceMock.hasSessionCart()).thenReturn(false);

    testObj.setAvailableShippingOptions();
  }

  @Test
  public void getsOfferDiscrepanciesWithoutNullValues() {
    List<ShippingOfferDiscrepancyData> result = testObj.getOfferDiscrepancies();

    assertThat(result).containsOnly(offerDiscrepancyMock);
    verify(offerDiscrepancyConverterMock).convertAllIgnoreExceptions(asList(firstOrderEntryMock, secondOrderEntryMock));
  }

  @Test(expected = IllegalArgumentException.class)
  public void getOfferDiscrepanciesThrowsIllegalArgumentExceptionIfNoSessionCartIsFound() {
    when(cartServiceMock.hasSessionCart()).thenReturn(false);

    testObj.getOfferDiscrepancies();
  }

  @Test
  public void removesInvalidOffers() throws CalculationException {
    when(shippingFeeServiceMock.getStoredShippingFees(cartMock)).thenReturn(shippingFeesMock);
    when(shippingFeesMock.getErrors()).thenReturn(singletonList(feeErrorMock));
    when(cartMock.getMarketplaceEntries()).thenReturn(asList(firstOrderEntryMock, secondOrderEntryMock));
    when(shippingFeeServiceMock.extractAllShippingFeeOffers(shippingFeesMock)).thenReturn(singletonList(feeOfferMock));

    testObj.removeInvalidOffers();

    verify(shippingFeeServiceMock).getStoredShippingFees(cartMock);
    verify(cartMock).getMarketplaceEntries();
    verify(shippingFeeServiceMock).extractAllShippingFeeOffers(shippingFeesMock);
    verify(shippingOptionsServiceMock).adjustOfferQuantities(asList(firstOrderEntryMock, secondOrderEntryMock),
        singletonList(feeOfferMock));
    verify(shippingOptionsServiceMock).removeOfferEntriesWithError(cartMock, singletonList(feeErrorMock));

    verify(commerceCheckoutServiceMock).calculateCart(checkoutParameterArgumentCaptor.capture());
    CommerceCheckoutParameter checkoutParameter = checkoutParameterArgumentCaptor.getValue();
    assertThat(checkoutParameter.getCart()).isEqualTo(cartMock);
    assertThat(checkoutParameter.isEnableHooks()).isTrue();
  }

  @Test(expected = IllegalArgumentException.class)
  public void removeInvalidOffersThrowsIllegalArgumentExceptionIfNoSessionCartIsFound() {
    when(cartServiceMock.hasSessionCart()).thenReturn(false);

    testObj.removeInvalidOffers();
  }

  @Test
  public void removeInvalidOffersDoesNotChangeCartIfNoShippingFeesForCartAreFound() {
    when(shippingFeeServiceMock.getStoredShippingFees(cartMock)).thenReturn(null);

    testObj.removeInvalidOffers();

    verify(shippingFeeServiceMock).getStoredShippingFees(cartMock);
    verifyNoMoreInteractions(cartMock);
  }

  @Test(expected = IllegalArgumentException.class)
  public void adjustShippingQuantityThrowsIllegalArgumentExceptionIfNoSessionCartIsFound() {
    when(cartServiceMock.hasSessionCart()).thenReturn(false);

    testObj.removeInvalidOffers();
  }
}
