package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.OfferOverviewData;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.facades.product.helpers.PriceDataFactoryHelper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklOfferOverviewDataPopulatorTest {

  private static final String OFFER_ID = "id";
  private static final String OFFER_CODE = "offer_code";
  private static final BigDecimal PRICE = BigDecimal.valueOf(100);
  private static final BigDecimal ORIGIN_PRICE = BigDecimal.valueOf(150);
  private static final BigDecimal MIN_SHIPPING_PRICE = BigDecimal.valueOf(7.5);
  private static final String SHOP_ID = "shop-id";
  private static final String SHOP_NAME = "shop-name";
  private static final double SHOP_GRADE = 3;

  @InjectMocks
  private MiraklOfferOverviewDataPopulator populator;

  @Mock
  private OfferCodeGenerationStrategy offerCodeGenerationStrategy;
  @Mock
  private PriceDataFactoryHelper priceDataFactoryHelper;
  @Mock
  private OfferModel offer;
  @Mock
  private ShopModel shop;
  @Mock
  private PriceData priceData, originPriceData, minShippingPriceData;

  @Before
  public void setUp() throws Exception {
    when(offerCodeGenerationStrategy.generateCode(OFFER_ID)).thenReturn(OFFER_CODE);

    when(offer.getId()).thenReturn(OFFER_ID);
    when(offer.getPrice()).thenReturn(PRICE);
    when(offer.getOriginPrice()).thenReturn(ORIGIN_PRICE);
    when(offer.getMinShippingPrice()).thenReturn(MIN_SHIPPING_PRICE);
    when(offer.getShop()).thenReturn(shop);
    when(shop.getId()).thenReturn(SHOP_ID);
    when(shop.getName()).thenReturn(SHOP_NAME);
    when(shop.getGrade()).thenReturn(SHOP_GRADE);

    when(priceDataFactoryHelper.createPrice(PRICE)).thenReturn(priceData);
    when(priceDataFactoryHelper.createPrice(ORIGIN_PRICE)).thenReturn(originPriceData);
    when(priceDataFactoryHelper.createPrice(MIN_SHIPPING_PRICE)).thenReturn(minShippingPriceData);
  }

  @Test
  public void shouldPopulateOfferSummary() {
    OfferOverviewData result = new OfferOverviewData();
    populator.populate(offer, result);

    assertThat(result.getCode()).isEqualTo(OFFER_CODE);
    assertThat(result.getPrice()).isEqualTo(priceData);
    assertThat(result.getOriginPrice()).isEqualTo(originPriceData);
    assertThat(result.getMinShippingPrice()).isEqualTo(minShippingPriceData);
    assertThat(result.getShopId()).isEqualTo(SHOP_ID);
    assertThat(result.getShopName()).isEqualTo(SHOP_NAME);
    assertThat(result.getShopGrade()).isEqualTo(SHOP_GRADE);

  }

}
