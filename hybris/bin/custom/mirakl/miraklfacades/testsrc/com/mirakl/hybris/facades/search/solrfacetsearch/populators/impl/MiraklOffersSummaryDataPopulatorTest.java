package com.mirakl.hybris.facades.search.solrfacetsearch.populators.impl;

import static com.google.common.collect.FluentIterable.from;
import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.base.Predicate;
import com.mirakl.hybris.beans.OfferOverviewData;
import com.mirakl.hybris.beans.OfferStateSummaryData;
import com.mirakl.hybris.beans.OffersSummaryData;
import com.mirakl.hybris.core.enums.OfferState;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.facades.product.helpers.PriceDataFactoryHelper;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MiraklOffersSummaryDataPopulatorTest {

  private static final BigDecimal PRICE_OTHER_OFFER = BigDecimal.valueOf(200);
  private static final BigDecimal PRICE_BEST_OFFER = BigDecimal.valueOf(150);
  private static final OfferState STATE_BEST_OFFER = OfferState.valueOf("NEW");
  private static final OfferState STATE_OTHER_OFFER = OfferState.valueOf("USED");

  @InjectMocks
  private MiraklOffersSummaryDataPopulator populator;

  @Mock
  private Converter<OfferModel, OfferOverviewData> offerSummaryConverter;
  @Mock
  private PriceDataFactoryHelper priceDataFactoryHelper;
  @Mock
  private OfferModel bestOffer;
  @Mock
  private OfferModel otherOffer1;
  @Mock
  private OfferModel otherOffer2;
  @Mock
  private OfferOverviewData bestOfferSummary;
  @Mock
  private PriceData bestOfferPriceData, otherOfferPriceData;

  private List<OfferModel> sortedOffers;

  @Before
  public void setUp() throws Exception {
    sortedOffers = asList(bestOffer, otherOffer1, otherOffer2);
    when(offerSummaryConverter.convert(bestOffer)).thenReturn(bestOfferSummary);
    when(bestOffer.getState()).thenReturn(STATE_BEST_OFFER);
    when(bestOffer.getPrice()).thenReturn(PRICE_BEST_OFFER);
    when(otherOffer1.getState()).thenReturn(STATE_OTHER_OFFER);
    when(otherOffer1.getPrice()).thenReturn(PRICE_OTHER_OFFER);
    when(otherOffer2.getState()).thenReturn(STATE_OTHER_OFFER);
    when(otherOffer2.getPrice()).thenReturn(PRICE_OTHER_OFFER);

    when(priceDataFactoryHelper.createPrice(PRICE_BEST_OFFER)).thenReturn(bestOfferPriceData);
    when(bestOfferPriceData.getValue()).thenReturn(PRICE_BEST_OFFER);
    when(priceDataFactoryHelper.createPrice(PRICE_OTHER_OFFER)).thenReturn(otherOfferPriceData);
    when(otherOfferPriceData.getValue()).thenReturn(PRICE_OTHER_OFFER);
  }

  @Test
  public void shouldPopulateOffersSummary() {
    OffersSummaryData result = new OffersSummaryData();
    populator.populate(sortedOffers, result);

    assertThat(result.getBestOffer()).isEqualTo(bestOfferSummary);
    assertThat(result.getStates()).isNotEmpty();
    assertThat(result.getOfferCount()).isEqualTo(sortedOffers.size());

    OfferStateSummaryData stateSummaryBestOffer = getSummaryForState(result, STATE_BEST_OFFER);
    assertThat(stateSummaryBestOffer.getMinPrice()).isEqualTo(bestOfferPriceData);
    assertThat(stateSummaryBestOffer.getOfferCount()).isEqualTo(1);
    assertThat(stateSummaryBestOffer.getStateCode()).isEqualTo(STATE_BEST_OFFER.getCode());

    OfferStateSummaryData stateSummaryOtherOffer = getSummaryForState(result, STATE_OTHER_OFFER);
    assertThat(stateSummaryOtherOffer.getMinPrice()).isEqualTo(otherOfferPriceData);
    assertThat(stateSummaryOtherOffer.getOfferCount()).isEqualTo(2);
    assertThat(stateSummaryOtherOffer.getStateCode()).isEqualTo(STATE_OTHER_OFFER.getCode());
  }

  protected OfferStateSummaryData getSummaryForState(final OffersSummaryData result, final OfferState offerState) {
    return from(result.getStates()).firstMatch(new Predicate<OfferStateSummaryData>() {

      @Override
      public boolean apply(OfferStateSummaryData summary) {
        return offerState.getCode().equals(summary.getStateCode());
      }
    }).get();
  }


}
