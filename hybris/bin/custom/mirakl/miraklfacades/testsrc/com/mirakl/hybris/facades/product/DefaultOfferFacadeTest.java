package com.mirakl.hybris.facades.product;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.core.model.OfferModel;
import com.mirakl.hybris.core.product.services.OfferService;
import com.mirakl.hybris.core.product.strategies.OfferCodeGenerationStrategy;
import com.mirakl.hybris.facades.product.OfferFacade;
import com.mirakl.hybris.facades.product.impl.DefaultOfferFacade;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

/**
 * Copyright (C) 2016 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOfferFacadeTest {

  private static final String OFFER_ID = "offer-id";
  private static final String OFFER_CODE = "offer-code";
  private static final String PRODUCT_CODE = "product_code";
  private static final String UNKNOWN_OFFER_CODE = "Whatever..";

  @InjectMocks
  private OfferFacade testObj = new DefaultOfferFacade();

  @Mock
  private OfferCodeGenerationStrategy offerCodeGenerationStrategyMock;
  @Mock
  private Converter<OfferModel, OfferData> offerConverterMock;
  @Mock
  private OfferService offerServiceMock;
  @Mock
  private OfferData firstOfferDataMock, secondOfferDataMock;
  @Mock
  private OfferModel firstOfferMock, secondOfferMock, offerMock;

  @Before
  public void setUp() {
    when(offerServiceMock.getOfferForId(OFFER_ID)).thenReturn(offerMock);
    when(offerCodeGenerationStrategyMock.isOfferCode(OFFER_CODE)).thenReturn(true);
    when(offerCodeGenerationStrategyMock.translateCodeToId(OFFER_CODE)).thenReturn(OFFER_ID);
  }

  @Test
  public void getOffersForProductWhenOffersFound() {
    when(offerServiceMock.getSortedOffersForProductCode(PRODUCT_CODE)).thenReturn(asList(firstOfferMock, secondOfferMock));
    when(offerConverterMock.convertAll(asList(firstOfferMock, secondOfferMock)))
        .thenReturn(asList(firstOfferDataMock, secondOfferDataMock));

    List<OfferData> result = testObj.getOffersForProductCode(PRODUCT_CODE);

    assertThat(result).containsExactly(firstOfferDataMock, secondOfferDataMock);

    verify(offerServiceMock).getSortedOffersForProductCode(PRODUCT_CODE);
    verify(offerConverterMock).convertAll(asList(firstOfferMock, secondOfferMock));
  }

  @Test
  public void getOffersForProductWhenNoOffersFound() {
    when(offerServiceMock.getSortedOffersForProductCode(PRODUCT_CODE)).thenReturn(Collections.<OfferModel>emptyList());
    when(offerConverterMock.convertAll(Collections.<OfferModel>emptyList())).thenReturn(Collections.<OfferData>emptyList());

    List<OfferData> result = testObj.getOffersForProductCode(PRODUCT_CODE);

    assertThat(result).isEmpty();

    verify(offerServiceMock).getSortedOffersForProductCode(PRODUCT_CODE);
    verify(offerConverterMock).convertAll(Collections.<OfferModel>emptyList());
  }

  @Test
  public void getOfferForCode() {
    OfferModel offer = testObj.getOfferForCode(OFFER_CODE);

    assertThat(offer).isEqualTo(offerMock);
  }

  @Test(expected = UnknownIdentifierException.class)
  public void getOfferForUnknownCodeThrowsUnknownIdentifierException() {
    testObj.getOfferForCode(UNKNOWN_OFFER_CODE);
  }

}
