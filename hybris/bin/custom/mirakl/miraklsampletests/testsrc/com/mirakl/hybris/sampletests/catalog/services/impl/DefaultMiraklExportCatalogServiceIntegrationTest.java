package com.mirakl.hybris.sampletests.catalog.services.impl;

import static java.util.Collections.singletonList;
import static org.fest.assertions.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.mirakl.hybris.beans.MiraklExportCatalogConfig;
import com.mirakl.hybris.beans.MiraklExportCatalogResultData;
import com.mirakl.hybris.core.catalog.daos.MiraklCoreAttributeConfigurationDao;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogContext;
import com.mirakl.hybris.core.catalog.services.MiraklExportCatalogService;
import com.mirakl.hybris.core.catalog.writer.ExportCatalogWriter;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.ServicelayerTest;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */

@IntegrationTest
public class DefaultMiraklExportCatalogServiceIntegrationTest extends ServicelayerTest {

  private static final Logger LOG = Logger.getLogger(DefaultMiraklExportCatalogServiceIntegrationTest.class);

  private static final String EXPORT_CATALOG_IMPEX = "/miraklsampletests/test/testExportCatalogService.impex";
  private static final String ATTRIBUTES_GENERATED_CSV_FILE = "attributes.csv";
  private static final String CATEGORIES_GENERATED_CSV_FILE = "categories.csv";
  private static final String VALUE_LISTS_GENERATED_CSV_FILE = "valueLists.csv";
  private static final String ATTRIBUTES_REFERENCE_FILE = "/miraklsampletests/test/attributeExportReference.csv";
  private static final String CATEGORIES_REFERENCE_FILE = "/miraklsampletests/test/categoryExportReference.csv";
  private static final String VALUE_LISTS_REFERENCE_FILE = "/miraklsampletests/test/valueListExportReference.csv";

  @Resource
  private MiraklExportCatalogService miraklExportCatalogService;
  @Resource
  private CategoryService categoryService;
  @Resource
  private CatalogVersionService catalogVersionService;
  @Resource
  private MiraklCoreAttributeConfigurationDao miraklCoreAttributeConfigurationDao;
  private MiraklExportCatalogConfig exportConfig;
  private CatalogVersionModel catalogVersion;

  @Before
  public void setUp() throws Exception {
    importCsv(EXPORT_CATALOG_IMPEX, "utf-8");
    initTestSession();
    initTestVariables();
  }

  @Test
  @SuppressWarnings("unchecked")
  public void export() throws Exception {
    try (ExportCatalogWriter writer = Registry.getApplicationContext().getBean(ExportCatalogWriter.class, exportConfig)) {
      MiraklExportCatalogResultData result;
      MiraklExportCatalogContext context = new MiraklExportCatalogContext();
      context.setMiraklValueCodes(Sets.newHashSet(Pair.of("iron", "iron-type"), //
          Pair.of("wood", "type-clubsCC"), //
          Pair.of("adidas", "brand-values"), //
          Pair.of("mephisto", "brand-values")));
      context.setMiraklCategoryCodes(Sets.newHashSet("golfpulls", "bikes"));
      context.setMiraklAttributeCodes(Sets.newHashSet(Pair.of("diameter", "gymballs"), Pair.of("size", "gymballs")));
      context.setWriter(writer);
      context.setExportConfig(exportConfig);
      context.setVisitedClassIds(new HashSet<String>());
      result = miraklExportCatalogService.export(context);
      context.getWriter().getAttributesFile();
      context.getWriter().getCategoriesFile();
      context.getWriter().getValueListsFile();
      assertThat(result).isNotNull();
      verifyExportFiles(context);
    }

  }

  private void verifyExportFiles(MiraklExportCatalogContext context) throws java.io.IOException {
    assertCSVFilesIdentical(getResourceFile(ATTRIBUTES_REFERENCE_FILE), context.getWriter().getAttributesFile());
    assertCSVFilesIdentical(getResourceFile(CATEGORIES_REFERENCE_FILE), context.getWriter().getCategoriesFile());
    assertCSVFilesIdentical(getResourceFile(VALUE_LISTS_REFERENCE_FILE), context.getWriter().getValueListsFile());
  }

  private void initTestVariables() {
    exportConfig = new MiraklExportCatalogConfig();
    exportConfig.setCoreAttributes(miraklCoreAttributeConfigurationDao
        .getCoreAttributeConfigurationForCode("default-core-attributes-configuration").getCoreAttributes());
    exportConfig.setAttributesFilename(ATTRIBUTES_GENERATED_CSV_FILE);
    exportConfig.setCategoriesFilename(CATEGORIES_GENERATED_CSV_FILE);
    exportConfig.setValueListsFilename(VALUE_LISTS_GENERATED_CSV_FILE);
    exportConfig.setDefaultLocale(Locale.ENGLISH);
    exportConfig.setAdditionalLocales(singletonList(Locale.FRENCH));
    exportConfig.setRootCategory(categoryService.getCategoryForCode(catalogVersion, "sport"));
    exportConfig.setExportCategories(true);
    exportConfig.setExportAttributes(true);
    exportConfig.setExportValueLists(true);
    exportConfig.setDryRunMode(true);
    exportConfig.setCatalogVersion(catalogVersion);
    exportConfig.setRootProductType(catalogVersion.getCatalog().getRootProductType().getCode());
  }

  private void initTestSession() {
    catalogVersion = catalogVersionService.getCatalogVersion("testCatalog", "Online");
    JaloSession.getCurrentSession().setUser(UserManager.getInstance().getAdminEmployee());
    JaloSession.getCurrentSession().getSessionContext().setAttribute("catalogversions", catalogVersion);
  }

  private File getResourceFile(String fileName) {
    return new File(ServicelayerTest.class.getResource(fileName).getFile());
  }

  private void assertCSVFilesIdentical(File referenceFile, File targetFile) throws IOException {
    HashSet<String> referenceFileLines = new HashSet<>(FileUtils.readLines(referenceFile));
    HashSet<String> targetFileLines = new HashSet<>(FileUtils.readLines(targetFile));
    assertThat(targetFileLines).containsOnly(referenceFileLines.toArray());
  }
}
