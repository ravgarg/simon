package com.mirakl.hybris.sampletests.product.jobs;

import static java.util.Arrays.asList;
import static org.fest.assertions.Assertions.assertThat;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.mirakl.hybris.core.product.strategies.VariantTypeResolutionStrategy;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductCreationStrategy;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

@IntegrationTest
public class MiraklMultidimensionalProductImportJobIntegrationTest extends AbstractMiraklProductImportJobIntegrationTest {
  private static final String TEST_SETUP_IMPEX = "/miraklsampletests/test/testProductImportJob-multidimensional.impex";
  private static final String SHOP_1_PRODUCT_FILE = "/miraklsampletests/test/shop1-0002-products-multidimensional.csv";


  @Resource(name = "defaultMultidimensionalVariantTypeResolutionStrategy")
  protected VariantTypeResolutionStrategy variantTypeResolutionStrategy;
  @Resource
  protected DefaultProductCreationStrategy productCreationStrategy;

  @Override
  @Before
  public void setUp() throws Exception {
    productCreationStrategy.setVariantTypeResolutionStrategy(variantTypeResolutionStrategy);
    super.setUp();
  }

  @Override
  protected String getCatalogSetupImpex() throws Exception {
    return TEST_SETUP_IMPEX;
  }

  @Override
  protected List<String> getInputFiles() {
    return asList(SHOP_1_PRODUCT_FILE);
  }

  @Test
  public void verifyMultidimensionalProductCreationWithNewVariantValues() {
    ProductModel product40 = findProductForEAN("0000000000040", catalogVersion);

    assertThat(product40).isInstanceOf(GenericVariantProductModel.class);
    assertThat(productHasCategoryWithCode(product40, "CAT_GREEN")).isTrue();
    assertThat(productHasCategoryWithCode(product40, "CAT_L")).isTrue();
    ProductModel baseProduct = ((GenericVariantProductModel) product40).getBaseProduct();
    assertThat(baseProduct).isInstanceOf(ProductModel.class);
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_COLOR")).isTrue();
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_SIZE")).isTrue();
  }

  @Test
  public void verifyMultidimensionalProductCreationWithExistingVariantValues() {
    ProductModel product41 = findProductForEAN("0000000000041", catalogVersion);

    assertThat(product41).isInstanceOf(GenericVariantProductModel.class);
    assertThat(productHasCategoryWithCode(product41, "CAT_RED")).isTrue();
    ProductModel baseProduct = ((GenericVariantProductModel) product41).getBaseProduct();
    assertThat(baseProduct).isInstanceOf(ProductModel.class);
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_COLOR")).isTrue();
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_SIZE")).isFalse();
  }

  @Test
  public void verifyMultidimensionalProductUpdate() {
    ProductModel product42 = findProductForEAN("0000000000042", catalogVersion);

    assertThat(product42).isInstanceOf(GenericVariantProductModel.class);
    assertThat(productHasCategoryWithCode(product42, "CAT_GREEN")).isFalse();
    assertThat(productHasCategoryWithCode(product42, "CAT_M")).isFalse();
    assertThat(productHasCategoryWithCode(product42, "CAT_BLUE")).isTrue();
    assertThat(productHasCategoryWithCode(product42, "CAT_S")).isTrue();
    ProductModel baseProduct = ((GenericVariantProductModel) product42).getBaseProduct();
    assertThat(baseProduct).isInstanceOf(ProductModel.class);
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_COLOR")).isTrue();
    assertThat(productHasCategoryWithCode(baseProduct, "CAT_SIZE")).isTrue();
  }

}
