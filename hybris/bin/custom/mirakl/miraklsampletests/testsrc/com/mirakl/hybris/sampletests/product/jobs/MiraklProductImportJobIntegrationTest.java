package com.mirakl.hybris.sampletests.product.jobs;

import static java.util.Arrays.asList;
import static java.util.Locale.ENGLISH;
import static java.util.Locale.GERMAN;
import static org.fest.assertions.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.mirakl.hybris.sampletests.enums.TestSimpleEnum;
import com.mirakl.hybris.test.model.TestProductModel;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.product.ProductService;

@IntegrationTest
public class MiraklProductImportJobIntegrationTest extends AbstractMiraklProductImportJobIntegrationTest {
  private static final String TEST_SETUP_IMPEX = "/miraklsampletests/test/testProductImportJob.impex";

  private static final String SHOP_1_PRODUCT_FILE = "/miraklsampletests/test/shop1-0001-products.csv";
  private static final String SHOP_2_PRODUCT_FILE = "/miraklsampletests/test/shop2-0001-products.csv";

  private static final String WASHING_TEMPERATURE_FEATURE_QUALIFIER = "washingTemperature";
  private static final String FIRST_TIME_IN_STORE_FEATURE_QUALIFIER = "firstTimeInStore";
  private static final String MATERIAL_FEATURE_QUALIFIER = "material";
  private static final String V_NECK_FEATURE_QUALIFIER = "vNeck";
  private static final String DETAILS_FEATURE_QUALIFIER = "details";
  private static final String PRODUCT_01_NAME_EN = "Product 0000001";
  private static final String PRODUCT_01_NAME_DE = "Produkt 0000001";
  private static final String PRODUCT_04_NAME_EN = "Product 0000004";

  @Resource
  protected ProductService productService;

  @Override
  @Before
  public void setUp() throws Exception {
    super.setUp();
  }

  @Override
  protected String getCatalogSetupImpex() throws Exception {
    return TEST_SETUP_IMPEX;
  }

  @Override
  protected List<String> getInputFiles() {
    return asList(SHOP_1_PRODUCT_FILE, SHOP_2_PRODUCT_FILE);
  }


  /*-
   * The variants with EAN:0000000000033 and EAN:000000000033b are imported from shop1 with VG:VG5
   * The variants were already imported from shop1 (code:PRD33_green_S and code:PRD33_purple_S) but with VG:VG5_old
   *
   * Expected result:
   * The VG of the existing variants should be updated to VG:VG5
   */
  @Test
  public void verifyVGEditionOnAGroupOfProducts() {
    ProductModel product33GreenS = productService.getProductForCode("PRD33_green_S");
    ProductModel product33PurpleS = productService.getProductForCode("PRD33_purple_S");
    ProductModel product33 = productService.getProductForCode("PRD33");
    assertThat(getRootBaseProduct(product33GreenS)).isEqualTo(product33);
    assertThat(getRootBaseProduct(product33PurpleS)).isEqualTo(product33);
    assertThat(productHasVariantGroup(product33, "shop1", "VG5")).isTrue();
    assertThat(productHasVariantGroup(product33, "shop1", "VG5_old")).isFalse();
  }

  /*-
   * The variant with EAN:0000000000032 is imported from shop1 (VG:VG995)
   * The variant with code:PRD98_blue_S already exists and was imported from shop1 with the same VG
   * The variant with EAN:0000000000032 already exists but was imported from shop2 with code:PRD97_black_M and VG:VG995_shop2
   * Both variants code:PRD98_blue_S and code:PRD97_black_M don't have the same root base product
   *
   * Expected result:
   * The variant with EAN:0000000000032 from shop1 should not be imported as it is already present
   */
  @Test
  public void verifyProductAdditionWhenVGPointsToDifferentBaseProducts() {
    ProductModel product97 = productService.getProductForCode("PRD97");

    assertThat(productHasVariantGroup(product97, "shop2", "VG995_shop2")).isTrue();
    assertThat(productHasVariantGroup(product97, "shop1", "VG995")).isFalse();
    assertThat(productHasNotFeature(product97, "details"));
  }

  /*-
   * The variant with EAN:0000000000031 is imported from shop1 (VG999)
   * The variant with EAN:0000000000031 already exists and has a VG from shop2 (VG998)
   *
   * Expected result:
   * The variant should have both VG (the one from shop1 and shop2)
   */
  @Test
  public void verifyProductAdditionWhenAlreadyExistingFromAnotherShopWithDifferentVG() {
    ProductModel product31 = productService.getProductForCode("PRD31");
    ProductModel product31BrownXS = findProductForEAN("0000000000031", catalogVersion);
    assertThat(getRootBaseProduct(product31BrownXS)).isEqualTo(product31);
    assertThat(product31BrownXS.getEan()).isEqualTo("0000000000031");
    assertThat(productHasShopSku(product31BrownXS, "shop1", "0000031")).isTrue();
    assertThat(productHasShopSku(product31BrownXS, "shop2", "0000031_shop2")).isTrue();
    assertThat(productHasVariantGroup(product31, "shop1", "VG999")).isTrue();
    assertThat(productHasVariantGroup(product31, "shop2", "VG998")).isTrue();
  }

  /*-
   * The variant PRD1_Red_S is imported from shop1 with localized core and classification attributes
   *
   * Expected result:
   * The replicated core attributes should be localized from the base product to the leaf
   * The classification attribute should be localized on the variant (the leaf)
   * Localized attributes with missing locale should be considered as English (== the thread default language)
   */
  @Test
  public void verifyAttributeValueLocalization() {
    ProductModel product01RedS = productService.getProductForCode("PRD1_red_S");
    assertThat(product01RedS.getName(ENGLISH)).isEqualTo(PRODUCT_01_NAME_EN);
    assertThat(product01RedS.getName(GERMAN)).isEqualTo(PRODUCT_01_NAME_DE);
    assertThat(productHasFeatureWithValue(product01RedS, DETAILS_FEATURE_QUALIFIER, ENGLISH, "details EN")).isTrue();
    assertThat(productHasFeatureWithValue(product01RedS, DETAILS_FEATURE_QUALIFIER, GERMAN, "details DE")).isTrue();

    ProductModel product01Red = productService.getProductForCode("PRD1_red");
    assertThat(product01Red.getName(ENGLISH)).isEqualTo(PRODUCT_01_NAME_EN);
    assertThat(product01Red.getName(GERMAN)).isEqualTo(PRODUCT_01_NAME_DE);

    ProductModel product01 = productService.getProductForCode("PRD1");
    assertThat(product01.getName(ENGLISH)).isEqualTo(PRODUCT_01_NAME_EN);
    assertThat(product01.getName(GERMAN)).isEqualTo(PRODUCT_01_NAME_DE);
  }

  /*-
   * The variants code:PRD1_red_S and EAN:0000000000004 are imported from shop1 with replicated core attributes
   *
   * Expected result:
   * The replicated core attributes should be set from the base product to the variant
   */
  @Test
  public void verifyAttributeReplication() {
    assertThat(productService.getProductForCode("PRD1_red_S").getName()).isEqualTo(PRODUCT_01_NAME_EN);
    assertThat(productService.getProductForCode("PRD1_red").getName()).isEqualTo(PRODUCT_01_NAME_EN);
    assertThat(productService.getProductForCode("PRD1").getName()).isEqualTo(PRODUCT_01_NAME_EN);

    productHasValueReplicated(findProductForEAN("0000000000004", catalogVersion), "name", PRODUCT_04_NAME_EN);
  }

  /*-
   * The product with EAN:0000000000030 and SKU:0000030 is imported from shop1
   * The product with code:PRD99 already has the SKU:0000030 from shop1
   *
   * Expected result:
   * The SKU:0000030 should be removed from the product with code:PRD99 and added to the new one
   */
  @Test
  public void verifyShopSkuUpdateOnDifferentProductsMatchingBySkuAndUID() {
    ProductModel product99 = productService.getProductForCode("PRD99");
    assertThat(productHasShopSku(product99, "shop1", "0000030")).isFalse();
    ProductModel product30 = findProductForEAN("0000000000030", catalogVersion);
    assertThat(productHasShopSku(product30, "shop1", "0000030")).isTrue();
    assertThat(product30).isNotEqualTo(product99);
  }

  /*-
   * The product with EAN:0000000000028 is imported from shop1 with SKU:0000028
   * The product with EAN:0000000000028 already exists and has code:PRD28 and SKU:28_old_sku from shop1 also
   *
   * Expected result:
   * The SKU:28_old_sku should be updated to be SKU:0000028 on product with code:PRD28
   */
  @Test
  public void verifyShopSkuUpdateOnProductMatchingByUID() {
    ProductModel product28 = productService.getProductForCode("PRD28");
    assertThat(productHasShopSku(product28, "shop1", "0000028")).isTrue();
    assertThat(productHasShopSku(product28, "shop1", "28_old_sku")).isFalse();
  }

  @Test
  public void verifyProductMatchByUIDAndSku() {
    ProductModel product14 = productService.getProductForCode("PRD14");
    assertThat(productHasFeatureWithValue(product14, DETAILS_FEATURE_QUALIFIER, "14 updated")).isTrue();
  }

  @Test
  public void verifyProductMatchBySku() {
    ProductModel product13 = productService.getProductForCode("PRD13");
    assertThat(productHasFeatureWithValue(product13, DETAILS_FEATURE_QUALIFIER, "13 updated")).isTrue();
  }

  @Test
  public void verifyProductMatchByUID() {
    ProductModel product12 = productService.getProductForCode("PRD12");
    assertThat(productHasFeatureWithValue(product12, DETAILS_FEATURE_QUALIFIER, "12 updated")).isTrue();
  }

  /*-
   * The product with EAN:0000000000021 is imported from shop1
   * The product with EAN:0000000000021 already exists. It was imported from shop1 and has code:PRD21
   * The old product had one type of each classification attribute defined (number, boolean, string, date and enum)
   * Each classification attribute on the new imported product is different of the old one
   *
   * Expected result:
   * The classification attributes of product with code:PRD21 should all be updated to the new value
   */
  @Test
  public void verifyClassificationAttributesEdition() {
    ProductModel product21 = productService.getProductForCode("PRD21");
    assertThat(productHasFeatureWithValue(product21, WASHING_TEMPERATURE_FEATURE_QUALIFIER, 80D)).isTrue();
    assertThat(productHasFeatureWithValue(product21, V_NECK_FEATURE_QUALIFIER, false)).isTrue();
    assertThat(productHasFeatureWithValue(product21, DETAILS_FEATURE_QUALIFIER, "updated")).isTrue();
    assertThat(productHasFeatureWithValue(product21, FIRST_TIME_IN_STORE_FEATURE_QUALIFIER, getDate("01-01-2017"))).isTrue();
    assertThat(productHasFeatureWithValues(product21, MATERIAL_FEATURE_QUALIFIER, "polyester", "cotton")).isTrue();
  }

  @Test
  public void verifyCategoryEdition() {
    ProductModel product18 = productService.getProductForCode("PRD18");
    assertThat(rootBaseProductHasCategoryWithCode(product18, "CAT02")).isTrue();
    assertThat(rootBaseProductHasCategoryWithCode(product18, "CAT05")).isFalse();
  }

  @Test
  public void verifyBrandEdition() {
    ProductModel product24 = productService.getProductForCode("PRD24");
    assertThat(rootBaseProductHasCategoryWithCode(product24, "BRD05")).isTrue();
    assertThat(rootBaseProductHasCategoryWithCode(product24, "BRD01")).isFalse();
  }

  @Test
  public void verifyBrandDeletion() {
    ProductModel product23 = productService.getProductForCode("PRD23");
    assertThat(rootBaseProductHasCategoryWithCode(product23, "BRD01")).isFalse();
  }

  @Test
  public void verifyVariantSizeCreationInNewVG() {
    ProductModel variant1 = findProductForEAN("0000000000002", catalogVersion);
    ProductModel variant2 = findProductForEAN("0000000000003", catalogVersion);
    assertThat(getRootBaseProduct(variant1)).isEqualTo(getRootBaseProduct(variant2));
    assertThat(rootBaseProductHasCategoryWithCode(variant1, "CAT02")).isTrue();
    assertThat(rootBaseProductHasCategoryWithCode(variant2, "CAT02")).isTrue();
  }

  @Test
  public void verifyClassificationAttributesCreationOnExistingProduct() {
    ProductModel product01RedS = findProductForEAN("0000000000001", catalogVersion);
    assertThat(productHasFeatureWithValue(product01RedS, WASHING_TEMPERATURE_FEATURE_QUALIFIER, 20D)).isTrue();
    assertThat(productHasFeatureWithValue(product01RedS, V_NECK_FEATURE_QUALIFIER, true)).isTrue();
    assertThat(productHasFeatureWithValue(product01RedS, DETAILS_FEATURE_QUALIFIER, "details EN")).isTrue();
    assertThat(productHasFeatureWithValue(product01RedS, FIRST_TIME_IN_STORE_FEATURE_QUALIFIER, getDate("21-04-2017"))).isTrue();
    assertThat(productHasFeatureWithValues(product01RedS, MATERIAL_FEATURE_QUALIFIER, "polyester", "leather")).isTrue();
  }

  @Test
  public void verifyClassificationAttributesDeletion() {
    ProductModel product22 = findProductForEAN("0000000000022", catalogVersion);
    assertThat(productHasNotFeature(product22, WASHING_TEMPERATURE_FEATURE_QUALIFIER)).isTrue();
    assertThat(productHasNotFeature(product22, V_NECK_FEATURE_QUALIFIER)).isTrue();
    assertThat(productHasNotFeature(product22, DETAILS_FEATURE_QUALIFIER)).isTrue();
    assertThat(productHasNotFeature(product22, FIRST_TIME_IN_STORE_FEATURE_QUALIFIER)).isTrue();
    assertThat(productHasNotFeature(product22, MATERIAL_FEATURE_QUALIFIER)).isTrue();
  }

  @Test
  public void verifyVariantColorCreationInExistingVG() {
    ProductModel productWithNewColor = findProductForEAN("9843616574938", catalogVersion);
    assertThat(productHasSize(productWithNewColor, "brown"));
    ProductModel product00 = productService.getProductForCode("PRD0");
    assertThat(product00.getVariants().size()).isEqualTo(2);
  }

  @Test
  public void verifyVariantSizeCreationInExistingVG() {
    ProductModel productWithNewSize = findProductForEAN("9843616574936", catalogVersion);
    assertThat(productHasSize(productWithNewSize, "XL"));
    ProductModel product00Red = productService.getProductForCode("PRD0_red");
    assertThat(product00Red.getVariants().size()).isEqualTo(2);
  }

  /*-
   * The variant with EAN:0000000000025 is imported from shop1
   * The variant with EAN:0000000000025 already exists. It was imported from shop1 and has code:PRD25_purple_XXL
   * The imported product has a style:blue. The old one has a style:purple
   *
   * Expected result:
   * The style should be updated from style:purple to style:blue
   */
  @Test
  public void verifyVariantStyleUpdate() {
    ProductModel product25PurpleXXL = productService.getProductForCode("PRD25_purple_XXL");
    assertThat(productHasColor(product25PurpleXXL, "blue")).isTrue();
  }

  /*-
   * The variant with EAN:0000000000000 is imported from shop1
   * The variant with EAN:0000000000000 already exists. Is was imported from shop1 and has code:PRD0_red_XS
   * The imported product has a size:S. The old one has a size:XS
   *
   * Expected result:
   * The size should be updated from XS to S on variant with code:PRD0_red_XS
   * The style should remain unchanged
   */
  @Test
  public void verifyVariantSizeUpdate() {
    ProductModel product00RedXS = productService.getProductForCode("PRD0_red_XS");
    assertThat(productHasSize(product00RedXS, "S")).isTrue();
    assertThat(productHasColor(product00RedXS, "red")).isTrue();
  }

  @Test
  public void verifyTranslatedCoreAttributesUpdate() {
    TestProductModel productWithTranslatedAttributes =
        getRootBaseProduct(findProductForEAN("test_translated_values", catalogVersion));
    assertThat(productWithTranslatedAttributes.getBooleanObjectAttribute()).isTrue();
    assertThat(productWithTranslatedAttributes.isBooleanPrimitiveAttribute()).isTrue();
    assertThat(productWithTranslatedAttributes.getIntObjectAttribute()).isEqualTo(-3);
    assertThat(productWithTranslatedAttributes.getIntPrimitiveAttribute()).isEqualTo(0);
    assertThat(productWithTranslatedAttributes.getDoubleObjectAttribute()).isEqualTo(9827.88D);
    assertThat(productWithTranslatedAttributes.getDoublePrimitiveAttribute()).isEqualTo(-233.99D);
    assertThat(productWithTranslatedAttributes.getByteObjectAttribute()).isEqualTo((byte) 10);
    assertThat(productWithTranslatedAttributes.getBytePrimitiveAttribute()).isEqualTo((byte) -20);
    assertThat(productWithTranslatedAttributes.getFloatObjectAttribute()).isEqualTo(3.88F);
    assertThat(productWithTranslatedAttributes.getFloatPrimitiveAttribute()).isEqualTo(-2F);
    assertThat(productWithTranslatedAttributes.getLongObjectAttribute()).isEqualTo(98002L);
    assertThat(productWithTranslatedAttributes.getLongPrimitiveAttribute()).isEqualTo(78991L);
    assertThat(productWithTranslatedAttributes.getShortObjectAttribute()).isEqualTo((short) 88);
    assertThat(productWithTranslatedAttributes.getShortPrimitiveAttribute()).isEqualTo((short) 0);
    assertThat(productWithTranslatedAttributes.getCharacterObjectAttribute()).isEqualTo('c');
    assertThat(productWithTranslatedAttributes.getCharacterPrimitiveAttribute()).isEqualTo('-');
    assertThat(productWithTranslatedAttributes.getStringAttribute()).isEqualTo("mci-integration");
    assertThat(productWithTranslatedAttributes.getBigDecimalAttribute().setScale(4))
        .isEqualTo(new BigDecimal("-9877.9182").setScale(4));
    assertThat(productWithTranslatedAttributes.getDateAttribute()).isEqualTo(getDate("21-05-2020"));
    assertThat(productWithTranslatedAttributes.getSimpleEnumAttribute()).isEqualTo(TestSimpleEnum.SIMPLE_VALUE_1);
    assertThat(productWithTranslatedAttributes.getDynamicEnumAttribute().getCode()).isEqualTo("dynamic-value-2");
  }

  @Test
  public void verifyCronJobResult() {
    assertThat(result).isNotNull();
    assertThat(result.getResult()).isEqualTo(CronJobResult.SUCCESS);
    assertThat(result.getStatus()).isEqualTo(CronJobStatus.FINISHED);
  }


}
