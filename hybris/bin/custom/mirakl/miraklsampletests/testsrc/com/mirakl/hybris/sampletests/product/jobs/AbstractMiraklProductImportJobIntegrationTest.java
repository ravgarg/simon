package com.mirakl.hybris.sampletests.product.jobs;

import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.io.FileUtils.copyFileToDirectory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;

import com.mirakl.hybris.core.catalog.daos.MiraklCoreAttributeConfigurationDao;
import com.mirakl.hybris.core.i18n.services.CurrencyService;
import com.mirakl.hybris.core.model.MiraklProductImportCronJobModel;
import com.mirakl.hybris.core.model.ShopSkuModel;
import com.mirakl.hybris.core.model.ShopVariantGroupModel;
import com.mirakl.hybris.core.product.daos.MiraklProductDao;
import com.mirakl.hybris.core.product.jobs.MiraklProductImportJob;
import com.mirakl.hybris.core.util.CronJobUtils;
import com.mirakl.hybris.test.model.TestColorSizeVariantProductModel;
import com.mirakl.hybris.test.model.TestColorVariantProductModel;
import com.mirakl.hybris.test.model.TestProductModel;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.ClassificationSystemService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.classification.features.LocalizedFeature;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTest;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantProductModel;

/**
 * Copyright (C) 2017 Mirakl. www.mirakl.com - info@mirakl.com All Rights Reserved. Tous droits réservés.
 */
@Ignore
public abstract class AbstractMiraklProductImportJobIntegrationTest extends ServicelayerTest {

  private static final String IMPORT_DATE_FORMAT = "dd-MM-yyyy";
  private static final String JOB_INPUT_DIRECTORY = "test/input";
  private static final String TEST_CLASSIFICATION_CATALOG_NAME = "TestClassificationCatalog";
  private static final String TEST_CLASSIFICATION_CATALOG_VERSION = "1.0";

  @Resource
  protected ModelService modelService;
  @Resource
  protected ClassificationSystemService classificationSystemService;
  @Resource
  protected UserService userService;
  @Resource
  protected CurrencyService currencyService;
  @Resource
  protected CommonI18NService commonI18NService;
  @Resource
  protected MiraklProductDao miraklProductDao;
  @Resource
  protected MiraklCoreAttributeConfigurationDao miraklCoreAttributeConfigurationDao;
  @Resource
  protected ClassificationService classificationService;
  @Resource
  protected TypeService typeService;
  @Resource
  protected CatalogVersionService catalogVersionService;
  @Resource(name = "miraklProductImportJob")
  protected MiraklProductImportJob testObj;

  protected CatalogVersionModel catalogVersion;
  protected PerformResult result;

  public void setUp() throws Exception {
    importCsv(getCatalogSetupImpex(), "UTF-8");
    catalogVersion = catalogVersionService.getCatalogVersion("TestProductCatalog", "Staged");
    final MiraklProductImportCronJobModel cronJob = createProductImportCronJob();
    copyInputFilesToInputDirectory(cronJob, getInputFiles());
    result = testObj.perform(cronJob);
  }

  protected abstract String getCatalogSetupImpex() throws Exception;

  protected abstract List<String> getInputFiles();

  protected MiraklProductImportCronJobModel createProductImportCronJob() {
    final MiraklProductImportCronJobModel productImportCronJob = modelService.create(MiraklProductImportCronJobModel.class);
    productImportCronJob.setCode("testMiraklProductImportCronJob");
    productImportCronJob.setInputDirectory(JOB_INPUT_DIRECTORY);
    productImportCronJob.setArchiveDirectory("resources/miraklsampletests/test/archive");
    productImportCronJob.setNumberOfWorkers(8);
    productImportCronJob.setInputFilePattern("([^-]+)-([^-]+)-(.*)\\.csv");
    productImportCronJob.setCatalogVersion(catalogVersion);
    productImportCronJob.setCoreAttributeConfiguration(
        miraklCoreAttributeConfigurationDao.getCoreAttributeConfigurationForCode("default-core-attributes-configuration"));
    productImportCronJob.setSystemVersion(
        classificationSystemService.getSystemVersion(TEST_CLASSIFICATION_CATALOG_NAME, TEST_CLASSIFICATION_CATALOG_VERSION));
    productImportCronJob.setSessionUser(userService.getUserForUID("admin"));
    productImportCronJob.setSessionLanguage(commonI18NService.getLanguage("en"));
    productImportCronJob.setSessionCurrency(currencyService.getCurrencyForCode("EUR"));
    productImportCronJob.setLogsDaysOld(1);
    productImportCronJob.setFilesDaysOld(1);
    return productImportCronJob;
  }

  protected void copyInputFilesToInputDirectory(final MiraklProductImportCronJobModel cronJob, List<String> filenames)
      throws IOException {
    final File inputDirectory = CronJobUtils.getOrCreateDirectory(cronJob.getInputDirectory(), testObj.getBaseDirectory());
    for (final String shopProductFileName : filenames) {
      copyFileToDirectory(new File(ServicelayerTest.class.getResource(shopProductFileName).getFile()), inputDirectory);
    }
  }

  protected boolean productHasValueReplicated(ProductModel attributeOwner, final String attributeName, final String wantedValue) {
    while (attributeOwner instanceof VariantProductModel) {
      if (!modelService.getAttributeValue(attributeOwner, attributeName).equals(wantedValue)) {
        return false;
      }
      attributeOwner = ((VariantProductModel) attributeOwner).getBaseProduct();
    }
    return modelService.getAttributeValue(attributeOwner, attributeName).equals(wantedValue);
  }

  protected boolean productHasShopSku(final ProductModel product, final String shopName, final String productSku) {
    for (final ShopSkuModel sku : product.getShopSkus()) {
      if (shopName.equalsIgnoreCase(sku.getShop().getId()) && sku.getSku().equals(productSku)) {
        return true;
      }
    }
    return false;
  }

  protected boolean productHasVariantGroup(final ProductModel product, final String shopName, final String variantGroupCode) {
    for (final ShopVariantGroupModel variantGroup : product.getShopVariantGroups()) {
      if (shopName.equalsIgnoreCase(variantGroup.getShop().getId()) && variantGroup.getCode().equals(variantGroupCode)) {
        return true;
      }
    }
    return false;
  }

  protected Date getDate(final String date) {
    final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(IMPORT_DATE_FORMAT);
    return dateTimeFormatter.parseDateTime(date).toDate();
  }

  protected boolean productHasSize(final ProductModel product, final String size) {
    return product instanceof TestColorSizeVariantProductModel
        && size.equalsIgnoreCase(((TestColorSizeVariantProductModel) product).getSize());
  }

  protected boolean productHasColor(final ProductModel product, final String style) {
    return product instanceof TestColorVariantProductModel
        && style.equalsIgnoreCase(((TestColorVariantProductModel) product).getColor());
  }

  protected boolean productHasNotFeature(final ProductModel product, final String featureName) {
    final Feature feature = classificationService.getFeatures(product).getFeatureByName(featureName);
    return feature.getValue() == null && isEmpty(feature.getValues());
  }

  protected boolean productHasFeatureWithValue(final ProductModel product, final String featureQualifier,
      final Locale featureLocale, final Object featureValue) {
    final Feature productFeature = classificationService.getFeatures(product).getFeatureByName(featureQualifier);
    if (productFeature instanceof LocalizedFeature) {
      return featureValue.equals(((LocalizedFeature) productFeature).getValue(featureLocale).getValue());
    }
    return productHasFeatureWithValue(product, featureQualifier, featureValue);
  }

  protected boolean productHasFeatureWithValue(final ProductModel product, final String featureName, final Object value) {
    final Feature feature = classificationService.getFeatures(product).getFeatureByName(featureName);
    return featureValueMatches(feature.getValue(), value);
  }

  private boolean featureValueMatches(final FeatureValue featureValue, final Object wantedValue) {
    final Object value = featureValue.getValue();
    return value instanceof ClassificationAttributeValueModel
        ? ((ClassificationAttributeValueModel) value).getCode().equals(wantedValue) : (value.equals(wantedValue));
  }

  protected boolean productHasFeatureWithValues(final ProductModel product, final String featureName,
      final Object... wantedValues) {
    final Feature feature = classificationService.getFeatures(product).getFeatureByName(featureName);
    for (final Object wantedValue : wantedValues) {
      if (!featureValuesContainValue(feature.getValues(), wantedValue)) {
        return false;
      }
    }
    return true;
  }

  private boolean featureValuesContainValue(final List<FeatureValue> featureValues, final Object wantedValue) {
    for (final FeatureValue featureValue : featureValues) {
      if (featureValueMatches(featureValue, wantedValue)) {
        return true;
      }
    }
    return false;
  }

  protected boolean rootBaseProductHasCategoryWithCode(final ProductModel product, final String categoryCode) {
    return productHasCategoryWithCode(getRootBaseProduct(product), categoryCode);
  }

  protected boolean productHasCategoryWithCode(final ProductModel product, final String categoryCode) {
    final Collection<CategoryModel> superCategories = product.getSupercategories();
    for (final CategoryModel superCategory : superCategories) {
      if (superCategory.getCode().equals(categoryCode)) {
        return true;
      }
    }
    return false;
  }

  protected TestProductModel getRootBaseProduct(final ProductModel identifiedProduct) {
    ProductModel rootBaseProduct = identifiedProduct;
    while (rootBaseProduct instanceof VariantProductModel) {
      rootBaseProduct = ((VariantProductModel) rootBaseProduct).getBaseProduct();
    }
    return (TestProductModel) rootBaseProduct;
  }

  protected ProductModel findProductForEAN(final String ean, CatalogVersionModel catalogVersion) {
    final Map<String, Object> params = new HashMap<>();
    params.put(ProductModel.EAN, ean);
    params.put(ProductModel.CATALOGVERSION, catalogVersion);
    final List<ProductModel> products = miraklProductDao.find(params);
    return (products.size() > 0) ? products.get(0) : null;
  }
}
