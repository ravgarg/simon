package com.mirakl.hybris.miraklsampledataaddon.product.strategies.impl;

import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.strategies.PostProcessProductLineImportStrategy;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.variants.model.VariantProductModel;

public class SamplePostProcessProductLineImportStrategy implements PostProcessProductLineImportStrategy {

  protected UnitService unitService;

  @Override
  public void postProcess(ProductImportData data, MiraklRawProductModel rawProduct, ProductImportFileContextData context) {
    applyProductUnit(data);
    applyApprovalStatus(data);
  }

  protected void applyApprovalStatus(ProductImportData data) {
    ProductModel product = data.getProductToUpdate();

    product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
    data.getModelsToSave().add(product);

    while (product instanceof VariantProductModel) {
      product = ((VariantProductModel) product).getBaseProduct();
      product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
      data.getModelsToSave().add(product);
    }
  }

  protected void applyProductUnit(ProductImportData data) {
    ProductModel product = data.getProductToUpdate();
    product.setUnit(unitService.getUnitForCode("pieces"));
    data.getModelsToSave().add(product);
  }

  @Required
  public void setUnitService(UnitService unitService) {
    this.unitService = unitService;
  }

}
