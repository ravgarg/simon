/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.simon.simonmedia.constants.SimonmediaConstants;
import com.simon.simonmedia.model.MediaImportInfoModel;
import com.simon.simonmedia.service.MediaImportDao;


/**
 *
 * Searches staged versions of catalog and media container.
 *
 */
public class MediaImportDaoImpl implements MediaImportDao
{

	/* Version of catalog to use */
	private String version;


	/* Flexible search service */
	private FlexibleSearchService flexibleSearchService;


	/**
	 * Searches staged version of catalog
	 *
	 * @param catalog
	 *           Name of catalog
	 * @return CatalogVersionModel when found, else null
	 */
	@Override
	public final CatalogVersionModel getCatalogVersion(final String catalog)
	{
		ServicesUtil.validateParameterNotNull(catalog, "Catalog name cannot be null");
		//Flex query to get staged catalog version
		CatalogVersionModel catalogVersion = null;
		final FlexibleSearchQuery catalogQuery = getFlexibleSearchQuery(SimonmediaConstants.CATALOG_QUERY);

		catalogQuery.addQueryParameter(CatalogVersionModel.CATALOG, catalog);
		catalogQuery.addQueryParameter(CatalogVersionModel.VERSION, version);

		final SearchResult<CatalogVersionModel> searchResult = flexibleSearchService.search(catalogQuery);

		final List<CatalogVersionModel> catalogVersionList = searchResult.getResult();

		if (!catalogVersionList.isEmpty())
		{
			catalogVersion = catalogVersionList.get(0);
		}

		return catalogVersion;

	}


	/**
	 * Searches staged version of media container
	 *
	 * @param containerName
	 *           Name of media container
	 * @param catalog
	 *           Catalog
	 * @return MediaContainerModel when found, else null
	 */
	@Override
	public final MediaContainerModel getMediaContainer(final String containerName, final String catalog)
	{

		ServicesUtil.validateParameterNotNull(containerName, "Container name cannot be null");
		ServicesUtil.validateParameterNotNull(containerName, "Catalog name cannot be null");
		MediaContainerModel mediaContainerModel = null;

		//Flex search to get staged version of media container
		final FlexibleSearchQuery containerQuery = getFlexibleSearchQuery(SimonmediaConstants.CONTAINER_QUERY);

		containerQuery.addQueryParameter(MediaContainerModel.NAME, containerName);
		containerQuery.addQueryParameter(CatalogVersionModel.CATALOG, catalog);
		containerQuery.addQueryParameter(CatalogVersionModel.VERSION, version);


		final SearchResult<MediaContainerModel> searchResult = flexibleSearchService.search(containerQuery);
		final List<MediaContainerModel> containerList = searchResult.getResult();

		if (!containerList.isEmpty())
		{
			mediaContainerModel = containerList.get(0);
		}

		return mediaContainerModel;
	}

	/**
	 * Searches staged version of media
	 *
	 * @param catalog
	 *           Catalog containing media
	 * @param fileName
	 *           File Name of media
	 * @param mediaFormat
	 *           Media format used
	 * @return MediaModel when found, else null
	 */
	@Override
	public final MediaModel getMedia(final String catalog, final String fileName, final MediaFormatModel mediaFormat)
	{
		MediaModel mediaModel = null;

		//Flex search to get media item from staged version of a media
		final FlexibleSearchQuery mediaQuery = getFlexibleSearchQuery(SimonmediaConstants.MEDIA_QUERY);
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(SimonmediaConstants.CHAR_SLASH);
		stringBuilder.append(mediaFormat.getQualifier());
		stringBuilder.append(SimonmediaConstants.CHAR_SLASH);
		stringBuilder.append(fileName);

		mediaQuery.addQueryParameter(MediaModel.CODE, stringBuilder.toString());
		mediaQuery.addQueryParameter(CatalogVersionModel.CATALOG, catalog);
		mediaQuery.addQueryParameter(CatalogVersionModel.VERSION, version);

		final SearchResult<MediaModel> searchResult = flexibleSearchService.search(mediaQuery);
		final List<MediaModel> containerList = searchResult.getResult();

		if (!containerList.isEmpty())
		{
			mediaModel = containerList.get(0);
		}

		return mediaModel;
	}

	/**
	 * Searches media import info saved for a folder
	 *
	 * @param folderPath
	 *           Folder path
	 * @return List of MediaImportInfoModel
	 */
	@Override
	public final List<MediaImportInfoModel> getMediaImportInfo(final String folderPath)
	{
		ServicesUtil.validateParameterNotNull(folderPath, "Folder path cannot be null");

		//Flex search to get media import information stored for a folder
		final FlexibleSearchQuery importQuery = getFlexibleSearchQuery(SimonmediaConstants.IMPORT_QUERY_BY_FOLDER);
		importQuery.addQueryParameter(MediaImportInfoModel.FOLDERPATH, folderPath);
		final SearchResult<MediaImportInfoModel> searchResult = flexibleSearchService.search(importQuery);
		return searchResult.getResult();
	}

	/**
	 * Searches media import info saved for a media file
	 *
	 * @param folderPath
	 *           Folder path
	 * @param fileName
	 *           File name
	 *
	 * @return List of MediaImportInfoModel
	 */
	@Override
	public final List<MediaImportInfoModel> getMediaImportInfo(final String folderPath, final String fileName)
	{
		ServicesUtil.validateParameterNotNull(folderPath, "Folder path cannot be null");
		ServicesUtil.validateParameterNotNull(fileName, "File name cannot be null");

		//Flex search to get media import information stored for a file
		final FlexibleSearchQuery importQuery = getFlexibleSearchQuery(SimonmediaConstants.IMPORT_QUERY_BY_FILE_NAME);
		importQuery.addQueryParameter(MediaImportInfoModel.FOLDERPATH, folderPath);
		importQuery.addQueryParameter(MediaImportInfoModel.FILENAME, fileName);

		final SearchResult<MediaImportInfoModel> searchResult = flexibleSearchService.search(importQuery);
		return searchResult.getResult();

	}

	/**
	 * Searches media model
	 *
	 * @param catalog
	 *           Catalog containing media
	 * @param realFileName
	 *           Real file name
	 * @return MediaModel when found, else null
	 */
	@Override
	public final MediaModel getMedia(final String catalog, final String realFileName)
	{
		MediaModel mediaModel = null;

		//Flex search to get media item from staged version of a media
		final FlexibleSearchQuery mediaQuery = getFlexibleSearchQuery(SimonmediaConstants.MEDIA_REALFILENAME_QUERY);
		mediaQuery.addQueryParameter(CatalogVersionModel.CATALOG, catalog);
		mediaQuery.addQueryParameter(CatalogVersionModel.VERSION, version);
		mediaQuery.addQueryParameter(MediaModel.REALFILENAME, realFileName);

		final SearchResult<MediaModel> searchResult = flexibleSearchService.search(mediaQuery);
		final List<MediaModel> containerList = searchResult.getResult();
		if (!containerList.isEmpty())
		{
			mediaModel = containerList.get(0);
		}
		return mediaModel;
	}

	/**
	 * Returns media formats
	 *
	 *
	 * @return List of MediaFormat
	 */
	@Override
	public final List<MediaFormatModel> getMediaFormats()
	{
		//Flex search to get media formats
		final FlexibleSearchQuery formatQuery = getFlexibleSearchQuery(SimonmediaConstants.MEDIA_FORMAT_QUERY);
		final SearchResult<MediaFormatModel> searchResult = flexibleSearchService.search(formatQuery);
		return searchResult.getResult();
	}

	/**
	 * Sets version of catalog to use
	 *
	 * @param version
	 *           the version to set
	 */
	public void setVersion(final String version)
	{
		this.version = version;
	}


	/* Convenience method for unit testing */
	protected FlexibleSearchQuery getFlexibleSearchQuery(final String query)
	{
		return new FlexibleSearchQuery(query);
	}

	/**
	 * Sets flexible search service.
	 *
	 * @param flexibleSearchService
	 */
	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

}
