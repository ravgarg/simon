/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.job;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;


/**
 * Media Import stats class file holds stats information and prints them
 *
 */
public class MediaImportStats
{
	/* Logger */
	private static final Logger LOGGER = Logger.getLogger(MediaImportStats.class);

	/* Configuration service */
	private ConfigurationService configurationService;

	/* Place holder to track imported files */
	private int importedFiles;

	/* Place holder to track files skipped by regex */
	private int rejectedByRegex;

	/* Place holder to track files skipped by file time stamp */
	private int alreadyImported;

	/* Place holder to track errored out files */
	private int erroredFiles;

	/**
	 * Prints job status
	 */
	public void printStats()
	{

		//Print stats
		LOGGER.info("");
		LOGGER.info("**** Job Stats ****");
		LOGGER.info("");
		LOGGER.info("Media folder name " + configurationService.getConfiguration().getString("mediaimporter.folderpath"));
		LOGGER.info("Catalog to use " + configurationService.getConfiguration().getString("mediaimporter.catalog"));
		LOGGER.info("Catalog version to put media file "
				+ configurationService.getConfiguration().getString("mediaimporter.catalogversion"));
		LOGGER.info("Source " + configurationService.getConfiguration().getString("mediaimporter.source"));
		LOGGER.info("");

		LOGGER.info("");
		LOGGER.info("Files imported :" + importedFiles);
		LOGGER.info("Files errored :" + erroredFiles);
		LOGGER.info("Files filtered by regex :" + rejectedByRegex);
		LOGGER.info("Files already imported :" + alreadyImported);

		LOGGER.info("");
		LOGGER.info("**** END****");
	}

	/**
	 * Getter method for imported count
	 *
	 * @return the importedFiles
	 */
	public int getImportedFiles()
	{
		return importedFiles;
	}


	/**
	 * Increment imported files count by 1
	 *
	 */
	public void incImportedFiles()
	{
		importedFiles++;
	}

	/**
	 * Increment rejected by regex count by 1
	 *
	 */
	public void incRejectedByRegex()
	{
		rejectedByRegex++;
	}


	/**
	 * Increment rejected by regex count by regexCount
	 *
	 * @param regexCount
	 *           count to increment by
	 *
	 */
	public void incRejectedByRegex(final int regexCount)
	{
		rejectedByRegex += regexCount;
	}

	/**
	 * Increment already imported count
	 *
	 */
	public void incAlreadyImported()
	{
		alreadyImported++;
	}


	/**
	 * Increment already imported count
	 *
	 * @param alreadyImportedCount
	 *           count to increment by
	 */
	public void incAlreadyImported(final int alreadyImportedCount)
	{
		alreadyImported += alreadyImportedCount;
	}

	/**
	 * Increment error count
	 *
	 */
	public void incErroredFiles()
	{
		erroredFiles++;
	}

	/**
	 * Stores current time in start time
	 *
	 */
	public void trackStartTime()
	{
		importedFiles = 0;
		rejectedByRegex = 0;
		alreadyImported = 0;
		erroredFiles = 0;
	}

	/**
	 * Setter method
	 *
	 * @param configurationService
	 *           Configuration Service
	 */
	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}


}
