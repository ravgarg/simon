/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.facade;

import java.io.File;
import java.util.List;

import com.simon.simonmedia.job.MediaImportStats;


public interface MediaImportFacade
{
	/**
	 * Checks media on file system
	 *
	 * @param mediaFolderPath
	 *           Path on file system
	 *
	 * @param mediaImportStats
	 *           Media stats
	 *
	 * @return List of Files
	 */
	List<File> checkMedia(final String mediaFolderPath, final MediaImportStats mediaImportStats);


	/**
	 * Imports media
	 *
	 * @param filesList
	 *           List of files
	 *
	 * @param mediaImportStats
	 *           Media stats
	 *
	 * @return List of Files
	 */
	List<File> importMedia(final List<File> filesList, final MediaImportStats mediaImportStats);
}
