/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service;

import de.hybris.platform.cronjob.model.CronJobModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.simon.simonmedia.job.MediaImportStats;


/**
 *
 * Creates media in staged catalog.
 *
 */
public interface MediaImportService
{
	/**
	 * Filters files based on regex and timestamp
	 *
	 * @param folderPath
	 *           File source
	 * @param files
	 *           Files in folder
	 * @param mediaImportStats
	 *           Stats object
	 *
	 * @return List of filtered files
	 *
	 */
	List<File> filterFiles(final String folderPath, final File[] files, final MediaImportStats mediaImportStats);


	/**
	 * Creates media model using media file
	 *
	 *
	 * @param mediaFile
	 *           Media file
	 * @param containerName
	 *           Name to be used for media container
	 * @param format
	 *           Format of image
	 */
	void createMediaInStaged(final File mediaFile, final String containerName, final String format) throws FileNotFoundException;

	/**
	 * Update file processed count on cron job.
	 *
	 * @param count
	 *           File count
	 * @param cronJob
	 *           Cron job
	 */
	void updateFileCount(final int count, final CronJobModel cronJob);

	/**
	 * Saves media import info
	 *
	 * @param source
	 *           File source
	 * @param folderPath
	 *           Folder path
	 * @param filesList
	 *           Files in folder
	 *
	 */
	void saveMediaImportInfo(final String source, final List<File> filesList);

	/**
	 * Searches media import info saved for a folder
	 *
	 * @param folderPath
	 *           Folder path
	 * @return Map containing timestamps for each file in the folder
	 */
	Map<String, Date> getFileTimeStamps(final String folderPath);

	/**
	 * Returns media formats
	 *
	 *
	 * @return List of MediaFormat
	 */
	List<String> getMediaFormats();

}
