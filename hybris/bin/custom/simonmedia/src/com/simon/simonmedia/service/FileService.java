/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service;

import java.io.File;
import java.util.Map;


public interface FileService
{
	/**
	 * Traverses folder structure and returns folders and files inside the folder. Folder level traversed is
	 * configurable.
	 *
	 * @param folderPath
	 *           Path on file system
	 *
	 * @return Map containing folder name as key, file list in each folder as value
	 */
	Map<String, File[]> getFolderFileInfo(final String folderPath);

}
