/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.service;

import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import com.simon.simonmedia.constants.SimonmediaConstants;


/**
 * Filter class for filtering files.
 *
 */
public class MediaFilter
{
	/* Regex pattern to filter files */
	private String mediaPattern;

	/* Pattern to apply filer */
	private Pattern pattern;

	/**
	 * Initialize media filter
	 */
	@PostConstruct
	public void initialize()
	{
		//Initialize default value for pattern
		if (pattern == null)
		{
			pattern = SimonmediaConstants.MEDIA_FILE_PATTERN;
		}
	}

	/**
	 * Checks file name using regex pattern set in the object.
	 *
	 * @param fileName
	 *           the file name to check
	 *
	 * @return true - when file name matches pattern, false otherwise
	 */
	public boolean checkFileName(final String fileName)
	{
		return pattern.matcher(fileName).matches();
	}

	/**
	 * Setter method for media file pattern
	 *
	 * @param mediaPattern
	 *           the mediaPattern to set
	 */
	public void setMediaPattern(final String mediaPattern)
	{
		this.mediaPattern = mediaPattern;
		evalPattern();
	}

	/* Method to create new pattern object or use default pattern object */
	private void evalPattern()
	{
		if (mediaPattern == null)
		{
			pattern = SimonmediaConstants.MEDIA_FILE_PATTERN;
		}
		else
		{
			pattern = Pattern.compile(mediaPattern);
		}
	}
}
