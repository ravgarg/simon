/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import org.apache.commons.collections.CollectionUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Required;

import com.simon.simonmedia.constants.SimonmediaConstants;
import com.simon.simonmedia.enums.MediaImportInfoStatus;
import com.simon.simonmedia.job.MediaImportStats;
import com.simon.simonmedia.model.MediaImportInfoModel;
import com.simon.simonmedia.service.MediaFilter;
import com.simon.simonmedia.service.MediaImportDao;
import com.simon.simonmedia.service.MediaImportService;


/**
 * Creates media in staged catalog.
 */
public class MediaImportServiceImpl implements MediaImportService
{
    /* Media service */
    private MediaService mediaService;

    /* Model service */
    private ModelService modelService;

    /* Dao */
    private MediaImportDao mediaImportDao;

    /* Catalog Name */
    private String catalog;

    /* Media Filter */
    private MediaFilter filter;


    /**
     * Creates media model using media file
     *
     *
     * @param mediaFile
     *           Media file
     * @param containerName
     *           Name to be used for media container
     * @param format
     *           Format of image
     */
    @Override
    public void createMediaInStaged(final File mediaFile, final String containerName, final String format)
            throws FileNotFoundException
    {

        ServicesUtil.validateParameterNotNull(mediaFile, "Media file cannot be null");

        //Validate file
        final String fileName = mediaFile.getName();

        if (!mediaFile.exists() || !mediaFile.isFile())
        {
            throw new IllegalArgumentException("Media file does not exist or is not a file");
        }

        //Validate file name
        if (!filter.checkFileName(fileName))
        {
            throw new IllegalArgumentException("Media file rejected by regex pattern ");
        }

        //Validate catalog
        final CatalogVersionModel catalogVersion = mediaImportDao.getCatalogVersion(catalog);
        if (catalogVersion == null)
        {
            throw new IllegalArgumentException("Staged catalog not found");
        }

        final MediaContainerModel mediaContainerModel = createContainerInStaged(containerName, catalogVersion, catalog);

        //Save media
        final MediaFormatModel mediaFormat = mediaService.getFormat(format);
        MediaModel media = mediaImportDao.getMedia(catalog, fileName, mediaFormat);
        if (media == null)
        {
            media = modelService.create(MediaModel.class);
            media.setRealFileName(fileName);
            final StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(SimonmediaConstants.CHAR_SLASH);
            stringBuilder.append(mediaFormat.getQualifier());
            stringBuilder.append(SimonmediaConstants.CHAR_SLASH);
            stringBuilder.append(fileName);
            media.setCode(stringBuilder.toString());
            media.setCatalogVersion(catalogVersion);
            media.setMediaContainer(mediaContainerModel);
            media.setMediaFormat(mediaFormat);
            final String fileExtension = fileName.substring(fileName.lastIndexOf(SimonmediaConstants.CHAR_DOT), fileName.length());
            media.setMime(SimonmediaConstants.MIME_IMAGE + SimonmediaConstants.CHAR_SLASH + fileExtension);
            final List<MediaModel> medias = new ArrayList<>();
            medias.add(media);
            final Collection<MediaModel> mediasInContainer = mediaContainerModel.getMedias();
            if(CollectionUtils.isNotEmpty(mediasInContainer))
            {
                medias.addAll(mediasInContainer);
            }
            mediaContainerModel.setMedias(medias);
            modelService.saveAll(mediaContainerModel, media);
            mediaService.setStreamForMedia(media, getInputStream(mediaFile));
        }

        //Update media
        mediaService.setStreamForMedia(media, getInputStream(mediaFile));

    }

    /**
     * Creates media container if it does not exist
     *
     * @param containerName
     *           Media Container Name
     * @param catalogVersion
     *           Catalog Version
     * @param catalog
     *           Catalog
     *
     */
    private MediaContainerModel createContainerInStaged(final String containerName, final CatalogVersionModel catalogVersion,
            final String catalog)
    {

        //Save media container
        MediaContainerModel mediaContainerModel = mediaImportDao.getMediaContainer(containerName, catalog);

        if (mediaContainerModel == null)
        {
            mediaContainerModel = modelService.create(MediaContainerModel.class);
            mediaContainerModel.setCatalogVersion(catalogVersion);
            mediaContainerModel.setQualifier(containerName);

        }
        return mediaContainerModel;
    }

    /**
     * Saves media import info
     *
     * @param source
     *           File source
     * @param filesList
     *           Files in folder
     *
     */
    @Override
    public void saveMediaImportInfo(final String source, final List<File> filesList)
    {
        ServicesUtil.validateParameterNotNull(source, "Source cannot be null");
        ServicesUtil.validateParameterNotNull(filesList, "Files list cannot be null");

        final List<MediaImportInfoModel> importInfo = new ArrayList<>();
        for (final File file : filesList)
        {
            if (!file.isDirectory())
            {
                final MediaImportInfoModel importInfoModel = new MediaImportInfoModel();
                importInfoModel.setSource(source);
                importInfoModel.setFileName(file.getName());
                importInfoModel.setFileTimeStamp(new Date(file.lastModified()));
                importInfoModel.setFolderPath(FilenameUtils.separatorsToUnix(file.getParent()));
                importInfoModel.setStatus(MediaImportInfoStatus.MEDIA_IMPORTED);
                importInfo.add(importInfoModel);
            }
        }

        if (!importInfo.isEmpty())
        {
            modelService.saveAll(importInfo);
        }
    }

    /**
     * Filters files based on regex and timestamp
     *
     * @param folderPath
     *           File source
     * @param files
     *           Files in folder
     * @param mediaImportStats
     *           Stats object
     *
     * @return List of filtered files
     *
     */
    @Override
    public List<File> filterFiles(final String folderPath, final File[] files, final MediaImportStats mediaImportStats)
    {

        /* Timestamps to prevent repeat import of files */
        final Map<String, Date> fileTimeStampMap = getFileTimeStamps(folderPath);

        final List<File> filteredFiles = new ArrayList<>();

        if (files == null || files.length == 0)
        {
            return filteredFiles;
        }

        for (final File file : files)
        {
            final String fileName = file.getName();
            if (!filter.checkFileName(fileName))
            {
                mediaImportStats.incRejectedByRegex();
                continue;
            }

            final Date previousImportTimeStamp = fileTimeStampMap.get(file.getName());
            final Date lastModified = new Date(file.lastModified());

            if (previousImportTimeStamp == null || (lastModified.after(previousImportTimeStamp)))
            {
            	filteredFiles.add(file);
            }
            else
            {
            	mediaImportStats.incAlreadyImported();
            }
        }
        return filteredFiles;
    }


    /**
     * Searches media import info saved for a folder
     *
     * @param folderPath
     *           Folder path
     * @return Map containing timestamps for each file in the folder
     */
    @Override
    public Map<String, Date> getFileTimeStamps(final String folderPath)
    {
        //Map to hold time stamps
        final Map<String, Date> fileTimeStampMap = new HashMap<>();

        //Returns time stamps of files inside a folder
        final List<MediaImportInfoModel> importInfoList = mediaImportDao.getMediaImportInfo(folderPath);
        for (final MediaImportInfoModel importInfo : importInfoList)
        {
            final String fileName = importInfo.getFileName();
            final MediaModel mediaModel = mediaImportDao.getMedia(catalog, fileName);
            if (mediaModel != null)
            {
                fileTimeStampMap.put(fileName, importInfo.getFileTimeStamp());
            }
        }
        return fileTimeStampMap;
    }


    /**
     * Returns media formats
     *
     *
     * @return List of MediaFormat
     */
    @Override
    public final List<String> getMediaFormats()
    {
        final List<String> formatList = new ArrayList<>();
        final List<MediaFormatModel> formats = mediaImportDao.getMediaFormats();
        for (final MediaFormatModel format : formats)
        {
            formatList.add(format.getQualifier());
        }
        return formatList;
    }

    /**
     * Update file processed count on cron job.
     *
     * @param count
     *           File count
     * @param cronJob
     *           Cron job
     */
    @Override
    public void updateFileCount(final int count, final CronJobModel cronJob)
    {
        ServicesUtil.validateParameterNotNull(cronJob, "Invalid input when updating cronjob");
        modelService.refresh(cronJob);
        cronJob.setFilesCount(count);
        modelService.save(cronJob);
    }


    /**
     * Setter for model service
     *
     * @param mediaService
     */
    @Required
    public void setMediaService(final MediaService mediaService)
    {
        this.mediaService = mediaService;
    }


    /**
     * Setter for model service
     *
     * @param modelService
     */
    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    /**
     * Setter for dao
     *
     * @param mediaImportDao
     */
    @Required
    public void setMediaImportDao(final MediaImportDao mediaImportDao)
    {
        this.mediaImportDao = mediaImportDao;
    }


    /**
     * Setter method for catalog name
     *
     * @param catalog
     *           Catalog Name
     */
    @Required
    public void setCatalog(final String catalog)
    {
        this.catalog = catalog;
    }


    /**
     * Setter method for media filter
     *
     * @param filter
     *           the filter to set
     */
    public void setFilter(final MediaFilter filter)
    {
        this.filter = filter;
    }


    /* Convenience method for unit testing */
    protected InputStream getInputStream(final File mediaFile) throws FileNotFoundException
    {
        return new FileInputStream(mediaFile);
    }

}