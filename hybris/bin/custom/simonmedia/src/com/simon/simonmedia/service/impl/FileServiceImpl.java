/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */


package com.simon.simonmedia.service.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import com.simon.simonmedia.service.FileService;


/**
 * Service class to traverse folders.
 *
 *
 */
public class FileServiceImpl implements FileService
{

	//Logger
	private static final Logger LOGGER = Logger.getLogger(FileServiceImpl.class);

	/* Folder depth to traverse */
	private int folderDepth;

	/**
	 * Traverses folder structure and returns folders and files inside the folder. Folder level traversed is
	 * configurable.
	 *
	 * @param folderPath
	 *           Path on file system
	 *
	 * @return Map containing folder name as key, file list in each folder as value
	 */
	@Override
	public Map<String, File[]> getFolderFileInfo(final String folderPath)
	{

		ServicesUtil.validateParameterNotNull(folderPath, "Invalid path");

		LOGGER.info("Folder path " + folderPath);
		LOGGER.info("Retrieving files upto " + folderDepth + " levels");

		final File folder = getFile(folderPath);
		if (folderPath.isEmpty() || !folder.exists() || !folder.isDirectory())
		{
			throw new IllegalArgumentException("Invalid path");
		}

		//Map holds references to files in each folder
		final Map<String, File[]> folderMap = new HashMap<>();

		//Build map traversing upto configured no of levels
		buildFolderMap(FilenameUtils.separatorsToUnix(folderPath), folderMap, folderDepth);


		return folderMap;
	}

	/* Build folder map */
	private void buildFolderMap(final String parentFolder, final Map<String, File[]> folderMap, final int depth)
	{
		//List to track files in folder
		final List<String> folderList = new ArrayList<>();
		final List<File> filesList = new ArrayList<>();
		final File[] filesFoldersList = getFile(parentFolder).listFiles();
		for (final File file : filesFoldersList)
		{
			//Add file to list if its not a directory
			if (file.isDirectory())
			{
				folderList.add(file.getName());
			}
			else
			{
				filesList.add(file);
			}
		}

		//Put file list in map
		if (!filesList.isEmpty())
		{
			final File[] filesListArr = new File[filesList.size()];
			folderMap.put(parentFolder, filesList.toArray(filesListArr));
		}

		//Traverse next level
		if (depth >= 0)
		{
			final int depthOneLevelBelow = depth - 1;
			for (final String folder : folderList)
			{
				buildFolderMap(parentFolder + "/" + folder, folderMap, depthOneLevelBelow);
			}
		}

	}

	/* Convenience method for unit testing */
	protected File getFile(final String folderPath)
	{
		return new File(folderPath);
	}

	/**
	 * Setter method for folder depth
	 *
	 * @param folderDepth
	 *           Folder depth to traverse
	 */
	public void setFolderDepth(final int folderDepth)
	{
		this.folderDepth = folderDepth;
	}
}
