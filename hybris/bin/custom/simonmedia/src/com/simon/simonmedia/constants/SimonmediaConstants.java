/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.constants;

import java.util.regex.Pattern;

import com.simon.simonmedia.model.MediaImportInfoModel;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;


/**
 * Global class for all Simonmedia constants.
 */
@SuppressWarnings(
{ "javadoc", "deprecation", "squid:S1192" })
public final class SimonmediaConstants
{

	/**
	 * Extension name
	 */
	public static final String EXTENSIONNAME = "simonmedia";

	/**
	 * Extension information
	 */
	public static class TC
	{
		public static final String MEDIAIMPORTINFO = "MediaImportInfo".intern();

		/**
		 * Empty constructor
		 */
		private TC()
		{

		}
	}

	/**
	 * Extension name
	 */
	public static final String PLATFORM_LOGO_CODE = "simonmediaPlatformLogo";

	// Queries used by DAO
	
	/**
	 * Query to get catalog version
	 */
	public static final String CATALOG_QUERY = "select {cv."+CatalogVersionModel.PK+"} from {"+CatalogVersionModel._TYPECODE+" as cv}, {"+CatalogModel._TYPECODE+" as c} where {cv."+CatalogVersionModel.CATALOG+"}={c."+CatalogModel.PK+"} and {c."+CatalogModel.ID+"}=?catalog "
			+ "and {cv."+CatalogVersionModel.VERSION+"}=?version";

	
	/**
	 * Query to get container by name, catalog, version
	 */
	public static final String CONTAINER_QUERY = "select {mc."+MediaContainerModel.PK+"} from {"+MediaContainerModel._TYPECODE+" as mc}, {"+CatalogVersionModel._TYPECODE+" as cv}, {"+CatalogModel._TYPECODE+" as c} where {mc."+MediaContainerModel.QUALIFIER+"}="
			+ "?name and  {mc."+MediaContainerModel.CATALOGVERSION+"} = {cv."+CatalogVersionModel.PK+"} and {cv."+CatalogVersionModel.CATALOG+"} = {c."+CatalogModel.PK+"} and {c."+CatalogModel.ID+"}=?catalog"
			+ " and {cv."+CatalogVersionModel.VERSION+"}=?version";

	/**
	 * Query to get media by code, catalog, version
	 */
	public static final String MEDIA_QUERY = "select {m."+MediaModel.PK+"} from {"+MediaModel._TYPECODE+" as m}, {"+CatalogVersionModel._TYPECODE+" as cv}, {"+CatalogModel._TYPECODE+" as c}  where {m."+MediaModel.CODE+"}=?code"
			+ " and  {m."+MediaModel.CATALOGVERSION+"} = {cv."+CatalogVersionModel.PK+"} and {cv."+CatalogVersionModel.CATALOG+"} = {c."+CatalogModel.PK+"} and {c."+CatalogModel.ID+"}=?catalog and {cv."+CatalogVersionModel.VERSION+"}=?version ";

	/**
	 * Query to get media import info by file folder
	 */
	public static final String IMPORT_QUERY_BY_FOLDER = "select {"+MediaImportInfoModel.PK+"} from {"+MediaImportInfoModel._TYPECODE+" as mii} where {"+MediaImportInfoModel.FOLDERPATH+"}=?folderPath";

	/**
	 * Query to get media import info by file name
	 */
	public static final String IMPORT_QUERY_BY_FILE_NAME = "select {"+MediaImportInfoModel.PK+"} from {"+MediaImportInfoModel._TYPECODE+" as mii} where {"+MediaImportInfoModel.FOLDERPATH+"}=?folderPath and {"+MediaImportInfoModel.FILENAME+"} = ?fileName";

	/**
	 * Query to get created media formats
	 */
	public static final String MEDIA_FORMAT_QUERY = "select {"+MediaFormatModel.PK+"} from {"+MediaFormatModel._TYPECODE+" as mf}";

	/**
	 * Query to get created media formats
	 */
	public static final String MEDIA_FORMAT_QUERY_BY_CODE = "select {"+MediaFormatModel.PK+"} from {"+MediaFormatModel._TYPECODE+" as mf} where {"+MediaFormatModel.QUALIFIER+"}=?code";

	/**
	 * Query to get file names of media
	 */
	public static final String MEDIA_REALFILENAME_QUERY = "select {m."+MediaModel.PK+"} from {"+MediaModel._TYPECODE+" as m}, {"+CatalogVersionModel._TYPECODE+" as cv}, {"+CatalogModel._TYPECODE+" as c}  where "
			+ " {m."+MediaModel.CATALOGVERSION+"} = {cv."+CatalogVersionModel.PK+"} and {cv."+CatalogVersionModel.CATALOG+"} = {c."+CatalogModel.PK+"} and {c."+CatalogModel.ID+"}=?catalog and {cv."+CatalogVersionModel.VERSION+"}=?version and {m."+MediaModel.REALFILENAME+"} = ?realFileName";



	//Regex patterns

	/**
	 * Fallback regex pattern. Matches files having extension .jpg, .png, .gif, .bmp.
	 */
	public static final Pattern MEDIA_FILE_PATTERN = Pattern.compile("([^\\t]+(\\.(?i)(jpeg|jpg|png|gif|bmp|svg))$)");

	/**
	 * Pattern for image format
	 */
	public static final Pattern WIDTH_HEIGHT_REGEX_PATTERN = Pattern.compile("([0-9]+(x|X)[0-9]+)");

	/**
	 * Pattern of media format qualifier
	 */
	public static final Pattern MEDIA_FORMAT_QUALIFIER_PATTERN = Pattern.compile("^([0-9]+(w|W)(x|X)[0-9]+(h|H))$");


	/**
	 * Fallback image format
	 */
	public static final String DEFAULT_IMAGE_FORMAT = "desktop";


	/**
	 * Media import stats bean name
	 */
	public static final String BEAN_MEDIA_IMPORT_STATS = "mediaImportStats";


	/**
	 * Media file info bean name
	 */
	public static final String BEAN_MEDIA_FILE_INFO = "mediaFileInfo";

	/**
	 * Slash character
	 */
	public static final char CHAR_SLASH = '/';

	/**
	 * Dot character
	 */
	public static final char CHAR_DOT = '.';
	
	/**
	 * W character
	 */
	public static final char CHAR_W_LOWERCASE = 'w';
	public static final char CHAR_W_UPPERCASE = 'W';

	/**
	 * X character
	 */
	public static final char CHAR_X_LOWERCASE = 'x';
	public static final char CHAR_X_UPPERCASE = 'X';

	/**
	 * H character
	 */
	public static final char CHAR_H_UPPERCASE = 'H';


	/**
	 * _ character
	 */
	public static final char CHAR_UNDER_SCORE = '_';

	/**
	 * MINUS_ONE
	 */
	public static final int INT_MINUS_ONE = -1;


	/**
	 * MIME image
	 */
	public static final String MIME_IMAGE = "image";

	/**
	 *
	 * Constructor
	 *
	 * Access is set to private to prevent instantiating of the class.
	 *
	 */
	private SimonmediaConstants()
	{
		super();
	}

}
