/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */


package com.simon.simonmedia.facade.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.exceptions.SystemException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.simon.simonmedia.constants.SimonmediaConstants;
import com.simon.simonmedia.facade.MediaImportFacade;
import com.simon.simonmedia.job.MediaImportStats;
import com.simon.simonmedia.service.FileService;
import com.simon.simonmedia.service.MediaFileInfo;
import com.simon.simonmedia.service.MediaImportService;


/**
 * Facade class to traverse folders.
 *
 *
 */
public class MediaImportFacadeImpl implements MediaImportFacade
{

	//Logger
	private static final Logger LOGGER = Logger.getLogger(MediaImportFacadeImpl.class);

	/* File service */
	private FileService fileService;


	/* Media import service */
	private MediaImportService mediaImportService;

	/**
	 * Checks media on file system
	 *
	 * @param mediaFolderPath
	 *           Path on file system
	 *
	 * @param mediaImportStats
	 *           Media stats
	 *
	 * @return List of Files
	 */
	@Override
	public List<File> checkMedia(final String mediaFolderPath, final MediaImportStats mediaImportStats)
	{
		final List<File> mediaFilesList = new ArrayList<>();


		//Get media folders and files inside each media folder
		final Map<String, File[]> medialFolderMap = fileService.getFolderFileInfo(mediaFolderPath);

		//Iterate on each folder
		for (final Map.Entry<String, File[]> entry : medialFolderMap.entrySet())
		{
			final String mediaFolder = entry.getKey();
			LOGGER.info("Folder :" + mediaFolder + " No of files :" + entry.getValue().length);

			//Read time stamps
			final File[] fileList = entry.getValue();

			//Filter files
			final List<File> filteredList = mediaImportService.filterFiles(mediaFolder, fileList, mediaImportStats);

			//Add to filtered list
			mediaFilesList.addAll(filteredList);

		}

		return mediaFilesList;
	}

	/**
	 * Imports media
	 *
	 * @param filesList
	 *           List of files
	 *
	 * @param mediaImportStats
	 *           Media stats
	 *
	 * @return List of Files
	 */
	@Override
	public List<File> importMedia(final List<File> filesList, final MediaImportStats mediaImportStats)
	{
		final List<File> importedFilesList = new ArrayList<>();

		final MediaFileInfo mediaFileInfo = getApplicationContext().getBean(SimonmediaConstants.BEAN_MEDIA_FILE_INFO,
				MediaFileInfo.class);

		//Check media formats
		final List<String> formatsList = mediaImportService.getMediaFormats();
		if (formatsList.isEmpty())
		{
			LOGGER.error("Media formats not available.");
			return importedFilesList;
		}
		mediaFileInfo.addImageFormats(formatsList);


		for (final File mediaFile : filesList)
		{
			//Find folder name from file name convention
			final String fileName = mediaFile.getName();
			mediaFileInfo.setFileName(fileName);
			final String containerName = mediaFileInfo.getContainerName();
			final String format = mediaFileInfo.getFormat();
			if (format == null)
			{
				LOGGER.info("Import skipped for media file : " + fileName + "Image format not found. " + "Image format : " + format);
				mediaImportStats.incErroredFiles();
				continue;
			}

			try
			{
				mediaImportService.createMediaInStaged(mediaFile, containerName, format);

				//log stats
				LOGGER.info("Import success for media file :  " + fileName + " Container name : " + containerName + " Image format : "
						+ format);
				mediaImportStats.incImportedFiles();
				importedFilesList.add(mediaFile);
			}
			catch (final FileNotFoundException | SystemException e)
			{
				//handle exception
				LOGGER.info("Import failed for media file :  " + fileName, e);
				mediaImportStats.incErroredFiles();
			}

		}
		return importedFilesList;
	}


	/**
	 * Setter method for File service
	 *
	 * @param fileService
	 *           File service
	 */
	public void setFileService(final FileService fileService)
	{
		this.fileService = fileService;
	}


	/**
	 * Setter method for media import service
	 *
	 * @param mediaImportService
	 *           Media Import Service
	 */
	public void setMediaImportService(final MediaImportService mediaImportService)
	{
		this.mediaImportService = mediaImportService;
	}


	/* Convenience method for unit testing */
	protected ApplicationContext getApplicationContext()
	{
		return Registry.getApplicationContext();
	}

}
