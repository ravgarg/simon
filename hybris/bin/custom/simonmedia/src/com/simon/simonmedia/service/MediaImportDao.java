/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.List;

import com.simon.simonmedia.model.MediaImportInfoModel;


/**
 *
 * Searches staged versions of catalog and media container.
 *
 */
public interface MediaImportDao
{

	/**
	 * Searches staged version of catalog
	 *
	 * @param catalog
	 *           Name of catalog
	 * @return CatalogVersionModel when found, else null
	 */
	CatalogVersionModel getCatalogVersion(final String catalog);

	/**
	 * Searches staged version of media container
	 *
	 * @param folderName
	 *           Name of media container
	 * @param catalog
	 *           Name of catalog
	 * @return MediaContainerModel when found, else null
	 */
	MediaContainerModel getMediaContainer(final String folderName, final String catalog);

	/**
	 * Searches media import info saved for a folder
	 *
	 * @param folderName
	 *           Folder path
	 * @return List of MediaImportInfoModel
	 */
	List<MediaImportInfoModel> getMediaImportInfo(final String folderName);

	/**
	 * Searches media import info saved for a media file
	 *
	 * @param folderPath
	 *           Folder path
	 * @param fileName
	 *           File name
	 *
	 * @return List of MediaImportInfoModel
	 */
	List<MediaImportInfoModel> getMediaImportInfo(final String folderPath, final String fileName);

	/**
	 * Searches staged version of media
	 *
	 * @param catalog
	 *           Catalog containing media
	 * @param fileName
	 *           File Name of media
	 * @param mediaFormat
	 *           Media format used
	 * @return MediaModel when found, else null
	 */
	MediaModel getMedia(final String catalog, String fileName, MediaFormatModel mediaFormat);

	/**
	 * Searches media model
	 *
	 * @param catalog
	 *           Catalog containing media
	 * @param realFileName
	 *           Real file name
	 * @return MediaModel when found, else null
	 */
	MediaModel getMedia(final String catalog, String realFileName);

	/**
	 * Returns media formats
	 *
	 *
	 * @return List of MediaFormat
	 */
	List<MediaFormatModel> getMediaFormats();

}
