/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.job;

import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.simon.simonmedia.constants.SimonmediaConstants;
import com.simon.simonmedia.facade.MediaImportFacade;
import com.simon.simonmedia.service.MediaImportService;


/**
 * Media Importer Cron job imports media files into Hybris from local file system.
 *
 */
public class MediaImporterJob extends AbstractJobPerformable<CronJobModel>
{

	/* Logger */
	private static final Logger LOGGER = Logger.getLogger(MediaImporterJob.class);

	/* Media import service */
	private MediaImportService mediaImportService;

	/* File source information */
	private String fileSource;

	/* Folder path of media directory */
	private String mediaFolderPath;

	/* Media Import Facade */
	private MediaImportFacade mediaImportFacade;


	/**
	 * Initialize media importer job
	 */
	@PostConstruct
	public void initialize()
	{
		LOGGER.info("MediaImporter Job initialized ");
	}

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{

		final MediaImportStats mediaImportStats = getApplicationContext().getBean(SimonmediaConstants.BEAN_MEDIA_IMPORT_STATS,
				MediaImportStats.class);

		LOGGER.info("Media Importer cron job started ");

		mediaImportStats.trackStartTime();

		//Check if media folder exists and is directory
		final File mediaFolder = getFolder(mediaFolderPath);

		if (mediaFolder.exists() && mediaFolder.isDirectory())
		{
			//Check media files
			final List<File> mediaFilesList = mediaImportFacade.checkMedia(mediaFolderPath, mediaImportStats);

			//Import media files using the list from the check media method
			final List<File> importedFilesList = mediaImportFacade.importMedia(mediaFilesList, mediaImportStats);

			//Create import info using the list from
			mediaImportService.saveMediaImportInfo(fileSource, importedFilesList);

			//Update file count in cron job
			updateStatus(cronJob, mediaImportStats);

			LOGGER.info("Media Importer cron job finished");

			//Print stats
			mediaImportStats.printStats();

			//Return success
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		else
		{
			LOGGER.error("Error running MediaImporterJob. Media folder is not accessible.");
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}

	}

	/**
	 * Updates cron job status
	 *
	 * @param cronJob
	 *           Cron Job
	 *
	 * @param mediaImportStats
	 *           Media stats
	 *
	 */
	private void updateStatus(final CronJobModel cronJob, final MediaImportStats mediaImportStats)
	{
		mediaImportService.updateFileCount(mediaImportStats.getImportedFiles(), cronJob);
	}

	/**
	 * Setter method for media import service
	 *
	 * @param mediaImportService
	 *           Media Import Service
	 */
	public void setMediaImportService(final MediaImportService mediaImportService)
	{
		this.mediaImportService = mediaImportService;
	}


	/**
	 * Setter method for File source information
	 *
	 * @param source
	 *           source
	 */
	public void setFileSource(final String source)
	{
		this.fileSource = source;
	}

	/**
	 * Setter method for media folder name
	 *
	 * @param folderPath
	 *           the folder path to set
	 */
	public void setMediaFolderPath(final String folderPath)
	{
		this.mediaFolderPath = folderPath;
	}

	/**
	 * Setter method for media import facade
	 *
	 * @param mediaImportFacade
	 *           the mediaImportFacade to set
	 */
	public void setMediaImportFacade(final MediaImportFacade mediaImportFacade)
	{
		this.mediaImportFacade = mediaImportFacade;
	}


	/**
	 * Convenience method for unit testing
	 *
	 * @param folderPath
	 *           the folder path to set
	 */
	protected File getFolder(final String folderPath)
	{
		return new File(folderPath);
	}

	/* Convenience method for unit testing */
	protected ApplicationContext getApplicationContext()
	{
		return Registry.getApplicationContext();
	}

}
