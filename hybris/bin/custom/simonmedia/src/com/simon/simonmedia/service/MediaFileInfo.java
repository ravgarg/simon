/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */
package com.simon.simonmedia.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;

import com.simon.simonmedia.constants.SimonmediaConstants;


/**
 * Class representing media file information embedded in file name
 *
 */
public class MediaFileInfo
{
	/* Logger */
	private static final Logger LOGGER = Logger.getLogger(MediaFileInfo.class);

	/* File name */
	private String fileName;

	/* Image width */
	private int width = SimonmediaConstants.INT_MINUS_ONE;

	/* Image height */
	private int height = SimonmediaConstants.INT_MINUS_ONE;

	/* Container name */
	private String containerName;

	/* Map of formats */
	private Map<Integer, String> imageFormats;


	/**
	 * Initialize media filter
	 */
	@PostConstruct
	public void initialize()
	{
		//Initialize image formats map to empty
		if (imageFormats == null)
		{
			imageFormats = new HashMap<>();
		}
	}



	/**
	 * Setter method for file name
	 *
	 * @param fileName
	 *           the fileName to set
	 */
	public void setFileName(final String fileName)
	{
		evalFileName(fileName);

	}


	/* Evaluate file name and find width, height, container name */
	private void evalFileName(final String fileName)
	{
		this.fileName = fileName;
		this.containerName = "";
		this.width = SimonmediaConstants.INT_MINUS_ONE;
		this.height = SimonmediaConstants.INT_MINUS_ONE;

		if (fileName != null)
		{
			findWidthAndHeight();
			findContainerName();

		}
	}

	/* Gets image width and height from file name */
	private void findWidthAndHeight()
	{

		String format = null;
		final Matcher matcher = SimonmediaConstants.WIDTH_HEIGHT_REGEX_PATTERN.matcher(fileName);

		if (matcher.find())
		{
			format = matcher.group(1);
			int xPos = format.indexOf(SimonmediaConstants.CHAR_X_LOWERCASE);
			if (xPos == -1)
			{
				xPos = format.indexOf(SimonmediaConstants.CHAR_X_UPPERCASE);
			}

			try
			{
				width = Integer.parseInt(format.substring(0, xPos));
				height = Integer.parseInt(format.substring(xPos + 1));
				LOGGER.debug("Width and height info for " + fileName + " : width " + width + " height " + height);
			}
			catch (final NumberFormatException nfe)
			{
				width = SimonmediaConstants.INT_MINUS_ONE;
				height = SimonmediaConstants.INT_MINUS_ONE;
			}
		}
	}

	/* Finds container name */
	private void findContainerName()
	{
		containerName = fileName;

		final int underScorePos = fileName.indexOf(SimonmediaConstants.CHAR_UNDER_SCORE);
		if (underScorePos > 0)
		{
			containerName = fileName.substring(0, underScorePos);
		}
		else
		{
			containerName = fileName;
		}

	}


	/**
	 * Getter method for container name
	 *
	 * @return String containing container name
	 */
	public String getContainerName()
	{
		return containerName;
	}

	/**
	 * Gets closest matching media format
	 *
	 *
	 * @return String containing media format
	 */
	public String getFormat()
	{
		//Return default format when width and height not available
		if (width == -1 && height == -1)
		{
			return SimonmediaConstants.DEFAULT_IMAGE_FORMAT;
		}
		
		String format = null;

		final Collection<String> formatsList = imageFormats.values();
		
		if(!formatsList.isEmpty()) 
		{
			final StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(Integer.toString(width));
			stringBuilder.append(SimonmediaConstants.CHAR_W_UPPERCASE);
			stringBuilder.append(SimonmediaConstants.CHAR_X_LOWERCASE);
			stringBuilder.append(Integer.toString(height));
			stringBuilder.append(SimonmediaConstants.CHAR_H_UPPERCASE);
			//Exact match
			format = findInList(formatsList, stringBuilder.toString());
			
			if (format == null)
			{
				//Find the closest match
				format = findInList(formatsList, getClosestFormat());
				
			}
		}
		
		return format;
		
	}

	/* Case insensitive search in list */
	private String findInList(final Collection<String> formatsList, final String format)
	{
		for (final String str : formatsList)
		{
			if (str.equalsIgnoreCase(format))
			{
				LOGGER.debug("Match found for " + format);
				return str;
			}
		}
		LOGGER.debug("No match found for " + format);
		return null;

	}

	/*
	 * Finds closest matching image format from configured out of the box formats
	 *
	 */
	private String getClosestFormat()
	{
		String format = null;
		final List<Integer> keysList = new ArrayList<>();
		keysList.addAll(imageFormats.keySet());
		Collections.sort(keysList);
		Integer lastKey = null;
		for (final Integer key : keysList)
		{
			lastKey = key;
			if (width <= key.intValue())
			{
				format = imageFormats.get(key);
				break;
			}
		}

		if (format == null && lastKey != null)
		{
			format = imageFormats.get(lastKey);
		}

		LOGGER.debug("Closest format found for width " + width + " is " + format);

		return format;
	}

	/**
	 * Setter method for image formats.
	 *
	 * @param imageFormats
	 *           the imageFormats to set
	 */
	public void setImageFormats(final Map<Integer, String> imageFormats)
	{
		this.imageFormats = imageFormats;
	}

	/**
	 * Adds image formats to internal map
	 *
	 * @param formatsList
	 *           image formats from media format item type
	 */
	public void addImageFormats(final Collection<String> formatsList)
	{
		if (formatsList == null)
		{
			return;
		}

		for (final String str : formatsList)
		{
			final Matcher matcher = SimonmediaConstants.MEDIA_FORMAT_QUALIFIER_PATTERN.matcher(str);
			if (matcher.matches())
			{
				final String format = matcher.group(1);
				int wPos = format.indexOf(SimonmediaConstants.CHAR_W_LOWERCASE);
				if (wPos == -1)
				{
					wPos = format.indexOf(SimonmediaConstants.CHAR_W_UPPERCASE);
				}
				final int widthInFormat = Integer.parseInt(format.substring(0, wPos));
				LOGGER.debug("Putting new entry in intenal map.  Width :" + widthInFormat + " Format :  " + format);
				imageFormats.put(Integer.valueOf(widthInFormat), str);
			}
			else
			{
				LOGGER.debug("Regex pattern match failed for format " + str);
			}
		}

	}
}
