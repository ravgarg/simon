/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.simonmedia.service.MediaFilter;


/**
 * Test class for MediaFilter
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaFilterTest
{
	@InjectMocks
	@Spy
	MediaFilter mediaFilter;

	@Mock
	File mediaFile;

	String fileName;

	String invalidName;

	/**
	 *
	 */
	@Before
	public void setup()
	{
		fileName = "2mdlp_b1_030118_817x252.jpg";
		invalidName = "test12.jpg88";
		mediaFilter.initialize();
	}


	@Test
	public void checkFileNameInvalidTest()
	{
		final boolean check = mediaFilter.checkFileName(invalidName);
		assertEquals(new Boolean(check), new Boolean(false));
	}

	@Test
	public void checkFileNameTest()
	{
		final boolean check = mediaFilter.checkFileName(fileName);
		assertEquals(new Boolean(check), new Boolean(true));
	}

}
