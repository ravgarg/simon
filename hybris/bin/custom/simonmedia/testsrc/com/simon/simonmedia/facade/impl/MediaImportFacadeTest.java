/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.facade.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.simon.simonmedia.job.MediaImportStats;
import com.simon.simonmedia.service.FileService;
import com.simon.simonmedia.service.MediaFileInfo;
import com.simon.simonmedia.service.MediaFilter;
import com.simon.simonmedia.service.MediaImportService;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaImportFacadeTest
{
	@InjectMocks
	@Spy
	MediaImportFacadeImpl mediaImportFacade;


	@Mock
	ApplicationContext applicationContext;


	@Mock
	MediaImportService mediaImportService;

	@Mock
	FileService fileService;

	@Mock
	MediaFilter filter;


	@Mock
	MediaImportStats mediaImportStats;

	@Mock
	private MediaFileInfo mediaFileInfo;


	@Mock
	File mediaFolder;

	@Mock
	File mediaFile;

	@Mock
	List<String> formatsList;

	String fileSource;

	String mediaFolderPath;

	String container;

	String format;

	String fileName;

	Map<String, File[]> medialFolderMap;

	List<File> filteredList;

	File[] fileArr;

	MediaFilter nonMockfilter;

	@Before
	public void setup()
	{
		mediaFolderPath = "/";
		fileSource = "NFSMount";

		container = "2mdlp";
		format = "1200Wx1200H";
		fileName = "2mdlp_b1_030118_817x252.jpg";

		fileArr = new File[1];
		fileArr[0] = mediaFile;

		medialFolderMap = new HashMap<String, File[]>();
		medialFolderMap.put(mediaFolderPath, fileArr);

		filteredList = new ArrayList<File>();
		filteredList.add(mediaFile);

		doReturn(new Boolean(true)).when(mediaFolder).exists();
		doReturn(new Boolean(true)).when(mediaFolder).isDirectory();
		doReturn(fileName).when(mediaFile).getName();

		mediaImportFacade.setMediaImportService(mediaImportService);
		mediaImportFacade.setFileService(fileService);

		doReturn(applicationContext).when(mediaImportFacade).getApplicationContext();
		doReturn(mediaFileInfo).when(applicationContext).getBean("mediaFileInfo", MediaFileInfo.class);

	}


	@Test
	public void testCheckMediaEmptyMap()
	{
		doReturn(new HashMap<String, File[]>()).when(fileService).getFolderFileInfo(mediaFolderPath);
		doReturn(Collections.EMPTY_LIST).when(mediaImportService).filterFiles(mediaFolderPath, fileArr, mediaImportStats);
		assertTrue(mediaImportFacade.checkMedia(mediaFolderPath, mediaImportStats).size() == 0);
	}


	@Test
	public void testCheckMediaEmpty()
	{

		doReturn(medialFolderMap).when(fileService).getFolderFileInfo(mediaFolderPath);
		doReturn(Collections.EMPTY_LIST).when(mediaImportService).filterFiles(mediaFolderPath, fileArr, mediaImportStats);
		assertTrue(mediaImportFacade.checkMedia(mediaFolderPath, mediaImportStats).size() == 0);
	}

	@Test
	public void testCheckMedia()
	{

		doReturn(medialFolderMap).when(fileService).getFolderFileInfo(mediaFolderPath);
		doReturn(filteredList).when(mediaImportService).filterFiles(mediaFolderPath, fileArr, mediaImportStats);
		assertTrue(mediaImportFacade.checkMedia(mediaFolderPath, mediaImportStats).size() == 1);
	}


	@Test
	public void testImportMedia()
	{

		doReturn(medialFolderMap).when(fileService).getFolderFileInfo(mediaFolderPath);
		doReturn(formatsList).when(mediaImportService).getMediaFormats();
		doReturn(container).when(mediaFileInfo).getContainerName();
		doReturn(format).when(mediaFileInfo).getFormat();

		mediaImportFacade.importMedia(filteredList, mediaImportStats);
	}



	@Test
	public void testImportMediaInvalid()
	{

		doReturn(medialFolderMap).when(fileService).getFolderFileInfo(mediaFolderPath);
		doReturn(formatsList).when(mediaImportService).getMediaFormats();
		doReturn(container).when(mediaFileInfo).getContainerName();
		doReturn(format).when(mediaFileInfo).getFormat();
		try
		{
			doThrow(new FileNotFoundException("Test")).when(mediaImportService).createMediaInStaged(mediaFile, container, format);
		}
		catch (final FileNotFoundException e)
		{
			// YTODO Auto-generated catch block
			//e.printStackTrace();
		}
		mediaImportFacade.importMedia(filteredList, mediaImportStats);

	}


}
