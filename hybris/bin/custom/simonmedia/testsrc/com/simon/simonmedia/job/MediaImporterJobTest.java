/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.job;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import com.simon.simonmedia.facade.MediaImportFacade;
import com.simon.simonmedia.service.MediaFileInfo;
import com.simon.simonmedia.service.MediaFilter;
import com.simon.simonmedia.service.MediaImportService;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaImporterJobTest
{
	@InjectMocks
	@Spy
	MediaImporterJob mediaImporterJob;

	@Mock
	ApplicationContext applicationContext;

	@Mock
	MediaImportService mediaImportService;

	@Mock
	MediaImportFacade mediaImportFacade;

	@Mock
	MediaFilter filter;

	@Mock
	MediaImportStats mediaImportStats;

	@Mock
	private MediaFileInfo mediaFileInfo;

	@Mock
	CronJobModel cronJob;

	@Mock
	File mediaFolder;

	@Mock
	File mediaFile;

	@Mock
	List<String> formatsList;

	@Mock
	List<File> importedFilesList;


	@Mock
	Map<String, Date> fileTimeStampMap;

	String fileSource;

	String mediaFolderPath;

	String container;

	String format;

	String fileName;

	Map<String, File[]> medialFolderMap;

	List<File> filteredList;

	File[] fileArr;

	MediaFilter nonMockfilter;

	@Before
	public void setup()
	{
		mediaFolderPath = "/";
		fileSource = "NFSMount";

		container = "2mdlp";
		format = "1200Wx1200H";
		fileName = "2mdlp_b1_030118_817x252.jpg";

		fileArr = new File[1];
		fileArr[0] = mediaFile;

		medialFolderMap = new HashMap<String, File[]>();
		medialFolderMap.put(mediaFolderPath, fileArr);

		filteredList = new ArrayList<File>();
		filteredList.add(mediaFile);

		doReturn(applicationContext).when(mediaImporterJob).getApplicationContext();
		doReturn(mediaFileInfo).when(applicationContext).getBean("mediaFileInfo", MediaFileInfo.class);
		doReturn(mediaImportStats).when(applicationContext).getBean("mediaImportStats", MediaImportStats.class);

		doReturn(new Boolean(true)).when(mediaFolder).exists();
		doReturn(new Boolean(true)).when(mediaFolder).isDirectory();
		doReturn(fileName).when(mediaFile).getName();

		mediaImporterJob.setMediaImportService(mediaImportService);
		mediaImporterJob.setMediaImportFacade(mediaImportFacade);
		mediaImporterJob.setMediaFolderPath(mediaFolderPath);

	}


	@Test
	public void testPerform()
	{

		doReturn(filteredList).when(mediaImportFacade).checkMedia(mediaFolderPath, mediaImportStats);
		doReturn(fileTimeStampMap).when(mediaImportService).getFileTimeStamps(mediaFolderPath);
		doReturn(filteredList).when(mediaImportService).filterFiles(mediaFolderPath, fileArr, mediaImportStats);

		doReturn(formatsList).when(mediaImportService).getMediaFormats();
		doReturn(container).when(mediaFileInfo).getContainerName();
		doReturn(format).when(mediaFileInfo).getFormat();

		mediaImporterJob.perform(cronJob);
	}

	@Test
	public void testPerformInvalid() throws FileNotFoundException
	{

		doReturn(filteredList).when(mediaImportFacade).checkMedia(mediaFolderPath, mediaImportStats);
		doReturn(fileTimeStampMap).when(mediaImportService).getFileTimeStamps(mediaFolderPath);
		doReturn(filteredList).when(mediaImportService).filterFiles(mediaFolderPath, fileArr, mediaImportStats);

		doReturn(formatsList).when(mediaImportService).getMediaFormats();
		doReturn(container).when(mediaFileInfo).getContainerName();
		doReturn(format).when(mediaFileInfo).getFormat();

		doThrow(new ModelSavingException("Test", new Exception())).when(mediaImportService).createMediaInStaged(mediaFile,
				container, format);
		mediaImporterJob.perform(cronJob);
	}


	@Test
	public void testPerformSkip()
	{
		doReturn(filteredList).when(mediaImportFacade).checkMedia(mediaFolderPath, mediaImportStats);
		final Map<String, Date> skipTimeStampMap = new HashMap<String, Date>();
		skipTimeStampMap.put(fileName, new Date());
		doReturn(skipTimeStampMap).when(mediaImportService).getFileTimeStamps(mediaFolderPath);

		doReturn(formatsList).when(mediaImportService).getMediaFormats();
		doReturn(container).when(mediaFileInfo).getContainerName();
		doReturn(format).when(mediaFileInfo).getFormat();

		nonMockfilter = new MediaFilter();
		nonMockfilter.initialize();
		//mediaImporterJob.setFilter(nonMockfilter);

		doReturn(new Boolean(false)).when(mediaFile).isDirectory();
		doReturn(new Long(1)).when(mediaFile).length();

		mediaImporterJob.perform(cronJob);


	}
}
