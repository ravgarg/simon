/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.simonmedia.job.MediaImportStats;
import com.simon.simonmedia.model.MediaImportInfoModel;
import com.simon.simonmedia.service.MediaFilter;
import com.simon.simonmedia.service.MediaImportDao;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaImportServiceImplTest
{
	@InjectMocks
	@Spy
	private MediaImportServiceImpl mediaImportServiceImpl;

	@Mock
	private MediaService mediaService;

	@Mock
	private ModelService modelService;

	@Mock
	private MediaImportDao mediaImportDao;

	@Mock
	private File mediaFile;

	@Mock
	private CatalogVersionModel catalogVersionModel;

	@Mock
	private MediaContainerModel mediaContainer;

	@Mock
	private MediaFormatModel mediaFormat;

	@Mock
	private MediaModel mediaModel;

	@Mock
	private MediaModel media;

	@Mock
	private InputStream inputStream;

	@Mock
	List<File> filesList;

	@Mock
	MediaFilter filter;

	@Mock
	MediaImportInfoModel mediaImportInfo;

	@Mock
	List<MediaImportInfoModel> importInfoList;

	@Mock
	List<MediaFormatModel> formats;

	@Mock
	List<MediaModel> models;

	@Mock
	MediaImportStats mediaImportStats;

	String fileName;
	String catalog;
	String container;
	String source;
	String format;
	String realFileName;
	String folderPath;
	String invalidName;

	int count;

	@Before
	public void setup()
	{
		fileName = "2mdlp_b1_030118_817x252.jpg";
		catalog = "simonContentCatalog";
		container = "2mdlp";
		source = "NFSMount";
		format = "1200Wx1200H";
		realFileName = "2mdlp_b1_030118_817x252.jpg";
		folderPath = "mediaFolder";
		invalidName = "test12.jpg88";
		mediaImportServiceImpl.setFilter(filter);
		doReturn(new Boolean(true)).when(filter).checkFileName(any());
	}

	@Test
	public void testCreateMediaInStagedInvalidArgs()
	{
		doReturn(new Boolean(true)).when(mediaFile).exists();
		doReturn(new Boolean(false)).when(mediaFile).isFile();
		try
		{
			mediaImportServiceImpl.createMediaInStaged(mediaFile, container, format);
		}
		catch (final IllegalArgumentException | FileNotFoundException e)
		{
			//ignnore
		}

		doReturn(new Boolean(false)).when(mediaFile).exists();
		doReturn(new Boolean(false)).when(mediaFile).isFile();
		try
		{
			mediaImportServiceImpl.createMediaInStaged(mediaFile, container, format);
		}
		catch (final IllegalArgumentException | FileNotFoundException e)
		{
			//ignnore
		}

		doReturn(new Boolean(true)).when(mediaFile).exists();
		doReturn(new Boolean(true)).when(mediaFile).isFile();
		doReturn(new Boolean(false)).when(filter).checkFileName(any());
		try
		{
			mediaImportServiceImpl.createMediaInStaged(mediaFile, container, format);
		}
		catch (final IllegalArgumentException | FileNotFoundException e)
		{
			//ignnore
		}

	}



	@Test
	public void testCreateMediaInStagedMedia()
	{
		doReturn(new Boolean(true)).when(mediaFile).exists();
		doReturn(new Boolean(true)).when(mediaFile).isFile();
		when(mediaFile.getName()).thenReturn(fileName);

		mediaImportServiceImpl.setCatalog(catalog);
		mediaImportServiceImpl.setMediaImportDao(mediaImportDao);
		when(mediaImportDao.getCatalogVersion(catalog)).thenReturn(catalogVersionModel);
		when(mediaImportDao.getMediaContainer(container, catalog)).thenReturn(null);

		mediaImportServiceImpl.setMediaService(mediaService);
		when(mediaService.getFormat(format)).thenReturn(mediaFormat);
		when(mediaFormat.getQualifier()).thenReturn(format);

		mediaImportServiceImpl.setModelService(modelService);
		when(modelService.create(MediaContainerModel.class)).thenReturn(mediaContainer);
		when(modelService.create(MediaModel.class)).thenReturn(media);

		when(mediaImportDao.getMedia(catalog, fileName, mediaFormat)).thenReturn(null);
		try
		{
			doReturn(inputStream).when(mediaImportServiceImpl).getInputStream(mediaFile);
			mediaImportServiceImpl.createMediaInStaged(mediaFile, container, format);
		}
		catch (final FileNotFoundException e)
		{
			//ignore
		}

	}

	@Test
	public void testCreateMediaInStagedMediaExists()
	{
		doReturn(new Boolean(true)).when(mediaFile).exists();
		doReturn(new Boolean(true)).when(mediaFile).isFile();
		when(mediaFile.getName()).thenReturn(fileName);

		mediaImportServiceImpl.setCatalog(catalog);
		mediaImportServiceImpl.setMediaImportDao(mediaImportDao);
		when(mediaImportDao.getCatalogVersion(catalog)).thenReturn(catalogVersionModel);
		when(mediaImportDao.getMediaContainer(container, catalog)).thenReturn(mediaContainer);

		mediaImportServiceImpl.setMediaService(mediaService);
		when(mediaService.getFormat(format)).thenReturn(mediaFormat);

		mediaImportServiceImpl.setModelService(modelService);

		when(mediaImportDao.getMedia(catalog, fileName, mediaFormat)).thenReturn(media);
		try
		{
			doReturn(inputStream).when(mediaImportServiceImpl).getInputStream(mediaFile);
			mediaImportServiceImpl.createMediaInStaged(mediaFile, container, format);
		}
		catch (final FileNotFoundException e)
		{
			e.printStackTrace();
			//ignore
		}

	}

	@Test
	public void testSaveMediaImportInfo()
	{
		final List<File> files = new ArrayList<File>();
		files.add(mediaFile);
		doReturn(new Long(System.currentTimeMillis())).when(mediaFile).lastModified();
		doReturn(new Boolean(false)).when(mediaFile).isDirectory();
		doReturn("mediaFolder").when(mediaFile).getParent();
		//mediaImportServiceImpl.saveMediaImportInfo(source, files);
	}


	@Test
	public void testGetFileTimeStamps()
	{
		final List<MediaImportInfoModel> importInfoList = new ArrayList<MediaImportInfoModel>();
		importInfoList.add(mediaImportInfo);
		mediaImportServiceImpl.getFileTimeStamps(folderPath);
	}


	@Test
	public void testMediaFormats()
	{

		final List<MediaFormatModel> formats = new ArrayList<MediaFormatModel>();
		formats.add(mediaFormat);
		when(mediaFormat.getQualifier()).thenReturn(format);
		mediaImportServiceImpl.getMediaFormats();
	}


	@Test
	public void filterFilesTest()
	{
		doReturn(new Boolean(false)).when(mediaFile).isDirectory();
		doReturn(new Long(1)).when(mediaFile).length();
		doReturn(fileName).when(mediaFile).getName();
		final File[] files = new File[1];
		files[0] = mediaFile;
		mediaImportServiceImpl.filterFiles(folderPath, files, mediaImportStats);
	}



	@Test
	public void filterFilesInvalidFileTest()
	{
		doReturn(new Boolean(false)).when(mediaFile).isDirectory();
		doReturn(new Long(1)).when(mediaFile).length();
		doReturn(invalidName).when(mediaFile).getName();
		final File[] files = new File[1];
		files[0] = mediaFile;
		mediaImportServiceImpl.filterFiles(folderPath, files, mediaImportStats);
	}

	@Test
	public void filterFilesInvalidSizeTest()
	{
		doReturn(new Boolean(false)).when(mediaFile).isDirectory();
		doReturn(new Long(0)).when(mediaFile).length();
		doReturn(invalidName).when(mediaFile).getName();
		final File[] files = new File[1];
		files[0] = mediaFile;
		mediaImportServiceImpl.filterFiles(folderPath, files, mediaImportStats);
	}

	public void testUpdateFileCount()
	{
		final CronJobModel cronJob = Mockito.mock(CronJobModel.class);
		mediaImportServiceImpl.updateFileCount(count, cronJob);
	}
}


