/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for FileServiceImpl
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FileServiceImplTest
{
	@InjectMocks
	@Spy
	private FileServiceImpl fileServiceImpl;

	@Mock
	File folder;

	@Mock
	File childFolder;


	@Before
	public void setup()
	{
		fileServiceImpl.setFolderDepth(5);
	}


	@Test(expected=IllegalArgumentException.class)
	public void getFolderFileInfoArgTest()
	{
		doReturn(folder).when(fileServiceImpl).getFile(any());

			fileServiceImpl.getFolderFileInfo("test");

	}
	

	@Test(expected=IllegalArgumentException.class)
	public void getFolderFileInfoArgTest2()
	{
		doReturn(new Boolean(true)).when(folder).exists();

			fileServiceImpl.getFolderFileInfo("test");
	}	


	@Test
	public void getFolderFileInfoChildTest()
	{
		doReturn(folder).when(fileServiceImpl).getFile(any());
		doReturn(new Boolean(true)).when(folder).exists();
		doReturn(new Boolean(true)).when(folder).isDirectory();
		final File[] fileArr =
		{ childFolder };
		doReturn(fileArr).when(folder).listFiles();
		doReturn(new Boolean(true)).when(childFolder).isDirectory();
		assertNotNull(fileServiceImpl.getFolderFileInfo("test"));
	}
}
