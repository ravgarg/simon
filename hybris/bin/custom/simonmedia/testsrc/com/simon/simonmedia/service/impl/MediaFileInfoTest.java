/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.simonmedia.service.MediaFileInfo;


/**
 * Test class for MediaFileInfo
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaFileInfoTest
{
	@InjectMocks
	@Spy
	private MediaFileInfo mediaFileInfo;

	List<String> formatsList;

	private Map<Integer, String> oobFormats;

	/**
	 *
	 */
	@Before
	public void setup()
	{
		oobFormats = new TreeMap<Integer, String>();
		oobFormats.put(new Integer(1200), "widescreen");
		oobFormats.put(new Integer(992), "desktop");
		oobFormats.put(new Integer(768), "tablet");
		oobFormats.put(new Integer(480), "mobile");
		mediaFileInfo.setImageFormats(oobFormats);

	}


	private void addImageFormats()
	{
		formatsList = new ArrayList<String>();
		formatsList.add("580Wx884H*2");
		formatsList.add("325Wx495H");
		formatsList.add("150Wx229H");
		formatsList.add("60Wx60H");
		formatsList.add("desktop");
		formatsList.add("mobile");
		formatsList.add("tablet");
		formatsList.add("widescreen");
		mediaFileInfo.addImageFormats(formatsList);
	}

	@Test
	public void addImageFormatTest()
	{
		addImageFormats();
	}


	@Test
	public void getFormatTest()
	{
		addImageFormats();
		mediaFileInfo.setFileName("WLP_B7B_030118_208X90.png");
		final String format = mediaFileInfo.getFormat();
		assertEquals("325Wx495H", format);

	}

	@Test
	public void getFormatClosestTest()
	{
		addImageFormats();

		mediaFileInfo.setFileName("2mdlp_b1_030118_817x252.jpg");
		String format = mediaFileInfo.getFormat();
		assertEquals("desktop", format);

		mediaFileInfo.setFileName("HP_H2_030118_1440x555.jpg");
		format = mediaFileInfo.getFormat();
		assertEquals("widescreen", format);
	}
}
