/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.simonmedia.enums.MediaImportInfoStatus;
import com.simon.simonmedia.model.MediaImportInfoModel;


/**
 * Integration tests for MediaImportDao
 *
 */


@IntegrationTest
public class MediaImportDaoIntegrationTest extends ServicelayerTransactionalTest
{
	@Resource
	private MediaImportDaoImpl mediaImportDao;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private CatalogVersionService catalogVersionService;	

	@Resource
	private ModelService modelService;

	@Before
	public void setup() throws ImpExException
	{
		mediaImportDao.setFlexibleSearchService(flexibleSearchService);
		mediaImportDao.setVersion("Staged");
		createFormats();
		importCsv("/simonmedia/test/TestCatalog.impex","UTF-8");
	}
	
	private void createFormats() {
		MediaFormatModel mediaFormatModel = this.modelService.create(MediaFormatModel.class);
		mediaFormatModel.setName("widescreen");
		mediaFormatModel.setQualifier("widescreen" + System.currentTimeMillis());
		this.modelService.save(mediaFormatModel);
		
		mediaFormatModel = this.modelService.create(MediaFormatModel.class);
		mediaFormatModel.setName("desktop");
		mediaFormatModel.setQualifier("desktop" + System.currentTimeMillis());
		this.modelService.save(mediaFormatModel);		
	}
	
	@Test
	public void getCatalogVersionTest()
	{
		assertNull(mediaImportDao.getCatalogVersion("simonTestContentCatalogNot"));
	}

	@Test
	public void getCatalogVersionTestSearchResult()
	{
		assertNotNull(mediaImportDao.getCatalogVersion("simonTestContentCatalog"));
	}

	@Test
	public void getMediaContainerTest()
	{
		assertNull(mediaImportDao.getMediaContainer("WB" + System.currentTimeMillis(), "simonTestContentCatalog"));
	}

	@Test
	public void getMediaTest()
	{
		final MediaFormatModel mediaFormat = new MediaFormatModel();
		mediaFormat.setQualifier("widescreen");

		final String mediaFileName = "HP_hero-image-e_1440x555.png" + System.currentTimeMillis();
		assertNull(mediaImportDao.getMedia("simonTestContentCatalog", mediaFileName, mediaFormat));
	}

	@Test
	public void getMediaContainerAndMediaTestSearchResult()
	{

		final List<MediaFormatModel> mediaFormatsList = mediaImportDao.getMediaFormats();
		assertNotNull(mediaFormatsList);
		assertTrue(mediaFormatsList.size() > 1);
		final MediaFormatModel mediaFormat = mediaFormatsList.get(0);

		final CatalogVersionModel catalogVersion = mediaImportDao.getCatalogVersion("simonTestContentCatalog");
		assertNotNull(catalogVersion);

		final MediaContainerModel mediaContainerModel = modelService.create(MediaContainerModel.class);
		mediaContainerModel.setCatalogVersion(catalogVersion);
		final String containerName = "HP" + System.currentTimeMillis();
		mediaContainerModel.setQualifier(containerName);

		final String mediaFileName = "HP_hero-image" + System.currentTimeMillis() + "-1e_1440x555.png";

		final MediaModel media = modelService.create(MediaModel.class);
		media.setRealFileName(mediaFileName);
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("/");
		stringBuilder.append(mediaFormat.getQualifier());
		stringBuilder.append("/");
		stringBuilder.append(mediaFileName);
		media.setCode(stringBuilder.toString());
		media.setCatalogVersion(catalogVersion);
		media.setMediaContainer(mediaContainerModel);
		media.setMediaFormat(mediaFormat);
		final String fileExtension = mediaFileName.substring(mediaFileName.lastIndexOf('.'), mediaFileName.length());
		media.setMime("image/" + fileExtension);
		
        final List<MediaModel> medias = new ArrayList<>();
        medias.add(media);
        mediaContainerModel.setMedias(medias);
		modelService.saveAll(mediaContainerModel,media);

		assertNotNull(mediaImportDao.getMediaContainer(containerName, "simonTestContentCatalog"));
		
		assertNotNull(mediaImportDao.getMedia("simonTestContentCatalog", mediaFileName, mediaFormat));
	}

	@Test
	public void getMediaImportInfoTest()
	{
		assertEquals(0, (mediaImportDao.getMediaImportInfo("bak" + System.currentTimeMillis())).size());
	}


	public void getMediaImportInfoTestSearchResult()
	{
		final String folderPath = "images" + System.currentTimeMillis();
		final MediaImportInfoModel importInfoModel = modelService.create(MediaImportInfoModel.class);
		importInfoModel.setFolderPath(folderPath);
		importInfoModel.setFileName("file");
		importInfoModel.setFileTimeStamp(new Date());
		importInfoModel.setSource("NFSMount");
		importInfoModel.setStatus(MediaImportInfoStatus.MEDIA_IMPORTED);
		modelService.save(importInfoModel);

		final List<MediaImportInfoModel> importInfoModelList = mediaImportDao.getMediaImportInfo(folderPath);
		assertNotNull(importInfoModelList);
		assertEquals(1, importInfoModelList.size());
	}

	@Test
	public void getMediaFormats()
	{
		assertNotNull(mediaImportDao.getMediaFormats());
		assertTrue(mediaImportDao.getMediaFormats().size() > 1);
	}
}
