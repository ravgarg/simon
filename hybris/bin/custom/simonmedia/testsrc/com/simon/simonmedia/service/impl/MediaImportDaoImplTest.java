/*
 * Copyright (c) 2018 Simon Property Group Inc.  All rights reserved.
 *
 * This software is the confidential and proprietary information of Simon Property Group Inc
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with Simon Property Group Inc.
 */

package com.simon.simonmedia.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.simonmedia.constants.SimonmediaConstants;
import com.simon.simonmedia.model.MediaImportInfoModel;


/**
 * Unit tests for MediaImportDaoImpl
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaImportDaoImplTest
{
	@InjectMocks
	@Spy
	private MediaImportDaoImpl mediaImportDaoImpl;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private FlexibleSearchQuery query;

	@Mock
	private List<MediaContainerModel> containerList;

	@Mock
	private List<CatalogVersionModel> catalogVersionList;

	@Mock
	private List<MediaModel> mediaList;

	@Mock
	private List<MediaImportInfoModel> importInfoList;

	@Mock
	private MediaImportInfoModel importInfo;

	@Mock
	private MediaModel media;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private MediaContainerModel containerModel;

	@Mock
	private SearchResult searchResult;

	@Mock
	private MediaFormatModel mediaFormat;

	private final String mediaDimensions = "1200Wx1200H";



	@Before
	public void setup()
	{
		doReturn(searchResult).when(flexibleSearchService).search(query);
		doReturn(mediaDimensions).when(mediaFormat).getQualifier();
	}

	@Test
	public void getCatalogVersionTest()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.CATALOG_QUERY);
		doReturn(catalogVersionList).when(searchResult).getResult();
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNull(mediaImportDaoImpl.getCatalogVersion("simonContentCatalog"));
	}

	@Test
	public void getCatalogVersionTestSearchResult()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.CATALOG_QUERY);
		doReturn(catalogVersionList).when(searchResult).getResult();
		doReturn(catalogVersion).when(catalogVersionList).get(0);

		catalogVersionList.add(catalogVersion);
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNotNull(mediaImportDaoImpl.getCatalogVersion("simonContentCatalog"));
	}

	@Test
	public void getMediaContainerTest()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.CONTAINER_QUERY);
		doReturn(mediaList).when(searchResult).getResult();
		mediaList.add(media);
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNull(mediaImportDaoImpl.getMediaContainer("WB", "simonContentCatalog"));
	}

	@Test
	public void getMediaContainerTestSearchResult()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.CONTAINER_QUERY);
		doReturn(containerList).when(searchResult).getResult();
		doReturn(containerModel).when(containerList).get(0);

		containerList.add(containerModel);
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNotNull(mediaImportDaoImpl.getMediaContainer("WB", "simonContentCatalog"));
	}

	@Test
	public void getMediaTest()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.MEDIA_QUERY);
		doReturn(mediaList).when(searchResult).getResult();
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNull(mediaImportDaoImpl.getMedia("WB", "simonContentCatalog", mediaFormat));
	}

	@Test
	public void getMediaTestSearchResult()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.MEDIA_QUERY);
		doReturn(mediaList).when(searchResult).getResult();
		doReturn(media).when(mediaList).get(0);

		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNotNull(mediaImportDaoImpl.getMedia("WB", "simonContentCatalog", mediaFormat));
	}


	@Test
	public void getMediaImportInfoTestSearchResult()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.IMPORT_QUERY_BY_FOLDER);
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.IMPORT_QUERY_BY_FILE_NAME);
		doReturn(importInfoList).when(searchResult).getResult();
		doReturn(importInfo).when(mediaList).get(0);

		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		mediaImportDaoImpl.getMediaImportInfo("WB");
		assertNotNull(mediaImportDaoImpl.getMediaImportInfo("WB", "test.png"));
	}


	@Test
	public void getMediaFormats()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.MEDIA_FORMAT_QUERY);
		doReturn(new ArrayList<MediaFormatModel>()).when(searchResult).getResult();
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		assertNotNull(mediaImportDaoImpl.getMediaFormats());
	}


	@Test
	public void getMediaModel()
	{
		doReturn(query).when(mediaImportDaoImpl).getFlexibleSearchQuery(SimonmediaConstants.MEDIA_REALFILENAME_QUERY);
		doReturn(new ArrayList<MediaModel>()).when(searchResult).getResult();
		mediaImportDaoImpl.setFlexibleSearchService(flexibleSearchService);
		mediaImportDaoImpl.setVersion("Staged");
		mediaImportDaoImpl.getMedia("simonContentCatalog", "Test");
	}


}
