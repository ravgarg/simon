package com.simon.cockpits.constants;

import com.simon.cockpits.constants.GeneratedSimonCockpitsConstants;
@SuppressWarnings("PMD")
public class SimonCockpitsConstants extends GeneratedSimonCockpitsConstants
{
	public static final String EXTENSIONNAME = "simoncockpits";
	
	private SimonCockpitsConstants()
	{
		//empty
	}
	
	
}
