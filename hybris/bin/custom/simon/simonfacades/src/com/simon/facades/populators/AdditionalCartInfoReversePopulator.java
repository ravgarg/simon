package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.CreateCartConfirmationRetailerDTO;


/**
 * populate additionalCartInfoModel from CreateCartConfirmationDTO which have information of retailers, added products
 * and failed products.
 *
 */
public class AdditionalCartInfoReversePopulator implements Populator<CreateCartConfirmationDTO, AdditionalCartInfoModel>
{
	private Converter<CreateCartConfirmationRetailerDTO, RetailersInfoModel> retailersInfoReverseConverter;
	@Resource
	private ModelService modelService;

	/**
	 * Method populate additionalCartInfoModel from CreateCartConfirmationDTO which have information of retailers, added
	 * products and failed products.
	 */
	@Override
	public void populate(final CreateCartConfirmationDTO source, final AdditionalCartInfoModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setCartId(StringUtils.isNotEmpty(source.getId()) ? source.getId() : source.getCartId());
		target.setErrorCode(source.getErrorCode());
		target.setErrorMessage(source.getErrorMessage());
		target.setCreateCartInvocationTime(source.getCreateCartInvocationTime());

		if (StringUtils.isEmpty(source.getErrorCode()))
		{
			final List<RetailersInfoModel> newRetailersInfoModelList = new ArrayList<>();
			newRetailersInfoModelList.addAll(getRetailersInfoReverseConverter().convertAll(source.getRetailers()));
			final List<RetailersInfoModel> oldRetailerInfoList = target.getRetailersInfo();
			if (CollectionUtils.isNotEmpty(oldRetailerInfoList) && CollectionUtils.isNotEmpty(newRetailersInfoModelList))
			{
				updateCurrentList(newRetailersInfoModelList, oldRetailerInfoList);
			}
			target.setRetailersInfo(newRetailersInfoModelList);
		}
		modelService.save(target);
	}

	private void updateCurrentList(final List<RetailersInfoModel> newRetailersInfoModelList,
			final List<RetailersInfoModel> oldRetailerInfoList)
	{
		newRetailersInfoModelList.stream()
				.forEach(newRetailerInfo -> oldRetailerInfoList.stream()
						.filter(oldRetailerInfo -> newRetailerInfo.getRetailerId().equalsIgnoreCase(oldRetailerInfo.getRetailerId()))
						.findFirst().ifPresent(oldRetailerInfo -> {
							newRetailerInfo.setAppliedSalesTax(oldRetailerInfo.getAppliedSalesTax());
							newRetailerInfo.setAppliedTaxRate(oldRetailerInfo.getAppliedTaxRate());
							newRetailerInfo.setTotalSalesTax(oldRetailerInfo.getTotalSalesTax());
							newRetailerInfo.setTotalShippingPrice(oldRetailerInfo.getTotalShippingPrice());
							updateShippingDetails(newRetailerInfo, oldRetailerInfo);
						}));

		modelService.saveAll(newRetailersInfoModelList);
		modelService.removeAll(oldRetailerInfoList);
	}

	private void updateShippingDetails(final RetailersInfoModel newRetailersInfoModel,
			final RetailersInfoModel oldRetailerInfoModel)
	{
		for (final ShippingDetailsModel oldInfo : oldRetailerInfoModel.getShippingDetails())
		{
			for (final ShippingDetailsModel newInfo : newRetailersInfoModel.getShippingDetails())
			{
				if (newInfo.getCode().equalsIgnoreCase(oldInfo.getCode()) && (null != oldInfo.getPrice()))
				{
					newInfo.setPrice(oldInfo.getPrice());
					modelService.save(newInfo);
					break;
				}
			}

		}

	}

	/**
	 * @return retailersInfoReverseConverter
	 */
	public Converter<CreateCartConfirmationRetailerDTO, RetailersInfoModel> getRetailersInfoReverseConverter()
	{
		return retailersInfoReverseConverter;
	}


	/**
	 * @param retailersInfoReverseConverter
	 */
	public void setRetailersInfoReverseConverter(
			final Converter<CreateCartConfirmationRetailerDTO, RetailersInfoModel> retailersInfoReverseConverter)
	{
		this.retailersInfoReverseConverter = retailersInfoReverseConverter;
	}

}
