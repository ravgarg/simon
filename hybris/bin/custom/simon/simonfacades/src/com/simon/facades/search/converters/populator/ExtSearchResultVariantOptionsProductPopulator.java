package com.simon.facades.search.converters.populator;


import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantOptionsProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.util.localization.Localization;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;

import com.google.common.collect.Lists;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.search.plp.PriceRangeData;



/**
 * The Class SimonSearchResultVariantOptionsProductPopulator. This populator extends
 * SearchResultVariantOptionsProductPopulator and call it populate method which populate all basic product details in
 * ProductData then it populate remaining attribute.
 */
public class ExtSearchResultVariantOptionsProductPopulator extends SearchResultVariantOptionsProductPopulator
{
	private static final Logger LOG = Logger.getLogger(ExtSearchResultVariantOptionsProductPopulator.class);
	private static final String SALE_PRICE = "SP";

	private static final String LIST_PRICE = "LP";

	private static final String MSRP = "MSRP";

	private static final String APPLICABLE_PRICE = "AP";

	private static final String PLUM_PRICE = "PP";

	/** The configuration service. */
	@Resource
	private ConfigurationService configurationService;

	/** The product data url resolver. */
	@Resource
	private UrlResolver<ProductData> productDataUrlResolver;

	private final DecimalFormat decimalFormat = new DecimalFormat("#.00");

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantOptionsProductPopulator#populate
	 * (de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData,
	 * de.hybris.platform.commercefacades.product.data.ProductData)
	 */
	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		superPopulator(source, target);
		if (CollectionUtils.isNotEmpty(source.getVariants()))
		{
			((ArrayList<SearchResultValueData>) source.getVariants()).add(SimonCoreConstants.ZERO_INT, source);
		}
		else
		{
			final ArrayList<SearchResultValueData> variantList = new ArrayList<>();
			variantList.add(source);
			source.setVariants(variantList);
		}
		populateVariantData(source, target);
		populatePotentialPromotionData(source, target);
	}

	/**
	 * Method to get localized label from locale properties.
	 *
	 * @param key
	 *           the key
	 * @return Localized title string.
	 */
	protected String getLocalizedString(final String key)
	{
		return Localization.getLocalizedString(key);
	}

	/**
	 * this method populate product data from searchResultValueData Populate variant data.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populateVariantData(final SearchResultValueData source, final ProductData target)
	{
		final List<VariantOptionData> productVariantDataList = new ArrayList<>();
		final Map<String, VariantOptionData> variantsWithUniqueColor = new LinkedHashMap<>();
		final Map<String, Boolean> mapOfVariantAndStockFlag = new HashMap<>();
		final PriceRangeData priceRangeData = new PriceRangeData();
		priceRangeData.setApplicablePriceSet(new TreeSet<>());
		priceRangeData.setListPriceSet(new TreeSet<>());
		priceRangeData.setMsrpPriceSet(new TreeSet<>());
		priceRangeData.setPercentageOffSet(new TreeSet<>());
		priceRangeData.setPlumPriceSet(new TreeSet<>());
		priceRangeData.setSalePriceSet(new TreeSet<>());
		for (final SearchResultValueData searchResultVariantValueData : source.getVariants())
		{
			VariantOptionData variantOptionData;
			Boolean variantStockFlag = this.<Boolean> getValue(searchResultVariantValueData, "inStockFlag");
			variantStockFlag = variantStockFlag != null ? variantStockFlag : false;
			final String colorName = this.<String> getValue(searchResultVariantValueData, "colorName");
			if (!variantsWithUniqueColor.containsKey(colorName) || !mapOfVariantAndStockFlag.get(colorName))
			{
				variantOptionData = populateVariantOptionData(searchResultVariantValueData, priceRangeData);
				variantsWithUniqueColor.put(colorName, variantOptionData);
				mapOfVariantAndStockFlag.put(colorName, variantStockFlag);
			}
		}
		productVariantDataList.addAll(variantsWithUniqueColor.values());
		target.setVariantOptions(productVariantDataList);
		setPriceRangeData(target, priceRangeData);

	}

	/**
	 * This method set Price ranges in target.
	 *
	 * @param target
	 *           the target
	 * @param priceRangeData
	 *           the price range data
	 */
	private void setPriceRangeData(final ProductData target, final PriceRangeData priceRangeData)
	{
		final String currencySymbol = getCommonI18NService().getCurrentCurrency().getSymbol();

		setPriceRange(target, priceRangeData.getApplicablePriceSet(), currencySymbol, APPLICABLE_PRICE);
		setPriceRange(target, priceRangeData.getPlumPriceSet(), currencySymbol, PLUM_PRICE);
		setPercentageSavingOff(target, priceRangeData);
		setActualPriceRange(target, priceRangeData.getMsrpPriceSet(), currencySymbol, MSRP, SimonCoreConstants.MSRP_STRING);
		setActualPriceRange(target, priceRangeData.getListPriceSet(), currencySymbol, LIST_PRICE, SimonCoreConstants.LIST_STRING);
		setActualPriceRange(target, priceRangeData.getSalePriceSet(), currencySymbol, SALE_PRICE, SimonCoreConstants.SALE_STRING);
	}

	/**
	 * Sets the actual price range.
	 *
	 * @param target
	 *           the target
	 * @param priceSet
	 *           the price set
	 * @param currencySymbol
	 *           the currency symbol
	 * @param priceType
	 *           the price type
	 */
	private void setActualPriceRange(final ProductData target, final TreeSet<Double> priceSet, final String currencySymbol,
			final String priceType, final String priceTypeString)
	{
		String priceRangeString = null;
		if (CollectionUtils.isNotEmpty(priceSet))
		{
			if (checkIfNoPriceRange(priceSet))
			{
				priceRangeString = priceTypeString + currencySymbol + decimalFormat.format(priceSet.first());
			}
			else
			{
				priceRangeString = priceTypeString + currencySymbol + decimalFormat.format(priceSet.first())
						+ SimonCoreConstants.HYPHEN + currencySymbol + decimalFormat.format(priceSet.last());
			}
			setPriceRangeString(priceType, priceRangeString, target);
		}
	}

	/**
	 * Sets the percentage saving off.
	 *
	 * @param target
	 *           the target
	 * @param priceRangeData
	 *           the price range data
	 */
	private void setPercentageSavingOff(final ProductData target, final PriceRangeData priceRangeData)
	{
		if (CollectionUtils.isNotEmpty(priceRangeData.getPercentageOffSet()))
		{
			String percentOff;
			if (checkIfNoPriceRange(priceRangeData.getPercentageOffSet()))
			{
				percentOff = priceRangeData.getPercentageOffSet().first().intValue()
						+ getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
			}
			else
			{
				percentOff = getLocalizedString(SimonCoreConstants.PERCENT_SAVING_PRE_MESSAGE)
						+ priceRangeData.getPercentageOffSet().last().intValue()
						+ getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);

			}
			target.setPercentageOffRange(percentOff);
		}
	}

	/**
	 * Sets the price range.
	 *
	 * @param target
	 *           the target
	 * @param priceRangeSet
	 *           the price range set
	 * @param currencySymbol
	 *           the currency symbol
	 * @param priceType
	 *           the price type
	 */
	private void setPriceRange(final ProductData target, final TreeSet<Double> priceRangeSet, final String currencySymbol,
			final String priceType)
	{
		String priceRangeString = null;
		if (CollectionUtils.isNotEmpty(priceRangeSet))
		{
			if (checkIfNoPriceRange(priceRangeSet))
			{
				priceRangeString = currencySymbol + decimalFormat.format(priceRangeSet.first());
			}
			else
			{
				priceRangeString = createPriceRangeString(priceRangeSet, currencySymbol);
			}

			setPriceRangeString(priceType, priceRangeString, target);
		}
	}

	/**
	 * Sets the price range string.
	 *
	 * @param priceType
	 *           the price type
	 * @param priceRangeString
	 *           the price range string
	 * @param target
	 *           the target
	 */
	private void setPriceRangeString(final String priceType, final String priceRangeString, final ProductData target)
	{
		switch (priceType)
		{
			case PLUM_PRICE:
				target.setPlumPriceRange(priceRangeString);
				break;
			case APPLICABLE_PRICE:
				target.setApplicablePriceRange(priceRangeString);
				break;
			case MSRP:
				target.setMsrpPriceRange(priceRangeString);
				break;
			case LIST_PRICE:
				target.setListPriceRange(priceRangeString);
				break;
			case SALE_PRICE:
				target.setSalePriceRange(priceRangeString);
				break;
			default:
				break;
		}

	}

	/**
	 * Creates the price range string.
	 *
	 * @param priceRange
	 *           the price range
	 * @param currencySymbol
	 *           the currency symbol
	 * @return the string
	 */
	private String createPriceRangeString(final TreeSet<Double> priceRange, final String currencySymbol)
	{
		return currencySymbol + decimalFormat.format(priceRange.first()) + SimonCoreConstants.HYPHEN + currencySymbol
				+ decimalFormat.format(priceRange.last());
	}

	/**
	 * Check if no price range.
	 *
	 * @param priceRange
	 *           the price range
	 * @return true, if successful
	 */
	private boolean checkIfNoPriceRange(final TreeSet<Double> priceRange)
	{
		return priceRange.size() == 1 || priceRange.first().equals(priceRange.last());
	}

	/**
	 * This method Populate variantOptionData and call populatePriceAndPercentOffData,populateUrl method to create url
	 * and populate plum price and applicable price. Populate variant option data.
	 *
	 * @param searchResultValueData
	 *           the search result value data
	 * @param priceRangeData
	 *           the price range data
	 * @return the variant option data
	 */
	private VariantOptionData populateVariantOptionData(final SearchResultValueData searchResultValueData,
			final PriceRangeData priceRangeData)
	{

		final String baseProductName = this.<String> getValue(searchResultValueData, "baseProductName");
		final String baseProductDesignerName = this.<String> getValue(searchResultValueData, "baseProductDesignerName");
		final String colorName = this.<String> getValue(searchResultValueData, "colorName");

		final StringBuilder altText = new StringBuilder();
		if (StringUtils.isNotEmpty(baseProductDesignerName))
		{
			altText.append(baseProductDesignerName);
		}
		if (StringUtils.isNotEmpty(colorName))
		{
			altText.append(SimonCoreConstants.SPACE).append(colorName);
		}
		if (StringUtils.isNotEmpty(baseProductName))
		{
			altText.append(SimonCoreConstants.SPACE).append(baseProductName);
		}

		final VariantOptionData variantOptionData = new VariantOptionData();
		variantOptionData.setBaseProductRetailerId(this.<String> getValue(searchResultValueData, "baseProductRetailerId"));
		variantOptionData.setBaseProductRetailerName(this.<String> getValue(searchResultValueData, "baseProductRetailerName"));
		variantOptionData.setBaseProductDesignerName(baseProductDesignerName);
		variantOptionData.setBaseProductDesignerURL(this.<String> getValue(searchResultValueData, "baseProductDesignerUrl"));
		final List<String> retailerUrl = this.<ArrayList> getValue(searchResultValueData, "baseProductRetailerUrl");
		if (CollectionUtils.isNotEmpty(retailerUrl))
		{
			variantOptionData.setBaseProductRetailerURL(retailerUrl.get(0));
		}
		variantOptionData.setBaseProductCode(this.<String> getValue(searchResultValueData, "baseProductCode"));
		variantOptionData.setBaseProductName(baseProductName);
		variantOptionData.setBaseProductDescription(this.<String> getValue(searchResultValueData, "baseProductDescription"));
		variantOptionData.setColorName(colorName);
		variantOptionData.setColorID(this.<String> getValue(searchResultValueData, "colorId"));
		variantOptionData.setSwatchImage(this.<String> getValue(searchResultValueData, "colorSwatchImage"));
		variantOptionData.setCode(this.<String> getValue(searchResultValueData, "code"));
		final Object inStockFlag = getValue(searchResultValueData, "inStockFlag");
		variantOptionData.setInStockFlag(inStockFlag == null ? Boolean.FALSE : (Boolean) inStockFlag);
		final Map<String, Pair<Double, Boolean>> variantPricemap = new HashMap<>();
		variantOptionData.setVariantPrice(variantPricemap);
		populatePriceAndPercentOffData(searchResultValueData, variantOptionData, priceRangeData);
		populateUrl(searchResultValueData, variantOptionData);
		variantOptionData.setVariantImage(populateImageData(searchResultValueData, altText.toString()));
		return variantOptionData;
	}

	/**
	 * Super populator.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	protected void superPopulator(final SearchResultValueData source, final ProductData target)
	{
		super.populate(source, target);
	}

	/**
	 * This method populate image data Populate image data.
	 *
	 * @param source
	 *           the source
	 * @param altText
	 * @return the list
	 */
	private List<ImageData> populateImageData(final SearchResultValueData source, final String altText)
	{
		final List<ImageData> imageDataList = new ArrayList<>();
		final Map<String, Map<String, String>> mediaContainerToImagesMap = new HashMap<>();
		final List<String> imagesListFromSolr = (List<String>) getValue(source, IndexedKeys.VARIANT_PRODUCT_IMAGES);
		if (imagesListFromSolr != null)
		{
			for (final String imageFromSolr : imagesListFromSolr)
			{
				final Map<String, String> formatToImageMap = new HashMap<>();
				final String[] imagePerMediaFormat = StringUtils.split(imageFromSolr, SimonCoreConstants.PIPE);
				final String[] mediaFormatPerMC = StringUtils.split(imagePerMediaFormat[SimonCoreConstants.ZERO_INT],
						SimonCoreConstants.UNDERSCORE);
				formatToImageMap.put(mediaFormatPerMC[SimonCoreConstants.ZERO_INT],
						imagePerMediaFormat[SimonCoreConstants.FIRST_INT]);

				if (mediaContainerToImagesMap.containsKey(mediaFormatPerMC[SimonCoreConstants.FIRST_INT]))
				{
					mediaContainerToImagesMap.get(mediaFormatPerMC[SimonCoreConstants.FIRST_INT]).putAll(formatToImageMap);
				}
				else
				{
					mediaContainerToImagesMap.put(mediaFormatPerMC[SimonCoreConstants.FIRST_INT], formatToImageMap);
				}

			}
		}
		final Map<String, ImageData> imageDataMap = new HashMap<>();
		populateProductImagesData(mediaContainerToImagesMap, imageDataMap);
		final ImageData desktopImageData = imageDataMap.get(SimonCoreConstants.DESKTOP_PRODUCT_IMAGE_FORMAT);
		if (null != desktopImageData)
		{
			desktopImageData.setFormat(SimonCoreConstants.DESKTOP_PRODUCT_IMAGE_TYPE);
			desktopImageData.setAltText(altText);
			imageDataList.add(desktopImageData);
		}
		final ImageData mobileImageData = imageDataMap.get(SimonCoreConstants.MOBILE_PRODUCT_IMAGE_FORMAT);
		if (null != mobileImageData)
		{
			mobileImageData.setFormat(SimonCoreConstants.MOBILE_PRODUCT_IMAGE_TYPE);
			mobileImageData.setAltText(altText);
			imageDataList.add(mobileImageData);
		}
		return imageDataList;
	}

	/**
	 * Sets the product images in different formats.
	 *
	 * @param mediaContainerToImagesMap
	 *           the media container to images map
	 * @param imageDataList
	 *           the image data list
	 */
	private void populateProductImagesData(final Map<String, Map<String, String>> mediaContainerToImagesMap,
			final Map<String, ImageData> imageDataList)
	{
		for (final Entry<String, Map<String, String>> entry : mediaContainerToImagesMap.entrySet())
		{
			final Map<String, String> mediaToImageUrl = entry.getValue();
			if (entry.getKey().equals(SimonCoreConstants.DEFAULT_IMAGE_KEY))
			{
				for (final Entry<String, String> imageAndURL : mediaToImageUrl.entrySet())
				{
					final ImageData imageData = new ImageData();
					imageData.setFormat(imageAndURL.getKey());
					imageData.setUrl(imageAndURL.getValue());
					imageData.setImageType(ImageDataType.PRIMARY);
					imageDataList.put(imageAndURL.getKey(), imageData);
				}
			}
		}

	}

	/**
	 * Populate url.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	void populateUrl(final SearchResultValueData source, final VariantOptionData target)
	{
		try
		{
			final ProductData productData = new ProductData();
			productData.setCode(target.getCode());
			final String url = this.<String> getValue(source, "url");
			if (StringUtils.isEmpty(url))
			{
				// Resolve the URL and set it on the product data
				target.setUrl(productDataUrlResolver.resolve(productData));
			}
			else
			{
				target.setUrl(url);
			}
			//populating baseproduct url also
			final String baseProductUrl = this.<String> getValue(source, "baseProductUrl");
			if (StringUtils.isEmpty(baseProductUrl))
			{
				final String baseProductCode = this.<String> getValue(source, "baseProductCode");
				if (baseProductCode != null)
				{
					productData.setCode(baseProductCode);
					target.setBaseProductUrl(productDataUrlResolver.resolve(productData));
				}
			}
			else
			{
				target.setBaseProductUrl(baseProductUrl);
			}
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.debug("Product with code '%s' not found!    " + target.getCode(), e);
		}



	}

	/**
	 * this method populate plum price data in target and set identify applicable price from MSRP, list price , sale
	 * price set them in target and crosponding tree set Populate price and percent off data.
	 *
	 * @param searchResultValueData
	 *           the search result value data
	 * @param variantOptionData
	 *           the variant option data
	 * @param priceRangeData
	 *           the price range data
	 */
	private void populatePriceAndPercentOffData(final SearchResultValueData searchResultValueData,
			final VariantOptionData variantOptionData, final PriceRangeData priceRangeData)
	{
		final Double plumPrice = this.<Double> getValue(searchResultValueData, "plumPrice");

		if (plumPrice != null)
		{
			variantOptionData.setPlumPrice(plumPrice);
			priceRangeData.getPlumPriceSet().add(plumPrice);
		}
		final List<String> vairantPrice = (List<String>) getValue(searchResultValueData, "variantPrice");
		if (CollectionUtils.isNotEmpty(vairantPrice))
		{
			for (final String priceValue : vairantPrice)
			{
				final String[] price = priceValue.split(Pattern.quote(SimonCoreConstants.PIPE));
				populatePriceAndApplicablePriceData(variantOptionData, price, priceRangeData);
			}
		}
	}

	/**
	 * this method put price data (msrp, list price, sale price) in variant option data and if price is stricked through
	 * then it add to applicableprice set. Populate price and applicable price data.
	 *
	 * @param variantOptionData
	 *           the variant option data
	 * @param price
	 *           the price
	 * @param priceRangeData
	 *           the price range data
	 */
	private void populatePriceAndApplicablePriceData(final VariantOptionData variantOptionData, final String[] price,
			final PriceRangeData priceRangeData)
	{
		if (price != null)
		{
			if (price.length == SimonCoreConstants.THREE_INT)
			{
				variantOptionData.getVariantPrice().put(price[SimonCoreConstants.ZERO_INT], new ImmutablePair<Double, Boolean>(
						Double.valueOf(price[SimonCoreConstants.FIRST_INT]), Boolean.valueOf(price[SimonCoreConstants.TWO_INT])));
				if (Boolean.valueOf(price[SimonCoreConstants.TWO_INT]) && StringUtils.isNotEmpty(price[SimonCoreConstants.FIRST_INT]))
				{
					priceRangeData.getApplicablePriceSet().add(Double.valueOf(price[SimonCoreConstants.FIRST_INT]));
				}
			}

			if (SimonCoreConstants.PRODUCT_LIST_PRICE.equals(price[SimonCoreConstants.ZERO_INT])
					&& StringUtils.isNotEmpty(price[SimonCoreConstants.FIRST_INT]))
			{
				priceRangeData.getListPriceSet().add(Double.valueOf(price[SimonCoreConstants.FIRST_INT]));
			}
			else if (SimonCoreConstants.PRODUCT_SALE_PRICE.equals(price[SimonCoreConstants.ZERO_INT])
					&& StringUtils.isNotEmpty(price[SimonCoreConstants.FIRST_INT]))
			{
				priceRangeData.getSalePriceSet().add(Double.valueOf(price[SimonCoreConstants.FIRST_INT]));
			}
			else if (SimonCoreConstants.PERCENTAGE_SAVING.equals(price[SimonCoreConstants.ZERO_INT]))
			{
				variantOptionData.getVariantPrice().put(price[SimonCoreConstants.ZERO_INT],
						new ImmutablePair<Double, Boolean>(Double.valueOf(price[SimonCoreConstants.FIRST_INT]), Boolean.TRUE));
				priceRangeData.getPercentageOffSet().add(Double.valueOf(price[SimonCoreConstants.FIRST_INT]));
			}
			else if (StringUtils.isNotEmpty(price[SimonCoreConstants.FIRST_INT]))
			{
				priceRangeData.getMsrpPriceSet().add(Double.valueOf(price[SimonCoreConstants.FIRST_INT]));
			}
		}
	}

	protected void populatePotentialPromotionData(final SearchResultValueData source, final ProductData target)
	{
		final PromotionData potentialPromoData = new PromotionData();
		final List<SearchResultValueData> variants = (List<SearchResultValueData>) source.getVariants();
		if (CollectionUtils.isNotEmpty(variants))
		{
			final Optional<SearchResultValueData> filteredResult = variants.stream()
					.filter(v -> StringUtils.isNotEmpty(this.<String> getValue(v, IndexedKeys.PROMOTION_CODE)))
					.max(Comparator.comparing(fR -> Integer.parseInt(this.<String> getValue(fR, IndexedKeys.PROMOTION_PRIORITY))));
			if (filteredResult.isPresent())
			{
				potentialPromoData.setCode(this.<String> getValue(filteredResult.get(), IndexedKeys.PROMOTION_CODE));
				potentialPromoData.setDescription(this.<String> getValue(filteredResult.get(), IndexedKeys.PROMOTION_DESCRIPTION));
				potentialPromoData.setPromotionType(this.<String> getValue(filteredResult.get(), IndexedKeys.PROMOTION_TYPE));
				potentialPromoData.setEndDate(this.<Date> getValue(filteredResult.get(), IndexedKeys.PROMOTION_END_DATE));
			}
		}
		target.setPotentialPromotions(Lists.newArrayList(potentialPromoData));
	}
}
