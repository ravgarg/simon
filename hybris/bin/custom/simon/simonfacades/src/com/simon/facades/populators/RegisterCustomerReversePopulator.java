package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;
import com.simon.facades.customer.data.MallData;


/**
 * Converter implementation for {@link de.hybris.platform.commercefacades.user.data.RegisterData} as source and
 * {@link de.hybris.platform.core.model.user.CustomerModel} as target type.
 */
public class RegisterCustomerReversePopulator implements Populator<RegisterData, CustomerModel>
{
	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "customerNameStrategy")
	private CustomerNameStrategy customerNameStrategy;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "mallService")
	private MallService mallService;



	/**
	 * Populate the {@link CustomerModel} instance with values from the {@link RegisterData} instance
	 *
	 */
	@Override
	public void populate(final RegisterData source, final CustomerModel target) throws ConversionException
	{
		target.setExternalId(source.getExternalId());
		if (StringUtils.isNotEmpty(source.getGender()))
		{
			target.setGender(CustomerGender.valueOf(source.getGender()));
		}

		if (StringUtils.isNotEmpty(source.getBirthMonth()))
		{
			target.setBirthMonth(enumerationService.getEnumerationValue(Months.class, source.getBirthMonth()));
		}

		target.setBirthYear(source.getBirthYear());
		if (StringUtils.isNotEmpty(source.getCountry()))
		{
			target.setCountry(commonI18NService.getCountry(source.getCountry()));
		}
		target.setZipCode(source.getZipcode());
		target.setName(customerNameStrategy.getName(source.getFirstName(), source.getLastName()));
		target.setUid(source.getLogin().toLowerCase());
		target.setOriginalUid(source.getLogin());
		target.setSessionLanguage(commonI18NService.getCurrentLanguage());
		target.setSessionCurrency(commonI18NService.getCurrentCurrency());
		target.setProfileCompleted(true);
		target.setOptedInEmail(source.isOptedInEmail());
		if (source.getPrimaryMall() != null)
		{
			final MallModel mall = mallService.getMallForCode(source.getPrimaryMall().getCode());
			target.setPrimaryMall(mall);
		}
		final List<String> mallCodes = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(source.getAlternateMalls()))
		{
			final List<MallData> listOfMallData = source.getAlternateMalls();
			for (final MallData mallData : listOfMallData)
			{
				mallCodes.add(mallData.getCode());
			}
		}
		Set<MallModel> alternateMalls = new HashSet<>();
		if (CollectionUtils.isNotEmpty(mallCodes))
		{
			alternateMalls = mallService.getMallListForCode(mallCodes);
		}
		target.setAlternateMalls(alternateMalls);
	}


}
