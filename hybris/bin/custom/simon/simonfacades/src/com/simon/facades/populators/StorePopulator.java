package com.simon.facades.populators;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmsfacades.data.CMSLinkComponentData;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.model.BackgroundImageItemModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.HeroBannerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.cms.BackgroundImageData;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * This is a populator class to populate ShopModel StoreData
 */
public class StorePopulator implements Populator<ShopModel, StoreData>
{

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource
	private ExtDealPopulator extDealPopulator;

	@Override
	public void populate(final ShopModel source, final StoreData target) throws ConversionException
	{
		if (null != source.getBanner())
		{
			target.setBanner(source.getBanner().getURL());
		}
		if (null != source.getLogo())
		{
			target.setLogo(source.getLogo().getURL());
			target.setAltTextLogo(source.getLogo().getAltText());
		}
		if (null != source.getBackgroundImage())
		{
			target.setBackgroundImage(source.getBackgroundImage().getURL());
			target.setAltTextBGImage(source.getBackgroundImage().getAltText());
		}
		if (null != source.getMobileBackgroundImage())
		{
			target.setMobileBackgroundImage(source.getMobileBackgroundImage().getURL());
			target.setAltTextMobileBGImage(source.getMobileBackgroundImage().getAltText());
		}
		if (null != source.getMobileLogo())
		{
			target.setMobileLogo(source.getMobileLogo().getURL());
			target.setAltTextMobileLogo(source.getMobileLogo().getAltText());
		}
		target.setId(source.getId());
		target.setDescription(source.getDescription());
		target.setName(source.getName());
		final Collection<ContentPageModel> contentModels = source.getContentPage();
		if (CollectionUtils.isNotEmpty(contentModels))
		{
			for (final ContentPageModel contentModel : contentModels)
			{
				if (ContentPageType.LISTINGPAGE.equals(contentModel.getPageType()))
				{
					target.setContentPageLabel(contentModel.getLabel());
				}
			}
		}

		if (null != source.getHeroBanner())
		{
			final HeroBannerData heroBannerData = new HeroBannerData();
			populateHeroBannerData(source.getHeroBanner(), heroBannerData);
			target.setHeroBanner(heroBannerData);
		}

		if (null != source.getDeals())
		{
			populateDealData(source.getDeals(), target);
		}
	}

	/**
	 * Used to populate the Deals data
	 *
	 * @param deals
	 * @param target
	 */
	protected void populateDealData(final Set<DealModel> deals, final StoreData target)
	{
		final List<DealData> dealsDataList = new ArrayList<>();
		for (final DealModel dealModel : deals)
		{
			final DealData dealData = new DealData();
			extDealPopulator.populate(dealModel, dealData);
			dealsDataList.add(dealData);
		}
		target.setDeals(dealsDataList);
	}

	/**
	 * Used to populate the Hero Banner Data
	 *
	 * @param heroBannerModal
	 * @param heroBannerData
	 */
	private void populateHeroBannerData(final PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel heroBannerModal,
			final HeroBannerData heroBannerData)
	{
		heroBannerData.setTitleText(heroBannerModal.getTitleText());
		heroBannerData.setSubheadingText(heroBannerModal.getSubheadingText());

		if (null != heroBannerModal.getFontColor())
		{
			heroBannerData.setFontColor(heroBannerModal.getFontColor().getCode());
		}
		if (null != heroBannerModal.getAlignment())
		{
			heroBannerData.setAlignment(heroBannerModal.getAlignment().getCode());
		}

		if (null != heroBannerModal.getBackgroundImage())
		{
			final BackgroundImageData backgroundImage = new BackgroundImageData();
			heroBannerData.setBackgroundImage(populateBackGroundImgData(heroBannerModal.getBackgroundImage(), backgroundImage));
		}


		if (null != heroBannerModal.getLink())
		{
			setLinkRelatedData(heroBannerModal.getLink(), heroBannerData);
		}
	}

	/**
	 * Used to populate the Link data for hero banner
	 *
	 * @param link
	 * @param heroBannerData
	 */
	protected void setLinkRelatedData(final CMSLinkComponentModel link, final HeroBannerData heroBannerData)
	{
		if (null != link.getTarget())
		{
			heroBannerData.setLinkTarget(link.getTarget().getCode());
		}
		heroBannerData.setLinkUrl(link.getUrl());
		heroBannerData.setLinkText(link.getLinkName());
	}

	/**
	 * Used to populate the Back ground image data for hero banner
	 *
	 * @param backgroundImageModel
	 * @param backgroundImageData
	 * @return
	 */
	protected BackgroundImageData populateBackGroundImgData(final BackgroundImageItemModel backgroundImageModel,
			final BackgroundImageData backgroundImageData)
	{

		if (null != backgroundImageModel.getDesktopImage())
		{
			final MediaData desktopImage = new MediaData();
			backgroundImageData.setDesktopImage(populateImageData(backgroundImageModel.getDesktopImage(), desktopImage));
		}
		if (null != backgroundImageModel.getMobileImage())
		{
			final MediaData mobileImage = new MediaData();
			backgroundImageData.setMobileImage(populateImageData(backgroundImageModel.getMobileImage(), mobileImage));
		}

		if (null != backgroundImageModel.getDestinationLink())
		{
			final CMSLinkComponentData destinationLink = new CMSLinkComponentData();
			backgroundImageData.setDestinationLink(popualteCMSLinkData(backgroundImageModel.getDestinationLink(), destinationLink));
		}
		backgroundImageData.setImageDescription(backgroundImageModel.getImageDescription());

		return backgroundImageData;
	}

	/**
	 * Used to populate the CMS Link Component Data for hero banner
	 *
	 * @param destinationLinkModel
	 * @param destinationLinkData
	 * @return
	 */
	protected CMSLinkComponentData popualteCMSLinkData(final CMSLinkComponentModel destinationLinkModel,
			final CMSLinkComponentData destinationLinkData)
	{
		destinationLinkData.setUrl(destinationLinkModel.getUrl());
		return destinationLinkData;
	}

	/**
	 * Used to populate the Desktop/Mobile image data for hero banner
	 *
	 * @param imageMediaModel
	 * @param imageMediaData
	 * @return
	 */
	protected MediaData populateImageData(final MediaModel imageMediaModel, final MediaData imageMediaData)
	{
		imageMediaData.setAltText(imageMediaModel.getAltText());
		imageMediaData.setUrl(imageMediaModel.getURL());
		imageMediaData.setCode(imageMediaModel.getCode());
		imageMediaData.setDescription(imageMediaModel.getDescription());
		imageMediaData.setDownloadUrl(imageMediaModel.getDownloadURL());
		imageMediaData.setMime(imageMediaModel.getMime());
		return imageMediaData;
	}

}
