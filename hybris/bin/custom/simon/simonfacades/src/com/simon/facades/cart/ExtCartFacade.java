package com.simon.facades.cart;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;


/**
 * Use to Add Simon Specific method
 */
public interface ExtCartFacade extends CartFacade
{

	/**
	 * Removes additional cart if present
	 * 
	 * @param cart
	 */
	void removeAdditionalCart();

	/**
	 * Remove OOS product from Cart And return the List of Product
	 *
	 * @return List<CartModificationData>
	 */
	List<CartModificationData> getChangeEntriesInCart();

	/**
	 * Use to populate the Cart Data to Modification
	 *
	 * @param source
	 * @param target
	 */
	void populateCartDataFromModification(final List<CartModificationData> source, final CartData target);

	/**
	 * Add Flash Message According to following scenario 1. OOS 2. Update Cart SucessFully 3. Remove Product From Cart
	 *
	 * @param cartData
	 * @param quantity
	 * @param cartModification
	 */
	void addFlashMessage(final CartData cartData, final long quantity, final CartModificationData cartModification);


	/**
	 * This method set shopping bag labels to cart Data object.
	 *
	 * @param cartData
	 *
	 */
	void setCartPageMessages(CartData cartData);

	/**
	 * method return cart data for mini. bag A json is used for display the mini cart in page. this class call cart
	 * service get session cart method and get CartModel, for display banner in mini cart MiniCartComponentModel has a
	 * LightboxBannerComponent which have simpleBannerComponentModel which provides the banner url and link url to
	 * display banner and redirect url to redirect the page.
	 *
	 * @return cartData
	 */
	CartData getMiniBag();

	/**
	 * This method accesses services to put the create cart message to JMS broker
	 *
	 * @throws CartCheckOutIntegrationException
	 */
	void createTwoTapCart() throws CartCheckOutIntegrationException;

	/**
	 * This method is used to save response from Cart callback to Additional cart model.
	 *
	 * @param createCartConfirmationDTO
	 *
	 */
	void saveCartCallBackResponse(CreateCartConfirmationDTO createCartConfirmationDTO);

	/**
	 * This method is used to save response from Cart callback to Additional cart model.
	 *
	 * @param createCartConfirmationDTO
	 *
	 */
	void saveCartCallBackResponse(CreateCartConfirmationDTO createCartConfirmationDTO, CartModel cartModel);

	/**
	 * This method is used for return SimpleCMSComponent for banner component for mini bag
	 *
	 * @param id
	 * @return MiniCartComponentModel
	 * @throws CMSItemNotFoundException
	 */
	public MiniCartComponentModel getMiniCartComponent(final String id) throws CMSItemNotFoundException;

	public int getCartCount();


}
