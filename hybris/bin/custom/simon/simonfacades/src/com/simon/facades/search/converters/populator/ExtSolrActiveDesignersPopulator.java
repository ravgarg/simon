package com.simon.facades.search.converters.populator;

import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.simon.facades.account.data.DesignerData;


/**
 * This Class is used to fetch all active designers from Solr whose Retailer is ACTIVE.
 *
 * @param <FACET_SEARCH_CONFIG_TYPE>
 * @param <INDEXED_TYPE_TYPE>
 * @param <INDEXED_PROPERTY_TYPE>
 * @param <INDEXED_TYPE_SORT_TYPE>
 * @param <ITEM>
 */
public class ExtSolrActiveDesignersPopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE, ITEM>
		implements
		Populator<SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult>, SearchPageData<ITEM>>
{
	private static final String BASE_PRODUCT_DESIGNER_NAME = "baseProductDesignerName";
	private static final String BASE_PRODUCT_DESIGNER_CODE = "baseProductDesignerCode";

	@Override
	public void populate(
			final SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult> source,
			final SearchPageData<ITEM> target) throws ConversionException
	{
		final SearchResult searchResult = source.getSearchResult();
		if (null != searchResult)
		{
			final List<DesignerData> activeDesigners = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(searchResult.getGroupCommands()))
			{
				searchResult.getGroupCommands().forEach(gc -> {
					if (CollectionUtils.isNotEmpty(gc.getGroups()))
					{
						gc.getGroups().forEach(g -> {
							if (CollectionUtils.isNotEmpty(g.getDocuments()))
							{
								populateDesignerData(activeDesigners, g.getDocuments().get(0));
							}
						});
					}
				});
			}
			target.setActiveDesigners(activeDesigners);
		}
	}

	/**
	 * Populate designer data from the doc
	 *
	 * @param activeDesigners
	 * @param doc
	 */
	private void populateDesignerData(final List<DesignerData> activeDesigners, final Document doc)
	{
		final String designerCode = (String) doc.getFieldValue(BASE_PRODUCT_DESIGNER_CODE);
		final String designerName = (String) doc.getFieldValue(BASE_PRODUCT_DESIGNER_NAME);
		if (designerCode != null && designerName != null)
		{
			final DesignerData designerData = new DesignerData();
			designerData.setCode(designerCode);
			designerData.setName(designerName);
			designerData.setBannerUrl("designer?code=" + designerCode);
			activeDesigners.add(designerData);
		}
	}
}
