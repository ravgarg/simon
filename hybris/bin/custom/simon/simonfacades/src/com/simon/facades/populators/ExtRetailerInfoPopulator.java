package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.order.data.ZoneDeliveryModeData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.deliveryzone.model.ZoneDeliveryModeModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.PromotionResultService;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.localization.Localization;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.api.mirakl.dto.SimonMiraklTrackingInfoBean;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.core.services.RetailerService;
import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.retailer.promotion.data.RetailerPromotionData;


/**
 * ExtRetailerInfoPopulator is implementation of Interface for a populator. A populator sets values in a target instance
 * based on values in the source instance. Populators are similar to converters except that unlike converters the target
 * instance must already exist.
 *
 * this class populate Retailer info Data and group the orders in group based on retailers
 *
 *
 * @param <S>
 * @param <T>
 */
public class ExtRetailerInfoPopulator<S extends AbstractOrderModel, T extends AbstractOrderData> implements Populator<S, T>
{
	@Resource(name = "extRetailerConverter")
	private Converter<ShopModel, RetailerInfoData> extRetailerConverter;

	@Resource
	private PriceDataFactory priceDataFactory;


	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	private Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter;

	private Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> zoneDeliveryModeConverter;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private RetailerService retailerService;

	@Resource
	private PromotionResultService promotionResultService;

	@Resource
	private ConfigurationService configurationService;


	/**
	 * this method calculate you save value when item level promotion is applied
	 *
	 * @param orderEntryData
	 * @param target
	 */
	protected void getAppliedProductPromotion(final OrderEntryData orderEntryData, final AbstractOrderData target)
	{
		double priceWithOutDiscount;
		double discountedPricePerUnit;
		double youSavedValue;
		final String isoCode = null != orderEntryData.getBasePrice() ? orderEntryData.getBasePrice().getCurrencyIso() : null;
		final List<PromotionResultData> appliedProductPromotionResultData = target.getAppliedProductPromotions();
		final List<String> promotionMessages = new ArrayList<>();
		final List<RetailerPromotionData> itemPromotion = new ArrayList<>();
		for (final PromotionResultData eachPromotionResultData : appliedProductPromotionResultData)
		{
			final List<PromotionOrderEntryConsumedData> consumedEntries = eachPromotionResultData.getConsumedEntries();

			for (final PromotionOrderEntryConsumedData eachPromotionOrderEntryConsumedData : consumedEntries)
			{
				if (orderEntryData.getEntryNumber() != null && eachPromotionOrderEntryConsumedData.getOrderEntryNumber() != null
						&& orderEntryData.getEntryNumber().equals(eachPromotionOrderEntryConsumedData.getOrderEntryNumber())
						&& null != isoCode)
				{
					final RetailerPromotionData retailerPromotionData = new RetailerPromotionData();
					priceWithOutDiscount = getStrikeOffPrice(orderEntryData).doubleValue();
					discountedPricePerUnit = orderEntryData.getTotalPrice().getValue().doubleValue() / (orderEntryData.getQuantity());
					youSavedValue = priceWithOutDiscount - discountedPricePerUnit;
					final PriceData youSaved = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(youSavedValue), isoCode);
					final PriceData yourPrice = priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(discountedPricePerUnit),
							isoCode);
					orderEntryData.setPromotionApplicable(true);
					orderEntryData.setYourPrice(yourPrice);
					orderEntryData.setYouSave(youSaved);
					promotionMessages.add(eachPromotionResultData.getDescription());
					retailerPromotionData.setCode(eachPromotionResultData.getPromotionData().getCode());
					retailerPromotionData.setType(eachPromotionResultData.getPromotionData().getPromotionType());
					retailerPromotionData.setDescription(eachPromotionResultData.getDescription());
					orderEntryData.setSavingsMessage(getPercentSaving(priceWithOutDiscount, discountedPricePerUnit));
					itemPromotion.add(retailerPromotionData);
				}
			}
		}
		orderEntryData.setPromotionMessage(promotionMessages);
		orderEntryData.setItemPromotionList(itemPromotion);
	}

	/**
	 * method calculate the % saving price for items which are eligible for promotion
	 *
	 * @param priceWithoutDiscount
	 * @param discountedPrice
	 * @return Double
	 */

	private String getPercentSaving(final double priceWithoutDiscount, final double discountedPrice)
	{
		String percentSaving = StringUtils.EMPTY;
		if (priceWithoutDiscount > 0.0)
		{
			percentSaving = Math
					.round(((priceWithoutDiscount - discountedPrice) / priceWithoutDiscount) * SimonCoreConstants.HUNDRED_INT)
					+ this.getPercentSavingMessage();
		}
		return percentSaving;
	}


	/**
	 * this method get the StrikeOffPrice for the product which are applicable for item level promotion
	 *
	 * @param orderEntryData
	 * @return
	 */
	protected BigDecimal getStrikeOffPrice(final OrderEntryData orderEntryData)
	{
		BigDecimal strikeOffPrice = BigDecimal.ZERO;
		if (orderEntryData.getMsrpValue() != null && orderEntryData.getMsrpValue().isStrikeOff())
		{
			strikeOffPrice = orderEntryData.getMsrpValue().getValue();
		}
		else if (orderEntryData.getSaleValue() != null && orderEntryData.getSaleValue().isStrikeOff())
		{
			strikeOffPrice = orderEntryData.getSaleValue().getValue();
		}
		else if (orderEntryData.getListValue() != null)
		{
			strikeOffPrice = orderEntryData.getListValue().getValue();
		}


		return strikeOffPrice;
	}

	/**
	 * This method populate Retailer info Data and group the orders in group based on retailers and get the price for
	 * cart
	 */
	@Override
	public void populate(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final Map<String, String> retailerSiteIdMap = new HashMap<>();

		final Map<String, RetailerInfoData> retailerInfoDataMap = getRetailerInfoDataMap();

		for (final AbstractOrderEntryModel orderEntry : source.getEntries())
		{
			final ProductModel productModel = orderEntry.getProduct();
			final ShopModel shop = null != productModel ? productModel.getShop() : null;

			if (null != shop)
			{
				final String shopId = shop.getId();
				retailerSiteIdMap.put(shopId, shop.getSiteId());

				final Optional<RetailerInfoData> retailerInfo = retailerInfoDataMap.values().stream()
						.filter(retailerInfoData -> retailerInfoData.getRetailerId().equals(shopId)).findFirst();

				final OrderEntryData orderEntryData = getOrderEntryConverter().convert(orderEntry);

				if (CollectionUtils.isNotEmpty(target.getAppliedProductPromotions()))
				{
					getAppliedProductPromotion(orderEntryData, target);
				}

				if (retailerInfo.isPresent())
				{
					final RetailerInfoData retailerInfoData = retailerInfo.get();
					retailerInfoData.getProductDetails().add(orderEntryData);
					retailerInfoData.setTotalUnitCount(retailerInfoData.getTotalUnitCount() + orderEntryData.getQuantity());
					addDeliveryInfo(source, retailerInfoData, shopId);
					retailerInfoData.setRetailerOrderNumber(source.getRetailersOrderNumbers().get(shopId));
					retailerInfoData.setOrderTrackingList(getOrderTrackingInfo(orderEntry, shopId, retailerInfoData));
					retailerInfoData.setCustomerServiceLink(
							configurationService.getConfiguration().getString("generic.link.customercare.zendesk.faq.page"));
				}
				else
				{
					final RetailerInfoData retailerInfoData = getExtRetailerConverter().convert(shop);
					retailerInfoData.setTotalUnitCount(orderEntry.getQuantity());
					final List<OrderEntryData> orderEntryDataList = new ArrayList<>();
					orderEntryDataList.add(orderEntryData);
					retailerInfoData.setProductDetails(orderEntryDataList);
					addDeliveryInfo(source, retailerInfoData, shopId);
					retailerInfoData.setRetailerOrderNumber(source.getRetailersOrderNumbers().get(shopId));
					retailerInfoData.setOrderTrackingList(getOrderTrackingInfo(orderEntry, shopId, retailerInfoData));
					retailerInfoData.setCustomerServiceLink(
							configurationService.getConfiguration().getString("generic.link.customercare.zendesk.faq.page"));
					retailerInfoDataMap.put(retailerInfoData.getRetailerId(), retailerInfoData);
				}
			}
		}

		target.setRetailerInfoData(new ArrayList<>(retailerInfoDataMap.values()));

		if (CollectionUtils.isNotEmpty(target.getRetailerInfoData()))
		{
			populatePromotionalDiscountForRetailers(source, target);
			populateShippingCost(source, target);
			populateRetailerSummary(source, target);
			populateCartSummary(target);
		}
	}

	/**
	 * In this method as per retailer making a list of Tracking Number. as per line item from consignments entries
	 *
	 * @param orderEntry
	 * @param shopId
	 * @param retailerInfoData
	 * @return
	 */
	private List<SimonMiraklTrackingInfoBean> getOrderTrackingInfo(final AbstractOrderEntryModel orderEntry, final String shopId,
			final RetailerInfoData retailerInfoData)
	{
		List<SimonMiraklTrackingInfoBean> trackingOrder = retailerInfoData.getOrderTrackingList();

		if (CollectionUtils.isEmpty(trackingOrder))
		{
			trackingOrder = new ArrayList<>();
		}

		if (CollectionUtils.isNotEmpty(orderEntry.getConsignmentEntries()))
		{
			for (final ConsignmentEntryModel consignmentEntryModel : orderEntry.getConsignmentEntries())
			{
				trackingOrder.add(setSimonMiraklTrackingInfo(consignmentEntryModel, shopId));
			}
		}
		return trackingOrder;
	}

	/**
	 * In this method setting properties of SimonMiraklTrackingInfoBean for display on Order History and Tracking page
	 *
	 * @param consignmentEntryModel
	 * @param shopId
	 * @return
	 */
	private SimonMiraklTrackingInfoBean setSimonMiraklTrackingInfo(final ConsignmentEntryModel consignmentEntryModel,
			final String shopId)
	{
		SimonMiraklTrackingInfoBean simonMiraklTrackingInfoBean = null;

		if (consignmentEntryModel.getOrderEntry().getShopId().equals(shopId))
		{
			simonMiraklTrackingInfoBean = new SimonMiraklTrackingInfoBean();
			simonMiraklTrackingInfoBean.setCarrierCode(
					null != consignmentEntryModel.getCarrierCode() ? consignmentEntryModel.getCarrierCode() : StringUtils.EMPTY);
			simonMiraklTrackingInfoBean.setCarrierName(
					null != consignmentEntryModel.getCarrierName() ? consignmentEntryModel.getCarrierName() : StringUtils.EMPTY);
			simonMiraklTrackingInfoBean.setTrackingNumber(
					null != consignmentEntryModel.getTrackingNo() ? consignmentEntryModel.getTrackingNo() : StringUtils.EMPTY);
			simonMiraklTrackingInfoBean.setCarrierUrl(
					null != consignmentEntryModel.getCarrierURL() ? consignmentEntryModel.getCarrierURL() : StringUtils.EMPTY);
		}

		return simonMiraklTrackingInfoBean;

	}

	/**
	 * This method populate delivery mode at retailer level cart
	 */
	private void addDeliveryInfo(final AbstractOrderModel source, final RetailerInfoData retailerInfoData, final String shopId)
	{
		if (null != source.getRetailersDeliveryModes())
		{
			final DeliveryModeModel deliveryMode = source.getRetailersDeliveryModes().get(shopId);
			if (deliveryMode != null)
			{
				DeliveryModeData deliveryModeData;
				if (deliveryMode instanceof ZoneDeliveryModeModel)
				{
					deliveryModeData = getZoneDeliveryModeConverter().convert((ZoneDeliveryModeModel) deliveryMode);
				}
				else
				{
					deliveryModeData = getDeliveryModeConverter().convert(deliveryMode);
				}

				retailerInfoData.setAppliedShippingMethod(deliveryModeData);
			}
		}

	}

	private void populatePromotionalDiscountForRetailers(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.getRetailerInfoData().stream().forEach(retailer -> populatePromotionalDiscountForRetailer(source, retailer));
	}

	private void populateShippingCost(final AbstractOrderModel source, final AbstractOrderData target)
	{
		final Map<String, Double> deliveryCost = source.getRetailersDeliveryCost();
		if (null != deliveryCost)
		{
			target.getRetailerInfoData().stream().forEach(retailerData -> setShippingPrice(retailerData, deliveryCost));
		}
	}

	private void setShippingPrice(final RetailerInfoData retailerData, final Map<String, Double> deliveryCost)
	{
		final String retailerId = retailerData.getRetailerId();
		final Double deliveryCostForRetailer = deliveryCost.get(retailerId);

		retailerData.setEstimatedShipping(deliveryCostForRetailer != null ? priceDataFactory.create(PriceDataType.BUY,
				BigDecimal.valueOf(deliveryCostForRetailer.doubleValue()), commonI18NService.getCurrentCurrency().getIsocode())
				: null);
	}

	/**
	 * Adding Retailer specific order level promotion messages.
	 *
	 * @param source
	 * @param retailerSiteIdMap
	 */
	private void populatePromoMessages(final AbstractOrderModel source, final RetailerInfoData retailerInfoData)
	{
		final List<String> promoMessage = new ArrayList<>();
		final List<RetailerPromotionData> retailerPromotion = new ArrayList<>();
		for (final PromotionResultModel promotionResultModel : source.getAllPromotionResults())
		{
			if (null != promotionResultModel.getRetailerID()
					&& promotionResultModel.getRetailerID().equals(retailerInfoData.getRetailerId())
					&& promotionResultModel.getPromotion() instanceof RuleBasedPromotionModel)
			{
				final RetailerPromotionData retailerPromotionData = new RetailerPromotionData();
				promoMessage.add(promotionResultService.getDescription(promotionResultModel));
				retailerPromotionData.setCode(promotionResultModel.getPromotion().getCode());
				retailerPromotionData.setType(promotionResultModel.getPromotion().getPromotionType());
				retailerPromotionData.setDescription(promotionResultService.getDescription(promotionResultModel));
				retailerPromotion.add(retailerPromotionData);
			}
		}
		retailerInfoData.setPromoMessages(promoMessage);
		retailerInfoData.setRetailerPromotionList(retailerPromotion);
	}

	private void populatePromotionalDiscountForRetailer(final AbstractOrderModel source, final RetailerInfoData retailerInfo)
	{
		final Double retailerDiscount = source.getRetailersTotalDiscount().get(retailerInfo.getRetailerId());
		if (retailerDiscount != null)
		{
			retailerInfo.setPromotionalDiscount(createPrice(retailerDiscount));
		}
	}



	/**
	 * Adding Retailer Sub-total from CartEntries and Adding Tax and delivery cost from proxy cart which got from Two-tap
	 * in checkout flow
	 *
	 * @param source
	 * @param target
	 */
	private void populateRetailerSummary(final AbstractOrderModel source, final AbstractOrderData target)
	{
		for (final RetailerInfoData retailerInfoData : target.getRetailerInfoData())
		{
			double subTotal = SimonCoreConstants.ZERO_DOUBLE;
			for (final OrderEntryData orderEntryData : retailerInfoData.getProductDetails())
			{
				subTotal = subTotal + orderEntryData.getTotalPrice().getValue().doubleValue();
			}

			final AdditionalCartInfoModel additionalCartInfo = source.getAdditionalCartInfo();
			if (null != additionalCartInfo)
			{
				populateRetailerSummary(retailerInfoData, additionalCartInfo);
			}
			setTotal(retailerInfoData, subTotal);
			final PriceData price = createPrice(retailerService.getRetailerSubBagSaving(source, retailerInfoData.getRetailerId()));
			retailerInfoData.setSaving(price);
			if (!CollectionUtils.isEmpty(source.getAllPromotionResults()))
			{
				populatePromoMessages(source, retailerInfoData);
			}
		}
	}

	private PriceData createPrice(final double price)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price),
				commonI18NService.getCurrentCurrency().getIsocode());
	}

	private void populateCartSummary(final AbstractOrderData target)
	{
		Double youSavedOnOrder = 0.0;
		Double totalShipping = null;
		Double totalTax = null;
		Double total = 0.0;
		double orderSubtotal = 0;
		for (final RetailerInfoData retailer : target.getRetailerInfoData())
		{
			youSavedOnOrder = youSavedOnOrder + (retailer.getSaving() != null ? retailer.getSaving().getValue().doubleValue() : 0.0);
			final double subtotal = null != retailer.getEstimatedSubtotal()
					? retailer.getEstimatedSubtotal().getValue().doubleValue() : 0.0;
			orderSubtotal += subtotal;
			total = total + subtotal;
			final PriceData retailerShipping = retailer.getEstimatedShipping();
			final PriceData retailerTax = retailer.getEstimatedTax();
			if (null != retailerShipping)
			{
				totalShipping = null == totalShipping ? retailerShipping.getValue().doubleValue()
						: totalShipping + retailerShipping.getValue().doubleValue();
				total += retailerShipping.getValue().doubleValue();
			}
			if (null != retailerTax)
			{
				totalTax = null == totalTax ? retailerTax.getValue().doubleValue() : totalTax + retailerTax.getValue().doubleValue();
				total += retailerTax.getValue().doubleValue();
			}
		}
		target.setSubTotal(createPrice(orderSubtotal));
		target.setDeliveryCost(null != totalShipping ? createPrice(totalShipping) : null);
		target.setTotalTax(null != totalTax ? createPrice(totalTax) : null);
		target.setTotalPrice(createPrice(total));
		target.setTotalDiscounts(createPrice(youSavedOnOrder));
	}

	private void populateRetailerSummary(final RetailerInfoData retailerInfoData, final AdditionalCartInfoModel proxyCartModel)
	{
		retailerInfoData.setSupportedShippingMethods(new ArrayList<>());
		for (final RetailersInfoModel retailer : proxyCartModel.getRetailersInfo())
		{
			if (retailer.getRetailerId().equals(retailerInfoData.getRetailerId()))
			{
				setShipping(retailerInfoData, retailer);
				setTax(retailerInfoData, retailer);
			}
		}

	}

	private void setTotal(final RetailerInfoData retailerInfoData, final double subTotalWithoutDiscount)
	{
		final double promotionalDiscount = null != retailerInfoData.getPromotionalDiscount()
				? retailerInfoData.getPromotionalDiscount().getValue().doubleValue() : 0.0;
		final double shipping = null != retailerInfoData.getEstimatedShipping()
				? retailerInfoData.getEstimatedShipping().getValue().doubleValue() : 0.0;
		final double saleTax = null != retailerInfoData.getEstimatedTax()
				? retailerInfoData.getEstimatedTax().getValue().doubleValue() : 0.0;
		final double subtotal = subTotalWithoutDiscount - promotionalDiscount;
		final double total = subtotal + shipping + saleTax;
		retailerInfoData.setEstimatedSubtotal(createPrice(subtotal));
		retailerInfoData.setEstimatedTotal(createPrice(total));
	}

	private void setTax(final RetailerInfoData retailerInfoData, final RetailersInfoModel retailer)
	{
		final Double retailerTax = retailer.getTotalSalesTax();
		if (null != retailerTax)
		{
			retailerInfoData.setEstimatedTax(createPrice(retailerTax));
		}
	}

	private void setShipping(final RetailerInfoData retailerInfoData, final RetailersInfoModel retailer)
	{
		final List<ShippingDetailsModel> shippingMethods = retailer.getShippingDetails();
		for (final ShippingDetailsModel shippingMethod : shippingMethods)
		{
			final DeliveryModeData shippingMethodData = new DeliveryModeData();
			shippingMethodData.setCode(shippingMethod.getCode());
			shippingMethodData.setName(shippingMethod.getDescription());
			final Double shippingPrice = shippingMethod.getShippingEstimate();

			if (null != shippingPrice)
			{
				shippingMethodData.setDeliveryCost(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(shippingPrice),
						commonI18NService.getCurrentCurrency().getIsocode()));
				if (shippingMethod.isSelectedForCart())
				{
					shippingMethodData.setIsSelected(true);
				}
			}
			retailerInfoData.getSupportedShippingMethods().add(shippingMethodData);
		}
	}

	protected Map<String, RetailerInfoData> getRetailerInfoDataMap()
	{
		return new HashMap<>();
	}


	/**
	 * @return extRetailerConverter
	 */
	public Converter<ShopModel, RetailerInfoData> getExtRetailerConverter()
	{
		return extRetailerConverter;
	}

	/**
	 * @param extRetailerConverter
	 */
	public void setExtRetailerConverter(final Converter<ShopModel, RetailerInfoData> extRetailerConverter)
	{
		this.extRetailerConverter = extRetailerConverter;
	}


	/**
	 * @return orderEntryConverter
	 */
	public Converter<AbstractOrderEntryModel, OrderEntryData> getOrderEntryConverter()
	{
		return orderEntryConverter;
	}

	/**
	 * @param orderEntryConverter
	 */
	public void setOrderEntryConverter(final Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter)
	{
		this.orderEntryConverter = orderEntryConverter;
	}

	/**
	 * Gets the localized percent saving message
	 *
	 * @return
	 */
	protected String getPercentSavingMessage()
	{
		return Localization.getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
	}

	/**
	 * @return deliveryModeConverter
	 */
	public Converter<DeliveryModeModel, DeliveryModeData> getDeliveryModeConverter()
	{
		return deliveryModeConverter;
	}

	/**
	 * @param deliveryModeConverter
	 */
	public void setDeliveryModeConverter(final Converter<DeliveryModeModel, DeliveryModeData> deliveryModeConverter)
	{
		this.deliveryModeConverter = deliveryModeConverter;
	}

	/**
	 * @return zoneDeliveryModeConverter
	 */
	public Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> getZoneDeliveryModeConverter()
	{
		return zoneDeliveryModeConverter;
	}

	/**
	 * @param zoneDeliveryModeConverter
	 */
	public void setZoneDeliveryModeConverter(
			final Converter<ZoneDeliveryModeModel, ZoneDeliveryModeData> zoneDeliveryModeConverter)
	{
		this.zoneDeliveryModeConverter = zoneDeliveryModeConverter;
	}


}
