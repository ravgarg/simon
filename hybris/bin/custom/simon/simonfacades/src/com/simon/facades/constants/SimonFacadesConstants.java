package com.simon.facades.constants;

@SuppressWarnings("PMD")
public class SimonFacadesConstants
{
	public static final String EXTENSIONNAME = "simonfacades";

	private SimonFacadesConstants()
	{
		//empty
	}

	public static final String NODE_COUNT_NAVIGATION_SUBCATEGORIES = "node.count.navigation.subcategories";
	public static final String TEXT_NAVIGATION_VIEW_ALL = "View All <span>&rsaquo;</span>";

	public static final String TOTAL_UNITCOUNT_LABEL = "mini.bag.total.unitcount.label";
	public static final String CHECKOUT_LABEL = "mini.bag.checkout.label";
	public static final String VIEW_MYBAG_LABEL = "mini.bag.view.my.bag.label";
	public static final String YOU_SAVE_LABEL = "type.PriceRow.youSave.name";
	public static final String ITEM_LABEL = "mini.bag.item.label";
	public static final String EMPTY_BAG_PROMOTIONAL_LABEL = "mini.bag.empty.bag.promotional.label";
	public static final String EMPTY_BAG_PROMOTIONAL_LABEL_URL = "mini.bag.empty.bag.promotional.label.url";
	public static final String EMPTY_BAG_LABEL = "mini.bag.empty.bag.label";

	public static final String TOTAL_UNITCOUNT = "totalunitCountLabel";
	public static final String CHECKOUT = "checkoutLabel";
	public static final String ITEM = "itemLabel";
	public static final String VIEW_MY_BAG = "viewMyBagLabel";
	public static final String EMPTY_BAG_PROMOTIONAL = "emptybagPromotionalLabel";
	public static final String EMPTY_BAG_PROMOTIONAL_URL = "emptybagPromotionalLabelUrl";
	public static final String EMPTY_BAG = "emptybagLabel";
	public static final String YOU_SAVE = "youSaveLabel";


	public static final String DISCLAIMER_TEXT_KEY = "disclaimerText";
	public static final String CONTINUE_SHOPPING_KEY = "continueShopping";
	public static final String CHECKOUT_TEXT_KEY = "checkoutText";
	public static final String VIEW_MYBAG_KEY = "viewMyBag";
	public static final String ADD_TO_CART_QUANTITY_KEY = "quantity";
	public static final String ITEM_TOTAL_KEY = "itemTotal";


	public static final String DISCLAIMER_TEXT = "add.cart.popup.disclaimer.text";
	public static final String CONTINUE_SHOPPING = "add.cart.popup.continue.shopping";
	public static final String CHECKOUT_TEXT = "add.cart.popup.checkout.text";
	public static final String VIEW_MYBAG = "add.cart.popup.view.bag";
	public static final String ADD_TO_CART_QUANTITY = "add.cart.popup.quantity";
	public static final String ITEM_TOTAL = "add.cart.popup.item.total";
	public static final String LOW_STOCK_ERROR = "add.cart.popup.error.low.stock";
	public static final String NO_STOCK_ERROR = "add.cart.popup.error.no.stock";

	public static final String MY_SHOPPING_BAG = "cart.page.my.shopping.bag";
	public static final String BAG_SUMMARY = "cart.page.bag.summary";
	public static final String ESTIMATED_ORDER_SUBTOTAL = "cart.page.estimated.order.total";
	public static final String ESTIMATED_TOTAL_SHIPPING = "cart.page.estimated.total.shipping";
	public static final String ESTIMATED_TOTAL_TAX = "cart.page.estimated.total.tax";
	public static final String ESTIMATED_BAG_TOTAL = "cart.page.estimated.total.bag";
	public static final String YOU_SAVED = "cart.page.estimated.you.saved";

	public static final String PROCEED_TO_CHECKOUT = "cart.page.proceed.to.checkout";
	public static final String RETAILER_SHIPPING_PRIVACY = "cart.page.retailer.shipping.privacy.label";
	public static final String ITEMS_PRICING = "cart.page.items.pricing.label";
	public static final String REMOVE = "cart.page.remove.label";
	public static final String ADD_TO_MY_FAVOURITE = "cart.page.add.to.my.favourite";
	public static final String COLOR = "cart.page.color.label";
	public static final String SIZE = "cart.page.size.label";
	public static final String ESTIMATED_SUBTOTAL = "cart.page.estimated.subtotal";
	public static final String ESTIMATED_SHIPPING = "cart.page.estimated.shipping";
	public static final String ESTIMATED_TAX = "cart.page.estimated.tax";
	public static final String ESTIMATED_TOTAL = "cart.page.estimated.total";
	public static final String RETAILER_ORDER_PROMOTION = "cart.page.retailer.order.promotion";

	public static final String HELPFULL_INFO = "cart.page.helpful.info";
	public static final String HELPFULL_INFO_MESSAGE = "cart.page.helpful.message";
	public static final String QUESTION = "cart.page.question";
	public static final String CONTACT_US = "cart.page.contact.us";
	public static final String SEE_SHIPPING = "cart.page.see.shipping.label";
	public static final String CONTINUE_URL = "cart.page.continue.url.label";
	public static final String BAG_SUMMARY_LABEL = "bagSummaryLabel";
	public static final String MY_SHOPPING_BAG_LABEL = "myShoppingBagLabel";
	public static final String ESTIMATED_ORDER_SUBTOTAL_LABEL = "estimatedOrderSubtotalLabel";
	public static final String ESTIMATED_TOTAL_SHIPPING_LABEL = "estimatedOrderShippingLabel";
	public static final String ESTIMATED_TOTAL_TAX_LABEL = "estimatedOrderTaxLabel";
	public static final String ESTIMATED_BAG_TOTAL_LABEL = "estimatedOrderTotalLabel";
	public static final String YOU_SAVED_LABEL = "youSavedLabel";

	public static final String PROCEED_TO_CHECKOUT_LABEL = "proceedToCheckoutLabel";
	public static final String RETAILER_SHIPPING_PRIVACY_LABEL = "RetailerShippingPrivacyLabel";
	public static final String ITEMS_PRICING_LABEL = "itemPriceLabel";
	public static final String REMOVE_LABEL = "removeLabel";
	public static final String ADD_TO_MY_FAVOURITE_LABEL = "addToMyFavouriteLabel";
	public static final String COLOR_LABEL = "colorLabel";
	public static final String SIZE_LABEL = "sizeLabel";
	public static final String ESTIMATED_SUBTOTAL_LABEL = "estimatedSubtotalLabel";
	public static final String ESTIMATED_SHIPPING_LABEL = "estimatedShippingLabel";
	public static final String ESTIMATED_TAX_LABEL = "estimatedTaxLabel";
	public static final String ESTIMATED_TOTAL_LABEL = "estimatedTotal";
	public static final String RETAILER_ORDER_PROMOTION_LABEL = "retailerOrderPromotionLabel";

	public static final String HELPFULL_INFO_LABEL = "helpFulInfoLabel";
	public static final String HELPFULL_INFO_MESSAGE_LABEL = "helpFullInfoMessageLabel";
	public static final String QUESTION_LABEL = "questionLabel";
	public static final String CONTACT_US_LABEL = "contactUsLabel";
	public static final String SEE_SHIPPING_LABEL = "seeShippingLabel";
	public static final String CONTINUE_URL_LABEL = "continueUrlLabel";
	public static final String TOTAL_MAX_QUANTITY_LABEL = "maxQuantity";
	public static final String TOTAL_MAX_QUANTITY = "cart.page.max.quantity";

	public static final String GUEST_MESSAGE = "shipping.checkout.guest.message";
	public static final String GUEST_MESSAGE_LABEL = "checkoutASGuest";
	public static final String HAVE_ACCOUNT = "shipping.checkout.have.account";
	public static final String HAVE_ACCOUNT_LABEL = "haveAnAccount";
	public static final String SIGN_IN = "shipping.checkout.sign.in";
	public static final String SIGN_IN_LABEL = "signInLabel";
	public static final String SHIPPING_INFORMATION = "shipping.checkout.shipping.info";
	public static final String SHIPPING_INFO_LABEL = "shippingInfoLabel";
	public static final String WE_DONT_SHIP_MESSAGE = "shipping.checkout.ship.outside";
	public static final String WE_DONT_SHIP_MESSAGE_LABEL = "weDontLabel";
	public static final String INDICATE_OPTIONAL_MESSAGE = "shipping.checkout.indicates.optional";
	public static final String INDICATE_OPTIONAL_LABEL = "indicateOptionalLabel";
	public static final String SUBSCRIBE_MESSAGE = "shipping.checkout.subscribe.newslater";
	public static final String SUBSCRIBE_LABEL = "subscribeLabel";
	public static final String PLEASE_REFER_MESSAGE = "shipping.checkout.please.refer";
	public static final String PLEASE_REFER_LABEL = "pleaseReferLabel";
	public static final String SHIPPING_ADDRESS_LABEL = "shippingAddressLabel";
	public static final String SHIPPING_ADDRESS_MESSAGE = "shipping.checkout.shipping.address";

	public static final String USE_SHIP_BILL_MESSAGE = "shipping.checkout.use.shipping.address";
	public static final String USE_SHIP_BILL_LABEL = "useShippingASBilling";
	public static final String ADD_NEW_ADDRESS = "shipping.checkout.add.new.address";
	public static final String ADD_NEW_ADDRESS_LABEL = "addNewAddress";
	public static final String SAVE_CONTINUE_MESSAGE = "shipping.checkout.save.continue";
	public static final String SAVE_CONTINUE_LABEL = "saveAndContinue";
	public static final String SAVE_ADDRESS_TO_BOOK = "shipping.checkout.save.address.book";
	public static final String SAVE_ADDRESS_TO_BOOK_LABEL = "saveThisAddress";
	public static final String STATE_MESSAGE = "shipping.checkout.form.state";
	public static final String STATE_LABEL = "stateLabel";
	public static final String WELCOME_BACK_MESSAGE = "shipping.checkout.welcome.back";
	public static final String WELCOME_LABEL = "welcomeback";
	public static final String PHONENUMBER_MESSAGE = "shipping.checkout.phone.number";
	public static final String PHONENUMBER_LABEL = "phoneNumberLabel";
	public static final String ZIPCODE_MESSAGE = "shipping.checkout.zip.code";
	public static final String ZIPCODE_LABEL = "zipcodeLabel";
	public static final String EMAIL_LABEL = "emailIdLabel";
	public static final String EMAIL_MESSAGE = "shipping.checkout.email.address";

	public static final String CITY_MESSAGE = "shipping.checkout.city";
	public static final String CITY_LABEL = "cityLabel";
	public static final String ADDRESS1_MESSAGE = "shipping.checkout.address1";
	public static final String ADDRESS1_LABEL = "address1Label";
	public static final String ADDRESS2_MESSAGE = "shipping.checkout.apt.floor";
	public static final String ADDRESS2_LABEL = "address2Label";
	public static final String FIRSTNAME_MESSAGE = "shipping.checkout.first.name";
	public static final String FIRSTNAME_LABEL = "firstNameLabel";
	public static final String MIDDLE_LABEL = "middleNameLabel";
	public static final String MIDDLE_MESSAGE = "shipping.checkout.mi";
	public static final String LASTNAME_MESSAGE = "shipping.checkout.last.name";
	public static final String LASTNAME_LABEL = "lastNameLabel";

	public static final String SHIPTO_MESSAGE = "shipping.checkout.ship.to";
	public static final String SHIPTO_LABEL = "shipToLabel";
	public static final String EDIT_LABEL = "editLabel";
	public static final String EDIT_MESSAGE = "shipping.checkout.edit.label";

	public static final String NAME = "name";

	public static final String VERIFY_ADD_LABEL = "verifyAdd";
	public static final String VERIFY_ADD_MESSAGE = "shipping.checkout.verify.add.text";
	public static final String SHIPPING_PANEL_ERROR_LABEL = "shippingErrorMessage";
	public static final String SHIPPING_PANEL_ERROR_MESSAGE = "shipping.checkout.error.message.text";
	public static final String VERIFICATION_TEXT_LABEL = "verificationText";
	public static final String VERIFICATION_TEXT_MESSAGE = "shipping.checkout.verify.text";
	public static final String YOU_ENTERED_LABEL = "youEnter";
	public static final String YOU_ENTERED_MESSAGE = "shipping.checkout.you.enter.text";
	public static final String USE_THIS_ADD_LABEL = "useThisAdd";
	public static final String USE_THIS_ADD_MESSAGE = "shipping.checkout.use.text";
	public static final String PRIVACYPOLICY_MESSAGE = "shipping.checkout.privacy.policy";
	public static final String PRIVACYPOLICY_LABEL = "privacyPolicyLabel";
	public static final String OR_LABEL = "orLabel";
	public static final String OR_MESSAGE = "shipping.checkout.or";
	public static final String CONTACTUS_MESSAGE = "shipping.checkout.contact.us";
	public static final String CONTACTUS_LABEL = "contactUsLabel";
	public static final String FORMORE_LABEL = "forMoreLabel";
	public static final String FORMORE_MESSAGE = "shipping.checkout.for.detail";

	public static final String SUGGESTED_ADD_LABEL = "suggestedAdd";
	public static final String SUGGESTED_ADD_MESSAGE = "shipping.checkout.suggested.text";



	public static final String PAYMENT_INFO_LABEL = "paymentInfoLabel";
	public static final String PAYMENT_INFO_MESSAGE = "shipping.checkout.payment.info";

	public static final String SECURE_INFO_LABEL = "secureInfoLabel";
	public static final String SECURE_INFO_MESSAGE = "shipping.checkout.secure.info";

	public static final String MONTH_LABEL = "monthLabel";
	public static final String MONTH_MESSAGE = "shipping.checkout.month";

	public static final String YEAR_LABEL = "yearLabel";
	public static final String YEAR_MESSAGE = "shipping.checkout.year";

	public static final String SAVE_CARD_DEFAULT_LABEL = "saveCardDefault";
	public static final String SAVE_CARD_DEFAULT_MESSAGE = "shipping.checkout.saveCardDefault";

	public static final String BILLING_ADDRESS_LABEL = "billingAddress";
	public static final String BILLING_ADDRESS_MESSAGE = "shipping.checkout.billingAddress";

	public static final String PLACE_ORDER_LABEL = "placeOrder";
	public static final String PLACE_ORDER_MESSAGE = "shipping.checkout.placeOrder";

	public static final String HELPFUL_INFO_LABEL = "helpFulInformation";
	public static final String HELPFUL_INFO_MESSAGE = "shipping.checkout.helpFulInformation";

	public static final String INFORMATION_DECLERATION_LABEL = "informationDecleration";
	public static final String INFORMATION_DECLERATION_MESSAGE = "shipping.checkout.informationDecleration";

	public static final String SHIPPINGMETHOD_AND_CHARGES_LABEL = "shippingMethodCharges";
	public static final String SHIPPINGMETHOD_AND_CHARGES_MESSAGE = "shipping.checkout.shippingMethodCharges";

	public static final String USER_ADDRESS_TYPE_SHIPPING = "2";
	public static final String USER_ADDRESS_TYPE_BILLING = "3";

	public static final String PRODUCT_OUT_OF_STOCK_ERROR_CODE = "ERROR_CODE_3";

	public static final String PAYMENT_AUTHORIZATION_FAILED = "place.checkout.order.payment.paymentauthorized.failed";

	public static final String PLACE_ORDER_FAILED = "place.checkout.order.failed";

	public static final String FAV_SESSION_DATA = "favoriteData";

	public static final String SIMON_COUNTRY_ISOCODE = "simon.country.isocode";

	public static final String ORDER_PROCESS_ACCEPTED_AND_REJECTED_EVENT = "OrderAcceptedAndRejected";

	public static final String PRODUCT_RECOMMENDATIONS_PAGE_SIZE = "storefront.product.recommendations.pageSize";
	public static final int INT_ZERO = 0;

	public static final String MY_SIZE_BASE_CATEGORY = "my.size.base.category";

	public static final String MY_SIZE_ROOT_CATEGORY = "my.size.root.category";

	public static final String PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SAVE_AND_CONTINUE_MESSAGE = "place.checkout.order.createAccount.saveAndContinue.message";
	public static final String CREATE_ACCOUNT_SKIP_MESSAGE_LABEL1 = "createAccountSkipMessageText1";
	public static final String PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SKIP_MESSAGE_TEXT1 = "place.checkout.order.createAccount.skip.message.text1";
	public static final String CREATE_ACCOUNT_SKIP_MESSAGE_LABEL2 = "createAccountSkipMessageText2";
	public static final String PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SKIP_MESSAGE_TEXT2 = "place.checkout.order.createAccount.skip.message.text2";

	public static final String PIPE = "|";
	public static final String COLON = ":";
	public static final String FIELD_NAME = "names";
	public static final String FIELD_VALUES = "values";
	public static final String OOS = "Out of stock";

	public static final String PIPE_ESCAPE_CHARACTER = "\\|";
	public static final int INT_ONE = 1;
	public static final String REQUIRED_FIELD_VALUE_FOUND_URL = "FoundUrl";

	public static final String CART_OUT_OF_STOCK = "text.cart.page.outofStock";
	public static final String CART_OUT_OF_STOCK_LABLE = "outofStockLabel";
	public static final String CART_INPUT_NOT_VALID = "text.cart.page.inputNotValid";
	public static final String CART_INPUT_NOT_VALID_LABEL = "inputNotValidLabel";
	public static final String CART_REMOVE = "text.cart.page.remove";
	public static final String CART_REMOVE_LABEL = "removeLabel";
	public static final String CART_ADDED = "text.cart.page.added";
	public static final String CART_ADDED_LABEL = "addedLabel";
	public static final String CART_ADD_TO_FAVORITE = "text.cart.page.addtomyfavorites";
	public static final String CART_ADD_TO_FAVORITE_LABEL = "addToFavoritesLabel";
	public static final String CART_ADDED_TO_FAVORITES = "text.cart.page.added.tomyfavorites";
	public static final String CART_ADDED_TO_FAVORITES_LABEL = "addedToFavoritesLabel";
	public static final String CART_CHOOSE_METHODIN_CHECKOUT = "text.cart.page.choose.methodInCheckout";
	public static final String CART_CHOOSE_METHODIN_CHECKOUT_LABEL = "chooseMethodInCheckoutLabel";
	public static final String CART_TAX_CALCULATED_CHECKOUT = "text.cart.page.tax.calculated.during.checkout";
	public static final String CART_TAX_CALCULATED_CHECKOUT_LABEL = "taxCalucatedDuringCheckoutLabel";
	public static final String CART_TOOLLTIP_MSRP = "text.cart.page.tooltip.MSRP";
	public static final String CART_TOOLLTIP_MSRP_LABEL = "MSRPLabel";
	public static final String CART_TOOLLTIP_LISTPRICE = "text.cart.page.tooltip.listPrice";
	public static final String CART_TOOLLTIP_LISTPRICE_LABEL = "listPriceLabel";
	public static final String CART_TOOLLTIP_SALEPRICE = "text.cart.page.tooltip.salePrice";
	public static final String CART_TOOLLTIP_SALEPRICE_LABEL = "salePriceLabel";
	public static final String CART_TOOLLTIP_YOUSAVED = "text.cart.page.tooltip.youSaved";
	public static final String CART_TOOLLTIP_YOUSAVED_LABEL = "youSavedLabel";
	public static final String CART_TOOLLTIP_YOURPRICE = "text.cart.page.tooltip.yourPrice";
	public static final String CART_TOOLLTIP_YOURPRICE_LABEL = "yourPriceLabel";
	public static final String CART_QUANTITY = "text.cart.page.quantity";
	public static final String CART_QUANTITY_LABEL = "quantityLabel";
	public static final String CART_VIEW_BAG_SUMMARY = "text.cart.page.view.bag.summary";
	public static final String CART_VIEW_BAG_SUMMARY_LABEL = "viewBagSummaryLabel";
	public static final String CART_ESTIMATE_COMBINED_SHIPPING = "text.cart.page.estimate.combined.shipping";
	public static final String CART_ESTIMATE_COMBINED_SHIPPING_LABEL = "estimateCombinedShippingLabel";
	public static final String CART_ESTIMATE_COMBINED_TAX = "text.cart.page.estimate.combined.tax";
	public static final String CART_ESTIMATE_COMBINED_TAX_LABEL = "estimateCombinedTaxLabel";
	public static final String CART_ESTIMATE_BAG_TOTAL = "text.cart.page.estimate.bag.total";
	public static final String CART_ESTIMATE_BAG_TOTAL_LABEL = "estimateBagTotalLabel";
	public static final String CART_INFO_REGARDING_TAXS = "text.cart.page.informationRegarding.taxs";
	public static final String CART_INFO_REGARDING_TAXS_LABEL = "informationRegardingTaxsLabel";
	public static final String CART_TAX_MESSAGE1 = "text.cart.page.tax.message1";
	public static final String CART_TAX_MESSAGE1_LABEL = "taxMessage1Label";
	public static final String CART_TAX_MESSAGE2 = "text.cart.page.tax.message2";
	public static final String CART_TAX_MESSAGE2_LABEL = "taxMessage2Label";
	public static final String CART_TAX_MESSAGE_CLOSE_BUTTON = "text.cart.page.tax.message.close.button.name";
	public static final String CART_TAX_MESSAGE_CLOSE_BUTTON_LABEL = "closeButtonNameLabel";
	public static final String CHECKOUT_VIEW_SHOP_BAG = "text.checkout.view.shopping.bag";
	public static final String CHECKOUT_VIEW_SHOP_BAG_LABEL = "viewShoppingBagLabel";
	public static final String CHECKOUT_FIRSTNAME_ERROE_MSG = "text.checkout.firstName.error.msg";
	public static final String CHECKOUT_FIRSTNAME_ERROE_MSG_LABEL = "firstNameErrorMsgLabel";
	public static final String CHECKOUT_LASTNAME_ERROE_MSG = "text.checkout.lastName.error.msg";
	public static final String CHECKOUT_LASTNAME_ERROE_MSG_LABEL = "lastNameErrorMsgLabel";
	public static final String CHECKOUT_ADDRESS_ERROE_MSG = "text.checkout.address.error.msg";
	public static final String CHECKOUT_ADDRESS_ERROE_MSG_LABEL = "addressErrorMsgLabel";
	public static final String CHECKOUT_CITY_ERROE_MSG = "text.checkout.city.error.msg";
	public static final String CHECKOUT_CITY_ERROE_MSG_LABEL = "cityErrorMsgLabel";
	public static final String CHECKOUT_PHONE_ERROE_MSG = "text.checkout.phone.error.msg";
	public static final String CHECKOUT_PHONE_ERROE_MSG_LABEL = "phoneErrorMsgLabel";
	public static final String CHECKOUT_EMAIL_ERROE_MSG = "text.checkout.email.error.msg";
	public static final String CHECKOUT_EMAIL_ERROE_MSG_LABEL = "emailErrorMsgLabel";
	public static final String CHECKOUT_DONOT_SHIP_MSG = "text.checkout.donot.Ship.msg";
	public static final String CHECKOUT_DONOT_SHIP_MSG_LABEL = "donotShipMsgLabel";
	public static final String CHECKOUT_REVIEWORDER_SHIP_METD = "text.checkout.ReviewOrder.Ship.method";
	public static final String CHECKOUT_REVIEWORDER_SHIP_METD_LABEL = "reviewOrderShipLabel";
	public static final String CHECKOUT_REVIEWORDER_SHIP_ITEMS = "text.checkout.ReviewOrder.Ship.items";
	public static final String CHECKOUT_REVIEWORDER_SHIP_ITEMS_LABEL = "itemsLabel";
	public static final String CHECKOUT_REVIEWORDER_SHIP_SUBTOTAL = "text.checkout.ReviewOrder.Ship.subtotal";
	public static final String CHECKOUT_REVIEWORDER_SHIP_SUBTOTAL_LABEL = "subtotalLabel";
	public static final String CHECKOUT_REVIEWORDER_SHIP_ITEM_PRICE = "text.checkout.ReviewOrder.Ship.itemPrice";
	public static final String CHECKOUT_REVIEWORDER_SHIP_ITEM_PRICE_LABEL = "itemPriceLabel";
	public static final String CHECKOUT_REVIEWORDER_SELECT_SHIP = "text.checkout.ReviewOrder.Ship.select.Ship";
	public static final String CHECKOUT_REVIEWORDER_SELECT_SHIP_LABEL = "selectShipLabel";
	public static final String CHECKOUT_REVIEWORDER_VIEW_ITEM = "text.checkout.ReviewOrder.Ship.viewItem";
	public static final String CHECKOUT_REVIEWORDER_VIEW_ITEM_LABEL = "viewItemLabel";
	public static final String CHECKOUT_REVIEWORDER_CLOSE_ITEM = "text.checkout.ReviewOrder.Ship.closeItem";
	public static final String CHECKOUT_REVIEWORDER_CLOSE_ITEM_LABEL = "closeItemLabel";
	public static final String CHECKOUT_SAVEINFO = "text.checkout.saveInfo.save.info";
	public static final String CHECKOUT_SAVEINFO_LABEL = "saveInfoLabel";
	public static final String CHECKOUT_SAVEINFO_OPTIONAL = "text.checkout.saveInfo.optional";
	public static final String CHECKOUT_SAVEINFO_OPTIONAL_LABEL = "optionalLabel";
	public static final String CHECKOUT_SAVEINFO_CREATE_VIP_MSG = "text.checkout.saveInfo.create.vip.msg";
	public static final String CHECKOUT_SAVEINFO_CREATE_VIP_MSG_LABEL = "createVipMsgLabel";
	public static final String CHECKOUT_SAVEINFO_UPPERCASE_CHAR = "text.checkout.saveInfo.uppercase.char";
	public static final String CHECKOUT_SAVEINFO_UPPERCASE_CHAR_LABEL = "uppercaseCharLabel";
	public static final String CHECKOUT_SAVEINFO_LOWERCASE_CHAR = "text.checkout.saveInfo.lowercase.char";
	public static final String CHECKOUT_SAVEINFO_LOWERCASE_CHAR_LABEL = "lowercaseCharLabel";
	public static final String CHECKOUT_SAVEINFO_NUMERIC_MSG = "text.checkout.saveInfo.numeric.msg";
	public static final String CHECKOUT_SAVEINFO_NUMERIC_MSG_LABEL = "numericMsgLabel";
	public static final String CHECKOUT_SAVEINFO_ALPHANUMERIC_MSG = "text.checkout.saveInfo.alphanumeric.msg";
	public static final String CHECKOUT_SAVEINFO_ALPHANUMERIC_MSG_LABEL = "alphanumericMsgLabel";

	public static final String CHECKOUT_SAVEINFO_PASS = "text.checkout.saveInfo.password";
	public static final String CHECKOUT_SAVEINFO_PASS_LABEL = "passwordLabel";
	public static final String CHECKOUT_SAVEINFO_PASS_ERROR_MSG = "text.checkout.saveInfo.password.error.msg";
	public static final String CHECKOUT_SAVEINFO_PASS_ERROR_MSG_LABEL = "passwordErrorMsgLabel";
	public static final String CHECKOUT_SAVEINFO_MIN_PASS_ERROR_MSG = "text.checkout.saveInfo.password.min.error.msg";
	public static final String CHECKOUT_SAVEINFO_MIN_PASS_ERROR_MSG_LABEL = "passwordMinErrorMsgLabel";
	public static final String CHECKOUT_SAVEINFO_CONFIRM_PASS = "text.checkout.saveInfo.confirm.password";
	public static final String CHECKOUT_SAVEINFO_CONFIRM_PASS_LABEL = "confirmPasswordLabel";
	public static final String CHECKOUT_SAVEINFO_CONFIRM_PASS_ERROR_MSG = "text.checkout.saveInfo.confirm.password.error.msg";
	public static final String CHECKOUT_SAVEINFO_CONFIRM_PASS_ERROR_MSG_LABEL = "confirmPwdErrorMsgLabel";
	public static final String CHECKOUT_SAVEINFO_SKIP = "text.checkout.saveInfo.skip";
	public static final String CHECKOUT_SAVEINFO_SKIP_LABEL = "skipLabel";

	public static final String CHECKOUT_PAYMENT = "text.checkout.payment";
	public static final String CHECKOUT_PAYMENT_LABEL = "paymentLabel";
	public static final String CHECKOUT_PAYMENT_CREDITOR_DEBIT = "text.checkout.payment.creditOrDebit";
	public static final String CHECKOUT_PAYMENT_CREDITOR_DEBIT_LABEL = "creditOrDebitLable";
	public static final String CHECKOUT_PAYMENT_INFO_SECURE = "text.checkout.payment.your.info.secure";
	public static final String CHECKOUT_PAYMENT_INFO_SECURE_LABEL = "infoSecureLabel";
	public static final String CHECKOUT_PAYMENT_INDICATE_OPTIONAL = "text.checkout.payment.indicate.optional.field";
	public static final String CHECKOUT_PAYMENT_INDICATE_OPTIONAL_LABEL = "indicateOptionalLabel";
	public static final String CHECKOUT_PAYMENT_PAYMENTMETHOD = "text.checkout.payment.payment.method";
	public static final String CHECKOUT_PAYMENT_PAYMENTMETHOD_LABEL = "paymentMethodLabel";
	public static final String CHECKOUT_PAYMENT_ENDINGIN = "text.checkout.payment.ending.in";
	public static final String CHECKOUT_PAYMENT_ENDINGIN_LABLE = "endingInLabel";
	public static final String CHECKOUT_PAYMENT_ADD_NEW_PAYMENT = "text.checkout.payment.add.new.payent.method";
	public static final String CHECKOUT_PAYMENT_ADD_NEW_PAYMENT_LABEL = "addNewPayentMethodLabel";
	public static final String CHECKOUT_PAYMENT_EXP_MONTH = "text.checkout.payment.exp.month";
	public static final String CHECKOUT_PAYMENT_EXP_MONTH_LABEL = "expmMonthLabel";
	public static final String CHECKOUT_PAYMENT_EXP_YEAR = "text.checkout.payment.exp.year";
	public static final String CHECKOUT_PAYMENT_EXP_YEAR_LABEL = "expYearLabel";
	public static final String CHECKOUT_PAYMENT_SAVE_CARD_BILLING = "text.checkout.payment.save.card.billing.address";
	public static final String CHECKOUT_PAYMENT_SAVE_CARD_BILLING_LABEL = "saveCardBillingLabel";
	public static final String CHECKOUT_PAYMENT_MAKE_DEFAULT = "text.checkout.payment.make.default.next.time";
	public static final String CHECKOUT_PAYMENT_MAKE_DEFAULT_LABEL = "makeDefaultLabel";
	public static final String CHECKOUT_PAYMENT_ADD_NEW_BILLING = "text.checkout.payment.add.new.billing.address";
	public static final String CHECKOUT_PAYMENT_ADD_NEW_BILLING_LABEL = "addNewBillingLabel";
	public static final String CHECKOUT_PAYMENT_ADDRESS2 = "text.checkout.payment.address2";
	public static final String CHECKOUT_PAYMENT_ADDRESS2_LABEL = "address2ndLabel";
	public static final String CHECKOUT_PAYMENT_REGION = "text.checkout.payment.region";
	public static final String CHECKOUT_PAYMENT_REGION_LABEL = "regionLabel";
	public static final String CHECKOUT_PAYMENT_POLICY_MSG = "text.checkout.payment.policy.msg";
	public static final String CHECKOUT_PAYMENT_POLICY_MSG_LABEL = "policyMsgLabel";
	public static final String CHECKOUT_PAYMENT_TERMOF_USE = "text.checkout.payment.termof.use";
	public static final String CHECKOUT_PAYMENT_TERMOF_USE_LABEL = "termofUseLabel";
	public static final String CHECKOUT_PAYMENT_AND = "text.checkout.payment.and";
	public static final String CHECKOUT_PAYMENT_AND_LABEL = "andLabel";
	public static final String CHECKOUT_PAYMENT_PRIVACY = "text.checkout.payment.privacy.policy";
	public static final String CHECKOUT_PAYMENT_PRIVACY_LABEL = "policyLabel";
	public static final String CHECKOUT_PAYMENT_HELPFUL_MSG = "text.checkout.payment.helpful.msg";
	public static final String CHECKOUT_PAYMENT_HELPFUL_MSG_LABEL = "helpfulMsgLabel";
	public static final String CHECKOUT_PAYMENT_HELPFUL_QUESTION = "text.checkout.payment.helpful.question";
	public static final String CHECKOUT_PAYMENT_HELPFUL_QUESTION_LABEL = "helpfulQuestionLabel";
	public static final String CHECKOUT_PAYMENT_CONTACT_US = "text.checkout.payment.contactUs";
	public static final String CHECKOUT_PAYMENT_CONTACT_US_LABEL = "contactUsLabel";


	public static final String CHECKOUT_VERIFY_ADDRESS = "text.checkout.Verify.address";
	public static final String CHECKOUT_VERIFY_ADDRESS_LABEL = "verifyAddressLabel";
	public static final String CHECKOUT_VERIFY_ADDRESS_MSG = "text.checkout.Verify.address.msg";
	public static final String CHECKOUT_VERIFY_ADDRESS_MSG_LABEL = "addressMsgLabel";
	public static final String CHECKOUT_VERIFY_ADDRESS_YOU_ENTERED = "text.checkout.Verify.address.you.entered";
	public static final String CHECKOUT_VERIFY_ADDRESS_YOU_ENTERED_LABEL = "youEnteredLabel";
	public static final String CHECKOUT_VERIFY_ADDRESS_USE_ADDRESS = "text.checkout.Verify.address.use.address";
	public static final String CHECKOUT_VERIFY_ADDRESS_USE_ADDRESS_LABEL = "useAddressLabel";
	public static final String CHECKOUT_VERIFY_ADDRESS_SUGGESTED_ADDRESS = "text.checkout.Verify.address.suggested.address";
	public static final String CHECKOUT_VERIFY_ADDRESS_SUGGESTED_ADDRESS_LABEL = "suggestedAddressLabel";

	public static final String TRACK_ORDER_RESULTFOR = "text.track.order.Resultfor";
	public static final String TRACK_ORDER_RESULTFOR_LABEL = "resultforLabel";
	public static final String TRACK_ORDER_AND = "text.track.order.and";
	public static final String TRACK_ORDER_AND_LABEL = "trackOrderAndLabel";
	public static final String TRACK_ORDER_ORDER_DATE = "text.track.order.order.date";
	public static final String TRACK_ORDER_ORDER_DATE_LABEL = "orderDateLabel";
	public static final String TRACK_ORDER_RETAILER = "text.track.order.reatiler";
	public static final String TRACK_ORDER_RETAILER_LABEL = "trackOrderReatilerLabel";
	public static final String TRACK_ORDER_TOTAL_PRICE = "text.track.order.total.price";
	public static final String TRACK_ORDER_TOTAL_PRICE_LABEL = "trackOrderTotalPriceLabel";
	public static final String TRACK_ORDER_ORDER_DETAIL = "text.track.order.order.detail";
	public static final String TRACK_ORDER_ORDER_DETAIL_LABEL = "trackOrderOrderDetailLabel";
	public static final String CHECKOUT_VERIFY_FAIL_ADDRESS_LABEL = "checkout.address.verification.fail";

}
