package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CreditCardPaymentInfoPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;


/**
 * Extend the credit cart info populator.
 */
public class ExtCreditCardPaymentInfoPopulator extends CreditCardPaymentInfoPopulator
{
	@Resource
	private ConfigurationService configurationService;
	private static final String CARD_TYPE_VAULT_PREFIX = "cardtype.vault.";


	@Override
	public void populate(final CreditCardPaymentInfoModel source, final CCPaymentInfoData target)
	{
		this.superPopulate(source, target);
		target.setPaymentToken(source.getPaymentToken());
		target.setUpdated(source.getUpdated());
		target.setLastFourDigits(source.getLastFourDigits());
		target.setCreditCardTypeInVault(
				configurationService.getConfiguration().getString(CARD_TYPE_VAULT_PREFIX + source.getType().getCode()));

		if (source.getBillingAddress() != null)
		{
			final AddressData billingAddress = getAddressConverter().convert(source.getBillingAddress());
			target.setBillingAddress(billingAddress);
		}

	}

	protected void superPopulate(final CreditCardPaymentInfoModel source, final CCPaymentInfoData target)
	{
		super.populate(source, target);
	}

}
