package com.simon.facades.navigation;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.simon.core.dto.NavigationData;


/**
 * This interface provides method which fetches category hierarchy of given L1 nodes and provides pair of L1 and leaf
 * category codes.
 */
public interface LeftNavigationCategoryFacade
{
	/**
	 * This method makes a pair of L1 category and leaf category from the category code
	 *
	 * @param categoryCode
	 *           category code part of URL
	 * @return Pair of L1 and leaf category
	 */
	Pair<String, String> getL1AndLeafCategoryCode(final String categoryCode);

	/**
	 * This method returns complete category hierarchy corresponding to particular L1 category. If no entry is found
	 * corresponding to L1 category, empty list is returned.
	 *
	 * @param l1categoryCode
	 * @return
	 */
	List<NavigationData> getNavigationData(final String l1categoryCode);


}
