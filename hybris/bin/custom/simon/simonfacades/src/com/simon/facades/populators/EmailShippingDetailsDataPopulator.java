package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.email.order.ShippingDetailsData;


/**
 * Converts AddressData to ShippingDetailsData
 */
public class EmailShippingDetailsDataPopulator implements Populator<AddressData, ShippingDetailsData>
{

	/**
	 * populates the AddressData from the contents in ShippingDetailsData
	 */
	@Override
	public void populate(final AddressData source, final ShippingDetailsData target)
	{
		target.setFirstName(StringUtils.isNotEmpty(source.getFirstName()) ? source.getFirstName() : StringUtils.EMPTY);
		target.setLastName(StringUtils.isNotEmpty(source.getLastName()) ? source.getLastName() : StringUtils.EMPTY);
		target.setAddressLine1(StringUtils.isNotEmpty(source.getLine1()) ? source.getLine1() : StringUtils.EMPTY);
		target.setAddressLine2(StringUtils.isNotEmpty(source.getLine2()) ? source.getLine2() : StringUtils.EMPTY);
		target.setCity(StringUtils.isNotEmpty(source.getTown()) ? source.getTown() : StringUtils.EMPTY);
		target.setZip(StringUtils.isNotEmpty(source.getPostalCode()) ? source.getPostalCode() : StringUtils.EMPTY);

		String shippingState = StringUtils.EMPTY;
		final RegionData regionData = source.getRegion();
		if (null != regionData)
		{
			shippingState = StringUtils.isNotEmpty(regionData.getName()) ? regionData.getName() : StringUtils.EMPTY;
		}
		target.setState(shippingState);
	}
}
