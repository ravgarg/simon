
package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * This is a populator class which is used to populate the Ext Address Data into User Address DTO to perform the CURD
 * operation on User address.
 */
public class UserAddressPopulator implements Populator<ExtAddressData, UserAddressDTO>
{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserAddressPopulator.class);

	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	@Override
	public void populate(final ExtAddressData source, final UserAddressDTO target)
	{

		LOGGER.debug("UserAddressPopulator ::: populate :: source : {} , target : {} ", source, target);

		target.setExternalId(userIntegrationUtils.addEnvPrefixToAddressId(source.getId()));
		target.setStreet3(StringUtils.EMPTY);
		target.setType(SimonFacadesConstants.USER_ADDRESS_TYPE_SHIPPING);

		target.setPrimary(source.isDefaultAddress() ? 1 : 0);

		target.setCity(source.getTown() != null ? source.getTown() : StringUtils.EMPTY);
		target.setFirstName(source.getFirstName() != null ? source.getFirstName() : StringUtils.EMPTY);
		target.setLastName(source.getLastName() != null ? source.getLastName() : StringUtils.EMPTY);
		target.setStreet1(source.getLine1() != null ? source.getLine1() : StringUtils.EMPTY);
		target.setStreet2(source.getLine2() != null ? source.getLine2() : StringUtils.EMPTY);
		target.setZip(source.getPostalCode() != null ? source.getPostalCode() : StringUtils.EMPTY);

		if (source.getRegion() != null)
		{
			target.setState(source.getRegion().getIsocodeShort());
		}
		if (source.getCountry() != null)
		{
			target.setCountry(source.getCountry().getIsocode());
		}

		LOGGER.debug("UserAddressPopulator ::: populate :: target : {}", target);
	}

}
