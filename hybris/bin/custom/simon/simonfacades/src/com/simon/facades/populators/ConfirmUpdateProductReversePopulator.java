package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


/**
 * populate AdditionalProductDetailsModel from OrderConfirmCartProductsCallBackDTO which have information of retailers,
 * added products.
 *
 */
public class ConfirmUpdateProductReversePopulator
		implements Populator<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel>
{

	@Resource
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	/**
	 * Method populate AdditionalProductDetailsModel from OrderConfirmCartProductsCallBackDTO which have information of
	 * retailers, added products.
	 */
	@Override
	public void populate(final OrderConfirmCartProductsCallBackDTO source, final AdditionalProductDetailsModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setProductID(source.getProductId() != null ? source.getProductId() : StringUtils.EMPTY);
		target.setDiscountedPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getDiscountedPrice()).toString());
		target.setOriginalPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getOriginalPrice()).toString());
		target.setProductPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getPrice()).toString());
		target.setStatus(source.getStatus() != null ? source.getStatus() : StringUtils.EMPTY);
	}

}
