package com.simon.facades.populators;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


/**
 * This class modifies the population logic of SolrSearchQueryEncoderPopulator class.
 *
 * If the filter terms contains multiple categoryPaths filter, only the last filter term is appended in the URL, to make
 * the MultiSelect filter behave like single select.
 */
public class ExtSolrSearchQueryEncoderPopulator implements Populator<SolrSearchQueryData, SearchQueryData>
{

	private static final Logger LOG = Logger.getLogger(ExtSolrSearchQueryEncoderPopulator.class);

	@Override
	public void populate(final SolrSearchQueryData source, final SearchQueryData target)
	{
		final StringBuilder builder = new StringBuilder();

		if (source != null)
		{
			if (StringUtils.isNotBlank(source.getFreeTextSearch()))
			{
				builder.append(source.getFreeTextSearch());
			}

			builder.append(':');

			if (StringUtils.isNotBlank(source.getSort()))
			{
				builder.append(source.getSort());
			}

			final List<SolrSearchQueryTermData> terms = source.getFilterTerms();
			final List<SolrSearchQueryTermData> categoryTermsList = new ArrayList<>();

			if (null != terms)
			{
			terms.stream().filter(term -> StringUtils.isNotBlank(term.getKey()) && StringUtils.isNotBlank(term.getValue()))
					.forEach(term -> {
						if (StringUtils.equalsIgnoreCase(term.getKey(), IndexedKeys.CATEGORY_PATH))
						{
							categoryTermsList.add(term);
						}
						else
						{
							setTermInBuilder(builder, term);
						}
					});
			}
			if (CollectionUtils.isNotEmpty(categoryTermsList))
			{
				setTermInBuilder(builder, categoryTermsList.get(categoryTermsList.size() - 1));
			}
		}

		final String result = builder.toString();

		// Special case for empty query
		target.setValue(StringUtils.equals(":", result) ? StringUtils.EMPTY : result);

	}

	/**
	 * This method appends the term key and value in the builder.
	 *
	 * @param builder
	 * @param term
	 */
	private void setTermInBuilder(final StringBuilder builder, final SolrSearchQueryTermData term)
	{
		try
		{
			builder.append(':').append(term.getKey()).append(':').append(URLEncoder.encode(term.getValue(), "UTF-8"));
		}
		catch (final UnsupportedEncodingException e)
		{
			// UTF-8 is supported encoding, so it shouldn't come here
			LOG.error("Solr search query URLencoding failed.", e);
		}
	}
}
