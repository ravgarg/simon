package com.simon.facades.populators;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cmsfacades.data.CMSLinkComponentData;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.simon.core.model.BackgroundImageItemModel;
import com.simon.facades.cms.BackgroundImageData;


/**
 * Populates {@link BackgroundImageData} with MediaData and CMSLinkComponentData
 */
public class SimonBackgroundImageDataPopulator implements Populator<BackgroundImageItemModel, BackgroundImageData>
{


	private Converter<MediaModel, MediaData> mediaModelConverter;

	private Converter<CMSLinkComponentModel, CMSLinkComponentData> cmsLinkComponentModelConverter;


	@Override
	public void populate(final BackgroundImageItemModel source, final BackgroundImageData target)
	{

		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");
		target.setDesktopImage(getMediaModelConverter().convert(source.getDesktopImage()));
		if (source.getDestinationLink() != null)
		{
			target.setDestinationLink(getCmsLinkComponentModelConverter().convert(source.getDestinationLink()));
		}
		target.setImageDescription(source.getImageDescription());
		if (source.getMobileImage() != null)
		{
			target.setMobileImage(getMediaModelConverter().convert(source.getMobileImage()));
		}
	}

	/**
	 * @return
	 */
	public Converter<MediaModel, MediaData> getMediaModelConverter()
	{
		return mediaModelConverter;
	}

	/**
	 * @param mediaModelConverter
	 */
	public void setMediaModelConverter(final Converter<MediaModel, MediaData> mediaModelConverter)
	{
		this.mediaModelConverter = mediaModelConverter;
	}




	/**
	 * @return
	 */
	public Converter<CMSLinkComponentModel, CMSLinkComponentData> getCmsLinkComponentModelConverter()
	{
		return cmsLinkComponentModelConverter;
	}

	/**
	 * @param cmsLinkComponentModelConverter
	 */
	public void setCmsLinkComponentModelConverter(
			final Converter<CMSLinkComponentModel, CMSLinkComponentData> cmsLinkComponentModelConverter)
	{
		this.cmsLinkComponentModelConverter = cmsLinkComponentModelConverter;
	}


}
