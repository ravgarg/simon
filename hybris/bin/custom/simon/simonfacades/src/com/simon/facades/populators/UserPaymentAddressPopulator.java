package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.integration.users.dto.UserPaymentAddressDTO;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * This is a populator class which is used to populate the "UserPaymentAddressDTO"
 */
public class UserPaymentAddressPopulator implements Populator<CCPaymentInfoData, UserPaymentAddressDTO>
{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserAddressPopulator.class);

	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	@Override
	public void populate(final CCPaymentInfoData source, final UserPaymentAddressDTO target)
	{
		LOGGER.debug("UserPaymentAddressPopulator ::: populate :: source : {} , target : {} ", source, target);

		final AddressData billingAddress = source.getBillingAddress();

		if (billingAddress != null)
		{
			populateAddressData(target, billingAddress);
		}

		target.setCardType(userIntegrationUtils.getCreditCardCode(source.getCardType()));
		target.setCardLastFourDigits(source.getLastFourDigits());
		target.setPaymentToken(source.getPaymentToken());

		LOGGER.debug("UserPaymentAddressPopulator ::: populate :: target : {} ", target);
	}


	/**
	 * This method is used to populate the address DTO for user payment address
	 *
	 * @param target
	 * @param source
	 */
	private void populateAddressData(final UserPaymentAddressDTO target, final AddressData billingAddress)
	{


		LOGGER.debug("UserPaymentAddressPopulator ::: populateAddressData :: billingAddress : {} ", billingAddress);

		target.setExternalId(userIntegrationUtils.addEnvPrefixToAddressId(billingAddress.getId()));
		target.setStreet3(StringUtils.EMPTY);
		target.setType(SimonFacadesConstants.USER_ADDRESS_TYPE_BILLING);

		target.setPrimary(billingAddress.isDefaultAddress() ? 1 : 0);

		target.setCity(billingAddress.getTown() != null ? billingAddress.getTown() : StringUtils.EMPTY);
		target.setFirstName(billingAddress.getFirstName() != null ? billingAddress.getFirstName() : StringUtils.EMPTY);
		target.setLastName(billingAddress.getLastName() != null ? billingAddress.getLastName() : StringUtils.EMPTY);
		target.setStreet1(billingAddress.getLine1() != null ? billingAddress.getLine1() : StringUtils.EMPTY);
		target.setStreet2(billingAddress.getLine2() != null ? billingAddress.getLine2() : StringUtils.EMPTY);
		target.setZip(billingAddress.getPostalCode() != null ? billingAddress.getPostalCode() : StringUtils.EMPTY);

		if (billingAddress.getRegion() != null)
		{
			target.setState(billingAddress.getRegion().getIsocodeShort());
		}
		if (billingAddress.getCountry() != null)
		{
			target.setCountry(billingAddress.getCountry().getIsocode());
		}

		LOGGER.debug("UserPaymentAddressPopulator ::: populateAddressData :: target : {}", target);
	}


}

