package com.simon.facades.search.converters.populator;

import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryFacetsPopulator;

import org.apache.solr.client.solrj.SolrQuery;


/**
 * The Class ExtFacetSearchQueryFacetsPopulator set facet limit to unlimited (-1).
 */
public class ExtFacetSearchQueryFacetsPopulator extends FacetSearchQueryFacetsPopulator
{

	/**
	 * Populate facet limit to unlimited.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target)
	{
		callSuperMethod(source, target);
		target.setFacetLimit(-1);
	}

	/**
	 * Call super method.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	protected void callSuperMethod(final SearchQueryConverterData source, final SolrQuery target)
	{
		super.populate(source, target);
	}

}
