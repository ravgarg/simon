/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.facades.order;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.ui.Model;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.checkout.data.ExtCheckoutData;
import com.simon.facades.checkout.data.StateData;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;


/**
 * Simon Checkout facade interface extending {@link CheckoutFacade}. Service is responsible for getting information for
 * checkout.
 */
public interface ExtCheckoutFacade extends AcceleratorCheckoutFacade
{

	/**
	 * This method is to check if payment information is present within cart.
	 *
	 * @return true if payment information is present.
	 */
	public boolean hasPaymentInfo();

	/**
	 * This method is used to set the details with in the cart, and then returning the cartData {@link CartData}. return
	 * cartData of type {@link CartData}.
	 */
	public CartData getCheckoutCart();

	/**
	 * Method is used to set the payment related address by using the information provided with in {@link AddressData}.
	 *
	 * @param addressData
	 *           as the {@link AddressData}, used to get the address related information for payment.
	 */
	public void setPaymentAddress(AddressData addressData);

	/**
	 * Gets the checkout data.
	 *
	 * @param model
	 *           the model
	 * @param cartData
	 *           the cart data
	 * @param addressData
	 *           the address data
	 * @param suggestedAddress
	 *           the suggested address
	 * @return the checkout data
	 */
	public ExtCheckoutData getCheckoutData(final Model model, final CartData cartData, final ExtAddressData addressData,
			final ExtAddressData suggestedAddress);

	/**
	 * @return ExtCheckoutData
	 */
	ExtCheckoutData getCheckoutData();

	/**
	 * @param additionalCartInfo
	 * @return boolean
	 */
	boolean validateCartForRetailerShippingMethods(AdditionalCartInfoModel additionalCartInfo);

	/**
	 * @param selectedShippingMethod
	 * @return boolean
	 * @throws CartCheckOutIntegrationException
	 * @throws IOException
	 * @throws CalculationException
	 */
	boolean estimateCart(String selectedShippingMethod) throws CartCheckOutIntegrationException, IOException, CalculationException;

	/**
	 * Create new payment subscription. Pass in a CCPaymentInfoData containing the customer's card details. A new payment
	 * subscription will be created, and the sorted card details will be returned in a new CCPaymentInfoData.
	 *
	 * @param extPaymentInfoData
	 *           the data instance containing the customers cart details
	 * @return the newly created payment info data
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	public CCPaymentInfoData createPaymentInfoSubscription(final CCPaymentInfoData extPaymentInfoData)
			throws UserAddressIntegrationException, CartCheckOutIntegrationException;


	/**
	 * @param retailerId
	 * @param shippingMethod
	 */
	public void setDeliveryMethodForRetailer(String retailerId, String shippingMethod);

	/**
	 * @param shippingmethod
	 * @param extCheckoutData
	 * @param loadReviewPanel
	 * @return ExtCheckoutData
	 */
	ExtCheckoutData getCheckoutDataForReview(String shippingmethod, ExtCheckoutData extCheckoutData);

	/**
	 * Method to populate states for shipping.
	 *
	 * @param model
	 *           the Model object.
	 *
	 * @return List of StateData.
	 *
	 */
	List<StateData> getAllStates(final Model model);

	/**
	 * This method will give the excluded states list.
	 *
	 * @return StateList
	 */
	List<String> getExcludedStateList();

	/**
	 * Determine if a credit card is chargeable card and available for purchases. if it is successfully verified then
	 * transaction request was successfully executed then true, otherwise false
	 *
	 * @param paymentMethodToken
	 * @param retainOnSuccess
	 * @param continueCaching
	 * @throws CartCheckOutIntegrationException
	 */
	void verifyToken(String paymentMethodToken, boolean retainOnSuccess, boolean continueCaching)
			throws CartCheckOutIntegrationException;

	/**
	 * This method use to deliver order via ESB to third party
	 *
	 * @param orderCode
	 * @param paymentMethodToken
	 * @throws CartCheckOutIntegrationException
	 */
	public void deliver(String orderCode, String paymentMethodToken) throws CartCheckOutIntegrationException;

	/**
	 * Gets the checkout data. This method is used to create ExtCheckoutData from CartData and model, whose url is passed
	 * as parameter to this method. This method is used for edit checkout.
	 *
	 * @param model
	 *           the model
	 * @param cartData
	 *           the cart data
	 * @return the checkout data
	 */
	public ExtCheckoutData getCheckoutData(final Model model, final CartData cartData);

	/**
	 * This method will check for the response received as the purchase callback and will calculate the order acceptance
	 * and rejection logic. Depending upon the resultant the order status will be saved and the order process that was in
	 * waiting will be re started.
	 *
	 * @param orderConfirmCallBackDTO
	 */
	void savePurchaseCallbackResponse(OrderConfirmCallBackDTO orderConfirmCallBackDTO);

	/**
	 * This method will handled error response for purchase call back. This method will check for any error code
	 * available then it will update the order status as Rejected.
	 *
	 * @param orderConfirmCallBackDTO
	 */
	void savePurchaseCallbackErrorResponse(OrderConfirmCallBackDTO orderConfirmCallBackDTO);

	/**
	 * This method will handled error response for purchase confirm call back. This method will check for any error code
	 * available then it will update the order status as Rejected.
	 *
	 * @param orderConfirmCallBackDTO
	 */
	void savePurchaseConfirmCallbackErrorResponse(OrderConfirmCallBackDTO orderConfirmCallBackDTO);

	/**
	 * This method will check for the response received as the purchase callback Depending upon the resultant the order
	 * status will be saved and the order process that was in waiting will be re started.
	 *
	 * @param orderConfirmCallBackDTO
	 * @param orderCode
	 */
	void savePurchaseConfirmCallBackResponse(OrderConfirmCallBackDTO orderConfirmCallBackDTO, String orderCode);

	/**
	 * This method will get order History Data for guest user depending upon the order Id and email id mapped on
	 * particular order
	 *
	 *
	 * @param orderId
	 * @param emailId
	 * @return OrderHistoryData
	 */
	OrderHistoryData getGuestOrderForTracking(String orderId, String emailId);

	/**
	 * This method validate create cart call back response
	 *
	 * @param cart
	 * @param waitTime
	 * @param count
	 * @return boolean
	 */
	boolean validateCreateCartCallbackResponse(CartModel cart, int waitTime, int count);

	/**
	 * This method use to validate cart
	 *
	 * @return
	 */
	public boolean validateCart();


	/**
	 * This Method use to estimate and validate cart
	 *
	 * @param extCheckoutData
	 * @param selectedShippingMethod
	 * @return
	 */
	public ExtCheckoutData estimateAndValidateCart(final ExtCheckoutData extCheckoutData, final String selectedShippingMethod);

	/**
	 * This method use to validate cart
	 *
	 * @return List<String>
	 */
	public List<String> validateCartForVariant();

	/**
	 * @description Overridden method to set the email address in the address model when email address is not exist
	 * @method setDeliveryAddress
	 * @param addressData
	 * @return boolean
	 */
	boolean setDeliveryAddress(final AddressData addressData);


	/**
	 * This method use to validate cart
	 *
	 * @return List<String>
	 */
	public List<String> validateCartForVariantValues();

	/**
	 * This method used to validate payment card expiry date.
	 *
	 * @param paymentInfoId
	 * @return boolean
	 * @throws ParseException
	 */
	boolean validatePaymentMethodInternal(String paymentInfoId) throws ParseException;

	/**
	 * @return List<CCPaymentInfoData>
	 * @throws UserAddressIntegrationException
	 */
	List<CCPaymentInfoData> getCCPaymentInfoFromExternalSystem() throws UserAddressIntegrationException;

	/**
	 * This method used to validate if retailer Order ID is present in last purchase callback or not
	 *
	 * @param orderConfirmCallBackDTO
	 * @return boolean
	 */
	public boolean validateRetailerOrderIDIsPresent(OrderConfirmCallBackDTO orderConfirmCallBackDTO);

	/**
	 * This method used to save last purchase callback response in case of partial success scenario
	 *
	 * @param orderConfirmCallBackDTO
	 * @param orderId
	 */
	public void savePurchaseConfirmCallBackPartialResponse(OrderConfirmCallBackDTO orderConfirmCallBackDTO, String orderId);

	/**
	 *
	 */
	public void checkAdditionalCartInfo(ExtCheckoutData checkoutData);

}
