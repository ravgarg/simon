package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.facades.account.data.StoreData;


public class ReverseStorePopulator implements Populator<StoreData, ShopModel>
{

	@Override
	public void populate(final StoreData source, final ShopModel target) throws ConversionException
	{
		if (null != target.getBanner())
		{
			target.getBanner().setURL(source.getBanner());
		}
		else
		{
			final MediaModel media = new MediaModel();
			target.setBanner(media);
			target.getBanner().setURL(source.getBanner());
		}

		target.setId(source.getId());
		target.setDescription(source.getDescription());
		target.setName(source.getName());
	}

}
