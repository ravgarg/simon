package com.simon.facades.mirakl.product.populator;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.io.File;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang3.tuple.Pair;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.product.populators.ProductImportFileContextDataPopulator;


/**
 * Extended Mirakl Product Import Context populator to populate custom fields
 *
 */
public class ExtProductImportFileContextDataPopulator extends ProductImportFileContextDataPopulator
{

	@Override
	public void populate(final Pair<ProductImportGlobalContextData, File> source, final ProductImportFileContextData target)
			throws ConversionException
	{
		super.populate(source, target);
		target.setMappingFailure(new ConcurrentLinkedQueue<String>());
		target.setProductFailure(new ConcurrentLinkedQueue<String>());
	}
}
