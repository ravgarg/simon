package com.simon.facades.populators;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.model.DealModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DealDataType;
import com.simon.facades.customer.ExtCustomerFacade;



public class ExtDealPopulator implements Populator<DealModel, DealData>
{
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Override
	public void populate(final DealModel source, final DealData target) throws ConversionException
	{
		populateDealURL(source, target);
		target.setCode(source.getCode());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		setValidity(target);
		if (null != source.getStoreType())
		{
			target.setStoreType(source.getStoreType().getCode());
		}
		target.setId(source.getCode());
		setLogo(source, target);
		final boolean favDeal = extCustomerFacade.isFavDeal(source.getCode());
		target.setFavDeal(favDeal);
		target.setFavorite(favDeal);
		if (source.getShop() != null)
		{
		target.setRetailerName(source.getShop().getName());
		}
	}

	/**
	 * This method calculate deal URL on basis of four conditions 1) if deal have promotion and promotion is product
	 * level promotion then deal URL will be deal listing page. 2) if deal have promotion and promotion is retailer
	 * specific promotion the URL will be that retailer store landing page. 3) if deal have no promotion and have direct
	 * association with product then URL will be deal listing page. 4) if deal have no promotion and no direct
	 * association with product the externalDeaURL we be URL
	 *
	 * @param source
	 * @param target
	 */
	private void populateDealURL(final DealModel source, final DealData target)
	{
		final List<PromotionSourceRuleModel> promotionList = ((List<PromotionSourceRuleModel>) source.getPromotionSourceRule());
		if (CollectionUtils.isNotEmpty(promotionList))
		{

			setUrlForDealWithPromotion(source, target, promotionList);
		}
		else if (CollectionUtils.isNotEmpty(source.getProducts()))
		{
			setStartAndEndDate(source.getStartDate(), source.getEndDate(), target);
			final Collection<ContentPageModel> contentPages = source.getContentPage();
			if (CollectionUtils.isNotEmpty(contentPages))
			{
				for (final ContentPageModel contentPage : contentPages)
				{
					if (ContentPageType.LISTINGPAGE.equals(contentPage.getPageType()))
					{
						target.setDealUrl(contentPage.getLabel());
						target.setDealUrlType(DealDataType.PRODUCT_DEAL_URL.name());
						break;
					}
				}
			}
		}
		else
		{
			setStartAndEndDate(source.getStartDate(), source.getEndDate(), target);
			if (null != source.getExternalDealUrl())
			{
				target.setDealUrl(source.getExternalDealUrl().getUrl());
				target.setDealUrlType(DealDataType.EXTERNAL_DEAL_URL.name());
			}

		}
	}

	/**
	 * This method populate deal url on basic of available promotion is product level of retailer specific.
	 *
	 * @param source
	 * @param target
	 * @param promotionList
	 */
	private void setUrlForDealWithPromotion(final DealModel source, final DealData target,
			final List<PromotionSourceRuleModel> promotionList)
	{
		for (final PromotionSourceRuleModel promotionSourceRuleModel : promotionList)
		{
			setStartAndEndDate(promotionSourceRuleModel.getStartDate(), promotionSourceRuleModel.getEndDate(), target);
			if (promotionSourceRuleModel.getStore() != null)
			{
				target.setDealUrl(getShopContentPageUrl(promotionSourceRuleModel.getStore()));
				target.setDealUrlType(DealDataType.RETAILER_PROMOTION_DEAL_URL.name());
			}
			else if (CollectionUtils.isNotEmpty(source.getContentPage()))
			{
				for (final ContentPageModel contentPage : source.getContentPage())
				{
					if (ContentPageType.LISTINGPAGE.equals(contentPage.getPageType()))
					{
						target.setDealUrl(contentPage.getLabel());
						target.setDealUrlType(DealDataType.PRODUCT_PROMOTION_DEAL_URL.name());
						break;
					}
				}
			}
		}
	}



	/**
	 * This method calculate shop content page url for shopModel.
	 *
	 * @param shop
	 * @return
	 */
	private String getShopContentPageUrl(final ShopModel shop)
	{
		String contentPageLabel = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(shop.getContentPage()))
		{
			for (final ContentPageModel contentModel : shop.getContentPage())
			{
				if (ContentPageType.LISTINGPAGE.equals(contentModel.getPageType()))
				{
					contentPageLabel = contentModel.getLabel();
				}
			}
		}
		return contentPageLabel;
	}

	/**
	 * This method set startdate and enddate to DealData
	 *
	 * @param startDate
	 * @param endDate
	 * @param target
	 */
	private void setStartAndEndDate(final Date startDate, final Date endDate, final DealData target)
	{
		target.setStartDate(startDate);
		target.setEndDate(endDate);
	}

	private void setValidity(final DealData target)
	{
		StringBuilder validity = new StringBuilder();
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM", Locale.US);
		final Date startDate = target.getStartDate();
		final Date endDate = target.getEndDate();
		if (null != startDate && null != endDate)
		{
			final Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			final String startDateMonth = simpleDateFormat.format(startDate);
			final int startDateDay = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTime(endDate);
			final String endDateMonth = simpleDateFormat.format(endDate);
			final int endDateDay = cal.get(Calendar.DAY_OF_MONTH);
			if (startDateMonth.equalsIgnoreCase(endDateMonth))
			{
				validity = validity.append(startDateMonth).append(". ").append(startDateDay).append("-").append(endDateDay);
			}
			else
			{
				validity = validity.append(startDateMonth).append(". ").append(startDateDay).append(" - ").append(endDateMonth)
						.append(". ").append(endDateDay);
			}

			target.setValidity(validity.toString());
		}
	}

	private void setLogo(final DealModel source, final DealData target)
	{
		if (null != source.getShop() && null != source.getShop().getLogo())
		{
			target.setImageURL(source.getShop().getLogo().getURL());
			target.setAltText(source.getShop().getLogo().getAltText());
		}

	}
}
