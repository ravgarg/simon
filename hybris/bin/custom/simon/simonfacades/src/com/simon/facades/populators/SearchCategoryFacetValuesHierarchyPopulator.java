package com.simon.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.converters.Populator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.google.common.base.Splitter;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;



/**
 * This class populates the Facet CategoryPath into hierarchial structure
 */
public class SearchCategoryFacetValuesHierarchyPopulator<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY>
		implements
		Populator<ProductCategorySearchPageData<QUERY, RESULT, SCAT>, ProductCategorySearchPageData<STATE, ITEM, CATEGORY>>
{

	@Value("${navigation.search.root.category.code}")
	protected String rootCategoryCode;

	@Value("${navigation.search.root.category.name}")
	protected String rootCategoryName;

	/**
	 * This method populates facet values from categoryPath Facet. It generates map from facet value and call recursive
	 * method. A parent Facet Value Node is also created.
	 */
	@Override
	public void populate(final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source,
			final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target)
	{
		if (null != target.getFacets())
		{
		final Optional<FacetData<STATE>> categoryFacet = target.getFacets().stream()
				.filter(facet -> facet.getCode().equalsIgnoreCase(IndexedKeys.CATEGORY_PATH)).findFirst();
		if (categoryFacet.isPresent())
		{
			final FacetValueData<STATE> rootNode = initRootFacetValueNode();
			for (final FacetValueData<STATE> facetValue : categoryFacet.get().getValues())
			{
				final String facetCode = facetValue.getCode();
				final Map<String, String> facetValueMap = new LinkedHashMap<>(
						Splitter.on("/").omitEmptyStrings().withKeyValueSeparator(SimonCoreConstants.PIPE).split(facetCode));
				fillFacetValueInHierarchy(facetValueMap, rootNode, facetValue);
			}
			categoryFacet.get().setValues(rootNode.getChildNode().get(0).getChildNode()); // Setting L1 categories facet values to category facet
		}
		}
	}

	/**
	 * Initialize root facet node
	 *
	 * @return
	 */
	private FacetValueData<STATE> initRootFacetValueNode()
	{
		final FacetValueData<STATE> hierarchialFacetValueData = new FacetValueData<>();
		hierarchialFacetValueData.setCode(rootCategoryCode);
		hierarchialFacetValueData.setName(rootCategoryName);
		return hierarchialFacetValueData;
	}

	/**
	 * Recursive method. Method checks if facetValue Map is not empty.
	 *
	 * It checks whether current Facet Node matches with facet entry in map.
	 *
	 * Flow 1: If it matches, facet entry is removed from Map and Map is checked again, if Map still contains any more
	 * facet entries.
	 *
	 * If Map contain more facet entries, further scenarios are handled in checkIfFacetCategoryNodeExistElseCreateNewNode
	 * method. If Map doesn't contain any more entries, necessary data is filled into current Facet Node.
	 *
	 * Flow 2: If current Facet Node does not matches with facet entry in map. Code and Name value in set in current
	 * Facet Node. Category Entry is removed from Map. Map is checked again, if Map still contains any more facet
	 * entries.
	 *
	 * If Map contain more facet entries, a new child Facet Node is created and added to current Facet Node. Recursive
	 * call is made. If Map doesn't contain any more entries, necessary data is filled into current Facet Node.
	 *
	 * @param facetValueMap
	 * @param hierarchialFacetValueData
	 * @param categoryPathFacetValue
	 */
	private void fillFacetValueInHierarchy(final Map<String, String> facetValueMap,
			final FacetValueData<STATE> hierarchialFacetValueData, final FacetValueData<STATE> categoryPathFacetValue)
	{
		if (MapUtils.isNotEmpty(facetValueMap))
		{
			final Map.Entry<String, String> facetData = facetValueMap.entrySet().iterator().next();
			if (StringUtils.equalsIgnoreCase(hierarchialFacetValueData.getCode(), facetData.getKey()))
			{
				facetValueMap.remove(facetData.getKey()); //Node already exist for current map entry. Removing from facetValueMap
				if (MapUtils.isNotEmpty(facetValueMap)) // check if map contains more values
				{
					checkIfFacetCategoryNodeExistElseCreateNewNode(facetValueMap, hierarchialFacetValueData, categoryPathFacetValue);
				}
				else
				{ // No more entries present in map. Update necessary values in leaf node
					setQueryUrlAndFacetCountInLeafFacet(hierarchialFacetValueData, categoryPathFacetValue);
				}
			}
			else
			{ //No Node exist for current category entry in Map
				hierarchialFacetValueData.setCode(facetData.getKey());
				hierarchialFacetValueData.setName(facetData.getValue());
				facetValueMap.remove(facetData.getKey());
				if (MapUtils.isNotEmpty(facetValueMap))
				{ // A new child node will always be created for recently created parent node, if facetValueMap still contain entries
					final FacetValueData<STATE> childFacetValueData = createChildNodeForCurrentFacet(hierarchialFacetValueData);
					fillFacetValueInHierarchy(facetValueMap, childFacetValueData, categoryPathFacetValue);
				}
				else
				{ // Leaf Node has been created. Update necessary values in leaf node
					setQueryUrlAndFacetCountInLeafFacet(hierarchialFacetValueData, categoryPathFacetValue);
				}
			}

		}
	}

	/**
	 * This method check is next item in original facetValueMap already has a node in current DTO.
	 *
	 * Flow 1 : If No child nodes exist for current DTO, a new chid node list and child node DTO is created.Recursive
	 * call is made to method.
	 *
	 * Flow 2: If Child node exist for current DTO, it is checked whether any child node in list, matches with current
	 * category code. If match is found, recursive method is called again on that child node. Else a new child node is
	 * created and added to existing childNodes list.
	 *
	 * @param facetValueMap
	 * @param currentNode
	 * @param categoryPathFacetValue
	 */
	private void checkIfFacetCategoryNodeExistElseCreateNewNode(final Map<String, String> facetValueMap,
			final FacetValueData<STATE> currentNode, final FacetValueData<STATE> categoryPathFacetValue)
	{
		final Map.Entry<String, String> facetData = facetValueMap.entrySet().iterator().next();
		final List<FacetValueData<STATE>> facetChildNodes = currentNode.getChildNode();
		if (CollectionUtils.isNotEmpty(facetChildNodes))
		{ // Child Node exists for current Node
			final FacetValueData<STATE> childFacetNode = checkIfFacetCodeExistInParentFacetChilds(facetData, facetChildNodes);
			if (null != childFacetNode)
			{ // Child Node matches with next item in iterator
				fillFacetValueInHierarchy(facetValueMap, childFacetNode, categoryPathFacetValue);
			}
			else
			{ // No child Node matches for next item in iterator. So we create a new Child Node and add to existing child Node liat
				final FacetValueData<STATE> childFacetValueData = new FacetValueData<>();
				facetChildNodes.add(childFacetValueData);
				fillFacetValueInHierarchy(facetValueMap, childFacetValueData, categoryPathFacetValue);
			}
		}
		else
		{ //No child Node exist for already existing current Node. So we create a new child node and list and assign to current Node
			final FacetValueData<STATE> childFacetValueData = createChildNodeForCurrentFacet(currentNode);
			fillFacetValueInHierarchy(facetValueMap, childFacetValueData, categoryPathFacetValue);
		}
	}

	/**
	 * This method check whether the next category entry in category Path, already exist in current hierarchy DTO
	 *
	 * @param facetData
	 * @param facetChildNodes
	 * @return
	 */
	private FacetValueData<STATE> checkIfFacetCodeExistInParentFacetChilds(final Map.Entry<String, String> facetData,
			final List<FacetValueData<STATE>> facetChildNodes)
	{
		final Optional<FacetValueData<STATE>> childNode = facetChildNodes.stream()
				.filter(facetChildNode -> StringUtils.equalsIgnoreCase(facetChildNode.getCode(), facetData.getKey())).findFirst();
		return childNode.isPresent() ? childNode.get() : null;

	}

	/**
	 * This method adds child node list and a new child node DTO to the existing DTO
	 *
	 * @param hierarchialFacetValueData
	 * @return
	 */
	private FacetValueData<STATE> createChildNodeForCurrentFacet(final FacetValueData<STATE> hierarchialFacetValueData)
	{
		final FacetValueData<STATE> childFacetValueData = new FacetValueData<>();
		final List<FacetValueData<STATE>> childFacetList = new ArrayList<>();
		childFacetList.add(childFacetValueData);
		hierarchialFacetValueData.setChildNode(childFacetList);
		return childFacetValueData;
	}

	/**
	 * This method sets query url, count snd selected attribute in leaf node
	 *
	 * @param hierarchialFacetValueData
	 * @param categoryPathFacetValue
	 */
	private void setQueryUrlAndFacetCountInLeafFacet(final FacetValueData<STATE> hierarchialFacetValueData,
			final FacetValueData<STATE> categoryPathFacetValue)
	{
		hierarchialFacetValueData.setCount(categoryPathFacetValue.getCount());
		hierarchialFacetValueData.setQuery(categoryPathFacetValue.getQuery());
		hierarchialFacetValueData.setSelected(categoryPathFacetValue.isSelected());
	}
}
