package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.util.localization.Localization;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.facades.cart.data.RetailerInfoData;


/**
 * This class will set retailer specific data in {@link RetailerInfoData}
 */
public class ExtRetailerPopulator implements Populator<ShopModel, RetailerInfoData>
{

	private static final String TEXT_VIEW_SHIPPING_RETURNS_PRIVACY_INFORMATION = "text.view.shipping.returns.privacy.information";

	@Resource
	private CartService cartService;

	/**
	 * This method will populate retailer specific data in {@link RetailerInfoData}
	 */
	@Override
	public void populate(final ShopModel source, final RetailerInfoData target)
	{
		target.setRetailerId(source.getId());
		target.setRetailerName(source.getName());
		target.setRetailerPolicyUrl("/contentPolicy/" + source.getId());
		target.setLabelTextViewShippingAndReturns(getViewShippingReturnsMessage());
		populateCustomerServiceLink(source, target);
	}

	/**
	 * Gets the localized ViewShippingReturns message.
	 *
	 * @return String value.
	 */
	protected String getViewShippingReturnsMessage()
	{
		return Localization.getLocalizedString(TEXT_VIEW_SHIPPING_RETURNS_PRIVACY_INFORMATION);
	}


	/**
	 * Gets the customer service link for the specific retailer.
	 *
	 * @param orderModel
	 * @param shopModel
	 */
	private void populateCustomerServiceLink(final ShopModel shopModel, final RetailerInfoData retailerInfoData)
	{
		final AbstractOrderModel abstractOrderModel = cartService.getSessionCart();
		String trackingLink = shopModel.getCustomerServiceLabel();

		final Optional<ConsignmentModel> consignment = abstractOrderModel.getConsignments().stream()
				.filter(consignmentModel -> consignmentModel.getShopDisplayName().equalsIgnoreCase(shopModel.getName())).findFirst();

		if (consignment.isPresent() && StringUtils.isNotEmpty(consignment.get().getTrackingID()))
		{
			trackingLink = consignment.get().getTrackingID();
		}

		retailerInfoData.setCustomerServiceLink(trackingLink);
	}

}
