/*
 *
 */
package com.simon.facades.cms;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmsfacades.pages.PageFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;


/**
 * The Interface ExtDefaultPageFacade.
 */
public interface ExtPageFacade extends PageFacade
{

	/**
	 * Populate url for left nav.
	 *
	 * @param facetData
	 *           the facet data
	 * @return the list
	 */
	List<FacetData<SearchStateData>> populateUrlForLeftNav(List<FacetData<SearchStateData>> facetData, String pageLabel);

	/**
	 * Search listing page data.
	 *
	 * @param model
	 *           the model
	 * @param pageForRequest
	 *           the page for request
	 * @param shopModel
	 *           the shop model
	 * @param pageId
	 *           the page id
	 * @return the product search page data
	 */
	ProductSearchPageData<SearchStateData, ProductData> searchListingPageData(final String searchQuery, final String sortCode,
			final Model model, final ContentPageModel pageForRequest, String pageLabel);

	/**
	 * This method basically find the valid user from betaUser table and after successful login it will create a cookie
	 * with yes value for valid session.
	 *
	 * @param username
	 * @param password
	 * @param request
	 * @param response
	 * @return true/false
	 */
	boolean betaLoginProcess(String username, String password, HttpServletRequest request, HttpServletResponse response);
}
