package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import com.simon.core.model.BackgroundImageItemModel;
import com.simon.core.model.PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel;
import com.simon.facades.cms.BackgroundImageData;
import com.simon.facades.cms.CMSLinkComponentData;
import com.simon.facades.cms.PromoBanner2ClickableImageWithTextAndSingleCTAComponentData;


/**
 * Populates {@link PromoBanner2ClickableImageWithTextAndSingleCTAComponentData} with CMSLinkComponentData and
 * BackgroundImageData
 */
public class SimonPromoBanner2ClickableImageWithTextAndSingleCTAComponentPopulator implements
		Populator<PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel, PromoBanner2ClickableImageWithTextAndSingleCTAComponentData>
{




	private Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter;




	@Override
	public void populate(final PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel source,
			final PromoBanner2ClickableImageWithTextAndSingleCTAComponentData target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");
		if (source.getBackgroundImage() != null)
		{
			target.setBackgroundImage(getSimonBackgroundImageDataConverter().convert(source.getBackgroundImage()));
		}
		target.setFontColor(source.getFontColor());

		final CMSLinkComponentData cmsLink = new CMSLinkComponentData();
		if (source.getLink() != null)
		{
			cmsLink.setLinkName(source.getLink().getLinkName());
			target.setLink(cmsLink);
		}
		target.setSubheadingText(source.getSubheadingText());
		target.setTitleText(source.getTitleText());

	}


	/**
	 * @return
	 */
	public Converter<BackgroundImageItemModel, BackgroundImageData> getSimonBackgroundImageDataConverter()
	{
		return simonBackgroundImageDataConverter;
	}

	/**
	 * @param simonBackgroundImageDataConverter
	 */
	public void setSimonBackgroundImageDataConverter(
			final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter)
	{
		this.simonBackgroundImageDataConverter = simonBackgroundImageDataConverter;
	}

}
