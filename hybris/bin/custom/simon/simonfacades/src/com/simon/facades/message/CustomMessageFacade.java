package com.simon.facades.message;

import com.simon.core.exceptions.SystemException;


/**
 * Facade Interface to fetch messageText with messageCode as input parameter.
 */
public interface CustomMessageFacade
{
	/**
	 * This method get the messageText for code. If code/messageText is not available, empty String is returned from
	 * service.
	 *
	 */
	String getMessageTextForCode(String code) throws SystemException;

}
