package com.simon.facades.share.email.impl;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;
import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.product.EmailProductData;
import com.simon.facades.share.email.ShareEmailFacade;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.integration.share.email.services.impl.DefaultShareEmailIntegrationService;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


/**
 * class ShareEmailFacadeImpl use to maintains the product model related methods at facade layer
 */
public class ShareEmailFacadeImpl implements ShareEmailFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(ShareEmailFacadeImpl.class);

	@Resource(name = "shareEmailIntegrationService")
	private DefaultShareEmailIntegrationService shareEmailIntegrationService;
	@Resource(name = "productService")
	private ProductService productService;
	@Resource
	private SessionService sessionService;
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Resource(name = "emailProductDataConverter")
	private Converter<ProductData, EmailProductData> emailProductDataConverter;

	@Resource(name = "shareEmailDealDataConverter")
	private Converter<DealModel, ShareEmailData> shareEmailDealDataConverter;

	@Resource
	private ExtDealService extDealService;

	@Resource(name = "orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;
	@Resource(name = "emailOrderConfirmationDataConverter")
	private Converter<OrderData, EmailOrderData> emailOrderConfirmationDataConverter;
	@Resource
	private ModelService modelService;

	/**
	 * @description Method use to get Product details and use those details to share product details in email
	 * @method getProductDetailsToSharedProductEmail
	 * @param mailCommonData
	 * @throws SharedEmailIntegrationException
	 */
	@Override
	public void getProductDetailsToSharedProductEmail(final ShareEmailData mailCommonData) throws SharedEmailIntegrationException
	{
		ShareEmailData sharedEmailCommonData = null;
		final ProductSearchPageData<SearchStateData, ProductData> productSearchPageData = getProductData(
				mailCommonData.getProductCode());
		if (null != productSearchPageData && CollectionUtils.isNotEmpty(productSearchPageData.getResults()))
		{
			final ProductData productData = productSearchPageData.getResults().get(0);
			sharedEmailCommonData = populateSharedProductEmailData(mailCommonData, productData);
			shareEmailIntegrationService.sharedProductEmailData(sharedEmailCommonData);
		}

	}

	/**
	 * @description Method use to get the product data from the product model
	 * @method getProductReferencesData
	 * @param productModel
	 * @return ProductSearchPageData
	 */
	private ProductSearchPageData<SearchStateData, ProductData> getProductData(final String productCode)
	{
		final List<String> productCodes = new ArrayList<>();
		productCodes.add(productCode);
		sessionService.setAttribute(SimonCoreConstants.PRODUCT_CODE_LIST, productCodes);
		final SearchStateData searchState = prepareSearchQueryData();
		final PageableData pageableData = preparePageableData();
		return searchProjectData(searchState, pageableData);
	}

	protected ProductSearchPageData<SearchStateData, ProductData> searchProjectData(final SearchStateData searchState,
			final PageableData pageableData)
	{
		return productSearchFacade.textSearch(searchState, pageableData);
	}

	/**
	 * This method prepares the PageableData object.
	 *
	 * @return PageableData
	 */
	protected PageableData preparePageableData()
	{
		final PageableData pageableData = new PageableData();
		pageableData.setPageSize(1);
		return pageableData;
	}

	/**
	 * This method prepares the SearchStateData object.
	 *
	 * @return SearchStateData
	 */
	protected SearchStateData prepareSearchQueryData()
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(StringUtils.EMPTY);
		final SearchStateData searchState = new SearchStateData();
		searchState.setQuery(searchQueryData);
		return searchState;
	}

	/**
	 * @method populateSharedProductEmailData
	 * @param sharedEmailCommonData
	 * @param variantOptionData
	 * @return
	 */
	private ShareEmailData populateSharedProductEmailData(final ShareEmailData sharedEmailCommonData,
			final ProductData productData)
	{
		sharedEmailCommonData.setEmailProductData(emailProductDataConverter.convert(productData));
		sharedEmailCommonData.setSubscriptionDate(shareEmailIntegrationUtils.convertCurrentDateTimeInString(new Date()));
		return sharedEmailCommonData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shareDealEmail(final String dealId, final ShareEmailData shareDealEmailData) throws SharedEmailIntegrationException
	{
		final DealModel dealModel = extDealService.getDealForCode(dealId);
		if (null != dealModel)
		{
			final ShareEmailData emailData = shareEmailDealDataConverter.convert(dealModel, shareDealEmailData);
			shareEmailIntegrationService.shareDealEmail(emailData);

		}
	}

	/**
	 * @description Method use to get Order details and use those details to share order confirmation details in email
	 * @method getOrderDetailsToSharedOrderConfirmationOrRejectionEmail
	 * @param orderModel
	 * @param status
	 */
	@Override
	public boolean getOrderDetailsToSharedOrderConfirmationOrRejectionEmail(final OrderModel orderModel, final String status)
	{
		ShareEmailData shareEmailData = new ShareEmailData();
		try
		{
			final OrderData orderData = orderConverter.convert(orderModel);
			setUpdateRetailerInfoData(orderData.getRetailerInfoData(), orderModel);
			shareEmailData = populateSharedOrderEmailData(shareEmailData, orderData);
			shareEmailIntegrationService.sharedOrderConfirmationEmailData(shareEmailData);
			if (StringUtils.isNotEmpty(status) && ("CONFIRM").equalsIgnoreCase(status))
			{
				orderModel.setStatus(OrderStatus.COMPLETED);
				modelService.save(orderModel);
			}
			return true;
		}
		catch (final SharedEmailIntegrationException ex)
		{
			LOG.error("Exception occured : getting error code {} and error message {}  for order code {} and exception is {} ",
					ex.getCode(), ex.getMessage(), orderModel.getCode(), ex);
			return false;
		}
		catch (final Exception ex)
		{
			LOG.error("Exception occured : when share the order confirmation details in email for order code {}, {}",
					orderModel.getCode(), ex);
			return false;
		}

	}


	/**
	 * @method populateSharedProductEmailData
	 * @param shareEmailData
	 * @param variantOptionData
	 * @return
	 */
	private ShareEmailData populateSharedOrderEmailData(final ShareEmailData shareEmailData, final OrderData orderData)
	{
		shareEmailData.setEmailOrderData(emailOrderConfirmationDataConverter.convert(orderData));
		shareEmailData.setSubscriptionDate(shareEmailIntegrationUtils.convertCurrentDateTimeInString(new Date()));
		return shareEmailData;
	}

	/**
	 * In this utility method setting Retailer order Number
	 *
	 * @param retailerInfoDataList
	 * @param orderModel
	 */
	protected void setUpdateRetailerInfoData(final List<RetailerInfoData> retailerInfoDataList, final OrderModel orderModel)
	{
		for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries())
		{
			final String shopId = orderEntry.getShopId();
			for (final RetailerInfoData retailerInfoData : retailerInfoDataList)
			{
				if (retailerInfoData.getRetailerId().equalsIgnoreCase(shopId))
				{
					retailerInfoData
							.setRetailerOrderNumber(orderModel.getRetailersOrderNumbers().get(retailerInfoData.getRetailerId()));
				}
			}
		}
	}

}
