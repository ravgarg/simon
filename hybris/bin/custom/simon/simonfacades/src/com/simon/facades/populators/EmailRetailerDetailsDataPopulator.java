package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.order.RetailerDetailsData;


/**
 * Converts RetailerInfoData to RetailerDetailsData
 */
public class EmailRetailerDetailsDataPopulator implements Populator<RetailerInfoData, RetailerDetailsData>
{

	/**
	 * populates the RetailerInfoData from the contents in RetailerDetailsData
	 */
	@Override
	public void populate(final RetailerInfoData source, final RetailerDetailsData target)
	{
		target.setRetailerId(StringUtils.isNotEmpty(source.getRetailerId()) ? source.getRetailerId() : StringUtils.EMPTY);

		target.setName(StringUtils.isNotEmpty(source.getRetailerName()) ? source.getRetailerName() : StringUtils.EMPTY);

		target.setOrderNumber(
				StringUtils.isNotEmpty(source.getRetailerOrderNumber()) ? source.getRetailerOrderNumber() : StringUtils.EMPTY);

		target.setShippingDesc(
				StringUtils.isNotEmpty(source.getSelectedShippingMethod()) ? source.getSelectedShippingMethod() : StringUtils.EMPTY);


	}

}
