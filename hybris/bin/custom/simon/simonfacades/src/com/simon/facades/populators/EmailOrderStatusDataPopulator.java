package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.converters.Populator;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;


/**
 * Converts OrderData to EmailOrderData
 */
public class EmailOrderStatusDataPopulator implements Populator<OrderData, EmailOrderData>
{

	private static final String ORDER_REJECTED = "Order Rejected";
	private static final String ITEM_REJECTED = "Item Rejected";
	private static final String STORE_REJECTED = "Store Rejected";

	private static final String RETAILER_CENCELLED = "Cancelled";
	private static final String PRODUCT_CENCELLED = "Cancelled";

	/**
	 * populates the OrderData from the contents in EmailOrderData
	 */
	@Override
	public void populate(final OrderData source, final EmailOrderData target)
	{
		final EmailOrderDetailsData orderDetailsData = target.getOrderDetail();

		final List<RetailerDetailsData> retailerDetailsDataList = target.getRetailersDetail();

		orderDetailsData.setOrderStatus(populateOrderLevelStatusAttributeValue(retailerDetailsDataList));

	}

	/**
	 * @method populateOrderLevelStatusAttributeValue
	 * @param retailerDetailsDataList
	 * @return String
	 */
	private String populateOrderLevelStatusAttributeValue(final List<RetailerDetailsData> retailerDetailsDataList)
	{
		String orderLevelStatus = StringUtils.EMPTY;
		boolean isAllRetailerRejected = false;
		boolean isSingleStoreRejected = false;

		for (final RetailerDetailsData retailerDetailsData : retailerDetailsDataList)
		{
			if (StringUtils.isNotEmpty(retailerDetailsData.getStatus()))
			{
				if (retailerDetailsData.getStatus().equalsIgnoreCase(RETAILER_CENCELLED))
				{
					isAllRetailerRejected = true;
				}
				else
				{
					isAllRetailerRejected = false;
					break;
				}
			}
		}

		if (isAllRetailerRejected)
		{
			orderLevelStatus = ORDER_REJECTED;
		}
		else
		{
			for (final RetailerDetailsData retailerDetailsData : retailerDetailsDataList)
			{
				if (StringUtils.isNotEmpty(retailerDetailsData.getStatus()))
				{
					if (retailerDetailsData.getStatus().equalsIgnoreCase(RETAILER_CENCELLED))
					{
						isSingleStoreRejected = true;
						break;
					}
					else
					{
						isSingleStoreRejected = false;
					}
				}
			}

			if (isSingleStoreRejected)
			{
				orderLevelStatus = STORE_REJECTED;
			}
			else
			{
				if (populateItemRejectedStatus(retailerDetailsDataList))
				{
					orderLevelStatus = ITEM_REJECTED;
				}
			}
		}

		return orderLevelStatus;
	}

	/**
	 * @method populateItemRejectedStatus
	 * @param retailerDetailsDataList
	 * @return boolean
	 */
	private boolean populateItemRejectedStatus(final List<RetailerDetailsData> retailerDetailsDataList)
	{
		boolean isItemRejected = false;
		for (final RetailerDetailsData retailerDetailsData : retailerDetailsDataList)
		{
			final List<ProductDetailsData> productDetailsDataList = retailerDetailsData.getProductsDetail();

			for (final ProductDetailsData productDetailsData : productDetailsDataList)
			{
				if (StringUtils.isNotEmpty(productDetailsData.getStatus())
						&& (productDetailsData.getStatus().equalsIgnoreCase(PRODUCT_CENCELLED)))
				{
					isItemRejected = true;
					break;
				}
			}

		}

		return isItemRejected;
	}

}
