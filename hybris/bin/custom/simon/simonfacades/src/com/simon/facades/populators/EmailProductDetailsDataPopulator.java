package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.email.order.ProductDetailsData;


/**
 * Converts RetailerInfoData to RetailerDetailsData
 */
public class EmailProductDetailsDataPopulator implements Populator<OrderEntryData, ProductDetailsData>
{

	private static final String DEFUALT_IMAGE_KEY = "thumbnail";
	private static final String PRODUCT_COLOR_KEY = "COLOR";
	private static final String PRODUCT_SIZE_KEY = "SIZE";

	/**
	 * populates the OrderEntryData from the contents in ProductDetailsData
	 */
	@Override
	public void populate(final OrderEntryData source, final ProductDetailsData target)
	{
		final ProductData productData = source.getProduct();

		if (null != productData)
		{
			final Map<String, String> productColorSizeMap = populateProductVariantValueMap(productData);

			target.setColor(populateColorSizeOfProduct(productColorSizeMap, PRODUCT_COLOR_KEY));
			target.setDescription(StringUtils.isNotEmpty(productData.getName()) ? productData.getName() : StringUtils.EMPTY);
			target.setPromoMsg(getProductPromoMsg(source.getPromotionMessage()));
			target.setQuantity(null != source.getQuantity() ? source.getQuantity().toString() : StringUtils.EMPTY);

			target.setPrice(populateProductPrice(source));
			target.setSize(populateColorSizeOfProduct(productColorSizeMap, PRODUCT_SIZE_KEY));

			target.setImageUrl(populateProductImageUrl(productData));
			target.setSubtotal(populateProductSalePrice(source));

		}

	}

	private String getProductPromoMsg(final List<String> promList)
	{
		String proMsg = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(promList))
		{
			proMsg = promList.get(SimonCoreConstants.ZERO_INT);
		}
		return proMsg;
	}

	/**
	 * @description Method use to get the image of the product
	 * @method populateProductImageUrl
	 * @param variantOptionData
	 * @return string
	 */
	private String populateProductImageUrl(final ProductData productData)
	{
		String imageUrl = StringUtils.EMPTY;
		final Collection<ImageData> imageDatas = productData.getImages();
		if (CollectionUtils.isNotEmpty(imageDatas))
		{
			for (final ImageData imageData : imageDatas)
			{
				if (StringUtils.isNotEmpty(imageData.getFormat()) && imageData.getFormat().equalsIgnoreCase(DEFUALT_IMAGE_KEY))
				{
					imageUrl = imageData.getUrl();
				}
			}
		}
		return imageUrl;
	}

	/**
	 * @description Method use to populate the product price
	 * @method populateProductPrice
	 * @param orderEntryData
	 * @return String
	 */
	private String populateProductPrice(final OrderEntryData orderEntryData)
	{
		String productPrice = StringUtils.EMPTY;
		final PriceData priceData = orderEntryData.getBasePrice();
		if (null != priceData && StringUtils.isNotEmpty(priceData.getFormattedValue()))
		{
			productPrice = priceData.getFormattedValue();
		}
		return productPrice;
	}

	/**
	 * @description Method use to populate the product sale price
	 * @method populateProductSalePrice
	 * @param orderEntryData
	 * @return String
	 */
	private String populateProductSalePrice(final OrderEntryData orderEntryData)
	{
		String productSalePrice = StringUtils.EMPTY;
		final PriceData priceData = orderEntryData.getSaleValue();
		if (null != priceData && StringUtils.isNotEmpty(priceData.getFormattedValue()))
		{
			productSalePrice = priceData.getFormattedValue();
		}
		return productSalePrice;
	}

	/**
	 * @description Method use to populate the color and size
	 * @method populateColorSizeOfProduct
	 * @param productData
	 * @param productOrSizekey
	 * @return String
	 */
	private String populateColorSizeOfProduct(final Map<String, String> productColorSizeMap, final String productOrSizekey)
	{
		String productColorOrSize = StringUtils.EMPTY;
		if (null != productColorSizeMap && productColorSizeMap.size() > 0)
		{
			productColorOrSize = productColorSizeMap.get(productOrSizekey.toLowerCase(Locale.US));
		}
		return productColorOrSize;
	}

	/**
	 * @description Method use to populate the color and size
	 * @method populateColorSizeOfProduct
	 * @param productData
	 * @param productOrSizekey
	 * @return String
	 */
	private Map<String, String> populateProductVariantValueMap(final ProductData productData)
	{
		Map<String, String> productColorSizeMap = null;
		final Map<String, String> productVariantValueMap = productData.getVariantValues();
		if (null != productVariantValueMap && productVariantValueMap.size() > 0)
		{
			productColorSizeMap = new HashMap<>();
			for (final Map.Entry<String, String> entry : productVariantValueMap.entrySet())
			{
				productColorSizeMap.put(entry.getKey().toLowerCase(), entry.getValue());
			}
		}
		return productColorSizeMap;
	}

}
