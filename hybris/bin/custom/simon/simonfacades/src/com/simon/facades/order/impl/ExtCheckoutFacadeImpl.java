package com.simon.facades.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.orderhistory.OrderHistoryService;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.StockService;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.core.services.ExtCartService;
import com.simon.core.services.ExtOrderService;
import com.simon.core.services.ShopGatewayService;
import com.simon.core.services.VariantAttributeMappingService;
import com.simon.core.services.impl.ExtCalculationService;
import com.simon.core.services.order.ExtCommerceCheckoutService;
import com.simon.core.util.CommonUtils;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.checkout.data.ExtCheckoutData;
import com.simon.facades.checkout.data.ShippingPriceCall;
import com.simon.facades.checkout.data.StateData;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.populators.OrderCallbackReversePopulator;
import com.simon.facades.user.ExtUserFacade;
import com.simon.generated.model.ErrorLogModel;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationService;
import com.simon.integration.purchase.services.OrderPurchaseIntegrationService;
import com.simon.integration.services.CartCheckOutIntegrationService;
import com.simon.integration.users.dto.GetAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;
import com.simon.integration.users.services.UserAddressIntegrationService;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * Simon Checkout facade class implements {@link ExtCheckoutFacade}. Service is responsible for getting information for
 * checkout.
 */
public class ExtCheckoutFacadeImpl extends DefaultAcceleratorCheckoutFacade implements ExtCheckoutFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtCheckoutFacadeImpl.class);

	@Resource
	private ExtCalculationService calculationService;
	@Resource
	private UserAddressIntegrationService userAddressIntegrationService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private ExtCustomerAccountService customerAccountService;
	@Resource
	private UserService userService;
	@Resource
	private CardPaymentIntegrationService cardPaymentIntegrationService;
	@Resource
	private ExtOrderService orderService;
	@Resource
	private BusinessProcessService businessProcessService;
	@Resource
	private ModelService modelService;
	@Resource
	private OrderPurchaseIntegrationService orderPurchaseIntegrationService;
	@Resource
	private ExtCommerceCheckoutService commerceCheckoutService;
	@Resource
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;
	@Resource
	private OrderHistoryService orderHistoryService;
	@Resource
	private VariantAttributeMappingService variantAttributeMappingService;
	@Resource
	private ShopGatewayService shopGatewayService;
	@Resource
	private CategoryService categoryService;
	@Resource
	private ExtCartService cartService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private StockService stockService;

	@Resource
	private I18NFacade i18NFacade;
	@Resource
	private ExtUserFacade userFacade;

	@Resource
	private ExtCustomMessageUtils extCustomMessageUtils;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	@Resource
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	@Resource
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Resource
	private Converter<OrderConfirmCallBackDTO, AdditionalCartInfoModel> confirmUpdateReverseConverter;
	@Resource
	private Converter<ProductModel, ProductData> productConverter;
	@Resource
	private OrderCallbackReversePopulator orderCallbackReversePopulator;
	@Resource
	private Converter<OrderModel, OrderHistoryData> orderHistoryConverter;
	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;

	@Value("${targetsystem.shopgateway.code}")
	protected String shopGatewayCode;

	@Value("${twotap.checkout}")
	private boolean twotapCheckout;

	@Resource
	private SessionService sessionService;

	private static final String FIRST_SHIPPING_METHOD = "simon.bot.first.shipping.price.call";
	private static final String STUB_REQUIRED_FIELD_VALIDATION = "simon.twotap.validate.required.field.call.enable.stub.flag";


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPaymentAddress(final AddressData billingAddressData)
	{
		final CartModel cart = getCartService().getSessionCart();
		final AddressModel billingAddress = createBillingAddress(billingAddressData);
		cart.setPaymentAddress(billingAddress);
		getModelService().saveAll(cart, billingAddress);
		getModelService().refresh(cart);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasPaymentInfo()
	{
		boolean result = false;
		final CartData cartData = getCheckoutCart();
		if (cartData != null)
		{
			result = cartData.getPaymentInfo() != null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartData getCheckoutCart()
	{
		// set up parent checkout cart
		final CartData cartData = super.getCheckoutCart();
		cartData.setPaymentInfo(getExtPaymentDetails());
		cartData.setShippingPriceCall(ShippingPriceCall.CHEAPEST.toString());
		return cartData;
	}


	/**
	 * @param cart
	 */
	public void reCalculateCart(final CartModel cart)
	{
		final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cart);
		getCommerceCheckoutService().calculateCart(parameter);
	}



	@Override
	public Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getCreditCardPaymentInfoConverter()
	{
		return creditCardPaymentInfoConverter;
	}

	@Override
	public void setCreditCardPaymentInfoConverter(
			final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter)
	{
		this.creditCardPaymentInfoConverter = creditCardPaymentInfoConverter;
	}

	/**
	 * This method is used to get the Spreedly payment details and populates the payment info data.
	 *
	 * @return
	 */
	protected CCPaymentInfoData getExtPaymentDetails()
	{
		CCPaymentInfoData paymentInfoData = null;
		final CartModel cart = getCartModel();
		if (cart != null)
		{
			final PaymentInfoModel paymentInfo = cart.getPaymentInfo();
			if (paymentInfo instanceof CreditCardPaymentInfoModel)
			{
				paymentInfoData = getCreditCardPaymentInfoConverter().convert((CreditCardPaymentInfoModel) paymentInfo);
			}
		}
		return paymentInfoData;
	}


	/**
	 * Set billing address into the cart and populate addressModel with addressData
	 */
	private AddressModel createBillingAddress(final AddressData addressData)
	{
		final AddressModel addressModel = getModelService().create(AddressModel.class);
		final CartModel cartModel = getCartModel();
		if (cartModel != null && addressData != null)
		{

			// Populate addressModel with addressData
			getAddressReversePopulator().populate(addressData, addressModel);
			addressModel.setOwner(cartModel);
			addressModel.setBillingAddress(Boolean.TRUE);
			addressModel.setVisibleInAddressBook(Boolean.FALSE);// because it's a billing address, therefore it doesn't need to show up in the address book

			// set email to the billing address always
			final String email = addressModel.getEmail();
			if (email == null || email.isEmpty())
			{
				addressModel.setEmail(getCurrentUserForCheckout().getContactEmail());
				// avoid NPE issue during guest checkout
			}

		}

		return addressModel;
	}

	/**
	 * Gets the checkout data.
	 *
	 * @param model
	 *           the model
	 * @param cartData
	 *           the cart data
	 * @param addressData
	 *           the address data
	 * @param suggestedAddress
	 *           the suggested address
	 * @return the checkout data
	 */
	public ExtCheckoutData getCheckoutData(final Model model, final CartData cartData, final ExtAddressData addressData,
			final ExtAddressData suggestedAddress)
	{

		final ExtCheckoutData extCheckoutData = new ExtCheckoutData();
		Map<String, String> messageLabelMap;
		extCheckoutData.setPaymentcompleted(false);
		extCheckoutData.setCartData(cartData);
		extCheckoutData.setSuggestedAddress(suggestedAddress);
		extCheckoutData.setUserAddress(addressData);
		messageLabelMap = extCustomMessageUtils.setCheckoutShippingMessages();
		extCheckoutData.setStatesMap(getAllStates(model));
		extCheckoutData.setExculdeStateList(getExcludedStateList());
		final CustomerType customerType = checkoutCustomerStrategy.getCurrentUserForCheckout().getType();
		if (!checkoutCustomerStrategy.isAnonymousCheckout() && !customerType.equals(CustomerType.GUEST))
		{
			extCheckoutData.setUserloggedin(Boolean.TRUE);
			messageLabelMap.put(SimonFacadesConstants.NAME, userService.getCurrentUser().getName());
		}

		extCheckoutData.setMessageLabels(messageLabelMap);

		return extCheckoutData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExtCheckoutData getCheckoutData(final Model model, final CartData cartData)
	{
		final ExtCheckoutData extCheckoutData = new ExtCheckoutData();
		final Map<String, String> messageLabelMap = extCustomMessageUtils.setCheckoutShippingMessages();
		extCheckoutData.setPaymentcompleted(false);
		extCheckoutData.setCartData(cartData);
		extCheckoutData.setStatesMap(getAllStates(model));
		extCheckoutData.setExculdeStateList(getExcludedStateList());
		final CustomerType customerType = checkoutCustomerStrategy.getCurrentUserForCheckout().getType();
		if (!checkoutCustomerStrategy.isAnonymousCheckout() && !customerType.equals(CustomerType.GUEST))
		{
			extCheckoutData.setUserloggedin(Boolean.TRUE);
			messageLabelMap.put(SimonFacadesConstants.NAME, userService.getCurrentUser().getName());
		}
		extCheckoutData.setMessageLabels(messageLabelMap);
		return extCheckoutData;
	}


	/**
	 * This method will populate excluded states in the shipping checkout data JSON.
	 */
	@Override
	public List<String> getExcludedStateList()
	{
		List<String> exculdeStateList = new ArrayList<>();
		final String states = configurationService.getConfiguration().getString("simon.excludeState.isocode.list");
		if (StringUtils.isNotEmpty(states))
		{
			if (states.contains(","))
			{
				final String[] stateList = states.split(",");
				exculdeStateList = Arrays.asList(stateList);
			}
			else
			{
				exculdeStateList.add(states);
			}
		}
		return exculdeStateList;
	}

	/**
	 * This method will populate states in the checkout data JSON.
	 *
	 * @param model
	 *
	 * @return -this will return the collection of states in the map.Map will contain name and region data iso codes.
	 */
	@Override
	public List<StateData> getAllStates(final Model model)
	{
		final List<StateData> stateList = new ArrayList<>();
		final String isoCode = configurationService.getConfiguration().getString(SimonFacadesConstants.SIMON_COUNTRY_ISOCODE);
		model.addAttribute("countryIsoCode", isoCode);
		final List<RegionData> statesList = i18NFacade.getRegionsForCountryIso(isoCode);
		if (CollectionUtils.isNotEmpty(statesList))
		{
			for (final RegionData regionData : statesList)
			{
				final StateData stateData = new StateData();
				stateData.setStateCode(regionData.getIsocode());
				stateData.setStateName(regionData.getName());
				stateData.setIsocodeShort(regionData.getIsocodeShort());
				stateList.add(stateData);
			}
		}

		return stateList;
	}

	/**
	 * This method creating payment info subscription
	 *
	 * @param extPaymentInfoData
	 * @return {@link CCPaymentInfoData}
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	@Override
	public CCPaymentInfoData createPaymentInfoSubscription(final CCPaymentInfoData extPaymentInfoData)
			throws UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		validateParameterNotNullStandardMessage("extPaymentInfoData", extPaymentInfoData);
		final AddressData billingAddressData = extPaymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddress", billingAddressData);

		if (checkIfCurrentUserIsTheCartUser())
		{
			final CreditCardPaymentInfoModel ccPaymentInfoModel = customerAccountService
					.createPaymentInfoSubscription(extPaymentInfoData);
			//Here checking only for register user and calling to synced with PO.com for new Payment method.
			if (!checkoutCustomerStrategy.isAnonymousCheckout() && !addPaymentInfo(extPaymentInfoData, ccPaymentInfoModel))
			{
				return null;
			}
			return ccPaymentInfoModel == null ? null : getCreditCardPaymentInfoConverter().convert(ccPaymentInfoModel);
		}
		return null;
	}

	/**
	 * This private method is used to call integration service. This method is used to add the user payment address with
	 * PO.com
	 *
	 * @param ccPaymentInfoData
	 * @param ccPaymentInfoModel
	 * @return boolean
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	private boolean addPaymentInfo(final CCPaymentInfoData ccPaymentInfoData, final CreditCardPaymentInfoModel ccPaymentInfoModel)
			throws UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		boolean success = false;
		if (null != ccPaymentInfoData && null != ccPaymentInfoModel)
		{
			success = userFacade.addUserPaymentAddress(ccPaymentInfoData, ccPaymentInfoModel);
			if (!success)
			{
				LOG.debug("Fail to adding payment address with po.com");
			}
		}
		else
		{
			LOG.debug("May be any Object was null so not able to add this payment address with po.com");
		}
		return success;
	}

	@Override
	public boolean checkIfCurrentUserIsTheCartUser()
	{
		return super.checkIfCurrentUserIsTheCartUser();
	}


	@Override
	public ExtCheckoutData getCheckoutDataForReview(final String shippingmethod, final ExtCheckoutData extCheckoutDataParam)
	{
		ExtCheckoutData extCheckoutData = extCheckoutDataParam;
		if (extCheckoutData == null)
		{
			extCheckoutData = new ExtCheckoutData();
		}
		final CartData cartData = getCheckoutCart();
		cartData.setShippingPriceCall(shippingPriceCall(shippingmethod));
		extCheckoutData.setCartData(cartData);

		if (!extCheckoutData.isLoadReviewPanel())
		{
			final String errorMessage = configurationService.getConfiguration().getString("checkout.error.cart.notestimated");
			extCheckoutData.setErrorMessage(errorMessage);
			cartData.setShippingPriceCall(shippingmethod);
		}
		return extCheckoutData;
	}

	/**
	 * @return ExtCheckoutData
	 */
	@Override
	public ExtCheckoutData getCheckoutData()
	{
		final ExtCheckoutData extCheckoutData = new ExtCheckoutData();
		extCheckoutData.setPaymentcompleted(false);
		extCheckoutData.setCartData(getCheckoutCart());
		final Map<String, String> messageLabelMap = extCustomMessageUtils.setCheckoutShippingMessages();
		final CustomerType customerType = getCheckoutCustomerStrategy().getCurrentUserForCheckout().getType();
		if (!getCheckoutCustomerStrategy().isAnonymousCheckout() && !customerType.equals(CustomerType.GUEST))
		{
			extCheckoutData.setUserloggedin(Boolean.TRUE);
			messageLabelMap.put(SimonFacadesConstants.NAME, getUserService().getCurrentUser().getName());
		}
		extCheckoutData.setMessageLabels(messageLabelMap);
		return extCheckoutData;
	}



	private String shippingPriceCall(final String shippingmethod)
	{
		String shippingPriceCall;
		if (ShippingPriceCall.CHEAPEST.toString().equalsIgnoreCase(shippingmethod))
		{
			shippingPriceCall = ShippingPriceCall.FASTEST.toString();
		}
		else
		{
			shippingPriceCall = ShippingPriceCall.ALL.toString();
		}
		return shippingPriceCall;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExtCheckoutData estimateAndValidateCart(final ExtCheckoutData extCheckoutData, final String selectedShippingMethod)
	{
		try
		{
			estimateCart(selectedShippingMethod.toLowerCase(Locale.US));
			extCheckoutData.setLoadReviewPanel(true);
		}
		catch (final CartCheckOutIntegrationException | IOException | CalculationException ex)
		{
			LOG.error("Error occurred in cart estimation", ex);
		}
		return extCheckoutData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean validateCreateCartCallbackResponse(final CartModel cart, final int waitTime, final int count)
	{
		LOG.info("Validating cart {} for create cart callback response with wait time {} and left retry attempt {}", cart.getCode(),
				waitTime, count);
		boolean result = cartService.hasReceivedUpdatedCreateCallback(cart);
		for (int i = count; (!result && i > 0); i--)
		{
			try
			{
				Thread.sleep(waitTime);
			}
			catch (final InterruptedException interruptedException)
			{
				LOG.error("Exception occurred in create cart callback response validation for cart {}", cart.getCode(),
						interruptedException);
				Thread.currentThread().interrupt();
			}
			result = cartService.hasReceivedUpdatedCreateCallback(cart);
			LOG.info(
					"Retry attempts completed for create cart callback response for cart {}. Create cart callback response not received.",
					getCart().getCode());

		}


		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean validateCartForRetailerShippingMethods(final AdditionalCartInfoModel additionalCartInfo)
	{
		for (final RetailersInfoModel retailerInfo : additionalCartInfo.getRetailersInfo())
		{
			if (CollectionUtils.isEmpty(retailerInfo.getShippingDetails()))
			{
				LOG.error("Shipping details are missing for one or more retailers");
				return false;
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public boolean estimateCart(final String selectedShippingMethod)
			throws CartCheckOutIntegrationException, IOException, CalculationException
	{
		final CartModel cart = getCartModel();

		final String firstShippingPriceCall = configurationService.getConfiguration().getString(FIRST_SHIPPING_METHOD);
		final String selectedShipping = StringUtils.isNotEmpty(selectedShippingMethod) ? selectedShippingMethod
				: firstShippingPriceCall;

		final List<RetailersInfoModel> retailersInfoList = cart.getAdditionalCartInfo().getRetailersInfo();

		// This will set the selected delivery method for estimate call. Attribute name 'selected'
		commerceCheckoutService.selectShippingForEstimateCall(selectedShipping, retailersInfoList);
		final List<VariantAttributeMappingModel> variantAttributeMappingList = getVariantAttributeMappingList();
		final CartEstimateResponseDTO responseDTO = cartCheckOutIntegrationService.invokeCartEstimateRequest(cart,
				variantAttributeMappingList);
		commerceCheckoutService.populateShippingAndTax(cart.getAdditionalCartInfo().getRetailersInfo(), responseDTO);

		if (selectedShipping.equalsIgnoreCase(firstShippingPriceCall))
		{
			// This will set the Cheapest delivery method as selected for checkout for each retailer. Attribute name 'selectedForCart'
			commerceCheckoutService.setDefaultDeliveryModePerRetailer(selectedShipping, retailersInfoList, cart);
		}

		getModelService().saveAll();
		return true;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDeliveryMethodForRetailer(final String retailerId, final String deliveryModeCode)
	{
		validateParameterNotNullStandardMessage("deliveryModeCode", deliveryModeCode);

		final CartModel cartModel = getCartModel();


		if (cartModel != null && null != cartModel.getAdditionalCartInfo())
		{
			final Optional<RetailersInfoModel> retailerInfo = cartModel.getAdditionalCartInfo().getRetailersInfo().stream()
					.filter(retailer -> retailer.getRetailerId().equalsIgnoreCase(retailerId)).findFirst();

			if (retailerInfo.isPresent())
			{
				final DeliveryModeModel deliveryModeModel = getDeliveryService().getDeliveryModeForCode(deliveryModeCode);
				if (deliveryModeModel != null)
				{
					cartModel.setCalculated(Boolean.FALSE);
					commerceCheckoutService.setShippingForRetailer(retailerInfo.get(), deliveryModeCode, cartModel);
					final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
					commerceCartParameter.setEnableHooks(true);
					commerceCartParameter.setCart(cartModel);
					commerceCartCalculationStrategy.calculateCart(commerceCartParameter);
				}
			}
			getModelService().saveAll();
		}
	}

	protected CartModel getCartModel()
	{
		return getCart();
	}

	/**
	 * Determine if a credit card is chargeable card and available for purchases. if it is successfully verified then
	 * transaction request was successfully executed then true, otherwise false
	 *
	 * @param paymentMethodToken
	 * @throws CartCheckOutIntegrationException
	 */
	@Override
	public void verifyToken(final String paymentMethodToken, final boolean retainOnSuccess, final boolean continueCaching)
			throws CartCheckOutIntegrationException
	{
		final CardVerificationResponseDTO cardVerificationResponseDTO = cardPaymentIntegrationService
				.invokeCardVerification(paymentMethodToken, retainOnSuccess, continueCaching);
		if (StringUtils.isNotBlank(cardVerificationResponseDTO.getErrorCode()))
		{
			throw new CartCheckOutIntegrationException(cardVerificationResponseDTO.getErrorMessage(),
					cardVerificationResponseDTO.getErrorCode());
		}
	}

	/**
	 * This method use to deliver order via ESB to third party
	 *
	 * @param orderCode
	 * @throws CartCheckOutIntegrationException
	 */
	@Override
	public void deliver(final String orderCode, final String paymentMethodToken) throws CartCheckOutIntegrationException
	{
		final OrderModel orderModel = orderService.getOrderByOrderId(orderCode);
		final List<VariantAttributeMappingModel> variantAttributeMappingList = getVariantAttributeMappingList();
		final String isMockResponse = configurationService.getConfiguration()
				.getString(SimonIntegrationConstants.ORDER_DELIVERY_STUB_FLAG);

		if (isMockResponse.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			invokePurchaseMockRequest(orderModel);
		}
		else
		{
			cardPaymentIntegrationService.invokeDeliverOrder(orderModel, paymentMethodToken, variantAttributeMappingList);
		}
	}

	/**
	 * {@link ExtCheckoutFacade} interface.
	 *
	 */
	@Override
	public void savePurchaseCallbackResponse(final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		final OrderModel orderModel = orderService.getOrderByOrderId(orderConfirmCallBackDTO.getOrderId());
		if (null != orderModel)
		{
			savePurchaseCallbackResponseForOrder(orderModel, orderConfirmCallBackDTO);
		}
		else
		{
			LOG.error("Order not found in hybris with the order code : ", orderConfirmCallBackDTO.getOrderId());
		}
	}

	/**
	 * This method will check for the response received as the purchase callback and will calculate the order acceptance
	 * and rejection logic. Depending upon the resultant the order status will be saved and the order process that was in
	 * waiting will be re started.
	 *
	 * @method saveOrderUpdateConfirmationCallBackResponse
	 *
	 * @param orderModel
	 * @param orderConfirmCallBackDTO
	 */
	private void savePurchaseCallbackResponseForOrder(final OrderModel orderModel,
			final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		try
		{
			if (null != orderModel.getAdditionalCartInfo())
			{

				AdditionalCartInfoModel additionalCartInfoModel = orderModel.getAdditionalCartInfo();
				additionalCartInfoModel = confirmUpdateReverseConverter.convert(orderConfirmCallBackDTO, additionalCartInfoModel);
				getModelService().save(additionalCartInfoModel);
				orderModel.setAdditionalCartInfo(additionalCartInfoModel);

				final boolean accepted = orderService.checkAcceptOrRejectTheOrder(orderModel);
				LOG.info("Order has been {} for order id {}", orderModel.getCode(), accepted ? "accepted" : "rejected");
				if (accepted)
				{
					final String isMockResponse = configurationService.getConfiguration()
							.getString(SimonIntegrationConstants.PURCHASE_CONFIRM_STUB);

					if (isMockResponse.equalsIgnoreCase(Boolean.TRUE.toString()))
					{
						invokePurchaseConfirmMockRequest(orderModel.getCode());
					}
					else
					{
						final PurchaseConfirmResponseDTO purchaseConfirmResponseDTO = orderPurchaseIntegrationService
								.invokePurchaseConfirmRequest(orderModel.getCode());
						if (null == purchaseConfirmResponseDTO || StringUtils.isNotEmpty(purchaseConfirmResponseDTO.getErrorCode()))
						{
							orderModel.setStatus(OrderStatus.REJECTED);
						}
					}
				}
				else
				{
					orderModel.setStatus(OrderStatus.REJECTED);
				}
				getModelService().save(orderModel);
				if (OrderStatus.REJECTED.equals(orderModel.getStatus()))
				{
					triggerOrderProcess(orderModel);
				}
				cartService.modifyStockForRemovedProducts(orderModel);
			}
		}
		catch (final ModelSavingException modelSavingException)
		{
			LOG.error("Error occured when getting to save or update the additional info model in the order model:",
					modelSavingException);
		}
	}

	/**
	 * This method return List of VariantAttributeMappingModal by shop gateway using
	 * {@link VariantAttributeMappingService}.Shop gateway is fetch from {@link ShopGatewayService} by using Shop gateway
	 * code which is driven form configuration file.
	 *
	 * @return List of {@link VariantAttributeMappingModel}
	 */
	private List<VariantAttributeMappingModel> getVariantAttributeMappingList()
	{
		final ShopGatewayModel shopGateway = shopGatewayService.getShopGatewayForCode(shopGatewayCode);
		return variantAttributeMappingService.getVariantAttributeMapping(shopGateway);
	}

	/**
	 * @param orderConfirmCallBackDTO
	 */
	@Override
	public void savePurchaseConfirmCallBackResponse(final OrderConfirmCallBackDTO orderConfirmCallBackDTO, final String orderCode)
	{
		final OrderModel orderModel = orderService.getOrderByOrderId(orderCode);
		final OrderModel versionedOrderModel = orderService.getVersionedOrderByOrderId(orderCode);


		if (null != orderModel && versionedOrderModel == null)
		{
			orderService.setOrderIdPerRetailer(orderModel, orderConfirmCallBackDTO);
			//clone order
			LOG.info("Purchase confirm call back received, taking snapshot of orderModel " + orderModel.getCode());
			getModelService().save(orderHistoryService.createHistorySnapshot(orderModel));
			//do calculation according to update callBack response
			orderCallbackReversePopulator.populate(orderConfirmCallBackDTO, orderModel);
			cartService.modifyStockForRemovedProducts(orderModel);
			triggerOrderProcess(orderModel);
		}
		else
		{
			LOG.error("Order not found in hybris with the order code : ", orderConfirmCallBackDTO.getOrderId());
		}
	}

	/**
	 * @description Method use to update the additional info model in the order model when system is on the mock service
	 *              and esb not available
	 * @method invokePurchaseConfirmUpdateMockRequest
	 * @param orderModel
	 */
	private void invokePurchaseMockRequest(final OrderModel orderModel)
	{
		try
		{
			final String stubFileName = configurationService.getConfiguration()
					.getString(SimonIntegrationConstants.PURCHASE_UPDATE_CONFIRM_STUB);

			final OrderConfirmCallBackDTO orderConfirmCallBackDTO = cartCheckOutIntegrationService
					.invokeOrderUpdateConfirmMockRequest(stubFileName);

			savePurchaseCallbackResponseForOrder(orderModel, orderConfirmCallBackDTO);
		}
		catch (final IOException ex)
		{
			LOG.error("IO Exception", ex);
		}
	}

	/**
	 * Method use to update the additional info model in the order model when system is on the mock service and esb not
	 * available
	 *
	 * @method invokeOrderConfirmDeliveryUpdateMockRequest
	 * @param orderModel
	 */
	private void invokePurchaseConfirmMockRequest(final String orderCode)
	{
		try
		{
			final String stubFileName = configurationService.getConfiguration()
					.getString(SimonIntegrationConstants.ORDER_UPDATE_CONFIRM_STUB);

			final OrderConfirmCallBackDTO orderConfirmCallBackDTO = cartCheckOutIntegrationService
					.invokeOrderUpdateConfirmMockRequest(stubFileName);

			savePurchaseConfirmCallBackResponse(orderConfirmCallBackDTO, orderCode);
		}
		catch (final IOException ex)
		{
			LOG.error("when getting response from purchase confirm call back response", ex);
		}
	}

	/**
	 * Trigger order Process when order accept or reject meanwhile process will be in waiting state
	 *
	 * @param orderModel
	 */
	private void triggerOrderProcess(final OrderModel orderModel)
	{
		orderModel.getOrderProcess().stream().filter(process ->
		{
			return process.getCode().startsWith("simon-order-process");
		}).forEach(filteredProcess ->
		{
			businessProcessService
					.triggerEvent(filteredProcess.getCode() + "_" + SimonFacadesConstants.ORDER_PROCESS_ACCEPTED_AND_REJECTED_EVENT);
		});

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderHistoryData getGuestOrderForTracking(final String orderId, final String emailId)
	{
		OrderHistoryData orderHistoryData = null;

		final OrderModel orderModel = orderService.getOrderByOrderId(orderId);

		if (orderModel != null && emailId.equalsIgnoreCase(orderModel.getDeliveryAddress().getEmail()))
		{
			orderHistoryData = orderHistoryConverter.convert(orderModel);
		}
		return orderHistoryData;

	}

	/**
	 * This method use to validate cart
	 *
	 * @return failedProduct
	 */
	@Override
	public boolean validateCart()
	{
		boolean failedProduct = false;
		final CartModel cartModel = getCartService().getSessionCart();
		if (null != cartModel.getAdditionalCartInfo())
		{
			failedProduct = validateFailedProduct(cartModel.getAdditionalCartInfo());
		}
		return failedProduct;
	}

	private boolean validateFailedProduct(final AdditionalCartInfoModel cartInfoModel)
	{
		boolean failedProduct = false;
		for (final RetailersInfoModel retailersInfoModel : cartInfoModel.getRetailersInfo())
		{
			if (CollectionUtils.isNotEmpty(retailersInfoModel.getFailedProducts()))
			{
				failedProduct = true;
				break;
			}
		}
		return failedProduct;
	}


	@Override
	public CheckoutCustomerStrategy getCheckoutCustomerStrategy()
	{
		return checkoutCustomerStrategy;
	}

	@Override
	public void setCheckoutCustomerStrategy(final CheckoutCustomerStrategy checkoutCustomerStrategy)
	{
		this.checkoutCustomerStrategy = checkoutCustomerStrategy;
	}

	@Override
	public UserService getUserService()
	{
		return userService;
	}

	@Override
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * this method will reject the order for purchase call back
	 *
	 * @param orderConfirmCallBackDTO
	 */
	@Override
	public void savePurchaseCallbackErrorResponse(final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		updatePurchaseStatus(orderConfirmCallBackDTO, OrderStatus.REJECTED);
	}

	@Override
	public void savePurchaseConfirmCallbackErrorResponse(final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		updatePurchaseStatus(orderConfirmCallBackDTO, OrderStatus.REJECTED);
	}

	/**
	 * this method will update the order model with provided status
	 *
	 * @param orderId
	 * @param status
	 */
	private void updatePurchaseStatus(final OrderConfirmCallBackDTO orderConfirmCallBackDTO, final OrderStatus status)
	{
		final OrderModel orderModel = orderService.getOrderByOrderId(orderConfirmCallBackDTO.getId());
		if (null != orderModel)
		{
			orderModel.setStatus(status);
			AdditionalCartInfoModel additionCartInfo = orderModel.getAdditionalCartInfo();
			additionCartInfo = confirmUpdateReverseConverter.convert(orderConfirmCallBackDTO, additionCartInfo);
			additionCartInfo.setErrorCode(orderConfirmCallBackDTO.getErrorCode());
			additionCartInfo.setErrorMessage(
					StringUtils.isNotEmpty(orderConfirmCallBackDTO.getErrorMessage()) ? orderConfirmCallBackDTO.getErrorMessage()
							: SimonIntegrationConstants.NO_ERROR_MSG_FROM_ESB);

			getModelService().save(additionCartInfo);
			getModelService().save(orderModel);
			cartService.modifyStockForRemovedProducts(orderModel);
			cartService.modifyStockForFailedProducts(orderModel);
			triggerOrderProcess(orderModel);
		}
		else
		{
			LOG.error("Purchase Call back, Order not found in hybris with the order code : ", orderConfirmCallBackDTO.getId());
		}

	}

	/**
	 * This method use to validate cart
	 *
	 * @return
	 */
	@Override
	public List<String> validateCartForVariant()
	{
		final Map<String, Map<String, String>> prodIdCartFieldNames = getCartVariant();
		final Map<String, List<String>> prodIdRequiredFieldNames = getRquiredVariant(SimonFacadesConstants.FIELD_NAME);
		return compareVariantOption(prodIdCartFieldNames, prodIdRequiredFieldNames, SimonFacadesConstants.FIELD_NAME);
	}

	/**
	 * This method use to find the variant of product
	 *
	 * @return map of product and variant
	 */
	private Map<String, Map<String, String>> getCartVariant()
	{
		final Map<String, Map<String, String>> prodIdRequiredField = new HashMap<String, Map<String, String>>();
		final CartModel cartModel = getCartService().getSessionCart();
		for (final AbstractOrderEntryModel orderEntryModel : cartModel.getEntries())
		{
			prodIdRequiredField.put(orderEntryModel.getProduct().getCode(), getVariantFieldTargetNames(orderEntryModel));
		}

		return prodIdRequiredField;
	}

	private Map<String, String> getVariantFieldTargetNames(final AbstractOrderEntryModel orderEntryModel)
	{
		Map<String, String> sourceAttributeMapInLowerCase = new HashMap<>();
		Map<String, String> sourceAttributeInSystem = new HashMap<>();

		final Map<String, String> sourceAttributeMapping = new HashMap<>();


		final ShopGatewayModel shopGateway = shopGatewayService.getShopGatewayForCode(shopGatewayCode);
		final Map<String, String> sourceAttributeMap = variantAttributeMappingService
				.getVarinatAttributeTwoTapLabelMap(orderEntryModel.getProduct().getShop(), shopGateway);

		if (!sourceAttributeMap.isEmpty())
		{
			sourceAttributeMapInLowerCase = CommonUtils.convertMapIntoLowerCase(sourceAttributeMap);
		}

		final OrderEntryData orderEntry = orderEntryConverter.convert(orderEntryModel);
		final Map<String, String> inSystem = orderEntry.getProduct().getVariantValues();

		if (!inSystem.isEmpty())
		{
			sourceAttributeInSystem = CommonUtils.convertMapIntoLowerCase(inSystem);
		}

		final Iterator<Map.Entry<String, String>> itr = sourceAttributeInSystem.entrySet().iterator();

		while (itr.hasNext())
		{
			final Map.Entry<String, String> entry = itr.next();

			if (sourceAttributeMapInLowerCase.keySet().contains(entry.getKey()))
			{
				sourceAttributeMapping.put(sourceAttributeMapInLowerCase.get(entry.getKey()), entry.getValue());
			}
			else
			{
				sourceAttributeMapping.put(entry.getKey(), entry.getValue());
			}

		}

		return sourceAttributeMapping;
	}

	/**
	 * This method create map for required variants
	 *
	 */
	private Map<String, List<String>> getRquiredVariant(final String nameOrValue)
	{
		final Map<String, List<String>> prodIdField = new HashMap<>();
		final CartModel cartModel = getCartService().getSessionCart();
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			prodIdField.put(entry.getProduct().getCode(),
					requiredFieldsOrValues(cartModel.getAdditionalCartInfo(), entry, nameOrValue));

		}

		return prodIdField;
	}

	/**
	 * return list of required fields
	 *
	 */
	private List<String> requiredFieldsOrValues(final AdditionalCartInfoModel cartInfoModel, final AbstractOrderEntryModel entry,
			final String nameOrValue)
	{
		List<String> requiredFields = new ArrayList<>();
		for (final RetailersInfoModel retailersInfoModel : cartInfoModel.getRetailersInfo())
		{
			if (CollectionUtils.isNotEmpty(retailersInfoModel.getAddedProducts()))
			{
				requiredFields = requiredFieldNameOrValueFromAdditionalProduct(entry.getProduct().getCode(),
						retailersInfoModel.getAddedProducts(), nameOrValue);
				if (CollectionUtils.isNotEmpty(requiredFields))
				{
					break;
				}
			}
		}
		return requiredFields;
	}

	/**
	 * return list of required fields from additional product details
	 *
	 */
	private List<String> requiredFieldNameOrValueFromAdditionalProduct(final String productCode,
			final List<AdditionalProductDetailsModel> additionalProductDetails, final String nameOrValue)
	{
		final List<String> requiredFields = new ArrayList<>();

		for (final AdditionalProductDetailsModel additionalProductDetailsModel : additionalProductDetails)
		{
			if (productCode.equalsIgnoreCase(additionalProductDetailsModel.getProductID()))
			{

				requiredFields.addAll(nameOrValue.equalsIgnoreCase(SimonFacadesConstants.FIELD_NAME)
						? additionalProductDetailsModel.getRequiredFieldNames()
						: additionalProductDetailsModel.getRequiredFieldValues());
				break;
			}
		}

		return requiredFields;
	}

	/**
	 * compare value of requried and given variants
	 *
	 */
	private List<String> compareVariantOption(final Map<String, Map<String, String>> prodIdCartFieldNameOrValues,
			final Map<String, List<String>> prodIdRequiredFieldNameOrValues, final String nameOrValue)
	{
		final List<String> productIds = new ArrayList<>();
		final Set<String> keys = prodIdRequiredFieldNameOrValues.keySet();
		for (final String key : keys)
		{
			final List<String> requiredFields = prodIdRequiredFieldNameOrValues.get(key);
			final Map<String, String> givenFields = prodIdCartFieldNameOrValues.get(key);
			if (nameOrValue.equalsIgnoreCase(SimonFacadesConstants.FIELD_NAME))
			{
				productIds.add(markProductUnapproved(givenFields, requiredFields, key));
			}
			else
			{
				productIds.add(markProductOOS(givenFields, requiredFields, key));
			}

		}
		productIds.removeAll(Arrays.asList("", null));
		return productIds;
	}

	private String markProductUnapproved(final Map<String, String> givenFields, final List<String> requiredFields,
			final String key)
	{
		String productIds = StringUtils.EMPTY;
		final List<String> fieldAvailable = new ArrayList<>();
		fieldAvailable.addAll(givenFields.keySet());
		final String fieldNames = allRequiredFieldsFind(requiredFields, fieldAvailable);
		if (StringUtils.isNotEmpty(fieldNames))
		{
			productIds = key;
			markedUnapproved(key, fieldNames);
		}
		return productIds;
	}

	private String markProductOOS(final Map<String, String> givenFields, final List<String> requiredFieldsValues, final String key)
	{
		String productIds = StringUtils.EMPTY;
		boolean result = false;

		for (final String requiredField : requiredFieldsValues)
		{
			final Map<String, String> requriedValues = convertIntoMap(requiredField);

			if (MapUtils.isEmpty(requriedValues) || givenFields.entrySet().containsAll(requriedValues.entrySet()))
			{
				result = true;
				break;
			}
		}
		if (!result)
		{
			productIds = key;
			markedOutOfStock(key);
		}

		return productIds;
	}

	private Map<String, String> convertIntoMap(final String variants)
	{
		final Map<String, String> keyValue = new HashMap<>();

		final String[] split = variants.split(SimonFacadesConstants.PIPE_ESCAPE_CHARACTER);
		for (final String value : split)
		{
			final String[] sp = value.split(SimonFacadesConstants.COLON);
			if (!SimonFacadesConstants.REQUIRED_FIELD_VALUE_FOUND_URL.equalsIgnoreCase(sp[SimonFacadesConstants.INT_ONE]))
			{
				keyValue.put(sp[SimonFacadesConstants.INT_ZERO], sp[SimonFacadesConstants.INT_ONE]);
			}
		}
		return keyValue;
	}


	/**
	 * method markedUnapproved log the product in ErrorLog
	 */
	private void markedUnapproved(final String productCode, final String fieldNames)
	{
		final CartModel cartModel = getCartService().getSessionCart();
		final ErrorLogModel error = modelService.create(ErrorLogModel.class);
		error.setId(cartModel.getCode());
		error.setLogId(cartModel.getCode() + SimonCoreConstants.HYPHEN + productCode + SimonCoreConstants.HYPHEN
				+ System.currentTimeMillis());
		error.setProductIds(productCode);
		error.setErrorMessage(SimonCoreConstants.UNAPPROVED);
		error.setErrorStackTrace(fieldNames.replace(SimonCoreConstants.HYPHEN, SimonCoreConstants.DELIMITER) + " missing");
		modelService.save(error);

	}

	/**
	 * method markedOOS log the product in ErrorLog
	 */
	private void markedOutOfStock(final String productCode)
	{
		final CartModel cartModel = getCartService().getSessionCart();
		final ErrorLogModel error = modelService.create(ErrorLogModel.class);
		error.setId(cartModel.getCode());
		error.setLogId(cartModel.getCode() + SimonCoreConstants.HYPHEN + productCode + SimonCoreConstants.HYPHEN
				+ System.currentTimeMillis());
		error.setProductIds(productCode);
		error.setErrorMessage(SimonFacadesConstants.OOS);
		modelService.save(error);

		cartService.updateStockForProductCode(cartModel, productCode, false);
	}

	/**
	 * check variant exist or not
	 *
	 */
	private String allRequiredFieldsFind(final List<String> requiredFields, final List<String> fieldAvailable)
	{
		final StringBuilder variantMissing = new StringBuilder();
		for (final String fieldName : requiredFields)
		{
			if (!fieldName.equalsIgnoreCase(SimonCoreConstants.QUANTITY)
					&& !fieldAvailable.contains(fieldName.toLowerCase(Locale.US)))
			{
				if (variantMissing.length() > SimonFacadesConstants.INT_ZERO)
				{
					variantMissing.append(SimonCoreConstants.HYPHEN).append(fieldName);
				}
				else
				{
					variantMissing.append(fieldName);
				}
			}

		}
		return variantMissing.toString();
	}

	/**
	 * @description Overridden method to set the email address in the address model when email address is not exist
	 * @method setDeliveryAddress
	 * @param addressData
	 * @return boolean
	 */
	@Override
	public boolean setDeliveryAddress(final AddressData addressData)
	{
		final CartModel cartModel = getCartModel();
		if (cartModel != null)
		{
			AddressModel addressModel = null;
			if (addressData != null)
			{
				addressModel = addressData.getId() == null ? createUpdatedDeliveryAddressModel(addressData, cartModel)
						: getUpdatedDeliveryAddressModelForCode(addressData.getId());

				addressModel.setEmail(addressData.getEmail());
				getModelService().save(addressModel);
			}

			final CommerceCheckoutParameter parameter = createCommerceCheckoutParameterFromCart(cartModel, true);
			parameter.setAddress(addressModel);
			parameter.setIsDeliveryAddress(false);
			return getCommerceCheckoutService().setDeliveryAddress(parameter);
		}
		return false;
	}

	protected AddressModel createUpdatedDeliveryAddressModel(final AddressData addressData, final CartModel cartModel)
	{
		return createDeliveryAddressModel(addressData, cartModel);
	}

	protected AddressModel getUpdatedDeliveryAddressModelForCode(final String code)
	{
		return getDeliveryAddressModelForCode(code);
	}

	protected CommerceCheckoutParameter createCommerceCheckoutParameterFromCart(final CartModel cart, final boolean enableHooks)
	{
		return createCommerceCheckoutParameter(cart, enableHooks);
	}

	/**
	 * This method use to validate cart for the variants exist on
	 *
	 * @return
	 */
	@Override
	public List<String> validateCartForVariantValues()
	{
		final Map<String, Map<String, String>> prodIdCartFieldValues = getCartVariant();
		final Map<String, List<String>> prodIdRequiredFieldValues = getRquiredVariant(SimonFacadesConstants.FIELD_VALUES);
		return compareVariantOption(prodIdCartFieldValues, prodIdRequiredFieldValues, SimonFacadesConstants.FIELD_VALUES);
	}


	@Override
	public boolean validatePaymentMethodInternal(final String paymentInfoId) throws ParseException
	{
		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();

		final CreditCardPaymentInfoModel creditCardPaymentInfo = getCustomerAccountService()
				.getCreditCardPaymentInfoForCode(currentCustomer, paymentInfoId);

		boolean flag = false;
		if (creditCardPaymentInfo != null)
		{
			final String expDate = new StringBuilder(creditCardPaymentInfo.getValidToYear()).append("-")
					.append(creditCardPaymentInfo.getValidToMonth()).toString();
			final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
			final Date cardExpdate = dateFormat.parse(expDate);
			final Date todayDate = dateFormat.parse(dateFormat.format(new Date()));

			if (todayDate.compareTo(cardExpdate) <= 0)
			{
				flag = true;
			}
		}
		return flag;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CCPaymentInfoData> getCCPaymentInfoFromExternalSystem() throws UserAddressIntegrationException
	{
		final List<CCPaymentInfoData> ccPaymentInfoDataLst = new ArrayList<>();
		List<GetAddressResponseDTO> addressesList = new ArrayList<>();

		final UserGetAddressResponseDTO userAddressResponseDTO = userAddressIntegrationService.getUserBillingAddress();
		if (userAddressResponseDTO != null && CollectionUtils.isNotEmpty(userAddressResponseDTO.getAddresses()))
		{
			addressesList = userAddressResponseDTO.getAddresses();
		}

		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final List<CreditCardPaymentInfoModel> creditCardPaymentInfoModelList = getCustomerAccountService()
				.getCreditCardPaymentInfos(currentCustomer, false);

		if (CollectionUtils.isNotEmpty(creditCardPaymentInfoModelList))
		{
			for (final CreditCardPaymentInfoModel creditCardPaymentInfo : creditCardPaymentInfoModelList)
			{
				boolean flag = false;
				for (final GetAddressResponseDTO address : addressesList)
				{
					final String addressExternalID = userIntegrationUtils.removeEnvPrefixToAddressId(address.getExternalId());

					if (addressExternalID.equals(creditCardPaymentInfo.getBillingAddress().getPk().toString()))
					{
						flag = true;
						ccPaymentInfoDataLst.add(setPaymentMethodToken(address, creditCardPaymentInfo));
						break;
					}
				}
				if (!flag)
				{
					LOG.error("Payment address missing in SPO.com but present in PO.com");
				}
			}
		}
		return ccPaymentInfoDataLst;
	}

	private CCPaymentInfoData setPaymentMethodToken(final GetAddressResponseDTO address,
			final CreditCardPaymentInfoModel creditCardPaymentInfo)
	{
		final CCPaymentInfoData paymentInfoData = getCreditCardPaymentInfoConverter().convert(creditCardPaymentInfo);
		if (CollectionUtils.isNotEmpty(address.getPayments()))
		{
			paymentInfoData.setPaymentToken(address.getPayments().get(0).getToken());
		}
		return paymentInfoData;
	}

	@Override
	protected void afterPlaceOrder(final CartModel cartModel, final OrderModel orderModel)
	{
		if (orderModel != null)
		{
			getCartService().removeSessionCart();
			getModelService().refresh(orderModel);
			orderModel.getEntries().stream().forEach(orderEntry ->
			{
				stockService.getAllStockLevels(orderEntry.getProduct()).stream().forEach(stockLevel ->
				{
					stockLevel.setReserved(stockLevel.getReserved() + orderEntry.getQuantity().intValue());
					modelService.save(stockLevel);
				});
			});
		}
	}

	public boolean validateRetailerOrderIDIsPresent(final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		boolean flag = false;
		final List<OrderConfirmRetailersInfoCallBackDTO> retailersList = orderConfirmCallBackDTO.getRetailers();

		for (final OrderConfirmRetailersInfoCallBackDTO orderConfirmRetailersInfoCallBackDTO : retailersList)
		{
			if (StringUtils.isNotEmpty(orderConfirmRetailersInfoCallBackDTO.getOrderId()))
			{
				flag = true;
			}
		}
		return flag;

	}

	/**
	 * {@inheritDoc}
	 */
	public void savePurchaseConfirmCallBackPartialResponse(final OrderConfirmCallBackDTO orderConfirmCallBackDTO,
			final String orderId)
	{
		final OrderModel orderModel = orderService.getOrderByOrderId(orderId);
		final OrderModel versionedOrderModel = orderService.getVersionedOrderByOrderId(orderId);

		if (null != orderModel && versionedOrderModel == null)
		{
			orderService.setOrderIdPerRetailer(orderModel, orderConfirmCallBackDTO);
			//clone order
			LOG.info("Purchase confirm call back received, taking snapshot of orderModel " + orderModel.getCode());
			getModelService().save(orderHistoryService.createHistorySnapshot(orderModel));

			cancelInvalidOrderEntries(orderModel, orderConfirmCallBackDTO);

			orderCallbackReversePopulator.populate(orderConfirmCallBackDTO, orderModel);
			final AdditionalCartInfoModel additionCartInfo = orderModel.getAdditionalCartInfo();
			additionCartInfo.setErrorCode(orderConfirmCallBackDTO.getErrorCode());
			additionCartInfo.setErrorMessage(
					StringUtils.isNotEmpty(orderConfirmCallBackDTO.getErrorMessage()) ? orderConfirmCallBackDTO.getErrorMessage()
							: SimonIntegrationConstants.NO_ERROR_MSG_FROM_ESB);
			getModelService().save(additionCartInfo);
			getModelService().save(orderModel);
			triggerOrderProcess(orderModel);
		}
		else
		{
			LOG.error("Order not found in hybris with the order code : ", orderConfirmCallBackDTO.getOrderId());
		}
	}

	private void cancelInvalidOrderEntries(final OrderModel orderModel, final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		final List<String> productCodes = new ArrayList<>();
		for (final OrderConfirmRetailersInfoCallBackDTO retailerInfo : orderConfirmCallBackDTO.getRetailers())
		{
			if (StringUtils.isEmpty(retailerInfo.getOrderId()))
			{
				orderConfirmCallBackDTO.getRetailers().stream().forEach(retailersInfo ->
				{
					retailersInfo.getCartProducts().stream().forEach(product -> productCodes.add(product.getProductId()));
					retailersInfo.getCartFailedProducts().stream().forEach(product -> productCodes.add(product.getProductId()));
					retailersInfo.getCartRemovedProducts().stream().forEach(product -> productCodes.add(product.getProductId()));
				});
			}
			else if (CollectionUtils.isNotEmpty(retailerInfo.getCartRemovedProducts())
					|| CollectionUtils.isNotEmpty(retailerInfo.getCartFailedProducts()))
			{
				orderConfirmCallBackDTO.getRetailers().stream().forEach(retailersInfo ->
				{
					retailersInfo.getCartFailedProducts().stream().forEach(product -> productCodes.add(product.getProductId()));
					retailersInfo.getCartRemovedProducts().stream().forEach(product -> productCodes.add(product.getProductId()));
				});
			}
		}

		for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries())
		{
			if (productCodes.contains(orderEntry.getProduct().getCode()))
			{
				cartService.modifyStockForRemovedProducts(orderModel);
			}
		}
	}

	@Override
	public void checkAdditionalCartInfo(final ExtCheckoutData checkoutdata)
	{
		final CartModel cart = cartService.getSessionCart();

		final int waitTime = Integer
				.parseInt(configurationService.getConfiguration().getString("create.cart.response.wait.time", "5000"));
		final int count = Integer
				.parseInt(configurationService.getConfiguration().getString("create.cart.response.retry.attempt", "3"));

		if (null != cart && validateCreateCartCallbackResponse(cart, waitTime / count, count))
		{
			checkoutdata.setCreateCartResponseReceived(true);
			if (configurationService.getConfiguration().getBoolean(STUB_REQUIRED_FIELD_VALIDATION))
			{
				final List<String> variantField = validateCartForVariant();
				variantField.addAll(productsAreNotAdded());
				final List<String> variantFieldValues = validateCartForVariantValues();

				if (validateCart() || CollectionUtils.isNotEmpty(variantField) || CollectionUtils.isNotEmpty(variantFieldValues))
				{
					sessionService.setAttribute(SimonCoreConstants.UNAPPROVED_PRODUCTIDS, variantField);
					sessionService.setAttribute(SimonCoreConstants.OOS_PRODUCTIDS, variantFieldValues);
					checkoutdata.setErrorMessage(SimonCoreConstants.ERROR_CODE3);
				}
			}
			else if (validateCart())
			{
				checkoutdata.setErrorMessage(SimonCoreConstants.ERROR_CODE3);
			}
		}

	}

	/**
	 * This method is use to find the products those are not have any information in additional cart info
	 *
	 * @return List<String>
	 */
	private List<String> productsAreNotAdded()
	{
		final CartModel cartModel = getCartService().getSessionCart();
		final List<String> notFoundInAdditionalCart = new ArrayList<>();
		final List<String> cartProduct = productsInCart(cartModel);
		final List<String> additionalCartProduct = productsInAdditionalCart(cartModel);
		for (final String productCode : cartProduct)
		{
			if (!additionalCartProduct.contains(productCode))
			{
				notFoundInAdditionalCart.add(productCode);
				markedUnapproved(productCode, StringUtils.EMPTY);
				LOG.error("Not found any information for the product :{} in additional cart : {}", productCode, cartModel.getCode());
			}
		}

		return notFoundInAdditionalCart;
	}

	/**
	 * This method is use to find the added products in the cart
	 *
	 * @return List<String>
	 */
	private List<String> productsInCart(final CartModel cartModel)
	{
		final List<String> cartProduct = new ArrayList<>();
		for (final AbstractOrderEntryModel orderEntryModel : cartModel.getEntries())
		{
			cartProduct.add(orderEntryModel.getProduct().getCode());
		}
		return cartProduct;
	}

	/**
	 * This method is use to find the added products in the additional cart
	 *
	 * @return List<String>
	 */
	private List<String> productsInAdditionalCart(final CartModel cartModel)
	{
		final List<String> additionalCartProduct = new ArrayList<>();
		final AdditionalCartInfoModel additionalCartInfo = cartModel.getAdditionalCartInfo();
		if (cartService.hasReceivedUpdatedCreateCallback(cartModel))
		{
			for (final RetailersInfoModel retailersInfoModel : additionalCartInfo.getRetailersInfo())
			{
				additionalCartProduct.addAll(productsInAdditionalCart(retailersInfoModel));
			}
		}

		return additionalCartProduct;
	}

	/**
	 * This method is use to find the added products from the retailer info
	 *
	 * @return List<String>
	 */
	private List<String> productsInAdditionalCart(final RetailersInfoModel retailersInfoModel)
	{
		final List<String> additionalCartProduct = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(retailersInfoModel.getAddedProducts()))
		{
			for (final AdditionalProductDetailsModel additionalProductDetailsModel : retailersInfoModel.getAddedProducts())
			{
				additionalCartProduct.add(additionalProductDetailsModel.getProductID());

			}
		}

		return additionalCartProduct;
	}

}
