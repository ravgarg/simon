package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;


/**
 *
 * Converter implementation for {@link de.hybris.platform.core.model.order.CartModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.CartData} as target type.
 */
public class ExtMiniCartPopulator extends CartPopulator<CartData>
{
	/**
	 * ExtMiniCartPopulator extends CartPopulator and call addCommon, addTotals, addPromotions adn set the Guid of target
	 */
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		addCommon(source, target);
		addTotals(source, target);
		addPromotions(source, target);
		target.setGuid(source.getGuid());
	}
}
