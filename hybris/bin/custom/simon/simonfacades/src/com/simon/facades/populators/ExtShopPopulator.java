package com.simon.facades.populators;

import static java.lang.String.format;
import static reactor.util.StringUtils.isEmpty;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.domain.additionalfield.MiraklAdditionalFieldType;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue.MiraklAbstractAdditionalFieldWithSingleValue;
import com.mirakl.client.mmp.domain.shop.MiraklShop;
import com.mirakl.client.mmp.domain.shop.MiraklShopStats;
import com.mirakl.hybris.core.enums.PremiumState;
import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.populators.ShopPopulator;
import com.simon.core.constants.SimonCoreConstants.ShopCustomFields;
import com.simon.core.enums.OrderExportType;


/**
 * Adds Custom Attributes in ShopModel for retailer information.
 */
public class ExtShopPopulator extends ShopPopulator
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtShopPopulator.class);

	/*
	 * (non-Javadoc)
	 *
	 * @see com.mirakl.hybris.core.shop.populators.ShopPopulator#populate(com.mirakl.client.mmp.domain.shop.MiraklShop,
	 * com.mirakl.hybris.core.model.ShopModel)
	 */
	@Override
	public void populate(final MiraklShop miraklShop, final ShopModel shopModel)
	{
		populateDefault(miraklShop, shopModel);
		populateAdditionalValue(miraklShop, shopModel);
	}

	/**
	 * Populate the Additional Values for Shop.
	 *
	 * @param miraklShop
	 *           the mirakl shop
	 * @param shopModel
	 *           the shop model
	 */
	private void populateAdditionalValue(final MiraklShop miraklShop, final ShopModel shopModel)
	{
		final List<MiraklAdditionalFieldValue> additionalValues = miraklShop.getAdditionalFieldValues();
		if (CollectionUtils.isNotEmpty(additionalValues))
		{
			for (final MiraklAdditionalFieldValue additionalFieldValue : additionalValues)
			{
				if (!MiraklAdditionalFieldType.MULTIPLE_VALUES_LIST.equals(additionalFieldValue.getFieldType()))
				{
					populateAddtionalSingleValue(shopModel, additionalFieldValue);
				}
			}
		}
	}


	/**
	 * Additional Single value Fields.
	 *
	 * @param shopModel
	 *           the shop model
	 * @param additionalFieldValue
	 *           the additional field value
	 */
	private void populateAddtionalSingleValue(final ShopModel shopModel, final MiraklAdditionalFieldValue additionalFieldValue)
	{
		final MiraklAbstractAdditionalFieldWithSingleValue singleAdditionalFieldValue = (MiraklAbstractAdditionalFieldWithSingleValue) additionalFieldValue;
		final String rawValue = singleAdditionalFieldValue.getValue();

		switch (singleAdditionalFieldValue.getCode())
		{
			case ShopCustomFields.PREFIX:
				shopModel.setPreFix(rawValue);
				break;
			case ShopCustomFields.ORDER_EXPORT:
				shopModel.setOrderExportType(setOrderExportEnumValue(rawValue));
				break;
			default:
				LOGGER.debug("Invalid Additional Field '{}' Recevied. Ignoring", singleAdditionalFieldValue.getCode());
				break;
		}
	}

	/**
	 * Setting Order Export Value
	 *
	 * @param rawValue
	 *
	 * @return OrderExportType
	 */
	private OrderExportType setOrderExportEnumValue(final String rawValue)
	{
		OrderExportType orderExportType = null;

		if (StringUtils.isNotEmpty(rawValue))
		{

			if (rawValue.equalsIgnoreCase(OrderExportType.BOT.toString()))
			{
				orderExportType = OrderExportType.BOT;
			}
			else if (rawValue.equalsIgnoreCase(OrderExportType.RETAILER.toString()))
			{
				orderExportType = OrderExportType.RETAILER;

			}
			else if (rawValue.equalsIgnoreCase(OrderExportType.NO_RETAILER.toString()))
			{
				orderExportType = OrderExportType.NO_RETAILER;
			}
		}
		return orderExportType;

	}

	protected void populateDefault(final MiraklShop miraklShop, final ShopModel shopModel)
	{
		final String currencyCode = miraklShop.getCurrencyIsoCode().toString();
		final CurrencyModel currency = currencyService.getCurrencyForCode(currencyCode);
		if (currency != null)
		{
			shopModel.setCurrency(currency);
		}
		else
		{
			throw new ConversionException(format("Impossible to find the currency with code '%s'", currencyCode));
		}

		final MiraklShopStats shopStatistic = miraklShop.getShopStatistic();
		if (shopStatistic != null)
		{
			shopModel.setApprovalDelay(shopStatistic.getApprovalDelay());
			shopModel.setOffersCount(shopStatistic.getOffersCount());
			shopModel.setOrdersCount(shopStatistic.getOrdersCount());
			if (shopStatistic.getApprovalRate() != null)
			{
				shopModel.setApprovalRate(shopStatistic.getApprovalRate().doubleValue());
			}
			if (shopStatistic.getEvaluationsCount() != null)
			{
				shopModel.setEvaluationCount(shopStatistic.getEvaluationsCount().intValue());
			}
		}

		if (miraklShop.getShippingInformation() != null)
		{
			shopModel.setFreeShipping(miraklShop.getShippingInformation().getFreeShipping());
			final String shippingCountry = miraklShop.getShippingInformation().getShippingCountry();
			if (!isEmpty(shippingCountry))
			{
				shopModel.setShippingCountry(countryService.getCountryForIsoAlpha3Code(shippingCountry));
			}
		}

		if (miraklShop.getPremiumState() != null)
		{
			shopModel.setPremiumState(PremiumState.valueOf(miraklShop.getPremiumState().toString()));
		}

		if (miraklShop.getState() != null)
		{
			shopModel.setState(ShopState.valueOf(miraklShop.getState().toString()));
		}

		if (miraklShop.getGrade() != null)
		{
			shopModel.setGrade(miraklShop.getGrade().doubleValue());
		}

		shopModel.setDescription(miraklShop.getDescription());
		shopModel.setRegistrationDate(miraklShop.getDateCreated());
		shopModel.setClosedFrom(miraklShop.getClosedFrom());
		shopModel.setClosedTo(miraklShop.getClosedTo());
		shopModel.setId(miraklShop.getId());
		shopModel.setInternalId(miraklShop.getOperatorInternalId());
		shopModel.setName(miraklShop.getName());
		shopModel.setPremium(miraklShop.isPremium());
		shopModel.setProfessional(miraklShop.isProfessional());
	}
}
