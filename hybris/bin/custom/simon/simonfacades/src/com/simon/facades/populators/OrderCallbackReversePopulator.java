package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


/**
 * populate AdditionalProductDetailsModel from OrderConfirmCartProductsCallBackDTO which have information of retailers,
 * added products.
 *
 */
public class OrderCallbackReversePopulator implements Populator<OrderConfirmCallBackDTO, OrderModel>
{


	@Resource
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	@Resource
	private ModelService modelService;

	@Override
	public void populate(final OrderConfirmCallBackDTO source, final OrderModel target) throws ConversionException
	{
		updateRetailersData(source, target);
		updateOrderPrices(source, target);
		modelService.saveAll(target.getEntries());
		modelService.save(target);
	}

	/**
	 * Update order prices.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void updateOrderPrices(final OrderConfirmCallBackDTO source, final OrderModel target)
	{
		target.setTotalPrice(getDoubleValue(source.getFinalPrice()));
		target.setTotalTax(getDoubleValue(source.getTax()));
		target.setDeliveryCost(getDoubleValue(source.getShippingPrice()));

	}

	/**
	 * Update retailers data.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void updateRetailersData(final OrderConfirmCallBackDTO source, final OrderModel target)
	{
		for (final OrderConfirmRetailersInfoCallBackDTO callBackDTO : source.getRetailers())
		{
			for (final AbstractOrderEntryModel orderEntry : target.getEntries())
			{
				if (null != orderEntry.getShopId() && orderEntry.getShopId().equals(callBackDTO.getRetailerId()))
				{
					updateApprovedOrderEntryData(callBackDTO, orderEntry);
					updateFailedOrderEntryData(callBackDTO, orderEntry);

				}

			}

		}
		updateRetailersMapData(source, target);

	}

	/**
	 * Update failed order entry data.
	 *
	 * @param callBackDTO
	 *           the call back DTO
	 * @param orderEntry
	 *           the order entry
	 */
	private void updateFailedOrderEntryData(final OrderConfirmRetailersInfoCallBackDTO callBackDTO,
			final AbstractOrderEntryModel orderEntry)
	{
		if (CollectionUtils.isNotEmpty(callBackDTO.getCartFailedProducts()))
		{
			for (final OrderConfirmCartProductsCallBackDTO productCallBackDto : callBackDTO.getCartFailedProducts())
			{
				if (orderEntry.getProduct().getCode().equalsIgnoreCase(productCallBackDto.getProductId()))
				{
					orderEntry.setCallBackOrderEntryStatus(CallBackOrderEntryStatus.CANCELLED);
					updateOrderEntryPrice(orderEntry, productCallBackDto);
				}

			}
		}

	}

	/**
	 * Update approved order entry data.
	 *
	 * @param callBackDTO
	 *           the call back DTO
	 * @param orderEntry
	 *           the order entry
	 */
	private void updateApprovedOrderEntryData(final OrderConfirmRetailersInfoCallBackDTO callBackDTO,
			final AbstractOrderEntryModel orderEntry)
	{
		for (final OrderConfirmCartProductsCallBackDTO productCallBackDto : callBackDTO.getCartProducts())
		{
			if (orderEntry.getProduct().getCode().equalsIgnoreCase(productCallBackDto.getProductId()))
			{
				orderEntry.setCallBackOrderEntryStatus(CallBackOrderEntryStatus.APPROVED);
				updateOrderEntryPrice(orderEntry, productCallBackDto);
			}

		}
	}

	/**
	 * Update order entry price.
	 *
	 * @param orderEntry
	 *           the order entry
	 * @param productCallBackDto
	 *           the product call back dto
	 */
	private void updateOrderEntryPrice(final AbstractOrderEntryModel orderEntry,
			final OrderConfirmCartProductsCallBackDTO productCallBackDto)
	{
		final Double totalPrice = !getDoubleValue(productCallBackDto.getDiscountedPrice()).equals(Double.valueOf(0))
				? getDoubleValue(productCallBackDto.getDiscountedPrice()) : getDoubleValue(productCallBackDto.getPrice());
		orderEntry.setTotalPrice((orderEntry.getQuantity() * totalPrice));
		orderEntry.setSaleValue(totalPrice);
		orderEntry.setBasePrice(totalPrice);
	}

	/**
	 * Update retailers map data.
	 *
	 * @param callBackDto
	 *           the call back dto
	 * @param target
	 *           the target
	 */
	private void updateRetailersMapData(final OrderConfirmCallBackDTO orderConfirmCallBackDTO, final OrderModel target)
	{

		final Map<String, Double> subTotal = new HashMap<>();
		final Map<String, Double> deliveryCost = new HashMap<>();
		final Map<String, Double> totalTax = new HashMap<>();
		final Map<String, Double> totalDiscount = new HashMap<>();
		for (final OrderConfirmRetailersInfoCallBackDTO callBackDTO : orderConfirmCallBackDTO.getRetailers())
		{
			final String retailerId = callBackDTO.getRetailerId();
			subTotal.put(retailerId, calcRetailerSubTotal(callBackDTO));
			deliveryCost.put(retailerId, getDoubleValue(callBackDTO.getShippingPrice()));
			totalDiscount.put(retailerId, getDoubleValue(callBackDTO.getCouponValue()));
			totalTax.put(retailerId, getDoubleValue(callBackDTO.getTax()));
		}
		target.setRetailersSubtotal(subTotal);
		target.setRetailersDeliveryCost(deliveryCost);
		target.setRetailersTotalTax(totalTax);
		target.setRetailersTotalDiscount(totalDiscount);

	}

	/**
	 * Calc retailer sub total.
	 *
	 * @param source
	 *           the source
	 * @return the double
	 */
	private Double calcRetailerSubTotal(final OrderConfirmRetailersInfoCallBackDTO orderConfirmRetailersInfoCallBackDTO)
	{
		return (getDoubleValue(orderConfirmRetailersInfoCallBackDTO.getFinalPrice())
				- ((getDoubleValue(orderConfirmRetailersInfoCallBackDTO.getTax())
						+ getDoubleValue(orderConfirmRetailersInfoCallBackDTO.getShippingPrice())))
				+ getDoubleValue(orderConfirmRetailersInfoCallBackDTO.getCouponValue()));
	}

	/**
	 * Gets the double value.
	 *
	 * @param value
	 *           the value
	 * @return the double value
	 */
	private Double getDoubleValue(final String value)
	{
		return orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(value);
	}




}
