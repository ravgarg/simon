package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.util.localization.Localization;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.facades.shop.data.ShopData;


/**
 * Extended ShopDataPopulator for populating ShopData.
 *
 */
public class ExtShopDataPopulator implements Populator<ShopModel, ShopData>
{

	private static final String TEXT_SHIPPING_RETURN_RETURN_METHOD = "text.shipping.return.returnMethod";
	private static final String TEXT_SHIPPING_RETURN_RETURN_COST = "text.shipping.return.returnCost";
	private static final String TEXT_SHIPPING_RETURN_RETURN_PERIOD = "text.shipping.return.returnPeriod";
	private static final String TEXT_SHIPPING_RETURNS_PRIVACY_INFORMATION = "text.shipping.returns.privacy.information";

	/**
	 * Method to populate ShopData.
	 *
	 * @param shopModel
	 * @param shopData
	 *
	 * @throws ConversionException
	 */
	public void populate(final ShopModel shopModel, final ShopData shopData) throws ConversionException
	{
		shopData.setShippingTitle(shopModel.getShippingTitle());
		shopData.setShippingMessage(shopModel.getShippingMessage());
		shopData.setShippingMethod1(shopModel.getShippingMethodOneName());
		shopData.setShippingMethod1DeliverySLA(shopModel.getShippingMethodOneDelivery());
		shopData.setShippingMethod2(shopModel.getShippingMethodTwoName());
		shopData.setShippingMethod2DeliverySLA(shopModel.getShippingMethodTwoDelivery());
		shopData.setReturnPolicy(shopModel.getReturnPolicyHeading());
		shopData.setReturnPolicyMessage(shopModel.getReturnPolicy());
		shopData.setRetailerName(shopModel.getName());
		shopData.setReturnPeriod(shopModel.getReturnPeriodMessage());
		shopData.setReturnCost(shopModel.getReturnCost());
		shopData.setReturnMethod(shopModel.getMethod());
		shopData.setPrivacyPolicy(shopModel.getPrivacyPolicyHeading());
		shopData.setPrivacyPolicyMessage(shopModel.getPrivacyPolicy());
		shopData.setPricingMessage(shopModel.getPricingMessage());
		shopData.setLabelTextShippingAndReturns(getLocalizedString(TEXT_SHIPPING_RETURNS_PRIVACY_INFORMATION));
		shopData.setLabelTextReturnPeriod(getLocalizedString(TEXT_SHIPPING_RETURN_RETURN_PERIOD));
		shopData.setLabelTextCost(getLocalizedString(TEXT_SHIPPING_RETURN_RETURN_COST));
		shopData.setLabelTextMethod(getLocalizedString(TEXT_SHIPPING_RETURN_RETURN_METHOD));
	}

	protected String getLocalizedString(final String key)
	{
		return Localization.getLocalizedString(key);
	}
}
