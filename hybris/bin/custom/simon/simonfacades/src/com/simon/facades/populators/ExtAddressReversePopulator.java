package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * This populator class added to show email of the user in Address model.It extends the {@link AddressReversePopulator}.
 */
public class ExtAddressReversePopulator extends AddressReversePopulator
{
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		super.populate(addressData, addressModel);
		addressModel.setEmail(addressData.getEmail());
	}
}
