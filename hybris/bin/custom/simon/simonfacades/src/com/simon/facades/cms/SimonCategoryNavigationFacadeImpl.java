package com.simon.facades.cms;

import de.hybris.platform.acceleratorcms.model.components.CategoryNavigationComponentModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.core.resolver.ExtProductModelUrlResolver;
import com.simon.core.services.SimonCategoryNavigationService;
import com.simon.facades.constants.SimonFacadesConstants;



/**
 * Simon Category Navigation facade class implements {@link SimonCategoryNavigationFacade}. Service is responsible for
 * getting information for the L3 sub categories.
 */
public class SimonCategoryNavigationFacadeImpl implements SimonCategoryNavigationFacade
{
	@Resource
	private SimonCategoryNavigationService simonCategoryNavigationService;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	private Converter<PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel, PromoBanner2ClickableImageWithTextAndSingleCTAComponentData> simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter;

	private String componentBelongsTo;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private Converter<CategoryModel, CategoryData> cmsCategoryDataConverter;

	@Resource
	private ExtProductModelUrlResolver extProductModelUrlResolver;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Map<NavigationDTO, List<NavigationDTO>>> getNavigationNodes(final CategoryNavigationComponentModel component)
	{
		componentBelongsTo = component.getNavigationPageType().getCode();
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationNodes = new ArrayList<>();
		final CMSNavigationNodeModel cmsNavigationNode = component.getNavigationNode();
		if (cmsNavigationNode.isVisible())
		{
			final List<CMSNavigationNodeModel> navigationNodesL1 = cmsNavigationNode.getChildren();
			for (final CMSNavigationNodeModel navigationNodeL1 : navigationNodesL1)
			{
				if (navigationNodeL1.isVisible())
				{
					final Map<NavigationDTO, List<NavigationDTO>> navigationNodeMap = new HashMap<>();

					final NavigationDTO navigationDtoL1 = new NavigationDTO();

					updateURLInNavigationDTO(navigationNodeL1, navigationDtoL1);

					if (navigationNodeL1.getPromoBanner2() != null)
					{
						navigationDtoL1.setPromoBanner(getSimonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter()
								.convert(navigationNodeL1.getPromoBanner2()));
					}
					navigationDtoL1.setName(navigationNodeL1.getName());
					navigationDtoL1.setUid(navigationNodeL1.getUid());
					navigationDtoL1.setIsLockVisible(navigationNodeL1.isIsLockVisible());

					navigationNodeMap.put(navigationDtoL1, getChildNodes(navigationNodeL1));

					navigationNodes.add(navigationNodeMap);
				}
			}
		}
		return navigationNodes;

	}

	/**
	 * This method fetches the URL either from category code or URL attribute from NavigationNode
	 *
	 * @param navigationNode
	 *           The source node from whom URL is to be fetched
	 * @param navigationDtoL1
	 * @return string URL
	 */
	private void updateURLInNavigationDTO(final CMSNavigationNodeModel navigationNode, final NavigationDTO navigationDtoL1)
	{
		String url = StringUtils.EMPTY;
		String target = StringUtils.EMPTY;
		for (final CMSNavigationEntryModel entry : navigationNode.getEntries())
		{
			final CMSLinkComponentModel cmsLinkComponentModel = (CMSLinkComponentModel) entry.getItem();

			url = getUrlFromCMSLinkComponentModel(cmsLinkComponentModel);

			if (null != cmsLinkComponentModel.getTarget())
			{
				target = cmsLinkComponentModel.getTarget().getCode();
			}

		}
		navigationDtoL1.setTarget(target);
		navigationDtoL1.setUrl(url);
	}

	/**
	 * @method getUrlFromCMSLinkComponentModel
	 * @param cmsLinkComponentModel
	 * @return
	 */
	private String getUrlFromCMSLinkComponentModel(final CMSLinkComponentModel cmsLinkComponentModel)
	{
		String url = StringUtils.EMPTY;
		if (null != cmsLinkComponentModel)
		{
			final ContentPageModel contentPage = cmsLinkComponentModel.getContentPage();
			if (null != contentPage)
			{
				url = contentPage.getLabel();
			}
			else if (null != cmsLinkComponentModel.getCategory())
			{
				url = extCategoryModelUrlResolver.resolveCategoryPath(cmsLinkComponentModel.getCategory(), url);
			}
			else if (null != cmsLinkComponentModel.getProduct())
			{
				url = extProductModelUrlResolver.resolve(cmsLinkComponentModel.getProduct());
			}
			else
			{
				url = cmsLinkComponentModel.getUrl();
			}
		}
		return url;
	}

	/**
	 * Method to fetch level 2 nodes and level 3 sub categories of dynamic categories which contains products.
	 *
	 * Dynamic behaviour is checked only at L2 level.
	 *
	 * Navigation Node will only be added if it has corresponding Entry associated. If categoryModel is present and
	 * dynamic is set to true, L3 categories will be fetched from DB. If categoryModel is present and dynamic is set to
	 * false, its URl will be treated as category hierarchy path. Else, it is considered as manual authored static link
	 * in URL.
	 *
	 * If categoryModel is not specified, URL is treated as link, else URL should consist of complete category code
	 * hierarchy from L1 to categoryModel specified
	 *
	 * @param navigationNodeL1
	 * @return List of Navigation DTO
	 */
	private List<NavigationDTO> getChildNodes(final CMSNavigationNodeModel navigationNodeL1)
	{
		final List<NavigationDTO> navigationNodes = new ArrayList<>();

		for (final CMSNavigationNodeModel navigationNodeL2 : navigationNodeL1.getChildren())
		{
			if (navigationNodeL2.isVisible())
			{
				final NavigationDTO navigationDTOL2 = new NavigationDTO();
				navigationDTOL2.setName(navigationNodeL2.getName());
				navigationDTOL2.setUid(navigationNodeL2.getUid());
				navigationDTOL2.setIsL2(true);
				navigationDTOL2.setIsLockVisible(navigationNodeL2.isIsLockVisible());
				final List<CMSNavigationEntryModel> cmsEntryModelList = navigationNodeL2.getEntries();
				navigationNodes.add(navigationDTOL2);
				if (CollectionUtils.isNotEmpty(cmsEntryModelList))
				{
					final CMSLinkComponentModel cmsLinkComponentModel = (CMSLinkComponentModel) cmsEntryModelList.get(0).getItem();
					final String url = getUrlFromCMSLinkComponentModel(cmsLinkComponentModel);
					final CategoryModel categorModel = cmsLinkComponentModel.getCategory();
					//Global Navigation case
					if (categorModel != null && navigationNodeL2.isDynamicCategory())
					{
						setL2NavNodeForDynamicCategory(navigationNodes, navigationDTOL2, url, categorModel,
								navigationNodeL2.isDynamicCategory());
					}
					else
					{
						//ManuallyAuthored Case
						if (categorModel != null && !navigationNodeL2.isDynamicCategory())
						{
							navigationDTOL2.setUrl(extCategoryModelUrlResolver.resolveCategoryPath(categorModel, url));
						}
						else
						{
							navigationDTOL2.setUrl(url);
						}
						getL3NodesFromL2(navigationNodes, navigationNodeL2, navigationDTOL2);
					}
				}
			}
		}
		return navigationNodes;
	}

	/**
	 * Sets the L2 nav node for dynamic category.
	 *
	 * @param navigationNodes
	 *           the navigation nodes
	 * @param navigationDTOL2
	 *           the navigation DTO L2 is used to save the url and name of the navigation node
	 * @param url
	 *           the url
	 * @param categorModel
	 *           the category model
	 */
	private void setL2NavNodeForDynamicCategory(final List<NavigationDTO> navigationNodes, final NavigationDTO navigationDTOL2,
			final String url, final CategoryModel categorModel, final boolean isDynamicCategory)
	{
		navigationDTOL2.setUrl(extCategoryModelUrlResolver.resolveCategoryPathForChildNode(categorModel, url));
		getChildNodes(navigationNodes, simonCategoryNavigationService.getSubCategories(categorModel.getCode()), navigationDTOL2,
				url, isDynamicCategory);
	}

	private void getL3NodesFromL2(final List<NavigationDTO> navigationNodes, final CMSNavigationNodeModel navigationNodeL2,
			final NavigationDTO navigationDTOL2)
	{
		if (CollectionUtils.isNotEmpty(navigationNodeL2.getChildren()))
		{
			final List<NavigationDTO> l3NavigationNodes = getManuallyAuthoredL3Nodes(navigationNodeL2);

			navigationDTOL2.setL2Size(l3NavigationNodes.size());
			navigationNodes.addAll(l3NavigationNodes);
		}
	}

	/**
	 * This method checks whether current component belongs to CLP or not. If it belongs to CLP page, "View ALl" node is
	 * added to start of L3 nodes list, else at last
	 *
	 * @param navigationNodes
	 * @param navigationDTO
	 */
	private void setViewAllNodeInNavigationList(final List<NavigationDTO> navigationNodes, final NavigationDTO navigationDTO)
	{
		if (componentBelongsTo.equalsIgnoreCase(SimonCoreConstants.CLP_PAGE_ID))
		{
			navigationNodes.add(0, navigationDTO);
		}
		else
		{
			navigationNodes.add(navigationDTO);
		}
	}

	/**
	 * This method fetches the children of L2 Node and set into List containing L3 links upto the configured limit
	 *
	 * @param navigationDTOL2
	 * @return List of Navigation DTO containing L3 links upto the configured limit
	 */
	private List<NavigationDTO> getManuallyAuthoredL3Nodes(final CMSNavigationNodeModel navigationNodeL2)
	{
		final List<NavigationDTO> l3NavigationNodes = new ArrayList<>();
		final int countOfL3CategoriesToDisplay = getL3CategoriesCountToDisplay();
		int nodeCountL3 = 1;
		for (final CMSNavigationNodeModel navigationNodeL3 : navigationNodeL2.getChildren())
		{
			if (nodeCountL3 <= countOfL3CategoriesToDisplay && navigationNodeL3.isVisible())
			{
				final NavigationDTO navigationDTOL3 = new NavigationDTO();
				updateURLInNavigationDTO(navigationNodeL3, navigationDTOL3);
				navigationDTOL3.setName(navigationNodeL3.getName());
				navigationDTOL3.setUid(navigationNodeL3.getUid());
				navigationDTOL3.setIsLockVisible(navigationNodeL3.isIsLockVisible());
				l3NavigationNodes.add(navigationDTOL3);
				nodeCountL3++;
			}
		}
		return l3NavigationNodes;
	}

	/**
	 * q=relevance This method returns the configured limit to display L3 categories under L2
	 *
	 */
	private int getL3CategoriesCountToDisplay()
	{
		return configurationService.getConfiguration().getInt(SimonFacadesConstants.NODE_COUNT_NAVIGATION_SUBCATEGORIES);
	}

	/**
	 * Method to populate navigation DTO for all the sub categories of level 2 dynamic categories.
	 *
	 * @param navigationNodes
	 * @param navigationNodesL3
	 * @param navigationDTOL2
	 */
	private void getChildNodes(final List<NavigationDTO> navigationNodes, final List<CategoryModel> navigationNodesL3,
			final NavigationDTO navigationDTOL2, final String url, final boolean isDynamicCategory)
	{
		int nodeCount = 1;

		for (final CategoryModel categoryL3 : navigationNodesL3)
		{
			getNavigationDTOL3(navigationNodes, categoryL3, url);
			final int totalNodes = getL3CategoriesCountToDisplay();
			if (nodeCount == totalNodes)
			{
				break;
			}
			nodeCount++;
		}
		navigationDTOL2.setL2Size(navigationNodesL3.size());
		if (CollectionUtils.isNotEmpty(navigationNodesL3) && isDynamicCategory)
		{
			getViewAllNavigationNode(navigationNodes, navigationDTOL2);
		}
	}



	/**
	 * Method is used to add View all node in the level 3 subcategories DTO
	 *
	 * @param navigationNodes
	 * @param navigationDTOL2
	 */
	private void getViewAllNavigationNode(final List<NavigationDTO> navigationNodes, final NavigationDTO navigationDTOL2)
	{
		final NavigationDTO navigationDTOL3 = new NavigationDTO();
		navigationDTOL3.setUrl(navigationDTOL2.getUrl());
		navigationDTOL3.setName(SimonFacadesConstants.TEXT_NAVIGATION_VIEW_ALL);
		setViewAllNodeInNavigationList(navigationNodes, navigationDTOL3);
	}



	/**
	 * Method is used to get Level 3 subcategories and set name,uid and URL of sam ein Navigation DTO.
	 *
	 * @param navigationNodes
	 * @param categoryL3
	 */
	private void getNavigationDTOL3(final List<NavigationDTO> navigationNodes, final CategoryModel categoryL3, final String url)
	{
		final NavigationDTO navigationDTOL3 = new NavigationDTO();
		final String catCodeURL = url + SimonCoreConstants.UNDERSCORE + categoryL3.getCode();
		navigationDTOL3.setUrl(extCategoryModelUrlResolver.resolveCategoryPathForChildNode(categoryL3, catCodeURL));
		navigationDTOL3.setName(categoryL3.getName());
		navigationDTOL3.setUid(categoryL3.getCode());
		navigationNodes.add(navigationDTOL3);
	}

	/**
	 * @return
	 */
	public Converter<PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel, PromoBanner2ClickableImageWithTextAndSingleCTAComponentData> getSimonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter()
	{
		return simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter;
	}



	/**
	 * @param simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter
	 */
	public void setSimonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter(
			final Converter<PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel, PromoBanner2ClickableImageWithTextAndSingleCTAComponentData> simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter)
	{
		this.simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter = simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Collection<CategoryData>> getCategoryforMySize()
	{

		final String sizeRootCategory = configurationService.getConfiguration()
				.getString(SimonFacadesConstants.MY_SIZE_ROOT_CATEGORY);
		final CategoryModel rootCategory = categoryService.getCategoryForCode(sizeRootCategory);
		final Map<String, Collection<CategoryData>> categorySizeMap = new HashMap<>();

		final Collection<CategoryModel> sizeCategoryList = rootCategory.getAllSubcategories();

		final String sizeBaseCategory = configurationService.getConfiguration()
				.getString(SimonFacadesConstants.MY_SIZE_BASE_CATEGORY);

		final List<String> sizeBaseCategoryList = new ArrayList<>(Arrays.asList(sizeBaseCategory.split("\\s*,\\s*")));

		for (final CategoryModel categoryModel : sizeCategoryList)
		{
			if (sizeBaseCategoryList.contains(categoryModel.getCode()))
			{
				categorySizeMap.put(categoryModel.getName(), getSubCategoryList(categoryModel));
			}
		}
		return categorySizeMap;

	}

	private Collection<CategoryData> getSubCategoryList(final CategoryModel categoryModel)
	{
		final List<CategoryModel> sizeSubCategoryList = new ArrayList<CategoryModel>(categoryModel.getAllSubcategories());
		if (CollectionUtils.isNotEmpty(sizeSubCategoryList))
		{
			sizeSubCategoryList.sort((c1, c2) -> c1.getName().compareTo(c2.getName()));
		}
		return cmsCategoryDataConverter.convertAll(sizeSubCategoryList);
	}
}
