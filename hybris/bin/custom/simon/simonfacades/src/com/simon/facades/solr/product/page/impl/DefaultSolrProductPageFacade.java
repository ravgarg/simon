package com.simon.facades.solr.product.page.impl;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.springframework.util.Assert;

import com.simon.core.dto.ProductDetailsData;
import com.simon.core.services.solr.product.page.SolrProductPageService;
import com.simon.facades.solr.product.page.SolrProductPageFacade;


/**
 * This Class is accessed from {@link SolrProductPageController} is used to fetch product data from solr. It calls
 * {@link SolrProductPageService} to make solr search and fetch {@link ProductDetailsData}.
 */
public class DefaultSolrProductPageFacade implements SolrProductPageFacade
{
	/** The product page search service. */
	private SolrProductPageService<SolrSearchQueryData, SearchResultValueData, ProductDetailsData> productPageSearchService;

	/**
	 * fetches the {@link ProductDetailsData} from solr using productPageSearchService.
	 *
	 * @param searchState
	 *           of type {@link SearchStateData} is used to fetch the Query which is converted into
	 *           {@link SearchStateData}
	 * @param pageableData
	 *           of type {@link PageableData} is passed to service to fetch the product details.
	 * @return the {@link ProductDetailsData} which represents the product details DTO.
	 */
	@Override
	public ProductDetailsData getProductDetails(final SearchStateData searchState, final PageableData pageableData)
	{
		Assert.notNull(searchState, "SearchStateData must not be null.");

		return getThreadContextService()
				.executeInContext(new ThreadContextService.Executor<ProductDetailsData, ThreadContextService.Nothing>()
				{
					@Override
					public ProductDetailsData execute()
					{
						return getProductDetailsOfDecodedState(searchState, pageableData);
					}


				});
	}

	/**
	 * @param searchState
	 * @param pageableData
	 * @return
	 */
	protected ProductDetailsData getProductDetailsOfDecodedState(final SearchStateData searchState,
			final PageableData pageableData)
	{
		return productPageSearchService.getProductDetails(decodeState(searchState, null), pageableData);
	}


	/**
	 * Decodes the {@link SearchStateData} to get the {@link SolrSearchQueryData}.
	 *
	 * @param searchState
	 *           of type {@link SearchStateData} is used to fetch the Query which is converted into
	 *           {@link SearchStateData}
	 * @param categoryCode
	 *           of type String is used to set the category code in {@link SolrSearchQueryData} if not null
	 * @return the {@link SolrSearchQueryData} converted from {@link SearchStateData}
	 */
	protected SolrSearchQueryData decodeState(final SearchStateData searchState, final String categoryCode)
	{
		final SolrSearchQueryData searchQueryData = getSearchQueryDecoder().convert(searchState.getQuery());
		if (categoryCode != null)
		{
			searchQueryData.setCategoryCode(categoryCode);
		}

		return searchQueryData;
	}


	/**
	 * Gets the product page search service.
	 *
	 * @return the product page search service
	 */
	public SolrProductPageService<SolrSearchQueryData, SearchResultValueData, ProductDetailsData> getProductPageSearchService()
	{
		return productPageSearchService;
	}

	/**
	 * Sets the product page search service.
	 *
	 * @param productPageSearchService
	 *           the product page search service
	 */
	public void setProductPageSearchService(
			final SolrProductPageService<SolrSearchQueryData, SearchResultValueData, ProductDetailsData> productPageSearchService)
	{
		this.productPageSearchService = productPageSearchService;
	}

	/** The thread context service. */
	private ThreadContextService threadContextService;

	/** The search query decoder. */
	private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;

	/**
	 * Gets the search query decoder.
	 *
	 * @return the search query decoder
	 */
	public Converter<SearchQueryData, SolrSearchQueryData> getSearchQueryDecoder()
	{
		return searchQueryDecoder;
	}

	/**
	 * Sets the search query decoder.
	 *
	 * @param searchQueryDecoder
	 *           the search query decoder
	 */
	public void setSearchQueryDecoder(final Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder)
	{
		this.searchQueryDecoder = searchQueryDecoder;
	}



	/**
	 * Gets the thread context service.
	 *
	 * @return the thread context service
	 */
	public ThreadContextService getThreadContextService()
	{
		return threadContextService;
	}

	/**
	 * Sets the thread context service.
	 *
	 * @param threadContextService
	 *           the new thread context service
	 */
	public void setThreadContextService(final ThreadContextService threadContextService)
	{
		this.threadContextService = threadContextService;
	}

}
