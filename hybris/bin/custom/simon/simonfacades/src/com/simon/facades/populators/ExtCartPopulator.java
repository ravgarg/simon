package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * Converter implementation for {@link de.hybris.platform.core.model.order.CartModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.CartData} as target type.
 *
 */
public class ExtCartPopulator extends CartPopulator<CartData>
{
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter;

	/**
	 * This method will populate the cart data for cart page.In this it is populate the common properties, every type of
	 * total and all the promotion.
	 */
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		addCommon(source, target);
		addTotals(source, target);
		addPromotions(source, target);
		addSavedCartData(source, target);
		target.setGuid(source.getGuid());
		target.setTotalUnitCount(calcTotalUnitCount(source));
		if (source.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel extPaymentInfoModel = (CreditCardPaymentInfoModel) source.getPaymentInfo();
			final CCPaymentInfoData paymentInfoData = getExtPaymentInfoConverter().convert(extPaymentInfoModel);
			target.setPaymentInfo(paymentInfoData);
		}
	}

	public Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getExtPaymentInfoConverter()
	{
		return extPaymentInfoConverter;
	}

	public void setExtPaymentInfoConverter(final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter)
	{
		this.extPaymentInfoConverter = extPaymentInfoConverter;
	}
}
