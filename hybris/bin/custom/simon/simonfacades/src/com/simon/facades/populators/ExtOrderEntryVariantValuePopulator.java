package com.simon.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.simon.core.constants.SimonCoreConstants;


/**
 * Converter implementation for {@link de.hybris.platform.core.model.order.AbstractOrderEntryModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.OrderEntryData} as target type.
 */
public class ExtOrderEntryVariantValuePopulator<S extends AbstractOrderEntryModel, T extends OrderEntryData>
		implements Populator<S, T>
{

	@Resource
	private CategoryService categoryService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
	{
		getVariantValues(source.getProduct(), target.getProduct());
	}


	/**
	 * get the Variant Category and Variant Value Category from product and base product model Set the Map<String,String>
	 * to variant values e.g <Color,Red> , <Size,Small>
	 *
	 * @param variant
	 * @param productData
	 */
	protected void getVariantValues(final ProductModel variant, final ProductData productData)
	{
		final Map<String, String> variantValues = new LinkedHashMap<>();
		final ProductModel productModel = ((GenericVariantProductModel) variant).getBaseProduct();
		final Collection<CategoryModel> categoryModels = productModel.getSupercategories();

		final Set<String> allPaths = new HashSet<>();
		final List<CategoryModel> variantcategories = categoryModels.stream()
				.filter(categoryModel -> categoryModel instanceof VariantCategoryModel).collect(Collectors.toList());
		for (final CategoryModel categoryModel : variantcategories)
		{
			final Collection<List<CategoryModel>> pathsForCategory = categoryService.getPathsForCategory(categoryModel);
			if (pathsForCategory != null)
			{
				for (final List<CategoryModel> categoryPath : pathsForCategory)
				{
					accumulateCategoryPaths(categoryPath, allPaths);
				}
			}
		}
		final List<String> longestPath = getLongestPathForCategory(allPaths);

		final Collection<CategoryModel> superCategories = variant.getSupercategories();
		for (final String categoryCode : longestPath)
		{
			accumulateCategoryNameInMap(variantValues, superCategories, categoryCode, productModel);
		}
		productData.setVariantValues(variantValues);
	}

	/**
	 * create the accumulate category path from list<categoryModel> and save the path in output.
	 *
	 * @param categoryPath
	 * @param output
	 */
	private void accumulateCategoryPaths(final List<CategoryModel> categoryPath, final Set<String> output)
	{
		final StringBuilder accumulator = new StringBuilder();
		for (final CategoryModel category : categoryPath)
		{
			if (category instanceof ClassificationClassModel)
			{
				break;
			}
			accumulator.append('/').append(category.getCode());
			output.add(accumulator.toString());
		}
	}

	/**
	 * Get the Longest Path from all category path which provided in input category path and return the longest path list
	 * e.g. if input is list of color,color/size,color/size/style then return color,Size,style in list categoryPath
	 * cannot be null
	 *
	 * @param categoryPath
	 * @return categoryPath
	 */
	private List<String> getLongestPathForCategory(final Set<String> categoryPath)
	{
		List<String> orderedList;
		final Optional<String> maxLength = categoryPath.stream().sorted((a, b) -> a.length() > b.length() ? -1 : 1).findFirst();
		if (maxLength.isPresent())
		{
			final String order = maxLength.get();
			final String[] hierarchyList = order.split("/");
			orderedList = Arrays.asList(hierarchyList).subList(SimonCoreConstants.FIRST_INT, hierarchyList.length);
		}
		else
		{
			orderedList = Collections.emptyList();
		}
		return orderedList;
	}

	/**
	 * Create Map of Variant Category Name with Variant Category Value Name . This values can be populated from
	 * variantSupercategory value and category Model
	 *
	 * @param variantValues
	 * @param variantValueSuperCategories
	 * @param categoryCode
	 * @param productModel
	 */
	private void accumulateCategoryNameInMap(final Map<String, String> variantValues,
			final Collection<CategoryModel> variantValueSuperCategories, final String categoryCode, final ProductModel productModel)
	{
		final CategoryModel categoryModel = categoryService.getCategoryForCode(productModel.getCatalogVersion(), categoryCode);

		for (final CategoryModel variantValueCategoryModel : variantValueSuperCategories)
		{
			if (variantValueCategoryModel instanceof VariantValueCategoryModel)
			{
				final VariantCategoryModel variantCategoryModel = ((VariantCategoryModel) variantValueCategoryModel
						.getSupercategories().get(0));
				if (variantCategoryModel.getCode().equalsIgnoreCase(categoryCode))
				{
					variantValues.put(categoryModel.getName(Locale.ENGLISH), variantValueCategoryModel.getName(Locale.ENGLISH));
					break;
				}
			}

		}
	}
}
