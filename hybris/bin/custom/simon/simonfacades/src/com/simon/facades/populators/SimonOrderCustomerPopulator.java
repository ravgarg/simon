package com.simon.facades.populators;

import static com.mirakl.client.core.internal.util.Preconditions.checkArgument;
import static java.lang.String.format;
import static org.apache.commons.lang.StringUtils.isNotBlank;

import com.mirakl.client.mmp.domain.order.MiraklOrderCustomer;
import com.mirakl.hybris.core.order.populators.MiraklOrderCustomerPopulator;

import de.hybris.platform.core.model.user.CustomerModel;

public class SimonOrderCustomerPopulator extends MiraklOrderCustomerPopulator {
	
	protected void setCustomerName(MiraklOrderCustomer miraklOrderCustomer, CustomerModel customer) {
		    String[] customerName = customerNameStrategy.splitName(customer.getName());

		    checkArgument(isNotBlank(customerName[0]), format("No first name found for customer with uid [%s]", customer.getUid()));
		   
			if(customerName.length >1 && org.springframework.util.StringUtils.isEmpty(customerName[1]) ){
				customerName[1] = customerName[0];
			}
			
			miraklOrderCustomer.setFirstname(customerName[0]);
		    miraklOrderCustomer.setLastname(customerName[1]);
	}

}
