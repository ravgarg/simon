package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * This populator is used to populate Integration layer UserDetailsDTO from the RegisterData entered from commerce
 * layer.
 */
public class RegisterUserPopulator implements Populator<RegisterData, UserRegisterUpdateRequestDTO>
{
	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	@Resource
	private Converter<MallData, UserCenterIdDTO> userCenterIdDataConverter;


	/**
	 * This method is used to populate Integration layer UserDetailsDTO from the RegisterData entered from commerce
	 * layer.
	 */
	@Override
	public void populate(final RegisterData source, final UserRegisterUpdateRequestDTO target)
	{
		target.setFirstName(source.getFirstName());
		target.setLastName(source.getLastName());
		target.setEmailAddress(StringUtils.lowerCase(source.getLogin()));
		target.setGender(userIntegrationUtils.getGenderCode(source.getGender()));
		target.setZipCode(source.getZipcode());
		target.setBirthYear(source.getBirthYear());
		target.setHouseholdIncome(source.getHouseholdIncomeCode());
		target.setCountry(getCountry(source));
		target.setCenterIds(getCenterIds(source));
		target.setPassword(source.getPassword());
		target.setMobileNumber(source.getMobileNumber());
		target.setBirthMonth(userIntegrationUtils.getBirthMonthCode(source.getBirthMonth()));

	}

	/**
	 * This method is used to get center Ids from RegisterData for UserDetailsDTO
	 *
	 * @param source
	 * @return
	 */
	private List<UserCenterIdDTO> getCenterIds(final RegisterData source)
	{
		final List<UserCenterIdDTO> centerIds = new ArrayList<>();
		centerIds.add(userCenterIdDataConverter.convert(source.getPrimaryMall()));
		final List<MallData> alternateMalls = source.getAlternateMalls();
		if (CollectionUtils.isNotEmpty(alternateMalls))
		{
			centerIds.addAll(userCenterIdDataConverter.convertAll(alternateMalls));
		}
		return centerIds;
	}

	/**
	 * this method is used to get external ID for country.
	 *
	 * @param source
	 * @return
	 */
	private String getCountry(final RegisterData source)
	{
		return commonI18NService.getCountry(source.getCountry()).getExternalId();
	}
}
