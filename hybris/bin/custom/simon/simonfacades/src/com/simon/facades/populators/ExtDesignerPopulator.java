package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;

import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DesignerData;


public class ExtDesignerPopulator implements Populator<DesignerModel, DesignerData>
{
	/**
	 * @param source
	 * @param target
	 */
	public void populate(final DesignerModel source, final DesignerData target)
	{
		target.setActive(source.isActive());
		target.setBannerUrl("designer?code=" + source.getCode());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		target.setName(source.getName());

	}

}
