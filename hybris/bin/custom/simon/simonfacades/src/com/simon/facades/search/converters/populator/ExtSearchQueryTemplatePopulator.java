package com.simon.facades.search.converters.populator;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.converters.populator.DefaultSearchQueryTemplatePopulator;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;


/**
 * The Class ExtSearchQueryTemplatePopulator.
 */
public class ExtSearchQueryTemplatePopulator extends DefaultSearchQueryTemplatePopulator
{

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final SolrSearchQueryTemplateModel source, final SearchQueryTemplate target) throws ConversionException
	{

		callSuperMethod(source, target);
		target.setWithinGroupSortProperties(source.getWithinGroupSortProperties());
	}


	/**
	 * Call super method.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	protected void callSuperMethod(final SolrSearchQueryTemplateModel source, final SearchQueryTemplate target)
			throws ConversionException
	{
		super.populate(source, target);
	}
}
