package com.simon.facades.populators;




import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.util.localization.Localization;

import java.util.List;

import javax.annotation.Resource;

import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.message.utils.ExtCustomMessageUtils;


/**
 * Populates {@link CartModificationData} with CartModel.Populating total item count of cart entries in cart
 * modification data.
 */
public class ExtCartModificationDataPopulator implements Populator<CartModel, CartModificationData>
{

	@Resource(name = "extCustomMessageUtils")
	private ExtCustomMessageUtils extCustomMessageUtils;


	/**
	 * Method populate is populating CartModificationData with total item count of cart and message labels for add to
	 * cart json object.
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final CartModel source, final CartModificationData target)
	{
		long itemCount = 0;
		final List<AbstractOrderEntryModel> entries = source.getEntries();
		for (final AbstractOrderEntryModel orderEntry : entries)
		{
			itemCount = itemCount + orderEntry.getQuantity();
		}

		target.setItemCount(itemCount);
		target.setMessageLabels(extCustomMessageUtils.setAddToCartMessages());
		if (target.getStatusCode().equals(CommerceCartModificationStatus.NO_STOCK))
		{
			target.setErrorMessage(setLocalizedString(SimonFacadesConstants.NO_STOCK_ERROR));
		}
		if (target.getStatusCode().equals(CommerceCartModificationStatus.LOW_STOCK))
		{
			target.setErrorMessage(setLocalizedString(SimonFacadesConstants.LOW_STOCK_ERROR));
		}
	}

	/**
	 * Method will set localized string for low stock and no stock error messages.
	 * 
	 * @param label
	 * @return String
	 */
	protected String setLocalizedString(final String label)
	{
		return Localization.getLocalizedString(label);
	}
}
