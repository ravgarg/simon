package com.simon.facades.populators;

import de.hybris.platform.basecommerce.enums.StockLevelStatus;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.impl.DefaultPriceService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.localization.Localization;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.util.CollectionUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.price.ExtPriceService;
import com.simon.facades.cart.data.RetailerInfoData;


/**
 * OOS Cart Entry will be merge into Cart Data product List. Converter implementation for
 * {@link de.hybris.platform.commercefacades.order.data.CartModificationData} as source and
 * {@link de.hybris.platform.commercefacades.order.data.CartData} as target type.
 */
public class ExtCartDataFromModificationPopulator implements Populator<CartModificationData, CartData>
{

	@Resource
	private ProductService productService;

	@Resource(name = "extRetailerConverter")
	private Converter<ShopModel, RetailerInfoData> extRetailerConverter;

	@Resource
	private DefaultPriceService defaultPriceService;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Resource
	private ExtPriceService priceService;

	/**
	 * set sales price , list price and base price after getting pricevalue from entry and set these value to list price
	 * and sales price as per FSD if list price is not zero sale price is base price and list price is list price. while
	 * if list price is zero sales price is zero and list price is base price.
	 *
	 * @param entry
	 * @param basePrice
	 * @throws CalculationException
	 */
	private void setPrice(final OrderEntryData entry, final ExtPriceValue extPriceValue, final double msrp)
	{
		final Map<DetailedPriceInfo, Object> priceDetail = priceService.getDetailedPriceInformation(msrp,
				extPriceValue.getListPrice(), extPriceValue.getValue());

		final PriceDataType priceType = PriceDataType.BUY;
		final String isoCode = extPriceValue.getCurrencyIso();
		if (extPriceValue.getListPrice() > 0)
		{
			final PriceData listValueData = priceDataFactory.create(priceType, BigDecimal.valueOf(extPriceValue.getListPrice()),
					isoCode);
			final PriceData saleValueData = priceDataFactory.create(priceType, BigDecimal.valueOf(extPriceValue.getValue()),
					isoCode);

			entry.setSaleValue(saleValueData);
			entry.setListValue(listValueData);
			listValueData.setStrikeOff((boolean) priceDetail.get(DetailedPriceInfo.IS_LP_STRIKEOFF));
		}
		else
		{
			final PriceData listValueData = priceDataFactory.create(priceType, BigDecimal.valueOf(extPriceValue.getValue()),
					isoCode);
			final PriceData saleValueData = priceDataFactory.create(priceType, BigDecimal.valueOf(0), isoCode);

			entry.setSaleValue(saleValueData);
			entry.setListValue(listValueData);
		}
		if (msrp > SimonCoreConstants.ZERO_DOUBLE)
		{
			final PriceData msrpValueData = priceDataFactory.create(priceType, BigDecimal.valueOf(msrp), isoCode);
			entry.setMsrpValue(msrpValueData);
			msrpValueData.setStrikeOff((boolean) priceDetail.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF));
		}
		if (priceDetail.containsKey(DetailedPriceInfo.PERCENTAGE_SAVING))
		{
			final String localizedString = getLocalizedString();
			entry.setSavingsMessage(priceDetail.get(DetailedPriceInfo.PERCENTAGE_SAVING) + localizedString);
		}
	}


	/**
	 * OOS Cart Entry will be merge into Cart Data product List. This method populate Retailer info Data and group the
	 * orders in group based on retailers and get the price for cart
	 *
	 * @param source
	 * @param target
	 * @throws ConversionException
	 */
	@Override
	public void populate(final CartModificationData source, final CartData target)
	{
		final ProductModel productModel = productService.getProductForCode(source.getEntry().getProduct().getCode());
		final ShopModel shopModel = productModel.getShop();
		final OrderEntryData entry = updateStock(source);
		RetailerInfoData retailerInfoData = null;
		List<RetailerInfoData> infoDatas = target.getRetailerInfoData();

		if (shopModel != null)
		{
			boolean retailerInformationFound = false;
			if (!CollectionUtils.isEmpty(infoDatas))
			{
				for (final RetailerInfoData currentRetailerInfoData : target.getRetailerInfoData())
				{
					if (currentRetailerInfoData.getRetailerId().equals(shopModel.getId()))
					{
						retailerInfoData = currentRetailerInfoData;
						retailerInformationFound = true;
						break;
					}
				}
			}
			else
			{
				infoDatas = new ArrayList<>();
			}

			if (!retailerInformationFound)
			{
				retailerInfoData = extRetailerConverter.convert(shopModel);
				retailerInfoData.setTotalUnitCount(0L);
				retailerInfoData.setProductDetails(new ArrayList<>());
				infoDatas.add(retailerInfoData);
				target.setRetailerInfoData(infoDatas);
			}

			setPriceValue(source, productModel);
			retailerInfoData.getProductDetails().add(0, entry);
		}

	}


	private OrderEntryData updateStock(final CartModificationData source)
	{
		final OrderEntryData entry = source.getEntry();
		final String status = source.getStatusCode();
		if (CommerceCartModificationStatus.UNAVAILABLE.equals(status) || CommerceCartModificationStatus.NO_STOCK.equals(status))
		{
			final StockData stock = entry.getProduct().getStock();
			stock.setStockLevel(0l);
			stock.setStockLevelStatus(StockLevelStatus.OUTOFSTOCK);
		}
		return entry;
	}

	/**
	 * Method to get localized label from locale properties
	 *
	 * @return String
	 */
	protected String getLocalizedString()
	{
		return Localization.getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
	}

	/**
	 * Set price Value for Order Entry
	 *
	 * @param source
	 * @param productModel
	 */
	private void setPriceValue(final CartModificationData source, final ProductModel productModel)
	{
		final List<PriceInformation> productPrice = defaultPriceService.getPriceInformationsForProduct(productModel);
		if (!CollectionUtils.isEmpty(productPrice))
		{
			final ExtPriceValue extPriceValue = (ExtPriceValue) productPrice.get(0).getPriceValue();
			setPrice(source.getEntry(), extPriceValue, null != productModel.getMsrp() ? productModel.getMsrp() : 0d);

		}
	}
}
