package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.simon.core.model.BackgroundImageItemModel;
import com.simon.core.model.ContentSlotClickableImageWithSingleCTAComponentModel;
import com.simon.facades.cms.BackgroundImageData;
import com.simon.facades.cms.CMSLinkComponentData;
import com.simon.facades.cms.ContentSlotClickableImageWithSingleCTAComponentData;


/**
 * Converter for ContentSlotClickableImageWithSingleCTAComponentModel as source and
 * ContentSlotClickableImageWithSingleCTAComponentData as target
 */
public class ContentSlotClickableImageComponentPopulator implements
		Populator<ContentSlotClickableImageWithSingleCTAComponentModel, ContentSlotClickableImageWithSingleCTAComponentData>
{
	@Resource(name = "simonBackgroundImageDataConverter")
	private Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter;


	@Override
	public void populate(final ContentSlotClickableImageWithSingleCTAComponentModel source,
			final ContentSlotClickableImageWithSingleCTAComponentData target)
	{
		target.setId(source.getUid());
		final CMSLinkComponentData cmsLink = new CMSLinkComponentData();
		if (source.getLink() != null)
		{
			cmsLink.setLinkName(source.getLink().getLinkName());
			cmsLink.setUrl(source.getLink().getUrl());
			cmsLink.setTarget(source.getLink().getTarget().getCode());
			target.setLink(cmsLink);
		}
		if (source.getBackgroundImage() != null)
		{
			target.setBackgroundImage(getSimonBackgroundImageDataConverter().convert(source.getBackgroundImage()));
		}
		if (source.getShape() != null)
		{
			target.setShape(source.getShape().getCode().toUpperCase());
		}
		target.setDisplayOnlyForMontrealMall(source.getDisplayOnlyForMontrealMall());
		target.setHideIfMontrealMall(source.getHideIfMontrealMall());
		target.setItemType(source.getItemtype());
		target.setName(source.getName());
	}

	/**
	 * @return simonBackgroundImageDataConverter
	 */
	public Converter<BackgroundImageItemModel, BackgroundImageData> getSimonBackgroundImageDataConverter()
	{
		return simonBackgroundImageDataConverter;
	}

	/**
	 * @param simonBackgroundImageDataConverter
	 */
	public void setSimonBackgroundImageDataConverter(
			final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter)
	{
		this.simonBackgroundImageDataConverter = simonBackgroundImageDataConverter;
	}
}
