package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.facades.email.order.ShippingDetailsData;


/**
 * Converts OrderData to EmailOrderData
 */
public class EmailOrderConfirmationDataPopulator implements Populator<OrderData, EmailOrderData>
{

	private static final String PRODUCT_CENCELLED = "Cancelled";
	private static final String RETAILER_CENCELLED = "Cancelled";

	@Resource(name = "emailOrderDetailsDataConverter")
	private Converter<OrderData, EmailOrderDetailsData> emailOrderDetailsDataConverter;
	@Resource(name = "emailRetailerDetailsDataConverter")
	private Converter<RetailerInfoData, RetailerDetailsData> emailRetailerDetailsDataConverter;
	@Resource(name = "emailShippingDetailsDataConverter")
	private Converter<AddressData, ShippingDetailsData> emailShippingDetailsDataConverter;
	@Resource(name = "emailOrderStatusDataConverter")
	private Converter<OrderData, EmailOrderData> emailOrderStatusDataConverter;
	@Resource(name = "emailProductDetailsDataConverter")
	private Converter<OrderEntryData, ProductDetailsData> emailProductDetailsDataConverter;


	/**
	 * populates the OrderData from the contents in EmailOrderData
	 */
	@Override
	public void populate(final OrderData source, final EmailOrderData target)
	{

		target.setEmailTo(source.getDeliveryAddress().getEmail());

		target.setOrderDetail(emailOrderDetailsDataConverter.convert(source));

		final List<RetailerDetailsData> retailerDetailsDatas = new ArrayList<>();
		retailerDetailsDatas.addAll(emailRetailerDetailsDataConverter.convertAll(source.getRetailerInfoData()));

		target.setRetailersDetail(populateRetailerDetailsData(source, retailerDetailsDatas));

		target.setShippingDetail(emailShippingDetailsDataConverter.convert(source.getDeliveryAddress()));

		emailOrderStatusDataConverter.convert(source, target);
	}

	/**
	 * @method populateRetailerDetailsData
	 * @param orderData
	 * @param retailerDetailsDatas
	 * @return
	 */
	private List<RetailerDetailsData> populateRetailerDetailsData(final OrderData orderData,
			final List<RetailerDetailsData> retailerDetailsDatas)
	{
		final List<RetailerDetailsData> detailsDatas = new ArrayList<>();
		for (final RetailerDetailsData retailerDetailsData : retailerDetailsDatas)
		{
			final List<RetailerInfoData> retailerInfoDatas = orderData.getRetailerInfoData();

			for (final RetailerInfoData retailerInfoData : retailerInfoDatas)
			{

				if (retailerInfoData.getRetailerId().equalsIgnoreCase(retailerDetailsData.getRetailerId()))
				{
					final List<OrderEntryData> orderEntryDataList = retailerInfoData.getProductDetails();
					if (CollectionUtils.isNotEmpty(orderEntryDataList))
					{
						// populate the product details
						retailerDetailsData.setProductsDetail(populateProductDetailsOnRetailerLevel(orderEntryDataList, orderData));

						// populate the retailer level status
						final String retailerStatus = populateRetailerStatus(retailerDetailsData, orderData);


						if (StringUtils.isNotEmpty(retailerStatus)
								&& retailerStatus.equalsIgnoreCase(CallBackOrderEntryStatus.CANCELLED.getCode()))
						{

							retailerDetailsData.setStatus(retailerStatus);
							retailerDetailsData.setSaved(StringUtils.EMPTY);
							retailerDetailsData.setShipping(StringUtils.EMPTY);
							retailerDetailsData.setSubTotal(StringUtils.EMPTY);
							retailerDetailsData.setTax(StringUtils.EMPTY);
							retailerDetailsData.setTotal(StringUtils.EMPTY);
							retailerDetailsData.setRetailerPromoMsg(StringUtils.EMPTY);

						}
						else
						{
							String savingPrice = StringUtils.EMPTY;
							final PriceData savingPriceData = retailerInfoData.getSaving();
							if (null != savingPriceData && StringUtils.isNotEmpty(savingPriceData.getFormattedValue()))
							{
								savingPrice = savingPriceData.getFormattedValue();
							}
							retailerDetailsData.setSaved(savingPrice);

							String shippingPrice = StringUtils.EMPTY;
							final PriceData shippingPriceData = retailerInfoData.getEstimatedShipping();
							if (null != shippingPriceData && StringUtils.isNotEmpty(shippingPriceData.getFormattedValue()))
							{
								shippingPrice = shippingPriceData.getFormattedValue();
							}
							retailerDetailsData.setShipping(shippingPrice);

							String subtotalPrice = StringUtils.EMPTY;
							final PriceData subtotalPriceData = retailerInfoData.getEstimatedSubtotal();
							if (null != subtotalPriceData && StringUtils.isNotEmpty(subtotalPriceData.getFormattedValue()))
							{
								subtotalPrice = subtotalPriceData.getFormattedValue();
							}
							retailerDetailsData.setSubTotal(subtotalPrice);

							String retailerTax = StringUtils.EMPTY;
							final PriceData retailerTaxPriceData = retailerInfoData.getEstimatedTax();
							if (null != retailerTaxPriceData && StringUtils.isNotEmpty(retailerTaxPriceData.getFormattedValue()))
							{
								retailerTax = retailerTaxPriceData.getFormattedValue();
							}
							retailerDetailsData.setTax(retailerTax);

							String retailerTotal = StringUtils.EMPTY;
							final PriceData retailerTotalPriceData = retailerInfoData.getEstimatedTotal();
							if (null != retailerTotalPriceData && StringUtils.isNotEmpty(retailerTotalPriceData.getFormattedValue()))
							{
								retailerTotal = retailerTotalPriceData.getFormattedValue();
							}
							retailerDetailsData.setTotal(retailerTotal);

							String retailerPromoMsg = StringUtils.EMPTY;
							final List<String> promotions = retailerInfoData.getPromoMessages();
							if (CollectionUtils.isNotEmpty(promotions) && StringUtils.isNotEmpty(promotions.get(0)))
							{
								retailerPromoMsg = promotions.get(0);
							}
							retailerDetailsData.setRetailerPromoMsg(retailerPromoMsg);
						}

					}

					detailsDatas.add(retailerDetailsData);
				}
			}
		}
		return detailsDatas;
	}


	/**
	 * @method populateProductDetailsOnRetailerLevel
	 * @param orderEntryDataList
	 * @return
	 */
	private List<ProductDetailsData> populateProductDetailsOnRetailerLevel(final List<OrderEntryData> orderEntryDataList,
			final OrderData orderData)
	{
		final List<ProductDetailsData> productDetailsDatas = new ArrayList<>();
		for (final OrderEntryData orderEntryData : orderEntryDataList)
		{
			final ProductDetailsData productDetailsData = emailProductDetailsDataConverter.convert(orderEntryData);
			if (null != orderData.getStatus() && orderData.getStatus().getCode().equalsIgnoreCase(OrderStatus.REJECTED.getCode()))
			{
				productDetailsData.setStatus(PRODUCT_CENCELLED);
			}
			else
			{
				productDetailsData.setStatus(populateProductStatusFromOrderEntry(orderEntryData));
			}
			productDetailsDatas.add(productDetailsData);
		}

		return productDetailsDatas;
	}

	/**
	 * @method populateProductStatusFromOrderEntry
	 * @param orderEntryData
	 * @return
	 */
	private String populateProductStatusFromOrderEntry(final OrderEntryData orderEntryData)
	{
		String orderLineItemStatus = StringUtils.EMPTY;
		if (StringUtils.isNotEmpty(orderEntryData.getCallBackOrderEntryStatus())
				&& orderEntryData.getCallBackOrderEntryStatus().equalsIgnoreCase(CallBackOrderEntryStatus.CANCELLED.getCode()))
		{
			orderLineItemStatus = PRODUCT_CENCELLED;
		}
		return orderLineItemStatus;
	}

	/**
	 * @method populateRetailerStatus
	 * @param retailerDetailsData
	 * @return String
	 */
	private String populateRetailerStatus(final RetailerDetailsData retailerDetailsData, final OrderData orderData)
	{
		String retailerStatus = StringUtils.EMPTY;
		if (null != orderData.getStatus() && orderData.getStatus().getCode().equalsIgnoreCase(OrderStatus.REJECTED.getCode()))
		{
			retailerStatus = RETAILER_CENCELLED;
		}
		else
		{
			final List<ProductDetailsData> productDetailsDataList = retailerDetailsData.getProductsDetail();
			if (CollectionUtils.isNotEmpty(productDetailsDataList))
			{
				retailerStatus = populateRetailerStatusByProductStatus(productDetailsDataList);
			}
		}
		return retailerStatus;
	}

	/**
	 * @method populateRetailerStatusByProductStatus
	 * @param productDetailsDataList
	 * @return String
	 */
	private String populateRetailerStatusByProductStatus(final List<ProductDetailsData> productDetailsDataList)
	{
		boolean isProducutCancelledForRetailer = false;
		String retailerLevelStatus = StringUtils.EMPTY;
		for (final ProductDetailsData productDetailsData : productDetailsDataList)
		{
			if (StringUtils.isNotEmpty(productDetailsData.getStatus())
					&& productDetailsData.getStatus().equalsIgnoreCase(PRODUCT_CENCELLED))
			{
				isProducutCancelledForRetailer = true;
			}
			else
			{
				isProducutCancelledForRetailer = false;
				break;
			}
		}

		if (isProducutCancelledForRetailer)
		{
			retailerLevelStatus = RETAILER_CENCELLED;
		}

		return retailerLevelStatus;
	}

}
