/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * Populates {@link ProductData} with genders
 */
public class SimonCartPopulator implements Populator<CartModel, CartData>
{
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter;


	@Override
	public void populate(final CartModel source, final CartData target)
	{
		//set the payment info
		if (source.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel extPaymentInfoModel = (CreditCardPaymentInfoModel) source.getPaymentInfo();
			final CCPaymentInfoData paymentInfoData = getExtPaymentInfoConverter().convert(extPaymentInfoModel);
			target.setPaymentInfo(paymentInfoData);
		}
	}


	public Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getExtPaymentInfoConverter()
	{
		return extPaymentInfoConverter;
	}


	public void setExtPaymentInfoConverter(final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter)
	{
		this.extPaymentInfoConverter = extPaymentInfoConverter;
	}




}
