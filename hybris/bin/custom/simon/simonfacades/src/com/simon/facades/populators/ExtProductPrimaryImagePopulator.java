package com.simon.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.util.CommonUtils;


public class ExtProductPrimaryImagePopulator implements Populator<ProductModel, ProductData>
{

	private ModelService modelService;

	protected ModelService getModelService()
	{
		return modelService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		final List<MediaContainerModel> primaryImageMediaContainer = getPrimaryImageMediaContainer(productModel);
		final List<ImageData> imageList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(primaryImageMediaContainer)
				&& CollectionUtils.isNotEmpty(primaryImageMediaContainer.get(0).getMedias()))
		{
			for (final MediaModel media : primaryImageMediaContainer.get(0).getMedias())
			{
				if (null != media.getMediaFormat()
						&& SimonCoreConstants.MEDIA_FORMAT_CART.equals(media.getMediaFormat().getName(Locale.ENGLISH)))
				{
					populateImageData(productModel.getName(), imageList, media);
					break;
				}
			}

			productData.setImages(imageList);
		}
	}

	private void populateImageData(final String name, final List<ImageData> imageList, final MediaModel media)
	{
		final ImageData imageData = new ImageData();
		imageData.setFormat("thumbnail");
		imageData.setImageType(ImageDataType.PRIMARY);
		imageData.setUrl(media.getURL());

		final String altText = null != media.getAltText() ? media.getAltText() : name;
		imageData.setAltText(CommonUtils.replaceAsciiWithHtmlCode(altText));
		imageList.add(imageData);
	}

	protected List<MediaContainerModel> getPrimaryImageMediaContainer(final ProductModel productModel)
	{

		return getModelService().getAttributeValue(productModel, ProductModel.GALLERYIMAGES);
	}
}
