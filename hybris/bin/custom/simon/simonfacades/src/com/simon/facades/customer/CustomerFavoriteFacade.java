package com.simon.facades.customer;

/**
 * This is the Customer Favorite Facade. It is having all favorite related methods.
 */
public interface CustomerFavoriteFacade
{
	/**
	 * This method sets the Favorite data is session.
	 *
	 * @param favType
	 * @param favName
	 * @param favId
	 */
	public void setFavoriteDataInSession(final String favType, final String favName, final String favId);
}
