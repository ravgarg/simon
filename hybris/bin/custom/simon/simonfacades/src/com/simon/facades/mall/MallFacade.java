package com.simon.facades.mall;

import java.util.List;

import com.simon.facades.customer.data.MallData;


/**
 * Defines an API to perform various Mall related operations in SPO.
 */
public interface MallFacade
{
	/**
	 * Method to get the list of Malls.
	 *
	 * @return listOfMallData.
	 */
	List<MallData> getMallList();
}
