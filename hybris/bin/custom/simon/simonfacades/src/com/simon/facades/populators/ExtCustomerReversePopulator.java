package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerReversePopulator;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.simon.core.model.DesignerModel;
import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.customer.data.MallData;




/**
 * The Class ExtCustomerReversePopulator.
 */
public class ExtCustomerReversePopulator extends CustomerReversePopulator
{
	@Resource(name = "extReverseDesignerConverter")
	private Converter<DesignerData, DesignerModel> extReverseDesignerConverter;

	@Resource(name = "mallService")
	private MallService mallService;

	@Override
	public void populate(final CustomerData source, final CustomerModel target)
	{

		target.setName(getCustomerNameStrategy().getName(source.getFirstName(), source.getLastName()));
		target.setZipCode(source.getZipcode());
		target.setBirthMonth(source.getBirthMonth());
		target.setBirthYear(source.getBirthYear());
		target.setGender(source.getGender());

		if (null != source.getPrimaryMall() && StringUtils.isNotEmpty(source.getPrimaryMall().getCode()))
		{
			final MallModel primaryMall = mallService.getMallForCode(source.getPrimaryMall().getCode());
			target.setPrimaryMall(primaryMall);
		}

		final List<String> mallCodes = new ArrayList<>();

		if (CollectionUtils.isNotEmpty(source.getAlternateMalls()))
		{
			final List<MallData> listOfMallData = source.getAlternateMalls();

			for (final MallData mallData : listOfMallData)
			{
				mallCodes.add(mallData.getCode());
			}

		}

		Set<MallModel> alternateMalls = new HashSet<>();

		if (CollectionUtils.isNotEmpty(mallCodes))
		{
			alternateMalls = mallService.getMallListForCode(mallCodes);
		}

		target.setAlternateMalls(alternateMalls);

		if (null != target.getProfileCompleted() && !target.getProfileCompleted().booleanValue())
		{
			target.setProfileCompleted(source.isProfileCompleted());
		}
		if (source.getCountry() != null)
		{
			final String isocode = source.getCountry();
			target.setCountry(getCommonI18NService().getCountry(isocode));
		}

		target.setOptedInEmail(source.isOptedInEmail());
	}

}
