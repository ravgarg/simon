package com.simon.facades.cms.impl;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmsfacades.pages.impl.DefaultPageFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import com.simon.core.model.WhitelistedUserModel;
import com.simon.core.services.ExtWhitelistedLoginService;
import com.simon.facades.cms.ExtPageFacade;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * The Class ExtDefaultPageFacadeImpl.
 */
public class ExtPageFacadeImpl extends DefaultPageFacade implements ExtPageFacade
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtPageFacadeImpl.class);
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;
	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;
	@Resource(name = "extWhitelistedLoginService")
	private ExtWhitelistedLoginService extWhitelistedLoginService;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	public static final String SPLIT = "q=";
	public static final String Q_URL = "?q=";


	/**
	 * Populate all child nav node url for left nav.
	 *
	 * @param facetData
	 *           the facet data
	 * @param pageLabel
	 *           the page id
	 * @return the list
	 */
	@Override
	public List<FacetData<SearchStateData>> populateUrlForLeftNav(final List<FacetData<SearchStateData>> facetData,
			final String pageLabel)
	{
		facetData.forEach(f -> {
			f.getValues().forEach(fv -> {
				populateUrl(fv.getQuery(), pageLabel);
				if (null != fv.getChildNode())
				{
					populateChildNode(pageLabel, fv);
				}
			});
		});
		return facetData;
	}

	/**
	 * Populate child node.
	 *
	 * @param pageLabel
	 *           the page label
	 * @param child
	 *           the child
	 */
	private void populateChildNode(final String pageLabel, final FacetValueData<SearchStateData> child)
	{
		child.getChildNode().forEach(childNode -> {
			populateUrl(childNode.getQuery(), pageLabel);
			if (null != childNode.getChildNode())
			{
				populateChildNode(pageLabel, childNode);
			}
		});
	}

	/**
	 * Populate url.
	 *
	 * @param search
	 *           the search
	 * @param pageLabel
	 *           the page label
	 */
	private void populateUrl(final SearchStateData search, final String pageLabel)
	{
		final String url = search.getUrl();
		if (null != url)
		{
			final String[] path = url.split(SPLIT);
			if (path.length >= 2)
			{
				search.setUrl(pageLabel + Q_URL + path[1]);
			}
		}

	}


	/**
	 * Search listing page data.
	 *
	 * @param searchQuery
	 *           the search query
	 * @param model
	 *           the model
	 * @param pageForRequest
	 *           the page for request
	 * @param shopModel
	 *           the shop model
	 * @param pageLabel
	 *           the page id
	 * @return the product search page data
	 */
	@Override
	public ProductSearchPageData<SearchStateData, ProductData> searchListingPageData(final String searchQuery,
			final String sortCode, final Model model, final ContentPageModel pageForRequest, final String pageLabel)
	{
		ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
		final PageableData pageableData = new PageableData();
		pageableData.setSort(sortCode);
		pageableData.setPageSize(siteConfigService.getInt("storefront.search.pageSize", 0));
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);
		searchPageData = productSearchFacade.textSearch(searchState, pageableData);
		if (CollectionUtils.isNotEmpty(searchPageData.getFacets()) && CollectionUtils.isNotEmpty(searchPageData.getResults()))
		{
			searchPageData.setFacets(populateUrlForLeftNav(searchPageData.getFacets(), pageLabel));
			searchPageData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
		}
		searchPageData.getCurrentQuery().setClearAllUrl(pageLabel);
		return searchPageData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean betaLoginProcess(final String username, final String password, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		LOGGER.info("betaAccountLogin start");
		boolean validUser = false;
		final WhitelistedUserModel betaUser = extWhitelistedLoginService.findUserByEmailAndPassword(username, password);
		if (null != betaUser)
		{
			validUser = true;
			final Cookie[] cookies = request.getCookies();
			if (null != cookies)
			{
				for (final Cookie cookie : cookies)
				{
					if ("betaSession".equals(cookie.getName()))
					{
						final Cookie betaCookie = new Cookie("betaSession", "yes");
						betaCookie.setMaxAge(-1);
						betaCookie.setSecure(true);
						response.addCookie(betaCookie);
						LOGGER.info("Beta Session with yes value");
						break;
					}
				}
			}
		}
		return validUser;
	}
}
