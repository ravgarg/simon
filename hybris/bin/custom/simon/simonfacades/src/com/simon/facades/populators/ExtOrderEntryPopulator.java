
package com.simon.facades.populators;

import de.hybris.platform.basecommerce.enums.OrderEntryStatus;
import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Optional;

import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.enums.MiraklOrderLineStatus;
import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.core.enums.OrderExportType;


/**
 * This populator will take care of the attribute populations of {@link OrderEntryData} from
 * {@link AbstractOrderEntryModel}
 */
public class ExtOrderEntryPopulator extends OrderEntryPopulator
{

	/**
	 * This method will populate {@link OrderEntryData} from {@link AbstractOrderEntryModel}
	 */
	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
	{
		super.populate(source, target);
		target.setCallBackOrderEntryStatus(
				null != source.getCallBackOrderEntryStatus() ? source.getCallBackOrderEntryStatus().getCode() : StringUtils.EMPTY);

		if (source.getOrder() instanceof OrderModel)
		{
			if (null != source.getOrder().getStatus()
					&& OrderStatus.REJECTED.getCode().equals(source.getOrder().getStatus().getCode()))
			{
				target.setStatus(OrderEntryStatus.CANCELLED.getCode());
			}
			else
			{
				target.setStatus(
						null != source.getCallBackOrderEntryStatus() ? getStatus(source) : OrderEntryStatus.PROCESSING.getCode());
			}
		}
	}

	/**
	 * This method will set the Order Line Status depending on the response received from Retailer and Mirakl
	 */
	private String getStatus(final AbstractOrderEntryModel orderEntry)
	{
		String status = null;
		if (CallBackOrderEntryStatus.CANCELLED.getCode().equalsIgnoreCase(orderEntry.getCallBackOrderEntryStatus().getCode()))
		{
			status = OrderEntryStatus.CANCELLED.getCode();
		}
		else
		{
			final OrderExportType orderExportType = orderEntry.getProduct().getShop().getOrderExportType();
			if (null != orderExportType && orderExportType.equals(OrderExportType.NO_RETAILER))
			{
				status = OrderEntryStatus.CONTACT_RETAILER.getCode();
			}
			else
			{
				final Optional<ConsignmentEntryModel> consignmentEntry = orderEntry.getConsignmentEntries().stream().findFirst();
				if (consignmentEntry.isPresent())
				{
					status = checkMiraklOrderLineStatus(consignmentEntry.get());
				}
				else
				{
					status = OrderEntryStatus.ORDER_SENT_TO_RETAILER.getCode();
				}
			}
		}
		return status;
	}

	private String checkMiraklOrderLineStatus(final ConsignmentEntryModel consignmentEntryModel)
	{
		String status;

		if (consignmentEntryModel.getMiraklOrderLineStatus() == null
				|| consignmentEntryModel.getMiraklOrderLineStatus().getCode().equals(MiraklOrderLineStatus.PROCESSING.getCode()))
		{
			status = OrderEntryStatus.ORDER_SENT_TO_RETAILER.getCode();

		}
		else
		{
			status = null != consignmentEntryModel.getMiraklOrderLineStatus()
					? consignmentEntryModel.getMiraklOrderLineStatus().getCode()
					: StringUtils.EMPTY;

		}
		return status;

	}

}
