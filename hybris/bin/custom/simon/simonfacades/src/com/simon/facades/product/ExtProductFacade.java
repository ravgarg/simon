package com.simon.facades.product;

import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * The Interface ExtProductFacade.
 */
public interface ExtProductFacade extends ProductFacade
{

	/**
	 * Gets the product shop status active or not.
	 *
	 * @param productCode
	 *
	 * @return the product shop status
	 */
	public boolean getProductShopStatus(final String productCode);

	/**
	 * This method return Product model with given code
	 * 
	 * @param productCode
	 * @return
	 */
	public ProductModel getProductForCode(final String productCode);
}
