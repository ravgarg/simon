package com.simon.facades.message.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.exceptions.SystemException;
import com.simon.core.services.CustomMessageService;
import com.simon.facades.message.CustomMessageFacade;


/**
 * This Facade Class gets the messageText for code. If code/messageText is not available, service returns empty String.
 *
 */
public class CustomMessageFacadeImpl implements CustomMessageFacade
{
	/*
	 * SimonMessageService implementation class is injected. Returns message text string.
	 */
	@Autowired
	private CustomMessageService messageService;

	/*
	 * Returns message Text corresponding to message code from CustomMessageService.
	 *
	 * @param code The uid/code of SimonMessage Type
	 *
	 * @throws SystemException. Forwards the wrapper SystemException, to be handled at upper class.
	 */
	@Override
	public String getMessageTextForCode(final String code) throws SystemException
	{
		return messageService.getMessageForCode(code);
	}



}
