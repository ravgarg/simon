package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.type.TypeService;

import org.springframework.beans.factory.annotation.Required;

import com.simon.core.enums.CustomerGender;
import com.simon.facades.product.data.GenderData;


/**
 * Populator for Customer Gender Data.
 */
public class CustomerGenderDataPopulator implements Populator<CustomerGender, GenderData>
{

	private TypeService typeService;

	/**
	 * This method is used to populate data from CustomerGender to GenderData.
	 *
	 */

	@Override
	public void populate(final CustomerGender source, final GenderData target)
	{
		target.setCode(source.getCode());
		target.setName(getTypeService().getEnumerationValue(source).getName());
	}


	/**
	 * Set type service
	 *
	 * @param typeService
	 */
	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	/**
	 * Get Type Service
	 */
	protected TypeService getTypeService()
	{
		return typeService;
	}

}
