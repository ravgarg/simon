package com.simon.facades.cart.impl;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.EntryGroupData;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.commerceservice.order.ExtCommerceCartService;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.services.ExtCartService;
import com.simon.facades.cart.ExtCartFacade;
import com.simon.facades.cart.data.CartGlobalFlashMsgData;
import com.simon.facades.cart.data.CartGlobalMessageType;
import com.simon.facades.message.CustomMessageFacade;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.services.CartCheckOutIntegrationService;


/**
 * Implementation of Default Cart Facade according to simon Requirement with following use case Remove OOS Entry from
 * cart, Populating Cart Data from Modification , add flash message which need to show , update cart entry with
 * recalculation, and get the Mini cart json
 */
public class ExtCartFacadeImpl extends DefaultCartFacade implements ExtCartFacade
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtCartFacadeImpl.class);

	@Resource
	private ExtCartService cartService;
	@Resource
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;
	@Resource
	private ExtCommerceCartService commerceCartService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private CMSComponentService cmsComponentService;
	@Resource
	private SessionService sessionService;
	@Resource
	private ExtCartFacade cartFacade;
	@Resource
	private CustomMessageFacade customMessageFacade;

	@Resource
	private ExtCustomMessageUtils extCustomMessageUtils;
	@Resource
	private Converter<CartModel, CartData> extMiniCartConverter;
	@Resource
	private Converter<CartModificationData, CartData> extCartModificationCartConverter;
	@Resource
	private Converter<CartModel, CartModificationData> extCartModificationDataConverter;
	@Resource
	private Converter<CreateCartConfirmationDTO, AdditionalCartInfoModel> additionalCartInfoReverseConverter;
	@Resource
	private Converter<OrderConfirmCallBackDTO, AdditionalCartInfoModel> confirmUpdateReverseConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeAdditionalCart()
	{
		final CartModel cart = cartService.getSessionCart();
		if (cart != null && null != cart.getAdditionalCartInfo())
		{
			getModelService().remove(cart.getAdditionalCartInfo());
			cart.setRetailersDeliveryCost(null);
			cart.setRetailersTotalTax(null);
			getModelService().save(cart);
			getModelService().refresh(cart);
		}
	}

	/**
	 * Remove the OOS Product from Cart and get the modification Data back
	 *
	 * @return modifications
	 */
	public List<CartModificationData> getChangeEntriesInCart()
	{
		//Validate the cart
		List<CartModificationData> modifications = new ArrayList<>();
		try
		{
			modifications = validateCartEntries();
		}
		catch (final CommerceCartModificationException exe)
		{
			LOG.error("Failed to validate cart", exe);
		}
		return modifications;
	}

	/**
	 * Validate cart entries with recalculate method and populate the modification if any occur. ReCalcualte required in
	 * Cart where cart Entry price row will be displayed.
	 *
	 * @return List<CartModificationData>
	 * @throws CommerceCartModificationException
	 */
	public List<CartModificationData> validateCartEntries() throws CommerceCartModificationException
	{
		if (hasExtSessionCart())
		{
			return Converters.convertAll(commerceCartService.getChangeEntriesInCart(getCommerceCartParameters()),
					getCartModificationConverter());
		}
		else
		{
			return Collections.emptyList();
		}
	}




	/**
	 * Populate the List of cartModifcation to CartData entries
	 *
	 * @param source
	 * @param target
	 */
	public void populateCartDataFromModification(final List<CartModificationData> source, final CartData target)
	{
		List<CartGlobalFlashMsgData> flashMessages = target.getGlobalFlashMsg();
		if (flashMessages == null)
		{
			flashMessages = new ArrayList<>();
		}
		// reset additional cart info if it contain failed product
		final CartModel cartModel = cartService.getSessionCart();
		if (null != cartModel.getAdditionalCartInfo())
		{
			for (final RetailersInfoModel retailersInfoModel : cartModel.getAdditionalCartInfo().getRetailersInfo())
			{
				if (CollectionUtils.isNotEmpty(retailersInfoModel.getFailedProducts())
						|| checkForMismatchedProducts(retailersInfoModel) || checkForMismatchedProductsVariants(retailersInfoModel))
				{
					getModelService().save(cartModel);
					sessionService.removeAttribute(SimonCoreConstants.UNAPPROVED_PRODUCTIDS);
					sessionService.removeAttribute(SimonCoreConstants.OOS_PRODUCTIDS);
				}
			}
		}
		boolean outOfStock = false;
		if (!source.isEmpty())
		{
			for (final CartModificationData cartModificationData : source)
			{
				outOfStock = getFlashMessage(flashMessages, outOfStock, cartModificationData);
				extCartModificationCartConverter.convert(cartModificationData, target);

			}
		}
		target.setGlobalFlashMsg(flashMessages);

	}

	/**
	 * @param flashMessages
	 * @param outOfStock
	 * @param cartModificationData
	 * @return
	 */
	private boolean getFlashMessage(final List<CartGlobalFlashMsgData> flashMessages, final boolean outOfStock,
			final CartModificationData cartModificationData)
	{
		boolean isOutOfStock = false;
		if (null != cartModificationData.getStatusCode()
				&& cartModificationData.getStatusCode().equalsIgnoreCase(CommerceCartModificationStatus.NO_STOCK))
		{
			if (!outOfStock)
			{
				isOutOfStock = true;
				final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
				flashMessage.setMessage(getLocalizedString(SimonCoreConstants.NOSTOCK));
				flashMessage.setMessageType(CartGlobalMessageType.OOS);
				flashMessages.add(flashMessage);
			}
		}
		else if (null != cartModificationData.getStatusCode()
				&& cartModificationData.getStatusCode().equalsIgnoreCase(CommerceCartModificationStatus.LOW_STOCK))
		{
			final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
			flashMessage.setMessage(getLocalizedString(SimonCoreConstants.LOWSTOCK));
			flashMessage.setMessageType(CartGlobalMessageType.OOS);
			flashMessages.add(flashMessage);
		}
		else if (null != cartModificationData.getStatusCode()
				&& cartModificationData.getStatusCode().equalsIgnoreCase(CommerceCartModificationStatus.UNAVAILABLE))
		{
			final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
			flashMessage.setMessage(getLocalizedString(SimonCoreConstants.UNAVAILABLE));
			flashMessage.setMessageType(CartGlobalMessageType.OOS);
			flashMessages.add(flashMessage);

		}
		return isOutOfStock;
	}

	/**
	 * check of variant field mismatched or not on retailer site
	 *
	 * @param retailersInfoModel
	 */
	private boolean checkForMismatchedProducts(final RetailersInfoModel retailersInfoModel)
	{
		boolean result = false;

		final List<String> missingProducts = sessionService.getAttribute(SimonCoreConstants.UNAPPROVED_PRODUCTIDS);
		if (CollectionUtils.isNotEmpty(missingProducts))
		{
			final List<AdditionalProductDetailsModel> additionalProductDetails = retailersInfoModel.getAddedProducts();
			result = ifMismatchedProductsExist(missingProducts, additionalProductDetails);
		}

		return result;
	}

	/**
	 * check of variant exist or not on retailer site
	 *
	 * @param retailersInfoModel
	 */
	private boolean checkForMismatchedProductsVariants(final RetailersInfoModel retailersInfoModel)
	{
		boolean result = false;

		final List<String> missingProductsVariants = sessionService.getAttribute(SimonCoreConstants.OOS_PRODUCTIDS);
		if (CollectionUtils.isNotEmpty(missingProductsVariants))
		{
			final List<AdditionalProductDetailsModel> additionalProductDetails = retailersInfoModel.getAddedProducts();
			result = ifMismatchedProductsExist(missingProductsVariants, additionalProductDetails);
		}

		return result;
	}

	private boolean ifMismatchedProductsExist(final List<String> missingProducts,
			final List<AdditionalProductDetailsModel> additionalProductDetails)
	{
		if (CollectionUtils.isNotEmpty(additionalProductDetails))
		{
			for (final AdditionalProductDetailsModel additionalProductDetailsModel : additionalProductDetails)
			{
				if (missingProducts.contains(additionalProductDetailsModel.getProductID()))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Add Flash Message According to following scenario 1. OOS 2. Update Cart SucessFully 3. Remove Product From Cart
	 *
	 * @param cartData
	 * @param quantity
	 * @param cartModification
	 */
	public void addFlashMessage(final CartData cartData, final long quantity, final CartModificationData cartModification)
	{
		List<CartGlobalFlashMsgData> flashMessages = cartData.getGlobalFlashMsg();
		if (flashMessages == null)
		{
			flashMessages = new ArrayList<>();
		}
		if (cartModification.getQuantity() == quantity)
		{
			// Success
			if (cartModification.getQuantity() == 0)
			{
				// Success in removing entry
				final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
				flashMessage.setMessage(getLocalizedString(SimonCoreConstants.REMOVE_SUCCESSFULLY).replace("{1}",
						cartModification.getEntry().getProduct().getName()));
				flashMessage.setMessageType(CartGlobalMessageType.REMOVE);
				flashMessages.add(flashMessage);
			}
			else
			{
				// Success in update quantity
				final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
				flashMessage.setMessage(getLocalizedString(SimonCoreConstants.UPDATE_SUCCESSFULLY));
				flashMessage.setMessageType(CartGlobalMessageType.UPDATE);
				flashMessages.add(flashMessage);
			}
		}
		else if (cartModification.getQuantity() > 0)
		{
			// Less than successful
			final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
			flashMessage.setMessage(getLocalizedString(SimonCoreConstants.LOWSTOCK));
			flashMessage.setMessageType(CartGlobalMessageType.OOS);
			flashMessages.add(flashMessage);
		}
		else
		{
			// No more stock available
			final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
			flashMessage.setMessage(getLocalizedString(SimonCoreConstants.NOSTOCK));
			flashMessage.setMessageType(CartGlobalMessageType.OOS);
			flashMessages.add(flashMessage);
		}
		cartData.setGlobalFlashMsg(flashMessages);
	}


	/**
	 * This method set shopping bag labels to json object.
	 *
	 */
	public void setCartPageMessages(final CartData cartData)
	{
		cartData.setMessageLabels(extCustomMessageUtils.setCartPageMessages());
	}



	/**
	 * method return cart data for mini. bag A json is used for display the mini cart in page. this class call cart
	 * service get session cart method and get CartModel, for display banner in mini cart MiniCartComponentModel has a
	 * LightboxBannerComponent which have simpleBannerComponentModel which provides the banner url and link url to
	 * display banner and redirect url to redirect the page.
	 *
	 * @return cartData
	 */
	public CartData getMiniBag()
	{
		CartData cartData;
		Map<String, String> miniCartMessages;
		if (hasExtSessionCart())
		{

			final List<CartModificationData> modifications = getChangeEntriesInCart();
			final CartModel cart = cartService.getSessionCart();
			cartData = extMiniCartConverter.convert(cart);
			cartFacade.populateCartDataFromModification(modifications, cartData);

			if (cartData.getTotalUnitCount() > 0)
			{
				miniCartMessages = extCustomMessageUtils.setMiniCartMessages();
			}
			else
			{
				miniCartMessages = extCustomMessageUtils.setEmptyMiniCartMessages();
			}
		}
		else
		{
			cartData = getMiniCartConverter().convert(null);
			miniCartMessages = extCustomMessageUtils.setEmptyMiniCartMessages();
		}
		cartData.setMessageLabels(miniCartMessages);
		return cartData;
	}

	/**
	 * Method addToCart is overrided method from the parent class for Add To Cart functionality and modify the populator
	 * for total item count and labels for json object.
	 *
	 * @param addToCartParams
	 * @return CommerceCartModification
	 */
	@Override
	public CartModificationData addToCart(final AddToCartParams addToCartParams) throws CommerceCartModificationException
	{
		final CartModificationData cartModification = super.addToCart(addToCartParams);
		extCartModificationDataConverter.convert(cartService.getSessionCart(), cartModification);
		return cartModification;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createTwoTapCart() throws CartCheckOutIntegrationException
	{
		final CartModel cartModel = cartService.getSessionCart();

		final String isMockResponse = configurationService.getConfiguration()
				.getString(SimonIntegrationConstants.CREATE_CART_STUB_FLAG);

		if (isMockResponse.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			LOG.info("Mock flag found true. Proceeding with mock service for the cart {}", cartModel.getCode());
			invokeCartConfirmationMockRequest(cartModel);
		}
		else
		{
			try
			{
				LOG.info("Mock flag found false. Proceeding with the createCartRequest on ESB for the cart {}", cartModel.getCode());
				cartCheckOutIntegrationService.invokeCreateCartRequest(cartModel);
			}
			catch (final CartCheckOutIntegrationException cartCheckOutIntegrationException)
			{
				LOG.error("Create Cart failed for the cart {}", cartModel.getCode());
				throw cartCheckOutIntegrationException;
			}
		}
	}

	@Override
	public CartData getSessionCartWithEntryOrdering(final boolean recentlyAddedFirst)
	{
		if (hasExtSessionCart())
		{
			final CartData data = getExtSessionCart();

			if (recentlyAddedFirst)
			{
				final List<OrderEntryData> recentlyAddedListEntries = new ArrayList<>(data.getEntries());
				Collections.reverse(recentlyAddedListEntries);
				data.setEntries(Collections.unmodifiableList(recentlyAddedListEntries));
				final List<EntryGroupData> recentlyChangedEntryGroups = new ArrayList<>(data.getRootGroups());
				Collections.reverse(recentlyChangedEntryGroups);
				data.setRootGroups(Collections.unmodifiableList(recentlyChangedEntryGroups));
			}

			return data;
		}
		return createEmptyCart();
	}

	/**
	 * Method take cart model and call two tap API's invokeCartConfirmationMockRequest which is a mocked service returns
	 * CreateCartConfirmationDTO. additionalCartInfoReverseConverter create AdditionalCartInfoModel by calling a reverse
	 * converter additionalCartInfoReverseConverter.
	 *
	 * @param cartModel
	 */
	public void invokeCartConfirmationMockRequest(final CartModel cartModel)
	{
		try
		{
			final CreateCartConfirmationDTO createCartConfirmationDTO = cartCheckOutIntegrationService
					.invokeCartConfirmationMockRequest();
			saveCartCallBackResponse(createCartConfirmationDTO, cartModel);
		}
		catch (final IOException ex)
		{
			LOG.error("IO Exception", ex);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveCartCallBackResponse(final CreateCartConfirmationDTO createCartConfirmationDTO)
	{
		final CartModel cartModel = cartService.getCartByCartId(createCartConfirmationDTO.getCartId());
		saveCartCallBackResponse(createCartConfirmationDTO, cartModel);
	}

	protected ObjectMapper getObjectMapper()
	{
		return new ObjectMapper();
	}

	protected boolean hasExtSessionCart()
	{
		return hasSessionCart();
	}

	protected CartData getExtSessionCart()
	{
		return getSessionCart();
	}

	protected CommerceCartParameter getCommerceCartParameters()
	{
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartService.getSessionCart());
		return parameter;
	}

	protected String getLocalizedString(final String str)
	{
		return Localization.getLocalizedString(str);
	}

	/**
	 * @return cmsComponentService
	 */
	public CMSComponentService getCmsComponentService()
	{
		return cmsComponentService;
	}

	/**
	 * @param cmsComponentService
	 */
	public void setCmsComponentService(final CMSComponentService cmsComponentService)
	{
		this.cmsComponentService = cmsComponentService;
	}

	/**
	 * This method is used for return SimpleCMSComponent for banner component in mini bag
	 *
	 * @param id
	 * @return MiniCartComponentModel
	 * @throws CMSItemNotFoundException
	 */
	@Override
	public MiniCartComponentModel getMiniCartComponent(final String id) throws CMSItemNotFoundException
	{
		MiniCartComponentModel miniCartComponentModel = null;
		final SimpleCMSComponentModel simpleCMSComponentModel = getCmsComponentService().getSimpleCMSComponent(id);
		if (simpleCMSComponentModel instanceof MiniCartComponentModel)
		{
			miniCartComponentModel = (MiniCartComponentModel) simpleCMSComponentModel;
		}
		return miniCartComponentModel;
	}

	@Override
	public void saveCartCallBackResponse(final CreateCartConfirmationDTO createCartConfirmationDTO, final CartModel cartModel)
	{

		if (null != cartModel)
		{
			if (null != cartModel.getAdditionalCartInfo())
			{
				removeAdditionalCart();
			}
			final AdditionalCartInfoModel additionalCartInfoModel = additionalCartInfoReverseConverter
					.convert(createCartConfirmationDTO);
			cartModel.setAdditionalCartInfo(additionalCartInfoModel);
			getModelService().saveAll(cartModel);
			cartService.modifyStockForFailedProducts(cartModel);
		}
	}

	@Override
	public int getCartCount()
	{
		return cartService.getCartCount();
	}

}
