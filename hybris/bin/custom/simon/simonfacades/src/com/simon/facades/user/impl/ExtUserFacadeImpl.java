package com.simon.facades.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.user.ExtUserFacade;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationService;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationService;
import com.simon.integration.users.dto.GetAddressResponseDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;
import com.simon.integration.users.services.UserAddressIntegrationService;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * Class user for setting default address for user
 *
 */
public class ExtUserFacadeImpl extends DefaultUserFacade implements ExtUserFacade
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtUserFacadeImpl.class);

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "extCustomerAccountService")
	private ExtCustomerAccountService customerAccountService;

	@Resource(name = "userAddressIntegrationService")
	private UserAddressIntegrationService userAddressIntegrationService;

	@Resource(name = "userAddressVerifyIntegrationService")
	private UserAddressVerifyIntegrationService userAddressVerifyIntegrationService;

	@Resource
	private CardPaymentIntegrationService cardPaymentIntegrationService;

	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	@Override
	public AddressData getAddress(final String addressPK)
	{
		final AddressModel addressModel = modelService.get(PK.parse(addressPK));
		final AddressData addressData = getAddressConverter().convert(addressModel);
		return addressData;
	}

	/**
	 * This method will fetch all the billing address for user
	 *
	 * @param saved
	 *           of type {@link AddressData}
	 * @return List<AddressData>
	 */
	@Override
	public List<AddressData> getBillingAddressList(final boolean saved)
	{
		final List<AddressData> addressList = new ArrayList<>();
		final CustomerModel currentCustomer = getCurrentUserForCheckout();
		final List<CreditCardPaymentInfoModel> creditCards = getCustomerAccountService().getCreditCardPaymentInfos(currentCustomer,
				saved);
		for (final CreditCardPaymentInfoModel creditCardPaymentInfo : creditCards)
		{
			if (null != creditCardPaymentInfo.getBillingAddress() && !creditCardPaymentInfo.getBillingAddress().getDuplicate())
			{
				addressList.add(getAddressConverter().convert(creditCardPaymentInfo.getBillingAddress()));
			}
		}
		return addressList;
	}


	/**
	 * Sets the payment info for the logged in customer
	 *
	 * @param ccPaymentInfoData
	 * @return boolean
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	@Override
	public boolean setNewPaymentInfo(final CCPaymentInfoData ccPaymentInfoData)
			throws ModelSavingException, UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		validateParameterNotNullStandardMessage("extPaymentInfoData", ccPaymentInfoData);
		final AddressData billingAddressData = ccPaymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddress", billingAddressData);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = customerAccountService
				.createPaymentInfoSubscription(ccPaymentInfoData);

		//adding the used payment address with po.com
		final boolean success = addUserPaymentAddress(ccPaymentInfoData, ccPaymentInfoModel);


		// set Default address
		if (success && ccPaymentInfoData.isDefaultPaymentInfo())
		{
			setDefaultPaymentInfo(getCreditCardPaymentInfoConverter().convert(ccPaymentInfoModel));
		}

		return success;
	}

	/**
	 * Method to add user's address in DB and keep it in Sync.
	 *
	 * @param addressData
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public boolean addUserAddress(final ExtAddressData addressData) throws UserAddressIntegrationException
	{
		addUserAddrSuper(addressData);
		try
		{
			final UserAddressResponseDTO responseDTO = userAddressIntegrationService.addUserAddress(addressData);

			if (!getSuccessFlag(responseDTO))
			{
				LOGGER.debug(
						"Address is being deleted from Database as we encounterred an error while adding address via User Integration Service");
				removeUserAddrSuper(addressData);
				return false;
			}
		}
		catch (final UserAddressIntegrationException ex)
		{
			LOGGER.error("Exception occurred while adding the address (delta update) via User Integration Service", ex);
			removeUserAddrSuper(addressData);
			throw ex;
		}

		LOGGER.debug("Address has been added sucessfully");
		return true;
	}



	/**
	 * Method to edit user's address in DB and keep it in Sync.
	 *
	 * @param addressData
	 *
	 * @return boolean
	 *
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public boolean editUserAddress(final ExtAddressData addressData) throws UserAddressIntegrationException
	{
		final UserAddressResponseDTO responseDTO = userAddressIntegrationService.updateUserAddress(addressData);
		if (getSuccessFlag(responseDTO))
		{
			LOGGER.debug("Address has been edited sucessfully");

			editUserAddrSuper(addressData);
			return true;
		}
		return false;
	}

	/**
	 * Method to remove user's address from DB and keep it in Sync.
	 *
	 * @param addressData
	 *
	 * @return boolean
	 *
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public boolean removeUserAddress(final ExtAddressData addressData) throws UserAddressIntegrationException
	{
		final UserAddressResponseDTO responseDTO = userAddressIntegrationService.removeUserAddress(addressData);

		if (getSuccessFlag(responseDTO))
		{
			removeUserAddrSuper(addressData);
			LOGGER.debug("Address has been deleted sucessfully");
			return true;
		}

		LOGGER.debug("Address has not been deleted");
		return false;
	}

	/**
	 * OOB Method to add the user's address.
	 *
	 * @param addressData
	 */
	protected void addUserAddrSuper(final AddressData addressData)
	{
		super.addAddress(addressData);
	}

	/**
	 * OOB Method to remove the user's address.
	 *
	 * @param addressData
	 */
	protected void removeUserAddrSuper(final AddressData addressData)
	{
		super.removeAddress(addressData);
	}

	/**
	 * OOB Method to edit the user's address.
	 *
	 * @param addressData
	 */
	protected void editUserAddrSuper(final AddressData addressData)
	{
		super.editAddress(addressData);
	}

	/**
	 * This method is used to add the user payment address with PO.com
	 *
	 * @param ccPaymentInfoData
	 * @param ccPaymentInfoModel
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	public boolean addUserPaymentAddress(final CCPaymentInfoData ccPaymentInfoData,
			final CreditCardPaymentInfoModel ccPaymentInfoModel)
			throws UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		LOGGER.debug("ExtUserFacadeImpl ::: addUserPaymentAddress :: ccPaymentInfoModel : {} , extPaymentInfoData : {} ",
				ccPaymentInfoModel, ccPaymentInfoData);

		boolean success = true;
		getAddressConverter().convert(ccPaymentInfoModel.getBillingAddress(), ccPaymentInfoData.getBillingAddress());

		try
		{
			final UserAddressResponseDTO userAddressResponseDTO = userAddressIntegrationService
					.addUserPaymentAddress(ccPaymentInfoData);

			if (!getSuccessFlag(userAddressResponseDTO))
			{

				unlinkCCPaymentInfoSuper(ccPaymentInfoModel);
				success = false;
			}
		}
		catch (final UserAddressIntegrationException userAddressIntegrationException)
		{
			LOGGER.error(
					"Getting User address integration while adding the user payment addess with PO.com, So now unlinking the added payment info with user. userAddressIntegrationException is  : {}",
					userAddressIntegrationException);

			unlinkCCPaymentInfoSuper(ccPaymentInfoModel);

			callRemovePaymentMethodToken(ccPaymentInfoData.getPaymentToken());

			throw userAddressIntegrationException;
		}

		return success;

	}

	/**
	 * This method is used to call the super class method unlinkCCPaymentInfo.
	 *
	 * @param ccPaymentInfoModel
	 */
	protected void unlinkCCPaymentInfoSuper(final CreditCardPaymentInfoModel ccPaymentInfoModel)
	{
		unlinkCCPaymentInfo(getCCPaymentInfoModelPK(ccPaymentInfoModel));
	}


	/**
	 * This method is used to remove the linked payment address from PO.com
	 *
	 * @param addressModel
	 * @return boolean
	 */
	@Override
	public boolean removeCCPaymentInfoUserAddress(final AddressModel addressModel) throws UserAddressIntegrationException
	{
		boolean success = true;
		final ExtAddressData addressData = new ExtAddressData();
		getAddressConverter().convert(addressModel, addressData);

		final UserAddressResponseDTO responseDTO = userAddressIntegrationService.removeUserAddress(addressData);
		if (!getSuccessFlag(responseDTO))
		{
			success = false;
		}
		return success;
	}


	/**
	 * This method is used to get the address info model from payment method id.
	 *
	 * @param paymentMethodId
	 * @return AddressModel
	 */

	@Override
	public AddressModel getCCPaymentInfoAddressModel(final String id)
	{
		validateParameterNotNullStandardMessage("id", id);
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final List<CreditCardPaymentInfoModel> creditCardPaymentInfoModelList = getCustomerAccountService()
				.getCreditCardPaymentInfos(currentCustomer, false);

		for (final CreditCardPaymentInfoModel creditCardPaymentInfo : creditCardPaymentInfoModelList)
		{
			if (getCCPaymentInfoModelPK(creditCardPaymentInfo).equals(id))
			{
				return creditCardPaymentInfo.getBillingAddress();

			}
		}
		return null;
	}

	/**
	 * This method is used to remove the payment token from Payment Center.
	 *
	 * @param paymentMethodId
	 * @throws CartCheckOutIntegrationException
	 * @throws UserAddressIntegrationException
	 */
	public void removePaymentMethodToken(final String paymentMethodId)
			throws CartCheckOutIntegrationException, UserAddressIntegrationException
	{
		validateParameterNotNullStandardMessage("paymentMethodId", paymentMethodId);

		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();

		final CreditCardPaymentInfoModel creditCardPaymentInfo = getCustomerAccountService()
				.getCreditCardPaymentInfoForCode(currentCustomer, paymentMethodId);

		List<GetAddressResponseDTO> addressesList = new ArrayList<>();
		final UserGetAddressResponseDTO userAddressResponseDTO = userAddressIntegrationService.getUserBillingAddress();
		if (userAddressResponseDTO != null && CollectionUtils.isNotEmpty(userAddressResponseDTO.getAddresses()))
		{
			addressesList = userAddressResponseDTO.getAddresses();
		}

		for (final GetAddressResponseDTO address : addressesList)
		{
			final String addressExternalID = userIntegrationUtils.removeEnvPrefixToAddressId(address.getExternalId());

			if (addressExternalID.equals(creditCardPaymentInfo.getBillingAddress().getPk().toString()))
			{
				callRemovePaymentMethodToken(address.getPayments().get(0).getToken());
				break;
			}
		}
	}

	/**
	 * This method is used to remove the payment token from Payment Center.
	 *
	 * @param paymentMethodToken
	 * @throws CartCheckOutIntegrationException
	 */
	public void callRemovePaymentMethodToken(final String paymentMethodToken) throws CartCheckOutIntegrationException
	{
		final RemovePaymentResponseDTO removePaymentResponseDTO = cardPaymentIntegrationService
				.invokeRemovePaymentMethodToken(paymentMethodToken);
		if (StringUtils.isNotBlank(removePaymentResponseDTO.getErrorCode()))
		{
			throw new CartCheckOutIntegrationException(removePaymentResponseDTO.getErrorMessage(),
					removePaymentResponseDTO.getErrorCode());
		}
	}

	/**
	 * This method is used to return the success flag value.
	 *
	 * @param responseDTO
	 * @return
	 */
	private boolean getSuccessFlag(final UserAddressResponseDTO responseDTO)
	{

		LOGGER.debug("ExtUserFacadeImpl ::: getSuccessFlag :: responseDTO : {} ", responseDTO);

		boolean successFlag = Boolean.FALSE;
		String messageFlag = StringUtils.EMPTY;

		if (responseDTO != null)
		{
			successFlag = responseDTO.getErrorCode() == null ? Boolean.TRUE : Boolean.FALSE;
			messageFlag = responseDTO.getErrorCode() == null ? responseDTO.getMessage() : responseDTO.getErrorMessage();
		}

		LOGGER.debug("ExtUserFacadeImpl ::: getSuccessFlag :: successFlag : {} ::: , messageFlag : {}", successFlag, messageFlag);

		return successFlag;
	}

	/**
	 * This method is used to get the PK of CreditCardPaymentInfoModel
	 *
	 * @param ccPaymentInfoModel
	 * @return
	 */
	protected String getCCPaymentInfoModelPK(final CreditCardPaymentInfoModel ccPaymentInfoModel)
	{
		return ccPaymentInfoModel.getPk().toString();
	}

	/**
	 * Gets the user address verify response DTO.
	 *
	 * @param addressData
	 *           the address data
	 * @return the user address verify response DTO
	 * @throws UserAddressVerifyIntegrationException
	 *            the user address verify integration exception
	 */
	@Override
	public UserAddressVerifyResponseDTO getUserAddressVerifyResponseDTO(final ExtAddressData addressData)
			throws UserAddressVerifyIntegrationException
	{
		return userAddressVerifyIntegrationService.validateAddress(addressData);

	}
}
