package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Map;

import org.apache.commons.lang.WordUtils;

import com.simon.core.model.ShippingDetailsModel;


/**
 * Method populate ShippingDetailsModel from Map<String, String> which have information of ShippingDetails.
 */
public class ShippingDetailsReversePopulator implements Populator<Map<String, String>, ShippingDetailsModel>
{


	/**
	 * Method populate ShippingDetailsModel from Map<String, String> which have information of ShippingDetails.
	 */
	@Override
	public void populate(final Map<String, String> source, final ShippingDetailsModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		final Map.Entry<String, String> entry = source.entrySet().iterator().next();
		target.setCode(entry.getKey());
		target.setDescription(WordUtils.capitalize(entry.getValue().toLowerCase()));
	}
}
