package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


/**
 * populate AdditionalProductDetailsModel from OrderConfirmCartProductsCallBackDTO which have information of retailers,
 * failed products.
 *
 */
public class ConfirmUpdateFailedProductReversePopulator
		implements Populator<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel>
{

	@Resource
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	/**
	 * Method populate AdditionalProductDetailsModel from OrderConfirmCartProductsCallBackDTO which have information of
	 * retailers, failed products.
	 */
	@Override
	public void populate(final OrderConfirmCartProductsCallBackDTO source, final AdditionalProductDetailsModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setProductID(StringUtils.isEmpty(source.getProductId()) ? StringUtils.EMPTY : source.getProductId());
	}

}
