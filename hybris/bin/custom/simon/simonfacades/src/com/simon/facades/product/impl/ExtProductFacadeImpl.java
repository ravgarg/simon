package com.simon.facades.product.impl;

import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.annotation.Resource;

import com.simon.facades.product.ExtProductFacade;


/**
 * The Class ExtProductFacadeImpl.
 */
public class ExtProductFacadeImpl extends DefaultProductFacade implements ExtProductFacade
{
	@Resource(name = "productService")
	private ProductService productService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getProductShopStatus(final String productCode)
	{
		final ProductModel productModel = getProductForCode(productCode);

		if (null != productModel && productModel.getShop() != null
				&& "CLOSE".equalsIgnoreCase(productModel.getShop().getState().getCode()))
		{
			return true;
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductModel getProductForCode(final String productCode)
	{
		return productService.getProductForCode(productCode);
	}

}
