package com.simon.facades.populators;

import static com.mirakl.client.core.internal.util.Preconditions.checkNotNull;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.order.tax.MiraklOrderTaxAmount;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderOffer;
import com.mirakl.client.mmp.request.additionalfield.MiraklRequestAdditionalFieldValue;
import com.mirakl.client.mmp.request.additionalfield.MiraklRequestAdditionalFieldValue.MiraklSimpleRequestAdditionalFieldValue;


public class MarketPlaceCreateOrderOfferPopulator implements Populator<AbstractOrderEntryModel, MiraklCreateOrderOffer>
{
	private static final Logger LOG = Logger.getLogger(MarketPlaceCreateOrderOfferPopulator.class);

	@Resource
	private ConfigurationService configurationService;

	protected Converter<TaxValue, MiraklOrderTaxAmount> miraklOrderTaxAmountConverter;

	@Override
	public void populate(final AbstractOrderEntryModel orderEntry, final MiraklCreateOrderOffer miraklCreateOrderOffer)
			throws ConversionException
	{
		checkNotNull(orderEntry);
		checkNotNull(miraklCreateOrderOffer);
		//		if found any entry CallBackOrderEntryStatus.CANCELLED then that order entry not will export
		final String offerId = orderEntry.getOfferId();
		checkNotNull(offerId);

		miraklCreateOrderOffer.setId(offerId);
		miraklCreateOrderOffer.setQuantity(orderEntry.getQuantity().intValue());

		setShippingInfo(miraklCreateOrderOffer, orderEntry);
		if (isTrue(orderEntry.getOrder().getNet()))
		{
			setTaxes(miraklCreateOrderOffer, orderEntry);
		}
		miraklCreateOrderOffer.setPriceUnit(BigDecimal.valueOf(orderEntry.getBasePrice()));
		miraklCreateOrderOffer
				.setPrice(miraklCreateOrderOffer.getPriceUnit().multiply(BigDecimal.valueOf(orderEntry.getQuantity())));
		miraklCreateOrderOffer.setCurrencyIsoCode(MiraklIsoCurrencyCode.valueOf(orderEntry.getOrder().getCurrency().getIsocode()));
		miraklCreateOrderOffer.setLeadtimeToShip(orderEntry.getLeadTimeToShip());
		miraklCreateOrderOffer.setOrderLineAdditionalFields(getOrderLineAdditionalFields(orderEntry));
		if (LOG.isDebugEnabled())
		{
			LOG.debug("MiraklCreateOrderOffer   " + offerId);
			LOG.debug("   quantity " + miraklCreateOrderOffer.getQuantity());
			LOG.debug("   price " + miraklCreateOrderOffer.getPrice().doubleValue());
			LOG.debug("   priceUnit " + miraklCreateOrderOffer.getPriceUnit().doubleValue());
			if (miraklCreateOrderOffer.getTaxes() != null)
			{
				for (final Object tax : miraklCreateOrderOffer.getTaxes())
				{
					LOG.debug("  tax " + tax);
				}
			}
			LOG.debug("  shippingPrice" + miraklCreateOrderOffer.getShippingPrice().doubleValue());
		}
	}

	private List<MiraklRequestAdditionalFieldValue> getOrderLineAdditionalFields(final AbstractOrderEntryModel orderEntry)
	{
		final List<MiraklRequestAdditionalFieldValue> list = new ArrayList<>();
		final String customFieldName = configurationService.getConfiguration().getString("marketplace.orderline.custom.field");
		final List<String> additionalFieldInfo = Arrays.asList(customFieldName.split("-"));
		if (additionalFieldInfo.contains("discount"))
		{
			final String customFieldValue = getDiscountValue(orderEntry);
			final MiraklSimpleRequestAdditionalFieldValue miraklSimpleRequestAdditionalFieldValueForTaxAmt = new MiraklSimpleRequestAdditionalFieldValue(
					customFieldName, getInRoundFormat(customFieldValue));
			list.add(miraklSimpleRequestAdditionalFieldValueForTaxAmt);
		}
		return list;
	}

	private String getDiscountValue(final AbstractOrderEntryModel orderEntry)
	{
		final double totaldiscount = orderEntry.getDiscountValues().stream().mapToDouble(discount -> discount.getAppliedValue())
				.sum();
		return String.valueOf(totaldiscount);
	}

	protected void setTaxes(final MiraklCreateOrderOffer miraklCreateOrderOffer, final AbstractOrderEntryModel orderEntry)
	{
		miraklCreateOrderOffer.setTaxes(miraklOrderTaxAmountConverter.convertAll(orderEntry.getTaxValues()));
	}

	protected void setShippingInfo(final MiraklCreateOrderOffer miraklCreateOrderOffer, final AbstractOrderEntryModel orderEntry)
	{
		final AbstractOrderModel order = orderEntry.getOrder();
		final String shopId = orderEntry.getProduct().getShop().getId();

		final DeliveryModeModel deliveryMode = order.getRetailersDeliveryModes().get(shopId);
		final Configuration configuration = configurationService.getConfiguration();
		final String lineShippingCode = "cheapest".equalsIgnoreCase(deliveryMode.getCode())
				? configuration.getString("marketplace.cheapest.code") : configuration.getString("marketplace.fastest.code");
		miraklCreateOrderOffer.setShippingTypeCode(lineShippingCode);

		miraklCreateOrderOffer.setShippingPrice(BigDecimal.valueOf(0));
	}

	@Required
	public void setMiraklOrderTaxAmountConverter(final Converter<TaxValue, MiraklOrderTaxAmount> miraklOrderTaxAmountConverter)
	{
		this.miraklOrderTaxAmountConverter = miraklOrderTaxAmountConverter;
	}

	private String getInRoundFormat(final String customFieldValue)
	{
		final double value = Double.parseDouble(customFieldValue);
		final DecimalFormat df = new DecimalFormat("#0.00");
		return df.format(value);
	}
}
