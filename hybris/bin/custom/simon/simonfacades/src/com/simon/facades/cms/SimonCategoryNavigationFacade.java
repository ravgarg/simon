package com.simon.facades.cms;

import de.hybris.platform.acceleratorcms.model.components.CategoryNavigationComponentModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * SimonCategoryNavigationFacade is responsible for populating DTO which contains L1,L2 and L3 nodes.
 *
 */
public interface SimonCategoryNavigationFacade
{

	/**
	 * Method is used to populate NavigationDTO {@link NavigationDTO} in which url,name and childnodes for L1,L2 and L3
	 * nodes are set. Category Navigation Service is called in which L3 nodes are fetched from database.
	 *
	 * @param component
	 *           component is the instance of category navigation component
	 * @return NavigationDTO navigationDTO is the DTO in which all the navigation node data is populated
	 */
	public List<Map<NavigationDTO, List<NavigationDTO>>> getNavigationNodes(CategoryNavigationComponentModel component);

	/**
	 * @return
	 */
	public Map<String, Collection<CategoryData>> getCategoryforMySize();

}
