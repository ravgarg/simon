package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


/**
 * populate AdditionalCartInfoModel from OrderConfirmCallBackDTO which have information of retailers, added products
 *
 */
public class ConfirmUpdateReversePopulator implements Populator<OrderConfirmCallBackDTO, AdditionalCartInfoModel>
{

	@Resource
	private Converter<OrderConfirmRetailersInfoCallBackDTO, RetailersInfoModel> confirmUpdateRetailersReverseConverter;
	@Resource
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	/**
	 * Method populate AdditionalCartInfoModel from OrderConfirmCallBackDTO which have information of retailers, added
	 * products.
	 */
	@Override
	public void populate(final OrderConfirmCallBackDTO source, final AdditionalCartInfoModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setCartId(source.getOrderId() != null ? source.getOrderId() : StringUtils.EMPTY);

		target.setTotalShippingPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getShippingPrice()));
		target.setFinalPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getFinalPrice()));
		target.setTotalSalesTax(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getTax()));
		target.setCouponValue(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getCouponValue()));

		target.setMessage(source.getMessage() != null ? source.getMessage() : StringUtils.EMPTY);
		target.setPendingConfirm(Boolean.valueOf(source.isPendingConfirm()));

		final List<OrderConfirmRetailersInfoCallBackDTO> retailersInfoDTOs = source.getRetailers();
		if (CollectionUtils.isNotEmpty(retailersInfoDTOs))
		{
			target.setRetailersInfo(confirmUpdateRetailersReverseConverter.convertAll(retailersInfoDTOs));
		}

	}



}
