
package com.simon.facades.search.converters.populator;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.ProductCategorySearchPagePopulator;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * This class overrides the ProductCategorySearchPagePopulator.
 *
 * @param <QUERY>
 * @param <STATE>
 * @param <RESULT>
 * @param <ITEM>
 * @param <SCAT>
 * @param <CATEGORY>
 */
public class ExtProductCategorySearchPagePopulator<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY>
		extends ProductCategorySearchPagePopulator<QUERY, STATE, RESULT, ITEM, SCAT, CATEGORY>

{

	/**
	 * This methods calls the super class method and then set the active designers in target.
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source,
			final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target) throws ConversionException
	{
		callSuperMethod(source, target);
		target.setActiveDesigners(source.getActiveDesigners());
	}

	/**
	 * Call super method.
	 *
	 * @param source
	 * @param target
	 */

	protected void callSuperMethod(final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source,
			final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target)
	{
		super.populate(source, target);
	}
}
