package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailerInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailerInfoCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


/**
 * populate RetailersInfoModel from OrderConfirmRetailersInfoCallBackDTO which have information of retailers, added
 * products.
 *
 */
public class ConfirmUpdateRetailersReversePopulator implements Populator<OrderConfirmRetailersInfoCallBackDTO, RetailersInfoModel>
{

	@Resource
	private Converter<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel> confirmUpdateProductReverseConverter;
	@Resource
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	@Resource
	private Converter<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel> confirmUpdateFailedProductReverseConverter;


	/**
	 * Method populate RetailersInfoModel from OrderConfirmRetailersInfoCallBackDTO which have information of retailers,
	 * added products.
	 */
	@Override
	public void populate(final OrderConfirmRetailersInfoCallBackDTO source, final RetailersInfoModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setRetailerId(source.getRetailerId() != null ? source.getRetailerId() : StringUtils.EMPTY);
		target.setTotalShippingPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getShippingPrice()));
		target.setFinalPrice(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getFinalPrice()));
		target.setTotalSalesTax(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getTax()));
		target.setCouponValue(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getCouponValue()));

		if (null != source.getCartFailedProducts())
		{
			target.setFailedProducts(confirmUpdateFailedProductReverseConverter.convertAll(source.getCartFailedProducts()));
		}
		if (null != source.getCartRemovedProducts())
		{
			target.setRemovedProducts(confirmUpdateProductReverseConverter.convertAll(source.getCartRemovedProducts()));
		}

		target.setOrderId(source.getOrderId() != null ? source.getOrderId() : StringUtils.EMPTY);

		final OrderConfirmRetailerInfoCallBackDTO orderConfirmRetailerInfoCallBackDTO = source.getRetailerInfo();
		if (null != orderConfirmRetailerInfoCallBackDTO)
		{
			RetailerInfoModel retailerInfoModel = target.getRetailerInfo();
			if (null == retailerInfoModel)
			{
				retailerInfoModel = new RetailerInfoModel();
			}
			retailerInfoModel.setShippingEstimate(orderConfirmRetailerInfoCallBackDTO.getShippingEstimate() != null
					? orderConfirmRetailerInfoCallBackDTO.getShippingEstimate() : StringUtils.EMPTY);
			target.setRetailerInfo(retailerInfoModel);
		}

		final List<OrderConfirmCartProductsCallBackDTO> confirmCartProductsCallBackDTOs = source.getCartProducts();
		if (CollectionUtils.isNotEmpty(confirmCartProductsCallBackDTOs))
		{
			target.setAddedProducts(confirmUpdateProductReverseConverter.convertAll(confirmCartProductsCallBackDTOs));
		}
	}

}
