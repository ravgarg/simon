package com.simon.facades.customer;

import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.Map;

import com.mirakl.hybris.facades.shop.ShopFacade;
import com.simon.core.model.CustomShopsComponentModel;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.shop.data.ShopData;


/**
 * Defines an API to perform various customer related operations in SPO
 */
public interface ExtShopFacade extends ShopFacade
{
	/**
	 * This method calls the converter to populate {@link StoreDataList} from {@link CustomShopsComponentModel}
	 *
	 * @param customShops
	 */
	public void loadCustomShops(final CustomShopsComponentModel customShops, final StoreDataList storesList);

	/**
	 * Update fav custom shops for my saved shops.
	 *
	 * @param customerData
	 *           the customer data
	 * @param storesList
	 *           the stores list
	 */
	void updateFavCustomShops(CustomerData customerData, StoreDataList storesList);

	/**
	 * This method return retailer list
	 *
	 * @return
	 */
	public Map<String, String> getRetailerList();

	/**
	 * @return
	 */
	public Map<String, String> getCustomerFavouritRetailerList();

	/**
	 * Method to get Shop data for shopId passed in.
	 *
	 * @param shopId
	 *           String
	 * @return ShopModel
	 *
	 */
	public ShopData getShop(String shopId);

}
