package com.simon.facades.populators;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.util.localization.Localization;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.enums.ContentPageType;
import com.simon.core.model.DealModel;
import com.simon.core.util.CommonUtils;
import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.data.OfferData;
import com.simon.facades.email.data.RetailerData;



/**
 * This is the Populator to populate {@link ShareEmailData} from {@link DealModel}
 */
public class ShareEmailDealDataPopulator implements Populator<DealModel, ShareEmailData>
{


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final DealModel source, final ShareEmailData target)
	{
		target.setSubject(getLocaleValue("text.share.email.deal.subject"));
		target.setSubscriptionDate(new Date().toString());
		populateRetailer(source, target);

	}

	private void populateRetailer(final DealModel source, final ShareEmailData target)
	{
		final RetailerData retailer = new RetailerData();
		if (null != source.getShop())
		{
			retailer.setName(source.getShop().getName());
			if (null != source.getShop().getLogo())
			{
				retailer.setLogo(source.getShop().getLogo().getURL());

			}
		}


		final OfferData offer = new OfferData();
		if (CollectionUtils.isNotEmpty(source.getPromotionSourceRule()))
		{
			for (final PromotionSourceRuleModel promotion : source.getPromotionSourceRule())
			{
				offer.setDescription(promotion.getDescription() != null ? promotion.getDescription() : StringUtils.EMPTY);
			}
		}
		else
		{
			offer.setDescription(StringUtils.EMPTY);
		}

		offer.setDisclaimer(source.getDescription() != null ? source.getDescription() : StringUtils.EMPTY);
		offer.setValid(CommonUtils.formateOfferValid(source.getStartDate(), source.getEndDate()));
		if (CollectionUtils.isNotEmpty(source.getContentPage()))
		{

			final List<ContentPageModel> contentpageList = (List<ContentPageModel>) source.getContentPage();
			for (final ContentPageModel contentpage : contentpageList)
			{
				if (null != contentpage && contentpage.getPageType() != null
						&& ContentPageType.LISTINGPAGE.equals(contentpage.getPageType()))
				{
					offer.setShopurl(contentpage.getLabel());
					break;
				}
			}

		}

		retailer.setOffer(offer);
		target.setRetailer(retailer);
	}

	protected String getLocaleValue(final String key)
	{
		return Localization.getLocalizedString(key);
	}



}
