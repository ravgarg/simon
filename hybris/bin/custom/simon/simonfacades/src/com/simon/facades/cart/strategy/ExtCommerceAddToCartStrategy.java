package com.simon.facades.cart.strategy;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import org.apache.commons.lang.StringUtils;


/**
 * Strategy to calculate cart.Recalculate cart is called to recalculate cart after merging entry.
 */
public class ExtCommerceAddToCartStrategy extends DefaultCommerceAddToCartStrategy
{

	/**
	 * Method addToCart is overrided method from the parent class for Add To Cart functionality and cart is recalculated
	 * after merge entry to update total price at order entry.
	 *
	 * @param parameter
	 * @return CommerceCartModification
	 */
	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{

		final CommerceCartModification modification = getCommerceCartModification(parameter);
		getCommerceCartCalculationStrategy().recalculateCart(parameter);
		return modification;
	}

	protected CommerceCartModification getCommerceCartModification(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final CommerceCartModification modification = doAddToCart(parameter);
		afterAddToCart(parameter, modification);
		mergeEntry(modification, parameter);
		return modification;
	}

	/**
	 * Do add to cart.
	 *
	 * @param parameter
	 *           the parameter
	 * @return the commerce cart modification
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	@Override
	protected CommerceCartModification doAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		CommerceCartModification modification;

		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		final long quantityToAdd = parameter.getQuantity();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

		this.beforeAddToCart(parameter);
		validateAddToCart(parameter);

		if (isProductForCode(parameter).booleanValue())
		{
			// So now work out what the maximum allowed to be added is (note that this may be negative!)
			final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
					deliveryPointOfService);
			final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();
			final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
			final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

			if (actualAllowedQuantityChange == quantityToAdd)
			{
				// We are allowed to add items to the cart
				final CartEntryModel entryModel = addCartEntry(parameter, actualAllowedQuantityChange);
				getModelService().save(entryModel);

				final String statusCode = getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
						quantityToAdd, cartLevelAfterQuantityChange);

				modification = createAddToCartResp(parameter, statusCode, entryModel, actualAllowedQuantityChange);
			}
			else
			{
				// Not allowed to add any quantity, or maybe even asked to reduce the quantity
				// Do nothing!
				String status = getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, maxOrderQuantity);
				if (actualAllowedQuantityChange < quantityToAdd)
				{
					status = CommerceCartModificationStatus.LOW_STOCK;
				}
				modification = createAddToCartResp(parameter, status, createEmptyCartEntry(parameter),
						actualAllowedQuantityChange > 0 ? actualAllowedQuantityChange : 0);

			}
		}
		else
		{
			modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
					createEmptyCartEntry(parameter), 0);
		}

		return modification;
	}


	@Override
	protected CartEntryModel addCartEntry(final CommerceCartParameter parameter, final long actualAllowedQuantityChange)
			throws CommerceCartModificationException
	{
		if (parameter.getUnit() == null)
		{
			parameter.setUnit(getUnit(parameter));
		}
		if (parameter.getProduct().getShop() == null || StringUtils.isEmpty(parameter.getProduct().getShop().getId()))
		{
			throw new CommerceCartModificationException(
					"Shop Model or Shop ID not found for Product: " + parameter.getProduct().getCode());
		}

		final CartEntryModel cartEntryModel = getCartService().addNewEntry(parameter.getCart(), parameter.getProduct(),
				actualAllowedQuantityChange, parameter.getUnit(), APPEND_AS_LAST, false);
		cartEntryModel.setDeliveryPointOfService(parameter.getPointOfService());
		cartEntryModel.setShopId(parameter.getProduct().getShop().getId());
		return cartEntryModel;
	}

}
