package com.simon.facades.deal.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;
import com.simon.facades.deal.ExtDealFacade;


/**
 * Deal facade
 */
public class ExtDealFacadeImpl implements ExtDealFacade
{


	/** The deal service. */
	@Resource
	private ExtDealService extDealService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DealModel getDealForCode(final String code)
	{
		return extDealService.getDealForCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DealModel> findListOfDeals()
	{
		return extDealService.findListOfDeals();
	}

	/**
	 * Gets the list of dealsModel. Returns matching deal for <code>code</code>
	 *
	 * @param objects
	 *           the deal <code>code</code>
	 * @return the {@link dealModel} for deal <code>code</code>
	 */
	@Override
	public List<DealModel> getListOfDealsForCodes(final List<String> objects)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", objects);
		return extDealService.getListOfDealsForCodes(objects);
	}



}
