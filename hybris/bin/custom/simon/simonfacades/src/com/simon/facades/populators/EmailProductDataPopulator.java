package com.simon.facades.populators;

import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;
import de.hybris.platform.converters.Populator;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.simon.facades.email.product.EmailProductData;
import com.simon.facades.email.product.EmailProductDetailsData;


/**
 * Converts ProductData to EmailProductData
 */
public class EmailProductDataPopulator implements Populator<ProductData, EmailProductData>
{

	private static final String LIST_PRICE = "LIST";
	private static final String SHARED_EMAIL_PRODUCT_QUANTITY = "1";
	private static final String MAX_SALE_RETAIL_PRICE = "MSRP";
	private static final String SALE_PRICE = "SALE";
	private static final String DEFUALT_IMAGE_KEY = "Desktop";
	private static final Double DOUBLE_VALUE_AS_ZERO = 0d;

	/**
	 * populates the ProductData from the contents in EmailProductData
	 */
	@Override
	public void populate(final ProductData source, final EmailProductData target)
	{
		target.setProductDetails(populateEmailProductDetailsData(source));
	}

	/**
	 * @description Method use to populate the product details that will use in the share product email.
	 * @method populateEmailProductDetailsData
	 * @param source
	 * @return
	 */
	private EmailProductDetailsData populateEmailProductDetailsData(final ProductData source)
	{
		final EmailProductDetailsData detailsData = new EmailProductDetailsData();

		final VariantOptionData variantOptionData = source.getVariantOptions().get(0);

		detailsData.setRetailerName(StringUtils.isNotEmpty(variantOptionData.getBaseProductRetailerName())
				? variantOptionData.getBaseProductRetailerName()
				: StringUtils.EMPTY);
		String imageUrl = StringUtils.EMPTY;
		final List<ImageData> imageDatas = variantOptionData.getVariantImage();
		for (final ImageData imageData : imageDatas)
		{
			if (StringUtils.isNotEmpty(imageData.getFormat()) && imageData.getFormat().equalsIgnoreCase(DEFUALT_IMAGE_KEY))
			{
				imageUrl = imageData.getUrl();
			}
		}
		detailsData.setImageUrl(imageUrl);

		detailsData.setPercentageOff(
				StringUtils.isNotEmpty(source.getPercentageOffRange()) ? source.getPercentageOffRange() : StringUtils.EMPTY);

		final Map<String, Pair<Double, Boolean>> variantPrice = variantOptionData.getVariantPrice();
		Double msrp = DOUBLE_VALUE_AS_ZERO;
		Double salePrice = DOUBLE_VALUE_AS_ZERO;
		Double listPrice = DOUBLE_VALUE_AS_ZERO;
		if (null != variantPrice)
		{
			final Pair<Double, Boolean> msrpPair = variantPrice.get(MAX_SALE_RETAIL_PRICE);
			final Pair<Double, Boolean> salePricePair = variantPrice.get(SALE_PRICE);
			final Pair<Double, Boolean> listPricePair = variantPrice.get(LIST_PRICE);
			if (null != msrpPair)
			{
				msrp = msrpPair.getKey();
			}
			if (null != salePricePair)
			{
				salePrice = salePricePair.getKey();
			}
			if (null != listPricePair)
			{
				listPrice = listPricePair.getKey();
			}
		}
		detailsData.setMsrp(DOUBLE_VALUE_AS_ZERO.equals(msrp) ? listPrice.toString() : msrp.toString());

		detailsData.setSalePrice(salePrice.toString());

		detailsData.setProductName(
				StringUtils.isNotEmpty(variantOptionData.getBaseProductName()) ? variantOptionData.getBaseProductName()
						: StringUtils.EMPTY);

		detailsData.setQuantity(SHARED_EMAIL_PRODUCT_QUANTITY);

		detailsData.setColor(
				StringUtils.isNotEmpty(variantOptionData.getColorName()) ? variantOptionData.getColorName() : StringUtils.EMPTY);
		detailsData.setShopNowLinkUrl(
				StringUtils.isNotEmpty(variantOptionData.getBaseProductUrl()) ? variantOptionData.getBaseProductUrl()
						: StringUtils.EMPTY);

		return detailsData;
	}

}
