package com.simon.facades.navigation.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.dto.NavigationData;
import com.simon.core.services.LeftNavigationCategoryService;
import com.simon.facades.navigation.LeftNavigationCategoryFacade;


/**
 * This class provides method which fetches category hierarchy of given L1 nodes and provides pair of L1 and leaf
 * category codes.
 */
public class LeftNavigationCategoryFacadeImpl implements LeftNavigationCategoryFacade
{

	@Resource
	private LeftNavigationCategoryService leftNavigationCategoryService;

	/**
	 * This method makes a pair of L1 category and leaf category from the category code
	 *
	 * @param categoryCode
	 *           category code part of URL
	 * @return Pair of L1 and leaf category
	 */
	@Override
	public Pair<String, String> getL1AndLeafCategoryCode(final String categoryCode)
	{
		final String[] catCodes = categoryCode.split(SimonCoreConstants.UNDERSCORE);
		return new ImmutablePair<>(catCodes[0], catCodes[catCodes.length - 1]);
	}

	/**
	 * This method returns complete category hierarchy corresponding to particular L1 category. If no entry is found
	 * corresponding to L1 category, empty list is returned.
	 */
	@Override
	public List<NavigationData> getNavigationData(final String l1categoryCode)
	{
		return leftNavigationCategoryService.getNavigationData(l1categoryCode);
	}

}
