package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.localization.Localization;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.services.RetailerService;
import com.simon.core.services.price.ExtPriceService;


/**
 * ExtPriceDataPopulator is implementation of Interface for a populator. A populator sets values in a target instance
 * based on values in the source instance. Populators are similar to converters except that unlike converters the target
 * instance must already exist.
 *
 * @param <AbstractOrderEntryModel>
 *           the type of the source object
 * @param <OrderEntryData>
 *           the type of the destination object
 */
public class ExtPriceDataPopulator<S extends AbstractOrderEntryModel, T extends OrderEntryData> implements Populator<S, T>
{

	@Resource
	private PriceDataFactory priceDataFactory;
	@Resource
	private ExtPriceService priceService;

	@Resource
	private RetailerService retailerService;

	/**
	 * This method will populate Product Prices.
	 *
	 * @param orderEntryModel
	 * @param orderEntryData
	 */
	@Override
	public void populate(final AbstractOrderEntryModel orderEntryModel, final OrderEntryData orderEntryData)
	{
		final PriceDataType priceType = PriceDataType.BUY;
		final String isoCode = null != orderEntryData.getBasePrice() ? orderEntryData.getBasePrice().getCurrencyIso() : null;
		if (null != isoCode)
		{
			populateAvailablePriceAsPerMatrix(orderEntryModel, orderEntryData, priceType, isoCode);
		}
	}

	/**
	 * This method will populate the Product Prices depending on the price availability. This will also mark the price if it
	 * be Strike-Off.
	 *
	 * @param orderEntryModel
	 * @param orderEntryData
	 * @param priceType
	 * @param isoCode
	 */
	private void populateAvailablePriceAsPerMatrix(final AbstractOrderEntryModel orderEntryModel,
			final OrderEntryData orderEntryData, final PriceDataType priceType, final String isoCode)
	{
		final Double msrp = orderEntryModel.getProduct().getMsrp();
		final Double listPrice = orderEntryModel.getListValue();
		final Double salePrice = orderEntryModel.getSaleValue();

		PriceData msrpValue = null;
		PriceData listValue = null;
		PriceData saleValue = null;
		PriceData youSaveValue = null;

		String percentSavingMessage = null;

		if (null != msrp && null != listPrice && null != salePrice)
		{
			msrpValue = priceDataFactory.create(priceType, BigDecimal.valueOf(msrp), isoCode);
			msrpValue.setStrikeOff(true);

			listValue = priceDataFactory.create(priceType, BigDecimal.valueOf(listPrice), isoCode);
			listValue.setStrikeOff(true);

			saleValue = priceDataFactory.create(priceType, BigDecimal.valueOf(salePrice), isoCode);
			youSaveValue = priceDataFactory.create(PriceDataType.BUY,
					BigDecimal.valueOf(msrp.doubleValue() - salePrice.doubleValue()), isoCode);

			percentSavingMessage = getPercentSaving(msrp.doubleValue(), salePrice.doubleValue()) + getPercentSavingMessage();

			fillOrderEntryDataPrices(orderEntryData, msrpValue, listValue, saleValue, youSaveValue, percentSavingMessage);
		}
		else if (null != msrp && null != listPrice)
		{
			msrpValue = priceDataFactory.create(priceType, BigDecimal.valueOf(msrp), isoCode);
			msrpValue.setStrikeOff(true);

			listValue = priceDataFactory.create(priceType, BigDecimal.valueOf(listPrice), isoCode);
			youSaveValue = priceDataFactory.create(PriceDataType.BUY,
					BigDecimal.valueOf(msrp.doubleValue() - listPrice.doubleValue()), isoCode);

			percentSavingMessage = getPercentSaving(msrp.doubleValue(), listPrice.doubleValue()) + getPercentSavingMessage();

			fillOrderEntryDataPrices(orderEntryData, msrpValue, listValue, saleValue, youSaveValue, percentSavingMessage);

		}
		else if (null == msrp && null != listPrice && null != salePrice)
		{
			listValue = priceDataFactory.create(priceType, BigDecimal.valueOf(listPrice), isoCode);
			listValue.setStrikeOff(true);

			saleValue = priceDataFactory.create(priceType, BigDecimal.valueOf(salePrice), isoCode);
			youSaveValue = priceDataFactory.create(PriceDataType.BUY,
					BigDecimal.valueOf(listPrice.doubleValue() - salePrice.doubleValue()), isoCode);

			percentSavingMessage = getPercentSaving(listPrice.doubleValue(), salePrice.doubleValue()) + getPercentSavingMessage();

			fillOrderEntryDataPrices(orderEntryData, msrpValue, listValue, saleValue, youSaveValue, percentSavingMessage);
		}
		else
		{
			listValue = priceDataFactory.create(priceType, BigDecimal.valueOf(listPrice), isoCode);
			orderEntryData.setListValue(listValue);

			fillOrderEntryDataPrices(orderEntryData, msrpValue, listValue, saleValue, youSaveValue, percentSavingMessage);
		}
	}

	/**
	 * Gets the localized percent saving message
	 *
	 * @return
	 */
	protected String getPercentSavingMessage()
	{
		return Localization.getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
	}

	private int getPercentSaving(final double strikeThroughPrice, final double sellingPrice)
	{
		return (int) Math.round(((strikeThroughPrice - sellingPrice) / strikeThroughPrice) * SimonCoreConstants.HUNDRED_INT);
	}

	private void fillOrderEntryDataPrices(final OrderEntryData orderEntryData, final PriceData msrpValue,
			final PriceData listPriceValue, final PriceData salePriceValue, final PriceData youSaveValue,
			final String percentSavingMessage)
	{
		orderEntryData.setMsrpValue(msrpValue);
		orderEntryData.setListValue(listPriceValue);
		orderEntryData.setSaleValue(salePriceValue);
		orderEntryData.setYouSave(youSaveValue);
		orderEntryData.setSavingsMessage(percentSavingMessage);
	}

}
