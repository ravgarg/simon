package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * Extended Address Populator
 */
public class ExtAddressPopulator extends AddressPopulator
{
	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		super.populate(source, target);
		if (StringUtils.isEmpty(target.getEmail()))
		{
			target.setEmail(((CustomerModel) userService.getCurrentUser()).getContactEmail());
		}

	}
}