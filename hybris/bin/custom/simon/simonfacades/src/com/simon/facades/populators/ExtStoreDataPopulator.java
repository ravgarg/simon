package com.simon.facades.populators;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.model.CustomShopsComponentModel;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * This populator class populates the {@link StoreDataList} using the data fetched from
 * {@link CustomShopsComponentModel}.
 */
public class ExtStoreDataPopulator implements Populator<CustomShopsComponentModel, StoreDataList>
{

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * This method takes the {@link CustomShopsComponentModel} as source and uses its value to populate
	 * target{@link StoreDataList}.
	 *
	 * @param source
	 *           of type {@link CustomShopsComponentModel}
	 * @param target
	 *           of type {@link StoreDataList}
	 */
	@Override
	public void populate(final CustomShopsComponentModel source, final StoreDataList target) throws ConversionException
	{
		final List<StoreData> storeDatas = new ArrayList<>();
		for (final ShopModel shop : source.getListOfShops())
		{
			if (null != shop && ShopState.OPEN.equals(shop.getState()))
			{
				final StoreData storeData = new StoreData();
				storeData.setId(shop.getId());
				storeData.setDescription(shop.getDescription());
				if (null != shop.getBackgroundImage())
				{
					storeData.setBackgroundImage(shop.getBackgroundImage().getURL());
					storeData.setAltTextBGImage(shop.getBackgroundImage().getAltText());
				}
				if (null != shop.getMobileLogo())
				{
					storeData.setMobileLogo(shop.getMobileLogo().getURL());
					storeData.setAltTextMobileLogo(shop.getMobileLogo().getAltText());
				}
				if (null != shop.getBanner())
				{
					storeData.setBanner(shop.getBanner().getURL());
				}
				if (null != shop.getLogo())
				{
					storeData.setLogo(shop.getLogo().getURL());
				}
				if (CollectionUtils.isNotEmpty(shop.getContentPage()))
				{
					for (final ContentPageModel contentModel : shop.getContentPage())
					{
						if (ContentPageType.LISTINGPAGE.equals(contentModel.getPageType()))
						{
							storeData.setContentPageLabel(contentModel.getLabel());
						}
					}
				}
				storeData.setName(shop.getName());

				storeData.setFavorite(extCustomerFacade.isFavStore(shop.getId()));
				storeDatas.add(storeData);
			}

		}
		target.setAllStoresList(storeDatas);

	}
}
