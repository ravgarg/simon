package com.simon.facades.search.converters.populator;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.search.FieldNameTranslator;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryGroupingPopulator;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrQuery;


/**
 * The Class ExtFacetSearchQueryGroupingPopulator.
 */
public class ExtFacetSearchQueryGroupingPopulator extends FacetSearchQueryGroupingPopulator
{
	@Resource
	private FieldNameTranslator fieldNameTranslator;

	/**
	 * Populate.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target) throws ConversionException
	{
		callSuperMethod(source, target);
		final SearchQueryTemplate searchQueryTemplate = source.getSearchQuery().getIndexedType().getSearchQueryTemplates()
				.get("SEARCH");
		if (CollectionUtils.isNotEmpty(source.getSearchQuery().getGroupCommands()) && null != searchQueryTemplate)
		{
			target.add("group.sort", new String[]
			{ searchQueryTemplate.getWithinGroupSortProperties() });
		}
	}

	protected void callSuperMethod(final SearchQueryConverterData source, final SolrQuery target) throws ConversionException
	{
		super.populate(source, target);
	}

}
