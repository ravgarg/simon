package com.simon.facades.customer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.impl.DefaultShopService;
import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.MallModel;
import com.simon.core.model.SizeModel;
import com.simon.core.services.ExtDealService;
import com.simon.core.services.ExtProductService;
import com.simon.core.services.ExtShopService;
import com.simon.core.services.MallService;
import com.simon.core.services.SizeService;
import com.simon.core.services.impl.DefaultDesignerService;
import com.simon.core.util.CommonUtils;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.SizeData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.checkout.data.StateData;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.customer.data.MonthData;
import com.simon.facades.favorite.data.FavoriteData;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.populators.ExtDealPopulator;
import com.simon.facades.product.data.GenderData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationService;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.services.UserIntegrationService;

import atg.taglib.json.util.JSONException;
import atg.taglib.json.util.JSONObject;


/**
 * This class is the Implementation class for the ExtCustomerFacade
 */
public class ExtCustomerFacadeImpl extends DefaultCustomerFacade implements ExtCustomerFacade
{
	private static final String FIND_ACTIVE_DESIGNERS = "findActiveDesigners";

	private static final Logger LOG = LoggerFactory.getLogger(ExtCustomerFacadeImpl.class);

	private static final int HOUSEHOLD_INCOME_CODE = 9;
	public static final String COUNTRY_CODE_US = "US";
	public static final String COUNTRY_CODE_CA = "CA";

	private static final String ACTION = "action";
	private static final String STATUS = "status";
	private static final String SUCCESS = "success";
	private static final String FAIL = "fail";
	private static final String ERRORKEYSAVINGCUSTOMER = "Error in saving CustomerModel: ";

	@Resource
	private ExtDealPopulator extDealPopulator;

	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	@Resource(name = "customerGenderConverter")
	private Converter<CustomerGender, GenderData> customerGenderConverter;

	@Resource(name = "defaultMonthConverter")
	private Converter<Months, MonthData> defaultMonthConverter;

	@Resource(name = "extCustomerAccountService")
	private ExtCustomerAccountService extCustomerAccountService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource
	private UserIntegrationService userIntegrationService;

	@Resource(name = "extCustomerReversePopulator")
	private Populator<CustomerData, UserModel> extCustomerReversePopulator;
	@Resource
	private Converter<MallModel, MallData> mallConverter;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "extCheckoutFacade")
	private ExtCheckoutFacade extCheckoutFacade;
	/** The ext Designer converter. */
	@Resource(name = "extDesignerConverter")
	private Converter<DesignerModel, DesignerData> extDesignerConverter;
	/** The ext store converter. */
	@Resource(name = "extStoreConverter")
	private Converter<ShopModel, StoreData> extStoreConverter;
	@Resource
	private DefaultDesignerService defaultDesignerService;

	@Resource(name = "extProductService")
	private ExtProductService extProductService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "userFavouritesIntegrationService")
	private UserFavouritesIntegrationService userFavouritesIntegrationService;

	@Autowired
	private ExtShopService extShopService;

	@Resource(name = "mallService")
	private MallService mallService;

	@Resource(name = "registerCustomerReversePopulator")
	private Populator<RegisterData, CustomerModel> registerCustomerReversePopulator;

	@Resource
	private DefaultShopService defaultShopService;
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource
	private ExtDealService extDealService;

	@Resource(name = "extDealConverter")
	private Converter<DealModel, DealData> extDealConverter;

	@Resource
	private SizeService sizeService;


	/** The size converter. */
	@Resource(name = "extSizeConverter")
	private Converter<SizeModel, SizeData> extSizeConverter;


	/**
	 * This method is implementation of ExtCustomerFacade registerShopper. It used to Register a user in SPO.com and also
	 * sends the data for registration to PO.com
	 *
	 * @param registerData
	 *           the user data the user will be registered with
	 * @param extRegistrationRequired
	 *           registration needed or not
	 * @throws DuplicateUidException
	 *            if the login is not unique
	 * @throws UserIntegrationException
	 *            if there is an exception while registering to PO.com.
	 * @throws AuthLoginIntegrationException
	 */
	@Override
	public void registerShopper(final RegisterData registerData, final boolean extRegistrationRequired)
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		validateUserParameter(registerData);
		addAdditionalAttributesForRegistration(registerData);
		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		if (extRegistrationRequired)
		{
			final VIPUserDTO registeredUser = userIntegrationService.registerUser(registerData);
			registerData.setExternalId(registeredUser.getId());
		}
		registerCustomerReversePopulator.populate(registerData, newCustomer);
		getCustomerAccountService().register(newCustomer, null);
	}

	/**
	 * This method convert guest user to register with saved order
	 *
	 * @param registerData
	 * @param orderGUID
	 * @throws DuplicateUidException
	 * @throws UserIntegrationException
	 * @throws AuthLoginIntegrationException
	 */
	@Override
	public void registerGuestCustomer(final RegisterData registerData, final String orderId)
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		validateUserParameter(registerData);
		addAdditionalAttributesForRegistration(registerData);
		registerData.setPrimaryMall(findMallDataFromZipCode(registerData.getZipcode()));
		final CustomerModel newCustomer = getModelService().create(CustomerModel.class);
		newCustomer.setExternalId(registerData.getExternalId());
		final VIPUserDTO registeredUser = userIntegrationService.registerUser(registerData);
		newCustomer.setExternalId(registeredUser.getId());
		if (StringUtils.isNotEmpty(registerData.getGender()))
		{
			newCustomer.setGender(CustomerGender.valueOf(registerData.getGender()));
		}

		if (StringUtils.isNotEmpty(registerData.getBirthMonth()))
		{
			newCustomer.setBirthMonth(enumerationService.getEnumerationValue(Months.class, registerData.getBirthMonth()));
		}

		newCustomer.setBirthYear(registerData.getBirthYear());
		if (StringUtils.isNotEmpty(registerData.getCountry()))
		{
			newCustomer.setCountry(getCommonI18NService().getCountry(registerData.getCountry()));
		}
		final MallModel mallModel = mallService.getMallForCode(registerData.getPrimaryMall().getCode());
		newCustomer.setPrimaryMall(mallModel);
		newCustomer.setZipCode(registerData.getZipcode());
		newCustomer.setName(getCustomerNameStrategy().getName(registerData.getFirstName(), registerData.getLastName()));
		setUidForRegister(registerData, newCustomer);
		newCustomer.setSessionLanguage(getCommonI18NService().getCurrentLanguage());
		newCustomer.setSessionCurrency(getCommonI18NService().getCurrentCurrency());
		extCustomerAccountService.convertGuestToRegisterCustomer(newCustomer, orderId);
	}

	/**
	 * Get Mall Data using ZipCode
	 *
	 * @param zipcode
	 * @return mallData
	 * @throws UserIntegrationException
	 */
	private MallData findMallDataFromZipCode(final String zipcode) throws UserIntegrationException
	{
		final MallData mallData = new MallData();
		final UserCenterIdDTO userCenterIdDTO = userIntegrationService.getDefaultCenter(zipcode);
		mallData.setCode(userCenterIdDTO.getId());
		mallData.setName(userCenterIdDTO.getOutletName());
		return mallData;
	}

	/**
	 * Validate user parameter.
	 *
	 * @param registerData
	 *           the register data
	 */
	private void validateUserParameter(final RegisterData registerData)
	{
		validateParameterNotNullStandardMessage("registerData", registerData);
		validateParameterNotNullStandardMessage(registerData.getFirstName(), "The field [FName] cannot be empty");
		validateParameterNotNullStandardMessage(registerData.getLastName(), "The field [LName] cannot be empty");
		validateParameterNotNullStandardMessage(registerData.getLogin(), "The field [Logdedin] cannot be empty");
	}

	/**
	 * Get all country In System to display Country List .
	 *
	 * @return List
	 */
	@Override
	public List<CountryData> getAllCountry()
	{
		final List<CountryData> listCountries = countryConverter.convertAll(getCommonI18NService().getAllCountries());
		final List<CountryData> listCountries2 = listCountries.stream()
				.filter(value -> !(COUNTRY_CODE_CA.equalsIgnoreCase(value.getIsocode())
						|| COUNTRY_CODE_US.equalsIgnoreCase(value.getIsocode())))
				.collect(Collectors.toList());
		final Comparator<CountryData> countryComparator = (c1, c2) -> c1.getName().compareTo(c2.getName());
		listCountries2.sort(countryComparator);
		final List<CountryData> listCountriescomplete = listCountries.stream().filter(
				value -> COUNTRY_CODE_CA.equalsIgnoreCase(value.getIsocode()) || COUNTRY_CODE_US.equalsIgnoreCase(value.getIsocode()))
				.collect(Collectors.toList());
		listCountriescomplete.sort((c1, c2) -> c2.getName().compareTo(c1.getName()));
		listCountriescomplete.addAll(listCountries2);
		return listCountriescomplete;
	}

	/**
	 * This is the method to get all Gender in list to display Gender list.
	 *
	 * @return List
	 */
	@Override
	public List<GenderData> getGenderList()
	{
		return customerGenderConverter.convertAll(enumerationService.getEnumerationValues(CustomerGender.class));
	}

	/**
	 * This is to get all Months list to display months List .
	 *
	 * @return List
	 */
	@Override
	public List<MonthData> getMonthsList()
	{
		return defaultMonthConverter.convertAll(enumerationService.getEnumerationValues(Months.class));
	}


	/*
	 * This method is to update the profile info of customer
	 *
	 * @throws DuplicateUidException if the login is not unique
	 *
	 * @throws UserIntegrationException if there is an exception while registering to PO.com.
	 *
	 */
	@Override
	public void updateProfileInformation(final CustomerData customerData, final boolean extUpdateRequired)
			throws DuplicateUidException, UserIntegrationException
	{
		CustomerModel customerModel;
		validateDataBeforeUpdate(customerData);
		addAdditionalAttributesForUpdate(customerData);
		if (extUpdateRequired)
		{
			userIntegrationService.updateUserDetails(customerData);
			customerModel = getCurrentSessionCustomer();

		}
		else
		{
			customerModel = (CustomerModel) getUserService().getUserForUID(StringUtils.lowerCase(customerData.getUid()));
		}


		getExtCustomerReversePopulator().populate(customerData, customerModel);
		extCustomerAccountService.updateProfileInfo(customerModel);

	}

	/**
	 * Method to get list of StateData for Shipping Address.
	 *
	 * @return list of StateData.
	 *
	 */
	@Override
	public List<StateData> getStateDataListForShippingAddress()
	{
		final String countryIsoCode = configurationService.getConfiguration()
				.getString(SimonFacadesConstants.SIMON_COUNTRY_ISOCODE);
		final List<StateData> listOfStateData = new ArrayList<>();

		final List<String> listOfExcludedStatesList = extCheckoutFacade.getExcludedStateList();
		final List<RegionData> listOfRegionData = i18NFacade.getRegionsForCountryIso(countryIsoCode);

		if (listOfExcludedStatesList != null && CollectionUtils.isNotEmpty(listOfRegionData))
		{
			for (final RegionData regionData : listOfRegionData)
			{
				if (!listOfExcludedStatesList.contains((regionData.getIsocode())))
				{
					final StateData stateData = new StateData();
					stateData.setStateCode(regionData.getIsocode());
					stateData.setStateName(regionData.getName());
					stateData.setIsocodeShort(regionData.getIsocodeShort());
					listOfStateData.add(stateData);
				}
			}
		}

		return listOfStateData;
	}

	/*
	 * This method is to validate the customer data
	 *
	 */
	@Override
	public void validateDataBeforeUpdate(final CustomerData customerData)
	{
		validateParameterNotNullStandardMessage("customerData", customerData);
		Assert.hasText(customerData.getFirstName(), "The field [FirstName] cannot be empty");
		Assert.hasText(customerData.getLastName(), "The field [LastName] cannot be empty");
		Assert.hasText(customerData.getGender().getCode(), "The field [Gender] cannot be empty");
		Assert.hasText(customerData.getBirthYear().toString(), "The field [BirthYear] cannot be empty");
	}

	/**
	 * This method is used to set additional parameters needed for updating in PO.com
	 *
	 * @param CustomerData
	 */
	private void addAdditionalAttributesForUpdate(final CustomerData customerData)
	{
		customerData.setHouseholdIncomeCode(HOUSEHOLD_INCOME_CODE);
	}

	/**
	 * This method is used to set additional parameters needed for registering in PO.com
	 *
	 * @param registerData
	 */
	private void addAdditionalAttributesForRegistration(final RegisterData registerData)
	{
		registerData.setHouseholdIncomeCode(HOUSEHOLD_INCOME_CODE);
	}


	/**
	 * This method will update the designer data to the customer.
	 */
	@Override
	public boolean updateDesignersToProfile(final String[] designerIds)
	{
		boolean status = true;
		final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
		final Set<DesignerModel> designerSet = new HashSet<>();
		try
		{
			final List<DesignerModel> designerModels = defaultDesignerService.getListOfDesignerForCodes(Arrays.asList(designerIds));
			designerSet.addAll(customer.getMyDesigners());
			designerSet.addAll(designerModels);
			customer.setMyDesigners(designerSet);
			extCustomerAccountService.updateProfileInfo(customer);
		}
		catch (final ModelSavingException | DuplicateUidException e)
		{
			LOG.error(ERRORKEYSAVINGCUSTOMER, e);
			status = false;
		}

		//PO.com integration service
		for (final String designerCode : designerIds)
		{
			userFavouritesIntegrationService.addUserFavouriteDesigner(designerCode, designerCode);
		}
		return status;
	}

	/**
	 * This method will fetch all the linked designer data to the customer.
	 */
	@Override
	public List<DesignerData> fetchAllDesignersExceptCustomerFavDesigners()
	{
		final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
		final Set<String> designerCodes = customer.getMyDesigners().stream().map(DesignerModel::getCode)
				.collect(Collectors.toSet());
		final List<DesignerData> designerDatas = getActiveRetailersDesignerList();
		return CommonUtils.getDesignerOrderByName(
				designerDatas.stream().filter(designer -> !designerCodes.contains(designer.getCode())).collect(Collectors.toList()));
	}

	/**
	 * @return the extCustomerReversePopulator
	 */
	public Populator<CustomerData, UserModel> getExtCustomerReversePopulator()
	{
		return extCustomerReversePopulator;
	}

	/**
	 * @param extCustomerReversePopulator
	 *           the extCustomerReversePopulator to set
	 */
	@Required
	public void setExtCustomerReversePopulator(final Populator<CustomerData, UserModel> extCustomerReversePopulator)
	{
		this.extCustomerReversePopulator = extCustomerReversePopulator;
	}

	/**
	 * update the to my favorite.
	 *
	 * @param products
	 *           the products
	 * @return true, if successful
	 */
	@Override
	public boolean updateMyFavorite(final List<String> products)
	{
		boolean status = true;
		if (checkIfUserIsLoggedIn())
		{
			try
			{
				final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
				final Set<ProductModel> productset = new HashSet<>();
				if (CollectionUtils.isNotEmpty(products))
				{
					products.forEach(productCode -> {
						prepareProductSet(productset, productCode);

					});
				}
				customer.setMyFavProduct(productset);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
		}
		return status;
	}

	private void prepareProductSet(final Set<ProductModel> productset, final String productCode)
	{
		try
		{
			final ProductModel product = extProductService.getProductForCode(productCode);
			if (null != product)
			{
				productset.add(product);
			}
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error("Product not found : ", e);
		}
	}

	/**
	 * This method will give all the Favourite designers of current user from PO.Com.
	 *
	 * @throws DuplicateUidException
	 */
	@Override
	public boolean getAllFavouriteDesigners(final List<DesignerData> designerDatas) throws DuplicateUidException
	{
		final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
		if (isEsbPoDisabled())
		{
			designerDatas.addAll(Converters.convertAll(customer.getMyDesigners(), extDesignerConverter));
			return true;
		}
		try
		{
			final List<String> favDesigners = userFavouritesIntegrationService.getAllFavouriteDesigners();
			if (CollectionUtils.isNotEmpty(favDesigners))
			{
				designerDatas.addAll(fetchAndSaveAllDesigners(favDesigners, customer));
			}
			else
			{
				final Set<DesignerModel> designerSet = new HashSet<>();
				customer.setMyDesigners(designerSet);
				extCustomerAccountService.updateProfileInfo(customer);
			}
			return true;
		}
		catch (final UserFavouritesIntegrationException e)
		{
			LOG.error("Error occurred while getting fav designers", e);
			return false;
		}
	}


	/**
	 * This method is used to fetch array of designerModels from Database and save to current customer.
	 *
	 * @param designers
	 * @param customer
	 * @param favDesigners
	 * @throws DuplicateUidException
	 */
	private List<DesignerData> fetchAndSaveAllDesigners(final List<String> designers, final CustomerModel customer)
			throws DuplicateUidException
	{
		final List<DesignerModel> designerModels = defaultDesignerService.getListOfDesignerForCodes(designers);
		if (CollectionUtils.isNotEmpty(designerModels))
		{
			final Set<DesignerModel> designerSet = new HashSet<>(designerModels);
			customer.setMyDesigners(designerSet);
			extCustomerAccountService.updateProfileInfo(customer);
			// getting active retailer's designer from solr
			return getActiveRetailersDesignerList().stream().filter(designer -> designers.contains(designer.getCode()))
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	/**
	 * This method will get all the active designer list from SOLR.
	 *
	 * @return list of active designers
	 */
	public List<DesignerData> getActiveRetailersDesignerList()
	{
		List<DesignerData> designerDatas = new ArrayList<>();
		getSessionService().setAttribute(FIND_ACTIVE_DESIGNERS, Boolean.TRUE);
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(StringUtils.EMPTY,
				SearchQueryContext.DESIGNERS);
		if (null != searchPageData)
		{
			designerDatas = searchPageData.getActiveDesigners();
		}
		return designerDatas;
	}

	/**
	 * Adds the to my favorite.
	 *
	 * @param productCode
	 *           the product code
	 * @param productName
	 *           the product name
	 * @return true, if successful
	 */
	@Override
	public boolean addToMyFavorite(final String productCode, final String productName)
	{
		final UserModel user = getUserService().getCurrentUser();
		boolean status = true;
		if (user instanceof CustomerModel && !getUserService().isAnonymousUser(user))
		{
			try
			{
				final CustomerModel customer = (CustomerModel) user;

				Set<ProductModel> productset = new HashSet<>();
				if (CollectionUtils.isNotEmpty(customer.getMyFavProduct()))
				{
					productset = customer.getMyFavProduct().stream().collect(Collectors.toSet());
				}
				final ProductModel product = extProductService.getProductForCode(productCode);
				if (null != product)
				{
					productset.add(product);
				}
				customer.setMyFavProduct(productset);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}

			userFavouritesIntegrationService.addUserFavouriteProduct(productCode, productName);
		}
		return status;
	}

	@Override
	public boolean checkIfUserIsLoggedIn()
	{
		final UserModel user = getUserService().getCurrentUser();
		return (user instanceof CustomerModel && !getUserService().isAnonymousUser(user));
	}

	/**
	 * Remove from my favorite.
	 *
	 * @param productCode
	 *           the product code
	 * @param productName
	 *           the product name
	 * @return true, if successful
	 */
	@Override
	public boolean removeFromMyFavorite(final String productCode, final String productName)
	{
		final UserModel user = getUserService().getCurrentUser();
		boolean status = true;
		if (user instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customer = (CustomerModel) user;
				final Set<ProductModel> products = customer.getMyFavProduct().stream().filter(p -> !productCode.equals(p.getCode()))
						.collect(Collectors.toSet());
				customer.setMyFavProduct(products);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
			userFavouritesIntegrationService.removeUserFavouriteProduct(productCode);
		}

		return status;
	}

	/**
	 * Gets the my favorite.
	 *
	 * @return the my favorite products list
	 */
	@Override
	public List<String> getMyFavoriteProductCodes()
	{
		final UserModel user = getUserService().getCurrentUser();
		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			return customer.getMyFavProduct().stream().map(ProductModel::getCode).collect(Collectors.toList());
		}
		return Collections.emptyList();

	}

	/**
	 * Get my favorite from PS.com.
	 *
	 * @return the my favorite from PS
	 */
	@Override
	public List<String> getMyFavoriteFromPO()
	{
		if (isEsbPoDisabled())
		{
			return getMyFavoriteProductCodes();
		}
		return userFavouritesIntegrationService.getAllFavouriteProducts();
	}

	/**
	 * Get my favorite. *
	 *
	 * @param products
	 *           the products
	 * @return the list
	 */
	@Override
	public List<ProductData> populateMyFavorite(final List<ProductData> products)
	{
		final List<String> favProductCodes = getMyFavoriteProductCodes();
		products.forEach(p -> {
			if (favProductCodes.contains(p.getBaseProduct()))
			{
				p.setFavorite(true);
			}
		});
		return products;
	}

	/**
	 * Remove from my designers.
	 *
	 * @param designerCode
	 *           the designer code
	 * @return true, if successful
	 */
	@Override
	public boolean removeFromMyDesigners(final String designerCode)
	{
		final UserModel user = getUserService().getCurrentUser();
		boolean status = true;
		if (user instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customer = (CustomerModel) user;
				final Set<DesignerModel> designer = customer.getMyDesigners().stream().filter(p -> !designerCode.equals(p.getCode()))
						.collect(Collectors.toSet());
				customer.setMyDesigners(designer);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
			userFavouritesIntegrationService.removeUserFavouriteDesigner(designerCode);
		}

		return status;
	}

	/**
	 * Create JSON Object for unfavorite from profile.
	 *
	 * @param code
	 * @param name
	 * @param action
	 * @param status
	 * @return JSON string
	 * @throws JSONException
	 */
	@Override
	public String createJsonForFavouriteUnFavourite(final String code, final String name, final boolean status,
			final String action) throws JSONException
	{
		final JSONObject json = new JSONObject();
		json.put(ACTION, action);
		json.put("code", code);
		if (status)
		{
			json.put(STATUS, SUCCESS);
		}
		else
		{
			json.put(STATUS, FAIL);
			json.put("failure", "Add to my Fav returning failure for" + code);
			LOG.info(String.format("Add to my Fav returning false for code : %s and name: %s and returning json:: %s", code, name,
					json.toString()));
		}
		return json.toString();
	}

	/**
	 * Remove from my favorite.
	 *
	 * @param designerCode
	 *           the designer code
	 * @param designerName
	 *           the product name
	 * @return true, if successful
	 */
	@Override
	public boolean addToMyDesigners(final String designerCode, final String designerName)
	{
		final CustomerModel user = (CustomerModel) getUserService().getCurrentUser();
		try
		{
			final Set<DesignerModel> designerSet = new HashSet<>(user.getMyDesigners());
			final DesignerModel designerModel = defaultDesignerService.getDesignerForCode(designerCode);
			designerSet.add(designerModel);
			user.setMyDesigners(designerSet);
			extCustomerAccountService.updateProfileInfo(user);
			userFavouritesIntegrationService.addUserFavouriteDesigner(designerCode, designerName);
			return true;
		}
		catch (final ModelSavingException | DuplicateUidException e)
		{
			LOG.error(ERRORKEYSAVINGCUSTOMER, e);
			return false;
		}
	}


	/**
	 * Remove from my stores.
	 *
	 * @param code
	 *           the store code
	 * @return true, if successful
	 */
	@Override
	public boolean removeFromMyStores(final String code)
	{

		final UserModel user = getUserService().getCurrentUser();
		boolean status = true;
		if (user instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customer = (CustomerModel) user;
				final Set<ShopModel> shopSet = customer.getShops().stream().filter(p -> !code.equals(p.getId()))
						.collect(Collectors.toSet());
				customer.setShops(shopSet);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
			userFavouritesIntegrationService.removeUserFavouriteStore(code);
		}

		return status;

	}



	@Override
	public boolean addToMyStores(final String storeCode, final String storeName)
	{
		final UserModel user = getUserService().getCurrentUser();
		boolean status = true;
		if (user instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customer = (CustomerModel) user;
				Set<ShopModel> shopSet = new HashSet<>();

				final ShopModel storeModel = defaultShopService.getShopForId(storeCode);
				if (CollectionUtils.isNotEmpty(customer.getShops()))
				{
					shopSet = customer.getShops().stream().collect(Collectors.toSet());
				}
				shopSet.add(storeModel);

				customer.setShops(shopSet);
				extCustomerAccountService.updateProfileInfo(customer);

			}
			catch (final ModelSavingException | DuplicateUidException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}

			userFavouritesIntegrationService.addUserFavouriteStore(storeCode, storeName);
		}

		return status;
	}



	@Override
	public boolean isFavStore(final String storeCode)
	{
		boolean isFavStore = false;
		final UserModel user = getUserService().getCurrentUser();
		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			if (null != storeCode && CollectionUtils.isNotEmpty(customer.getShops()))
			{
				for (final ShopModel shop : customer.getShops())
				{
					if (storeCode.equalsIgnoreCase(shop.getId()))
					{
						isFavStore = true;
					}
				}
			}
		}
		return isFavStore;
	}


	/**
	 * getMyFavProductOrderByDate *
	 *
	 * @param products
	 *           the products
	 * @return the list
	 */
	@Override
	public List<ProductData> getMyFavProductOrderByDate(final List<ProductData> products)
	{
		final UserModel user = getUserService().getCurrentUser();
		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			final List<List<Object>> myfavProducts = extCustomerAccountService.getMyFavProductOrderByDate(customer);
			myfavProducts.forEach(favProducts -> {
				final Date date1 = CommonUtils.convertStringToDate(favProducts.get(0).toString());
				final ProductModel product = (ProductModel) favProducts.get(1);
				products.forEach(p -> {
					if (product.getCode().equals(p.getBaseProduct()))
					{
						p.setModifiedTime(date1);
					}
				});
			});
		}

		products.sort((m1, m2) -> {
			if (null != m1.getModifiedTime() && null != m2.getModifiedTime())
			{
				return m1.getModifiedTime().compareTo(m2.getModifiedTime());
			}
			return -1;
		});
		return products;

	}

	@Override
	public boolean updateStoreToProfile(final CustomerData customerData, final String[] storeIds)
	{
		boolean status = true;
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerData.getUid());
		Set<ShopModel> shopSet = new HashSet<>();
		try
		{
			final List<ShopModel> shopModels = extShopService.getListOfShopsForCodes(storeIds);
			if (CollectionUtils.isNotEmpty(customer.getShops()))
			{
				shopSet = customer.getShops().stream().collect(Collectors.toSet());
			}
			shopSet.addAll(shopModels);

			customer.setShops(shopSet);
			extCustomerAccountService.updateProfileInfo(customer);
		}
		catch (final ModelSavingException | DuplicateUidException e)
		{
			LOG.error(ERRORKEYSAVINGCUSTOMER, e);
			status = false;
		}
		//PO.com integration service
		for (final String shopCode : storeIds)
		{
			userFavouritesIntegrationService.addUserFavouriteStore(shopCode, shopCode);
		}
		return status;
	}

	@Override
	public List<StoreData> fetchAllStoresExceptLinked(final CustomerData customerData)
	{
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerData.getUid());
		final List<ShopModel> shopList = extCustomerAccountService.findListOfAllStores(customer);
		return Converters.convertAll(shopList, extStoreConverter);
	}

	@Override
	public boolean getAllFavouriteStores(final CustomerData customerData) throws DuplicateUidException
	{
		boolean status = true;
		List<String> favStores = new ArrayList<>();
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerData.getUid());
		try
		{
			favStores = getAllFavouriteStoresFromPO();
		}
		catch (final UserFavouritesIntegrationException e)
		{
			LOG.error("Error occurred :" + e);
			status = false;
		}
		if (CollectionUtils.isNotEmpty(favStores))
		{
			fetchAndSaveAllStores(favStores.toArray(new String[favStores.size()]), customer);
		}
		else
		{
			final Set<ShopModel> storeSet = new HashSet<>();
			customer.setShops(storeSet);
			extCustomerAccountService.updateProfileInfo(customer);
		}
		getCustomerConverter().convert(customer, customerData);
		return status;
	}

	/**
	 * @return
	 */
	@Override
	public List<String> getAllFavouriteStoresFromPO()
	{
		if (isEsbPoDisabled())
		{
			final UserModel user = getUserService().getCurrentUser();
			if (user instanceof CustomerModel)
			{
				final CustomerModel customer = (CustomerModel) user;
				final List<String> shops = new ArrayList<>();
				customer.getShops().forEach(shop -> shops.add(shop.getId()));
				return shops;
			}
		}
		return userFavouritesIntegrationService.getAllFavouriteStores();
	}

	/**
	 * This method is used to fetch array of shopModels from Database and save to current customer.
	 *
	 * @param stores
	 * @param customer
	 * @throws DuplicateUidException
	 */
	private void fetchAndSaveAllStores(final String[] stores, final CustomerModel customer) throws DuplicateUidException
	{
		List<ShopModel> storeModels = extShopService.getListOfShopsForCodes(stores);

		Set<ShopModel> storeSet;
		if (null == storeModels)
		{
			storeModels = Collections.emptyList();
		}
		storeSet = new HashSet<>(storeModels);
		customer.setShops(storeSet);
		extCustomerAccountService.updateProfileInfo(customer);
	}


	/**
	 * This method will give the primary Mall Data of the current customer
	 *
	 * @return MallData
	 *
	 */
	@Override
	public MallData getCustomerPrimaryMallData()
	{
		MallData mallData = null;
		final MallModel mallModel = getCurrentSessionCustomer().getPrimaryMall();
		if (null != mallModel)
		{
			mallData = mallConverter.convert(mallModel);
		}
		return mallData;
	}


	/**
	 * This method will fetch all the designer data.
	 */
	@Override
	public List<DesignerData> fetchAllDesigners()
	{
		final List<DesignerModel> designerList = extCustomerAccountService.findListOfDesigners();
		final List<DesignerData> designerDatas = Converters.convertAll(designerList, extDesignerConverter);
		return CommonUtils.getDesignerOrderByName(designerDatas);
	}

	/*
	 * This method tests my favorite designers are not empty and then favorite flag is set for available my designers.
	 *
	 * @param customerData of type {@link CustomerData} is used to fetch saved shops data
	 *
	 * @param designerList of type {@link Set<DesignerData>} is updated for favorite designers
	 */
	@Override
	public boolean updateFavDesigners(final CustomerData customerData, final List<DesignerData> designerList)
	{
		if (!CollectionUtils.isEmpty(customerData.getMyDesigners()))
		{
			setFavoriteFlag(designerList, customerData.getMyDesigners());
			return true;
		}
		return false;
	}

	/*
	 * This method tests of designers exist in my saved designers, if yes a flag is set to true.
	 *
	 * @param myDesigners of type {@link Set<DesignerData>} is used to fetch saved shops data
	 *
	 * @param designer of type {@link Set<DesignerData>} is updated for favorite stores
	 */
	private void setFavoriteFlag(final List<DesignerData> allDesigner, final Set<DesignerData> customerDesigners)
	{

		allDesigner.stream().filter(e -> CollectionUtils.intersection(customerDesigners, allDesigner).contains(e))
				.forEach(e -> e.setFavorite(true));

	}

	@Override
	public boolean isFavDesigner(final String code)
	{
		boolean isFavDesigner = false;
		final UserModel user = getUserService().getCurrentUser();

		if (user instanceof CustomerModel && null != code)
		{

			final CustomerModel customer = (CustomerModel) user;
			if (CollectionUtils.isNotEmpty(customer.getMyDesigners()))
			{
				for (final DesignerModel designer : customer.getMyDesigners())
				{
					if (code.equalsIgnoreCase(designer.getCode()))
					{
						isFavDesigner = true;
						break;
					}
				}
			}
		}
		return isFavDesigner;
	}

	@Override
	public boolean removeFavDeal(final String dealCode)
	{
		final UserModel userModel = getUserService().getCurrentUser();
		boolean status = true;
		if (userModel instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customerModel = (CustomerModel) userModel;

				final Set<DealModel> dealModelSet = customerModel.getMyOffers().stream().filter(p -> !dealCode.equals(p.getCode()))
						.collect(Collectors.toSet());

				customerModel.setMyOffers(dealModelSet);

				extCustomerAccountService.updateCustomerMyFavorite(customerModel);

				userFavouritesIntegrationService.removeUserFavouriteOffer(dealCode);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
		}
		LOG.debug("ExtCustomerFacadeImpl ::: removeFavDeal :: status : {} ", status);

		return status;
	}

	@Override
	public boolean addFavDeal(final String dealCode, final String dealName)
	{
		final UserModel userModel = getUserService().getCurrentUser();
		boolean status = true;
		if (userModel instanceof CustomerModel)
		{
			try
			{
				final CustomerModel customerModel = (CustomerModel) userModel;

				Set<DealModel> addedDealModels = new HashSet<>();

				if (CollectionUtils.isNotEmpty(customerModel.getMyOffers()))
				{
					addedDealModels = customerModel.getMyOffers().stream().collect(Collectors.toSet());
				}

				final DealModel newDealModel = extDealService.getDealForCode(dealCode);

				if (null != newDealModel)
				{
					addedDealModels.add(newDealModel);

					customerModel.setMyOffers(addedDealModels);

					extCustomerAccountService.updateProfileInfo(customerModel);
					userFavouritesIntegrationService.addUserFavouriteOffer(dealCode, dealName);
				}
			}
			catch (final ModelSavingException | DuplicateUidException e)
			{
				LOG.error("Error while saving the deals with customer", e);
				status = false;
			}

		}
		LOG.debug("ExtCustomerFacadeImpl ::: addFavDeal :: status : {} ", status);
		return status;
	}

	/**
	 * This method will give all the Favorite designers of current user from PO.Com.
	 *
	 * @param customerData
	 *           of type {@link CustomerData} hold the customer data to be displayed on frontend
	 * @param dealData
	 *           list of {@link DealData} hold the customers deals information
	 * @return the all favorite deals
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	@Override
	public boolean getAllFavoriteDeals(final CustomerData customerData, final List<DealData> dealData) throws DuplicateUidException
	{
		boolean status = true;

		List<String> favDeals = new ArrayList<>();
		final CustomerModel customer = (CustomerModel) getUserService().getUserForUID(customerData.getUid());
		if (isEsbPoDisabled())
		{
			dealData.addAll(Converters.convertAll(customer.getMyOffers(), extDealConverter));
			return status;
		}
		try
		{
			favDeals = userFavouritesIntegrationService.getAllFavouriteOffers();
		}
		catch (final UserFavouritesIntegrationException e)
		{
			LOG.error("Error occurred :" + e);
			status = false;
		}
		if (CollectionUtils.isNotEmpty(favDeals))
		{
			dealData.addAll(fetchAndSaveAllDeals(customer, favDeals));
		}
		else
		{
			final Set<DealModel> dealSet = new HashSet<>();
			customer.setMyOffers(dealSet);
			extCustomerAccountService.updateProfileInfo(customer);
		}
		getCustomerConverter().convert(customer, customerData);
		return status;

	}

	@Override
	public boolean isFavDeal(final String dealCode)
	{
		boolean isFavDeal = false;
		final UserModel user = getUserService().getCurrentUser();

		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			if (StringUtils.isNotEmpty(dealCode) && CollectionUtils.isNotEmpty(customer.getMyOffers()))
			{
				for (final DealModel dealModel : customer.getMyOffers())
				{
					if (dealCode.equalsIgnoreCase(dealModel.getCode()))
					{
						isFavDeal = true;
					}

				}

			}

		}
		LOG.debug("ExtCustomerFacadeImpl ::: isFavDeal :: is Deal favourite : {} for deal code : {} ", isFavDeal, dealCode);
		return isFavDeal;
	}


	/**
	 * This method is used to fetch array of dealModels from Database and save to current customer.
	 *
	 * @param customer
	 * @param favDeals
	 * @throws DuplicateUidException
	 */
	private List<DealData> fetchAndSaveAllDeals(final CustomerModel customer, final List<String> favDeals)
			throws DuplicateUidException
	{
		List<DealData> dealDatas = new ArrayList<>();
		final List<DealModel> dealModels = extDealService.getListOfDealsForCodes(favDeals);
		if (CollectionUtils.isNotEmpty(dealModels))
		{
			final Set<DealModel> dealSet = new HashSet<>(dealModels);
			customer.setMyOffers(dealSet);
			extCustomerAccountService.updateProfileInfo(customer);

			if (CollectionUtils.isNotEmpty(dealModels))
			{
				dealDatas = Converters.convertAll(dealModels, extDealConverter);
			}
		}
		return dealDatas;
	}

	/**
	 * This method is used to fetch list of dealData .
	 */
	@Override
	public List<DealData> fetchAllDeals()
	{
		final List<DealModel> dealList = extDealService.findListOfDeals();
		return Converters.convertAll(dealList, extDealConverter);
	}


	/*
	 * This method tests my favorite deals are not empty and then favorite flag is set for available my deals.
	 *
	 * @param customerData of type {@link CustomerData} is used to fetch saved shops data
	 *
	 * @param dealList of type {@link Set<DealData>} is updated for favorite deals
	 */
	@Override
	public boolean updateFavDeals(final CustomerData customerData, final List<DealData> dealList)
	{

		if (!CollectionUtils.isEmpty(customerData.getMyOffers()))
		{
			setFavDealFlag(dealList, customerData.getMyOffers());
			return true;
		}
		return false;

	}

	/*
	 * This method tests of deals exist in my saved deals, if yes a flag is set to true.
	 *
	 * @param allDeal of type {@link Set<DealData>} is used to fetch saved shops data
	 *
	 * @param customerDeals of type {@link Set<DealData>} is updated for favorite stores
	 */
	private void setFavDealFlag(final List<DealData> allDeal, final Set<DealData> customerDeals)
	{
		if (CollectionUtils.isNotEmpty(allDeal) && CollectionUtils.isNotEmpty(customerDeals))
		{
			for (final DealData dealData : customerDeals)
			{
				for (final DealData deal : allDeal)
				{
					if (dealData.getCode().equalsIgnoreCase(deal.getCode()))
					{
						deal.setFavorite(true);
					}
				}
			}

		}
	}


	@Override
	public List<String> fetchMyFavSizesFromExtService()
	{
		if (isEsbPoDisabled())
		{
			final UserModel user = getUserService().getCurrentUser();
			if (user instanceof CustomerModel)
			{
				final CustomerModel customer = (CustomerModel) user;
				final List<String> sizes = new ArrayList<>();
				customer.getMySizes().forEach(size -> sizes.add(size.getCode()));
				return sizes;
			}
		}
		return userFavouritesIntegrationService.getAllFavouriteSizes();
	}

	@Override
	public List<SizeData> getMySizes(final List<String> mySizeIds)
	{
		return extSizeConverter.convertAll(sizeService.getAllSizesByCode(mySizeIds));
	}

	@Override
	public boolean addToMySize(final String oldSizeCode, final String newSizeCode, final String sizeName)
	{

		final UserModel user = getUserService().getCurrentUser();
		boolean status = false;
		if (user instanceof CustomerModel)
		{

			final CustomerModel customer = (CustomerModel) user;
			Set<SizeModel> sizeSet = new HashSet<>();
			if (CollectionUtils.isNotEmpty(customer.getMySizes()))
			{
				sizeSet = customer.getMySizes().stream().collect(Collectors.toSet());
			}

			if (StringUtils.isNotBlank(oldSizeCode))
			{
				userFavouritesIntegrationService.removeUserFavouriteSize(oldSizeCode);

				final SizeModel sizeModel = sizeService.getSizeForId(newSizeCode);
				sizeSet.remove(sizeModel);

			}

			userFavouritesIntegrationService.addUserFavouriteSize(newSizeCode, sizeName);

			final SizeModel sizeModel = sizeService.getSizeForId(newSizeCode);
			sizeSet.add(sizeModel);

			customer.setMySizes(sizeSet);

			try
			{
				extCustomerAccountService.updateProfileInfo(customer);
				status = true;
			}
			catch (final DuplicateUidException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);

			}


		}

		return status;

	}

	@Override
	public Map<String, List<SizeData>> getAllSizes()
	{
		final List<SizeModel> sizeModelList = sizeService.getAllSizes();

		final Map<String, List<SizeData>> sizeSubCategoryMap = new HashMap<>();

		for (final SizeModel sizeModel : sizeModelList)
		{
			final String categoryCode = sizeModel.getCategory().getCode();
			if (sizeSubCategoryMap.containsKey(categoryCode))
			{
				final List<SizeData> existingSizeList = sizeSubCategoryMap.get(categoryCode);
				existingSizeList.add(getSizeData(sizeModel));
			}
			else
			{
				final List<SizeData> newSizeList = new ArrayList<>();
				newSizeList.add(getSizeData(sizeModel));
				sizeSubCategoryMap.put(categoryCode, newSizeList);
			}

		}
		return sizeSubCategoryMap;

	}

	private SizeData getSizeData(final SizeModel sizeModel)
	{
		return extSizeConverter.convert(sizeModel);
	}

	/**
	 * update the my size.
	 *
	 * @param sizeCodes
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean updateMySize(final List<String> sizeCodes)
	{
		boolean status = true;
		if (checkIfUserIsLoggedIn())
		{
			try
			{
				final CustomerModel customer = (CustomerModel) getUserService().getCurrentUser();
				final Set<SizeModel> sizeSet = new HashSet<>();
				if (CollectionUtils.isNotEmpty(sizeCodes))
				{
					sizeCodes.forEach(sizeCode -> {
						final SizeModel sizeModel = sizeService.getSizeForId(sizeCode);
						if (null != sizeModel)
						{
							sizeSet.add(sizeModel);
						}
					});
				}
				customer.setMySizes(sizeSet);
				extCustomerAccountService.updateCustomerMyFavorite(customer);
			}
			catch (final ModelSavingException e)
			{
				LOG.error(ERRORKEYSAVINGCUSTOMER, e);
				status = false;
			}
		}
		return status;
	}

	@Override
	public boolean isSizeSelected(final String sizeCode)
	{
		boolean isSizeSelected = false;
		final UserModel user = getUserService().getCurrentUser();

		if (user instanceof CustomerModel)
		{
			final CustomerModel customer = (CustomerModel) user;
			if (StringUtils.isNotEmpty(sizeCode) && CollectionUtils.isNotEmpty(customer.getMySizes()))
			{
				for (final SizeModel sizeModel : customer.getMySizes())
				{
					if (sizeCode.equalsIgnoreCase(sizeModel.getCode()))
					{
						isSizeSelected = true;
					}
				}
			}
		}
		LOG.debug("ExtCustomerFacadeImpl ::: isSizeSelected :: is Size slected : {} for size code : {} ", isSizeSelected, sizeCode);
		return isSizeSelected;
	}


	/**
	 * Used to populate the Deals data
	 *
	 * @param dealModel
	 * @return target
	 */
	@Override
	public DealData getDealData(final DealModel dealModel)
	{
		final DealData dealData = new DealData();
		extDealPopulator.populate(dealModel, dealData);
		return dealData;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateFavorite(final String favoriteType)
	{
		final FavoriteData favData = getSessionService().getAttribute(SimonFacadesConstants.FAV_SESSION_DATA);
		if (null != favData)
		{
			final FavoriteTypeEnum fav = favData.getFavType();

			if (fav.name().equals(FavoriteTypeEnum.RETAILERS.name()))
			{
				addToMyStores(favData.getFavId(), favData.getFavName());
			}
			else if (fav.name().equals(FavoriteTypeEnum.DESIGNERS.name()))
			{
				addToMyDesigners(favData.getFavId(), favData.getFavName());
			}
			else if (fav.name().equals(FavoriteTypeEnum.OFFERS.name()))
			{
				addFavDeal(favData.getFavId(), favData.getFavName());
			}
			else if (fav.name().equals(FavoriteTypeEnum.PRODUCTS.name()))
			{
				addToMyFavorite(favData.getFavId(), favData.getFavName());
			}
			getSessionService().removeAttribute(SimonFacadesConstants.FAV_SESSION_DATA);
		}

	}

	/**
	 * @return
	 */
	private boolean isEsbPoDisabled()
	{
		return configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StoreData getFeaturedStoreData(final ShopModel shopModel)
	{
		return extStoreConverter.convert(shopModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DesignerData getFeaturedDesignerData(final DesignerModel designerModel)
	{
		return extDesignerConverter.convert(designerModel);
	}

}
