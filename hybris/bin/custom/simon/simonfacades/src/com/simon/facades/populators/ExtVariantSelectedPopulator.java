package com.simon.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.VariantSelectedPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;


/**
 * This class is used to populate the {@link ProductModel} from {@link ProductData}
 */
public class ExtVariantSelectedPopulator extends VariantSelectedPopulator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target) throws ConversionException
	{
		super.populate(source, target);
		if (CollectionUtils.isNotEmpty(target.getBaseOptions()) && target.getBaseOptions().iterator().next().getSelected() != null
				&& source.getProductDesigner() != null)
		{
			target.getBaseOptions().iterator().next().getSelected()
					.setBaseProductDesignerName(source.getProductDesigner().getName());
		}
	}
}
