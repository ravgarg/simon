package com.simon.facades.customer;

import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.List;
import java.util.Map;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.SizeData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.checkout.data.StateData;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.customer.data.MonthData;
import com.simon.facades.product.data.GenderData;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;

import atg.taglib.json.util.JSONException;


/**
 * Defines an API to perform various customer related operations in SPO
 */
public interface ExtCustomerFacade extends CustomerFacade
{
	/**
	 * This method us used to Register a user in SPO.com.
	 *
	 * @param registerData
	 *           the user data the user will be registered with
	 * @param extRegistrationRequired
	 *           the user registration is required on third party.
	 * @throws DuplicateUidException
	 *            if the login is not unique
	 * @throws UserIntegrationException
	 * @throws AuthLoginIntegrationException
	 */
	void registerShopper(RegisterData registerData, boolean extRegistrationRequired)
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException;


	/**
	 * Get all country In System to display Country List .
	 *
	 * @return List
	 */
	public List<CountryData> getAllCountry();

	/**
	 * This is the method to get all Gender in list to display Gender list.
	 *
	 * @return List
	 */
	public List<GenderData> getGenderList();

	/**
	 * This is to get all Months list to display months List .
	 *
	 * @return List
	 */
	public List<MonthData> getMonthsList();

	/**
	 * @param customerData
	 * @param extUpdateRequired
	 * @throws DuplicateUidException
	 *            when uid is duplicate
	 * @throws UserIntegrationException
	 *            if there is an exception at PO.com
	 */
	void updateProfileInformation(CustomerData customerData, boolean extUpdateRequired)
			throws DuplicateUidException, UserIntegrationException;


	/**
	 * Method to get list of StateData for Shipping Address.
	 *
	 * @return list of StateData.
	 *
	 */
	List<StateData> getStateDataListForShippingAddress();

	/**
	 * This method will update the user profile with designer data.
	 *
	 * @param designerIds
	 *
	 * @throws DuplicateUidException
	 *            when uid is duplicate
	 * @throws UserIntegrationException
	 *            if there is an exception at PO.com
	 */
	boolean updateDesignersToProfile(String[] designerIds);

	/**
	 * This method will fetch all the designers from the DB except which are connected to current session user .
	 *
	 * @param designerIds
	 *
	 * @return List of designer data
	 */
	List<DesignerData> fetchAllDesignersExceptCustomerFavDesigners();

	/**
	 * This method will give all the favourite designer of user.
	 *
	 * @param designerDatas
	 * @param designerIds
	 *
	 * @return true if no UserIntegration exception
	 * @throws DuplicateUidException
	 *            when uid is duplicate
	 */
	boolean getAllFavouriteDesigners(List<DesignerData> designerDatas) throws DuplicateUidException;

	/**
	 * Adds the to my favorite.
	 *
	 * @param productCode
	 *           the product code
	 * @param productName
	 *           the product name
	 * @return true, if successful
	 */
	boolean addToMyFavorite(String productCode, String productName);

	/**
	 * Removes the from my favorite.
	 *
	 * @param productCode
	 *           the product code
	 * @param productName
	 *           the product name
	 * @return true, if successful
	 */
	boolean removeFromMyFavorite(String productCode, String productName);

	/**
	 * Gets the my favorite.
	 *
	 * @return the my favorite products list
	 */
	List<String> getMyFavoriteProductCodes();

	/**
	 * Gets the my favorite.
	 *
	 * @return the my favorite
	 */
	List<String> getMyFavoriteFromPO();

	/**
	 * Update my favorite.
	 *
	 * @param products
	 *           the products
	 * @return true, if successful
	 */
	boolean updateMyFavorite(final List<String> products);

	/**
	 * Populate my favorite.
	 *
	 * @param products
	 *           the products
	 * @return the list
	 */
	List<ProductData> populateMyFavorite(final List<ProductData> products);

	/**
	 * Create JSON Object for unfavorite from profile.
	 *
	 * @param code
	 * @param name
	 * @param action
	 * @param status
	 * @return JSON string
	 * @throws JSONException
	 */
	String createJsonForFavouriteUnFavourite(final String code, final String name, final boolean status, final String action)
			throws JSONException;

	/**
	 * Removes the from my designers.
	 *
	 * @param designerCode
	 *           the designer code
	 * @return true, if successful
	 */
	boolean removeFromMyDesigners(final String designerCode);

	/**
	 * Add to my designers.
	 *
	 * @param designerCode
	 *           the designer code
	 * @param designerName
	 *           the designer name
	 * @return true, if successful
	 */
	boolean addToMyDesigners(String designerCode, String designerName);

	/**
	 * Removes the from my stores.
	 *
	 * @param code
	 *           the shop code
	 * @return true, if successful
	 */
	boolean removeFromMyStores(String code);

	/**
	 * Add to my stores.
	 *
	 * @param storeCode
	 *           the store code
	 * @param storeName
	 *           the store name
	 * @return true, if successful
	 */
	boolean addToMyStores(String storeCode, String storeName);


	/**
	 * Checks if is fav store.
	 *
	 * @param storeCode
	 *           the store code
	 * @return true, if is fav store
	 */
	boolean isFavStore(final String storeCode);

	/**
	 * Gets the my fav product order by date.
	 *
	 * @param product
	 *           the product
	 * @return the my fav product order by date
	 */
	List<ProductData> getMyFavProductOrderByDate(List<ProductData> product);

	/**
	 * Check if user is logged in.
	 *
	 * @return true, if successful
	 */
	boolean checkIfUserIsLoggedIn();

	/**
	 * This method will update the user profile with store data.
	 *
	 * @param customerData
	 * @param storeIds
	 * @return true if no exception
	 */
	boolean updateStoreToProfile(CustomerData customerData, String[] storeIds);

	/**
	 * This method will fetch all the stores from the DB except which are connected to current session user .
	 *
	 * @param customerData
	 * @param storeIds
	 * @return List of store data
	 */
	List<StoreData> fetchAllStoresExceptLinked(CustomerData customerData);

	/**
	 * This method will give all the favourite stores of user.
	 *
	 * @param customerData
	 * @param storeIds
	 * @return true if no exception occured
	 * @throws DuplicateUidException
	 *            when uid is duplicate
	 */
	boolean getAllFavouriteStores(CustomerData customerData) throws DuplicateUidException;

	/**
	 * @param registerData
	 * @param orderGUID
	 * @throws DuplicateUidException
	 * @throws UserIntegrationException
	 * @throws AuthLoginIntegrationException
	 */
	public void registerGuestCustomer(RegisterData registerData, final String orderId)
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException;



	/**
	 * This method will give the primary Mall Data of the current customer
	 *
	 * @return MallData
	 *
	 */
	MallData getCustomerPrimaryMallData();

	/**
	 * This method will fetch all the designers from the DB except.
	 *
	 * @return List of designer data
	 */
	List<DesignerData> fetchAllDesigners();


	/**
	 * Update fav designers for my saved designers.
	 *
	 * @param customerData
	 * @param designerList
	 * @return true if favorite
	 */
	boolean updateFavDesigners(CustomerData customerData, List<DesignerData> designerList);

	/**
	 * This method will fetch favorite designer from DB.
	 *
	 * @param code
	 * @return true/false
	 */
	boolean isFavDesigner(String code);

	/**
	 * This method is used to remove the deal from customer account
	 *
	 * @param designerCode
	 * @return boolean
	 */
	boolean removeFavDeal(final String designerCode);

	/**
	 * This method is used to add a deal with customer account
	 *
	 * @param designerCode
	 * @param designerName
	 * @return boolean
	 */
	boolean addFavDeal(String designerCode, String designerName);

	/**
	 * This method is used to check deal is fav or not
	 *
	 * @param dealCode
	 * @return boolean
	 */
	boolean isFavDeal(final String dealCode);


	/**
	 * Method to fetch all deals
	 *
	 * @return List
	 */
	List<DealData> fetchAllDeals();


	/**
	 * Method to update favorite deals
	 *
	 * @param customerData
	 * @param dealList
	 * @return boolean
	 */
	boolean updateFavDeals(CustomerData customerData, List<DealData> dealList);

	/**
	 * Method to get favorite deals
	 *
	 * @param customerData
	 * @param dealData
	 * @return boolean
	 * @throws DuplicateUidException
	 */
	boolean getAllFavoriteDeals(final CustomerData customerData, final List<DealData> dealData) throws DuplicateUidException;

	/**
	 * Gets my size.
	 *
	 * @return my sizes
	 */
	List<String> fetchMyFavSizesFromExtService();

	/**
	 * Gets my size.
	 *
	 * @param mySizeIds
	 *
	 * @return my sizes
	 */
	List<SizeData> getMySizes(List<String> mySizeIds);

	/**
	 * This method is used to add a size with customer account
	 *
	 * @param oldCode
	 * @param newCode
	 * @param name
	 * @return boolean
	 */
	boolean addToMySize(String oldCode, String newCode, String name);

	/**
	 * This method will get all the size category map .
	 *
	 * @return Map of sub category with respective size data
	 */
	Map<String, List<SizeData>> getAllSizes();

	/**
	 * Update my sizes.
	 *
	 * @param sizeCodes
	 *
	 * @return true, if successful
	 */
	boolean updateMySize(final List<String> sizeCodes);


	boolean isSizeSelected(String sizeCode);

	/**
	 * This method is used to update the my favorite like (Deal,Store,Designer)
	 */
	void updateFavorite(String favoriteType);


	List<String> getAllFavouriteStoresFromPO();


	/**
	 * This method is used to convert the ShopModel into StoreData
	 *
	 * @param shopModel
	 * @return {@link StoreData}
	 */
	StoreData getFeaturedStoreData(ShopModel shopModel);

	/**
	 * This method is used to convert the @DesignerModel into @DesignerData
	 *
	 * @param designerModel
	 * @return {@link DesignerData}
	 */

	DesignerData getFeaturedDesignerData(DesignerModel designerModel);

	/**
	 * This method will get all the active designer list from SOLR.
	 *
	 * @return list of active designers
	 */
	List<DesignerData> getActiveRetailersDesignerList();

	/**
	 * Used to populate the Deals data
	 *
	 * @param dealModel
	 * @return target
	 */
	DealData getDealData(final DealModel dealModel);
}
