package com.simon.facades.search.converters.populator;

import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ProductModelToSiteMapUrlDataPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringEscapeUtils;


/**
 *
 */
public class ExtProductModelToSiteMapUrlDataPopulator extends ProductModelToSiteMapUrlDataPopulator
{
	@Override
	public void populate(final ProductModel productModel, final SiteMapUrlData siteMapUrlData) throws ConversionException
	{
		final String relUrl = StringEscapeUtils.escapeXml(getUrlResolver().resolve(productModel));
		siteMapUrlData.setLoc(relUrl);
	}

}
