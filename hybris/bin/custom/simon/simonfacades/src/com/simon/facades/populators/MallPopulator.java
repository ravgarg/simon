package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;

import com.simon.core.model.MallModel;
import com.simon.facades.customer.data.MallData;


/**
 * Converts MallModel to mall data
 */
public class MallPopulator implements Populator<MallModel, MallData>
{

	/*
	 * populates the mall data from the contents in mall model
	 */
	@Override
	public void populate(final MallModel source, final MallData target)
	{
		target.setCode(source.getCode());
		target.setName(source.getPropertyName());
		target.setLogo(source.getLogo());
		target.setShopperFacingURL(source.getShopperFacingURL());
		target.setMontrealMallFlag(source.getMontrealMallFlag());
	}

}
