package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.ResourceUtils;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.integration.dto.CartVariantValuesDTO;
import com.simon.integration.dto.CreateCartConfirmationProductDTO;
import com.simon.integration.dto.CreateCartVariantOptionsDTO;


/**
 * This class will be used to populate {@link AdditionalProductDetailsModel} from
 * {@link CreateCartConfirmationProductDTO}
 */
public class ProductDetailsReversePopulator implements Populator<CreateCartConfirmationProductDTO, AdditionalProductDetailsModel>
{
	/**
	 * Method populate will be used to populate {@link AdditionalProductDetailsModel} from
	 * {@link CreateCartConfirmationProductDTO}
	 */
	@Override
	public void populate(final CreateCartConfirmationProductDTO source, final AdditionalProductDetailsModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setProductID(source.getProductId());
		target.setStatusReason(source.getStatusReason());
		final List<String> requiredfields = source.getRequiredFieldNames();
		target.setRequiredFieldNames(requiredfields);
		target.setRequiredFieldValues(getRequiredValues(source));

	}

	private List<String> getRequiredValues(final CreateCartConfirmationProductDTO source)
	{
		final List<String> requiredValues = new ArrayList<>();
		final List<CreateCartVariantOptionsDTO> requiredFieldValues = source.getRequiredFieldValues();
		if (CollectionUtils.isNotEmpty(requiredFieldValues))
		{
			for (final CreateCartVariantOptionsDTO createCartVariantOptionsDTO : requiredFieldValues)
			{
				requiredValues.add(processVariantValues(createCartVariantOptionsDTO));
			}
		}
		return requiredValues;
	}

	private String processVariantValues(final CreateCartVariantOptionsDTO createCartVariantOptionsDTO)
	{
		final Map<String, CartVariantValuesDTO> variantAndValues = createCartVariantOptionsDTO.getOptions();
		final StringBuilder resultString = new StringBuilder();
		for (final String key : variantAndValues.keySet())
		{
			final CartVariantValuesDTO variantValues = variantAndValues.get(key);
			if (resultString.length() > SimonFacadesConstants.INT_ZERO)
			{
				resultString.append(SimonFacadesConstants.PIPE).append(key).append(SimonFacadesConstants.COLON).append(
						ResourceUtils.isUrl(variantValues.getValue()) ? checkForValue(variantValues) : variantValues.getValue());
			}
			else
			{
				resultString.append(key).append(SimonFacadesConstants.COLON).append(
						ResourceUtils.isUrl(variantValues.getValue()) ? checkForValue(variantValues) : variantValues.getValue());
			}
		}
		return resultString.toString().toLowerCase(Locale.US);
	}

	/**
	 * Method populate will be used to populate required field value from {@link CreateCartConfirmationProductDTO}
	 */
	private String checkForValue(final CartVariantValuesDTO variantValues)
	{
		return ResourceUtils.isUrl(variantValues.getText()) ? SimonFacadesConstants.REQUIRED_FIELD_VALUE_FOUND_URL
				: variantValues.getText();
	}
}
