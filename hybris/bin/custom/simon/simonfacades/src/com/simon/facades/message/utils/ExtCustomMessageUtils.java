package com.simon.facades.message.utils;

import de.hybris.platform.util.localization.Localization;

import java.util.HashMap;
import java.util.Map;

import com.simon.facades.constants.SimonFacadesConstants;


/**
 * Utils class for set CustomMessages into the messageLabels map, it is used for localization of messages which are
 * displayed in jsp and set into JSON object.
 */
public class ExtCustomMessageUtils
{

	/**
	 * method set mini cart labels to json object
	 *
	 * @return messageLabels
	 */
	public Map<String, String> setMiniCartMessages()
	{
		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.TOTAL_UNITCOUNT,
				Localization.getLocalizedString(SimonFacadesConstants.TOTAL_UNITCOUNT_LABEL));
		messageLabels.put(SimonFacadesConstants.CHECKOUT, Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_LABEL));
		messageLabels.put(SimonFacadesConstants.ITEM, Localization.getLocalizedString(SimonFacadesConstants.ITEM_LABEL));
		messageLabels.put(SimonFacadesConstants.VIEW_MY_BAG,
				Localization.getLocalizedString(SimonFacadesConstants.VIEW_MYBAG_LABEL));
		messageLabels.put(SimonFacadesConstants.YOU_SAVE, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVE_LABEL));
		return messageLabels;
	}

	/**
	 * method set mini cart labels to json object when mini cart is empty
	 *
	 *
	 * @return messageLabels
	 */
	public Map<String, String> setEmptyMiniCartMessages()
	{
		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.EMPTY_BAG_PROMOTIONAL,
				Localization.getLocalizedString(SimonFacadesConstants.EMPTY_BAG_PROMOTIONAL_LABEL));
		messageLabels.put(SimonFacadesConstants.EMPTY_BAG_PROMOTIONAL_URL,
				Localization.getLocalizedString(SimonFacadesConstants.EMPTY_BAG_PROMOTIONAL_LABEL_URL));
		messageLabels.put(SimonFacadesConstants.EMPTY_BAG, Localization.getLocalizedString(SimonFacadesConstants.EMPTY_BAG_LABEL));
		messageLabels.put(SimonFacadesConstants.VIEW_MY_BAG,
				Localization.getLocalizedString(SimonFacadesConstants.VIEW_MYBAG_LABEL));
		return messageLabels;
	}

	/**
	 * Method set add to cart labels to json object using message source
	 *
	 * @return messageLabels
	 */
	public Map<String, String> setAddToCartMessages()
	{
		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.DISCLAIMER_TEXT_KEY,
				Localization.getLocalizedString(SimonFacadesConstants.DISCLAIMER_TEXT));
		messageLabels.put(SimonFacadesConstants.CONTINUE_SHOPPING_KEY,
				Localization.getLocalizedString(SimonFacadesConstants.CONTINUE_SHOPPING));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_TEXT_KEY,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_TEXT));
		messageLabels.put(SimonFacadesConstants.VIEW_MYBAG_KEY, Localization.getLocalizedString(SimonFacadesConstants.VIEW_MYBAG));
		messageLabels.put(SimonFacadesConstants.ADD_TO_CART_QUANTITY_KEY,
				Localization.getLocalizedString(SimonFacadesConstants.ADD_TO_CART_QUANTITY));
		messageLabels.put(SimonFacadesConstants.ITEM_TOTAL_KEY, Localization.getLocalizedString(SimonFacadesConstants.ITEM_TOTAL));
		messageLabels.put(SimonFacadesConstants.YOU_SAVE, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVE_LABEL));
		return messageLabels;
	}

	/**
	 * This method set shopping bag labels to json object.
	 *
	 * @return messageLabels
	 */
	public Map<String, String> setCartPageMessages()
	{

		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.MY_SHOPPING_BAG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.MY_SHOPPING_BAG));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_ORDER_SUBTOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_ORDER_SUBTOTAL));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL_SHIPPING));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_TAX_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL_TAX));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_BAG_TOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_BAG_TOTAL));
		messageLabels.put(SimonFacadesConstants.YOU_SAVED_LABEL, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVED));
		messageLabels.put(SimonFacadesConstants.BAG_SUMMARY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.BAG_SUMMARY));
		messageLabels.put(SimonFacadesConstants.PROCEED_TO_CHECKOUT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PROCEED_TO_CHECKOUT));
		messageLabels.put(SimonFacadesConstants.RETAILER_SHIPPING_PRIVACY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.RETAILER_SHIPPING_PRIVACY));
		messageLabels.put(SimonFacadesConstants.ITEMS_PRICING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ITEMS_PRICING));
		messageLabels.put(SimonFacadesConstants.TOTAL_MAX_QUANTITY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TOTAL_MAX_QUANTITY));
		messageLabels.put(SimonFacadesConstants.REMOVE_LABEL, Localization.getLocalizedString(SimonFacadesConstants.REMOVE));
		messageLabels.put(SimonFacadesConstants.ADD_TO_MY_FAVOURITE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ADD_TO_MY_FAVOURITE));
		messageLabels.put(SimonFacadesConstants.COLOR_LABEL, Localization.getLocalizedString(SimonFacadesConstants.COLOR));
		messageLabels.put(SimonFacadesConstants.SIZE_LABEL, Localization.getLocalizedString(SimonFacadesConstants.SIZE));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_SUBTOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_SUBTOTAL));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_SHIPPING));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TAX_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TAX));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL));
		messageLabels.put(SimonFacadesConstants.RETAILER_ORDER_PROMOTION_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.RETAILER_ORDER_PROMOTION));
		messageLabels.put(SimonFacadesConstants.HELPFULL_INFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.HELPFULL_INFO));
		messageLabels.put(SimonFacadesConstants.HELPFULL_INFO_MESSAGE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.HELPFULL_INFO_MESSAGE));
		messageLabels.put(SimonFacadesConstants.QUESTION_LABEL, Localization.getLocalizedString(SimonFacadesConstants.QUESTION));
		messageLabels.put(SimonFacadesConstants.CONTACT_US_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CONTACT_US));
		messageLabels.put(SimonFacadesConstants.CONTINUE_URL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CONTINUE_URL));
		messageLabels.put(SimonFacadesConstants.SEE_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SEE_SHIPPING));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_TAX,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL_TAX));
		messageLabels.put(SimonFacadesConstants.YOU_SAVE, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVE_LABEL));
		messageLabels.put(SimonFacadesConstants.CART_OUT_OF_STOCK_LABLE,
				Localization.getLocalizedString(SimonFacadesConstants.CART_OUT_OF_STOCK));
		messageLabels.put(SimonFacadesConstants.CART_INPUT_NOT_VALID_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_INPUT_NOT_VALID));
		messageLabels.put(SimonFacadesConstants.CART_REMOVE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_REMOVE));
		messageLabels.put(SimonFacadesConstants.CART_ADDED_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ADDED));
		messageLabels.put(SimonFacadesConstants.CART_ADD_TO_FAVORITE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ADD_TO_FAVORITE));
		messageLabels.put(SimonFacadesConstants.CART_ADDED_TO_FAVORITES_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ADDED_TO_FAVORITES));
		messageLabels.put(SimonFacadesConstants.CART_CHOOSE_METHODIN_CHECKOUT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_CHOOSE_METHODIN_CHECKOUT));
		messageLabels.put(SimonFacadesConstants.CART_TAX_CALCULATED_CHECKOUT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_CALCULATED_CHECKOUT));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_MSRP_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_MSRP));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_LISTPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_LISTPRICE));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_SALEPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_SALEPRICE));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_YOUSAVED_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_YOUSAVED));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_YOURPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_YOURPRICE));
		messageLabels.put(SimonFacadesConstants.CART_QUANTITY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_QUANTITY));
		messageLabels.put(SimonFacadesConstants.CART_VIEW_BAG_SUMMARY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_VIEW_BAG_SUMMARY));
		messageLabels.put(SimonFacadesConstants.CART_ESTIMATE_COMBINED_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ESTIMATE_COMBINED_SHIPPING));
		messageLabels.put(SimonFacadesConstants.CART_ESTIMATE_COMBINED_TAX_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ESTIMATE_COMBINED_TAX));
		messageLabels.put(SimonFacadesConstants.CART_ESTIMATE_BAG_TOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_ESTIMATE_BAG_TOTAL));
		messageLabels.put(SimonFacadesConstants.CART_INFO_REGARDING_TAXS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_INFO_REGARDING_TAXS));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE1_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE1));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE2_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE2));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE_CLOSE_BUTTON_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE_CLOSE_BUTTON));









		return messageLabels;
	}

	/**
	 * Method set checkout shipping labels to json object using message source
	 *
	 * @return messageLabels
	 */
	public Map<String, String> setCheckoutShippingMessages()
	{
		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.GUEST_MESSAGE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.GUEST_MESSAGE));
		messageLabels.put(SimonFacadesConstants.HAVE_ACCOUNT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.HAVE_ACCOUNT));
		messageLabels.put(SimonFacadesConstants.SIGN_IN_LABEL, Localization.getLocalizedString(SimonFacadesConstants.SIGN_IN));
		messageLabels.put(SimonFacadesConstants.SHIPPING_INFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SHIPPING_INFORMATION));
		messageLabels.put(SimonFacadesConstants.WE_DONT_SHIP_MESSAGE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.WE_DONT_SHIP_MESSAGE));
		messageLabels.put(SimonFacadesConstants.INDICATE_OPTIONAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.INDICATE_OPTIONAL_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SUBSCRIBE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SUBSCRIBE_MESSAGE));
		messageLabels.put(SimonFacadesConstants.PLEASE_REFER_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PLEASE_REFER_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SHIPPING_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SHIPPING_ADDRESS_MESSAGE));

		messageLabels.put(SimonFacadesConstants.USE_SHIP_BILL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.USE_SHIP_BILL_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SAVE_CONTINUE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SAVE_CONTINUE_MESSAGE));
		messageLabels.put(SimonFacadesConstants.ADD_NEW_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ADD_NEW_ADDRESS));

		messageLabels.put(SimonFacadesConstants.SAVE_ADDRESS_TO_BOOK_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SAVE_ADDRESS_TO_BOOK));
		messageLabels.put(SimonFacadesConstants.STATE_LABEL, Localization.getLocalizedString(SimonFacadesConstants.STATE_MESSAGE));
		messageLabels.put(SimonFacadesConstants.WELCOME_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.WELCOME_BACK_MESSAGE));
		messageLabels.put(SimonFacadesConstants.PHONENUMBER_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PHONENUMBER_MESSAGE));
		messageLabels.put(SimonFacadesConstants.ZIPCODE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ZIPCODE_MESSAGE));
		messageLabels.put(SimonFacadesConstants.EMAIL_LABEL, Localization.getLocalizedString(SimonFacadesConstants.EMAIL_MESSAGE));

		messageLabels.put(SimonFacadesConstants.CITY_LABEL, Localization.getLocalizedString(SimonFacadesConstants.CITY_MESSAGE));
		messageLabels.put(SimonFacadesConstants.ADDRESS1_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ADDRESS1_MESSAGE));
		messageLabels.put(SimonFacadesConstants.ADDRESS2_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ADDRESS2_MESSAGE));
		messageLabels.put(SimonFacadesConstants.FIRSTNAME_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.FIRSTNAME_MESSAGE));
		messageLabels.put(SimonFacadesConstants.MIDDLE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.MIDDLE_MESSAGE));
		messageLabels.put(SimonFacadesConstants.LASTNAME_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.LASTNAME_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SHIPTO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SHIPTO_MESSAGE));
		messageLabels.put(SimonFacadesConstants.EDIT_LABEL, Localization.getLocalizedString(SimonFacadesConstants.EDIT_MESSAGE));
		messageLabels.put(SimonFacadesConstants.VERIFY_ADD_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.VERIFY_ADD_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SHIPPING_PANEL_ERROR_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SHIPPING_PANEL_ERROR_MESSAGE));
		messageLabels.put(SimonFacadesConstants.VERIFICATION_TEXT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.VERIFICATION_TEXT_MESSAGE));
		messageLabels.put(SimonFacadesConstants.YOU_ENTERED_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.YOU_ENTERED_MESSAGE));
		messageLabels.put(SimonFacadesConstants.USE_THIS_ADD_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.USE_THIS_ADD_MESSAGE));
		messageLabels.put(SimonFacadesConstants.SUGGESTED_ADD_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SUGGESTED_ADD_MESSAGE));

		messageLabels.put(SimonFacadesConstants.PRIVACYPOLICY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PRIVACYPOLICY_MESSAGE));

		messageLabels.put(SimonFacadesConstants.OR_LABEL, Localization.getLocalizedString(SimonFacadesConstants.OR_MESSAGE));
		messageLabels.put(SimonFacadesConstants.CONTACTUS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CONTACTUS_MESSAGE));
		messageLabels.put(SimonFacadesConstants.FORMORE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.FORMORE_MESSAGE));

		messageLabels.put(SimonFacadesConstants.PAYMENT_INFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PAYMENT_INFO_MESSAGE));

		messageLabels.put(SimonFacadesConstants.SECURE_INFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SECURE_INFO_MESSAGE));

		messageLabels.put(SimonFacadesConstants.MONTH_LABEL, Localization.getLocalizedString(SimonFacadesConstants.MONTH_MESSAGE));

		messageLabels.put(SimonFacadesConstants.YEAR_LABEL, Localization.getLocalizedString(SimonFacadesConstants.YEAR_MESSAGE));

		messageLabels.put(SimonFacadesConstants.SAVE_CARD_DEFAULT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SAVE_CARD_DEFAULT_MESSAGE));

		messageLabels.put(SimonFacadesConstants.BILLING_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.BILLING_ADDRESS_MESSAGE));

		messageLabels.put(SimonFacadesConstants.PLACE_ORDER_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.PLACE_ORDER_MESSAGE));

		messageLabels.put(SimonFacadesConstants.HELPFUL_INFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.HELPFUL_INFO_MESSAGE));

		messageLabels.put(SimonFacadesConstants.INFORMATION_DECLERATION_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.INFORMATION_DECLERATION_MESSAGE));

		messageLabels.put(SimonFacadesConstants.SHIPPINGMETHOD_AND_CHARGES_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.SHIPPINGMETHOD_AND_CHARGES_MESSAGE));
		messageLabels.put(SimonFacadesConstants.YOU_SAVE, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVE_LABEL));

		messageLabels.put(SimonFacadesConstants.CREATE_ACCOUNT_SKIP_MESSAGE_LABEL1,
				Localization.getLocalizedString(SimonFacadesConstants.PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SKIP_MESSAGE_TEXT1));
		messageLabels.put(SimonFacadesConstants.CREATE_ACCOUNT_SKIP_MESSAGE_LABEL2,
				Localization.getLocalizedString(SimonFacadesConstants.PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SKIP_MESSAGE_TEXT2));
		messageLabels.put(SimonFacadesConstants.BAG_SUMMARY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.BAG_SUMMARY));
		messageLabels.put(SimonFacadesConstants.CART_VIEW_BAG_SUMMARY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_VIEW_BAG_SUMMARY));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_ORDER_SUBTOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_ORDER_SUBTOTAL));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL_SHIPPING));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_TAX_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL_TAX));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_BAG_TOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_BAG_TOTAL));
		messageLabels.put(SimonFacadesConstants.CART_INFO_REGARDING_TAXS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_INFO_REGARDING_TAXS));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE1_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE1));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE2_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE2));
		messageLabels.put(SimonFacadesConstants.CART_TAX_MESSAGE_CLOSE_BUTTON_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TAX_MESSAGE_CLOSE_BUTTON));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VIEW_SHOP_BAG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VIEW_SHOP_BAG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_FIRSTNAME_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_FIRSTNAME_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_LASTNAME_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_LASTNAME_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_ADDRESS_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_ADDRESS_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_CITY_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_CITY_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PHONE_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PHONE_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_EMAIL_ERROE_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_EMAIL_ERROE_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_DONOT_SHIP_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_DONOT_SHIP_MSG));
		//Review Order
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_METD_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_METD));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_ITEMS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_ITEMS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_SUBTOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_SUBTOTAL));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_ITEM_PRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SHIP_ITEM_PRICE));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_MSRP_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_MSRP));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_LISTPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_LISTPRICE));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_SALEPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_SALEPRICE));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_YOUSAVED_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_YOUSAVED));
		messageLabels.put(SimonFacadesConstants.CART_TOOLLTIP_YOURPRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_TOOLLTIP_YOURPRICE));
		messageLabels.put(SimonFacadesConstants.CART_QUANTITY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CART_QUANTITY));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_SUBTOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_SUBTOTAL));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_SHIPPING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_SHIPPING));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TAX_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TAX));
		messageLabels.put(SimonFacadesConstants.ESTIMATED_TOTAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.ESTIMATED_TOTAL));
		messageLabels.put(SimonFacadesConstants.YOU_SAVED_LABEL, Localization.getLocalizedString(SimonFacadesConstants.YOU_SAVED));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SELECT_SHIP_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_SELECT_SHIP));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_OPTIONAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_OPTIONAL));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_CREATE_VIP_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_CREATE_VIP_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_UPPERCASE_CHAR_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_UPPERCASE_CHAR));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_LOWERCASE_CHAR_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_LOWERCASE_CHAR));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_NUMERIC_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_NUMERIC_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_ALPHANUMERIC_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_ALPHANUMERIC_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_PASS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_PASS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_PASS_ERROR_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_PASS_ERROR_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_MIN_PASS_ERROR_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_MIN_PASS_ERROR_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_CONFIRM_PASS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_CONFIRM_PASS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_CONFIRM_PASS_ERROR_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_CONFIRM_PASS_ERROR_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_SAVEINFO_SKIP_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_SAVEINFO_SKIP));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_ENDINGIN_LABLE,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_ENDINGIN));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_ADD_NEW_PAYMENT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_ADD_NEW_PAYMENT));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_EXP_MONTH_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_EXP_MONTH));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_EXP_YEAR_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_EXP_YEAR));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_SAVE_CARD_BILLING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_SAVE_CARD_BILLING));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_MAKE_DEFAULT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_MAKE_DEFAULT));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_ADD_NEW_BILLING_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_ADD_NEW_BILLING));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_ADDRESS2_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_ADDRESS2));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_REGION_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_REGION));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_POLICY_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_POLICY_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_TERMOF_USE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_TERMOF_USE));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_AND_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_AND));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_PRIVACY_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_PRIVACY));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_HELPFUL_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_HELPFUL_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_HELPFUL_QUESTION_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_HELPFUL_QUESTION));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_CONTACT_US_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_CONTACT_US));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_INFO_SECURE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_INFO_SECURE));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_INDICATE_OPTIONAL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_INDICATE_OPTIONAL));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_PAYMENTMETHOD_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT_PAYMENTMETHOD));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_PAYMENT_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_PAYMENT));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_MSG_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_MSG));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_YOU_ENTERED_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_YOU_ENTERED));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_USE_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_USE_ADDRESS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_SUGGESTED_ADDRESS_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_ADDRESS_SUGGESTED_ADDRESS));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_VIEW_ITEM_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_VIEW_ITEM));
		messageLabels.put(SimonFacadesConstants.CHECKOUT_REVIEWORDER_CLOSE_ITEM_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_REVIEWORDER_CLOSE_ITEM));

		return messageLabels;
	}

	/**
	 * @return
	 */
	public Map<String, String> setTrackOrderMessages()
	{
		final HashMap<String, String> messageLabels = new HashMap<>();
		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_RESULTFOR_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_RESULTFOR));
		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_AND_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_AND));

		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_ORDER_DATE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_ORDER_DATE));
		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_RETAILER_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_RETAILER));
		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_TOTAL_PRICE_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_TOTAL_PRICE));
		messageLabels.put(SimonFacadesConstants.TRACK_ORDER_ORDER_DETAIL_LABEL,
				Localization.getLocalizedString(SimonFacadesConstants.TRACK_ORDER_ORDER_DETAIL));

		return messageLabels;

	}

}
