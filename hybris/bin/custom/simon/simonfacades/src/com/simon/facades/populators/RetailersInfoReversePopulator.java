package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.CreateCartConfirmationProductDTO;
import com.simon.integration.dto.CreateCartConfirmationRetailerDTO;


/**
 * ExtRetailersInfoReversePopulator class populate RetailersInfoModel from CreateCartConfirmationRetailerDTO which also
 * call extProductsInfoReverseConverter for ProductsInfoModel and populate ShippingDetailsModel.
 */
public class RetailersInfoReversePopulator implements Populator<CreateCartConfirmationRetailerDTO, RetailersInfoModel>
{
	@Resource
	private Converter<Map<String, String>, ShippingDetailsModel> shippingDetailsReverseConverter;

	@Resource
	private Converter<CreateCartConfirmationProductDTO, AdditionalProductDetailsModel> productDetailsReverseConverter;

	/**
	 * RetailersInfoReversePopulator class populate RetailersInfoModel from CreateCartConfirmationRetailerDTO which also
	 * call extProductsInfoReverseConverter for ProductsInfoModel and populate ShippingDetailsModel.
	 */
	@Override
	public void populate(final CreateCartConfirmationRetailerDTO source, final RetailersInfoModel target)
	{
		ServicesUtil.validateParameterNotNull(source, "source is null");
		ServicesUtil.validateParameterNotNull(target, "target is null");

		target.setRetailerId(source.getRetailerId());
		target.setShippingDetails(addShippingDetails(source));
		target.setAddedProducts(addProductDetails(source));
		target.setFailedProducts(addFailedProductDetails(source));

	}

	/**
	 * This method create a List of successfully added products which are added to cart and pass it to
	 * productsDetailsReverseConverter to create AdditionalProductDetails list.
	 *
	 * @param CreateCartConfirmationRetailerDTO
	 *           source object
	 * @return AdditionalProductDetails list
	 */

	private List<AdditionalProductDetailsModel> addProductDetails(final CreateCartConfirmationRetailerDTO source)
	{
		final List<AdditionalProductDetailsModel> additionalProductDetailsModelList = new ArrayList<>();

		additionalProductDetailsModelList.addAll(productDetailsReverseConverter.convertAll(source.getAddedProducts()));

		return additionalProductDetailsModelList;
	}

	/**
	 * This method create a List of failed products which are not able to add to cart and pass to
	 * productsDetailsReverseConverter to create AdditionalProductDetails list.
	 *
	 * @param CreateCartConfirmationRetailerDTO
	 *           source object
	 * @return AdditionalProductDetails list
	 */

	private List<AdditionalProductDetailsModel> addFailedProductDetails(final CreateCartConfirmationRetailerDTO source)
	{
		final List<AdditionalProductDetailsModel> additionalProductDetailsModelList = new ArrayList<>();

		additionalProductDetailsModelList.addAll(productDetailsReverseConverter.convertAll(source.getFailedProducts()));

		return additionalProductDetailsModelList;
	}

	/**
	 * method create a hash map and pass to shippingDetailsReverseConverter to create shippingDetails list.
	 *
	 * @param source
	 * @return
	 */
	private List<ShippingDetailsModel> addShippingDetails(final CreateCartConfirmationRetailerDTO source)
	{
		final List<ShippingDetailsModel> shippingDetailsModelList = new ArrayList<>();

		final Map<String, String> shippingOptions = source.getShippingOptions();


		shippingOptions.forEach((key, value) -> {
			final Map<String, String> shippingDetailsMap = new HashMap<>();
			shippingDetailsMap.put(key, value);
			shippingDetailsModelList.add(shippingDetailsReverseConverter.convert(shippingDetailsMap));
		});

		return shippingDetailsModelList;
	}
}
