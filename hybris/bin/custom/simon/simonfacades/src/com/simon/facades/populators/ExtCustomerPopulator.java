package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.MallModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.customer.data.MallData;


/**
 *
 * Converter implementation for {@link de.hybris.platform.core.model.user.CustomerModel} as source and
 * {@link de.hybris.platform.commercefacades.user.data.CustomerData} as target type.
 */
public class ExtCustomerPopulator extends CustomerPopulator
{

	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;
	@Resource
	private Converter<DesignerModel, DesignerData> extDesignerConverter;
	@Resource
	private Converter<DealModel, DealData> extDealConverter;
	@Resource
	private Converter<ShopModel, StoreData> extStoreConverter;


	@Resource(name = "mallConverter")
	private Converter<MallModel, MallData> mallConverter;

	/**
	 * extends customer populator Converts customer Model to customer data populating country, gender, zipcode,
	 * birthmonth, birthyear
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		super.populate(source, target);
		if (source.getCountry() != null)
		{
			target.setCountry(getCountryConverter().convert(source.getCountry()).getIsocode());
		}
		target.setEmailAddress(source.getUid());
		target.setGender(source.getGender());
		target.setZipcode(source.getZipCode());
		target.setBirthMonth(source.getBirthMonth());
		target.setBirthYear(source.getBirthYear());
		if (null != source.getProfileCompleted())
		{
			target.setProfileCompleted(source.getProfileCompleted().booleanValue());
		}
		else
		{
			target.setProfileCompleted(false);
		}
		if (CollectionUtils.isNotEmpty(source.getMyDesigners()))
		{
			final Set<DesignerData> designerSet = new HashSet<DesignerData>();
			final List<DesignerData> designerList = extDesignerConverter.convertAll(source.getMyDesigners());
			designerSet.addAll(designerList);
			target.setMyDesigners(designerSet);
		}
		if (CollectionUtils.isNotEmpty(source.getShops()))
		{
			final Set<StoreData> shopSet = new HashSet<StoreData>();
			final List<StoreData> shopList = extStoreConverter.convertAll(source.getShops());
			shopSet.addAll(shopList);
			target.setMyStores(shopSet);
		}
		if (CollectionUtils.isNotEmpty(source.getMyOffers()))
		{
			final Set<DealData> dealSet = new HashSet<DealData>();
			final List<DealData> dealsList = extDealConverter.convertAll(source.getMyOffers());
			dealSet.addAll(dealsList);
			target.setMyDeals(dealSet);
		}
		if (CollectionUtils.isNotEmpty(source.getAlternateMalls()))
		{
			final List<MallData> alternateMalls = getExtMallConverter().convertAll(source.getAlternateMalls());
			target.setAlternateMalls(alternateMalls);
		}
		if (source.getPrimaryMall() != null)
		{
			target.setPrimaryMall(getExtMallConverter().convert(source.getPrimaryMall()));
		}
		if (source.getOptedInEmail() != null)
		{
			target.setOptedInEmail(source.getOptedInEmail());
		}
		if (CollectionUtils.isNotEmpty(source.getMyOffers()))
		{
			final Set<DealData> dealSet = new HashSet<>();
			final List<DealData> dealList = extDealConverter.convertAll(source.getMyOffers());
			dealSet.addAll(dealList);
			target.setMyOffers(dealSet);
		}

	}

	/**
	 * @return the countryConverter
	 */
	public Converter<CountryModel, CountryData> getCountryConverter()
	{
		return countryConverter;
	}

	/**
	 * @param countryConverter
	 *           the countryConverter to set
	 */
	public void setCountryConverter(final Converter<CountryModel, CountryData> countryConverter)
	{
		this.countryConverter = countryConverter;
	}

	/**
	 * @return extMallConverter
	 */
	public Converter<MallModel, MallData> getExtMallConverter()
	{
		return mallConverter;
	}

	/**
	 * @param extMallConverter
	 */
	public void setExtMallConverter(final Converter<MallModel, MallData> extMallConverter)
	{
		this.mallConverter = extMallConverter;
	}
}
