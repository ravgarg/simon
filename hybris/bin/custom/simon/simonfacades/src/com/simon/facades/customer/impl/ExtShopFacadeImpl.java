package com.simon.facades.customer.impl;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.facades.shop.impl.DefaultShopFacade;
import com.simon.core.model.CustomShopsComponentModel;
import com.simon.core.services.ExtShopService;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.ExtShopFacade;
import com.simon.facades.shop.data.ShopData;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationService;


/**
 * This class is the implementation class for the ExtShopFacade.
 */
public class ExtShopFacadeImpl extends DefaultShopFacade implements ExtShopFacade
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ExtShopFacadeImpl.class);


	/** The ext shop data converter. */
	@Resource(name = "extStoreDataConverter")
	private Converter<CustomShopsComponentModel, StoreDataList> extStoreDataConverter;

	@Resource(name = "shopService")
	private ExtShopService extShopService;
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "userFavouritesIntegrationService")
	private UserFavouritesIntegrationService userFavouritesIntegrationService;

	@Resource(name = "extShopDataConverter")
	private Converter<ShopModel, ShopData> extShopDataConverter;

	/**
	 * Gets the ext shop data converter.
	 *
	 * @return the ext shop data converter
	 */
	public Converter<CustomShopsComponentModel, StoreDataList> getExtStoreDataConverter()
	{
		return extStoreDataConverter;
	}


	/**
	 * Sets the ext shop data converter.
	 *
	 * @param extStoreDataConverter
	 *           the ext shop data converter
	 */
	public void setExtStoreDataConverter(final Converter<CustomShopsComponentModel, StoreDataList> extStoreDataConverter)
	{
		this.extStoreDataConverter = extStoreDataConverter;
	}


	/*
	 * @param customShops of type {@link CustomShopsComponentModel} is used to fetch shop model data
	 *
	 * @param storeList of type {@link StoreDataList} is populated using customShops
	 */
	@Override
	public void loadCustomShops(final CustomShopsComponentModel customShops, final StoreDataList storesList)
	{
		getExtStoreDataConverter().convert(customShops, storesList);
	}

	/*
	 * This method tests my favorite shops are not empty and then favorite flag is set for available my shops.
	 *
	 * @param customerData of type {@link CustomerData} is used to fetch saved shops data
	 *
	 * @param storeList of type {@link StoreDataList} is updated for favorite stores
	 */
	@Override
	public void updateFavCustomShops(final CustomerData customerData, final StoreDataList storesList)
	{
		if (!CollectionUtils.isEmpty(customerData.getMyStores()))
		{
			setFavoriteFlag(storesList, customerData.getMyStores());
		}
	}

	/*
	 * This method tests of custom shops exist in my saved shops, if yes a flag is set to true.
	 *
	 * @param myStores of type {@link Set<StoreData>} is used to fetch saved shops data
	 *
	 * @param storeList of type {@link StoreDataList} is updated for favorite stores
	 */
	private void setFavoriteFlag(final StoreDataList storeDataList, final Set<StoreData> myStores)
	{
		storeDataList.getAllStoresList().stream()
				.filter(e -> CollectionUtils.intersection(myStores, storeDataList.getAllStoresList()).contains(e))
				.forEach(e -> e.setFavorite(true));
	}

	/**
	 * This method return retailer list
	 *
	 * @return
	 */
	@Override
	public Map<String, String> getRetailerList()
	{
		final List<ShopModel> allRetailers = new ArrayList();
		if (CollectionUtils.isNotEmpty(extShopService.getRetailerlist()))
		{
			allRetailers.addAll(extShopService.getRetailerlist());
		}
		final List<ShopModel> shopList = fetchAllStores();

		if (CollectionUtils.isNotEmpty(shopList))
		{
			allRetailers.removeAll(shopList);
		}
		return getRetailersMap(allRetailers);
	}

	/**
	 * This method return Customer Favourite Retailer
	 *
	 * @return
	 */
	@Override
	public Map<String, String> getCustomerFavouritRetailerList()
	{
		return getRetailersMap(fetchAllStores());
	}


	/**
	 * Fetch all stores.
	 *
	 * @return the list
	 */
	private List<ShopModel> fetchAllStores()
	{
		final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
		List<String> favStores = new ArrayList<>();
		List<ShopModel> shopList = new ArrayList();
		try
		{
			if (null != customerModel.getType()
					&& CustomerType.REGISTERED.toString().equalsIgnoreCase(customerModel.getType().getCode()))
			{
				favStores = extCustomerFacade.getAllFavouriteStoresFromPO();
				if (CollectionUtils.isNotEmpty(favStores))
				{
					shopList = extShopService.getListOfShopsForCodes(favStores.toArray(new String[favStores.size()]));
				}
			}
		}
		catch (final UserFavouritesIntegrationException e)
		{
			LOG.error("Error occurred :" + e);
		}

		return shopList;
	}

	/**
	 * Gets the alphabetically sorted retailers map.
	 *
	 * @param retailersList
	 *           the retailers list
	 * @return the retailers map
	 */
	private Map<String, String> getRetailersMap(final List<ShopModel> retailersList)
	{
		final Map<String, String> retailersMap = new HashMap();
		for (final ShopModel shop : retailersList)
		{
			retailersMap.put(shop.getName(), shop.getId());
		}
		return new TreeMap<>(retailersMap);
	}

	/**
	 * Gets the ShopModel from the DB
	 *
	 * @param shopId
	 * @return the ShopModel
	 */
	@Override
	public ShopData getShop(final String shopId)
	{
		final ShopData shopData = new ShopData();
		final ShopModel shopModel = extShopService.getShopForId(shopId);
		extShopDataConverter.convert(shopModel, shopData);
		return shopData;
	}
}
