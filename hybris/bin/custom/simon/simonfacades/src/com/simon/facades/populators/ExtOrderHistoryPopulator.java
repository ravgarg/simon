
package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.OrderModel;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.simon.core.services.ExtOrderService;


/**
 * This class is used to populate the {@link OrderHistoryData} from {@link OrderModel}
 */
public class ExtOrderHistoryPopulator extends OrderHistoryPopulator
{

	@Resource
	private ExtOrderService orderService;

	/**
	 * This method populates the Retailer Name list
	 */
	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setCode(source.getCode());
		target.setGuid(source.getGuid());
		target.setPlaced(source.getDate());
		target.setStatus(source.getStatus());
		target.setStatusDisplay(source.getStatusDisplay());
		if (source.getTotalPrice() != null)
		{
			final BigDecimal totalPrice = BigDecimal.valueOf(source.getTotalPrice().doubleValue());
			target.setTotal(getPriceDataFactory().create(PriceDataType.BUY, totalPrice, source.getCurrency()));
		}
		target.setRetailers(orderService.getRetailerList(source));
	}

}
