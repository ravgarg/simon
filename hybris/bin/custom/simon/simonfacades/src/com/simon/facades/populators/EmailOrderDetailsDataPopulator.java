package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.converters.Populator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


/**
 * Converts OrderData to EmailOrderDetailsData
 */
public class EmailOrderDetailsDataPopulator implements Populator<OrderData, EmailOrderDetailsData>
{

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	/**
	 * populates the OrderData from the contents in EmailOrderDetailsData
	 */
	@Override
	public void populate(final OrderData source, final EmailOrderDetailsData target)
	{
		target.setOrderDate(shareEmailIntegrationUtils.convertDateInString(source.getCreated()));

		target.setOrderId(StringUtils.isNotEmpty(source.getCode()) ? source.getCode() : StringUtils.EMPTY);

		final CCPaymentInfoData paymentInfoData = source.getPaymentInfo();
		target.setOrderPayment(
				StringUtils.isNotEmpty(paymentInfoData.getCardType()) ? paymentInfoData.getCardType() : StringUtils.EMPTY);

		String orderDiscount = StringUtils.EMPTY;
		final PriceData orderDiscountPriceData = source.getTotalDiscounts();
		if (null != orderDiscountPriceData)
		{
			orderDiscount = orderDiscountPriceData.getFormattedValue();
		}
		target.setOrderSaved(orderDiscount);

		String orderShipping = StringUtils.EMPTY;
		final PriceData orderShippingPriceData = source.getDeliveryCost();
		if (null != orderShippingPriceData)
		{
			orderShipping = orderShippingPriceData.getFormattedValue();
		}
		target.setOrderShipping(orderShipping);


		String orderTax = StringUtils.EMPTY;
		final PriceData orderTaxPriceData = source.getTotalTax();
		if (null != orderTaxPriceData)
		{
			orderTax = orderTaxPriceData.getFormattedValue();
		}
		target.setOrderTaxes(orderTax);

		String orderSubtotal = StringUtils.EMPTY;
		final PriceData orderSubtotalPriceData = source.getSubTotal();
		if (null != orderSubtotalPriceData)
		{
			orderSubtotal = orderSubtotalPriceData.getFormattedValue();
		}
		target.setOrderSubtotal(orderSubtotal);

		String orderTotalPrice = StringUtils.EMPTY;
		final PriceData orderTotalPriceData = source.getTotalPrice();
		if (null != orderTotalPriceData)
		{
			orderTotalPrice = orderTotalPriceData.getFormattedValue();
		}
		target.setOrderTotal(orderTotalPrice);
	}
}
