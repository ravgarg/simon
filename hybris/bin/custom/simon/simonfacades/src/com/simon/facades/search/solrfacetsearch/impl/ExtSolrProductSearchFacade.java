package com.simon.facades.search.solrfacetsearch.impl;

import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.search.solrfacetsearch.impl.DefaultSolrProductSearchFacade;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * This class extends DefaultSolrProductSearchFacade and override textSearch and categorySearch method.
 *
 * @param <ITEM>
 *           the generic type
 */
public class ExtSolrProductSearchFacade<ITEM extends ProductData> extends DefaultSolrProductSearchFacade<ITEM>
{

	/** The Constant BASE_PRODUCT_RETAILER_NAME. */
	private static final String BASE_PRODUCT_RETAILER_NAME = "baseProductRetailerName";
	private static final String BASE_PRODUCT_DESIGNER_NAME = "baseProductDesignerName";

	/** The ext customer facade. */
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * This method is overridden to add customer specific favorite store and designer data into productSearchPageData
	 */
	@Override
	public ProductSearchPageData<SearchStateData, ITEM> textSearch(final SearchStateData searchState,
			final PageableData pageableData)
	{
		final ProductSearchPageData<SearchStateData, ITEM> productSearchPageData = callSuper(searchState, pageableData);
		modifyFacetDataForStores(productSearchPageData);
		modifyFacetDataForDesigners(productSearchPageData);
		return productSearchPageData;
	}

	protected ProductSearchPageData<SearchStateData, ITEM> callSuper(final SearchStateData searchState,
			final PageableData pageableData)
	{
		return super.textSearch(searchState, pageableData);
	}

	/**
	 * This method will get all designers related to current users and update facet data for all designers that which is
	 * favorites designer and which is not favorite
	 *
	 * @param searchPageData
	 */
	private void modifyFacetDataForDesigners(final ProductSearchPageData<SearchStateData, ITEM> searchPageData)
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final Set<DesignerData> designerSet = currentCustomerData.getMyDesigners();
		final List<String> myDesignerList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(designerSet))
		{
			for (final DesignerData designerData : designerSet)
			{
				myDesignerList.add(designerData.getName());
			}
			setFacetFavoriteData(searchPageData, myDesignerList, BASE_PRODUCT_DESIGNER_NAME);
		}
	}

	private void setFacetFavoriteData(final ProductSearchPageData<SearchStateData, ITEM> searchPageData, final List<String> myList,
			final String facetName)
	{
		final List<FacetData<SearchStateData>> facets = searchPageData.getFacets();
		if (CollectionUtils.isNotEmpty(facets))
		{
			for (final FacetData<SearchStateData> facetData : facets)
			{
				if (facetName.equals(facetData.getCode()))
				{
					setFavoriteFlagOfFacetValueData(myList, facetData, facetName);
				}
			}
		}
	}

	private void setFavoriteFlagOfFacetValueData(final List<String> myList, final FacetData<SearchStateData> facetData,
			final String facetName)
	{
		final List<FacetValueData<SearchStateData>> values = facetData.getValues();
		if (CollectionUtils.isNotEmpty(values))
		{
			for (final FacetValueData<SearchStateData> valueData : values)
			{
				setFavorites(myList, facetName, valueData);
			}
		}
	}

	private void setFavorites(final List<String> myList, final String facetName, final FacetValueData<SearchStateData> valueData)
	{
		if (null != valueData && myList.contains(valueData.getName()))
		{
			if (BASE_PRODUCT_DESIGNER_NAME.equals(facetName))
			{
				valueData.setFavoriteDesigner(true);
			}
			else
			{
				valueData.setFavoriteStore(true);
			}
		}
	}

	/**
	 * This method is overridden to add customer specific favorite store and designer data into
	 * ProductCategorySearchPageData
	 */
	@Override
	public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> categorySearch(final String categoryCode)
	{
		final ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> productSearchPageData = callCategorySearch(
				categoryCode);
		modifyFacetDataForStores(productSearchPageData);
		modifyFacetDataForDesigners(productSearchPageData);
		return productSearchPageData;
	}

	@Override
	public ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> categorySearch(final String categoryCode,
			final SearchStateData searchState, final PageableData pageableData)
	{
		final ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> productSearchPageData = callCategorySearch(
				categoryCode, searchState, pageableData);
		modifyFacetDataForStores(productSearchPageData);
		modifyFacetDataForDesigners(productSearchPageData);
		return productSearchPageData;
	}

	protected ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> callCategorySearch(final String categoryCode)
	{
		return super.categorySearch(categoryCode);
	}

	protected ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData> callCategorySearch(final String categoryCode,
			final SearchStateData searchState, final PageableData pageableData)
	{
		return super.categorySearch(categoryCode, searchState, pageableData);
	}

	/**
	 * This method will get all stores related to current users and update facet data for all stores that which is
	 * favorites stores and which is not favorite
	 *
	 * @param searchPageData
	 *           the search page data
	 */
	private void modifyFacetDataForStores(final ProductSearchPageData<SearchStateData, ITEM> searchPageData)
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final Set<StoreData> store = currentCustomerData.getMyStores();
		final List<String> myStoreList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(store))
		{
			for (final StoreData storeData : store)
			{
				myStoreList.add(storeData.getName());
			}
			setFacetFavoriteData(searchPageData, myStoreList, BASE_PRODUCT_RETAILER_NAME);
		}
	}

}
