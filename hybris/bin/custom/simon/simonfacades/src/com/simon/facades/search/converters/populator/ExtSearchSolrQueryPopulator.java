package com.simon.facades.search.converters.populator;


import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchSolrQueryPopulator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchQuery.Operator;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.constants.SimonCoreConstants;




/**
 * The Class ExtSearchSolrQueryPopulator. This populator extends SearchSolrQueryPopulator and call it populate method
 * which create custom query for favorite products.
 */
public class ExtSearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
		extends SearchSolrQueryPopulator<INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE>
{
	private static final int DEFAULT_DESIGNER_PAGE_SIZE = 20;
	private static final String STOREFRONT_DESINGER_RESULT_PAGE_SIZE = "storefront.designer.result.pageSize";
	private static final String FIND_ACTIVE_DESIGNERS = "findActiveDesigners";
	private static final String URL_EN_STRING = "url_en_string";
	private static final String BASE_PRODUCT_DESIGNER_NAME_STRING = "baseProductDesignerName_string";
	private static final String BASE_PRODUCT_DESIGNER_CODE_STRING = "baseProductDesignerCode_string";
	public static final String FAVOURITE_PRODUCT = "favoriteProducts";
	public static final String DESIGNER_CODE = "designerCode";
	public static final String TREND_CODE = "trendCode";
	public static final String INDEX_PROP = "baseProductCode";
	public static final String INDEX_PROP_BASE_PRODUCT_CODE = "baseProductCode_string:";
	public static final String INDEX_PROP_BASE_TREND_CODE = "baseProductTrendCode_string_mv:";
	public static final String INDEX_PROP_BASE_PRODUCT_DESIGNER = "baseProductDesignerName_string:";
	public static final String INDEX_PROP_RETAILER_ACTIVE = "retailerActive_boolean:";
	public static final String INDEX_PROP_BASE_PRODUCT_STORE = "baseProductRetailerId_string:";
	public static final String INDEX_PROP_BASE_PRODUCT_DESIGNER_CODE = "baseProductDesignerCode_string:";
	public static final String INDEX_PROP_DEAL_CODE = "dealCode_string_mv:";
	public static final String INDEX_PROP_PRODUCT_DEAL_CODE = "productDealCode_string_mv:";


	/** The session service. */
	@Autowired
	private SessionService sessionService;

	/** The ConfigurationService service. */
	@Autowired
	private ConfigurationService configurationService;


	/**
	 * The method is customized for adding search query params in case of fetching data for favorites/designers/shops.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	@Override
	public void populate(final SearchQueryPageableData<SolrSearchQueryData> source,
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target)
	{
		callsuper(source, target);
		final Map<String, IndexedProperty> indexedProperties = target.getIndexedType().getIndexedProperties();
		final SearchQuery searchQuery = target.getSearchQuery();
		searchQuery.addFilterQuery("retailerActive_boolean", Operator.OR, "true");
		target.setSearchQuery(searchQuery);
		final List<String> favoriteProducts = sessionService.getAttribute(FAVOURITE_PRODUCT);
		final List<String> productCodes = sessionService.getAttribute(SimonCoreConstants.PRODUCT_CODE_LIST);
		final String storeId = sessionService.getAttribute("store");
		final String designerCode = sessionService.getAttribute(DESIGNER_CODE);
		final String trendCode = sessionService.getAttribute(TREND_CODE);

		populateFavoriteQuery(target, indexedProperties, searchQuery, favoriteProducts);

		populateDesignerQuery(target, searchQuery, designerCode);

		populateTrendQuery(target, indexedProperties, searchQuery, trendCode);

		populateProductRecumdation(target, indexedProperties, searchQuery, productCodes);

		populateStoreQuery(target, searchQuery, storeId);
		populateActiveDesignersQuery(target, searchQuery);

		populateDealQuery(target, searchQuery);
	}

	private void populateActiveDesignersQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final SearchQuery searchQuery)
	{
		final Boolean fetchAllDesigners = sessionService.getAttribute(FIND_ACTIVE_DESIGNERS);
		if (fetchAllDesigners != null && fetchAllDesigners)
		{
			searchQuery.addField(BASE_PRODUCT_DESIGNER_CODE_STRING);
			searchQuery.addField(BASE_PRODUCT_DESIGNER_NAME_STRING);
			searchQuery.addField(URL_EN_STRING);
			final String designerPageSize = configurationService.getConfiguration().getString(STOREFRONT_DESINGER_RESULT_PAGE_SIZE);
			searchQuery.setPageSize(
					StringUtils.isEmpty(designerPageSize) ? DEFAULT_DESIGNER_PAGE_SIZE : Integer.parseInt(designerPageSize));
			target.setSearchQuery(searchQuery);
			sessionService.removeAttribute(FIND_ACTIVE_DESIGNERS);
		}
	}

	/**
	 * Populate product recumdation.
	 *
	 * @param target
	 *           the target
	 * @param indexedProperties
	 *           the indexed properties
	 * @param searchQuery
	 *           the search query
	 * @param productCodes
	 *           the product codes
	 */
	private void populateProductRecumdation(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final Map<String, IndexedProperty> indexedProperties, final SearchQuery searchQuery, final List<String> productCodes)
	{
		if (null != productCodes)
		{
			final Optional<IndexedProperty> indexedProperty = Optional.ofNullable(indexedProperties.get(INDEX_PROP));
			if (indexedProperty.isPresent())
			{
				for (final String productCode : productCodes)
				{
					searchQuery.addRawQuery("baseProductCode_string:(" + productCode + ")", Operator.OR);
				}
			}
			target.setSearchQuery(searchQuery);
			sessionService.removeAttribute(SimonCoreConstants.PRODUCT_CODE_LIST);
		}
	}

	/**
	 * Populate designer query.
	 *
	 * @param target
	 *           the target
	 * @param searchQuery
	 *           the search query
	 * @param designerName
	 *           the designer name
	 */
	private void populateDesignerQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final SearchQuery searchQuery, final String designerName)
	{
		if (null != designerName)
		{
			searchQuery.addRawQuery(INDEX_PROP_BASE_PRODUCT_DESIGNER_CODE + "(" + designerName + ")) " + Operator.AND + "( "
					+ INDEX_PROP_RETAILER_ACTIVE + true, Operator.OR);
			target.setSearchQuery(searchQuery);
			sessionService.removeAttribute(DESIGNER_CODE);
		}
	}

	/**
	 * Populate favorite query.
	 *
	 * @param target
	 *           the target
	 * @param indexedProperties
	 *           the indexed properties
	 * @param searchQuery
	 *           the search query
	 * @param favoriteProducts
	 *           the favorite products
	 */
	private void populateFavoriteQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final Map<String, IndexedProperty> indexedProperties, final SearchQuery searchQuery, final List<String> favoriteProducts)
	{
		if (null != favoriteProducts)
		{
			final Optional<IndexedProperty> indexedProperty = Optional.ofNullable(indexedProperties.get(INDEX_PROP));
			if (indexedProperty.isPresent())
			{
				for (final String productCode : favoriteProducts)
				{
					searchQuery.addRawQuery(
							INDEX_PROP_BASE_PRODUCT_CODE + productCode + " " + Operator.AND + " " + INDEX_PROP_RETAILER_ACTIVE + true,
							Operator.OR);
				}
			}
			target.setSearchQuery(searchQuery);
			sessionService.removeAttribute(FAVOURITE_PRODUCT);
		}
	}

	/**
	 * Populate store query.
	 *
	 * @param target
	 *           the target
	 * @param searchQuery
	 *           the search query
	 * @param storeId
	 *           the store id
	 */
	private void populateStoreQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final SearchQuery searchQuery, final String storeId)
	{
		if (null != storeId)
		{
			searchQuery.addRawQuery(
					INDEX_PROP_BASE_PRODUCT_STORE + "(" + storeId + ")) " + Operator.AND + "( " + INDEX_PROP_RETAILER_ACTIVE + true,
					Operator.OR);
			target.setSearchQuery(searchQuery);
			sessionService.removeAttribute("store");
		}
	}

	private void populateDealQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final SearchQuery searchQuery)
	{
		final String dealCode = sessionService.getAttribute("dealCode");
		final String productDealCode = sessionService.getAttribute("productDealCode");
		if (StringUtils.isNotEmpty(dealCode))
		{
			searchQuery.addRawQuery(
					INDEX_PROP_DEAL_CODE + "(" + dealCode + ")) " + Operator.AND + "( " + INDEX_PROP_RETAILER_ACTIVE + true,
					Operator.OR);

			sessionService.removeAttribute("dealCode");
		}
		else if (StringUtils.isEmpty(dealCode) && StringUtils.isNotEmpty(productDealCode))
		{
			searchQuery.addRawQuery(INDEX_PROP_PRODUCT_DEAL_CODE + "(" + productDealCode + ")) " + Operator.AND + "( "
					+ INDEX_PROP_RETAILER_ACTIVE + true, Operator.OR);
			sessionService.removeAttribute("productDealCode");

		}
		target.setSearchQuery(searchQuery);
	}

	/**
	 * Populate trend query.
	 *
	 * @param target
	 *           the target
	 * @param indexedProperties
	 *           the indexed properties
	 * @param searchQuery
	 *           the search query
	 * @param trends
	 *           the trends
	 */
	private void populateTrendQuery(
			final SolrSearchRequest<FacetSearchConfig, IndexedType, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE> target,
			final Map<String, IndexedProperty> indexedProperties, final SearchQuery searchQuery, final String trendCode)
	{
		if (null != trendCode)
		{
			final Optional<IndexedProperty> indexedProperty = Optional.ofNullable(indexedProperties.get(INDEX_PROP));
			if (indexedProperty.isPresent())
			{
				searchQuery.addRawQuery(
						INDEX_PROP_BASE_TREND_CODE + "(" + trendCode + ")) " + Operator.AND + "( " + INDEX_PROP_RETAILER_ACTIVE + true,
						Operator.OR);
			}

		}
		target.setSearchQuery(searchQuery);
		sessionService.removeAttribute(TREND_CODE);

	}

	/**
	 * The method calls the super method.
	 *
	 * @param source
	 * @param target
	 */
	protected void callsuper(final SearchQueryPageableData source, final SolrSearchRequest target)
	{
		super.populate(source, target);
	}

}
