package com.simon.facades.user;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.List;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;


/**
 *
 */
public interface ExtUserFacade extends UserFacade
{
	/**
	 * @param addressPK
	 * @return AddressData
	 */
	public AddressData getAddress(String addressPK);

	/**
	 * @param saved
	 * @return List<AddressData>
	 */
	public List<AddressData> getBillingAddressList(final boolean saved);

	/**
	 * @param extPaymentInfoData
	 * @return boolean
	 * @throws ModelSavingException
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	boolean setNewPaymentInfo(CCPaymentInfoData extPaymentInfoData)
			throws ModelSavingException, UserAddressIntegrationException, CartCheckOutIntegrationException;

	/**
	 * Method to add user's address in Db and keep it in Sync.
	 *
	 * @param addressData
	 * @return boolean
	 *
	 * @throws UserAddressIntegrationException
	 */
	boolean addUserAddress(ExtAddressData addressData) throws UserAddressIntegrationException;

	/**
	 * Method to edit user's address in Db and keep it in Sync.
	 *
	 * @param addressData
	 * @return boolean
	 *
	 * @throws UserAddressIntegrationException
	 */
	boolean editUserAddress(ExtAddressData addressData) throws UserAddressIntegrationException;

	/**
	 * Method to remove user's address from Db and keep it in Sync.
	 *
	 * @param addressData
	 * @return boolean
	 *
	 * @throws UserAddressIntegrationException
	 */
	boolean removeUserAddress(ExtAddressData addressData) throws UserAddressIntegrationException;

	/**
	 * This method is used to get the address info model from payment method id.
	 *
	 * @param paymentMethodId
	 * @return AddressModel
	 */
	AddressModel getCCPaymentInfoAddressModel(String paymentMethodId);

	/**
	 * This method is used to remove the linked payment address from PO.com
	 *
	 * @param addressModel
	 * @return boolean
	 * @throws UserAddressIntegrationException
	 */
	boolean removeCCPaymentInfoUserAddress(final AddressModel addressModel) throws UserAddressIntegrationException;

	/**
	 * This method is used to add the user payment address with PO.com
	 *
	 * @param ccPaymentInfoData
	 * @param ccPaymentInfoModel
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	boolean addUserPaymentAddress(CCPaymentInfoData ccPaymentInfoData, CreditCardPaymentInfoModel ccPaymentInfoModel)
			throws UserAddressIntegrationException, CartCheckOutIntegrationException;

	/**
	 * Gets the user address verify response DTO.
	 *
	 * @param addressData
	 *           the address data
	 * @return the user address verify response DTO
	 * @throws UserAddressVerifyIntegrationException
	 *            the user address verify integration exception
	 */
	public UserAddressVerifyResponseDTO getUserAddressVerifyResponseDTO(final ExtAddressData addressData)
			throws UserAddressVerifyIntegrationException;

	/**
	 * This method is used to remove the payment token from Payment Center.
	 *
	 * @param paymentMethodId
	 * @throws CartCheckOutIntegrationException
	 * @throws UserAddressIntegrationException
	 *
	 */
	void removePaymentMethodToken(String paymentMethodId) throws CartCheckOutIntegrationException, UserAddressIntegrationException;

	/**
	 * This method is used to remove the payment token from Payment Center through Checkout.
	 *
	 * @param paymentMethodToken
	 * @throws CartCheckOutIntegrationException
	 *
	 */
	void callRemovePaymentMethodToken(String paymentMethodToken) throws CartCheckOutIntegrationException;
}
