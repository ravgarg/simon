package com.simon.facades.share.email;

import de.hybris.platform.core.model.order.OrderModel;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;


/**
 * interface ShareEmailFacade
 */
public interface ShareEmailFacade
{

	/**
	 * @description Method use to get Product details and use those details to share product details in email
	 * @method getProductDetailsToSharedProductEmail
	 * @param sharedEmailCommonData
	 * @throws SharedEmailIntegrationException
	 */
	void getProductDetailsToSharedProductEmail(ShareEmailData sharedEmailCommonData) throws SharedEmailIntegrationException;

	/**
	 * Share deal Email.By using Deal Service we get deal from DB and convert all data into {@link ShareDealEmailData}
	 * and by using integration service Email will be share.
	 *
	 * @param dealId
	 *           The deal Id
	 * @param shareDealEmailData
	 *           The Share deal email data
	 * @throws SharedEmailIntegrationException
	 *            If any Email integration exception occurs.
	 */
	void shareDealEmail(final String dealId, final ShareEmailData shareDealEmailData) throws SharedEmailIntegrationException;



	/**
	 * @description Method use to get Order details and use those details to share order confirmation details in email
	 * @method getOrderDetailsToSharedOrderConfirmationOrRejectionEmail
	 * @param orderModel
	 * @param status
	 * @return boolean
	 */
	boolean getOrderDetailsToSharedOrderConfirmationOrRejectionEmail(OrderModel orderModel, String status);
}
