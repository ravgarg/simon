
package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.model.SizeModel;
import com.simon.facades.account.data.SizeData;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * Converts Size Model to Size Data
 */
public class SizePopulator implements Populator<SizeModel, SizeData>
{

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/*
	 * populates the size data from the contents in size model
	 */
	@Override
	public void populate(final SizeModel source, final SizeData target)
	{

		target.setCode(source.getCode());
		target.setCategoryCode(source.getCategory().getCode());

		if ((CollectionUtils.isNotEmpty(source.getCategory().getSupercategories())))
		{
			target.setBaseCategoryName(source.getCategory().getSupercategories().get(0).getName());
			target.setCategoryCode(source.getCategory().getSupercategories().get(0).getName());
		}
		target.setOperatorSize(source.getOperatorSize());
		target.setValue(source.getValue());
		target.setIsSelected(extCustomerFacade.isSizeSelected(source.getCode()));
	}

}

