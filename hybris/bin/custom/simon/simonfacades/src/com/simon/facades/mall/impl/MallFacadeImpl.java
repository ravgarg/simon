package com.simon.facades.mall.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.mall.MallFacade;


/**
 * Provides implementation to perform various Mall related operations in SPO.
 */
public class MallFacadeImpl implements MallFacade
{

	@Resource(name = "mallService")
	private MallService mallService;

	@Resource(name = "mallConverter")
	private Converter<MallModel, MallData> mallConverter;

	/**
	 * Method to get the list of Malls.
	 *
	 * @return listOfMallData.
	 */
	@Override
	public List<MallData> getMallList()
	{
		final List<MallModel> allMalls = mallService.getAllMalls();
		if (!allMalls.isEmpty())
		{
			return mallConverter.convertAll(mallService.getAllMalls());
		}
		else
		{
			return Collections.emptyList();
		}
	}

}
