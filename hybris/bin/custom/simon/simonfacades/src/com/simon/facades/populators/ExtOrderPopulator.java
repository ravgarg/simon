
package com.simon.facades.populators;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionResultService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import javax.annotation.Resource;

import com.simon.core.services.RetailerService;


/**
 * This ExtOrderPopulator populate OrderData for order confirmation page
 */
public class ExtOrderPopulator implements Populator<OrderModel, OrderData>
{
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Resource
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource
	private RetailerService retailerService;

	@Resource
	private Converter<ProductModel, ProductData> productConverter;

	@Resource
	private PromotionResultService promotionResultService;


	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		//set the payment info
		if (source.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel spreedlyModel = (CreditCardPaymentInfoModel) source.getPaymentInfo();
			final CCPaymentInfoData spreedlyData = getExtPaymentInfoConverter().convert(spreedlyModel);
			target.setPaymentInfo(spreedlyData);
			target.setDeliveryCost(getPriceData(source.getDeliveryCost()));
		}
	}


	/**
	 * Gets the price data.
	 *
	 * @param price
	 *           the price
	 * @return the price data
	 */
	private PriceData getPriceData(final double price)
	{

		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(price),
				commonI18NService.getCurrentCurrency().getIsocode());
	}

	/**
	 * @return extPaymentInfoConverter
	 */
	public Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getExtPaymentInfoConverter()
	{
		return extPaymentInfoConverter;
	}


	/**
	 * @param extPaymentInfoConverter
	 */
	public void setExtPaymentInfoConverter(final Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter)
	{
		this.extPaymentInfoConverter = extPaymentInfoConverter;
	}



}
