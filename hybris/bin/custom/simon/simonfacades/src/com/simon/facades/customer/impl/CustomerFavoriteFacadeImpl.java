package com.simon.facades.customer.impl;

import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.customer.CustomerFavoriteFacade;
import com.simon.facades.favorite.data.FavoriteData;
import com.simon.facades.favorite.data.FavoriteTypeEnum;




/**
 * This is the implementation class of CustomerFavoriteFacade interface.
 */
public class CustomerFavoriteFacadeImpl implements CustomerFavoriteFacade
{
	private static final Logger LOG = LoggerFactory.getLogger(CustomerFavoriteFacadeImpl.class);
	@Resource
	private SessionService sessionService;


	/**
	 * This method sets the Favorite data is session.
	 *
	 * @param favType
	 * @param favName
	 * @param favId
	 */
	@Override
	public void setFavoriteDataInSession(final String favType, final String favName, final String favId)
	{
		try
		{
			LOG.debug("While setting Favorite data in session favType {} or favId {} is  ", favType, favId);
			if (StringUtils.isNotEmpty(favId) && StringUtils.isNotEmpty(favType))
			{

				final FavoriteData favData = new FavoriteData();
				favData.setFavId(favId);
				favData.setFavType(FavoriteTypeEnum.valueOf(StringUtils.upperCase(favType)));
				favData.setFavName(favName == null ? StringUtils.EMPTY : favName);
				sessionService.setAttribute(SimonFacadesConstants.FAV_SESSION_DATA, favData);
			}

		}
		catch (final Exception e)
		{
			LOG.error("Exception thrown while setting FavType in  FavoriteData using FavoriteTypeEnum ", e);
		}
	}
}
