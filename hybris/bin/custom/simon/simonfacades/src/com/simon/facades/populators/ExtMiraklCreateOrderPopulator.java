package com.simon.facades.populators;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.mirakl.client.mmp.domain.payment.MiraklPaymentWorkflow;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderOffer;
import com.mirakl.client.mmp.request.additionalfield.MiraklRequestAdditionalFieldValue;
import com.mirakl.client.mmp.request.additionalfield.MiraklRequestAdditionalFieldValue.MiraklSimpleRequestAdditionalFieldValue;
import com.mirakl.hybris.core.order.populators.MiraklCreateOrderPopulator;
import com.simon.core.dao.ExtOrderDao;
import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.core.services.RetailerService;


/**
 * Class is used to set the paymentworkflow.
 *
 * @author skaus9
 *
 */
public class ExtMiraklCreateOrderPopulator extends MiraklCreateOrderPopulator
{
	@Resource
	private RetailerService retailerService;

	@Resource
	private ModelService modelService;

	@Resource
	private ExtOrderDao extOrderDao;

	@Resource
	private ConfigurationService configurationService;

	private static final String GLOBAL_ORDER_ID = "global-order-id";


	/**
	 * Method is used to set the PaymentWorkFlow as <code>Pay on Delivery</code>.
	 *
	 * @param orderModel
	 *           OrderModel
	 * @param miraklCreateOrder
	 *           MirackCreateOrder
	 * @return void.
	 */
	@Override
	public void populate(final OrderModel orderModel, final MiraklCreateOrder miraklCreateOrder) throws ConversionException
	{
		validateParameterNotNull(orderModel.getRetailersOrderNumbers(), "retailer order number expected here");
		validateParameterNotNull(miraklCreateOrder.getCommercialId(),
				"commercialId should not set before calling this populator, however, it will get overridden further");
		validateParameterNotNull(orderModel.getRetailersOrderNumbers().get(miraklCreateOrder.getCommercialId()),
				"CommercialId expected to be set as retailerId");

		final String retailer = miraklCreateOrder.getCommercialId();
		final List<AbstractOrderEntryModel> retailerMarketPlaceEntries = getEntriesToBeExportedForRetailer(orderModel, retailer);
		final List<MiraklCreateOrderOffer> offers = miraklCreateOrderOfferConverter.convertAll(retailerMarketPlaceEntries);
		miraklCreateOrder.setOffers(offers);
		populatedHybrisValues(getOrderWithHybrisDetails(orderModel), retailer, offers);
		miraklCreateOrder.setCommercialId(retailerService.getRetailerOrderNumberForMarketPlace(orderModel, retailer));
		miraklCreateOrder.setScored(scoringAlreadyDone);
		miraklCreateOrder.setCustomer(miraklOrderCustomerConverter.convert(orderModel));
		miraklCreateOrder.setPaymentInfo(miraklCreateOrderPaymentInfoConverter.convert(orderModel.getPaymentInfo()));
		miraklCreateOrder.setShippingZoneCode(shippingZoneStrategy.getShippingZoneCode(orderModel));
		miraklCreateOrder.setPaymentWorkflow(MiraklPaymentWorkflow.PAY_ON_DELIVERY);
		miraklCreateOrder.setOrderAdditionalFields(getOrderAdditionalFields(orderModel, retailer));
	}

	private void populatedHybrisValues(final OrderModel orderModel, final String retailer,
			final List<MiraklCreateOrderOffer> offers)
	{
		final List<AbstractOrderEntryModel> retailerMarketPlaceEntriesFromHybris = orderModel.getMarketplaceEntries().stream()
				.filter(entry -> entry.getProduct().getShop().getId().equalsIgnoreCase(retailer)).collect(Collectors.toList());
		for (final AbstractOrderEntryModel retailerMarketPlaceEntryFromHybris : retailerMarketPlaceEntriesFromHybris)
		{
			final Optional<MiraklCreateOrderOffer> offerOptional = offers.stream()
					.filter(offer -> offer.getId().equals(retailerMarketPlaceEntryFromHybris.getOfferId())).findFirst();
			if (offerOptional.isPresent())
			{
				final MiraklCreateOrderOffer offer = offerOptional.get();
				final BigDecimal unitPrice = BigDecimal.valueOf(retailerMarketPlaceEntryFromHybris.getBasePrice());
				final BigDecimal quantity = BigDecimal.valueOf(retailerMarketPlaceEntryFromHybris.getQuantity());
				offer.setPriceUnit(unitPrice);
				offer.setPrice(unitPrice.multiply(quantity));
			}
		}
	}

	private List<AbstractOrderEntryModel> getEntriesToBeExportedForRetailer(final OrderModel orderModel, final String retailer)
	{
		return orderModel.getMarketplaceEntries().stream().filter(entry -> isEntryEligibleForExport(retailer, entry))
				.collect(Collectors.toList());
	}

	private boolean isEntryEligibleForExport(final String retailer, final AbstractOrderEntryModel entry)
	{
		return entry.getProduct().getShop().getId().equalsIgnoreCase(retailer)
				&& CallBackOrderEntryStatus.APPROVED.equals(entry.getCallBackOrderEntryStatus()) && entry.getQuantity() > 0;
	}

	/**
	 *
	 * **/
	private List<MiraklRequestAdditionalFieldValue> getOrderAdditionalFields(final OrderModel orderModel, final String retailer)
	{
		final List<MiraklRequestAdditionalFieldValue> list = new ArrayList<>();
		final OrderModel hybrisOrderModel = getOrderWithHybrisDetails(orderModel);
		final String additionalField = configurationService.getConfiguration().getString("marketplace.order.custom.field");
		final List<String> orderAdditionalFields = Arrays.asList(additionalField.split(","));

		for (final String orderAdditionalField : orderAdditionalFields)
		{
			OrderModel orderWhichHasTheValue = orderModel;
			final List<String> additionalFieldInfo = Arrays.asList(orderAdditionalField.split("-"));
			if (additionalFieldInfo.contains("hybris"))
			{
				orderWhichHasTheValue = hybrisOrderModel;
			}
			if (GLOBAL_ORDER_ID.equalsIgnoreCase(orderAdditionalField))
			{
				setCustomAttribute(list, orderAdditionalField, orderModel.getCode());
			}
			else
			{
				setCustomAttribute(list, orderAdditionalField,
						getAttributeValue(orderAdditionalField, orderWhichHasTheValue, retailer));
			}
		}
		return list;

	}

	private OrderModel getOrderWithHybrisDetails(final OrderModel orderModel)
	{
		return extOrderDao.getVersionedOrderByOrderId(orderModel.getCode());
	}

	private String getAttributeValue(final String orderAdditionalField, final OrderModel hybrisOrderModel, final String retailer)
	{
		final String attributeName = configurationService.getConfiguration().getString(orderAdditionalField);
		final Object value = ((Map<String, Object>) modelService.getAttributeValue(hybrisOrderModel, attributeName)).get(retailer);
		return value != null ? value.toString() : "";
	}

	private void setCustomAttribute(final List<MiraklRequestAdditionalFieldValue> list, final String customFieldName,
			final String customFieldValue)
	{
		if (StringUtils.isNotEmpty(customFieldValue))
		{
			final MiraklSimpleRequestAdditionalFieldValue miraklSimpleRequestAdditionalFieldValueForTaxAmt = new MiraklSimpleRequestAdditionalFieldValue(
					customFieldName, getInRoundFormat(customFieldName, customFieldValue));
			list.add(miraklSimpleRequestAdditionalFieldValueForTaxAmt);
		}
	}

	private String getInRoundFormat(final String customFieldName, final String customFieldValue)
	{
		if (GLOBAL_ORDER_ID.equalsIgnoreCase(customFieldName))
		{
			return customFieldValue;
		}
		else
		{
			final double value = Double.parseDouble(customFieldValue);
			final DecimalFormat df = new DecimalFormat("#0.00");
			return df.format(value);
		}
	}


}
