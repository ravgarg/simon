package com.simon.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DesignerData;


public class ExtReverseDesignerPopulator implements Populator<DesignerData, DesignerModel>
{

	@Override
	public void populate(final DesignerData source, final DesignerModel target) throws ConversionException
	{
		target.setActive(source.isActive());
		target.setBannerUrl(source.getBannerUrl());
		target.setCode(source.getCode());
		target.setDescription(source.getDescription());
		target.setName(source.getName());
	}

}
