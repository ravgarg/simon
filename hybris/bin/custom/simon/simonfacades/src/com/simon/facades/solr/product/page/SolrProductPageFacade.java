package com.simon.facades.solr.product.page;

import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import com.simon.core.dto.ProductDetailsData;


public interface SolrProductPageFacade
{

	public ProductDetailsData getProductDetails(final SearchStateData searchState, final PageableData pageableData);
}
