package com.simon.facades.cms.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * The Class ExtDefaultPageFacadeImplTest.
 */
@UnitTest
public class ExtPageFacadeImplTest
{


	@InjectMocks
	private ExtPageFacadeImpl extDefaultPageFacadeImpl;
	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test populate url for left nav.
	 */
	@Test
	public void testPopulateUrlForLeftNav()
	{
		final List<FacetData<SearchStateData>> facetDataList = new ArrayList<FacetData<SearchStateData>>();
		final FacetData<SearchStateData> facetData = new FacetData<SearchStateData>();
		final List<FacetValueData<SearchStateData>> facetValueDataList = new ArrayList<FacetValueData<SearchStateData>>();
		final FacetValueData<SearchStateData> facetValueData = new FacetValueData<SearchStateData>();
		final SearchStateData searchStateData = new SearchStateData();
		searchStateData.setUrl(
				"search/testShop1?q=%3Arelevance%3AcategoryPath%3A%252Fm0003334%257CSPO%252Fm100000%257CHome%252Fm200000%257CWomen");
		facetValueData.setQuery(searchStateData);
		facetValueDataList.add(facetValueData);
		facetData.setValues(facetValueDataList);
		facetDataList.add(facetData);
		extDefaultPageFacadeImpl.populateUrlForLeftNav(facetDataList, "/store/testShop1");

	}

}
