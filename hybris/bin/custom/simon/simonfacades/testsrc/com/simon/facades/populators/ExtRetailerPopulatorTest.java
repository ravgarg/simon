package com.simon.facades.populators;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.facades.cart.data.RetailerInfoData;




/**
 * ExtRetailerPopulatorTest test ExtRetailerPopulator set the retailer data like retailer id, retailer name to
 * RetailerInfoData
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtRetailerPopulatorTest
{

	@InjectMocks
	@Spy
	private ExtRetailerPopulator extRetailerPopulator;
	@Mock
	private ShopModel source;
	@Mock
	private CartService cartService;
	@Mock
	private CartModel cartModel;

	/**
	 * populate method test RetailerInfoData object ExtRetailerPopulator set the retailer data like retailer id, retailer
	 * name to RetailerInfoData
	 */
	@Test
	public void populate()
	{
		final RetailerInfoData target = new RetailerInfoData();
		Mockito.doReturn("shopid").when(source).getId();
		Mockito.doReturn("shopname").when(source).getName();
		doReturn("ViewShippingReturns").when(extRetailerPopulator).getViewShippingReturnsMessage();
		final ConsignmentModel ConsignmentModel = new ConsignmentModel();
		final Set<ConsignmentModel> setOfConsignmentModel = new HashSet<>();
		Mockito.doReturn(cartModel).when(cartService).getSessionCart();
		Mockito.doReturn(setOfConsignmentModel).when(cartModel).getConsignments();
		extRetailerPopulator.populate(source, target);
		Assert.assertEquals("shopid", target.getRetailerId());
		Assert.assertEquals("shopname", target.getRetailerName());
	}
}
