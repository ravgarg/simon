package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.model.CustomShopsComponentModel;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.customer.ExtCustomerFacade;


@UnitTest
public class ExtStoreDataPopulatorTest
{
	@InjectMocks
	private ExtStoreDataPopulator extStoreDataPopulator;

	@Mock
	private Converter<CustomShopsComponentModel, StoreDataList> extStoreDataConverter;
	@Mock
	private ExtCustomerFacade extCustomerFacade;

	private final StoreDataList target = new StoreDataList();

	private final CustomShopsComponentModel source = new CustomShopsComponentModel();

	final List<ShopModel> shopList = new ArrayList<ShopModel>();
	final ShopModel shop = new ShopModel();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		shop.setId("5000");
		shop.setDescription("sells the clothing");
		shop.setName("I like no other");
		shop.setState(ShopState.OPEN);
		shopList.add(shop);
		source.setListOfShops(shopList);
	}

	@Test
	public void testPopulateForEmptyShopList() throws ConversionException
	{
		source.setListOfShops(CollectionUtils.EMPTY_COLLECTION);
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isEmpty(target.getAllStoresList()));
	}

	@Test
	public void testPopulateForNonEmptyShopList() throws ConversionException
	{
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getAllStoresList()));
		Assert.assertEquals(shop.getId(), target.getAllStoresList().get(0).getId());
	}

	@Test
	public void testPopulateForNonEmptyBackgroundImageInShopList() throws ConversionException
	{
		final MediaModel backgroundImage = new MediaModel();
		backgroundImage.setURL("bgUrl");
		shop.setBackgroundImage(backgroundImage);
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getAllStoresList()));
		Assert.assertEquals(shop.getBackgroundImage().getURL(), target.getAllStoresList().get(0).getBackgroundImage());
	}

	@Test
	public void testPopulateForNonEmptyBannerInShopList() throws ConversionException
	{
		final MediaModel banner = new MediaModel();
		banner.setURL("logoUrl");
		shop.setBanner(banner);
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getAllStoresList()));
		Assert.assertEquals(shop.getBanner().getURL(), target.getAllStoresList().get(0).getBanner());
	}

	@Test
	public void testPopulateForNonEmptyLogoInShopList() throws ConversionException
	{
		final MediaModel logo = new MediaModel();
		logo.setURL("logoUrl");
		shop.setLogo(logo);
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getAllStoresList()));
		Assert.assertEquals(shop.getLogo().getURL(), target.getAllStoresList().get(0).getLogo());
	}

	@Test
	public void testPopulateForNonEmptyPageInShopList() throws ConversionException
	{
		final ContentPageModel page = new ContentPageModel();
		page.setLabel("/homepage");
		page.setPageType(ContentPageType.LISTINGPAGE);
		final Collection<ContentPageModel> contentModels = new ArrayList<>();
		contentModels.add(page);
		shop.setContentPage(contentModels);
		extStoreDataPopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getAllStoresList()));
		Assert.assertEquals(page.getLabel(), target.getAllStoresList().get(0).getContentPageLabel());
	}

}
