package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sape.junit.FunctionalTest;
import com.simon.core.constants.SimonCoreConstants;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
@FunctionalTest(features =
{ SimonCoreConstants.CART })
public class ExtOrderEntryVariantValuePopulatorTest
{
	@InjectMocks
	private ExtOrderEntryVariantValuePopulator<AbstractOrderEntryModel, OrderEntryData> extOrderEntryVariantValuePopulator;

	@Mock
	private AbstractOrderEntryModel eachOrderEntryModel;

	@Mock
	private ProductModel baseproductModel;

	@Mock
	private GenericVariantProductModel variantProductModel;

	@Mock
	private VariantCategoryModel categoryModel;

	@Mock
	private VariantValueCategoryModel valuesCategoryModel;

	@Mock
	private VariantCategoryModel variantCategoryModel;

	@Mock
	private CategoryService categoryService;

	/**
	 * this method test ExtPriceDataPopulator which set the list value, sales value and msrp value in OrderEntryData
	 * object
	 */
	@Test
	public void populate()
	{

		final Collection<List<CategoryModel>> pathsForCategory = new ArrayList<>();
		final List<CategoryModel> categoryModels = new ArrayList();
		categoryModels.add(categoryModel);
		pathsForCategory.add(categoryModels);
		final List<CategoryModel> valuescategoryModels = new ArrayList();
		valuescategoryModels.add(valuesCategoryModel);

		final List<CategoryModel> variantcategoryModels = new ArrayList();
		variantcategoryModels.add(variantCategoryModel);

		final OrderEntryData orderEntryData = new OrderEntryData();
		final ProductData productData = new ProductData();
		orderEntryData.setProduct(productData);

		Mockito.doReturn(variantProductModel).when(eachOrderEntryModel).getProduct();
		Mockito.doReturn(baseproductModel).when(variantProductModel).getBaseProduct();

		Mockito.doReturn("color").when(categoryModel).getCode();
		Mockito.doReturn("color1").when(variantCategoryModel).getCode();

		Mockito.doReturn("color").when(categoryModel).getName(Locale.ENGLISH);
		Mockito.doReturn("RED").when(variantCategoryModel).getName(Locale.ENGLISH);


		Mockito.doReturn(valuescategoryModels).when(variantProductModel).getSupercategories();

		Mockito.doReturn(categoryModels).when(baseproductModel).getSupercategories();

		Mockito.doReturn(valuescategoryModels).when(categoryModel).getAllSubcategories();

		Mockito.doReturn(pathsForCategory).when(categoryService).getPathsForCategory(categoryModel);

		Mockito.doReturn(variantcategoryModels).when(valuesCategoryModel).getSupercategories();

		extOrderEntryVariantValuePopulator.populate(eachOrderEntryModel, orderEntryData);
		Assert.assertNotNull(orderEntryData.getProduct().getVariantValues());
		Assert.assertEquals(orderEntryData.getProduct().getVariantValues().size(), 0);
	}


}
