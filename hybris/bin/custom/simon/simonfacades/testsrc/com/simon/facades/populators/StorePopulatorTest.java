package com.simon.facades.populators;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cmsfacades.data.CMSLinkComponentData;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.core.model.media.MediaModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.model.BackgroundImageItemModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.HeroBannerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.cms.BackgroundImageData;
import com.simon.facades.customer.ExtCustomerFacade;



/**
 * This is a test class to test store populator.
 */
public class StorePopulatorTest
{

	/** The store populator. */
	@InjectMocks
	StorePopulator storePopulator;

	@Mock
	private ExtCustomerFacade extCustomerFacade;
	@Mock
	private ExtDealPopulator extDealPopulator;

	/** The shop model. */
	@Mock
	private ShopModel shopModel;

	private StoreData storeData;
	private MediaModel mediaModel;
	private PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel heroBannerModel;
	private HeroBannerData heroBannerData;

	private MediaData mediaData;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}


	/**
	 * This Method test populate method of store populator which populate ShopModel in StoreData
	 */
	@Test
	public void testPopulateMethod()
	{
		final MediaModel mediaModel = Mockito.mock(MediaModel.class);
		final PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel heroBannerModel = Mockito
				.mock(PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel.class);
		final ShopModel shopModel = mock(ShopModel.class);
		final Collection<ContentPageModel> contentModels = new ArrayList<>();
		final ContentPageModel contentPageModel = mock(ContentPageModel.class);
		contentModels.add(contentPageModel);
		contentPageModel.setLabel("/store/test");
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		given(shopModel.getLogo()).willReturn(mediaModel);
		given(shopModel.getBanner()).willReturn(mediaModel);
		given(shopModel.getMobileBackgroundImage()).willReturn(mediaModel);
		given(shopModel.getMobileLogo()).willReturn(mediaModel);
		given(shopModel.getId()).willReturn("StoreId");
		given(shopModel.getName()).willReturn("test");
		given(shopModel.getBackgroundImage()).willReturn(mediaModel);
		given(shopModel.getDescription()).willReturn("Unit testing");
		given(shopModel.getHeroBanner()).willReturn(heroBannerModel);
		given(shopModel.getContentPage()).willReturn(contentModels);
		final StoreData storeData = new StoreData();
		storePopulator.populate(shopModel, storeData);
		shopModel.setBanner(mediaModel);
		Mockito.doNothing().when(mediaModel).setURL("URL");
		Assert.assertEquals("StoreId", storeData.getId());

	}


	@Test
	public void testPopulateImageData()
	{
		final MediaModel mediaModel = Mockito.mock(MediaModel.class);
		final MediaData mediaData = mock(MediaData.class);
		given(mediaModel.getAltText()).willReturn("altText");
		given(mediaModel.getURL()).willReturn("url");
		given(mediaModel.getCode()).willReturn("code");
		given(mediaModel.getDownloadURL()).willReturn("downloadURL");
		given(mediaModel.getMime()).willReturn("Mime");
		Mockito.doNothing().when(mediaData).setUrl("url");
		storePopulator.populateImageData(mediaModel, mediaData);

		Assert.assertNotNull(mediaData);

	}

	@Test
	public void testPopualteCMSLinkData()
	{
		final CMSLinkComponentModel cmdLinkComponentModel = Mockito.mock(CMSLinkComponentModel.class);
		final CMSLinkComponentData cmsLinkComponentData = mock(CMSLinkComponentData.class);
		given(cmdLinkComponentModel.getUrl()).willReturn("url");
		Mockito.doNothing().when(cmsLinkComponentData).setUrl("url");
		storePopulator.popualteCMSLinkData(cmdLinkComponentModel, cmsLinkComponentData);

		Assert.assertNotNull(cmsLinkComponentData);

	}

	@Test
	public void testPopualteBackGroundImgData()
	{
		final BackgroundImageItemModel backgroundImageItemModel = Mockito.mock(BackgroundImageItemModel.class);
		final BackgroundImageData backgroundImageData = mock(BackgroundImageData.class);
		final MediaModel deskImageMediaModel = mock(MediaModel.class);
		final MediaModel mobImageMediaModel = mock(MediaModel.class);
		final CMSLinkComponentModel cmsLinkComponentModel = mock(CMSLinkComponentModel.class);
		given(backgroundImageItemModel.getDesktopImage()).willReturn(deskImageMediaModel);
		given(backgroundImageItemModel.getMobileImage()).willReturn(mobImageMediaModel);
		given(backgroundImageItemModel.getDestinationLink()).willReturn(cmsLinkComponentModel);

		storePopulator.populateBackGroundImgData(backgroundImageItemModel, backgroundImageData);

		Assert.assertNotNull(backgroundImageData);

	}

	@Test
	public void testPopualteBackGroundImgDataNull()
	{
		final BackgroundImageItemModel backgroundImageItemModel = Mockito.mock(BackgroundImageItemModel.class);
		final BackgroundImageData backgroundImageData = mock(BackgroundImageData.class);
		given(backgroundImageItemModel.getDesktopImage()).willReturn(null);
		given(backgroundImageItemModel.getMobileImage()).willReturn(null);
		given(backgroundImageItemModel.getDestinationLink()).willReturn(null);

		storePopulator.populateBackGroundImgData(backgroundImageItemModel, backgroundImageData);

		Assert.assertNotNull(backgroundImageData);

	}

	@Test
	public void testSetLinkRelatedData()
	{
		final HeroBannerData heroBannerData = mock(HeroBannerData.class);
		final CMSLinkComponentModel cmsLinkComponentModel = mock(CMSLinkComponentModel.class);
		given(cmsLinkComponentModel.getUrl()).willReturn("URL");
		given(cmsLinkComponentModel.getLinkName()).willReturn("Link Name");

		storePopulator.setLinkRelatedData(cmsLinkComponentModel, heroBannerData);

		Assert.assertNotNull(heroBannerData);

	}

	@Test
	public void testPopulateDealData()
	{
		final StoreData storeData = mock(StoreData.class);
		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		final DealModel dealModel = mock(DealModel.class);
		final DealData dealData = mock(DealData.class);
		final ShopModel shopModel = mock(ShopModel.class);
		final MediaModel mediaModel = Mockito.mock(MediaModel.class);
		Mockito.when(dealModel.getShop()).thenReturn(shopModel);
		Mockito.when(shopModel.getLogo()).thenReturn(mediaModel);
		given(dealModel.getCode()).willReturn("Code");
		given(dealModel.getDescription()).willReturn("Desc");
		given(mediaModel.getURL()).willReturn("ImageUrl");
		given(dealModel.getName()).willReturn("Name");
		given(dealModel.getStartDate()).willReturn(startDate);
		given(dealModel.getEndDate()).willReturn(endDate);

		final Set<DealModel> dealsModel = new HashSet<>();
		dealsModel.add(dealModel);

		when(extCustomerFacade.isFavDeal("10002")).thenReturn(true);

		Mockito.doNothing().when(dealData).setName("Name");
		final List<DealData> deals = new ArrayList<>();

		storeData.setDeals(deals);
		storePopulator.populateDealData(dealsModel, storeData);

		Assert.assertNotNull(storeData.getDeals());

	}

	@Test
	public void testPopulateMethodNullDeals()
	{
		final MediaModel mediaModel = Mockito.mock(MediaModel.class);
		final PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel heroBannerModel = Mockito
				.mock(PromoBanner1ClickableImageWithTextAndSingleCTAComponentModel.class);
		final ShopModel shopModel = mock(ShopModel.class);
		final Collection<ContentPageModel> contentModels = new ArrayList<>();
		final ContentPageModel contentPageModel = mock(ContentPageModel.class);
		contentPageModel.setLabel("/store/test");
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		contentModels.add(contentPageModel);
		given(shopModel.getLogo()).willReturn(mediaModel);
		given(shopModel.getBanner()).willReturn(mediaModel);
		given(shopModel.getMobileBackgroundImage()).willReturn(mediaModel);
		given(shopModel.getMobileLogo()).willReturn(mediaModel);
		given(shopModel.getId()).willReturn("StoreId");
		given(shopModel.getName()).willReturn("test");
		given(shopModel.getBackgroundImage()).willReturn(mediaModel);
		given(shopModel.getDescription()).willReturn("Unit testing");
		given(shopModel.getHeroBanner()).willReturn(heroBannerModel);
		given(shopModel.getContentPage()).willReturn(contentModels);

		final StoreData storeData = new StoreData();
		storeData.setDeals(null);
		storePopulator.populate(shopModel, storeData);
		shopModel.setBanner(mediaModel);
		Mockito.doNothing().when(mediaModel).setURL("URL");
		Assert.assertEquals("StoreId", storeData.getId());

	}

}
