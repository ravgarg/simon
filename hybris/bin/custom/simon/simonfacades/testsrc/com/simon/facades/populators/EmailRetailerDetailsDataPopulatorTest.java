package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


/**
 * Test Class for EmailRetailerDetailsDataPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailRetailerDetailsDataPopulatorTest
{
	@InjectMocks
	private EmailRetailerDetailsDataPopulator emailRetailerDetailsDataPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	/**
	 * Initialize mocks
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}

	/**
	 * Test populate Method Valid Retailer Data
	 */
	@Test
	public void testpopulateWhenRetailerDataNotNull()
	{
		final RetailerInfoData source = mock(RetailerInfoData.class);
		final RetailerDetailsData target = new RetailerDetailsData();
		when(source.getRetailerId()).thenReturn("retailerId");
		when(source.getRetailerName()).thenReturn("retailerName");
		when(source.getRetailerOrderNumber()).thenReturn("OrderNUm");
		when(source.getSelectedShippingMethod()).thenReturn("ShippingMethod");
		emailRetailerDetailsDataPopulator.populate(source, target);
		assertEquals("retailerId", target.getRetailerId());
	}

	/**
	 * Test populate Method for NUll data
	 */
	@Test
	public void testpopulateWhenRetailerDataNull()
	{
		final RetailerInfoData source = mock(RetailerInfoData.class);
		final RetailerDetailsData target = new RetailerDetailsData();
		when(source.getRetailerId()).thenReturn(null);
		when(source.getRetailerName()).thenReturn(null);
		when(source.getRetailerOrderNumber()).thenReturn(null);
		when(source.getSelectedShippingMethod()).thenReturn(null);
		emailRetailerDetailsDataPopulator.populate(source, target);
		assertEquals(StringUtils.EMPTY, target.getRetailerId());
	}

}
