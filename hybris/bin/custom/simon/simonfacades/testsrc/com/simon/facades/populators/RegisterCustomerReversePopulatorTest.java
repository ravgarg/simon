package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;
import com.simon.facades.customer.data.MallData;


@UnitTest
public class RegisterCustomerReversePopulatorTest
{

	@InjectMocks
	RegisterCustomerReversePopulator registerCustomerReversePopulator;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private CustomerNameStrategy customerNameStrategy;

	@Mock
	private EnumerationService enumerationService;

	@Mock
	private MallService mallService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testPopulate()
	{
		final CustomerModel target = new CustomerModel();
		final RegisterData source = new RegisterData();
		source.setExternalId("123");
		source.setFirstName("Test_Dummy");
		source.setLastName("Test_Dummy");
		source.setLogin("Test_Dummy");
		source.setPassword("Test_Dummy");
		source.setGender("FEMALE");
		source.setBirthMonth("JAN");
		source.setBirthYear(1999);
		source.setCountry("US");
		source.setZipcode("12911");
		final MallData mallData = new MallData();
		mallData.setCode("123");
		source.setPrimaryMall(mallData);
		final MallModel mallModel = Mockito.mock(MallModel.class);
		Mockito.when(mallService.getMallForCode(source.getPrimaryMall().getCode())).thenReturn(mallModel);
		final MallData mallData1 = new MallData(), mallData2 = new MallData(), mallData3 = new MallData();
		mallData1.setCode("123");
		mallData2.setCode("456");
		mallData3.setCode("789");
		final List<MallData> alternateMalls = new ArrayList<>();
		alternateMalls.add(mallData1);
		alternateMalls.add(mallData2);
		alternateMalls.add(mallData3);
		source.setAlternateMalls(alternateMalls);
		final List<String> mallCodes = Arrays.asList("123", "456", "789");
		final Set<MallModel> mallModelSet = new HashSet<>();
		mallModelSet.add(mallModel);
		Mockito.when(mallService.getMallListForCode(mallCodes)).thenReturn(mallModelSet);
		registerCustomerReversePopulator.populate(source, target);
		Assert.assertEquals(target.getZipCode(), "12911");
	}

	@Test
	public void testPopulateNullGenderCountryMonth()
	{
		final CustomerModel target = new CustomerModel();
		final RegisterData source = new RegisterData();
		source.setExternalId("123");
		source.setFirstName("Test_Dummy");
		source.setLastName("Test_Dummy");
		source.setLogin("Test_Dummy");
		source.setPassword("Test_Dummy");
		source.setBirthYear(1999);
		source.setZipcode("12911");
		final MallData mallData = new MallData();
		mallData.setCode("123");
		source.setPrimaryMall(mallData);
		final MallModel mallModel = Mockito.mock(MallModel.class);
		Mockito.when(mallService.getMallForCode(source.getPrimaryMall().getCode())).thenReturn(mallModel);
		final MallData mallData1 = new MallData(), mallData2 = new MallData(), mallData3 = new MallData();
		mallData1.setCode("123");
		mallData2.setCode("456");
		mallData3.setCode("789");
		final List<MallData> alternateMalls = new ArrayList<>();
		alternateMalls.add(mallData1);
		alternateMalls.add(mallData2);
		alternateMalls.add(mallData3);
		source.setAlternateMalls(alternateMalls);
		final List<String> mallCodes = Arrays.asList("123", "456", "789");
		final Set<MallModel> mallModelSet = new HashSet<>();
		mallModelSet.add(mallModel);
		Mockito.when(mallService.getMallListForCode(mallCodes)).thenReturn(mallModelSet);
		registerCustomerReversePopulator.populate(source, target);
		Assert.assertEquals(target.getZipCode(), "12911");
	}

	@Test
	public void testPopulateEmptyPrimaryAndAlternateMall()
	{
		final CustomerModel target = new CustomerModel();
		final RegisterData source = new RegisterData();
		source.setExternalId("123");
		source.setFirstName("Test_Dummy");
		source.setLastName("Test_Dummy");
		source.setLogin("Test_Dummy");
		source.setPassword("Test_Dummy");
		source.setGender("FEMALE");
		source.setBirthMonth("JAN");
		source.setBirthYear(1999);
		source.setCountry("US");
		source.setZipcode("12911");
		registerCustomerReversePopulator.populate(source, target);
		Assert.assertEquals(target.getZipCode(), "12911");
	}




}
