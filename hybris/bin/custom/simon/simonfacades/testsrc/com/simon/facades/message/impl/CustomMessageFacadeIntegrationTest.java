package com.simon.facades.message.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.core.exceptions.SystemException;
import com.simon.facades.message.CustomMessageFacade;


/**
 * Integration test suite for {@link CustomMessageFacadeImpl}.
 */
@IntegrationTest
public class CustomMessageFacadeIntegrationTest extends ServicelayerTransactionalTest
{
	@Resource
	CustomMessageFacade customMessageFacade;

	@Resource
	private UserService userService;

	@Resource
	private CatalogVersionService catalogVersionService;


	@Before
	public void setUp() throws Exception
	{
		userService.setCurrentUser(userService.getAnonymousUser());
		importCsv("/simonfacades/test/testCustomMessageFacade.csv", "utf-8");
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion("testCatalog", "Online");
		assertNotNull(catalogVersionModel);
		catalogVersionService.setSessionCatalogVersions(Collections.singletonList(catalogVersionModel));
	}


	@Test
	public void testSimonMessageTextWithCorrectCode()
	{
		final String code = "label.text.home";
		assertEquals("Home Customize Label", customMessageFacade.getMessageTextForCode(code));
	}


	@Test 
	public void getMessageTextForCode_whichHasAnEmptyMessage()
	{
		final String code = "label.text.home.custom";
		assertEquals("", customMessageFacade.getMessageTextForCode(code));
	}



	@Test(expected = SystemException.class)
	public void getMessageTextForCode_whenModelNotFound()
	{
		final String code = "label.text.home.error";
	  customMessageFacade.getMessageTextForCode(code);
	}


}
