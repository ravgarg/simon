package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.simon.core.model.ShippingDetailsModel;


/**
 * The Class ShippingDetailsReversePopulatorTest.
 */
@UnitTest
public class ShippingDetailsReversePopulatorTest
{
	@InjectMocks
	private final ShippingDetailsReversePopulator shippingDetailsReversePopulator = new ShippingDetailsReversePopulator();



	/**
	 * test the population of data from Shipping Details Model to Map
	 */
	@Test
	public void testPopulate()
	{
		final Map<String, String> source = new HashMap<String, String>();
		source.put("1", "Test");

		final ShippingDetailsModel target = new ShippingDetailsModel();

		final Map.Entry<String, String> entry = source.entrySet().iterator().next();
		target.setCode(entry.getKey());
		target.setDescription(entry.getValue());
		shippingDetailsReversePopulator.populate(source, target);
		Assert.assertEquals(target.getCode(), entry.getKey());

	}

}
