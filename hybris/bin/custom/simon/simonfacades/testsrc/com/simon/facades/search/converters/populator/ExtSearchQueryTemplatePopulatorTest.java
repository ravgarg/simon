package com.simon.facades.search.converters.populator;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.model.SolrSearchQueryTemplateModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * The Class ExtSearchQueryTemplatePopulatorTest.
 */
@UnitTest
public class ExtSearchQueryTemplatePopulatorTest
{

	/** The ext search query template populator. */
	@Spy
	@InjectMocks
	private ExtSearchQueryTemplatePopulator extSearchQueryTemplatePopulator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test within group sort properties.
	 */
	@Test
	public void testWithinGroupSortProperties()
	{
		final SolrSearchQueryTemplateModel source = Mockito.mock(SolrSearchQueryTemplateModel.class);
		final SearchQueryTemplate target = new SearchQueryTemplate();
		target.setWithinGroupSortProperties("test");
		doNothing().when(extSearchQueryTemplatePopulator).callSuperMethod(source, target);
		doReturn("abc").when(source).getWithinGroupSortProperties();
		extSearchQueryTemplatePopulator.populate(source, target);
		Assert.assertEquals("abc", target.getWithinGroupSortProperties());
	}
}
