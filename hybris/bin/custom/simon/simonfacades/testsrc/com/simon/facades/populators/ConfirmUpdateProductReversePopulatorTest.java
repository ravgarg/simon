package com.simon.facades.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConfirmUpdateProductReversePopulatorTest
{
	@InjectMocks
	private ConfirmUpdateProductReversePopulator confirmUpdateProductReversePopulator;
	@Mock
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;

	@Test
	public void confirmUpdateProductReversePopulatorPopulateTest()
	{
		final AdditionalProductDetailsModel target = mock(AdditionalProductDetailsModel.class);

		final OrderConfirmCartProductsCallBackDTO source = mock(OrderConfirmCartProductsCallBackDTO.class);

		source.setDiscountedPrice("$10.00");
		source.setOriginalPrice("$11.00");
		source.setPrice("$12.00");
		source.setStatus("Done");
		source.setProductId("P1");

		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$10.00")).thenReturn(10.00);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$11.00")).thenReturn(11.00);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$12.00")).thenReturn(12.00);

		when(target.getStatus()).thenReturn("Done");
		when(target.getProductID()).thenReturn("P1");

		confirmUpdateProductReversePopulator.populate(source, target);

		Assert.assertEquals("Done", target.getStatus());
		Assert.assertEquals("P1", target.getProductID());

	}

}
