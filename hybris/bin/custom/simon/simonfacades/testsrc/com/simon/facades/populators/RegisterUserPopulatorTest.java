package com.simon.facades.populators;

import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.utils.UserIntegrationUtils;


public class RegisterUserPopulatorTest
{
	@InjectMocks
	private RegisterUserPopulator registerUserPopulator;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private Converter<MallData, UserCenterIdDTO> userCenterIdDataConverter;

	private RegisterData source;
	private UserRegisterUpdateRequestDTO target;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		source = new RegisterData();
		source.setFirstName("firstName");
		source.setLastName("lastName");
		source.setLogin("email");
		source.setZipcode("12344");
		source.setBirthYear(1992);
		source.setPassword("pass");
		source.setMobileNumber("9876543210");
		source.setBirthMonth("Jan");
		source.setHouseholdIncomeCode(1);
		final List<MallData> alternateMalls = new ArrayList<>();
		final MallData mallData1 = new MallData();
		mallData1.setCode("111");
		final MallData mallData2 = new MallData();
		mallData2.setCode("222");
		final MallData mallData3 = new MallData();
		mallData3.setCode("333");
		alternateMalls.add(mallData1);
		alternateMalls.add(mallData2);
		alternateMalls.add(mallData3);
		source.setAlternateMalls(alternateMalls);

		final MallData primaryMall = new MallData();
		primaryMall.setCode("ID");
		primaryMall.setName("OutletName");

		source.setPrimaryMall(primaryMall);
		source.setCountry("US");
		final UserCenterIdDTO userCenterIdDTO = new UserCenterIdDTO();
		userCenterIdDTO.setId("ID");
		userCenterIdDTO.setOutletName("outletName");
		Mockito.when(userCenterIdDataConverter.convert(primaryMall)).thenReturn(userCenterIdDTO);
		final CountryModel country = new CountryModel();
		country.setExternalId("ID");
		Mockito.when(commonI18NService.getCountry("US")).thenReturn(country);
		Mockito.when(userIntegrationUtils.getBirthMonthCode("Jan")).thenReturn("1");
		target = new UserRegisterUpdateRequestDTO();
	}

	@Test
	public void testpopulateWithMale()
	{
		source.setGender("MALE");
		Mockito.when(userIntegrationUtils.getGenderCode("MALE")).thenReturn("1");
		registerUserPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), "1");
	}

	@Test
	public void testpopulateWithFeMale()
	{
		source.setGender("FEMALE");
		Mockito.when(userIntegrationUtils.getGenderCode("FEMALE")).thenReturn("2");
		registerUserPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), "2");
	}

	@Test
	public void testpopulateWithOther()
	{
		source.setGender("OTHERS");
		Mockito.when(userIntegrationUtils.getGenderCode("OTHERS")).thenReturn("0");
		registerUserPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), "0");
	}

	@Test
	public void testpopulateEmptyAlternateMall()
	{
		source.setAlternateMalls(null);
		source.setGender("OTHERS");
		Mockito.when(userIntegrationUtils.getGenderCode("OTHERS")).thenReturn("0");
		registerUserPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), "0");
	}

}
