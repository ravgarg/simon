package com.simon.facades.populators;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.additionalfield.MiraklAdditionalFieldType;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue.MiraklAbstractAdditionalFieldWithSingleValue;
import com.mirakl.client.mmp.domain.shop.MiraklShop;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants.ShopCustomFields;
import com.simon.core.enums.OrderExportType;


/**
 * ExtShopPopulatorTest unit test for {@link ExtShopPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtShopPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtShopPopulator extShopPopulator;

	@Mock
	private ModelService modelService;

	@Mock
	private MediaService mediaService;

	@Mock
	private CatalogVersionService catalogVersionService;

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		doNothing().when(extShopPopulator).populateDefault(any(), any());
	}


	/**
	 * Verify prefix updated correctly.
	 */
	@Test
	public void verifyPrefixUpdatedCorrectly()
	{
		final MiraklShop miraklShop = mock(MiraklShop.class);
		final ShopModel shopModel = mock(ShopModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("COLE");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ShopCustomFields.PREFIX);
		when(miraklShop.getAdditionalFieldValues()).thenReturn(Arrays.asList(additionalFieldValue));
		extShopPopulator.populate(miraklShop, shopModel);
		verify(shopModel).setPreFix("COLE");
	}


	@Test
	public void verifyOrderExportUpdatedCorrectly()
	{
		final MiraklShop miraklShop = mock(MiraklShop.class);
		final ShopModel shopModel = mock(ShopModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("BOT");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ShopCustomFields.ORDER_EXPORT);
		when(miraklShop.getAdditionalFieldValues()).thenReturn(Arrays.asList(additionalFieldValue));
		extShopPopulator.populate(miraklShop, shopModel);
		verify(shopModel).setOrderExportType(OrderExportType.BOT);
	}

}
