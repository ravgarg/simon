package com.simon.facades.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * This is a JUnit class of UserAddressPopulator
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UserAddressPopulatorTest
{

	@InjectMocks
	private UserAddressPopulator userAddressPopulator;

	@Mock
	private UserIntegrationUtils userIntegrationUtils;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void addUserAddressTest()
	{

		final String envId = "local_123";
		final ExtAddressData source = mock(ExtAddressData.class);
		source.setId("123");
		when(userIntegrationUtils.addEnvPrefixToAddressId(source.getId())).thenReturn(envId);

		source.setFirstName("FirstNamee");
		source.setLastName("LastNamee");
		source.setTown("Cityy");
		source.setLine1("Line11");
		source.setLine2("Line12");
		source.setPostalCode("12345");
		source.setDefaultAddress(true);

		final CountryData countryData = mock(CountryData.class);
		countryData.setIsocode("US");
		source.setCountry(countryData);

		final RegionData regionData = mock(RegionData.class);
		regionData.setIsocode("IN");
		source.setRegion(regionData);

		when(source.getFirstName()).thenReturn("FirstNamee");
		when(source.getLastName()).thenReturn("LastNamee");
		when(source.getTown()).thenReturn("Cityy");
		when(source.getLine1()).thenReturn("Line11");
		when(source.getLine2()).thenReturn("Line12");
		when(source.getPostalCode()).thenReturn("12345");
		when(source.isDefaultAddress()).thenReturn(true);
		when(source.getCountry()).thenReturn(countryData);
		when(source.getRegion()).thenReturn(regionData);

		final UserAddressDTO target = mock(UserAddressDTO.class);

		when(target.getFirstName()).thenReturn("FirstNamee");
		when(target.getLastName()).thenReturn("LastNamee");

		userAddressPopulator.populate(source, target);

		Assert.assertEquals("FirstNamee", target.getFirstName());

	}

	@Test
	public void addUserAddressForEmptyTest()
	{

		final String envId = "local_123";
		final ExtAddressData source = mock(ExtAddressData.class);
		source.setId("123");
		when(userIntegrationUtils.addEnvPrefixToAddressId(source.getId())).thenReturn(envId);

		source.setFirstName(null);
		source.setLastName(null);
		source.setTown(null);
		source.setLine1(null);
		source.setLine2(null);
		source.setPostalCode(null);
		source.setDefaultAddress(true);

		final CountryData countryData = mock(CountryData.class);
		countryData.setIsocode(null);
		source.setCountry(countryData);

		final RegionData regionData = mock(RegionData.class);
		regionData.setIsocode(null);
		source.setRegion(regionData);

		when(source.getFirstName()).thenReturn(null);
		when(source.getLastName()).thenReturn(null);
		when(source.getTown()).thenReturn(null);
		when(source.getLine1()).thenReturn(null);
		when(source.getLine2()).thenReturn(null);
		when(source.getPostalCode()).thenReturn(null);
		when(source.isDefaultAddress()).thenReturn(false);
		when(source.getCountry()).thenReturn(null);
		when(source.getRegion()).thenReturn(null);

		final UserAddressDTO target = mock(UserAddressDTO.class);

		when(target.getFirstName()).thenReturn(null);
		when(target.getLastName()).thenReturn(null);

		userAddressPopulator.populate(source, target);

		Assert.assertEquals(null, target.getFirstName());

	}


}
