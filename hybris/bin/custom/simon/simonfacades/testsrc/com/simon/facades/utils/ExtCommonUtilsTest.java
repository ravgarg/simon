package com.simon.facades.utils;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.util.CommonUtils;


/**
 * Test Class For SimonFacadeUtils
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCommonUtilsTest
{
	/**
	 * Test Format valid for Same Month
	 */
	@Test
	public void testFormatValidForSameMonth()
	{
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		final Date enddate = new GregorianCalendar(2018, Calendar.JANUARY, 25).getTime();
		final String formatDate = CommonUtils.formateOfferValid(startdate, enddate);
		assertEquals("Valid Jan. 11-25", formatDate);
	}

	/**
	 * Test Format valid for Different Month
	 */
	@Test
	public void testFormatValidForDiffMonth()
	{
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		final Date enddate = new GregorianCalendar(2018, Calendar.FEBRUARY, 25).getTime();
		final String formatDate = CommonUtils.formateOfferValid(startdate, enddate);
		assertEquals("Valid Jan. 11 - Feb. 25", formatDate);
	}

	/**
	 * Test Format valid for Different Month
	 */
	@Test
	public void testFormatValidFornull()
	{
		final Date startdate = null;
		final Date enddate = null;
		final String formatDate = CommonUtils.formateOfferValid(startdate, enddate);
		assertEquals("", formatDate);
	}

	/**
	 * Test Format valid for Different Month
	 */
	@Test
	public void convertMapIntoLowerCase()
	{
		final Map<String, String> capitalMap = new HashMap<>();
		capitalMap.put("Test", "TestValue");
		final Map<String, String> lowerCase = CommonUtils.convertMapIntoLowerCase(capitalMap);
		assertEquals("test", lowerCase.keySet().iterator().next());
	}


}
