package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailOrderStatusDataPopulatorTest
{
	@InjectMocks
	private EmailOrderStatusDataPopulator emailOrderStatusDataPopulator;

	/**
	 * Initialize mocks
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}

	/**
	 * Test Populate Method for Order Rejected
	 */
	@Test
	public void testpopulateForOrderRejected()
	{

		final OrderData source = mock(OrderData.class);
		final EmailOrderData target = new EmailOrderData();
		final List<RetailerDetailsData> retailerDetailList = new ArrayList<>();
		final RetailerDetailsData targetRetailerData = new RetailerDetailsData();
		targetRetailerData.setStatus("Cancelled");
		retailerDetailList.add(targetRetailerData);
		target.setRetailersDetail(retailerDetailList);
		final List<ProductDetailsData> productDetailslist = new ArrayList<>();
		final ProductDetailsData productData = new ProductDetailsData();
		productData.setStatus("Cancelled");
		targetRetailerData.setProductsDetail(productDetailslist);
		final EmailOrderDetailsData orderDetailsData = new EmailOrderDetailsData();
		target.setOrderDetail(orderDetailsData);
		emailOrderStatusDataPopulator.populate(source, target);
		assertEquals("Order Rejected", target.getOrderDetail().getOrderStatus());
	}

	/**
	 * Test Populate Method for Order Rejected
	 */
	@Test
	public void testpopulateForStoreRejected()
	{

		final OrderData source = mock(OrderData.class);
		final EmailOrderData target = new EmailOrderData();
		final List<RetailerDetailsData> retailerDetailList = new ArrayList<>();
		final RetailerDetailsData targetRetailerData = new RetailerDetailsData();
		targetRetailerData.setStatus("Success");
		final RetailerDetailsData targetRetailerData1 = new RetailerDetailsData();
		targetRetailerData1.setStatus("Cancelled");
		retailerDetailList.add(targetRetailerData);
		retailerDetailList.add(targetRetailerData1);
		target.setRetailersDetail(retailerDetailList);
		final List<ProductDetailsData> productDetailslist = new ArrayList<>();
		final ProductDetailsData productData = new ProductDetailsData();
		productData.setStatus("Cancelled");
		targetRetailerData.setProductsDetail(productDetailslist);
		final EmailOrderDetailsData orderDetailsData = new EmailOrderDetailsData();
		target.setOrderDetail(orderDetailsData);
		emailOrderStatusDataPopulator.populate(source, target);
		assertEquals("Store Rejected", target.getOrderDetail().getOrderStatus());
	}

	/**
	 * Test Populate Method for Item Rejected
	 */
	@Test
	public void testpopulateForItemRejected()
	{

		final OrderData source = mock(OrderData.class);
		final EmailOrderData target = new EmailOrderData();
		final List<RetailerDetailsData> retailerDetailList = new ArrayList<>();
		final RetailerDetailsData targetRetailerData = new RetailerDetailsData();
		targetRetailerData.setStatus("Success");
		retailerDetailList.add(targetRetailerData);
		target.setRetailersDetail(retailerDetailList);
		final List<ProductDetailsData> productDetailslist = new ArrayList<>();
		final ProductDetailsData productData = new ProductDetailsData();
		productData.setStatus("Cancelled");
		productDetailslist.add(productData);
		targetRetailerData.setProductsDetail(productDetailslist);
		final EmailOrderDetailsData orderDetailsData = new EmailOrderDetailsData();
		target.setOrderDetail(orderDetailsData);
		emailOrderStatusDataPopulator.populate(source, target);
		assertEquals("Item Rejected", target.getOrderDetail().getOrderStatus());
	}
}
