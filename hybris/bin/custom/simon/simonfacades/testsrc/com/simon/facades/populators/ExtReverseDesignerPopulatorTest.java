package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DesignerData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtReverseDesignerPopulatorTest
{
	@InjectMocks
	private ExtReverseDesignerPopulator extREverseDesignerPopulator;

	@Test
	public void testConvert()
	{
		final DesignerData designerData = new DesignerData();
		designerData.setActive(false);
		designerData.setBannerUrl("/test");
		designerData.setName("TestName");
		designerData.setCode("/test");
		designerData.setDescription("/test");

		final DesignerModel designerModel = new DesignerModel();

		extREverseDesignerPopulator.populate(designerData, designerModel);

		Assert.assertEquals(designerData.getCode(), designerModel.getCode());
		Assert.assertEquals(designerData.getBannerUrl(), designerModel.getBannerUrl());
		Assert.assertEquals(designerData.getName(), designerModel.getName());
		Assert.assertEquals(designerData.getDescription(), designerModel.getDescription());
	}

}
