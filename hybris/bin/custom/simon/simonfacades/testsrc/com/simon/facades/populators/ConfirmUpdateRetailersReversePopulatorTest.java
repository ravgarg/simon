package com.simon.facades.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.integration.dto.OrderConfirmCartProductsCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailerInfoCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConfirmUpdateRetailersReversePopulatorTest
{

	@InjectMocks
	private ConfirmUpdateRetailersReversePopulator confirmUpdateRetailersReversePopulator;
	@Mock
	private List<OrderConfirmCartProductsCallBackDTO> confirmCartProductsCallBackDTOs;
	@Mock
	private List<AdditionalProductDetailsModel> additionalProductDetailsModels;
	@Mock
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;
	@Mock
	private Converter<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel> confirmUpdateProductReverseConverter;
	@Mock
	private Converter<OrderConfirmCartProductsCallBackDTO, AdditionalProductDetailsModel> confirmUpdateFailedProductReverseConverter;

	@Test
	public void confirmUpdateRetailersReversePopulatorTest()
	{
		final RetailersInfoModel target = mock(RetailersInfoModel.class);
		final OrderConfirmRetailersInfoCallBackDTO source = mock(OrderConfirmRetailersInfoCallBackDTO.class);
		final OrderConfirmRetailerInfoCallBackDTO orderConfirmRetailerInfoCallBackDTO = mock(
				OrderConfirmRetailerInfoCallBackDTO.class);
		orderConfirmRetailerInfoCallBackDTO.setShippingEstimate("3-5 working days");

		source.setFinalPrice("$12.00");
		source.setShippingPrice("$11.00");
		source.setTax("$10.00");
		source.setOrderId("11111111111");
		source.setRetailerId("10101010");
		source.setCouponValue("$5.00");

		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$10.00")).thenReturn(10.00);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$11.00")).thenReturn(11.00);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$12.00")).thenReturn(12.00);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign("$5.00")).thenReturn(5.00);


		final LinkedList<OrderConfirmCartProductsCallBackDTO> list = new LinkedList<>();
		when(source.getCartFailedProducts()).thenReturn(list);
		when(confirmUpdateFailedProductReverseConverter.convertAll(list)).thenReturn(new LinkedList<>());
		source.setRetailerInfo(orderConfirmRetailerInfoCallBackDTO);

		final OrderConfirmCartProductsCallBackDTO confirmCartProductsCallBackDTO = mock(OrderConfirmCartProductsCallBackDTO.class);
		confirmCartProductsCallBackDTOs.add(confirmCartProductsCallBackDTO);
		source.setCartProducts(confirmCartProductsCallBackDTOs);

		when(source.getRetailerInfo()).thenReturn(orderConfirmRetailerInfoCallBackDTO);
		when(source.getCartProducts()).thenReturn(confirmCartProductsCallBackDTOs);
		when(confirmUpdateProductReverseConverter.convertAll(confirmCartProductsCallBackDTOs))
				.thenReturn(additionalProductDetailsModels);
		when(target.getRetailerId()).thenReturn("10101010");
		when(target.getOrderId()).thenReturn("11111111111");

		confirmUpdateRetailersReversePopulator.populate(source, target);

		Assert.assertEquals("10101010", target.getRetailerId());
		Assert.assertEquals("11111111111", target.getOrderId());
	}

}
