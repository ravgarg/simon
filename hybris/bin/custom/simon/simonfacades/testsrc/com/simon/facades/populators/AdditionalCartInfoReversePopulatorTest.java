package com.simon.facades.populators;

import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.CreateCartConfirmationRetailerDTO;



/**
 * The Class AdditionalCartInfoReversePopulatorTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AdditionalCartInfoReversePopulatorTest
{

	@InjectMocks
	private final AdditionalCartInfoReversePopulator additionalCartInfoReversePopulator = new AdditionalCartInfoReversePopulator();

	@Mock
	private Converter<CreateCartConfirmationRetailerDTO, RetailersInfoModel> retailersInfoReverseConverter;

	@Mock
	private AdditionalCartInfoModel target;

	@Mock
	private ModelService modelService;


	/**
	 * test the populate additional CartInfo Model from Create Cart ConfirmationDTO which have information of retailers,
	 * added products and failed products.
	 */
	@Test
	public void testPopulate()
	{
		final CreateCartConfirmationDTO source = new CreateCartConfirmationDTO();
		source.setCartId("123");
		final CreateCartConfirmationRetailerDTO createCartConfirmationRetailerDTO = Mockito
				.mock(CreateCartConfirmationRetailerDTO.class);
		final List<CreateCartConfirmationRetailerDTO> retailersInfoModelList = new ArrayList<>();
		retailersInfoModelList.add(createCartConfirmationRetailerDTO);
		source.setRetailers(retailersInfoModelList);
		final List<RetailersInfoModel> retailerList = new ArrayList<>();
		Mockito.when(target.getRetailersInfo()).thenReturn(retailerList);
		Mockito.doNothing().when(modelService).remove(retailerList);
		Mockito.doReturn(retailersInfoModelList).when(retailersInfoReverseConverter).convertAll(source.getRetailers());
		additionalCartInfoReversePopulator.populate(source, target);
		assertNotNull(retailersInfoModelList.get(0));
	}

}
