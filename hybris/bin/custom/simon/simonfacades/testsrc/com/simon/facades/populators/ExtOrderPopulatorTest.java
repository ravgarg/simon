package com.simon.facades.populators;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.order.impl.ExtCheckoutFacadeImplTest;


/**
 * JUnit test suite for {@link ExtCheckoutFacadeImplTest}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtOrderPopulatorTest
{

	@InjectMocks
	@Spy
	private ExtOrderPopulator orderPopulator;

	@Mock
	private OrderModel orderModel;
	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModel;
	@Mock
	private PaymentInfoModel paymentInfoModel;
	@Mock
	private CurrencyModel currencyModel;

	private OrderData orderData;

	private PriceData priceData;

	@Mock
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> extPaymentInfoConverter;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private PriceDataFactory priceDataFactory;

	/**
	 * set up
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		priceData = new PriceData();
		priceData.setValue(BigDecimal.TEN);
		orderData = new OrderData();
	}

	/**
	 *
	 */
	@Test
	public void testPopulate()
	{
		when(orderModel.getPaymentInfo()).thenReturn(creditCardPaymentInfoModel);
		when(orderModel.getDeliveryCost()).thenReturn(10d);
		when(commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10d), "USD")).thenReturn(priceData);

		orderPopulator.populate(orderModel, orderData);

		assertTrue(orderData.getDeliveryCost().getValue().compareTo(BigDecimal.valueOf(orderModel.getDeliveryCost())) == 0);
	}

	/**
	 *
	 */
	@Test
	public void testPopulateWithOtherTypePaymentInfo()
	{
		when(orderModel.getPaymentInfo()).thenReturn(paymentInfoModel);

		orderPopulator.populate(orderModel, orderData);

		assertNotNull(orderData);
	}

	/**
	 *
	 */
	@Test
	public void testPopulateWithOtherDeliveryCost()
	{
		when(orderModel.getPaymentInfo()).thenReturn(creditCardPaymentInfoModel);
		when(orderModel.getDeliveryCost()).thenReturn(11d);
		when(commonI18NService.getCurrentCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(10d), "USD")).thenReturn(priceData);

		orderPopulator.populate(orderModel, orderData);

		assertNotNull(orderData);
	}

	/**
	 * @return the extPaymentInfoConverter
	 */
	public Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> getExtPaymentInfoConverter()
	{
		return extPaymentInfoConverter;
	}

}
