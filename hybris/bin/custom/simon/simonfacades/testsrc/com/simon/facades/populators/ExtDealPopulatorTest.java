package com.simon.facades.populators;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;
import com.simon.core.enums.DealStoreType;
import com.simon.core.model.DealModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.customer.ExtCustomerFacade;


/**
 * Test class to test Deal Populator
 */
@UnitTest
public class ExtDealPopulatorTest
{
	@InjectMocks
	private ExtDealPopulator extDealPopulator;
	private final DealModel source = new DealModel();
	private final DealData target = new DealData();
	private final DealStoreType storeType = DealStoreType.BOTH;
	private final Calendar cal = Calendar.getInstance();
	private Date startDate;
	private Date endDate;
	@Mock
	private ShopModel shop;
	@Mock
	private PromotionSourceRuleModel promotionSourceRule;
	@Mock
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		cal.set(2018, 2, 21);
		startDate = cal.getTime();
		cal.set(2018, 2, 24);
		endDate = cal.getTime();
		source.setStoreType(storeType);
		shop.setName("Cole Haan");
	}


	/**
	 * Test to check for empty shop and date
	 */
	@Test
	public void testPopulateWithEmptyDateAndShop()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
	}

	/**
	 * Test to check for start date
	 */
	@Test
	public void testPopulateWithOnlyStartDate()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setStartDate(startDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());

	}

	/**
	 * Test to check for end date
	 */
	@Test
	public void testPopulateWithOnlyEndDate()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());

	}

	/**
	 * Test to check for same months for date
	 */
	@Test
	public void testPopulateWithSameMonthsOfDates()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setStartDate(startDate);
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
		Assert.assertEquals("Mar. 21-24", target.getValidity());
	}

	/**
	 * Test to check for different month in start and end date
	 */
	@Test
	public void testPopulateWithDifferentMonthsInDates()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
		Assert.assertEquals("Mar. 21 - Aug. 2", target.getValidity());
	}

	/**
	 * Test to check for empty shops logo
	 */
	@Test
	public void testPopulateWithEmptyShopLogo()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setShop(shop);
		when(shop.getLogo()).thenReturn(null);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
		Assert.assertEquals("Mar. 21 - Aug. 2", target.getValidity());
		Assert.assertNull(target.getImageURL());
	}

	/**
	 * Test to check for not empty shops logo
	 */
	@Test
	public void testPopulateWithNonEmptyShopLogo()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setShop(shop);
		final MediaModel mediaModel = new MediaModel();
		when(shop.getLogo()).thenReturn(mediaModel);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
		Assert.assertEquals("Mar. 21 - Aug. 2", target.getValidity());
		Assert.assertEquals(mediaModel.getURL(), target.getImageURL());
	}

	/**
	 * Test to check deal with Product level Promotion.
	 */
	@Test
	public void testPopulateWithProductLevelPromotion()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setShop(shop);

		final ContentPageModel contentPageModel = new ContentPageModel();
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		contentPageModel.setLabel("hello");
		final List<ContentPageModel> contentPageList = new ArrayList<>();
		contentPageList.add(contentPageModel);
		source.setContentPage(contentPageList);
		final List<PromotionSourceRuleModel> listOfPromotion = new ArrayList<>();
		final PromotionSourceRuleModel promotionSourceRuleModel = new PromotionSourceRuleModel();
		listOfPromotion.add(promotionSourceRuleModel);
		source.setPromotionSourceRule(listOfPromotion);
		final MediaModel mediaModel = new MediaModel();
		when(shop.getLogo()).thenReturn(mediaModel);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");

	}

	/**
	 * Test to check deal with Retailer specific Promotion.
	 */
	@Test
	public void testPopulateWithRetailerSpecificPromotion()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setShop(shop);
		final ContentPageModel contentPageModel = new ContentPageModel();
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		contentPageModel.setLabel("hello");
		final List<ContentPageModel> contentPageList = new ArrayList<>();
		contentPageList.add(contentPageModel);
		source.setContentPage(contentPageList);
		final List<PromotionSourceRuleModel> listOfPromotion = new ArrayList<>();
		final PromotionSourceRuleModel promotionSourceRuleModel = new PromotionSourceRuleModel();
		final ShopModel shopModel = new ShopModel();
		final List<ContentPageModel> listContentPageModel = new ArrayList<>();
		listContentPageModel.add(contentPageModel);
		shopModel.setContentPage(listContentPageModel);
		promotionSourceRuleModel.setStore(shopModel);
		listOfPromotion.add(promotionSourceRuleModel);
		source.setPromotionSourceRule(listOfPromotion);
		final MediaModel mediaModel = new MediaModel();
		when(shop.getLogo()).thenReturn(mediaModel);
		when(shop.getName()).thenReturn("Cole Haan");
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
		Assert.assertEquals(source.getShop().getName(), target.getRetailerName());
	}

	/**
	 * Test to check deal with no Promotion and having direct product association with deal.
	 */
	@Test
	public void testPopulateForDealWithProductsAndNoPromotion()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setShop(shop);
		final ContentPageModel contentPageModel = new ContentPageModel();
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		contentPageModel.setLabel("hello");
		final List<ContentPageModel> contentPageList = new ArrayList<>();
		contentPageList.add(contentPageModel);
		source.setContentPage(contentPageList);
		final GenericVariantProductModel product = new GenericVariantProductModel();
		final Set<GenericVariantProductModel> setOfProduct = new HashSet<>();
		setOfProduct.add(product);
		source.setProducts(setOfProduct);
		final MediaModel mediaModel = new MediaModel();
		when(shop.getLogo()).thenReturn(mediaModel);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");
	}

	/**
	 * Test to check deal with no Promotion and having direct no product association with deal.
	 */

	@Test
	public void testPopulateForDealWithOutProductsAndNoPromotion()
	{
		source.setCode("testDeal1");
		source.setName("test deal");
		source.setStoreType(storeType);
		source.setShop(shop);
		final ContentPageModel contentPageModel = new ContentPageModel();
		contentPageModel.setPageType(ContentPageType.LISTINGPAGE);
		contentPageModel.setLabel("hello");
		final List<ContentPageModel> contentPageList = new ArrayList<>();
		contentPageList.add(contentPageModel);
		source.setContentPage(contentPageList);
		final MediaModel mediaModel = new MediaModel();
		when(shop.getLogo()).thenReturn(mediaModel);
		source.setStartDate(startDate);
		cal.set(2018, 7, 2);
		endDate = cal.getTime();
		source.setEndDate(endDate);
		extDealPopulator.populate(source, target);
		Assert.assertEquals(source.getCode(), target.getCode());
		Assert.assertNotNull(target.getValidity(), "Validity is non empty");

	}
}
