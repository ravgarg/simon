
package com.simon.facades.populators;

import de.hybris.platform.cms2.enums.LinkTargets;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import com.simon.core.enums.Shape;
import com.simon.core.model.BackgroundImageItemModel;
import com.simon.core.model.ContentSlotClickableImageWithSingleCTAComponentModel;
import com.simon.facades.cms.BackgroundImageData;
import com.simon.facades.cms.ContentSlotClickableImageWithSingleCTAComponentData;




/**
 * The Class ContentSlotClickableImageComponentPopulatorTest.
 */
public class ContentSlotClickableImageComponentPopulatorTest
{

	@InjectMocks
	private final ContentSlotClickableImageComponentPopulator contentSlotClickableImageComponentPopulator = new ContentSlotClickableImageComponentPopulator();



	/**
	 * test the population of data from ContentSlotClickableImageWithSingleCTAComponentModel to
	 * ContentSlotClickableImageWithSingleCTAComponentData
	 */
	@Test
	public void testPopulate()
	{
		final ContentSlotClickableImageWithSingleCTAComponentData target = new ContentSlotClickableImageWithSingleCTAComponentData();
		final ContentSlotClickableImageWithSingleCTAComponentModel source = Mockito
				.mock(ContentSlotClickableImageWithSingleCTAComponentModel.class);
		final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter = Mockito
				.mock(Converter.class);
		contentSlotClickableImageComponentPopulator.setSimonBackgroundImageDataConverter(simonBackgroundImageDataConverter);
		Mockito.when(source.getUid()).thenReturn("Component");
		final CMSLinkComponentModel cmsLink = Mockito.mock(CMSLinkComponentModel.class);

		Mockito.when(cmsLink.getTarget()).thenReturn(LinkTargets.NEWWINDOW);

		Mockito.when(source.getLink()).thenReturn(cmsLink);
		Mockito.when(cmsLink.getLinkName()).thenReturn("TestUrl");
		final BackgroundImageData image = Mockito.mock(BackgroundImageData.class);
		final BackgroundImageItemModel imageModel = Mockito.mock(BackgroundImageItemModel.class);
		Mockito.when(source.getBackgroundImage()).thenReturn(imageModel);
		Mockito.when(simonBackgroundImageDataConverter.convert(imageModel)).thenReturn(image);
		Mockito.when(source.getShape()).thenReturn(Shape.SQUARE);
		contentSlotClickableImageComponentPopulator.populate(source, target);
		Assert.assertEquals(target.getId(), "Component");
	}

	/**
	 * test the population of data from ContentSlotClickableImageWithSingleCTAComponentModel to
	 * ContentSlotClickableImageWithSingleCTAComponentData when link is null
	 */
	@Test
	public void testPopulateNullLink()
	{
		final ContentSlotClickableImageWithSingleCTAComponentData target = new ContentSlotClickableImageWithSingleCTAComponentData();
		final ContentSlotClickableImageWithSingleCTAComponentModel source = Mockito
				.mock(ContentSlotClickableImageWithSingleCTAComponentModel.class);
		final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter = Mockito
				.mock(Converter.class);
		contentSlotClickableImageComponentPopulator.setSimonBackgroundImageDataConverter(simonBackgroundImageDataConverter);
		Mockito.when(source.getUid()).thenReturn("Component");
		Mockito.when(source.getLink()).thenReturn(null);
		final BackgroundImageData image = Mockito.mock(BackgroundImageData.class);
		final BackgroundImageItemModel imageModel = Mockito.mock(BackgroundImageItemModel.class);
		Mockito.when(source.getBackgroundImage()).thenReturn(imageModel);
		Mockito.when(simonBackgroundImageDataConverter.convert(imageModel)).thenReturn(image);
		Mockito.when(source.getShape()).thenReturn(Shape.SQUARE);
		contentSlotClickableImageComponentPopulator.populate(source, target);
		Assert.assertEquals(target.getId(), "Component");
	}

	/**
	 * test the population of data from ContentSlotClickableImageWithSingleCTAComponentModel to
	 * ContentSlotClickableImageWithSingleCTAComponentData when BackgroundImage is Null
	 */
	@Test
	public void testPopulateNullBackgroundImage()
	{
		final ContentSlotClickableImageWithSingleCTAComponentData target = new ContentSlotClickableImageWithSingleCTAComponentData();
		final ContentSlotClickableImageWithSingleCTAComponentModel source = Mockito
				.mock(ContentSlotClickableImageWithSingleCTAComponentModel.class);
		final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter = Mockito
				.mock(Converter.class);
		contentSlotClickableImageComponentPopulator.setSimonBackgroundImageDataConverter(simonBackgroundImageDataConverter);
		Mockito.when(source.getUid()).thenReturn("Component");
		Mockito.when(source.getLink()).thenReturn(null);
		Mockito.when(source.getBackgroundImage()).thenReturn(null);
		Mockito.when(source.getShape()).thenReturn(Shape.SQUARE);
		contentSlotClickableImageComponentPopulator.populate(source, target);
		Assert.assertEquals(target.getId(), "Component");
		Assert.assertEquals(target.getBackgroundImage(), null);
	}

	/**
	 * test the population of data from ContentSlotClickableImageWithSingleCTAComponentModel to
	 * ContentSlotClickableImageWithSingleCTAComponentData when BackgroundImage is Null
	 */
	@Test
	public void testPopulateNullShape()
	{
		final ContentSlotClickableImageWithSingleCTAComponentData target = new ContentSlotClickableImageWithSingleCTAComponentData();
		final ContentSlotClickableImageWithSingleCTAComponentModel source = Mockito
				.mock(ContentSlotClickableImageWithSingleCTAComponentModel.class);
		final Converter<BackgroundImageItemModel, BackgroundImageData> simonBackgroundImageDataConverter = Mockito
				.mock(Converter.class);
		contentSlotClickableImageComponentPopulator.setSimonBackgroundImageDataConverter(simonBackgroundImageDataConverter);
		Mockito.when(source.getUid()).thenReturn("Component");
		Mockito.when(source.getLink()).thenReturn(null);
		Mockito.when(source.getBackgroundImage()).thenReturn(null);
		Mockito.when(source.getShape()).thenReturn(null);
		contentSlotClickableImageComponentPopulator.populate(source, target);
		Assert.assertEquals(target.getShape(), null);
	}

}
