package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.ProductDetailsData;


/**
 * Test method for EmailProductDetailsDataPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailProductDetailsDataPopulatorTest
{
	@InjectMocks
	private EmailProductDetailsDataPopulator emailProductDetailsDataPopulator;

	/**
	 * Initialize mocks
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}

	/**
	 * Test Populate Method
	 */
	@Test
	public void testPopulate()
	{
		final OrderEntryData source = mock(OrderEntryData.class);
		final ProductDetailsData target = new ProductDetailsData();
		final ProductData productData = mock(ProductData.class);
		when(source.getProduct()).thenReturn(productData);
		final Map<String, String> variantValueMap = new HashMap<>();
		variantValueMap.put("COLOR", "Red");
		variantValueMap.put("SIZE", "M");
		when(productData.getVariantValues()).thenReturn(variantValueMap);
		final PriceData priceData = mock(PriceData.class);
		when(source.getBasePrice()).thenReturn(priceData);
		when(priceData.getFormattedValue()).thenReturn("32.00");
		final List<ImageData> iamgeList = new ArrayList<>();
		final ImageData imageData = mock(ImageData.class);
		when(imageData.getFormat()).thenReturn("Desktop");
		iamgeList.add(imageData);
		when(productData.getImages()).thenReturn(iamgeList);
		final PriceData salePrice = mock(PriceData.class);
		when(source.getSaleValue()).thenReturn(salePrice);
		when(salePrice.getFormattedValue()).thenReturn("10.00");
		emailProductDetailsDataPopulator.populate(source, target);
		assertEquals("32.00", target.getPrice());
		assertEquals("Red", target.getColor());
	}

}
