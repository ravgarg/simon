package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


@UnitTest
public class SearchCategoryFacetValuesHierarchyPopulatorTest<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY>

{
	@InjectMocks
	SearchCategoryFacetValuesHierarchyPopulator searchCategoryFacetValuesHierarchyPopulator;

	ProductCategorySearchPageData<QUERY, RESULT, SCAT> source;

	ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		source = new ProductCategorySearchPageData<>();
		target = new ProductCategorySearchPageData<>();

		final FacetValueData<STATE> facetValue1 = new FacetValueData<>();
		final FacetValueData<STATE> facetValue2 = new FacetValueData<>();
		final FacetValueData<STATE> facetValue3 = new FacetValueData<>();
		final FacetValueData<STATE> facetValue4 = new FacetValueData<>();
		final FacetValueData<STATE> facetValue5 = new FacetValueData<>();

		facetValue1.setCode("/m0003334|SPO");
		facetValue2.setCode("/m0003334|SPO/m100000|Home");
		facetValue3.setCode("/m0003334|SPO/m100000|Home/m200000|Women");
		facetValue4.setCode("/m0003334|SPO/m100000|Home/m300000|Men/m300500|Clothing");
		facetValue5.setCode("/m0003334|SPO/m100000|Home/m300000|Men");


		final List<FacetData<STATE>> facets = new ArrayList<>();
		final FacetData<STATE> categoryPathFacet = new FacetData<>();
		categoryPathFacet.setCode(IndexedKeys.CATEGORY_PATH);
		categoryPathFacet.setValues(Arrays.asList(facetValue1, facetValue2, facetValue3, facetValue4, facetValue5));
		final FacetData<STATE> colorFacet = new FacetData<>();
		colorFacet.setCode("normalizedColorCode");
		facets.add(categoryPathFacet);
		//facets.add(colorFacet);
		target.setFacets(facets);
	}

	@Test
	public void testPopulateMethodIntoHierarchialDTO()
	{
		searchCategoryFacetValuesHierarchyPopulator.rootCategoryCode = "m0003334";
		searchCategoryFacetValuesHierarchyPopulator.rootCategoryName = "SPO";
		searchCategoryFacetValuesHierarchyPopulator.populate(source, target);
		assertEquals(target.getFacets().get(0).getValues().get(0).getCode(), "m200000");
	}
}
