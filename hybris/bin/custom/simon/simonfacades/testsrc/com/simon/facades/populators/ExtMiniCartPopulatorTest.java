package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotions.PromotionsService;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * ExtMiniCartPopulatorTest tested ExtMiniCartPopulator class's populate method which populate common, cart total,
 * Promotions and guid from cartModel to cart data.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMiniCartPopulatorTest
{

	@InjectMocks
	private final CartPopulator<CartData> miniCartPopulator = new ExtMiniCartPopulator();
	@Mock
	private CartModel cartModel;
	@Mock
	private CurrencyModel currency;
	@Mock
	private PriceDataFactory priceDataFactory;
	@Mock
	private PriceData priceData;
	@Mock
	PromotionsService promotionsService;

	private static String CART_GUID = "testid";

	/**
	 * populate test ExtMiniCartPopulator class's method which populate common, cart total, Promotions and guid from
	 * cartModel to cart data.
	 */
	@Test
	public void populate()
	{
		final CartData cartData = new CartData();
		final CurrencyModel currency = Mockito.mock(CurrencyModel.class);
		miniCartPopulator.setPriceDataFactory(priceDataFactory);
		Mockito.doReturn(priceData).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.ZERO, currency);
		Mockito.doReturn(currency).when(cartModel).getCurrency();
		Mockito.doReturn(CART_GUID).when(cartModel).getGuid();
		cartModel.setGuid("minicart");
		miniCartPopulator.populate(cartModel, cartData);
		Assert.assertEquals(CART_GUID, cartData.getGuid());

	}
}
