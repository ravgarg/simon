package com.simon.facades.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;

import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * The Class ExtFacetSearchQueryFacetsPopulatorTest.
 */
@UnitTest
public class ExtFacetSearchQueryFacetsPopulatorTest
{

	@Spy
	@InjectMocks
	private ExtFacetSearchQueryFacetsPopulator extFacetSearchQueryFacetsPopulator;

	/**
	 * Test facet limit.
	 */
	@Test
	public void testFacetLimit()
	{
		MockitoAnnotations.initMocks(this);
		final SearchQueryConverterData source = Mockito.mock(SearchQueryConverterData.class);
		final SolrQuery target = new SolrQuery();
		target.setFacetLimit(-1);
		Mockito.doNothing().when(extFacetSearchQueryFacetsPopulator).callSuperMethod(source, target);
		extFacetSearchQueryFacetsPopulator.populate(source, target);
		Assert.assertEquals(-1, target.getFacetLimit());
	}

}
