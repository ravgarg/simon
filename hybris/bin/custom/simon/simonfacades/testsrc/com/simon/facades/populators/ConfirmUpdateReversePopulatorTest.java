package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;
import com.simon.integration.utils.OrderConfirmationIntegrationUtils;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ConfirmUpdateReversePopulatorTest
{
	@InjectMocks
	private ConfirmUpdateReversePopulator confirmUpdateReversePopulator;
	@Mock
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;
	@Mock
	private List<OrderConfirmRetailersInfoCallBackDTO> retailersInfoDTOs;
	@Mock
	private Converter<OrderConfirmRetailersInfoCallBackDTO, RetailersInfoModel> confirmUpdateRetailersReverseConverter;
	@Mock
	private List<RetailersInfoModel> retailersInfoModels;
	AdditionalCartInfoModel target = new AdditionalCartInfoModel();
	OrderConfirmCallBackDTO source = new OrderConfirmCallBackDTO();

	@Before
	public void setUp()
	{
		source.setFinalPrice("$10.00");
		source.setShippingPrice("$11.00");
		source.setOrderId("11111111");
		source.setPendingConfirm(true);

		source.setTax("$12.00");
		source.setCouponValue("$5.00");
		source.setMessage("Test");

		source.setRetailers(retailersInfoDTOs);



	}

	@Test
	public void testPopulate_priceValues_cart_pendingconfirm()
	{
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getShippingPrice())).thenReturn(11.00d);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getFinalPrice())).thenReturn(10.00d);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getTax())).thenReturn(12.00d);
		when(orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(source.getCouponValue())).thenReturn(5.00d);
		when(CollectionUtils.isNotEmpty(retailersInfoDTOs)).thenReturn(true);
		when(confirmUpdateRetailersReverseConverter.convertAll(retailersInfoDTOs)).thenReturn(retailersInfoModels);



		confirmUpdateReversePopulator.populate(source, target);
		assertEquals(11.00d, target.getTotalShippingPrice().doubleValue(), 0d);
		assertEquals(10.00, target.getFinalPrice().doubleValue(), 0d);
		assertEquals(12.00, target.getTotalSalesTax().doubleValue(), 0d);
		assertEquals(5.00, target.getCouponValue().doubleValue(), 0d);
		assertEquals(source.getOrderId(), target.getCartId());
		assertEquals(source.getMessage(), target.getMessage());

		assertEquals(source.isPendingConfirm(), target.getPendingConfirm());

	}

	@Test
	public void testPopulate_cart_message_nullvalue()
	{

		source.setOrderId(null);
		source.setMessage(null);

		confirmUpdateReversePopulator.populate(source, target);

		assertEquals(StringUtils.EMPTY, target.getCartId());
		assertEquals(StringUtils.EMPTY, target.getMessage());

	}



}
