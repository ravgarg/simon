package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.DealModel;
import com.simon.facades.email.ShareEmailData;



/**
 * Test class of ShareEmailDealDataPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShareEmailDealDataPopulatorTest
{
	@InjectMocks
	@Spy
	private ShareEmailDealDataPopulator shareEmailDealDataPopulator;


	/**
	 * Initialize mocks
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}

	/**
	 * This is the method to verify Valid when start and end date are from same month and Description of Offer.
	 */
	@Test
	public void testpopulateVerifyValidForSameMOnth()
	{

		final DealModel source = mock(DealModel.class);
		final ShareEmailData target = new ShareEmailData();
		final ShopModel shop = mock(ShopModel.class);
		when(source.getShop()).thenReturn(shop);
		when(shop.getName()).thenReturn("TestShop1");
		final MediaModel logo = mock(MediaModel.class);
		when(shop.getLogo()).thenReturn(logo);
		when(logo.getURL()).thenReturn("TestURL");
		final List<PromotionSourceRuleModel> promotioncollection = new ArrayList<>();
		final PromotionSourceRuleModel promotion = mock(PromotionSourceRuleModel.class);
		when(promotion.getDescription()).thenReturn("Promotion Desc");
		promotioncollection.add(promotion);
		when(source.getPromotionSourceRule()).thenReturn(promotioncollection);
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		when(source.getStartDate()).thenReturn(startdate);
		final Date enddate = new GregorianCalendar(2018, Calendar.JANUARY, 25).getTime();
		when(source.getEndDate()).thenReturn(enddate);
		doReturn("test").when(shareEmailDealDataPopulator).getLocaleValue(any());
		shareEmailDealDataPopulator.populate(source, target);
		assertEquals("Valid Jan. 11-25", target.getRetailer().getOffer().getValid());
		assertEquals("Promotion Desc", target.getRetailer().getOffer().getDescription());

	}

	/**
	 * This is the method to verify Valid when start and end date are from same different Month.
	 */
	@Test
	public void testpopulateVerifyValidFordifferentMOnth()
	{

		final DealModel source = mock(DealModel.class);
		final ShareEmailData target = new ShareEmailData();
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		when(source.getStartDate()).thenReturn(startdate);
		final Date enddate = new GregorianCalendar(2018, Calendar.FEBRUARY, 25).getTime();
		when(source.getEndDate()).thenReturn(enddate);
		doReturn("test").when(shareEmailDealDataPopulator).getLocaleValue(any());
		shareEmailDealDataPopulator.populate(source, target);
		assertEquals("Valid Jan. 11 - Feb. 25", target.getRetailer().getOffer().getValid());
	}

	/**
	 * This is the method to verify Valid when start date is null.
	 */
	@Test
	public void testpopulateVerifyValidForNoStartDate()
	{

		final DealModel source = mock(DealModel.class);
		final ShareEmailData target = new ShareEmailData();
		final ShopModel shop = mock(ShopModel.class);
		when(source.getShop()).thenReturn(shop);
		when(shop.getName()).thenReturn("TestShop1");
		final Date enddate = new GregorianCalendar(2018, Calendar.FEBRUARY, 25).getTime();
		when(source.getEndDate()).thenReturn(enddate);
		doReturn("test").when(shareEmailDealDataPopulator).getLocaleValue(any());
		shareEmailDealDataPopulator.populate(source, target);
		Assert.assertNotEquals(null, target.getRetailer().getOffer().getValid());
	}

	/**
	 * This is the method to verify Valid when start date is null.
	 */
	@Test
	public void testpopulateVerifyValidForNoEndDate()
	{

		final DealModel source = mock(DealModel.class);
		final ShareEmailData target = new ShareEmailData();
		final ShopModel shop = mock(ShopModel.class);
		when(source.getShop()).thenReturn(shop);
		when(shop.getName()).thenReturn("TestShop1");
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		when(source.getStartDate()).thenReturn(startdate);
		doReturn("test").when(shareEmailDealDataPopulator).getLocaleValue(any());
		shareEmailDealDataPopulator.populate(source, target);
		Assert.assertNotEquals(null, target.getRetailer().getOffer().getValid());
	}

}
