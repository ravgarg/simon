package com.simon.facades.user.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationService;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationService;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.services.UserAddressIntegrationService;


/**
 * The Class ExtUserFacadeImplTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtUserFacadeImplTest
{
	@InjectMocks
	@Spy
	ExtUserFacadeImpl extUserFacadeImpl;

	@Mock
	private ExtCustomerAccountService customerAccountService;
	@Mock
	private UserFacade userFacade;
	@Mock
	private UserService userService;
	@Mock
	private CustomerModel custModel;
	@Mock
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;
	@Mock
	private UserAddressIntegrationService userAddressIntegrationService;
	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	@Mock
	private Converter<AddressModel, AddressData> addressConverter;
	@Mock
	private AddressModel addressModel;
	@Mock
	private ModelService modelService;
	@Mock
	private Populator<AddressData, AddressModel> addressReversePopulator;
	@Mock
	private UserAddressVerifyIntegrationService userAddressVerifyIntegrationService;
	@Mock
	private UserAddressVerifyResponseDTO userAddressVerifyResponseDTO;
	@Mock
	private CardPaymentIntegrationService cardPaymentIntegrationService;

	/**
	 * Test getbilling Address
	 */
	@Test
	public void testGetBillingAddressList()
	{
		final CustomerModel currentCustomer = Mockito.mock(CustomerModel.class);
		extUserFacadeImpl.setCustomerAccountService(customerAccountService);
		final CheckoutCustomerStrategy checkoutCustomerStrategy = Mockito.mock(CheckoutCustomerStrategy.class);
		extUserFacadeImpl.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(currentCustomer);
		final CreditCardPaymentInfoModel creditcard = new CreditCardPaymentInfoModel();
		final AddressModel address = Mockito.mock(AddressModel.class);
		creditcard.setBillingAddress(address);
		final List<CreditCardPaymentInfoModel> creditCards = new ArrayList<CreditCardPaymentInfoModel>();
		creditCards.add(creditcard);
		when(customerAccountService.getCreditCardPaymentInfos(currentCustomer, true)).thenReturn(creditCards);
		final Converter<AddressModel, AddressData> addressConverter = Mockito.mock(Converter.class);
		extUserFacadeImpl.setAddressConverter(addressConverter);
		final AddressData addressData = new AddressData();
		addressData.setFirstName("Test");
		when(addressConverter.convert(address)).thenReturn(addressData);
		assertNotNull(extUserFacadeImpl.getBillingAddressList(true));
	}


	@Test
	public void testSetNewPaymentInfoTrue()
			throws ModelSavingException, UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		final String success = "true";
		final String message = "Test Message";
		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage(message);
		userAddressResponseDTO.setSuccess(success);

		extUserFacadeImpl.setCreditCardPaymentInfoConverter(creditCardPaymentInfoConverter);
		final CCPaymentInfoData extPaymentInfoData = Mockito.mock(CCPaymentInfoData.class);
		final AddressData billingAddress = Mockito.mock(AddressData.class);
		billingAddress.setId("123");
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		when(customerAccountService.createPaymentInfoSubscription(extPaymentInfoData)).thenReturn(ccPaymentInfoModel);
		final CCPaymentInfoData paymentInfo = new CCPaymentInfoData();
		when(creditCardPaymentInfoConverter.convert(ccPaymentInfoModel)).thenReturn(paymentInfo);
		when(userAddressIntegrationService.addUserPaymentAddress(extPaymentInfoData)).thenReturn(userAddressResponseDTO);
		Mockito.doNothing().when(userFacade).setDefaultPaymentInfo(Mockito.any(CCPaymentInfoData.class));
		assertEquals(true, extUserFacadeImpl.setNewPaymentInfo(extPaymentInfoData));

	}

	@Test
	public void testSetNewPaymentInfoFalse()
			throws ModelSavingException, UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		final String success = "false";
		final String message = "Test Message";
		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage(message);
		userAddressResponseDTO.setSuccess(success);
		userAddressResponseDTO.setErrorCode("141");
		final String ccPaymentInfoModelPK = "123";
		extUserFacadeImpl.setCreditCardPaymentInfoConverter(creditCardPaymentInfoConverter);
		final CCPaymentInfoData extPaymentInfoData = Mockito.mock(CCPaymentInfoData.class);
		final AddressData billingAddress = Mockito.mock(AddressData.class);
		billingAddress.setId("123");
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		when(customerAccountService.createPaymentInfoSubscription(extPaymentInfoData)).thenReturn(ccPaymentInfoModel);
		final CCPaymentInfoData paymentInfo = new CCPaymentInfoData();
		when(creditCardPaymentInfoConverter.convert(ccPaymentInfoModel)).thenReturn(paymentInfo);
		when(userAddressIntegrationService.addUserPaymentAddress(extPaymentInfoData)).thenReturn(userAddressResponseDTO);
		Mockito.doReturn(ccPaymentInfoModelPK).when(extUserFacadeImpl).getCCPaymentInfoModelPK(ccPaymentInfoModel);
		Mockito.doNothing().when(extUserFacadeImpl).unlinkCCPaymentInfoSuper(ccPaymentInfoModel);
		assertEquals(false, extUserFacadeImpl.setNewPaymentInfo(extPaymentInfoData));

	}

	@Test(expected = UserAddressIntegrationException.class)
	public void testSetNewPaymentInfoException()
			throws UserAddressIntegrationException, ModelSavingException, CartCheckOutIntegrationException
	{
		final String success = "false";
		final String message = "Test Message";
		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage(message);
		userAddressResponseDTO.setSuccess(success);
		final String ccPaymentInfoModelPK = "123";
		extUserFacadeImpl.setCreditCardPaymentInfoConverter(creditCardPaymentInfoConverter);
		final CCPaymentInfoData extPaymentInfoData = Mockito.mock(CCPaymentInfoData.class);
		final AddressData billingAddress = Mockito.mock(AddressData.class);
		billingAddress.setId("123");
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		when(extPaymentInfoData.getPaymentToken()).thenReturn("PAYMENT_TOKEN");
		final RemovePaymentResponseDTO removePaymentResponse = new RemovePaymentResponseDTO();
		removePaymentResponse.setErrorCode(Mockito.anyString());
		when(cardPaymentIntegrationService.invokeRemovePaymentMethodToken("PAYMENT_TOKEN")).thenReturn(removePaymentResponse);
		final CreditCardPaymentInfoModel ccPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(extPaymentInfoData.getBillingAddress()).thenReturn(billingAddress);
		when(customerAccountService.createPaymentInfoSubscription(extPaymentInfoData)).thenReturn(ccPaymentInfoModel);
		final CCPaymentInfoData paymentInfo = new CCPaymentInfoData();
		when(creditCardPaymentInfoConverter.convert(ccPaymentInfoModel)).thenReturn(paymentInfo);
		when(userAddressIntegrationService.addUserPaymentAddress(extPaymentInfoData))
				.thenThrow(new UserAddressIntegrationException("Exception"));
		Mockito.doReturn(ccPaymentInfoModelPK).when(extUserFacadeImpl).getCCPaymentInfoModelPK(ccPaymentInfoModel);
		Mockito.doNothing().when(extUserFacadeImpl).unlinkCCPaymentInfoSuper(ccPaymentInfoModel);
		extUserFacadeImpl.setNewPaymentInfo(extPaymentInfoData);

	}

	@Test
	public void removeCCPaymentInfoUserAddressTrueTest() throws UserAddressIntegrationException
	{
		final String success = "true";
		final UserAddressResponseDTO response = Mockito.mock(UserAddressResponseDTO.class);
		when(response.getSuccess()).thenReturn(success);
		response.setSuccess(success);
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		final ExtAddressData addressData = Mockito.mock(ExtAddressData.class);
		when(addressConverter.convert(addressModel)).thenReturn(addressData);
		when(userAddressIntegrationService.removeUserAddress(Mockito.any(ExtAddressData.class))).thenReturn(response);
		assertEquals(true, extUserFacadeImpl.removeCCPaymentInfoUserAddress(addressModel));
	}

	@Test
	public void removeCCPaymentInfoUserAddressFalseTest() throws UserAddressIntegrationException
	{
		final String success = "false";
		final UserAddressResponseDTO response = Mockito.mock(UserAddressResponseDTO.class);
		when(response.getSuccess()).thenReturn(success);
		when(response.getErrorCode()).thenReturn("123");
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		final ExtAddressData addressData = Mockito.mock(ExtAddressData.class);
		addressData.setId(null);
		when(addressConverter.convert(addressModel)).thenReturn(addressData);
		when(userAddressIntegrationService.removeUserAddress(Mockito.any(ExtAddressData.class))).thenReturn(response);
		assertEquals(false, extUserFacadeImpl.removeCCPaymentInfoUserAddress(addressModel));

	}

	@Test
	public void removeCCPaymentInfoUserAddressTest() throws UserAddressIntegrationException
	{
		final AddressModel addressModel = new AddressModel();
		final ExtAddressData addressData = Mockito.mock(ExtAddressData.class);
		addressData.setId(null);
		when(addressConverter.convert(addressModel)).thenReturn(addressData);
		assertEquals(false, extUserFacadeImpl.removeCCPaymentInfoUserAddress(addressModel));
	}

	@Test
	public void getCCPaymentInfoAddressModelEmptyTest()
	{
		final String ccPaymentInfoModelPK = "123";
		when(userService.getCurrentUser()).thenReturn(custModel);
		final List<CreditCardPaymentInfoModel> creditCardPaymentInfoModelList = Mockito.mock(List.class);
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		final CustomerAccountService customerAccountService = Mockito.mock(CustomerAccountService.class);
		when(customerAccountService.getCreditCardPaymentInfos(custModel, false)).thenReturn(creditCardPaymentInfoModelList);
		Mockito.doReturn(ccPaymentInfoModelPK).when(extUserFacadeImpl).getCCPaymentInfoModelPK(creditCardPaymentInfoModel);
		assertEquals(null, extUserFacadeImpl.getCCPaymentInfoAddressModel(ccPaymentInfoModelPK));
	}


	@Test
	public void getCCPaymentInfoAddressModelTest()
	{
		final String ccPaymentInfoModelPK = "123";
		final AddressModel ccPaymentInfoAddressModel = Mockito.mock(AddressModel.class);
		when(userService.getCurrentUser()).thenReturn(custModel);
		final List<CreditCardPaymentInfoModel> creditCardPaymentInfoModelList = new ArrayList<>();
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		creditCardPaymentInfoModelList.add(creditCardPaymentInfoModel);
		when(customerAccountService.getCreditCardPaymentInfos(custModel, false)).thenReturn(creditCardPaymentInfoModelList);
		Mockito.doReturn(ccPaymentInfoModelPK).when(extUserFacadeImpl).getCCPaymentInfoModelPK(creditCardPaymentInfoModel);
		when(creditCardPaymentInfoModel.getBillingAddress()).thenReturn(ccPaymentInfoAddressModel);
		assertEquals(ccPaymentInfoAddressModel, extUserFacadeImpl.getCCPaymentInfoAddressModel(ccPaymentInfoModelPK));

	}



	@Test
	public void getCCPaymentInfoAddressModeErrorlTest()
	{

		final String ccPaymentInfoModelPK = "123";
		final AddressModel ccPaymentInfoAddressModel = Mockito.mock(AddressModel.class);
		when(userService.getCurrentUser()).thenReturn(custModel);
		final List<CreditCardPaymentInfoModel> creditCardPaymentInfoModelList = new ArrayList<>();
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		creditCardPaymentInfoModelList.add(creditCardPaymentInfoModel);
		when(customerAccountService.getCreditCardPaymentInfos(custModel, false)).thenReturn(creditCardPaymentInfoModelList);
		Mockito.doReturn(ccPaymentInfoModelPK).when(extUserFacadeImpl).getCCPaymentInfoModelPK(creditCardPaymentInfoModel);
		when(creditCardPaymentInfoModel.getBillingAddress()).thenReturn(ccPaymentInfoAddressModel);
		assertEquals(null, extUserFacadeImpl.getCCPaymentInfoAddressModel("987"));

	}


	@Test
	public void testAddUserAddress_True_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("true");

		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.addUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).addUserAddrSuper(extAddressData);
		final boolean result = extUserFacadeImpl.addUserAddress(extAddressData);

		Assert.assertTrue(result);
	}


	@Test
	public void testAddUserAddress_False_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("false");
		userAddressResponseDTO.setErrorCode("121");
		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.addUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).addUserAddrSuper(extAddressData);
		final boolean result = extUserFacadeImpl.addUserAddress(extAddressData);

		Assert.assertFalse(result);
	}

	@Test(expected = UserAddressIntegrationException.class)
	public void testAddUserAddress_Exception_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("false");

		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.addUserAddress(extAddressData))
				.thenThrow(new UserAddressIntegrationException("Exception"));
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).addUserAddrSuper(extAddressData);
		extUserFacadeImpl.addUserAddress(extAddressData);

	}




	@Test
	public void testEditUserAddress_True_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("true");

		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.updateUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).editUserAddrSuper(extAddressData);

		final boolean result = extUserFacadeImpl.editUserAddress(extAddressData);

		Assert.assertTrue(result);
	}

	@Test
	public void testEditUserAddress_False_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("false");
		userAddressResponseDTO.setErrorCode("111");
		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.updateUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).editUserAddrSuper(extAddressData);

		final boolean result = extUserFacadeImpl.editUserAddress(extAddressData);

		Assert.assertFalse(result);
	}


	@Test
	public void testRemoveUserAddress_True_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("true");

		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.removeUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).removeUserAddrSuper(extAddressData);

		final boolean result = extUserFacadeImpl.removeUserAddress(extAddressData);

		Assert.assertTrue(result);
	}

	@Test
	public void testRemoveUserAddress_False_Case() throws UserAddressIntegrationException
	{

		final UserAddressResponseDTO userAddressResponseDTO = new UserAddressResponseDTO();
		userAddressResponseDTO.setMessage("test message");
		userAddressResponseDTO.setSuccess("false");
		userAddressResponseDTO.setErrorCode("112");
		final ExtAddressData extAddressData = Mockito.mock(ExtAddressData.class);

		when(userAddressIntegrationService.removeUserAddress(extAddressData)).thenReturn(userAddressResponseDTO);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(custModel);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.doNothing().when(addressReversePopulator).populate(extAddressData, addressModel);
		Mockito.doNothing().when(customerAccountService).saveAddressEntry(custModel, addressModel);
		Mockito.doNothing().when(extUserFacadeImpl).removeUserAddrSuper(extAddressData);

		final boolean result = extUserFacadeImpl.removeUserAddress(extAddressData);

		Assert.assertFalse(result);
	}

	@Test
	public void testgetUserAddressVerifyResponseDTO() throws UserAddressVerifyIntegrationException
	{
		final ExtAddressData addressData = new ExtAddressData();
		when(userAddressVerifyIntegrationService.validateAddress(addressData)).thenReturn(userAddressVerifyResponseDTO);
		userAddressVerifyIntegrationService.validateAddress(addressData);
	}

}


