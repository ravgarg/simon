package com.simon.facades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.type.TypeService;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.enums.Months;
import com.simon.facades.customer.data.MonthData;


/**
 * Test class form MonthDataPopulator. Having all test method for Class MonthDataPopulator.
 */
public class MonthDataPopulatorTest
{
	@Mock
	private TypeService typeService;

	@InjectMocks
	MonthDataPopulator monthDataDataPopulator = new MonthDataPopulator();

	/**
	 * This is the method to test convert method of MonthDataPopulator.
	 */
	@Test
	public void testConvert()
	{
		final Months months = Months.JAN;
		final TypeService typeService = Mockito.mock(TypeService.class);
		final EnumerationValueModel value = Mockito.mock(EnumerationValueModel.class);
		given(typeService.getEnumerationValue(months)).willReturn(value);
		given(value.getName()).willReturn("Jan");
		given(typeService.getEnumerationValue(months).getName()).willReturn("Jan");
		final MonthData monthData = new MonthData();
		monthDataDataPopulator.setTypeService(typeService);
		monthDataDataPopulator.populate(months, monthData);
		Assert.assertEquals("Jan", monthData.getCode());
	}
}
