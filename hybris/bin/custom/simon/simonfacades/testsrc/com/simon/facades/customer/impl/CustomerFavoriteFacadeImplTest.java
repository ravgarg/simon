package com.simon.facades.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test class of CustomerFavoriteFacadeImpl
 */
@UnitTest
public class CustomerFavoriteFacadeImplTest
{
	@Mock
	private SessionService sessionService;
	@InjectMocks
	private CustomerFavoriteFacadeImpl customerFavoriteFacade;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method to test regular scenario.
	 */
	@Test
	public void setFavoriteDataInSessionTest()
	{
		final String favType = "Products";
		final String favName = "productName";
		final String favId = "productId";
		customerFavoriteFacade.setFavoriteDataInSession(favType, favName, favId);
		Mockito.verify(sessionService).setAttribute(Mockito.any(), Mockito.any());
	}

	/**
	 * Method to test when favorite type is null
	 */
	@Test
	public void setFavoriteDataInSessionFavTypeNullTest()
	{
		final String favType = null;
		final String favName = "productName";
		final String favId = "productId";
		customerFavoriteFacade.setFavoriteDataInSession(favType, favName, favId);
		Mockito.verifyZeroInteractions(sessionService);
	}

	/**
	 * Method to test when favorite name is null
	 */
	@Test
	public void setFavoriteDataInSessionFavNameNUllTest()
	{
		final String favType = "Products";
		final String favName = null;
		final String favId = "productId";
		customerFavoriteFacade.setFavoriteDataInSession(favType, favName, favId);
		Mockito.verify(sessionService).setAttribute(Mockito.any(), Mockito.any());
	}

	/**
	 * Method to test when favorite id is null
	 */
	@Test
	public void setFavoriteDataInSessionFavIdNUllTest()
	{
		final String favType = "Products";
		final String favName = "productName";
		final String favId = null;
		customerFavoriteFacade.setFavoriteDataInSession(favType, favName, favId);
		Mockito.verifyZeroInteractions(sessionService);
	}

	/**
	 * Method to test when favorite type is not form provided enum
	 */
	@Test
	public void setFavoriteDataInSessionFavTypeDiffTes()
	{
		final String favType = "Product";
		final String favName = "productName";
		final String favId = "productId";
		customerFavoriteFacade.setFavoriteDataInSession(favType, favName, favId);

	}
}
