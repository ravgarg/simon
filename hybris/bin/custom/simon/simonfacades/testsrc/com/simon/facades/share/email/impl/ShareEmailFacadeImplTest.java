package com.simon.facades.share.email.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;
import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.product.EmailProductData;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.integration.share.email.services.impl.DefaultShareEmailIntegrationService;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


/**
 * The Class ExtUserFacadeImplTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShareEmailFacadeImplTest
{
	private static final String PRODUCT_CODE = "testProductMVId1";

	@InjectMocks
	@Spy
	ShareEmailFacadeImpl sharedEmailFacade;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Mock
	private Converter<ProductModel, EmailProductData> emailProductDataConverter;
	@Mock
	private DefaultShareEmailIntegrationService shareEmailIntegrationService;
	@Mock
	private SessionService sessionService;
	@Mock
	private ProductService productService;
	@Mock
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Mock
	private ProductModel productModel;
	@Mock
	private ProductSearchPageData<SearchStateData, ProductData> searchPageData;
	@Mock
	private SearchStateData searchState;
	@Mock
	private PageableData pageableData;
	@Mock
	private ExtDealService extDealService;
	@Mock
	private Converter<DealModel, ShareEmailData> shareEmailDealDataConverter;
	@Mock
	private Converter<OrderModel, OrderData> orderConverter;
	@Mock
	private Converter<OrderData, EmailOrderData> emailOrderConfirmationDataConverter;
	@Mock
	private ModelService modelService;

	/**
	 * Init Mocks
	 */
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * @throws SharedEmailIntegrationException
	 *
	 */
	@Test
	public void getProductForCodeTestWithoutNull() throws SharedEmailIntegrationException
	{
		final EmailProductData emailProductData = mock(EmailProductData.class);
		final ShareEmailData shareEmailData = mock(ShareEmailData.class);
		shareEmailData.setProductCode("testProduct");
		final List<String> productCodes = new ArrayList<>();
		productCodes.add("testProduct");
		when(productService.getProductForCode(PRODUCT_CODE)).thenReturn(productModel);
		Mockito.doNothing().when(sessionService).setAttribute(SimonCoreConstants.PRODUCT_CODE_LIST, productCodes);
		doReturn(pageableData).when(sharedEmailFacade).preparePageableData();
		doReturn(searchState).when(sharedEmailFacade).prepareSearchQueryData();
		doReturn(searchPageData).when(sharedEmailFacade).searchProjectData(searchState, pageableData);
		final ProductData productData = mock(ProductData.class);
		final List<ProductData> results = new ArrayList<>();
		results.add(productData);
		searchPageData.setResults(results);
		when(shareEmailData.getProductCode()).thenReturn("testProduct");
		when(searchPageData.getResults()).thenReturn(results);
		when(emailProductDataConverter.convert(productModel)).thenReturn(emailProductData);
		sharedEmailFacade.getProductDetailsToSharedProductEmail(shareEmailData);
		Mockito.doNothing().when(shareEmailIntegrationService).sharedProductEmailData(shareEmailData);
	}

	/**
	 * @throws SharedEmailIntegrationException
	 */
	@Test
	public void getProductForCodeTestWithNull() throws SharedEmailIntegrationException
	{
		final EmailProductData emailProductData = mock(EmailProductData.class);
		final ShareEmailData shareEmailData = mock(ShareEmailData.class);
		shareEmailData.setProductCode("testProduct");
		final List<String> productCodes = new ArrayList<>();
		productCodes.add("testProduct");
		when(productService.getProductForCode(PRODUCT_CODE)).thenReturn(productModel);
		Mockito.doNothing().when(sessionService).setAttribute(SimonCoreConstants.PRODUCT_CODE_LIST, productCodes);
		final ProductData productData = mock(ProductData.class);
		final List<ProductData> results = new ArrayList<>();
		results.add(productData);
		searchPageData.setResults(results);
		when(shareEmailData.getProductCode()).thenReturn("testProduct");
		when(searchPageData.getResults()).thenReturn(results);
		doReturn(searchPageData).when(sharedEmailFacade).searchProjectData(searchState, pageableData);
		when(emailProductDataConverter.convert(productModel)).thenReturn(emailProductData);
		sharedEmailFacade.getProductDetailsToSharedProductEmail(shareEmailData);
		Mockito.doNothing().when(shareEmailIntegrationService).sharedProductEmailData(shareEmailData);
	}

	/**
	 * Test For Share Deal Email
	 *
	 * @throws SharedEmailIntegrationException
	 *
	 */
	@Test
	public void testshareDealEmail() throws SharedEmailIntegrationException
	{
		final String dealId = "TestDeal1";
		final ShareEmailData shareDealEmailData = mock(ShareEmailData.class);
		when(shareDealEmailData.getDealCode()).thenReturn(dealId);
		final DealModel dealModel = mock(DealModel.class);
		when(extDealService.getDealForCode(any())).thenReturn(dealModel);
		when(shareEmailDealDataConverter.convert(dealModel, shareDealEmailData)).thenReturn(shareDealEmailData);
		sharedEmailFacade.shareDealEmail(dealId, shareDealEmailData);
		Mockito.verify(shareEmailIntegrationService).shareDealEmail(shareDealEmailData);
	}

	/**
	 * Test For share Order Email
	 *
	 * @throws SharedEmailIntegrationException
	 */
	@Test
	public void testOrderConfirm() throws SharedEmailIntegrationException
	{
		final OrderModel orderModel = new OrderModel();
		orderModel.setCode("test");
		final OrderData orderData = new OrderData();
		orderData.setCode("test");
		final RetailerInfoData retailerInfoData = new RetailerInfoData();
		retailerInfoData.setRetailerId("123");
		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);
		orderData.setRetailerInfoData(retailerInfoDataList);
		final AbstractOrderEntryModel orderEntryModel = new AbstractOrderEntryModel();
		orderEntryModel.setShopId("123");
		final List<AbstractOrderEntryModel> orderEntrlList = new ArrayList<>();
		orderEntrlList.add(orderEntryModel);
		Mockito.doNothing().when(modelService).save(orderModel);
		final boolean result = sharedEmailFacade.getOrderDetailsToSharedOrderConfirmationOrRejectionEmail(orderModel, "CONFIRM");
		System.out.println("Result::" + result);
		assertFalse(result);
	}

}



