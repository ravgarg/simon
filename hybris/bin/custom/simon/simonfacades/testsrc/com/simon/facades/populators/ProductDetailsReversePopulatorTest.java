package com.simon.facades.populators;

import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.integration.dto.CreateCartConfirmationProductDTO;


/**
 * The Class ProductsDetailsReversePopulatorTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductDetailsReversePopulatorTest
{

	@InjectMocks
	private final ProductDetailsReversePopulator productsDetailsReversePopulator = new ProductDetailsReversePopulator();

	@Mock
	private AdditionalProductDetailsModel target;

	/**
	 * populate ShippingDetailsModel from Map<String, String> which have information of ShippingDetails
	 */
	@Test
	public void testPopulate()
	{
		final CreateCartConfirmationProductDTO source = new CreateCartConfirmationProductDTO();
		source.setProductId("p123");
		final List<String> requiredfields = new ArrayList<>();
		requiredfields.add("Test");
		source.setRequiredFieldNames(requiredfields);
		productsDetailsReversePopulator.populate(source, target);
		assertNotNull(target.getRequiredFieldNames());
	}

}
