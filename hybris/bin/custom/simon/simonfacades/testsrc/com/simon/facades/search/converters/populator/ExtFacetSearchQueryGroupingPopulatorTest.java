package com.simon.facades.search.converters.populator;

import static org.mockito.Mockito.doNothing;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.SearchQueryTemplate;
import de.hybris.platform.solrfacetsearch.search.GroupCommandField;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * The Class ExtFacetSearchQueryGroupingPopulatorTest.
 */
@UnitTest
public class ExtFacetSearchQueryGroupingPopulatorTest
{

	/** The ext facet search query grouping populator. */
	@Spy
	@InjectMocks
	private ExtFacetSearchQueryGroupingPopulator extFacetSearchQueryGroupingPopulator;

	/**
	 * Test gropup sort.
	 */
	@Test
	public void testGroupSort()
	{
		MockitoAnnotations.initMocks(this);
		final SearchQueryConverterData source = Mockito.mock(SearchQueryConverterData.class);
		final SolrQuery target = new SolrQuery();
		target.add("group.sort", new String[]
		{ "normalizedColorCode_string asc" });
		final SearchQueryTemplate searchQueryTemplate = new SearchQueryTemplate();
		searchQueryTemplate.setWithinGroupSortProperties("normalizedColorCode_string asc");
		final IndexedType indexedType = Mockito.mock(IndexedType.class);
		final SearchQuery solrQuery = Mockito.mock(SearchQuery.class);
		Mockito.when(source.getSearchQuery()).thenReturn(solrQuery);
		final Map<String, SearchQueryTemplate> hmap = new HashMap<String, SearchQueryTemplate>();
		indexedType.setSearchQueryTemplates(hmap);
		Mockito.when(solrQuery.getIndexedType()).thenReturn(indexedType);
		final ArrayList alist = new ArrayList();
		alist.add(new GroupCommandField(""));
		Mockito.when(source.getSearchQuery().getGroupCommands()).thenReturn(alist);
		doNothing().when(extFacetSearchQueryGroupingPopulator).callSuperMethod(source, target);
		extFacetSearchQueryGroupingPopulator.populate(source, target);
		Assert.assertNotNull(target.get("group.sort"));
	}

}
