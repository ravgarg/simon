package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.ConsignmentEntryData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.PrincipalData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.enums.MiraklOrderLineStatus;
import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.facades.cart.data.RetailerInfoData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.facades.email.order.ShippingDetailsData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailOrderConfirmationDataPopulatorTest
{
	@InjectMocks
	private EmailOrderConfirmationDataPopulator emailOrderConfirmationDataPopulator;
	@Mock
	private Converter<OrderData, EmailOrderDetailsData> emailOrderDetailsDataConverter;
	@Mock
	private Converter<RetailerInfoData, RetailerDetailsData> emailRetailerDetailsDataConverter;
	@Mock
	private Converter<AddressData, ShippingDetailsData> emailShippingDetailsDataConverter;
	@Mock
	private Converter<OrderData, EmailOrderData> emailOrderStatusDataConverter;
	@Mock
	private Converter<OrderEntryData, ProductDetailsData> emailProductDetailsDataConverter;

	/**
	 * Init Mocks
	 */
	public void setUp()
	{
		initMocks(this);
	}

	@Test
	public void testPopulateForOrderRejected()
	{
		final OrderData source = mock(OrderData.class);
		final AddressData deliveryAddress = mock(AddressData.class);
		when(source.getStatus()).thenReturn(OrderStatus.REJECTED);
		final List<ConsignmentData> consignmentList = new ArrayList<>();
		final ConsignmentData consignment = mock(ConsignmentData.class);
		final List<ConsignmentEntryData> consignmentEntryList = new ArrayList<>();
		final ConsignmentEntryData consignmentEntryData = mock(ConsignmentEntryData.class);
		final MiraklOrderLineStatus mirakleOrderLineStatus = mock(MiraklOrderLineStatus.class);
		when(consignmentEntryData.getMiraklOrderLineStatus()).thenReturn(mirakleOrderLineStatus);
		when(mirakleOrderLineStatus.getCode()).thenReturn("Cancelled");
		final OrderEntryData orderEntryData = mock(OrderEntryData.class);
		final ProductData productData = mock(ProductData.class);
		when(orderEntryData.getProduct()).thenReturn(productData);

		when(consignmentEntryData.getOrderEntry()).thenReturn(orderEntryData);
		consignmentEntryList.add(consignmentEntryData);
		when(consignment.getEntries()).thenReturn(consignmentEntryList);

		consignmentList.add(consignment);
		when(source.getConsignments()).thenReturn(consignmentList);

		final EmailOrderData target = new EmailOrderData();
		final PrincipalData principalData = mock(PrincipalData.class);
		when(source.getUser()).thenReturn(principalData);
		final List<RetailerInfoData> retailerInfoData = new ArrayList<>();
		final RetailerInfoData retailerInfo = mock(RetailerInfoData.class);
		final List<OrderEntryData> orderEntryDataList = new ArrayList<>();
		orderEntryDataList.add(orderEntryData);
		when(retailerInfo.getProductDetails()).thenReturn(orderEntryDataList);
		when(orderEntryData.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.CANCELLED.getCode());
		final ProductDetailsData productDetailDta = mock(ProductDetailsData.class);
		when(emailProductDetailsDataConverter.convert(orderEntryData)).thenReturn(productDetailDta);

		when(retailerInfo.getRetailerId()).thenReturn("TestShop1");
		when(source.getRetailerInfoData()).thenReturn(retailerInfoData);
		final List<RetailerDetailsData> retailerDetailData = new ArrayList<>();

		final RetailerDetailsData retailerdata = mock(RetailerDetailsData.class);
		when(retailerdata.getStatus()).thenReturn("Cancelled");
		when(retailerdata.getRetailerId()).thenReturn("TestShop1");
		retailerDetailData.add(retailerdata);
		when(source.getDeliveryAddress()).thenReturn(deliveryAddress);
		when(deliveryAddress.getEmail()).thenReturn("test@test.com|mytest@test.com");
		when(emailRetailerDetailsDataConverter.convertAll(source.getRetailerInfoData())).thenReturn(retailerDetailData);
		retailerInfoData.add(retailerInfo);
		emailOrderConfirmationDataPopulator.populate(source, target);
		assertEquals("Cancelled", target.getRetailersDetail().get(0).getStatus());

	}

	@Test
	public void testPopulateForOrderCompleted()
	{
		final OrderData source = mock(OrderData.class);
		final AddressData deliveryAddress = mock(AddressData.class);
		when(source.getStatus()).thenReturn(OrderStatus.COMPLETED);
		final List<ConsignmentData> consignmentList = new ArrayList<>();
		final ConsignmentData consignment = mock(ConsignmentData.class);
		final List<ConsignmentEntryData> consignmentEntryList = new ArrayList<>();
		final ConsignmentEntryData consignmentEntryData = mock(ConsignmentEntryData.class);
		final MiraklOrderLineStatus mirakleOrderLineStatus = mock(MiraklOrderLineStatus.class);
		when(consignmentEntryData.getMiraklOrderLineStatus()).thenReturn(mirakleOrderLineStatus);
		when(mirakleOrderLineStatus.getCode()).thenReturn("Canceled");
		final OrderEntryData orderEntryData = mock(OrderEntryData.class);
		final ProductData productData = mock(ProductData.class);
		when(orderEntryData.getProduct()).thenReturn(productData);

		when(consignmentEntryData.getOrderEntry()).thenReturn(orderEntryData);
		consignmentEntryList.add(consignmentEntryData);
		when(consignment.getEntries()).thenReturn(consignmentEntryList);

		consignmentList.add(consignment);
		when(source.getConsignments()).thenReturn(consignmentList);

		final EmailOrderData target = new EmailOrderData();
		final PrincipalData principalData = mock(PrincipalData.class);
		when(source.getUser()).thenReturn(principalData);
		when(source.getDeliveryAddress()).thenReturn(deliveryAddress);
		when(deliveryAddress.getEmail()).thenReturn("mytest@test.com");
		final List<RetailerInfoData> retailerInfoData = new ArrayList<>();
		final RetailerInfoData retailerInfo = mock(RetailerInfoData.class);
		final List<OrderEntryData> orderEntryDataList = new ArrayList<>();
		orderEntryDataList.add(orderEntryData);
		when(retailerInfo.getProductDetails()).thenReturn(orderEntryDataList);
		when(orderEntryData.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.CANCELLED.getCode());
		final ProductDetailsData productDetailData = mock(ProductDetailsData.class);
		when(emailProductDetailsDataConverter.convert(orderEntryData)).thenReturn(productDetailData);

		when(retailerInfo.getRetailerId()).thenReturn("TestShop1");
		when(source.getRetailerInfoData()).thenReturn(retailerInfoData);
		final List<RetailerDetailsData> retailerDetailData = new ArrayList<>();

		final RetailerDetailsData retailerdata = mock(RetailerDetailsData.class);

		final List<ProductDetailsData> productDetailDataList = new ArrayList<>();
		productDetailDataList.add(productDetailData);
		when(productDetailData.getStatus()).thenReturn("Success");
		when(retailerdata.getProductsDetail()).thenReturn(productDetailDataList);
		retailerDetailData.add(retailerdata);
		when(emailRetailerDetailsDataConverter.convertAll(source.getRetailerInfoData())).thenReturn(retailerDetailData);
		retailerInfoData.add(retailerInfo);
		emailOrderConfirmationDataPopulator.populate(source, target);
		assertEquals("mytest@test.com", target.getEmailTo());
	}

}
