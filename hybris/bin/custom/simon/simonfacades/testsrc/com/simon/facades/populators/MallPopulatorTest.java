package com.simon.facades.populators;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;

import com.simon.core.model.MallModel;
import com.simon.facades.customer.data.MallData;


/**
 * Test class for malldata populator
 */
public class MallPopulatorTest
{

	@InjectMocks
	private final MallPopulator mallPopulator = new MallPopulator();

	/**
	 * populates the mall data with mall model
	 */
	@Test
	public void testPopulate()
	{
		final MallModel mall = Mockito.mock(MallModel.class);
		final MallData mallData = new MallData();
		Mockito.when(mall.getPropertyName()).thenReturn("Test");
		Mockito.when(mall.getCode()).thenReturn("Test");
		mallPopulator.populate(mall, mallData);
		Assert.assertEquals(mallData.getCode(), "Test");
		Assert.assertEquals(mallData.getName(), "Test");
	}
}
