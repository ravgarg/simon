package com.simon.facades.cart.strategy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.dao.CartEntryDao;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.hybris.core.model.ShopModel;
import com.sape.junit.FunctionalTest;


/**
 * Unit test case for {@link ExtCommerceAddToCartStrategyTest}
 */
@UnitTest
@FunctionalTest(features =
{ "cart" })
public class ExtCommerceAddToCartStrategyTest
{


	@InjectMocks
	@Spy
	private ExtCommerceAddToCartStrategy extCommerceAddToCartStrategy;

	@Mock
	private CommerceCartParameter parameter;

	@Mock
	private CommerceCartModification commerceCartModification;

	@Mock
	private DefaultCommerceCartCalculationStrategy defaultCommerceCartCalculationStrategy;

	@Mock
	private CartModel cartModel;

	@Mock
	private ProductModel productModel;

	@Mock
	private ProductService productService;

	@Mock
	private CartService cartService;

	@Mock
	private CartEntryDao cartEntryDao;

	@Mock
	private BaseStoreService baseStoreService;

	@Mock
	private BaseStoreModel baseStoreModel;

	@Mock
	private CommerceStockService commerceStockService;

	@Mock
	private ShopModel shop;

	@Mock
	private PointOfServiceModel pointOfService;

	@Mock
	private CartEntryModel cartEntry;

	@Mock
	private ModelService modelService;



	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method to test add to cart strategy and recalculation
	 *
	 * @throws CommerceCartModificationException
	 *
	 */
	@Test
	public void testAddToCart() throws CommerceCartModificationException
	{
		final Long qty = 2L;
		final String productCode = "testProduct";
		final List<CartEntryModel> entry = new ArrayList<>();
		when(defaultCommerceCartCalculationStrategy.recalculateCart(parameter)).thenReturn(true);
		when(commerceStockService.isStockSystemEnabled(baseStoreModel)).thenReturn(true);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
		when(cartService.getEntriesForProduct(cartModel, productModel)).thenReturn(entry);
		when(productService.getProductForCode(productCode)).thenReturn(productModel);
		when(parameter.getQuantity()).thenReturn(qty);
		when(parameter.getCart()).thenReturn(cartModel);
		when(parameter.getProduct()).thenReturn(productModel);
		assertNotNull(extCommerceAddToCartStrategy.addToCart(parameter));
	}

	@Test
	public void testAddToCartWhenAllowedQuantityEqualsQuantitytobeadded() throws CommerceCartModificationException
	{
		final Long qty = 2L;
		final String productCode = "testProduct";
		final List<CartEntryModel> entry = new ArrayList<>();
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
		when(defaultCommerceCartCalculationStrategy.recalculateCart(parameter)).thenReturn(true);
		when(commerceStockService.isStockSystemEnabled(baseStoreModel)).thenReturn(true);
		when(cartService.getEntriesForProduct(cartModel, productModel)).thenReturn(entry);
		when(productService.getProductForCode(productCode)).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shop);
		when(shop.getId()).thenReturn("shop");
		when(parameter.getProduct()).thenReturn(productModel);
		when(parameter.getQuantity()).thenReturn(10L);
		when(parameter.getCart()).thenReturn(cartModel);
		when(parameter.getProduct()).thenReturn(productModel);
		when(parameter.getPointOfService()).thenReturn(pointOfService);
		when(productModel.getMaxOrderQuantity()).thenReturn(new Integer(20));
		when(commerceStockService.getStockLevelForProductAndPointOfService(productModel, pointOfService)).thenReturn(10L);
		when(cartEntryDao.findEntriesByProductAndPointOfService(cartModel, productModel, pointOfService)).thenReturn(entry);
		when(cartService.addNewEntry(cartModel, productModel, 10L, null, -1, false)).thenReturn(cartEntry);
		doNothing().when(modelService).save(cartEntry);
		assertEquals(extCommerceAddToCartStrategy.addToCart(parameter).getStatusCode(), "success");
	}

	@Test
	public void testAddToCartWhenAllowedQuantityNotEqualsQuantitytobeadded() throws CommerceCartModificationException
	{
		final Long qty = 2L;
		final String productCode = "testProduct";
		final List<CartEntryModel> entry = new ArrayList<>();
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStoreModel);
		when(defaultCommerceCartCalculationStrategy.recalculateCart(parameter)).thenReturn(true);
		when(commerceStockService.isStockSystemEnabled(baseStoreModel)).thenReturn(true);
		when(commerceStockService.getStockLevelForProductAndBaseStore(productModel, baseStoreModel)).thenReturn(10L);
		when(cartService.getEntriesForProduct(cartModel, productModel)).thenReturn(entry);
		when(productService.getProductForCode(productCode)).thenReturn(productModel);
		when(parameter.getQuantity()).thenReturn(11L);
		when(parameter.getCart()).thenReturn(cartModel);
		when(parameter.getProduct()).thenReturn(productModel);
		when(productModel.getMaxOrderQuantity()).thenReturn(new Integer(20));
		assertEquals(extCommerceAddToCartStrategy.addToCart(parameter).getStatusCode(), "lowStock");
	}

}
