package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.VariantOptionData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.product.EmailProductData;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailProductDataPopulatorTest
{

	private static final String MAX_SALE_RETAIL_PRICE = "MSRP";
	private static final String SALE_PRICE = "SALE";
	private static final String DEFUALT_IMAGE_KEY = "Desktop";

	@InjectMocks
	@Spy
	private EmailProductDataPopulator emailProductDataPopulator;

	private ProductData source;
	private EmailProductData target;

	private VariantOptionData variantOptionData;
	private ImageData imageData;
	private final List<ImageData> imageDatas = new ArrayList<>();
	private final List<VariantOptionData> variantOptionDatas = new ArrayList<>();
	private final Map<String, Pair<Double, Boolean>> variantPrice = new HashMap<String, Pair<Double, Boolean>>();

	/**
	 *
	 */
	@Test
	public void emailProductDataPopulatorTest()
	{
		source = new ProductData();
		imageData = new ImageData();
		variantPrice.put(MAX_SALE_RETAIL_PRICE, Pair.of(123d, true));
		variantPrice.put(SALE_PRICE, Pair.of(112d, true));

		imageData.setFormat(DEFUALT_IMAGE_KEY);
		imageData.setUrl("abc");
		imageDatas.add(imageData);

		variantOptionData = new VariantOptionData();
		variantOptionData.setBaseProductName("Test Product");
		variantOptionData.setColorName("Red");
		variantOptionData.setUrl("xyz");
		variantOptionData.setBaseProductRetailerName("Test shop");
		variantOptionData.setVariantImage(imageDatas);
		variantOptionData.setVariantPrice(variantPrice);
		variantOptionDatas.add(variantOptionData);

		source.setVariantOptions(variantOptionDatas);
		source.setPercentageOffRange("upto 25 %");

		target = new EmailProductData();
		emailProductDataPopulator.populate(source, target);
		Assert.assertEquals(source.getVariantOptions().get(0).getColorName(), target.getProductDetails().getColor());
		Assert.assertEquals(source.getVariantOptions().get(0).getBaseProductRetailerName(),
				target.getProductDetails().getRetailerName());
	}

}
