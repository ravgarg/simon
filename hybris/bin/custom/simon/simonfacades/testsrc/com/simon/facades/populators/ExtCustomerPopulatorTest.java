
package com.simon.facades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.enums.CustomerGender;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.MallModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.customer.data.MallData;




/**
 * The Class ExtCustomerPopulatorTest.
 */
@UnitTest
public class ExtCustomerPopulatorTest
{

	@InjectMocks
	private final ExtCustomerPopulator extCustomerPopulator = new ExtCustomerPopulator();

	@Mock
	private Converter<CountryModel, CountryData> countryConverter;

	@Mock
	private CustomerNameStrategy customerNameStrategy;

	@Mock
	private Converter<DealModel, DealData> extDealConverter;

	@Mock
	private Converter<DesignerModel, DesignerData> extDesignerConverter;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * test the population of data from customer model to CustomerData
	 */
	@Test
	public void testPopulate()
	{
		final Converter<MallModel, MallData> extMallConverter = Mockito.mock(Converter.class);
		final CustomerNameStrategy customerNameStrategy = Mockito.mock(CustomerNameStrategy.class);
		final TitleModel title = Mockito.mock(TitleModel.class);
		final String[] names =
		{ "test", "test" };
		extCustomerPopulator.setCountryConverter(countryConverter);
		extCustomerPopulator.setCustomerNameStrategy(customerNameStrategy);
		extCustomerPopulator.setExtMallConverter(extMallConverter);
		final CustomerModel source = new CustomerModel();
		source.setName("Tesst");
		source.setUid("test@test.com");
		source.setTitle(title);
		source.setGender(CustomerGender.FEMALE);
		source.setOptedInEmail(true);
		final MallModel mall = Mockito.mock(MallModel.class);
		Mockito.when(mall.getCode()).thenReturn("Mall123");
		Mockito.when(mall.getLogo()).thenReturn("URL");
		source.setPrimaryMall(mall);
		final CustomerData target = new CustomerData();
		given(customerNameStrategy.splitName(source.getName())).willReturn(names);
		extCustomerPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), CustomerGender.FEMALE);

	}

	/**
	 * test the population of data from customer model to CustomerData when primary center is empty
	 */
	@Test
	public void testPopulateEmptyPrimaryCenter()
	{
		final Converter<MallModel, MallData> extMallConverter = Mockito.mock(Converter.class);
		final CustomerNameStrategy customerNameStrategy = Mockito.mock(CustomerNameStrategy.class);
		final TitleModel title = Mockito.mock(TitleModel.class);
		final String[] names =
		{ "test", "test" };
		extCustomerPopulator.setCountryConverter(countryConverter);
		extCustomerPopulator.setCustomerNameStrategy(customerNameStrategy);
		extCustomerPopulator.setExtMallConverter(extMallConverter);
		final CustomerModel source = new CustomerModel();
		source.setName("Tesst");
		source.setUid("test@test.com");
		source.setTitle(title);
		source.setGender(CustomerGender.FEMALE);
		source.setOptedInEmail(true);
		final CustomerData target = new CustomerData();
		given(customerNameStrategy.splitName(source.getName())).willReturn(names);
		extCustomerPopulator.populate(source, target);
		Assert.assertEquals(target.getGender(), CustomerGender.FEMALE);

	}

	/**
	 * test for valid deals
	 */
	@Test
	public void testPopulateWithValidOffers()
	{
		final CustomerModel source = new CustomerModel();
		final Set<DealModel> offers = new HashSet<>();

		final DealModel deal = new DealModel();
		offers.add(deal);
		source.setMyOffers(offers);
		final List<DealData> dealsData = new ArrayList<>();
		final DealData dealData = new DealData();
		dealsData.add(dealData);
		Mockito.when(extDealConverter.convertAll(offers)).thenReturn(dealsData);
		final CustomerData target = new CustomerData();
		extCustomerPopulator.populate(source, target);
		Assert.assertEquals(1, target.getMyDeals().size());
	}

	/**
	 * test for valid designers
	 */
	@Test
	public void testPopulateWithDesigners()
	{
		final CustomerModel source = new CustomerModel();
		final Set<DesignerModel> designers = new HashSet<>();

		final DesignerModel designer = new DesignerModel();
		designers.add(designer);
		source.setMyDesigners(designers);
		final List<DesignerData> designersData = new ArrayList<>();
		final DesignerData designerData = new DesignerData();
		designersData.add(designerData);
		Mockito.when(extDesignerConverter.convertAll(designers)).thenReturn(designersData);
		final CustomerData target = new CustomerData();
		extCustomerPopulator.populate(source, target);
		Assert.assertEquals(1, target.getMyDesigners().size());
	}

}
