package com.simon.facades.solr.product.page.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dto.ProductDetailsData;
import com.simon.core.services.solr.product.page.SolrProductPageService;


@UnitTest
public class DefaultSolrProductPageFacadeTest
{
	@InjectMocks
	DefaultSolrProductPageFacade defaultSolrProductPageFacade;

	@Mock
	private SolrProductPageService<SolrSearchQueryData, SearchResultValueData, ProductDetailsData> productPageSearchService;
	@Mock
	private ThreadContextService threadContextService;
	@Mock
	private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;
	private SearchStateData searchState;
	private PageableData pageableData;
	private ProductDetailsData productDetailsData;

	@Before
	public void setUp() throws Throwable
	{
		MockitoAnnotations.initMocks(this);
		searchState = new SearchStateData();
		final SearchQueryData query = new SearchQueryData();
		searchState.setQuery(query);
		pageableData = new PageableData();
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		when(searchQueryDecoder.convert(query)).thenReturn(searchQueryData);
		productDetailsData = new ProductDetailsData();
		when(defaultSolrProductPageFacade.getProductPageSearchService().getProductDetails(searchQueryData, pageableData))
				.thenReturn(productDetailsData);
		when(threadContextService.executeInContext(Matchers.any())).thenReturn(productDetailsData);
	}

	@Test
	public void testGetProductDetails()
	{
		assertEquals(productDetailsData, defaultSolrProductPageFacade.getProductDetails(searchState, pageableData));
	}

	@Test
	public void testGetProductDetailsOfDecodedState()
	{
		assertEquals(productDetailsData, defaultSolrProductPageFacade.getProductDetailsOfDecodedState(searchState, pageableData));
	}


	@Test
	public void testDecodeState()
	{
		assertEquals("categoryCode", defaultSolrProductPageFacade.decodeState(searchState, "categoryCode").getCategoryCode());
	}
}
