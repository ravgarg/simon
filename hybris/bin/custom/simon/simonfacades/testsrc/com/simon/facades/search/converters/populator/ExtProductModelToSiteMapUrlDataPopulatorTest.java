package com.simon.facades.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.data.SiteMapUrlData;
import de.hybris.platform.acceleratorservices.sitemap.populators.ProductModelToSiteMapUrlDataPopulator;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * Test class for ExtProductModelToSiteMapUrlDataPopulator
 *
 */
@UnitTest
public class ExtProductModelToSiteMapUrlDataPopulatorTest
{
	@Spy
	@InjectMocks
	private ExtProductModelToSiteMapUrlDataPopulator extProductModelToSiteMapUrlDataPopulator;

	@Mock
	private ProductModelToSiteMapUrlDataPopulator productModelToSiteMapUrlDataPopulator;

	@Mock
	private UrlResolver<ProductModel> urlResolver;

	/**
	 * Setup Method for Junit
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test method for populate
	 */
	@Test
	public void testPopulate()
	{
		final ProductModel productModel = Mockito.mock(ProductModel.class);
		final SiteMapUrlData siteMapUrlData = new SiteMapUrlData();
		Mockito.when(productModelToSiteMapUrlDataPopulator.getUrlResolver()).thenReturn(urlResolver);
		Mockito.when(urlResolver.resolve(productModel)).thenReturn("url");
		extProductModelToSiteMapUrlDataPopulator.populate(productModel, siteMapUrlData);
		Assert.assertEquals(siteMapUrlData.getLoc(), "url");
	}

}
