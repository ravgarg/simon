package com.simon.facades.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchSolrQueryPopulator;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.SearchConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;



/**
 * The Class ExtSearchSolrQueryPopulatorTest.
 */
@UnitTest
public class ExtSearchSolrQueryPopulatorTest
{


	@InjectMocks
	@Spy
	private ExtSearchSolrQueryPopulator populator;

	@Mock
	private FieldNameProvider fieldNameProvider;

	@Mock
	private SearchQueryPageableData source;

	@Mock
	private SearchSolrQueryPopulator searchSolrQueryPopulator;

	private final SolrSearchRequest target = new SolrSearchRequest<>();

	/** The variable sessionService. */
	@Mock
	private SessionService sessionService;

	@Mock
	private BaseSiteService baseSiteService;

	@Mock
	private BaseSiteModel baseSiteModel;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private CatalogVersionModel sessionCatalogVersion;

	@Mock
	private Collection<CatalogVersionModel> sessionCatalogVersions;

	@Mock
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;

	@Mock
	private FacetSearchConfigService facetSearchConfigService;
	@Mock
	private SearchConfig searchConfig;
	@Mock
	private IndexedType indexType;

	private SearchQuery searchQuery;

	private final List<String> favproducts = new ArrayList<>();

	@Mock
	private FacetSearchConfig facetSearchConfig;
	@Mock
	private CatalogModel catalogModel;
	@Mock
	private IndexedType indexedType;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private SolrFacetSearchConfigModel solrFacetSearchConfigModel;
	@Mock
	private IndexedProperty property;


	public static final String FAVOURITE_PRODUCT = "favoriteProducts";
	public static final String DESIGNER_NAME = "designerName";
	public static final String INDEX_PROP = "baseProductCode";

	/**
	 * Sets it up.
	 *
	 * @throws Exception
	 *            the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		target.setFacetSearchConfig(facetSearchConfig);
		target.setIndexedType(indexType);
	}

	/**
	 * Test case for populate with Null
	 *
	 * @throws NoValidSolrConfigException
	 * @throws FacetConfigServiceException
	 *
	 */
	@Test
	public void testPopulateWithEmptySearchQuery() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductCode", property);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();

		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);

		target.setSearchQuery(searchQuery);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		populator.populate(source, target);
		Assert.assertNull(target.getSearchText());
	}

	@Test
	public void testPopulateWithNullFavorites() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final IndexedProperty property = Mockito.mock(IndexedProperty.class);
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductCode", property);
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = Mockito.mock(SolrFacetSearchConfigModel.class);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);

		target.setSearchQuery(searchQuery);
		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		populator.populate(source, target);
		Assert.assertNull(target.getSearchText());
	}

	@Test
	public void testPopulateWithEmptyFavorites() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final IndexedProperty property = Mockito.mock(IndexedProperty.class);
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductCode", property);
		final IndexedType indexedType = Mockito.mock(IndexedType.class);
		final CatalogModel catalogModel = Mockito.mock(CatalogModel.class);
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = Mockito.mock(SolrFacetSearchConfigModel.class);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final FacetSearchConfig facetSearchConfig = Mockito.mock(FacetSearchConfig.class);
		final IndexConfig indexConfig = Mockito.mock(IndexConfig.class);
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);
		target.setSearchQuery(searchQuery);
		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		Mockito.when(sessionService.getAttribute(FAVOURITE_PRODUCT)).thenReturn(favproducts);
		populator.populate(source, target);
		Assert.assertNull(target.getSearchText());
	}

	@Test
	public void testPopulateWithValidFavorites() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final IndexedProperty property = Mockito.mock(IndexedProperty.class);
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductCode", property);
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = Mockito.mock(SolrFacetSearchConfigModel.class);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);
		target.setSearchQuery(searchQuery);
		favproducts.add("111");
		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		Mockito.when(sessionService.getAttribute(FAVOURITE_PRODUCT)).thenReturn(favproducts);
		populator.populate(source, target);
		Assert.assertNotNull(target.getSearchQuery());
	}

	private void mockObjects(final Map<String, IndexedProperty> indexedProperties, final IndexedType indexedType,
			final CatalogModel catalogModel, final SolrFacetSearchConfigModel solrFacetSearchConfigModel,
			final Collection<CatalogVersionModel> sessionCatalogVersions, final FacetSearchConfig facetSearchConfig,
			final IndexConfig indexConfig, final List<CatalogModel> productCatalogs)
			throws NoValidSolrConfigException, FacetConfigServiceException
	{
		Mockito.when(catalogVersionService.getSessionCatalogVersions()).thenReturn(sessionCatalogVersions);
		Mockito.when(sessionCatalogVersion.getCatalog()).thenReturn(catalogModel);
		Mockito.when(indexedType.getIndexedProperties()).thenReturn(indexedProperties);
		Mockito.when(baseSiteService.getCurrentBaseSite()).thenReturn(baseSiteModel);
		Mockito.when(baseSiteService.getProductCatalogs(baseSiteModel)).thenReturn(productCatalogs);
		Mockito.when(solrFacetSearchConfigSelectionStrategy.getCurrentSolrFacetSearchConfig())
				.thenReturn(solrFacetSearchConfigModel);
		Mockito.when(solrFacetSearchConfigModel.getName()).thenReturn("Name");
		Mockito.when(facetSearchConfigService.getConfiguration("name")).thenReturn(facetSearchConfig);
		Mockito.when(facetSearchConfig.getIndexConfig()).thenReturn(indexConfig);
		Mockito.when(facetSearchConfig.getName()).thenReturn("name");
		Mockito.when(facetSearchConfig.getSearchConfig()).thenReturn(searchConfig);
		Mockito.when(searchConfig.isLegacyMode()).thenReturn(true);
		Mockito.doNothing().when(populator).callsuper(source, target);
		Mockito.when(indexType.getIndexedProperties()).thenReturn(indexedProperties);

		Mockito.when(sessionService.getAttribute(FAVOURITE_PRODUCT)).thenReturn(null);
		Mockito.when(sessionService.getAttribute(DESIGNER_NAME)).thenReturn(null);
	}

	@Test
	public void testPopulateWithEmptyDesignerProducts() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final IndexedProperty property = Mockito.mock(IndexedProperty.class);
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductCode", property);
		final IndexedType indexedType = Mockito.mock(IndexedType.class);
		final CatalogModel catalogModel = Mockito.mock(CatalogModel.class);
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = Mockito.mock(SolrFacetSearchConfigModel.class);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final FacetSearchConfig facetSearchConfig = Mockito.mock(FacetSearchConfig.class);
		final IndexConfig indexConfig = Mockito.mock(IndexConfig.class);
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);
		target.setSearchQuery(searchQuery);
		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		Mockito.when(sessionService.getAttribute(FAVOURITE_PRODUCT)).thenReturn(null);
		Mockito.when(sessionService.getAttribute(DESIGNER_NAME)).thenReturn(null);
		populator.populate(source, target);
		Assert.assertNull(target.getSearchText());
	}

	@Test
	public void testPopulateWithValidDesignerProducts() throws NoValidSolrConfigException, FacetConfigServiceException
	{

		final IndexedProperty property = Mockito.mock(IndexedProperty.class);
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("baseProductDesignerName", property);
		indexedProperties.put("retailerActive", property);
		final SolrFacetSearchConfigModel solrFacetSearchConfigModel = Mockito.mock(SolrFacetSearchConfigModel.class);
		final Collection<CatalogVersionModel> sessionCatalogVersions = new ArrayList<CatalogVersionModel>();
		final CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
		final Map<String, IndexedType> map = new HashMap<String, IndexedType>();
		searchQuery = new SearchQuery(facetSearchConfig, indexedType);
		target.setSearchQuery(searchQuery);
		catalogVersionModel.setCatalog(catalogModel);
		sessionCatalogVersions.add(catalogVersionModel);
		indexedType.setIndexName("Name");
		indexedType.setCode("code");
		map.put("as", indexedType);
		indexConfig.setIndexedTypes(map);
		final List<CatalogModel> productCatalogs = new ArrayList<>();
		productCatalogs.add(catalogModel);
		mockObjects(indexedProperties, indexedType, catalogModel, solrFacetSearchConfigModel, sessionCatalogVersions,
				facetSearchConfig, indexConfig, productCatalogs);
		Mockito.when(sessionService.getAttribute(FAVOURITE_PRODUCT)).thenReturn(null);
		Mockito.when(sessionService.getAttribute(DESIGNER_NAME)).thenReturn("abc");
		populator.populate(source, target);
		Assert.assertNotNull(target.getSearchQuery());
	}


}
