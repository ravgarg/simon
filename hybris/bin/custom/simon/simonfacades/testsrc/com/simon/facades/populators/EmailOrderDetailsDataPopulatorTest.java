package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailOrderDetailsDataPopulatorTest
{
	@InjectMocks
	private EmailOrderDetailsDataPopulator emailOrderDetailsDataPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	/**
	 * Init Mocks
	 */
	public void setUp()
	{
		initMocks(this);
	}

	@Test
	public void testPopulate()
	{
		final OrderData source = mock(OrderData.class);
		when(source.getCode()).thenReturn("Order123");
		final EmailOrderDetailsData target = new EmailOrderDetailsData();
		final CCPaymentInfoData ccpaymentInfodata = mock(CCPaymentInfoData.class);
		when(source.getPaymentInfo()).thenReturn(ccpaymentInfodata);
		emailOrderDetailsDataPopulator.populate(source, target);
		assertEquals("Order123", target.getOrderId());
	}

}
