package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.sape.junit.FunctionalTest;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.services.price.ExtPriceService;


/**
 * ExtPriceDataPopulatorTest test ExtPriceDataPopulator which set the list value, sales value and msrp value in
 * OrderEntryData object
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
@FunctionalTest(features =
{ SimonCoreConstants.CART })
public class ExtPriceDataPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtPriceDataPopulator<AbstractOrderEntryModel, OrderEntryData> extPriceDataPopulator;

	@Mock
	private AbstractOrderEntryModel eachOrderEntryModel;
	@Mock
	private PriceDataFactory priceDataFactory;
	@Mock
	private ProductModel productModel;
	@Mock
	private PriceData listValueData;
	@Mock
	private PriceData saleValueData;
	@Mock
	private PriceData msrpValueData;
	@Mock
	private PriceData youSave;
	@Mock
	private ExtPriceService priceService;
	Map<DetailedPriceInfo, Object> map;

	/**
	 * this method test ExtPriceDataPopulator which set the list value, sales value and msrp value in OrderEntryData
	 * object
	 */
	@Before
	public void setup()
	{
		when(eachOrderEntryModel.getProduct()).thenReturn(productModel);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(Double.valueOf(25.00d)), "USD"))
				.thenReturn(listValueData);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(Double.valueOf(23.00d)), "USD"))
				.thenReturn(saleValueData);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(Double.valueOf(28.00d)), "USD"))
				.thenReturn(msrpValueData);
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(Double.valueOf(0.0)), "USD")).thenReturn(msrpValueData);
		map = new HashMap<>();
		map.put(DetailedPriceInfo.IS_LP_STRIKEOFF, true);
		map.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, false);
		map.put(DetailedPriceInfo.PERCENTAGE_SAVING, 20);
	}

	@Test
	public void populate_prices_when_BasePrice_Is_Available()
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso("USD");
		orderEntryData.setBasePrice(priceData);
		when(productModel.getMsrp()).thenReturn(Double.valueOf(28.00d));
		when(eachOrderEntryModel.getListValue()).thenReturn(Double.valueOf(25.00d));
		when(eachOrderEntryModel.getSaleValue()).thenReturn(Double.valueOf(23.00d));
		when(priceService.getDetailedPriceInformation(28.00d, 25.00d, 23.00d)).thenReturn(map);
		doReturn("% off").when(extPriceDataPopulator).getPercentSavingMessage();
		extPriceDataPopulator.populate(eachOrderEntryModel, orderEntryData);
		assertEquals(listValueData, orderEntryData.getListValue());
	}

	@Test
	public void populate_prices_when_basePrice_is_null()
	{

		final OrderEntryData orderEntryData = new OrderEntryData();
		assertNull(orderEntryData.getListValue());
	}

	@Test
	public void populate_prices_when_msrp_is_null()
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso("USD");
		orderEntryData.setBasePrice(priceData);
		when(productModel.getMsrp()).thenReturn(null);
		when(eachOrderEntryModel.getListValue()).thenReturn(Double.valueOf(25.00d));
		when(eachOrderEntryModel.getSaleValue()).thenReturn(Double.valueOf(23.00d));
		doReturn(map).when(priceService).getDetailedPriceInformation(0.0, 25.00d, 23.00d);
		doReturn("% off").when(extPriceDataPopulator).getPercentSavingMessage();
		extPriceDataPopulator.populate(eachOrderEntryModel, orderEntryData);
		assertEquals(listValueData, orderEntryData.getListValue());
	}

	@Test
	public void populate_prices_when_slePrice_is_null()
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso("USD");
		orderEntryData.setBasePrice(priceData);
		when(productModel.getMsrp()).thenReturn(Double.valueOf(28.00d));
		when(eachOrderEntryModel.getListValue()).thenReturn(Double.valueOf(25.00d));
		when(eachOrderEntryModel.getSaleValue()).thenReturn(null);
		doReturn(map).when(priceService).getDetailedPriceInformation(28.00d, 25.00d, 0.0);
		doReturn("% off").when(extPriceDataPopulator).getPercentSavingMessage();
		extPriceDataPopulator.populate(eachOrderEntryModel, orderEntryData);
		assertEquals(listValueData, orderEntryData.getListValue());
	}

	@Test
	public void populate_prices_when_listPrice_is_null()
	{
		final OrderEntryData orderEntryData = new OrderEntryData();
		final PriceData priceData = new PriceData();
		priceData.setCurrencyIso("USD");
		orderEntryData.setBasePrice(priceData);
		when(productModel.getMsrp()).thenReturn(Double.valueOf(28.00d));
		when(eachOrderEntryModel.getListValue()).thenReturn(0.0);
		when(eachOrderEntryModel.getSaleValue()).thenReturn(Double.valueOf(25.00d));
		doReturn(map).when(priceService).getDetailedPriceInformation(28.00d, 0.0, 25.00d);
		doReturn("% off").when(extPriceDataPopulator).getPercentSavingMessage();
		extPriceDataPopulator.populate(eachOrderEntryModel, orderEntryData);
		assertEquals(msrpValueData, orderEntryData.getListValue());
	}

}
