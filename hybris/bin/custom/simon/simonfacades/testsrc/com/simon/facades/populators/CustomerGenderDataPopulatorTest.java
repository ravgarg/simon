package com.simon.facades.populators;

import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.type.TypeService;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.enums.CustomerGender;
import com.simon.facades.product.data.GenderData;


/**
 * Test class for CustomerGenderDataPopulator It is having all test methods to test CustomerGenderDataPopulator.
 */
@UnitTest
public class CustomerGenderDataPopulatorTest
{

	@Mock
	private TypeService typeService;

	@InjectMocks
	private final CustomerGenderDataPopulator customerGenderDataPopulator = new CustomerGenderDataPopulator(); //NOPMD

	/**
	 * This is the method to test convert method of CustomerGenderDataPopulator.
	 */
	@Test
	public void testConvert()
	{
		final CustomerGender customergendermale = CustomerGender.MALE;
		final TypeService typeService = Mockito.mock(TypeService.class);
		final EnumerationValueModel value = Mockito.mock(EnumerationValueModel.class);
		given(typeService.getEnumerationValue(customergendermale)).willReturn(value);
		given(value.getName()).willReturn("Male");
		given(typeService.getEnumerationValue(customergendermale).getName()).willReturn("Male");
		final GenderData genderData = new GenderData();
		customerGenderDataPopulator.setTypeService(typeService);
		customerGenderDataPopulator.populate(customergendermale, genderData);
		Assert.assertEquals("MALE", genderData.getCode());
	}
}
