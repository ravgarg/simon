package com.simon.facades.populators;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklOrderCustomer;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderOffer;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrderPaymentInfo;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.order.strategies.ShippingZoneStrategy;
import com.simon.core.dao.ExtOrderDao;
import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.core.services.RetailerService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMiraklCreateOrderPopulatorTest
{
	@InjectMocks
	private ExtMiraklCreateOrderPopulator extMiraklCreateOrderPopulator;
	@Mock
	private RetailerService retailerService;

	@Mock
	private ModelService modelService;

	@Mock
	private ExtOrderDao extOrderDao;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Converter<AbstractOrderEntryModel, MiraklCreateOrderOffer> miraklCreateOrderOfferConverter;

	@Mock
	private Converter<OrderModel, MiraklOrderCustomer> miraklOrderCustomerConverter;
	@Mock
	private ShippingZoneStrategy shippingZoneStrategy;
	@Mock
	private Converter<PaymentInfoModel, MiraklCreateOrderPaymentInfo> miraklCreateOrderPaymentInfoConverter;
	@Mock
	private Configuration config;

	@Mock
	private OrderModel orderModel;
	@Mock
	private AbstractOrderEntryModel retailer1Entry1;
	@Mock
	private AbstractOrderEntryModel retailer1Entry2;
	@Mock
	private AbstractOrderEntryModel retailer2Entry1;
	@Mock
	private AbstractOrderEntryModel retailer2Entry2;
	@Mock
	private OrderModel hybOrderModel;
	@Mock
	private AbstractOrderEntryModel hybRetailer1Entry1;
	@Mock
	private AbstractOrderEntryModel hybRetailer1Entry2;
	@Mock
	private AbstractOrderEntryModel hybRetailer2Entry1;
	@Mock
	private AbstractOrderEntryModel hybRetailer2Entry2;
	@Mock
	private ProductModel product1FromRetailer1;
	@Mock
	private ProductModel product2FromRetailer1;
	@Mock
	private ProductModel product1FromRetailer2;
	@Mock
	private ProductModel product2FromRetailer2;
	@Mock
	private ShopModel shopOfRetailer1;
	@Mock
	private ShopModel shopOfRetailer2;

	private MiraklCreateOrderOffer offer;

	private MiraklCreateOrder miraklCreateOrder;
	private Map<String, String> retailerOrderNumbers;
	final private String retailer1 = "RETAILER1";
	final private String retailer2 = "RETAILER2";
	final private String retailer1OrderNumber = "RETAILER1ORNO1";
	final private String retailer2OrderNumber = "RETAILER2ORNO2";

	private List<AbstractOrderEntryModel> marketEntries;
	private List<MiraklCreateOrderOffer> offers;
	private final String orderNumber = "00001";


	@Before
	public void setup()
	{
		offer = new MiraklCreateOrderOffer();
		retailerOrderNumbers = new HashMap();
		retailerOrderNumbers.put(retailer1, retailer1OrderNumber);
		retailerOrderNumbers.put(retailer2, retailer2OrderNumber);
		miraklCreateOrder = new MiraklCreateOrder();
		marketEntries = new ArrayList();
		marketEntries.add(retailer1Entry1);
		marketEntries.add(retailer1Entry2);
		marketEntries.add(retailer2Entry1);
		marketEntries.add(retailer2Entry2);
		offers = new ArrayList();
		offers.add(offer);

		when(orderModel.getCode()).thenReturn(orderNumber);
		when(orderModel.getRetailersOrderNumbers()).thenReturn(retailerOrderNumbers);
		when(orderModel.getMarketplaceEntries()).thenReturn(marketEntries);
		when(retailer1Entry1.getProduct()).thenReturn(product1FromRetailer1);
		when(retailer1Entry2.getProduct()).thenReturn(product2FromRetailer1);
		when(retailer2Entry1.getProduct()).thenReturn(product1FromRetailer2);
		when(retailer2Entry2.getProduct()).thenReturn(product2FromRetailer2);
		when(retailer1Entry1.getQuantity()).thenReturn(1L);
		when(retailer1Entry2.getQuantity()).thenReturn(2L);
		when(retailer2Entry1.getQuantity()).thenReturn(3L);
		when(retailer2Entry2.getQuantity()).thenReturn(0L);
		when(retailer1Entry1.getBasePrice()).thenReturn(1d);
		when(retailer1Entry2.getBasePrice()).thenReturn(2d);
		when(retailer2Entry1.getBasePrice()).thenReturn(3d);
		when(retailer2Entry2.getBasePrice()).thenReturn(4d);
		when(retailer1Entry1.getOfferId()).thenReturn("offer");
		when(retailer1Entry2.getOfferId()).thenReturn("offer");
		when(retailer2Entry1.getOfferId()).thenReturn("offer");
		when(retailer2Entry2.getOfferId()).thenReturn("offer");

		when(hybOrderModel.getRetailersOrderNumbers()).thenReturn(retailerOrderNumbers);
		when(hybOrderModel.getMarketplaceEntries()).thenReturn(marketEntries);
		when(hybRetailer1Entry1.getProduct()).thenReturn(product1FromRetailer1);
		when(hybRetailer1Entry2.getProduct()).thenReturn(product2FromRetailer1);
		when(hybRetailer2Entry1.getProduct()).thenReturn(product1FromRetailer2);
		when(hybRetailer2Entry2.getProduct()).thenReturn(product2FromRetailer2);
		when(hybRetailer1Entry1.getQuantity()).thenReturn(1L);
		when(hybRetailer1Entry2.getQuantity()).thenReturn(2L);
		when(hybRetailer2Entry1.getQuantity()).thenReturn(3L);
		when(hybRetailer2Entry2.getQuantity()).thenReturn(0L);
		when(hybRetailer1Entry1.getBasePrice()).thenReturn(2d);
		when(hybRetailer1Entry2.getBasePrice()).thenReturn(4d);
		when(hybRetailer2Entry1.getBasePrice()).thenReturn(6d);
		when(hybRetailer2Entry2.getBasePrice()).thenReturn(8d);
		when(hybRetailer1Entry1.getOfferId()).thenReturn("offer");
		when(hybRetailer1Entry2.getOfferId()).thenReturn("offer");
		when(hybRetailer2Entry1.getOfferId()).thenReturn("offer");
		when(hybRetailer2Entry2.getOfferId()).thenReturn("offer");

		offer.setId("offer");

		when(product1FromRetailer1.getShop()).thenReturn(shopOfRetailer1);
		when(product2FromRetailer1.getShop()).thenReturn(shopOfRetailer1);
		when(product1FromRetailer2.getShop()).thenReturn(shopOfRetailer2);
		when(product2FromRetailer2.getShop()).thenReturn(shopOfRetailer2);
		when(retailer1Entry1.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.APPROVED);
		when(retailer1Entry2.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.CANCELLED);
		when(retailer2Entry1.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.APPROVED);
		when(retailer2Entry2.getCallBackOrderEntryStatus()).thenReturn(CallBackOrderEntryStatus.CANCELLED);
		when(shopOfRetailer1.getId()).thenReturn(retailer1);
		when(shopOfRetailer2.getId()).thenReturn(retailer2);
		when(miraklCreateOrderOfferConverter.convertAll(Matchers.any())).thenReturn(offers);
		when(shippingZoneStrategy.getShippingZoneCode(orderModel)).thenReturn("cheapest");
		when(miraklCreateOrderPaymentInfoConverter.convert(orderModel.getPaymentInfo()))
				.thenReturn(new MiraklCreateOrderPaymentInfo());
		when(miraklOrderCustomerConverter.convert(orderModel)).thenReturn(new MiraklOrderCustomer());
		when(extOrderDao.getVersionedOrderByOrderId(orderNumber)).thenReturn(hybOrderModel);
		when(config.getString("marketplace.order.custom.field")).thenReturn("hybris-subtotal,bot-subtotal");
		when(config.getString("marketplace.order.custom.field")).thenReturn("hybris-subtotal,bot-subtotal");
		when(config.getString("hybris-subtotal")).thenReturn("retailersSubtotal");
		when(config.getString("bot-subtotal")).thenReturn("retailersSubtotal");
		when(configurationService.getConfiguration()).thenReturn(config);

		final Map<String, Double> botSubtotalMap = new HashMap<>();
		botSubtotalMap.put(retailer1, 30d);
		botSubtotalMap.put(retailer2, 40d);
		final Map<String, Double> hybSubtotalMap = new HashMap<>();
		hybSubtotalMap.put(retailer1, 50d);
		hybSubtotalMap.put(retailer2, 60d);
		when(modelService.getAttributeValue(orderModel, "retailersSubtotal")).thenReturn(botSubtotalMap);
		when(modelService.getAttributeValue(hybOrderModel, "retailersSubtotal")).thenReturn(botSubtotalMap);

	}

	@Test
	public void testPopulate_WhenOneEntryForRetailer1_IsCancelled()
	{
		miraklCreateOrder.setCommercialId(retailer1);
		extMiraklCreateOrderPopulator.populate(orderModel, miraklCreateOrder);
		Assert.assertEquals(4, offer.getPrice().doubleValue(), 0d);
	}

	@Test
	public void testPopulate_WhenOneEntryForRetailer2_HasZeroQuantity()
	{
		miraklCreateOrder.setCommercialId(retailer2);
		extMiraklCreateOrderPopulator.populate(orderModel, miraklCreateOrder);
		Assert.assertEquals(0, offer.getPrice().doubleValue(), 0d);

	}
}
