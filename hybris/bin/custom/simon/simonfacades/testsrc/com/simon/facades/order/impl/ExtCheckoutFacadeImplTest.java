package com.simon.facades.order.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;

import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.core.service.mirakl.MarketPlaceService;
import com.simon.core.services.ExtCartService;
import com.simon.core.services.ExtOrderService;
import com.simon.core.services.ShopGatewayService;
import com.simon.core.services.VariantAttributeMappingService;
import com.simon.core.services.order.ExtCommerceCheckoutService;
import com.simon.core.strategies.impl.ExtCheckoutCustomerStrategy;
import com.simon.facades.checkout.data.ShippingPriceCall;
import com.simon.facades.checkout.data.StateData;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.facades.user.ExtUserFacade;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationService;
import com.simon.integration.purchase.services.OrderPurchaseIntegrationService;
import com.simon.integration.services.CartCheckOutIntegrationService;



/**
 * JUnit test suite for {@link ExtCheckoutFacadeImplTest}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCheckoutFacadeImplTest
{
	@InjectMocks
	@Spy
	private ExtCheckoutFacadeImpl extCheckoutFacade;

	@Mock
	private Model model;
	@Mock
	private CustomerModel checkoutCustomerModel;
	@Mock
	private UserModel checkoutUserModel;
	@Mock
	private CreditCardPaymentInfoModel ccPaymentInfoModel;
	@Mock
	private OrderModel orderModel;
	@Mock
	AdditionalCartInfoModel additionalCartInfoModel;

	@Mock
	private CartModel cartForCreateCallBack;
	@Mock
	private AdditionalCartInfoModel additionalCartInfoForCreateCallBack;
	@Mock
	private RetailersInfoModel retailerInfo1ForCreateCartCallBack;
	@Mock
	private AdditionalProductDetailsModel additionalProduct1ForCreateCartCallBack;
	@Mock
	private AbstractOrderEntryModel entry1ForCreateCallBack;
	@Mock
	private ProductModel product1ForCreateCallBac;

	@Mock
	private RetailersInfoModel retailerInfo2ForCreateCartCallBack;
	@Mock
	private AdditionalProductDetailsModel additionalProduct2ForCreateCartCallBack;
	@Mock
	private AdditionalProductDetailsModel additionalProduct3ForCreateCartCallBack;
	@Mock
	private AbstractOrderEntryModel entry2ForCreateCallBack;
	@Mock
	private ProductModel product2ForCreateCallBac;

	@Mock
	private AbstractOrderEntryModel entry3ForCreateCallBack;
	@Mock
	private ProductModel product3ForCreateCallBac;

	@Mock
	private ExtUserFacade userFacade;

	@Mock
	private CartData checkoutCartData;
	@Mock
	private StateData stateData;
	@Mock
	private CCPaymentInfoData paymentInfoData;
	@Mock
	private OrderConfirmCallBackDTO orderConfirmCallBackDTO;

	@Mock
	private CheckoutCustomerStrategy checkoutCustomerStrategy;
	@Mock
	private ExtCheckoutCustomerStrategy extCheckoutCustomerStrategy;

	@Mock
	private ExtCartService cartService;
	@Mock
	private UserService userService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private ExtCommerceCheckoutService commerceCheckoutService;
	@Mock
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;
	@Mock
	private ExtCustomerAccountService customerAccountService;
	@Mock
	private CardPaymentIntegrationService cardPaymentIntegrationService;
	@Mock
	private ModelService modelService;
	@Mock
	private ExtOrderService extOrderService;
	@Mock
	private OrderPurchaseIntegrationService orderPurchaseIntegrationService;
	@Mock
	private PurchaseConfirmResponseDTO purchaseConfirmResponseDTO;
	@Mock
	private ExtCustomMessageUtils extCustomMessageUtils;

	@Mock
	private Configuration configuration;
	@Mock
	private MarketPlaceService marketPlaceService;
	@Mock
	private VariantAttributeMappingService variantAttributeMappingService;
	@Mock
	private ShopGatewayService shopGatewayService;
	@Mock
	private OrderHistoryData orderHistoryData;
	@Mock
	private PriceData totalPrice;
	@Mock
	private OrderStatus orderStatus;

	@Mock
	private PriceDataFactory priceDataFactory;


	@Mock
	private Converter<OrderModel, OrderHistoryData> orderHistoryConverter;

	@Mock
	private AddressModel address;

	@Mock
	private RetailersInfoModel retailersInfo;
	@Mock
	private AdditionalCartInfoModel additionalCartInfo;

	@Mock
	private Converter<CreditCardPaymentInfoModel, CCPaymentInfoData> creditCardPaymentInfoConverter;
	@Mock
	private Converter<OrderConfirmCallBackDTO, AdditionalCartInfoModel> confirmUpdateReverseConverter;

	@Mock
	private CartModel cart;
	@Mock
	private Configuration config;




	private List<RetailersInfoModel> retailersInfoModelList;
	private List<ShippingDetailsModel> shippingDetails;
	private ShippingDetailsModel shippingDetailsModel;
	private RetailersInfoModel retailersInfoModel;
	private CartModel cartModel;
	private CurrencyModel currencyModel;
	private CountryModel countryModel;
	private AddressModel addressModel;
	private CustomerModel customerModel;
	private UserModel userModel;

	private AddressData addressData;
	private CCPaymentInfoData extPaymentInfoData;
	private CartData cartData;

	/**
	 * set up
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(config);
		when(config.getString("checkout.error.cart.notestimated")).thenReturn("notEstimated");
		when(orderHistoryConverter.convert(orderModel)).thenReturn(orderHistoryData);
	}

	/**
	 * test for importing common data
	 *
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testCreatePaymentInfoSubscription() throws UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		setupAddressData();
		setupPaymentInfo();
		setupCartModel();
		extCheckoutFacade.setModelService(modelService);
		doReturn(true).when(extCheckoutFacade).checkIfCurrentUserIsTheCartUser();
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartService.hasSessionCart()).thenReturn(true);
		when(checkoutCustomerStrategy.isAnonymousCheckout()).thenReturn(true);
		when(userFacade.addUserPaymentAddress(paymentInfoData, ccPaymentInfoModel)).thenReturn(true);
		doReturn(ccPaymentInfoModel).when(customerAccountService).createPaymentInfoSubscription(extPaymentInfoData);
		doNothing().when(modelService).save(cartModel);
		doReturn(creditCardPaymentInfoConverter).when(extCheckoutFacade).getCreditCardPaymentInfoConverter();
		doReturn(paymentInfoData).when(creditCardPaymentInfoConverter).convert(ccPaymentInfoModel);
		extCheckoutFacade.createPaymentInfoSubscription(extPaymentInfoData);
		extCheckoutFacade.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
	}

	/**
	 * Test case for method validateCartForRetailerShippingMethods
	 */
	@Test
	public void testValidateCartForRetailerShippingMethods()
	{
		populateAdditionalCartInfo();
		retailersInfoModel.setShippingDetails(shippingDetails);
		assertTrue(extCheckoutFacade.validateCartForRetailerShippingMethods(additionalCartInfoModel));
	}

	/**
	 * Test case for method validateCartForRetailerShippingMethods
	 */
	@Test
	public void testValidateCartForRetailerShippingMethodsForEmptyShippingDetails()
	{
		populateAdditionalCartInfo();
		assertFalse(extCheckoutFacade.validateCartForRetailerShippingMethods(additionalCartInfoModel));
	}

	/**
	 *
	 */
	@Test
	public void testGetCheckoutDataForReview()
	{
		setupCommonDataForGetCheckoutDataForReview();

		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		assertNotNull(extCheckoutFacade.getCheckoutDataForReview("CHEAPEST", null));
		assertEquals(ShippingPriceCall.CHEAPEST.toString(),
				extCheckoutFacade.getCheckoutDataForReview("CHEAPEST", null).getCartData().getShippingPriceCall());
	}

	/**
	 *
	 */
	@Test
	public void testGetCheckoutDataForReviewForLoggedInUser()
	{
		setupCommonDataForGetCheckoutDataForReview();

		customerModel.setType(CustomerType.REGISTERED);

		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		when(extCheckoutCustomerStrategy.isAnonymousCheckout()).thenReturn(false);

		when(getUserService().getCurrentUser()).thenReturn(userModel);

		when(userService.getCurrentUser()).thenReturn(userModel);

		assertNotNull(extCheckoutFacade.getCheckoutDataForReview("CHEAPEST", null));
		assertEquals(ShippingPriceCall.CHEAPEST.toString(),
				extCheckoutFacade.getCheckoutDataForReview("CHEAPEST", null).getCartData().getShippingPriceCall());
	}

	/**
	 *
	 */
	@Test
	public void testGetCheckoutDataForReviewWithError()
	{
		setupCommonDataForGetCheckoutDataForReview();

		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		when(extCheckoutCustomerStrategy.isAnonymousCheckout()).thenReturn(true);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("checkout.error.cart.notestimated")).thenReturn("Error Message");

		assertNotNull(extCheckoutFacade.getCheckoutDataForReview("Fastest", null));
		assertEquals("Error Message", extCheckoutFacade.getCheckoutDataForReview("Fastest", null).getErrorMessage());
	}

	/**
	 * @throws IOException
	 * @throws CartCheckOutIntegrationException
	 * @throws CalculationException
	 *
	 */
	@Test
	public void testEstimateCartForCheapest() throws CartCheckOutIntegrationException, IOException, CalculationException
	{
		setupCommonDataForEstimateCart();
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(additionalCartInfo).when(cart).getAdditionalCartInfo();

		final List<RetailersInfoModel> retailersInfoModelList = new ArrayList<>();
		retailersInfoModelList.add(retailersInfo);
		doReturn(retailersInfoModelList).when(additionalCartInfo).getRetailersInfo();
		assertTrue(extCheckoutFacade.estimateCart("Cheapest"));
	}

	/**
	 * @throws IOException
	 * @throws CartCheckOutIntegrationException
	 * @throws CalculationException
	 *
	 */
	@Test
	public void testEstimateCartForFastest() throws CartCheckOutIntegrationException, IOException, CalculationException
	{
		setupCommonDataForEstimateCart();
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(additionalCartInfo).when(cart).getAdditionalCartInfo();

		final List<RetailersInfoModel> retailersInfoModelList = new ArrayList<>();
		retailersInfoModelList.add(retailersInfo);
		doReturn(retailersInfoModelList).when(additionalCartInfo).getRetailersInfo();

		assertTrue(extCheckoutFacade.estimateCart("Fastest"));
	}

	/**
	 * @throws IOException
	 * @throws CartCheckOutIntegrationException
	 * @throws CalculationException
	 *
	 */
	@Test
	public void testEstimateCartForEmptyString() throws CartCheckOutIntegrationException, IOException, CalculationException
	{
		setupCommonDataForEstimateCart();
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(additionalCartInfo).when(cart).getAdditionalCartInfo();
		final List<RetailersInfoModel> retailersInfoModelList = new ArrayList<>();
		retailersInfoModelList.add(retailersInfo);
		doReturn(retailersInfoModelList).when(additionalCartInfo).getRetailersInfo();
		assertTrue(extCheckoutFacade.estimateCart(StringUtils.EMPTY));
	}

	/**
	 *
	 */
	@Test
	public void testSetDeliveryMethodPerRetailer()
	{
		populateAdditionalCartInfo();
		setupCartModel();
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		cartModel.setAdditionalCartInfo(additionalCartInfoModel);

		when(cartService.getSessionCart()).thenReturn(cartModel);

		extCheckoutFacade.setDeliveryMethodForRetailer("Retailer1", "Cheapest");
	}

	/**
	 *
	 */
	@Test
	public void testSetDeliveryMethodPerRetailerWithNoRetailer()
	{
		populateAdditionalCartInfo();
		setupCartModel();

		cartModel.setAdditionalCartInfo(additionalCartInfoModel);
		additionalCartInfoModel.setRetailersInfo(Collections.emptyList());
		when(cartService.getSessionCart()).thenReturn(cartModel);
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(additionalCartInfo).when(cart).getAdditionalCartInfo();
		extCheckoutFacade.setDeliveryMethodForRetailer("Retailer1", "Cheapest");
	}

	@Test
	public void testSetDeliveryMethodPerRetailerWith1Retailer()
	{
		populateAdditionalCartInfo();
		setupCartModel();

		cartModel.setAdditionalCartInfo(additionalCartInfoModel);
		final List<RetailersInfoModel> retailerInfo = new ArrayList<>();
		retailerInfo.add(retailersInfo);
		additionalCartInfoModel.setRetailersInfo(retailerInfo);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(retailersInfo.getRetailerId()).thenReturn("Retailer1");
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(additionalCartInfo).when(cart).getAdditionalCartInfo();
		extCheckoutFacade.setDeliveryMethodForRetailer("Retailer1", "Cheapest");
	}

	private void setupAddressData()
	{
		final CountryData countryData = new CountryData();
		countryData.setIsocode("US");

		final RegionData regionData = new RegionData();
		regionData.setCountryIso("US");
		regionData.setIsocode("US-NY");

		addressData = new AddressData();
		addressData.setFirstName("Sachin");
		addressData.setLastName("Shrivastava");
		addressData.setCountry(countryData);
		addressData.setLine1("Tower C, Plot No. -7");
		addressData.setLine2("Oxygen Business Park, SEZ, Sec-144");
		addressData.setTown("Noida");
		addressData.setRegion(regionData);
		addressData.setPostalCode("201301");
		addressData.setPhone("7042922799");
		addressData.setTitle("Mr");
		addressData.setEmail("test@test.com");
	}

	private void setupPaymentInfo()
	{
		extPaymentInfoData = new CCPaymentInfoData();
		extPaymentInfoData.setPaymentToken("4BaK5ytLJyqL92EdCqfp4EY5iNt");
		extPaymentInfoData.setCardNumber("************1111");
		extPaymentInfoData.setLastFourDigits("1111");
		extPaymentInfoData.setCardType("visa");
		extPaymentInfoData.setExpiryMonth("09");
		extPaymentInfoData.setExpiryYear("2018");
		extPaymentInfoData.setSaveInAccount(true);
		extPaymentInfoData.setBillingAddress(addressData);
	}

	private void setupCartModel()
	{
		countryModel = new CountryModel();
		countryModel.setIsocode("us");

		addressModel = new AddressModel();
		addressModel.setCountry(countryModel);

		currencyModel = new CurrencyModel();
		currencyModel.setDigits(Integer.valueOf(4));
		currencyModel.setIsocode("us");

		cartModel = new CartModel();
		cartModel.setCode("1000");
		cartModel.setCurrency(currencyModel);
		cartModel.setPaymentAddress(addressModel);
		//cartModel.setPaymentInfo(paymentInfoModel);
		cartModel.setTotalPrice(Double.valueOf(150d));
		cartModel.setGuid("guid");
		cartModel.setSaveTime(new Date());
		cartModel.setNet(Boolean.FALSE);
	}

	private void populateAdditionalCartInfo()
	{
		shippingDetailsModel = new ShippingDetailsModel();
		shippingDetails = new ArrayList<>();
		shippingDetails.add(shippingDetailsModel);

		retailersInfoModel = new RetailersInfoModel();
		retailersInfoModel.setRetailerId("Retailer1");
		retailersInfoModelList = new ArrayList<RetailersInfoModel>();
		retailersInfoModelList.add(retailersInfoModel);

		additionalCartInfoModel = new AdditionalCartInfoModel();
		additionalCartInfoModel.setRetailersInfo(retailersInfoModelList);

	}

	private void setupCommonDataForGetCheckoutDataForReview()
	{
		cartData = new CartData();
		cartData.setShippingPriceCall(ShippingPriceCall.CHEAPEST.toString());

		doReturn(cartData).when(extCheckoutFacade).getCheckoutCart();

		customerModel = new CustomerModel();
		customerModel.setType(CustomerType.GUEST);

		userModel = new UserModel();
		userModel.setName("Regsitered User");
	}

	private void setupCommonDataForEstimateCart()
	{
		populateAdditionalCartInfo();
		setupCartModel();

		cartModel.setAdditionalCartInfo(additionalCartInfoModel);

		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("simon.bot.first.shipping.price.call")).thenReturn("cheapest");
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}


	/**
	 * No return if successfully verified and throws CartCheckOutIntegrationException if got any error codes
	 *
	 * @throws CartCheckOutIntegrationException
	 *
	 */

	@Test(expected = CartCheckOutIntegrationException.class)
	public void testVerifyToken_whenCardVerificationResponseDTOIsFalse() throws CartCheckOutIntegrationException
	{
		final CardVerificationResponseDTO cardVerificationResponseDTO = new CardVerificationResponseDTO();
		cardVerificationResponseDTO.setErrorCode("false");
		when(cardPaymentIntegrationService.invokeCardVerification("Test", true, true)).thenReturn(cardVerificationResponseDTO);
		extCheckoutFacade.verifyToken("Test", true, true);
	}

	private void testDeliveryCommon() throws IOException
	{
		extCheckoutFacade.setModelService(modelService);
		when(extOrderService.getOrderByOrderId("Test")).thenReturn(orderModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("mock.checkout.cart.confirmation.callback.response")).thenReturn("TRUE");
		when(configuration.getString("esb.order.purchase.confirm.enable.stub.flag")).thenReturn("TRUE");
		when(configuration.getString("order.update.confirm.service.stub")).thenReturn("stubFileName");
		when(configuration.getString("purchase.update.confirm.service.stub")).thenReturn("stubFileName");
		when(configuration.getString("esb.order.delivery.api.enable.stub.flag")).thenReturn("TRUE");
		when(cartCheckOutIntegrationService.invokeOrderUpdateConfirmMockRequest("stubFileName"))
				.thenReturn(orderConfirmCallBackDTO);
		doNothing().when(modelService).save(Matchers.any(AdditionalCartInfoModel.class));
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(confirmUpdateReverseConverter.convert(Mockito.any(), Mockito.any())).thenReturn(additionalCartInfoModel);
	}

	/**
	 * it is successfully with order rejected status
	 *
	 * @throws CartCheckOutIntegrationException
	 * @throws IOException
	 */
	@Test
	public void testDelivery_whenMockIsTrueRejected() throws CartCheckOutIntegrationException, IOException
	{
		testDeliveryCommon();
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ORDER_DELIVERY_STUB_FLAG)).thenReturn(Boolean.TRUE.toString());
		extCheckoutFacade.deliver("Test", "Test");
		verify(modelService, Mockito.times(1)).save(orderModel);

	}

	/**
	 * it is successfully with order accepted status
	 *
	 * @throws CartCheckOutIntegrationException
	 * @throws IOException
	 */
	@Test
	public void testDelivery_whenMockIsTrueAccepted() throws CartCheckOutIntegrationException, IOException
	{
		testDeliveryCommon();
		when(extOrderService.checkAcceptOrRejectTheOrder(orderModel)).thenReturn(true);



		when(orderPurchaseIntegrationService.invokePurchaseConfirmRequest(orderModel.getCode()))
				.thenReturn(purchaseConfirmResponseDTO);

		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ORDER_DELIVERY_STUB_FLAG)).thenReturn(Boolean.TRUE.toString());
		when(configuration.getString(SimonIntegrationConstants.PURCHASE_CONFIRM_STUB)).thenReturn(Boolean.TRUE.toString());

		extCheckoutFacade.deliver("Test", "Test");
		verify(modelService, Mockito.times(1)).save(orderModel);

	}

	/**
	 * @throws CartCheckOutIntegrationException
	 * @throws IOException
	 */
	@Test
	public void testDelivery_whenMockIsFalse() throws CartCheckOutIntegrationException, IOException
	{
		extCheckoutFacade.setModelService(modelService);
		when(extOrderService.getOrderByOrderId("Test")).thenReturn(orderModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ORDER_DELIVERY_STUB_FLAG)).thenReturn("FALSE");
		final List<VariantAttributeMappingModel> variantAttributeMappingList = new ArrayList<>();
		final VariantAttributeMappingModel variantAttributeMappingModel = mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributeMappingModel);
		doNothing().when(cardPaymentIntegrationService).invokeDeliverOrder(orderModel, "Test", variantAttributeMappingList);
		doNothing().when(modelService).save(Matchers.any(AdditionalCartInfoModel.class));
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(confirmUpdateReverseConverter.convert(Mockito.any(), Mockito.any())).thenReturn(additionalCartInfoModel);
		final ShopGatewayModel shopGatewayModel = mock(ShopGatewayModel.class);
		when(shopGatewayService.getShopGatewayForCode(any())).thenReturn(shopGatewayModel);
		when(variantAttributeMappingService.getVariantAttributeMapping(shopGatewayModel)).thenReturn(variantAttributeMappingList);
		extCheckoutFacade.deliver("Test", "Test");
		verify(cardPaymentIntegrationService, Mockito.times(1)).invokeDeliverOrder(orderModel, "Test", variantAttributeMappingList);

	}

	@Mock
	private CustomerType customerType;

	/**
	 *
	 */
	@Test
	public void textCheckoutData()
	{
		Mockito.doReturn(new HashMap<String, String>()).when(extCustomMessageUtils).setCheckoutShippingMessages();
		final List<StateData> stateDataList = new ArrayList<>();
		final List<String> excludedStateList = new ArrayList<>();
		excludedStateList.add("excludedState");
		stateDataList.add(stateData);
		Mockito.doReturn(stateDataList).when(extCheckoutFacade).getAllStates(model);
		Mockito.doReturn(excludedStateList).when(extCheckoutFacade).getExcludedStateList();
		extCheckoutFacade.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(checkoutCustomerModel);
		when(checkoutCustomerModel.getType()).thenReturn(CustomerType.GUEST);
		when(checkoutCustomerStrategy.isAnonymousCheckout()).thenReturn(Boolean.FALSE);
		when(userService.getCurrentUser()).thenReturn(checkoutUserModel);
		when(checkoutUserModel.getName()).thenReturn("userName");
		assertEquals(Boolean.FALSE, extCheckoutFacade.getCheckoutData(model, cartData).isPaymentcompleted());
	}

	/**
	 *
	 */
	@Test
	public void textCheckoutDataIsAnonymousCheckout()
	{
		Mockito.doReturn(new HashMap<String, String>()).when(extCustomMessageUtils).setCheckoutShippingMessages();
		final List<StateData> stateDataList = new ArrayList<>();
		final List<String> excludedStateList = new ArrayList<>();
		excludedStateList.add("excludedState");
		stateDataList.add(stateData);
		Mockito.doReturn(stateDataList).when(extCheckoutFacade).getAllStates(model);
		Mockito.doReturn(excludedStateList).when(extCheckoutFacade).getExcludedStateList();
		extCheckoutFacade.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(checkoutCustomerModel);
		when(checkoutCustomerModel.getType()).thenReturn(CustomerType.GUEST);
		when(checkoutCustomerStrategy.isAnonymousCheckout()).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(checkoutUserModel);
		when(checkoutUserModel.getName()).thenReturn("userName");
		assertEquals(Boolean.FALSE, extCheckoutFacade.getCheckoutData(model, cartData).isPaymentcompleted());
	}

	/**
	 *
	 */
	@Test
	public void textCheckoutDataIsRegisteredUser()
	{
		Mockito.doReturn(new HashMap<String, String>()).when(extCustomMessageUtils).setCheckoutShippingMessages();
		final List<StateData> stateDataList = new ArrayList<>();
		final List<String> excludedStateList = new ArrayList<>();
		excludedStateList.add("excludedState");
		stateDataList.add(stateData);
		Mockito.doReturn(stateDataList).when(extCheckoutFacade).getAllStates(model);
		Mockito.doReturn(excludedStateList).when(extCheckoutFacade).getExcludedStateList();
		extCheckoutFacade.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(checkoutCustomerModel);
		when(checkoutCustomerModel.getType()).thenReturn(CustomerType.REGISTERED);
		when(checkoutCustomerStrategy.isAnonymousCheckout()).thenReturn(Boolean.FALSE);
		when(userService.getCurrentUser()).thenReturn(checkoutUserModel);
		when(checkoutUserModel.getName()).thenReturn("userName");
		assertEquals(Boolean.FALSE, extCheckoutFacade.getCheckoutData(model, cartData).isPaymentcompleted());
	}

	/**
	 *
	 */
	@Test
	public void textCheckoutDataIsAnonymousCheckoutAndGust()
	{
		Mockito.doReturn(new HashMap<String, String>()).when(extCustomMessageUtils).setCheckoutShippingMessages();
		final List<StateData> stateDataList = new ArrayList<>();
		final List<String> excludedStateList = new ArrayList<>();
		excludedStateList.add("excludedState");
		stateDataList.add(stateData);
		Mockito.doReturn(stateDataList).when(extCheckoutFacade).getAllStates(model);
		Mockito.doReturn(excludedStateList).when(extCheckoutFacade).getExcludedStateList();
		extCheckoutFacade.setCheckoutCustomerStrategy(checkoutCustomerStrategy);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(checkoutCustomerModel);
		when(checkoutCustomerModel.getType()).thenReturn(CustomerType.GUEST);
		when(checkoutCustomerStrategy.isAnonymousCheckout()).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(checkoutUserModel);
		when(checkoutUserModel.getName()).thenReturn("userName");
		assertEquals(Boolean.FALSE, extCheckoutFacade.getCheckoutData(model, cartData).isPaymentcompleted());
	}

	/**
	 * @throws IOException
	 *
	 */
	@Test
	public void testSaveConfirmDeliveryCallBackResponse() throws IOException
	{
		populateAdditionalCartInfo();

		when(orderConfirmCallBackDTO.getOrderId()).thenReturn("1001");
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(orderModel);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(confirmUpdateReverseConverter.convert(Mockito.any(), Mockito.any())).thenReturn(additionalCartInfoModel);

		doNothing().when(marketPlaceService).acceptOrRejectOrder(orderModel, false);
		doNothing().when(modelService).save(orderModel);

		extCheckoutFacade.savePurchaseCallbackResponse(orderConfirmCallBackDTO);

		verify(modelService, Mockito.times(1)).save(orderModel);
	}

	/**
	 * @throws IOException
	 *
	 */
	@Test
	public void testSaveConfirmDeliveryCallBackResponse_WhenOederIsNull() throws IOException
	{
		populateAdditionalCartInfo();

		when(orderConfirmCallBackDTO.getOrderId()).thenReturn("1001");
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(null);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(confirmUpdateReverseConverter.convert(Mockito.any(), Mockito.any())).thenReturn(additionalCartInfoModel);

		doNothing().when(marketPlaceService).acceptOrRejectOrder(orderModel, false);
		doNothing().when(modelService).save(orderModel);

		extCheckoutFacade.savePurchaseCallbackResponse(orderConfirmCallBackDTO);

		verify(orderConfirmCallBackDTO, Mockito.times(2)).getOrderId();
	}


	@Test
	public void testgetGuestOrderForTracking()
	{
		final Date date = mock(Date.class);
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(orderModel);
		when(orderModel.getDeliveryAddress()).thenReturn(address);
		when(address.getEmail()).thenReturn("test@mail.com");
		final AddressModel address = mock(AddressModel.class);
		when(orderModel.getDeliveryAddress()).thenReturn(address);
		when(orderModel.getDeliveryAddress().getEmail()).thenReturn("test1@mail.com");
		given(orderModel.getCode()).willReturn("1001");
		given(orderModel.getCode()).willReturn("code");
		given(orderModel.getDate()).willReturn(date);
		given(orderModel.getStatus()).willReturn(orderStatus);
		given(orderModel.getTotalPrice()).willReturn(Double.valueOf(123.0));
		doReturn(orderHistoryData).when(orderHistoryConverter).convert(orderModel);
		final OrderHistoryData result = extCheckoutFacade.getGuestOrderForTracking("1001", "test@mail.com");
		Assert.assertEquals(null, result);
	}

	/**
	 * this is test method for savePurchaseCallbackErrorResponse
	 *
	 */
	@Test
	public void testsavePurchaseCallbackErrorResponse()
	{
		when(orderConfirmCallBackDTO.getId()).thenReturn("1001");
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(orderModel);
		when(confirmUpdateReverseConverter.convert(orderConfirmCallBackDTO, additionalCartInfo)).thenReturn(additionalCartInfo);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfo);

		when(orderConfirmCallBackDTO.getErrorCode()).thenReturn("ErrorCode");
		when(orderConfirmCallBackDTO.getErrorMessage()).thenReturn("errorMsg");


		doNothing().when(modelService).save(orderModel);
		extCheckoutFacade.savePurchaseCallbackErrorResponse(orderConfirmCallBackDTO);
		verify(modelService, Mockito.times(1)).save(orderModel);
	}

	/**
	 * this is test method for savePurchaseConfirmCallbackErrorResponse.
	 *
	 */
	@Test
	public void testsavePurchaseConfirmCallbackErrorResponse()
	{
		when(orderConfirmCallBackDTO.getId()).thenReturn("1001");
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(orderModel);
		doNothing().when(modelService).save(orderModel);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfo);
		when(confirmUpdateReverseConverter.convert(orderConfirmCallBackDTO, additionalCartInfo)).thenReturn(additionalCartInfo);
		extCheckoutFacade.savePurchaseConfirmCallbackErrorResponse(orderConfirmCallBackDTO);
		verify(modelService, Mockito.times(1)).save(orderModel);
	}

	/**
	 * this is test method for savePurchaseConfirmCallbackErrorResponse with order model does not exist.
	 *
	 */
	@Test
	public void testsavePurchaseConfirmCallbackErrorResponseNullOrder()
	{
		when(orderConfirmCallBackDTO.getId()).thenReturn("1001");
		when(extOrderService.getOrderByOrderId("1001")).thenReturn(null);
		extCheckoutFacade.savePurchaseConfirmCallbackErrorResponse(orderConfirmCallBackDTO);
		verify(modelService, Mockito.times(0)).save(orderModel);
	}

	/**
	 *
	 */
	@Test
	public void testSetDeliveryAddressCreateDeliveryAddressModel()
	{
		setupAddressData();
		final CommerceCheckoutParameter parameter = mock(CommerceCheckoutParameter.class);
		doReturn(cart).when(extCheckoutFacade).getCartModel();
		doReturn(address).when(extCheckoutFacade).createUpdatedDeliveryAddressModel(addressData, cart);
		when(extCheckoutFacade.createCommerceCheckoutParameterFromCart(cart, true)).thenReturn(parameter);
		extCheckoutFacade.setDeliveryAddress(addressData);
		verify(commerceCheckoutService).setDeliveryAddress(any(CommerceCheckoutParameter.class));
	}

}
