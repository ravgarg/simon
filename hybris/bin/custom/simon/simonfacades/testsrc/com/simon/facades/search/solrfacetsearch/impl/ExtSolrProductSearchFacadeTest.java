package com.simon.facades.search.solrfacetsearch.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.FetchProfile.Item;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.customer.ExtCustomerFacade;

import junit.framework.Assert;


@UnitTest
public class ExtSolrProductSearchFacadeTest
{
	@InjectMocks
	@Spy
	private ExtSolrProductSearchFacade<ProductData> facade;
	/** The Constant BASE_PRODUCT_RETAILER_NAME. */
	private static final String BASE_PRODUCT_RETAILER_NAME = "baseProductRetailerName";
	private static final String BASE_PRODUCT_DESIGNER_NAME = "baseProductDesignerName";

	@Mock
	private ExtCustomerFacade extCustomerFacade;
	@Mock
	private SearchStateData searchState;
	@Mock
	private PageableData pageableData;
	@Mock
	private ProductSearchPageData<SearchStateData, Item> productSearchPageData;
	@Mock
	private ProductCategorySearchPageData<SearchStateData, Item, CategoryData> catProductSearchData;
	@Mock
	private CustomerData currentCustomerData;
	@Mock
	private List<FacetData<SearchStateData>> facets;
	@Mock
	private ProductSearchPageData searchPageData;
	@Mock
	private FacetData<SearchStateData> facetData;
	@Mock
	private FacetValueData<SearchStateData> valueData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTextSearchForNoMyStoresAndDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		doReturn(productSearchPageData).when(facade).callSuper(searchState, pageableData);
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		Assert.assertTrue(CollectionUtils.isEmpty(facade.textSearch(searchState, pageableData).getResults()));
	}

	@Test
	public void testTextSearchForMyStoresAndDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("abc");
		doReturn(productSearchPageData).when(facade).callSuper(searchState, pageableData);
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		when(searchPageData.getFacets()).thenReturn(facets);
		Assert.assertTrue(CollectionUtils.isEmpty(facade.textSearch(searchState, pageableData).getResults()));
	}

	@Test
	public void testTextSearchForMyDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("testDesigner");
		final List<FacetValueData<SearchStateData>> values = new ArrayList<>();
		final FacetData<SearchStateData> facetData = new FacetData<>();
		final FacetValueData<SearchStateData> valueData = new FacetValueData<>();
		values.add(valueData);
		doReturn(productSearchPageData).when(facade).callSuper(searchState, pageableData);
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		facets.add(facetData);

		Mockito.when(searchPageData.getFacets()).thenReturn(facets);
		facetData.setCode("baseProductDesignerName");
		facetData.setValues(values);
		valueData.setName("testDesigner");
		Assert.assertTrue(CollectionUtils.isEmpty(facade.textSearch(searchState, pageableData).getResults()));
	}

	@Test
	public void testTextSearchForMyStores()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("testDesigner");
		final List<FacetValueData<SearchStateData>> values = new ArrayList<>();
		final FacetData<SearchStateData> facetData = new FacetData<>();
		final FacetValueData<SearchStateData> valueData = new FacetValueData<>();
		values.add(valueData);
		doReturn(productSearchPageData).when(facade).callSuper(searchState, pageableData);
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		facets.add(facetData);

		Mockito.when(searchPageData.getFacets()).thenReturn(facets);
		facetData.setCode("baseProductRetailerName");
		facetData.setValues(values);
		valueData.setName("testDesigner");
		Assert.assertTrue(CollectionUtils.isEmpty(facade.textSearch(searchState, pageableData).getResults()));
	}

	@Test
	public void testCategorySearchForNoMyStoresAndDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		doReturn(catProductSearchData).when(facade).callCategorySearch("m300000");
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		Assert.assertTrue(CollectionUtils.isEmpty(facade.categorySearch("m300000").getResults()));
	}

	@Test
	public void testCategorySearchForMyStoresAndDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("abc");
		doReturn(catProductSearchData).when(facade).callCategorySearch("m300000");
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		when(searchPageData.getFacets()).thenReturn(facets);
		Assert.assertTrue(CollectionUtils.isEmpty(facade.categorySearch("m300000").getResults()));
	}

	@Test
	public void testCategorySearchForMyDesigners()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("testDesigner");
		final List<FacetValueData<SearchStateData>> values = new ArrayList<>();
		final FacetData<SearchStateData> facetData = new FacetData<>();
		final FacetValueData<SearchStateData> valueData = new FacetValueData<>();
		values.add(valueData);
		doReturn(catProductSearchData).when(facade).callCategorySearch("m300000");
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		facets.add(facetData);

		Mockito.when(searchPageData.getFacets()).thenReturn(facets);
		facetData.setCode("baseProductDesignerName");
		facetData.setValues(values);
		valueData.setName("testDesigner");
		Assert.assertTrue(CollectionUtils.isEmpty(facade.categorySearch("m300000").getResults()));
	}

	@Test
	public void testCategorySearchForMyStores()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("testDesigner");
		final List<FacetValueData<SearchStateData>> values = new ArrayList<>();
		final FacetData<SearchStateData> facetData = new FacetData<>();
		final FacetValueData<SearchStateData> valueData = new FacetValueData<>();
		values.add(valueData);
		doReturn(catProductSearchData).when(facade).callCategorySearch("m300000");
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);
		facets.add(facetData);

		Mockito.when(searchPageData.getFacets()).thenReturn(facets);
		facetData.setCode("baseProductRetailerName");
		facetData.setValues(values);
		valueData.setName("testDesigner");
		Assert.assertTrue(CollectionUtils.isEmpty(facade.categorySearch("m300000").getResults()));
	}

	@Test
	public void testoverLodedCategorySearchForMyStores()
	{
		final Set<StoreData> store = new HashSet<>();
		final Set<DesignerData> designerSet = new HashSet<>();
		final List<FacetData<SearchStateData>> facets = new ArrayList<>();
		final StoreData storeData = mock(StoreData.class);
		store.add(storeData);
		final DesignerData designerData = mock(DesignerData.class);
		designerSet.add(designerData);
		final List<String> myDesignerList = new ArrayList<>();
		myDesignerList.add("testDesigner");
		final List<FacetValueData<SearchStateData>> values = new ArrayList<>();
		values.add(valueData);
		facets.add(facetData);
		when(facetData.getCode()).thenReturn("baseProductRetailerName");
		when(facetData.getName()).thenReturn("testDesigner");
		when(facetData.getValues()).thenReturn(values);
		when(catProductSearchData.getFacets()).thenReturn(facets);

		doReturn(catProductSearchData).when(facade).callCategorySearch("m300000", searchState, pageableData);
		when(extCustomerFacade.getCurrentCustomer()).thenReturn(currentCustomerData);
		when(currentCustomerData.getMyStores()).thenReturn(store);
		when(currentCustomerData.getMyDesigners()).thenReturn(designerSet);

		Mockito.when(searchPageData.getFacets()).thenReturn(facets);
		Assert.assertTrue(CollectionUtils.isEmpty(facade.categorySearch("m300000", searchState, pageableData).getResults()));
	}


}
