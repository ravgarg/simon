package com.simon.facades.cart.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.commerceservice.order.impl.ExtCommerceCartServiceImpl;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.services.ExtCartService;
import com.simon.facades.cart.ExtCartFacade;
import com.simon.facades.cart.data.CartGlobalFlashMsgData;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.services.CartCheckOutIntegrationService;


/**
 * Junit Test case to test ExtCartFacadeImplTest
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCartFacadeImplTest
{


	@InjectMocks
	@Spy
	private ExtCartFacadeImpl extCartFacadeImpl;

	@Mock
	private CartModificationData cartModificationData;
	@Mock
	private CartService cartService;
	@Mock
	CartModel cartModel;
	@Mock
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;
	@Mock
	private CommerceCartParameter commerceCartParameter;

	@Mock
	private CommerceCartModification commerceCartModification;
	@Mock
	private ExtCommerceCartServiceImpl extCommerceCartService;

	@Mock
	private List<CartModificationData> source;
	@Mock
	private CartData target;

	@Mock
	private CartGlobalFlashMsgData cartGlobalFlashMsgData;

	@Mock
	private Iterator<CartModificationData> iterator;

	@Mock
	private Converter<CartModificationData, CartData> extCartModificationCartConverter;
	@Mock
	private OrderEntryData orderEntryData;

	@Mock
	private ExtCustomMessageUtils extCustomMessageUtils;
	@Mock
	private Converter<CartModel, CartData> extMiniCartConverter;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;

	@Mock
	private CreateCartConfirmationDTO createCartConfirmationDTO;

	@Mock
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;
	@Mock
	private AdditionalCartInfoModel additionalCartInfoModel;
	@Mock
	private ModelService modelService;
	@Mock
	private Converter<CreateCartConfirmationDTO, AdditionalCartInfoModel> additionalCartInfoReverseConverter;
	@Mock
	private ObjectMapper objectMapper;
	@Mock
	private ExtCartService extCartService;
	@Mock
	private ExtCartFacade cartFacade;


	@Before
	public void setup()
	{
		when(extCartService.getSessionCart()).thenReturn(cartModel);
	}

	@Test
	public void testGetChangeEntriesInCart() throws CommerceCartModificationException
	{

		final List<CartModificationData> cartModificationDataList = new ArrayList<>();
		cartModificationDataList.add(cartModificationData);
		doReturn(cartModificationDataList).when(extCartFacadeImpl).validateCartEntries();
		assertEquals(1, extCartFacadeImpl.getChangeEntriesInCart().size());
	}

	@Test
	public void testGetChangeEntriesInCartWhenCommerceCartModificationException() throws CommerceCartModificationException
	{

		final List<CartModificationData> cartModificationDataList = new ArrayList<>();
		cartModificationDataList.add(cartModificationData);
		doThrow(new CommerceCartModificationException("throw new CommerceCartModificationException")).when(extCartFacadeImpl)
				.validateCartEntries();
		assertEquals(0, extCartFacadeImpl.getChangeEntriesInCart().size());


	}



	@Test
	public void validateCartEntries() throws CommerceCartModificationException
	{

		doReturn(false).when(extCartFacadeImpl).hasExtSessionCart();
		assertEquals(0, extCartFacadeImpl.validateCartEntries().size());
	}



	@Test
	public void populateCartDataFromModificationGlobalFlashMsgIsNull()
	{
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(target.getGlobalFlashMsg()).thenReturn(null);
		when(source.isEmpty()).thenReturn(true);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(null, target.getGlobalFlashMsg());
	}



	@Test
	public void populateCartDataFromModificationSourceIsEmpty()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(true);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(1, target.getGlobalFlashMsg().size());
	}



	@Test
	public void populateCartDataFromModificationSourceIsNotEmpty()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(false);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.NO_STOCK);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(1, target.getGlobalFlashMsg().size());
	}



	@Test
	public void populateCartDataFromModificationCartModificationStatusNO_STOCK()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.NO_STOCK);
		doReturn(cartModel).when(extCartModificationCartConverter).convert(cartModificationData, target);
		doReturn("basket.page.message.update.reducedNumberOfItemsAdded.noStock").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.NOSTOCK);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(2, target.getGlobalFlashMsg().size());
	}

	@Test
	public void populateCartDataFromModificationCartModificationStatus_NO_STOCK()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.NO_STOCK);
		doReturn(cartModel).when(extCartModificationCartConverter).convert(cartModificationData, target);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		doReturn("basket.page.message.update.reducedNumberOfItemsAdded.noStock").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.NOSTOCK);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(2, target.getGlobalFlashMsg().size());
	}

	@Test
	public void populateCartDataFromModificationCartModificationStatusLOW_STOCK()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.LOW_STOCK);
		doReturn(cartModel).when(extCartModificationCartConverter).convert(cartModificationData, target);
		doReturn("basket.page.message.update.reducedNumberOfItemsAdded.noStock").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.LOWSTOCK);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(2, target.getGlobalFlashMsg().size());
	}

	@Test
	public void populateCartDataFromModificationCartModificationStatusSUCCESS()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.SUCCESS);
		doReturn(cartModel).when(extCartModificationCartConverter).convert(cartModificationData, target);
		doReturn("basket.page.message.update.reducedNumberOfItemsAdded.noStock").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.LOWSTOCK);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(1, target.getGlobalFlashMsg().size());
	}

	@Mock
	private ProductData productData;

	@Test
	public void addFlashMessage()
	{
		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(cartModificationData.getQuantity()).thenReturn(0L);
		doReturn("{1} has been removed from this order.").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.REMOVE_SUCCESSFULLY);
		when(cartModificationData.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(productData);
		when(productData.getName()).thenReturn("ProductName");
		extCartFacadeImpl.addFlashMessage(target, 0L, cartModificationData);
		assertEquals(2, target.getGlobalFlashMsg().size());
	}

	@Test
	public void addFlashMessageFlashMessagesIsNull()
	{
		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(null);
		when(cartModificationData.getQuantity()).thenReturn(0L);
		doReturn("{1} has been removed from this order.").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.REMOVE_SUCCESSFULLY);
		when(cartModificationData.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(productData);
		when(productData.getName()).thenReturn("ProductName");
		extCartFacadeImpl.addFlashMessage(target, 0L, cartModificationData);
		assertEquals(null, target.getGlobalFlashMsg());
	}

	@Test
	public void addFlashMessageQuantityGt0()
	{
		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(null);
		when(cartModificationData.getQuantity()).thenReturn(1L);
		doReturn("{1} has been removed from this order.").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.UPDATE_SUCCESSFULLY);
		when(cartModificationData.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(productData);
		when(productData.getName()).thenReturn("ProductName");
		extCartFacadeImpl.addFlashMessage(target, 1L, cartModificationData);
		assertEquals(null, target.getGlobalFlashMsg());
	}

	@Test
	public void addFlashMessageCartQuantityGt0()
	{
		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(null);
		when(cartModificationData.getQuantity()).thenReturn(1L);
		doReturn("{1} has been removed from this order.").when(extCartFacadeImpl).getLocalizedString(SimonCoreConstants.LOWSTOCK);
		when(cartModificationData.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(productData);
		when(productData.getName()).thenReturn("ProductName");
		extCartFacadeImpl.addFlashMessage(target, 2L, cartModificationData);
		assertEquals(null, target.getGlobalFlashMsg());
	}

	@Test
	public void addFlashMessageCartQuantityEq0()
	{
		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(null);
		when(cartModificationData.getQuantity()).thenReturn(0L);
		doReturn("{1} has been removed from this order.").when(extCartFacadeImpl).getLocalizedString(SimonCoreConstants.NOSTOCK);
		when(cartModificationData.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(productData);
		when(productData.getName()).thenReturn("ProductName");
		extCartFacadeImpl.addFlashMessage(target, 2L, cartModificationData);
		assertEquals(null, target.getGlobalFlashMsg());
	}


	@Test
	public void getExtMiniBag() throws CommerceCartModificationException
	{
		doReturn(true).when(extCartFacadeImpl).hasExtSessionCart();
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(extMiniCartConverter.convert(cartModel)).thenReturn(target);
		when(target.getTotalUnitCount()).thenReturn(1);
		final Map<String, String> miniCartMessages = new HashMap<>();
		miniCartMessages.put("key", "value");
		when(extCustomMessageUtils.setMiniCartMessages()).thenReturn(miniCartMessages);
		final List<CartModificationData> cartModificationDataList = new ArrayList<>();
		cartModificationDataList.add(cartModificationData);
		doReturn(cartModificationDataList).when(extCartFacadeImpl).validateCartEntries();
		doNothing().when(cartFacade).populateCartDataFromModification(cartModificationDataList, target);
		extCartFacadeImpl.getMiniBag();
		assertEquals(1, target.getTotalUnitCount().intValue());

	}

	@Test
	public void getExtMiniBagUnitCountEq0()
	{
		doReturn(true).when(extCartFacadeImpl).hasExtSessionCart();
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(extMiniCartConverter.convert(cartModel)).thenReturn(target);
		when(target.getTotalUnitCount()).thenReturn(0);
		final Map<String, String> miniCartMessages = new HashMap<>();
		miniCartMessages.put("key", "value");
		when(extCustomMessageUtils.setEmptyMiniCartMessages()).thenReturn(miniCartMessages);
		extCartFacadeImpl.getMiniBag();
		assertEquals(0, target.getTotalUnitCount().intValue());

	}

	@Test
	public void createTwoTapCart() throws CartCheckOutIntegrationException
	{
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CREATE_CART_STUB_FLAG)).thenReturn("true");
		doNothing().when(extCartFacadeImpl).invokeCartConfirmationMockRequest(cartModel);
		extCartFacadeImpl.createTwoTapCart();
		verify(extCartService, times(1)).getSessionCart();
	}



	@Test
	public void createTwoTapCartMockResponseIsFalse() throws CartCheckOutIntegrationException, URISyntaxException
	{
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CREATE_CART_STUB_FLAG)).thenReturn("false");
		doNothing().when(cartCheckOutIntegrationService).invokeCreateCartRequest(cartModel);
		extCartFacadeImpl.createTwoTapCart();
		verify(extCartService, times(1)).getSessionCart();
	}

	@Test(expected = CartCheckOutIntegrationException.class)
	public void createTwoTapCartThrowsCartCheckOutIntegrationException()
			throws CartCheckOutIntegrationException, URISyntaxException
	{
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CREATE_CART_STUB_FLAG)).thenReturn("false");
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		doThrow(new CartCheckOutIntegrationException("CartCheckOutIntegrationException")).when(cartCheckOutIntegrationService)
				.invokeCreateCartRequest(cartModel);
		extCartFacadeImpl.createTwoTapCart();
	}


	@Test
	public void invokeCartConfirmationMockRequest() throws IOException
	{

		when(cartCheckOutIntegrationService.invokeCartConfirmationMockRequest()).thenReturn(createCartConfirmationDTO);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		doNothing().when(modelService).remove(additionalCartInfoModel);
		when(additionalCartInfoReverseConverter.convert(createCartConfirmationDTO)).thenReturn(additionalCartInfoModel);
		doNothing().when(modelService).saveAll(cartModel);
		extCartFacadeImpl.invokeCartConfirmationMockRequest(cartModel);
	}

	@Test
	public void invokeCartConfirmationMockRequestAdditionalCartInfoIsNull() throws IOException
	{

		when(cartCheckOutIntegrationService.invokeCartConfirmationMockRequest()).thenReturn(createCartConfirmationDTO);
		when(cartModel.getAdditionalCartInfo()).thenReturn(null);
		doNothing().when(modelService).remove(additionalCartInfoModel);
		when(additionalCartInfoReverseConverter.convert(createCartConfirmationDTO)).thenReturn(additionalCartInfoModel);
		doNothing().when(modelService).saveAll(cartModel);
		extCartFacadeImpl.invokeCartConfirmationMockRequest(cartModel);
		verify(cartModel, times(1)).getAdditionalCartInfo();
	}

	@Test
	public void invokeCartConfirmationMockRequestThrowsIOException() throws IOException
	{

		doThrow(new IOException("IOException")).when(cartCheckOutIntegrationService).invokeCartConfirmationMockRequest();
		extCartFacadeImpl.invokeCartConfirmationMockRequest(cartModel);
		verify(extCartFacadeImpl, times(1)).invokeCartConfirmationMockRequest(cartModel);
	}

	@Test
	public void saveCartCallBackResponse() throws IOException
	{
		doReturn(objectMapper).when(extCartFacadeImpl).getObjectMapper();
		when(objectMapper.readValue("true", CreateCartConfirmationDTO.class)).thenReturn(createCartConfirmationDTO);
		when(createCartConfirmationDTO.getCartId()).thenReturn("cartId");
		when(extCartService.getCartByCartId("cartId")).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(additionalCartInfoReverseConverter.convert(createCartConfirmationDTO)).thenReturn(additionalCartInfoModel);
		doNothing().when(modelService).remove(additionalCartInfoModel);
		extCartFacadeImpl.saveCartCallBackResponse(createCartConfirmationDTO);
		verify(extCartService, times(1)).getCartByCartId("cartId");
	}

	@Test
	public void saveCartCallBackResponseWhenAdditionalCartInfoIsNull() throws IOException
	{
		doReturn(objectMapper).when(extCartFacadeImpl).getObjectMapper();
		when(objectMapper.readValue("true", CreateCartConfirmationDTO.class)).thenReturn(createCartConfirmationDTO);
		when(createCartConfirmationDTO.getCartId()).thenReturn("cartId");
		when(extCartService.getCartByCartId("cartId")).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(null);
		when(additionalCartInfoReverseConverter.convert(createCartConfirmationDTO)).thenReturn(additionalCartInfoModel);
		doNothing().when(modelService).remove(additionalCartInfoModel);
		extCartFacadeImpl.saveCartCallBackResponse(createCartConfirmationDTO);
		verify(extCartService, times(1)).getCartByCartId("cartId");
	}

	@Test
	public void populateCartDataFromModificationCartModificationStatusUNAVAILABLE()
	{

		final List<CartGlobalFlashMsgData> flashMessages = new ArrayList<>();
		flashMessages.add(cartGlobalFlashMsgData);
		when(target.getGlobalFlashMsg()).thenReturn(flashMessages);
		when(source.isEmpty()).thenReturn(false);
		when(source.iterator()).thenReturn(iterator);
		when(iterator.hasNext()).thenReturn(true).thenReturn(false);
		when(cartService.getSessionCart()).thenReturn(cartModel);
		when(cartModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(iterator.next()).thenReturn(cartModificationData);
		when(cartModificationData.getStatusCode()).thenReturn(CommerceCartModificationStatus.UNAVAILABLE);
		doReturn(cartModel).when(extCartModificationCartConverter).convert(cartModificationData, target);
		doReturn("basket.page.message.retailer.now.inactive").when(extCartFacadeImpl)
				.getLocalizedString(SimonCoreConstants.UNAVAILABLE);
		extCartFacadeImpl.populateCartDataFromModification(source, target);
		assertEquals(2, target.getGlobalFlashMsg().size());
	}

	@Test
	public void getSessionCartWithEntryOrderingSessionCartTrue()
	{
		doReturn(true).when(extCartFacadeImpl).hasExtSessionCart();
		doReturn(target).when(extCartFacadeImpl).getExtSessionCart();
		assertNotNull(extCartFacadeImpl.getSessionCartWithEntryOrdering(true));
	}
}
