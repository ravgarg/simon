package com.simon.facades.cms;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorcms.model.components.CategoryNavigationComponentModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.components.CMSLinkComponentModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationEntryModel;
import de.hybris.platform.cms2.model.navigation.CMSNavigationNodeModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.enums.NavigationPageType;
import com.simon.core.model.PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.core.services.SimonCategoryNavigationService;
import com.simon.facades.constants.SimonFacadesConstants;


/**
 * Unit test case for SimonCategoryNavigationFacadeImplTest
 */
@UnitTest
public class SimonCategoryNavigationFacadeImplTest
{
	@InjectMocks
	private SimonCategoryNavigationFacadeImpl simonCategoryNavigationFacadeImpl;

	@Mock
	private CategoryNavigationComponentModel component;

	@Mock
	private CMSNavigationNodeModel navigationNodeModel;

	@Mock
	private CMSNavigationEntryModel navigationEntryModel;

	@Mock
	private CMSLinkComponentModel linkComponentModel;

	@Mock
	private CategoryModel categoryModel;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;
	@Mock
	private CategoryService categoryService;
	@Mock
	private Converter<CategoryModel, CategoryData> cmsCategoryDataConverter;
	@Mock
	private SimonCategoryNavigationService simonCategoryNavigationService;

	@Mock
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;
	@Mock
	private NavigationPageType navigationPageType;

	@Mock
	private Converter<PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel, PromoBanner2ClickableImageWithTextAndSingleCTAComponentData> simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter;

	@Mock
	PromoBanner2ClickableImageWithTextAndSingleCTAComponentModel promoBanner2ClickableImageWithTextAndSingleCTAComponent;

	@Mock
	PromoBanner2ClickableImageWithTextAndSingleCTAComponentData promoBanner2ClickableImageWithTextAndSingleCTAComponentData;
	public static final String CLP_PAGE_ID = "clp";
	final List<NavigationDTO> navigationNodes = new ArrayList<>();
	final NavigationDTO navigationDTO = mock(NavigationDTO.class);

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(configuration.getInt("node.count.navigation.subcategories")).thenReturn(1);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		simonCategoryNavigationFacadeImpl.setSimonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter(
				simonPromoBanner2ClickableImageWithTextAndSingleCTAComponentConverter);
		when(extCategoryModelUrlResolver.resolveCategoryPath(Matchers.any(CategoryModel.class), Matchers.anyString()))
				.thenReturn("URL");
	}

	/**
	 * Method will test if Navigation Node DTO is populated
	 */
	@Test
	public void testGetNavigationNodes()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		final List<CMSNavigationEntryModel> entriesL1 = new ArrayList<>();
		entriesL1.add(navigationEntryModel);

		final List<CategoryModel> categories = new ArrayList<>();
		categories.add(categoryModel);
		final String categoryId = "m100100";
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		navigationNodes.add(0, navigationDTO);
		when(navigationPageType.getCode()).thenReturn(CLP_PAGE_ID);
		when(navigationNodeModel.getPromoBanner2()).thenReturn(promoBanner2ClickableImageWithTextAndSingleCTAComponent);

		when(configuration.getInt("node.count.navigation.subcategories")).thenReturn(10);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(simonCategoryNavigationService.getSubCategories(categoryId)).thenReturn(categories);
		when(navigationNodeModel.isDynamicCategory()).thenReturn(true);

		when(categoryModel.getCode()).thenReturn("m100100");
		when(linkComponentModel.getCategory()).thenReturn(categoryModel);
		when(navigationEntryModel.getItem()).thenReturn(linkComponentModel);
		when(navigationNodeModel.getEntries()).thenReturn(entriesL1);
		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);


		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO1 = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO1);

	}

	/**
	 * Method will test if L2 nodes are static and promo banner is null
	 */
	@Test
	public void testGetNavigationNodesForStaticNode()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		final List<CMSNavigationEntryModel> entriesL1 = new ArrayList<>();
		entriesL1.add(navigationEntryModel);

		final List<CategoryModel> categories = new ArrayList<>();
		categories.add(categoryModel);
		final String categoryId = "m100100";
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		when(navigationPageType.getCode()).thenReturn(CLP_PAGE_ID);
		navigationNodes.add(0, navigationDTO);
		when(simonCategoryNavigationService.getSubCategories(categoryId)).thenReturn(categories);
		when(navigationNodeModel.isDynamicCategory()).thenReturn(false);
		when(linkComponentModel.getUrl()).thenReturn("/c/m100100");
		when(linkComponentModel.getCategory()).thenReturn(null);
		when(navigationEntryModel.getItem()).thenReturn(linkComponentModel);
		when(navigationNodeModel.getEntries()).thenReturn(entriesL1);
		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);

	}

	/**
	 * Method will test if entries for L2 nodes are null
	 */
	@Test
	public void testGetNavigationNodesForNullEntries()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);
	}

	/**
	 * Metod will test if L2 nodes does not contains sub categories
	 */
	@Test
	public void testGetNavigationForSubCategoriesNull()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		final List<CMSNavigationEntryModel> entriesL1 = new ArrayList<>();
		entriesL1.add(navigationEntryModel);
		final List<CategoryModel> categories = new ArrayList<>();

		final String categoryId = "m100100";
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		when(navigationPageType.getCode()).thenReturn(CLP_PAGE_ID);
		when(simonCategoryNavigationService.getSubCategories(categoryId)).thenReturn(categories);
		when(navigationNodeModel.isDynamicCategory()).thenReturn(true);
		when(categoryModel.getCode()).thenReturn("m100100");
		when(linkComponentModel.getCategory()).thenReturn(categoryModel);
		when(navigationEntryModel.getItem()).thenReturn(linkComponentModel);
		when(navigationNodeModel.getEntries()).thenReturn(entriesL1);
		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);
	}

	/**
	 * Method will test when nodecount is equal to totalnodes
	 */
	@Test
	public void testGetNavigationNodesForEqualNodesCounts()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		final List<CMSNavigationEntryModel> entriesL1 = new ArrayList<>();
		entriesL1.add(navigationEntryModel);

		final List<CategoryModel> categories = new ArrayList<>();
		categories.add(categoryModel);
		final String categoryId = "m100100";
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		when(navigationPageType.getCode()).thenReturn(CLP_PAGE_ID);
		when(configuration.getInt("node.count.navigation.subcategories")).thenReturn(1);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(simonCategoryNavigationService.getSubCategories(categoryId)).thenReturn(categories);
		when(navigationNodeModel.isDynamicCategory()).thenReturn(true);

		when(categoryModel.getCode()).thenReturn("m100100");
		when(linkComponentModel.getCategory()).thenReturn(categoryModel);
		when(navigationEntryModel.getItem()).thenReturn(linkComponentModel);
		when(navigationNodeModel.getEntries()).thenReturn(entriesL1);
		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);

	}



	/**
	 * Method will test id L2 nodes are static and dynamic category is false
	 */
	@Test
	public void testGetNavigationNodesForStaticCategoryNode()
	{
		final List<CMSNavigationNodeModel> childrenL1 = new ArrayList<>();
		childrenL1.add(navigationNodeModel);

		final List<CMSNavigationEntryModel> entriesL1 = new ArrayList<>();
		entriesL1.add(navigationEntryModel);

		final List<CategoryModel> categories = new ArrayList<>();
		categories.add(categoryModel);
		final String categoryId = "m100100";
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		when(navigationPageType.getCode()).thenReturn(CLP_PAGE_ID);
		when(navigationNodeModel.getPromoBanner2()).thenReturn(promoBanner2ClickableImageWithTextAndSingleCTAComponent);
		when(configuration.getInt("node.count.navigation.subcategories")).thenReturn(10);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(simonCategoryNavigationService.getSubCategories(categoryId)).thenReturn(categories);
		when(navigationNodeModel.isDynamicCategory()).thenReturn(false);

		when(categoryModel.getCode()).thenReturn("m100100");
		when(linkComponentModel.getCategory()).thenReturn(categoryModel);
		when(navigationEntryModel.getItem()).thenReturn(linkComponentModel);
		when(navigationNodeModel.getEntries()).thenReturn(entriesL1);
		when(navigationNodeModel.getChildren()).thenReturn(childrenL1);
		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(navigationNodeModel.isVisible()).thenReturn(true);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);

	}

	/**
	 * Method will when visible flag is false
	 */
	@Test
	public void testGetNavigationNodeForIsvisibleFalse()
	{

		when(component.getNavigationNode()).thenReturn(navigationNodeModel);
		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacadeImpl
				.getNavigationNodes(component);
		assertNotNull(navigationDTO);
	}

	/**
	 * Method will test for category of my size
	 */
	@Test
	public void testGetCategoryforMySize()
	{

		when(component.getNavigationPageType()).thenReturn(navigationPageType);
		when(configuration.getString(SimonFacadesConstants.MY_SIZE_ROOT_CATEGORY)).thenReturn("rootCategory");
		when(categoryService.getCategoryForCode(Mockito.anyString())).thenReturn(categoryModel);
		final List<CategoryModel> categoryList = new ArrayList<>();
		categoryList.add(categoryModel);
		when(categoryModel.getCode()).thenReturn("size-men");
		when(categoryModel.getAllSubcategories()).thenReturn(categoryList);
		when(configuration.getString(SimonFacadesConstants.MY_SIZE_BASE_CATEGORY)).thenReturn("size-men,size-women");
		final Map<String, Collection<CategoryData>> category = simonCategoryNavigationFacadeImpl.getCategoryforMySize();
		assertNotNull(category);
	}



}
