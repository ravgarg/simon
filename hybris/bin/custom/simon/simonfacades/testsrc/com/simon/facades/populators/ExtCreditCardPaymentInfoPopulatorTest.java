package com.simon.facades.populators;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Date;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * The Class ExtPaymentInfoPopulatorTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCreditCardPaymentInfoPopulatorTest
{
	private static final String CARD_TYPE_VAULT_PREFIX = "cardtype.vault.";
	@InjectMocks
	@Spy
	private final ExtCreditCardPaymentInfoPopulator extPaymentInfoPopulator = new ExtCreditCardPaymentInfoPopulator();

	@Mock
	private Converter<AddressModel, AddressData> addressConverter;
	@Mock
	private AddressData addressData;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;

	/**
	 * test the population of data from customer model to CustomerData
	 */
	@Test
	public void testPopulate()
	{
		final CreditCardPaymentInfoModel source = new CreditCardPaymentInfoModel();
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		final CCPaymentInfoData target = new CCPaymentInfoData();
		final AddressData addressData = Mockito.mock(AddressData.class);
		Mockito.doReturn(addressData).when(addressConverter).convert(addressModel);
		source.setBillingAddress(addressModel);
		source.setPaymentToken("test");
		source.setUpdated(new Date());
		source.setValidToMonth("2");
		source.setValidToYear("2022");
		source.setType(CreditCardType.VISA);
		source.setLastFourDigits("1234");
		Mockito.doNothing().when(extPaymentInfoPopulator).superPopulate(source, target);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(CARD_TYPE_VAULT_PREFIX + source.getType().getCode())).thenReturn("visa");
		extPaymentInfoPopulator.populate(source, target);
		Assert.assertEquals(target.getCardNumber(), source.getCcOwner());
	}


	/**
	 * Negative test the population of data from customer model to CustomerData
	 */
	@Test
	public void testPopulateNullInput()
	{
		final CreditCardPaymentInfoModel source = new CreditCardPaymentInfoModel();
		final AddressModel addressModel = null;//Mockito.mock(AddressModel.class);
		final CCPaymentInfoData target = new CCPaymentInfoData();
		source.setBillingAddress(addressModel);
		source.setPaymentToken("test");
		source.setUpdated(new Date());
		source.setNumber("123");
		source.setValidToMonth("2");
		source.setValidToYear("2022");
		source.setType(CreditCardType.VISA);
		source.setLastFourDigits("1234");
		Mockito.doNothing().when(extPaymentInfoPopulator).superPopulate(source, target);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(CARD_TYPE_VAULT_PREFIX + source.getType().getCode())).thenReturn("visa");
		extPaymentInfoPopulator.populate(source, target);
		assertNull(source.getBillingAddress());
	}


}
