package com.simon.facades.navigation.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dto.NavigationData;
import com.simon.core.services.LeftNavigationCategoryService;


@UnitTest
public class LeftNavigationCategoryFacadeImplTest
{
	@InjectMocks
	private LeftNavigationCategoryFacadeImpl leftNavigationCategoryFacade;

	@Mock
	private LeftNavigationCategoryService leftNavigationCategoryService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testgetL1AndLeafCategoryCode()
	{
		final String categoryCode = "m300000_m300500_m300501";
		final Pair<String, String> categoryPair = leftNavigationCategoryFacade.getL1AndLeafCategoryCode(categoryCode);
		assertEquals(categoryPair.getLeft(), "m300000");
		assertEquals(categoryPair.getRight(), "m300501");
	}


	@Test
	public void testNavigationData()
	{
		final String L1categoryCode = "m300000";
		when(leftNavigationCategoryService.getNavigationData(L1categoryCode)).thenReturn(Matchers.anyList());
		final List<NavigationData> navdata = leftNavigationCategoryFacade.getNavigationData(L1categoryCode);
		assertNotNull(navdata);
	}
}
