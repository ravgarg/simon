package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DesignerData;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtDesignerPopulatorTest
{
	@InjectMocks
	private ExtDesignerPopulator extDesignerPopulator;

	@Test
	public void testConvert()
	{
		final DesignerModel designerModel = new DesignerModel();
		final ContentPageModel contentPageModel = Mockito.mock(ContentPageModel.class);
		contentPageModel.setLabel("/designer/test");
		designerModel.setActive(false);
		designerModel.setBannerUrl("/test");
		designerModel.setName("TestName");
		designerModel.setCode("/test");
		designerModel.setDescription("/test");
		final Collection<ContentPageModel> contentModels = new ArrayList<>();
		designerModel.setContentPage(contentModels);

		final DesignerData designerData = new DesignerData();
		extDesignerPopulator.populate(designerModel, designerData);

		Assert.assertEquals(designerModel.getCode(), designerData.getCode());
		Assert.assertEquals(designerModel.getName(), designerData.getName());
		Assert.assertEquals(designerModel.getDescription(), designerData.getDescription());
	}

}
