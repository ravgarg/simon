package com.simon.facades.search.converters.populator;

import static org.mockito.Mockito.doNothing;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


/**
 * This Class tests the methods of SimonSearchResultVariantOptionsProductPopulator.
 */
@UnitTest
public class ExtSearchResultVariantOptionsProductPopulatorTest
{
	private static final String SAMPLE_PROMOTION_TYPE = "Rule Based Promotion";

	private static final String SAMPLE_PROMOTION_PRIORITY = "100";

	private static final String SAMPLE_PROMO_DESCRIPTION = "promoDescription";

	private static final String SAMPLE_PROMO_CODE = "promoCode";

	private static final String VARIANT_PRODUCT_IMAGES = "images";
	@Spy
	@InjectMocks
	private ExtSearchResultVariantOptionsProductPopulator simonSearchResultVariantOptionsProductPopulator;

	@Mock
	private Configuration configuration;

	private SearchResultValueData source;

	@Mock
	private CommonI18NService commonI18NService;

	private ProductData target;


	private List<String> listOfImages;


	private Map<String, Object> mapOfImages;


	private SearchResultValueData variantProduct;
	private SearchResultValueData variantProduct1;
	private CurrencyModel currencyModel;

	@Mock
	private SearchResultValueData searchResultValueData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		final String desktopImage = "200Wx305H_default|/medias/?context=bWFzdGVyfGltYWdlc3wxOTUwN3xpbWFnZS9qcGVnfGltYWdlcy9oYzYvaDY4Lzg3OTYxOTIxNzgyMDYuanBnfDkwYmEzMTQzNmNiZWMyYWQwMmMzZTE2NzVhNzQxMDcyMDY3NzVkNDA3ODZjNjRjZDY3OTA3NGE1ODM4MjYyNjk";
		final String mobileImage = "150Wx229H_default|/medias/?context=bWFzdGVyfGltYWdlc3wxOTUwN3xpbWFnZS9qcGVnfGltYWdlcy9oYzYvaDY4Lzg3OTYxOTIxNzgyMDYuanBnfDkwYmEzMTQzNmNiZWMyYWQwMmMzZTE2NzVhNzQxMDcyMDY3NzVkNDA3ODZjNjRjZDY3OTA3NGE1ODM4MjYyNjk";
		final List<SearchResultValueData> listOfVariantProduct = new ArrayList<>();
		final Map<String, Object> mapOfVariantProductValues = new HashMap<>();
		final List<String> variantOptions = new ArrayList<>();
		variantOptions.add("LIST|19.99|false");
		variantOptions.add("SALE|0.14214285714285713|true");
		variantOptions.add("MSRP|98.0|false");
		variantOptions.add("PERCENT_SAVING|99");
		source = new SearchResultValueData();
		target = new ProductData();
		listOfImages = new ArrayList<>();
		mapOfImages = new HashMap<>();
		variantProduct = new SearchResultValueData();
		variantProduct1 = new SearchResultValueData();

		listOfImages.add(desktopImage);
		listOfImages.add(mobileImage);
		mapOfImages.put(VARIANT_PRODUCT_IMAGES, listOfImages);
		mapOfImages.put("inStockFlag", Boolean.TRUE);
		mapOfImages.put("url", "sampleurl");
		mapOfImages.put("plumPrice", 30.5D);
		mapOfImages.put("colorName", "red");
		mapOfImages.put("variantPrice", variantOptions);
		source.setValues(mapOfImages);

		mapOfVariantProductValues.put("url", "sampleurl");
		mapOfVariantProductValues.put("colorName", "red");
		mapOfVariantProductValues.put("inStockFlag", Boolean.TRUE);
		variantProduct.setValues(mapOfImages);
		variantProduct1.setValues(mapOfImages);

		listOfVariantProduct.add(variantProduct);
		listOfVariantProduct.add(variantProduct1);
		source.setVariants(listOfVariantProduct);

		currencyModel = new CurrencyModel();
		currencyModel.setSymbol("$");
		Mockito.doReturn(currencyModel).when(commonI18NService).getCurrentCurrency();
		Mockito.doReturn("Up to ").when(simonSearchResultVariantOptionsProductPopulator)
				.getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
		Mockito.doReturn("% Off").when(simonSearchResultVariantOptionsProductPopulator)
				.getLocalizedString(SimonCoreConstants.PERCENT_SAVING_PRE_MESSAGE);
	}

	@Test
	public void testProductWithNoVariant()
	{
		source.setVariants(null);
		mapOfImages.put("baseProductName", "name");
		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(1, target.getVariantOptions().size());
	}

	@Test
	public void testProductWithVariantAndPrice()
	{

		mapOfImages.put("baseProductName", "name");
		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(1, target.getVariantOptions().size());
	}

	@Test
	public void testProductWithVariant()
	{
		mapOfImages.put("baseProductName", "name");
		mapOfImages.put("baseProductDesignerName", "designer");
		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(target.getVariantOptions().size(), 1);
	}

	@Test
	public void testProductWithImages()
	{

		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(target.getVariantOptions().get(0).getVariantImage().size(), 1);
	}

	@Test
	public void testPromotionPopulationInProductWithPromotionInfo()
	{
		mapOfImages.put(IndexedKeys.PROMOTION_CODE, SAMPLE_PROMO_CODE);
		mapOfImages.put(IndexedKeys.PROMOTION_DESCRIPTION, SAMPLE_PROMO_DESCRIPTION);
		mapOfImages.put(IndexedKeys.PROMOTION_PRIORITY, SAMPLE_PROMOTION_PRIORITY);
		mapOfImages.put(IndexedKeys.PROMOTION_TYPE, SAMPLE_PROMOTION_TYPE);
		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(target.getVariantOptions().size(), 1);
		Assert.assertEquals(((List<PromotionData>) target.getPotentialPromotions()).get(0).getCode(), SAMPLE_PROMO_CODE);
	}

	@Test
	public void testPromotionPopulationInProductWithoutPromotionInfo()
	{
		doNothing().when(simonSearchResultVariantOptionsProductPopulator).superPopulator(source, target);
		simonSearchResultVariantOptionsProductPopulator.populate(source, target);
		Assert.assertEquals(target.getVariantOptions().size(), 1);
		Assert.assertNull(((List<PromotionData>) target.getPotentialPromotions()).get(0).getCode());
	}
}
