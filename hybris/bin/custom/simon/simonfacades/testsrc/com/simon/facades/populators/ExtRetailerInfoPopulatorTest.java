package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.data.PromotionOrderEntryConsumedData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.AdditionalCartEstimateModel;
import com.simon.core.model.AdditionalCartEstimateSiteModel;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.AdditionalPriceModel;
import com.simon.core.services.RetailerService;
import com.simon.facades.cart.data.RetailerInfoData;


/**
 *
 * Junit test for ExtRetailerInfoPopulator
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtRetailerInfoPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtRetailerInfoPopulator<CartModel, CartData> extRetailerInfoPopulator;

	@Mock
	private ExtPriceDataPopulator<AbstractOrderEntryModel, OrderEntryData> extPriceDataPopulator;

	@Mock
	private Converter<ShopModel, RetailerInfoData> extRetailerConverter;

	@Mock
	private CommercePriceService commercePriceService;

	@Mock
	private ProductService productService;
	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;


	@Mock
	private ProductModel productModel;
	@Mock
	private ShopModel shopModel;
	@Mock
	private OrderEntryData orderEntryData;
	@Mock
	private RetailerInfoData retailerInfoData;
	@Mock
	private ProductData variant;
	@Mock
	private ProductModel variantproductModel;
	@Mock
	private ProductModel productModelVariant;
	@Mock
	private VariantCategoryModel variantCategoryModel;
	@Mock
	private CategoryModel variantSuperCategoryModel;
	@Mock
	private CategoryModel value;
	@Mock
	private AdditionalCartInfoModel proxyCartModel;
	@Mock
	private AdditionalCartEstimateModel estimate;
	@Mock
	private AdditionalCartEstimateSiteModel estimateSiteModel;
	@Mock
	private AdditionalPriceModel proxyPriceModel;


	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private CurrencyModel currency;

	@Mock
	private PriceData priceData;
	@Mock
	private RetailerService retailerService;

	@Mock
	private PromotionResultData promotionResultData;
	@Mock
	private PromotionOrderEntryConsumedData promotionOrderEntryConsumedData;
	@Mock
	private PriceData youSaved;

	@Mock
	private PriceData listValue;

	@Mock
	private PriceData saleValue;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration config;

	private final String stringValue = "abc";


	/**
	 *
	 */
	@Test
	public void populateNoRetailerExist()
	{

		final CartData target = new CartData();
		final CartEntryModel cartEntryModel = mock(CartEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<>();
		abstractOrderEntryModelList.add(cartEntryModel);
		final CartModel cartModel = mock(CartModel.class);
		when(cartModel.getEntries()).thenReturn(abstractOrderEntryModelList);

		when(cartModel.getAdditionalCartInfo()).thenReturn(proxyCartModel);
		when(proxyCartModel.getEstimate()).thenReturn(estimate);

		final List<AdditionalCartEstimateSiteModel> estimates = new ArrayList<>();
		estimates.add(estimateSiteModel);

		when(proxyPriceModel.getSalesTax()).thenReturn("10");
		when(proxyPriceModel.getShippingPrice()).thenReturn("10");

		when(estimateSiteModel.getPrices()).thenReturn(proxyPriceModel);


		when(estimateSiteModel.getSiteId()).thenReturn("siteId");

		when(estimate.getEstimates()).thenReturn(estimates);

		when(retailerInfoData.getRetailerId()).thenReturn("shopId");

		when(shopModel.getSiteId()).thenReturn("siteId");


		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");

		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(0.0), "USD")).thenReturn(priceData);

		when(cartEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getId()).thenReturn("shopId");
		extRetailerInfoPopulator.setOrderEntryConverter(orderEntryConverter);
		doReturn(orderEntryData).when(orderEntryConverter).convert(cartEntryModel);
		extRetailerInfoPopulator.setExtRetailerConverter(extRetailerConverter);
		final List<OrderEntryData> productDetails = new ArrayList<>();
		productDetails.add(orderEntryData);
		retailerInfoData.setProductDetails(productDetails);
		doReturn(retailerInfoData).when(extRetailerConverter).convert(shopModel);
		doReturn(Long.valueOf(1)).when(cartEntryModel).getQuantity();
		doNothing().when(extPriceDataPopulator).populate(cartEntryModel, orderEntryData);
		doReturn(variant).when(orderEntryData).getProduct();
		doReturn("variantCode").when(variant).getCode();
		doReturn("BaseProduct").when(variant).getBaseProduct();
		doReturn(variantproductModel).when(productService).getProductForCode("variantCode");
		doReturn(productModelVariant).when(productService).getProductForCode("BaseProduct");
		final Collection<VariantCategoryModel> categoryModels = new ArrayList<VariantCategoryModel>();
		categoryModels.add(variantCategoryModel);
		doReturn(categoryModels).when(productModelVariant).getSupercategories();
		final Collection<CategoryModel> variantCategoryModels = new ArrayList<CategoryModel>();
		variantCategoryModels.add(variantSuperCategoryModel);
		doReturn(variantCategoryModels).when(variantproductModel).getSupercategories();
		doReturn(variantCategoryModels).when(variantCategoryModel).getAllSubcategories();
		doReturn(10.0d).when(retailerService).getRetailerSubBagSaving(cartModel, "shopId");

		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");
		when(configurationService.getConfiguration()).thenReturn(config);
		when(config.getString("generic.link.customercare.zendesk.faq.page")).thenReturn(stringValue);

		when(priceData.getValue()).thenReturn(BigDecimal.ZERO);
		when(retailerInfoData.getSaving()).thenReturn(priceData);
		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);
		target.setRetailerInfoData(retailerInfoDataList);


		extRetailerInfoPopulator.populate(cartModel, target);
		Assert.assertNotNull(target.getRetailerInfoData());

	}


	/**
	 *
	 */
	@Test
	public void populateRetailerExist()
	{

		final RetailerInfoData retailerInfoData = mock(RetailerInfoData.class);
		final Map<String, RetailerInfoData> retailerInfoDataMap = new HashMap<>();
		retailerInfoDataMap.put("retailerInfoData", retailerInfoData);
		doReturn(retailerInfoDataMap).when(extRetailerInfoPopulator).getRetailerInfoDataMap();
		doReturn("shopId").when(retailerInfoData).getRetailerId();
		final CartData target = new CartData();
		final CartEntryModel cartEntryModel = mock(CartEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<>();
		abstractOrderEntryModelList.add(cartEntryModel);
		final CartModel cartModel = mock(CartModel.class);
		when(cartModel.getEntries()).thenReturn(abstractOrderEntryModelList);
		when(cartEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getId()).thenReturn("shopId");
		extRetailerInfoPopulator.setOrderEntryConverter(orderEntryConverter);
		doReturn(orderEntryData).when(orderEntryConverter).convert(cartEntryModel);
		extRetailerInfoPopulator.setExtRetailerConverter(extRetailerConverter);
		doReturn(retailerInfoData).when(extRetailerConverter).convert(shopModel);
		doReturn(Long.valueOf(1)).when(cartEntryModel).getQuantity();
		doNothing().when(extPriceDataPopulator).populate(cartEntryModel, orderEntryData);
		doReturn(variant).when(orderEntryData).getProduct();
		doReturn("variantCode").when(variant).getCode();
		doReturn("BaseProduct").when(variant).getBaseProduct();
		doReturn(variantproductModel).when(productService).getProductForCode("variantCode");
		doReturn(productModelVariant).when(productService).getProductForCode("BaseProduct");
		final Collection<VariantCategoryModel> categoryModels = new ArrayList<VariantCategoryModel>();
		categoryModels.add(variantCategoryModel);
		Mockito.doReturn(categoryModels).when(productModelVariant).getSupercategories();
		final Collection<CategoryModel> variantCategoryModels = new ArrayList<CategoryModel>();
		variantCategoryModels.add(variantSuperCategoryModel);
		Mockito.doReturn(variantCategoryModels).when(variantproductModel).getSupercategories();
		Mockito.doReturn(variantCategoryModels).when(variantCategoryModel).getAllSubcategories();

		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");
		when(retailerInfoData.getSaving()).thenReturn(priceData);
		when(priceData.getValue()).thenReturn(BigDecimal.ZERO);
		when(configurationService.getConfiguration()).thenReturn(config);
		when(config.getString("generic.link.customercare.zendesk.faq.page")).thenReturn(stringValue);
		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);
		target.setRetailerInfoData(retailerInfoDataList);

		extRetailerInfoPopulator.populate(cartModel, target);
		Assert.assertNotNull(target.getRetailerInfoData());


	}



	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotion()
	{
		final CartData target = mock(CartData.class);
		when(priceData.getValue()).thenReturn(BigDecimal.valueOf(1000d));
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(orderEntryData.getTotalPrice()).thenReturn(priceData);
		when(orderEntryData.getQuantity()).thenReturn(10l);
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(priceData.getCurrencyIso()).thenReturn("USD");
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		final PromotionData promotionData = mock(PromotionData.class);
		when(promotionData.getCode()).thenReturn("test");
		when(promotionData.getPromotionType()).thenReturn("testPro");
		when(promotionResultData.getPromotionData()).thenReturn(promotionData);
		when(promotionResultData.getDescription()).thenReturn("This is test");
		promotionResultDataList.add(promotionResultData);
		doReturn("% off").when(extRetailerInfoPopulator).getPercentSavingMessage();
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}

	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotionWhenBasePriceIsNull()
	{
		final CartData target = mock(CartData.class);
		when(priceData.getValue()).thenReturn(BigDecimal.valueOf(1000d));
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(orderEntryData.getTotalPrice()).thenReturn(priceData);
		when(orderEntryData.getQuantity()).thenReturn(10l);
		when(orderEntryData.getBasePrice()).thenReturn(null);
		doReturn("% off").when(extRetailerInfoPopulator).getPercentSavingMessage();
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		final PromotionData promotionData = mock(PromotionData.class);
		when(promotionData.getCode()).thenReturn("test");
		when(promotionData.getPromotionType()).thenReturn("testPro");
		when(promotionResultData.getPromotionData()).thenReturn(promotionData);
		when(promotionResultData.getDescription()).thenReturn("This is test");
		promotionResultDataList.add(promotionResultData);
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}

	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotionWhenCurrencyIsoIsNull()
	{
		final CartData target = mock(CartData.class);
		when(priceData.getValue()).thenReturn(BigDecimal.valueOf(1000d));
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(orderEntryData.getTotalPrice()).thenReturn(priceData);
		when(orderEntryData.getQuantity()).thenReturn(10l);
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(priceData.getCurrencyIso()).thenReturn(null);
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		final PromotionData promotionData = mock(PromotionData.class);
		when(promotionData.getCode()).thenReturn("test");
		when(promotionData.getPromotionType()).thenReturn("testPro");
		when(promotionResultData.getPromotionData()).thenReturn(promotionData);
		when(promotionResultData.getDescription()).thenReturn("This is test");
		promotionResultDataList.add(promotionResultData);
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		doReturn("% off").when(extRetailerInfoPopulator).getPercentSavingMessage();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}

	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotionWhengetEntryNumberIsNull()
	{
		final CartData target = mock(CartData.class);
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(priceData.getCurrencyIso()).thenReturn(null);
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		promotionResultDataList.add(promotionResultData);
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		doReturn(null).when(orderEntryData).getEntryNumber();
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertNull(orderEntryData.getEntryNumber());
	}


	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotionWhengetOrderEntryNumberIsNull()
	{
		final CartData target = mock(CartData.class);
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(priceData.getCurrencyIso()).thenReturn(null);
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		final PromotionData promotionData = mock(PromotionData.class);
		when(promotionData.getCode()).thenReturn("test");
		when(promotionData.getPromotionType()).thenReturn("testPro");
		when(promotionResultData.getPromotionData()).thenReturn(promotionData);
		when(promotionResultData.getDescription()).thenReturn("This is test");
		promotionResultDataList.add(promotionResultData);
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		doReturn(null).when(promotionOrderEntryConsumedData).getOrderEntryNumber();
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}

	/**
	 *
	 */
	@Test
	public void testAppliedProductPromotionWhengetOrderEntryNumberIsNotEquals()
	{
		final CartData target = mock(CartData.class);
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(priceData.getCurrencyIso()).thenReturn(null);
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		promotionResultDataList.add(promotionResultData);
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(2.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		doReturn(2).when(promotionOrderEntryConsumedData).getOrderEntryNumber();
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}

	@Test
	public void testAppliedProductPromotionWhenStrikeThroughPrice()
	{
		final CartData target = mock(CartData.class);
		when(priceData.getValue()).thenReturn(BigDecimal.valueOf(1000d));
		when(orderEntryData.getBasePrice()).thenReturn(priceData);
		when(orderEntryData.getTotalPrice()).thenReturn(priceData);
		when(orderEntryData.getQuantity()).thenReturn(10l);
		when(priceData.getCurrencyIso()).thenReturn("USD");
		final List<PromotionResultData> promotionResultDataList = new ArrayList<>();
		final PromotionData promotionData = mock(PromotionData.class);
		when(promotionData.getCode()).thenReturn("test");
		when(promotionData.getPromotionType()).thenReturn("testPro");
		when(promotionResultData.getPromotionData()).thenReturn(promotionData);
		when(promotionResultData.getDescription()).thenReturn("This is test");
		promotionResultDataList.add(promotionResultData);
		doReturn("% off").when(extRetailerInfoPopulator).getPercentSavingMessage();
		when(target.getAppliedProductPromotions()).thenReturn(promotionResultDataList);
		final List<PromotionOrderEntryConsumedData> promotionOrderEntryConsumedDataList = new ArrayList<>();
		promotionOrderEntryConsumedDataList.add(promotionOrderEntryConsumedData);
		when(promotionResultData.getConsumedEntries()).thenReturn(promotionOrderEntryConsumedDataList);
		doReturn(BigDecimal.valueOf(0.0d)).when(extRetailerInfoPopulator).getStrikeOffPrice(orderEntryData);
		when(promotionOrderEntryConsumedData.getAdjustedUnitPrice()).thenReturn(Double.valueOf(1.0d));
		doReturn(youSaved).when(priceDataFactory).create(PriceDataType.BUY, BigDecimal.valueOf(1.0d), "USD");
		extRetailerInfoPopulator.getAppliedProductPromotion(orderEntryData, target);
		assertEquals(0, orderEntryData.getEntryNumber().intValue());
	}



	/**
	 *
	 */
	@Test
	public void testPlumPriceIsMsrp()
	{
		when(orderEntryData.getMsrpValue()).thenReturn(priceData);
		when(priceData.isStrikeOff()).thenReturn(true);
		when(priceData.getValue()).thenReturn(BigDecimal.ONE);
		assertEquals(BigDecimal.ONE.intValue(), extRetailerInfoPopulator.getStrikeOffPrice(orderEntryData).intValue());
	}




	/**
	 *
	 */
	@Test
	public void testPlumPriceIsListValue()
	{
		when(orderEntryData.getMsrpValue()).thenReturn(priceData);
		when(priceData.isStrikeOff()).thenReturn(false);
		when(orderEntryData.getListValue()).thenReturn(listValue);
		when(listValue.isStrikeOff()).thenReturn(true);
		when(listValue.getValue()).thenReturn(BigDecimal.ONE);
		assertEquals(BigDecimal.ONE.intValue(), extRetailerInfoPopulator.getStrikeOffPrice(orderEntryData).intValue());
	}


	/**
	 *
	 */
	@Test
	public void testPlumPriceIsSaleValue()
	{
		when(orderEntryData.getMsrpValue()).thenReturn(priceData);
		when(priceData.isStrikeOff()).thenReturn(false);
		when(orderEntryData.getListValue()).thenReturn(listValue);
		when(listValue.isStrikeOff()).thenReturn(false);
		when(orderEntryData.getSaleValue()).thenReturn(saleValue);
		when(saleValue.isStrikeOff()).thenReturn(true);
		when(saleValue.getValue()).thenReturn(BigDecimal.ONE);
		assertEquals(BigDecimal.ONE.intValue(), extRetailerInfoPopulator.getStrikeOffPrice(orderEntryData).intValue());
	}





}
