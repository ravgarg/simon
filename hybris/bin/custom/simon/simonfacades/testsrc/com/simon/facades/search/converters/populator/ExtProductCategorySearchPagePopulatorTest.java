package com.simon.facades.search.converters.populator;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.facades.account.data.DesignerData;


/**
 * Test class for ExtProductCategorySearchPagePopulator
 *
 * @param <QUERY>
 * @param <STATE>
 * @param <RESULT>
 * @param <ITEM>
 * @param <SCAT>
 * @param <CATEGORY>
 */
@UnitTest
public class ExtProductCategorySearchPagePopulatorTest<QUERY, STATE, RESULT, ITEM extends ProductData, SCAT, CATEGORY>
{
	@Spy
	@InjectMocks
	private ExtProductCategorySearchPagePopulator extProductCategorySearchPagePopulator;

	/**
	 * Setup Method for Junit
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test method for populate
	 */
	@Test
	public void testPopulate()
	{
		final List<DesignerData> activeDesigners = new ArrayList<>();
		final ProductCategorySearchPageData<QUERY, RESULT, SCAT> source = Mockito.mock(ProductCategorySearchPageData.class);
		final ProductCategorySearchPageData<STATE, ITEM, CATEGORY> target = new ProductCategorySearchPageData();
		activeDesigners.add(new DesignerData());
		Mockito.when(source.getActiveDesigners()).thenReturn(activeDesigners);
		Mockito.doNothing().when(extProductCategorySearchPagePopulator).callSuperMethod(source, target);
		extProductCategorySearchPagePopulator.populate(source, target);
		Assert.assertTrue(CollectionUtils.isNotEmpty(target.getActiveDesigners()));
	}

}
