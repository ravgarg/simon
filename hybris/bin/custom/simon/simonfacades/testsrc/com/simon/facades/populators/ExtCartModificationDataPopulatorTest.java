package com.simon.facades.populators;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.sape.junit.FunctionalTest;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.message.utils.ExtCustomMessageUtils;




/**
 * Unit test case for {@link ExtCartModificationDataPopulatorTest}
 */
@UnitTest
@FunctionalTest(features =
{ SimonCoreConstants.CART })
public class ExtCartModificationDataPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtCartModificationDataPopulator extCartModificationDataPopulator;

	@Mock
	private CartModel cartModel;

	@Mock
	private CartModificationData cartModificationData;

	@Mock
	private AbstractOrderEntryModel abstractOrderEntryModel;

	@Mock
	private ExtCustomMessageUtils extCustomMessageUtils;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method will test count of entries in each order entry.
	 */
	@Test
	public void testPopulate()
	{
		final long itemCount = 0;
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<AbstractOrderEntryModel>();
		abstractOrderEntryModelList.add(abstractOrderEntryModel);
		when(cartModificationData.getStatusCode()).thenReturn("noStock");
		when(abstractOrderEntryModel.getQuantity()).thenReturn(Long.valueOf(itemCount));
		when(cartModel.getEntries()).thenReturn(abstractOrderEntryModelList);
		Mockito.doReturn(SimonFacadesConstants.NO_STOCK_ERROR).when(extCartModificationDataPopulator)
				.setLocalizedString(SimonFacadesConstants.NO_STOCK_ERROR);
		extCartModificationDataPopulator.populate(cartModel, cartModificationData);
		Assert.assertEquals("noStock", cartModificationData.getStatusCode());
		verify(cartModel, times(1)).getEntries();
		verify(abstractOrderEntryModel, times(1)).getQuantity();
	}

	/**
	 * Method will test count of entries in each order entry when stock is low.
	 */
	@Test
	public void testPopulateWhenLowStock()
	{
		final long itemCount = 0;
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList<AbstractOrderEntryModel>();
		abstractOrderEntryModelList.add(abstractOrderEntryModel);
		when(cartModificationData.getStatusCode()).thenReturn("lowStock");
		when(abstractOrderEntryModel.getQuantity()).thenReturn(Long.valueOf(itemCount));
		when(cartModel.getEntries()).thenReturn(abstractOrderEntryModelList);
		Mockito.doReturn(SimonFacadesConstants.LOW_STOCK_ERROR).when(extCartModificationDataPopulator)
				.setLocalizedString(SimonFacadesConstants.LOW_STOCK_ERROR);
		extCartModificationDataPopulator.populate(cartModel, cartModificationData);
		Assert.assertEquals("lowStock", cartModificationData.getStatusCode());
		verify(cartModel, times(1)).getEntries();
		verify(abstractOrderEntryModel, times(1)).getQuantity();
	}
}
