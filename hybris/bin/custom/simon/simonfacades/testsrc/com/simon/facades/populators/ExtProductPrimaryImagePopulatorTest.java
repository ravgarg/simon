package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import junit.framework.Assert;


@UnitTest
public class ExtProductPrimaryImagePopulatorTest
{
	@InjectMocks
	private ExtProductPrimaryImagePopulator extProductPrimaryImagePopulator;

	@Mock
	private ModelService modelService;

	@Mock
	private VariantProductModel productModel;

	private final ProductData productData = new ProductData();

	private final List<MediaContainerModel> mediaContainerList = new ArrayList<MediaContainerModel>();

	private final MediaContainerModel mediaContainer = new MediaContainerModel();

	private final List<MediaModel> medias = new ArrayList<MediaModel>();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPopulateWithEmptyContainerList()
	{
		Mockito.when(modelService.getAttributeValue(productModel, ProductModel.GALLERYIMAGES)).thenReturn(null);
		extProductPrimaryImagePopulator.populate(productModel, productData);
		Assert.assertNull("null images", productData.getImages());
	}

	@Test
	public void testPopulateWithEmptyMediaList()
	{
		mediaContainerList.add(mediaContainer);
		Mockito.when(modelService.getAttributeValue(productModel, ProductModel.GALLERYIMAGES)).thenReturn(mediaContainerList);
		extProductPrimaryImagePopulator.populate(productModel, productData);
		Assert.assertNull("null images", productData.getImages());
	}

	@Test
	public void testPopulateWithNoCartImage()
	{
		final MediaModel media = new MediaModel();
		final MediaFormatModel format = new MediaFormatModel();
		format.setName("100Wx450H", Locale.ENGLISH);
		media.setMediaFormat(format);
		medias.add(media);
		mediaContainer.setMedias(medias);
		mediaContainerList.add(mediaContainer);
		Mockito.when(modelService.getAttributeValue(productModel, ProductModel.GALLERYIMAGES)).thenReturn(mediaContainerList);
		extProductPrimaryImagePopulator.populate(productModel, productData);
		Assert.assertTrue("empty images", CollectionUtils.isEmpty(productData.getImages()));
	}

	@Test
	public void testPopulateWithNoValidCartImage()
	{
		final MediaModel media = new MediaModel();
		final MediaFormatModel format = new MediaFormatModel();
		format.setName("105Wx160H", Locale.ENGLISH);
		media.setMediaFormat(format);
		media.setURL("url");
		media.setAltText("alt");
		medias.add(media);
		mediaContainer.setMedias(medias);
		mediaContainerList.add(mediaContainer);
		Mockito.when(modelService.getAttributeValue(productModel, ProductModel.GALLERYIMAGES)).thenReturn(mediaContainerList);
		extProductPrimaryImagePopulator.populate(productModel, productData);
		Assert.assertNotNull("not null images", productData.getImages());
	}

}
