package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.commerceservices.util.ConverterFactory;
import de.hybris.platform.converters.impl.AbstractPopulatingConverter;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


@UnitTest
public class ExtSolrSearchQueryEncoderPopulatorTest
{
	protected static final String FREE_TEXT_SEARCH = "free text search";
	protected static final String SORT = "sort";
	protected static final String KEY1 = "key1";
	protected static final String KEY2 = "key2";
	protected static final String VALUE1 = "value1";
	protected static final String VALUE2 = "value2";

	private final AbstractPopulatingConverter<SolrSearchQueryData, SearchQueryData> solrSearchQueryEncoder = new ConverterFactory<SolrSearchQueryData, SearchQueryData, ExtSolrSearchQueryEncoderPopulator>()
			.create(SearchQueryData.class, new ExtSolrSearchQueryEncoderPopulator());
	@Before
	public void setUp()
	{
		//Do Nothing
	}

	@Test
	public void testConvertNull()
	{
		final SearchQueryData result = solrSearchQueryEncoder.convert(null);
		Assert.assertEquals("", result.getValue());
	}

	@Test
	public void testConvertEmpty()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals("", result.getValue());
	}

	@Test
	public void testConvertEmptyTerms()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setFilterTerms(Collections.<SolrSearchQueryTermData> emptyList());
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals("", result.getValue());
	}

	@Test
	public void testConvertEmptyTerms2()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SolrSearchQueryTermData searchQueryTermData = new SolrSearchQueryTermData();
		searchQueryData.setFilterTerms(Collections.singletonList(searchQueryTermData));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals("", result.getValue());
	}

	@Test
	public void testConvertFreeText()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setFreeTextSearch(FREE_TEXT_SEARCH);
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals(FREE_TEXT_SEARCH + ":", result.getValue());
	}

	@Test
	public void testConvertSort()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setSort(SORT);
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals(":" + SORT, result.getValue());
	}

	@Test
	public void testConvertTerms1()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SolrSearchQueryTermData searchQueryTermData = new SolrSearchQueryTermData();
		searchQueryTermData.setKey(KEY1);
		searchQueryTermData.setValue(VALUE1);
		searchQueryData.setFilterTerms(Collections.singletonList(searchQueryTermData));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals("::key1:value1", result.getValue());
	}

	@Test
	public void testConvertTerms2()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SolrSearchQueryTermData searchQueryTermData1 = new SolrSearchQueryTermData();
		searchQueryTermData1.setKey(IndexedKeys.CATEGORY_PATH);
		searchQueryTermData1.setValue(VALUE1);
		final SolrSearchQueryTermData searchQueryTermData2 = new SolrSearchQueryTermData();
		searchQueryTermData2.setKey(IndexedKeys.CATEGORY_PATH);
		searchQueryTermData2.setValue(VALUE2);

		searchQueryData.setFilterTerms(Arrays.asList(searchQueryTermData1, searchQueryTermData2));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals("::categoryPath:value2", result.getValue());
	}

	@Test
	public void testConvertTerms3()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SolrSearchQueryTermData searchQueryTermData = new SolrSearchQueryTermData();
		searchQueryTermData.setKey(KEY1);
		searchQueryTermData.setValue("5:'text'");
		searchQueryData.setFilterTerms(Collections.singletonList(searchQueryTermData));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		// Verifying special characters encoding for : '
		Assert.assertEquals("::key1:5%3A%27text%27", result.getValue());
	}

	@Test
	public void testConvertTerms4()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final SolrSearchQueryTermData searchQueryTermData1 = new SolrSearchQueryTermData();
		searchQueryTermData1.setKey(KEY1);
		searchQueryTermData1.setValue("7+-&&||!(){}text");
		final SolrSearchQueryTermData searchQueryTermData2 = new SolrSearchQueryTermData();
		searchQueryTermData2.setKey(KEY2);
		searchQueryTermData2.setValue("8[]^\"~*?:\\/text");

		searchQueryData.setFilterTerms(Arrays.asList(searchQueryTermData1, searchQueryTermData2));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		// Verifying special characters encoding for + - && || ! ( ) { } [ ] ^ " ~ * ? : \
		Assert.assertEquals("::key1:7%2B-%26%26%7C%7C%21%28%29%7B%7Dtext:key2:8%5B%5D%5E%22%7E*%3F%3A%5C%2Ftext",
				result.getValue());
	}

	@Test
	public void testConvertAll()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setFreeTextSearch(FREE_TEXT_SEARCH);
		searchQueryData.setSort(SORT);
		final SolrSearchQueryTermData searchQueryTermData1 = new SolrSearchQueryTermData();
		searchQueryTermData1.setKey(KEY1);
		searchQueryTermData1.setValue(VALUE1);
		final SolrSearchQueryTermData searchQueryTermData2 = new SolrSearchQueryTermData();
		searchQueryTermData2.setKey(KEY2);
		searchQueryTermData2.setValue(VALUE2);

		searchQueryData.setFilterTerms(Arrays.asList(searchQueryTermData1, searchQueryTermData2));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		Assert.assertEquals(FREE_TEXT_SEARCH + ":" + SORT + ":key1:value1:key2:value2", result.getValue());
	}

	@Test
	public void testConvertAllWithSpecialCharacters()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		searchQueryData.setFreeTextSearch(FREE_TEXT_SEARCH);
		searchQueryData.setSort(SORT);
		final SolrSearchQueryTermData searchQueryTermData1 = new SolrSearchQueryTermData();
		searchQueryTermData1.setKey(KEY1);
		searchQueryTermData1.setValue("7+-&&||!(){}text");
		final SolrSearchQueryTermData searchQueryTermData2 = new SolrSearchQueryTermData();
		searchQueryTermData2.setKey(KEY2);
		searchQueryTermData2.setValue("8[]^\"~*?:\\/text");

		searchQueryData.setFilterTerms(Arrays.asList(searchQueryTermData1, searchQueryTermData2));
		final SearchQueryData result = solrSearchQueryEncoder.convert(searchQueryData);
		// Verifying special characters encoding for + - && || ! ( ) { } [ ] ^ " ~ * ? : \ with free text search and sort
		Assert.assertEquals(
				FREE_TEXT_SEARCH + ":" + SORT + ":key1:7%2B-%26%26%7C%7C%21%28%29%7B%7Dtext:key2:8%5B%5D%5E%22%7E*%3F%3A%5C%2Ftext",
				result.getValue());
	}
}
