package com.simon.facades.message.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.services.CustomMessageService;


/**
 * Test Class for SimonMessageFacadeImpl
 */
@UnitTest
public class CustomMessageFacadeTest
{
	@InjectMocks
	CustomMessageFacadeImpl customMessageFacade;

	@Mock
	CustomMessageService customMessageService;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test to check correct string is returned from facade
	 */
	@Test
	public void testMessageReturnedFromService()
	{
		final String messageCode = "com.text.code";
		final String messageText = "Customize Label";
		when(customMessageService.getMessageForCode(Matchers.anyString())).thenReturn(messageText);
		assertEquals(messageText, customMessageFacade.getMessageTextForCode(messageCode));
	}


}
