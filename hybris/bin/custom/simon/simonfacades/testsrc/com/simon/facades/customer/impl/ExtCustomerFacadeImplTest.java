package com.simon.facades.customer.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.Model;

import com.google.common.collect.Lists;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.impl.DefaultShopService;
import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.MallModel;
import com.simon.core.model.SizeModel;
import com.simon.core.services.ExtProductService;
import com.simon.core.services.ExtShopService;
import com.simon.core.services.MallService;
import com.simon.core.services.impl.DefaultDesignerService;
import com.simon.core.services.impl.ExtDealServiceImpl;
import com.simon.core.util.CommonUtils;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.checkout.data.StateData;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.customer.data.MonthData;
import com.simon.facades.favorite.data.FavoriteData;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.product.data.GenderData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationService;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.services.UserIntegrationService;

import atg.taglib.json.util.JSONException;
import atg.taglib.json.util.JSONObject;




/**
 * Implementation for the ExtCustomerFacadetest
 */
@UnitTest
public class ExtCustomerFacadeImplTest
{
	private static final String PRODUCT_CODE = "qwe";
	private static final String TEST_DUMMY = "dummy";
	private static final String TEST_DUMMY_GENDER = "FEMALE";
	private static final String TEST_DUMMY_MONTH = "JAN";
	private static final Integer TEST_DUMMY_YEAR = 1999;
	private static final String TEST_DUMMY_COUNTRY = "US";
	private static final String TEST_DUMMY_ZIP = "12911";

	@Spy
	@InjectMocks
	private ExtCustomerFacadeImpl extCustomerFacade;
	@Mock
	private ModelService mockModelService;
	@Mock
	private CustomerAccountService customerAccountService;
	@Mock
	private CustomerNameStrategy customerNameStrategy;
	@Mock
	private Converter<CountryModel, CountryData> countryConverter;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private ExtCustomerAccountService extCustomerAccountService;

	@Mock
	private EnumerationService enumerationServices;
	@Mock
	private UserIntegrationService userIntegrationService;

	@Mock
	private Converter<Months, MonthData> defaultMonthConverter;
	@Mock
	private UserService userService;
	@Mock
	private ExtShopService extShopService;
	@Mock
	private Converter<CustomerGender, GenderData> customerGenderConverter;

	@Mock
	private Populator<RegisterData, CustomerModel> registerCustomerReversePopulator;

	@Mock
	private Populator<AddressData, AddressModel> addressReversePopulator;

	@Mock
	private MallService mallService;

	@Mock
	private UserFavouritesIntegrationService userFavouritesIntegrationService;
	@Mock
	private I18NFacade i18NFacade;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private ExtCheckoutFacade extCheckoutFacade;

	@Mock
	private Configuration configuration;

	@Mock
	private Converter<ProductModel, ProductData> productConverter;
	@Mock
	private DefaultShopService defaultShopService;
	@Mock
	private DefaultDesignerService defaultDesignerService;
	@Mock
	private Converter<ShopModel, StoreData> extStoreConverter;

	@Mock
	private Converter<DesignerModel, DesignerData> extDesignerConverter;
	@Mock
	private Converter<MallModel, MallData> mallConverter;
	@Mock
	private CommonUtils commonUtils;
	@Mock
	private Converter<CustomerModel, CustomerData> customerConverter;

	@Mock
	private UserCenterIdDTO userCenterIdDTO;

	@Mock
	private SessionService sessionService;

	@Mock
	private ProductSearchFacade<ProductData> productSearchFacade;
	@Mock
	private ProductSearchPageData<SearchStateData, ProductData> searchPageData;


	private RegionData usRegionData;
	private RegionData ukRegionData;
	private List<String> listOfExcludedStatesList;
	private List<RegionData> listOfRegionData;


	@Mock
	private ExtDealServiceImpl extDealService;

	@Mock
	private Converter<DealModel, DealModel> dealConverter;

	@Mock
	private Converter<DealModel, DealData> extDealConverter;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		listOfExcludedStatesList = new ArrayList<>();
		listOfExcludedStatesList.add("US-HI");
		listOfExcludedStatesList.add("UK-TEST");

		usRegionData = new RegionData();
		usRegionData.setIsocode("US-TEST");
		usRegionData.setName("US_Test");

		ukRegionData = new RegionData();
		ukRegionData.setIsocode("UK-TEST");
		ukRegionData.setName("UK_Test");

		listOfRegionData = new ArrayList<>();
		listOfRegionData.add(usRegionData);
		listOfRegionData.add(ukRegionData);

		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false)).thenReturn(false);
		when(configuration.getString(Mockito.anyString())).thenReturn("US");
		when(extCheckoutFacade.getExcludedStateList()).thenReturn(listOfExcludedStatesList);
		when(i18NFacade.getRegionsForCountryIso(Mockito.anyString())).thenReturn(listOfRegionData);

	}


	/**
	 * Test method for Register Shopper
	 *
	 * @throws DuplicateUidException
	 * @throws UserIntegrationException
	 * @throws AuthLoginIntegrationException
	 */
	@Test
	public void testRegisterShopper() throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(TEST_DUMMY);
		data.setLastName(TEST_DUMMY);
		data.setLogin(TEST_DUMMY);
		data.setPassword(TEST_DUMMY);
		data.setGender(TEST_DUMMY_GENDER);
		data.setBirthMonth(TEST_DUMMY_MONTH);
		data.setBirthYear(TEST_DUMMY_YEAR);
		data.setCountry(TEST_DUMMY_COUNTRY);
		data.setZipcode(TEST_DUMMY_ZIP);
		final MallData primaryMall = new MallData();
		primaryMall.setCode("Code");
		primaryMall.setName("Name");
		data.setPrimaryMall(primaryMall);
		final MallModel mall = Mockito.mock(MallModel.class);
		Mockito.when(mallService.getMallForCode(Mockito.anyString())).thenReturn(mall);
		final CustomerModel model = new CustomerModel();
		final VIPUserDTO vipUser = new VIPUserDTO();
		vipUser.setId("12");
		Mockito.when(userIntegrationService.registerUser(data)).thenReturn(vipUser);
		given(mockModelService.create(CustomerModel.class)).willReturn(model);
		extCustomerFacade.registerShopper(data, true);
		Mockito.verify(customerAccountService).register(model, null);

	}

	/**
	 * Test method for Register Shopper
	 *
	 * @throws DuplicateUidException
	 * @throws UserIntegrationException
	 * @throws AuthLoginIntegrationException
	 */
	@Test
	public void testregisterGuestCustomerShopper()
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(TEST_DUMMY);
		data.setLastName(TEST_DUMMY);
		data.setLogin(TEST_DUMMY);
		data.setPassword(TEST_DUMMY);
		data.setGender(TEST_DUMMY_GENDER);
		data.setBirthMonth(TEST_DUMMY_MONTH);
		data.setBirthYear(TEST_DUMMY_YEAR);
		data.setCountry(TEST_DUMMY_COUNTRY);
		data.setZipcode(TEST_DUMMY_ZIP);
		final CustomerModel model = new CustomerModel();
		final VIPUserDTO vipUser = new VIPUserDTO();
		vipUser.setId("12");
		Mockito.when(userIntegrationService.registerUser(data)).thenReturn(vipUser);
		given(mockModelService.create(CustomerModel.class)).willReturn(model);
		given(userIntegrationService.getDefaultCenter(data.getZipcode())).willReturn(userCenterIdDTO);
		given(userCenterIdDTO.getId()).willReturn("123");
		given(userCenterIdDTO.getOutletName()).willReturn("test");

		extCustomerFacade.registerGuestCustomer(data, "ordercode");
		Mockito.verify(extCustomerAccountService).convertGuestToRegisterCustomer(model, "ordercode");
	}

	@Test
	public void testRegisterShopperWithExistingUser()
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(TEST_DUMMY);
		data.setLastName(TEST_DUMMY);
		data.setLogin(TEST_DUMMY);
		data.setPassword(TEST_DUMMY);
		data.setGender(TEST_DUMMY_GENDER);
		data.setBirthMonth(TEST_DUMMY_MONTH);
		data.setBirthYear(TEST_DUMMY_YEAR);
		data.setCountry(TEST_DUMMY_COUNTRY);
		data.setZipcode(TEST_DUMMY_ZIP);
		final CustomerModel model = new CustomerModel();
		given(mockModelService.create(CustomerModel.class)).willReturn(model);
		extCustomerFacade.registerShopper(data, false);
		Mockito.verify(customerAccountService).register(model, null);

	}

	/**
	 * Test to return the list of Month from Facade
	 */

	@Test
	public void testGetMonthList()
	{

		final Months months = Months.APR;
		final List<Months> month = new ArrayList<Months>();
		month.add(months);
		final MonthData monthsData = Mockito.mock(MonthData.class);
		final List<MonthData> monthDatas = new ArrayList<MonthData>();
		monthDatas.add(monthsData);
		Mockito.when(enumerationServices.getEnumerationValues(Months.class)).thenReturn(month);
		Mockito.when(defaultMonthConverter.convertAll(month)).thenReturn(monthDatas);
		Assert.assertEquals(extCustomerFacade.getMonthsList(), monthDatas);
	}

	/**
	 * Test to return the list of Gender from Facade
	 */
	@Test
	public void testGetGenderList()
	{

		final CustomerGender gender = CustomerGender.FEMALE;
		final List<CustomerGender> genderList = new ArrayList<CustomerGender>();
		genderList.add(gender);
		final GenderData genderdata = Mockito.mock(GenderData.class);
		final List<GenderData> genderDataList = new ArrayList<GenderData>();
		genderDataList.add(genderdata);
		Mockito.when(enumerationServices.getEnumerationValues(CustomerGender.class)).thenReturn(genderList);
		Mockito.when(customerGenderConverter.convertAll(genderList)).thenReturn(genderDataList);
		Assert.assertEquals(extCustomerFacade.getGenderList(), genderDataList);
	}

	/**
	 * Tests the all country from the ext facade
	 */
	@Test
	public void testGetAllCountryUS()
	{

		final CountryModel country = Mockito.mock(CountryModel.class);
		country.setIsocode("US");
		final List<CountryModel> countries = new ArrayList<CountryModel>();
		countries.add(country);
		final CountryData countryData = new CountryData();
		countryData.setIsocode("US");
		countryData.setName("Test");
		final List<CountryData> countriesData = new ArrayList<>();
		countriesData.add(countryData);
		Mockito.when(commonI18NService.getAllCountries()).thenReturn(countries);
		Mockito.when(countryConverter.convertAll(countries)).thenReturn(countriesData);
		final List<CountryData> countriesfromFacade = extCustomerFacade.getAllCountry();
		Assert.assertEquals(countriesData, countriesfromFacade);

	}

	/**
	 * Tests the all country from the ext facade
	 */
	@Test
	public void testGetAllCountryCA()
	{

		final CountryModel country = Mockito.mock(CountryModel.class);
		country.setIsocode("CA");
		final List<CountryModel> countries = new ArrayList<CountryModel>();
		countries.add(country);
		final CountryData countryData = new CountryData();
		countryData.setIsocode("CA");
		countryData.setName("Test");
		final List<CountryData> countriesData = new ArrayList<>();
		countriesData.add(countryData);
		Mockito.when(commonI18NService.getAllCountries()).thenReturn(countries);
		Mockito.when(countryConverter.convertAll(countries)).thenReturn(countriesData);
		final List<CountryData> countriesfromFacade = extCustomerFacade.getAllCountry();
		Assert.assertEquals(countriesData, countriesfromFacade);

	}

	/**
	 * Tests the all country from the ext facade
	 */
	@Test
	public void testGetAllCountryOthers()
	{

		final CountryModel country = Mockito.mock(CountryModel.class);
		country.setIsocode("ZWE");
		final List<CountryModel> countries = new ArrayList<CountryModel>();
		countries.add(country);
		final CountryData countryData = new CountryData();
		countryData.setIsocode("ZWE");
		countryData.setName("Test");
		final List<CountryData> countriesData = new ArrayList<>();
		countriesData.add(countryData);
		Mockito.when(commonI18NService.getAllCountries()).thenReturn(countries);
		Mockito.when(countryConverter.convertAll(countries)).thenReturn(countriesData);
		final List<CountryData> countriesfromFacade = extCustomerFacade.getAllCountry();
		Assert.assertEquals(countriesData, countriesfromFacade);

	}



	@Test
	public void testRegisterShopperWithMissingData()
			throws DuplicateUidException, UserIntegrationException, AuthLoginIntegrationException
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(TEST_DUMMY);
		data.setLastName(TEST_DUMMY);
		data.setLogin(TEST_DUMMY);
		data.setPassword(TEST_DUMMY);
		data.setBirthYear(TEST_DUMMY_YEAR);
		data.setZipcode(TEST_DUMMY_ZIP);
		final CustomerModel model = new CustomerModel();
		final VIPUserDTO vipUser = new VIPUserDTO();
		vipUser.setId("12");
		given(userIntegrationService.registerUser(data)).willReturn(vipUser);
		given(mockModelService.create(CustomerModel.class)).willReturn(model);
		extCustomerFacade.registerShopper(data, false);
		Mockito.verify(customerAccountService).register(model, null);

	}

	/**
	 * Method to test getStateDataListForShippingAddress() in valid list scenario.
	 *
	 */
	@Test
	public void testGetStateData_Valid_List_Scenario()
	{
		final List<StateData> listOfStateData = extCustomerFacade.getStateDataListForShippingAddress();

		Assert.assertTrue(!listOfStateData.isEmpty());
	}

	/**
	 * Method to test update Favorite for user.
	 */
	@Test
	public void testupdateMyFavorite()
	{
		final String productCode = "code";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		customerModel.setMyFavProduct(productset);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		given(extProductService.getProductForCode(productCode)).willReturn(model);
		Assert.assertEquals(extCustomerFacade.updateMyFavorite(product), true);

	}

	@Test
	public void testupdateMyFavoriteThowsException()
	{
		final String productCode = "code";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		customerModel.setMyFavProduct(productset);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		given(extProductService.getProductForCode(productCode)).willReturn(model);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateCustomerMyFavorite(customerModel);
		Assert.assertEquals(extCustomerFacade.updateMyFavorite(product), false);

	}

	/**
	 * Method to test add Favorite for user.
	 */
	@Test
	public void testaddToMyFavorite()
	{
		final String productCode = "code";
		final String productName = "name";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(extProductService.getProductForCode(productCode)).thenReturn(model);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		given(extProductService.getProductForCode(productCode)).willReturn(model);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteProduct(productCode, productName)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.addToMyFavorite(productCode, productName), true);
	}


	@Test
	public void testaddToMyFavoriteModelSavingException()
	{
		final String productCode = "code";
		final String productName = "name";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(extProductService.getProductForCode(productCode)).thenReturn(model);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		given(extProductService.getProductForCode(productCode)).willReturn(model);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateCustomerMyFavorite(customerModel);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteProduct(productCode, productName)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.addToMyFavorite(productCode, productName), false);
	}

	/**
	 * Method to test remove Favorite for user.
	 */
	@Test
	public void testremoveFromMyFavorite()
	{
		final String productCode = "code";
		final String productName = "name";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		Mockito.doNothing().when(customerModel).setMyFavProduct(productset);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteProduct(productCode)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.removeFromMyFavorite(productCode, productName), true);
	}


	@Test
	public void testremoveFromMyFavoriteModelSavingException()
	{
		final String productCode = "code";
		final String productName = "name";
		final ProductModel model = new ProductModel();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		Mockito.doNothing().when(customerModel).setMyFavProduct(productset);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteProduct(productCode)).thenReturn(true);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateCustomerMyFavorite(customerModel);
		Assert.assertEquals(extCustomerFacade.removeFromMyFavorite(productCode, productName), false);
	}

	/**
	 * Method to test view My Favorite for user.
	 */
	@Test
	public void testgetMyFavorite()
	{
		final ProductModel model = new ProductModel();
		model.setCode(PRODUCT_CODE);
		final ProductData data = new ProductData();
		final List<String> productCodes = new ArrayList<String>();
		productCodes.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final List<ProductData> productData = new ArrayList<>();
		productData.add(data);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		Assert.assertEquals(extCustomerFacade.getMyFavoriteProductCodes(), productCodes);
	}

	/**
	 * Method to test view My Favorite for user.
	 */
	@Test
	public void testgetNoMyFavorite()
	{
		final ProductModel model = new ProductModel();
		final ProductData data = new ProductData();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final List<ProductData> productData = new ArrayList<>();
		productData.add(data);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		Assert.assertNull(extCustomerFacade.getMyFavoriteProductCodes().get(0));
	}

	/**
	 * Method to test add Favorite designers for user.
	 */
	@Test
	public void testAddToMyDesigners()
	{
		final String designerCode = "code";
		final String name = "name";
		final DesignerModel model = Mockito.mock(DesignerModel.class);
		final List<String> designer = new ArrayList<String>();
		designer.add(PRODUCT_CODE);
		final Set<DesignerModel> designerSet = new HashSet<DesignerModel>();
		designerSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(defaultDesignerService.getDesignerForCode(designerCode)).thenReturn(model);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteDesigner(designerCode, name)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.addToMyDesigners(designerCode, name), true);
	}



	/**
	 * Method to test remove Favorite for user.
	 */
	@Test
	public void testRemoveFromMyDesigners()
	{
		final String designerCode = "code";
		final String name = "name";
		final DesignerModel model = new DesignerModel();
		final List<String> designer = new ArrayList<String>();
		designer.add(PRODUCT_CODE);
		final Set<DesignerModel> designerSet = new HashSet<>();
		designerSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.doNothing().when(customerModel).setMyDesigners(designerSet);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteDesigner(designerCode)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.removeFromMyDesigners(designerCode), true);
	}


	@Test
	public void testRemoveFromMyDesignersThrowsException()
	{
		final String designerCode = "code";
		final String name = "name";
		final DesignerModel model = new DesignerModel();
		final List<String> designer = new ArrayList<String>();
		designer.add(PRODUCT_CODE);
		final Set<DesignerModel> designerSet = new HashSet<>();
		designerSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.doNothing().when(customerModel).setMyDesigners(designerSet);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteDesigner(designerCode)).thenReturn(true);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateCustomerMyFavorite(customerModel);
		Assert.assertEquals(extCustomerFacade.removeFromMyDesigners(designerCode), false);
	}

	/**
	 * Method to test add Favorite designers for user.
	 */
	@Test
	public void testAddToMyStores()
	{
		final String storeId = "code";
		final String name = "name";
		final ShopModel model = Mockito.mock(ShopModel.class);
		final List<String> store = new ArrayList<String>();
		store.add(PRODUCT_CODE);
		final Set<ShopModel> storeSet = new HashSet<ShopModel>();
		storeSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(defaultShopService.getShopForId(storeId)).thenReturn(model);
		Mockito.when(customerModel.getShops()).thenReturn(storeSet);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteStore(storeId, name)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.addToMyStores(storeId, name), true);
	}

	@Test
	public void testAddToMyStoresThrowsException() throws DuplicateUidException
	{
		final String storeId = "code";
		final String name = "name";
		final ShopModel model = Mockito.mock(ShopModel.class);
		final List<String> store = new ArrayList<String>();
		store.add(PRODUCT_CODE);
		final Set<ShopModel> storeSet = new HashSet<ShopModel>();
		storeSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(defaultShopService.getShopForId(storeId)).thenReturn(model);
		Mockito.when(customerModel.getShops()).thenReturn(storeSet);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteStore(storeId, name)).thenReturn(true);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateProfileInfo(customerModel);
		Assert.assertEquals(extCustomerFacade.addToMyStores(storeId, name), false);
	}

	/**
	 * Method to test remove Favorite for user.
	 */
	@Test
	public void testRemoveFromMyStores()
	{
		final String storeId = "code";
		final ShopModel model = new ShopModel();
		final List<String> store = new ArrayList<String>();
		store.add(PRODUCT_CODE);
		final Set<ShopModel> storeSet = new HashSet<ShopModel>();
		storeSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getShops()).thenReturn(storeSet);
		Mockito.doNothing().when(customerModel).setShops(storeSet);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteStore(storeId)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.removeFromMyStores(storeId), true);
	}

	@Test
	public void testRemoveFromMyStoresThrowsException()
	{
		final String storeId = "code";
		final ShopModel model = new ShopModel();
		final List<String> store = new ArrayList<String>();
		store.add(PRODUCT_CODE);
		final Set<ShopModel> storeSet = new HashSet<ShopModel>();
		storeSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getShops()).thenReturn(storeSet);
		Mockito.doNothing().when(customerModel).setShops(storeSet);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteStore(storeId)).thenReturn(true);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateCustomerMyFavorite(customerModel);
		Assert.assertEquals(extCustomerFacade.removeFromMyStores(storeId), false);
	}


	/**
	 * Test method to update stores for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testUpdateStoreToProfile() throws DuplicateUidException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final ShopModel shopModel = Mockito.mock(ShopModel.class);

		final Set<ShopModel> shopSet = new HashSet<ShopModel>();
		final List<ShopModel> shopModels = new ArrayList<ShopModel>();
		final String[] storeIds =
		{ "id" };
		shopModels.add(shopModel);
		shopSet.add(shopModel);
		Mockito.when(customerData.getUid()).thenReturn("id");
		Mockito.when(extShopService.getListOfShopsForCodes(storeIds)).thenReturn(shopModels);
		Mockito.when(userService.getUserForUID("id")).thenReturn(customerModel);
		Mockito.when(customerModel.getShops()).thenReturn(shopSet);
		shopSet.add(shopModel);
		Mockito.doNothing().when(extCustomerAccountService).updateProfileInfo(customerModel);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteStore("code", "code")).thenReturn(true);
		extCustomerFacade.updateStoreToProfile(customerData, storeIds);
		Mockito.verify(userFavouritesIntegrationService).addUserFavouriteStore(Mockito.anyString(), Mockito.anyString());
	}

	/**
	 * Test method to fetch all stores except linked for user.
	 */
	@Test
	public void testFetchAllStoresExceptLinked()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final ShopModel shopModel = Mockito.mock(ShopModel.class);
		final List<ShopModel> shopModels = new ArrayList<ShopModel>();
		customerData.setUid("id");
		shopModels.add(shopModel);
		Mockito.when(extCustomerAccountService.findListOfAllStores(customerModel)).thenReturn(shopModels);
		Mockito.when(customerData.getUid()).thenReturn("id");
		Mockito.when(userService.getUserForUID("id")).thenReturn(customerModel);
		extCustomerFacade.fetchAllStoresExceptLinked(customerData);
		Mockito.verify(extCustomerAccountService).findListOfAllStores(customerModel);
	}

	/**
	 * Test method to fetch all stores for user no store.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testGetNoFavouriteStores() throws DuplicateUidException
	{
		final Converter<CustomerModel, CustomerData> extCustomerConverter = Mockito.mock(Converter.class);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final ShopModel shopModel = Mockito.mock(ShopModel.class);
		final Set<ShopModel> shopSet = new HashSet<ShopModel>();
		final List<ShopModel> shopModels = new ArrayList<ShopModel>();
		final String[] stores =
		{ "asdas" };
		final List<String> favStores = new ArrayList<String>();

		shopModels.add(shopModel);
		shopSet.add(shopModel);
		Mockito.when(customerData.getUid()).thenReturn("id");
		Mockito.when(userService.getUserForUID("id")).thenReturn(customerModel);
		Mockito.when(extShopService.getListOfShopsForCodes(stores)).thenReturn(shopModels);
		Mockito.doNothing().when(customerModel).setShops(shopSet);
		customerModel.setShops(shopSet);
		Mockito.doNothing().when(extCustomerAccountService).updateProfileInfo(customerModel);
		extCustomerFacade.getAllFavouriteStores(customerData);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(customerModel);
	}

	/**
	 * Test method to fetch all stores for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testGetAllFavouriteStores() throws DuplicateUidException
	{
		final Converter<CustomerModel, CustomerData> extCustomerConverter = Mockito.mock(Converter.class);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final ShopModel shopModel = Mockito.mock(ShopModel.class);
		final Set<ShopModel> shopSet = new HashSet<ShopModel>();
		final List<ShopModel> shopModels = new ArrayList<ShopModel>();
		final Object[] stores =
		{ "asdas" };
		final List<String> favStores = new ArrayList<String>();
		favStores.add("testStore");

		shopModels.add(shopModel);
		shopSet.add(shopModel);
		Mockito.when(customerData.getUid()).thenReturn("id");
		Mockito.when(userService.getUserForUID("id")).thenReturn(customerModel);

		Mockito.when(userFavouritesIntegrationService.getAllFavouriteStores()).thenReturn(favStores);
		Mockito.when(extShopService.getListOfShopsForCodes(Mockito.any())).thenReturn(shopModels);
		Mockito.doNothing().when(customerModel).setShops(shopSet);
		customerModel.setShops(shopSet);
		Mockito.doNothing().when(extCustomerAccountService).updateProfileInfo(customerModel);
		extCustomerFacade.getAllFavouriteStores(customerData);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(customerModel);
	}

	/**
	 * Test method to update designers for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testUpdateDesignersToProfile() throws DuplicateUidException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final DesignerModel model = Mockito.mock(DesignerModel.class);
		final List<String> designer = new ArrayList<String>();
		final List<DesignerModel> designerModels = new ArrayList<DesignerModel>();
		final String[] designerIds =
		{ "id" };
		final Set<DesignerModel> designerSet = new HashSet<DesignerModel>();
		designerModels.add(model);
		designer.add(PRODUCT_CODE);
		designerSet.addAll(designerModels);
		customerModel.setMyDesigners(designerSet);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.when(defaultDesignerService.getListOfDesignerForCodes(Arrays.asList(designerIds))).thenReturn(designerModels);
		extCustomerFacade.updateDesignersToProfile(designerIds);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(customerModel);
	}

	@Test
	public void testUpdateDesignersToProfileThrowsModelException() throws DuplicateUidException
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final DesignerModel model = Mockito.mock(DesignerModel.class);
		final List<String> designer = new ArrayList<String>();
		final List<DesignerModel> designerModels = new ArrayList<DesignerModel>();
		final String[] designerIds =
		{ "id" };
		final Set<DesignerModel> designerSet = new HashSet<DesignerModel>();
		designerModels.add(model);
		designer.add(PRODUCT_CODE);
		designerSet.addAll(designerModels);
		customerModel.setMyDesigners(designerSet);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.when(defaultDesignerService.getListOfDesignerForCodes(Arrays.asList(designerIds))).thenReturn(designerModels);
		Mockito.doThrow(ModelSavingException.class).when(extCustomerAccountService).updateProfileInfo(customerModel);
		Assert.assertEquals(extCustomerFacade.updateDesignersToProfile(designerIds), false);
	}

	/**
	 * Test method to update store for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testgetAllFavouriteNoDesigners() throws DuplicateUidException
	{
		final Model model1 = Mockito.mock(Model.class);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final DesignerModel model = Mockito.mock(DesignerModel.class);
		final List<String> designer = new ArrayList<String>();
		final List<DesignerModel> designerModels = new ArrayList<>();
		final List<DesignerData> designerDatas = new ArrayList<>();

		final Set<DesignerModel> designerSet = new HashSet<DesignerModel>();
		final Converter<CustomerModel, CustomerData> extCustomerConverter = Mockito.mock(Converter.class);
		designerModels.add(model);
		designer.add(PRODUCT_CODE);
		designerSet.addAll(designerModels);
		customerModel.setMyDesigners(designerSet);

		Mockito.when(customerData.getUid()).thenReturn("id");

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.when(extCustomerConverter.convert(customerModel, customerData)).thenReturn(customerData);
		Mockito.doNothing().when(sessionService).setAttribute(Mockito.anyString(), Mockito.any());
		Mockito.when(productSearchFacade.textSearch("", SearchQueryContext.DESIGNERS)).thenReturn(null);
		Mockito.when(extCustomerConverter.convert(customerModel, customerData)).thenReturn(customerData);
		extCustomerFacade.getAllFavouriteDesigners(designerDatas);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(customerModel);
	}

	/**
	 * Test method to update store for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testgetAllFavouriteDesigners() throws DuplicateUidException
	{
		final Model model1 = Mockito.mock(Model.class);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final DesignerModel model = Mockito.mock(DesignerModel.class);
		final List<String> designer = new ArrayList<String>();
		final List<DesignerModel> designerModels = new ArrayList<DesignerModel>();


		final Set<DesignerModel> designerSet = new HashSet<DesignerModel>();
		final Converter<CustomerModel, CustomerData> extCustomerConverter = Mockito.mock(Converter.class);
		designerModels.add(model);
		designer.add(PRODUCT_CODE);
		designerSet.addAll(designerModels);
		customerModel.setMyDesigners(designerSet);
		final List<DesignerData> designerDatas = new ArrayList<>();
		Mockito.when(defaultDesignerService.getListOfDesignerForCodes(Mockito.any())).thenReturn(designerModels);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyDesigners()).thenReturn(designerSet);
		Mockito.when(userFavouritesIntegrationService.getAllFavouriteDesigners()).thenReturn(designer);
		Mockito.doNothing().when(sessionService).setAttribute(Mockito.anyString(), Mockito.any());
		Mockito.when(productSearchFacade.textSearch("", SearchQueryContext.DESIGNERS)).thenReturn(null);
		extCustomerFacade.getAllFavouriteDesigners(designerDatas);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(customerModel);
	}

	/**
	 * Test method for createJsonForFavouriteUnFavourite.
	 *
	 * @throws JSONException
	 */
	@Test
	public void createJsonForFavouriteUnFavouriteSuccess() throws JSONException
	{
		final JSONObject json = Mockito.mock(JSONObject.class);
		final boolean status = true;
		final String str = extCustomerFacade.createJsonForFavouriteUnFavourite("code", "name", status, "action");
		Assert.assertTrue(str.contains("success"));
	}

	/**
	 * Test method for createJsonForFavouriteUnFavourite.
	 *
	 * @throws JSONException
	 */
	@Test
	public void createJsonForFavouriteUnFavouriteUnsuccess() throws JSONException
	{
		final JSONObject json = Mockito.mock(JSONObject.class);
		final boolean status = false;
		final String str = extCustomerFacade.createJsonForFavouriteUnFavourite("code", "name", status, "action");
		Assert.assertTrue(str.contains("fail"));
	}


	@Test
	public void testCreateUpdateProfileInformation() throws UserIntegrationException, DuplicateUidException
	{

		final CustomerData customerData = new CustomerData();
		customerData.setBirthMonth(Months.APR);
		customerData.setBirthYear(TEST_DUMMY_YEAR);
		customerData.setFirstName(TEST_DUMMY);
		customerData.setLastName(TEST_DUMMY);
		customerData.setZipcode(TEST_DUMMY);
		final Populator<CustomerData, UserModel> extCustomerReversePopulator = Mockito.mock(Populator.class);
		when(extCustomerFacade.getExtCustomerReversePopulator()).thenReturn(extCustomerReversePopulator);
		customerData.setGender(CustomerGender.FEMALE);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		given(userService.getCurrentUser()).willReturn(customerModel);
		final MallModel alternateMall1 = Mockito.mock(MallModel.class);
		final MallModel alternateMall2 = Mockito.mock(MallModel.class);
		final Set<MallModel> alternateMalls = new HashSet<>();
		alternateMalls.add(alternateMall1);
		alternateMalls.add(alternateMall2);
		Mockito.when(customerModel.getAlternateMalls()).thenReturn(alternateMalls);
		final MallModel primaryMall = Mockito.mock(MallModel.class);
		Mockito.when(customerModel.getPrimaryMall()).thenReturn(primaryMall);
		final MallData primaryMallData = new MallData();
		primaryMallData.setCode("Test");
		primaryMallData.setName("TestName");
		Mockito.when(mallConverter.convert(primaryMall)).thenReturn(primaryMallData);
		final List<MallData> alternateMallDatas = new ArrayList();
		final MallData alternateMallData1 = new MallData();
		alternateMallData1.setCode("TestAlternate");
		alternateMallData1.setName("TestNameAlternate");
		alternateMallDatas.add(alternateMallData1);
		Mockito.when(mallConverter.convertAll(alternateMalls)).thenReturn(alternateMallDatas);
		extCustomerFacade.updateProfileInformation(customerData, Boolean.TRUE);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(Mockito.any());
	}

	@Test
	public void testCreateUpdateProfileInformationRegisteredUser() throws UserIntegrationException, DuplicateUidException
	{

		final CustomerData customerData = new CustomerData();
		customerData.setBirthMonth(Months.APR);
		customerData.setBirthYear(TEST_DUMMY_YEAR);
		customerData.setFirstName(TEST_DUMMY);
		customerData.setLastName(TEST_DUMMY);
		customerData.setZipcode(TEST_DUMMY);
		final Populator<CustomerData, UserModel> extCustomerReversePopulator = Mockito.mock(Populator.class);
		when(extCustomerFacade.getExtCustomerReversePopulator()).thenReturn(extCustomerReversePopulator);
		customerData.setGender(CustomerGender.FEMALE);
		extCustomerFacade.setUserService(userService);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		when(userService.getUserForUID(Mockito.anyString())).thenReturn(customerModel);
		extCustomerFacade.updateProfileInformation(customerData, Boolean.FALSE);
		Mockito.verify(extCustomerAccountService).updateProfileInfo(Mockito.any());
	}

	@Test
	public void testFetchAllDesignersExceptCustomerFavDesigners()
	{
		extCustomerFacade.setUserService(userService);
		final List<DesignerModel> designerList = new ArrayList();
		final DesignerModel designer = Mockito.mock(DesignerModel.class);
		designerList.add(designer);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		when(userService.getCurrentUser()).thenReturn(customerModel);
		when(extCustomerAccountService.findListOfAllDesigners(customerModel)).thenReturn(designerList);
		final List<DesignerData> designerDatas = new ArrayList();
		final DesignerData designerData = Mockito.mock(DesignerData.class);
		designerDatas.add(designerData);
		final Converter<CustomerModel, CustomerData> extCustomerConverter = Mockito.mock(Converter.class);
		Mockito.doNothing().when(sessionService).setAttribute(Mockito.anyString(), Mockito.any());
		Mockito.when(productSearchFacade.textSearch("", SearchQueryContext.DESIGNERS)).thenReturn(null);

		when(Converters.convertAll(designerList, extDesignerConverter)).thenReturn(designerDatas);
		Assert.assertNotNull(extCustomerFacade.fetchAllDesignersExceptCustomerFavDesigners());
	}

	@Test
	public void testUpdateMyFavorite()
	{
		final SessionService sessionService = Mockito.mock(SessionService.class);
		extCustomerFacade.setSessionService(sessionService);
		final FavoriteData favData = Mockito.mock(FavoriteData.class);
		when(sessionService.getAttribute("favoriteData")).thenReturn(favData);
		when(favData.getFavType()).thenReturn(FavoriteTypeEnum.PRODUCTS);
		when(favData.getFavId()).thenReturn(TEST_DUMMY);
		extCustomerFacade.updateFavorite("PRODUCTS");
		Mockito.verify(sessionService).removeAttribute("favoriteData");

	}

	@Test
	public void testGetCustomerPrimaryCenterLogoWithoutNull()
	{
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		final MallData mallData = Mockito.mock(MallData.class);
		given(userService.getCurrentUser()).willReturn(customer);
		final MallModel primaryMall = Mockito.mock(MallModel.class);
		when(customer.getPrimaryMall()).thenReturn(primaryMall);
		when(mallConverter.convert(primaryMall)).thenReturn(mallData);
		Assert.assertNotNull(extCustomerFacade.getCustomerPrimaryMallData());

	}

	@Test
	public void testGetCustomerPrimaryCenterLogoWithNull()
	{
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		final MallData mallData = null;
		given(userService.getCurrentUser()).willReturn(customer);
		when(customer.getPrimaryMall()).thenReturn(null);
		Assert.assertNull(extCustomerFacade.getCustomerPrimaryMallData());

	}


	/**
	 * Method to test fetch all the designer data
	 */
	@Test
	public void testFetchAllDesigners()
	{

		final List<DesignerModel> designerList = new ArrayList<DesignerModel>();
		final List<DesignerData> designerDatas = new ArrayList<DesignerData>();
		final DesignerData designerData = Mockito.mock(DesignerData.class);
		final DesignerModel designer = Mockito.mock(DesignerModel.class);
		designerList.add(designer);
		designerDatas.add(designerData);

		extCustomerFacade.fetchAllDesigners();
		Mockito.verify(extCustomerAccountService).findListOfDesigners();

	}

	/**
	 * Test Method to add fav designer value
	 */
	@Test
	public void testUpdateFavDesigners()
	{
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final List<DesignerData> designerDatas = new ArrayList<DesignerData>();
		final Set<DesignerData> customerDesigners = new HashSet<>();
		final DesignerData designerData = Mockito.mock(DesignerData.class);
		customerDesigners.add(designerData);
		Mockito.when(customerData.getMyDesigners()).thenReturn(customerDesigners);
		customerData.getMyDesigners();
		extCustomerFacade.updateFavDesigners(customerData, designerDatas);
		assertEquals(customerDesigners.size(), 1);
	}

	/**
	 * Test Method to add when no fav designer value
	 */
	@Test
	public void testUpdateNoFavDesigners()
	{
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final List<DesignerData> designerDatas = new ArrayList<DesignerData>();
		final Set<DesignerData> customerDesigners = new HashSet<>();
		Mockito.when(customerData.getMyDesigners()).thenReturn(customerDesigners);
		customerData.getMyDesigners();
		extCustomerFacade.updateFavDesigners(customerData, designerDatas);
		assertEquals(customerDesigners.size(), 0);
	}

	/**
	 * Method to test PopulateMyFavorite in valid case.
	 */
	@Test
	public void testPopulateMyFavorite()
	{
		final ProductData productData = new ProductData();
		productData.setBaseProduct("baseProduct");

		final List<ProductData> products = new ArrayList<>();
		products.add(productData);

		final Set<ProductModel> productset = new HashSet<>();
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setMyFavProduct(productset);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		final List<ProductData> result = extCustomerFacade.populateMyFavorite(products);

		Assert.assertTrue(!result.isEmpty());
	}

	/**
	 * Method to test isFavStore in valid case.
	 */
	@Test
	public void testIsFavStore()
	{
		final ProductData productData = new ProductData();
		productData.setBaseProduct("baseProduct");

		final List<ProductData> products = new ArrayList<>();
		products.add(productData);

		final ShopModel shopModel = new ShopModel();
		shopModel.setContentPolicy("ContentPolicy");
		shopModel.setId("storecode");

		final Set<ShopModel> setOfShops = new HashSet<>();
		setOfShops.add(shopModel);

		final Set<ProductModel> productset = new HashSet<>();
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setMyFavProduct(productset);
		customerModel.setShops(setOfShops);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		final boolean result = extCustomerFacade.isFavStore("storecode");

		Assert.assertTrue(result);
	}

	/**
	 * Method to test isFavDesigner in valid case.
	 */
	@Test
	public void testIsFavDesigner()
	{
		final ProductData productData = new ProductData();
		productData.setBaseProduct("baseProduct");

		final List<ProductData> products = new ArrayList<>();
		products.add(productData);

		final DesignerModel designerModel = new DesignerModel();
		designerModel.setCode("code");

		final Set<DesignerModel> setOfDesigMod = new HashSet<>();
		setOfDesigMod.add(designerModel);

		final Set<ProductModel> productset = new HashSet<>();
		final CustomerModel customerModel = new CustomerModel();
		customerModel.setMyFavProduct(productset);
		customerModel.setMyDesigners(setOfDesigMod);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);

		final boolean result = extCustomerFacade.isFavDesigner("code");

		Assert.assertTrue(result);
	}

	@Test
	public void testGetActiveRetailersDesignerList()
	{
		Mockito.when(productSearchFacade.textSearch(StringUtils.EMPTY, SearchQueryContext.DESIGNERS)).thenReturn(searchPageData);
		Mockito.when(searchPageData.getActiveDesigners()).thenReturn(Lists.newArrayList(new DesignerData()));
		Assert.assertEquals(extCustomerFacade.getActiveRetailersDesignerList().size(), 1);
	}


	/**
	 * Method to test add Favorite deal for user.
	 */
	@Test
	public void testAddFavDeal()
	{
		final String dealCode = "code";
		final String name = "name";
		final DealModel model = Mockito.mock(DealModel.class);
		final List<String> deal = new ArrayList<String>();
		deal.add(PRODUCT_CODE);
		final Set<DealModel> dealSet = new HashSet<DealModel>();
		dealSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(extDealService.getDealForCode(dealCode)).thenReturn(model);
		Mockito.when(customerModel.getMyOffers()).thenReturn(dealSet);
		Mockito.when(userFavouritesIntegrationService.addUserFavouriteOffer(dealCode, name)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.addFavDeal(dealCode, name), true);
	}

	/**
	 * Method to test remove Favorite deal for user.
	 */
	@Test
	public void testRemoveFavDeal()
	{
		final String dealCode = "code";
		final DealModel model = new DealModel();
		final List<String> designer = new ArrayList<String>();
		designer.add(PRODUCT_CODE);
		final Set<DealModel> dealSet = new HashSet<>();
		dealSet.add(model);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyOffers()).thenReturn(dealSet);
		Mockito.doNothing().when(customerModel).setMyOffers(dealSet);
		Mockito.when(userFavouritesIntegrationService.removeUserFavouriteOffer(dealCode)).thenReturn(true);
		Assert.assertEquals(extCustomerFacade.removeFavDeal(dealCode), true);
	}

	/**
	 * Method to test get all Favorite deal for user.
	 *
	 * @throws DuplicateUidException
	 */
	@Test
	public void testGetAllFavoriteDeals() throws DuplicateUidException
	{
		final DealModel model = new DealModel();
		final List<String> deal = new ArrayList<String>();
		final List<DealModel> dealList = new ArrayList<>();
		final Set<DealModel> dealSet = new HashSet<>();
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final DealData dealData = Mockito.mock(DealData.class);
		final List<DealData> deals = new ArrayList<>();
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		deal.add(PRODUCT_CODE);
		dealList.add(model);
		dealSet.addAll(dealList);
		deals.add(dealData);

		when(userService.getUserForUID(Mockito.anyString())).thenReturn(customerModel);
		Mockito.when(customerData.getUid()).thenReturn("user");
		Mockito.when(customerModel.getMyOffers()).thenReturn(dealSet);
		Mockito.when(customerModel.getUid()).thenReturn("user");
		Mockito.when(extDealService.findListOfDeals()).thenReturn(dealList);
		Mockito.doNothing().when(extCustomerAccountService).updateProfileInfo(customerModel);
		Mockito.when(userFavouritesIntegrationService.getAllFavouriteOffers()).thenReturn(deal);
		Assert.assertEquals(extCustomerFacade.getAllFavoriteDeals(customerData, deals), true);
	}

	/**
	 * Method to test get all deals.
	 */
	@Test
	public void testFetchAllDeals()
	{
		final DealModel model = Mockito.mock(DealModel.class);
		final List<DealModel> dealList = new ArrayList<>();
		dealList.add(model);
		final DealData deal = Mockito.mock(DealData.class);
		final List<DealData> dealsList = new ArrayList<>();
		dealsList.add(deal);
		Mockito.when(extDealService.findListOfDeals()).thenReturn(dealList);
		Mockito.when(Converters.convertAll(dealList, extDealConverter)).thenReturn(dealsList);
		Assert.assertNotNull(extCustomerFacade.fetchAllDeals());
	}

	/**
	 * Method to test update deals to profile.
	 */
	@Test
	public void testUpdateFavDeals()
	{
		final DealModel model = Mockito.mock(DealModel.class);
		final List<String> deal = new ArrayList<String>();
		final DealData dealData = new DealData();
		dealData.setCode("deal");
		deal.add(PRODUCT_CODE);
		final List<DealModel> dealList = new ArrayList<>();
		dealList.add(model);
		final Set<DealData> dealSet = new HashSet<>();
		dealSet.add(dealData);

		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final List<DealData> deals = new ArrayList<>();
		deals.add(dealData);


		Mockito.when(customerData.getMyOffers()).thenReturn(dealSet);

		Assert.assertEquals(extCustomerFacade.updateFavDeals(customerData, deals), dealData.isFavorite());

	}

	/**
	 * Method to test is fav deals.
	 */
	@Test
	public void testisFavDeal()
	{
		final DealModel model = Mockito.mock(DealModel.class);
		final List<String> deal = new ArrayList<String>();
		final DealData dealData = new DealData();
		final UserModel user = Mockito.mock(UserModel.class);

		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);

		deal.add("deal");
		final List<DealModel> dealList = new ArrayList<>();
		dealList.add(model);
		final Set<DealData> dealSet = new HashSet<>();
		dealSet.add(dealData);
		final Set<DealModel> dealsSet = new HashSet<>();
		dealList.add(model);
		final CustomerData customerData = Mockito.mock(CustomerData.class);
		final List<DealData> deals = new ArrayList<>();
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(customerModel.getMyOffers()).thenReturn(dealsSet);
		deals.add(dealData);
		dealData.setCode("deal");

		Mockito.when(customerData.getMyOffers()).thenReturn(dealSet);

		Assert.assertEquals(extCustomerFacade.isFavDeal(dealData.getCode()), dealData.isFavDeal());

	}

	@Test
	public void testFetchMyFavSizesFromExtService()
	{
		final List<String> sizes = new ArrayList<>();
		when(userFavouritesIntegrationService.getAllFavouriteSizes()).thenReturn(sizes);
		Assert.assertEquals(sizes, extCustomerFacade.fetchMyFavSizesFromExtService());
	}

	@Test
	public void testFetchMyFavSizesFromExtServiceMock()
	{
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false)).thenReturn(true);
		final Set<SizeModel> sizes = new HashSet<>();
		final SizeModel size = new SizeModel();
		size.setCode("code");
		sizes.add(size);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setMySizes(sizes);
		Assert.assertNotNull(extCustomerFacade.fetchMyFavSizesFromExtService());
	}

	@Test
	public void testgetAllFavouriteStoresFromPOMock()
	{
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false)).thenReturn(true);
		final Set<ShopModel> stores = new HashSet<>();
		final ShopModel shop = new ShopModel();
		shop.setId("ID");
		stores.add(shop);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setShops(stores);
		Assert.assertNotNull(extCustomerFacade.getAllFavouriteStoresFromPO());
	}

	@Test
	public void testgetMyFavoriteFromPO()
	{
		final List<String> products = new ArrayList<>();
		when(userFavouritesIntegrationService.getAllFavouriteProducts()).thenReturn(products);
		Assert.assertEquals(products, extCustomerFacade.getMyFavoriteFromPO());
	}

	@Test
	public void testgetMyFavoriteFromPOMock()
	{
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false)).thenReturn(true);
		final ProductModel model = new ProductModel();
		final ProductData data = new ProductData();
		final List<String> product = new ArrayList<String>();
		product.add(PRODUCT_CODE);
		final Set<ProductModel> productset = new HashSet<>();
		productset.add(model);
		final List<ProductData> productData = new ArrayList<>();
		productData.add(data);
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getMyFavProduct()).thenReturn(productset);
		Mockito.when(productConverter.convertAll(productset)).thenReturn(productData);
		Assert.assertNotNull(extCustomerFacade.getMyFavoriteFromPO());
	}

	@Test
	public void testIsSizeSelected()
	{
		final Set<SizeModel> sizes = new HashSet<>();
		final SizeModel size = new SizeModel();
		size.setCode("code");
		sizes.add(size);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setMySizes(sizes);
		Assert.assertTrue(extCustomerFacade.isSizeSelected("code"));
	}

	@Test
	public void testIsSizeSelectedFalse()
	{
		final Set<SizeModel> sizes = new HashSet<>();
		final SizeModel size = new SizeModel();
		size.setCode("code1");
		sizes.add(size);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setMySizes(sizes);
		Assert.assertFalse(extCustomerFacade.isSizeSelected("code"));
	}

	@Test
	public void testIsFavDeal()
	{
		final Set<DealModel> sizes = new HashSet<>();
		final DealModel size = new DealModel();
		size.setCode("code");
		sizes.add(size);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setMyOffers(sizes);
		Assert.assertTrue(extCustomerFacade.isFavDeal("code"));
	}

	@Test
	public void testIsFavDealFalse()
	{
		final Set<DealModel> sizes = new HashSet<>();
		final DealModel size = new DealModel();
		size.setCode("code1");
		sizes.add(size);
		final CustomerModel customerModel = new CustomerModel();
		when(userService.getCurrentUser()).thenReturn(customerModel);
		customerModel.setMyOffers(sizes);
		Assert.assertFalse(extCustomerFacade.isFavDeal("code"));
	}

}
