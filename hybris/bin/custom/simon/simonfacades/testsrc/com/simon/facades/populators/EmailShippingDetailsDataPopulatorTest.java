package com.simon.facades.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.ShippingDetailsData;
import com.simon.integration.utils.ShareEmailIntegrationUtils;


/**
 * Test class for EmailShippingDetailsDataPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailShippingDetailsDataPopulatorTest
{
	@InjectMocks
	private EmailShippingDetailsDataPopulator emailShippingDetailsDataPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	/**
	 * Initialize mocks
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}

	/**
	 * Test Populate Method for Valid data
	 */
	@Test
	public void testpopulate()
	{
		final AddressData source = mock(AddressData.class);
		final ShippingDetailsData target = new ShippingDetailsData();
		when(source.getFirstName()).thenReturn("firstName");
		when(source.getLastName()).thenReturn("lastName");
		when(source.getLine1()).thenReturn("Line1");
		when(source.getLine2()).thenReturn("Line2");
		when(source.getPostalCode()).thenReturn("12101");
		when(source.getTown()).thenReturn("town");
		final RegionData regionData = mock(RegionData.class);
		when(regionData.getName()).thenReturn("AL");
		when(source.getRegion()).thenReturn(regionData);
		emailShippingDetailsDataPopulator.populate(source, target);
		assertEquals("AL", target.getState());

	}

	/**
	 * Test Populate Method for null data
	 */
	@Test
	public void testpopulateForNullData()
	{
		final AddressData source = mock(AddressData.class);
		final ShippingDetailsData target = new ShippingDetailsData();
		emailShippingDetailsDataPopulator.populate(source, target);
		assertEquals(StringUtils.EMPTY, target.getState());

	}

}
