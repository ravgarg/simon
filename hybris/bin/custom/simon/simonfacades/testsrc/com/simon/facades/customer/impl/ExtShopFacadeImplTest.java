package com.simon.facades.customer.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.CustomShopsComponentModel;
import com.simon.core.services.ExtShopService;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationService;

import junit.framework.Assert;




/**
 * Implementation for the ExtCustomerFacadetest
 */
@UnitTest
public class ExtShopFacadeImplTest
{

	@InjectMocks
	private ExtShopFacadeImpl shopFacade;

	@Mock
	private Converter<CustomShopsComponentModel, StoreDataList> extStoreDataConverter;

	@Mock
	private CustomShopsComponentModel customShops;
	@Mock
	private StoreDataList storeDataList;
	@Mock
	private ExtShopService extShopService;
	@Mock
	private UserService userService;
	@Mock
	private ExtCustomerFacade extCustomerFacade;
	@Mock
	private UserFavouritesIntegrationService userFavouritesIntegrationService;

	private final CustomerData customerData = new CustomerData();

	private final StoreDataList storesList = new StoreDataList();
	@Mock
	private CustomerModel customerModel;
	@Mock
	private CustomerType type;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testGetCustomShops()
	{
		Mockito.when(extStoreDataConverter.convert(customShops, storeDataList)).thenReturn(storeDataList);
		shopFacade.loadCustomShops(customShops, storeDataList);
		Assert.assertNotNull(storeDataList.getAllStoresList());
	}

	@Test
	public void testUpdateFavCustomShopsForEmptyMyStores()
	{
		customerData.setMyStores(new HashSet<StoreData>());
		final List<StoreData> allStoresList = new ArrayList<>();
		final StoreData storeData = new StoreData();
		allStoresList.add(storeData);
		storesList.setAllStoresList(allStoresList);

		shopFacade.updateFavCustomShops(customerData, storesList);
		Assert.assertNotNull(storesList.getAllStoresList());
	}

	@Test
	public void testUpdateFavCustomShopsForNonEmptyMyStoresAndNoFavorite()
	{
		final Set<StoreData> storesSet = new HashSet<>();
		final StoreData store = new StoreData();
		store.setId("121");
		storesSet.add(store);
		customerData.setMyStores(storesSet);
		final List<StoreData> allStoresList = new ArrayList<>();
		final StoreData storeData = new StoreData();
		storeData.setId("111");
		allStoresList.add(storeData);
		storesList.setAllStoresList(allStoresList);

		customerData.setMyStores(storesSet);
		shopFacade.updateFavCustomShops(customerData, storesList);
		Assert.assertNotNull(storesList.getAllStoresList());
	}

	@Test
	public void testgetRetailerList()
	{
		final ShopModel shop = new ShopModel();
		shop.setId("2000");
		shop.setName("test");
		final List<ShopModel> shopList = new ArrayList();
		shopList.add(shop);
		final List<String> retailers = new ArrayList();
		retailers.add("2000");
		Mockito.when(extShopService.getRetailerlist()).thenReturn(shopList);
		Mockito.when(extCustomerFacade.getAllFavouriteStoresFromPO()).thenReturn(retailers);
		Mockito.when(extShopService.getListOfShopsForCodes(retailers.toArray(new String[retailers.size()]))).thenReturn(shopList);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getType()).thenReturn(type);
		Mockito.when(type.getCode()).thenReturn("REGISTERED");

		shopFacade.getRetailerList();
		Assert.assertNotNull(shopFacade.getRetailerList());
	}

	@Test
	public void testgetCustomerFavouritRetailerList()
	{
		final ShopModel shop = new ShopModel();
		shop.setId("2000");
		shop.setName("test");
		final List<ShopModel> shopList = new ArrayList();
		final List<String> retailers = new ArrayList();
		retailers.add("2000");
		shopList.add(shop);
		Mockito.when(extShopService.getRetailerlist()).thenReturn(shopList);
		Mockito.when(extCustomerFacade.getAllFavouriteStoresFromPO()).thenReturn(retailers);
		Mockito.when(extShopService.getListOfShopsForCodes(retailers.toArray(new String[retailers.size()]))).thenReturn(shopList);
		Mockito.when(userService.getCurrentUser()).thenReturn(customerModel);
		Mockito.when(customerModel.getType()).thenReturn(type);
		Mockito.when(type.getCode()).thenReturn("REGISTERED");

		shopFacade.getCustomerFavouritRetailerList();
		Assert.assertNotNull(shopFacade.getCustomerFavouritRetailerList());
	}




}
