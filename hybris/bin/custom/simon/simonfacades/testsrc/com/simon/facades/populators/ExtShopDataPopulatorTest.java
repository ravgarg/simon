package com.simon.facades.populators;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.facades.shop.data.ShopData;


/**
 * Contains tests for ExtShopDataPopulator.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtShopDataPopulatorTest
{

	@InjectMocks
	@Spy
	private ExtShopDataPopulator extShopDataPopulator;
	@Mock
	private ShopModel shopModel;
	@Mock
	private ShopData shopData;

	/**
	 * Method to setup resources.
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		shopModel = new ShopModel();
		shopModel.setShippingTitle("title");
		shopModel.setShippingMessage("msg");
		shopModel.setShippingMethodOneDelivery("OneDelivery");
		shopModel.setShippingMethodOneName("OneName");
		shopModel.setShippingMethodTwoDelivery("TwoDelivery");
		shopModel.setShippingMethodTwoName("TwoName");
		shopModel.setReturnCost("cost");
		shopModel.setReturnPolicy("policy");
		shopModel.setReturnPeriodMessage("period");
		shopModel.setReturnPolicyHeading("heading");
		shopModel.setName("shopName");
		shopModel.setMethod("method");
		shopModel.setPrivacyPolicy("privacypolicy");
		shopModel.setPrivacyPolicyHeading("privacypolicyHeading");
		shopModel.setPricingMessage("PricingMessage");

		shopData = new ShopData();
	}

	/**
	 * Method to test populate() method.
	 *
	 */
	@Test
	public void testPopulate()
	{

		doReturn("").when(extShopDataPopulator).getLocalizedString(any());
		extShopDataPopulator.populate(shopModel, shopData);

		Assert.assertEquals("title", shopData.getShippingTitle());
		Assert.assertEquals("privacypolicy", shopData.getPrivacyPolicyMessage());
	}

}
