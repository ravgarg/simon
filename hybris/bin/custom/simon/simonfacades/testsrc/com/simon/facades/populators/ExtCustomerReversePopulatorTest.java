package com.simon.facades.populators;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.core.model.DesignerModel;
import com.simon.facades.account.data.DesignerData;


/**
 * Junit for ExtCustomerReversePopulatorTest.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCustomerReversePopulatorTest
{
	@InjectMocks
	private ExtCustomerReversePopulator extCustomerReversePopulator;

	@Mock
	private CustomerNameStrategy customerNameStrategy;
	@Mock
	private Converter<DesignerData, DesignerModel> extReverseDesignerConverter;

	@Mock
	private CommonI18NService commonI18NService;
	private CustomerModel target;
	private CustomerData source;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		source = new CustomerData();
		target = new CustomerModel();
		source.setBirthMonth(Months.APR);
		source.setGender(CustomerGender.FEMALE);
		source.setBirthYear(1992);
		source.setEmailAddress("test");
		source.setFirstName("test");
		source.setLastName("test");
		source.setZipcode("22222");
		source.setOptedInEmail(true);
		Mockito.when(customerNameStrategy.getName(source.getFirstName(), source.getLastName())).thenReturn("Name");


	}


	@Test
	public void testPopulateWithValidCountry()
	{
		final CountryData country = new CountryData();
		country.setIsocode("US");
		source.setCountry(country.getIsocode());
		final CountryModel countryModel = new CountryModel();
		countryModel.setIsocode("US");
		Mockito.when(commonI18NService.getCountry("US")).thenReturn(countryModel);
		extCustomerReversePopulator.populate(source, target);
		Assert.assertEquals(target.getZipCode(), source.getZipcode());
		Assert.assertEquals(target.getCountry().getIsocode(), source.getCountry());
	}

	@Test
	public void testPopulateWithNullCountry()
	{
		extCustomerReversePopulator.populate(source, target);
		Assert.assertEquals(target.getZipCode(), source.getZipcode());
	}
}
