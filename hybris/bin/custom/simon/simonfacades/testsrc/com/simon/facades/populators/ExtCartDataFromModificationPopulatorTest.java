package com.simon.facades.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.product.impl.DefaultPriceService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.price.ExtPriceService;
import com.simon.facades.cart.data.RetailerInfoData;


/**
 * This class tests the methods of class {@link ExtCartDataFromModificationPopulator}.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCartDataFromModificationPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtCartDataFromModificationPopulator extCartDataFromModificationPopulator;


	@Mock
	private Converter<ShopModel, RetailerInfoData> extRetailerConverter;

	@Mock
	private ProductService productService;

	@Mock
	private OrderEntryModel entryModel;

	@Mock
	private ProductModel productModel;

	@Mock
	private ShopModel shopModel;

	@Mock
	private OrderEntryData orderEntryData;

	@Mock
	RetailerInfoData retailerInfoData;

	@Mock
	ProductData variant;

	@Mock
	ProductModel variantproductModel;

	@Mock
	ProductModel productModelVariant;
	@Mock
	VariantCategoryModel variantCategoryModel;
	@Mock
	CategoryModel variantSuperCategoryModel;
	@Mock
	CategoryModel value;
	@Mock
	CartData target;
	@Mock
	private DefaultPriceService defaultPriceService;

	@Mock
	PriceInformation priceInformation;

	@Mock
	private ExtPriceService priceService;

	@Mock
	private ExtPriceValue extPriceValue;


	@Mock
	private PriceDataFactory priceDataFactory;

	//@Mock
	private PriceData priceData;



	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * When Retailer already present in CartData
	 */
	@Test
	public void populateRetailerExistInCartData()
	{
		final Double msrp = 1.0;
		final Double lp = 120.0;
		final Double sp = 30.0;
		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);

		final CartModificationData source = mock(CartModificationData.class);
		when(source.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(variant);
		when(variant.getCode()).thenReturn("productCode");
		when(productService.getProductForCode("productCode")).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getId()).thenReturn("shopId");

		when(target.getRetailerInfoData()).thenReturn(retailerInfoDataList);
		final List<PriceInformation> productPrice = new ArrayList<>();
		productPrice.add(priceInformation);
		final Map<DetailedPriceInfo, Object> priceDetail = new HashMap<>();
		priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, true);
		priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, true);
		priceDetail.put(DetailedPriceInfo.PERCENTAGE_SAVING, "60%");
		priceData = new PriceData();


		when(priceInformation.getPriceValue()).thenReturn(extPriceValue);
		when(productModel.getMsrp()).thenReturn(msrp);
		when(extPriceValue.getCurrencyIso()).thenReturn("en");
		when(extPriceValue.getListPrice()).thenReturn(lp);
		when(extPriceValue.getValue()).thenReturn(msrp);
		when(priceService.getDetailedPriceInformation(msrp, lp, msrp)).thenReturn(priceDetail);
		when(defaultPriceService.getPriceInformationsForProduct(productModel)).thenReturn(productPrice);
		when(retailerInfoData.getRetailerId()).thenReturn("shopId");
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(extPriceValue.getListPrice()),
				extPriceValue.getCurrencyIso())).thenReturn(priceData);
		Mockito.doReturn("% off").when(extCartDataFromModificationPopulator).getLocalizedString();
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(extPriceValue.getValue()),
				extPriceValue.getCurrencyIso())).thenReturn(priceData);
		extCartDataFromModificationPopulator.populate(source, target);
		Assert.assertNotNull(target.getRetailerInfoData());

	}


	/**
	 * When Retailer already present in CartData and list price is less than zero
	 */
	@Test
	public void populateRetailerExistInCartDataListPrice()
	{
		final Double msrp = 1.0;
		final Double lp = -1.0;
		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);

		final CartModificationData source = mock(CartModificationData.class);
		when(source.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(variant);
		when(variant.getCode()).thenReturn("productCode");
		when(productService.getProductForCode("productCode")).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getId()).thenReturn("shopId");

		when(target.getRetailerInfoData()).thenReturn(retailerInfoDataList);
		final List<PriceInformation> productPrice = new ArrayList<>();
		productPrice.add(priceInformation);
		final Map<DetailedPriceInfo, Object> priceDetail = new HashMap<>();
		priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, true);
		priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, true);
		priceDetail.put(DetailedPriceInfo.PERCENTAGE_SAVING, "60%");
		priceData = new PriceData();


		when(priceInformation.getPriceValue()).thenReturn(extPriceValue);
		when(productModel.getMsrp()).thenReturn(msrp);
		when(extPriceValue.getCurrencyIso()).thenReturn("en");
		when(extPriceValue.getListPrice()).thenReturn(lp);
		when(extPriceValue.getValue()).thenReturn(msrp);
		when(priceService.getDetailedPriceInformation(msrp, lp, msrp)).thenReturn(priceDetail);
		when(defaultPriceService.getPriceInformationsForProduct(productModel)).thenReturn(productPrice);
		when(retailerInfoData.getRetailerId()).thenReturn("shopId");
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(extPriceValue.getListPrice()),
				extPriceValue.getCurrencyIso())).thenReturn(priceData);
		Mockito.doReturn("% off").when(extCartDataFromModificationPopulator).getLocalizedString();
		when(priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(extPriceValue.getValue()),
				extPriceValue.getCurrencyIso())).thenReturn(priceData);
		extCartDataFromModificationPopulator.populate(source, target);
		Assert.assertNotNull(target.getRetailerInfoData());

	}


	/**
	 * When Retailer not Present in CartData
	 */
	@Test
	public void populateNoRetailerExistInCartData()
	{
		final CartModificationData source = mock(CartModificationData.class);
		when(source.getEntry()).thenReturn(orderEntryData);
		when(orderEntryData.getProduct()).thenReturn(variant);
		when(variant.getCode()).thenReturn("productCode");
		when(productService.getProductForCode("productCode")).thenReturn(productModel);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getId()).thenReturn("shopId");

		final List<RetailerInfoData> retailerInfoDataList = new ArrayList<>();
		retailerInfoDataList.add(retailerInfoData);

		when(target.getRetailerInfoData()).thenReturn(retailerInfoDataList);
		when(retailerInfoData.getRetailerId()).thenReturn("shopId1");
		Mockito.doReturn(retailerInfoData).when(extRetailerConverter).convert(shopModel);
		when(source.getEntry()).thenReturn(orderEntryData);

		extCartDataFromModificationPopulator.populate(source, target);
		Assert.assertNotNull(target.getRetailerInfoData());
	}


}
