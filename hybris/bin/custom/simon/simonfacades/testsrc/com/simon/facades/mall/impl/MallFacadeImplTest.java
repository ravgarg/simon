package com.simon.facades.mall.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;
import com.simon.facades.customer.data.MallData;


/**
 * Contains tests for MallFacadeImpl.
 *
 */
@UnitTest
public class MallFacadeImplTest
{

	@InjectMocks
	private MallFacadeImpl mallFacadeImpl;

	@Mock
	private MallService mallService;

	@Mock
	private Converter<MallModel, MallData> mallConverter;

	private MallModel mallModel;
	private List<MallModel> allMalls;
	private MallData mallData;
	private List<MallData> listOfMallData;

	/**
	 * Setting up resources for tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		mallModel = new MallModel();
		mallModel.setActive(true);
		mallModel.setCode("12345");

		allMalls = new ArrayList<>();
		allMalls.add(mallModel);

		mallData = new MallData();
		mallData.setCode("12345");
		mallData.setName("Mall Name");

		listOfMallData = new ArrayList<>();
		listOfMallData.add(mallData);
	}

	/**
	 * Method to test getMallList() in if block scenario.
	 *
	 */
	@Test
	public void testGetMallList_If_Scenario()
	{
		Mockito.when(mallService.getAllMalls()).thenReturn(allMalls);
		Mockito.when(mallConverter.convertAll(mallService.getAllMalls())).thenReturn(listOfMallData);

		final List<MallData> results = mallFacadeImpl.getMallList();

		Assert.assertTrue(!results.isEmpty());
	}

	/**
	 * Method to test getMallList() in else block scenario.
	 *
	 */
	@Test
	public void testGetMallList_Else_Scenario()
	{
		allMalls = new ArrayList<>();

		Mockito.when(mallService.getAllMalls()).thenReturn(allMalls);
		Mockito.when(mallConverter.convertAll(mallService.getAllMalls())).thenReturn(listOfMallData);

		final List<MallData> results = mallFacadeImpl.getMallList();

		Assert.assertTrue(results.isEmpty());
	}

}
