package com.simon.webservices.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class provides ability to log different objects in formatted string format
 */
public class LoggerUtils
{
	private LoggerUtils()
	{
		throw new IllegalStateException("Utility class");
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(LoggerUtils.class);

	/**
	 * @description This method is used to log request
	 * @param object
	 * @param method
	 */
	public static void logRequestResponse(final Object object, final String method)
	{
		if (null == object || null == method)
		{
			throw new IllegalStateException("Parameter cannot be null");
		}
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
			LOGGER.info("Logging request for method {} and {} object in json String format {} ", method,
					object.getClass().getSimpleName(), jsonString);
		}
		catch (final JsonProcessingException ex)
		{
			LOGGER.info("Error while parsing Json for logging" + ex);
		}
	}
}
