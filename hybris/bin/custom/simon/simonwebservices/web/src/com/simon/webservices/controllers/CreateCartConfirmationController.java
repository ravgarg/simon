package com.simon.webservices.controllers;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.facades.cart.ExtCartFacade;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.webservices.utils.LoggerUtils;


/**
 * This is the webservice endpoint class which will capture the callbacks for the confirmation of the create cart
 * request. Callback will be made from ESB to Hybris system.
 *
 */
@Controller
@RequestMapping("/integration")
public class CreateCartConfirmationController
{

	private static final Logger LOG = LoggerFactory.getLogger(CreateCartConfirmationController.class);

	@Resource
	private ExtCartFacade cartFacade;

	/**
	 * This method will be used by ESB to post the JSON for the create cart confirmation callback. JSON will be in the
	 * following format:
	 *
	 * URL : http://<hostname>/simonwebservices/integration/confirmCreateCart</br>
	 * Method : POST</br>
	 * Header : Content-Type=application/json</br>
	 * POST body parameter : </br>
	 *
	 * JSON format for the success scenario<br/>
	 * <code>
	 * {
	 *  	"cartId":"CART_ID",
	 *  	"cartStatus":"CART_STATUS",
	 *  	"retailers":[
	 *  		{
	 *  			"retailerId":"SHOP_ID",
	 *  			"addedProducts":[
	 *  			{
	 *  				"productId":"",
	 *  				"requiredFieldNames":[""]
	 *  				}
	 *  			],
	 *  			"failedProducts":[
	 *
	 *  			],
	 *  			"shippingOptions":{
	 *  				"SHIPPING_METHOD":"DESCRIPTION"
	 *  			}
	 *  		}
	 *  	]
	 *  }
	 * </code> <br/>
	 * JSON format for the error scenario<br/>
	 * <code>
	 * {
	 *  	"id":"CART_ID/ORDER_ID",
	 *  	"errorCode":"Error Code",
	 *  	"errorMessage":"Error Message"
	 *  }
	 *  </code>
	 *
	 * @param createCartConfirmation
	 *           The String which will capture the JSON body
	 * @return HttpStatus
	 */
	@RequestMapping(value = "/confirmCreateCart", method = RequestMethod.POST)
	public ResponseEntity<HttpStatus> confirmCreateCart(@RequestBody final String createCartConfirmation)
	{
		HttpStatus responseStatus = HttpStatus.BAD_REQUEST;
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			final CreateCartConfirmationDTO createCartConfirmationDTO = mapper.readValue(createCartConfirmation,
					CreateCartConfirmationDTO.class);
			if (LOG.isInfoEnabled())
			{
				LoggerUtils.logRequestResponse(createCartConfirmationDTO, "Create Cart Confirmation Callback");
			}
			if (null != createCartConfirmationDTO)
			{
				responseStatus = processResponse(createCartConfirmationDTO);
			}
		}
		catch (final IllegalArgumentException illegalArgumentException)
		{
			LOG.error("Cart ID not found in the request ", illegalArgumentException);
		}
		catch (final IOException | IllegalStateException exception)
		{
			LOG.error("Could not parse callback response to object : ", exception);
		}
		return new ResponseEntity<>(responseStatus);
	}

	private HttpStatus processResponse(final CreateCartConfirmationDTO createCartConfirmationDTO)
	{
		cartFacade.saveCartCallBackResponse(createCartConfirmationDTO);

		if (StringUtils.isNotEmpty(createCartConfirmationDTO.getErrorCode()))
		{
			if ("400".equalsIgnoreCase(createCartConfirmationDTO.getErrorCode()))
			{
				LOG.error(
						"Create cart request schema validation failed on ESB. Request contains error with code: {} and message : {}",
						createCartConfirmationDTO.getErrorCode(), createCartConfirmationDTO.getErrorMessage());
			}
			else if ("608".equalsIgnoreCase(createCartConfirmationDTO.getErrorCode()))
			{
				LOG.error("Retailer connection timed out. Request contains error with code: {} and message : {}",
						createCartConfirmationDTO.getErrorCode(), createCartConfirmationDTO.getErrorMessage());
			}
			else
			{
				LOG.error("Create cart request failed with code: {} and message : {}", createCartConfirmationDTO.getErrorCode(),
						createCartConfirmationDTO.getErrorMessage());
			}
		}
		return HttpStatus.OK;
	}
}
