package com.simon.webservices.controllers;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.integration.dto.OrderConfirmCallBackDTO;


/**
 * This is the webservice endpoint which will capture the callbacks for the confirmation of the order update request.
 * Callback will be made from ESB to Hybris system.
 *
 */
@Controller
@RequestMapping("/integration")
public class OrderConfirmationController
{

	private static final Logger LOG = LoggerFactory.getLogger(OrderConfirmationController.class);

	@Resource
	private ExtCheckoutFacade checkoutFacade;
	@Resource
	private ConfigurationService configurationService;

	/**
	 * @description Method use when call confirm delivery from the esb layer and want to save or update data in the
	 *              hybris layer
	 * @method confirmOrderDeliveryUpdate
	 * @param orderConfirmCallBackDTO
	 * @return ResponseEntity<T>
	 */
	@RequestMapping(value = "/confirmDeliveryUpdate", method = RequestMethod.POST)
	public ResponseEntity<T> confirmOrderDeliveryUpdate(@RequestBody final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		logMethodBodyRequestDTO(orderConfirmCallBackDTO, "confirmOrderDeliveryUpdate");
		if (StringUtils.isEmpty(orderConfirmCallBackDTO.getErrorCode())
				&& StringUtils.isEmpty(orderConfirmCallBackDTO.getErrorMessage()))
		{
			checkoutFacade.savePurchaseCallbackResponse(orderConfirmCallBackDTO);
		}
		else
		{
			checkoutFacade.savePurchaseCallbackErrorResponse(orderConfirmCallBackDTO);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * @description Method use when call confirm purchase from the esb layer and want to save or update data in the
	 *              hybris layer
	 * @method confirmOrderPurchaseUpdate
	 * @param orderConfirmCallBackDTO
	 * @return ResponseEntity<T>
	 */
	@RequestMapping(value = "/confirmPurchaseUpdate", method = RequestMethod.POST)
	public ResponseEntity<T> confirmOrderPurchaseUpdate(@RequestBody final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		logMethodBodyRequestDTO(orderConfirmCallBackDTO, "confirmOrderPurchaseUpdate");

		final String orderId = StringUtils.isNotEmpty(orderConfirmCallBackDTO.getOrderId()) ? orderConfirmCallBackDTO.getOrderId()
				: orderConfirmCallBackDTO.getId();

		if (StringUtils.isEmpty(orderConfirmCallBackDTO.getErrorCode())
				&& StringUtils.isEmpty(orderConfirmCallBackDTO.getErrorMessage()))
		{
			checkoutFacade.savePurchaseConfirmCallBackResponse(orderConfirmCallBackDTO, orderId);
		}
		else if (StringUtils.isNotEmpty(orderConfirmCallBackDTO.getErrorCode())
				&& checkoutFacade.validateRetailerOrderIDIsPresent(orderConfirmCallBackDTO))
		{
			checkoutFacade.savePurchaseConfirmCallBackPartialResponse(orderConfirmCallBackDTO, orderId);
		}
		else
		{
			checkoutFacade.savePurchaseConfirmCallbackErrorResponse(orderConfirmCallBackDTO);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}


	/**
	 * @description This method is used to log request
	 * @param orderConfirmCallBackDTO
	 */
	private void logMethodBodyRequestDTO(final OrderConfirmCallBackDTO orderConfirmCallBackDTO, final String method)
	{
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonString = mapper.writeValueAsString(orderConfirmCallBackDTO);
			LOG.info("Logging request for method {} and OrderConfirmCallBackDTO object in json String format {} ", method,
					jsonString);
		}
		catch (final JsonProcessingException ex)
		{
			LOG.info("Error while parsing Json for logging" + ex);
		}
	}
}
