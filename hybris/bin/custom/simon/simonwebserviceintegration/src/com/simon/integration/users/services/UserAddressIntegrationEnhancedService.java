package com.simon.integration.users.services;

import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;
import com.simon.integration.users.dto.UserPaymentAddressDTO;

/**
 * The Interface UserAddressIntegrationEnhancedService.
 */
public interface UserAddressIntegrationEnhancedService {

	/**
	 * This method is used to add a user address with PO.com with user address
	 * details mapped in {@link UserAddressDTO} and then parse response in
	 * {@link UserAddressResponseDTO}.
	 * 
	 * @param userAddressDTO
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO addUserAddress(UserAddressDTO userAddressDTO) throws UserAddressIntegrationException;

	/**
	 * This method is used to update a user address with PO.com with user
	 * address details mapped in {@link UserAddressDTO} and then parse response
	 * in {@link UserAddressResponseDTO}.
	 * 
	 * @param userAddressDTO
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO updateUserAddress(UserAddressDTO userAddressDTO) throws UserAddressIntegrationException;

	/**
	 * This method is used to remove a user address with PO.com and then parse
	 * response in {@link UserAddressResponseDTO}.
	 * 
	 * @param userAddressDTO
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO removeUserAddress(UserAddressDTO userAddressDTO) throws UserAddressIntegrationException;

	/**
	 * This method is used to add a user Payment address with PO.com with user
	 * Payment address details mapped in {@link UserPaymentAddressDTO} and then
	 * parse response in {@link UserAddressResponseDTO}.
	 * 
	 * @param ccPaymentInfoData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO addUserPaymentAddress(UserPaymentAddressDTO userPaymentAddressDTO)
			throws UserAddressIntegrationException;

	UserGetAddressResponseDTO getUserAddress(String addressTypeHome) throws UserAddressIntegrationException;
}
