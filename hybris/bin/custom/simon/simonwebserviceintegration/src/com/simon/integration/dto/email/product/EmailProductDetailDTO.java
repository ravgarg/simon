package com.simon.integration.dto.email.product;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "item")
public class EmailProductDetailDTO {
	
	@XmlAttribute(name = "img")
	private String imageUrl;
	
	@XmlAttribute(name = "description")
	private String productName;
	
	@XmlAttribute(name = "cost")
	private String msrp;
	
	@XmlAttribute(name = "discounted")
	private String salePrice;
	
	@XmlAttribute(name = "qty")
	private String quantity;
	
	@XmlAttribute(name = "color")
	private String colorName;

	@XmlAttribute(name = "shopurl")
	private String shopNow;
	
	@XmlAttribute(name = "promo")
	private String percentageOff;
	
	
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setMsrp(String msrp) {
		this.msrp = msrp;
	}

	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public void setShopNow(String shopNow) {
		this.shopNow = shopNow;
	}

	public void setPercentageOff(String percentageOff) {
		this.percentageOff = percentageOff;
	}

}
