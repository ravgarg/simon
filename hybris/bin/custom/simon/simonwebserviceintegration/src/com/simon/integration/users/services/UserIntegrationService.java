
package com.simon.integration.users.services;

import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;

/**
 * The Interface UserIntegrationService.
 */
public interface UserIntegrationService
{	
	/**
	 * @description Method use to update the registered user details to show on the user profile page
	 * @method updateUserDetails
	 * @param updateUserData
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException
	 */
	VIPUserDTO updateUserDetails(CustomerData updateUserData) throws UserIntegrationException;
	/**
	 * This method is used to register a user with PO.com with user details mapped in {@link RegisterData} and then parse response in
	 * UserLoginResponseDTO.
	 * @param registerData 
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException 
	 * @throws AuthLoginIntegrationException 
	 */
	VIPUserDTO registerUser(RegisterData registerData) throws UserIntegrationException, AuthLoginIntegrationException ;
	
	/**
	 * This method is used get default center associated with a zip-code from PO.com.
	 * 
	 * @param zipCode
	 * @return {@link UserCenterIdDTO}
	 * @throws UserIntegrationException 
	 */
	UserCenterIdDTO getDefaultCenter(String zipCode) throws UserIntegrationException;
}
