package com.simon.integration.share.email.order.populators;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.integration.dto.email.order.EmailOrderProductDTO;
import com.simon.integration.dto.email.order.EmailOrderRetailerDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;

/**
 * Converts RetailerDetailsData to EmailOrderRetailerDTO
 */
public class EmailOrderRetailerDetailsDTOPopulator implements Populator<RetailerDetailsData, EmailOrderRetailerDTO> {

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(RetailerDetailsData source, EmailOrderRetailerDTO target) {

		target.setName(source.getName());
		target.setOrdernumber(source.getOrderNumber());
		target.setRetailerpromomsg(source.getRetailerPromoMsg());
		target.setStatus(source.getStatus());
		target.setSubtotal(source.getSubTotal());
		target.setShipping(source.getShipping());
		target.setTax(source.getTax());
		target.setTotal(source.getTotal());
		target.setSaved(source.getSaved());
		target.setShippingdesc(source.getShippingDesc());

		target.setItems(pouulateItems(source.getProductsDetail()));
	}

	private List<EmailOrderProductDTO> pouulateItems(List<ProductDetailsData> productsDetail) {
		final List<EmailOrderProductDTO> itemList = new ArrayList<>();
		for (final ProductDetailsData productdata : productsDetail) {
			final EmailOrderProductDTO orderProductDto = new EmailOrderProductDTO();
			orderProductDto.setImg(shareEmailIntegrationUtils.buildImageUrl(productdata.getImageUrl()));
			orderProductDto.setDescription(productdata.getDescription());
			orderProductDto.setPrice(productdata.getPrice());
			orderProductDto.setSize(productdata.getSize());
			orderProductDto.setQuantity(productdata.getQuantity());
			orderProductDto.setStatus(productdata.getStatus());
			orderProductDto.setSubtotal(productdata.getSubtotal());
			orderProductDto.setColor(productdata.getColor());
			orderProductDto.setPromo(productdata.getPromoMsg());
			itemList.add(orderProductDto);
		}
		return itemList;
	}

}
