
package com.simon.integration.services;

import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;

/**
 * The Interface CartService.
 */
public interface CartCheckOutIntegrationEnhancedService
{
    /**
     * This method is used to access services for pushing the Create Cart request to JMS
     *
     * @param {@link CreateCartDTO}
     */
    void createCart(CreateCartDTO createCartDTO);

    /**
     * This method is used to access services for pushing the Estimate Cart request to JMS
     *
     * @param {@link CartEstimateRequestDTO}
     * @return {@link CartEstimateResponseDTO}
     */
    CartEstimateResponseDTO estimateCart(CartEstimateRequestDTO cartEstimateRequestDTO);

}
