package com.simon.integration.dto.card.verification;

import com.simon.integration.dto.ResponseDTO;

public class CardVerificationResponseDTO extends ResponseDTO {

	private static final long serialVersionUID = 1L;
	private Boolean successFlag;
	private String message;	
	
	public Boolean getSuccessFlag() {
		return successFlag;
	}
	public void setSuccessFlag(Boolean successFlag) {
		this.successFlag = successFlag;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
