package com.simon.integration.payment.services.impl;

import javax.annotation.Resource;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.card.redact.RemovePaymentRequestDTO;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.dto.card.verification.CardVerificationRequestDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.payment.services.CardPaymentIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.config.ConfigurationService;

public class DefaultCardPaymentIntegrationEnhancedService implements CardPaymentIntegrationEnhancedService
{

    @Resource
    private RestWebService restWebService;

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private UserIntegrationUtils userIntegrationUtils;

    @Resource
    private EnqueueService enqueueService;

    @Override
    public CardVerificationResponseDTO verifyCard(final CardVerificationRequestDTO cardVerificationRequestDTO)
    {
        final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.CARD_VERIFY_URL);

        final String stubCall = configurationService.getConfiguration().getString(SimonIntegrationConstants.CARD_VERIFY_STUB);
        final String stubFileName = SimonIntegrationConstants.CARD_VERIFY_STUB_FILE;

        return restWebService.executeRestEvent(serviceUrl, cardVerificationRequestDTO, stubCall, stubFileName, CardVerificationResponseDTO.class, RequestMethod.POST,
            getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());
    }

    protected MultiValueMap<String, Object> getHeaders()
    {
        final MultiValueMap<String, Object> headers = getHeadersMap();
        headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON_UTF_8);
        headers.add(SimonIntegrationConstants.CHAR_SET, SimonIntegrationConstants.UTF_8);
        return headers;
    }

    protected MultiValueMap<String, Object> getHeadersMap()
    {
        return new LinkedMultiValueMap<>();
    }

    @Override
    public void deliverOrder(DeliverOrderRequestDTO deliverOrderRequestDTO)
    {
        enqueueService.sendObjectMessage(deliverOrderRequestDTO, JmsTemplateNameEnum.PLACE_ORDER);
    }

	@Override
	public RemovePaymentResponseDTO removePaymentMethodToken(RemovePaymentRequestDTO removePaymentRequestDTO) {
		
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.REOMOVE_PAYMENT_TOKEN_URL);

		final String stubCall = configurationService.getConfiguration()
				.getString(SimonIntegrationConstants.REMOVE_PAYMENT_METHOD_TOKEN);
		final String stubFileName = SimonIntegrationConstants.REMOVE_PAYMENT_METHOD_TOKEN_STUB_FILE;

		return restWebService.executeRestEvent(serviceUrl, removePaymentRequestDTO, stubCall, stubFileName,
				RemovePaymentResponseDTO.class, RequestMethod.POST, getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());
	}
}
