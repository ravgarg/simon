package com.simon.integration.dto.cart.createcart;

import java.io.Serializable;
import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

public class CreateCartDTO extends PojoAnnotation implements Serializable {
	
	private static long serialVersionUID = 1L;
	
	private String cartID;
	private String createCartInvocationTime;
	
	public String getCreateCartInvocationTime() {
		return createCartInvocationTime;
	}

	public void setCreateCartInvocationTime(String createCartInvocationTime) {
		this.createCartInvocationTime = createCartInvocationTime;
	}
	
	private List<RetailerDTO> retailers;
	
	public String getCartID() {
		return cartID;
	}
	
	public void setCartID(String cartID) {
		this.cartID = cartID;
	}
	
	public List<RetailerDTO> getRetailers() {
		return retailers;
	}
	
	public void setRetailers(List<RetailerDTO> retailers) {
		this.retailers = retailers;
	}
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public static void setSerialVersionUID(long serialVersionUID) {
		CreateCartDTO.serialVersionUID = serialVersionUID;
	}
}
