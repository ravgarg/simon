package com.simon.integration.dto.cart.cartestimate;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

public class CartEstimateRequestDTO extends PojoAnnotation {

	private String cartID;
	private List<RetailerRequestDTO> retailers;

	public void setCartID(final String cartID) {
		this.cartID = cartID;
	}

	public String getCartID() {
		return this.cartID;
	}

	public void setRetailers(final List<RetailerRequestDTO> retailers) {
		this.retailers = retailers;
	}

	public List<RetailerRequestDTO> getRetailers() {
		return this.retailers;
	}

}