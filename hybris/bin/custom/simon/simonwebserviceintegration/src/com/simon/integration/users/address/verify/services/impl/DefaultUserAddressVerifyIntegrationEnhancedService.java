
package com.simon.integration.users.address.verify.services.impl;

import javax.annotation.Resource;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * The Implementation DefaultUserAddressVerifyIntegrationEnhancedService class, use to call the esb layer service api
 */
public class DefaultUserAddressVerifyIntegrationEnhancedService implements UserAddressVerifyIntegrationEnhancedService {
	

	@Resource
	private RestWebService restWebService;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	
	public MultiValueMap<String, Object> getHeadersMap() {
		return new LinkedMultiValueMap<>();
	}

	@Override
	public UserAddressVerifyResponseDTO validateAddress(UserAddressVerifyRequestDTO userAddressVerifyRequestDTO) {
		final String serviceUrl = buildVerifyUserAddressServiceRequestApiUrl();

		return restWebService.executeRestEvent(serviceUrl, userAddressVerifyRequestDTO, Boolean.FALSE.toString(), null, UserAddressVerifyResponseDTO.class, RequestMethod.POST, getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());   
	}
	
	private String buildVerifyUserAddressServiceRequestApiUrl(){
		return userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_SERVICE_API);
	}

	private MultiValueMap<String, Object> getHeaders() {
		final MultiValueMap<String, Object> headers = getHeadersMap();
		headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON);
		return headers;
	}

}