
package com.simon.integration.activemq.client;

import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The Interface ActiveMqClient.
 */
public interface ActiveMqClient
{

    /**
     * This method is used to send your message to ActiveMQ Broker.
     *
     * @param message the message
     * @throws JsonProcessingException
     */
    public void send(final Object message, String command) throws JsonProcessingException;

    /**
     * This method is used to send your message with JMS header to ActiveMQ Broker.
     * 
     * @param message
     * @param command
     * @param headers
     * @throws JsonProcessingException
     */
    public void send(Object message, String command, Map<String, String> headers) throws JsonProcessingException;

}
