package com.simon.integration.dto.card.redact;

import com.simon.integration.dto.PojoAnnotation;

public class RemovePaymentRequestDTO extends PojoAnnotation{
	
	private String paymentMethodToken;
	
	public String getPaymentMethodToken() {
		return paymentMethodToken;
	}

	public void setPaymentMethodToken(String paymentMethodToken) {
		this.paymentMethodToken = paymentMethodToken;
	}
}
