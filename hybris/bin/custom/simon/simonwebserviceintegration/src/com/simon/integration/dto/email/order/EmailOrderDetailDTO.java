package com.simon.integration.dto.email.order;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "order")
public class EmailOrderDetailDTO {
	
	@XmlAttribute(name = "orderid")
	private String orderid;
	
	@XmlAttribute(name = "orderdate")
	private String orderdate;
	
	@XmlAttribute(name = "orderpayment")
	private String orderpayment;
	
	@XmlAttribute(name = "ordersubtotal")
	private String ordersubtotal;
	
	@XmlAttribute(name = "ordershipping")
	private String ordershipping;
	
	@XmlAttribute(name = "ordertaxes")
	private String ordertaxes;
	
	@XmlAttribute(name = "ordertotal")
	private String ordertotal;
	
	@XmlAttribute(name = "ordersaved")
	private String ordersaved;
	
	@XmlAttribute(name = "orderstatus")
	private String orderstatus;

	@XmlAttribute(name = "siteURL")
	private String siteURL;

	/**
	 * @param siteURL Site URL to set
	 */
	public void setSiteURL(String siteURL) {
		this.siteURL = siteURL;
	}

	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	/**
	 * @param orderdate the orderdate to set
	 */
	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}

	/**
	 * @param orderpayment the orderpayment to set
	 */
	public void setOrderpayment(String orderpayment) {
		this.orderpayment = orderpayment;
	}

	/**
	 * @param ordersubtotal the ordersubtotal to set
	 */
	public void setOrdersubtotal(String ordersubtotal) {
		this.ordersubtotal = ordersubtotal;
	}

	/**
	 * @param ordershipping the ordershipping to set
	 */
	public void setOrdershipping(String ordershipping) {
		this.ordershipping = ordershipping;
	}

	/**
	 * @param ordertaxes the ordertaxes to set
	 */
	public void setOrdertaxes(String ordertaxes) {
		this.ordertaxes = ordertaxes;
	}

	/**
	 * @param ordertotal the ordertotal to set
	 */
	public void setOrdertotal(String ordertotal) {
		this.ordertotal = ordertotal;
	}

	/**
	 * @param ordersaved the ordersaved to set
	 */
	public void setOrdersaved(String ordersaved) {
		this.ordersaved = ordersaved;
	}

	/**
	 * @param orderstatus the orderstatus to set
	 */
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	

}
