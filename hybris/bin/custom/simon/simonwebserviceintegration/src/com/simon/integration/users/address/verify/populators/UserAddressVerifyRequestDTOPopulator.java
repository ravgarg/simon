package com.simon.integration.users.address.verify.populators;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;

import de.hybris.platform.converters.Populator;

/**
 * Converter implementation for {@link ExtAddressData} as source and
 * {@link UserAddressVerifyRequestDTO} as target type.
 */
public class UserAddressVerifyRequestDTOPopulator implements Populator<ExtAddressData, UserAddressVerifyRequestDTO> {

	private static final String SPACE = " ";
	
	@Override
	public void populate(ExtAddressData source, UserAddressVerifyRequestDTO target) {

		target.setStreet1(source.getLine1() != null ? source.getLine1() : StringUtils.EMPTY);
		target.setStreet2(source.getLine2() != null ? source.getLine2() : StringUtils.EMPTY);
		target.setCity(source.getTown() != null ? source.getTown() : StringUtils.EMPTY);
		target.setZip(source.getPostalCode() != null ? source.getPostalCode() : StringUtils.EMPTY);
		if (null != source.getRegion())
		{
			target.setState(source.getRegion().getIsocodeShort());
		}
		if (null != source.getCountry())
		{
			target.setCountry(source.getCountry().getIsocode());
		}
		target.setPhone(source.getPhone() != null ? source.getPhone() : StringUtils.EMPTY);
		target.setEmail(source.getEmail() != null ? source.getEmail() : StringUtils.EMPTY);
		final StringBuilder userFullName = new StringBuilder(source.getFirstName());
		if(StringUtils.isNotEmpty(source.getLastName())){
			userFullName.append(SPACE + source.getLastName());
		}
		target.setName(userFullName.toString());
		target.setCompany(source.getCompanyName() != null ? source.getCompanyName() : StringUtils.EMPTY);
	}


}