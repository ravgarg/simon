package com.simon.integration.users.login.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCenterIdDTO implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String id;
	private String outletName;
	
	public String getId()
	{
		return id;
	}
	
	@JsonProperty("ID") 
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getOutletName()
	{
		return outletName;
	}
	
	@JsonProperty("OutletName") 
	public void setOutletName(String outletName)
	{
		this.outletName = outletName;
	}
}
