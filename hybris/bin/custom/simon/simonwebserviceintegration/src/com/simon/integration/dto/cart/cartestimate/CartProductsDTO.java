
package com.simon.integration.dto.cart.cartestimate;

import java.util.Map;

import com.simon.integration.dto.PojoAnnotation;

/**
 * 
 * @author ssi249
 *
 */
public class CartProductsDTO extends PojoAnnotation {

	/**
	 * <i>Generated property</i> for <code>CartProductsDTO.productID</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String productID;

	/**
	 * <i>Generated property</i> for <code>CartProductsDTO.size</code> property
	 * defined at extension <code>simonwebserviceintegration</code>.
	 */

	private Map<String, String> variants;

	/**
	 * <i>Generated property</i> for <code>CartProductsDTO.quantity</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String quantity;

	public void setProductID(final String productID) {
		this.productID = productID;
	}

	public String getProductID() {
		return productID;
	}

	public Map<String, String> getVariants() {
		return variants;
	}

	public void setVariants(Map<String, String> variants) {
		this.variants = variants;
	}

	public void setQuantity(final String quantity) {
		this.quantity = quantity;
	}

	public String getQuantity() {
		return quantity;
	}

}
