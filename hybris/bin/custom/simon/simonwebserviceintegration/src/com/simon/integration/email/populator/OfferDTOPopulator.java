package com.simon.integration.email.populator;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.deal.EmailOfferDTO;
import com.simon.integration.dto.email.deal.OfferDTO;
import com.simon.integration.dto.email.deal.RetailerDTO;
import com.simon.integration.dto.email.product.ShareMessageDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;

/**
 * The offerDTO populator from ShareEmailData.
 */
public class OfferDTOPopulator implements Populator<ShareEmailData, EmailOfferDTO> {

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(ShareEmailData source, EmailOfferDTO target) {
		final ShareMessageDTO message = new ShareMessageDTO();
		message.setText(source.getMessage());
		target.setMessage(message);
		populateRetailerDto(source, target);
	}

	/**
	 * Populate EmailOfferDTO from ShareEmailData
	 * 
	 * @param source
	 *            The ShareEmailData
	 * @param target
	 *            The EmailOfferDTO
	 */
	private void populateRetailerDto(ShareEmailData source, EmailOfferDTO target) {
		if (source.getRetailer() != null) {
			final RetailerDTO retailer = new RetailerDTO();
			final String logo = source.getRetailer().getLogo();
			retailer.setLogo(
					StringUtils.isNotEmpty(logo) ? shareEmailIntegrationUtils.buildImageUrl(logo) : StringUtils.EMPTY);
			retailer.setName(source.getRetailer().getName());
			retailer.setSiteURL(shareEmailIntegrationUtils.getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_CONTEXT_ROOT));
			populateOffer(source, retailer);
			target.setRetailer(retailer);
		}
	}

	/**
	 * Populate RetailerDTO from ShareEmailData.
	 * 
	 * @param source
	 *            The ShareEmailData
	 * @param retailer
	 *            The RetailerDTO
	 */
	private void populateOffer(ShareEmailData source, RetailerDTO retailer) {
		if (source.getRetailer().getOffer() != null) {
			final  OfferDTO offer = new OfferDTO();
			offer.setDescription(source.getRetailer().getOffer().getDescription());
			offer.setDisclaimer(source.getRetailer().getOffer().getDisclaimer());
			offer.setValid(source.getRetailer().getOffer().getValid());
			final String shopUrl = source.getRetailer().getOffer().getShopurl();
			offer.setShopurl(StringUtils.isNotEmpty(shopUrl) ? shareEmailIntegrationUtils.buildShopNowLabel(shopUrl)
					: StringUtils.EMPTY);
			retailer.setOffer(offer);
		}

	}

}
