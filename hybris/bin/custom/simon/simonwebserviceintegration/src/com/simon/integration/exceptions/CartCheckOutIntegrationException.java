package com.simon.integration.exceptions;

public class CartCheckOutIntegrationException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99002L;

	/** Exception code. */
	private final String code;


	/**
	 * Instantiates a new CartCheckOutIntegration exception.
	 *
	 * @param message
	 *           the message
	 */
	public CartCheckOutIntegrationException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new CartCheckOutIntegration exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public CartCheckOutIntegrationException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}


	/**
	 * Instantiates a new CartCheckOutIntegration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public CartCheckOutIntegrationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public CartCheckOutIntegrationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public CartCheckOutIntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}


}
