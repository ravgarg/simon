package com.simon.integration.dto.email.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "retailer")
@XmlAccessorType (XmlAccessType.FIELD)
public class EmailProductRetailerDTO {
	
	@XmlAttribute(name = "name")
	private String retailerName;
	
	@XmlAttribute(name = "siteURL")
	private String siteURL;
	
	@XmlElement(name = "item")
	private EmailProductDetailDTO productDetails;

	public void setSiteURL(String siteURL) {
		this.siteURL = siteURL;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public void setProductDetails(EmailProductDetailDTO productDetails) {
		this.productDetails = productDetails;
	}
	
}
