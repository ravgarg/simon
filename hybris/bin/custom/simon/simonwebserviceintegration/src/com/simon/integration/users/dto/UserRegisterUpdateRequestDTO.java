package com.simon.integration.users.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.PojoAnnotation;
import com.simon.integration.users.login.dto.UserCenterIdDTO;

public class UserRegisterUpdateRequestDTO extends PojoAnnotation {

	private String firstName;
	private String lastName;
	private String gender;
	private String zipCode;
	private String emailAddress;
	private Integer householdIncome;
	private String password;
	private Integer birthYear;
	private String country;
	private List<UserCenterIdDTO> centerIds;
	private String birthMonth;
	private String mobileNumber;
	private String oldPassword;
	private int optedInEmail;
	

	public String getEmailAddress()
	{
		return emailAddress;
	}

	@JsonProperty("EmailAddress") 
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getFirstName()
	{
		return firstName;
	}

	@JsonProperty("FirstName") 
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	@JsonProperty("LastName") 
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public int getBirthYear()
	{
		return birthYear;
	}
	public String getGender()
	{
		return gender;
	}

	@JsonProperty("Gender") 
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	@JsonProperty("ZipCode") 
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getPassword()
	{
		return password;
	}

	@JsonProperty("Password") 
	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getCountry()
	{
		return country;
	}

	@JsonProperty("Country") 
	public void setCountry(String country)
	{
		this.country = country;
	}


	
	public List<UserCenterIdDTO> getCenterIds()
	{
		return centerIds;
	}

	@JsonProperty("CenterIds") 
	public void setCenterIds(List<UserCenterIdDTO> centerIds)
	{
		this.centerIds = centerIds;
	}

	
	public String getBirthMonth()
	{
		return birthMonth;
	}

	@JsonProperty("BirthMonth") 
	public void setBirthMonth(String birthMonth)
	{
		this.birthMonth = birthMonth;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	@JsonProperty("MobileNumber") 
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}

	public String getOldPassword()
	{
		return oldPassword;
	}

	@JsonProperty("OldPassword") 
	public void setOldPassword(String oldPassword)
	{
		this.oldPassword = oldPassword;
	}

	public int getOptedInEmail() {
		return optedInEmail;
	}
	public Integer getHouseholdIncome() {
		return householdIncome;
	}

	@JsonProperty("opted_in_email")
	public void setOptedInEmail(int optedInEmail) {
		this.optedInEmail = optedInEmail;
	}
	@JsonProperty("HouseholdIncome") 
	public void setHouseholdIncome(Integer householdIncome) {
		this.householdIncome = householdIncome;
	}
	@JsonProperty("BirthYear") 
	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	
	
}