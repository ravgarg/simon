package com.simon.integration.cart.populators;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.dto.cart.createcart.RetailerDTO;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

public class CreateCartDataPopulator implements Populator<CartModel, CreateCartDTO> {

	@Override
	public void populate(CartModel source, CreateCartDTO target) {

		target.setCartID(source.getCode());
		target.setCreateCartInvocationTime(source.getCreateCartInvocationTime());

		final Map<String, RetailerDTO> retailerDTOMap = new HashMap<>();

		for (final AbstractOrderEntryModel entry : source.getEntries()) {
			final String retailerId = entry.getProduct().getShop().getId();
			if (!retailerDTOMap.containsKey(retailerId)) {
				final RetailerDTO retailerDTO = new RetailerDTO();
				retailerDTO.setRetailerID(retailerId);
				final Map<String, String> productMap = new HashMap<>();
				retailerDTO.setProductMap(productMap);
				retailerDTOMap.put(retailerId, retailerDTO);
			}

			final GenericVariantProductModel product = (GenericVariantProductModel) entry.getProduct();
			final RetailerDTO existingRetailerDTO = retailerDTOMap.get(retailerId);
			final Map<String, String> productMap = existingRetailerDTO.getProductMap();
			productMap.put(product.getCode(), product.getProductURL());

			if (StringUtils.isEmpty(existingRetailerDTO.getRetailerURL())) {
				if (StringUtils.isNotBlank(entry.getProduct().getShop().getRetailerURL())) {
					existingRetailerDTO.setRetailerURL(entry.getProduct().getShop().getRetailerURL());
				} else if (StringUtils.isNotBlank(product.getProductURL())) {
					try {
						final URI uri = new URI(product.getProductURL());
						final String url = uri.getHost();
						existingRetailerDTO.setRetailerURL(url);
					} catch (URISyntaxException uriSyntaxException) {
						throw new IntegrationException("Retailer URL found invalid", uriSyntaxException);
					}
				}
			}
		}
		final List<RetailerDTO> retailerDTOList = new ArrayList<>(retailerDTOMap.values());
		target.setRetailers(retailerDTOList);
	}
}
