package com.simon.integration.users.address.verify.exceptions;

public class UserAddressVerifyIntegrationException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99002L;

	/** Exception code. */
	private final String code;


	/**
	 * Instantiates a new UserAddressVerifyIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 */
	public UserAddressVerifyIntegrationException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new UserAddressVerifyIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public UserAddressVerifyIntegrationException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}


	/**
	 * Instantiates a new UserAddressVerifyIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public UserAddressVerifyIntegrationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public UserAddressVerifyIntegrationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public UserAddressVerifyIntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}

}