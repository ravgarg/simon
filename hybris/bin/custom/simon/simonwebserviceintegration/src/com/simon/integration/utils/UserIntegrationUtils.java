package com.simon.integration.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.ResponseDTO;


import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

/**
 *User Integration Utils provides utility methods for Integration Layer
 *
 */
public class UserIntegrationUtils
{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserIntegrationUtils.class);
	

	private static final String TOKEN_EXPIRED_MESSAGE = "Session Expired. Please Re-Login";
	private static final List<String> TOKEN_EXPIRED_ERROR_CODE_LIST = Arrays.asList("717", "712", "722", "701", "731", "741", "736", "860");
	
	@Resource
	private ConfigurationService configurationService;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	private static List<String> creditCardTypeList = Arrays.asList(
			SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_VISA,
			SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_AMERICAN_EXPRESS,
			SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_MASTER);
	
	private static final List<String> BIRTH_MONTHS=Arrays.asList(Months.JAN.getCode(),Months.FEB.getCode(),Months.MAR.getCode(),Months.APR.getCode(),Months.MAY.getCode(),Months.JUN.getCode(),Months.JUL.getCode(),Months.AUG.getCode(),Months.SEPT.getCode(),Months.OCT.getCode(),Months.NOV.getCode(),Months.DEC.getCode());
	/**
	 * This method is used to get BirthMonth number corresponding to the short name. 
	 * @param birthMonth
	 * @return birth Month Code
	 */
	public String getBirthMonthCode(final String birthMonth){
		return String.valueOf(BIRTH_MONTHS.indexOf(birthMonth)+1);
	}
	/**
	 * This method is used to get Birth Month corresponding to the birth month number. 
	 * @param birthMonthCode
	 * @return birth Month
	 */
	public String getBirthMonthFromCode(final String birthMonthCode){
		return BIRTH_MONTHS.get(Integer.parseInt(birthMonthCode)-1);
	}
	
	/**
	 * This method is used to map gender to the gender code.
	 *
	 * @param gender
	 * @return 	1 for Male
	 * 			2 for Female
	 * 			0 for Not Disclosed
	 */
	public String getGenderCode(final String gender)
	{
		String genderCode = null;
		switch (StringUtils.upperCase(gender))
		{
			case "MALE":
				genderCode = "1";
				break;
			case "FEMALE":
				genderCode = "2";
				break;
			default:
				genderCode = "0";
				break;
		}
		return genderCode;
	}
	/**
	 * This method is used to get Customer  Gender from Code.
	 *
	 * @param gender
	 * @return 	M for Male
	 * 			F for Female
	 * 			Other for Not Disclosed
	 */
	public CustomerGender getGenderFromCode(final String gender)
	{
		
			switch (StringUtils.upperCase(gender))
			{
				case "1":
					return CustomerGender.MALE;
					
				case "2":
					return CustomerGender.FEMALE;
					
				default:
					return CustomerGender.NOT_DISCLOSED;
				
		}
	}

	/**
	 * @method validateState
	 * @param isValid
	 * @param errorMessage
	 */
	public void validateState(boolean isValid, String errorMessage) {

		LOGGER.debug("UserIntegrationUtils ::: validateState :: isValid : {} , errorMessage : {} ", isValid,
				errorMessage);
		if (!isValid) {
			LOGGER.error(errorMessage);
			throw new IllegalStateException(errorMessage);
		}
	}
	
	/**
	 * This method is used to get creditCardType number corresponding to the
	 * short name.
	 * 
	 * @param creditCardType
	 * @return credit card Code
	 */
	public String getCreditCardCode(final String creditCardType) {
		
		if (StringUtils.isNoneBlank(creditCardType)) {
			return String.valueOf(creditCardTypeList.indexOf(creditCardType.toLowerCase(Locale.US)) + 1);
		} else {
			return null;
		}
	}

	/**
	 * This method is used to prefix the env to address Id
	 * 
	 * @param addressId
	 * @return String
	 */
	public String addEnvPrefixToAddressId(final String addressId) {

		LOGGER.debug("UserIntegrationUtils ::: addEnvPrefixToAddressId :: addressId : {} ", addressId);

		String envPrefixedAddressId = StringUtils.EMPTY;

		if (StringUtils.isBlank(addressId)) {
			return envPrefixedAddressId;
		}
		final String envPrefix = configurationService.getConfiguration()
				.getString(SimonIntegrationConstants.USER_ADDRESS_EXPORT_EXTERNAL_ID_PREFIX);

		envPrefixedAddressId = envPrefix + addressId;

		LOGGER.debug("UserIntegrationUtils ::: addEnvPrefixToAddressId :: envPrefixedAddressId : {}",
				envPrefixedAddressId);

		return envPrefixedAddressId;
	}
	
	/**
	 * This method is used to prefix the env to address Id
	 * 
	 * @param addressId
	 * @return String
	 */
	public String removeEnvPrefixToAddressId(final String addressId) {

		LOGGER.debug("UserIntegrationUtils ::: addEnvPrefixToAddressId :: addressId : {} ", addressId);

		String envPrefixedRemovedFromId = StringUtils.EMPTY;

		if (StringUtils.isBlank(addressId)) {
			return envPrefixedRemovedFromId;
		}
		envPrefixedRemovedFromId=StringUtils.substringAfter(addressId, "_");

		LOGGER.debug("UserIntegrationUtils ::: addEnvPrefixToAddressId :: envPrefixedAddressId : {}",
				envPrefixedRemovedFromId);

		return envPrefixedRemovedFromId;
	}
	
	/**
	 * @param serviceEndpointApiUrlKey 
	 * @description Method use to build the esb layer endpoint service api end point url
	 * @method buildEsbLayerIntegrationServiceEndpointApiUrl
	 * @return String
	 */
	public String buildEsbLayerIntegrationServiceEndpointApiUrl(String serviceEndpointApiUrlKey){
		final StringBuilder serviceEndPointApiUrl = new StringBuilder();
		final String baseServiceApiUrl = buildEsbLayerIntegrationServiceBaseApiUrl();
		serviceEndPointApiUrl.append(baseServiceApiUrl);
		
		final String esbServiceEndpointApiUrl = configurationService.getConfiguration().getString(serviceEndpointApiUrlKey);
		validateState(StringUtils.isNotEmpty(esbServiceEndpointApiUrl), "ESB Integration Layer end point not found for " + serviceEndpointApiUrlKey);
		serviceEndPointApiUrl.append(SimonIntegrationConstants.SLASH);
		serviceEndPointApiUrl.append(esbServiceEndpointApiUrl);	
		return serviceEndPointApiUrl.toString();

	}
	
	/**
	 * @description Method use to build the esb layer base service api end point url
	 * @method buildEsbLayerIntegrationServiceBaseApiUrl
	 * @return StringBuilder
	 */
	private String buildEsbLayerIntegrationServiceBaseApiUrl() {
		final String baseServiceApiUrl = configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_LAYER_SERVICE_BASE_API_URL);
		LOGGER.debug("OrderConfirmationIntegrationUtils ::: buildEsbLayerIntegrationServiceBaseApiUrl :: baseServiceApiUrl : {}", baseServiceApiUrl);
		validateState(StringUtils.isNotEmpty(baseServiceApiUrl), "Base url for ESB Layer Integration Service API not found");
		return baseServiceApiUrl;
	}
	
	/**
	 * @description Method use to get Header Authorization value to call the ESB Integration Service
	 * @method getEsbIntegrationServiceHeaderAuthorizationValue
	 * @return String
	 */
	public String getEsbIntegrationServiceHeaderAuthorizationValue(){
		final String esbIntegrationHeaderAuthValue = configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_INTEGRATION_SERVICE_HEADER_AUTHORIZATION_VALUE);
		validateState(StringUtils.isNotEmpty(esbIntegrationHeaderAuthValue), "ESB integration service header authorization value not found");
		return esbIntegrationHeaderAuthValue;
	}
	
	/**
	 * This method is used to generate Auth header for authenticating with ESB layer.
	 * @return {@link RestAuthHeaderDTO}
	 */
	public RestAuthHeaderDTO populateAuthHeaderForESB	() {
		final RestAuthHeaderDTO restAuthHeaderDTO = new RestAuthHeaderDTO();
		final String userName = configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_USER_ID);
		final String password = configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_USER_PWRD);
		restAuthHeaderDTO.setPassword(password);
		restAuthHeaderDTO.setUserName(userName);
		return restAuthHeaderDTO;
	}
	
	/**
	 * This method creates a multi-value map and adds header data in it.
	 * @return {@link MultiValueMap}
	 */
	public MultiValueMap<String, Object> populateHeadersForESB() {
		final String token=sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN);
		final MultiValueMap<String, Object> headers = new LinkedMultiValueMap<>();
		headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON);
		headers.add(SimonIntegrationConstants.PO_TOKEN, token);
		return headers;
	}
	/**
	 * This method is used to handle common errors thrown by ESB.
	 * @param response
	 * @throws TokenExpiredException
	 */
	public void handleCommonErrors(ResponseDTO response) 
	{
			
		if(TOKEN_EXPIRED_ERROR_CODE_LIST.contains(response.getErrorCode()))
		{
			LOGGER.error(TOKEN_EXPIRED_MESSAGE,response.getErrorCode());
		}
	}	
	
}
