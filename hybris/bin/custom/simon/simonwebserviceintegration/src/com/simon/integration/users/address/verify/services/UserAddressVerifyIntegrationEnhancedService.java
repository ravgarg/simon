
package com.simon.integration.users.address.verify.services;

import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;

/**
 * The Interface UserAddressVerifyIntegrationEnhancedService.	
 */
public interface UserAddressVerifyIntegrationEnhancedService
{

	/**
	 * Create Cart. This method take cartModel and create cart message to ESB
	 *
	 * @param cartModel
	 *           the cart model
	 */
	UserAddressVerifyResponseDTO validateAddress(UserAddressVerifyRequestDTO addressData);
}