
package com.simon.integration.users.address.verify.services.impl;

import javax.annotation.Resource;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.exceptions.IntegrationException;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationEnhancedService;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationService;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * The Implementation DefaultUserAddressVerifyIntegrationService Class, use to populate data to dto and call the enhanced service
 */
public class DefaultUserAddressVerifyIntegrationService implements UserAddressVerifyIntegrationService  {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserAddressVerifyIntegrationService.class);
	/** The integration enhanced service. */
	@Resource
	private UserAddressVerifyIntegrationEnhancedService userAddressVerifyIntegrationEnhancedService;
	
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private Converter<ExtAddressData, UserAddressVerifyRequestDTO> userAddressVerifyRequestDTOConverter;
	
	@Override
	public UserAddressVerifyResponseDTO validateAddress(ExtAddressData addressData) throws UserAddressVerifyIntegrationException {
		
		try{
			final UserAddressVerifyRequestDTO userAddressVerifyRequestDTO = userAddressVerifyRequestDTOConverter.convert(addressData);
			return userAddressVerifyIntegrationEnhancedService.validateAddress(userAddressVerifyRequestDTO);
		}
		catch(IntegrationException exception){
			LOGGER.error("Error getting when calling verify user address service for the user email {}", addressData.getEmail());
			LOGGER.error("Error getting when calling verify user address service ", exception);
			throw new UserAddressVerifyIntegrationException(exception.getMessage(), exception);
		}
		
	}
	
}