
package com.simon.integration.services;

import java.io.IOException;
import java.util.List;

import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;

import de.hybris.platform.core.model.order.CartModel;

/**
 * The Interface IntegrationService.
 */
public interface CartCheckOutIntegrationService
{

    /**
     * This method takes the {@link CartModel}, converts it into {@link CreateCartDTO} and passes onto services
     * accessing JMS
     *
     * @param cartModel the cart model
     */
    void invokeCreateCartRequest(CartModel cartModel) throws CartCheckOutIntegrationException;

    /**
     * This method stubs the mock response into the system for Create Cart Callback
     * 
     * @throws IOException
     */
    CreateCartConfirmationDTO invokeCartConfirmationMockRequest() throws IOException;

    /**
     * This method takes the {@link CartModel} and a list of {@link VariantAttributeMappingModel}. It converts
     * {@link CartModel} to {@link CartEstimateRequestDTO} and maps the variant attributes as well.
     *
     * @param {@link CartModel}
     * @param {@link List {@link VariantAttributeMappingModel}}
     * @return {@link CartEstimateResponseDTO}
     */
    CartEstimateResponseDTO invokeCartEstimateRequest(CartModel cartModel, List<VariantAttributeMappingModel> variantAttributeMappingList)
        throws CartCheckOutIntegrationException;

    /**
     * This method stubs the mock response for Order Update into the system
     *
     * @param stubFileName
     */
    OrderConfirmCallBackDTO invokeOrderUpdateConfirmMockRequest(String stubFileName) throws IOException;

}
