package com.simon.integration.users.favourites.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.util.StringUtils;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.users.favourites.dto.AddUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.RemoveUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteDTO;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationEnhancedService;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.config.ConfigurationService;


/**
 * The DefaultUserFavouritesIntegrationService is used to integrate favorites data with PO.com
 *
 */
public class DefaultUserFavouritesIntegrationService implements UserFavouritesIntegrationService
{
	@Resource
	private UserFavouritesIntegrationEnhancedService userFavouritesIntegrationEnhancedService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	@Resource
	private ConfigurationService configurationService;
	/**
	 * This method is used to get favorite product added to PO.com.
	 *  
	 * @param externalID
	 * @param favouriteValue
	 * @return true if favorite added successfully
	 */
	@Override
	public boolean addUserFavouriteProduct(String externalID,String favouriteValue)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final AddUserFavouriteRequestDTO request = populateRequestData(externalID,StringUtils.isEmpty(favouriteValue) ? externalID : favouriteValue);
	 
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_PRODUCT);		
		return userFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	/**
	 * This method is used to get favorite size added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue
	 * @return true if favorite added successfully
	 */
	@Override
	public boolean addUserFavouriteSize(String externalID,String favouriteValue)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final AddUserFavouriteRequestDTO request = populateRequestData(externalID,favouriteValue);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_SIZE);	
		return userFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	/**
	 * This method is used to get favorite designer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue
	 * @return true if favorite added successfully
	 */
	@Override
	public boolean addUserFavouriteDesigner(String externalID,String favouriteValue)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final AddUserFavouriteRequestDTO request = populateRequestData(externalID,favouriteValue);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_DESIGNER);
		return userFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	/**
	 * This method is used to get favorite retailer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue
	 * @return true if favorite added successfully
	 */
	@Override
	public boolean addUserFavouriteStore(String externalID,String favouriteValue)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final AddUserFavouriteRequestDTO request = populateRequestData(externalID,favouriteValue);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_RETAILER);
		return userFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	/**
	 * This method is used to get favorite offer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue
	 * @return true if favorite added successfully
	 */
	@Override
	public boolean addUserFavouriteOffer(String externalID,String favouriteValue)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final AddUserFavouriteRequestDTO request = populateRequestData(externalID,favouriteValue);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_OFFER);
		return userFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	/**
	 * This method is used to get favorite product removed from favorites list from PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	@Override
	public boolean removeUserFavouriteProduct(String externalID)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final RemoveUserFavouriteRequestDTO request = new RemoveUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_PRODUCT);
		return userFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	/**
	 * This method is used to get favorite size removed from favorites list from PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	@Override
	public boolean removeUserFavouriteSize(String externalID)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final RemoveUserFavouriteRequestDTO request = new RemoveUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_SIZE);
		return userFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	/**
	 * This method is used to get favorite designer removed from favorites list from PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	@Override
	public boolean removeUserFavouriteDesigner(String externalID)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final RemoveUserFavouriteRequestDTO request = new RemoveUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_DESIGNER);
		return userFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	/**
	 * This method is used to get favorite retailer removed from favorites list from PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	@Override
	public boolean removeUserFavouriteStore(String externalID)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final RemoveUserFavouriteRequestDTO request = new RemoveUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_RETAILER);
		return userFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	/**
	 * This method is used to get favorite offer removed from favorites list from PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	@Override
	public boolean removeUserFavouriteOffer(String externalID)
	{
		if (isEsbPoDisabled())
		{
			return true;
		}
		final RemoveUserFavouriteRequestDTO request = new RemoveUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavType(SimonIntegrationConstants.FAVOURITE_TYPE_OFFER);
		return userFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	/**
	 * This method is used to get all added favorite Products from PO.com.
	 * 
	 * @return all Favorite Products
	 */
	@Override
	public List<String> getAllFavouriteProducts()
	{
		return extractExternalIds(userFavouritesIntegrationEnhancedService.getFavourites(SimonIntegrationConstants.FAVOURITE_TYPE_PRODUCT).getProducts());
	}
	
	/**
	 * This method is used to get all added favorite Sizes from PO.com.
	 * 
	 * @return all Favorite Sizes
	 */
	@Override
	public List<String> getAllFavouriteSizes()
	{
		return extractExternalIds(userFavouritesIntegrationEnhancedService.getFavourites(SimonIntegrationConstants.FAVOURITE_TYPE_SIZE).getSizes());

	}
	
	/**
	 * This method is used to get all added favorite Designers from PO.com.
	 * 
	 * @return all Favorite Designers
	 */
	@Override
	public List<String> getAllFavouriteDesigners()
	{
		return extractExternalIds(userFavouritesIntegrationEnhancedService.getFavourites(SimonIntegrationConstants.FAVOURITE_TYPE_DESIGNER).getDesigners());	
		
	}
	
	/**
	 * This method is used to get all added favorite Retailers from PO.com.
	 * 
	 * @return all Favorite Retailers
	 */
	@Override
	public List<String> getAllFavouriteStores()
	{
		return extractExternalIds(userFavouritesIntegrationEnhancedService.getFavourites(SimonIntegrationConstants.FAVOURITE_TYPE_RETAILER).getRetailers());
		
	}
	
	/**
	 * This method is used to get all added favorite Offers from PO.com.
	 * 
	 * @return all Favorite Offers
	 */ 
	@Override
	public List<String> getAllFavouriteOffers()
	{
		return extractExternalIds(userFavouritesIntegrationEnhancedService.getFavourites(SimonIntegrationConstants.FAVOURITE_TYPE_OFFER).getOffers());
		
	}


		
	/**
	 * This method is used to populate request data for Add User Favorites. 
	 * 
	 * @param externalID
	 * @return
	 */
	private AddUserFavouriteRequestDTO populateRequestData(String externalID,String favouriteValue)
	{
		final AddUserFavouriteRequestDTO request=new AddUserFavouriteRequestDTO();
		request.setExternalID(externalID);
		request.setFavoriteValue(favouriteValue);
		return request;
	}
	
	/**
	 * This method is used to extract External IDs from the list of favorites obtained from PO.com.
	 * @param favourites
	 * @return List of External IDs
	 */
	private List<String> extractExternalIds(final List<UserFavouriteDTO> favourites){
		final List<String> externalIDs=new ArrayList<>();
		favourites.forEach(fav->externalIDs.add(fav.getExternalID()));
		return externalIDs;
	}
	/**
	 * @return
	 */
	private boolean isEsbPoDisabled()
	{
		return configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false);
	}

}
