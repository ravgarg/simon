
package com.simon.integration.dto.cart.cartestimate;

import com.simon.integration.dto.PojoAnnotation;

/**
 * 
 * @author ssi249
 *
 */
public class ShippingDetailsDTO extends PojoAnnotation {

	/**
	 * <i>Generated property</i> for <code>ShippingDetailsDTO.shippingZip</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String shippingZip;

	/**
	 * <i>Generated property</i> for
	 * <code>ShippingDetailsDTO.shippingCity</code> property defined at
	 * extension <code>simonwebserviceintegration</code>.
	 */

	private String shippingCity;

	/**
	 * <i>Generated property</i> for
	 * <code>ShippingDetailsDTO.shippingState</code> property defined at
	 * extension <code>simonwebserviceintegration</code>.
	 */

	private String shippingState;

	public void setShippingZip(final String shippingZip) {
		this.shippingZip = shippingZip;
	}

	public String getShippingZip() {
		return shippingZip;
	}

	public void setShippingCity(final String shippingCity) {
		this.shippingCity = shippingCity;
	}

	public String getShippingCity() {
		return shippingCity;
	}

	public void setShippingState(final String shippingState) {
		this.shippingState = shippingState;
	}

	public String getShippingState() {
		return shippingState;
	}

}
