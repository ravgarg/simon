
package com.simon.integration.dto.cart.cartestimate;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

public class CartEstimateResponseDTO extends PojoAnnotation
{

    private String cartID;

    private List<RetailerResponseDTO> retailers;

    private EstimatedPriceDTO totalEstimatedPriceDTO;

    private EstimatedDeliveryDaysDTO estimatedDeliveryDays;

    public void setCartID(final String cartID)
    {
        this.cartID = cartID;
    }

    public String getCartID()
    {
        return cartID;
    }

    public void setRetailers(final List<RetailerResponseDTO> retailers)
    {
        this.retailers = retailers;
    }

    public List<RetailerResponseDTO> getRetailers()
    {
        return retailers;
    }

    public void setTotalEstimatedPriceDTO(final EstimatedPriceDTO totalEstimatedPriceDTO)
    {
        this.totalEstimatedPriceDTO = totalEstimatedPriceDTO;
    }

    public EstimatedPriceDTO getTotalEstimatedPriceDTO()
    {
        return totalEstimatedPriceDTO;
    }

    public EstimatedDeliveryDaysDTO getEstimatedDeliveryDays()
    {
        return estimatedDeliveryDays;
    }

    public void setEstimatedDeliveryDays(EstimatedDeliveryDaysDTO estimatedDeliveryDays)
    {
        this.estimatedDeliveryDays = estimatedDeliveryDays;
    }

}
