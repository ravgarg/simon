package com.simon.integration.users.favourites.services;

import com.simon.integration.users.favourites.dto.AddUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.RemoveUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteResponseDTO;
/**
 * This Interface is used to integrate favorites data with PO.com
 *
 */
public interface UserFavouritesIntegrationEnhancedService
{
	/**
	 * This method is used to synch a favorite added of particular type with PO.com. 
	 * 
	 * @param request
	 * @return true if favorite added successfully, else false.
	 */
	public boolean addUserFavourite(AddUserFavouriteRequestDTO request);
	/**
	 * This method is used to synch a favorite removed of particular type with PO.com depending on the URL provided. 
	 * 
	 * @param request
	 * @return true if favorite removed successfully, else false.
	 */
	public boolean removeFavourite(RemoveUserFavouriteRequestDTO request);
	/**
	 * This method is used to get all favorites synched with PO.com depending upon URL. 
	 * 
	 * @param url
	 * @return {@link UserFavouriteResponseDTO}.
	 */
	public UserFavouriteResponseDTO getFavourites(String url);
}
