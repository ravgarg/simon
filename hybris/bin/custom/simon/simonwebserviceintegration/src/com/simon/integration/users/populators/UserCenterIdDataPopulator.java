package com.simon.integration.users.populators;

import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.login.dto.UserCenterIdDTO;

import de.hybris.platform.converters.Populator;

/**
* Converter implementation for
* {@link com.simon.facades.users.data.UserCenterIdData} as source and
* {@link com.simon.integration.users.login.dto.UserCenterIdDTO} as target
* type.
*/
public class UserCenterIdDataPopulator implements Populator<MallData, UserCenterIdDTO>{
	
	@Override
	public void populate(MallData source, UserCenterIdDTO target){
		target.setId(source.getCode());
		target.setOutletName(source.getName());
	}
}