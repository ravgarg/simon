package com.simon.integration.dto.email.order;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
@XmlAccessorType (XmlAccessType.FIELD)
public class EmailOrderRequestDTO {
	
	@XmlElement(name = "order")
	private EmailOrderDetailDTO order;
	
	@XmlElement(name = "shipping")
	private EmailShippingDetailDTO shipping;
	
	@XmlElement(name = "retailer")
	private List<EmailOrderRetailerDTO> retailers;

	/**
	 * @param order the order to set
	 */
	public void setOrder(EmailOrderDetailDTO order) {
		this.order = order;
	}

	/**
	 * @param shipping the shipping to set
	 */
	public void setShipping(EmailShippingDetailDTO shipping) {
		this.shipping = shipping;
	}

	/**
	 * @param retailers the retailers to set
	 */
	public void setRetailers(List<EmailOrderRetailerDTO> retailers) {
		this.retailers = retailers;
	}

}
