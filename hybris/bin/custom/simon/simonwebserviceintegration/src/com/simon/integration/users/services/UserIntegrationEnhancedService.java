
package com.simon.integration.users.services;

import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserCenterRequestDTO;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;

/**
 * The Interface UserIntegrationEnhancedService.	
 */
public interface UserIntegrationEnhancedService
{
	
	/**
	 * @description Method use to update the registered user details
	 * @method updateUserDetails
	 * @param updateUserDetailsDTO
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException
	 */
	VIPUserDTO updateUserDetails(UserRegisterUpdateRequestDTO updateUserDetailsDTO) throws UserIntegrationException;
	
	/**
	 * This method is used to register a user with PO.com with user details mapped in {@link UserRegisterUpdateRequestDTO} and then parse response in
	 * UserLoginResponseDTO.
	 * @param poVIPUserDTO
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException 
	 * @throws AuthLoginIntegrationException 
	 */
	VIPUserDTO registerUser(UserRegisterUpdateRequestDTO poVIPUserDTO) throws UserIntegrationException, AuthLoginIntegrationException;
	/**
	 * This method is used get default center associated with a zip-code from PO.com.
	 * 
	 * @param userCenterRequestDTO
	 * @return {@link UserCenterIdDTO}
	 * @throws UserIntegrationException 
	 */
	UserCenterIdDTO getDefaultPrimaryCenter(UserCenterRequestDTO userCenterRequestDTO) throws UserIntegrationException;
}