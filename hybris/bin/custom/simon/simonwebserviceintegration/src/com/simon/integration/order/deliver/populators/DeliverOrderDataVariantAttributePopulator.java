package com.simon.integration.order.deliver.populators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.order.deliver.CartProductsDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.dto.order.deliver.RetailerDTO;
import com.simon.integration.utils.VariantAttributeMappingsUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;

/**
 * Class to populate Target Variant name in DeliverOrderRequestDTO.
 */
public class DeliverOrderDataVariantAttributePopulator
		implements Populator<Pair<OrderModel, List<VariantAttributeMappingModel>>, DeliverOrderRequestDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeliverOrderDataVariantAttributePopulator.class);

	@Override
	public void populate(Pair<OrderModel, List<VariantAttributeMappingModel>> orderModalVariantAttributePair,
			DeliverOrderRequestDTO target) {
		final List<RetailerDTO> retailersList = target.getRetailers();
		final List<VariantAttributeMappingModel> variantAttributeList = orderModalVariantAttributePair.getRight();
		putTargetVariantattributeInProduct(retailersList, variantAttributeList);

	}

	private void putTargetVariantattributeInProduct(List<RetailerDTO> retailersList,
			List<VariantAttributeMappingModel> varientAttributeMappings) {
		final Map<String, Map<String, String>> varientAttributeMappingsMap = VariantAttributeMappingsUtils
				.convertVariantAttributeMappingsMap(varientAttributeMappings);
		for (final RetailerDTO retailerDto : retailersList) {
			final List<CartProductsDTO> cartProducts = retailerDto.getProducts();
			for (final CartProductsDTO cartProduct : cartProducts) {
				final Map<String, String> productAttributeMap = putTargetVariantAttributeInProductMap(
						retailerDto.getRetailerId(), cartProduct, varientAttributeMappingsMap);
				cartProduct.setProductAttributeMap(productAttributeMap);
			}

		}
	}

	private Map<String, String> putTargetVariantAttributeInProductMap(String shopId, CartProductsDTO cartProduct,
			Map<String, Map<String, String>> varientAttributeMappingsMap) {
		final Map<String, String> productAttributes = new HashMap<>();

		for (final Map.Entry<String, String> cartProductAttributeEntry : cartProduct.getProductAttributeMap().entrySet()) {
			productAttributes.put(
					getTargetAttribute(cartProductAttributeEntry.getKey(), varientAttributeMappingsMap, shopId),
					cartProductAttributeEntry.getValue());
		}
		return productAttributes;
	}

	private String getTargetAttribute(String source, Map<String, Map<String, String>> varientAttributeMappingsMap,
			String shopId) {
		String targetAttributeName = source;
		if (!CollectionUtils.isEmpty(varientAttributeMappingsMap)) {
			final Map<String, String> sourceTargetAttributeMap = varientAttributeMappingsMap.get(shopId);
			if (MapUtils.isNotEmpty(sourceTargetAttributeMap) && StringUtils.isNotEmpty(sourceTargetAttributeMap.get(source))) {
				targetAttributeName = sourceTargetAttributeMap.get(source);
				LOGGER.debug("Target Attribute {} is added as per Mapping table for Source {} with Shop id {}",
						sourceTargetAttributeMap.get(source), source, shopId);
			}
		}
		return targetAttributeName;
	}

}