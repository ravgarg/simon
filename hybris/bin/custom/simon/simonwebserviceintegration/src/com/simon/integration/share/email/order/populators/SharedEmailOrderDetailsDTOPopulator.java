package com.simon.integration.share.email.order.populators;

import java.io.StringWriter;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.order.EmailOrderRequestDTO;
import com.simon.integration.dto.email.product.ShareEmailAttributesDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Converts ShareEmailData to ShareEmailRequestDTO
 */
public class SharedEmailOrderDetailsDTOPopulator implements Populator<ShareEmailData, ShareEmailRequestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SharedEmailOrderDetailsDTOPopulator.class);

	@Resource
	private Converter<EmailOrderData, EmailOrderRequestDTO> emailOrderDetailsDTOConverter;

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(ShareEmailData source, ShareEmailRequestDTO target) {

		target.setCustomerKey(shareEmailIntegrationUtils.getConfiguredStaticString("share.order.email.customer.key"));
		source.setEmailTo(source.getEmailOrderData().getEmailTo());
		target.setSubscriberDetails(shareEmailIntegrationUtils.populateShareSubscriberDetails(source));
		target.setEmailAttributes(populateShareEmailAttributes(source));

	}

	/**
	 * @method populateShareEmailAttributes
	 * @param source
	 * @return ShareEmailAttributesDTO
	 */
	private ShareEmailAttributesDTO populateShareEmailAttributes(ShareEmailData source) {
		final ShareEmailAttributesDTO shareEmailAttributesDTO = new ShareEmailAttributesDTO();
		shareEmailAttributesDTO
				.setSubjectLine(shareEmailIntegrationUtils.getConfiguredStaticString("share.order.email.subject.line"));
		shareEmailAttributesDTO
				.setXmlType(shareEmailIntegrationUtils.getConfiguredStaticString("share.order.email.xml.type"));

		final EmailOrderRequestDTO emailOrderRequestDto = emailOrderDetailsDTOConverter
				.convert(source.getEmailOrderData());
		shareEmailAttributesDTO.setXmlData(populateOrderEmailDataInXml(emailOrderRequestDto));
		return shareEmailAttributesDTO;
	}

	/**
	 * @description Method use to convert the DTO to String
	 * @method populateDataInXml
	 * @param productData
	 * @return String
	 */
	private String populateOrderEmailDataInXml(EmailOrderRequestDTO emailOrderRequestDto) {
		String escapedResult = StringUtils.EMPTY;
		final StringWriter stringWriter = new StringWriter();
		try {
			final JAXBContext context = JAXBContext.newInstance(EmailOrderRequestDTO.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(emailOrderRequestDto, stringWriter);
			escapedResult = shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter);
			LOG.debug("Shared Order email data :{}", escapedResult);
		} catch (JAXBException exception) {
			LOG.error("Exception occured while convert DTO object in the xml and return as String {} ", exception);
		}
		return escapedResult;
	}

}
