package com.simon.integration.share.email.services.impl;

import javax.annotation.Resource;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.integration.share.email.services.ShareEmailIntegrationService;

import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * class DefaultShareEmailIntegrationService use to maintains the methods for the shared product email data
 */
public class DefaultShareEmailIntegrationService implements ShareEmailIntegrationService {
	

	@Resource(name = "sharedEmailProductDTOConverter")
	private Converter<ShareEmailData, ShareEmailRequestDTO> sharedEmailProductDTOConverter;
	
	@Resource(name = "shareEmailIntegrationEnhancedService")
	private DefaultShareEmailIntegrationEnhancedService shareEmailIntegrationEnhancedService;
	
	@Resource
	private Converter<ShareEmailData,ShareEmailRequestDTO> shareEmailDTOConverter;
	
	@Resource(name = "sharedEmailOrderDetailsDTOConverter")
	private Converter<ShareEmailData, ShareEmailRequestDTO> sharedEmailOrderDetailsDTOConverter;
	
	/**
	 * @description Method use to populate product details to shared in email 
	 * @method sharedProductEmailData
	 * @param sharedProductEmailData
	 * @throws SharedEmailIntegrationException
	 */
	public void sharedProductEmailData(ShareEmailData sharedEmailCommonData) throws SharedEmailIntegrationException{
		final ShareEmailRequestDTO sharedProductEmailRequestDTO = sharedEmailProductDTOConverter.convert(sharedEmailCommonData);
		
		shareEmailIntegrationEnhancedService.shareEmail(sharedProductEmailRequestDTO);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void shareDealEmail(ShareEmailData shareEmailData)
			throws SharedEmailIntegrationException {
		final ShareEmailRequestDTO emailOfferDTO = shareEmailDTOConverter.convert(shareEmailData);
		shareEmailIntegrationEnhancedService.shareEmail(emailOfferDTO);
		
	}
	
	
	/**
	 * @description Method use to populate order data to share the order confirmation details in email 
	 * @method sharedOrderConfirmationEmailData
	 * @param emailOrderData
	 * @throws SharedEmailIntegrationException
	 */
	public void sharedOrderConfirmationEmailData(ShareEmailData shareEmailData) throws SharedEmailIntegrationException{
		final ShareEmailRequestDTO shareEmailRequestDTO = sharedEmailOrderDetailsDTOConverter.convert(shareEmailData);
		shareEmailIntegrationEnhancedService.shareEmail(shareEmailRequestDTO);
	}
}
