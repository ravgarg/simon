package com.simon.integration.dto.email.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
@XmlAccessorType (XmlAccessType.FIELD)
public class EmailProductRootDTO {
	
	@XmlElement(name = "retailer")
	private EmailProductRetailerDTO productDetails;
	
	@XmlElement(name = "message")
	private ShareMessageDTO message;
	
	public void setProductDetails(EmailProductRetailerDTO productDetails) {
		this.productDetails = productDetails;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(ShareMessageDTO message) {
		this.message = message;
	}

	
	
}
