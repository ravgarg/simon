package com.simon.integration.users.services;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;

/**
 * The Interface UserAddressIntegrationService.
 */
public interface UserAddressIntegrationService {

	/**
	 * This method is used to add a user address with PO.com with user address
	 * details mapped in {@link ExtAddressData} and then parse response in
	 * {@link UserAddressResponseDTO}.
	 * 
	 * @param extAddressData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO addUserAddress(ExtAddressData extAddressData) throws UserAddressIntegrationException;

	/**
	 * This method is used to update a user address with PO.com with user
	 * address details mapped in {@link ExtAddressData} and then parse response
	 * in {@link UserAddressResponseDTO}.
	 * 
	 * @param extAddressData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO updateUserAddress(ExtAddressData extAddressData) throws UserAddressIntegrationException;

	/**
	 * This method is used to remove a user address with PO.com and then parse
	 * response in {@link UserAddressResponseDTO}.
	 * 
	 * @param extAddressData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO removeUserAddress(ExtAddressData extAddressData) throws UserAddressIntegrationException;
	
	/**
	 * This method is used to add a user Payment address with PO.com with user Payment address
	 * details mapped in {@link CCPaymentInfoData} and then parse response in
	 * {@link UserAddressResponseDTO}.
	 * 
	 * @param ccPaymentInfoData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	UserAddressResponseDTO addUserPaymentAddress(CCPaymentInfoData ccPaymentInfoData) throws UserAddressIntegrationException;

	UserGetAddressResponseDTO getUserHomeAddress() throws UserAddressIntegrationException;

	UserGetAddressResponseDTO getUserShippingAddress() throws UserAddressIntegrationException;

	UserGetAddressResponseDTO getUserBillingAddress() throws UserAddressIntegrationException;
}
