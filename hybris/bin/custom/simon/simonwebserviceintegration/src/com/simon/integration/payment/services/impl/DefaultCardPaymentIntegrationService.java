package com.simon.integration.payment.services.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.exceptions.IntegrationException;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.generated.model.ErrorLogModel;
import com.simon.integration.dto.card.redact.RemovePaymentRequestDTO;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.dto.card.verification.CardVerificationRequestDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationEnhancedService;
import com.simon.integration.payment.services.CardPaymentIntegrationService;


import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelCreationException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;

public class DefaultCardPaymentIntegrationService implements CardPaymentIntegrationService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCardPaymentIntegrationService.class);

    @Resource
    private CardPaymentIntegrationEnhancedService cardPaymentIntegrationEnhancedService;

    @Resource
    private Converter<OrderModel, DeliverOrderRequestDTO> deliverOrderDataConverter;

	@Resource
	private Converter<Pair<OrderModel, Exception>, ErrorLogModel> errorLogConverter;
	
    @Resource
    private Converter<Pair<OrderModel, List<VariantAttributeMappingModel>>, DeliverOrderRequestDTO> deliverOrderDataVariantAttributeConverter;
	@Resource
	private ModelService modelService;
	
    @Override
    public CardVerificationResponseDTO invokeCardVerification(final String paymentMethodToken,final boolean retainOnSuccess,final boolean continueCaching) throws CartCheckOutIntegrationException
    {
        try
        {
            final CardVerificationRequestDTO cardVerificationRequestDTO = new CardVerificationRequestDTO();
            cardVerificationRequestDTO.setPaymentMethodToken(paymentMethodToken);
            cardVerificationRequestDTO.setRetainOnSuccess(retainOnSuccess);
            cardVerificationRequestDTO.setContinueCaching(continueCaching);
            return cardPaymentIntegrationEnhancedService.verifyCard(cardVerificationRequestDTO);
        }
        catch (IntegrationException exception)
        {
            throw new CartCheckOutIntegrationException(exception.getMessage(), exception);
        }
    }

    @Override
    public void invokeDeliverOrder(final OrderModel orderModel,final String paymentMethodToken,final List<VariantAttributeMappingModel> variantAttributeMappingList) throws CartCheckOutIntegrationException
    {
		DeliverOrderRequestDTO deliverOrderRequestDTO = deliverOrderDataConverter.convert(orderModel);
		deliverOrderRequestDTO.setPaymentMethodToken(paymentMethodToken);
		deliverOrderRequestDTO=deliverOrderDataVariantAttributeConverter.convert(ImmutablePair.of(orderModel, variantAttributeMappingList),deliverOrderRequestDTO);
		try {
			cardPaymentIntegrationEnhancedService.deliverOrder(deliverOrderRequestDTO);
		} catch (IntegrationException exception) {
			LOG.error("Exception : when calling deliver Order Service", exception);
			try{
			final ErrorLogModel errorLogModel=modelService.create(ErrorLogModel.class);
			errorLogConverter.convert(ImmutablePair.of(orderModel, exception), errorLogModel);
			modelService.save(errorLogModel);
			}catch(ModelSavingException | ModelCreationException e){
				LOG.error("Exception occurs while creating or saving Error Log", e);	
			}
			throw new CartCheckOutIntegrationException(exception.getMessage(), exception);
		}
	}

	@Override
	public RemovePaymentResponseDTO invokeRemovePaymentMethodToken(final String paymentMethodToken) throws CartCheckOutIntegrationException 
	{
		try {
			final RemovePaymentRequestDTO removePaymentRequestDTO = new RemovePaymentRequestDTO();
			removePaymentRequestDTO.setPaymentMethodToken(paymentMethodToken);
			return cardPaymentIntegrationEnhancedService.removePaymentMethodToken(removePaymentRequestDTO);
		} catch (IntegrationException exception) {
			throw new CartCheckOutIntegrationException(exception.getMessage(), exception);
		}
	}
}
