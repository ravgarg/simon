package com.simon.integration.share.email.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.email.SharedEmailResponseDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.integration.share.email.services.ShareEmailIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

/**
 * class DefaultShareEmailIntegrationEnhancedService use to maintains the methods for the shared product email data
 */
public class DefaultShareEmailIntegrationEnhancedService implements ShareEmailIntegrationEnhancedService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultShareEmailIntegrationEnhancedService.class);
	
	@Resource
	private RestWebService restWebService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	
	/**
	 * @description Method use to shared email using restWebService.
	 * @method sharedProductEmail
	 * @param shareEmailRequestDTO
	 * @throws SharedEmailIntegrationException
	 */
	@Override
	public void shareEmail(ShareEmailRequestDTO shareEmailRequestDTO) throws SharedEmailIntegrationException {
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.SHARED_EMAIL_INTEGRATION_SERVICE_END_POINT);
		SharedEmailResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(serviceUrl, shareEmailRequestDTO, Boolean.FALSE.toString(), null,
					SharedEmailResponseDTO.class, RequestMethod.POST, getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to share "+shareEmailRequestDTO.getEmailAttributes().getXmlType()+" details in email and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				throw new SharedEmailIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while share "+shareEmailRequestDTO.getEmailAttributes().getXmlType()+" details in email by third party: ", exception);
			throw new SharedEmailIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * @description This method creates a multi-value map and adds header data in it.
	 * @method getHeaders
	 * @return
	 */
	private MultiValueMap<String, Object> getHeaders()
    {
		 final MultiValueMap<String, Object> headers = getHeadersMap();
	     headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON);
	     return headers; 
    }

	private MultiValueMap<String, Object> getHeadersMap()
    {
        return new LinkedMultiValueMap<>();
    }

}
