package com.simon.integration.payment.services;

import java.util.List;

import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;

import de.hybris.platform.core.model.order.OrderModel;

/**
 * Payment and Deliver Integration Interface .This interface verify card with gateway and deliver Order.
 */

public interface CardPaymentIntegrationService {

    /**
     * Card Verification.This method take payment method token and verify card with gateway and then parse response in
     * CardVerificationResponseDTO.
     *
     * @param paymentMethodToken the payment token
     * @param paymentMethodToken the retainOnSuccess
     * @param paymentMethodToken the continueCaching
     * @return the Card Verification Response DTO
     */

    CardVerificationResponseDTO invokeCardVerification(String paymentMethodToken,boolean retainOnSuccess,boolean continueCaching)
            throws CartCheckOutIntegrationException;
    /**
     * Deliver Order.This method take Order model and deliver order via ESB to third party Interface.
     * @param paymentMethodToken 
     * @param variantAttributeMappingList 
     * @param OrderModel orderModel
     *
     */

    void invokeDeliverOrder(OrderModel orderModel, String paymentMethodToken, List<VariantAttributeMappingModel> variantAttributeMappingList)
            throws CartCheckOutIntegrationException;
    
    /**
     * Remove Payment Token .This method remove payment method token and then parse response in
     * CardVerificationResponseDTO.
     *
     * @param paymentMethodToken the payment token
     * @return the Card Verification Response DTO
     */

    RemovePaymentResponseDTO invokeRemovePaymentMethodToken(String paymentMethodToken)
            throws CartCheckOutIntegrationException;

}
