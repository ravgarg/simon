package com.simon.integration.dto.order.deliver;

import java.util.Map;

import com.simon.integration.dto.PojoAnnotation;

public class CartProductsDTO extends PojoAnnotation{
	
	private String productId;
	private Map<String,String> productAttributeMap;
	private String quantity;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Map<String, String> getProductAttributeMap() {
		return productAttributeMap;
	}
	public void setProductAttributeMap(Map<String, String> productAttributeMap) {
		this.productAttributeMap = productAttributeMap;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}
