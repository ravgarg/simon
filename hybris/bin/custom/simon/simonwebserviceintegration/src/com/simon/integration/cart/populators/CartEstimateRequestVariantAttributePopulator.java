package com.simon.integration.cart.populators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartProductsDTO;
import com.simon.integration.dto.cart.cartestimate.RetailerRequestDTO;
import com.simon.integration.utils.VariantAttributeMappingsUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.CartModel;

/**
 * Class to populate Target Variant name in CartEstimateRequestDTO.
 */
public class CartEstimateRequestVariantAttributePopulator
		implements Populator<Pair<CartModel, List<VariantAttributeMappingModel>>, CartEstimateRequestDTO> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CartEstimateRequestVariantAttributePopulator.class);

	@Override
	public void populate(Pair<CartModel, List<VariantAttributeMappingModel>> cartMappingPair,
			final CartEstimateRequestDTO target) {

		final List<VariantAttributeMappingModel> varientAttributeMappings = cartMappingPair.getRight();
		final List<RetailerRequestDTO> retailersList = target.getRetailers();
		putTargetVariantattributeInProduct(retailersList, varientAttributeMappings);
	}

	private void putTargetVariantattributeInProduct(List<RetailerRequestDTO> retailersList,
			List<VariantAttributeMappingModel> varientAttributeMappings) {
		final Map<String, Map<String, String>> varientAttributeMappingsMap = VariantAttributeMappingsUtils
				.convertVariantAttributeMappingsMap(varientAttributeMappings);
		for (final RetailerRequestDTO retailerRequestDto : retailersList) {

		final 	List<CartProductsDTO> cartProducts = retailerRequestDto.getCartProducts();
			for (final CartProductsDTO cartProduct : cartProducts) {
				final Map<String, String> variantMap = putTargetVariantAttributeInProductMap(
						retailerRequestDto.getRetailerID(), cartProduct, varientAttributeMappingsMap);
				cartProduct.setVariants(variantMap);
			}

		}
	}

	private Map<String, String> putTargetVariantAttributeInProductMap(String shopId, CartProductsDTO cartProduct,
			Map<String, Map<String, String>> varientAttributeMappingsMap) {
		final Map<String, String> productAttributes = new HashMap<>();

		for (final Map.Entry<String, String> cartProductAttributeEntry : cartProduct.getVariants().entrySet()) {
			productAttributes.put(
					getTargetAttribute(cartProductAttributeEntry.getKey(), varientAttributeMappingsMap, shopId),
					cartProductAttributeEntry.getValue());
		}
		return productAttributes;
	}

	private String getTargetAttribute(String source, Map<String, Map<String, String>> varientAttributeMappingsMap,
			String shopId) {
		String targetAttributeName = source;
		if (!CollectionUtils.isEmpty(varientAttributeMappingsMap)) {
		 	final  Map<String, String> sourceTargetAttributeMap = varientAttributeMappingsMap.get(shopId);
			if (MapUtils.isNotEmpty(sourceTargetAttributeMap) && StringUtils.isNotEmpty(sourceTargetAttributeMap.get(source))) {
				targetAttributeName = sourceTargetAttributeMap.get(source);
				LOGGER.debug("Target Attribute {} is added as per Mapping table for Source {} with Shop id {}",
						sourceTargetAttributeMap.get(source), source, shopId);
			}
		}
		return targetAttributeName;
	}

}