package com.simon.integration.users.login.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.ResponseDTO;

public class VIPUserDTO extends ResponseDTO 
{	private static final long serialVersionUID = 1L;
	private String id;
	private String emailAddress;
	private String firstName;
	private String lastName;
	private Integer birthYear;
	private String gender;
    private String zipCode;
    private String password;
    private String country;
    private String countryCode;
    private int householdIncome;
    private String language;
    private List<UserCenterIdDTO> centerIds;
    private List<UserBrandIdDTO> brandIds;
    private boolean verified;
    private String passwordResetLink;
    private boolean optedInEmail;
    private String stateCd;
    private boolean profileComplete;	
    private String usersPreferredCenter;
    private boolean passwordComplex;
    private String registerDate;
    private String birthMonth;
    private String mobileNumber;
    
	public String getId()
	{
		return id;
	}

	@JsonProperty("ID") 
	public void setId(String id)
	{
		this.id = id;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	@JsonProperty("EmailAddress") 
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getFirstName()
	{
		return firstName;
	}

	@JsonProperty("FirstName") 
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	@JsonProperty("LastName") 
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public Integer getBirthYear()
	{
		return birthYear;
	}

	@JsonProperty("BirthYear") 
	public void setBirthYear(Integer birthYear)
	{
		this.birthYear = birthYear;
	}

	public String getGender()
	{
		return gender;
	}

	@JsonProperty("Gender") 
	public void setGender(String gender)
	{
		this.gender = gender;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	@JsonProperty("ZipCode") 
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getPassword()
	{
		return password;
	}

	@JsonProperty("Password") 
	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getCountry()
	{
		return country;
	}

	@JsonProperty("Country") 
	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	@JsonProperty("CountryCode") 
	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public int getHouseholdIncome()
	{
		return householdIncome;
	}

	@JsonProperty("HouseholdIncome") 
	public void setHouseholdIncome(int householdIncome)
	{
		this.householdIncome = householdIncome;
	}

	public String getLanguage()
	{
		return language;
	}

	@JsonProperty("Language") 
	public void setLanguage(String language)
	{
		this.language = language;
	}

	public List<UserCenterIdDTO> getCenterIds()
	{
		return centerIds;
	}

	@JsonProperty("CenterIds") 
	public void setCenterIds(List<UserCenterIdDTO> centerIds)
	{
		this.centerIds = centerIds;
	}

	public List<UserBrandIdDTO> getBrandIds()
	{
		return brandIds;
	}

	@JsonProperty("BrandIds") 
	public void setBrandIds(List<UserBrandIdDTO> brandIds)
	{
		this.brandIds = brandIds;
	}
	
	public boolean isVerified()
	{
		return verified;
	}

	@JsonProperty("Verified") 
	public void setVerified(boolean verified)
	{
		this.verified = verified;
	}

	public String getPasswordResetLink()
	{
		return passwordResetLink;
	}

	@JsonProperty("PasswordResetLink") 
	public void setPasswordResetLink(String passwordResetLink)
	{
		this.passwordResetLink = passwordResetLink;
	}

	public boolean isOptedInEmail()
	{
		return optedInEmail;
	}

	@JsonProperty("opted_in_email") 
	public void setOptedInEmail(boolean optedInEmail)
	{
		this.optedInEmail = optedInEmail;
	}

	public String getStateCd()
	{
		return stateCd;
	}

	@JsonProperty("StateCd") 
	public void setStateCd(String stateCd)
	{
		this.stateCd = stateCd;
	}

	public boolean isProfileComplete()
	{
		return profileComplete;
	}

	@JsonProperty("ProfileComplete") 
	public void setProfileComplete(boolean profileComplete)
	{
		this.profileComplete = profileComplete;
	}

	public String getUsersPreferredCenter()
	{
		return usersPreferredCenter;
	}

	@JsonProperty("UsersPreferredCenter") 
	public void setUsersPreferredCenter(String usersPreferredCenter)
	{
		this.usersPreferredCenter = usersPreferredCenter;
	}

	public boolean isPasswordComplex()
	{
		return passwordComplex;
	}

	@JsonProperty("IsPasswordComplex") 
	public void setPasswordComplex(boolean isPasswordComplex)
	{
		this.passwordComplex = isPasswordComplex;
	}

	public String getRegisterDate()
	{
		return registerDate;
	}

	@JsonProperty("RegisterDate") 
	public void setRegisterDate(String registerDate)
	{
		this.registerDate = registerDate;
	}
	
	public String getBirthMonth()
	{
		return birthMonth;
	}

	@JsonProperty("BirthMonth") 
	public void setBirthMonth(String birthMonth)
	{
		this.birthMonth = birthMonth;
	}

	public String getMobileNumber()
	{
		return mobileNumber;
	}

	@JsonProperty("MobileNumber") 
	public void setMobileNumber(String mobileNumber)
	{
		this.mobileNumber = mobileNumber;
	}
}
