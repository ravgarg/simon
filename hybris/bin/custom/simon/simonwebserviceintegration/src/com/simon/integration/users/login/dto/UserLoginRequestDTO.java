package com.simon.integration.users.login.dto;

import com.simon.integration.dto.PojoAnnotation;

public class UserLoginRequestDTO extends PojoAnnotation
{
	private String emailaddress;
	private String password;
	
	public String getEmailaddress()
	{
		return emailaddress;
	}
	
	public void setEmailaddress(String emailaddress)
	{
		this.emailaddress = emailaddress;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}

}
