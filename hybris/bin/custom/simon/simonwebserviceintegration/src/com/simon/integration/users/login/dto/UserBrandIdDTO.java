package com.simon.integration.users.login.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.ResponseDTO;

public class UserBrandIdDTO extends ResponseDTO implements Serializable 
{
	private static final long serialVersionUID = 1L;
	private String id;
	private String brandName;
	
	public String getId()
	{
		return id;
	}
	
	@JsonProperty("ID") 
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getBrandName()
	{
		return brandName;
	}
	
	@JsonProperty("BrandName") 
	public void setBrandName(String brandName)
	{
		this.brandName = brandName;
	}
}
