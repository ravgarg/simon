
package com.simon.integration.purchase.services;

import com.simon.integration.dto.purchase.confirm.PurchaseConfirmRequestDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.OrderPurchaseIntegrationException;

/**
 * The Interface OrderPurchaseIntegrationEnhancedService.
 */
public interface OrderPurchaseIntegrationEnhancedService
{	
	/**
	 * @description Method use to call ESB layer to call purchase update
	 * @method purchaseConfirmRequest
	 * @param purchaseConfirmRequestDTO
	 * @return PurchaseConfirmResponseDTO
	 */
	PurchaseConfirmResponseDTO purchaseConfirmRequest(PurchaseConfirmRequestDTO purchaseConfirmRequestDTO) throws OrderPurchaseIntegrationException;
}