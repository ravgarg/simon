package com.simon.integration.dto.cart.createcart;

import java.io.Serializable;
import java.util.Map;

import com.simon.integration.dto.PojoAnnotation;

public class RetailerDTO extends PojoAnnotation implements Serializable  {

	private static long serialVersionUID = 1L;
	
	private String retailerID;
	private String retailerURL;
	private Map<String,String> productMap;
	
	public String getRetailerID() {
		return retailerID;
	}
	
	public void setRetailerID(String retailerID) {
		this.retailerID = retailerID;
	}
	public Map<String, String> getProductMap() {
		return productMap;
	}
	
	public void setProductMap(Map<String, String> productMap) {
		this.productMap = productMap;
	}
	
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	
	public static void setSerialVersionUID(long serialVersionUID) {
		RetailerDTO.serialVersionUID = serialVersionUID;
	}

	public String getRetailerURL() {
		return retailerURL;
	}

	public void setRetailerURL(String retailerURL) {
		this.retailerURL = retailerURL;
	}
}