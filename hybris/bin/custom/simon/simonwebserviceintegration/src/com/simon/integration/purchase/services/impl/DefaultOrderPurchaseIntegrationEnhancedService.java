package com.simon.integration.purchase.services.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmRequestDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.OrderPurchaseIntegrationException;
import com.simon.integration.purchase.services.OrderPurchaseIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

/**
 * The implementation class DefaultOrderPurchaseIntegrationEnhancedService use call the third party api services.
 */
@EnableRetry
@Configuration
public class DefaultOrderPurchaseIntegrationEnhancedService implements OrderPurchaseIntegrationEnhancedService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderPurchaseIntegrationEnhancedService.class);

    @Resource
    private RestWebService restWebService;

    @Resource
    private UserIntegrationUtils userIntegrationUtils;

    /**
     * @description Method use to call ESB layer to call purchase update
     * @method purchaseConfirmRequest
     * @param purchaseConfirmRequestDTO
     */
    @Override
    @Retryable(value = {RestClientException.class}, maxAttempts = SimonIntegrationConstants.PURCHASE_CONFIRM_ESB_RETRY_MAX_COUNT,backoff = @Backoff(delay = SimonIntegrationConstants.PURCHASE_CONFIRM_ESB_RETRY_DELAY))
    public PurchaseConfirmResponseDTO purchaseConfirmRequest(PurchaseConfirmRequestDTO purchaseConfirmRequestDTO) throws OrderPurchaseIntegrationException
    {
        PurchaseConfirmResponseDTO purchaseConfirmResponseDTO = null;
        try
        {
            final String serviceUrl = buildPurchaseConfirmServiceRequestApiUrl();

            purchaseConfirmResponseDTO = restWebService.executeRestEvent(serviceUrl, purchaseConfirmRequestDTO, null, null, PurchaseConfirmResponseDTO.class,
                RequestMethod.POST, getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());
        }
        catch (IntegrationException exception)
        {
            LOG.error("Exception occured while calling purchase update integration third party service : ", exception);
            throw new OrderPurchaseIntegrationException(exception.getMessage(), exception.getCause(), "");
        }
        return purchaseConfirmResponseDTO;
    }

	/**
	 * this is recover method for purchaseConfirmRequest to overcome from RestClientException
	 * @param clientExp
	 * @param purchaseConfirmRequestDTO
	 * @return
	 */
	@Recover
	public PurchaseConfirmResponseDTO recover(RestClientException clientExp,PurchaseConfirmRequestDTO purchaseConfirmRequestDTO) {
		LOG.error("Purchase confirm request, reached at max retry count for order id: ",purchaseConfirmRequestDTO.getOrderId());
		LOG.error("Purchase confirm request, RestClientException: ",clientExp);
		return null;
	}

    private String buildPurchaseConfirmServiceRequestApiUrl()
    {
        return userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.ORDER_PURCHAGE_CONFIRM_API_URL);
    }

    private MultiValueMap<String, Object> getHeaders()
    {
        final MultiValueMap<String, Object> headers = getHeadersMap();
        headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON_UTF_8);
        headers.add(SimonIntegrationConstants.CHAR_SET, SimonIntegrationConstants.UTF_8);
        return headers;
    }

    private MultiValueMap<String, Object> getHeadersMap()
    {
        return new LinkedMultiValueMap<>();
    }

}
