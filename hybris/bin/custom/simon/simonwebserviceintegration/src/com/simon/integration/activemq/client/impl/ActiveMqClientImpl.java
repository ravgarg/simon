
package com.simon.integration.activemq.client.impl;

import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.integration.activemq.client.ActiveMqClient;

/**
 * The Class ActiveMqClientImpl is client for active it pass same object to broker which it receive from service class
 */
public class ActiveMqClientImpl implements ActiveMqClient
{

    private static final Logger LOG = LoggerFactory.getLogger(ActiveMqClientImpl.class);

    @Resource
    private Map jmsTemplateRefrenceMap;

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final Object message, final String command) throws JsonProcessingException
    {
        final JmsTemplate jmsTemplate = (JmsTemplate) jmsTemplateRefrenceMap.get(command);
        final Object jsonString = createObject(message);
        jmsTemplate.convertAndSend(jsonString);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void send(final Object message, final String command, final Map<String, String> headers) throws JsonProcessingException
    {
        final JmsTemplate jmsTemplate = (JmsTemplate) jmsTemplateRefrenceMap.get(command);
        final Object jsonString = createObject(message);
        jmsTemplate.convertAndSend(jsonString, finalMessage ->
        {
            for (final Entry<String, String> header : headers.entrySet())
            {
                finalMessage.setStringProperty(header.getKey(), header.getValue());
            }
            return finalMessage;
        });
    }

    private Object createObject(final Object message) throws JsonProcessingException
    {
        final ObjectMapper jsonMapper = new ObjectMapper();
        final Object jsonString = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
        LOG.info("Data sent to ESB : {}", jsonString);
        return jsonString;
    }

}
