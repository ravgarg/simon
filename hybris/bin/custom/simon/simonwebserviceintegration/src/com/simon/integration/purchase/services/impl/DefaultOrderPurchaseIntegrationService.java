package com.simon.integration.purchase.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.integration.dto.purchase.confirm.PurchaseConfirmRequestDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.OrderPurchaseIntegrationException;
import com.simon.integration.purchase.services.OrderPurchaseIntegrationEnhancedService;
import com.simon.integration.purchase.services.OrderPurchaseIntegrationService;

/**
 * The implementation class DefaultOrderPurchaseIntegrationService use to convert the data in dto and call the enhanced service.
 */
public class DefaultOrderPurchaseIntegrationService implements OrderPurchaseIntegrationService {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderPurchaseIntegrationService.class);
	@Resource
	private OrderPurchaseIntegrationEnhancedService orderPurchaseIntegrationEnhancedService;
	
	/**
	 * @description Method use to call purchase update and convert the method param attributes in the DTO
	 * @method invokePurchaseConfirmRequest
	 * @param orderCode
	 */
	@Override
	public PurchaseConfirmResponseDTO invokePurchaseConfirmRequest(String orderCode) throws OrderPurchaseIntegrationException {
		final PurchaseConfirmRequestDTO purchaseConfirmRequestDTO = new PurchaseConfirmRequestDTO();
		purchaseConfirmRequestDTO.setOrderId(orderCode);
		final PurchaseConfirmResponseDTO purchaseConfirmResponseDTO = orderPurchaseIntegrationEnhancedService.purchaseConfirmRequest(purchaseConfirmRequestDTO);
		if(null!=purchaseConfirmResponseDTO && StringUtils.isNotEmpty(purchaseConfirmResponseDTO.getErrorCode())){
			LOG.error("Error occured when hitting the order purchase service with error code {} and error message {}", purchaseConfirmResponseDTO.getErrorCode(), purchaseConfirmResponseDTO.getErrorMessage());
		}
		return purchaseConfirmResponseDTO;
	}

}
