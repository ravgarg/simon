
package com.simon.integration.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.exceptions.IntegrationException;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.generated.model.ErrorLogModel;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.services.CartCheckOutIntegrationEnhancedService;
import com.simon.integration.services.CartCheckOutIntegrationService;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelCreationException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;

/**
 * The Class acts like a middle man between the commerce and the integration layer
 */
public class DefaultCartCheckOutIntegrationService implements CartCheckOutIntegrationService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultCartCheckOutIntegrationService.class);

    @Resource
    private Converter<CartModel, CartEstimateRequestDTO> cartEstimateRequestDataConverter;

    @Resource
    private Converter<Pair<CartModel, List<VariantAttributeMappingModel>>, CartEstimateRequestDTO> cartEstimateRequestVariantAttributeConverter;

	/** The integration enhanced service. */
	@Resource
	private CartCheckOutIntegrationEnhancedService cartCheckOutIntegrationEnhancedService;
	
	@Resource
	private Converter<CartModel, CreateCartDTO> createCartDataConverter;
	
	@Resource
	private ConfigurationService configurationService;
	
	@Resource
	private Converter<Pair<CartModel, Exception>, ErrorLogModel> errorLogConverter;
	
	@Resource
	private ModelService modelService;

	@Resource
	private TimeService timeService;

	
	/**
	 * Parsing runtime exception into business exception so caller of this
	 * method explicitly catch integration exception and handle these exception
	 * as per there business scenario.
	 * 
	 * @see com.simon.integration.services.CartCheckOutIntegrationService#
	 * invokeCartEstimateRequest(de.hybris.platform.core.model.order.CartModel)
	 */
	@Override
	public CartEstimateResponseDTO invokeCartEstimateRequest(final CartModel cartModel, List<VariantAttributeMappingModel> variantAttributeMappingList)
			throws CartCheckOutIntegrationException {
		 CartEstimateRequestDTO cartEstimateRequestDTO = cartEstimateRequestDataConverter.convert(cartModel);
		cartEstimateRequestDTO=cartEstimateRequestVariantAttributeConverter.convert(ImmutablePair.of(cartModel, variantAttributeMappingList),cartEstimateRequestDTO);
		try {
			return cartCheckOutIntegrationEnhancedService.estimateCart(cartEstimateRequestDTO);
		} catch (IntegrationException exception) {
			LOG.error("Exception : when calling Estimate Cart Service",exception);
			try{
			final ErrorLogModel errorLogModel=modelService.create(ErrorLogModel.class);
			errorLogConverter.convert(ImmutablePair.of(cartModel, exception), errorLogModel);
			modelService.save(errorLogModel);
			}catch(ModelSavingException | ModelCreationException e){
				LOG.error("Exception occurs while creating or saving Error Log", e);	
			}
			throw new CartCheckOutIntegrationException(exception.getMessage(), exception);
		}
	}
    /**
     * {@inheritDoc}
     */
    @Override
    public void invokeCreateCartRequest(CartModel cartModel) throws CartCheckOutIntegrationException
    {
        try
        {
        	cartModel.setCreateCartInvocationTime(String.valueOf(timeService.getCurrentTime().getTime()));
            final CreateCartDTO createCartDTO = createCartDataConverter.convert(cartModel);
            cartCheckOutIntegrationEnhancedService.createCart(createCartDTO);
            modelService.save(cartModel);
        }
        catch (IntegrationException integrationException)
        {
            throw new CartCheckOutIntegrationException(integrationException.getMessage(), integrationException);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CreateCartConfirmationDTO invokeCartConfirmationMockRequest() throws IOException
    {

        final String stubFileName = configurationService.getConfiguration().getString(SimonIntegrationConstants.CREATE_CART_STUB);
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            final ClassLoader classLoader = getClass().getClassLoader();
            return mapper.readValue(new File(classLoader.getResource(stubFileName).getFile()), CreateCartConfirmationDTO.class);
        }
        catch (final IOException exception)
        {
            LOG.error("Error during in Parsing of json to mock third party service", exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OrderConfirmCallBackDTO invokeOrderUpdateConfirmMockRequest(String stubFileName) throws IOException
    {
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            final ClassLoader classLoader = getClass().getClassLoader();
            return mapper.readValue(new File(classLoader.getResource(stubFileName).getFile()), OrderConfirmCallBackDTO.class);
        }
        catch (final IOException exception)
        {
            LOG.error("Error occured when parsing json to mock third party service ", exception);
            throw exception;
        }
    }

}
