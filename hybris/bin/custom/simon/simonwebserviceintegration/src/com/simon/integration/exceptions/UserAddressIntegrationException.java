package com.simon.integration.exceptions;

public class UserAddressIntegrationException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99002L;

	/** Exception code. */
	private final String code;


	/**
	 * Instantiates a new UserIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 */
	public UserAddressIntegrationException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new UserAddressIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public UserAddressIntegrationException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}


	/**
	 * Instantiates a new UserAddressIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public UserAddressIntegrationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public UserAddressIntegrationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public UserAddressIntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}


}
