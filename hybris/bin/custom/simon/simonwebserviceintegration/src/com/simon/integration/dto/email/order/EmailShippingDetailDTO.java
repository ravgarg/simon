package com.simon.integration.dto.email.order;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "shipping")
public class EmailShippingDetailDTO {
	
	@XmlAttribute(name = "fname")
	private String fname;
	
	@XmlAttribute(name = "lname")
	private String lname;
	
	@XmlAttribute(name = "address1")
	private String address1;
	
	@XmlAttribute(name = "address2")
	private String address2;
	
	@XmlAttribute(name = "city")
	private String city;
	
	@XmlAttribute(name = "state")
	private String state;
	
	@XmlAttribute(name = "zip")
	private String zip;

	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	

}
