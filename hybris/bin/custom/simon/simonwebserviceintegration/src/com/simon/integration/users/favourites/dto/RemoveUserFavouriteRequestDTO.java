package com.simon.integration.users.favourites.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.PojoAnnotation;

public class RemoveUserFavouriteRequestDTO extends PojoAnnotation 
{
	private String externalID;
	private String favType;
	
	public String getExternalID()
	{
		return externalID;
	}
	
	@JsonProperty("externalID") 
	public void setExternalID(String externalID)
	{
		this.externalID = externalID;
	}
	
	public String getFavType()
	{
		return favType;
	}
	
	@JsonProperty("favType") 
	public void setFavType(String favType)
	{
		this.favType = favType;
	}
}
