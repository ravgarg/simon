
package com.simon.integration.cart.populators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartProductsDTO;
import com.simon.integration.dto.cart.cartestimate.RetailerRequestDTO;
import com.simon.integration.dto.cart.cartestimate.ShippingDetailsDTO;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

/**
 * Populator to create Cart Estimate Request Object from Cart Model
 */
public class CartEstimateRequestDataPopulator implements Populator<CartModel, CartEstimateRequestDTO>
{
    @Override
    public void populate(final CartModel source, final CartEstimateRequestDTO target)
    {
        target.setCartID(source.getCode());
        target.setRetailers(getRetailers(source));
    }

    private List<RetailerRequestDTO> getRetailers(CartModel source)
    {

        final List<RetailerRequestDTO> retailersList = new ArrayList<>();

        final Map<String, List<CartProductsDTO>> retailers = getRetailerWithProduct(source);
        final AddressModel deliveryAddress = source.getDeliveryAddress();
        if (MapUtils.isNotEmpty(retailers))
        {
            for (final Entry<String, List<CartProductsDTO>> retailer : retailers.entrySet())
            {
                final RetailerRequestDTO retailerRequestDTO = new RetailerRequestDTO();
                retailerRequestDTO.setRetailerID(retailer.getKey());
                retailerRequestDTO.setCartProducts(retailer.getValue());

                final String shippingCode = getShippingOptions(retailer, source);
                retailerRequestDTO.setShippingOptions(shippingCode);
                final ShippingDetailsDTO shippingDetails = new ShippingDetailsDTO();
                shippingDetails.setShippingCity(deliveryAddress.getTown());
                shippingDetails.setShippingState(deliveryAddress.getRegion().getName());
                shippingDetails.setShippingZip(deliveryAddress.getPostalcode());
                retailerRequestDTO.setShippingDetails(shippingDetails);
                retailersList.add(retailerRequestDTO);
            }
        }
        return retailersList;
    }

    private String getShippingOptions(Entry<String, List<CartProductsDTO>> retailer, CartModel source)
    {
        String shippingCode = StringUtils.EMPTY;
        for (final RetailersInfoModel retailersInfoModel : source.getAdditionalCartInfo().getRetailersInfo())
        {
            if (retailersInfoModel.getRetailerId().equals(retailer.getKey()))
            {
                for (final ShippingDetailsModel shippingDetailsModel : retailersInfoModel.getShippingDetails())
                {
                    if (shippingDetailsModel.isSelected())
                    {
                        shippingCode = shippingDetailsModel.getCode();
                        break;
                    }
                }
            }
        }
        return shippingCode;
    }

    private Map<String, List<CartProductsDTO>> getRetailerWithProduct(CartModel source)
    {

        String retailerId = "";
        List<CartProductsDTO> products;
        final Map<String, List<CartProductsDTO>> retailer = new HashMap<>();
        for (final AbstractOrderEntryModel entry : source.getEntries())
        {
            retailerId = entry.getProduct().getShop().getId();
            if (!retailer.containsKey(retailerId))
            {
                products = new ArrayList<>();
                retailer.put(retailerId, products);
            }
            else
            {
                products = retailer.get(retailerId);
            }

            final CartProductsDTO cartProductsDTO = new CartProductsDTO();
            final ProductModel productModel = entry.getProduct();
            cartProductsDTO.setProductID(productModel.getCode());
            final Map<String, String> productAttributes = getProductAttributes(productModel);
            cartProductsDTO.setVariants(productAttributes);
            cartProductsDTO.setQuantity(entry.getQuantity().toString());
            products.add(cartProductsDTO);

            retailer.put(retailerId, products);
        }
        return retailer;
    }

    private Map<String, String> getProductAttributes(ProductModel productModel)
    {
        final Map<String, String> productAttributes = new HashMap<>();
        if (productModel instanceof GenericVariantProductModel)
        {
            final GenericVariantProductModel genericVariantProductModel = (GenericVariantProductModel) productModel;
            for (final CategoryModel category : genericVariantProductModel.getSupercategories())
            {
                productAttributes.put(category.getSupercategories().get(0).getCode(), category.getName());

            }
        }

        return productAttributes;

    }

}
