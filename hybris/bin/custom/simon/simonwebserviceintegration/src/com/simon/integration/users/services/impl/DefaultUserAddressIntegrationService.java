package com.simon.integration.users.services.impl;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;
import com.simon.integration.users.dto.UserPaymentAddressDTO;
import com.simon.integration.users.services.UserAddressIntegrationEnhancedService;
import com.simon.integration.users.services.UserAddressIntegrationService;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Implementation class of UserAddressIntegrationService.
 *
 */
public class DefaultUserAddressIntegrationService implements UserAddressIntegrationService {

	private static final String SUCCESS_RESPONSE_FROM_MOCKED_ESB = "Success Response from Mocked ESB";

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserAddressIntegrationService.class);

	@Resource
	private Converter<ExtAddressData, UserAddressDTO> userAddressConverter;

	@Resource
	private Converter<CCPaymentInfoData, UserPaymentAddressDTO> userPaymentAddressConverter;

	@Resource
	private UserAddressIntegrationEnhancedService userAddressIntegrationEnhancedService;
	@Resource
	private ConfigurationService configurationService;

	/**
	 * This method is used to add a user address with PO.com with user address
	 * details mapped in {@link ExtAddressData} and then parse response in
	 * {@link UserAddressResponseDTO}.
	 * 
	 * @param extAddressData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public UserAddressResponseDTO addUserAddress(final ExtAddressData extAddressData)
			throws UserAddressIntegrationException {

		LOGGER.debug("DefaultUserAddressIntegrationService ::: addUserAddress :: extAddressData : {} ", extAddressData);
		if(isEsbPoDisabled()){
			final UserAddressResponseDTO mockResponse=new UserAddressResponseDTO();
			mockResponse.setMessage(SUCCESS_RESPONSE_FROM_MOCKED_ESB);
			return mockResponse;
		}
		if (extAddressData == null) {
			LOGGER.error("ExtAddressData is as coming null for addUserAddress in DefaultUserAddressIntegrationService ");
			return null;
		}
		
		final UserAddressDTO userAddressDTO = userAddressConverter.convert(extAddressData);
		return userAddressIntegrationEnhancedService.addUserAddress(userAddressDTO);
	}

	/**
	 * This method is used to update a user address with PO.com with user
	 * address details mapped in {@link ExtAddressData} and then parse response
	 * in {@link UserAddressResponseDTO}.
	 * 
	 * @param extAddressData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */

	@Override
	public UserAddressResponseDTO updateUserAddress(ExtAddressData extAddressData)
			throws UserAddressIntegrationException {

		LOGGER.debug("DefaultUserAddressIntegrationService ::: updateUserAddress :: extAddressData : {} ",
				extAddressData);
		if(isEsbPoDisabled()){
			final UserAddressResponseDTO mockResponse=new UserAddressResponseDTO();
			mockResponse.setMessage(SUCCESS_RESPONSE_FROM_MOCKED_ESB);
			return mockResponse;
		}
		if (extAddressData == null) {
			LOGGER.error("ExtAddressData is as coming null for updateUserAddress in DefaultUserAddressIntegrationService ");
			return null;
		}
		
		final UserAddressDTO userAddressDTO = userAddressConverter.convert(extAddressData);
		return userAddressIntegrationEnhancedService.updateUserAddress(userAddressDTO);
	}

	/**
	 * This method is used to remove a user address with PO.com and then parse
	 * response in {@link UserAddressResponseDTO}.
	 * 
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public UserAddressResponseDTO removeUserAddress(ExtAddressData extAddressData) throws UserAddressIntegrationException {

		LOGGER.debug("DefaultUserAddressIntegrationService ::: removeUserAddress :: extAddressData : {} ", extAddressData);
		if(isEsbPoDisabled()){
			final UserAddressResponseDTO mockResponse=new UserAddressResponseDTO();
			mockResponse.setMessage(SUCCESS_RESPONSE_FROM_MOCKED_ESB);
			return mockResponse;
		}
		if (extAddressData == null) {
			LOGGER.error("ExtAddressData is coming null for removeUserAddress in DefaultUserAddressIntegrationService");
			return null;
		}

		final UserAddressDTO userAddressDTO = userAddressConverter.convert(extAddressData);
		return userAddressIntegrationEnhancedService.removeUserAddress(userAddressDTO);
	}

	/**
	 * This method is used to add a user Payment address with PO.com with user
	 * Payment address details mapped in {@link CCPaymentInfoData} and then
	 * parse response in {@link UserAddressResponseDTO}.
	 * 
	 * @param ccPaymentInfoData
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	
	@Override
	public UserAddressResponseDTO addUserPaymentAddress(CCPaymentInfoData ccPaymentInfoData)
			throws UserAddressIntegrationException {

		LOGGER.debug("DefaultUserAddressIntegrationService ::: addUserPaymentAddress :: extAddressData : {} ",
				ccPaymentInfoData);
		if(isEsbPoDisabled()){
			final UserAddressResponseDTO mockResponse=new UserAddressResponseDTO();
			mockResponse.setMessage(SUCCESS_RESPONSE_FROM_MOCKED_ESB);
			return mockResponse;
		}
		if (ccPaymentInfoData == null) {
			LOGGER.error("CCPaymentInfoData is as coming null for addUserPaymentAddress in DefaultUserAddressIntegrationService ");
			return null;
}
		
		final UserPaymentAddressDTO userPaymentAddressDTO = userPaymentAddressConverter.convert(ccPaymentInfoData);

		return userAddressIntegrationEnhancedService.addUserPaymentAddress(userPaymentAddressDTO);
	}
	
	@Override
	public UserGetAddressResponseDTO getUserHomeAddress()
			throws UserAddressIntegrationException {
		
		return userAddressIntegrationEnhancedService.getUserAddress(SimonIntegrationConstants.ADDRESS_TYPE_HOME);
	}
	
	
	@Override
	public UserGetAddressResponseDTO getUserShippingAddress()
			throws UserAddressIntegrationException {
		
		return userAddressIntegrationEnhancedService.getUserAddress(SimonIntegrationConstants.ADDRESS_TYPE_SHIPPING);
	}
	
	
	@Override
	public UserGetAddressResponseDTO getUserBillingAddress()
			throws UserAddressIntegrationException {
		
		return userAddressIntegrationEnhancedService.getUserAddress(SimonIntegrationConstants.ADDRESS_TYPE_BILLING);
	}
	
	
	/**
	 * @return
	 */
	private boolean isEsbPoDisabled()
	{
		return configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false);
	}
}
