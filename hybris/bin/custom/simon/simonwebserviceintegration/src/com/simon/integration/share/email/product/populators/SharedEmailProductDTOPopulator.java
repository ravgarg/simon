package com.simon.integration.share.email.product.populators;

import java.io.StringWriter;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.product.EmailProductDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.product.EmailProductRetailerDTO;
import com.simon.integration.dto.email.product.EmailProductRootDTO;
import com.simon.integration.dto.email.product.ShareEmailAttributesDTO;
import com.simon.integration.dto.email.product.ShareMessageDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Converts ShareEmailData to ShareEmailRequestDTO
 */
public class SharedEmailProductDTOPopulator implements Populator<ShareEmailData, ShareEmailRequestDTO> {

	private static final Logger LOG = LoggerFactory.getLogger(SharedEmailProductDTOPopulator.class);

	private static final String SHARE_PROUDCT_EMAIL_CUSTOMER_KEY = "share.product.email.customer.key";
	private static final String SHARE_PROUDCT_EMAIL_SUBJECT_LINE = "share.product.email.subject.line";

	@Resource(name = "sharedEmailProductDetailsDTOConverter")
	private Converter<EmailProductDetailsData, EmailProductRetailerDTO> sharedEmailProductDetailsDTOConverter;
	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(ShareEmailData source, ShareEmailRequestDTO target) {

		target.setCustomerKey(shareEmailIntegrationUtils.getConfiguredStaticString(SHARE_PROUDCT_EMAIL_CUSTOMER_KEY));
		target.setSubscriberDetails(shareEmailIntegrationUtils.populateShareSubscriberDetails(source));
		target.setEmailAttributes(populateShareEmailAttributes(source));
	}

	/**
	 * @method populateShareEmailAttributes
	 * @param source
	 * @return ShareEmailAttributesDTO
	 */
	private ShareEmailAttributesDTO populateShareEmailAttributes(ShareEmailData source) {
		final ShareEmailAttributesDTO shareEmailAttributesDTO = new ShareEmailAttributesDTO();
		shareEmailAttributesDTO
				.setSubjectLine(shareEmailIntegrationUtils.getConfiguredStaticString(SHARE_PROUDCT_EMAIL_SUBJECT_LINE));
		shareEmailAttributesDTO.setXmlType(shareEmailIntegrationUtils
				.getConfiguredStaticString(SimonIntegrationConstants.SHARE_PROUDCT_OFFER_EMAIL_XML_TYPE));

		final EmailProductRootDTO emailProductDTO = populateEmailProductDTO(source);
		shareEmailAttributesDTO.setXmlData(populateProductEmailDataInXml(emailProductDTO));
		return shareEmailAttributesDTO;
	}

	/**
	 * @method populateEmailProductDTO
	 * @param emailProductData
	 * @return EmailProductDTO
	 */
	private EmailProductRootDTO populateEmailProductDTO(ShareEmailData source) {
		final EmailProductRootDTO emailProductDTO = new EmailProductRootDTO();
		final ShareMessageDTO message = new ShareMessageDTO();
		message.setText(source.getMessage());
		emailProductDTO.setMessage(message);
		if (source.getEmailProductData() != null) {
			emailProductDTO.setProductDetails(
					sharedEmailProductDetailsDTOConverter.convert(source.getEmailProductData().getProductDetails()));
		}

		return emailProductDTO;
	}

	/**
	 * @description Method use to convert the DTO to String
	 * @method populateDataInXml
	 * @param productData
	 * @return String
	 */
	private String populateProductEmailDataInXml(EmailProductRootDTO productData) {
		String escapedResult = StringUtils.EMPTY;
		final StringWriter stringWriter = new StringWriter();
		try {
			final JAXBContext context = JAXBContext.newInstance(EmailProductRootDTO.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(productData, stringWriter);
			escapedResult = shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter);
			LOG.info("Shared Product email data :  " + escapedResult);
		} catch (JAXBException exception) {
			LOG.error("Exception occured while convert DTO object in the xml and return as String {} ", exception);
		}
		return escapedResult;
	}

}
