package com.simon.integration.users.favourites.services.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.users.favourites.dto.AddUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.RemoveUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteResponseDTO;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.session.SessionService;
/**
 * The class DefaultUserFavouritesIntegrationEnhancedService is used to integrate favorites data with PO.com
 *
 */
public class DefaultUserFavouritesIntegrationEnhancedService implements UserFavouritesIntegrationEnhancedService
{
	private static final String FAV_TYPE_PARAM = "?favType=";
	private static final String CONNECTION_FAILED_MESSAGE = "Get Favourites:- Exception occured while connecting to third party: {}";
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserFavouritesIntegrationEnhancedService.class);
	@Resource
	private RestWebService restWebService;
	@Resource
	private EnqueueService enqueueService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	/**
	 * This method is used to get a favorite added of particular type with PO.com. 
	 * 
	 * @param request
	 * @return true if favorite added successfully, else false.
	 */
	@Override
	public boolean addUserFavourite(AddUserFavouriteRequestDTO request){
		try{
			final Map<String,String> jmsHeaders= populateJMSHeaders();
			enqueueService.sendObjectMessage(request, JmsTemplateNameEnum.ADD_FAV,jmsHeaders);
		 	return true;
		}
		catch(IntegrationException exception){	
			LOGGER.error("Exception Occured while adding user Favourite through JMS: {}", exception);
			throw new UserFavouritesIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * This method is used to get a favorite removed of particular type with PO.com depending on the URL provided. 
	 * 
	 * @param request
	 * @return true if favorite removed successfully, else false.
	 */
	@Override
	public boolean removeFavourite(RemoveUserFavouriteRequestDTO request)
	{
		try{
			final Map<String,String> jmsHeaders= populateJMSHeaders();
			enqueueService.sendObjectMessage(request, JmsTemplateNameEnum.REMOVE_FAV,jmsHeaders);
		 	return true;
		}
		catch(IntegrationException exception){	
			LOGGER.error("Exception Occured while deleting user Favourite through JMS: {}", exception);
			throw new UserFavouritesIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * This method is used to get all favorites from PO.com depending upon URL. 
	 * 
	 * @param favouriteType
	 * @return {@link UserFavouriteResponseDTO}.
	 */
	@Override
	public UserFavouriteResponseDTO getFavourites(String favouriteType)
	{
		try{
			 final UserFavouriteResponseDTO response = restWebService.executeRestEvent(generateGetFavouritesByTypeUrl(favouriteType),null, Boolean.FALSE.toString(), null,
					UserFavouriteResponseDTO.class, RequestMethod.GET, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserFavouritesIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;
		}
		catch(IntegrationException exception){	
			LOGGER.error(CONNECTION_FAILED_MESSAGE, exception);
			throw new UserFavouritesIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * Method used to get the URL for the Get User Favorites By Type API.
	 * 
	 * @return String
	 */
	private String generateGetFavouritesByTypeUrl(String favouriteType){
		final StringBuilder serviceUrl = new StringBuilder(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_FAVOURITES_GET_URL));
		serviceUrl.append(FAV_TYPE_PARAM);
		serviceUrl.append(favouriteType);
		return serviceUrl.toString();
	}
	
	private Map<String, String> populateJMSHeaders()
	{
		final Map<String, String> jmsHeaders=new HashMap<>();
		jmsHeaders.put(SimonIntegrationConstants.PO_TOKEN, sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN));
		return jmsHeaders;
	}
}
