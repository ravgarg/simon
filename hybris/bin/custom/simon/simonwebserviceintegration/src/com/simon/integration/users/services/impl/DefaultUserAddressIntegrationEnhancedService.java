package com.simon.integration.users.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserGetAddressResponseDTO;
import com.simon.integration.users.dto.UserPaymentAddressDTO;
import com.simon.integration.users.services.UserAddressIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.servicelayer.session.SessionService;

/**
 * Implementation class of UserAddressIntegrationEnhancedService
 *
 */
public class DefaultUserAddressIntegrationEnhancedService implements UserAddressIntegrationEnhancedService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserAddressIntegrationEnhancedService.class);
	@Resource
	private RestWebService restWebService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	/**
	 * This method is used to add a user address with PO.com with user address
	 * details mapped in {@link UserAddressDTO} and then parse response in
	 * {@link UserAddressResponseDTO}.
	 * 
	 * @param userAddressDTO
	 * @return {@link UserAddressResponseDTO}
	 * 
	 */
	@Override
	public UserAddressResponseDTO addUserAddress(UserAddressDTO userAddressDTO) throws UserAddressIntegrationException {

		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_ADDRESS_END_POINT);
		UserAddressResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(serviceUrl, userAddressDTO, Boolean.FALSE.toString(), null,
					UserAddressResponseDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				userIntegrationUtils.handleCommonErrors(response);
				LOGGER.error("Error occured when calling ESB to add user address and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				throw new UserAddressIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while adding a user address to third party: ", exception);
			throw new UserAddressIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}

	

	/**
	 * This method is used to update a user address with PO.com with user
	 * address details mapped in {@link UserAddressDTO} and then parse response
	 * in {@link UserAddressResponseDTO}.
	 * 
	 * @param userAddressDTO
	 * @return {@link UserAddressResponseDTO}
	 * 
	 */
	@Override
	public UserAddressResponseDTO updateUserAddress(UserAddressDTO userAddressDTO)
			throws UserAddressIntegrationException {

		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_UPDATE_USER_ADDRESS_END_POINT);

		UserAddressResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(serviceUrl, userAddressDTO, Boolean.FALSE.toString(), null,
					UserAddressResponseDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to update user address and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserAddressIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while updating a user address to third party: ", exception);
			throw new UserAddressIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}

	/**
	 * This method is used to remove a user address with PO.com and then parse
	 * response in {@link UserAddressResponseDTO}.
	 * 
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */
	@Override
	public UserAddressResponseDTO removeUserAddress(UserAddressDTO userAddressDTO) throws UserAddressIntegrationException {

		LOGGER.debug("DefaultUserAddressIntegrationEnhancedService ::: removeUserAddress :: externalId : {} ",
				userAddressDTO.getExternalId());
		UserAddressResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_REMOVE_USER_ADDRESS_END_POINT), userAddressDTO,
					Boolean.FALSE.toString(), null, UserAddressResponseDTO.class, RequestMethod.DELETE,
					userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to remove user address and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserAddressIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while removing a user address to third party: ", exception);
			throw new UserAddressIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}



	/**
	 * This method is used to add a user Payment address with PO.com with user
	 * Payment address details mapped in {@link CCPaymentInfoData} and then
	 * parse response in {@link UserAddressResponseDTO}.
	 *
	 * @return {@link UserAddressResponseDTO}
	 * @throws UserAddressIntegrationException
	 */

	@Override
	public UserAddressResponseDTO addUserPaymentAddress(UserPaymentAddressDTO userPaymentAddressDTO)
			throws UserAddressIntegrationException {

		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_PAYMENT_ADDRESS_END_POINT);

		UserAddressResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(serviceUrl, userPaymentAddressDTO, Boolean.FALSE.toString(),
					null, UserAddressResponseDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to add user payment address and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserAddressIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while adding a user payment address to third party: ", exception);
			throw new UserAddressIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}



	@Override
	public UserGetAddressResponseDTO getUserAddress(String addressType) throws UserAddressIntegrationException 
	{
		final StringBuilder serviceUrl = new StringBuilder(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_GET_USER_ADDRESS_END_POINT));
		serviceUrl.append(addressType);

		UserGetAddressResponseDTO response = null;
		try {

			response = restWebService.executeRestEvent(serviceUrl.toString(), null, Boolean.FALSE.toString(),
					null, UserGetAddressResponseDTO.class, RequestMethod.GET, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to get user payment address and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserAddressIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		} catch (IntegrationException exception) {
			LOGGER.error("Exception occured while getting a user payment address from third party: ", exception);
			throw new UserAddressIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
}
