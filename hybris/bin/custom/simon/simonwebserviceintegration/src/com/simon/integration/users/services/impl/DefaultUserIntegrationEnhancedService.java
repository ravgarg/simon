
package com.simon.integration.users.services.impl;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserCenterDTO;
import com.simon.integration.users.dto.UserCenterRequestDTO;
import com.simon.integration.users.dto.UserCentersResponseDTO;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationService;
import com.simon.integration.users.services.UserIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.session.SessionService;

/**
 * Implementation class DefaultUserIntegrationEnhancedService 
 */
public class DefaultUserIntegrationEnhancedService implements UserIntegrationEnhancedService {
	
	private static final String DEFAULT_MALL_NAME = "Test Outlet";
	private static final String DEFAULT_MALL_ID = "888";
	private static final String OUTLET_CENTER = "OutletCenter";
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultUserIntegrationEnhancedService.class);
	@Resource
	private RestWebService restWebService;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource
	private AuthLoginIntegrationService authLoginIntegrationService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	
	/**
	 * @description Method use to get the registered user details and validate the token
	 * @method updateUserDetails
	 * @param userDetailsDTO - UserData
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException 
	 */
	@Override
	public VIPUserDTO updateUserDetails(UserRegisterUpdateRequestDTO userDetailsDTO) throws UserIntegrationException  {
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_UPDATE_USER_END_POINT);
		try{
			final VIPUserDTO response=restWebService.executeRestEvent(serviceUrl, userDetailsDTO, Boolean.FALSE.toString(), null,
					VIPUserDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to update user profile and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
		return response;	
	}catch(IntegrationException exception){
			LOGGER.error("Exception occured while calling user integration third party service to update user profile details: ", exception);
			throw new UserIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	

	/**
	 * This method is used to register a user with PO.com with user details mapped in {@link UserRegisterUpdateRequestDTO} and then parse response in
	 * {@link VIPUserDTO}.
	 * @param userDetailsDTO
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException 
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public VIPUserDTO registerUser(UserRegisterUpdateRequestDTO userDetailsDTO) throws UserIntegrationException, AuthLoginIntegrationException
	{	
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL);
		try{
			final VIPUserDTO response=restWebService.executeRestEvent(serviceUrl, userDetailsDTO, Boolean.FALSE.toString(), null,
					VIPUserDTO.class, RequestMethod.POST, populateHeaderForRegisterUser(),  userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to register user and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new UserIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		}catch(IntegrationException exception){
			LOGGER.error("Exception occured while registering a user to third party: ", exception);
			throw new UserIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * This method is used get centers associated with a zip-code from PO.com.
	 * 
	 * @param userCenterRequestDTO
	 * @return {@link UserCenterIdDTO}
	 * @throws UserIntegrationException 
	 */
	@Override
	public UserCenterIdDTO getDefaultPrimaryCenter(UserCenterRequestDTO userCenterRequestDTO) throws UserIntegrationException
	{
		final UserCenterIdDTO primaryCenter = new UserCenterIdDTO();
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_CENTER_BY_ZIPCODE_END_POINT_URL);
		try{
			final UserCentersResponseDTO userCentersResponseDTO = restWebService.executeRestEvent(serviceUrl, userCenterRequestDTO, Boolean.FALSE.toString(), null,
					UserCentersResponseDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(), userIntegrationUtils.populateAuthHeaderForESB());		
			if(StringUtils.isNotEmpty(userCentersResponseDTO.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to get the default user center and getting error with code {} and message {}.", userCentersResponseDTO.getErrorCode(), userCentersResponseDTO.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(userCentersResponseDTO);
				throw new UserIntegrationException(userCentersResponseDTO.getErrorMessage(),userCentersResponseDTO.getErrorCode());
			}else{
					final Optional<UserCenterDTO> userCenterDTO=userCentersResponseDTO.getCenters().stream().filter(userCenter-> OUTLET_CENTER.equalsIgnoreCase(userCenter.getPropertyType())).findFirst();
					if(userCenterDTO.isPresent()){
						
						final Integer mallId = userCenterDTO.get().getMallId();
						final String mallName = userCenterDTO.get().getMallName();
						primaryCenter.setId(StringUtils.isNotEmpty(mallId.toString()) ? mallId.toString():DEFAULT_MALL_ID );
						primaryCenter.setOutletName(StringUtils.isNotEmpty(mallName)? mallName : DEFAULT_MALL_NAME );
					}else{
						LOGGER.info("Center not found by given zipcode {}, hence setting default mall / center.", userCenterRequestDTO.getZipCode());
						primaryCenter.setId(DEFAULT_MALL_ID);
						primaryCenter.setOutletName(DEFAULT_MALL_NAME);	
					}
			}
			return primaryCenter;
		}catch(IntegrationException exception){
			LOGGER.error("Exception occured while fetching my center: ", exception);
			throw new UserIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
		
	}

	/**
	 * This method creates a multi-value map and adds header data in it.
	 * @return {@link MultiValueMap}
	 * @throws AuthLoginIntegrationException 
	 * @throws UserIntegrationException 
	 */
	private MultiValueMap<String, Object> populateHeaderForRegisterUser() throws AuthLoginIntegrationException, UserIntegrationException {
		if(authLoginIntegrationService.generateAndSaveOAuthTokenForRegistration()){
			final MultiValueMap<String, Object> headers=userIntegrationUtils.populateHeadersForESB();
			sessionService.removeAttribute(SimonIntegrationConstants.AUTH_TOKEN);
		return headers;
		}
		else{
			LOGGER.error("Exception occured while getting Auth token registering a user to third party");
			throw new UserIntegrationException("Exception occured while getting Auth token registering a user to third party",SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
		
}