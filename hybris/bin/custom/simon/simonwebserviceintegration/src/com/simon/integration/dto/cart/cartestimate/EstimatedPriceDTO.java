
package com.simon.integration.dto.cart.cartestimate;

import com.simon.integration.dto.PojoAnnotation;

/**
 * 
 * @author ssi249
 *
 */
public class EstimatedPriceDTO extends PojoAnnotation{


	/**
	 * <i>Generated property</i> for
	 * <code>TotalEstimatedPriceDTO.totalShippingPrice</code> property defined
	 * at extension <code>simonwebserviceintegration</code>.
	 */

	private String totalShippingPrice;

	/**
	 * <i>Generated property</i> for
	 * <code>TotalEstimatedPriceDTO.subtotal</code> property defined at
	 * extension <code>simonwebserviceintegration</code>.
	 */

	private String subtotal;

	/**
	 * <i>Generated property</i> for
	 * <code>TotalEstimatedPriceDTO.totalSalesTax</code> property defined at
	 * extension <code>simonwebserviceintegration</code>.
	 */

	private String totalSalesTax;

	/**
	 * <i>Generated property</i> for <code>TotalEstimatedPriceDTO.total</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String finalPrice;

	public void setTotalShippingPrice(final String totalShippingPrice) {
		this.totalShippingPrice = totalShippingPrice;
	}

	public String getTotalShippingPrice() {
		return totalShippingPrice;
	}

	public void setSubtotal(final String subtotal) {
		this.subtotal = subtotal;
	}

	public String getSubtotal() {
		return subtotal;
	}

	public void setTotalSalesTax(final String totalSalesTax) {
		this.totalSalesTax = totalSalesTax;
	}

	public String getTotalSalesTax() {
		return totalSalesTax;
	}

	public void setFinalPrice(final String finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getFinalPrice() {
		return finalPrice;
	}

}
