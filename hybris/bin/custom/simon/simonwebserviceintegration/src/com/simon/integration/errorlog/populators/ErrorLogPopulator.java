package com.simon.integration.errorlog.populators;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;

import com.simon.generated.model.ErrorLogModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

/**
 * Class to populate  {@link ErrorLogModel} from Pair of {@link AbstractOrderModel} and {@link Exception}.
 * In this we use {@link guidKeyGenerator} to generate LogId for {@link ErrorLogModel}.
 */
public class ErrorLogPopulator implements Populator<Pair<AbstractOrderModel, Exception>, ErrorLogModel> {

	@Resource
	private KeyGenerator guidKeyGenerator;
	
	@Value("${errorlog.enable.stackTrace}")
	protected String isStackTraceLogEnabled;
	
	@Override
	public void populate(Pair<AbstractOrderModel, Exception> source,ErrorLogModel target)  {
		final AbstractOrderModel model =  source.getLeft();
		target.setId(model.getCode());
		final Exception exception=source.getRight();
		target.setLogId(guidKeyGenerator.generate().toString());
		target.setErrorMessage(exception.getMessage());
		if(Boolean.getBoolean(isStackTraceLogEnabled)){
			target.setErrorStackTrace(Arrays.toString(exception.getStackTrace()));
		}
		final List<AbstractOrderEntryModel> enrties = model.getEntries();
		final String productIdCommaSeparated=enrties.stream().map(entry->entry.getProduct().getCode()).collect(Collectors.joining(","));
		target.setProductIds(productIdCommaSeparated);
		
	}

}

