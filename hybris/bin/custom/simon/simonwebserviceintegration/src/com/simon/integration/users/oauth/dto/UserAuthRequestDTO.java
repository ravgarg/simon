package com.simon.integration.users.oauth.dto;

import com.simon.integration.dto.PojoAnnotation;

public class UserAuthRequestDTO extends PojoAnnotation
{
	private String username;
	private String password;
	
		public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
}
