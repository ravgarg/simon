package com.simon.integration.dto.email;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;
import com.simon.integration.dto.email.product.ShareEmailAttributesDTO;
import com.simon.integration.dto.email.product.ShareSubscriberDetails;

public class ShareEmailRequestDTO extends PojoAnnotation {

	private String customerKey;
	
	private List<ShareSubscriberDetails> subscriberDetails;
	
	private ShareEmailAttributesDTO emailAttributes;
	
	private String subscriptionDate;

	public String getCustomerKey() {
		return customerKey;
	}

	public void setCustomerKey(String customerKey) {
		this.customerKey = customerKey;
	}

	public List<ShareSubscriberDetails> getSubscriberDetails() {
		return subscriberDetails;
	}

	public void setSubscriberDetails(List<ShareSubscriberDetails> subscriberDetails) {
		this.subscriberDetails = subscriberDetails;
	}

	public ShareEmailAttributesDTO getEmailAttributes() {
		return emailAttributes;
	}

	public void setEmailAttributes(ShareEmailAttributesDTO emailAttributes) {
		this.emailAttributes = emailAttributes;
	}

	public String getSubscriptionDate() {
		return subscriptionDate;
	}

	public void setSubscriptionDate(String subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

}
