
package com.simon.integration.users.dto;

import com.simon.integration.dto.PojoAnnotation;

/**
 * This a DTO class used to perform get operation for user center.
 *
 */
public class UserCenterRequestDTO extends PojoAnnotation {

	private String zipCode;
	private Integer radius;
	private boolean lw;
	
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Integer getRadius() {
		return radius;
	}
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	public boolean isLw() {
		return lw;
	}
	public void setLw(boolean lw) {
		this.lw = lw;
	}
}
