
package com.simon.integration.users.address.verify.services;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;

/**
 * The Interface UserAddressVerifyIntegrationService.
 */
 public interface UserAddressVerifyIntegrationService
{

	/**
	 * Cart estimate.This method take cartModel and create cartEstimate request to call ESB then parse response in
	 * CarrtEstimateResponseDTO.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return the cart estimate response DTO
	 */

	 UserAddressVerifyResponseDTO validateAddress(ExtAddressData addressData) throws UserAddressVerifyIntegrationException;
	
}
