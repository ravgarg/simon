package com.simon.integration.dto.email.deal;

public class EmailAttributeDTO {

	private String subjectLine;
	private String message;
	private String xmlType;
	private String xmlData;
	/**
	 * @return the subjectLine
	 */
	public String getSubjectLine() {
		return subjectLine;
	}
	/**
	 * @param subjectLine the subjectLine to set
	 */
	public void setSubjectLine(String subjectLine) {
		this.subjectLine = subjectLine;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the xmlType
	 */
	public String getXmlType() {
		return xmlType;
	}
	/**
	 * @param xmlType the xmlType to set
	 */
	public void setXmlType(String xmlType) {
		this.xmlType = xmlType;
	}
	/**
	 * @return the xmlData
	 */
	public String getXmlData() {
		return xmlData;
	}
	/**
	 * @param xmlData the xmlData to set
	 */
	public void setXmlData(String xmlData) {
		this.xmlData = xmlData;
	}


	
	
}
