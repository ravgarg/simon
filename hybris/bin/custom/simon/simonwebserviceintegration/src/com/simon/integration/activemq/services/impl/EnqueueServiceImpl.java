
package com.simon.integration.activemq.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.JmsException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.activemq.client.impl.ActiveMqClientImpl;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.dto.PojoAnnotation;

/**
 * The Class EnqueueServiceImpl It have method to send your message to activeMq client.
 */
public class EnqueueServiceImpl implements EnqueueService
{

    private static final Logger LOG = LoggerFactory.getLogger(EnqueueServiceImpl.class);

    @Resource
    private ActiveMqClientImpl activeMqMessageSender;

    @Resource
    private Map jmsTemplateRefrenceMap;

    private static final String QUEUE_PUSH_FAILED_MESSAGE = "Exception occured while calling activemq client send method";

    private static final String OBJECT_NOTNULL_ERROR_MESSAGE = "Parameter object must not be null";

    private static final String COMMAND_NOTNULL_ERROR_MESSAGE = "Parameter command must not be null";

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendObjectMessage(final PojoAnnotation object, final JmsTemplateNameEnum command)
    {
        validateParameterNotNull(object, OBJECT_NOTNULL_ERROR_MESSAGE);
        validateParameterNotNull(command, COMMAND_NOTNULL_ERROR_MESSAGE);

        try
        {
            activeMqMessageSender.send(object, command.name());
        }
        catch (final JmsException | JsonProcessingException exception)
        {
            LOG.error(QUEUE_PUSH_FAILED_MESSAGE, exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendStringMessage(final String object, final JmsTemplateNameEnum command)
    {

        validateParameterNotNull(object, OBJECT_NOTNULL_ERROR_MESSAGE);
        validateParameterNotNull(command, COMMAND_NOTNULL_ERROR_MESSAGE);

        try
        {
            activeMqMessageSender.send(object, command.name());
        }
        catch (final JmsException | JsonProcessingException exception)
        {
            LOG.error(QUEUE_PUSH_FAILED_MESSAGE, exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendBytesArrayMessage(final byte[] object, final JmsTemplateNameEnum command)
    {
        validateParameterNotNull(object, OBJECT_NOTNULL_ERROR_MESSAGE);
        validateParameterNotNull(command, COMMAND_NOTNULL_ERROR_MESSAGE);

        try
        {
            activeMqMessageSender.send(object, command.name());
        }
        catch (final JmsException | JsonProcessingException exception)
        {
            LOG.error(QUEUE_PUSH_FAILED_MESSAGE, exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMapMessage(final Map object, final JmsTemplateNameEnum command)
    {
        validateParameterNotNull(object, OBJECT_NOTNULL_ERROR_MESSAGE);
        validateParameterNotNull(command, COMMAND_NOTNULL_ERROR_MESSAGE);

        try
        {
            activeMqMessageSender.send(object, command.name());
        }
        catch (final JmsException | JsonProcessingException exception)
        {
            LOG.error(QUEUE_PUSH_FAILED_MESSAGE, exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendObjectMessage(final PojoAnnotation object, final JmsTemplateNameEnum command, final Map<String, String> jmsHeaders)
    {
        validateParameterNotNull(object, OBJECT_NOTNULL_ERROR_MESSAGE);
        validateParameterNotNull(command, COMMAND_NOTNULL_ERROR_MESSAGE);

        try
        {
            activeMqMessageSender.send(object, command.name(), jmsHeaders);
        }
        catch (final JmsException | JsonProcessingException exception)
        {
            LOG.error(QUEUE_PUSH_FAILED_MESSAGE, exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause());
        }
    }

}
