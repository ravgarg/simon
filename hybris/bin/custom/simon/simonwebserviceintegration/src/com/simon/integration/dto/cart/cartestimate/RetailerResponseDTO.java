
package com.simon.integration.dto.cart.cartestimate;

import com.simon.integration.dto.PojoAnnotation;

public class RetailerResponseDTO extends PojoAnnotation
{

    private String retailerID;

    private EstimatedPriceDTO retailerPriceDTO;

    private RetailerExpectedDeliveryDaysDTO retailerExpectedDeliveryDays;

    public void setRetailerID(final String retailerID)
    {
        this.retailerID = retailerID;
    }

    public String getRetailerID()
    {
        return retailerID;
    }

    public EstimatedPriceDTO getRetailerPriceDTO()
    {
        return retailerPriceDTO;
    }

    public void setRetailerPriceDTO(EstimatedPriceDTO retailerPriceDTO)
    {
        this.retailerPriceDTO = retailerPriceDTO;
    }

    public void setRetailerExpectedDeliveryDays(final RetailerExpectedDeliveryDaysDTO retailerExpectedDeliveryDays)
    {
        this.retailerExpectedDeliveryDays = retailerExpectedDeliveryDays;
    }

    public RetailerExpectedDeliveryDaysDTO getRetailerExpectedDeliveryDays()
    {
        return retailerExpectedDeliveryDays;
    }

}
