package com.simon.integration.order.deliver.populators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.order.deliver.BillingDetailsDTO;
import com.simon.integration.dto.order.deliver.CartProductsDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.dto.order.deliver.RetailerDTO;
import com.simon.integration.dto.order.deliver.ShippingDetailsDTO;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

/**
 * Converter implementation for {@link OrderModel} as source and
 * {@link DeliverOrderRequestDTO} as target type.
 */
public class DeliverOrderDataPopulator implements Populator<OrderModel, DeliverOrderRequestDTO> {

	private static final String COUNTRY_USA = "United States of America";
	
	@Resource
    private ConfigurationService configurationService;
	
	@Override
	public void populate(OrderModel source, DeliverOrderRequestDTO target) {

		target.setCartId(source.getAdditionalCartInfo().getCartId());
		target.setOrderId(source.getCode());
		target.setRetailers(getRetailers(source));
		target.setRemoveOOS(Boolean.parseBoolean(configurationService.getConfiguration().getString(SimonIntegrationConstants.REMOVE_OOS)));
	}

	/**
	 * this method is used to get List of retailers in a Order.
	 *
	 * @param OrderModel source
	 * @return retailersList
	 */
	private List<RetailerDTO> getRetailers(OrderModel source) {

		final List<RetailerDTO> retailersList = new ArrayList<>();
		final Map<String, List<CartProductsDTO>> retailers = getRetailerWithProduct(source);
		if (MapUtils.isNotEmpty(retailers)) {
			for (final Entry<String, List<CartProductsDTO>> retailer : retailers.entrySet()) {
				final RetailerDTO retailerDTO = new RetailerDTO();
				retailerDTO.setRetailerId(retailer.getKey());
				retailerDTO.setProducts(retailer.getValue());
				retailerDTO.setShippingOption(getShippingOptions(source, retailer.getKey()));
				retailerDTO.setBillingAddress(getBillingDetails(source));
				retailerDTO.setShippingAddress(getShippingDetails(source));
				retailerDTO.setCoupons(getAppliedCouponsOnRetailers(source.getAllPromotionResults(), retailer.getKey()));
				retailersList.add(retailerDTO);
			}
		}
		return retailersList;
	}

	/**
	 * @description Method use to get the coupon list which has applied on the retailers
	 * @method getAppliedCouponsOnRetailers
	 * @param promotionResultModels
	 * @param retailerKey
	 * @return couponsList
	 */
	private List<String> getAppliedCouponsOnRetailers(Set<PromotionResultModel> promotionResultModels, String retailerKey) {
		final List<String> couponsList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(promotionResultModels)){
			for (final PromotionResultModel promotionResult : promotionResultModels) {
				final String retailerId = getRetailerIdForPromotion(promotionResult);
				if(StringUtils.isNotEmpty(retailerId) && retailerId.equals(retailerKey)){
					final String couponValue = promotionResult.getPromotion().getPromoCode();
					if(StringUtils.isNotBlank(couponValue)) {
					    couponsList.add(couponValue);
					}
				}
			}	
		}
		return couponsList;
	}
	
	/**
	 * @description Method use to get the retailer id for which promotion has been applied
	 * @method getRetailerIdForPromotion
	 * @param promotionResultModels
	 * @return retailerId
	 */
	private String getRetailerIdForPromotion(PromotionResultModel promotionResult) {
		//Retailer Id will be always present in retailer level promotion
		String retailerId = promotionResult.getRetailerID();
		
		//Retailer ID needs to be fetched from shopid in product from OrderEntry for Item Level promotion
		if(StringUtils.isEmpty(retailerId)){
			final Optional<PromotionOrderEntryConsumedModel> entyOption = promotionResult.getConsumedEntries().stream().findAny();
			if(entyOption.isPresent()){
				retailerId=entyOption.get().getOrderEntry().getProduct().getShop().getId();
			}
		}
			
		return retailerId;
	}

	/**
	 * this method is used to get Delivery Mode.
	 *
	 * @param OrderModel source and retailerID
	 * @return delivery Mode
	 */
	private String getShippingOptions(OrderModel source, String retailerID) {

		final Map<String, DeliveryModeModel> retailerShippingMode = source.getRetailersDeliveryModes();
		final DeliveryModeModel deliveryModeModel = retailerShippingMode.get(retailerID);
		return deliveryModeModel.getCode();
	}

	/**
	 * this method is used to get Shipping Address.
	 *
	 * @param OrderModel source
	 * @return shippingDetailsDTO
	 */
	private ShippingDetailsDTO getShippingDetails(OrderModel source) {

		final AddressModel shippingAddressModel = source.getDeliveryAddress();
		final ShippingDetailsDTO shippingDetailsDTO = new ShippingDetailsDTO();
		shippingDetailsDTO.setFirstName(shippingAddressModel.getFirstname());
		shippingDetailsDTO.setLastName(shippingAddressModel.getLastname());
		shippingDetailsDTO.setAddress(
				new StringBuilder(shippingAddressModel.getLine1()).append(" ").append(shippingAddressModel.getLine2()).toString());
		shippingDetailsDTO.setCity(shippingAddressModel.getTown());
		shippingDetailsDTO.setState(shippingAddressModel.getRegion().getName());
		shippingDetailsDTO.setZipCode(shippingAddressModel.getPostalcode());
		shippingDetailsDTO.setTelephone(shippingAddressModel.getPhone1());
		final CountryModel countryModel = shippingAddressModel.getCountry();
		if(null != countryModel){
			shippingDetailsDTO.setCountry(resolveCountry(countryModel));
		}else{
			shippingDetailsDTO.setCountry(StringUtils.EMPTY);
		}
		shippingDetailsDTO.setEmail(shippingAddressModel.getEmail());
		
		return shippingDetailsDTO;
	}
	
	/**
	 * @description Method use to resolve the country name issue to send the country in the order delivery esb call. 
	 * 			For Ex. order delivery call accept the country as United States of America not as US / USA / United States
	 * @method resolveCountry
	 * @param countryModel
	 * @return String
	 */
	private String resolveCountry(CountryModel countryModel){
		String countryIso = countryModel.getIsocode();
		if(SimonIntegrationConstants.COUNTRY_CODE_US.equalsIgnoreCase(countryIso)){
			countryIso = COUNTRY_USA;
		}
		return countryIso;
	}

	/**
	 * this method is used to get Billing Address.
	 *
	 * @param OrderModel source
	 * @return billingDetailsDTO
	 */
	private BillingDetailsDTO getBillingDetails(OrderModel source) {

		final AddressModel billingAddressModel = source.getPaymentInfo().getBillingAddress();
		final BillingDetailsDTO billingDetailsDTO = new BillingDetailsDTO();
		billingDetailsDTO.setFirstName(billingAddressModel.getFirstname());
		billingDetailsDTO.setLastName(billingAddressModel.getLastname());
		billingDetailsDTO.setAddress(
				new StringBuilder(billingAddressModel.getLine1()).append(billingAddressModel.getLine2()).toString());
		billingDetailsDTO.setCity(billingAddressModel.getTown());
		billingDetailsDTO.setState(billingAddressModel.getRegion().getName());
		billingDetailsDTO.setZipCode(billingAddressModel.getPostalcode());
		final CountryModel countryModel = billingAddressModel.getCountry();
		if(null != countryModel){
			billingDetailsDTO.setCountry(resolveCountry(countryModel));
		}else{
			billingDetailsDTO.setCountry(StringUtils.EMPTY);
		}
		billingDetailsDTO.setEmail(source.getDeliveryAddress().getEmail());
		billingDetailsDTO.setTelephone(source.getDeliveryAddress().getPhone1() != null ? source.getDeliveryAddress().getPhone1() : StringUtils.EMPTY);

		return billingDetailsDTO;
	}

	/**
	 * this method is used to get retailer with product in a map.
	 *
	 * @param  OrderModel source
	 * @return retailer Collection of Map<String, List<CartProductsDTO>>
	 */
	private Map<String, List<CartProductsDTO>> getRetailerWithProduct(OrderModel source) {

		String retailerId = StringUtils.EMPTY;
		List<CartProductsDTO> products;
		final Map<String, List<CartProductsDTO>> retailer = new HashMap<>();
		for (final AbstractOrderEntryModel entry : source.getEntries()) {
			retailerId = entry.getProduct().getShop().getId();
			if (!retailer.containsKey(retailerId)) {
				products = new ArrayList<>();
				retailer.put(retailerId, products);
			} else {
				products = retailer.get(retailerId);
			}

			final CartProductsDTO cartProductsDTO = new CartProductsDTO();
			final ProductModel productModel = entry.getProduct();
			cartProductsDTO.setProductId(productModel.getCode());
			final Map<String, String> productAttributes = getProductAttributes(productModel);
			cartProductsDTO.setProductAttributeMap(productAttributes);
			cartProductsDTO.setQuantity(entry.getQuantity().toString());
			products.add(cartProductsDTO);

			retailer.put(retailerId, products);
		}
		return retailer;
	}

	/**
	 * this method is used to get Product Attributes from Product.
	 *
	 * @param productModel
	 * @return productAttributes
	 */
	private Map<String, String> getProductAttributes(ProductModel productModel) {
		final Map<String, String> productAttributes = new HashMap<>();
		if (productModel instanceof GenericVariantProductModel) {
			final GenericVariantProductModel genericVariantProductModel = (GenericVariantProductModel) productModel;
			for (final CategoryModel category : genericVariantProductModel.getSupercategories()) {
				productAttributes.put(category.getSupercategories().get(0).getCode(), category.getName());
			}
		}
		return productAttributes;
	}
}