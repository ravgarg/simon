package com.simon.integration.share.email.exceptions;

public class SharedEmailIntegrationException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99002L;

	/** Exception code. */
	private final String code;


	/**
	 * Instantiates a new SharedEmailIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 */
	public SharedEmailIntegrationException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new SharedEmailIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public SharedEmailIntegrationException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}


	/**
	 * Instantiates a new SharedEmailIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public SharedEmailIntegrationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public SharedEmailIntegrationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public SharedEmailIntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}

}