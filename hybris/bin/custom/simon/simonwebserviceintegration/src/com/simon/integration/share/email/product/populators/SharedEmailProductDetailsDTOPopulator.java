package com.simon.integration.share.email.product.populators;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.email.product.EmailProductDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.product.EmailProductDetailDTO;
import com.simon.integration.dto.email.product.EmailProductRetailerDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;

/**
 * Converts EmailProductDetailsData to EmailProductRetailerDTO
 */
public class SharedEmailProductDetailsDTOPopulator
		implements Populator<EmailProductDetailsData, EmailProductRetailerDTO> {

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(EmailProductDetailsData source, EmailProductRetailerDTO target) {
		target.setRetailerName(source.getRetailerName());
		target.setSiteURL(shareEmailIntegrationUtils.getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_CONTEXT_ROOT));
		target.setProductDetails(populateProductDetails(source));
	}

	/**
	 * @method populateProductDetails
	 * @param source
	 * @return EmailProductDetailDTO
	 */
	private EmailProductDetailDTO populateProductDetails(EmailProductDetailsData source) {
		final EmailProductDetailDTO emailProductDetailDTO = new EmailProductDetailDTO();

		emailProductDetailDTO.setMsrp(source.getMsrp());
		emailProductDetailDTO.setProductName(source.getProductName());
		emailProductDetailDTO.setQuantity(source.getQuantity());
		emailProductDetailDTO.setSalePrice(source.getSalePrice());
		emailProductDetailDTO.setColorName(source.getColor());
		final String imageUrl = source.getImageUrl();
		emailProductDetailDTO.setImageUrl(StringUtils.isNotEmpty(imageUrl)
				? shareEmailIntegrationUtils.buildImageUrl(imageUrl) : StringUtils.EMPTY);
		final String shopNowUrl = source.getShopNowLinkUrl();
		emailProductDetailDTO.setShopNow(StringUtils.isNotEmpty(shopNowUrl)
				? shareEmailIntegrationUtils.buildShopNowLabel(shopNowUrl) : StringUtils.EMPTY);
		emailProductDetailDTO.setPercentageOff(source.getPercentageOff());
		return emailProductDetailDTO;
	}

}
