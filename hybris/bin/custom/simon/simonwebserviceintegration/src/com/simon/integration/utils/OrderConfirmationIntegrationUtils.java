package com.simon.integration.utils;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.simon.integration.constants.SimonIntegrationConstants;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 *Order Confirmation Integration Utils provides utility methods for Integration Layer
 *
 */
public class OrderConfirmationIntegrationUtils
{
	
	@Resource
	private ConfigurationService configurationService;
	
	/**
	 * @description Method use to validate that the given string contains '$' sign or not, if contains then remove it and
	 *              convert the given string in the Double.
	 * @method validateAndRemoveDollerSign
	 * @param priceValueWithDollerSign
	 * @return Double
	 */
	public Double validateAndRemoveDollerSign(String priceValueWithDollerSign)
	{
		Double priceValueWithoutDollerSign = Double.valueOf(0);
		String finalPriceValue = priceValueWithDollerSign;
		
		if(StringUtils.isEmpty(finalPriceValue)){
			finalPriceValue = String.valueOf(0);
		}
		
		if (StringUtils.isNotEmpty(finalPriceValue)
				&& finalPriceValue.contains(SimonIntegrationConstants.DOLLER_SIGN))
		{
			priceValueWithoutDollerSign = Double.parseDouble(finalPriceValue.substring(1));
		}
		else
		{
			priceValueWithoutDollerSign = Double.parseDouble(finalPriceValue);
		}
		return priceValueWithoutDollerSign;
	}
	
	
}
