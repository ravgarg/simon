/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.generated.setup;

import static com.simon.generated.constants.SimonwebserviceintegrationConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.simon.generated.constants.SimonwebserviceintegrationConstants;
import com.simon.generated.service.SimonwebserviceintegrationService;


@SystemSetup(extension = SimonwebserviceintegrationConstants.EXTENSIONNAME)
public class SimonwebserviceintegrationSystemSetup
{
	private final SimonwebserviceintegrationService simonwebserviceintegrationService;

	public SimonwebserviceintegrationSystemSetup(final SimonwebserviceintegrationService simonwebserviceintegrationService)
	{
		this.simonwebserviceintegrationService = simonwebserviceintegrationService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		simonwebserviceintegrationService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SimonwebserviceintegrationSystemSetup.class.getResourceAsStream("/simonwebserviceintegration/sap-hybris-platform.png");
	}
}
