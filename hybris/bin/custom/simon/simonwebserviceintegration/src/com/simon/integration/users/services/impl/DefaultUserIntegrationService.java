
package com.simon.integration.users.services.impl;

import javax.annotation.Resource;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserCenterRequestDTO;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.services.UserIntegrationEnhancedService;
import com.simon.integration.users.services.UserIntegrationService;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Implementation class DefaultUserIntegrationService.
 *
 */
public class DefaultUserIntegrationService implements UserIntegrationService  {

	private static final String DEFAULT_MALL_NAME = "Test Outlet";
	private static final String DEFAULT_MALL_ID = "888";
	/** The integration enhanced service. */
	@Resource
	private UserIntegrationEnhancedService userIntegrationEnhancedService;
	@Resource
	private Converter<RegisterData,UserRegisterUpdateRequestDTO> registerUserConverter;
	@Resource
	private Converter<CustomerData, UserRegisterUpdateRequestDTO> userDataConverter;
	@Resource
	private ConfigurationService configurationService;
	
	/**
	 * @description Method use to update the registered user details to show on the user profile page
	 * @method updateUserDetails
	 * @param userData
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException
	 */
	@Override
	public VIPUserDTO updateUserDetails(CustomerData userData) throws UserIntegrationException{
		if(isEsbPoDisabled()){
			final VIPUserDTO mockUser=new VIPUserDTO();
			mockUser.setId(SimonIntegrationConstants.DEFAULT_USER_ID);
			return mockUser;
		}
		final UserRegisterUpdateRequestDTO userDetailsDTO = userDataConverter.convert(userData);
		return userIntegrationEnhancedService.updateUserDetails(userDetailsDTO);
	}
	
	/**
	 * This method is used to register a user with PO.com with user details mapped in {@link RegisterData} and then parse response in
	 * UserLoginResponseDTO.
	 * @param registerData 
	 * @return {@link VIPUserDTO}
	 * @throws UserIntegrationException 
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public VIPUserDTO registerUser(final RegisterData registerData) throws UserIntegrationException, AuthLoginIntegrationException
	{
		if(isEsbPoDisabled()){
			final VIPUserDTO mockUser=new VIPUserDTO();
			mockUser.setId(SimonIntegrationConstants.DEFAULT_USER_ID);
			return mockUser;
		}
		final UserRegisterUpdateRequestDTO registerUserRequestDTO=registerUserConverter.convert(registerData);
		return userIntegrationEnhancedService.registerUser(registerUserRequestDTO);
	}

	/**
	 * This method is used get default center associated with a zip-code from PO.com.
	 * 
	 * @param zipCode
	 * @return {@link UserCenterIdDTO}
	 * @throws UserIntegrationException 
	 */
	@Override
	public UserCenterIdDTO getDefaultCenter(String zipCode) throws UserIntegrationException
	{
		if(isEsbPoDisabled()){
			final UserCenterIdDTO mockedCenter=new UserCenterIdDTO();
			mockedCenter.setId(DEFAULT_MALL_ID);
			mockedCenter.setOutletName(DEFAULT_MALL_NAME);	
			return mockedCenter;
		}
		final UserCenterRequestDTO userCenterRequestDTO = new UserCenterRequestDTO();
		userCenterRequestDTO.setZipCode(zipCode);
		userCenterRequestDTO.setRadius(Integer.parseInt(configurationService.getConfiguration().getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_MYCENTER_DEFAULT_RADIUS)));
		userCenterRequestDTO.setLw(Boolean.parseBoolean(configurationService.getConfiguration().getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_MYCENTER_LW)));
		
		return userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO);
	}

	/**
	 * @return
	 */
	private boolean isEsbPoDisabled()
	{
		return configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false);
	}

}