package com.simon.integration.users.oauth.services;

import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.users.login.dto.UserLoginRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
/**
 * The Interface AuthLoginIntegrationEnhancedService
 *
 */
public interface AuthLoginIntegrationEnhancedService
{
	/**
	 * This method is used to generate Auth token for a particular user with corresponding details in {@link UserAuthRequestDTO}
	 * @param pOAuthRequestDTO
	 * @return {@link UserAuthResponseDTO}
	 * @throws AuthLoginIntegrationException 
	 */
	UserAuthResponseDTO getOAuthToken(UserAuthRequestDTO pOAuthRequestDTO) throws AuthLoginIntegrationException;

	/**
	 * This method validates with PO.com for Login with user credentials(email and password) mapped in {@link UserLoginRequestDTO} and then parse response in
	 * POLoginResponseDTO.
	 * @param poLoginRequestDTO
	 * @return {@link VIPUserDTO}
	 * @throws AuthLoginIntegrationException 
	 * @throws TokenExpiredException 
	 */
	VIPUserDTO validateLogin(UserLoginRequestDTO poLoginRequestDTO) throws AuthLoginIntegrationException;

	/**
	 * This method is used to generate Auth token at the time of Registration
	 * @param pOAuthRequestDTO
	 * @return {@link UserAuthResponseDTO}
	 * @throws AuthLoginIntegrationException 
	 */
	UserAuthResponseDTO getOAuthTokenForRegisteration() throws AuthLoginIntegrationException;
}
