package com.simon.integration.users.oauth.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;

import com.simon.integration.users.login.dto.UserLoginRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;
/**
 * The class DefaultAuthLoginIntegrationEnhancedService
 *
 */
public class DefaultAuthLoginIntegrationEnhancedService implements AuthLoginIntegrationEnhancedService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAuthLoginIntegrationEnhancedService.class);
	@Resource
	private RestWebService restWebService;
	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	/**
	 * This method is used to generate Auth token for a particular user with corresponding details in {@link UserAuthRequestDTO}
	 * @param userAuthRequestDTO
	 * @return {@link UserAuthResponseDTO}
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public UserAuthResponseDTO getOAuthToken(UserAuthRequestDTO userAuthRequestDTO) throws AuthLoginIntegrationException{
		final String serviceUrl =userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_SERVICE_URL);
		try{
		final UserAuthResponseDTO response=restWebService.executeRestEvent(serviceUrl,userAuthRequestDTO, Boolean.FALSE.toString(), null,
				UserAuthResponseDTO.class, RequestMethod.POST, getHeadersForAuthenticationTokenGeneration(), userIntegrationUtils.populateAuthHeaderForESB());	
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to get authetication token and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new AuthLoginIntegrationException(response.getErrorMessage(),response.getErrorCode(), response.getUserName());
			}
		return response;	
		}catch(IntegrationException exception){
			LOGGER.error("Exception occured while generating auth token from third party: ", exception);
			throw new AuthLoginIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	
	/**
	 * This method is used to generate Auth token at the time of Registration
	 * 
	 * @return {@link UserAuthResponseDTO}
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public UserAuthResponseDTO getOAuthTokenForRegisteration() throws AuthLoginIntegrationException
	{
		final String serviceUrl =userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_SERVICE_URL);
		try{
			final UserAuthResponseDTO response=restWebService.executeRestEvent(serviceUrl,null, Boolean.FALSE.toString(), null,
			UserAuthResponseDTO.class, RequestMethod.GET, getHeadersForAuthenticationTokenGeneration(),  userIntegrationUtils.populateAuthHeaderForESB());	
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to get authetication token for registration and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new AuthLoginIntegrationException(response.getErrorMessage(),response.getErrorCode(), response.getUserName());
			}
			return response;	
		}catch(IntegrationException exception){
			LOGGER.error("Exception occured while generating Auth token for Registration from third party: ", exception);
			throw new AuthLoginIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}

	
	/**
	 * This method validates with PO.com for Login with user credentials(email and password) mapped in {@link UserLoginRequestDTO} and then parse response in
	 * {@link VIPUserDTO}.
	 * @param userLoginRequestDTO
	 * @return {@link VIPUserDTO}
	 * @throws AuthLoginIntegrationException 
	 * @throws TokenExpiredException 
	 */
	@Override
	public VIPUserDTO validateLogin(UserLoginRequestDTO userLoginRequestDTO) throws AuthLoginIntegrationException
	{
		final String serviceUrl = userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_LOGIN_END_POINT_URL);
		try{
		final VIPUserDTO response=restWebService.executeRestEvent(serviceUrl, userLoginRequestDTO, Boolean.FALSE.toString(), null,
				 VIPUserDTO.class, RequestMethod.POST, userIntegrationUtils.populateHeadersForESB(),  userIntegrationUtils.populateAuthHeaderForESB());
			if(StringUtils.isNotEmpty(response.getErrorCode())){
				LOGGER.error("Error occured when calling ESB to validate user login and getting error with code {} and message {}.", response.getErrorCode(), response.getErrorMessage());
				userIntegrationUtils.handleCommonErrors(response);
				throw new AuthLoginIntegrationException(response.getErrorMessage(),response.getErrorCode());
			}
			return response;	
		}catch(IntegrationException exception){
					LOGGER.error("Exception occured while validate Login call for the user to third party: {}", exception);
			throw new AuthLoginIntegrationException(exception.getMessage(), exception.getCause(), SimonIntegrationConstants.ERROR_FROM_ESB);
		}
	}
	
	/**
	 * This method creates a multi-value map and adds header data in it.
	 * @return {@link MultiValueMap}
	 */
	private MultiValueMap<String, Object> getHeadersForAuthenticationTokenGeneration() {
		final MultiValueMap<String, Object> headers = new LinkedMultiValueMap<>();
		headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON);
		return headers;
	}	

	
	
	
	
}
