package com.simon.integration.utils;

import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.product.ShareSubscriberDetails;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * Share Email Integration Utils provides utility methods for Integration Layer
 *
 */
public class ShareEmailIntegrationUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ShareEmailIntegrationUtils.class);
	private static final String SINGLE_QUOTES = "'";

	@Resource
	private ConfigurationService configurationService;
	
	public String convertShareEmailXmlDataInString(StringWriter stringWriter) {
		final String result = stringWriter.toString();
		final String[] finalXml = result.split("\\?>");
		return finalXml[1].replace('"', '\"');
	}


	/**
	 * @description Method use to convert current date and time in the define format
	 * @method convertDateTimeInString
	 * @param currentDate
	 * @return String
	 */
	public String convertCurrentDateTimeInString(Date currentDate) {
		String testDateString = StringUtils.EMPTY;
		final DateFormat dateFormat = new SimpleDateFormat(SimonIntegrationConstants.SHARE_EMAIL_DATE_TIME_FORMAT, Locale.US);
		try {
			testDateString = dateFormat.format(currentDate);
			LOGGER.info("Current Data and Time format {} is: {}", SimonIntegrationConstants.SHARE_EMAIL_DATE_TIME_FORMAT, testDateString);
		} catch (Exception ex) {
			LOGGER.error("Error getting in  parsing the current date in the given format {}", ex);
		}
		return testDateString;
	}
	
	/**
	 * @description Method use to convert current date and time in the define format
	 * @method convertDateTimeInString
	 * @param currentDate
	 * @return String
	 */
	public String convertDateInString(Date date) {
		String testDateString = StringUtils.EMPTY;
		final DateFormat dateFormat = new SimpleDateFormat(SimonIntegrationConstants.SHARE_ORDER_EMAIL_DATE_FORMAT, Locale.US);
		try {
			testDateString = dateFormat.format(date);
			LOGGER.info("Current Data and Time format {} is",SimonIntegrationConstants.SHARE_ORDER_EMAIL_DATE_FORMAT, testDateString);
		} catch (Exception ex) {
			LOGGER.error("Error getting in  parsing the current date in the given format {}", ex);
		}
		return testDateString;
	}
	
	/**
	 * @description Method use to build complete image url
	 * @method buildImageUrl
	 * @param imageUrl
	 * @return String
	 */
	public String buildImageUrl(String imageUrl){
		final StringBuilder finalImageUrl = new StringBuilder();
		finalImageUrl.append(getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_MEDIA_ROOT));
		if(StringUtils.isNotEmpty(finalImageUrl.toString())){
			finalImageUrl.append(imageUrl);
		}
		return finalImageUrl.toString();
	}
	
	/**
	 * @method populateShareSubscriberDetails
	 * @param source
	 * @return List<ShareSubscriberDetails>
	 */
	public List<ShareSubscriberDetails> populateShareSubscriberDetails(ShareEmailData source){
		final List<ShareSubscriberDetails> subscriberDetailsList = new ArrayList<>();
		List<String> userToEmails ;
		final String userToEmail = source.getEmailTo();
		if(userToEmail.contains(SimonIntegrationConstants.COMMA)){
			userToEmails = Arrays.asList(userToEmail.split(SimonIntegrationConstants.COMMA));
		}else{
			userToEmails = Arrays.asList(userToEmail);
		}
		for (final String toEmail : userToEmails) {
			final ShareSubscriberDetails subscriberDetails = new ShareSubscriberDetails();
			subscriberDetails.setEmailAddress(toEmail);
			subscriberDetails.setSubscriberKey(toEmail);
			subscriberDetails.setFromAddress(source.getEmailFrom());
			subscriberDetailsList.add(subscriberDetails);	
		}
		return subscriberDetailsList;
	}
	
	/**
	 * @description Method use to build product shop now link url
	 * @method buildShopNowLabel
	 * @param emailProductData
	 * @return String
	 */
	public String buildShopNowLabel(String shopNowUrl){
		final StringBuilder productShopNowLinkUrl = new StringBuilder();
		productShopNowLinkUrl.append(getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_CONTEXT_ROOT));
		if(StringUtils.isNotEmpty(productShopNowLinkUrl.toString())){
			productShopNowLinkUrl.append(shopNowUrl);
		}
		return productShopNowLinkUrl.toString();
	}
	

	public String getConfiguredStaticString(final String key)
	{
		final String configuredStringContent = configurationService.getConfiguration().getString(key);
		return (StringUtils.isNotBlank(configuredStringContent) ? configuredStringContent : StringUtils.EMPTY);
	}
	
}
