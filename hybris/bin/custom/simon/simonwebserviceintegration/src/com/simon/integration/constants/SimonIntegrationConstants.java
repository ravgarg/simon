/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.simon.integration.constants;

/**
 *
 */
public class SimonIntegrationConstants
{

	private SimonIntegrationConstants()
	{
		super();
	}

	public static final String ESB_LAYER_SERVICE_BASE_API_URL = "esb.layer.service.base.api.url";
	
	public static final String ESTIMATE_CART_STUB_FILE = "estimate.cart.service.stub.fileName";
	public static final String CARD_VERIFY_STUB_FILE = "stubs/CardVerificationResponse.json";
	public static final String REMOVE_PAYMENT_METHOD_TOKEN_STUB_FILE = "stubs/removePaymentResponse.json";
	
	public static final String ESTIMATE_CART_URL = "esb.estimate.cart.api.end.point.url";
	public static final String CARD_VERIFY_URL = "esb.card.verify.api.end.point.url";
	public static final String REOMOVE_PAYMENT_TOKEN_URL = "esb.remove.payment.token.api.end.point.url";
	public static final String ORDER_PURCHAGE_CONFIRM_API_URL = "esb.order.purchase.confirm.api.end.point.url";
	
	public static final String CREATE_CART_STUB = "create.cart.service.stub";
	public static final String ESTIMATE_CART_STUB = "esb.estimate.cart.api.enable.stub.flag";
	public static final String CARD_VERIFY_STUB = "esb.card.verify.api.enable.stub.flag";
	public static final String REMOVE_PAYMENT_METHOD_TOKEN = "esb.payment.method.token.enable.stub.flag";
	public static final String ORDER_UPDATE_CONFIRM_STUB = "order.update.confirm.service.stub";
	public static final String PURCHASE_CONFIRM_STUB = "esb.order.purchase.confirm.enable.stub.flag";
	public static final String PURCHASE_UPDATE_CONFIRM_STUB = "purchase.update.confirm.service.stub";
	public static final String CREATE_CART_STUB_FLAG = "esb.create.cart.api.enable.stub.flag";
	public static final String ORDER_DELIVERY_STUB_FLAG = "esb.order.delivery.api.enable.stub.flag";
	public static final String ESB_PO_API_SWITCH_FLAG = "esb.po.api.disable.stub.flag";
	public static final String USER_ADDRESS_SERVICE_API = "esb.users.address.verify.api.end.point.url";
	
	public static final String BASIC="Basic ";
	
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String APPLICATION_JSON_UTF_8 = "application/json;charset=UTF-8";
	public static final String CHAR_SET = "charset";
	public static final String UTF_8 = "UTF-8";
	
	public static final String APPLICATION_JSON = "application/json";
	
	public static final String AUTHORIZATION="Authorization";

	public static final String AUTH_LOGIN_INTEGRATION_SERVICE_URL = "authLogin.integration.service.url";
	public static final String AUTH_LOGIN_INTEGRATION_CLIENT_ID = "authLogin.integration.authClientId";
	public static final String AUTH_LOGIN_INTEGRATION_CLIENT_SECRET = "authLogin.integration.authClientSecret";
	public static final String AUTH_LOGIN_INTEGRATION_GRANT_TYPE = "authLogin.integration.authGrantType";
	public static final String AUTH_LOGIN_INTEGRATION_GRANT_TYPE_REGISTER = "authLogin.integration.authRegisterGrantType";
	public static final String AUTH_LOGIN_INTEGRATION_AUTH_SCOPE = "authLogin.integration.authScope";
	public static final String USER_INTEGRATION_SERVICE_LOGIN_END_POINT_URL = "user.integration.service.login.end.point.url";
	public static final String USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL = "user.integration.service.register.user.end.point.url";
	public static final String USER_INTEGRATION_SERVICE_CENTER_BY_ZIPCODE_END_POINT_URL = "user.integration.service.center.by.zipcode.end.point.url";
	public static final String DEFAULT_USER_ID="defaultId";
	public static final String AUTH_TOKEN = "authToken";
	public static final String BEARER="Bearer ";
	public static final String EQUAL = "=";
	public static final String SPACE = " ";
	public static final String COMMA = ",";
	public static final String SLASH = "/";
	public static final String DOLLER_SIGN = "$";
	public static final String PIPE = "|";
	public static final String COUNTRY_CODE_US = "US";
	public static final String USER_INTEGRATION_SERVICE_UPDATE_USER_END_POINT = "user.integration.service.update.user.end.point.url";
	public static final String INVALID_USER_CREDENTIALS= "Invalid username or password";
	
	public static final String ESB_INTEGRATION_SERVICE_HEADER_AUTHORIZATION_VALUE = "esb.integration.service.header.authorization.value";

	public static final String FAVOURITE_TYPE_PRODUCT = "products";
	public static final String FAVOURITE_TYPE_RETAILER = "retailers";
	public static final String FAVOURITE_TYPE_SIZE = "sizes";
	public static final String FAVOURITE_TYPE_DESIGNER = "designers";
	public static final String FAVOURITE_TYPE_OFFER = "offers";
	public static final String USER_INTEGRATION_SERVICE_FAVOURITES_GET_URL="user.integration.service.favourites.get.url";
	public static final String SHARED_EMAIL_INTEGRATION_SERVICE_END_POINT = "shared.email.integration.service.end.point.url";
	public static final String USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_ADDRESS_END_POINT = "user.address.integration.service.add.user.address.end.point.url";
	public static final String USER_ADDRESS_INTEGRATION_SERVICE_UPDATE_USER_ADDRESS_END_POINT = "user.address.integration.service.update.user.address.end.point.url";
	public static final String USER_ADDRESS_INTEGRATION_SERVICE_REMOVE_USER_ADDRESS_END_POINT = "user.address.integration.service.remove.user.address.end.point.url";
	public static final String USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_PAYMENT_ADDRESS_END_POINT = "user.address.integration.service.add.user.payment.address.end.point.url";
	public static final String USER_ADDRESS_INTEGRATION_SERVICE_GET_USER_ADDRESS_END_POINT="user.address.integration.service.get.user.address.end.point.url";
	public static final String USER_ADDRESS_EXPORT_EXTERNAL_ID_PREFIX = "user.address.export.external.id.prefix";
	public static final String PAYMENT_SERVICE_CARD_TYPE_VISA = "visa";
	public static final String PAYMENT_SERVICE_CARD_TYPE_AMERICAN_EXPRESS = "american_express";
	public static final String PAYMENT_SERVICE_CARD_TYPE_MASTER = "master";
	public static final String ESB_USER_ID="esb.integration.user.id";
	public static final String ESB_USER_PWRD="esb.integration.user.pwrd";
	public static final String PO_TOKEN="potoken";
	public static final String USER_INTEGRATION_SERVICE_MYCENTER_DEFAULT_RADIUS="user.integration.service.mycenter.radius";
	public static final String USER_INTEGRATION_SERVICE_MYCENTER_LW="user.integration.service.mycenter.lw";
	public static final String ERROR_FROM_ESB = "ESB_ERROR_1";
	public static final int PURCHASE_CONFIRM_ESB_RETRY_MAX_COUNT = 3;
	public static final int PURCHASE_CONFIRM_ESB_RETRY_DELAY = 1000;
	public static final String NO_ERROR_MSG_FROM_ESB = "Error message from ESB is null or empty";
	
	public static final String SHARE_EMAIL_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	public static final String SHARE_ORDER_EMAIL_DATE_FORMAT = "MM/dd/yyyy";
	public static final String SHARE_PROUDCT_OFFER_EMAIL_XML_TYPE = "share.product.offer.email.xml.type";
	public static final String WEBSITE_MEDIA_ROOT = "media.simon.https";
	public static final String WEBSITE_CONTEXT_ROOT = "website.simon.https";
	public static final String REMOVE_OOS = "deliver.remove.oos.flag";
	public static final String DUMMY_TOKEN = "dummyToken";

	public static final String ADDRESS_TYPE_HOME = "1";
	public static final String ADDRESS_TYPE_SHIPPING = "2";
	public static final String ADDRESS_TYPE_BILLING = "3";

}
