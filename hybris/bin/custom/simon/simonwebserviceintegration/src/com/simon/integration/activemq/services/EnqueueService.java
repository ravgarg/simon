
package com.simon.integration.activemq.services;

import java.util.Map;

import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.dto.PojoAnnotation;

/**
 * The Class EnqueueService It have method to send your message to activeMq client.
 */
public interface EnqueueService
{

    /**
     * This method is used to send Object Message to ActiveMQ Broker
     *
     * @param object POJO Message to be sent to ActiveMQ Broker
     * @param command Action for which the message is being sent
     * @throws IntegrationException Wrapper for the exceptions generated
     */
    public void sendObjectMessage(final PojoAnnotation object, final JmsTemplateNameEnum command);

    /**
     * This method is used to send string data to ActiveMQ Broker
     *
     * @param object String Message to be sent to ActiveMQ Broker
     * @param command Action for which the message is being sent
     * @throws IntegrationException Wrapper for the exceptions generated
     */
    public void sendStringMessage(final String object, final JmsTemplateNameEnum command);

    /**
     * This method is used to send bytes array data to ActiveMQ Broker
     *
     * @param object Byte Array Message to be sent to ActiveMQ Broker
     * @param command Action for which the message is being sent
     * @throws IntegrationException Wrapper for the exceptions generated
     */
    public void sendBytesArrayMessage(final byte[] object, final JmsTemplateNameEnum command);

    /**
     * This method is used to send map data to ActiveMQ Broker
     *
     * @param object Map Message to be sent to ActiveMQ Broker
     * @param command Action for which the message is being sent
     * @throws IntegrationException Wrapper for the exceptions generated
     */
    public void sendMapMessage(final Map object, final JmsTemplateNameEnum command);

    /**
     * This method is used to send map data to ActiveMQ Broker with JMS headers.
     *
     * @param object POJO Object Message to be sent to ActiveMQ Broker
     * @param command Action for which the message is being sent
     * @param jmsHeaders Additional attributes for PO
     * @throws IntegrationException Wrapper for the exceptions generated
     */
    public void sendObjectMessage(final PojoAnnotation object, final JmsTemplateNameEnum command, final Map<String, String> jmsHeaders);
}
