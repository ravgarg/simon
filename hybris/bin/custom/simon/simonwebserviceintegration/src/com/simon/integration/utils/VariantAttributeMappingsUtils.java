package com.simon.integration.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simon.core.model.VariantAttributeMappingModel;

/**
 * This is a utility class for VariantAttributeMappings in Integration Layer . This is having method to convertVariantAttributeMappingsMap from convertVariantAttributeMappingsList . 
 *
 */
public class VariantAttributeMappingsUtils {	

	private VariantAttributeMappingsUtils(){
		throw new IllegalStateException("VariantAttributeMappingsUtils class");
		}
	
	/**
	 * This method return Map of varientAttributeMappings Which is having key as Shop id and value is a map of source and
	 * target Attribute. 
	 *
	 * @param varientAttributeMappings
	 *           List of {@link VariantAttributeMappingModel}
	 * @return Variant attribute Map 
	 */
	public static Map<String, Map<String, String>> convertVariantAttributeMappingsMap(
			final List<VariantAttributeMappingModel> varientAttributeMappings)
	{
		final Map<String, Map<String, String>> varientAttributeMappingsMap = new HashMap<>();
		for (final VariantAttributeMappingModel variantAttributeModal : varientAttributeMappings)
		{
			final String shopId = variantAttributeModal.getShop().getId();
			Map<String, String> sourceTargetAttributeMap;
			if (varientAttributeMappingsMap.containsKey(shopId))
			{
				sourceTargetAttributeMap = varientAttributeMappingsMap.get(shopId);
			}else{
				sourceTargetAttributeMap = new HashMap<>();
			}
			sourceTargetAttributeMap.put(variantAttributeModal.getSourceAttribute(), variantAttributeModal.getTargetAttribute());
			varientAttributeMappingsMap.put(shopId, sourceTargetAttributeMap);
		}
		return varientAttributeMappingsMap;

	}
}
