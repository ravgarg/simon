package com.simon.integration.dto.email.product;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
public class ShareMessageDTO {
	
	@XmlAttribute(name = "text")
	private String text;

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	

}
