package com.simon.integration.share.email.services;

import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;

/**
 * Interface ShareEmailIntegrationEnhancedService use to maintains the methods for the shared product email data
 */
public interface ShareEmailIntegrationEnhancedService {

	/**
	 * @description Method use to shared product details in email 
	 * @method sharedProductEmail
	 * @param sharedProductEmailRequestDTO
	 * @throws SharedEmailIntegrationException
	 */
	void shareEmail(ShareEmailRequestDTO sharedProductEmailRequestDTO) throws SharedEmailIntegrationException;

	
}
