package com.simon.integration.dto.email.deal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.simon.integration.dto.email.product.ShareMessageDTO;

@XmlRootElement(name = "root")
@XmlAccessorType (XmlAccessType.FIELD)
public class EmailOfferDTO {

	
	@XmlElement(name = "retailer")
	private RetailerDTO retailer;
	
	@XmlElement(name = "message")
	private ShareMessageDTO message;

	/**
	 * @param message the message to set
	 */
	public void setMessage(ShareMessageDTO message) {
		this.message = message;
	}

	/**
	 * @param retailer the retailer to set
	 */
	public void setRetailer(RetailerDTO retailer) {
		this.retailer = retailer;
	}

	

	

}
