package com.simon.integration.dto.email.deal;



import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;

@XmlRootElement(name = "offer")
public class OfferDTO{
	
	@XmlAttribute(name = "description")
	private String description;
	
	@XmlAttribute(name = "valid")
	private String valid;
	
	@XmlAttribute(name = "disclaimer")
	private String disclaimer;
	
	@XmlAttribute(name = "shopurl")
	private String shopurl;

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(String valid) {
		this.valid = valid;
	}

	/**
	 * @param disclaimer the disclaimer to set
	 */
	public void setDisclaimer(String disclaimer) {
		this.disclaimer = disclaimer;
	}

	/**
	 * @param shopurl the shopurl to set
	 */
	public void setShopurl(String shopurl) {
		this.shopurl = shopurl;
	}

}
