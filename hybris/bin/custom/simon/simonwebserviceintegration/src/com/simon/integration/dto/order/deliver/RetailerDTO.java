package com.simon.integration.dto.order.deliver;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

public class RetailerDTO extends PojoAnnotation{
	
	private String retailerId;
	private List<CartProductsDTO> products;	 
	private ShippingDetailsDTO shippingAddress;	 	 
	private BillingDetailsDTO billingAddress;
	private String shippingOption;
	private List<String> coupons;
	
	public String getRetailerId() {
		return retailerId;
	}
	public void setRetailerId(String retailerId) {
		this.retailerId = retailerId;
	}
	public List<CartProductsDTO> getProducts() {
		return products;
	}
	public void setProducts(List<CartProductsDTO> products) {
		this.products = products;
	}
	public ShippingDetailsDTO getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(ShippingDetailsDTO shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public BillingDetailsDTO getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(BillingDetailsDTO billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getShippingOption() {
		return shippingOption;
	}
	public void setShippingOption(String shippingOption) {
		this.shippingOption = shippingOption;
	}
	public List<String> getCoupons() {
		return coupons;
	}
	public void setCoupons(List<String> coupons) {
		this.coupons = coupons;
	}
	
}
