package com.simon.integration.exceptions;

import org.apache.commons.lang.StringUtils;

public class AuthLoginIntegrationException extends Exception {

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99002L;

	/** Exception code. */
	private final String code;
	
	/** Exception for userName. */
	private final String userName;


	/**
	 * Instantiates a new AuthLoginIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 */
	public AuthLoginIntegrationException(final String message)
	{
		super(message);
		this.code = StringUtils.EMPTY;
		this.userName = StringUtils.EMPTY;
	}

	/**
	 * Instantiates a new AuthLoginIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public AuthLoginIntegrationException(final String message, final String code)
	{
		super(message);
		this.code = code;
		this.userName = StringUtils.EMPTY;
	}
	
	/**
	 * Instantiates a new AuthLoginIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public AuthLoginIntegrationException(final String message, final String code, final String userName)
	{
		super(message);
		this.code = code;
		this.userName = userName;
	}


	/**
	 * Instantiates a new AuthLoginIntegrationException exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public AuthLoginIntegrationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
		this.userName = StringUtils.EMPTY;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public AuthLoginIntegrationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
		this.userName = StringUtils.EMPTY;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public AuthLoginIntegrationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = StringUtils.EMPTY;
		this.userName = StringUtils.EMPTY;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}

	public String getUserName() {
		return userName;
	}

}
