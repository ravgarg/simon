
package com.simon.integration.purchase.services;

import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.OrderPurchaseIntegrationException;

/**
 * The Interface OrderPurchaseIntegrationService.
 */
public interface OrderPurchaseIntegrationService
{
	/**
	 * @description Method use to call ESB layer to call purchase update 
	 * @method invokePurchaseConfirmRequest
	 * @param orderCode
	 * @return PurchaseConfirmResponseDTO
	 */
	PurchaseConfirmResponseDTO invokePurchaseConfirmRequest(String orderCode) throws OrderPurchaseIntegrationException;
}