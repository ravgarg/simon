package com.simon.integration.users.oauth.services;

import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
/**
 * The Interface AuthLoginIntegrationService
 *
 */
public interface AuthLoginIntegrationService
{
	/**
	 * This method is used to generate Auth token for a particular user with corresponding details in {@link UserAuthRequestDTO} and returns true if it is generated successfully else false.
	 * @param username
	 * @param password
	 * @return boolean
	 * @throws AuthLoginIntegrationException 
	 */
	boolean getOAuthToken(String username,String password) throws AuthLoginIntegrationException;
	
	/**
	 * This method validates with PO.com for Login with user credentials(email and password) and then parse response in
	 * UserLoginResponseDTO.
	 * @param email
	 * @param password
	 * @return {@link VIPUserDTO}
	 * @throws AuthLoginIntegrationException 
	 * @throws TokenExpiredException 
	 */
	VIPUserDTO validateLogin(String email,String password) throws AuthLoginIntegrationException;
	
	/**
	 * This method is used to generate Auth token while registering  and returns true if it is generated successfully else false.
	 *
	 * @return boolean
	 * @throws AuthLoginIntegrationException 
	 */
	public boolean generateAndSaveOAuthTokenForRegistration() throws AuthLoginIntegrationException;
}
