package com.simon.integration.share.email.order.populators;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.facades.email.order.ShippingDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.order.EmailOrderDetailDTO;
import com.simon.integration.dto.email.order.EmailOrderRequestDTO;
import com.simon.integration.dto.email.order.EmailOrderRetailerDTO;
import com.simon.integration.dto.email.order.EmailShippingDetailDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

/**
 * Converts EmailOrderData to EmailOrderRequestDTO
 */
public class EmailOrderDetailsDTOPopulator implements Populator<EmailOrderData, EmailOrderRequestDTO>
{

    @Resource(name = "emailOrderRetailerDetailsDTOConverter")
    private Converter<RetailerDetailsData, EmailOrderRetailerDTO> emailOrderRetailerDetailsDTOConverter;

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	
    @Override
    public void populate(EmailOrderData source, EmailOrderRequestDTO target)
    {
        target.setOrder(populateOrderDetails(source.getOrderDetail()));
        target.setShipping(populateShippingDetails(source.getShippingDetail()));
        target.setRetailers(populateRetailersDetail(source.getRetailersDetail()));
    }

    /**
     * @description Method use to populate the order details
     * @method populateOrderDetails
     * @param emailOrderDetailsData
     * @return
     */
    private EmailOrderDetailDTO populateOrderDetails(EmailOrderDetailsData emailOrderDetailsData)
    {
        final EmailOrderDetailDTO emailOrderDetailDTO = new EmailOrderDetailDTO();

        emailOrderDetailDTO.setSiteURL(shareEmailIntegrationUtils
				.getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_CONTEXT_ROOT));
        emailOrderDetailDTO.setOrderdate(emailOrderDetailsData.getOrderDate());
        emailOrderDetailDTO.setOrderid(emailOrderDetailsData.getOrderId());
        emailOrderDetailDTO.setOrderpayment(emailOrderDetailsData.getOrderPayment());
        emailOrderDetailDTO.setOrdersaved(emailOrderDetailsData.getOrderSaved());
        emailOrderDetailDTO.setOrdershipping(emailOrderDetailsData.getOrderShipping());
        emailOrderDetailDTO.setOrderstatus(emailOrderDetailsData.getOrderStatus());
        emailOrderDetailDTO.setOrdersubtotal(emailOrderDetailsData.getOrderSubtotal());
        emailOrderDetailDTO.setOrdertaxes(emailOrderDetailsData.getOrderTaxes());
        emailOrderDetailDTO.setOrdertotal(emailOrderDetailsData.getOrderTotal());

        return emailOrderDetailDTO;
    }

    /**
     * @description Method use to populate the Shipping Address details
     * @method populateShippingDetails
     * @param shippingDetailsData
     * @return
     */
    private EmailShippingDetailDTO populateShippingDetails(ShippingDetailsData shippingDetailsData)
    {
        final EmailShippingDetailDTO shippingDetailDTO = new EmailShippingDetailDTO();

        shippingDetailDTO.setFname(shippingDetailsData.getFirstName());
        shippingDetailDTO.setLname(shippingDetailsData.getLastName());
        shippingDetailDTO.setAddress1(shippingDetailsData.getAddressLine1());
        shippingDetailDTO.setAddress2(shippingDetailsData.getAddressLine2());
        shippingDetailDTO.setCity(shippingDetailsData.getCity());
        shippingDetailDTO.setZip(shippingDetailsData.getZip());
        shippingDetailDTO.setState(shippingDetailsData.getState());

        return shippingDetailDTO;
    }

    /**
     * @description Method use to populate the Order Retailer DTO List
     * @method populateRetailersDetail
     * @param retailerDetailsDatas
     * @return
     */
    private List<EmailOrderRetailerDTO> populateRetailersDetail(List<RetailerDetailsData> retailerDetailsDatas)
    {
        final List<EmailOrderRetailerDTO> orderRetailerDTOs = new ArrayList<>();
        orderRetailerDTOs.addAll(emailOrderRetailerDetailsDTOConverter.convertAll(retailerDetailsDatas));
        return orderRetailerDTOs;
    }
}
