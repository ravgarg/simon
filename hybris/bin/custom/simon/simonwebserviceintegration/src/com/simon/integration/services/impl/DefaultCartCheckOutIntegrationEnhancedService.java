
package com.simon.integration.services.impl;

import javax.annotation.Resource;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.services.CartCheckOutIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * Enhanced implementation for {@link DefaultCartCheckOutIntegrationService}<br>
 * Default implementation for {@link CartCheckOutIntegrationEnhancedService}
 */
public class DefaultCartCheckOutIntegrationEnhancedService implements CartCheckOutIntegrationEnhancedService
{

    @Resource
    private RestWebService restWebService;

    @Resource
    private ConfigurationService configurationService;

    @Resource
    private EnqueueService enqueueService;

    @Resource
    private UserIntegrationUtils userIntegrationUtils;

    /**
     * {@inheritDoc}
     */
    @Override
    public void createCart(final CreateCartDTO createCartDTO)
    {
        enqueueService.sendObjectMessage(createCartDTO, JmsTemplateNameEnum.CREATE_CART);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CartEstimateResponseDTO estimateCart(final CartEstimateRequestDTO cartEstimateRequestDTO)
    {
        final String serviceUrl = buildCartCheckoutServiceRequestApiUrl();

        final String stubCall = configurationService.getConfiguration().getString(SimonIntegrationConstants.ESTIMATE_CART_STUB);
        final String stubFileName =configurationService.getConfiguration().getString(SimonIntegrationConstants.ESTIMATE_CART_STUB_FILE); 
        		
        return restWebService.executeRestEvent(serviceUrl, cartEstimateRequestDTO, stubCall, stubFileName, CartEstimateResponseDTO.class, RequestMethod.POST,
            getHeaders(), userIntegrationUtils.populateAuthHeaderForESB());

    }

    private String buildCartCheckoutServiceRequestApiUrl()
    {
        return userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.ESTIMATE_CART_URL);
    }

    private MultiValueMap<String, Object> getHeaders()
    {
        final MultiValueMap<String, Object> headers = getHeadersMap();
        headers.add(SimonIntegrationConstants.CONTENT_TYPE, SimonIntegrationConstants.APPLICATION_JSON_UTF_8);
        headers.add(SimonIntegrationConstants.CHAR_SET, SimonIntegrationConstants.UTF_8);
        return headers;
    }

    private MultiValueMap<String, Object> getHeadersMap()
    {
        return new LinkedMultiValueMap<>();
    }

}
