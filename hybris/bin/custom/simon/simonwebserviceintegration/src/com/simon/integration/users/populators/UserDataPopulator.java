package com.simon.integration.users.populators;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

/**
* Converter implementation for
* {@link de.hybris.platform.commercefacades.user.data.CustomerData} as source and
* {@link com.simon.integration.users.dto.UserRegisterUpdateRequestDTO} as target
* type.
*/
public class UserDataPopulator implements Populator<CustomerData, UserRegisterUpdateRequestDTO>{
	@Resource
	private CommonI18NService commonI18NService;
	
	@Resource
	private UserIntegrationUtils userIntegrationUtils;
	
	@Resource
	private Converter<MallData, UserCenterIdDTO> userCenterIdDataConverter;
	
	@Override
	public void populate(CustomerData source, UserRegisterUpdateRequestDTO target){
		
		target.setFirstName(source.getFirstName() != null ? source.getFirstName() : StringUtils.EMPTY);
		target.setLastName(source.getLastName() != null ? source.getLastName() : StringUtils.EMPTY);
		target.setZipCode(source.getZipcode());
		target.setEmailAddress(source.getEmailAddress() != null ? StringUtils.lowerCase(source.getEmailAddress()) : StringUtils.EMPTY);
		target.setPassword(source.getPassword() != null ? source.getPassword() : StringUtils.EMPTY);
		target.setOldPassword(source.getOldPassword()!= null ?source.getOldPassword(): StringUtils.EMPTY);
		target.setCountry(source.getCountry() != null ? getCountry(source) : StringUtils.EMPTY);
		target.setMobileNumber(source.getMobileNumber()!= null ? source.getMobileNumber() : StringUtils.EMPTY);
		target.setBirthMonth(source.getBirthMonth()!= null ?userIntegrationUtils.getBirthMonthCode(source.getBirthMonth().getCode()):StringUtils.EMPTY); 
		if(source.getGender()!=null){
			target.setGender(userIntegrationUtils.getGenderCode(source.getGender().getCode()));	
		}
		target.setBirthYear(source.getBirthYear());
		target.setHouseholdIncome(source.getHouseholdIncomeCode());
		final List<UserCenterIdDTO> userCenterIdDTOList = new ArrayList<>();
		final MallData primaryMall=source.getPrimaryMall();
		if(primaryMall!=null){
			userCenterIdDTOList.add(userCenterIdDataConverter.convert(primaryMall));
		}
		final List<MallData> centerIdDatas = source.getAlternateMalls();
		if(CollectionUtils.isNotEmpty(centerIdDatas)){
			userCenterIdDTOList.addAll(userCenterIdDataConverter.convertAll(centerIdDatas));	
		}
		
		
		target.setCenterIds(userCenterIdDTOList);	
		target.setOptedInEmail(BooleanUtils.toInteger(source.isOptedInEmail()));
	}
	

	
	
	/**
	 * this method is used to get external ID for country.
	 *
	 * @param source
	 * @return
	 */
	private String getCountry(final CustomerData source)
	{
		return commonI18NService.getCountry(source.getCountry()).getExternalId();
	}
}