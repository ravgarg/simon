
package com.simon.integration.activemq.enums;

/**
 * This Enum have jms template name which a developer pass to ActiveMqServiceImpl class methods.
 */
public enum JmsTemplateNameEnum
{
	CREATE_CART, PLACE_ORDER, ADD_FAV , REMOVE_FAV;

}
