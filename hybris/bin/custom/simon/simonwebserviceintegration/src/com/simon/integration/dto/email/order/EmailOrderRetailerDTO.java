package com.simon.integration.dto.email.order;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "retailer")
@XmlAccessorType (XmlAccessType.FIELD)
public class EmailOrderRetailerDTO {
	
	@XmlAttribute(name = "name")
	private String name;
	
	@XmlAttribute(name = "ordernumber")
	private String  ordernumber;
	
	@XmlAttribute(name = "retailerpromomsg")
	private String  retailerpromomsg;
	
	@XmlAttribute(name = "status")
	private String  status;
	
	@XmlAttribute(name = "subtotal")
	private String  subtotal;
	
	@XmlAttribute(name = "shipping")
	private String  shipping;
	
	@XmlAttribute(name = "tax")
	private String  tax;
	
	@XmlAttribute(name = "total")
	private String  total;
	
	@XmlAttribute(name = "saved")
	private String  saved;
	
	@XmlAttribute(name = "shippingdesc")
	private String  shippingdesc;
	
	@XmlElement(name = "item")
	private List<EmailOrderProductDTO> items;
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param ordernumber the ordernumber to set
	 */
	public void setOrdernumber(String ordernumber) {
		this.ordernumber = ordernumber;
	}
	/**
	 * @param retailerpromomsg the retailerpromomsg to set
	 */
	public void setRetailerpromomsg(String retailerpromomsg) {
		this.retailerpromomsg = retailerpromomsg;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}
	/**
	 * @param shipping the shipping to set
	 */
	public void setShipping(String shipping) {
		this.shipping = shipping;
	}
	/**
	 * @param tax the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	/**
	 * @param saved the saved to set
	 */
	public void setSaved(String saved) {
		this.saved = saved;
	}
	/**
	 * @param shippingdesc the shippingdesc to set
	 */
	public void setShippingdesc(String shippingdesc) {
		this.shippingdesc = shippingdesc;
	}
	/**
	 * @param items the items to set
	 */
	public void setItems(List<EmailOrderProductDTO> items) {
		this.items = items;
	}

}
