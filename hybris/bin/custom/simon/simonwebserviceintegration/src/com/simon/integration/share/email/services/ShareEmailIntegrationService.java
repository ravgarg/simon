package com.simon.integration.share.email.services;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;

/**
 * Interface ShareEmailIntegrationService use to maintains the methods for the share email data for Product and Deal
 */
public interface ShareEmailIntegrationService {
	
	/**
	 * @description Method use to shared product details in email
	 * @method sharedProductEmailData
	 * @param shareEmailData
	 * @throws SharedEmailIntegrationException
	 */
	void sharedProductEmailData(ShareEmailData shareEmailData) throws SharedEmailIntegrationException;

	/**
	 * Share Email Data.In this method we convert {@link ShareEmailData}
	 * into {@link ShareEmailRequestDTO} and by using enhance Service share email
	 * 
	 * @param shareDealEmailData
	 *  	The Share deal Email Data
	 * @throws SharedEmailIntegrationException
	 *         Any email exception Occurs
	 */
	void shareDealEmail(ShareEmailData shareEmailData)	throws SharedEmailIntegrationException;
	
	/**
	 * @description Method use to populate order data to share the order confirmation details in email 
	 * @method sharedOrderConfirmationEmailData
	 * @param emailOrderData
	 * @throws SharedEmailIntegrationException
	 */
	void sharedOrderConfirmationEmailData(ShareEmailData shareEmailData) throws SharedEmailIntegrationException;
}
