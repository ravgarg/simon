package com.simon.integration.users.favourites.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.PojoAnnotation;

public class AddUserFavouriteRequestDTO extends PojoAnnotation 
{
	private String favoriteValue;
	private String externalID;
	private String favType;
	
	
	public String getFavoriteValue()
	{
		return favoriteValue;
	}
	
	@JsonProperty("FavoriteValue") 
	public void setFavoriteValue(String favoriteValue)
	{
		this.favoriteValue = favoriteValue;
	}
	
	public String getExternalID()
	{
		return externalID;
	}
	
	@JsonProperty("ExternalID") 
	public void setExternalID(String externalID)
	{
		this.externalID = externalID;
	}
	
	public String getFavType()
	{
		return favType;
	}
	
	@JsonProperty("FavType") 
	public void setFavType(String favType)
	{
		this.favType = favType;
	}
}
