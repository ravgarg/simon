package com.simon.integration.dto.card.verification;

import com.simon.integration.dto.PojoAnnotation;

public class CardVerificationRequestDTO extends PojoAnnotation{

	private String paymentMethodToken;
	private boolean retainOnSuccess;
	private boolean continueCaching;

	public boolean isRetainOnSuccess() {
		return retainOnSuccess;
	}

	public void setRetainOnSuccess(boolean retainOnSuccess) {
		this.retainOnSuccess = retainOnSuccess;
	}

	public boolean isContinueCaching() {
		return continueCaching;
	}

	public void setContinueCaching(boolean continueCaching) {
		this.continueCaching = continueCaching;
	}

	public String getPaymentMethodToken() {
		return paymentMethodToken;
	}

	public void setPaymentMethodToken(String paymentMethodToken) {
		this.paymentMethodToken = paymentMethodToken;
	}
}
