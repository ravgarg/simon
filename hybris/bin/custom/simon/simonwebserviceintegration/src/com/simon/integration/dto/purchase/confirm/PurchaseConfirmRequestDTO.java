package com.simon.integration.dto.purchase.confirm;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.simon.integration.dto.PojoAnnotation;

public class PurchaseConfirmRequestDTO extends PojoAnnotation {

	private String orderId;

	public String getOrderId() {
		return orderId;
	}
	@JsonProperty("orderId")
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	
}