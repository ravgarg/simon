
package com.simon.integration.users.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.simon.integration.dto.PojoAnnotation;

/**
 * This a DTO class used to perform CURD operation for user address.
 *
 */
public class UserAddressDTO extends PojoAnnotation {

	private String userId;
	private String externalId;
	private String street1;
	private String street2;
	private String street3;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String firstName;
	private String lastName;
	private String type;
	private int primary;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	@JsonProperty("userID")
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the externalId
	 */
	public String getExternalId() {
		return externalId;
	}

	/**
	 * @param externalId
	 *            the externalId to set
	 */
	@JsonProperty("external_id")
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	/**
	 * @return the street1
	 */
	public String getStreet1() {
		return street1;
	}

	/**
	 * @param street1
	 *            the street1 to set
	 */
	@JsonProperty("street1")
	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	/**
	 * @return the street2
	 */
	public String getStreet2() {
		return street2;
	}

	/**
	 * @param street2
	 *            the street2 to set
	 */
	@JsonProperty("street2")
	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	/**
	 * @return the street3
	 */
	public String getStreet3() {
		return street3;
	}

	/**
	 * @param street3
	 *            the street3 to set
	 */
	@JsonProperty("street3")
	public void setStreet3(String street3) {
		this.street3 = street3;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	@JsonProperty("city")
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	@JsonProperty("state")
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	@JsonProperty("zip")
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	@JsonProperty("country")
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	@JsonProperty("firstname")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	@JsonProperty("lastname")
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the primary
	 */
	public int getPrimary() {
		return primary;
	}

	/**
	 * @param primary
	 *            the primary to set
	 */
	@JsonProperty("primary")
	public void setPrimary(int primary) {
		this.primary = primary;
	}

}
