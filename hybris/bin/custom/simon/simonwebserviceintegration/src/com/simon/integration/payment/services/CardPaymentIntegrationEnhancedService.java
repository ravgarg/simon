package com.simon.integration.payment.services;

import com.simon.integration.dto.card.redact.RemovePaymentRequestDTO;
import com.simon.integration.dto.card.redact.RemovePaymentResponseDTO;
import com.simon.integration.dto.card.verification.CardVerificationRequestDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;

/**
 * Payment Integration Enhanced Interface .This interface verify card with gateway and deliver order.
 */
public interface CardPaymentIntegrationEnhancedService
{

    /**
     * Card Verification. This method take CardVerificationRequestDTO and verify card with a call to ESB and then parse
     * response in CardVerificationResponseDTO
     *
     * @param cardVerificationRequestDTO
     * @return Card VerificationResponseDTO
     */
    CardVerificationResponseDTO verifyCard(final CardVerificationRequestDTO cardVerificationRequestDTO);

    /**
     * Deliver order. This method take DeliverOrderRequestDTO and deliver order with a call to ESB
     *
     * @param deliverOrderRequestDTO
     */
    void deliverOrder(DeliverOrderRequestDTO deliverOrderRequestDTO);
    
    /**
     * Remove Payment Token Verification. This method remove Payment Token with a call to ESB and then parse
     * response in CardVerificationResponseDTO
     *
     * @param cardVerificationRequestDTO
     * @return Card VerificationResponseDTO
     */
	RemovePaymentResponseDTO removePaymentMethodToken(RemovePaymentRequestDTO removePaymentRequestDTO);
}
