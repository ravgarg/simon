package com.simon.integration.users.favourites.services;

import java.util.List;

/**
 * This Interface is used to integrate favorites data with PO.com
 *
 */
public interface UserFavouritesIntegrationService
{
	/**
	 * This method is used to synch favorite product added to PO.com.
	 *  
	 * @param externalID
	 * @param favouriteValue 
	 * 
	 * @return true if favorite added successfully
	 */
	public boolean addUserFavouriteProduct(String externalID,String favouriteValue); 
	/**
	 * This method is used to synch favorite size added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue 
	 * 
	 * @return true if favorite added successfully
	 */
	public boolean addUserFavouriteSize(String externalID,String favouriteValue); 
	/**
	 * This method is used to synch favorite designer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue 
	 * 
	 * @return true if favorite added successfully
	 */
	public boolean addUserFavouriteDesigner(String externalID,String favouriteValue);
	/**
	 * This method is used to synch favorite retailer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue 
	 * 
	 * @return true if favorite added successfully
	 */
	public boolean addUserFavouriteStore(String externalID,String favouriteValue);
	/**
	 * This method is used to synch favorite offer added to PO.com.
	 * 
	 * @param externalID
	 * @param favouriteValue 
	 * 
	 * @return true if favorite added successfully
	 */
	public boolean addUserFavouriteOffer(String externalID,String favouriteValue);
	/**
	 * This method is used to synch favorite product removed from favorites list to PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	public boolean removeUserFavouriteProduct(String externalID); 
	/**
	 * This method is used to synch favorite size removed from favorites list to PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	public boolean removeUserFavouriteSize(String externalID); 
	/**
	 * This method is used to synch favorite designer removed from favorites list to PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	public boolean removeUserFavouriteDesigner(String externalID);
	/**
	 * This method is used to synch favorite retailer removed from favorites list to PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	public boolean removeUserFavouriteStore(String externalID);
	/**
	 * This method is used to synch favorite offer removed from favorites list to PO.com.
	 * 
	 * @param externalID
	 * @return true if favorite removed successfully
	 */
	public boolean removeUserFavouriteOffer(String externalID);
	/**
	 * This method is used to get all added favorite Products from PO.com.
	 * 
	 * @return all Favorite Products
	 */
	public List<String> getAllFavouriteProducts();
	/**
	 * This method is used to get all added favorite Sizes from PO.com.
	 * 
	 * @return all Favorite Sizes
	 */
	public List<String> getAllFavouriteSizes();
	/**
	 * This method is used to get all added favorite Designers from PO.com.
	 * 
	 * @return all Favorite Designers
	 */
	public List<String> getAllFavouriteDesigners();
	/**
	 * This method is used to get all added favorite Retailers from PO.com.
	 * 
	 * @return all Favorite Retailers
	 */
	public List<String> getAllFavouriteStores();
	/**
	 * This method is used to get all added favorite Offers from PO.com.
	 * 
	 * @return all Favorite Offers
	 */
	public List<String> getAllFavouriteOffers();
}
