
package com.simon.integration.dto.cart.cartestimate;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

/**
 * 
 * @author ssi249
 *
 */
public class RetailerRequestDTO extends PojoAnnotation {

	/**
	 * <i>Generated property</i> for <code>RetailerDTO.retailerID</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String retailerID;

	/**
	 * <i>Generated property</i> for <code>RetailerDTO.cartProducts</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private List<CartProductsDTO> cartProducts;

	/**
	 * <i>Generated property</i> for <code>RetailerDTO.shippingOptions</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private String shippingOptions;

	/**
	 * <i>Generated property</i> for <code>RetailerDTO.shippingDetails</code>
	 * property defined at extension <code>simonwebserviceintegration</code>.
	 */

	private ShippingDetailsDTO shippingDetails;

	public void setRetailerID(final String retailerID) {
		this.retailerID = retailerID;
	}

	public String getRetailerID() {
		return retailerID;
	}

	public void setCartProducts(final List<CartProductsDTO> cartProducts) {
		this.cartProducts = cartProducts;
	}

	public List<CartProductsDTO> getCartProducts() {
		return cartProducts;
	}

	public void setShippingOptions(final String shippingOptions) {
		this.shippingOptions = shippingOptions;
	}

	public String getShippingOptions() {
		return shippingOptions;
	}

	public void setShippingDetails(final ShippingDetailsDTO shippingDetails) {
		this.shippingDetails = shippingDetails;
	}

	public ShippingDetailsDTO getShippingDetails() {
		return shippingDetails;
	}

}
