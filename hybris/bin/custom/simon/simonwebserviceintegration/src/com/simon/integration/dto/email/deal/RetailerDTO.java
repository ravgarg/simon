package com.simon.integration.dto.email.deal;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import com.simon.integration.dto.email.deal.OfferDTO;



@XmlRootElement(name = "retailer")
@XmlAccessorType (XmlAccessType.FIELD)
public class RetailerDTO{

	@XmlAttribute(name = "name")
	private String name;
	
	@XmlAttribute(name = "logo")
	private String logo;
	
	@XmlAttribute(name = "siteURL")
	private String siteURL;
	
	@XmlElement(name = "offer")
	private OfferDTO offer;

	public void setSiteURL(String siteURL) {
		this.siteURL = siteURL;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param logo the logo to set
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}
	/**
	 * @param offer the offer to set
	 */
	public void setOffer(OfferDTO offer) {
		this.offer = offer;
	}
	
}
