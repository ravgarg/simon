package com.simon.integration.dto.email.order;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "item")
public class EmailOrderProductDTO {
	
	@XmlAttribute(name = "img")
	private String img;
	
	@XmlAttribute(name = "description")
	private String description;
	
	@XmlAttribute(name = "price")
	private String price;
	
	@XmlAttribute(name = "size")
	private String size;
	
	@XmlAttribute(name = "qty")
	private String quantity;
	
	@XmlAttribute(name = "status")
	private String status;
	
	@XmlAttribute(name = "subtotal")
	private String subtotal;
	
	@XmlAttribute(name = "color")
	private String color;
	
	@XmlAttribute(name = "promo")
	private String promo;

	/**
	 * @param img the img to set
	 */
	public void setImg(String img) {
		this.img = img;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @param subtotal the subtotal to set
	 */
	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @param promo the promo to set
	 */
	public void setPromo(String promo) {
		this.promo = promo;
	}
	
	
}
