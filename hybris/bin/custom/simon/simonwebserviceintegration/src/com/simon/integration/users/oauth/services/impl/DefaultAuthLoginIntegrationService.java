package com.simon.integration.users.oauth.services.impl;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.users.login.dto.UserLoginRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationEnhancedService;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationService;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;


/**
 * The class DefaultAuthLoginIntegrationService
 *
 */
public class DefaultAuthLoginIntegrationService implements AuthLoginIntegrationService
{
	@Resource
	private AuthLoginIntegrationEnhancedService authLoginIntegrationEnhancedService;
	@Resource(name = "sessionService")
	private SessionService sessionService;
	@Resource
	private ConfigurationService configurationService;
	private boolean esbPoAPIDisabled=false;
	
	/**
	 * This method is used to generate Auth token for a particular user with corresponding details in {@link UserAuthRequestDTO} and returns true if it is generated successfully else false.
	 * @param username
	 * @param password
	 * @return boolean
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public boolean getOAuthToken(String username,String password) throws AuthLoginIntegrationException
	{
		esbPoAPIDisabled=configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false);
		final UserAuthRequestDTO userAuthRequestDTO=new UserAuthRequestDTO();			
		userAuthRequestDTO.setUsername(StringUtils.lowerCase(username));
		userAuthRequestDTO.setPassword(password);
			if(!esbPoAPIDisabled){
				final UserAuthResponseDTO authResponse = authLoginIntegrationEnhancedService.getOAuthToken(userAuthRequestDTO);
				sessionService.setAttribute(SimonIntegrationConstants.AUTH_TOKEN, authResponse.getAccessToken());	
			}
			else{
				sessionService.setAttribute(SimonIntegrationConstants.AUTH_TOKEN, SimonIntegrationConstants.DUMMY_TOKEN);
			}
			return true;
	}
	
	/**
	 * This method is used to generate Auth token while registering  and returns true if it is generated successfully else false.
	 *
	 * @return boolean
	 * @throws AuthLoginIntegrationException 
	 */
	@Override
	public boolean generateAndSaveOAuthTokenForRegistration() throws AuthLoginIntegrationException
	{
		esbPoAPIDisabled=configurationService.getConfiguration().getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false);
			if(!esbPoAPIDisabled){
				final UserAuthResponseDTO authResponse = authLoginIntegrationEnhancedService.getOAuthTokenForRegisteration();
				sessionService.setAttribute(SimonIntegrationConstants.AUTH_TOKEN, authResponse.getAccessToken());
			}
			else{
				sessionService.setAttribute(SimonIntegrationConstants.AUTH_TOKEN, SimonIntegrationConstants.DUMMY_TOKEN);
			}
			return true;
	}
	
	/**
	 * This method validates with PO.com for Login with user credentials(email and password) and then parse response in
	 * UserLoginResponseDTO.
	 * @param email
	 * @param password
	 * @return {@link VIPUserDTO}
	 * @throws AuthLoginIntegrationException 
	 * @throws TokenExpiredException 
	 */
	@Override
	public VIPUserDTO validateLogin(String email,String password) throws AuthLoginIntegrationException
	{
		final UserLoginRequestDTO userLoginRequestDTO=new UserLoginRequestDTO();
		userLoginRequestDTO.setEmailaddress(StringUtils.lowerCase(email));
		userLoginRequestDTO.setPassword(password);
		return authLoginIntegrationEnhancedService.validateLogin(userLoginRequestDTO);
	}
}
