package com.simon.integration.users.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This a DTO class used to for user payment address.
 *
 */
public class UserPaymentAddressDTO extends UserAddressDTO {

	private String cardLastFourDigits;
	private String cardType;
	private String paymentToken;

	/**
	 * @return the cardLastFourDigits
	 */
	public String getCardLastFourDigits() {
		return cardLastFourDigits;
	}

	/**
	 * @param cardLastFourDigits
	 *            the cardLastFourDigits to set
	 */
	@JsonProperty("last4")
	public void setCardLastFourDigits(String cardLastFourDigits) {
		this.cardLastFourDigits = cardLastFourDigits;
	}

	/**
	 * @return the cardType
	 */
	public String getCardType() {
		return cardType;
	}

	/**
	 * @param cardType
	 *            the cardType to set
	 */
	@JsonProperty("cardtype")
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	/**
	 * @return the paymentToken
	 */
	public String getPaymentToken() {
		return paymentToken;
	}

	/**
	 * @param paymentToken
	 *            the paymentToken to set
	 */
	@JsonProperty("token")
	public void setPaymentToken(String paymentToken) {
		this.paymentToken = paymentToken;
	}

}
