package com.simon.integration.dto.order.deliver;

import java.util.List;

import com.simon.integration.dto.PojoAnnotation;

public class DeliverOrderRequestDTO extends PojoAnnotation{
	
	private String cartId;
	private String orderId;
	private List<RetailerDTO> retailers;
	private String paymentMethodToken;
	private boolean removeOOS;
	
	public boolean isRemoveOOS() {
		return removeOOS;
	}
	public void setRemoveOOS(boolean removeOOS) {
		this.removeOOS = removeOOS;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public List<RetailerDTO> getRetailers() {
		return retailers;
	}
	public void setRetailers(List<RetailerDTO> retailers) {
		this.retailers = retailers;
	}
	public String getPaymentMethodToken() {
		return paymentMethodToken;
	}
	public void setPaymentMethodToken(String paymentMethodToken) {
		this.paymentMethodToken = paymentMethodToken;
	}
	
}
