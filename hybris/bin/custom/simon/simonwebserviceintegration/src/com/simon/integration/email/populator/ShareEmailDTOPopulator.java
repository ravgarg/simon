package com.simon.integration.email.populator;

import java.io.StringWriter;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.deal.EmailOfferDTO;
import com.simon.integration.dto.email.product.ShareEmailAttributesDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

public class ShareEmailDTOPopulator implements Populator<ShareEmailData, ShareEmailRequestDTO> {
	private static final Logger LOG = LoggerFactory.getLogger(ShareEmailDTOPopulator.class);

	@Resource
	private Converter<ShareEmailData, EmailOfferDTO> offerDTOConverter;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;

	@Override
	public void populate(ShareEmailData source, ShareEmailRequestDTO target) {
		target.setCustomerKey(configurationService.getConfiguration().getString("share.offer.email.customer.key"));
		target.setSubscriberDetails(shareEmailIntegrationUtils.populateShareSubscriberDetails(source));
		final ShareEmailAttributesDTO emailAttributes = new ShareEmailAttributesDTO();
		emailAttributes.setSubjectLine(source.getSubject());

		emailAttributes
				.setXmlType(configurationService.getConfiguration().getString("share.product.offer.email.xml.type"));
		LOG.debug("Converting offerDTO");
		final EmailOfferDTO emailOfferDto = offerDTOConverter.convert(source);
		emailAttributes.setXmlData(populateEmailDataInXml(emailOfferDto));
		target.setSubscriptionDate(source.getSubscriptionDate());
		target.setEmailAttributes(emailAttributes);

	}

	/**
	 * @description Method use to convert the DTO to String
	 * @method populateDataInXml
	 * @param emailOfferDto
	 * @return String
	 */
	protected String populateEmailDataInXml(EmailOfferDTO emailOfferDto) {
		String escapedResult = StringUtils.EMPTY;
		final StringWriter stringWriter = new StringWriter();
		try {
			final JAXBContext context = JAXBContext.newInstance(EmailOfferDTO.class);
			final Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(emailOfferDto, stringWriter);
			escapedResult = shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter);
			LOG.info("Shared Deal email data :{}", escapedResult);
		} catch (JAXBException exception) {
			LOG.error("Exception occured while convert DTO object in the xml and return as String {} ", exception);
		}
		return escapedResult;
	}

}
