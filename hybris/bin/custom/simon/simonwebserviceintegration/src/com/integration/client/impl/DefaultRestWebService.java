package com.integration.client.impl;

import java.io.File;
import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.integration.client.RestClient;
import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.PojoAnnotation;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Base64;

/**
 * This is the implementation class of {@link RestWebService} used for executing Rest Event.
 */
public class DefaultRestWebService implements RestWebService
{

    private static final Logger LOG = LoggerFactory.getLogger(DefaultRestWebService.class);

    private static final String AUTHORIZATION = "Authorization";

    @Resource
    private RestClient restClient;

    @Resource
    private ConfigurationService configurationService;

    /**
     * This method execute the Rest Request event associated with the {@link DefaultRestWebService} and parameter
     * including end point URL,Request POJO,Target Class, Header Map,RestAuthHeaderDTO.
     *
     * @param urlPath The End point URL to be targeted in order to achieve response object
     * @param input Input request object which is a POJO
     * @param targetClass Class instance of response type
     * @param requestType Request Method Type in order to decide whether it is GET,POST etc.
     * @param headers Map containing exact Key and Value required for rest header
     * @param restAuthHeaderDTO Rest Authentication DTO containing information like Username and password
     * @param StubCall Flag to decide mocking should be enabled or not
     * @param Stub File Name Stub file name for mock response
     * @throws IntegrationException if a problem is encountered during execution of Rest Event
     * @return Response Object
     */
    @Override
    public <T> T executeRestEvent(final String uri, final PojoAnnotation input, final String stubCall, final String stubFileName, final Class<T> targetClass,
        final RequestMethod requestType, final MultiValueMap<String, Object> headers, RestAuthHeaderDTO restAuthHeaderDTO) throws IntegrationException
    {
        try
        {
            if (null != restAuthHeaderDTO)
            {
                validateAuthentication(headers, restAuthHeaderDTO);
            }

            if ("true".equals(stubCall))
            {
                return createRestMockResponse(stubFileName, targetClass);
            }
            else if (requestType == RequestMethod.GET)
            {
                return restClient.executeRestGetRequest(uri, input, targetClass, headers);
            }
            else if (requestType == RequestMethod.POST)
            {
                return restClient.executeRestPostRequest(uri, input, targetClass, headers);
            }
            else if (requestType == RequestMethod.DELETE)
            {
                return restClient.executeRestDeleteRequest(uri, input, targetClass, headers);
            }
            else
            {
                throw new IntegrationException("Unsupported Request Method");
            }
        }
        catch (final IOException exception)
        {
            LOG.error("Exception occured while mocking third party service", exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause(), "");
        }
        catch(final RestClientException restClientEx)
        {
        	LOG.error("RestClientException occured while calling third party service", restClientEx);
        	throw new IntegrationException(restClientEx.getMessage(), restClientEx.getCause(), "");
        }        
        catch (final Exception exception)
        {
            LOG.error("Exception occured while calling third party service", exception);
            throw new IntegrationException(exception.getMessage(), exception.getCause(), "");
        }
    }

    /**
     * This method validates and add Authentication headers
     *
     * @param headers Map containing exact Key and Value required for rest header
     * @param restAuthHeaderDTO Rest Authentication DTO containing information like Username and password
     */
    private void validateAuthentication(MultiValueMap<String, Object> headers, RestAuthHeaderDTO restAuthHeaderDTO)
    {
        if (SimonIntegrationConstants.BASIC.equals(ObjectUtils.toString(restAuthHeaderDTO.getAuthType()))
            && (StringUtils.isEmpty(restAuthHeaderDTO.getUserName()) || StringUtils.isEmpty(restAuthHeaderDTO.getPassword())))
        {
            throw new IntegrationException("Username and Password both required for Basic authentication Type");
        }
        headers.add(AUTHORIZATION, webMethodAuthentication(restAuthHeaderDTO.getUserName(), restAuthHeaderDTO.getPassword()));
    }
    /**
     * This method create string to for Base authentication to add it in request header.
     *
     * @return Header Value for Authorization
     */
    private String webMethodAuthentication(String userName, String password)
    {
        final String authString = userName + ":" + password;
        final String authStringEnc = Base64.encodeBytes(authString.getBytes());
        return SimonIntegrationConstants.BASIC + authStringEnc.replaceAll("\n", "");
    }

    /**
     * This method create mock reponse for request header.
     *
     * @return mock object
     */
    private <T> T createRestMockResponse(final String stubFileName, final Class targetClass) throws IOException
    {

        final String fileName = stubFileName;
        try
        {
            final ObjectMapper mapper = new ObjectMapper();
            final ClassLoader classLoader = getClass().getClassLoader();
            return (T) mapper.readValue(new File(classLoader.getResource(fileName).getFile()), targetClass);
        }
        catch (final IOException exception)
        {
            LOG.error("Error during in Parsing of json to mock third party service", exception);
            throw exception;
        }

    }
}
