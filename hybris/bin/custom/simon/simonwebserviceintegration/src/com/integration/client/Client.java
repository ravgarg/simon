package com.integration.client;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.ws.client.core.WebServiceMessageCallback;


/**
 * Interface for Client
 */
public abstract interface Client
{

	/**
	 * @param uri,
	 *           input,reqestCallBack
	 *
	 *           This marshal input object to xml and send call call to service url then unmarshal response from XMl to
	 *           java object.
	 */
	Object executeServiceRequest(String uri, Object requestEntity, WebServiceMessageCallback requestCallback)
			throws IOException, JAXBException;

	/**
	 * To execute the RestRequest
	 *
	 * @param xclientRefId
	 * @param urlPath
	 * @param requestEntity
	 * @param responseEntity
	 * @return Response
	 * @throws AbstractCisServiceException
	 */

	public abstract Object executeRestRequest(final String urlPath, Object requestEntity, Class responseEntity);

	/**
	 *
	 * @param xclientRefId
	 * @param urlPath
	 * @param responseEntity
	 * @return
	 */
	public abstract Object executeRestGetRequest(final String urlPath, Class responseEntity);

}
