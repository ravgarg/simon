package com.integration.client;

import java.io.IOException;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.impl.DefaultRestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.integration.dto.PojoAnnotation;

/**
 * This is the interface for Rest Web Services and will be used to execute Rest Event using different request type e.g.
 * GET ,POST etc.
 * <p>
 * This interface will be implemented by {@link DefaultRestWebService} for executing events through Rest Client
 * </p>
 */
public interface RestWebService
{
    /**
     * This method will be overridden to execute the Rest Request event associated with the
     * {@link DefaultRestWebService} and parameter including end point URL,Request POJO,Target Class, Header
     * Map,RestAuthHeaderDTO.
     *
     * @param urlPath The End point URL to be targeted in order to achieve response object
     * @param input Input request object which is a POJO
     * @param targetClass Class instance of response type
     * @param requestType Request Method Type in order to decide whether it is GET,POST etc.
     * @param headers Map containing exact Key and Value required for rest header
     * @param restAuthHeaderDTO Rest Authentication DTO containing information like Username and password
     * @param StubCall Flag to decide mocking should be enabled or not
     * @param Stub File Name Stub file name for mock response
     * @return Response Object
     * @throws IOException
     */
    <T> T executeRestEvent(final String uri, final PojoAnnotation input, final String stubCall, final String stubFileName, final Class<T> targetClass,
        final RequestMethod requestType, final MultiValueMap<String, Object> headers, RestAuthHeaderDTO restAuthHeaderDTO);

}
