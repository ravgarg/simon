package com.integration.client;

import org.springframework.util.MultiValueMap;

import com.integration.client.impl.DefaultRestClient;
import com.simon.integration.dto.PojoAnnotation;

/**
 * This is the interface for Rest Client and will be used to make calls using different request type
 * e.g. GET ,POST etc.
 *
 * <p>
 * This interface will be implemented by {@link DefaultRestClient} for executing Rest Calls 
 * using Rest Template
 * </p>
 *
 */
	public interface RestClient {
	
	/**
	 * This method will be overridden to execute the Rest Get Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	<T> T executeRestGetRequest(final String urlPath, final PojoAnnotation requestEntity, final Class<T> clazz,
			final MultiValueMap<String, Object> headers);

	/**
	 * This method will be overridden to execute the Rest Post Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	<T> T executeRestPostRequest(final String urlPath, PojoAnnotation requestEntity, Class<T> responseEntity,
			final MultiValueMap<String, Object> headers);
	/**
	 * This method will be overridden to execute the Rest Delete Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	<T> T executeRestDeleteRequest(String uri, PojoAnnotation input, Class<T> targetClass, MultiValueMap<String, Object> headers);
}
