package com.integration.client;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.ws.client.core.WebServiceMessageCallback;



public interface WebService
{
	public Object executeRestEvent(final String uri, final Object input, final String stubCall, final Class targetClass,
			final String stubFileName, final RequestMethod requestType) throws IOException;

	public Object executeSoapEvent(final String uri, final Object input, final String stubCall, final Class targetClass,
			final String stubFileName, final WebServiceMessageCallback requestCallback) throws Exception;

}

