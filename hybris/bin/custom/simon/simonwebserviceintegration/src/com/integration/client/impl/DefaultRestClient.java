package com.integration.client.impl;

import java.util.Arrays;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.integration.client.RestClient;
import com.simon.integration.dto.PojoAnnotation;

import de.hybris.platform.servicelayer.config.ConfigurationService;

/**
 * This is the implementation class of {@link RestClient} used
 * for executing Rest Call
 *
*/

public class DefaultRestClient implements RestClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRestClient.class);

	private static final String LOGGING_ENABLE_FLAG = "client.rest.logging.enable";
	private static final String LOGGING_MASK_ATTRIBUTES_VALUE_LIST_KEY = "security.integration.logging.mask.attributes.value";

	private RestTemplate restfulWebServiceTemplate;
	
	@Resource
	private ConfigurationService configurationService;
	
	/**
	 * This method execute the Rest Get Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	@Override
	public <T> T executeRestGetRequest(final String urlPath, final PojoAnnotation requestEntity, final Class<T> clazz,
			final MultiValueMap<String, Object> headers){
		final HttpEntity request = getHttpEntity(requestEntity,headers);
		if(null != requestEntity){
			logRequestResponse(request, requestEntity.getClass());
		}
		final ResponseEntity<T> responseEntity = restfulWebServiceTemplate.exchange(urlPath, HttpMethod.GET, request, clazz);
		final T response=responseEntity.getBody();
		logRequestResponse(response, clazz);
		return response;
	}
	/**
	 * This method execute the Rest DELETE Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	@Override
	public <T> T executeRestDeleteRequest(final String urlPath, final PojoAnnotation requestEntity, final Class<T> clazz,
			final MultiValueMap<String, Object> headers)
	{
		final HttpEntity request = getHttpEntity(requestEntity,headers);
		if(null != requestEntity){
			logRequestResponse(request, requestEntity.getClass());
		}
		final ResponseEntity<T> responseEntity = restfulWebServiceTemplate.exchange(urlPath, HttpMethod.DELETE, request, clazz);
		final T response=responseEntity.getBody();
		logRequestResponse(response, clazz);
		return response;
	}
	
	/**
	 * This method is used to return new instance of HttpEntity 
	 *
	 * return HttpEntity Object
	 * @param requestEntity 
	 * @param headers 
	 * @return HttpEntity
	 */
	public HttpEntity getHttpEntity(final Object requestEntity,final MultiValueMap<String, Object> headers)
	{
		return new HttpEntity(requestEntity,headers);
	}

	/**
	 * This method execute the Rest Post Request associated with the
	 * {@link DefaultRestClient} and parameter including end point URL,Request POJO,Target Class,Header Map
	 *
	 * @param urlPath
	 *           The product core attribute passed to it for which the value list is to be prepared
	 * @param requestEntity
	 *           Input request object which is a POJO
	 * @param clazz
	 *           Class instance of response type  
	 * @param headers
	 *           Map containing exact Key and Value required for rest header             
	 * @return Response Object
	 */
	@Override
	public <T> T executeRestPostRequest(final String urlPath, final PojoAnnotation requestEntity, final Class<T> clazz,
			final MultiValueMap<String, Object> headers) {
		final HttpEntity request =getHttpEntity(requestEntity,headers);
		if(requestEntity!=null){
			logRequestResponse(request, requestEntity.getClass());
		}
		final T response = restfulWebServiceTemplate.postForObject(urlPath, request, clazz);
		logRequestResponse(response, clazz);
		return response;
	}

	/**
	 * This method is used to log Rest request/response
	 *
	 * @param entity
	 */
	public void logRequestResponse(final Object entity, final Class<?> clazz) {
		try {
			if (LOGGER.isInfoEnabled()) { 
				final MappingJackson2HttpMessageConverter jacksonConverter = getMappingJackson2HttpMessageConverter();
				if (null != jacksonConverter && configurationService.getConfiguration().getBoolean(LOGGING_ENABLE_FLAG)) {
					String jsonString = jacksonConverter.getObjectMapper().writeValueAsString(entity);
					jsonString = maskSensitiveInformation(jsonString);
					LOGGER.info("LOGGING REQUEST RESPONSE JSON FOR " + clazz + " :: " + jsonString);
				}
			}
		} catch (final JsonProcessingException ex) {
			LOGGER.info("Error while parsing Json for logging" + ex);
		}
	}

	/**
	 * @return the jacksonConverter
	 */
	public MappingJackson2HttpMessageConverter getMappingJackson2HttpMessageConverter() {
		return (MappingJackson2HttpMessageConverter) restfulWebServiceTemplate.getMessageConverters().get(0);
	}

	/**
	 * @return the restfulWebServiceTemplate
	 */
	public RestTemplate getRestfulWebServiceTemplate() {
		return restfulWebServiceTemplate;
	}

	/**
	 * @param restfulWebServiceTemplate
	 *            the restfulWebServiceTemplate to set
	 */
	public void setRestfulWebServiceTemplate(final RestTemplate restfulWebServiceTemplate) {
		this.restfulWebServiceTemplate = restfulWebServiceTemplate;
	}

	/**
	 * This method is used to log Rest request/response
	 *
	 * @param entity
	 */
	private String maskSensitiveInformation(final String jsonString) {
		String localJsonString = jsonString;
		final String maskVariables = configurationService.getConfiguration().getString(LOGGING_MASK_ATTRIBUTES_VALUE_LIST_KEY);

		for (String maskVariable : Arrays.asList(maskVariables.split(","))) {
			maskVariable = "\"" + maskVariable + "\"";
			if (jsonString.toLowerCase().contains(maskVariable.toLowerCase())) {
				localJsonString = localJsonString.replaceAll(maskVariable + "[ ]*:[^,}\\]]*[,]?",
						 maskVariable + ":" + "\"" + "XXXXXXX" + "\",");
			}
		}
		return localJsonString;

	}

}
