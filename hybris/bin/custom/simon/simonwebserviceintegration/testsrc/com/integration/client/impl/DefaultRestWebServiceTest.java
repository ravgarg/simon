
package com.integration.client.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;

import com.integration.client.RestClient;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.AuthType;
import com.simon.integration.activemq.dto.MyTestPojo;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * Unit Test for DefaultRestWebService
 */

@UnitTest
public class DefaultRestWebServiceTest
{

	/** The DefaultRestWebService service implementation */
	@InjectMocks
	@Spy
	private DefaultRestWebService restWebService;
	@Mock
	private MyTestPojo myTestPojo;
	@Mock
	private MultiValueMap<String, Object> headers;
	@Mock
	private DeliverOrderRequestDTO response;
	@Mock
	private ResponseEntity<CreateCartConfirmationDTO> cartrResponse;
	@Mock
	private HttpEntity request;
	@Mock
	private RestAuthHeaderDTO restAuthHeaderDTO;
	@Mock
	private RestClient restClient;
	@Mock
	private DeliverOrderRequestDTO postResponse;
	@Mock
	private RestClientException restClientException;
	@Mock
	private IOException ioException;
	
	private String stubCall;
	
	private String stubFileName;
	
	private static final String URL = "https://abc.com";

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		
	}

	/**
	 * This method test execute Rest event for Get request
	 */
	@Test
	public void testExecuteRestEventForGetRequest() 
	{
		stubCall="true"; 
		stubFileName="stubs/getCartStatusServiceResponse.json";
		RequestMethod requestType=RequestMethod.GET;
		RestAuthHeaderDTO restAuthHeaderDTO=null;
		Object cartResponse = restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,CreateCartConfirmationDTO.class,requestType, headers,
				restAuthHeaderDTO);
		assertTrue(cartResponse instanceof CreateCartConfirmationDTO);
	}
	
	/**
	 * This method test execute Rest event for Get request
	 */
	@Test
	public void testExecuteRestEventForGetRequestWithAuth() 
	{
		RequestMethod requestType=RequestMethod.GET;
		RestAuthHeaderDTO restAuthHeaderDTO=new RestAuthHeaderDTO();
		restAuthHeaderDTO.setAuthType(AuthType.BASIC);
		restAuthHeaderDTO.setPassword("test");
		restAuthHeaderDTO.setUserName("test");
		restAuthHeaderDTO.setAuthType(AuthType.BASIC);
		restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO);
	}
	
	/**
	 * This method test execute Post event for Post request
	 */
	@Test
	public void testExecuteRestEventForPostRequest()
	{
		RequestMethod requestType=RequestMethod.POST;
		RestAuthHeaderDTO restAuthHeaderDTO=null;
		doReturn(response).when(restClient).executeRestPostRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers);
		Assert.assertEquals(restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO),response);
	}
	
	/**
	 * This method test IntegrationException
	 */
	@Test(expected=IntegrationException.class)
	public void testExecuteIntegrationException()
	{
		RequestMethod requestType=RequestMethod.HEAD;
		RestAuthHeaderDTO restAuthHeaderDTO=null;
		doReturn(response).when(restClient).executeRestPostRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers);
		restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO);
	}
	
	/**
	 * This method test RestClientException
	 */
	@Test(expected=IntegrationException.class)
	public void testExecuteRestClientException()
	{
		RequestMethod requestType=RequestMethod.POST;
		RestAuthHeaderDTO restAuthHeaderDTO=null; 
		when(restClient.executeRestPostRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers)).thenThrow(restClientException);
		restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO);
	}
	
	/**
	 * This method test IOException
	 */
	@Test(expected=IntegrationException.class)
	public void testExecuteIOException()
	{
		RequestMethod requestType=RequestMethod.POST;
		RestAuthHeaderDTO restAuthHeaderDTO=null;
		when(restClient.executeRestPostRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers)).thenThrow(IOException.class);
		restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO);
	}
	
	/**
	 * This method test execute Rest event for Delete request
	 */
	@Test
	public void testExecuteRestEventForDeleteRequest() 
	{
		RequestMethod requestType=RequestMethod.DELETE;
		RestAuthHeaderDTO restAuthHeaderDTO=null;
		when(restClient.executeRestDeleteRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers)).thenReturn(response);
		Assert.assertEquals(restWebService.executeRestEvent(URL, myTestPojo,stubCall,stubFileName,DeliverOrderRequestDTO.class,requestType, headers,
				restAuthHeaderDTO),response);
	}
}
