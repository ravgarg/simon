
package com.integration.client.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.integration.activemq.dto.MyTestPojo;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
/**
 * Unit Test for DefaultRestClient
 */

@UnitTest
public class DefaultRestClientTest {

	private final String INTEGRATION_VARIABLE_NEED_TO_MASK = "integration.variables.needto.mask";
	@InjectMocks
	@Spy	
	private DefaultRestClient defaultRestClient;
	@Mock
	private List<HttpMessageConverter<?>> messageConverters;
	@Mock
	private RestTemplate restfulWebServiceTemplate;
	@Mock
	private MyTestPojo myTestPojo;
	@Mock
	private Configuration configuration;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private HttpEntity request;
	@Mock
	private DeliverOrderRequestDTO response;
	@Mock
	private ObjectMapper objectMapper;
	@Mock
	private MultiValueMap<String, Object> headers;
	@Mock
	private ResponseEntity<DeliverOrderRequestDTO> responseEntity;
	@Mock
	private MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter;
	@Mock
	private UserAuthRequestDTO pOAuthRequestDTO;
	@Mock
	private UserAuthResponseDTO poAuthDTO;

	private static final String LOGGING_ENABLE_FLAG = "client.rest.logging.enable";
	private static final String LOGGING_MASK_ATTRIBUTES_VALUE_LIST_KEY = "security.integration.logging.mask.attributes.value";
	private static final String URL = "https://abc.com";

	@Before
	public void setUp() throws JsonProcessingException {
		MockitoAnnotations.initMocks(this);
	
		final List<MappingJackson2HttpMessageConverter> test = new ArrayList<>();
		test.add(mappingJackson2HttpMessageConverter);
		doReturn(test).when(restfulWebServiceTemplate).getMessageConverters();	
		when(mappingJackson2HttpMessageConverter.getObjectMapper()).thenReturn(objectMapper);
		when(objectMapper.writeValueAsString(request)).thenReturn("hello");
		when(configuration.getString(INTEGRATION_VARIABLE_NEED_TO_MASK)).thenReturn("hello");
		when(configurationService.getConfiguration()).thenReturn(configuration);
		doReturn(request).when(defaultRestClient).getHttpEntity(myTestPojo,headers);
	}

	/**
	 * This method is to test ExecuteRestGetRequest method for GET calls.
	 */
	@Test
	public void testExecuteRestGetRequest(){ 
		
		when(restfulWebServiceTemplate.exchange(URL, HttpMethod.GET, request, DeliverOrderRequestDTO.class)).thenReturn(responseEntity);
		when(responseEntity.getBody()).thenReturn(response);
		doReturn(response).when(responseEntity).getBody();
		Assert.assertEquals(defaultRestClient.executeRestGetRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers),
				response);
	}

	/**
	 * This method is to test ExecuteRestPostRequest method for POST calls.
	 */
	@Test
	public void testExecuteRestPostRequest(){

		when(restfulWebServiceTemplate.postForObject(URL, request, DeliverOrderRequestDTO.class)).thenReturn(response);
		Assert.assertEquals(defaultRestClient.executeRestPostRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers),
				response);
	}
	/**
	 * This method is to test ExecuteRestPostRequest method for POST calls.
	 */
	@Test
	public void testExecuteLogResponse(){

		when(configuration.getBoolean(LOGGING_ENABLE_FLAG)).thenReturn(Boolean.TRUE);
		when(configuration.getString(LOGGING_MASK_ATTRIBUTES_VALUE_LIST_KEY)).thenReturn("password,Authorization"); 
		defaultRestClient.logRequestResponse(request, DeliverOrderRequestDTO.class);
	}
	/**
	 * This method is to test ExecuteRestGetRequest method for GET calls.
	 */
	@Test
	public void testExecuteRestDeleteRequest(){ 
		when( defaultRestClient.getRestfulWebServiceTemplate().exchange(URL,HttpMethod.DELETE,request,DeliverOrderRequestDTO.class)).thenReturn(responseEntity);
		doReturn(response).when(responseEntity).getBody();
		Assert.assertEquals(defaultRestClient.executeRestDeleteRequest(URL, myTestPojo, DeliverOrderRequestDTO.class, headers),
				response);
	}
}
