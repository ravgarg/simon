package com.simon.integration.users.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UserDataPopulatorTest{

	@InjectMocks
	private UserDataPopulator userDataPopulator;
	@Mock
	private Converter<MallData, UserCenterIdDTO> userCenterIdDataConverter;
	@Mock
	private List<MallData> centerIdDatas;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Test
	public void updateUserDataPopulateTest1(){
		
		UserRegisterUpdateRequestDTO  updateUserDetailsDTO = mock(UserRegisterUpdateRequestDTO.class);
		CustomerData updateUserData =  mock(CustomerData.class);
		CountryData countryData =  mock(CountryData.class);
		countryData.setIsocode("US");
	
		updateUserData.setFirstName("First");
		updateUserData.setLastName("Last");
		updateUserData.setBirthYear(1987);
		updateUserData.setCountry(countryData.getIsocode());
		updateUserData.setEmailAddress("test@test.com");
		updateUserData.setGender(CustomerGender.MALE);
		updateUserData.setHouseholdIncomeCode(1);
		updateUserData.setPassword("password");
		updateUserData.setZipcode("123456");
		updateUserData.setOptedInEmail(true);
		
		when(updateUserData.getAlternateMalls()).thenReturn(centerIdDatas);
		when(updateUserData.getBirthYear()).thenReturn(1900);
		when(updateUserData.getHouseholdIncomeCode()).thenReturn(1);
		updateUserData.setAlternateMalls(centerIdDatas);
		updateUserData.setBirthMonth(Months.APR);
		when(updateUserData.getBirthMonth()).thenReturn(Months.APR);
		when(userIntegrationUtils.getBirthMonthCode(Months.APR.getCode())).thenReturn("4");
		when(updateUserData.getGender()).thenReturn(CustomerGender.MALE);
		when(userIntegrationUtils.getGenderCode(updateUserData.getGender().getCode())).thenReturn("1");
		final List<UserCenterIdDTO> userCenterIdDTOs=new ArrayList<>();
		UserCenterIdDTO userCenterIdDTO=new UserCenterIdDTO();
		userCenterIdDTO.setId("ID");
		userCenterIdDTO.setOutletName("outletName");
		userCenterIdDTOs.add(userCenterIdDTO);
		when(userCenterIdDataConverter.convertAll(centerIdDatas)).thenReturn(userCenterIdDTOs);
		when(updateUserDetailsDTO.getFirstName()).thenReturn("First");
		when(updateUserDetailsDTO.getLastName()).thenReturn("Last");
		userDataPopulator.populate(updateUserData, updateUserDetailsDTO);
		Assert.assertEquals("First", updateUserDetailsDTO.getFirstName());
		Assert.assertEquals("Last", updateUserDetailsDTO.getLastName());
	}
	
	@Test
	public void updateUserDataPopulateTest2(){
		UserRegisterUpdateRequestDTO  updateUserDetailsDTO = mock(UserRegisterUpdateRequestDTO.class);
		CustomerData updateUserData =  mock(CustomerData.class);
		CountryData countryData =  mock(CountryData.class);
		countryData.setIsocode("US");
		
		updateUserData.setFirstName("First");
		updateUserData.setLastName("Last");
		updateUserData.setBirthYear(1987);
		updateUserData.setCountry(countryData.getIsocode());
		updateUserData.setEmailAddress("test@test.com");
		updateUserData.setGender(CustomerGender.FEMALE);
		updateUserData.setHouseholdIncomeCode(1);
		updateUserData.setPassword("password");
		updateUserData.setZipcode("123456");
		when(updateUserData.getBirthYear()).thenReturn(1900);
		when(updateUserData.getHouseholdIncomeCode()).thenReturn(1);
		updateUserData.setAlternateMalls(centerIdDatas);
		updateUserData.setBirthMonth(Months.APR);
		updateUserData.setOptedInEmail(true);
		
		when(updateUserData.getBirthMonth()).thenReturn(Months.APR);
		when(userIntegrationUtils.getBirthMonthCode(Months.APR.getCode())).thenReturn("4");
		final MallData primaryMall= mock(MallData.class);
		when(updateUserData.getPrimaryMall()).thenReturn(primaryMall);
		when(updateUserData.getOldPassword()).thenReturn("oldPassword");
		when(updateUserData.getCountry()).thenReturn("US");
		CountryModel country=new CountryModel();
		country.setExternalId("ID");
		when(commonI18NService.getCountry("US")).thenReturn(country);
		UserCenterIdDTO userCenterId=new UserCenterIdDTO();
		when(userCenterIdDataConverter.convert(primaryMall)).thenReturn(userCenterId);
		final List<UserCenterIdDTO> userCenterIdDTOs=new ArrayList<>();
		UserCenterIdDTO userCenterIdDTO=new UserCenterIdDTO();
		userCenterIdDTO.setId("ID");
		userCenterIdDTO.setOutletName("outletName");
		userCenterIdDTOs.add(userCenterIdDTO);
		when(userCenterIdDataConverter.convertAll(centerIdDatas)).thenReturn(userCenterIdDTOs);
		when(updateUserDetailsDTO.getFirstName()).thenReturn("First");
		when(updateUserDetailsDTO.getLastName()).thenReturn("Last");
		userDataPopulator.populate(updateUserData, updateUserDetailsDTO);
		Assert.assertEquals("First", updateUserDetailsDTO.getFirstName());
		Assert.assertEquals("Last", updateUserDetailsDTO.getLastName());
	}
	
	@Test
	public void updateUserDataPopulateTest3(){
		UserRegisterUpdateRequestDTO  updateUserDetailsDTO = mock(UserRegisterUpdateRequestDTO.class);
		CustomerData updateUserData =  mock(CustomerData.class);
		CountryData countryData =  mock(CountryData.class);
		countryData.setIsocode("US");
		
		updateUserData.setFirstName("First");
		updateUserData.setLastName("Last");
		updateUserData.setBirthYear(1987);
		updateUserData.setCountry(countryData.getIsocode());
		updateUserData.setEmailAddress("test@test.com");
		updateUserData.setGender(CustomerGender.NOT_DISCLOSED);
		updateUserData.setHouseholdIncomeCode(1);
		updateUserData.setPassword("password");
		updateUserData.setZipcode("123456");
		when(updateUserData.getFirstName()).thenReturn("First");
		when(updateUserData.getLastName()).thenReturn("Last");
		when(updateUserData.getBirthYear()).thenReturn(1987);
		when(updateUserData.getEmailAddress()).thenReturn("test@test.com");
		when(updateUserData.getGender()).thenReturn(CustomerGender.NOT_DISCLOSED);
		when(updateUserData.getZipcode()).thenReturn("123456");
		when(updateUserData.getAlternateMalls()).thenReturn(centerIdDatas);
		when(updateUserData.getBirthYear()).thenReturn(1900);
		when(updateUserData.getHouseholdIncomeCode()).thenReturn(1);
		when(updateUserData.getPassword()).thenReturn("password");
		when(updateUserData.getMobileNumber()).thenReturn("1234567681");
		updateUserData.setAlternateMalls(centerIdDatas);
		updateUserData.setBirthMonth(Months.APR);
		updateUserData.setOptedInEmail(true);
		
		when(updateUserData.getBirthMonth()).thenReturn(Months.APR);
		when(userIntegrationUtils.getBirthMonthCode(Months.APR.getCode())).thenReturn("4");
		when(updateUserData.getGender()).thenReturn(CustomerGender.NOT_DISCLOSED);
		when(userIntegrationUtils.getGenderCode(updateUserData.getGender().getCode())).thenReturn("0");
		final List<UserCenterIdDTO> userCenterIdDTOs=new ArrayList<>();
		UserCenterIdDTO userCenterIdDTO=new UserCenterIdDTO();
		userCenterIdDTO.setId("ID");
		userCenterIdDTO.setOutletName("outletName");
		userCenterIdDTOs.add(userCenterIdDTO);
		when(userCenterIdDataConverter.convertAll(centerIdDatas)).thenReturn(userCenterIdDTOs);
		when(updateUserDetailsDTO.getFirstName()).thenReturn("First");
		when(updateUserDetailsDTO.getLastName()).thenReturn("Last");
		userDataPopulator.populate(updateUserData, updateUserDetailsDTO);
		Assert.assertEquals("First", updateUserDetailsDTO.getFirstName());
		Assert.assertEquals("Last", updateUserDetailsDTO.getLastName());
	}
	
	/**
	 * Test method to test Null check in populate method. 
	 */
	@Test
	public void updateUserDataPopulateForEmpty(){
		UserRegisterUpdateRequestDTO  updateUserDetailsDTO = mock(UserRegisterUpdateRequestDTO.class);
		CustomerData updateUserData =  mock(CustomerData.class);
		CountryData countryData =  mock(CountryData.class);
		countryData.setIsocode("US");
		updateUserData.setCountry(countryData.getIsocode());
		updateUserData.setFirstName(null);
		updateUserData.setLastName(null);
		when(updateUserData.getFirstName()).thenReturn(null);
		when(updateUserData.getLastName()).thenReturn(null);
		when(updateUserData.getBirthYear()).thenReturn(1987);
		when(updateUserData.getEmailAddress()).thenReturn(null);
		when(updateUserData.getGender()).thenReturn(CustomerGender.NOT_DISCLOSED);
		when(updateUserData.getZipcode()).thenReturn(null);
		when(updateUserData.getAlternateMalls()).thenReturn(centerIdDatas);
		when(updateUserData.getBirthYear()).thenReturn(1900);
		when(updateUserData.getHouseholdIncomeCode()).thenReturn(1);
		when(updateUserData.getPassword()).thenReturn(null);
		
		when(updateUserData.getMobileNumber()).thenReturn("1234567681");
		when(updateUserData.getAlternateMalls()).thenReturn(centerIdDatas);
		when(updateUserData.getHouseholdIncomeCode()).thenReturn(1);
		updateUserData.setAlternateMalls(centerIdDatas);
		when(updateUserDetailsDTO.getFirstName()).thenReturn(StringUtils.EMPTY);
		when(updateUserDetailsDTO.getLastName()).thenReturn("Last");
		userDataPopulator.populate(updateUserData, updateUserDetailsDTO);
		Assert.assertEquals(StringUtils.EMPTY, updateUserDetailsDTO.getFirstName());
		
	}

}