package com.simon.integration.share.email.services.impl;


import static org.mockito.MockitoAnnotations.initMocks;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShareEmailIntegrationServiceTest {

	@InjectMocks
	private DefaultShareEmailIntegrationService shareEmailIntegrationService;
	
	@Mock
	private Converter<ShareEmailData,ShareEmailRequestDTO> shareEmailDTOConverter;
	
	@Mock
	private Converter<ShareEmailData, ShareEmailRequestDTO> sharedEmailProductDTOConverter;
	
	@Mock
	private DefaultShareEmailIntegrationEnhancedService shareEmailIntegrationEnhancedService;
	
	@Mock
	private ShareEmailRequestDTO sharedProductEmailRequestDTO;

	/**
	 * Init Mock
	 */
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	

	/**
	 * Test Share deal Email
	 * @throws SharedEmailIntegrationException
	 *
	 */
	@Test
	public void testshareDealEmail() throws SharedEmailIntegrationException
	{
		ShareEmailData shareEmailData=mock(ShareEmailData.class);
		ShareEmailRequestDTO emailREquestDto=mock(ShareEmailRequestDTO.class);
		when(shareEmailDTOConverter.convert(shareEmailData)).thenReturn(emailREquestDto);
		shareEmailIntegrationService.shareDealEmail(shareEmailData);
		Mockito.verify(shareEmailIntegrationEnhancedService).shareEmail(emailREquestDto);
		
	}
	@Test
	public void sharedProductEmailDataTest() throws SharedEmailIntegrationException{
		ShareEmailData sharedProductEmailData = mock(ShareEmailData.class);
		when(sharedEmailProductDTOConverter.convert(sharedProductEmailData)).thenReturn(sharedProductEmailRequestDTO);
		shareEmailIntegrationService.sharedProductEmailData(sharedProductEmailData);
		verify(shareEmailIntegrationEnhancedService).shareEmail(sharedProductEmailRequestDTO);
	}
}
