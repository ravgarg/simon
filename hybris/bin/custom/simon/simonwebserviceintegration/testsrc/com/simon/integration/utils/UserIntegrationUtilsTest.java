package com.simon.integration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.enums.CustomerGender;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.ResponseDTO;


import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

public class UserIntegrationUtilsTest
{
	
	private final String HEADER_URL = "headerValue";
	private final String BASE_API_END_POINT_URL = "baseApiEndPointUrl";
	private final String SERVICE_END_POINTAPI_URL_KEY = "serviceEndpointApiUrlKey";
	private final String ESB_SERVICE_END_POINT_API_URL = "esbServiceEndpointApiUrl";

	
	
	@InjectMocks
	UserIntegrationUtils userIntegrationUtils;
	
	@Mock
	private ConfigurationService configurationService;
	
	@Mock
	private Configuration configuration;
	@Mock
	private SessionService sessionService;
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_ADDRESS_EXPORT_EXTERNAL_ID_PREFIX)).thenReturn("envPrefix_");
	}
	
	@Test
	public void testGetBirthMonthCode()
	{
		assertEquals("1",userIntegrationUtils.getBirthMonthCode("Jan"));
	}

	@Test
	public void testGetBirthMonthFromCode()
	{
		assertEquals("Jan",userIntegrationUtils.getBirthMonthFromCode("1"));
	}

	@Test
	public void testGetGenderCodeWhenMale()
	{
		assertEquals("1",userIntegrationUtils.getGenderCode("Male"));
	}
	
	@Test
	public void testGetGenderCodeWhenFeMale()
	{
		assertEquals("2",userIntegrationUtils.getGenderCode("Female"));
	}
	
	@Test
	public void testGetGenderCodeWhenOthers()
	{
		assertEquals("0",userIntegrationUtils.getGenderCode("Others"));
	}
	
	@Test
	public void testGetGenderCodeForMale()
	{
		assertEquals(CustomerGender.MALE,userIntegrationUtils.getGenderFromCode("1"));
	}
	
	@Test
	public void testGetGenderCodeForFamle()
	{
		assertEquals(CustomerGender.FEMALE,userIntegrationUtils.getGenderFromCode("2"));
	}
	@Test
	public void testGetGenderCodeForOther()
	{
		assertEquals(CustomerGender.NOT_DISCLOSED,userIntegrationUtils.getGenderFromCode("Other"));
	}
	
	@Test
	public void buildFinalIntegrationServiceUrlTest(){
		
		String endPointUrl =  "addAddress/";
		String finalUrl = "https://testapi.baseUrl/addAddress/";
		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(Mockito.any())).thenReturn(endPointUrl);
		
	//	userIntegrationUtils.buildFinalUrlByAppendingToTargetUrl("test");
		Assert.assertEquals(finalUrl, "https://testapi.baseUrl/addAddress/");
		
	}
	
	
	@Test
	public void buildIntegrationServiceBaseApiUrlTest() {

		String baseUrl = new String("https://testapi.baseUrl");
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(Mockito.any())).thenReturn(baseUrl);
	//	userIntegrationUtils.getURLFromConfiguration();

	}
	
	@Test(expected = IllegalStateException.class)
	public void validateStateTest() {

		userIntegrationUtils.validateState(false, "hello");
	}
	
	@Test
	public void validateStateTrueTest() {

		userIntegrationUtils.validateState(true, "hello");
	}
	
	/**
	 * Test method for
	 * {@link com.simon.integration.utils.UserIntegrationUtils.addEnvPrefixToAddressId#editUserAddress(java.lang.String)}.
	 */
	@Test
	public void testAddEnvPrefixToAddressId_Blank_Case()
	{
		final String addressId ="";
		
		String actualAddressId = userIntegrationUtils.addEnvPrefixToAddressId(addressId);
		
		Assert.assertEquals(StringUtils.EMPTY, actualAddressId);
	}
	
	/**
	 * Test method for
	 * {@link com.simon.integration.utils.UserIntegrationUtils.addEnvPrefixToAddressId#editUserAddress(java.lang.String)}.
	 */
	@Test
	public void testAddEnvPrefixToAddressId_Valid_Case()
	{
		final String addressId ="testAddressId";
		
		String actualAddressId = userIntegrationUtils.addEnvPrefixToAddressId(addressId);
		
		Assert.assertEquals("envPrefix_testAddressId", actualAddressId);
	}
	
	@Test
	public void testGetCreditCardCode()
	{
		assertEquals("1",userIntegrationUtils.getCreditCardCode("visa"));
	}
	
	@Test
	public void testGetCreditCardCodeNull()
	{
		assertEquals(null,userIntegrationUtils.getCreditCardCode(null));
	}
	
	@Test
	public void getEsbIntegrationServiceHeaderAuthorizationValueTest(){
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ESB_INTEGRATION_SERVICE_HEADER_AUTHORIZATION_VALUE)).thenReturn(HEADER_URL);
		Assert.assertNotNull(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue());
	}
	
	@Test
	public void buildEsbLayerIntegrationServiceEndpointApiUrlTest(){
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ESB_LAYER_SERVICE_BASE_API_URL)).thenReturn(BASE_API_END_POINT_URL);
		when(configuration.getString(SERVICE_END_POINTAPI_URL_KEY)).thenReturn(ESB_SERVICE_END_POINT_API_URL);
		Assert.assertNotNull(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SERVICE_END_POINTAPI_URL_KEY));
	}
	
	@Test
	public void populateAuthHeaderForESBTest(){
		when(configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_USER_ID)).thenReturn("ID");
		when(configurationService.getConfiguration().getString(SimonIntegrationConstants.ESB_USER_PWRD)).thenReturn("PWRD");
		Assert.assertEquals("ID", userIntegrationUtils.populateAuthHeaderForESB().getUserName());
	}
	
	@Test
	public void populateHeadersForESBTest(){
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn("token");
		Assert.assertTrue(userIntegrationUtils.populateHeadersForESB().get(SimonIntegrationConstants.PO_TOKEN).contains("token" ));
	}
	
	@Test
	public void handleCommonErrorsTestWithCode725()
	{	ResponseDTO response=new ResponseDTO();
		response.setErrorCode("725");
		userIntegrationUtils.handleCommonErrors(response);
		Assert.assertEquals("725" , response.getErrorCode());
	}
	
}
