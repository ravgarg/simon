package com.simon.integration.users.oauth.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import com.fasterxml.jackson.core.JsonParseException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.util.MultiValueMap;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.users.login.dto.UserLoginRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
import com.simon.integration.users.oauth.services.impl.DefaultAuthLoginIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
@UnitTest
public class DefaultAuthLoginIntegrationEnhancedServiceTest
{
	@InjectMocks
	DefaultAuthLoginIntegrationEnhancedService authLoginIntegrationEnhancedService;
	@Mock
	RestWebService restWebService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private RestAuthHeaderDTO authheader;
	@Mock
	private MultiValueMap<String, Object> headers;
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_SERVICE_URL)).thenReturn("url");
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(authheader);
		when(userIntegrationUtils.populateHeadersForESB()).thenReturn(headers);
	}

	@Test
	public void testGetOAuthToken() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		assertEquals(response, authLoginIntegrationEnhancedService.getOAuthToken(pOAuthRequestDTO));
	}
	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testGetOAuthTokenWithException() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenThrow(new IntegrationException(""));
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		authLoginIntegrationEnhancedService.getOAuthToken(pOAuthRequestDTO);
	}
	
	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testGetOAuthTokenWithErrorCode() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		response.setErrorCode("112");
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		authLoginIntegrationEnhancedService.getOAuthToken(pOAuthRequestDTO);
	}
	
	
	@Test
	public void testGetOAuthTokenForRegisteration() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		assertEquals(response, authLoginIntegrationEnhancedService.getOAuthTokenForRegisteration());
	}
	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testGetOAuthTokenForRegisterationWithException() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenThrow(new IntegrationException(""));
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		 authLoginIntegrationEnhancedService.getOAuthTokenForRegisteration();
	}
	
	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testGetOAuthTokenForRegisterationWithErrorCode() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		response.setErrorCode("112");
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		UserAuthRequestDTO pOAuthRequestDTO=new UserAuthRequestDTO();
		authLoginIntegrationEnhancedService.getOAuthTokenForRegisteration();
	}
	@Test
	public void testValidateLogin() throws AuthLoginIntegrationException
	{
		UserLoginRequestDTO poLoginRequestDTO=new UserLoginRequestDTO();
		VIPUserDTO response=new VIPUserDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		assertEquals(response, authLoginIntegrationEnhancedService.validateLogin(poLoginRequestDTO));
	}

	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testValidateLoginWithInvalidCredentials() throws AuthLoginIntegrationException
	{
		UserLoginRequestDTO poLoginRequestDTO=new UserLoginRequestDTO();
		VIPUserDTO response=new VIPUserDTO();
		response.setErrorCode("111");
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(response);
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenReturn(response);
		authLoginIntegrationEnhancedService.validateLogin(poLoginRequestDTO);
	}
	
	@Test(expected=AuthLoginIntegrationException.class)
	public void testValidateLoginWithExceptionWhileFetchingResponse() throws AuthLoginIntegrationException
	{
		UserLoginRequestDTO poLoginRequestDTO=new UserLoginRequestDTO();
		when(restWebService.executeRestEvent(Matchers.anyString(), Matchers.any(), Matchers.any(), Matchers.any(),
				Matchers.any(), Matchers.any(),Matchers.any(), Matchers.any())).thenThrow(new IntegrationException("Exception"));
		authLoginIntegrationEnhancedService.validateLogin(poLoginRequestDTO);
	}
}
