package com.simon.integration.payment.services.impl;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;


import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.exceptions.IntegrationException;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.generated.model.ErrorLogModel;
import com.simon.integration.dto.card.verification.CardVerificationRequestDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.payment.services.CardPaymentIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

@UnitTest
public class DefaultCardPaymentIntegrationServiceTest {

	@InjectMocks
	@Spy
	DefaultCardPaymentIntegrationService defaultCardPaymentIntegrationService;

	@Mock
	private CardVerificationRequestDTO cardVerificationRequestDTO;
	@Mock
	private CardVerificationResponseDTO cardVerificationResponseDTO;
	@Mock
	private CardPaymentIntegrationEnhancedService cardPaymentIntegrationEnhancedService;
	@Mock
	private IntegrationException exception;
	@Mock
	private OrderModel orderModel;
	@Mock
	private DeliverOrderRequestDTO deliverOrderRequestDTO;
	@Mock
	private Converter<OrderModel, DeliverOrderRequestDTO> deliverOrderDataConverter;
	@Mock
	private Converter<Pair<OrderModel, List<VariantAttributeMappingModel>>, DeliverOrderRequestDTO> deliverOrderDataVariantAttributeConverter;
	
	@Mock
	private Converter<Pair<OrderModel, Exception>, ErrorLogModel> errorLogConverter;
	@Mock
	private ModelService modelService;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	/**
	 * This method test InvokeCardVerification method
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testInvokeCardVerification() throws CartCheckOutIntegrationException {
		String paymentMethodToken = "123";
		Mockito.when(cardPaymentIntegrationEnhancedService.verifyCard(Mockito.any()))
				.thenReturn(cardVerificationResponseDTO);
		Assert.assertEquals(defaultCardPaymentIntegrationService.invokeCardVerification(paymentMethodToken,true,true),
				cardVerificationResponseDTO);
	}

	@Test(expected = CartCheckOutIntegrationException.class)
	public void testInvokeCardVerificationException() throws CartCheckOutIntegrationException {
		String paymentMethodToken = "123";
		when(cardPaymentIntegrationEnhancedService.verifyCard(Mockito.any())).thenThrow(exception);
		defaultCardPaymentIntegrationService.invokeCardVerification(paymentMethodToken,true,true);
	}

	/**
	 * This method test Invoke Deliver Order method
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testInvokeDeliverOrder() throws CartCheckOutIntegrationException {
		Mockito.when(deliverOrderDataConverter.convert(orderModel)).thenReturn(deliverOrderRequestDTO);
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributeMapping=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributeMapping);
		Mockito.when(deliverOrderDataVariantAttributeConverter.convert(ImmutablePair.of(orderModel, variantAttributeMappingList), deliverOrderRequestDTO)).thenReturn(deliverOrderRequestDTO);
		defaultCardPaymentIntegrationService.invokeDeliverOrder(orderModel, "test", variantAttributeMappingList);
		
		Mockito.verify(cardPaymentIntegrationEnhancedService).deliverOrder(deliverOrderRequestDTO);
	}

	@Test(expected = CartCheckOutIntegrationException.class)
	public void testInvokeDeliverOrderException() throws CartCheckOutIntegrationException {
		Mockito.when(deliverOrderDataConverter.convert(orderModel)).thenReturn(deliverOrderRequestDTO);
		Mockito.doThrow(new IntegrationException("", "")).when(cardPaymentIntegrationEnhancedService)
				.deliverOrder(deliverOrderRequestDTO);
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributeMapping=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributeMapping);
		Mockito.when(deliverOrderDataVariantAttributeConverter.convert(ImmutablePair.of(orderModel, variantAttributeMappingList), deliverOrderRequestDTO)).thenReturn(deliverOrderRequestDTO);
		defaultCardPaymentIntegrationService.invokeDeliverOrder(orderModel,"test", variantAttributeMappingList);
	}
	
	
}