package com.simon.integration.users.address.verify.services.impl;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.simon.core.exceptions.IntegrationException;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.integration.users.address.verify.services.UserAddressVerifyIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
public class DefaultUserAddressVerifyIntegrationServiceTest {

	@InjectMocks
	@Spy	
	private DefaultUserAddressVerifyIntegrationService defaultUserAddressVerifyIntegrationService;
	@Mock
	private UserAddressVerifyIntegrationEnhancedService userAddressVerifyIntegrationEnhancedService;
	@Mock
	private Converter<ExtAddressData, UserAddressVerifyRequestDTO> userAddressVerifyRequestDataConverter;
	
	
	@Before
	public void setUp() throws JsonProcessingException {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testValidateAddress() throws UserAddressVerifyIntegrationException{ 
		ExtAddressData addressData = mock(ExtAddressData.class);
		UserAddressVerifyRequestDTO userAddressVerifyRequestDTO = mock(UserAddressVerifyRequestDTO.class);
		when(userAddressVerifyRequestDataConverter.convert(addressData)).thenReturn(userAddressVerifyRequestDTO);
		defaultUserAddressVerifyIntegrationService.validateAddress(addressData);
		Mockito.verify(userAddressVerifyIntegrationEnhancedService).validateAddress(userAddressVerifyRequestDTO);
	}	
	
	@Test(expected = UserAddressVerifyIntegrationException.class)
	public void testValidateAddressException() throws UserAddressVerifyIntegrationException { 
		ExtAddressData addressData = mock(ExtAddressData.class);
		UserAddressVerifyRequestDTO userAddressVerifyRequestDTO = mock(UserAddressVerifyRequestDTO.class);
		when(userAddressVerifyRequestDataConverter.convert(addressData)).thenReturn(userAddressVerifyRequestDTO);
		when(userAddressVerifyIntegrationEnhancedService.validateAddress(userAddressVerifyRequestDTO)).thenThrow(new IntegrationException("Exception"));
		defaultUserAddressVerifyIntegrationService.validateAddress(addressData);
	}
}
