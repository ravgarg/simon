package com.simon.integration.purchase.services;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.integration.client.RestWebService;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmRequestDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.exceptions.OrderPurchaseIntegrationException;
import com.simon.integration.purchase.services.impl.DefaultOrderPurchaseIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOrderPurchaseIntegrationEnhancedServiceTest {
	
	private static final String ORDER_CODE = "000010001";
	private static final String SERVICE_URL = "serviceUrl";
	
	@InjectMocks
	@Spy
	private DefaultOrderPurchaseIntegrationEnhancedService orderPurchaseIntegrationEnhancedService;
	@Mock
	private RestWebService restWebService;
	@Mock
	private IntegrationException integrationException;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;

	
	@Test
	public void invokePurchaseConfirmRequestTest1()  throws OrderPurchaseIntegrationException{
		PurchaseConfirmResponseDTO purchaseConfirmResponseDTO = mock(PurchaseConfirmResponseDTO.class);
		PurchaseConfirmRequestDTO purchaseConfirmRequestDTO = mock(PurchaseConfirmRequestDTO.class);
		purchaseConfirmRequestDTO.setOrderId(ORDER_CODE);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.ORDER_PURCHAGE_CONFIRM_API_URL)).thenReturn(SERVICE_URL);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn("simonTestEsbToken");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(purchaseConfirmResponseDTO);
		orderPurchaseIntegrationEnhancedService.purchaseConfirmRequest(purchaseConfirmRequestDTO);
		verify(orderPurchaseIntegrationEnhancedService).purchaseConfirmRequest(purchaseConfirmRequestDTO);
	}
	
	@Test(expected = OrderPurchaseIntegrationException.class)
	public void invokePurchaseConfirmRequestTest2() {
		PurchaseConfirmRequestDTO purchaseConfirmRequestDTO = mock(PurchaseConfirmRequestDTO.class);
		purchaseConfirmRequestDTO.setOrderId(ORDER_CODE);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.ORDER_PURCHAGE_CONFIRM_API_URL)).thenReturn(SERVICE_URL);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn("simonTestEsbToken");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(integrationException);
		orderPurchaseIntegrationEnhancedService.purchaseConfirmRequest(purchaseConfirmRequestDTO);
		verify(orderPurchaseIntegrationEnhancedService).purchaseConfirmRequest(purchaseConfirmRequestDTO);
	}
}
