package com.simon.integration.order.deliver.populators;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.variants.model.GenericVariantProductModel;

@UnitTest
public class DeliverOrderDataPopulatorTest {

	private static final String EMAIL = "email";
	private static final String PHONE = "12345678900";
	
	@InjectMocks
	private DeliverOrderDataPopulator deliverOrderDataPopulator;
	@Mock
    private ConfigurationService configurationService;
	@Mock
	private AddressModel billingAddressModel;
	@Mock
	private AddressModel shippingAddressModel;
	@Mock
	private RegionModel regionModel;
	@Mock
	private CountryModel countryModel;
	@Mock
	private CreditCardPaymentInfoModel CreditCardPaymentInfo;
	
	private DeliverOrderRequestDTO deliverOrderRequestDTO;
	private OrderModel orderModel;
	@Mock
	private Configuration configuration;
	

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.REMOVE_OOS)).thenReturn("true");
		when(billingAddressModel.getFirstname()).thenReturn("FirstName");
		when(billingAddressModel.getLastname()).thenReturn("LastName");
		when(billingAddressModel.getTown()).thenReturn("Cityy");
		when(billingAddressModel.getLine1()).thenReturn("Line11");
		when(billingAddressModel.getLine2()).thenReturn("Line12");
		when(billingAddressModel.getPostalcode()).thenReturn("12345");
		when(billingAddressModel.getRegion()).thenReturn(regionModel);
		when(billingAddressModel.getRegion().getName()).thenReturn("NewYork");
		when(billingAddressModel.getPhone1()).thenReturn(PHONE);
		when(shippingAddressModel.getFirstname()).thenReturn("FirstName");
		when(shippingAddressModel.getLastname()).thenReturn("LastName");
		when(shippingAddressModel.getTown()).thenReturn("Cityy");
		when(shippingAddressModel.getLine1()).thenReturn("Line11");
		when(shippingAddressModel.getLine2()).thenReturn("Line12");
		when(shippingAddressModel.getPostalcode()).thenReturn("12345");
		when(shippingAddressModel.getRegion()).thenReturn(regionModel);
		when(shippingAddressModel.getRegion().getName()).thenReturn("NewYork");
		when(shippingAddressModel.getPhone1()).thenReturn(PHONE);

	}

	private void populateRequestDTO(){
		
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final CategoryModel categoryModel2 = new CategoryModel();
		final ItemModelContextImpl itemModelContext1 = (ItemModelContextImpl) categoryModel2.getItemModelContext();
		itemModelContext1.setLocaleProvider(localeProvider);
		categoryModel2.setCode("categorycodekey");
		categoryModel2.setName("test");
		
		List<CategoryModel> categoryModelList2= new ArrayList<>();
		categoryModelList2.add(categoryModel2);
		
		final CategoryModel categoryModel = new CategoryModel();
		final ItemModelContextImpl itemModelContext2 = (ItemModelContextImpl) categoryModel.getItemModelContext();
		itemModelContext2.setLocaleProvider(localeProvider);
		categoryModel.setCode("categorycodekey");
		categoryModel.setName("test");
		
		categoryModel.setSupercategories(categoryModelList2);

		Collection<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(categoryModel);

		ShopModel shopModel = new ShopModel();
		shopModel.setId("1234");

		ProductModel productModel = new GenericVariantProductModel();
		productModel.setSupercategories(categoryModelList);
		productModel.setCode("1234");
		productModel.setShop(shopModel);

		OrderEntryModel orderEntry = new OrderEntryModel();
		orderEntry.setProduct(productModel);
		orderEntry.setQuantity(10L);

		List<AbstractOrderEntryModel> listEntry = new ArrayList<>();
		listEntry.add(orderEntry);
		listEntry.add(orderEntry);

		List<RetailersInfoModel> retailersInfoModels = new ArrayList<>();
		List<ShippingDetailsModel> shippingDetailsModels = new ArrayList<>();
		AdditionalCartInfoModel additionalCartInfoModel = new AdditionalCartInfoModel();

		RetailersInfoModel retailersInfoModel = new RetailersInfoModel();
		retailersInfoModel.setRetailerId("1234");

		retailersInfoModel.setShippingDetails(shippingDetailsModels);
		retailersInfoModels.add(retailersInfoModel);
		additionalCartInfoModel.setRetailersInfo(retailersInfoModels);
		
		DeliveryModeModel deliveryModeModel=new DeliveryModeModel();
		deliveryModeModel.setCode("Cheapest");
		final Map<String, DeliveryModeModel> retailersDeliveryModes = new HashMap<>();
		retailersDeliveryModes.put("1234", deliveryModeModel);
		
		CreditCardPaymentInfoModel paymentInfo=new CreditCardPaymentInfoModel();
		paymentInfo.setBillingAddress(billingAddressModel);
		paymentInfo.setNumber("4111111111111111");
		paymentInfo.setCode("111");
		paymentInfo.setType(CreditCardType.VISA);
		paymentInfo.setValidToMonth("04");
		paymentInfo.setValidToYear("2020");
		
		Set<PromotionResultModel> promotionResultModels = new HashSet<>();
		PromotionResultModel promotionResultModel = new PromotionResultModel();
		promotionResultModel.setRetailerID("1234");
		
		AbstractPromotionModel abstractPromotionModel = new  AbstractPromotionModel();
		abstractPromotionModel.setPromoCode("Coupon value 1");
		
		promotionResultModel.setPromotion(abstractPromotionModel);
		promotionResultModels.add(promotionResultModel);
		
		UserModel userModel = new UserModel();
		userModel.setUid(EMAIL);
		
		orderModel = new OrderModel();
		orderModel.setEntries(listEntry);
		orderModel.setCode("1234");
		orderModel.setAdditionalCartInfo(additionalCartInfoModel);
		orderModel.setRetailersDeliveryModes(retailersDeliveryModes);
		orderModel.setPaymentInfo(paymentInfo);
		orderModel.setDeliveryAddress(shippingAddressModel);
		orderModel.setAllPromotionResults(promotionResultModels);
		orderModel.setUser(userModel);
		
		deliverOrderRequestDTO = new DeliverOrderRequestDTO();
	}
	/**
	 * This method test populate method of DeliverOrderDataPopulator
	 */
	@Test
	public void testPopulateDeliverOrderRequestDTO1() {
		populateRequestDTO();
		
		when(billingAddressModel.getCountry()).thenReturn(countryModel);
		when(countryModel.getIsocode()).thenReturn("US");
		
		when(shippingAddressModel.getCountry()).thenReturn(countryModel);
		when(countryModel.getIsocode()).thenReturn("US");
		
		deliverOrderDataPopulator.populate(orderModel, deliverOrderRequestDTO);
		Assert.assertEquals(deliverOrderRequestDTO.getRetailers().size(), 1);
	}
	
	@Test
	public void testPopulateDeliverOrderRequestDTO2() {
		populateRequestDTO();
		
		deliverOrderDataPopulator.populate(orderModel, deliverOrderRequestDTO);
		Assert.assertEquals(deliverOrderRequestDTO.getRetailers().size(), 1);
	}
}
