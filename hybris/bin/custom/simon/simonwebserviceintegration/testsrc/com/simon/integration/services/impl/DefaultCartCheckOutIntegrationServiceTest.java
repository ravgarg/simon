package com.simon.integration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.exceptions.IntegrationException;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.generated.model.ErrorLogModel;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.services.CartCheckOutIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.time.TimeService;
@UnitTest
public class DefaultCartCheckOutIntegrationServiceTest {
	
	private final String CART_STUB = "stubs/getCartStatusServiceResponse.json";
	private final String ORDER_CONFIRM_UPDATE_STUB = "stubs/getOrderUpdateConfirmationServiceResponseTest.json";
		
	@InjectMocks
	@Spy
	DefaultCartCheckOutIntegrationService defaultIntegrationService;
	@Mock
	private Converter<CartModel, CartEstimateRequestDTO> cartEstimateRequestDataConverter;
	@Mock
	private CartCheckOutIntegrationEnhancedService integrationEnhancedService;
	@Mock
	private CartModel cartModel;
	@Mock
	private CartEstimateRequestDTO cartEstimateRequestDTO;
	@Mock
	private CartEstimateResponseDTO  cartEstimateResponseDTO;
	@Mock
	private CreateCartDTO createCartDTO;
	@Mock
	private Converter<CartModel, CreateCartDTO> createCartDataConverter;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private ClassLoader classLoader;
	@Mock
	private IntegrationException integrationException;
	@Mock
	private Converter<Pair<CartModel, List<VariantAttributeMappingModel>>, CartEstimateRequestDTO> cartEstimateRequestVariantAttributeConverter;
	@Mock
	private Converter<Pair<OrderModel, Exception>, ErrorLogModel> errorLogConverter;
	@Mock
	private ModelService modelService;
	@Mock
	private TimeService timeService;
	@Mock
	private Date date;
	/*
	 * @Before public void setUp() {
	 *
	 * }
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		// cartCheckOutIntegrationException= new
		// CartCheckOutIntegrationException("");
	}

	/**
	 * This method test InvokeCartEstimateRequest method .
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testinvokeCartEstimateRequest() throws CartCheckOutIntegrationException {
		when(cartEstimateRequestDataConverter.convert(cartModel)).thenReturn(cartEstimateRequestDTO);
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributemap=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributemap);
		when(cartEstimateRequestVariantAttributeConverter.convert(ImmutablePair.of(cartModel, variantAttributeMappingList), cartEstimateRequestDTO)).thenReturn(cartEstimateRequestDTO);
		when(defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList)).thenReturn(cartEstimateResponseDTO);
		Assert.assertEquals(defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList), cartEstimateResponseDTO);

	}

	/**
	 * This method test InvokeCartEstimateRequest method when it throw
	 * integration exception .
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test(expected = CartCheckOutIntegrationException.class)
	public void testinvokeCartEstimateRequestWithException() throws CartCheckOutIntegrationException {
		when(cartEstimateRequestDataConverter.convert(cartModel)).thenReturn(cartEstimateRequestDTO);
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributemap=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributemap);
		when(cartEstimateRequestVariantAttributeConverter.convert(ImmutablePair.of(cartModel, variantAttributeMappingList), cartEstimateRequestDTO)).thenReturn(cartEstimateRequestDTO);
		when(defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList)).thenThrow(integrationException);
		defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList);

	}
	
	@Test
	public void testCreateCartCall() throws CartCheckOutIntegrationException, URISyntaxException{
		when(createCartDataConverter.convert(cartModel)).thenReturn(createCartDTO);
		when(timeService.getCurrentTime()).thenReturn(date);
		when(date.getTime()).thenReturn(123L);
		defaultIntegrationService.invokeCreateCartRequest(cartModel);
		
		verify(integrationEnhancedService).createCart(createCartDTO);
	}

	@Test(expected = CartCheckOutIntegrationException.class)
	public void testCreateCartCallReturnOtherObject() throws CartCheckOutIntegrationException, URISyntaxException{
		when(createCartDataConverter.convert(cartModel)).thenReturn(createCartDTO);
		when(timeService.getCurrentTime()).thenReturn(date);
		when(date.getTime()).thenReturn(123L);
		Mockito.doThrow(new IntegrationException("","")).when(integrationEnhancedService).createCart(createCartDTO);

		defaultIntegrationService.invokeCreateCartRequest(cartModel);
	}
	
	
	
	@Test
	public void invokeCartConfirmationMockRequestTest() throws IOException{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CREATE_CART_STUB)).thenReturn(CART_STUB);
		defaultIntegrationService.invokeCartConfirmationMockRequest();
		verify(defaultIntegrationService).invokeCartConfirmationMockRequest();
	}
	@Test
	public void invokeCartEstimateTestIfMockIsTrue() throws CartCheckOutIntegrationException, IOException{
		
		doReturn(configuration).when(configurationService).getConfiguration();
		doReturn("true").when(configuration).getString(SimonIntegrationConstants.ESTIMATE_CART_STUB);	
		doReturn(CART_STUB).when(configuration).getString("estimate.cart.service.stub.fileName");		
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributemap=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributemap);
		defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList);
	}
	
	@Test
	public void invokeCartEstimateTestIfMockThrowException() throws CartCheckOutIntegrationException, IOException{
		CartCheckOutIntegrationEnhancedService cartCheckOutIntegrationEnhancedService = Mockito.mock(CartCheckOutIntegrationEnhancedService.class);
		when(cartCheckOutIntegrationEnhancedService.estimateCart(cartEstimateRequestDTO)).thenReturn(cartEstimateResponseDTO);
		doReturn(configuration).when(configurationService).getConfiguration();
		doReturn("true").when(configuration).getString(SimonIntegrationConstants.ESTIMATE_CART_STUB);
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributemap=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributemap);
		defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList);
	}
	
	@Test
	public void invokeOrderUpdateConfirmMockRequestTest() throws IOException{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CREATE_CART_STUB)).thenReturn(ORDER_CONFIRM_UPDATE_STUB);
		defaultIntegrationService.invokeOrderUpdateConfirmMockRequest(ORDER_CONFIRM_UPDATE_STUB);
		verify(defaultIntegrationService).invokeOrderUpdateConfirmMockRequest(ORDER_CONFIRM_UPDATE_STUB);
	}
	
	@Test
	public void invokeCartEstimateTestIfMockIsFalse() throws CartCheckOutIntegrationException, IOException{
		
		CartEstimateResponseDTO responseDTO = new CartEstimateResponseDTO();
		responseDTO.setCartID("123");
		doReturn(configuration).when(configurationService).getConfiguration();
		doReturn("false").when(configuration).getString(SimonIntegrationConstants.ESTIMATE_CART_STUB);	
		doReturn(CART_STUB).when(configuration).getString("estimate.cart.service.stub.fileName");		
		List<VariantAttributeMappingModel> variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributemap=mock(VariantAttributeMappingModel.class);
		variantAttributeMappingList.add(variantAttributemap);
		when(defaultIntegrationService.invokeCartEstimateRequest(cartModel, variantAttributeMappingList)).thenReturn(responseDTO);
		assertEquals("123",responseDTO.getCartID());
	}
	
}