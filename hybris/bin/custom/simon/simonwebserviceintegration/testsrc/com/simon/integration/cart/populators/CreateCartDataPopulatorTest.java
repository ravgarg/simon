package com.simon.integration.cart.populators;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

@UnitTest
public class CreateCartDataPopulatorTest {

	@InjectMocks
	private CreateCartDataPopulator createCartDataPopulator;
	
	private CreateCartDTO createCartDTO;

	private CartModel cartModel;
	
	private GenericVariantProductModel productModel;
	
	private CartEntryModel cartEntry;
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		
		ShopModel shopModel= new ShopModel(); 
		shopModel.setId("1234");
		
		productModel= new GenericVariantProductModel();
		productModel.setCode("1234");
		productModel.setShop(shopModel);
		productModel.setProductURL("url");
				
		cartEntry= new CartEntryModel();
		cartEntry.setProduct(productModel);
		cartEntry.setQuantity(10L);
		
		List<AbstractOrderEntryModel> listEntry= new ArrayList<>();
		listEntry.add(cartEntry);
		listEntry.add(cartEntry);
		
		cartModel= new CartModel();	
		cartModel.setEntries(listEntry);
		cartModel.setCode("1234");
		
		createCartDTO= new CreateCartDTO();
		
	}
	/**
	 * This method test populate method of CreateCartDataPopulator
	 * 
	 */
	@Test
	public void testPopulateCreateCartRequestDTO() {
		
		ShopModel shopModel = new ShopModel();
		shopModel.setId("shopId");
		
		GenericVariantProductModel genericVariantProductModel = new GenericVariantProductModel();
		genericVariantProductModel.setProductURL("url");
		genericVariantProductModel.setShop(shopModel);
		
		AbstractOrderEntryModel abstractOrderEntryModel = new AbstractOrderEntryModel();
		abstractOrderEntryModel.setProduct(genericVariantProductModel);
		
		List<AbstractOrderEntryModel> listOfAbstractOrderEntryModel = new ArrayList<>();
		listOfAbstractOrderEntryModel.add(abstractOrderEntryModel);
		
		cartModel.setEntries(listOfAbstractOrderEntryModel);
		
		createCartDataPopulator.populate(cartModel, createCartDTO); 
		Assert.assertEquals(1, createCartDTO.getRetailers().size());	
	}
}
