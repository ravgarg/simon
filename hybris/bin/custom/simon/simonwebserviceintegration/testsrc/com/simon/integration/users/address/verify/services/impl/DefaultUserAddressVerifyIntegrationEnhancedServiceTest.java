package com.simon.integration.users.address.verify.services.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.integration.client.RestWebService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class DefaultUserAddressVerifyIntegrationEnhancedServiceTest {

	@InjectMocks
	@Spy	
	private DefaultUserAddressVerifyIntegrationEnhancedService userAddressVerifyIntegrationEnhancedService;
	
	@Mock
	private UserAddressVerifyResponseDTO userAddressVerifyResponseDTO;
	@Mock
	private UserAddressVerifyRequestDTO userAddressVerifyRequestDTO;
	@Mock
	private RestWebService restWebService;
	@Mock
	private MultiValueMap<String, Object> headersMap;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	
	private String serviceURl;
	private String stub;
	private String stubFile;
	
	@Before
	public void setUp() throws JsonProcessingException {
		MockitoAnnotations.initMocks(this);
		serviceURl="https://abc.com";
		stub="false"; 
		stubFile=null;
	}

	@Test
	public void testValidateAddress(){ 
		doReturn(headersMap).when(userAddressVerifyIntegrationEnhancedService).getHeadersMap();
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_ADDRESS_SERVICE_API)).thenReturn(serviceURl);
		when(restWebService.executeRestEvent(serviceURl, userAddressVerifyRequestDTO ,stub ,stubFile, UserAddressVerifyResponseDTO.class, RequestMethod.POST, headersMap, null)).thenReturn(userAddressVerifyResponseDTO);
		Assert.assertEquals(userAddressVerifyIntegrationEnhancedService.validateAddress(userAddressVerifyRequestDTO),
			userAddressVerifyResponseDTO);
	}	
}
