package com.simon.integration.purchase.services;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.integration.dto.purchase.confirm.PurchaseConfirmRequestDTO;
import com.simon.integration.dto.purchase.confirm.PurchaseConfirmResponseDTO;
import com.simon.integration.purchase.services.impl.DefaultOrderPurchaseIntegrationService;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOrderPurchaseIntegrationServiceTest {

	private static final String ORDER_CODE = "000010001";
	private static final String ERROR_CODE = "errorCode";
	@InjectMocks
	@Spy
	private DefaultOrderPurchaseIntegrationService orderPurchaseIntegrationService;
	@Mock
	private OrderPurchaseIntegrationEnhancedService orderPurchaseIntegrationEnhancedService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void invokePurchaseConfirmRequestTest(){
		final PurchaseConfirmResponseDTO purchaseConfirmResponseDTO = mock(PurchaseConfirmResponseDTO.class);
		when(orderPurchaseIntegrationEnhancedService.purchaseConfirmRequest(Mockito.anyObject())).thenReturn(purchaseConfirmResponseDTO);
		when(purchaseConfirmResponseDTO.getErrorCode()).thenReturn(ERROR_CODE);
		orderPurchaseIntegrationService.invokePurchaseConfirmRequest(ORDER_CODE);
		verify(orderPurchaseIntegrationService).invokePurchaseConfirmRequest(ORDER_CODE);
	}
}
