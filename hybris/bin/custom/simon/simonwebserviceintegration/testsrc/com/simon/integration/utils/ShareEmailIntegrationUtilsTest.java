package com.simon.integration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.StringWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShareEmailIntegrationUtilsTest {
	@InjectMocks
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Mock
	private ConfigurationService configurationService;
	
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testconvertShareEmailXmlDataInString()
	{
		
		StringWriter stringWriter=new StringWriter();
		stringWriter.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><offerData><heading>Love \" this & Deal</heading></offerData>")	;
		String resultStr=shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter);
		assertEquals(true,resultStr.contains("<offerData>"));
		
	}
	
	@Test
	public void testconvertDateTimeInString()
	{
		final Date startdate = new GregorianCalendar(2018, Calendar.JANUARY, 11).getTime();
		String resultStr=shareEmailIntegrationUtils.convertCurrentDateTimeInString(startdate);
		assertEquals("2018-01-11T00:00:00+0530",resultStr);
	}
	
	@Test
	public void testgetConfiguredStaticString(){
		Configuration configuration=mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(any())).thenReturn("TestCongig");
		String resultStr=shareEmailIntegrationUtils.getConfiguredStaticString("newTEst");
		assertEquals("TestCongig",resultStr);
	}

}
