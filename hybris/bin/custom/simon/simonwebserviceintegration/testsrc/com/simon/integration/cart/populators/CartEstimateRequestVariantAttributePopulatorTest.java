package com.simon.integration.cart.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.cartestimate.CartProductsDTO;
import com.simon.integration.dto.cart.cartestimate.RetailerRequestDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;


@UnitTest
public class CartEstimateRequestVariantAttributePopulatorTest {
	 
	@InjectMocks
	private CartEstimateRequestVariantAttributePopulator  cartEstimateRequestVariantAttributePopulator;
	
	private CartEstimateRequestDTO cartEstimateRequestDTO;
	@Mock
	private CartEstimateResponseDTO  cartEstimateResponseDTO;
	
	private CartModel source;
	
	private List<VariantAttributeMappingModel>  variantAttributeMappingList;
	
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		
	}
	/**
	 * This method test populate method of CartEstimateRequestVariantAttributePopulator
	 * @throws JsonProcessingException 
	 */
	
	
	private void populateData(){
		ShopModel shopModel= mock(ShopModel.class); 
		when(shopModel.getId()).thenReturn("shop1234");
		variantAttributeMappingList=new ArrayList<>();
		VariantAttributeMappingModel variantAttributeModal=mock(VariantAttributeMappingModel.class);
		when(variantAttributeModal.getSourceAttribute()).thenReturn("categorycodevalueName");
		when(variantAttributeModal.getTargetAttribute()).thenReturn("TargetcodevalueName");
		when(variantAttributeModal.getShop()).thenReturn(shopModel);
		variantAttributeMappingList.add(variantAttributeModal);
		cartEstimateRequestDTO=mock(CartEstimateRequestDTO.class);
		
		List<RetailerRequestDTO> retailers = new ArrayList<>();
		RetailerRequestDTO retailerRequestDTO=mock(RetailerRequestDTO.class);
		List<CartProductsDTO> cartProducts = new ArrayList<>();
		CartProductsDTO cartProductsDTO=mock(CartProductsDTO.class);
		when(cartProductsDTO.getProductID()).thenReturn("123");
		Map<String, String> variants=new HashMap<>();
		variants.put("categorycodevalueName", "categoryValue");
		when(cartProductsDTO.getVariants()).thenReturn(variants);
		cartProducts.add(cartProductsDTO);
		cartProducts.add(cartProductsDTO);
		retailers.add(retailerRequestDTO);
		when(retailerRequestDTO.getCartProducts()).thenReturn(cartProducts);
		when(retailerRequestDTO.getRetailerID()).thenReturn("shop1234");
		when(cartEstimateRequestDTO.getRetailers()).thenReturn(retailers);
	}
	@Test
	public void testpopulate(){
		 populateData();
		 Map<String, String> variantsMap=new HashMap<>();
		variantsMap.put("categorycodevalueName", "categoryValue");
		cartEstimateRequestVariantAttributePopulator.populate(ImmutablePair.of(source, variantAttributeMappingList), cartEstimateRequestDTO);
		assertEquals(variantsMap, cartEstimateRequestDTO.getRetailers().get(0).getCartProducts().get(0).getVariants());
	}
	
}
