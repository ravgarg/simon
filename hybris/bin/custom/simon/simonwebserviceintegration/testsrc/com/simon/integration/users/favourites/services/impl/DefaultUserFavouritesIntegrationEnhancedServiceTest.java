package com.simon.integration.users.favourites.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.users.favourites.dto.AddUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.RemoveUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteResponseDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;

@UnitTest
public class DefaultUserFavouritesIntegrationEnhancedServiceTest
{
	@InjectMocks
	private DefaultUserFavouritesIntegrationEnhancedService defaultUserFavouritesIntegrationEnhancedService;
	@Mock
	private RestWebService restWebService;
	@Mock
	private EnqueueService enqueueService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	
	@Mock
	private SessionService sessionService;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
		when(userIntegrationUtils.populateHeadersForESB()).thenReturn(map);
		RestAuthHeaderDTO authMap=new RestAuthHeaderDTO();
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(authMap);
	}

	@Test
	public void testAddUserFavourite()
	{
		AddUserFavouriteRequestDTO request=new AddUserFavouriteRequestDTO();
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn("teststring");
		Mockito.doNothing().when(enqueueService).sendObjectMessage(request, JmsTemplateNameEnum.ADD_FAV);
		assertTrue(defaultUserFavouritesIntegrationEnhancedService.addUserFavourite(request));
	}

	@Test
	public void testAddUserFavouriteConnectionFailed()
	{
		AddUserFavouriteRequestDTO request=new AddUserFavouriteRequestDTO();
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn("teststring");
		Mockito.doThrow(new IntegrationException("")).when(enqueueService).sendObjectMessage(request, JmsTemplateNameEnum.ADD_FAV);
		defaultUserFavouritesIntegrationEnhancedService.addUserFavourite(request);
	}
	
	@Test
	public void testRemoveFavourite()
	{
		RemoveUserFavouriteRequestDTO request=new RemoveUserFavouriteRequestDTO();
		Mockito.doNothing().when(enqueueService).sendObjectMessage(request, JmsTemplateNameEnum.REMOVE_FAV);
		assertTrue(defaultUserFavouritesIntegrationEnhancedService.removeFavourite(request));
		
	}
	
	@Test
	public void testRemoveFavouriteConnectionFailed()
	{
		RemoveUserFavouriteRequestDTO request=new RemoveUserFavouriteRequestDTO();
		Mockito.doThrow(new IntegrationException("")).when(enqueueService).sendObjectMessage(request, JmsTemplateNameEnum.REMOVE_FAV);
		defaultUserFavouritesIntegrationEnhancedService.removeFavourite(request);
	}
	
	@Test
	public void testGetFavourites()
	{
		UserFavouriteResponseDTO response=new UserFavouriteResponseDTO();
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_FAVOURITES_GET_URL)).thenReturn("url");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		assertEquals(response,defaultUserFavouritesIntegrationEnhancedService.getFavourites("url"));
	}
	
	@Test(expected=UserFavouritesIntegrationException.class)
	public void testGetFavouritesWithError()
	{	UserFavouriteResponseDTO response=new UserFavouriteResponseDTO();
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_FAVOURITES_GET_URL)).thenReturn("url");
		response.setErrorCode("111");
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(response);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		defaultUserFavouritesIntegrationEnhancedService.getFavourites("url");
	}
	
	@Test(expected=UserFavouritesIntegrationException.class)
	public void testGetFavouritesConnectionFailed()
	{	UserFavouriteResponseDTO response=new UserFavouriteResponseDTO();
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_FAVOURITES_GET_URL)).thenReturn("url");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException(""));
		defaultUserFavouritesIntegrationEnhancedService.getFavourites("url");
	}
}
