package com.simon.integration.users.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.ResponseDTO;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserCenterRequestDTO;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.services.UserIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultUserIntegrationServiceTest {

	@InjectMocks
	private DefaultUserIntegrationService userIntegrationService;
	@Mock
	private UserIntegrationEnhancedService userIntegrationEnhancedService;
	@Mock
	private Converter<CustomerData, UserRegisterUpdateRequestDTO> updateUserDataConverter;
	@Mock
	private Converter<RegisterData,UserRegisterUpdateRequestDTO> registerUserConverter;
	@Mock
	private RegisterData registerData;
	@Mock
	private VIPUserDTO response;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	

	@Before
	public void setup()
	{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(false);
	}
	@Test
	public void updateUserDetailsTest() throws  UserIntegrationException{
		CustomerData userData = mock(CustomerData.class);
		VIPUserDTO getUserDetailsDTO = mock(VIPUserDTO.class);
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);
		when(updateUserDataConverter.convert(userData)).thenReturn(userDetailsDto);
		when(userIntegrationEnhancedService.updateUserDetails(userDetailsDto)).thenReturn(getUserDetailsDTO);
		userIntegrationService.updateUserDetails(userData);
	}

	
	@Test
	public void registerUserTest() throws  UserIntegrationException, AuthLoginIntegrationException{
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);
		when(registerUserConverter.convert(registerData)).thenReturn(userDetailsDto);
		when(userIntegrationEnhancedService.registerUser(userDetailsDto)).thenReturn(response);
		assertEquals(response,userIntegrationService.registerUser(registerData));
	}
	
	@Test
	public void getDefaultCenterTest() throws UserIntegrationException{
		UserCenterIdDTO userCenterIdDTO = mock(UserCenterIdDTO.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_MYCENTER_DEFAULT_RADIUS)).thenReturn("500");
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_MYCENTER_LW)).thenReturn("true");
		UserCenterRequestDTO userCenterRequestDTO = mock(UserCenterRequestDTO.class);
		when(userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO)).thenReturn(userCenterIdDTO);
		userIntegrationService.getDefaultCenter("12345");
		Assert.assertNotNull(userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO));
	}
	@Test
	public void updateUserDetailsMockTest() throws  UserIntegrationException{
		CustomerData userData = mock(CustomerData.class);
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(SimonIntegrationConstants.DEFAULT_USER_ID,userIntegrationService.updateUserDetails(userData).getId());
	}

	
	@Test
	public void registerUserMockTest() throws  UserIntegrationException, AuthLoginIntegrationException{
		CustomerData userData = mock(CustomerData.class);
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(SimonIntegrationConstants.DEFAULT_USER_ID,userIntegrationService.registerUser(registerData).getId());
	}
	
	@Test
	public void getDefaultCenterMockTest() throws UserIntegrationException{
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals("888",userIntegrationService.getDefaultCenter("12345").getId());
	}
}
