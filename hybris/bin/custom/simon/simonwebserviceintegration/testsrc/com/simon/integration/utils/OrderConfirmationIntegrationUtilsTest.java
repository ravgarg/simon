package com.simon.integration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.integration.constants.SimonIntegrationConstants;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OrderConfirmationIntegrationUtilsTest
{
	private final String PRICE_VALUE_WITH_DOLLER_SIGN = "$10.00";
	private final String PRICE_VALUE_WITHOUT_DOLLER_SIGN = "10.00";
	private final String BLANK_PRICE_VALUE = "";
	
	@InjectMocks
	private OrderConfirmationIntegrationUtils orderConfirmationIntegrationUtils;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	
	@Test
	public void validateAndRemoveDollerSignTest1(){
		Double price = orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(PRICE_VALUE_WITH_DOLLER_SIGN);
		assertEquals(Double.valueOf(10.0),price);
	}
	
	@Test
	public void validateAndRemoveDollerSignTest2(){
		Double price = orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(PRICE_VALUE_WITHOUT_DOLLER_SIGN);
		assertEquals(Double.valueOf(10.0),price);
	}

	@Test
	public void validateAndRemoveDollerSignTest3(){
		Double price = orderConfirmationIntegrationUtils.validateAndRemoveDollerSign(BLANK_PRICE_VALUE);
		assertEquals(Double.valueOf(0),price);
	}
	
}
