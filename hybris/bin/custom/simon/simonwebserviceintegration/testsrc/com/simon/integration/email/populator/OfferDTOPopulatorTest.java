package com.simon.integration.email.populator;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.data.OfferData;
import com.simon.facades.email.data.RetailerData;

import com.simon.integration.dto.email.deal.EmailOfferDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class OfferDTOPopulatorTest {
	@InjectMocks
	private OfferDTOPopulator offerDTOPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testPopulate(){
		
		final ShareEmailData source=mock(ShareEmailData.class);
		when(source.getEmailFrom()).thenReturn("fromEmail");
		final RetailerData retailer=mock(RetailerData.class);
		when(source.getRetailer()).thenReturn(retailer);
		final OfferData offer=mock(OfferData.class);
		when(retailer.getOffer()).thenReturn(offer);
		when(offer.getDescription()).thenReturn("Offer Description");
		final EmailOfferDTO target=new EmailOfferDTO();
		offerDTOPopulator.populate(source, target);
		Mockito.verify(source,Mockito.atLeastOnce()).getRetailer();
		
	}
}
