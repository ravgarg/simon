package com.simon.integration.users.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserPaymentAddressDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;;

/**
 * This is a JUnit class of DefaultUserAddressIntegrationService
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultUserAddressIntegrationServiceTest {

	private static final String SUCCESS_RESPONSE_FROM_MOCKED_ESB = "Success Response from Mocked ESB";

	@InjectMocks
	private DefaultUserAddressIntegrationService userAddressIntegrationService;

	@Mock
	private DefaultUserAddressIntegrationEnhancedService userAddressIntegrationEnhancedService;
	@Mock
	private Converter<ExtAddressData, UserAddressDTO> userAddressConverter;
	@Mock
	private Converter<CCPaymentInfoData, UserPaymentAddressDTO> userPaymentAddressConverter;
	@Mock
	private ExtAddressData extAddressData;
	
	@Mock
	private CCPaymentInfoData ccPaymentInfoData;

	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(false);
	}

	@Test
	public void addUserAddressTest() throws UserAddressIntegrationException {
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO userAddressResponseDTO = mock(UserAddressResponseDTO.class);
		when(userAddressConverter.convert(extAddressData)).thenReturn(userAddressDTO);
		when(userAddressIntegrationEnhancedService.addUserAddress(userAddressDTO)).thenReturn(userAddressResponseDTO);
		assertEquals(userAddressIntegrationService.addUserAddress(extAddressData),userAddressResponseDTO);
	}
	
	@Test
	public void addUserAddressNullTest() throws UserAddressIntegrationException {
		assertEquals(userAddressIntegrationService.addUserAddress(null),null);
	}
	
	@Test
	public void updateUserAddressTest() throws UserAddressIntegrationException {
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO userAddressResponseDTO = mock(UserAddressResponseDTO.class);
		when(userAddressConverter.convert(extAddressData)).thenReturn(userAddressDTO);
		when(userAddressIntegrationEnhancedService.updateUserAddress(userAddressDTO)).thenReturn(userAddressResponseDTO);
		assertEquals(userAddressIntegrationService.updateUserAddress(extAddressData),userAddressResponseDTO);
	}
	
	@Test
	public void updateUserAddressNullTest() throws UserAddressIntegrationException {
		assertEquals(userAddressIntegrationService.updateUserAddress(null),null);
	}
	
	@Test
	public void removeUserAddressTest() throws UserAddressIntegrationException {
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO userAddressResponseDTO = mock(UserAddressResponseDTO.class);
		when(userAddressConverter.convert(extAddressData)).thenReturn(userAddressDTO);
		when(userAddressIntegrationEnhancedService.removeUserAddress(userAddressDTO)).thenReturn(userAddressResponseDTO);
		assertEquals(userAddressIntegrationService.removeUserAddress(extAddressData),userAddressResponseDTO);
	}
	
	@Test
	public void removeUserAddressExternalIdNullTest() throws UserAddressIntegrationException {
		UserAddressResponseDTO userAddressResponseDTO = null;
		when(userAddressIntegrationEnhancedService.removeUserAddress(null)).thenReturn(userAddressResponseDTO);
		assertEquals(userAddressIntegrationService.removeUserAddress(null),userAddressResponseDTO);
	}
	
	@Test
	public void addUserAddressPaymentTest() throws UserAddressIntegrationException {
		UserPaymentAddressDTO userPaymentAddressDTO = mock(UserPaymentAddressDTO.class);
		UserAddressResponseDTO userAddressResponseDTO = mock(UserAddressResponseDTO.class);
		when(userPaymentAddressConverter.convert(ccPaymentInfoData)).thenReturn(userPaymentAddressDTO);
		when(userAddressIntegrationEnhancedService.addUserPaymentAddress(userPaymentAddressDTO)).thenReturn(userAddressResponseDTO);
		assertEquals(userAddressIntegrationService.addUserPaymentAddress(ccPaymentInfoData),userAddressResponseDTO);
	}
	
	@Test
	public void addUserAddressPaymentNullTest() throws UserAddressIntegrationException {
		assertEquals(userAddressIntegrationService.addUserPaymentAddress(null),null);
	}

	@Test
	public void addUserAddressMockTest()
			throws UserAddressIntegrationException {
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(userAddressIntegrationService.addUserAddress(null).getMessage(),SUCCESS_RESPONSE_FROM_MOCKED_ESB);
	}

	

	@Test
	public void updateUserAddressMockTest()
			throws UserAddressIntegrationException {
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(userAddressIntegrationService.updateUserAddress(null).getMessage(),SUCCESS_RESPONSE_FROM_MOCKED_ESB);
	}

	
	@Test
	public void removeUserAddressMockTest() throws UserAddressIntegrationException {
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(userAddressIntegrationService.removeUserAddress(null).getMessage(),SUCCESS_RESPONSE_FROM_MOCKED_ESB);
	}

	
	
	@Test
	public void addUserPaymentAddressMockTest()
			throws UserAddressIntegrationException {
		when(configuration.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertEquals(userAddressIntegrationService.addUserPaymentAddress(null).getMessage(),SUCCESS_RESPONSE_FROM_MOCKED_ESB);
	}
}
