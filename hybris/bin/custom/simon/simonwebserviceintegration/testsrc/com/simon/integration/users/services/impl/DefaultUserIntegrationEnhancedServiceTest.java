package com.simon.integration.users.services.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.MultiValueMap;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.facades.customer.data.MallData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.ResponseDTO;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserCenterDTO;
import com.simon.integration.users.dto.UserCenterRequestDTO;
import com.simon.integration.users.dto.UserCentersResponseDTO;
import com.simon.integration.users.dto.UserRegisterUpdateRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultUserIntegrationEnhancedServiceTest {

	private String END_POINT_URL = "endPointUrl";
	private static final String AUTH_TOKEN = "AUTHTOKEN";
	public static final String AUTH_TOKEN_2 = "authToken";
	
	@InjectMocks
	private DefaultUserIntegrationEnhancedService userIntegrationEnhancedService;
	@Mock
	RestWebService restWebService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private SessionService sessionService;
	@Mock
	private MultiValueMap<String, Object> header;
	@Mock
	private IntegrationException integrationException;
	@Mock
	private AuthLoginIntegrationService authLoginIntegrationService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private RestAuthHeaderDTO authheader;
	
	private List<MallData> centerIdDatas = new ArrayList<>();
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(Matchers.any(ResponseDTO.class));
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_SERVICE_URL)).thenReturn("url");
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(authheader);
		when(userIntegrationUtils.populateHeadersForESB()).thenReturn(header);
	}
	
	@Test
	public void updateUserDetailsTest() throws UserIntegrationException{
		VIPUserDTO getUserDetailsDTO = new VIPUserDTO();
		MallData userCenterIdData = mock(MallData.class);
		centerIdDatas.add(userCenterIdData);
		CustomerData userData = mock(CustomerData.class);
		when(userData.getHouseholdIncomeCode()).thenReturn(1);
		when(userData.getBirthYear()).thenReturn(1987);
		when(userData.getAlternateMalls()).thenReturn(centerIdDatas);
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_UPDATE_USER_END_POINT)).thenReturn(END_POINT_URL);
		when(sessionService.getAttribute(AUTH_TOKEN_2)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(getUserDetailsDTO);
		Assert.assertEquals(getUserDetailsDTO,userIntegrationEnhancedService.updateUserDetails(userDetailsDto));
	}
	
	@Test(expected = UserIntegrationException.class)
	public void updateUserDetailsWithErrorCodeTest() throws UserIntegrationException{
		VIPUserDTO getUserDetailsDTO = new VIPUserDTO();
		getUserDetailsDTO.setErrorCode("112");
		MallData userCenterIdData = mock(MallData.class);
		centerIdDatas.add(userCenterIdData);
		CustomerData userData = mock(CustomerData.class);
		when(userData.getHouseholdIncomeCode()).thenReturn(1);
		when(userData.getBirthYear()).thenReturn(1987);
		when(userData.getAlternateMalls()).thenReturn(centerIdDatas);
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_UPDATE_USER_END_POINT)).thenReturn(END_POINT_URL);
		when(sessionService.getAttribute(AUTH_TOKEN_2)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(getUserDetailsDTO);
		Assert.assertEquals(getUserDetailsDTO,userIntegrationEnhancedService.updateUserDetails(userDetailsDto));
	}
	
	@Test(expected = UserIntegrationException.class)
	public void updateUserDetailsTestException() throws Exception{
		MallData userCenterIdData = mock(MallData.class);
		centerIdDatas.add(userCenterIdData);	
		CustomerData userData = mock(CustomerData.class);	
		when(userData.getHouseholdIncomeCode()).thenReturn(1);
		when(userData.getBirthYear()).thenReturn(1987);
		when(userData.getAlternateMalls()).thenReturn(centerIdDatas);
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_UPDATE_USER_END_POINT)).thenReturn(END_POINT_URL);
		when(sessionService.getAttribute(AUTH_TOKEN_2)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(integrationException);
		userIntegrationEnhancedService.updateUserDetails(userDetailsDto);
	}
	@Test
	public void registerUserTest() throws UserIntegrationException, AuthLoginIntegrationException{
		VIPUserDTO vipUser=new VIPUserDTO();
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL)).thenReturn(END_POINT_URL);
		when(authLoginIntegrationService.generateAndSaveOAuthTokenForRegistration()).thenReturn(true);
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(vipUser);
		doNothing().when(sessionService).removeAttribute(SimonIntegrationConstants.AUTH_TOKEN);
		Assert.assertEquals(vipUser, userIntegrationEnhancedService.registerUser(userDetailsDto));
	}
	
	@Test(expected=UserIntegrationException.class)
	public void registerUserWithAuthFailedTest() throws UserIntegrationException, AuthLoginIntegrationException{
		VIPUserDTO vipUser=new VIPUserDTO();
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL)).thenReturn(END_POINT_URL);
		when(authLoginIntegrationService.generateAndSaveOAuthTokenForRegistration()).thenReturn(false);
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(vipUser);
		doNothing().when(sessionService).removeAttribute(SimonIntegrationConstants.AUTH_TOKEN);
		Assert.assertEquals(vipUser, userIntegrationEnhancedService.registerUser(userDetailsDto));
	}
	
	@Test(expected=UserIntegrationException.class)
	public void registerUserWithEmailExistsTest() throws UserIntegrationException, AuthLoginIntegrationException{
		VIPUserDTO response = new VIPUserDTO();
		response.setErrorCode("111");
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL)).thenReturn(END_POINT_URL);
		when(authLoginIntegrationService.generateAndSaveOAuthTokenForRegistration()).thenReturn(true);
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userIntegrationEnhancedService.registerUser(userDetailsDto);
		doNothing().when(sessionService).removeAttribute(SimonIntegrationConstants.AUTH_TOKEN);
	}
	
	@Test(expected=UserIntegrationException.class)
	public void registerUserWithErrorInResponseTest() throws UserIntegrationException, AuthLoginIntegrationException{
		UserRegisterUpdateRequestDTO userDetailsDto = mock(UserRegisterUpdateRequestDTO.class);		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_REGISTER_USER_END_POINT_URL)).thenReturn(END_POINT_URL);
		when(authLoginIntegrationService.generateAndSaveOAuthTokenForRegistration()).thenReturn(true);
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn(AUTH_TOKEN);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userIntegrationEnhancedService.registerUser(userDetailsDto);
		doNothing().when(sessionService).removeAttribute(SimonIntegrationConstants.AUTH_TOKEN);
	}
	
	@Test(expected=UserIntegrationException.class)
	public void getDefaultPrimaryCenterTestWithEception() throws UserIntegrationException{
		UserCenterRequestDTO userCenterRequestDTO = mock(UserCenterRequestDTO.class);
		userCenterRequestDTO.setZipCode("12345");
		userCenterRequestDTO.setRadius(500);
		userCenterRequestDTO.setLw(true);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(Matchers.anyString())).thenReturn("test");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO);
	}
	
	@Test
	public void getDefaultPrimaryCenterTest() throws UserIntegrationException{
		UserCenterRequestDTO userCenterRequestDTO = mock(UserCenterRequestDTO.class);
		userCenterRequestDTO.setZipCode("12345");
		userCenterRequestDTO.setRadius(500);
		userCenterRequestDTO.setLw(true);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_CENTER_BY_ZIPCODE_END_POINT_URL)).thenReturn("serviceUrl");
		
		List<UserCenterDTO> centers = new ArrayList<>();
		final UserCentersResponseDTO userCentersResponseDTO = mock(UserCentersResponseDTO.class);
		final UserCenterDTO center = mock(UserCenterDTO.class);
		center.setPropertyType("OutletCenter");
		center.setMallId(123);
		center.setMallName("NAME");
		centers.add(center);
		userCentersResponseDTO.setCenters(centers);
		
		when(userCentersResponseDTO.getCenters()).thenReturn(centers);
		when(center.getPropertyType()).thenReturn("OutletCenter");
		when(center.getMallId()).thenReturn(123);
		when(center.getMallName()).thenReturn("NAME");
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(userCentersResponseDTO);
		Assert.assertEquals("123", userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO).getId());
	}
	
	@Test(expected=UserIntegrationException.class)
	public void getDefaultPrimaryCenterTestBlankCenterList() throws UserIntegrationException{
		UserCenterRequestDTO userCenterRequestDTO = mock(UserCenterRequestDTO.class);
		userCenterRequestDTO.setZipCode("12345");
		userCenterRequestDTO.setRadius(500);
		userCenterRequestDTO.setLw(true);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_CENTER_BY_ZIPCODE_END_POINT_URL)).thenReturn("serviceUrl");
		
		List<UserCenterDTO> centers = new ArrayList<>();
		final UserCentersResponseDTO userCentersResponseDTO = mock(UserCentersResponseDTO.class);
		userCentersResponseDTO.setCenters(centers);
		when(userCentersResponseDTO.getErrorCode()).thenReturn("123");
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(userCentersResponseDTO);
		when(userCentersResponseDTO.getCenters()).thenReturn(centers);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(userCentersResponseDTO);
		userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO);
	}
	
	@Test(expected=UserIntegrationException.class)
	public void getDefaultPrimaryCenterTestErrorCode() throws UserIntegrationException{
		UserCenterRequestDTO userCenterRequestDTO = mock(UserCenterRequestDTO.class);
		userCenterRequestDTO.setZipCode("12345");
		userCenterRequestDTO.setRadius(500);
		userCenterRequestDTO.setLw(true);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.USER_INTEGRATION_SERVICE_CENTER_BY_ZIPCODE_END_POINT_URL)).thenReturn("serviceUrl");
		
		final UserCentersResponseDTO userCentersResponseDTO = mock(UserCentersResponseDTO.class);
		when(userCentersResponseDTO.getErrorCode()).thenReturn("860");
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(Matchers.any(ResponseDTO.class));
		
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(userCentersResponseDTO);
		userIntegrationEnhancedService.getDefaultPrimaryCenter(userCenterRequestDTO);
	}
}
