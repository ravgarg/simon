package com.simon.integration.email.populator;

import static org.mockito.MockitoAnnotations.initMocks;

import java.io.StringWriter;

import org.apache.commons.configuration.Configuration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.ShareEmailData;
import com.simon.integration.dto.email.deal.EmailOfferDTO;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShareEmailDTOPopulatorTest {
	@InjectMocks
	@Spy
	private ShareEmailDTOPopulator shareEmailDTOPopulator;
	
	@Mock
	private Converter<ShareEmailData,EmailOfferDTO> offerDTOConverter;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testpopulate()
	{
		final ShareEmailData source=mock(ShareEmailData.class);
		when(source.getEmailTo()).thenReturn("test@test.com,test1@test.com");
		final ShareEmailRequestDTO target=new ShareEmailRequestDTO();
		final EmailOfferDTO emailOffer=new EmailOfferDTO();
		doReturn(emailOffer).when(offerDTOConverter).convert(source);
		final Configuration configuration=mock(Configuration.class);
		when(configuration.getString(any())).thenReturn("Test Val");
		when(configuration.getString("share.product.offer.email.xml.type")).thenReturn("ShareXml");
		when(configurationService.getConfiguration()).thenReturn(configuration);
		doReturn("").when(shareEmailDTOPopulator).populateEmailDataInXml(emailOffer);
		shareEmailDTOPopulator.populate(source, target);
		assertEquals("ShareXml", target.getEmailAttributes().getXmlType());
	}
	
	@Test
	public void sharedEmailProductDTOPopulatorTest(){
		ShareEmailRequestDTO target = new ShareEmailRequestDTO();
		StringWriter stringWriter=new StringWriter();
		stringWriter.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><offerData><heading>Love \" this & Deal</heading></offerData>")	;
		
		final ShareEmailData source=mock(ShareEmailData.class);
		when(source.getEmailTo()).thenReturn("test@test.com,test1@test.com");
		final EmailOfferDTO emailOffer=new EmailOfferDTO();
		doReturn(emailOffer).when(offerDTOConverter).convert(source);
		final Configuration configuration=mock(Configuration.class);
		when(configuration.getString(any())).thenReturn("Test Val");
		
		when(configurationService.getConfiguration()).thenReturn(configuration);
		
		doReturn(emailOffer).when(offerDTOConverter).convert(source);
		
		when(shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter)).thenReturn("escapedXmlString");
		shareEmailDTOPopulator.populate(source, target);
		assertEquals("Test Val", target.getEmailAttributes().getXmlType());
		
	}

}
