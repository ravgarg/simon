package com.simon.integration.share.email.order.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeast;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.EmailOrderData;
import com.simon.facades.email.order.EmailOrderDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.facades.email.order.ShippingDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.order.EmailOrderRequestDTO;
import com.simon.integration.dto.email.order.EmailOrderRetailerDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailOrderDetailsDTOPopulatorTest {
	
	@InjectMocks
	private EmailOrderDetailsDTOPopulator emailOrderDetailsDTOPopulator;
	
	@Mock
	private Converter<RetailerDetailsData, EmailOrderRetailerDTO> emailOrderRetailerDetailsDTOConverter;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testPopulate(){
		final EmailOrderData source=mock(EmailOrderData.class);
				
		when(shareEmailIntegrationUtils.getConfiguredStaticString(SimonIntegrationConstants.WEBSITE_CONTEXT_ROOT)).thenReturn("siteurl");
		final EmailOrderDetailsData emailOrderDetailData=mock(EmailOrderDetailsData.class);
		when(source.getOrderDetail()).thenReturn(emailOrderDetailData);
		
		final ShippingDetailsData shippingDetailData=mock(ShippingDetailsData.class);
		when(shippingDetailData.getFirstName()).thenReturn("FirstName");
		when(source.getShippingDetail()).thenReturn(shippingDetailData);
		
		final List<RetailerDetailsData> retailerDataList=new ArrayList<>();
		final RetailerDetailsData retailerData=mock(RetailerDetailsData.class);
		retailerDataList.add(retailerData);
		when(source.getRetailersDetail()).thenReturn(retailerDataList);
		EmailOrderRequestDTO target=new EmailOrderRequestDTO();
		emailOrderDetailsDTOPopulator.populate(source, target);	
		verify(emailOrderRetailerDetailsDTOConverter,atLeast(1)).convertAll(retailerDataList);
	}

}
