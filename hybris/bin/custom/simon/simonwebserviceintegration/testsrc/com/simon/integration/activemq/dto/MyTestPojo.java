package com.simon.integration.activemq.dto;

import com.simon.integration.dto.PojoAnnotation;

public class MyTestPojo extends PojoAnnotation{

	String CartId;
	String ProductURL;
	
	public String getCartId() {
		return CartId;
	}
	public void setCartId(String cartId) {
		CartId = cartId;
	}
	public String getProductURL() {
		return ProductURL;
	}
	public void setProductURL(String productURL) {
		ProductURL = productURL;
	}
}
