package com.simon.integration.errorlog.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.generated.model.ErrorLogModel;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;




@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ErrorLogPopulatorTest {
	
	@InjectMocks
	private ErrorLogPopulator errorLogPopulator;
	
	@Mock
	private KeyGenerator guidKeyGenerator;
	
	@Before
	public void setUp() {
		initMocks(this);
	}
	
	@Test
	public void testpopulate_WhenSourceIsCart() {
		ErrorLogModel errorLogModel=new ErrorLogModel();
		CartModel cartModel=mock(CartModel.class);
		when(cartModel.getCode()).thenReturn("CartId_123");
		List<AbstractOrderEntryModel> entryList=new ArrayList<>();
		AbstractOrderEntryModel entryModel=mock(AbstractOrderEntryModel.class);
		ProductModel product=mock(ProductModel.class);
		when(product.getCode()).thenReturn("Prod123");
		when(entryModel.getProduct()).thenReturn(product);
		entryList.add(entryModel);
		AbstractOrderEntryModel entryModel1=mock(AbstractOrderEntryModel.class);
		ProductModel product1=mock(ProductModel.class);
		when(product1.getCode()).thenReturn("Prod1234");
		when(entryModel1.getProduct()).thenReturn(product1);
		entryList.add(entryModel1);
		when(cartModel.getEntries()).thenReturn(entryList);
		Exception exception=mock(Exception.class);
		when(guidKeyGenerator.generate()).thenReturn("123123");
		errorLogPopulator.populate(Pair.of(cartModel, exception), errorLogModel);
		assertEquals("CartId_123", errorLogModel.getId());
		assertEquals("Prod123,Prod1234",errorLogModel.getProductIds());
	}
	@Test
	public void testpopulate_WhenSourceIsOrder() {
		ErrorLogModel errorLogModel=new ErrorLogModel();
		OrderModel orderModel=mock(OrderModel.class);
		when(orderModel.getCode()).thenReturn("order_123");
		List<AbstractOrderEntryModel> entryList=new ArrayList<>();
		AbstractOrderEntryModel entryModel=mock(AbstractOrderEntryModel.class);
		ProductModel product=mock(ProductModel.class);
		when(product.getCode()).thenReturn("Prod123");
		when(entryModel.getProduct()).thenReturn(product);
		entryList.add(entryModel);
		when(orderModel.getEntries()).thenReturn(entryList);
		Exception exception=mock(Exception.class);
		when(guidKeyGenerator.generate()).thenReturn("123123");
		errorLogPopulator.populate(Pair.of(orderModel, exception), errorLogModel);
		assertEquals("order_123", errorLogModel.getId());
		
	}

}
