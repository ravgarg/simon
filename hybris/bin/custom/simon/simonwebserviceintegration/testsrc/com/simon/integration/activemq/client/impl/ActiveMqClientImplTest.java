
package com.simon.integration.activemq.client.impl;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jms.JmsException;
import org.springframework.jms.UncategorizedJmsException;
import org.springframework.jms.core.JmsTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.simon.integration.activemq.dto.MyMessage;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * The Class ActiveMqClientImplTest.
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class ActiveMqClientImplTest
{

    @InjectMocks
    private ActiveMqClientImpl activeMqClientImpl;

    @Mock
    private JmsTemplate jmsTemplate;

    @Mock
    private Map jmsTemplateRefrenceMap;

    private String command;

    private UncategorizedJmsException uncategorizedJmsException;

    private MyMessage message;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        message = new MyMessage();
        message.setStockCompany("testcompany");
        message.setStockName("test");
        command = "CREATE_CART";
        uncategorizedJmsException = Mockito.spy(new UncategorizedJmsException(""));
    }

    /**
     * Testsend.
     * 
     * @throws JsonProcessingException
     */
    @Test
    public void testsend() throws JsonProcessingException
    {
        when(jmsTemplateRefrenceMap.get(command)).thenReturn(jmsTemplate);
        Mockito.doNothing().when(jmsTemplate).convertAndSend(message);
        activeMqClientImpl.send(message, command);
        Mockito.verify(jmsTemplate, Mockito.times(1)).convertAndSend(Mockito.any());
    }

    @Test
    public void testsendWithHeader() throws JsonProcessingException
    {
        final Map<String, String> headers = new HashMap<>();
        headers.put("key", "value");
        when(jmsTemplateRefrenceMap.get(command)).thenReturn(jmsTemplate);
        Mockito.doNothing().when(jmsTemplate).convertAndSend(Mockito.any(Object.class), Mockito.any());
        activeMqClientImpl.send(message, command, headers);
        Mockito.verify(jmsTemplate, Mockito.times(1)).convertAndSend(Mockito.any(Object.class), Mockito.any());
    }

    @Test(expected = JmsException.class)
    public void testsendWithHeaderAndException() throws JsonProcessingException
    {
        final Map<String, String> headers = new HashMap<>();
        headers.put("key", "value");
        when(jmsTemplateRefrenceMap.get(command)).thenReturn(jmsTemplate);
        Mockito.doThrow(uncategorizedJmsException).when(jmsTemplate).convertAndSend(Mockito.any(Object.class), Mockito.any());
        activeMqClientImpl.send(message, command, headers);
    }

    /**
     * Testsend with jms exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = JmsException.class)
    public void testsendWithJmsException() throws JsonProcessingException
    {
        when(jmsTemplateRefrenceMap.get(command)).thenReturn(jmsTemplate);
        Mockito.doThrow(uncategorizedJmsException).when(jmsTemplate).convertAndSend(Mockito.any());
        activeMqClientImpl.send(message, command);
    }
}
