package com.simon.integration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.VariantAttributeMappingModel;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantAttributeMappingsUtilsTest {

	VariantAttributeMappingsUtils variantAttributeMappingsUtils;
	
	@Test
	public void testConverVariantAttributeMap()
	{
		List<VariantAttributeMappingModel> varientAttributeMappings=new ArrayList<>();
		VariantAttributeMappingModel  variantAttributeMappingModel=mock(VariantAttributeMappingModel.class);
		ShopModel shop=mock(ShopModel.class);
		when(shop.getId()).thenReturn("testShop1");
		when(variantAttributeMappingModel.getShop()).thenReturn(shop);
		when(variantAttributeMappingModel.getSourceAttribute()).thenReturn("color");
		when(variantAttributeMappingModel.getTargetAttribute()).thenReturn("Colour");
		varientAttributeMappings.add(variantAttributeMappingModel);
		varientAttributeMappings.add(variantAttributeMappingModel);
		Map<String, Map<String, String>> convertMap=VariantAttributeMappingsUtils.convertVariantAttributeMappingsMap(varientAttributeMappings);
		assertEquals("Colour", convertMap.get("testShop1").get("color"));
	}
}
