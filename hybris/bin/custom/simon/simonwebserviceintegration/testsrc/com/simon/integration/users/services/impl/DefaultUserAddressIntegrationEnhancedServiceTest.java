package com.simon.integration.users.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.MultiValueMap;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.ResponseDTO;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.dto.UserAddressDTO;
import com.simon.integration.users.dto.UserAddressResponseDTO;
import com.simon.integration.users.dto.UserPaymentAddressDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.session.SessionService;

/**
 * This is a JUnit class of DefaultUserAddressIntegrationEnhancedService
 *
 */

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultUserAddressIntegrationEnhancedServiceTest {

	private static final String ERROR_CODE = "111";

	@InjectMocks
	DefaultUserAddressIntegrationEnhancedService userAddressIntegrationEnhancedService;

	@Mock
	RestWebService restWebService;
	@Mock
	private SessionService sessionService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private MultiValueMap<String, Object> header;
	@Mock
	private IntegrationException integrationException;
	@Mock
	private RestAuthHeaderDTO authheader;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(userIntegrationUtils).handleCommonErrors(Matchers.any(ResponseDTO.class));
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_SERVICE_URL)).thenReturn("url");
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(authheader);
		when(userIntegrationUtils.populateHeadersForESB()).thenReturn(header);
	}

	@Test
	public void addUserAddressTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.addUserAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setSuccess("true");
		response.setMessage("User Address Added successfully");
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.addUserAddress(userAddressDTO);
		Assert.assertEquals("true", response.getSuccess());
		Assert.assertEquals("User Address Added successfully", response.getMessage());
	}
	
	@Test(expected = UserAddressIntegrationException.class)
	public void addUserAddressWithErrorCodeTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.addUserAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setSuccess("true");
		response.setMessage("User Address Added successfully");
		response.setErrorCode(ERROR_CODE);
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.addUserAddress(userAddressDTO);
	}


	@Test(expected = UserAddressIntegrationException.class)
	public void addUserAddressExceptionTest() throws UserAddressIntegrationException {

		String finalURL = "https://testapi.addUserAddress";
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setMessage("Error Test");
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);

		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userAddressIntegrationEnhancedService.addUserAddress(userAddressDTO);
	}
	
	
	@Test
	public void updateUserAddressTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.updateUserAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setSuccess("true");
		response.setMessage("User Address Updated successfully");
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_UPDATE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.updateUserAddress(userAddressDTO);
		Assert.assertEquals("true", response.getSuccess());
		Assert.assertEquals("User Address Updated successfully", response.getMessage());
	}
	
	@Test(expected = UserAddressIntegrationException.class)
	public void updateUserAddressWithErrorCodeTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.updateUserAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setErrorCode(ERROR_CODE);
		response.setSuccess("true");
		response.setMessage("User Address Updated successfully");
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_UPDATE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.updateUserAddress(userAddressDTO);
	}

	@Test(expected = UserAddressIntegrationException.class)
	public void updateUserAddressExceptionTest() throws UserAddressIntegrationException {

		String finalURL = "https://testapi.updateUserAddress";
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setMessage("Error Test");
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_UPDATE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);

		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userAddressIntegrationEnhancedService.updateUserAddress(userAddressDTO);
	}
	
	@Test
	public void removeUserAddressTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.removeUserAddress";
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setSuccess("true");
		response.setMessage("User Address Removed successfully");
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_REMOVE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.removeUserAddress(userAddressDTO);
		Assert.assertEquals("true", response.getSuccess());
		Assert.assertEquals("User Address Removed successfully", response.getMessage());
	}
	
	@Test(expected = UserAddressIntegrationException.class)
	public void removeUserAddressWithErrorCodeTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.removeUserAddress";
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setErrorCode(ERROR_CODE);
		response.setSuccess("true");
		response.setMessage("User Address Removed successfully");
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_REMOVE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.removeUserAddress(userAddressDTO);
	}
	
	@Test(expected = UserAddressIntegrationException.class)
	public void removeUserAddressExceptionTest() throws UserAddressIntegrationException {

		String finalURL = "https://testapi.removeUserAddress";
		UserAddressDTO userAddressDTO = mock(UserAddressDTO.class);
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setMessage("Test Error");
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_REMOVE_USER_ADDRESS_END_POINT))
						.thenReturn(finalURL);

		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userAddressIntegrationEnhancedService.removeUserAddress(userAddressDTO);
	}
	
	@Test
	public void addUserAddressPaymentTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.addUserPaymentAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setSuccess("true");
		response.setMessage("Payment Address Added successfully");
		UserPaymentAddressDTO userPaymentAddressDTO = mock(UserPaymentAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_PAYMENT_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.addUserPaymentAddress(userPaymentAddressDTO);
		Assert.assertEquals("true", response.getSuccess());
		Assert.assertEquals("Payment Address Added successfully", response.getMessage());
}
	
	@Test(expected = UserAddressIntegrationException.class)
	public void addUserAddressPaymentWithErrorCodeTest() throws UserAddressIntegrationException {
		String finalURL = "https://testapi.addUserPaymentAddress";
		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setErrorCode(ERROR_CODE);
		response.setSuccess("true");
		response.setMessage("Payment Address Added successfully");
		UserPaymentAddressDTO userPaymentAddressDTO = mock(UserPaymentAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_PAYMENT_ADDRESS_END_POINT))
						.thenReturn(finalURL);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		userAddressIntegrationEnhancedService.addUserPaymentAddress(userPaymentAddressDTO);
}

	@Test(expected = UserAddressIntegrationException.class)
	public void addUserAddressPaymentExceptionTest() throws UserAddressIntegrationException {

		UserAddressResponseDTO response = new UserAddressResponseDTO();
		response.setMessage("Test Error");
		String finalURL = "https://testapi.addUserPaymentAddress";
		UserPaymentAddressDTO userPaymentAddressDTO = mock(UserPaymentAddressDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(
				SimonIntegrationConstants.USER_ADDRESS_INTEGRATION_SERVICE_ADD_USER_PAYMENT_ADDRESS_END_POINT))
						.thenReturn(finalURL);

		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		userAddressIntegrationEnhancedService.addUserPaymentAddress(userPaymentAddressDTO);
	}
}
