package com.simon.integration.cart.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import junit.framework.Assert;

@UnitTest
public class CartEstimateRequestDataPopulatorTest {
	 
	@InjectMocks
	private CartEstimateRequestDataPopulator  cartEstimateRequestDataPopulator;
	
	private CartEstimateRequestDTO cartEstimateRequestDTO;
	@Mock
	private CartEstimateResponseDTO  cartEstimateResponseDTO;
	
	private CartModel source;
	
	
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final CategoryModel categoryModel2 = new CategoryModel();
		final ItemModelContextImpl itemModelContext1 = (ItemModelContextImpl) categoryModel2.getItemModelContext();
		itemModelContext1.setLocaleProvider(localeProvider);
		categoryModel2.setCode("categorycodekey");
		categoryModel2.setName("test");
		
		List<CategoryModel> categoryModelList2= new ArrayList<>();
		categoryModelList2.add(categoryModel2);
		
		final CategoryModel categoryModel = new CategoryModel();
		final ItemModelContextImpl itemModelContext2 = (ItemModelContextImpl) categoryModel.getItemModelContext();
		itemModelContext2.setLocaleProvider(localeProvider);
		categoryModel.setCode("categorycodekey");
		categoryModel.setName("test");
		categoryModel.setSupercategories(categoryModelList2);
		
		Collection<CategoryModel> categoryModelList= new ArrayList<>();
		categoryModelList.add(categoryModel);
		
		
		
		ShopModel shopModel= new ShopModel(); 
		shopModel.setId("1234");
		
		
		ProductModel productModel= new GenericVariantProductModel();
		productModel.setSupercategories(categoryModelList);
		productModel.setCode("1234");
		productModel.setShop(shopModel);
				
		CartEntryModel cartEntry= new CartEntryModel();
		cartEntry.setProduct(productModel);
		cartEntry.setQuantity(10L);
		
		List<AbstractOrderEntryModel> listEntry= new ArrayList<>();
		listEntry.add(cartEntry);
		listEntry.add(cartEntry);
	
		List<RetailersInfoModel> retailersInfoModels = new ArrayList<>();
		List<ShippingDetailsModel> shippingDetailsModels = new ArrayList<>();
		AdditionalCartInfoModel additionalCartInfoModel = new AdditionalCartInfoModel();

		RetailersInfoModel retailersInfoModel = new RetailersInfoModel();
		retailersInfoModel.setRetailerId("1234");
		
		ShippingDetailsModel shippingDetailsModel = new ShippingDetailsModel();
		shippingDetailsModel.setSelected(true);
		
		shippingDetailsModels.add(shippingDetailsModel);
		
		retailersInfoModel.setShippingDetails(shippingDetailsModels);
		retailersInfoModels.add(retailersInfoModel);
		additionalCartInfoModel.setRetailersInfo(retailersInfoModels);;
		
		source= new CartModel();	
		source.setEntries(listEntry);
		source.setCode("1234");
		source.setAdditionalCartInfo(additionalCartInfoModel);
		
		cartEstimateRequestDTO= new CartEstimateRequestDTO();
		
	}
	/**
	 * This method test populate method of CartEstimateRequestDataPopulator
	 * @throws JsonProcessingException 
	 */
	@Test
	public void testPopulateCartEstimateRequestDTO() {
		final AddressModel deliveryAddress=mock(AddressModel.class);
		final RegionModel region=mock(RegionModel.class);
		source.setDeliveryAddress(deliveryAddress);
		when(deliveryAddress.getTown()).thenReturn("City");
		when(deliveryAddress.getPostalcode()).thenReturn("12345");
		when(deliveryAddress.getRegion()).thenReturn(region);
		when(deliveryAddress.getRegion().getName()).thenReturn("NewYork");
		
		cartEstimateRequestDataPopulator.populate(source, cartEstimateRequestDTO); 
		Assert.assertEquals(cartEstimateRequestDTO.getRetailers().size(),1);
		
	}
}
