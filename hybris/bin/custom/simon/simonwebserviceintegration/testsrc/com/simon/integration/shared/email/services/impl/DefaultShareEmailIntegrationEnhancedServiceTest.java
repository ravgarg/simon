package com.simon.integration.shared.email.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.product.ShareEmailAttributesDTO;
import com.simon.integration.email.SharedEmailResponseDTO;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.integration.share.email.services.impl.DefaultShareEmailIntegrationEnhancedService;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultShareEmailIntegrationEnhancedServiceTest {

	private static final String SERVICE_URL = "serviceUrl";
	private static final String ESB_CRED = "esbCredentials";
	private static final String ERROR_CODE = "errorCode";
	@InjectMocks
	private DefaultShareEmailIntegrationEnhancedService shareEmailIntegrationEnhancedService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private RestWebService restWebService;
	@Mock
	private SharedEmailResponseDTO response;
	@Mock
	private ShareEmailAttributesDTO shareEmailAttributesDTO;
	@Mock
	private RestAuthHeaderDTO restAuthHeaderDTO;
	
	@Test
	public void sharedProductEmailTest() throws SharedEmailIntegrationException{
		ShareEmailRequestDTO sharedProductEmailRequestDTO = mock(ShareEmailRequestDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.SHARED_EMAIL_INTEGRATION_SERVICE_END_POINT)).thenReturn(SERVICE_URL);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn(ESB_CRED);
		when(sharedProductEmailRequestDTO.getEmailAttributes()).thenReturn(shareEmailAttributesDTO);
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(restAuthHeaderDTO);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		shareEmailIntegrationEnhancedService.shareEmail(sharedProductEmailRequestDTO);
		verify(restWebService).executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}
	
	@Test(expected=SharedEmailIntegrationException.class)
	public void sharedProductEmailResponseErrorCodeTest() throws SharedEmailIntegrationException{
		ShareEmailRequestDTO sharedProductEmailRequestDTO = mock(ShareEmailRequestDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.SHARED_EMAIL_INTEGRATION_SERVICE_END_POINT)).thenReturn(SERVICE_URL);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn(ESB_CRED);
		when(sharedProductEmailRequestDTO.getEmailAttributes()).thenReturn(shareEmailAttributesDTO);
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(restAuthHeaderDTO);
		when(response.getErrorCode()).thenReturn(ERROR_CODE);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(response);
		shareEmailIntegrationEnhancedService.shareEmail(sharedProductEmailRequestDTO);
		verify(restWebService).executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
	}
	
	@Test(expected=SharedEmailIntegrationException.class)
	public void sharedProductEmailResponseErrorTest() throws SharedEmailIntegrationException{
		ShareEmailRequestDTO sharedProductEmailRequestDTO = mock(ShareEmailRequestDTO.class);
		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.SHARED_EMAIL_INTEGRATION_SERVICE_END_POINT)).thenReturn(SERVICE_URL);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn(ESB_CRED);
		when(sharedProductEmailRequestDTO.getEmailAttributes()).thenReturn(shareEmailAttributesDTO);
		when(userIntegrationUtils.populateAuthHeaderForESB()).thenReturn(restAuthHeaderDTO);
		when(response.getErrorCode()).thenReturn(ERROR_CODE);
		when(restWebService.executeRestEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(new IntegrationException("Exception"));
		shareEmailIntegrationEnhancedService.shareEmail(sharedProductEmailRequestDTO);
	}
}
