package com.simon.integration.users.address.verify.populators;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.dto.users.address.verify.UserAddressVerifyRequestDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;

@UnitTest
public class UserAddressVerifyRequestDTOPopulatorTest {

	@InjectMocks
	private UserAddressVerifyRequestDTOPopulator userAddressVerifyRequestDataPopulator;
	
	private ExtAddressData source;
	@Before
	public void setUp() {
		source = new ExtAddressData();
		
		source.setFirstName("User First Name");
		source.setLastName("User Last Name");
		source.setLine1("Address Line 1");
		source.setLine2("Address Line 2");
		source.setCompanyName("User Company Name");
		source.setEmail("test@test.com");
		source.setPhone("1234567898");
		source.setPostalCode("10001");
		source.setTown("User City");
		
		RegionData region = new RegionData();
		region.setIsocodeShort("NY");
		
		CountryData country = new CountryData();
		country.setIsocode("US");
		
		source.setRegion(region);
		source.setCountry(country);
		
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testUserAddressVerifyRequestDTO(){
		UserAddressVerifyRequestDTO target = new UserAddressVerifyRequestDTO();
		
		userAddressVerifyRequestDataPopulator.populate(source, target);
		Assert.assertEquals(source.getLine1(), target.getStreet1());
		Assert.assertEquals(source.getEmail(), target.getEmail());
	}
}
