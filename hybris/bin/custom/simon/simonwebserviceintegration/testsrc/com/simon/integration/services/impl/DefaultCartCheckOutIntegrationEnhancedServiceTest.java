package com.simon.integration.services.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.cart.cartestimate.CartEstimateRequestDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.createcart.CreateCartDTO;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class DefaultCartCheckOutIntegrationEnhancedServiceTest {

	@InjectMocks
	@Spy
	private DefaultCartCheckOutIntegrationEnhancedService defaultIntegrationEnhancedService;
	@Mock
	private RestWebService restWebService;
	@Mock
	private Configuration configuration;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private RestAuthHeaderDTO restAuthHeaderDTO;
	@Mock
	private MultiValueMap<String, Object> headersMap;
	@Mock
	private CartEstimateResponseDTO cartEstimateResponseDTO;
	@Mock
	private CartEstimateRequestDTO cartEstimateRequestDTO;
	@Mock
	private CreateCartDTO createCartDTO;
	@Mock
	private EnqueueService enqueueService;

	private String serviceURl;
	private String stub;
	private String stubFile;

	/*
	 * @Before public void setUp() {
	 *
	 * }
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		serviceURl = "http://demo8779820.mockable.io/restcall";
		stub="true";
		stubFile="/stub/EstimateCart.json";

	}

	/**
	 * This method test CartEstimateCall method .
	 */
	@Test
	public void testCartEstimateCall() {
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.ESTIMATE_CART_URL)).thenReturn(serviceURl);
		when(configuration.getString(SimonIntegrationConstants.ESTIMATE_CART_STUB)).thenReturn(stub);
		when(configuration.getString(SimonIntegrationConstants.ESTIMATE_CART_STUB_FILE)).thenReturn(stubFile);

		when(userIntegrationUtils.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.CARD_VERIFY_URL)).thenReturn(serviceURl);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue()).thenReturn("simonTestEsbToken");
		
		when(restWebService.executeRestEvent(serviceURl, cartEstimateRequestDTO,stub ,stubFile,CartEstimateResponseDTO.class,
				RequestMethod.POST, headersMap, restAuthHeaderDTO)).thenReturn(cartEstimateResponseDTO);
		defaultIntegrationEnhancedService.estimateCart(cartEstimateRequestDTO);
		verify(defaultIntegrationEnhancedService).estimateCart(cartEstimateRequestDTO);
	}

	/**
	 * This method test Create cart method.
	 */
	@Test
	public void testCreateCartCall() {

		Mockito.doNothing().when(enqueueService).sendObjectMessage(createCartDTO,JmsTemplateNameEnum.CREATE_CART);
		defaultIntegrationEnhancedService.createCart(createCartDTO);
		verify(enqueueService).sendObjectMessage(createCartDTO,JmsTemplateNameEnum.CREATE_CART);

	}

}
