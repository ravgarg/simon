package com.simon.integration.users.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.customer.data.MallData;
import com.simon.integration.users.login.dto.UserCenterIdDTO;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class UserCenterIdDataPopulatorTest{
	
	@InjectMocks
	private UserCenterIdDataPopulator userCenterIdDataPopulator;
	
	@Test
	public void UserCenterIdDataPopulatTest(){
		UserCenterIdDTO  userCenterIdDTO = mock(UserCenterIdDTO.class);
		MallData centerIdData =  new MallData();
		centerIdData.setCode("1");
		centerIdData.setName("Test");
		when(userCenterIdDTO.getOutletName()).thenReturn("Test");
		userCenterIdDataPopulator.populate(centerIdData, userCenterIdDTO);
		Assert.assertEquals("Test", userCenterIdDTO.getOutletName());
	}
}