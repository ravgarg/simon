package com.simon.integration.share.email.product.populators;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.product.EmailProductDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.product.EmailProductRetailerDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SharedEmailProductDetailsDTOPopulatorTest {

	@InjectMocks
	@Spy
	SharedEmailProductDetailsDTOPopulator sharedEmailProductDetailsDTOPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Mock
	private EmailProductDetailsData source; 
	
	
	@Test
	public void sharedEmailProductDetailsDTOPopulatorTest(){
		
		source.setRetailerName("Test Shop 1");
		source.setMsrp("123.33");
		source.setProductName("Test Product");
		source.setQuantity("1");
		source.setSalePrice("112.32");
		source.setColor("Red");
		source.setPercentageOff("Up to 10 %");
		source.setImageUrl("/abcd");
		
		EmailProductRetailerDTO target = mock(EmailProductRetailerDTO.class);
		
		when(source.getImageUrl()).thenReturn("imageUrl");
		when(shareEmailIntegrationUtils.buildImageUrl("imageUrl")).thenReturn("abcd");
		
		when(source.getShopNowLinkUrl()).thenReturn("shopUrl");
		when(shareEmailIntegrationUtils.buildShopNowLabel("shopUrl")).thenReturn("xyz");
		sharedEmailProductDetailsDTOPopulator.populate(source, target);
		
		Assert.assertNotNull(target);
		Mockito.verify(source, Mockito.atLeastOnce()).setColor("Red");		
	}
}
