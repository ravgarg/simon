package com.simon.integration.order.deliver.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;

import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.integration.dto.order.deliver.CartProductsDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.dto.order.deliver.RetailerDTO;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;


@UnitTest
public class DeliverOrderDataVariantAttributePopulatorTest {

	@InjectMocks
	private DeliverOrderDataVariantAttributePopulator deliverOrderDataVariantAttributePopulator;

	@Mock
	private DeliverOrderRequestDTO deliverOrderRequestDTO;
	@Mock
	private OrderModel orderModel;

	private List<VariantAttributeMappingModel> variantAttributeMappingList;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	private void populateData() {
		ShopModel shopModel = mock(ShopModel.class);
		when(shopModel.getId()).thenReturn("shop1234");

		variantAttributeMappingList = new ArrayList<>();
		VariantAttributeMappingModel variantAttributeModal = mock(VariantAttributeMappingModel.class);
		when(variantAttributeModal.getSourceAttribute()).thenReturn("categoryValue");
		when(variantAttributeModal.getTargetAttribute()).thenReturn("TargetcodevalueName");
		when(variantAttributeModal.getShop()).thenReturn(shopModel);
		deliverOrderRequestDTO=mock(DeliverOrderRequestDTO.class);
		List<RetailerDTO> retailers = new ArrayList<>();
		RetailerDTO retailerDTO = mock(RetailerDTO.class);
		retailers.add(retailerDTO);
		List<CartProductsDTO> products = new ArrayList<>();
		CartProductsDTO cartProductsDTO = mock(CartProductsDTO.class);
		products.add(cartProductsDTO);
		retailerDTO.setProducts(products);
		when(retailerDTO.getRetailerId()).thenReturn("shop1234");
		when(retailerDTO.getProducts()).thenReturn(products);

		Map<String, String> variants = new HashMap<>();
		variants.put("superCategorycodevalueName", "categoryValue");
		when(cartProductsDTO.getProductAttributeMap()).thenReturn(variants);
		when(deliverOrderRequestDTO.getRetailers()).thenReturn(retailers);
	}

	/**
	 * This method test populate method of DeliverOrderDataPopulator
	 */
	@Test
	public void testpopulate() {
		populateData();
		deliverOrderDataVariantAttributePopulator.populate(ImmutablePair.of(orderModel, variantAttributeMappingList),
				deliverOrderRequestDTO);
		assertEquals("categoryValue", deliverOrderRequestDTO.getRetailers().get(0).getProducts().get(0)
				.getProductAttributeMap().get("superCategorycodevalueName"));
	}
}
