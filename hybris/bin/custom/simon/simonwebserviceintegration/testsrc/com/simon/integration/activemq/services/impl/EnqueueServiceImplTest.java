
package com.simon.integration.activemq.services.impl;

import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.jms.UncategorizedJmsException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.simon.core.exceptions.IntegrationException;
import com.simon.integration.activemq.client.impl.ActiveMqClientImpl;
import com.simon.integration.activemq.dto.MyMessage;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;

import de.hybris.bootstrap.annotations.UnitTest;

/**
 * The Class ActiveMqServiceImplTest.
 */

@UnitTest
public class EnqueueServiceImplTest
{

    @InjectMocks
    EnqueueServiceImpl activeMqServiceImpl;

    @Mock
    private ActiveMqClientImpl activeMqClientImpl;

    @Mock
    private Map jmsTemplateRefrenceMap;

    @Mock
    private MyMessage myMessage;

    @Mock
    private HashMap<String, String> mapMessage;

    @Mock
    private UncategorizedJmsException uncategorizedJmsException;

    private String stringMessage;

    private byte[] byteMessage;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        stringMessage = "hello";
        byteMessage = new byte[5];

    }

    /**
     * This method test populate method with simon product.
     * 
     * @throws JsonProcessingException
     */
    @Test
    public void testsendObjectMessage() throws JsonProcessingException
    {
        activeMqServiceImpl.sendObjectMessage(myMessage, JmsTemplateNameEnum.CREATE_CART);
        verify(activeMqClientImpl).send(myMessage, JmsTemplateNameEnum.CREATE_CART.name());
    }

    /**
     * Test send object message with exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = IntegrationException.class)
    public void testsendObjectMessageWithException() throws JsonProcessingException
    {
        Mockito.doThrow(uncategorizedJmsException).when(activeMqClientImpl).send(myMessage, JmsTemplateNameEnum.CREATE_CART.name());
        activeMqServiceImpl.sendObjectMessage(myMessage, JmsTemplateNameEnum.CREATE_CART);

    }

    /**
     * Test send string message.
     * 
     * @throws JsonProcessingException
     */
    @Test
    public void testsendStringMessage() throws JsonProcessingException
    {
        activeMqServiceImpl.sendStringMessage(stringMessage, JmsTemplateNameEnum.CREATE_CART);
        verify(activeMqClientImpl).send(stringMessage, JmsTemplateNameEnum.CREATE_CART.name());
    }

    /**
     * Test send string message with exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = IntegrationException.class)
    public void testsendStringMessageWithException() throws JsonProcessingException
    {
        Mockito.doThrow(uncategorizedJmsException).when(activeMqClientImpl).send(stringMessage, JmsTemplateNameEnum.CREATE_CART.name());
        activeMqServiceImpl.sendStringMessage(stringMessage, JmsTemplateNameEnum.CREATE_CART);

    }

    /**
     * Test send bytes array message.
     * 
     * @throws JsonProcessingException
     */
    @Test
    public void testsendBytesArrayMessage() throws JsonProcessingException
    {
        activeMqServiceImpl.sendBytesArrayMessage(byteMessage, JmsTemplateNameEnum.CREATE_CART);
        verify(activeMqClientImpl).send(byteMessage, JmsTemplateNameEnum.CREATE_CART.name());

    }

    /**
     * Test send bytes array message with exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = IntegrationException.class)
    public void testsendBytesArrayMessageWithException() throws JsonProcessingException
    {
        Mockito.doThrow(uncategorizedJmsException).when(activeMqClientImpl).send(byteMessage, JmsTemplateNameEnum.CREATE_CART.name());
        activeMqServiceImpl.sendBytesArrayMessage(byteMessage, JmsTemplateNameEnum.CREATE_CART);
    }

    /**
     * Test send map message.
     * 
     * @throws JsonProcessingException
     */
    @Test
    public void testsendMapMessage() throws JsonProcessingException
    {
        activeMqServiceImpl.sendMapMessage(mapMessage, JmsTemplateNameEnum.CREATE_CART);
        verify(activeMqClientImpl).send(mapMessage, JmsTemplateNameEnum.CREATE_CART.name());
    }

    /**
     * Test send map message with exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = IntegrationException.class)
    public void testsendMapMessageWithException() throws JsonProcessingException
    {
        Mockito.doThrow(uncategorizedJmsException).when(activeMqClientImpl).send(mapMessage, JmsTemplateNameEnum.CREATE_CART.name());
        activeMqServiceImpl.sendMapMessage(mapMessage, JmsTemplateNameEnum.CREATE_CART);
    }

    @Test
    public void testsendObjectMessageWithHeader() throws JsonProcessingException
    {
        final Map<String, String> headers = new HashMap<>();
        headers.put("key", "value");
        activeMqServiceImpl.sendObjectMessage(myMessage, JmsTemplateNameEnum.CREATE_CART, headers);
        verify(activeMqClientImpl).send(myMessage, JmsTemplateNameEnum.CREATE_CART.name(), headers);
    }

    /**
     * Test send object message with exception.
     * 
     * @throws JsonProcessingException
     */
    @Test(expected = IntegrationException.class)
    public void testsendObjectMessageWithHeaderAndException() throws JsonProcessingException
    {
        final Map<String, String> headers = new HashMap<>();
        headers.put("key", "value");
        Mockito.doThrow(uncategorizedJmsException).when(activeMqClientImpl).send(myMessage, JmsTemplateNameEnum.CREATE_CART.name(), headers);
        activeMqServiceImpl.sendObjectMessage(myMessage, JmsTemplateNameEnum.CREATE_CART, headers);

    }
}
