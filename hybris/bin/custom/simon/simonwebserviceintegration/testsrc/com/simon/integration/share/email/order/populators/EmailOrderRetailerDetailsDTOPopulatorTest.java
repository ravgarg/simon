package com.simon.integration.share.email.order.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.atLeast;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.ArrayList;
import java.util.List;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.order.ProductDetailsData;
import com.simon.facades.email.order.RetailerDetailsData;
import com.simon.integration.dto.email.order.EmailOrderRetailerDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class EmailOrderRetailerDetailsDTOPopulatorTest {
	
	@InjectMocks
	private EmailOrderRetailerDetailsDTOPopulator emailOrderRetailerDetailsDTOPopulator;
	
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testPopulate(){
		RetailerDetailsData source=mock(RetailerDetailsData.class);
		when(source.getName()).thenReturn("TestName");
		List<ProductDetailsData> productDetailDataList=new ArrayList<>();
		ProductDetailsData productDetailData=mock(ProductDetailsData.class);
		when(productDetailData.getImageUrl()).thenReturn("imageUrl");
		productDetailDataList.add(productDetailData);
		when(source.getProductsDetail()).thenReturn(productDetailDataList);
		EmailOrderRetailerDTO target=new EmailOrderRetailerDTO();
		emailOrderRetailerDetailsDTOPopulator.populate(source, target);
		verify(shareEmailIntegrationUtils,atLeast(1)).buildImageUrl("imageUrl");
	}

}
