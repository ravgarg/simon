package com.simon.integration.activemq.dto;

import java.io.Serializable;

import com.simon.integration.dto.PojoAnnotation;


public class MyMessage extends PojoAnnotation implements Serializable
{
	/**
	 *
	 */
	private static long serialVersionUID = 1L;
	int stockPrice;
	String stockName;
	String stockCompany;

	public static long getSerialVersionUID()
	{
		return serialVersionUID; 
	}

	public static void setSerialVersionUID(final long serialVersionUID)
	{
		MyMessage.serialVersionUID = serialVersionUID;
	}

	public int getStockPrice()
	{
		return stockPrice;
	}

	public void setStockPrice(final int stockPrice)
	{
		this.stockPrice = stockPrice;
	}

	public String getStockName()
	{
		return stockName;
	}

	public void setStockName(final String stockName)
	{
		this.stockName = stockName;
	}

	public String getStockCompany()
	{
		return stockCompany;
	}

	public void setStockCompany(final String stockCompany)
	{
		this.stockCompany = stockCompany;
	}



}