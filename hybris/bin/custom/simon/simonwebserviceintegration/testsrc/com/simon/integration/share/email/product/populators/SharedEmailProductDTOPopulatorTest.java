package com.simon.integration.share.email.product.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.product.EmailProductData;
import com.simon.facades.email.product.EmailProductDetailsData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.email.product.EmailProductRetailerDTO;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.product.ShareSubscriberDetails;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SharedEmailProductDTOPopulatorTest {

	private static final String SHARE_PROUDCT_EMAIL_CUSTOMER_KEY = "share.product.email.customer.key";
	private static final String SHARE_PROUDCT_EMAIL_SUBJECT_LINE = "share.product.email.subject.line";
	
	@InjectMocks
	@Spy
	SharedEmailProductDTOPopulator sharedEmailProductDTOPopulator;
	@Mock
	private Converter<EmailProductDetailsData, EmailProductRetailerDTO> sharedEmailProductDetailsDTOConverter;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	
	@Mock
	private ShareEmailData source;
	@Mock
	private List<ShareSubscriberDetails> subscriberList = new ArrayList<>();
	@Mock
	private ShareSubscriberDetails subscriber;
	@Mock
	private EmailProductRetailerDTO emailProductRetailerDto;
	
	@Test
	public void sharedEmailProductDTOPopulatorTest(){
		ShareEmailRequestDTO target = mock(ShareEmailRequestDTO.class);
		StringWriter stringWriter=new StringWriter();
		stringWriter.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?><offerData><heading>Love \" this & Deal</heading></offerData>")	;
		
		EmailProductDetailsData emailProductDetailsData = mock(EmailProductDetailsData.class);
		
		EmailProductData emailProductData = mock(EmailProductData.class);
		emailProductData.setProductDetails(emailProductDetailsData);

		source.setEmailProductData(emailProductData);
		source.setEmailFrom("test@test.com");
		subscriberList.add(subscriber);
		when(source.getEmailProductData()).thenReturn(emailProductData);
		when(emailProductData.getProductDetails()).thenReturn(emailProductDetailsData);
		when(shareEmailIntegrationUtils.getConfiguredStaticString(SHARE_PROUDCT_EMAIL_CUSTOMER_KEY)).thenReturn("customer_key");
		when(shareEmailIntegrationUtils.getConfiguredStaticString(SHARE_PROUDCT_EMAIL_SUBJECT_LINE)).thenReturn("subject_line");
		when(shareEmailIntegrationUtils.getConfiguredStaticString(SimonIntegrationConstants.SHARE_PROUDCT_OFFER_EMAIL_XML_TYPE)).thenReturn("product_email_xml_type");
		when(shareEmailIntegrationUtils.populateShareSubscriberDetails(source)).thenReturn(subscriberList);
		when(sharedEmailProductDetailsDTOConverter.convert(source.getEmailProductData().getProductDetails())).thenReturn(emailProductRetailerDto);
		when(shareEmailIntegrationUtils.convertShareEmailXmlDataInString(stringWriter)).thenReturn("escapedXmlString");
		sharedEmailProductDTOPopulator.populate(source, target);
		
		Assert.assertNotNull(target);
		Mockito.verify(source, Mockito.atLeastOnce()).setEmailFrom("test@test.com");
	}
	
}
