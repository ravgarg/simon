package com.simon.integration.users.favourites.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.users.favourites.dto.AddUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.RemoveUserFavouriteRequestDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteDTO;
import com.simon.integration.users.favourites.dto.UserFavouriteResponseDTO;
import com.simon.integration.users.favourites.services.UserFavouritesIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class DefaultUserFavouritesIntegrationServiceTest
{
	private static final String BAD_REQUEST = "708";
	private static final String VALUE = "Value";
	private static final String ID = "ID";
	
	@InjectMocks
	private DefaultUserFavouritesIntegrationService defaultUserFavouritesIntegrationService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private UserFavouritesIntegrationEnhancedService userFavouritesIntegrationEnhancedService;
	@Mock
	private Configuration config;
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(config);
		when(config.getString(Matchers.anyString())).thenReturn("url");
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(false);
		when(userFavouritesIntegrationEnhancedService.addUserFavourite(Matchers.any(AddUserFavouriteRequestDTO.class))).thenReturn(true);
		when(userFavouritesIntegrationEnhancedService.removeFavourite(Matchers.any(RemoveUserFavouriteRequestDTO.class))).thenReturn(true);
		UserFavouriteResponseDTO response=new UserFavouriteResponseDTO();
		List<UserFavouriteDTO> externalIds=new ArrayList<>();
		UserFavouriteDTO fav=new UserFavouriteDTO();
		fav.setExternalID(ID);
		externalIds.add(fav);
		response.setDesigners(externalIds);
		response.setOffers(externalIds);
		response.setProducts(externalIds);
		response.setRetailers(externalIds);
		response.setSizes(externalIds);
		when(userFavouritesIntegrationEnhancedService.getFavourites(Matchers.anyString())).thenReturn(response);
	}

	@Test
	public void testAddUserFavouriteProduct()
	{	
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteProduct(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteSize()
	{
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteSize(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteDesigner()
	{
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteDesigner(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteStore()
	{
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteStore(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteOffer()
	{
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteOffer(ID, VALUE));	
	}

	@Test
	public void testRemoveUserFavouriteProduct()
	{
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteProduct(ID));
	}

	@Test
	public void testRemoveUserFavouriteSize()
	{
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteSize(ID));
	}

	@Test
	public void testRemoveUserFavouriteDesigner()
	{
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteDesigner(ID));
	}

	@Test
	public void testRemoveUserFavouriteStore()
	{
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteStore(ID));
	}

	@Test
	public void testRemoveUserFavouriteOffer()
	{
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteOffer(ID));
	}
	@Test
	public void testAddUserFavouriteProductMock()
	{	
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteProduct(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteSizeMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteSize(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteDesignerMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteDesigner(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteStoreMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteStore(ID, VALUE));	
	}

	@Test
	public void testAddUserFavouriteOfferMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.addUserFavouriteOffer(ID, VALUE));	
	}

	@Test
	public void testRemoveUserFavouriteProductMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteProduct(ID));
	}

	@Test
	public void testRemoveUserFavouriteSizeMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteSize(ID));
	}

	@Test
	public void testRemoveUserFavouriteDesignerMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteDesigner(ID));
	}

	@Test
	public void testRemoveUserFavouriteStoreMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteStore(ID));
	}

	@Test
	public void testRemoveUserFavouriteOfferMock()
	{
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultUserFavouritesIntegrationService.removeUserFavouriteOffer(ID));
	}

	
	@Test
	public void testGetAllFavouriteProducts()
	{
		assertEquals(ID, defaultUserFavouritesIntegrationService.getAllFavouriteProducts().get(0));
	}
	
	@Test(expected=UserFavouritesIntegrationException.class)
	public void testGetAllFavouriteProductsWithException()
	{
		when(userFavouritesIntegrationEnhancedService.getFavourites(Matchers.anyString())).thenThrow(new UserFavouritesIntegrationException(""));
		defaultUserFavouritesIntegrationService.getAllFavouriteProducts();
	}

	@Test
	public void testGetAllFavouriteSizes()
	{
		assertEquals(ID, defaultUserFavouritesIntegrationService.getAllFavouriteSizes().get(0));
	}
	
	@Test
	public void testGetAllFavouriteDesigners()
	{
		assertEquals(ID, defaultUserFavouritesIntegrationService.getAllFavouriteDesigners().get(0));
	}

	@Test
	public void testGetAllFavouriteStores()
	{
		assertEquals(ID, defaultUserFavouritesIntegrationService.getAllFavouriteStores().get(0));
	}

	
	@Test
	public void testGetAllFavouriteOffers()
	{
		assertEquals(ID, defaultUserFavouritesIntegrationService.getAllFavouriteOffers().get(0));
	}
}
