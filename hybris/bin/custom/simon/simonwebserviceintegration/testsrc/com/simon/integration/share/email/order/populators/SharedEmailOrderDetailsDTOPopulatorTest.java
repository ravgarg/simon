package com.simon.integration.share.email.order.populators;

import static org.mockito.MockitoAnnotations.initMocks;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.email.order.EmailOrderData;
import com.simon.integration.dto.email.ShareEmailRequestDTO;
import com.simon.integration.dto.email.order.EmailOrderRequestDTO;
import com.simon.integration.utils.ShareEmailIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SharedEmailOrderDetailsDTOPopulatorTest {
	
	@InjectMocks
	private SharedEmailOrderDetailsDTOPopulator sharedEmailOrderDetailsDTOPopulator;
	@Mock
	private ShareEmailIntegrationUtils shareEmailIntegrationUtils;
	@Mock
	private Converter<EmailOrderData, EmailOrderRequestDTO> emailOrderDetailsDTOConverter;
	
	@Before
	public void setUp()
	{
		initMocks(this);

	}
	
	@Test
	public void testPopulate(){
		ShareEmailData source=mock(ShareEmailData.class);
		when(source.getEmailTo()).thenReturn("toemail@testEmail.com");
		EmailOrderData orderData=mock(EmailOrderData.class);
		when(source.getEmailOrderData()).thenReturn(orderData);
		EmailOrderRequestDTO emailRequestDTO=mock(EmailOrderRequestDTO.class);
		when(emailOrderDetailsDTOConverter.convert(orderData)).thenReturn(emailRequestDTO);
		ShareEmailRequestDTO target=new ShareEmailRequestDTO();
		when(shareEmailIntegrationUtils.getConfiguredStaticString("share.order.email.xml.type")).thenReturn("TextXml");
		sharedEmailOrderDetailsDTOPopulator.populate(source, target);
		assertEquals("TextXml", target.getEmailAttributes().getXmlType());
	}
	

}
