package com.simon.integration.payment.services.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMethod;

import com.integration.client.RestWebService;
import com.simon.core.dto.RestAuthHeaderDTO;
import com.simon.integration.activemq.enums.JmsTemplateNameEnum;
import com.simon.integration.activemq.services.EnqueueService;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.dto.card.verification.CardVerificationRequestDTO;
import com.simon.integration.dto.card.verification.CardVerificationResponseDTO;
import com.simon.integration.dto.order.deliver.DeliverOrderRequestDTO;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.utils.UserIntegrationUtils;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

@UnitTest
public class DefaultCardPaymentIntegrationEnhancedServiceTest {

	@InjectMocks
	@Spy
	DefaultCardPaymentIntegrationEnhancedService defaultCardPaymentIntegrationEnhancedService;
	@Mock
	private RestWebService restWebService;
	@Mock
	private Configuration configuration;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private UserIntegrationUtils userIntegrationUtils;
	@Mock
	private RestAuthHeaderDTO restAuthHeaderDTO;
	@Mock
	private MultiValueMap<String, Object> headersMap;
	@Mock
	private CardVerificationRequestDTO cardVerificationRequestDTO;
	@Mock
	private CardVerificationResponseDTO cardVerificationResponseDTO;

	@Mock
	private DeliverOrderRequestDTO deliverOrderRequestDTO;
	@Mock
	private EnqueueService enqueueService;

	private String serviceURl;
	private String stubCall;
	private String stubFileName;
	@Mock
	private MultiValueMap<String, Object> map;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		serviceURl = "http://demotest.com";
		stubCall = "true";
		stubFileName = "stubs/CardVerificationResponse.json";
	}

	/**
	 * This method test InvokeCartEstimateRequest method .
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testInvokeCardVerification() throws CartCheckOutIntegrationException {
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonIntegrationConstants.CARD_VERIFY_URL)).thenReturn(serviceURl);
		when(configuration.getString(SimonIntegrationConstants.CARD_VERIFY_STUB)).thenReturn(stubCall);
		when(configuration.getString(SimonIntegrationConstants.CARD_VERIFY_STUB_FILE)).thenReturn(stubFileName);
		when(userIntegrationUtils
				.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.CARD_VERIFY_URL))
						.thenReturn(serviceURl);
		when(userIntegrationUtils.getEsbIntegrationServiceHeaderAuthorizationValue())
				.thenReturn("simonTestEsbToken");
		when(userIntegrationUtils
				.buildEsbLayerIntegrationServiceEndpointApiUrl(SimonIntegrationConstants.CARD_VERIFY_URL))
						.thenReturn(serviceURl);
		when(restWebService.executeRestEvent(serviceURl, cardVerificationRequestDTO, stubCall, stubFileName,
				CardVerificationResponseDTO.class, RequestMethod.POST,
				defaultCardPaymentIntegrationEnhancedService.getHeaders(), null))
						.thenReturn(cardVerificationResponseDTO);
		Assert.assertEquals(defaultCardPaymentIntegrationEnhancedService.verifyCard(cardVerificationRequestDTO),
				cardVerificationResponseDTO);
	}

	/**
	 * This method test Invoke Invoke Deliver Order method .
	 * 
	 * @throws CartCheckOutIntegrationException
	 */
	@Test
	public void testInvokeDeliverOrder() throws CartCheckOutIntegrationException {

		Mockito.doNothing().when(enqueueService).sendObjectMessage(deliverOrderRequestDTO,
				JmsTemplateNameEnum.PLACE_ORDER);
		defaultCardPaymentIntegrationEnhancedService.deliverOrder(deliverOrderRequestDTO);
		verify(enqueueService).sendObjectMessage(deliverOrderRequestDTO, JmsTemplateNameEnum.PLACE_ORDER);

	}
}
