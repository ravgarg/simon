package com.simon.integration.users.oauth.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.users.login.dto.UserLoginRequestDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.dto.UserAuthRequestDTO;
import com.simon.integration.users.oauth.dto.UserAuthResponseDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationEnhancedService;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
@UnitTest
public class DefaultAuthLoginIntegrationServiceTest
{
	@InjectMocks
	DefaultAuthLoginIntegrationService defaultAuthLoginIntegrationService;
	@Mock
	AuthLoginIntegrationEnhancedService authLoginIntegrationEnhancedService;
	@Mock
	ConfigurationService configurationService;
	@Mock
	private SessionService sessionService;
	@Mock
	private Configuration config;
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when(sessionService.getAttribute(SimonIntegrationConstants.AUTH_TOKEN)).thenReturn("authToken");
		when(configurationService.getConfiguration()).thenReturn(config);
		when(config.getString(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_CLIENT_ID)).thenReturn("clientID");
		when(config.getString(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_CLIENT_SECRET)).thenReturn("clientSecret");
		when(config.getString(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_GRANT_TYPE)).thenReturn("grantType");
		when(config.getString(SimonIntegrationConstants.AUTH_LOGIN_INTEGRATION_AUTH_SCOPE)).thenReturn("scope");
		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(false);
	}

	@Test
	public void testGetOAuthToken() throws AuthLoginIntegrationException
	{
		
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		response.setAccessToken("token");
		when(authLoginIntegrationEnhancedService.getOAuthToken(Matchers.any(UserAuthRequestDTO.class))).thenReturn(response);
		doNothing().when(sessionService).setAttribute(SimonIntegrationConstants.AUTH_TOKEN, "authToken");
		assertTrue(defaultAuthLoginIntegrationService.getOAuthToken("username", "password"));
	}
	
	@Test
	public void testGetOAuthTokenMock() throws AuthLoginIntegrationException
	{

		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultAuthLoginIntegrationService.getOAuthToken("username", "password"));
	}
	@Test
	public void testGetOAuthTokenRegisterMock() throws AuthLoginIntegrationException
	{

		when(config.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG,false)).thenReturn(true);
		assertTrue(defaultAuthLoginIntegrationService.generateAndSaveOAuthTokenForRegistration());
	}
	
	@Test
	public void testValidateLogin() throws AuthLoginIntegrationException
	{
		VIPUserDTO response=new VIPUserDTO();
		when(authLoginIntegrationEnhancedService.validateLogin(Matchers.any(UserLoginRequestDTO.class))).thenReturn(response);
		assertEquals(response,defaultAuthLoginIntegrationService.validateLogin("email@gmail.com", "password"));
	}
	@Test
	public void testGetOAuthTokenForRegistration() throws AuthLoginIntegrationException
	{
		UserAuthResponseDTO response=new UserAuthResponseDTO();
		response.setAccessToken("token");
		when(authLoginIntegrationEnhancedService.getOAuthTokenForRegisteration()).thenReturn(response);
		doNothing().when(sessionService).setAttribute(SimonIntegrationConstants.AUTH_TOKEN, "authToken");
		assertTrue(defaultAuthLoginIntegrationService.generateAndSaveOAuthTokenForRegistration());
	}
}
