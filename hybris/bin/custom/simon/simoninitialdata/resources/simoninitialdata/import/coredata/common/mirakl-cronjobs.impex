$catalog=simonProductCatalog
$catalogVersion=catalogVersion(catalog(id[default = $catalog]), version[default = 'Staged'])[unique = true]
$classificationCatalog=SimonClassification
$classificationCatalogVersion=1.0
$classCatalogVersion=catalogversion(catalog(id[default=$classificationCatalog]), version[default=$classificationCatalogVersion])[unique=true, default=$classificationCatalog:$classificationCatalogVersion]
$systemVersion=systemVersion(catalog(id[default = $classificationCatalog]), version[default = $classificationCatalogVersion])[unique = true]
$rootCategory=rootCategory(code, $catalogVersion)[default=google]
$rootBrandCategory=rootBrandCategory(code, $catalogVersion)[default=google]
$sessionUser=mirakl-system-user
$sessionCurrency=USD
$lang=en

# Import config properties into impex macros
UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor] ; pk[unique=true]

#### Jobs ####                                                                                             
# Offer Import Job
INSERT_UPDATE MiraklImportOffersCronJob ; code[unique = true]        ;active; fullImport ; nodeID ; job(code)[default = defaultImportOffersJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                        ; fullImportOffersCronJob    ;false; true        ; 0
                                        ; partialImportOffersCronJob ;false; false       ; 0

# Offer Import Job                                                                                         
INSERT_UPDATE SimonImportOffersCronJob ; code[unique = true]              ; fullImport ; batchSize ; numberOfWorkers ; nodeID ; job(code)[default = simonImportOffersJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                       ; simon-fullImportOffersCronJob    ; true       ; 100       ; 2               ; 0      ;                                   
                                       ; simon-partialImportOffersCronJob ; false      ; 100       ; 2               ; 0      ;                                  
							                                
# Shop Import Job                      
INSERT_UPDATE MiraklImportShopsCronjob ; code[unique = true]             ; fullImport ; nodeID ; job(code)[default = defaultShopImportJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isoCode)[default = $lang] ; active[default = true] ;  
                                       ; fullMiraklImportShopsCronjob    ; true       ;	0
                                       ; partialMiraklImportShopsCronjob ; false      ;	0

# Export Status Job                    
INSERT_UPDATE MiraklExportStatusCronJob ; code[unique = true] ; nodeID ; job(code)[default = defaultExportStatusJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                        ; exportStatusCronJob ; 0 

# Retrieve Consignment updates Job      
INSERT_UPDATE MiraklRetrieveConsignmentUpdatesCronJob ; code[unique=true]             ; nodeID ; job(code)[default = defaultRetrieveConsignmentUpdatesJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                                      ; retrieveConsignmentUpdatesJob ; 0

# Shops -> Offers CompositeJob                        
INSERT_UPDATE CompositeEntry ; code[unique = true]                    ; executableCronJob(code)         ;  
                             ; partialMiraklImportShopsCompositeEntry ; partialMiraklImportShopsCronjob 
                             ; partialImportOffersCompositeEntry      ; simon-partialImportOffersCronJob
                             ; fullMiraklImportShopsCompositeEntry    ; fullMiraklImportShopsCronjob    
                             ; fullImportOffersCompositeEntry         ; simon-fullImportOffersCronJob   
                             
INSERT_UPDATE MiraklImportShopOffersCompositeCronJob ; code[unique = true]                            ; nodeID ; compositeEntries(code)                                                   ; job(code)[default = compositeJobPerformable] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                                     ; partialMiraklImportShopsOffersCompositeCronJob ; 0	   ; partialMiraklImportShopsCompositeEntry, partialImportOffersCompositeEntry
                                                     ; fullMiraklImportShopsOffersCompositeCronJob    ; 0	   ; fullMiraklImportShopsCompositeEntry, fullImportOffersCompositeEntry      

# Mirakl refunds processing job                      
INSERT_UPDATE MiraklRefundPaymentCronJob ; code[unique=true]    ; nodeID ; job(code)[default = defaultRefundPaymentJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                         ; refundPaymentCronJob ; 0 

#### Triggers ####                       

INSERT_UPDATE Trigger ; cronJob(code)[unique = true]                   ; second ; minute ; hour ; day ; month ; year ; maxAcceptableDelay ; relative[default = true] ; active[default = true] ;  
#% afterEach: impex.getLastImportedItem().setActivationTime(org.apache.commons.lang.time.DateUtils.addMinutes(new Date(),1));

#CA2 - Category Export Status - Maximum every 1 minute
#P22 - Product Export Status - Maximum every 1 minute
                      ; exportStatusCronJob                            ; 0      ; 1      ; -1   ; -1  ; -1    ; -1   ; -1                 

#OR11 - Consignment updates (List orders) - Maximum every 1 minute
                      ; retrieveConsignmentUpdatesJob                  ; 0      ; 1      ; -1   ; -1  ; -1    ; -1   ; -1                 

#S20 -> OF51 Composite every 5 minutes
                      ; partialMiraklImportShopsOffersCompositeCronJob ; 0      ; 5      ; -1   ; -1  ; -1    ; -1   ; -1                 

# Refund payment - Every 5 minutes
                      ; refundPaymentCronJob                           ; 0      ; 5      ; -1   ; -1  ; -1    ; -1   ; -1                 

INSERT_UPDATE MiraklProductImportCronJob ; code[unique = true]       ; nodeID ; inputDirectory             ; archiveDirectory             ; inputFilePattern          ; numberOfWorkers ; coreAttributeConfiguration(code)  ; cleanupRawProducts ; $catalogVersion ; $systemVersion ; job(code)[default = defaultMiraklProductImportJob] ; sessionUser(uid)[default = $sessionUser] ; sessionLanguage(isocode)[default = $lang] ; sessionCurrency(isocode)[default = $sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                         ; simonProductImportCronJob ; 0 	  ; $config-product.feed.input ; $config-product.feed.archive ; ([^-]+)-([^-]+)-(.*)\.csv ; 4               ; simon-coreAttributesConfiguration ; true               ;                 ;                ;                                                    

INSERT_UPDATE MiraklExportCommissionCategoriesCronJob ; code[unique = true]          ; job(code)                  ; nodeID ; $rootCategory ; synchronizationFileName    ; sessionUser(uid)[default=$sessionUser] ; sessionLanguage(isocode)[default=$lang] ; sessionCurrency(isocode)[default=$sessionCurrency] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                                      ; simonExportCategoriesCronJob ; defaultExportCategoriesJob ; 0	   ;               ; simonProductCatalog-online ;                                        ;                                         ;                                                    ;                          ;                          

INSERT_UPDATE MiraklExportSellableProductsCronJob ; code[unique = true]                                   ; nodeID ; job(code)                ; baseSite(uid) ; sessionLanguage(isocode)[default=$lang] ; sessionCurrency(isocode)[default=$sessionCurrency] ; fullExport ; synchronizationFileName               ; $rootCategory ; $rootBrandCategory ; sessionUser(uid)[default=$sessionUser] ; logsDaysOld[default = 1] ; filesDaysOld[default = 1]
                                                  ; simon-MiraklExportSellableProductsCronJob-full        ;	0	   ; defaultExportProductsJob ; simon         ;                                         ;                                                    ; true       ; simonProductCatalog-online-full       
                                                  ; simon-MiraklExportSellableProductsCronJob-incremental ;	0	   ; defaultExportProductsJob ; simon         ;                                         ;                                                    ; false      ; simonProductCatalog-online-incremental

INSERT_UPDATE Trigger ; cronJob(code)[unique = true]                          ; second ; minute ; hour ; day ; month ; year ; relative ; active[default = false] ; maxAcceptableDelay ;  
#% afterEach: impex.getLastImportedItem().setActivationTime(org.apache.commons.lang.time.DateUtils.addMinutes(new Date(),1));

#CA01 - Recommended every 1 minute
                      ; simonExportCategoriesCronJob                          ; 0      ; 1      ; -1   ; -1  ; -1    ; -1   ; true     ;                         ; -1                 

#P21 - Recommended every 15 minute
                      ; simon-MiraklExportSellableProductsCronJob-incremental ; 0      ; 15     ; -1   ; -1  ; -1    ; -1   ; true     ;                         ; -1                 
                      ; simon-MiraklExportSellableProductsCronJob-full        ; 0      ; -1     ; -1   ; 1   ; -1    ; -1   ; true     ;                         ; -1                 

INSERT_UPDATE MiraklExportCatalogCronJob ; $catalogVersion ; code[unique=true]                ; nodeID ; job(code)                     ; coreAttributeConfiguration(code)  ; defaultLanguage(isocode) ; additionalLanguages(isocode) ; exportCategories ; exportAttributes ; exportValueLists ; categoriesFileName ; attributesFileName ; valueListsFileName ; importTimeout ; importCheckInterval ; sessionUser(uid)[default=$sessionUser] ; sessionLanguage(isocode)[default=$lang] ; sessionCurrency(isocode)[default=$sessionCurrency] ; logsDaysOld[default=1] ; filesDaysOld[default=1]
                                         ;                 ; simon-MiraklExportCatalogCronJob ; 0 	   ; defaultMiraklExportCatalogJob ; simon-coreAttributesConfiguration ; en                       ;                              ; true             ; true             ; true             ; catalog-categories ; attributes         ; value-lists        ; 3600          ; 60                  
											                              
				                                     
