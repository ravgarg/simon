/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.generated.initialdata.setup;

import de.hybris.platform.addonsupport.setup.impl.DefaultAddonSampleDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.simon.initialdata.constants.SimonInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
//@SystemSetup(extension = SimonInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_LASTCALL_PRODUCTS = "import.lastcall.products";
	private static final String IMPORT_LASTCALL_CATEGORIES = "import.lastcall.categories";
	private static final String IMPORT_LASTCALL_IMAGES = "import.lastcall.images";
	private static final String IMPORT_SIMON_DATA = "importSimonData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";
	private static final String STORE_NAME = "simon";
	private static final String PRODUCT_CATALOG_NAME = STORE_NAME;
	private static final String CONTENT_CATALOG_NAME = STORE_NAME;


	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;
	private DefaultAddonSampleDataImportService addonSampleDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SIMON_DATA, "Import Simon Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List&lt;ImportData&gt; importData = new ArrayList&lt;ImportData&gt;();
	 *
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 *
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 *
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/retailers.impex", true);
		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/designers.impex", true);

		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData sampleImportData = new ImportData();
		sampleImportData.setProductCatalogName(PRODUCT_CATALOG_NAME);
		sampleImportData.setContentCatalogNames(Arrays.asList(CONTENT_CATALOG_NAME));
		sampleImportData.setStoreNames(Arrays.asList(STORE_NAME));
		importData.add(sampleImportData);

		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getAddonSampleDataImportService().importSampleData(SimonInitialDataConstants.EXTENSIONNAME, context, importData, true);

		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/mirakl-user-groups.impex", true);

		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/mirakl-cms-content.impex", true);

		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/mirakl-common-addon-extra.impex", true);

		getSetupImpexService().importImpexFile(
				"/simoninitialdata/import/coredata/productCatalogs/simonProductCatalog/mirakl-products-addon-extra.impex", true);

		getSetupImpexService().importImpexFile("/simoninitialdata/import/stores/simon/mirakl-site.impex", true);


		if (checkLastCallLoadProducts())
		{
			LOG.info(
					"Loading LAST CALL products *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-products.impex", true);

			LOG.info(
					"Loaded LAST CALL products *************************************************************************************************");
		}
		else
		{
			LOG.info(
					"Loading LAST CALL products MIN *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-products-min.impex", true);

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-stocklevels.impex", true);

			LOG.info(
					"Loaded LAST CALL products MIN *************************************************************************************************");

		}

		if (checkLastCallLoadCategories())
		{
			LOG.info(
					"Loading LAST CALL categories *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-fixup.impex", true);

			LOG.info(
					"Loaded LAST CALL categories *************************************************************************************************");
		}
		else
		{
			LOG.info(
					"Loading LAST CALL categories MIN *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-fixup-min.impex", true);

			LOG.info(
					"Loaded LAST CALL categories MIM *************************************************************************************************");
		}

		if (checkLastCallLoadImages())
		{
			LOG.info(
					"Loading LAST CALL images *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-media.impex", true);

			LOG.info(
					"Loaded LAST CALL images *************************************************************************************************");
		}
		else
		{
			LOG.info(
					"Loading LAST CALL images MIN *************************************************************************************************");

			getSetupImpexService()
					.importImpexFile("/simoninitialdata/import/productCatalogs/simonProductCatalog/lastcall-media-min.impex", true);

			LOG.info(
					"Loaded LAST CALL images MIN *************************************************************************************************");
		}

		getSetupImpexService().importImpexFile("/simoninitialdata/import/stores/simon/solrtrigger.impex", true);

		executeCatalogSyncJob(context, PRODUCT_CATALOG_NAME + "ProductCatalog");

		executeCatalogSyncJob(context, CONTENT_CATALOG_NAME + "ContentCatalog");

		getSetupImpexService().importImpexFile("/simoninitialdata/import/coredata/common/user-groups.impex", true);


	}

	/**
	 * Reads value for IMPORT_LASTCALL_PRODUCTS
	 *
	 * @return boolean
	 */
	public boolean checkLastCallLoadProducts()
	{
		final String lastCallBoolean = Config.getParameter(IMPORT_LASTCALL_PRODUCTS);

		LOG.info("lastCallBoolean: '" + lastCallBoolean + "'.");

		if (lastCallBoolean != null && lastCallBoolean.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			return true;
		}

		return false;
	}


	/**
	 * Reads Value for IMPORT_LASTCALL_CATEGORIES
	 *
	 * @return boolean
	 */
	public boolean checkLastCallLoadCategories()
	{
		final String lastCallBoolean = Config.getParameter(IMPORT_LASTCALL_CATEGORIES);

		LOG.info("lastCallBoolean: '" + lastCallBoolean + "'.");

		if (lastCallBoolean != null && lastCallBoolean.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			return true;
		}

		return false;
	}

	/**
	 * Reads value for IMPORT_LASTCALL_IMAGES
	 *
	 * @return boolean
	 */
	public boolean checkLastCallLoadImages()
	{
		final String lastCallBoolean = Config.getParameter(IMPORT_LASTCALL_IMAGES);

		LOG.info("lastCallBoolean: '" + lastCallBoolean + "'.");

		if (lastCallBoolean != null && lastCallBoolean.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			return true;
		}

		return false;
	}


	/**
	 * @return coreDataImportService
	 *
	 *
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}


	/**
	 * @param coreDataImportService
	 */
	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	/**
	 * @return sampleDataImportService
	 */
	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}


	/**
	 * @param sampleDataImportService
	 *
	 *
	 */
	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}

	/**
	 * @return the addonSampleDataImportService
	 */
	public DefaultAddonSampleDataImportService getAddonSampleDataImportService()
	{
		return addonSampleDataImportService;
	}

	/**
	 * @param addonSampleDataImportService
	 *           the addonSampleDataImportService to set
	 */
	public void setAddonSampleDataImportService(final DefaultAddonSampleDataImportService addonSampleDataImportService)
	{
		this.addonSampleDataImportService = addonSampleDataImportService;
	}


}
