/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.initialdata.setup;

import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import com.simon.generated.initialdata.setup.InitialDataSystemSetup;
import com.simon.initialdata.constants.SimonInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = SimonInitialDataConstants.EXTENSIONNAME)
public class SimonInitialDataSystemSetup extends InitialDataSystemSetup
{

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SIMON_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";
	private static final String STORE_NAME = "simon";
	private static final String PRODUCT_CATALOG_NAME = STORE_NAME;
	private static final String CONTENT_CATALOG_NAME = STORE_NAME;
	private static final String IMPORT_SAMPLE_PRODUCT_DATA = "importSampleProduct";
	private static final String ACTIVATE_SOLR_CRON_JOBS_KEY = "simon.initialdata.systemsetup.activatesolrcronjobs";
	private static final String IMPORT_SAMPLE_DATA_KEY = "simon.initialdata.systemsetup.importsampledata";
	private static final String IMPORT_SAMPLE_PRODUCT_DATA_KEY = "simon.initialdata.systemsetup.importsampleproductdata";
	private static final String IMPORT_CORE_DATA_KEY = "simon.initialdata.systemsetup.importcoredata";
	private static final String IMPORT_DELTA_IMPEX_KEY = "simon.initialdata.systemsetup.importdeltaimpex";
	private static final String IMPORT_DELTA_IMPEX = "importDeltaImpex";
	private static final String IMPEX_EXT = ".impex";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();
		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", getPropertyValue(IMPORT_CORE_DATA_KEY)));
		params.add(
				createBooleanSystemSetupParameter(IMPORT_SIMON_DATA, "Import Sample Data", getPropertyValue(IMPORT_SAMPLE_DATA_KEY)));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_PRODUCT_DATA, "Import Sample Product Data",
				getPropertyValue(IMPORT_SAMPLE_PRODUCT_DATA_KEY)));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs",
				getPropertyValue(ACTIVATE_SOLR_CRON_JOBS_KEY)));
		params.add(createMultiValueSystemSetupParameter(IMPORT_DELTA_IMPEX, "Import Delta Impexes",
				getPropertyValue(IMPORT_DELTA_IMPEX_KEY)));
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List&lt;ImportData&gt; importData = new ArrayList&lt;ImportData&gt;();
	 *
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 *
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 *
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	@Override
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = setSampleImportData();
		getCoreDataImportService().execute(this, context, importData);
		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
		getAddonSampleDataImportService().importSampleData(SimonInitialDataConstants.EXTENSIONNAME, context, importData, true);
		importReleaseData(context);
	}

	private List<ImportData> setSampleImportData()
	{
		final List<ImportData> importData = new ArrayList<>();
		final ImportData sampleImportData = new ImportData();
		sampleImportData.setProductCatalogName(PRODUCT_CATALOG_NAME);
		sampleImportData.setContentCatalogNames(Arrays.asList(CONTENT_CATALOG_NAME));
		sampleImportData.setStoreNames(Arrays.asList(STORE_NAME));
		importData.add(sampleImportData);
		return importData;
	}

	/**
	 * This method used to get the property value from config properties file based on the the property key.
	 *
	 * @param propertyKey
	 * @return boolean value to property value
	 */
	private boolean getPropertyValue(final String propertyKey)
	{
		return configurationService.getConfiguration().getBoolean(propertyKey);
	}

	private void importReleaseData(final SystemSetupContext context)
	{
		final String[] versionsToImport = context.getParameters(context.getExtensionName() + "_" + IMPORT_DELTA_IMPEX);
		if (null != versionsToImport)
		{
			for (final String version : versionsToImport)
			{
				importAllImpex(version, context.getExtensionName());
			}
		}
	}

	private void importAllImpex(final String version, final String extensionName)
	{
		final ClassLoader classLoader = getClass().getClassLoader();
		final File file = new File(
				classLoader.getResource(String.format("%s/import/deltaImpex/%s", extensionName, version)).getFile());
		final String[] fileNames = file.list(new FilenameFilter()
		{
			@Override
			public boolean accept(final File current, final String name)
			{
				return (!new File(current, name).isDirectory()) && name.endsWith(IMPEX_EXT);
			}
		});
		for (final String fileToImport : fileNames)
		{
			getCoreDataImportService().getSetupImpexService()
					.importImpexFile(String.format("/%s/import/deltaImpex/%s/%s", extensionName, version, fileToImport), false);
		}
	}

	/**
	 * @param key
	 * @param label
	 * @param defaultValue
	 * @return SystemSetupParameter
	 */
	public SystemSetupParameter createMultiValueSystemSetupParameter(final String key, final String label,
			final boolean defaultValue)
	{
		final SystemSetupParameter syncProductsParam = new SystemSetupParameter(key);
		syncProductsParam.setLabel(label);
		syncProductsParam.setMultiSelect(true);
		final String[] versionFolder = getCurrentReleaseDeltaFolderNames();
		if (versionFolder.length > 0)
		{
			for (final String version : versionFolder)
			{
				syncProductsParam.addValue(version, defaultValue);
			}
		}
		return syncProductsParam;
	}

	private String[] getCurrentReleaseDeltaFolderNames()
	{
		final ClassLoader classLoader = getClass().getClassLoader();
		final File file = new File(classLoader.getResource("simoninitialdata/import/deltaImpex/").getFile());
		final String[] directories = file.list(new FilenameFilter()
		{
			@Override
			public boolean accept(final File current, final String name)
			{
				return new File(current, name).isDirectory();
			}
		});
		if (null != directories && directories.length > 0)
		{
			return directories;
		}
		return new String[0];
	}
}
