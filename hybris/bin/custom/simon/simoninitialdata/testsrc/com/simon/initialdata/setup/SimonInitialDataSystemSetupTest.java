package com.simon.initialdata.setup;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.addonsupport.setup.impl.DefaultAddonSampleDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.event.EventService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * Test Class for {@link SimonInitialDataSystemSetup}
 */
@UnitTest
public class SimonInitialDataSystemSetupTest
{

	@InjectMocks
	SimonInitialDataSystemSetup simonInitialDataSystemSetup;

	@Mock
	SystemSetupContext context;

	@Mock
	CoreDataImportService coreDataImportService;

	@Mock
	SampleDataImportService sampleDataImportService;

	@Mock
	EventService eventService;

	@Mock
	DefaultAddonSampleDataImportService defaultAddonSampleDataImportService;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		doNothing().when(coreDataImportService).execute(Matchers.any(SimonInitialDataSystemSetup.class),
				Matchers.any(SystemSetupContext.class), Matchers.anyListOf(ImportData.class));
		doNothing().when(sampleDataImportService).execute(Matchers.any(SimonInitialDataSystemSetup.class),
				Matchers.any(SystemSetupContext.class), Matchers.anyListOf(ImportData.class));
		doNothing().when(eventService).publishEvent(Matchers.any(CoreDataImportedEvent.class));
		doNothing().when(defaultAddonSampleDataImportService).importSampleData(Matchers.anyString(),
				Matchers.any(SystemSetupContext.class), Matchers.anyListOf(ImportData.class), Matchers.anyBoolean());

	}

	/**
	 * tests {@link SimonInitialDataSystemSetup#createProjectData(SystemSetupContext)} when all last call loads are
	 * false.
	 */
	@Test
	public void testCreateProjectDataWhenAllLastCallLoadsFalse()
	{
		simonInitialDataSystemSetup.createProjectData(context);
		verify(coreDataImportService, times(1)).execute(Matchers.any(SimonInitialDataSystemSetup.class),
				Matchers.any(SystemSetupContext.class), Matchers.anyListOf(ImportData.class));
		verify(sampleDataImportService, times(1)).execute(Matchers.any(SimonInitialDataSystemSetup.class),
				Matchers.any(SystemSetupContext.class), Matchers.anyListOf(ImportData.class));
		verify(eventService).publishEvent(Matchers.any(CoreDataImportedEvent.class));
		verify(defaultAddonSampleDataImportService).importSampleData(Matchers.anyString(), Matchers.any(SystemSetupContext.class),
				Matchers.anyListOf(ImportData.class), Matchers.anyBoolean());


	}


}
