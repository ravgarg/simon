package com.simon.storefront.controllers.cart;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorcms.model.components.SimpleBannerComponentModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.facades.cart.ExtCartFacade;
import com.simon.facades.cart.data.CartGlobalFlashMsgData;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * Controller for MiniBag functionality which loads the mini-bag data
 *
 */
@Controller
@Scope("tenant")
public class ExtMiniCartController
{

	@Resource(name = "extCartFacade")
	private ExtCartFacade extMiniBagFacade;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource
	private SessionService sessionService;

	private static final String MINI_CART = "cm_miniCartComponent";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtMiniCartController.class);

	/**
	 * This method handles '/cart/minibag' request url. It returns the minibag JSON.
	 *
	 * @return MiniCartEntryData in the form of JSON
	 *
	 *
	 */
	@ResponseBody
	@RequestMapping(value = "/cart/minibag", method = RequestMethod.GET, produces = "application/json")
	public CartData rolloverMiniBag()
	{


		final CartData cartData = getExtMiniBagFacade().getMiniBag();

		final List<CartGlobalFlashMsgData> globalFlashMsg = sessionService.getAttribute(SimonWebConstants.GLOBALFLASH_MSG);

		if (null != globalFlashMsg)
		{
			if (null != cartData.getGlobalFlashMsg() && CollectionUtils.isNotEmpty(globalFlashMsg))
			{
				cartData.getGlobalFlashMsg().addAll(globalFlashMsg);
			}
			else
			{
				cartData.setGlobalFlashMsg(globalFlashMsg);
			}
			sessionService.removeAttribute(SimonWebConstants.GLOBALFLASH_MSG);
		}
		MiniCartComponentModel miniCartComponentModel = null;
		SimpleBannerComponentModel simpleBannerComponentModel = null;

		try
		{
			miniCartComponentModel = getExtMiniBagFacade().getMiniCartComponent(MINI_CART);
			if (null != miniCartComponentModel)
			{
				simpleBannerComponentModel = miniCartComponentModel.getLightboxBannerComponent();
			}
		}
		catch (final CMSItemNotFoundException ex)
		{
			LOGGER.error("CMSItemNotFoundException : mini Cart Component Model not Found", ex);
		}

		if (null != simpleBannerComponentModel)
		{
			final String topBannerUrl = null != simpleBannerComponentModel.getMedia()
					? simpleBannerComponentModel.getMedia().getURL() : "";
			final String topBannerRedirectUrl = simpleBannerComponentModel.getUrlLink();
			cartData.setTopBannerUrl(topBannerUrl);
			cartData.setTopBannerRedirectUrl(topBannerRedirectUrl);
		}

		if (checkoutCustomerStrategy.isAnonymousCheckout())
		{
			cartData.setAnonymousUser(true);
		}

		return cartData;
	}

	/**
	 * @return extMiniBagFacade
	 */
	public ExtCartFacade getExtMiniBagFacade()
	{
		return extMiniBagFacade;
	}


	/**
	 * @param extMiniBagFacade
	 */
	public void setExtMiniBagFacade(final ExtCartFacade extMiniBagFacade)
	{
		this.extMiniBagFacade = extMiniBagFacade;
	}

}
