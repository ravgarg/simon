package com.simon.storefront.builder.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Value;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;


/**
 * This class makes breadcrumb for PLP/CLP/PDP pages
 */
public class ExtBreadcrumbBuilder
{
	@Resource
	private CommerceCategoryService commerceCategoryService;

	@Resource
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Value("${navigation.home.category.name:Home}")
	private String homeCategoryName;

	/**
	 * This method returns list of breadcrumbs, based on category hierarchy codes passed. And adds Home category
	 * breadcrumb as root breadcrumb
	 *
	 * @param url
	 * @return
	 */
	public List<Breadcrumb> getBreadcrumbs(final String url)
	{
		final List<Breadcrumb> breadcrumb = new ArrayList<>();
		Pair<String, String> categoryPair;
		String breadCrumbCategoryCode;
		String catCodeURL = url;
		CategoryModel source;
		String categoryUrl;

		while (StringUtils.isNotEmpty(catCodeURL))
		{
			categoryPair = getLeafCategoryCodeAndRestSubString(catCodeURL);
			breadCrumbCategoryCode = categoryPair.getLeft();
			source = commerceCategoryService.getCategoryForCode(breadCrumbCategoryCode);
			categoryUrl = extCategoryModelUrlResolver.resolveCategoryPath(source, catCodeURL);
			catCodeURL = categoryPair.getRight();
			breadcrumb.add(new Breadcrumb(categoryUrl, source.getName(), null));
		}
		breadcrumb.add(new Breadcrumb("/", homeCategoryName, null));
		Collections.reverse(breadcrumb);
		return breadcrumb;

	}

	/**
	 * This method returns Pair of leaf category code and rest of substring
	 * 
	 * @param categoryCode
	 * @return
	 */
	private Pair<String, String> getLeafCategoryCodeAndRestSubString(final String categoryCode)
	{
		final int lastUnderscoreIndex = categoryCode.lastIndexOf(SimonCoreConstants.UNDERSCORE);
		final String lastCategoryCode = categoryCode.substring(lastUnderscoreIndex + 1);
		final String restSubString = lastUnderscoreIndex < SimonCoreConstants.ZERO_INT ? StringUtils.EMPTY
				: categoryCode.substring(0, lastUnderscoreIndex);
		return new ImmutablePair<String, String>(lastCategoryCode, restSubString);
	}
}
