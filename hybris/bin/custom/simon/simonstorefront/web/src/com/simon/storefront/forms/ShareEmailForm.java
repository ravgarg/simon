package com.simon.storefront.forms;

/**
 * This is share Email Form.
 */
public class ShareEmailForm
{
	private String fromEmail;
	private String toEmail;
	private String comments;
	private String productId;
	private String dealId;


	/**
	 * @return the fromEmail
	 */
	public String getFromEmail()
	{
		return fromEmail;
	}

	/**
	 * @param fromEmail
	 *           the fromEmail to set
	 */
	public void setFromEmail(final String fromEmail)
	{
		this.fromEmail = fromEmail;
	}

	/**
	 * @return the toEmail
	 */
	public String getToEmail()
	{
		return toEmail;
	}

	/**
	 * @param toEmail
	 *           the toEmail to set
	 */
	public void setToEmail(final String toEmail)
	{
		this.toEmail = toEmail;
	}

	/**
	 * @return the comments
	 */
	public String getComments()
	{
		return comments;
	}

	/**
	 * @param comments
	 *           the comments to set
	 */
	public void setComments(final String comments)
	{
		this.comments = comments;
	}

	/**
	 * @return the productId
	 */
	public String getProductId()
	{
		return productId;
	}

	/**
	 * @param productId
	 *           the productId to set
	 */
	public void setProductId(final String productId)
	{
		this.productId = productId;
	}

	/**
	 * @return the dealId
	 */
	public String getDealId()
	{
		return dealId;
	}

	/**
	 * @param dealId
	 *           the dealId to set
	 */
	public void setDealId(final String dealId)
	{
		this.dealId = dealId;
	}



}
