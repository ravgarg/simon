/**
 *
 */
package com.simon.storefront.breadcrumb.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.cms2.model.pages.ContentPageModel;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;


/**
 *
 */
public class ExtContentPageBreadcrumbBuilder extends ContentPageBreadcrumbBuilder
{

	private static final String LAST_LINK_CLASS = "active";

	/**
	 * @param page
	 *           - page to build up breadcrumb for
	 * @return breadcrumb for given page
	 */
	@Override
	public List<Breadcrumb> getBreadcrumbs(final ContentPageModel page)
	{
		String title = page.getTitle();
		if (title == null)
		{
			title = page.getName();
		}

		String url = page.getLabel();

		if (StringUtils.isBlank(url))
		{
			url = "#";
		}

		return Collections.singletonList(new Breadcrumb(url, title, LAST_LINK_CLASS));
	}
}
