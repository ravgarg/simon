package com.simon.storefront.controllers.cart;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.core.util.JsonUtils;
import com.simon.facades.cart.ExtCartFacade;
import com.simon.facades.cart.data.CartGlobalFlashMsgData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.message.CustomMessageFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.generated.storefront.controllers.pages.CartPageController;
import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * This is the Cart page controller to enable the cart functionality. It include the Show cart and update cart
 */
@Controller
@RequestMapping(value = "/cart")
public class ExtCartPageController extends CartPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtCartPageController.class);

	@Resource
	private CustomMessageFacade customMessageFacade;

	@Resource
	private ExtCartFacade cartFacade;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource
	private SessionService sessionService;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;


	/**
	 *
	 *
	 * @return Returns number of entries in cart
	 */
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	@ResponseBody
	public int getCount()
	{
		return cartFacade.getCartCount();
	}

	/**
	 * Use to Render the Cart Page
	 *
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String showCart(final Model model) throws CMSItemNotFoundException
	{
		final List<CartModificationData> modifications = cartFacade.getChangeEntriesInCart();
		extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
		super.showCart(model);
		final CartData cartData = (CartData) model.asMap().get("cartData");
		cartFacade.setCartPageMessages(cartData);
		final List<CartModificationData> modificationsRedirected = (List<CartModificationData>) model.asMap().get("validationData");
		if (modificationsRedirected != null)
		{
			cartFacade.populateCartDataFromModification(modificationsRedirected, cartData);
		}

		if (checkoutCustomerStrategy.isAnonymousCheckout())
		{
			cartData.setAnonymousUser(true);
		}
		cartFacade.populateCartDataFromModification(modifications, cartData);
		getGlobalMessage(model, cartData);
		model.addAttribute("cartDataJSON", JsonUtils.getJSONStringFromObject(cartData));
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
		if (cartData.getTotalItems() == 0 && null != cartData.getGlobalFlashMsg() && !cartData.getGlobalFlashMsg().isEmpty())
		{
			sessionService.setAttribute(SimonWebConstants.GLOBALFLASH_MSG, cartData.getGlobalFlashMsg());
		}
		return ControllerConstants.Views.Pages.Cart.CartPage;
	}


	/**
	 * This is the method which is creating product list for the cart.And setting the cmspage information into the cart.
	 */
	/**
	 * @param entryNumber
	 * @param productCode
	 * @param form
	 * @return cartData
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/updateMultiD", method = RequestMethod.POST)
	@ResponseBody
	public CartData updateCartQuantitiesMulti(@RequestParam("entryNumber") final Integer entryNumber,
			@RequestParam("productCode") final String productCode, @Valid final UpdateQuantityForm form)
			throws CMSItemNotFoundException
	{
		CartData cartData = null;
		try
		{
			final CartModificationData cartModification = getCartFacade()
					.updateCartEntry(getOrderEntryData(form.getQuantity(), productCode, entryNumber));
			cartFacade.removeAdditionalCart();
			final List<CartModificationData> modifications = cartFacade.getChangeEntriesInCart();
			cartData = getCartFacade().getSessionCart();
			cartFacade.populateCartDataFromModification(modifications, cartData);
			cartFacade.addFlashMessage(cartData, form.getQuantity(), cartModification);
			cartFacade.setCartPageMessages(cartData);
		}
		catch (final CommerceCartModificationException ex)
		{
			LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
		}
		return cartData;
	}

	private void getGlobalMessage(final Model model, final CartData cartData)
	{
		List<CartGlobalFlashMsgData> flashMessages = cartData.getGlobalFlashMsg();
		if (flashMessages == null)
		{
			flashMessages = new ArrayList<>();
		}
		final List confMessageList = (List) model.asMap().get(SimonWebConstants.JMS_ERROR);
		if (CollectionUtils.isNotEmpty(confMessageList))
		{
			final GlobalMessage message = (GlobalMessage) confMessageList.get(0);

			final CartGlobalFlashMsgData flashMessage = new CartGlobalFlashMsgData();
			flashMessage.setMessage(message.getCode());
			flashMessages.add(flashMessage);

			cartData.setGlobalFlashMsg(flashMessages);
		}
	}
}
