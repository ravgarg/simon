package com.simon.storefront.account.validation;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.simon.storefront.checkout.form.ExtAddressForm;


/**
 * Validator for address forms. Enforces the order of validation.
 */
@Component("extAccountAddressValidator")
public class ExtAccountAddressValidator extends AddressValidator
{

	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;

	/**
	 * This method is used to validate the address form with common fields.
	 *
	 * @param object
	 * @param errors
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ExtAddressForm addressForm = (ExtAddressForm) object;
		validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getTownCity(), AddressField.TOWN, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getPostcode(), AddressField.POSTCODE, MAX_POSTCODE_LENGTH, errors);
		validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
	}

	/**
	 * Method used to validate individual string fields.
	 *
	 * @param addressField
	 *           the String object.
	 *
	 * @param fieldType
	 *           the AddressField object.
	 *
	 * @param maxFieldLength
	 *           the integer value.
	 *
	 * @param errors
	 *           the Errors object.
	 *
	 */
	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/**
	 * Method to validate fields for not null.
	 *
	 * @param addressField
	 *           the String object.
	 *
	 * @param fieldType
	 *           the AddressField object.
	 *
	 * @param errors
	 *           the Errors object.
	 *
	 */
	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	/**
	 * AddressField enum for various fields.
	 *
	 */
	protected enum AddressField
	{
		FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName", "address.lastName.invalid"), LINE1("line1",
				"address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN("townCity",
						"address.townCity.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
								"address.regionIso.invalid"), COUNTRY("countryIso", "address.country.invalid");

		private final String fieldKey;
		private final String errorKey;

		/**
		 * Private constructor used to assign values at startup.
		 *
		 * @param fieldKey
		 * @param errorKey
		 */
		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		/**
		 * Method to get field key.
		 *
		 * @return the fieldKey value.
		 */
		public String getFieldKey()
		{
			return fieldKey;
		}

		/**
		 * Method to get error key.
		 *
		 * @return the errorKey value.
		 *
		 */
		public String getErrorKey()
		{
			return errorKey;
		}
	}

}
