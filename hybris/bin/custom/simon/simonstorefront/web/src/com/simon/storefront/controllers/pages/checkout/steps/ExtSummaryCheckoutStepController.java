package com.simon.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.util.localization.Localization;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.NotFoundException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.exceptions.BusinessException;
import com.simon.facades.checkout.data.ExtCheckoutData;
import com.simon.facades.checkout.data.ExtCheckoutErrorData;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.order.impl.ExtCheckoutFacadeImpl;
import com.simon.facades.user.ExtUserFacade;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.forms.PlaceOrderForm;
import com.simon.storefront.util.ExtWebUtils;
import com.siomon.exceptions.AddressValidationException;


/**
 * Controller class used for summary of checkout steps by extending {@link AbstractCheckoutStepController}, it is
 * implemented for placing an order using Twotap and spreedly
 *
 */
@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class ExtSummaryCheckoutStepController extends AbstractCheckoutStepController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtSummaryCheckoutStepController.class);

	private static final String SUMMARY = "summary";
	private static final String USA_ISOCODE = Locale.US.getCountry();
	protected static final String URL_ORDER_CONFIRMATION = "/checkout/orderConfirmation/";
	private static final String CHECKOUT_VERIFY_CONFIGURABLE_PARAMETER = "simon.checkout.verify.configurable.field.flag";

	@Resource
	private ExtCheckoutFacade checkoutFacade;

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Value("${twotap.checkout}")
	private Boolean twotapCheckout;

	@Resource(name = "userFacade")
	private ExtUserFacade userFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource
	private I18NService i18NService;
	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * Method implemented to get the List of ExpiryYears for select Option.
	 *
	 * @return list of
	 *         {@link de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController.SelectOption}
	 */
	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < calender.get(Calendar.YEAR) + 11; i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	/**
	 * Method is used to get and store informations such as cart estimate. It is used to update cart with shippingCost
	 * and tax from TwoTap and also request the security code if the SubscriptionPciOption is set to Default
	 *
	 * @param model
	 *           of type Model.
	 * @param redirectAttributes
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load
	 */
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException, // NOSONAR
			CommerceCartModificationException
	{

		final CartData cartData = checkoutFacade.getCheckoutCart();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode, Arrays.asList(
						ProductOption.BASIC, ProductOption.PRICE, ProductOption.VARIANT_MATRIX_BASE, ProductOption.PRICE_RANGE));

				entry.setProduct(product);
			}
		}

		model.addAttribute("cartData", cartData);
		model.addAttribute("allItems", cartData.getEntries());
		model.addAttribute("deliveryAddress", cartData.getDeliveryAddress());
		model.addAttribute("deliveryMode", cartData.getDeliveryMode());
		model.addAttribute("paymentInfo", cartData.getPaymentInfo());

		// Only request the security code if the SubscriptionPciOption is set to
		// Default.
		final boolean requestSecurityCode = CheckoutPciOptionEnum.DEFAULT
				.equals(getCheckoutFlowFacade().getSubscriptionPciOption());
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));

		model.addAttribute(new PlaceOrderForm());

		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		model.addAttribute("pageType", "checkoutSummary");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);

		return SimonControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}




	/**
	 * Method is used to Place order using Spreedly, it uses placeOrderForm of type {@link PlaceOrderForm} and set
	 * {@link SpreedlyPaymentInfoData} to pass it to Spreedly Delivery call get and store informations such as cart
	 * estimate using {@link ExtCheckoutFacadeImpl#processSpreedlyDeliverCall()}
	 *
	 * @param placeOrderForm
	 *           is the form used to get the order form details.
	 * @param model
	 *           of type Model.
	 * @param request
	 * @param redirectModel
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load
	 * @throws CMSItemNotFoundException
	 * @throws InvalidCartException
	 * @throws CommerceCartModificationException
	 * @throws BusinessException
	 */
	@RequestMapping(value = "/placeOrder")
	@PreValidateQuoteCheckoutStep
	@RequireHardLogIn
	@ResponseBody
	public Object placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException, // NOSONAR
			InvalidCartException, CommerceCartModificationException, BusinessException
	{
		//start spreedly validation
		OrderData orderData = null;
		ExtCheckoutErrorData errorData = null;
		final ExtCheckoutData checkoutData = new ExtCheckoutData();

		// Validate the cart
		if (validateExtCart(redirectModel))
		{
			// Invalid cart or Product out of stock
			errorData = new ExtCheckoutErrorData();
			errorData.setCode(SimonFacadesConstants.PRODUCT_OUT_OF_STOCK_ERROR_CODE);
			return errorData;
		}

		try
		{
			// FIRST VERIFY TOKEN AND THEN ADDING PAYMENT METHOD WITH ADDRESS AT PO.COM .IF FAIL CHECKOUT WILL BE STOPPED.

			//If User chooses to save payment
			if (placeOrderForm.isSaveInAccount())
			{
				checkoutFacade.verifyToken(placeOrderForm.getSpreedlyToken(), true, true);
			}
			//If existing payment present for User.
			else if (placeOrderForm.getPaymentInfoId() != null)
			{
				if (Boolean.parseBoolean(configurationService.getConfiguration().getString(CHECKOUT_VERIFY_CONFIGURABLE_PARAMETER)))
				{
					checkoutFacade.verifyToken(placeOrderForm.getSpreedlyToken(), true, true);
				}
				else
				{
					if (!checkoutFacade.validatePaymentMethodInternal(placeOrderForm.getPaymentInfoId()))
					{
						LOGGER.error("Failed to place Order::Internal Card Expiry Date Validation Failed for Payment ID {}",
								placeOrderForm.getPaymentInfoId());
						return getErrorMessage(SimonFacadesConstants.PAYMENT_AUTHORIZATION_FAILED);
					}
				}
			}
			//For Guest User.
			else
			{
				checkoutFacade.verifyToken(placeOrderForm.getSpreedlyToken(), false, true);
			}

			if (setPaymentInfo(placeOrderForm))
			{
				orderData = checkoutFacade.placeOrder();
				checkoutFacade.deliver(orderData.getCode(), placeOrderForm.getSpreedlyToken());
				checkoutData.setConfirmationPageUrl(URL_ORDER_CONFIRMATION
						+ (getCheckoutCustomerStrategy().isAnonymousCheckout() ? orderData.getGuid() : orderData.getCode()));
			}
			else
			{
				LOGGER.error("Failed to place Order::Something went wrong in Payment processing");
				errorData = getErrorMessage(SimonFacadesConstants.PAYMENT_AUTHORIZATION_FAILED);
			}
		}
		catch (final UserAddressIntegrationException e)
		{
			LOGGER.error("Failed to place Order::Something went wrong in Payment processing", e);
			errorData = getErrorMessage(SimonCoreConstants.INTEGRATION_USER_ADDRESS_PAYMENT_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE);
		}
		catch (final CartCheckOutIntegrationException e)
		{
			LOGGER.error("Failed to place Order::Something went wrong in Payment processing", e);
			errorData = getErrorMessage(SimonCoreConstants.INTEGRATION_USER_ADDRESS_PAYMENT_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE);
		}
		catch (final NotFoundException e)
		{
			LOGGER.error("Failed to place Order ", e);
			errorData = getErrorMessage(SimonFacadesConstants.PLACE_ORDER_FAILED);
		}
		catch (final AddressValidationException e)
		{
			LOGGER.error("Billing address validation failed ", e);
			errorData = getErrorMessage(e.getMessage());
		}
		catch (final ParseException e)
		{
			LOGGER.error("Date Parsing for Card Expiry Date failed ", e);
			errorData = getErrorMessage(e.getMessage());
		}
		return null == errorData ? checkoutData : errorData;
	}

	/**
	 * Validate billing address.
	 *
	 * @param placeOrderForm
	 *           the place order form
	 * @throws AddressValidationException
	 *            the address validation exception
	 */
	private void validateBillingAddress(final PlaceOrderForm placeOrderForm) throws AddressValidationException
	{
		try
		{
			ServicesUtil.validateParameterNotNullStandardMessage("Address line 1 can't be empty", placeOrderForm.getLine1());
			ServicesUtil.validateParameterNotNullStandardMessage("Postal Code can't be empty", placeOrderForm.getPostalCode());
			ServicesUtil.validateParameterNotNullStandardMessage("State can't be empty", placeOrderForm.getState());
			ServicesUtil.validateParameterNotNullStandardMessage("City can't be empty", placeOrderForm.getTown());
		}
		catch (final IllegalArgumentException e)
		{
			LOGGER.error("Billing address validation failed ", e);
			throw new AddressValidationException(e.getMessage());
		}
	}

	/**
	 * This method is used to set existing or new payment info
	 *
	 * @param placeOrderForm
	 * @return boolean
	 * @throws BusinessException
	 * @throws AddressValidationException
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	public boolean setPaymentInfo(final PlaceOrderForm placeOrderForm)
			throws BusinessException, AddressValidationException, UserAddressIntegrationException, CartCheckOutIntegrationException
	{
		boolean result = false;
		if (StringUtils.isNotBlank(placeOrderForm.getPaymentInfoId()))
		{
			result = checkoutFacade.setPaymentDetails(placeOrderForm.getPaymentInfoId());
		}
		else
		{
			result = setNewPaymentInfo(placeOrderForm);
		}
		return result;
	}

	/**
	 * This method will save user password in session.
	 *
	 * @param userPassword
	 * @return @String
	 */
	@RequestMapping(value = "/save-information", method = RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
	public String savePersonalInfo(@RequestParam final String userPassword)
	{
		sessionService.setAttribute("userPassword", userPassword);
		return messageSource.getMessage(SimonFacadesConstants.PLACE_CHECKOUT_ORDER_CREATE_ACCOUNT_SAVE_AND_CONTINUE_MESSAGE, null,
				getI18nService().getCurrentLocale());
	}

	/**
	 * This method is used to get previous Checkout step.
	 */
	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	/**
	 * This method is used to get next Checkout step.
	 */
	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}

	/**
	 * This method is used to set the spreedly payment info
	 *
	 * @param placeOrderForm
	 * @throws BusinessException
	 * @throws AddressValidationException
	 * @throws UserAddressIntegrationException
	 * @throws CartCheckOutIntegrationException
	 */
	private boolean setNewPaymentInfo(final PlaceOrderForm placeOrderForm)
			throws AddressValidationException, UserAddressIntegrationException, BusinessException, CartCheckOutIntegrationException
	{
		boolean result = false;

		final CCPaymentInfoData extPaymentInfoData = fillPaymentInfoData(placeOrderForm);
		final AddressData addressData = getAddress(placeOrderForm);

		extPaymentInfoData.setBillingAddress(addressData);
		final CCPaymentInfoData newPaymentSubscription = checkoutFacade.createPaymentInfoSubscription(extPaymentInfoData);
		if (null != newPaymentSubscription && checkPaymentSubscription(placeOrderForm, newPaymentSubscription))
		{
			result = true;
		}
		return result;
	}

	/**
	 * This method is used to set the default payment info and set in cart
	 *
	 * @param placeOrderForm
	 * @param model
	 */
	protected boolean checkPaymentSubscription(@Valid final PlaceOrderForm paymentDetailsForm,
			final CCPaymentInfoData newPaymentSubscription)
	{
		if (Boolean.TRUE.equals(Boolean.parseBoolean(paymentDetailsForm.getDefaultCardForNextTime())))
		{
			userFacade.setDefaultPaymentInfo(newPaymentSubscription);
		}
		checkoutFacade.setPaymentDetails(newPaymentSubscription.getId());
		return true;
	}

	/**
	 * This method is used to fill address form from placeOrderForm
	 *
	 * @param placeOrderForm
	 */
	private AddressForm fillAddressInfoDData(final PlaceOrderForm placeOrderForm)
	{
		final AddressForm billingAddress = new AddressForm();

		final CartData cartData = checkoutFacade.getCheckoutCart();

		billingAddress.setLine1(placeOrderForm.getLine1());
		billingAddress.setLine2(placeOrderForm.getLine2());
		billingAddress.setPostcode(placeOrderForm.getPostalCode());
		billingAddress.setRegionIso(placeOrderForm.getState());
		billingAddress.setCountryIso(USA_ISOCODE);
		billingAddress.setTownCity(placeOrderForm.getTown());
		billingAddress.setFirstName(cartData.getDeliveryAddress().getFirstName());
		billingAddress.setLastName(cartData.getDeliveryAddress().getLastName());
		billingAddress.setPhone(cartData.getDeliveryAddress().getPhone());
		billingAddress.setBillingAddress(Boolean.TRUE);

		return billingAddress;
	}

	/**
	 * Method use to populate from place order form into SpreedlyPaymentInfoData object
	 *
	 * @param placeOrderForm
	 * @return spreedlyPaymentInfoData
	 */
	private CCPaymentInfoData fillPaymentInfoData(final PlaceOrderForm placeOrderForm)
	{
		final CCPaymentInfoData extPaymentInfoData = new CCPaymentInfoData();
		extPaymentInfoData.setPaymentToken(placeOrderForm.getSpreedlyToken());
		extPaymentInfoData.setAccountHolderName(placeOrderForm.getNameOnCard());
		extPaymentInfoData.setCardNumber(placeOrderForm.getCardNumber());
		extPaymentInfoData.setLastFourDigits(placeOrderForm.getLastFourDigits());
		extPaymentInfoData.setCardType(placeOrderForm.getCardType());
		extPaymentInfoData.setExpiryMonth(placeOrderForm.getExpirationMonth());
		extPaymentInfoData.setExpiryYear(placeOrderForm.getExpirationYear());
		extPaymentInfoData.setSaveInAccount(placeOrderForm.isSaveInAccount());


		return extPaymentInfoData;
	}

	/**
	 * This method use to get the billing address for guest/register user
	 *
	 * @param PlaceOrderForm
	 * @return AddressData
	 * @throws AddressValidationException
	 */
	private AddressData getAddress(final PlaceOrderForm placeOrderForm) throws BusinessException, AddressValidationException
	{
		AddressData addressData;
		if (StringUtils.isNotEmpty(placeOrderForm.getBillingAddId()))
		{
			addressData = userFacade.getAddress(placeOrderForm.getBillingAddId());
		}
		else if (Boolean.TRUE.equals(placeOrderForm.isUseShippingAddressAsBilling()))
		{
			addressData = checkoutFacade.getCheckoutCart().getDeliveryAddress();
			if (addressData == null)
			{
				throw new BusinessException("Deliver address not found");
			}
			addressData.setBillingAddress(true); // mark this as billing address
			addressData.setId(null);
		}
		else
		{
			//validate billing address
			validateBillingAddress(placeOrderForm);
			final AddressForm addressForm = fillAddressInfoDData(placeOrderForm);
			addressData = addressDataUtil.convertToAddressData(addressForm);
			addressData.setShippingAddress(Boolean.TRUE.equals(addressForm.getShippingAddress()));
			addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
		}

		return addressData;
	}

	/*
	 * This method calling for Validate Cart
	 *
	 * @param redirectModel RedirectAttributes
	 *
	 * @return boolean
	 */
	protected boolean validateExtCart(final RedirectAttributes redirectModel)
	{
		boolean result = false;
		if (!checkoutFacade.hasValidCart())
		{
			result = true;
		}
		else
		{
			result = validateCart(redirectModel);
		}
		return result;
	}

	/*
	 * This method setting error message when fail very payment token is fail
	 *
	 * @param keyConstant String
	 *
	 * @return ExtCheckoutErrorData
	 */
	protected ExtCheckoutErrorData getErrorMessage(final String keyConstant)
	{
		final ExtCheckoutErrorData errorData = new ExtCheckoutErrorData();
		errorData.setDescription(getLocalizedMessage(keyConstant));
		return errorData;

	}

	/*
	 * This method setting localize message
	 *
	 * @param keyConstant String
	 *
	 * @return String
	 */
	protected String getLocalizedMessage(final String keyConstant)
	{
		return Localization.getLocalizedString(keyConstant);
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.NOINDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
