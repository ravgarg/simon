/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ProductPageModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.product.impl.ExtProductFacadeImpl;
import com.simon.generated.storefront.controllers.pages.ProductPageController;
import com.simon.storefront.builder.impl.ExtBreadcrumbBuilder;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtPageTitleResolver;
import com.simon.storefront.util.ExtWebUtils;


/**
 * Controller for product details page
 */
@Controller
@RequestMapping(value = "/**/p")
public class ExtProductPageController extends ProductPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtProductPageController.class);

	protected static final String PRODUCT_ATTRIBUTE = "product";
	public static final String FORWARD_PREFIX = "forward:";

	@Resource(name = "productDataUrlResolver")
	private UrlResolver<ProductData> productDataUrlResolver;

	@Resource
	private ExtBreadcrumbBuilder extBreadcrumbBuilder;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private SessionService sessionService;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource
	private ExtProductFacadeImpl productFacade;

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "pageTitleResolver")
	private ExtPageTitleResolver pageTitleResolver;
	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * This method is used to return the product detail page URL for the product in {@link ProductData}.
	 *
	 * @param productCode
	 *           used to identify which product is to be processed to display
	 * @param model
	 * @param request
	 *           {@link HttpServletRequest}
	 * @param response
	 *           {@link HttpServletResponse}
	 * @return view for the product
	 * @throws CMSItemNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	@Override
	@RequestMapping(value = "/{productCode:.*}", method = RequestMethod.GET)
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		boolean isPunchout = false;
		LOG.debug("entring productDetails method for product {}", productCode);

		if (productFacade.getProductShopStatus(productCode))
		{
			return FORWARD_PREFIX + "/404";
		}

		final String catCodePath = getCategoryPath(request);

		final ProductData productData = new ProductData();
		productData.setCode(productCode);
		extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
		if (extCustomerFacade.getMyFavoriteProductCodes().contains(productCode))
		{
			productData.setFavorite(true);
		}

		sessionService.setAttribute(SimonCoreConstants.CATEGORY_CODE_PATH, catCodePath);

		final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		sessionService.removeAttribute(SimonCoreConstants.CATEGORY_CODE_PATH);

		if (StringUtils.isNotEmpty(catCodePath))
		{
			updatePageTitle(productCode, model, catCodePath);
		}
		else
		{
			updatePageTitle(productCode, model);
		}
		final ProductModel productModel = productFacade.getProductForCode(productCode);
		if (productModel instanceof GenericVariantProductModel)
		{
			final ProductModel baseProductModel = ((GenericVariantProductModel) productModel).getBaseProduct();
			if (null != baseProductModel)
			{
				productData.setBaseProduct(baseProductModel.getCode());
			}
		}
		else
		{
			productData.setBaseProduct(productCode);
		}

		populateModelAttributes(productCode, model, productData, catCodePath);

		final ProductSearchPageData<SearchStateData, ProductData> productReferencesData = getProductReferencesData(productModel);
		model.addAttribute("searchPageData", productReferencesData);
		model.addAttribute(SimonCoreConstants.CANONICAL_URL, request.getRequestURL());
		if (StringUtils.contains(request.getQueryString(), "punchout"))
		{
			isPunchout = true;
		}
		model.addAttribute("ispunchout", isPunchout);
		return getViewForPage(model);
	}

	/**
	 * This method checks whether current URL contains category hierarchy. If yes, then returns category hierarchy else
	 * return null
	 *
	 * @param request
	 * @return
	 */
	private String getCategoryPath(final HttpServletRequest request)
	{
		final String catCodePath = request.getRequestURI();
		if (catCodePath.contains(SimonCoreConstants.CATEGORY_CODE_URL_PARAM))
		{
			return catCodePath.substring(catCodePath.indexOf(SimonCoreConstants.CATEGORY_CODE_URL_PARAM)
					+ SimonCoreConstants.CATEGORY_CODE_URL_PARAM.length(), catCodePath.indexOf("/p/"));
		}
		return null;
	}

	/**
	 * This method is used to set model attributes inside {@link Model} for the product detail page .
	 *
	 * @param productCode
	 *           used to identify which product is to be processed to display
	 * @param model
	 *           of type {@link Model} used to set attributes
	 * @param productData
	 *           of type {@link ProductData} hold the product information like code and name used on frontend to fetch
	 *           product detail json from Solr
	 * @param catCodes
	 * @throws CMSItemNotFoundException
	 *            throws {@link CMSItemNotFoundException} when the page not found for the product code provided
	 */
	private void populateModelAttributes(final String productCode, final Model model, final ProductData productData,
			final String catCodes) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getPageForProduct(productCode));
		model.addAttribute(PRODUCT_ATTRIBUTE, productData);
		model.addAttribute(new ReviewForm());
		model.addAttribute("pageType", PageType.PRODUCT.name());
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
		final List<Breadcrumb> breadCrumbs = extBreadcrumbBuilder.getBreadcrumbs(catCodes);
		model.addAttribute("productCategory", extWebUtils.populateCategoryForAnalytics(breadCrumbs));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadCrumbs);
		model.addAttribute("minimumStockQuantity", configurationService.getConfiguration().getString("minimum.stock.quantity"));
		model.addAttribute("maximumStockQuantity", configurationService.getConfiguration().getString("maximum.stock.quantity"));
	}

	private ProductSearchPageData<SearchStateData, ProductData> getProductReferencesData(final ProductModel productModel)
	{
		ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
		final Collection<ProductReferenceModel> references = productModel.getProductReferences();
		if (CollectionUtils.isNotEmpty(references))
		{
			final List<String> productCodes = new ArrayList<>();

			for (final ProductReferenceModel reference : references)
			{
				final ProductModel product = reference.getTarget();
				if (null != product)
				{
					ProductModel baseProduct;
					if (product instanceof GenericVariantProductModel)
					{
						baseProduct = ((GenericVariantProductModel) product).getBaseProduct();
					}
					else
					{
						baseProduct = product;
					}
					productCodes.add(baseProduct.getCode());
				}
			}
			final SearchStateData searchState = prepareSearchQueryData();
			final PageableData pageableData = preparePageableData();
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
			sessionService.setAttribute(SimonCoreConstants.PRODUCT_CODE_LIST, productCodes);
			searchPageData = productSearchFacade.textSearch(searchState, pageableData);
			if (null != searchPageData)
			{
				searchPageData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			}
		}
		return searchPageData;
	}

	/**
	 * This method prepares the PageableData object.
	 *
	 * @return PageableData
	 */
	private PageableData preparePageableData()
	{
		final int pageSize = configurationService.getConfiguration()
				.getInt(SimonFacadesConstants.PRODUCT_RECOMMENDATIONS_PAGE_SIZE);
		final PageableData pageableData = new PageableData();
		pageableData.setPageSize(pageSize);
		return pageableData;
	}

	/**
	 * This method prepares the SearchStateData object.
	 *
	 * @return SearchStateData
	 */
	private SearchStateData prepareSearchQueryData()
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(StringUtils.EMPTY);
		final SearchStateData searchState = new SearchStateData();
		searchState.setQuery(searchQueryData);
		return searchState;
	}

	/**
	 * Override the updatePageTitle method to resolve title for product detial page pass the cateogry code to get the sub
	 * category
	 *
	 * @param searchText
	 * @param model
	 * @param catCodes
	 */
	protected void updatePageTitle(final String productCode, final Model model, final String catCodes)
	{
		storeContentPageTitleInModel(model, pageTitleResolver.resolveProductPageTitle(productCode, catCodes));
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ProductPageModel pageForRequest = (ProductPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.FOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			setMetaData(model, pageForRequest);
		}
	}

	protected void setMetaData(final Model model, final ProductPageModel pageForRequest)
	{
		final List<MetaElementData> metadata = new LinkedList<>();
		final StringBuilder meta = new StringBuilder();
		if (StringUtils.isNotEmpty((meta.append(pageForRequest.getIndex().toString().toLowerCase()).append(",")
				.append(pageForRequest.getFollow().toString().toLowerCase())).toString()))
		{
			final MetaElementData element = new MetaElementData();
			element.setName("robots");
			element.setContent(meta.toString());
			model.addAttribute("metatags", metadata);
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, meta.toString());
		}
		super.setUpMetaData(model, null, null);
	}
}
