package com.simon.storefront.interceptors.beforeview;

import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.generated.storefront.interceptors.beforeview.UiThemeResourceBeforeViewHandler;


/**
 * Interceptor to setup the paths to the UI resource paths in the model before passing it to the view. Sets up the path
 * to the web accessible UI resources for the following: * The current site * The current theme * The common resources
 * All of these paths are qualified by the current UiExperienceLevel
 */
public class ExtUiThemeResourceBeforeViewHandler extends UiThemeResourceBeforeViewHandler
{
	private static final String DOMAIN_URL = "website.simon.https";
	private static final String SCRIPT_ANALYTICS_URL = "analytics.env.header.key";
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "userService")
	private UserService userService;

	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		super.beforeView(request, response, modelAndView);
		final String domainURL = (String) configurationService.getConfiguration().getProperty(DOMAIN_URL);
		modelAndView.addObject(SimonCoreConstants.DOMAIN_URL, domainURL);
		final String scriptURL = (String) configurationService.getConfiguration().getProperty(SCRIPT_ANALYTICS_URL);
		modelAndView.addObject(SimonCoreConstants.SCRIPT_URL, scriptURL);
		final String relativePath = request.getRequestURI();
		modelAndView.addObject(SimonCoreConstants.PATH, relativePath);
		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			final UserModel user = userService.getCurrentUser();
			if (user instanceof CustomerModel)
			{
				final CustomerModel customer = (CustomerModel) user;
				modelAndView.addObject("user", customer.getExternalId());
			}
		}
	}
}
