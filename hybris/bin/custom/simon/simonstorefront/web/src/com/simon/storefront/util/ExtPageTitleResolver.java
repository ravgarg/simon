package com.simon.storefront.util;

import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * Use to resolve page title
 */
public class ExtPageTitleResolver extends PageTitleResolver
{
	@Value("${navigation.home.category.code}")
	private String homeCategoryCode;
	@Resource
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Resource(name = "messageSource")
	private MessageSource messageSource;
	@Resource(name = "i18nService")
	private I18NService i18nService;

	/**
	 * @param productCode
	 * @param catCodes
	 * @return String
	 */
	public String resolveProductPageTitle(final String productCode, final String catCodes)
	{
		CategoryModel source;
		// Lookup the product
		final ProductModel product = getProductService().getProductForCode(productCode);
		final int lastUnderscoreIndex = catCodes.lastIndexOf(SimonCoreConstants.UNDERSCORE);
		final String lastCategoryCode = catCodes.substring(lastUnderscoreIndex + 1);
		source = getCommerceCategoryService().getCategoryForCode(lastCategoryCode);
		return resolveProductPageTitle(product, StringUtils.isNotEmpty(source.getName()) ? source.getName() : StringUtils.EMPTY);
	}


	/**
	 * Use to resolve page title for product detail page
	 *
	 * @param product
	 * @param catName
	 * @return String
	 */
	public String resolveProductPageTitle(final ProductModel product, final String catName)
	{

		// Construct page title
		final String designerName = product.getProductDesigner() != null ? product.getProductDesigner().getName()
				: StringUtils.EMPTY;
		final String identifier = product.getName();
		final String articleNumber = product.getCode();
		final String productName = StringUtils.isEmpty(identifier) ? articleNumber : identifier;
		final StringBuilder builder = new StringBuilder(StringUtils.isEmpty(designerName) ? StringUtils.EMPTY : designerName);

		if (StringUtils.isNotEmpty(catName))
		{
			builder.append(SimonWebConstants.SINGLE_SPACE).append(catName);
		}
		builder.append(SimonWebConstants.SINGLE_SPACE).append(productName).append(TITLE_WORD_SEPARATOR);
		builder.append(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT,
				i18nService.getCurrentLocale()));

		return StringEscapeUtils.escapeHtml(builder.toString());

	}

	/**
	 * Use to resolve page title for CLP, PLP remove the currentsite
	 *
	 * @param category
	 *
	 */
	@Override
	public String resolveCategoryPageTitle(final CategoryModel category)
	{
		final StringBuilder stringBuilder = new StringBuilder();
		final List<CategoryModel> superCategories = category.getSupercategories();
		if (CollectionUtils.isNotEmpty(superCategories))
		{
			boolean isCLP = false;
			for (final CategoryModel cat : superCategories)
			{
				if (cat.getCode().equalsIgnoreCase(homeCategoryCode))
				{
					isCLP = true;
					break;
				}
			}

			if (isCLP)
			{
				stringBuilder.append(category.getName());
			}
			else
			{
				final List<CategoryModel> categories = getCategoryPathForCategory(category);
				Collections.reverse(categories);
				final String firstCategory = buildCatCodePathString(categories);
				stringBuilder.append(firstCategory).append(SimonWebConstants.SINGLE_SPACE);
				stringBuilder.append(categories.get(categories.size() - 1).getName());

			}

		}
		stringBuilder.append(SimonWebConstants.TITLE_WORD_SEPARATOR).append(messageSource.getMessage(
				SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT, i18nService.getCurrentLocale()));
		return StringEscapeUtils.escapeHtml(stringBuilder.toString());
	}

	/**
	 * @param category
	 * @return List<CategoryModel>
	 */
	public List<CategoryModel> getCategoryPathForCategory(final CategoryModel category)
	{
		return super.getCategoryPath(category);
	}


	/**
	 * Use to resolve page title for CLP, PLP remove the currentsite
	 *
	 * @param title
	 *
	 */
	@Override
	public String resolveContentPageTitle(final String title)
	{
		final StringBuilder builder = new StringBuilder();
		if (!StringUtils.isEmpty(title))
		{
			builder.append(title);
		}
		builder.append(SimonWebConstants.TITLE_WORD_SEPARATOR).append(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT,
				null, SimonWebConstants.TITLE_CONSTANT, i18nService.getCurrentLocale()));
		return StringEscapeUtils.escapeHtml(builder.toString());
	}

	/**
	 *
	 * @param searchText
	 * @return category hierarchy path
	 */
	public String resolveSearchTextPageTitle(final String searchText)
	{
		final StringBuilder builder = new StringBuilder();
		if (!StringUtils.isEmpty(searchText))
		{
			builder.append(searchText).append(SimonWebConstants.TITLE_WORD_SEPARATOR);
		}
		builder.append(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT,
				i18nService.getCurrentLocale()));

		return StringEscapeUtils.escapeHtml(builder.toString());
	}

	/**
	 *
	 * @param path
	 *           Current category model path
	 * @return category name after home category
	 */
	public String buildCatCodePathString(final List<CategoryModel> path)
	{
		int index = 0;
		for (int i = 0; i < path.size(); i++)
		{

			if (path.get(i).getCode().equalsIgnoreCase(homeCategoryCode))
			{
				index = i;
				break;
			}
		}

		return path.size() > index ? path.get(index + 1).getName() : StringUtils.EMPTY;
	}

}
