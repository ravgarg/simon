package com.simon.storefront.security;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;

import com.simon.facades.customer.CustomerFavoriteFacade;
import com.simon.generated.storefront.security.LoginAuthenticationFailureHandler;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * This is the class to handle login Authentication Failure page redirection. This extends OOB
 * LoginAuthenticationFailureHandler which use defaultFailureUrl to redirect
 *
 */
public class ExtLoginAuthenticationFailureHandler extends LoginAuthenticationFailureHandler
{
	@Resource
	private CustomerFavoriteFacade customerFavoriteFacade;



	/**
	 * This is the method which is used to handle login Authentication Failure page redirection.
	 */

	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException
	{

		// Store the j_username in the session
		request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", request.getParameter("j_username"));

		customerFavoriteFacade.setFavoriteDataInSession(request.getParameter(SimonWebConstants.FAV_TYPE),
				request.getParameter(SimonWebConstants.FAV_NAME), request.getParameter(SimonWebConstants.FAV_ID));
		super.onAuthenticationFailure(request, response, exception);
	}

}
