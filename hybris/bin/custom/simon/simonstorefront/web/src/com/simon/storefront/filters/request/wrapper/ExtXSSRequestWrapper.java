package com.simon.storefront.filters.request.wrapper;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.collections.CaseInsensitiveStringMap;

import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.simon.storefront.filters.ExtXSSFilter;



/**
 * Custom XSSRequestWrapper for accomodating the XSS wrapping capabiliy fot JSON data sent in the request payload
 *
 */
public class ExtXSSRequestWrapper extends HttpServletRequestWrapper
{

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	private ExtXSSFilter.XSSValueTranslator lazyValueTranslator;
	private Boolean noParametersStripped;
	private Map<String, String[]> strippedParametersMap;
	private Boolean noHeadersStripped;
	private Map<String, String[]> strippedHeadersMap;

	/**
	 * @param servletRequest
	 * @param translator
	 */
	public ExtXSSRequestWrapper(final HttpServletRequest servletRequest, final ExtXSSFilter.XSSValueTranslator translator)
	{
		super(servletRequest);
		this.lazyValueTranslator = translator;
	}

	/**
	 * @param servletRequest
	 * @param strippedHeadersMap
	 * @param strippedParametersMap
	 */
	public ExtXSSRequestWrapper(final HttpServletRequest servletRequest, final Map<String, String[]> strippedHeadersMap,
			final Map<String, String[]> strippedParametersMap)
	{
		super(servletRequest);
		this.strippedHeadersMap = strippedHeadersMap;
		this.strippedParametersMap = strippedParametersMap;
	}

	/**
	 * Return the parameter values for a given string parameter, if no XSS striping is done then return the original map
	 */
	@Override
	public String[] getParameterValues(final String parameter)
	{
		if (this.parametersAreClean())
		{
			return super.getParameterValues(parameter);
		}
		return this.getParameterMap().get(parameter);
	}

	/**
	 * Return the parameter value for a given string parameter, if no XSS striping is done then return the original map
	 */
	@Override
	public String getParameter(final String parameter)
	{
		if (this.parametersAreClean())
		{
			return super.getParameter(parameter);
		}
		final String[] parameterValues = this.getParameterValues(parameter);
		return (parameterValues == null || parameterValues.length == 0) ? null : parameterValues[0];
	}

	/**
	 * Return the parameter map, if no XSS striping is done then return the original map
	 */
	@Override
	public Map<String, String[]> getParameterMap()
	{
		return this.parametersAreClean() ? super.getParameterMap() : this.strippedParametersMap;
	}

	/**
	 * Check if the parameters are are clean i.e. no XSS stripping is done
	 */
	protected boolean parametersAreClean()
	{
		this.ensureParametersTranslated();
		return Boolean.TRUE.equals(this.noParametersStripped);
	}

	/**
	 * Method to check if the parameters have any XSS stripping requirement
	 */
	protected void ensureParametersTranslated()
	{
		if (this.noParametersStripped == null)
		{
			final Map originalNoCaseInsensitiveMap = super.getParameterMap();
			final Map<String, String[]> stripped = this.lazyValueTranslator.translateParameters(originalNoCaseInsensitiveMap);
			if (originalNoCaseInsensitiveMap.equals(stripped))
			{
				this.noParametersStripped = Boolean.TRUE;
			}
			else
			{
				this.noParametersStripped = Boolean.FALSE;
				this.strippedParametersMap = stripped;
			}
		}
	}

	/**
	 * Return the header value for a given string name, if no XSS striping is done then return the original header value
	 */
	@Override
	public String getHeader(final String name)
	{
		if (this.headersAreClean())
		{
			return super.getHeader(name);
		}
		final String[] strippedHeaderValues = this.strippedHeadersMap.get(name);
		return (strippedHeaderValues == null || strippedHeaderValues.length == 0) ? null : strippedHeaderValues[0];
	}

	/**
	 * Return the header values for a given string name, if no XSS striping is done then return the original header
	 * values
	 */
	@Override
	public Enumeration getHeaders(final String name)
	{
		if (this.headersAreClean())
		{
			return super.getHeaders(name);
		}
		final String[] strippedHeaderValues = this.strippedHeadersMap.get(name);
		if (strippedHeaderValues == null || strippedHeaderValues.length == 0)
		{
			return Collections.emptyEnumeration();
		}
		return Collections.enumeration(Arrays.asList(strippedHeaderValues));
	}

	/**
	 * Method to check if the headers have any XSS stripping requirement
	 */
	protected void ensureHeadersTranslated()
	{
		if (this.strippedHeadersMap == null)
		{
			final Map<String, String[]> headerValueMap = ExtXSSFilter.getValueMapFromHeaders((HttpServletRequest) this.getRequest());
			final Map<String, String[]> stripped = this.lazyValueTranslator.translateHeaders(headerValueMap);
			if (stripped.equals(headerValueMap))
			{
				this.noHeadersStripped = Boolean.TRUE;
			}
			else
			{
				this.noHeadersStripped = Boolean.FALSE;
				this.strippedHeadersMap = new CaseInsensitiveStringMap(stripped);
			}
		}
	}

	/**
	 * Check if the headers are are clean i.e. no XSS stripping is done
	 */
	protected boolean headersAreClean()
	{
		this.ensureHeadersTranslated();
		return Boolean.TRUE.equals(this.noHeadersStripped);
	}

}
