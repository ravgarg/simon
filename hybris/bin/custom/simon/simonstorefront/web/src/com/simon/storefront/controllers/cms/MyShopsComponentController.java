package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.MyShopsComponentModel;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Category Navigation Controller for Category Navigation CMS Component. This controller is used to return a DTO that
 * contains all the navigation nodes.
 */
@Controller("MyShopsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.MyShopsComponent)
public class MyShopsComponentController extends AbstractCMSComponentController<MyShopsComponentModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(MyShopsComponentController.class);
	private static final String ACCOUNT_PAGE_NOTIFICATION_UPDATE_KEY = "accountPageNotificationUpdateKey";
	private static final String FAVORITE_PAGE_NOTIFICATION_UPDATE_KEY = "favoritePageNotificationUpdateKey";
	private static final String MY_FAVORITES_MESSAGE_VALUE_KEY = "text.myfavorites.view.error.msg.text";
	private static final String MY_FAVORITES_NO_MESSAGE_VALUE_KEY = "text.no.load.myfavorites.view.msg.text";
	private static final String ACCOUNT_DETAILS_TITLE_KEY = "titleKey";
	private static final String TEXT_ACCOUNT_MYSTORE_TITLE = "text.account.myStores.title";
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final MyShopsComponentModel component)
	{
		if (extCustomerFacade.checkIfUserIsLoggedIn())
		{
			final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.RETAILERS.name());
			try
			{
				final boolean status = extCustomerFacade.getAllFavouriteStores(currentCustomerData);
				if (!status)
				{
					model.addAttribute(ACCOUNT_PAGE_NOTIFICATION_UPDATE_KEY, MY_FAVORITES_MESSAGE_VALUE_KEY);
					model.addAttribute(FAVORITE_PAGE_NOTIFICATION_UPDATE_KEY, MY_FAVORITES_NO_MESSAGE_VALUE_KEY);
				}
			}
			catch (final DuplicateUidException e)
			{
				LOGGER.error("Stores Update Failed while fetching and saving storeData: ", e);
			}
			if (!CollectionUtils.isEmpty(currentCustomerData.getMyStores()))
			{
				final List<StoreData> myStores = new ArrayList<>(currentCustomerData.getMyStores());
				Collections.sort(myStores, (o1, o2) -> (o1.getName().compareTo(o2.getName())));
				model.addAttribute("storeList", myStores);
				model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, TEXT_ACCOUNT_MYSTORE_TITLE);
			}
			model.addAttribute("alignment", component.getAlignment().getCode());
			model.addAttribute("pageType", "storedirectory");
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}

	}

	@Override
	protected String getView(final MyShopsComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(MyShopsComponentModel._TYPECODE);
	}

}
