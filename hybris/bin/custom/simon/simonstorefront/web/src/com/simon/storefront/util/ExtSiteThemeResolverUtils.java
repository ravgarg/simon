package com.simon.storefront.util;

import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;

import com.simon.generated.storefront.util.SiteThemeResolverUtils;


/**
 * This is extended Theme resolver utility class which extend OOB SiteThemeResolverUtils. We have extended it as while
 * hitting any page from external link we do not get uiExperienceLevel from default so in this class we are overwriting
 * resolveThemeForCurrentSite method and add null check to prevent null pointer.This is done to get Sign in page from
 * external Link
 */
public class ExtSiteThemeResolverUtils extends SiteThemeResolverUtils
{


	/**
	 * In this method we have added null check for uiExperienceLevel and putting uiExperienceLevel.code() as Desktop.
	 */
	@Override
	public String resolveThemeForCurrentSite()
	{
		final UiExperienceLevel uiExperienceLevel = getUiExperienceService().getUiExperienceLevel();

		// Resolve Theme from CMSSiteService
		final CMSSiteModel currentSite = getCmsSiteService().getCurrentSite();
		if (currentSite != null)
		{
			return combineSiteAndTheme(uiExperienceLevel != null ? uiExperienceLevel.getCode() : UiExperienceLevel.DESKTOP.getCode(),
					currentSite.getUid(), getThemeNameForSite(currentSite));
		}
		return null;
	}

}
