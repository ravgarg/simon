package com.simon.storefront.security;

import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.customer.CustomerFavoriteFacade;
import com.simon.generated.storefront.security.GUIDAuthenticationSuccessHandler;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * ExtGUIDAuthenticationSuccessHandler is Authenticate success handler class which extends
 * GUIDAuthenticationSuccessHandler. In this we getting favorite Id and favorite type as request parameter and set this
 * value in session . This session favorite data will further being used by favorite service to make any favorite Type
 * as Favorite. Session data will be removed after using its value.
 */
public class ExtGUIDAuthenticationSuccessHandler extends GUIDAuthenticationSuccessHandler
{
	@Resource
	private CustomerFavoriteFacade customerFavoriteFacade;
	@Resource
	private SessionService sessionService;

	/**
	 * In this method we are getting favorite Id and favorite type as request parameter and set this value in session and
	 * call super method to do rest same OOB
	 *
	 */
	@Override
	public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{
		customerFavoriteFacade.setFavoriteDataInSession(request.getParameter(SimonWebConstants.FAV_TYPE),
				request.getParameter(SimonWebConstants.FAV_NAME), request.getParameter(SimonWebConstants.FAV_ID));

		super.onAuthenticationSuccess(request, response, authentication);
		sessionService.setAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE, Boolean.TRUE);
		final String trackOrderRedirect = request.getParameter(SimonWebConstants.FAV_NAME);
		if (StringUtils.isNotEmpty(trackOrderRedirect) && trackOrderRedirect.equals(SimonWebConstants.TRACK_ORDER))
		{
			sessionService.setAttribute(SimonCoreConstants.REDIRECT_LOGIN_TRACK_ORDER, Boolean.TRUE);
		}
	}
}
