package com.simon.storefront.controllers.pages.email;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.facades.email.ShareEmailData;
import com.simon.facades.share.email.ShareEmailFacade;
import com.simon.integration.share.email.exceptions.SharedEmailIntegrationException;
import com.simon.storefront.forms.ShareEmailForm;


/**
 * This is controller to Share Deal Email. This controller is used to share email's from PDP and deal Page.
 */
@Controller
@RequestMapping("/share-email")
public class ShareEmailController extends AbstractPageController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ShareEmailController.class);
	private static final String SEND_EMAIL = "send-email";
	private static final String SLASH = "/";
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";

	@Resource(name = "shareEmailFacade")
	private ShareEmailFacade shareEmailFacade;

	/**
	 * Share email from PDP and Deal page. By this method we can share email by using Share mail Facade. It return
	 * success when email is send properly and return error when we have any exception
	 *
	 * @param shareEmailForm
	 * @param request
	 * @param response
	 * @return String
	 */
	@RequestMapping(value = SLASH + SEND_EMAIL, method = RequestMethod.POST)
	@ResponseBody
	public String shareEmail(@ModelAttribute("shareEmailForm") final ShareEmailForm shareEmailForm,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		String result = SUCCESS;
		try
		{
			if (StringUtils.isNotEmpty(shareEmailForm.getProductId()))
			{
				final ShareEmailData sharedEmailCommonData = populateShareEmailCommonData(shareEmailForm);
				shareEmailFacade.getProductDetailsToSharedProductEmail(sharedEmailCommonData);
				LOGGER.debug("Email Product data successfully shared for product code: ", shareEmailForm.getProductId());
			}
			else if (StringUtils.isNotEmpty(shareEmailForm.getDealId()))
			{
				final ShareEmailData shareEmailData = populateShareEmailCommonData(shareEmailForm);
				shareEmailFacade.shareDealEmail(shareEmailForm.getDealId(), shareEmailData);
				LOGGER.debug("Email Deal data successfully shared for deal code: ", shareEmailForm.getDealId());
			}
			else
			{
				LOGGER.debug("Product or Offer code not found to share Email");
				result = ERROR;
			}

		}
		catch (final SharedEmailIntegrationException ex)
		{
			LOGGER.error("Exception occured : getting error code {} and error message {} and exception is {} ", ex.getCode(),
					ex.getMessage(), ex);
			result = ERROR;
		}
		return result;
	}

	/**
	 * @method populateShareEmailCommonData
	 * @param shareEmailForm
	 * @return ShareEmailData
	 */
	private ShareEmailData populateShareEmailCommonData(final ShareEmailForm shareEmailForm)
	{
		final ShareEmailData sharedEmailCommonData = new ShareEmailData();
		sharedEmailCommonData.setEmailFrom(shareEmailForm.getFromEmail());
		sharedEmailCommonData.setEmailTo(shareEmailForm.getToEmail());
		sharedEmailCommonData.setMessage(shareEmailForm.getComments());
		sharedEmailCommonData.setProductCode(shareEmailForm.getProductId());
		return sharedEmailCommonData;
	}


}
