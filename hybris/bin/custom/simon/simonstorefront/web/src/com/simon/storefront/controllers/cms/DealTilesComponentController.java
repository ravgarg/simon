package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.DealModel;
import com.simon.core.model.DealTilesComponentModel;
import com.simon.facades.account.data.DealData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.user.ExtUserFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.controllers.account.ExtAccountPageController;


/**
 * Controller to show deals on Deals Landing Page
 */
@Controller("DealTilesComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.DealTilesComponent)
public class DealTilesComponentController extends AbstractCMSComponentController<DealTilesComponentModel>
{
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "extUserFacade")
	private ExtUserFacade userFacade;

	@Resource(name = "extDealConverter")
	private Converter<DealModel, DealData> extDealConverter;


	private static final Logger LOGGER = Logger.getLogger(ExtAccountPageController.class);

	/**
	 * Data of all Designers
	 *
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param model
	 *           of type {@link Model}
	 * @param component
	 *           of type {@link DealTilesComponentModel}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final DealTilesComponentModel component)
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final List<DealData> dealDatas = new ArrayList<>();
		try
		{
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.OFFERS.name());
			if (!userFacade.isAnonymousUser())
			{
				extCustomerFacade.getAllFavoriteDeals(currentCustomerData, dealDatas);
				model.addAttribute("favDealsList", dealDatas);
			}

			List<DealData> dealDatasList = new ArrayList<>();
			final List<DealModel> dealModelList = component.getDeals();
			if (CollectionUtils.isNotEmpty(dealModelList))
			{
				dealDatasList = extDealConverter.convertAll(dealModelList);
			}
			extCustomerFacade.updateFavDeals(currentCustomerData, dealDatasList);
			model.addAttribute("dealsDataList", dealDatasList);
			model.addAttribute("pageType", "deallanding");
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.error("designer Update Failed while fetching and saving dealData: ", e);
		}
	}

	@Override
	protected String getView(final DealTilesComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(getTypeCode(component));
	}

}
