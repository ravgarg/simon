package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.FeaturedDesignerComponentModel;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Controller for CMS FeaturedDesignerComponent
 */

@Controller("FeaturedDesignerComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.FeaturedDesignerComponent)
public class FeaturedDesignerComponentController extends AbstractCMSComponentController<FeaturedDesignerComponentModel>
{

	private static final String DESIGNER = "designer";

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * Data of all Featured Designers
	 *
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param model
	 *           of type {@link Model}
	 * @param component
	 *           of type {@link FeaturedDesignerComponentModel}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final FeaturedDesignerComponentModel component)
	{
		if (component.getDesigner() != null)
		{
			final DesignerData designerData = extCustomerFacade.getFeaturedDesignerData(component.getDesigner());
			designerData.setFavorite(extCustomerFacade.isFavDesigner(designerData.getCode()));
			model.addAttribute(DESIGNER, designerData);
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}
	}

	@Override
	protected String getView(final FeaturedDesignerComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(getTypeCode(component));
	}

}
