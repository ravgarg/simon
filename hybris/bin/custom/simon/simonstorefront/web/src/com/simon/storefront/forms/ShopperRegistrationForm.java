package com.simon.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;


/**
 * Form object for Shopper registration which extends Register form.
 */
public class ShopperRegistrationForm extends RegisterForm
{

	private String zip;
	private String country;
	private String birthmonth;
	private int birthyear;
	private String gender;
	private String ordercode;
	private String primaryMall;
	private String alternateMallIds;




	public String getOrdercode()
	{
		return ordercode;
	}

	public void setOrdercode(final String ordercode)
	{
		this.ordercode = ordercode;
	}

	/**
	 * get Zip
	 *
	 * @return String
	 */
	public String getZip()
	{
		return zip;
	}

	/**
	 * Set Zip
	 *
	 * @param zip
	 */
	public void setZip(final String zip)
	{
		this.zip = zip;
	}

	/**
	 * Get Country
	 *
	 * @return String
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * Set Country
	 *
	 * @param country
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * Get Birth Month
	 *
	 * @return String
	 */
	public String getBirthmonth()
	{
		return birthmonth;
	}

	/**
	 * Set Birth Month
	 *
	 * @param birthmonth
	 */
	public void setBirthmonth(final String birthmonth)
	{
		this.birthmonth = birthmonth;
	}

	/**
	 * Get Birth Year
	 *
	 * @return String
	 */
	public int getBirthyear()
	{
		return birthyear;
	}

	/**
	 * Set Birth Year
	 *
	 * @param birthyear
	 */
	public void setBirthyear(final int birthyear)
	{
		this.birthyear = birthyear;
	}

	/**
	 * GEt Gender
	 *
	 * @return String
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * Set Gender
	 *
	 * @param gender
	 */
	public void setGender(final String gender)
	{
		this.gender = gender;
	}

	/**
	 * @return String
	 */
	public String getPrimaryMall()
	{
		return primaryMall;
	}

	/**
	 * @param primaryMall
	 */
	public void setPrimaryMall(final String primaryMall)
	{
		this.primaryMall = primaryMall;
	}

	/**
	 * @return String
	 */
	public String getAlternateMallIds()
	{
		return alternateMallIds;
	}

	/**
	 * @param alternateMallIds
	 */
	public void setAlternateMallIds(final String alternateMallIds)
	{
		this.alternateMallIds = alternateMallIds;
	}



}
