/**
 *
 */
package com.simon.storefront.constant;

/**
 * @author skaus9
 *
 */
public final class SimonWebConstants
{

	public static final String EXTENSIONNAME = "simonstorefront";
	public static final String NAME = "name";
	public static final String HYPHEN = "-";

	public static final String SINGLE_SPACE = " ";
	public static final String TOP_LEVEL_CATEGORY = "SPO";

	public static final String CHECKOUT_DATA = "checkoutData";

	public static final String FAV_TYPE = "favType";
	public static final String FAV_ID = "favId";
	public static final String FAV_NAME = "favName";

	public static final String COUNTRIES_VALID_FOR_ZIPCODE = "simonstorefront.account.countries.zipcode.valid";

	public static final String BIRTH_MONTH = "Jan";
	public static final int BIRTH_YEAR = 1907;
	public static final String GENDER_CODE = "NOT_DISCLOSED";
	public static final String JMS_ERROR = "jmsError";
	public static final String GLOBALFLASH_MSG = "global.flash.msg";
	public static final String SPREEDLY_ENV_KEY = "spreedly.env.key";

	public static final String TITLE_WORD_SEPARATOR = " | ";
	public static final String TITLE_CONSTANT = "page.title.constant";
	public static final String COLOR_CODE = "color";
	public static final String DOUBLE_QOUTES = "\"";

	public static final String TRACK_ORDER = "track-order";

	private SimonWebConstants()
	{

	}

}
