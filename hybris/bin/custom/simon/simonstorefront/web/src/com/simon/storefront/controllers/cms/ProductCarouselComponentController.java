package com.simon.storefront.controllers.cms;

import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.generated.storefront.controllers.cms.AbstractAcceleratorCMSComponentController;


/**
 * Controller for CMS ProductCarouselComponent.
 *
 */
@Controller("ProductCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.ProductCarouselComponent)
public class ProductCarouselComponentController extends AbstractAcceleratorCMSComponentController<ProductCarouselComponentModel>
{
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource
	private SessionService sessionService;

	@Resource
	private ConfigurationService configurationService;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final ProductCarouselComponentModel component)
	{
		ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
		final List<ProductModel> products = component.getProducts();
		if (CollectionUtils.isNotEmpty(products))
		{
			final List<String> productCodes = component.getProductCodes();
			final SearchStateData searchState = prepareSearchQueryData();

			final PageableData pageableData = preparePageableData();
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
			sessionService.setAttribute(SimonCoreConstants.PRODUCT_CODE_LIST, productCodes);
			searchPageData = productSearchFacade.textSearch(searchState, pageableData);

			if (null != searchPageData)
			{
				searchPageData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			}
		}
		model.addAttribute("title", component.getTitle());
		model.addAttribute("searchPageData", searchPageData);
	}

	/**
	 * This method prepares the PageableData object.
	 *
	 * @return PageableData
	 */
	private PageableData preparePageableData()
	{
		final int pageSize = configurationService.getConfiguration()
				.getInt(SimonFacadesConstants.PRODUCT_RECOMMENDATIONS_PAGE_SIZE);
		final PageableData pageableData = new PageableData();
		pageableData.setPageSize(pageSize);
		return pageableData;
	}

	/**
	 * This method prepares the SearchStateData object.
	 *
	 * @return SearchStateData
	 */
	private SearchStateData prepareSearchQueryData()
	{
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(StringUtils.EMPTY);
		final SearchStateData searchState = new SearchStateData();
		searchState.setQuery(searchQueryData);
		return searchState;
	}

}
