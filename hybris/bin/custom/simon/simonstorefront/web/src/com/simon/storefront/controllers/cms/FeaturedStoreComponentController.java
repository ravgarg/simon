package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.FeaturedStoreComponentModel;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Controller for CMS FeaturedStoreComponent
 */

@Controller("FeaturedStoreComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.FeaturedStoreComponent)
public class FeaturedStoreComponentController extends AbstractCMSComponentController<FeaturedStoreComponentModel>
{

	private static final String FEATURED_STORE_DATA = "featuredStoreData";

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * Data of all Featured Store
	 *
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param model
	 *           of type {@link Model}
	 * @param component
	 *           of type {@link FeaturedStoreComponentModel}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final FeaturedStoreComponentModel component)
	{
		if (component.getShop() != null)
		{
			final StoreData storeData = extCustomerFacade.getFeaturedStoreData(component.getShop());
			storeData.setFavorite(extCustomerFacade.isFavStore(storeData.getId()));
			model.addAttribute(FEATURED_STORE_DATA, storeData);
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}
	}

	@Override
	protected String getView(final FeaturedStoreComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(getTypeCode(component));
	}

}
