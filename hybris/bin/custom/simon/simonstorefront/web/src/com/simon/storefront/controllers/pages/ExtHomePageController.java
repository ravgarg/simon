package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.generated.storefront.controllers.pages.HomePageController;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtWebUtils;


/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class ExtHomePageController extends HomePageController
{
	private static final String PAGETYPE = "pageType";
	private static final String PAGENAME = "homepage";

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * @param logout
	 * @param model
	 * @param redirectModel
	 * @param request
	 * @param response
	 * @return vieworpage
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String home(@RequestParam(value = "logout", defaultValue = "false") final boolean logout, final Model model,
			final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForLabelOrId(null);
		model.addAttribute(SimonCoreConstants.CANONICAL_URL, setCanonicalInCmsPageInModel(pageForRequest));
		model.addAttribute(PAGETYPE, PAGENAME);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);

		storeCmsPageInModel(model, pageForRequest);
		return super.home(logout, model, redirectModel);
	}

	/**
	 * Gets the breadcrumb for designer layout and designer landing.
	 *
	 * @param cmsPage
	 * @return String
	 */
	private String setCanonicalInCmsPageInModel(final AbstractPageModel cmsPage)
	{
		String result = "";

		if (cmsPage instanceof ContentPageModel)
		{
			result = StringUtils.isNotEmpty(((ContentPageModel) cmsPage).getCanonicalTag())
					? ((ContentPageModel) cmsPage).getCanonicalTag()
					: SimonCoreConstants.NOT_APPLICABLE;
		}
		return result;
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.FOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
