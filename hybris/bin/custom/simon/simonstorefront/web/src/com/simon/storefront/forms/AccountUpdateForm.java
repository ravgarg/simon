package com.simon.storefront.forms;

public class AccountUpdateForm
{
	private String firstName;
	private String lastName;
	private String email;
	private String zipCode;
	private String passwordUpdate;
	private String country;
	private String birthMonth;
	private int birthYear;
	private String gender;
	private String currentPassword;
	private String password;
	private String confirmPassword;
	private String primaryMall;
	private boolean updateCenter;
	private String alternateMallIds;
	private boolean optedInEmail;

	/**
	 * @return the firstName
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *           the zipCode to set
	 */
	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}


	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the birthMonth
	 */
	public String getBirthMonth()
	{
		return birthMonth;
	}

	/**
	 * @param birthMonth
	 *           the birthMonth to set
	 */
	public void setBirthMonth(final String birthMonth)
	{
		this.birthMonth = birthMonth;
	}

	/**
	 * @return the birthYear
	 */
	public int getBirthYear()
	{
		return birthYear;
	}

	/**
	 * @param birthYear
	 *           the birthYear to set
	 */
	public void setBirthYear(final int birthYear)
	{
		this.birthYear = birthYear;
	}

	/**
	 * @return the gender
	 */
	public String getGender()
	{
		return gender;
	}

	/**
	 * @param gender
	 *           the gender to set
	 */
	public void setGender(final String gender)
	{
		this.gender = gender;
	}

	/**
	 * @return the password
	 */
	public String getPassword()
	{
		return password;
	}

	/**
	 * @param password
	 *           the password to set
	 */
	public void setPassword(final String password)
	{
		this.password = password;
	}

	/**
	 * @return the confirmPassword
	 */
	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	/**
	 * @param confirmPassword
	 *           the confirmPassword to set
	 */
	public void setConfirmPassword(final String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	/**
	 * @return the passwordUpdate
	 */
	public String getPasswordUpdate()
	{
		return passwordUpdate;
	}

	/**
	 * @param passwordUpdate
	 *           the passwordUpdate to set
	 */
	public void setPasswordUpdate(final String passwordUpdate)
	{
		this.passwordUpdate = passwordUpdate;
	}

	/**
	 * @return currentPassword
	 */
	public String getCurrentPassword()
	{
		return currentPassword;
	}

	/**
	 * @param currentPassword
	 */
	public void setCurrentPassword(final String currentPassword)
	{
		this.currentPassword = currentPassword;
	}

	/**
	 * @return the primaryMall
	 */
	public String getPrimaryMall()
	{
		return primaryMall;
	}

	/**
	 * @param primaryMall
	 *           the primaryMall to set
	 */
	public void setPrimaryMall(final String primaryMall)
	{
		this.primaryMall = primaryMall;
	}

	/**
	 * @return the isUpdateCenter
	 */
	public boolean isUpdateCenter()
	{
		return updateCenter;
	}

	/**
	 * @param isUpdateCenter
	 *           the isUpdateCenter to set
	 */
	public void setUpdateCenter(final boolean isUpdateCenter)
	{
		this.updateCenter = isUpdateCenter;
	}

	/**
	 * @return the alternateMallIds
	 */
	public String getAlternateMallIds()
	{
		return alternateMallIds;
	}

	/**
	 * @param alternateMallIds
	 *           the alternateMallIds to set
	 */
	public void setAlternateMallIds(final String alternateMallIds)
	{
		this.alternateMallIds = alternateMallIds;
	}

	/**
	 * @return
	 */
	public boolean isOptedInEmail()
	{
		return optedInEmail;
	}

	/**
	 * @param optedInEmail
	 */
	public void setOptedInEmail(final boolean optedInEmail)
	{
		this.optedInEmail = optedInEmail;
	}




}
