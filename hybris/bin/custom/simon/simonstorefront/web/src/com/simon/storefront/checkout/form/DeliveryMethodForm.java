package com.simon.storefront.checkout.form;

import java.util.Map;


/**
 * This form will be used to capture the selected shipping method per retailer from the review and select shipping
 * method panel.
 */
public class DeliveryMethodForm
{
	private Map<String, String> retailerSelectedDeliveryMethod;

	/**
	 * @return the retailerSelectedDeliveryMethod
	 */
	public Map<String, String> getRetailerSelectedDeliveryMethod()
	{
		return retailerSelectedDeliveryMethod;
	}

	/**
	 * @param retailerSelectedDeliveryMethod
	 *           the retailerSelectedDeliveryMethod to set
	 */
	public void setRetailerSelectedDeliveryMethod(final Map<String, String> retailerSelectedDeliveryMethod)
	{
		this.retailerSelectedDeliveryMethod = retailerSelectedDeliveryMethod;
	}

}
