package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.RegisterForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.generated.storefront.controllers.pages.CheckoutLoginController;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Checkout Login Controller. Handles login and register for the checkout flow.
 */
@Controller
@RequestMapping(value = "/login/checkout")
public class ExtCheckoutLoginController extends CheckoutLoginController
{
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	@Resource
	private SessionService sessionService;

	/**
	 * This method will be used to do the guest checkout with an anonymous user.
	 */
	@Override
	@RequestMapping(value = "/guest", method = RequestMethod.POST)
	public String doAnonymousCheckout(final GuestForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		return processAnonymousCheckoutUserRequest(form, bindingResult, model, request, response);
	}

	/**
	 * This is the method to land on login checkout page during Checkout.This method is redirecting to checkout page in
	 * error scenario with error message.
	 *
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String doCheckoutLogin(@RequestParam(value = "error", defaultValue = "false") final boolean loginError,
			final HttpSession session, final Model model, final HttpServletRequest request) throws CMSItemNotFoundException //NOSONAR
	{

		if (loginError)
		{
			model.addAttribute("pageType", "checkoutlogin");
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
			model.addAttribute("message", sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_MESSAGE_CODE));
		}
		model.addAttribute("enableCaptcha", sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA));
		model.addAttribute("pageType", "checkoutlogin");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return getDefaultLoginPage(loginError, session, model);
	}


	/**
	 * Anonymous checkout process start with an anonymos user.
	 *
	 * Creates a new guest customer and updates the session cart with this user. The session user will be anonymous and
	 * it's never updated with this guest user.
	 *
	 * At this time user will be an anonymos user
	 *
	 * @throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	 */
	@Override
	protected String processAnonymousCheckoutUserRequest(final GuestForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{

		if (bindingResult.hasErrors())
		{
			model.addAttribute(form);
			model.addAttribute(new LoginForm());
			model.addAttribute(new RegisterForm());
			model.addAttribute("pageType", "checkoutlogin");
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
			GlobalMessages.addErrorMessage(model, FORM_GLOBAL_ERROR);
			return handleRegistrationError(model);
		}

		getGuidCookieStrategy().setCookie(request, response);
		getSessionService().setAttribute(WebConstants.ANONYMOUS_CHECKOUT, Boolean.TRUE);
		return REDIRECT_PREFIX + getSuccessRedirect(request, response);
	}

}
