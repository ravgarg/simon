/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mirakl.hybris.facades.product.OfferFacade;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.dto.NavigationData;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.core.util.CommonUtils;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.facades.navigation.LeftNavigationCategoryFacade;
import com.simon.generated.storefront.controllers.pages.CategoryPageController;
import com.simon.storefront.builder.impl.ExtBreadcrumbBuilder;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Controller for a category page which provide the functionality of category page like search on the base of Category
 * Code
 */
@Controller
@RequestMapping(value = "/**/c")
public class ExtCategoryPageController extends CategoryPageController
{

	private static final String PAGE_TYPE = "pageType";

	private static final String CATEGORYLANDING = "categorylanding";

	private static final String USER_LOCATION = "userLocation";

	private static final String PRODUCT_DATA = "productData";

	private static final String CATEGORY_DESCRIPTION = "categoryDescription";

	private static final String SHOW_CATEGORIES_ONLY = "showCategoriesOnly";

	private static final String CATEGORY_NAME = "categoryName";

	private static final String SEO_LINKS = "seoLinks";

	private static final String HERO_BANNER = "heroBanner";

	private static final String PROMO_IN_LINE_BANNER = "promoInLineBanner";

	private static final String NAVIGATION_DATA = "navigationData";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtCategoryPageController.class);

	@Resource(name = "offerFacade")
	private OfferFacade offerFacade;

	@Resource
	private LeftNavigationCategoryFacade leftNavigationCategoryFacade;

	@Resource
	private ExtBreadcrumbBuilder extBreadcrumbBuilder;

	@Resource
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Resource
	private SessionService sessionService;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Method to get localized label from locale properties.
	 *
	 * @param key
	 *           the key
	 * @return Localized title string.
	 */
	protected String getLocalizedString(final String key)
	{
		return Localization.getLocalizedString(key);
	}


	/**
	 * Method is used to search base on Category Code and is used to perform search and get the result page.
	 *
	 *
	 * @param categoryCode
	 *           is the complete category hierarchy in URL.
	 * @param searchQuery
	 *           is the query to search..
	 * @param sortCode
	 *           used to sort the code.
	 * @return - a URL to the page to load
	 */
	@Override
	protected String performSearchAndGetResultsPage(final String categoryCode, final String searchQuery, final int page, // NOSONAR
			final ShowMode showMode, final String sortCode, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws UnsupportedEncodingException
	{
		final Pair<String, String> categoryPair = leftNavigationCategoryFacade.getL1AndLeafCategoryCode(categoryCode);
		final String l1CategoryCode = categoryPair.getLeft();
		final String leafCategoryCode = categoryPair.getRight();
		final CategoryModel category = getCommerceCategoryService().getCategoryForCode(leafCategoryCode);
		final List<NavigationData> leftNavData = leftNavigationCategoryFacade.getNavigationData(l1CategoryCode);
		final String redirection = checkRedirectionIfrequired(categoryCode, request, response, category, leftNavData);
		if (StringUtils.isNotEmpty(redirection))
		{
			LOGGER.debug("redirection:", redirection);
			return redirection;
		}
		model.addAttribute(NAVIGATION_DATA, leftNavData);
		if (null != category)
		{
			model.addAttribute(PROMO_IN_LINE_BANNER, category.getPromoInLineBanner());
			model.addAttribute(HERO_BANNER, category.getHeroBanner());
			model.addAttribute(SEO_LINKS, category.getSeoCmsLinks());
		}
		final CategoryPageModel categoryPage = getSimonCategoryPage(category);
		storeCmsPageInModel(model, categoryPage);
		final RequestContextData requestContextData = getSimonRequestContextData(request);
		requestContextData.setCategory(category);
		if (null != category && CollectionUtils.isEmpty(category.getRestrictions()))
		{
				final StringBuilder updatedSearchQuery = new StringBuilder();
				updateSearchQuery(searchQuery, updatedSearchQuery);
				model.addAttribute(PAGE_TYPE, PageType.CATEGORY.name());
				model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
				sessionService.setAttribute(SimonCoreConstants.CATEGORY_CODE_PATH, categoryCode);
				final CategorySearchEvaluator categorySearch = getSimonCategorySearchEvaluator(leafCategoryCode,
						XSSFilterUtil.filter(updatedSearchQuery.toString()), page, showMode, sortCode, categoryPage);
				final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = getSearchPageData(
						categorySearch, leafCategoryCode);
				requestContextData.setSearch(searchPageData);
				model.addAttribute(SHOW_CATEGORIES_ONLY, Boolean.valueOf(categorySearch.isShowCategoriesOnly()));
				populateModel(model, searchPageData, showMode);
				sessionService.removeAttribute(SimonCoreConstants.CATEGORY_CODE_PATH);
				if (extCustomerFacade.checkIfUserIsLoggedIn())
				{
					extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
					if (null != searchPageData)
					{
						searchPageData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
					}
				}
				if (searchPageData != null)
				{
					model.addAttribute(PRODUCT_DATA, searchPageData.getResults());
				}
		}
		else
		{
			model.addAttribute(PAGE_TYPE, CATEGORYLANDING);
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}
		storeContinueUrl(request);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, extBreadcrumbBuilder.getBreadcrumbs(categoryCode));
		model.addAttribute(CATEGORY_NAME,
				null != category ? CommonUtils.replaceAsciiWithHtmlCode(category.getName()) : StringUtils.EMPTY);
		model.addAttribute(CATEGORY_DESCRIPTION, null != category && null != category.getDescription()
				? CommonUtils.replaceAsciiWithHtmlCode(category.getDescription()) : StringUtils.EMPTY);
		model.addAttribute(USER_LOCATION, getCustomerLocationService().getUserLocation());
		model.addAttribute(SimonCoreConstants.CANONICAL_URL, request.getRequestURL());
		doUpdatePageTitle(category, model);
		if (searchQuery != null)
		{
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_FOLLOW);
		}
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(category.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(category.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		return getViewPage(categoryPage);
	}


	private void updateSearchQuery(final String searchQuery, final StringBuilder updatedSearchQuery)
	{
		if (StringUtils.isEmpty(searchQuery))
		{
			updatedSearchQuery.append(SimonCoreConstants.RELEVANCE);
		}
		else
		{
			updatedSearchQuery.append(searchQuery);
		}
	}

	@Override
	protected SearchResultsData<ProductData> performSearchAndGetResultsData(final String categoryCode, final String searchQuery,
			final int page, final ShowMode showMode, final String sortCode)
	{
		final ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = populateSearchPageData(
				categoryCode, searchQuery, page, showMode, sortCode);
		final ExtSearchResultsData<ProductData> searchResultsData = new ExtSearchResultsData<>();
		if (null != searchPageData)
		{
			searchResultsData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			searchResultsData.setPagination(searchPageData.getPagination());
			searchResultsData.setLoggedInUser(extCustomerFacade.checkIfUserIsLoggedIn());
			searchResultsData.setFavSuccessMessage(getLocalizedString(SimonCoreConstants.FAV_ADDED_MESSAGE));
		}
		return searchResultsData;
	}

	public static class ExtSearchResultsData<RESULT> extends SearchResultsData<RESULT>
	{
		private boolean loggedInUser;
		private String favSuccessMessage;

		public String getFavSuccessMessage()
		{
			return favSuccessMessage;
		}

		public void setFavSuccessMessage(final String favSuccessMessage)
		{
			this.favSuccessMessage = favSuccessMessage;
		}

		public boolean isLoggedInUser()
		{
			return loggedInUser;
		}

		public void setLoggedInUser(final boolean loggedInUser)
		{
			this.loggedInUser = loggedInUser;
		}

	}

	/**
	 * This method checks whether redirection is required. Redirection will occur in two scenarios 1. If no corresponding
	 * L1 category is not found in category hierarchy map and it is a valid category 2. User has entered valid category
	 * hierarchy but URL is not complete
	 *
	 * @param categoryCode
	 *           Current leaf category code
	 * @param request
	 * @param response
	 * @param category
	 *           Current category Model
	 * @param leftNavData
	 *           Category Hierarchy correspnding to L1 category of category Code
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String checkRedirectionIfrequired(final String categoryCode, final HttpServletRequest request,
			final HttpServletResponse response, final CategoryModel category, final List<NavigationData> leftNavData)
			throws UnsupportedEncodingException
	{
		String redirection;
		if (CollectionUtils.isEmpty(leftNavData) && null != category)
		{
			redirection = makeRequestUrl(request, extCategoryModelUrlResolver.resolveCategoryPath(category, StringUtils.EMPTY));
		}
		else
		{
			redirection = checkRequestUrl(request, response,
					extCategoryModelUrlResolver.resolveCategoryPath(category, categoryCode));
		}
		return redirection;
	}

	/**
	 * If no L1 category is found in map for valid category, we make complete category hierarchy through resolver and
	 * redirect to the newly formed URL
	 *
	 * @param request
	 * @param resolvedUrlPath
	 * @return
	 */
	private String makeRequestUrl(final HttpServletRequest request, final String resolvedUrlPath)
	{
		String redirection = null;
		if (StringUtils.contains(resolvedUrlPath, SimonCoreConstants.UNDERSCORE))
		{
			final String l1CategoryCode = resolvedUrlPath.substring(resolvedUrlPath.lastIndexOf('/') + 1)
					.split(SimonCoreConstants.UNDERSCORE)[0];
			if (CollectionUtils.isNotEmpty(leftNavigationCategoryFacade.getNavigationData(l1CategoryCode)))
			{
				request.setAttribute("org.springframework.web.servlet.View.responseStatus", HttpStatus.MOVED_PERMANENTLY);
				final String queryString = request.getQueryString();
				if (StringUtils.isNotEmpty(queryString))
				{
					redirection = SimonCoreConstants.REDIRECT + resolvedUrlPath + "?" + queryString;
				}
				else
				{
					redirection = SimonCoreConstants.REDIRECT + resolvedUrlPath;
				}
			}
		}
		return redirection;
	}

	protected class CategorySearchEvaluator
	{
		private final String categoryCode;
		private final SearchQueryData searchQueryData = new SearchQueryData();
		private final int page;
		private final ShowMode showMode;
		private final String sortCode;
		private CategoryPageModel categoryPage;
		private boolean showCategoriesOnly;
		private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData;

		public CategorySearchEvaluator(final String categoryCode, final String searchQuery, final int page, final ShowMode showMode,
				final String sortCode, final CategoryPageModel categoryPage)
		{
			this.categoryCode = categoryCode;
			this.searchQueryData.setValue(searchQuery);
			this.page = page;
			this.showMode = showMode;
			this.sortCode = sortCode;
			this.categoryPage = categoryPage;
		}

		/**
		 * Method doSearch is used to search for Direct category link without filtering.
		 *
		 */
		public void doSearch()
		{
			showCategoriesOnly = false;

			LOGGER.debug("searchQueryData.getValue(): '" + searchQueryData.getValue());

			if (StringUtils.isEmpty(searchQueryData.getValue()))
			{

				// Direct category link without filtering
				searchPageData = getProductSearchFacade().categorySearch(categoryCode);

				LOGGER.debug("searchPageData.getResults().size(): ", searchPageData.getResults().size());

				if (categoryPage != null)
				{
					showCategoriesOnly = !categoryHasDefaultPage(categoryPage)
							&& CollectionUtils.isNotEmpty(searchPageData.getSubCategories());
				}

			}
			else
			{
				LOGGER.debug("searchQueryData.getValue(): '" + searchQueryData.getValue());

				// We have some search filtering
				if (categoryPage == null || !categoryHasDefaultPage(categoryPage))
				{
					// Load the default category page
					categoryPage = getDefaultCategoryPage();
				}

				final SearchStateData searchState = new SearchStateData();
				searchState.setQuery(searchQueryData);

				final PageableData pageableData = createPageableData(page, getSearchPageSize(), sortCode, showMode);
				searchPageData = getProductSearchFacade().categorySearch(categoryCode, searchState, pageableData);
			}
		}

		public int getPage()
		{
			return page;
		}

		/**
		 * Method used to get the Category Page
		 *
		 * @return model for {@link CategoryPageModel}
		 */
		public CategoryPageModel getCategoryPage()
		{
			return categoryPage;
		}

		/**
		 * Method used to check if need to showCategory only by returning the boolean value.
		 *
		 * @return boolean trye, if show Categories only.
		 */
		public boolean isShowCategoriesOnly()
		{
			return showCategoriesOnly;
		}

		/**
		 * Method to get the search page data by using {@link SearchStateData} {@link ProductData} and
		 * {@link CategoryData}
		 *
		 * @return the category page data in the form of {@link ProductCategorySearchPageData}
		 */
		public ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData()
		{
			return searchPageData;
		}
	}

	protected CategorySearchEvaluator getSimonCategorySearchEvaluator(final String categoryCode, final String searchQuery,
			final int page, final ShowMode showMode, final String sortCode, final CategoryPageModel categoryPage)
	{
		return new CategorySearchEvaluator(categoryCode, XSSFilterUtil.filter(searchQuery), page, showMode, sortCode, categoryPage);

	}

	/**
	 * get the category Page Model based on category Page
	 *
	 * @param category
	 * @return
	 */
	protected CategoryPageModel getSimonCategoryPage(final CategoryModel category)
	{
		return super.getCategoryPage(category);
	}

	/**
	 * get RequestContextData based on request
	 *
	 * @param request
	 * @return
	 */
	protected RequestContextData getSimonRequestContextData(final HttpServletRequest request)
	{
		return super.getRequestContextData(request);
	}

	/**
	 * update page title
	 *
	 * @param category
	 * @param model
	 */
	protected <QUERY> void doUpdatePageTitle(final CategoryModel category, final Model model)
	{
		super.updatePageTitle(category, model);
	}

	/**
	 *
	 * Method for search product based on {@link SearchStateData} and {@link ProductData} and {@link CategoryData}
	 *
	 * @param categorySearch
	 * @param categoryCode
	 * @return
	 */
	private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> getSearchPageData(
			final CategorySearchEvaluator categorySearch, final String categoryCode)
	{
		ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> searchPageData = null;
		try
		{
			categorySearch.doSearch();
			searchPageData = categorySearch.getSearchPageData();

			if (searchPageData.getCurrentQuery() != null)
			{
				final String[] clearUrl = searchPageData.getCurrentQuery().getUrl().split("\\?");
				if (clearUrl != null)
				{
					searchPageData.getCurrentQuery().setClearAllUrl(clearUrl[0]);
				}
			}

			if (searchPageData.getResults() == null)
			{
				LOGGER.debug("searchPageData Results is 'null'.");
			}
			else
			{
				LOGGER.debug("searchPageData.getResults().size(): ", searchPageData.getResults().size());
			}
		}
		catch (final ConversionException e) // NOSONAR
		{
			LOGGER.error(e.getMessage(), e);
			searchPageData = createEmptySearchResult(categoryCode);
		}
		return searchPageData;
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final CategoryPageModel pageForRequest = (CategoryPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.FOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}

			final StringBuilder meta = new StringBuilder();

			if (StringUtils.isNotEmpty((meta.append(pageForRequest.getIndex().toString().toLowerCase()).append(",")
					.append(pageForRequest.getFollow().toString().toLowerCase())).toString()))
			{
				model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, meta.toString());
			}
		}
	}
}
