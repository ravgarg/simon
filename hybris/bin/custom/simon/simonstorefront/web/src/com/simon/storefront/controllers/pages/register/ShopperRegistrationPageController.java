package com.simon.storefront.controllers.pages.register;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractRegisterPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.mall.MallFacade;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.forms.ShopperRegistrationForm;
import com.simon.storefront.util.ExtWebUtils;


/**
 * This is the Controller to handle Shopper Registration for SPO.com
 *
 */
@Controller
@RequestMapping("/join-now")
public class ShopperRegistrationPageController extends AbstractRegisterPageController
{
	private static final String ACC_CONF_MSGS = "accConfMsgs";
	private static final String REDIRECT_TO_SHOP_REGISTER = REDIRECT_PREFIX + "/join-now?registration=failure";
	private static final String REGISTRATION_FAILED = "registration failed: ";
	private static final String USER_REGISTER_PAGE_CAPTCHA_ENABLED = "user.register.page.captcha.enabled";
	private static final String FORM_GLOBAL_ERROR = "form.global.error";
	private static final String ERROR_MESSAGE = "errorMessage";
	private static final Logger LOGGER = Logger.getLogger(ShopperRegistrationPageController.class);
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "extRegistrationValidator")
	private Validator extRegistrationValidator;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "mallFacade")
	private MallFacade mallFacade;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	private static final String REGISTER_CMS_PAGE = "shopperRegistrationPage";
	private static final String BIRTH_YEAR_START = "simonstorefront.account.birthyear.start";
	private static final String PAGETYPE = "pageType";

	/**
	 * This is the method to land on Shopper Registration page. This is using shopper registration cms page .
	 *
	 * @param model
	 * @param response
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getShopperRegisterLandingPage(final Model model, final HttpServletResponse response)
			throws CMSItemNotFoundException
	{
		final ShopperRegistrationForm registerForm = new ShopperRegistrationForm();
		model.addAttribute("registerForm", registerForm);
		final List errorMessages = (List) model.asMap().get(ACC_CONF_MSGS);
		if (CollectionUtils.isNotEmpty(errorMessages))
		{
			final GlobalMessage message = (GlobalMessage) errorMessages.get(0);
			if (message != null)
			{
				model.addAttribute(ERROR_MESSAGE, message.getCode());
			}

		}
		pageSetUp(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
		return getViewForPage(model);

	}



	/**
	 * This method is used to register user in to SPO.com.In this we are using ShopperRegistrationForm as formdata. and
	 * convert this data into RegisterData and send it to facade to register user.
	 *
	 * @param formData
	 * @param model
	 * @param request
	 * @param response
	 * @param bindingResult
	 * @param redirectAttributes
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/create-account", method = RequestMethod.POST)
	public String createAccount(@ModelAttribute("registerFormData") final ShopperRegistrationForm formData, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final BindingResult bindingResult,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		extRegistrationValidator.validate(formData, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, FORM_GLOBAL_ERROR);
			return REDIRECT_TO_SHOP_REGISTER;

		}
		final RegisterData data = prepareShopperRegistrationData(formData);
		try
		{
			extCustomerFacade.registerShopper(data, true);
			getAutoLoginStrategy().login(formData.getEmail().toLowerCase(), formData.getPwd(), request, response);
		}
		catch (final UserIntegrationException e)
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS,
					messageSource.getMessage(
							SimonCoreConstants.INTEGRATION_USER_REGISTER_ERROR_CODE + e.getCode()
									+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE,
							new Object[] {}, getI18nService().getCurrentLocale()));
			LOGGER.warn(REGISTRATION_FAILED + e);
			return REDIRECT_TO_SHOP_REGISTER;
		}
		catch (final AuthLoginIntegrationException e)
		{
			LOGGER.warn(REGISTRATION_FAILED + e);
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS,
					messageSource.getMessage(
							SimonCoreConstants.INTEGRATION_USER_AUTH_LOGIN_ERROR_CODE + e.getCode()
									+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE,
							new Object[] {}, getI18nService().getCurrentLocale()));
			return REDIRECT_TO_SHOP_REGISTER;
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.warn(REGISTRATION_FAILED + e);
			bindingResult.rejectValue("email", "registration.error.account.exists.title");
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, messageSource
					.getMessage("registration.error.account.exists.title", new Object[] {}, getI18nService().getCurrentLocale()));
			return REDIRECT_TO_SHOP_REGISTER;
		}
		model.addAttribute(PAGETYPE, REGISTER_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		storeCmsPageInModel(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REGISTER_CMS_PAGE));
		return REDIRECT_PREFIX + getSuccessRedirect(request, response);

	}

	/**
	 * This method is redirecting to home page after success registration.
	 */
	@Override
	protected String getSuccessRedirect(final HttpServletRequest request, final HttpServletResponse response)
	{
		if (httpSessionRequestCache.getRequest(request, response) != null)
		{
			return httpSessionRequestCache.getRequest(request, response).getRedirectUrl();
		}
		return "/my-account/my-premium-outlets?registration=success";
	}

	/**
	 * This method is returning shopper registration page.
	 */
	@Override
	protected AbstractPageModel getCmsPage() throws CMSItemNotFoundException
	{
		return getContentPageForLabelOrId(REGISTER_CMS_PAGE);
	}

	/**
	 * This method is returning Shopper registration JSP.
	 */
	@Override
	protected String getView()
	{
		return SimonControllerConstants.Views.Pages.Account.ShopperRegisterPage;
	}

	/**
	 * This method is used to prepare data from registration Form.
	 *
	 * @param formData
	 * @return
	 */
	private RegisterData prepareShopperRegistrationData(final ShopperRegistrationForm formData)
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(formData.getFirstName());
		data.setLastName(formData.getLastName());
		data.setLogin(formData.getEmail());
		data.setGender(formData.getGender());
		data.setBirthMonth(formData.getBirthmonth());
		data.setBirthYear(formData.getBirthyear());
		data.setCountry(formData.getCountry());
		if (ArrayUtils.contains(
				StringUtils.split(configurationService.getConfiguration().getString(SimonWebConstants.COUNTRIES_VALID_FOR_ZIPCODE),
						SimonCoreConstants.DELIMITER),
				formData.getCountry()))
		{
			data.setZipcode(formData.getZip());
		}
		data.setPassword(formData.getPwd());
		if (StringUtils.isNotBlank(formData.getPrimaryMall()))
		{
			final MallData primaryMallData = new MallData();
			primaryMallData.setCode(formData.getPrimaryMall());
			data.setPrimaryMall(primaryMallData);
		}
		final List<String> alternateMallIds = Arrays
				.asList(StringUtils.split(formData.getAlternateMallIds(), SimonCoreConstants.DELIMITER));
		if (CollectionUtils.isNotEmpty(alternateMallIds))
		{
			final List<MallData> alternateMallDataSet = new ArrayList<>();
			for (final String mallCode : alternateMallIds)
			{
				final MallData mallData = new MallData();
				mallData.setCode(mallCode);
				alternateMallDataSet.add(mallData);
			}
			data.setAlternateMalls(alternateMallDataSet);
		}
		return data;
	}

	/**
	 * This method is used to set up page data for Register Page.
	 *
	 * @param model
	 * @throws CMSItemNotFoundException
	 */
	private void pageSetUp(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("countryList", extCustomerFacade.getAllCountry());
		model.addAttribute("birthMonthList", extCustomerFacade.getMonthsList());
		model.addAttribute("genderList", extCustomerFacade.getGenderList());
		model.addAttribute("startYear", String.valueOf(configurationService.getConfiguration().getString(BIRTH_YEAR_START)));
		final int endYear = Calendar.getInstance().get(Calendar.YEAR) - 18;
		model.addAttribute("endYear", endYear);
		model.addAttribute("validCountriesforZipcode",
				configurationService.getConfiguration().getList(SimonWebConstants.COUNTRIES_VALID_FOR_ZIPCODE));

		model.addAttribute("mallList", mallFacade.getMallList());
		model.addAttribute("captchaEnabledForRegister",
				String.valueOf(configurationService.getConfiguration().getString(USER_REGISTER_PAGE_CAPTCHA_ENABLED)));
		model.addAttribute(PAGETYPE, REGISTER_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
	}


	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
