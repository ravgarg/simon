/**
 *
 */
package com.simon.storefront.advice;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.simon.core.exceptions.BusinessException;
import com.simon.core.exceptions.IntegrationException;
import com.simon.core.exceptions.SystemException;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.integration.exceptions.TokenExpiredException;




/**
 * This <tt>class</tt> will be used for global {@link ExceptionControllerAdvice} exception handling. This class
 * represents handling of all global exception which will be thrown during the request processing.
 */
//TODO add logical redirection with actual error page
@ControllerAdvice
public class ExceptionControllerAdvice
{

	@Resource
	private ExtCheckoutFacade checkoutFacade;
	/**
	 * Redirect Error page constant
	 */

	private static final String REDIRECT_500_ERROR_PAGE = "redirect:/500ErrorPage";

	/**
	 *
	 */
	private static final String DUMMY_ERROR_PAGE = "pages/error/dummyErrorPage";


	/** The Constant EXCEPTION_NAME. */
	private static final String EXCEPTION_NAME = "name";

	/** The Constant MESSAGE. */
	private static final String MESSAGE = "message";

	/** The Constant LOG_MESSAGE. */
	private static final String LOG_MESSAGE = "Getting exception from application. redirecting to error page.";
	/** The Constant for LOGGER. */
	private static final Logger LOG = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

	/**
	 * This method is used to handle system exception thrown by the application.
	 *
	 * <p>
	 * In this method we will receive exception as a parameter and then, will create a view for front end with
	 * appropriate massage and logs.
	 * </p>
	 *
	 * @param ex
	 *           the SystemException
	 * @return the model and view
	 */
	@ExceptionHandler(SystemException.class)
	public ModelAndView handleSystemException(final SystemException ex)
	{
		LOG.error(LOG_MESSAGE, ex);
		final ModelAndView mav = new ModelAndView(DUMMY_ERROR_PAGE);
		mav.addObject(EXCEPTION_NAME, ex.getClass().getSimpleName());
		mav.addObject(MESSAGE, ex.getMessage());

		return mav;
	}

	/**
	 * This method is used to handle Integration exception thrown by the application.
	 *
	 * <p>
	 * In this method we will receive exception as a parameter and then, will create a view for front end with
	 * appropriate massage and logs.
	 * </p>
	 *
	 * @param ex
	 *           the IntegrationException
	 * @return the model and view
	 */
	@ExceptionHandler(IntegrationException.class)
	public ModelAndView handleIntegrationException(final IntegrationException ex)
	{
		LOG.error(LOG_MESSAGE, ex);
		final ModelAndView mav = new ModelAndView(DUMMY_ERROR_PAGE);
		mav.addObject(EXCEPTION_NAME, ex.getClass().getSimpleName());
		mav.addObject(MESSAGE, ex.getMessage());

		return mav;
	}


	/**
	 * This method is used to handle Business exception thrown by the application.
	 *
	 * <p>
	 * In this method we will receive exception as a parameter and then, will create a view for front end with
	 * appropriate massage and logs.
	 * </p>
	 *
	 * @param ex
	 *           the BusinessException
	 * @return the model and view
	 */
	@ExceptionHandler(BusinessException.class)
	public ModelAndView handleBuisnessException(final BusinessException ex)
	{
		LOG.error(LOG_MESSAGE, ex);
		final ModelAndView mav = new ModelAndView(DUMMY_ERROR_PAGE);
		mav.addObject(EXCEPTION_NAME, ex.getClass().getSimpleName());
		mav.addObject(MESSAGE, ex.getMessage());

		return mav;
	}

	/**
	 * This method is used to handle Token Expired Exception thrown by the application.
	 *
	 * <p>
	 * In this method we will receive exception as a parameter and then, will create a view for front end with
	 * appropriate massage and logs.
	 * </p>
	 *
	 * @param ex
	 *           the TokenExpiredException
	 * @return the model and view
	 */
	@ExceptionHandler(TokenExpiredException.class)
	public String handleTokenExpiredException(final TokenExpiredException ex)
	{
		LOG.error(LOG_MESSAGE, ex);
		final ModelAndView mav = new ModelAndView(DUMMY_ERROR_PAGE);
		mav.addObject(EXCEPTION_NAME, ex.getClass().getSimpleName());
		mav.addObject(MESSAGE, ex.getMessage());

		return REDIRECT_500_ERROR_PAGE;
	}

}
