package com.simon.storefront.tags;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.Registry;
import de.hybris.platform.enumeration.EnumerationService;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;


/**
 * Enumeration Tag. This tag extracts name from hybris enum value and writes to jsp.
 */
public class EnumerationTag extends SimpleTagSupport
{

	private HybrisEnumValue hybrisEnum;

	@Override
	public void doTag() throws JspException, IOException
	{
		if (hybrisEnum != null)
		{
			final EnumerationService enumerationService = (EnumerationService) Registry.getApplicationContext()
					.getBean("enumerationService");
			getJspContext().setAttribute("value", enumerationService.getEnumerationName(hybrisEnum));
		}
		else
		{
			getJspContext().removeAttribute("value");
		}
	}

	/**
	 * Set Hybris Enum Value
	 *
	 * @param hybrisEnum
	 */
	public void setHybrisEnum(final HybrisEnumValue hybrisEnum)
	{
		this.hybrisEnum = hybrisEnum;
	}
}
