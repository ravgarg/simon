package com.simon.storefront.validator.form;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ProfileValidator;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.forms.AccountUpdateForm;


/**
 * The Class ExtUpdateProfileValidator.
 */
@Component("extUpdateProfileValidator")
public class ExtUpdateProfileValidator extends ProfileValidator
{

	/** Email Regex. */
	public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

	/** PASSWORD_REGEX. */
	public static final Pattern PASSWORD_REGEX = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%]).{6,15})");

	/** The Constant PWD_FIELD. */
	private static final String PSWRD_FIELD = "password";

	/** The configuration service. */
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;


	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AccountUpdateForm.class.equals(aClass);
	}


	/*
	 * validates the values of account update form
	 */
	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AccountUpdateForm updateProfileForm = (AccountUpdateForm) object;

		final String firstName = updateProfileForm.getFirstName();
		final String lastName = updateProfileForm.getLastName();
		final String email = updateProfileForm.getEmail();
		final String zipCode = updateProfileForm.getZipCode();
		final String isPasswordUpdate = updateProfileForm.getPasswordUpdate();
		final String country = updateProfileForm.getCountry();
		final int birthYear = updateProfileForm.getBirthYear();
		final String birthmonth = updateProfileForm.getBirthMonth();


		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");
		validateCountry(errors, country, "country", "register.country.invalid");
		validateName(errors, birthmonth, "birthmonth", "register.birthmonth.invalid");
		validatebirth(errors, birthYear, "birthyear", "register.birthmonth.invalid");
		if (configurationService.getConfiguration().getList(SimonWebConstants.COUNTRIES_VALID_FOR_ZIPCODE).contains(country))
		{
			validateZipcode(errors, zipCode, "zipcode", "text.account.profile.myaccount.zipcode.invalid");
		}
		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "text.account.profile.myaccount.name.invalid");
			errors.rejectValue("firstName", "text.account.profile.myaccount.name.invalid");
		}

		validateEmail(errors, email);
		if (isPasswordUpdate.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			final String password = updateProfileForm.getPassword();
			final String confirmPassword = updateProfileForm.getConfirmPassword();
			validatePassword(errors, password);
			comparePasswords(errors, password, confirmPassword);
		}


	}

	/**
	 * Validate zipcode.
	 *
	 * @param errors
	 *           the errors
	 * @param zipCode
	 *           the zip code
	 * @param propertyName
	 *           the property name
	 * @param property
	 *           the property
	 */
	private void validateZipcode(final Errors errors, final String zipCode, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(zipCode))
		{
			errors.rejectValue(propertyName, property);
		}
	}

	/**
	 * Validate country.
	 *
	 * @param errors
	 *           the errors
	 * @param country
	 *           the country
	 * @param propertyName
	 *           the property name
	 * @param property
	 *           the property
	 */
	protected void validateCountry(final Errors errors, final String country, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(country))
		{
			errors.rejectValue(propertyName, property);
		}

	}

	/**
	 * Validatebirth.
	 *
	 * @param errors
	 *           the errors
	 * @param monthYear
	 *           the month year
	 * @param propertyName
	 *           the property name
	 * @param property
	 *           the property
	 */
	protected void validatebirth(final Errors errors, final int monthYear, final String propertyName, final String property)
	{
		if (monthYear <= 0)
		{
			errors.rejectValue(propertyName, property);
		}

	}


	/**
	 * Validate password.
	 *
	 * @param errors
	 *           the errors
	 * @param pwd
	 *           the pwd
	 */
	protected void validatePassword(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd) || (StringUtils.length(pwd) < 6 || StringUtils.length(pwd) > 16))
		{
			errors.rejectValue(PSWRD_FIELD, "text.account.profile.myaccount.pwd.invalid");
		}
		else if (!validatePasswordCategory(pwd))
		{
			errors.rejectValue(PSWRD_FIELD, "text.account.profile.myaccount.pwd.invalid.category");
		}
	}

	/**
	 * This method is used to validate password like Passwords must be between 6 and 15 characters long and contain
	 * characters from three of the following four categories: Uppercase characters, Lowercase characters, Numeric:
	 * 0-9,Non-alphanumeric: ex. ~!@#$%
	 *
	 * @param pwd
	 *           the pwd
	 * @return true, if successful
	 */
	protected boolean validatePasswordCategory(final String pwd)
	{
		final Matcher matcher = PASSWORD_REGEX.matcher(pwd);
		return matcher.matches();
	}

	/**
	 * Compare passwords.
	 *
	 * @param errors
	 *           the errors
	 * @param pwd
	 *           the pwd
	 * @param checkPwd
	 *           the check pwd
	 */
	protected void comparePasswords(final Errors errors, final String pwd, final String checkPwd)
	{
		if (StringUtils.isNotEmpty(pwd) && StringUtils.isNotEmpty(checkPwd) && !StringUtils.equals(pwd, checkPwd))
		{
			errors.rejectValue("confirmPassword", "validation.checkPwd.equals");
		}
		else
		{
			if (StringUtils.isEmpty(checkPwd))
			{
				errors.rejectValue("confirmPassword", "text.account.profile.myaccount.checkPwd.invalid");
			}
		}
	}



	/**
	 * Validate email.
	 *
	 * @param errors
	 *           the errors
	 * @param email
	 *           the email
	 */
	protected void validateEmail(final Errors errors, final String email)
	{
		if (StringUtils.isEmpty(email))
		{
			errors.rejectValue("email", "text.account.profile.myaccount.email.empty");
		}
		else if (StringUtils.length(email) > 255 || !validateEmailAddress(email))
		{
			errors.rejectValue("email", "text.account.profile.myaccount.email.invalid");
		}
	}

	/**
	 * Validate name.
	 *
	 * @param errors
	 *           the errors
	 * @param name
	 *           the name
	 * @param propertyName
	 *           the property name
	 * @param property
	 *           the property
	 */
	protected void validateName(final Errors errors, final String name, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(name) || StringUtils.length(name) > 255)
		{
			errors.rejectValue(propertyName, property);
		}
	}


	/**
	 * Validate email address.
	 *
	 * @param email
	 *           the email
	 * @return true, if successful
	 */
	protected boolean validateEmailAddress(final String email)
	{
		final Matcher matcher = EMAIL_REGEX.matcher(email);
		return matcher.matches();
	}


}
