package com.simon.storefront.builder.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.simon.storefront.constant.SimonWebConstants;


/**
 * This class is used to create the breadcrumbs for the search result/category landing/product listing pages.
 */
public class ExtSearchBreadcrumbBuilder extends SearchBreadcrumbBuilder
{
	/**
	 * Returns a list of breadcrumbs for the valid supercategories hiearchy for the given product. This method also
	 * removes the Top level category "SPO" from the breadcrumb list.
	 *
	 * @param categoryCode
	 *           represents the unique category code of the category.
	 * @param searchPageData
	 *           represents the SearchPage Data object returned from Solr.
	 * @return breadcrumbs which is a list of {@link Breadcrumb} for the given category.
	 */
	@Override
	public List<Breadcrumb> getBreadcrumbs(final String categoryCode,
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		final List<Breadcrumb> breadcrumbData = callSuperMethod(categoryCode, searchPageData);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(breadcrumbData))
		{
			for (final Breadcrumb breadcrumb : breadcrumbData)
			{
				if (!SimonWebConstants.TOP_LEVEL_CATEGORY.equalsIgnoreCase(breadcrumb.getName()))
				{
					breadcrumbs.add(breadcrumb);
				}
			}
		}
		return breadcrumbs;
	}

	/**
	 * Returns a list of breadcrumbs for the valid supercategories hiearchy for the given product.
	 *
	 * @param categoryCode
	 *           represents the unique category code of the category.
	 *
	 * @param searchPageData
	 *           represents the SearchPage Data object returned from Solr.
	 * @return breadcrumbs which is a list of {@link Breadcrumb} for the given category.
	 */
	protected List<Breadcrumb> callSuperMethod(final String categoryCode,
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		return super.getBreadcrumbs(categoryCode, searchPageData);
	}


}
