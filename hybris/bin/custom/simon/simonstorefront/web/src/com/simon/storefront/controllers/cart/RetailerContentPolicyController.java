package com.simon.storefront.controllers.cart;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.facades.customer.ExtShopFacade;
import com.simon.facades.shop.data.ShopData;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtWebUtils;


/**
 * This class will populate data for content policy of retailer.
 */
@Controller
public class RetailerContentPolicyController extends AbstractPageController
{
	private static final Logger LOG = LoggerFactory.getLogger(RetailerContentPolicyController.class);
	private static final String RETAILERS_CMS_PAGE_LABEL = "shipping-returns";

	@Resource(name = "shopFacade")
	private ExtShopFacade shopFacade;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * This method will populate the content policy of retailer while clicking on cart page.
	 *
	 * @param retailerId
	 *           retailer's ID
	 * @param model
	 * @return this will return the content of retailer's policy.
	 */
	@RequestMapping(value = "/contentPolicy/{retailerId}", method = RequestMethod.GET)
	@ResponseBody
	public ShopData retailerContentPolicy(@PathVariable("retailerId") final String retailerId)
	{
		return shopFacade.getShop(retailerId);
	}

	/**
	 * This method will populate the retailer content data.
	 *
	 * @param retailerId
	 *           the retailer id
	 * @return the retailer
	 */
	@RequestMapping(value = "/select-retailer", method = RequestMethod.POST)
	@ResponseBody
	public ShopData getRetailer(@RequestParam final String retailerId)
	{
		return shopFacade.getShop(retailerId);
	}

	/**
	 * This method will populate the retailer list.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 */
	@RequestMapping(value = "/customer-service/shipping-returns", method = RequestMethod.GET)
	public String retailerList(final Model model)
	{
		try
		{
			final ContentPageModel cmsPage = getContentPageForLabelOrId(RETAILERS_CMS_PAGE_LABEL);
			model.addAttribute("retailerMap", shopFacade.getRetailerList());
			model.addAttribute("userRetailerMap", shopFacade.getCustomerFavouritRetailerList());
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(cmsPage));
			model.addAttribute("pageId", RETAILERS_CMS_PAGE_LABEL);
			model.addAttribute("pageType", "shipping-returns");
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
			storeCmsPageInModel(model, cmsPage);
			setUpMetaDataForContentPage(model, cmsPage);
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.error("CMS page not found", e);
		}
		return getViewForPage(model);
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.FOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
