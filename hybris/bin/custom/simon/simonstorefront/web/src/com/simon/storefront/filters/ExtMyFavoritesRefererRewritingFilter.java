package com.simon.storefront.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 *
 */
public class ExtMyFavoritesRefererRewritingFilter extends OncePerRequestFilter
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ExtMyFavoritesRefererRewritingFilter.class);

	@Override
	protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse httpServletResponse,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final boolean flag = isAjaxRequest(request);
		if (flag && request.getServletPath().contains("/my-account/"))
		{
			httpServletResponse.setHeader("referer", request.getServletPath());
		}
		filterChain.doFilter(request, httpServletResponse);
	}

	/**
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(final HttpServletRequest request)
	{
		return "ajax".equals(request.getHeader("viewType"));
	}


}
