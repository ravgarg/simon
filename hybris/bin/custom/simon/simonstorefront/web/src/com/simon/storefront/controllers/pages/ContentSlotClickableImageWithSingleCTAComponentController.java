package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.ContentSlotClickableImageWithSingleCTAComponentModel;
import com.simon.facades.cms.CMSLinkComponentData;
import com.simon.facades.cms.ContentSlotClickableImageWithSingleCTAComponentData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.populators.ContentSlotClickableImageComponentPopulator;
import com.simon.facades.user.ExtUserFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Controller class use to build the clickable image with single component and add the images with the component
 */
@Controller("ContentSlotClickableImageWithSingleCTAComponentController")
@RequestMapping(value = SimonControllerConstants.Actions.Cms.ContentSlotClickableImageWithSingleCTAComponent)
public class ContentSlotClickableImageWithSingleCTAComponentController
		extends AbstractCMSComponentController<ContentSlotClickableImageWithSingleCTAComponentModel>
{

	private static final String MYCENTER_PAGE_MONTREAL_MALL_URL_BASE_KEY = "mycenter.page.montreal.mall.updated.url.";

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "extUserFacade")
	private ExtUserFacade userFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "contentSlotClickableImageComponentPopulator")
	private ContentSlotClickableImageComponentPopulator contentSlotClickableImageComponentPopulator;

	/*
	 * Changes the background image if the user is logged in
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model,
			final ContentSlotClickableImageWithSingleCTAComponentModel component)
	{

		ContentSlotClickableImageWithSingleCTAComponentData contentSlotClickableImageWithSingleCTAComponentData = new ContentSlotClickableImageWithSingleCTAComponentData();
		contentSlotClickableImageComponentPopulator.populate(component, contentSlotClickableImageWithSingleCTAComponentData);

		final Boolean isLinkURLOverrideNeeded = component.getNeedOverrideLink();
		final Boolean isLogoURLOverrideNeeded = (!userFacade.isAnonymousUser() && component.getNeedOverrideLogo());
		boolean hideComponent = false;

		//Below if condition will come in to picture only for My center page & My premium outlets page
		if (isLinkURLOverrideNeeded || isLogoURLOverrideNeeded
				|| contentSlotClickableImageWithSingleCTAComponentData.getDisplayOnlyForMontrealMall())
		{
			final MallData mallData = extCustomerFacade.getCustomerPrimaryMallData();
			if (null != mallData)
			{

				if (mallData.isMontrealMallFlag() && contentSlotClickableImageWithSingleCTAComponentData.getHideIfMontrealMall())
				{
					//This condition will fall only for My Center page where we need to hide components which is not part of the Montreal Mall.
					hideComponent = true;
				}
				else if (!mallData.isMontrealMallFlag()
						&& contentSlotClickableImageWithSingleCTAComponentData.getDisplayOnlyForMontrealMall())
				{
					//This condition will fall only for My center page where we need to hide a component because that component needs to be displayed only if the mall is Montreal. For e.g.- Group Tours
					hideComponent = true;
				}
				else if (mallData.isMontrealMallFlag()
						&& contentSlotClickableImageWithSingleCTAComponentData.getDisplayOnlyForMontrealMall())
				{
					//This condition will fall only for My center page where we need to display a component only if user primary mall is a Montreal Mall. For this condition the URL override is not needed.
					hideComponent = false;
				}
				else
				{
					overrideImageLogoUrlIfNeeded(contentSlotClickableImageWithSingleCTAComponentData, mallData,
							isLogoURLOverrideNeeded);

					overrideLinkUrlIfNeeded(contentSlotClickableImageWithSingleCTAComponentData, mallData, isLinkURLOverrideNeeded);
				}
			}
		}

		if (hideComponent)
		{
			contentSlotClickableImageWithSingleCTAComponentData = null;
		}

		model.addAttribute("component", contentSlotClickableImageWithSingleCTAComponentData);
		model.addAttribute("isAuthenticatedMyCenter", isLogoURLOverrideNeeded);

	}

	/**
	 * @description Method use to set the image for Premium Outlets with the clickable image component
	 * @method setLogoUrlWithImage
	 * @param contentSlotClickableImageWithSingleCTAComponentData
	 * @param primaryMallData
	 * @param isLogoURLOverrideNeeded
	 */
	private void overrideImageLogoUrlIfNeeded(
			final ContentSlotClickableImageWithSingleCTAComponentData contentSlotClickableImageWithSingleCTAComponentData,
			final MallData primaryMallData, final Boolean isLogoURLOverrideNeeded)
	{

		if (isLogoURLOverrideNeeded)
		{
			if (contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage() != null
					&& contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage().getDesktopImage() != null)
			{
				contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage().getDesktopImage()
						.setUrl(primaryMallData.getLogo());
			}
			if (contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage() != null
					&& contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage().getMobileImage() != null)
			{
				contentSlotClickableImageWithSingleCTAComponentData.getBackgroundImage().getMobileImage()
						.setUrl(primaryMallData.getLogo());
			}
		}


	}

	/**
	 * @description Method use to set the Link for the My Center with the clickable image component
	 * @method setLinkUrlWithImage
	 * @param contentSlotClickableImageWithSingleCTAComponentData
	 * @param primaryMallData
	 * @param isLinkURLOverrideNeeded
	 */
	private void overrideLinkUrlIfNeeded(
			final ContentSlotClickableImageWithSingleCTAComponentData contentSlotClickableImageWithSingleCTAComponentData,
			final MallData primaryMallData, final Boolean isLinkURLOverrideNeeded)
	{

		if (isLinkURLOverrideNeeded && null != contentSlotClickableImageWithSingleCTAComponentData.getLink()
				&& StringUtils.isNotEmpty(contentSlotClickableImageWithSingleCTAComponentData.getLink().getUrl()))
		{
			contentSlotClickableImageWithSingleCTAComponentData.getLink()
					.setUrl(buildFinalShopperFacingUrl(primaryMallData, contentSlotClickableImageWithSingleCTAComponentData));
		}
	}

	/**
	 * @description Method use to populate the final shopper facing url which will use in the my centers content page
	 * @method buildFinalShopperFacingUrl
	 * @param primaryMall
	 * @param contentSlotClickableImageWithSingleCTAComponentData
	 * @return String
	 */
	private String buildFinalShopperFacingUrl(final MallData primaryMall,
			final ContentSlotClickableImageWithSingleCTAComponentData contentSlotClickableImageWithSingleCTAComponentData)
	{
		final StringBuilder shopperFacingUrl = new StringBuilder();
		final CMSLinkComponentData cmsLinkComponentData = contentSlotClickableImageWithSingleCTAComponentData.getLink();
		if (null != cmsLinkComponentData)
		{
			shopperFacingUrl.append(primaryMall.getShopperFacingURL());
			if (StringUtils.isNotEmpty(shopperFacingUrl.toString()) && StringUtils.isNotEmpty(cmsLinkComponentData.getUrl()))
			{
				shopperFacingUrl.append(resolveCMSLinkUrlValue(cmsLinkComponentData.getUrl(), primaryMall));
			}
		}
		return shopperFacingUrl.toString();
	}


	/**
	 * @description Method use to resolve the url value when Mall type Montreal
	 * @method resolveLinkUrlValue
	 * @param linkUrl
	 * @param primaryMall
	 * @return String
	 */
	private String resolveCMSLinkUrlValue(final String linkUrl, final MallData primaryMall)
	{
		String returnLink = linkUrl;
		if (primaryMall.isMontrealMallFlag() && StringUtils.isNotEmpty(linkUrl))
		{
			final String updatedLink = configurationService.getConfiguration()
					.getString(MYCENTER_PAGE_MONTREAL_MALL_URL_BASE_KEY + linkUrl.substring(1));
			if (StringUtils.isNotBlank(updatedLink))
			{
				returnLink = SimonIntegrationConstants.SLASH + updatedLink;
			}
		}
		return returnLink;

	}

	/*
	 * Returns the view
	 */
	@Override
	protected String getView(final ContentSlotClickableImageWithSingleCTAComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(getTypeCode(component));
	}
}
