package com.simon.storefront.checkout.util;

import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.localization.Localization;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.storefront.checkout.form.ExtAddressForm;
import com.simon.storefront.constant.SimonWebConstants;


/**
 *
 */
public class ExtAddressDataUtil
{


	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;
	@Resource(name = "userService")
	private UserService userService;


	/**
	 * @param addressForm
	 * @return
	 */
	public ExtAddressData convertToAddressData(final ExtAddressForm addressForm)
	{
		final ExtAddressData addressData = new ExtAddressData();
		addressData.setId(addressForm.getAddressId());
		addressData.setFirstName(addressForm.getFirstName());
		addressData.setLastName(addressForm.getLastName());
		addressData.setLine1(addressForm.getLine1());
		addressData.setLine2(addressForm.getLine2());
		addressData.setTown(addressForm.getTownCity());
		addressData.setPostalCode(addressForm.getPostcode());

		if (null != addressForm.getDefaultAddress())
		{
			addressData.setDefaultAddress(addressForm.getDefaultAddress().booleanValue());
		}

		if (null != addressForm.getBillingAddress())
		{
			addressData.setBillingAddress(addressForm.getBillingAddress());
		}

		addressData.setShippingAddress(true);
		addressData.setPhone(addressForm.getPhone());
		addressData.setEmail(getEmailId(addressForm));

		if (null != addressForm.getSaveInAddressBook())
		{
			addressData.setVisibleInAddressBook(addressForm.getSaveInAddressBook());
		}

		if (StringUtils.isNotEmpty(addressForm.getCountryIso()))
		{
			final CountryData countryData = i18NFacade.getCountryForIsocode(addressForm.getCountryIso());
			addressData.setCountry(countryData);
		}
		if (StringUtils.isNotEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = i18NFacade.getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			addressData.setRegion(regionData);
		}

		return addressData;
	}

	/**
	 * This method return email id
	 *
	 * @param addressForm
	 *           the address form
	 * @return the email id
	 */
	private String getEmailId(final ExtAddressForm addressForm)
	{
		String emailId = null;
		if (StringUtils.isEmpty(addressForm.getEmailId()))
		{
			final CustomerModel customerModel = (CustomerModel) userService.getCurrentUser();
			emailId = customerModel.getContactEmail();
		}
		else
		{
			emailId = addressForm.getEmailId();
		}
		return emailId;
	}

	/**
	 * Convert to suggested address dto.
	 *
	 * @param userAddressVerifyResponseDTO
	 *           the easy post address DTO
	 * @param addressData
	 *           the address data
	 * @return the ext address data
	 */
	public ExtAddressData convertToSuggestedAddressDto(final UserAddressVerifyResponseDTO userAddressVerifyResponseDTO,
			final ExtAddressData addressData)
	{
		final ExtAddressData suggestedAddressData = new ExtAddressData();
		if (null != userAddressVerifyResponseDTO)
		{
			suggestedAddressData.setId(addressData.getId());
			suggestedAddressData.setFirstName(addressData.getFirstName());
			suggestedAddressData.setLastName(addressData.getLastName());
			suggestedAddressData.setEmail(addressData.getEmail());
			suggestedAddressData.setLine1(userAddressVerifyResponseDTO.getStreet1());
			suggestedAddressData.setLine2(userAddressVerifyResponseDTO.getStreet2());
			suggestedAddressData.setTown(userAddressVerifyResponseDTO.getCity());
			suggestedAddressData.setBillingAddress(addressData.isBillingAddress());
			suggestedAddressData.setShippingAddress(true);
			suggestedAddressData.setPhone(addressData.getPhone());
			suggestedAddressData.setVisibleInAddressBook(addressData.isVisibleInAddressBook());
			suggestedAddressData.setDefaultAddress(addressData.isDefaultAddress());

			if (userAddressVerifyResponseDTO.getZip().contains(SimonWebConstants.HYPHEN))
			{
				final String[] zipCode = userAddressVerifyResponseDTO.getZip().split(SimonWebConstants.HYPHEN);
				suggestedAddressData.setPostalCode(zipCode[0]);
			}
			else
			{
				suggestedAddressData.setPostalCode(userAddressVerifyResponseDTO.getZip());
			}
			if (null != userAddressVerifyResponseDTO.getVerifications())
			{
				final boolean addressVerify = userAddressVerifyResponseDTO.getVerifications().getDelivery().isSuccess();
				suggestedAddressData.setVerification(addressVerify);
				if (!addressVerify)
				{
					suggestedAddressData
							.setErrorMessage(Localization.getLocalizedString(SimonFacadesConstants.CHECKOUT_VERIFY_FAIL_ADDRESS_LABEL));

				}
			}

			if (StringUtils.isNotEmpty(userAddressVerifyResponseDTO.getCountry()))
			{
				final CountryData countryData = i18NFacade.getCountryForIsocode(userAddressVerifyResponseDTO.getCountry());
				suggestedAddressData.setCountry(countryData);
			}
			if (StringUtils.isNotEmpty(userAddressVerifyResponseDTO.getState()))
			{
				final RegionData regionData = i18NFacade.getRegion(userAddressVerifyResponseDTO.getCountry(),
						userAddressVerifyResponseDTO.getCountry() + SimonWebConstants.HYPHEN + userAddressVerifyResponseDTO.getState());
				suggestedAddressData.setRegion(regionData);
			}
		}


		return suggestedAddressData;
	}

	/**
	 * Method to convert to visible address data.
	 *
	 * @param extAddressForm
	 *           the ExtAddressForm object.
	 *
	 * @return addressData the AddressData object.
	 *
	 */
	public ExtAddressData convertToVisibleAddressData(final ExtAddressForm extAddressForm)
	{
		final ExtAddressData extAddressData = convertToAddressData(extAddressForm);
		extAddressData.setVisibleInAddressBook(true);
		return extAddressData;
	}
}
