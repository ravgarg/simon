package com.simon.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.AbstractAcceleratorAuthenticationProvider;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.I18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsChecker;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Months;
import com.simon.core.exceptions.SystemException;
import com.simon.core.services.ExtCountryService;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.data.MallData;
import com.simon.integration.constants.SimonIntegrationConstants;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.login.dto.UserCenterIdDTO;
import com.simon.integration.users.login.dto.VIPUserDTO;
import com.simon.integration.users.oauth.services.AuthLoginIntegrationService;
import com.simon.integration.utils.UserIntegrationUtils;


/**
 * This is the class to Authentication all login request. If user is available in Hybris then we updated user and else
 * not available we registe same user.If any error occurs during integration call it return to login/checkout page with
 * error and after sucessfull logon redirected to respectvi page according to scenario.
 */
public class ExtAuthenticationProvider extends AbstractAcceleratorAuthenticationProvider
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtAuthenticationProvider.class);
	private static final String INTEGRATION_USER_AUTH_TOKEN_NOT_MEET_COMPLEXITY = "752";

	/**
	 * Something went wrong label Constant
	 */
	public static final String LABEL_AUTHENTICATION_PROVIDER_SOMETHINGWENT_WRONG = "label.CoreAuthenticationProvider.somthingWentWrong";
	/**
	 * Bad credential wrong label Constant
	 */
	public static final String LABEL_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS = "label.CoreAuthenticationProvider.badCredentials";

	private final UserDetailsChecker postAuthenticationChecks = new DefaultPostAuthenticationChecks();

	private UserService userService;
	private ModelService modelService;
	@Resource
	private SessionService sessionService;
	@Resource
	private AuthLoginIntegrationService authLoginIntegrationService;
	@Resource
	private ConfigurationService configurationService;
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource
	private UserIntegrationUtils userIntegrationUtils;

	@Resource
	private ExtCountryService extCountryService;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "i18nService")
	private I18NService i18nService;

	/**
	 * This is the method where we make call to integration to verify is user exists or not .If user is authentic, return
	 * Authentication.
	 *
	 *
	 */
	@Override
	public Authentication authenticate(final Authentication authentication)
			throws BadCredentialsException, UnknownIdentifierException
	{
		final UserDetails userDetails = authenticateUser(authentication);
		if (userDetails != null)
		{
			final User user = UserManager.getInstance().getUserByLogin(userDetails.getUsername());
			additionalAuthenticationChecks(userDetails, (AbstractAuthenticationToken) authentication);
			postAuthenticationChecks.check(userDetails);
			JaloSession.getCurrentSession().setUser(user);
			sessionService.setAttribute(SimonCoreConstants.LOGIN_SUCCESS_MESSAGE_CODE, Boolean.TRUE);
			return createSuccessAuthentication(authentication, userDetails);
		}
		return null;
	}

	/**
	 * @see de.hybris.platform.spring.security.CoreAuthenticationProvider#additionalAuthenticationChecks(org.springframework.security.core.userdetails.UserDetails,
	 *      org.springframework.security.authentication.AbstractAuthenticationToken)
	 */
	@Override
	protected void additionalAuthenticationChecks(final UserDetails details, final AbstractAuthenticationToken authentication)

	{
		super.additionalAuthenticationChecks(details, authentication);

		// Check if user has supplied no password
		if (StringUtils.isEmpty((String) authentication.getCredentials()))
		{
			throw new BadCredentialsException("Login without password");
		}
	}

	/***
	 * This is the method which is calling integration services to verify is user exists or not
	 *
	 * @param authentication
	 * @param username
	 * @return
	 * @throws TokenExpiredException
	 * @throws AuthLoginIntegrationException
	 */
	private UserDetails authenticateUser(final Authentication authentication)
			throws BadCredentialsException, UnknownIdentifierException
	{
		final boolean esbPoAPIDisabled = configurationService.getConfiguration()
				.getBoolean(SimonIntegrationConstants.ESB_PO_API_SWITCH_FLAG, false);
		final String username = (authentication.getPrincipal() == null) ? "NONE_PROVIDED" : authentication.getName();
		UserDetails userDetails = null;
		removeErrorCodeFromSession();
		if (esbPoAPIDisabled)
		{
			try
			{
				return retrieveUser(username);
			}
			catch (final UsernameNotFoundException notFound)
			{
				LOG.debug(" Authentication Credentials is not instance of String {}", notFound);
				throwMessageText(LABEL_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, StringUtils.EMPTY);
			}
		}
		if ((authentication.getCredentials() instanceof String))
		{
			Boolean isAuthTokenGenratedSucessfully = false;
			try
			{
				isAuthTokenGenratedSucessfully = authLoginIntegrationService.getOAuthToken(username,
						(String) authentication.getCredentials());
			}
			catch (final AuthLoginIntegrationException exception)
			{
				LOG.error("Bad Credentials.", exception);
				if (StringUtils.isNotEmpty(exception.getUserName())
						&& INTEGRATION_USER_AUTH_TOKEN_NOT_MEET_COMPLEXITY.equalsIgnoreCase(exception.getCode()))
				{
					final String messageText = SimonCoreConstants.INTEGRATION_USER_AUTH_TOKEN_ERROR_CODE + exception.getCode()
							+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
					throwMessageText(messageText, exception.getUserName());
				}
				else
				{
					throwMessageText(SimonCoreConstants.INTEGRATION_USER_AUTH_TOKEN_ERROR_CODE + exception.getCode()
							+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE, StringUtils.EMPTY);
				}
			}

			if (isAuthTokenGenratedSucessfully)
			{

				VIPUserDTO loginResponseDTO = null;
				try
				{
					loginResponseDTO = authLoginIntegrationService.validateLogin(username, (String) authentication.getCredentials());
				}
				catch (final AuthLoginIntegrationException e)
				{
					LOG.error("Bad Credentials.", e);
					throwMessageText(SimonCoreConstants.INTEGRATION_USER_AUTH_LOGIN_ERROR_CODE + e.getCode()
							+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE, StringUtils.EMPTY);
				}
				if (loginResponseDTO != null)
				{

					try
					{
						userDetails = retrieveUser(username);
						updateUser(loginResponseDTO, username);
					}
					catch (final UsernameNotFoundException notFound)
					{
						LOG.debug("User is not available In hybris.Creating user {} ", notFound);
						registerUser(loginResponseDTO, username);
						userDetails = retrieveUser(username);
					}

				}
				else
				{
					LOG.debug("Integration service is not returning user after Auth Token Genrated Sucessfully  ");
					throwMessageText(LABEL_AUTHENTICATION_PROVIDER_SOMETHINGWENT_WRONG, StringUtils.EMPTY);
				}

			}
			else
			{
				LOG.debug(" Error ocured during Auth Token generation call");
				throwMessageText(LABEL_AUTHENTICATION_PROVIDER_SOMETHINGWENT_WRONG, StringUtils.EMPTY);
			}
		}
		else
		{
			LOG.debug(" Authentication Credentials is not instance of String ");
			throwMessageText(LABEL_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, StringUtils.EMPTY);
		}
		return userDetails;
	}

	/***
	 * This the method to remove Error Message from session.
	 */
	private void removeErrorCodeFromSession()
	{
		if (StringUtils.isNotEmpty(sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_MESSAGE_CODE)))
		{
			sessionService.removeAttribute(SimonCoreConstants.LOGIN_ERROR_MESSAGE_CODE);
		}
		if (StringUtils.isNotEmpty(sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA)))
		{
			sessionService.removeAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA);
		}
	}

	/**
	 * This is the method to register customer after Successfully validated from third party get user details as well and
	 * this user is not available in our system. So we register user in oUr system as well.
	 *
	 * @param loginResponseDTO
	 * @param emailId
	 */

	private void registerUser(final VIPUserDTO loginResponseDTO, final String emailId)
			throws BadCredentialsException, UnknownIdentifierException
	{
		final RegisterData data = new RegisterData();

		data.setLogin(emailId);

		data.setFirstName(loginResponseDTO.getFirstName());
		data.setLastName(loginResponseDTO.getLastName());

		if (StringUtils.isNotEmpty(loginResponseDTO.getBirthMonth()))
		{
			data.setBirthMonth(userIntegrationUtils.getBirthMonthFromCode(loginResponseDTO.getBirthMonth()));
		}
		if (StringUtils.isNotEmpty(loginResponseDTO.getCountry()))
		{
			data.setCountry(extCountryService.getCountryForExternalId(loginResponseDTO.getCountry()).getIsocode());
		}

		if (loginResponseDTO.getBirthYear() != null)
		{
			data.setBirthYear(loginResponseDTO.getBirthYear());
		}
		data.setPrimaryMall(fetchPrimaryMall(loginResponseDTO.getCenterIds()));
		data.setAlternateMalls(fetchAlternateMalls(loginResponseDTO.getCenterIds()));
		data.setZipcode(loginResponseDTO.getZipCode());
		data.setExternalId(loginResponseDTO.getId());
		data.setOptedInEmail(loginResponseDTO.isOptedInEmail());
		if (StringUtils.isNotEmpty(loginResponseDTO.getGender()))
		{
			data.setGender(userIntegrationUtils.getGenderFromCode(loginResponseDTO.getGender()).getCode());
		}
		try
		{
			extCustomerFacade.registerShopper(data, false);
		}
		catch (final DuplicateUidException ex)
		{
			LOG.error("Registration failed during login/checkout new user : ", ex);
			throwMessageText(LABEL_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, StringUtils.EMPTY);
		}
		catch (final UserIntegrationException e)
		{
			LOG.error("Registration failed during login/checkout new user : ", e);
			throwMessageText(SimonCoreConstants.INTEGRATION_USER_REGISTER_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE, StringUtils.EMPTY);
		}
		catch (final AuthLoginIntegrationException e)
		{
			LOG.error("Registration failed during login/checkout new user due to Authentication Failure: ", e);
			throwMessageText(SimonCoreConstants.INTEGRATION_USER_AUTH_LOGIN_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE, StringUtils.EMPTY);
		}
	}


	private void updateUser(final VIPUserDTO loginResponseDTO, final String emailId)
			throws BadCredentialsException, UnknownIdentifierException
	{

		final CustomerData customerData = new CustomerData();
		customerData.setFirstName(loginResponseDTO.getFirstName());
		customerData.setLastName(loginResponseDTO.getLastName());

		customerData.setEmailAddress(emailId);
		customerData.setZipcode(loginResponseDTO.getZipCode());
		customerData.setBirthYear(loginResponseDTO.getBirthYear());
		if (StringUtils.isNotEmpty(loginResponseDTO.getBirthMonth()))
		{
			customerData.setBirthMonth(
					Months.valueOf(userIntegrationUtils.getBirthMonthFromCode(loginResponseDTO.getBirthMonth()).toUpperCase()));
		}
		if (StringUtils.isNotEmpty(loginResponseDTO.getGender()))
		{
			customerData.setGender(userIntegrationUtils.getGenderFromCode(loginResponseDTO.getGender()));
		}
		if (StringUtils.isNotEmpty(loginResponseDTO.getCountry()))
		{
			customerData.setCountry(extCountryService.getCountryForExternalId(loginResponseDTO.getCountry()).getIsocode());
		}
		customerData.setUid(emailId);
		customerData.setOptedInEmail(loginResponseDTO.isOptedInEmail());

		customerData.setPrimaryMall(fetchPrimaryMall(loginResponseDTO.getCenterIds()));
		customerData.setAlternateMalls(fetchAlternateMalls(loginResponseDTO.getCenterIds()));
		try
		{
			extCustomerFacade.updateProfileInformation(customerData, false);
		}
		catch (final DuplicateUidException ex)
		{
			LOG.error("Registration failed deuring login/checkout new user : ", ex);
			throwMessageText(LABEL_AUTHENTICATION_PROVIDER_BAD_CREDENTIALS, StringUtils.EMPTY);
		}
		catch (final UserIntegrationException e)
		{
			LOG.error("Registration failed deuring login/checkout new user : ", e);
			throwMessageText(SimonCoreConstants.INTEGRATION_USER_UPDATE_PROFILE_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE, StringUtils.EMPTY);
		}
	}



	/**
	 * Fetches primary mall from the LoginDTO.
	 *
	 * @param listOfCenterIds
	 *           the list of center ids
	 * @return the mall data
	 */
	private MallData fetchPrimaryMall(final List<UserCenterIdDTO> listOfCenterIds)
	{
		if (null != listOfCenterIds && CollectionUtils.isNotEmpty(listOfCenterIds))
		{
			final MallData primaryMallData = new MallData();
			primaryMallData.setCode(listOfCenterIds.get(0).getId());
			return primaryMallData;
		}
		return null;
	}

	/**
	 * Fetches alternate malls from LoginDTO.
	 *
	 * @param listOfCenterIds
	 *           the list of center ids
	 * @return the list
	 */
	private List<MallData> fetchAlternateMalls(final List<UserCenterIdDTO> listOfCenterIds)
	{
		if (null != listOfCenterIds && CollectionUtils.isNotEmpty(listOfCenterIds) && listOfCenterIds.size() >= 2)
		{
			final List<MallData> alternateMalls = new ArrayList<>();
			for (int i = 1; i < listOfCenterIds.size(); i++)
			{
				final MallData alternateMallData = new MallData();
				alternateMallData.setCode(listOfCenterIds.get(i).getId());
				alternateMalls.add(alternateMallData);
			}
			return alternateMalls;
		}
		return Collections.emptyList();

	}


	@Override
	protected UserService getUserService()
	{
		return userService;
	}

	@Override
	@Required
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	@Override
	protected ModelService getModelService()
	{
		return modelService;
	}

	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	private class DefaultPostAuthenticationChecks implements UserDetailsChecker
	{
		private DefaultPostAuthenticationChecks()
		{
		}

		@Override
		public void check(final UserDetails user) throws BadCredentialsException, UnknownIdentifierException
		{
			if (!user.isAccountNonLocked())
			{
				throwMessageText("label.AccountStatusUserDetailsChecker.locked", StringUtils.EMPTY);
			}

			if (!user.isEnabled())
			{
				throwMessageText("label.AccountStatusUserDetailsChecker.disabled", StringUtils.EMPTY);
			}

			if (!user.isAccountNonExpired())
			{
				throwMessageText("label.AccountStatusUserDetailsChecker.expired", StringUtils.EMPTY);
			}

			if (!user.isCredentialsNonExpired())
			{
				throwMessageText("label.AccountStatusUserDetailsChecker.credentialsExpired", StringUtils.EMPTY);
			}
		}
	}

	private void throwMessageText(final String messageCode, final String userName)
			throws BadCredentialsException, UnknownIdentifierException
	{
		final String message = messageSource.getMessage(messageCode, new Object[]
		{ userName }, SimonCoreConstants.SOMETHING_WENT_WRONG_MESSAGE, i18nService.getCurrentLocale());
		try
		{
			sessionService.setAttribute(SimonCoreConstants.LOGIN_ERROR_MESSAGE_CODE, message);
			sessionService.setAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA, "true");

			throw new BadCredentialsException(messageCode);
		}
		catch (final SystemException exception)
		{
			LOG.error("Error occured : to set login Error Message ", exception);
			throw new UnknownIdentifierException(messageCode);
		}
	}
}
