package com.simon.storefront.util;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.facades.account.data.DesignerData;


/**
 * This Class is for providing utility for storefront related modules.
 */
public class ExtWebUtils
{
	private static final char CHAR_Z = 'Z';
	private static final char CHAR_A = 'A';
	private static final String DIGIT_KEY = "#";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * Partition the data designerData on the basis of the starting letter
	 *
	 * @param designerList
	 * @return {@link Map}
	 */
	public Map<String, Set<DesignerData>> getAlphabeticallyPartionedDesignerDataMap(final List<DesignerData> designerList)
	{
		final Map<String, Set<DesignerData>> dataMap = new LinkedHashMap<>();
		dataMap.put(DIGIT_KEY, null);
		for (char ch = CHAR_A; ch <= CHAR_Z; ++ch)
		{
			dataMap.put(String.valueOf(ch), null);
		}
		designerList.forEach(designer -> {
			final String dName = designer.getName();
			if (StringUtils.isNotEmpty(dName))
			{
				final char charAtZeroIndex = dName.charAt(0);
				if (Character.isDigit(charAtZeroIndex))
				{
					fillDesignerDataToMap(dataMap, designer, DIGIT_KEY);
				}
				else if (Character.isLetter(charAtZeroIndex))
				{
					fillDesignerDataToMap(dataMap, designer, String.valueOf(charAtZeroIndex).toUpperCase());
				}
			}
		});
		return dataMap;
	}

	private void fillDesignerDataToMap(final Map<String, Set<DesignerData>> dataMap, final DesignerData designer, final String key)
	{
		Set<DesignerData> designerSet = dataMap.get(key);
		if (designerSet != null)
		{
			designerSet.add(designer);
		}
		else
		{
			designerSet = new HashSet<>();
			designerSet.add(designer);
			dataMap.put(key, designerSet);
		}
	}

	/**
	 * This gives the breadcrumb in a single string with pipe seperation.
	 *
	 * @param breadCrumbs
	 * @return {@link String}
	 */
	public String populateCategoryForAnalytics(final List<Breadcrumb> breadCrumbs)
	{
		final StringBuilder breadCrumbStr = new StringBuilder();
		final String homeStr = (String) configurationService.getConfiguration().getProperty("navigation.home.category.name");
		for (final Breadcrumb breadcrumb : breadCrumbs)
		{
			if (!homeStr.equals(breadcrumb.getName()))
			{
				breadCrumbStr.append(breadcrumb.getName());
			}

			if (!breadcrumb.equals(breadCrumbs.get(0)) && !breadcrumb.equals(breadCrumbs.get(breadCrumbs.size() - 1)))
			{
				breadCrumbStr.append("|");
			}
		}
		return breadCrumbStr.toString();
	}

	/**
	 * Method to set values for robots.txt
	 *
	 * @param pageForRequest
	 * @return String
	 */
	public String getMetaInfo(final ContentPageModel pageForRequest)
	{
		final StringBuilder meta = new StringBuilder();

		if (StringUtils.isNotEmpty(pageForRequest.getIndex().toString())
				&& StringUtils.isNotEmpty(pageForRequest.getFollow().toString()))
		{
			return meta.append(pageForRequest.getIndex().toString().toLowerCase()).append(",")
					.append(pageForRequest.getFollow().toString().toLowerCase()).toString();
		}
		return null;
	}
}

