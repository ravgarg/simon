package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.AtoZDesignersComponentModel;
import com.simon.core.util.CommonUtils;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtWebUtils;


/**
 * Controller for CMS AtozDesignersComponent
 */
@Controller("AtoZDesignersComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.AtoZDesignersComponent)
public class AtoZDesignersComponentController extends AbstractCMSComponentController<AtoZDesignersComponentModel>
{

	private static final String ALPHABET_MAP = "alphabetMap";
	private static final String ATOZDESIGNERS_PAGETYPE = "atozdesigners";
	private static final String PAGE_TYPE = "pageType";
	private static final String ALPHABETS2 = "alphabets";
	private static final String DESIGNER_DATA_LIST = "designerDataList";
	private static final String ACTIVE_DESIGNER_LIST = "activeDesignerList";

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource
	private SessionService sessionService;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	private static final Logger LOGGER = Logger.getLogger(AtoZDesignersComponentController.class);

	/**
	 * Data of all Designers
	 *
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param model
	 *           of type {@link Model}
	 * @param component
	 *           of type {@link AtoZDesignersComponentModel}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final AtoZDesignersComponentModel component)
	{
		final List<DesignerData> customerFavDesigners = new ArrayList<>();
		try
		{
			if (extCustomerFacade.checkIfUserIsLoggedIn())
			{
				extCustomerFacade.updateFavorite(FavoriteTypeEnum.DESIGNERS.name());
				extCustomerFacade.getAllFavouriteDesigners(customerFavDesigners);
				model.addAttribute(ACTIVE_DESIGNER_LIST, CommonUtils.getDesignerOrderByName(customerFavDesigners));
			}
			final List<DesignerData> designerList = extCustomerFacade.getActiveRetailersDesignerList();
			if (CollectionUtils.isNotEmpty(designerList))
			{
				updateFavFlagInDesignerList(customerFavDesigners, designerList);
				model.addAttribute(ALPHABET_MAP, extWebUtils.getAlphabeticallyPartionedDesignerDataMap(designerList));
			}
			model.addAttribute(PAGE_TYPE, ATOZDESIGNERS_PAGETYPE);
			model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.error("designer Update Failed while fetching and saving designerData: ", e);
		}
	}

	/**
	 * Update the favourite flag of all the designers which are marked as customer favourite
	 *
	 * @param customerFav
	 * @param allDesigners
	 */
	private void updateFavFlagInDesignerList(final List<DesignerData> customerFav, final List<DesignerData> allDesigners)
	{
		final Set<String> favDesignerCodes = customerFav.stream().map(DesignerData::getCode).collect(Collectors.toSet());
		allDesigners.parallelStream().forEach(designer -> {
			if (favDesignerCodes.contains(designer.getCode()))
			{
				designer.setFavorite(true);
			}
		});
	}

	@Override
	protected String getView(final AtoZDesignersComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(getTypeCode(component));
	}

}
