package com.simon.storefront.controllers.cart;


import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.facades.cart.ExtCartFacade;
import com.simon.generated.storefront.controllers.misc.AddToCartController;


/**
 * ExtAddToCartController is extending AddToCartController for Add To Cart Functionality.
 */
@Controller
public class ExtAddToCartController extends AddToCartController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtAddToCartController.class);

	@Resource(name = "cartFacade")
	private ExtCartFacade cartFacade;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	/**
	 * Method will take product SKU and quantity that needs to be added in the cart,ExtCartModificationDataConverter
	 * populates CartModificationData with total item count and labels of add to cart popup.
	 *
	 * @param productCode
	 * @param form
	 * @return CartModificationData
	 */
	@ResponseBody
	@RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
	public CartModificationData addToCartTemp(@RequestParam("productCodePost") final String productCode,
			@Valid final AddToCartForm form)
	{

		CartModificationData cartModification = null;
		try
		{
			cartModification = cartFacade.addToCart(productCode, form.getQty());
			cartFacade.removeAdditionalCart();
			if (checkoutCustomerStrategy.isAnonymousCheckout())
			{
				cartModification.setAnonymousUser(true);
			}
		}
		catch (final CommerceCartModificationException | NumberFormatException exception)
		{
			LOGGER.error("Error occurred while adding to Cart", exception);
		}

		return cartModification;
	}

}
