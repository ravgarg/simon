package com.simon.storefront.forms;

/**
 * The Class PaymentInfoForm.
 */
public class PaymentInfoForm
{


	/** The security code. */
	private String securityCode;

	/** The card name on card. */
	private String nameOnCard;

	/** The card card type. */
	private String cardType;

	/** The card last four digits. */
	private String lastFourDigits;

	/** The card card number. */
	private String cardNumber;

	/** The card expiration month. */
	private String expirationMonth;

	/** The card expiration year. */
	private String expirationYear;

	/** The line 1. */
	private String line1;

	/** The line 2. */
	private String line2;

	/** The town. */
	private String town;

	/** The state. */
	private String state;

	/** The postal code. */
	private String postalCode;

	/** The billing add id. */
	private String billingAddId;

	/** The default payment. */
	private boolean defaultPayment;

	/**
	 * Gets the security code.
	 *
	 * @return the security code
	 */
	public String getSecurityCode()
	{
		return securityCode;
	}

	/**
	 * Sets the security code.
	 *
	 * @param securityCode
	 *           the new security code
	 */
	public void setSecurityCode(final String securityCode)
	{
		this.securityCode = securityCode;
	}



	/**
	 * Gets the name on card.
	 *
	 * @return the name on card
	 */
	public String getNameOnCard()
	{
		return nameOnCard;
	}

	/**
	 * Sets the name on card.
	 *
	 * @param nameOnCard
	 *           the new name on card
	 */
	public void setNameOnCard(final String nameOnCard)
	{
		this.nameOnCard = nameOnCard;
	}

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	public String getCardType()
	{
		return cardType;
	}

	/**
	 * Sets the card type.
	 *
	 * @param cardType
	 *           the new card type
	 */
	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	/**
	 * Gets the last four digits.
	 *
	 * @return the last four digits
	 */
	public String getLastFourDigits()
	{
		return lastFourDigits;
	}

	/**
	 * Sets the last four digits.
	 *
	 * @param lastFourDigits
	 *           the new last four digits
	 */
	public void setLastFourDigits(final String lastFourDigits)
	{
		this.lastFourDigits = lastFourDigits;
	}

	/**
	 * Gets the card number.
	 *
	 * @return the card number
	 */
	public String getCardNumber()
	{
		return cardNumber;
	}

	/**
	 * Sets the card number.
	 *
	 * @param cardNumber
	 *           the new card number
	 */
	public void setCardNumber(final String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	/**
	 * Gets the expiration month.
	 *
	 * @return the expiration month
	 */
	public String getExpirationMonth()
	{
		return expirationMonth;
	}

	/**
	 * Sets the expiration month.
	 *
	 * @param expirationMonth
	 *           the new expiration month
	 */
	public void setExpirationMonth(final String expirationMonth)
	{
		this.expirationMonth = expirationMonth;
	}

	/**
	 * Gets the expiration year.
	 *
	 * @return the expiration year
	 */
	public String getExpirationYear()
	{
		return expirationYear;
	}

	/**
	 * Sets the expiration year.
	 *
	 * @param expirationYear
	 *           the new expiration year
	 */
	public void setExpirationYear(final String expirationYear)
	{
		this.expirationYear = expirationYear;
	}

	/**
	 * Gets the line 1.
	 *
	 * @return the line 1
	 */
	public String getLine1()
	{
		return line1;
	}

	/**
	 * Sets the line 1.
	 *
	 * @param line1
	 *           the new line 1
	 */
	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	/**
	 * Gets the line 2.
	 *
	 * @return the line 2
	 */
	public String getLine2()
	{
		return line2;
	}

	/**
	 * Sets the line 2.
	 *
	 * @param line2
	 *           the new line 2
	 */
	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	/**
	 * Gets the town.
	 *
	 * @return the town
	 */
	public String getTown()
	{
		return town;
	}

	/**
	 * Sets the town.
	 *
	 * @param town
	 *           the new town
	 */
	public void setTown(final String town)
	{
		this.town = town;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state
	 *           the new state
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * Sets the postal code.
	 *
	 * @param postalCode
	 *           the new postal code
	 */
	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * Gets the billing add id.
	 *
	 * @return the billingAddId
	 */
	public String getBillingAddId()
	{
		return billingAddId;
	}

	/**
	 * Sets the billing add id.
	 *
	 * @param billingAddId
	 *           the billingAddId to set
	 */
	public void setBillingAddId(final String billingAddId)
	{
		this.billingAddId = billingAddId;
	}

	/**
	 * Checks if is default payment.
	 *
	 * @return the defaultPayment
	 */
	public boolean isDefaultPayment()
	{
		return defaultPayment;
	}

	/**
	 * Sets the default payment.
	 *
	 * @param defaultPayment
	 *           the defaultPayment to set
	 */
	public void setDefaultPayment(final boolean defaultPayment)
	{
		this.defaultPayment = defaultPayment;
	}

}
