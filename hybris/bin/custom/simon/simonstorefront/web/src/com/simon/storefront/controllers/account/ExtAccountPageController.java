package com.simon.storefront.controllers.account;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sap.security.core.server.csi.XSSEncoder;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.enums.Months;
import com.simon.core.model.MallModel;
import com.simon.core.util.CommonUtils;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DesignerData;
import com.simon.facades.account.data.DesignerDataList;
import com.simon.facades.account.data.SizeData;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.checkout.data.AccountData;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.cms.SimonCategoryNavigationFacade;
import com.simon.facades.constants.SimonFacadesConstants;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.data.MallData;
import com.simon.facades.mall.MallFacade;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.user.ExtUserFacade;
import com.simon.generated.storefront.controllers.pages.AccountPageController;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.exceptions.UserFavouritesIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.storefront.account.validation.ExtAccountAddressValidator;
import com.simon.storefront.checkout.form.ExtAddressForm;
import com.simon.storefront.checkout.util.ExtAddressDataUtil;
import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.forms.AccountUpdateForm;
import com.simon.storefront.forms.PaymentInfoForm;
import com.simon.storefront.forms.UpdateSizeForm;
import com.simon.storefront.util.ExtWebUtils;
import com.simon.storefront.validator.form.ExtUpdateProfileValidator;

import atg.taglib.json.util.JSONException;


/**
 * ExtAccountPageController is extending AccountPageController to Update Profile Details
 */
@Controller
@RequestMapping("/my-account")
public class ExtAccountPageController extends AccountPageController
{
	private static final String TEXT_ACCOUNT_MY_DESIGNERS_CONFIRMATION_UPDATED = "text.account.myDesigners.confirmationUpdated";

	private static final String CHECKED_ROWS = "checkedRows";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtAccountPageController.class);

	private static final String ENABLE_SPREEDLY_CALLS = "enableSpreedlyCalls";
	private static final String COUNTRY_ISOCODE = "countryIsocode";
	private static final String ADDREES_EDITED_SUCCESS_MESSAGE_VALUE_KEY = "text.accountaddressbook.edited.success.msg.text";
	private static final String ACCOUNT_DETAILS_NO_SAVED_DATA_KEY = "noSavedDataKey";
	private static final String ACCOUNT_DETAILS_MY_FAVORITES_DETAILS = "myFavoriteDetails";
	private static final String ACCOUNT_DETAILS_HOW_TO_ADD_KEY = "howToAddKey";
	private static final String ACCOUNT_DETAILS_USERNAME_KEY = "userName";
	private static final String ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY = "pageLevelClass";
	private static final String ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY = "favPopupMappingClass";
	private static final String ACCOUNT_DETAILS_TITLE_KEY = "titleKey";
	private static final String STATE_DATA_LIST = "stateList";
	private static final String ACC_CONF_MSGS = "accConfMsgs";
	private static final String REDIRECT_TO_UPDATE_PROFILE = REDIRECT_PREFIX + "/my-account/update-profile";
	private static final String REDIRECT_TO_MY_DESIGNER = REDIRECT_PREFIX + "/my-account/my-designers";
	private static final String BIRTH_YEAR_START = "simonstorefront.account.birthyear.start";
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String TEXT_ACCOUNT_PROFILE = "text.account.profile";

	private static final String UPDATE_PROFILE_CMS_PAGE = "update-profile";
	private static final String MY_FAVORITES_CMS_PAGE = "my-favorites";
	private static final String TEXT_ACCOUNT_ADDRESS_BOOK = "text.account.addressBook";
	private static final String ADDRESS_DATA_ATTR = "addressData";

	private static final String ACCOUNT_PAGE_SPECIFIC_DIV_CLASS_KEY = "accountPageSpecificDivClass";
	private static final String ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE = "address-book";
	private static final String PROFILE_PAGE_SPECIFIC_DIV_CLASS_VALUE = "myprofile-container";

	private static final String ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY = "accountPageHeaderMessageKey";
	private static final String PROFILE_PAGE_SPECIFIC_HEADER_MESSAGE_VALUE_KEY = "text.account.profile.myaccount.myprofile";
	private static final String MY_ORDERS_PAGE_SPECIFIC_HEADER_MESSAGE_VALUE_KEY = "order.history.header.text";

	private static final String ACCOUNT_PAGE_NOTIFICATION_UPDATE_KEY = "accountPageNotificationUpdateKey";
	private static final String ACCOUNT_PAGE_NOTIFICATION_ERROR_KEY = "accountPageNotificationErrorKey";

	private static final String STUB_SPREEDLY_CALLS = "simon.spreedly.validate.card.browser.call.enable.stub.flag";
	private static final String ADDREES_ADDED_SUCCESS_MESSAGE_VALUE_KEY = "text.accountaddressbook.added.success.msg.text";
	private static final String MY_DESIGNERS_CMS_PAGE_LABEL = "my-designers";
	private static final String TEXT_ACCOUNT_MYDESIGNERS_TITLE = "text.account.myDesigners.title";
	private static final String TEXT_ACCOUNT_MYDESIGNERS_HOTOADD = "text.account.myDesigners.howToAdd";
	private static final String TEXT_ACCOUNT_MYDESIGNERS_NOSAVEDTITLE = "text.account.myDesigners.noSavedTile";
	private static final String TEXT_ACCOUNT_MYDESIGNERS_NOSAVEDDATA = "text.account.myDesigners.noSavedDate";
	private static final String MY_DEALS_CMS_PAGE_LABEL = "my-deals";
	private static final String TEXT_ACCOUNT_MYDEALS_TITLE = "text.account.myDeals.title";
	private static final String TEXT_ACCOUNT_MYDEALS_HOTOADD = "text.account.myDeals.howToAdd";
	private static final String TEXT_ACCOUNT_MYDEALS_NOSAVEDTITLE = "text.account.myDeals.noSavedTile";
	private static final String TEXT_ACCOUNT_MYDEALS_NOSAVEDDATA = "text.account.myDeals.noSavedData";
	private static final Object TEXT_ACCOUNT_MYDEALS_VIEWBUTTON = "text.account.myDeals.viewDeal.button";
	private static final String TEXT_ACCOUNT_FEATUREDDESIGNERS_TITLE = "text.account.featuredDesigners.title";
	private static final String MY_ACCOUNT_ADDRESS_BOOK_URL = "/my-account/address-book";
	private static final String REDIRECT_TO_ADDRESS_BOOK_PAGE = REDIRECT_PREFIX + MY_ACCOUNT_ADDRESS_BOOK_URL;
	private static final String MY_STORES_CMS_PAGE_LABEL = "my-stores";
	private static final String TEXT_ACCOUNT_MYSTORE_TITLE = "text.account.myStores.title";
	private static final String TEXT_ACCOUNT_MYSTORE_HOTOADD = "text.account.myStores.howToAdd";
	private static final String TEXT_ACCOUNT_MYSTORE_NOSAVEDTITLE = "text.account.myStores.noSavedTile";
	private static final String TEXT_ACCOUNT_MYSTORE_MYSTOREADVANTGETEXT = "text.account.myStores.myStoreAdvantageText";
	private static final String TEXT_ACCOUNT_FEATUREDSTORES_TITLE = "text.account.featuredStores.title";
	private static final String TEXT_ACCOUNT_TRENDINGNOW_TITLE = "text.account.trendingNow.title";
	private static final String TEXT_ACCOUNT_CMS1_TITLE_LABEL = "cmsComponent1titleKey";
	private static final String TEXT_ACCOUNT_COMINGSOON_TITLE = "text.account.comingsoon.title";
	private static final String TEXT_ACCOUNT_CMS2_TITLE_LABEL = "cmsComponent2titleKey";
	private static final String SLASH = "/";
	private static final String PAGEID = "pageId";
	private static final String PAYMENT_DETAILS_CMS_PAGE = "payment-details";
	private static final String PAYMENT_PAGE_SPECIFIC_DIV_CLASS_VALUE = "payment-method";
	private static final String PAYMENT_METHOD_ADDED_SUCCESS_MESSAGE_VALUE_KEY = "text.account.profile.added.success";
	private static final String US_ISO = "US";
	private static final String RETURN_RESPONSE_SUCCESS_PAYMENT = "success=true";
	private static final String RETURN_RESPONSE_FAILURE_PAYMENT = "success=false";
	private static final String PAYMENT_DETAILS_SPECIFIC_HEADER_MESSAGE_VALUE_KEY = "text.account.paymentDetails";

	private static final String PROFILE_UPDATE_FAILED = "text.account.profile.update.failed";
	private static final String PROFILE_UPDATE_SUCCESS = "text.account.profile.update.successful";
	private static final String MY_FAVORITES_MESSAGE_VALUE_KEY = "text.myfavorites.view.error.msg.text";
	private static final String MY_SIZES_MESSAGE_VALUE_KEY = "text.mysizes.view.error.msg.text";
	private static final String MY_DESIGNERS_MESSAGE_VALUE_KEY = "text.mydesigners.view.error.msg.text";
	private static final String MY_STORES_MESSAGE_VALUE_KEY = "text.mystores.view.error.msg.text";
	private static final String MY_DEALS_MESSAGE_VALUE_KEY = "text.mydeals.view.error.msg.text";
	private static final String ORDER_HISTORY_CMS_PAGE = "orders";
	private static final String NO_ORDER_HISTORY_CMS_PAGE = "no-orders";
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String ORDER_DETAIL_CMS_PAGE = "/order/";
	private static final String MY_FAVORITES_TITLE_TEXT = "myFavorites.title.text";

	// CMS Pages
	private static final Object TEXT_ACCOUNT_MYDESIGNERS_ADDBUTTON = "text.account.myDesigners.addDesigner.button";
	private static final Object TEXT_ACCOUNT_MYDESIGNERS_ADDMOREBUTTON = "text.account.myDesigners.addMoreDesigner.button";
	private static final Object TEXT_ACCOUNT_MYSTORES_ADDMOREBUTTON = "text.account.myStores.addMoreStores.button";
	private static final Object TEXT_ACCOUNT_MYSTORES_ADDBUTTON = "text.account.myStores.addStores.button";
	private static final Object TEXT_ACCOUNT_MYSTORES_NOSAVEDDATA = "text.account.myStores.noSavedData";

	private static final String MY_FAV_PRODUCTS = "myFavProduts";
	private static final String MY_SIZES = "sizeList";
	private static final String BASE_CATEGORY_NAME_TO_SUBCATEGORY_DATA_LIST_MAP = "categoryToCategoryMap";
	private static final String SUBCATEGORY_CODE_TO_SIZE_LIST_MAP = "categoryCodeToSizeMap";
	private static final String ADD = "add";
	private static final String REMOVE = "remove";
	private static final String REDIRECT_TO_MY_STORE = REDIRECT_PREFIX + "/my-account/my-stores";
	private static final String REDIRECT_TO_MY_SIZE = REDIRECT_PREFIX + "/my-account/my-size";
	private static final String REDIRECT_TO_PAYMENT_INFO_PAGE = REDIRECT_PREFIX + "/my-account/payment-details";

	private static final String PREMIUM_OUTLETS = "my-premium-outlets";
	private static final String HOW_TO_ADD_FAVORITES = "text.account.premiumoutlets.howto";
	private static final String MY_PREMIUM_OUTLETS = "text.account.premiumoutlets.title";
	private static final String MY_PREMIUM_OUTLET = "text.account.premiumoutlets.breadcrumb";
	private static final String MY_CENTER = "my-center";
	private static final String HOW_TO_ADD_MY_CENTER = "text.account.mycenter.changemycenter";
	private static final String MY_CENTER_BREADCRUMB = "text.account.mycenter.breadcrumb";

	private static final String EMAIL_PREFERENCES = "email-preferences";
	private static final String UPDATE_EMAIL_PREFERENCES = "update-email-preferences";
	private static final String REDIRECT_TO_EMAIL_PREFERENCES = REDIRECT_PREFIX + "/my-account/email-preferences";
	private static final String TEXT_ACCOUNT_EMAIL_PREFERENCES = "text.account.email.preferences";
	private static final String EMAIL_PREFERENCES_UPDATE_FAILED = "text.account.email.preferences.update.failed";
	private static final String EMAIL_PREFERENCES_OPTED_UPDATE_SUCCESS = "text.account.email.preferences.opted.update.successful";
	private static final String EMAIL_PREFERENCES_UNOPTED_UPDATE_SUCCESS = "text.account.email.preferences.unopted.update.successful";
	private static final Object TEXT_ACCOUNT_MYDEALS_ADDBUTTON = "text.account.myDeals.addDeal.button";
	private static final String SET_PAYMENT_ERROR_MESSAGE = "setPaymentErrorMessage";

	private static final String ADD_EDIT_ADDRESS_SUCCESS_FAILURE_MESSAGE_KEY = "addEditAddressSuccessFailureMessageKey";

	private static final String MY_SIZE = "my-size";

	private static final String TEXT_ACCOUNT_MY_SIZES = "text.account.my.sizes";

	private static final String MY_SIZE_UPDATE_SUCCESS = "text.account.mySizes.updated.success";
	private static final String MY_SIZE_UPDATE_ERROR = "text.account.mySizes.updated.error";
	private static final String PAGETYPE = "pageType";

	private static final String CMS_PARAGRAPH_CLASS = "cmsParagraphClass";
	private static final Object PAYMENT_DETAILS_CMS_PARAGRAPH_CSS = "payment-method-text";

	private static final String RETURN_RESPONSE_SUCCESS = "true";
	private static final String RETURN_RESPONSE_FAILURE = "false";

	@Resource(name = "extAddressDataUtil")
	private ExtAddressDataUtil extAddressDataUtil;

	@Resource(name = "extAccountAddressValidator")
	private ExtAccountAddressValidator extAccountAddressValidator;

	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "extUpdateProfileValidator")
	private ExtUpdateProfileValidator extUpdateProfileValidator;

	@Resource(name = "userFacade")
	private ExtUserFacade userFacade;
	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;
	@Resource(name = "extCheckoutFacade")
	private ExtCheckoutFacade extCheckoutFacade;
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "mallFacade")
	private MallFacade mallFacade;

	@Autowired
	private SessionService sessionService;

	@Resource
	private Converter<MallModel, MallData> mallConverter;

	@Resource
	private SimonCategoryNavigationFacade simonCategoryNavigationFacade;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "extCustomMessageUtils")
	private ExtCustomMessageUtils extCustomMessageUtils;

	/**
	 * Controller for View Profile details
	 *
	 * @param model
	 *           the Model object.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	@RequestMapping(value = SLASH + UPDATE_PROFILE_CMS_PAGE, method = RequestMethod.GET)
	@RequireHardLogIn
	public String editProfile(final Model model) throws CMSItemNotFoundException
	{

		final CustomerData customerData = extCustomerFacade.getCurrentCustomer();
		final AccountUpdateForm updateProfileForm = new AccountUpdateForm();
		populateAccountUpdateForm(customerData, updateProfileForm);
		model.addAttribute("customerAlternateMallList", getListOfCustomerAlternateMallsIds(customerData));
		model.addAttribute("customerData", customerData);
		model.addAttribute("updateProfileForm", updateProfileForm);
		model.addAttribute("mallList", mallFacade.getMallList());
		model.addAttribute("startYear", String.valueOf(getConfigurationService().getConfiguration().getString(BIRTH_YEAR_START)));
		final int endYear = Calendar.getInstance().get(Calendar.YEAR) - 18;
		model.addAttribute("endYear", endYear);
		model.addAttribute("isProfileCompleted", customerData.isProfileCompleted());
		model.addAttribute("countries", extCustomerFacade.getAllCountry());
		model.addAttribute("birthMonthList", extCustomerFacade.getMonthsList());
		model.addAttribute("genderList", extCustomerFacade.getGenderList());
		model.addAttribute("validCountriesforZipcode",
				configurationService.getConfiguration().getList(SimonWebConstants.COUNTRIES_VALID_FOR_ZIPCODE));
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_DIV_CLASS_KEY, PROFILE_PAGE_SPECIFIC_DIV_CLASS_VALUE);
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, PROFILE_PAGE_SPECIFIC_HEADER_MESSAGE_VALUE_KEY);

		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		getGlobalMessage(model);
		model.addAttribute(PAGEID, UPDATE_PROFILE_CMS_PAGE);
		model.addAttribute(PAGETYPE, UPDATE_PROFILE_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		return getViewForPage(model);
	}

	/**
	 * @param updateProfileForm
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return String
	 *
	 * @throws CMSItemNotFoundException
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + UPDATE_PROFILE_CMS_PAGE, method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateProfile(@ModelAttribute("updateProfileForm") final AccountUpdateForm updateProfileForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException
	{

		final String returnAction = REDIRECT_TO_UPDATE_PROFILE;
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();


		storeCmsPageInModel(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(UPDATE_PROFILE_CMS_PAGE));

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, PROFILE_UPDATE_FAILED);
		}
		else
		{
			try
			{
				if (updateProfileForm.isUpdateCenter())
				{
					populateCustomerDataForUpdateMyCenter(updateProfileForm, currentCustomerData);
				}
				else
				{
					getExtUpdateProfileValidator().validate(updateProfileForm, bindingResult);
					populateCustomerDataForUpdateProfile(updateProfileForm, currentCustomerData);
				}
				extCustomerFacade.updateProfileInformation(currentCustomerData, true);
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, PROFILE_UPDATE_SUCCESS);
			}
			catch (final DuplicateUidException duplicateUidException)
			{
				LOGGER.error("Update Failed failed : ", duplicateUidException);
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, PROFILE_UPDATE_FAILED);
			}
			catch (final UserIntegrationException userIntegrationException)
			{
				LOGGER.error("Update Failed failed: ", userIntegrationException);
				bindingResult.rejectValue("email", "registration.error.account.exists.title");
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS,
						SimonCoreConstants.INTEGRATION_USER_UPDATE_PROFILE_ERROR_CODE + userIntegrationException.getCode()
								+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE);
			}
		}
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_PROFILE));
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return returnAction;
	}

	/**
	 * Method to populate currentCustomerData ForUpdateMyCenter scenario.
	 *
	 * @param updateProfileForm
	 * @param currentCustomerData
	 */
	private void populateCustomerDataForUpdateMyCenter(final AccountUpdateForm updateProfileForm,
			final CustomerData currentCustomerData)
	{

		if (StringUtils.isNotBlank(updateProfileForm.getPrimaryMall()))
		{
			final MallData primaryMallData = new MallData();
			primaryMallData.setCode(updateProfileForm.getPrimaryMall());
			currentCustomerData.setPrimaryMall(primaryMallData);
		}

		final List<String> alternateMallIds = Arrays
				.asList(StringUtils.split(updateProfileForm.getAlternateMallIds(), SimonCoreConstants.DELIMITER));
		if (CollectionUtils.isNotEmpty(alternateMallIds))
		{
			final List<MallData> alternateMallDataSet = new ArrayList<>();
			for (final String mallCode : alternateMallIds)
			{
				final MallData mallData = new MallData();
				mallData.setCode(mallCode);
				alternateMallDataSet.add(mallData);
			}
			currentCustomerData.setAlternateMalls(alternateMallDataSet);
		}

	}

	/**
	 * This method is called for displaying saved designer of the profile.
	 *
	 * @param model
	 * @return pagename the String object.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + MY_STORES_CMS_PAGE_LABEL, method = RequestMethod.GET)
	@RequireHardLogIn
	public String showStores(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		try
		{
			final boolean status = extCustomerFacade.getAllFavouriteStores(currentCustomerData);
			if (!status)
			{
				setAccountPageNotificationUpdateKey(model, MY_STORES_MESSAGE_VALUE_KEY);
			}
		}
		catch (final DuplicateUidException duplicateUidException)
		{
			LOGGER.error("Stores Update Failed while fetching and saving storeData: ", duplicateUidException);
		}
		if (!CollectionUtils.isEmpty(currentCustomerData.getMyStores()))
		{
			model.addAttribute("storeList", currentCustomerData.getMyStores());
			model.addAttribute("addMoreStores", TEXT_ACCOUNT_MYSTORES_ADDMOREBUTTON);
		}
		else
		{
			model.addAttribute("noSavedStoresTitle", TEXT_ACCOUNT_MYSTORE_NOSAVEDTITLE);
			model.addAttribute("noSavedStoreData", TEXT_ACCOUNT_MYSTORES_NOSAVEDDATA);
			model.addAttribute(ACCOUNT_DETAILS_USERNAME_KEY, currentCustomerData.getFirstName());
			model.addAttribute("addStores", TEXT_ACCOUNT_MYSTORES_ADDBUTTON);
		}
		model.addAttribute(ACCOUNT_DETAILS_NO_SAVED_DATA_KEY, TEXT_ACCOUNT_MYSTORE_NOSAVEDTITLE);
		model.addAttribute("mystoreAdvantgeText", TEXT_ACCOUNT_MYSTORE_MYSTOREADVANTGETEXT);
		model.addAttribute(ACCOUNT_DETAILS_USERNAME_KEY, currentCustomerData.getFirstName());
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, TEXT_ACCOUNT_MYSTORE_TITLE);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, TEXT_ACCOUNT_MYSTORE_HOTOADD);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_STORES_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_STORES_CMS_PAGE_LABEL));
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, MY_STORES_CMS_PAGE_LABEL);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_STORES_CMS_PAGE_LABEL);
		model.addAttribute(TEXT_ACCOUNT_CMS2_TITLE_LABEL, TEXT_ACCOUNT_COMINGSOON_TITLE);
		model.addAttribute(TEXT_ACCOUNT_CMS1_TITLE_LABEL, TEXT_ACCOUNT_FEATUREDSTORES_TITLE);
		getGlobalMessage(model);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_MYSTORE_TITLE));
		model.addAttribute(PAGEID, MY_STORES_CMS_PAGE_LABEL);
		model.addAttribute(PAGETYPE, MY_STORES_CMS_PAGE_LABEL);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return getViewForPage(model);
	}

	/**
	 * This method will fetch all Store from the database.it will exclude the user saved Stores from the list.
	 *
	 * @return pagename the String object.
	 */
	@RequestMapping(value = "/get-stores", method = RequestMethod.GET)
	@RequireHardLogIn
	@ResponseBody
	public StoreDataList getAllStores()
	{
		final CustomerData customerData = extCustomerFacade.getCurrentCustomer();
		final List<StoreData> allStoresList = extCustomerFacade.fetchAllStoresExceptLinked(customerData);
		final StoreDataList storeDataList = new StoreDataList();
		storeDataList.setAllStoresList(allStoresList);
		return storeDataList;

	}

	/**
	 * This method is called for saving selected store of the profile.
	 *
	 * @param redirectModel
	 * @param request
	 * @return returnAction
	 */
	@ResponseBody
	@RequestMapping(value = "/add-stores", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addStores(final RedirectAttributes redirectModel, final HttpServletRequest request)
	{
		final String[] storeIds = request.getParameterValues(CHECKED_ROWS);
		final CustomerData customerData = extCustomerFacade.getCurrentCustomer();
		final boolean addStatus = extCustomerFacade.updateStoreToProfile(customerData, storeIds);
		if (addStatus)
		{
			GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS, "text.account.myStores.confirmationUpdated", null);
			return RETURN_RESPONSE_SUCCESS;

		}
		else
		{
			return RETURN_RESPONSE_FAILURE;
		}
	}


	/**
	 * Populates the customerData form the profileForm
	 *
	 * @param updateProfileForm
	 * @param currentCustomerData
	 * @param customerData
	 */
	private void populateCustomerDataForUpdateProfile(final AccountUpdateForm updateProfileForm,
			final CustomerData currentCustomerData)
	{
		currentCustomerData.setFirstName(updateProfileForm.getFirstName());
		currentCustomerData.setLastName(updateProfileForm.getLastName());
		if (ArrayUtils.contains(
				StringUtils.split(configurationService.getConfiguration().getString(SimonWebConstants.COUNTRIES_VALID_FOR_ZIPCODE),
						SimonCoreConstants.DELIMITER),
				updateProfileForm.getCountry()))
		{
			currentCustomerData.setZipcode(updateProfileForm.getZipCode());
		}
		else
		{
			currentCustomerData.setZipcode(null);
		}
		currentCustomerData.setBirthYear(updateProfileForm.getBirthYear());
		currentCustomerData.setBirthMonth(Months.valueOf(updateProfileForm.getBirthMonth().toUpperCase()));
		currentCustomerData.setGender(CustomerGender.valueOf(updateProfileForm.getGender()));
		currentCustomerData.setCountry(updateProfileForm.getCountry());
		currentCustomerData.setOldPassword(updateProfileForm.getCurrentPassword());
		if (Boolean.TRUE.toString().equalsIgnoreCase(updateProfileForm.getPasswordUpdate()))
		{
			currentCustomerData.setPassword(updateProfileForm.getPassword());
		}
		else
		{
			currentCustomerData.setPassword(null);
		}
		currentCustomerData.setProfileCompleted(true);
	}

	/**
	 * Method to get the Address Book CMS page.
	 *
	 * @param model
	 *           the Model object.
	 *
	 * @return String the view page name.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	@RequestMapping(value = SLASH + ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE, method = RequestMethod.GET)
	@RequireHardLogIn
	public String getAddressBook(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(COUNTRY_ISOCODE,
				configurationService.getConfiguration().getString(SimonFacadesConstants.SIMON_COUNTRY_ISOCODE));
		model.addAttribute(STATE_DATA_LIST, extCustomerFacade.getStateDataListForShippingAddress());
		model.addAttribute(ADDRESS_DATA_ATTR, userFacade.getAddressBook());
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_DIV_CLASS_KEY, ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE);
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, TEXT_ACCOUNT_ADDRESS_BOOK);
		storeCmsPageInModel(model, getContentPageForLabelOrId(ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE));
		model.addAttribute(PAGEID, ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE);
		model.addAttribute(PAGETYPE, ADDRESS_PAGE_SPECIFIC_DIV_CLASS_VALUE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_ADDRESS_BOOK));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		getGlobalMessage(model);

		final String addEditAddressSuccessFailureErrorMessage = sessionService
				.getAttribute(ADD_EDIT_ADDRESS_SUCCESS_FAILURE_MESSAGE_KEY);
		if (StringUtils.isNotEmpty(addEditAddressSuccessFailureErrorMessage))
		{
			setAccountPageNotificationUpdateKey(model, addEditAddressSuccessFailureErrorMessage);
			sessionService.removeAttribute(ADD_EDIT_ADDRESS_SUCCESS_FAILURE_MESSAGE_KEY);
		}

		return getViewForPage(model);
	}

	/**
	 * Method is used to add or save user address suggested or user entered address into the database for the respective
	 * Customer's Profile.
	 *
	 *
	 * @param extAddressForm
	 *           the ExtAddressForm object.
	 * @return accountData the AccountData object.
	 *
	 */
	@RequestMapping(value = "/addedit-address", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public AccountData addEditAddress(final ExtAddressForm extAddressForm)
	{
		final ExtAddressData newAddress = extAddressDataUtil.convertToVisibleAddressData(extAddressForm);

		if (userFacade.isAddressBookEmpty())
		{
			newAddress.setDefaultAddress(true);
		}
		else
		{
			newAddress.setDefaultAddress(
					extAddressForm.getDefaultAddress() != null && extAddressForm.getDefaultAddress().booleanValue());
		}

		if (StringUtils.isNotBlank(newAddress.getId()))
		{
			return editAddress(newAddress);
		}
		else
		{
			return addAddress(newAddress);
		}

	}

	/**
	 * Method is invoked once user enter data into Add Address popup modal. This method is responsible to verify the
	 * address via easy post. It actually check if Delivery Address is valid or not.
	 *
	 * @param extAddressForm
	 *           the ExtAddressForm object
	 * @param bindingResult
	 *           the BindingResult object
	 *
	 * @return the accountData object the AccountData object
	 *
	 * @throws UserAddressVerifyIntegrationException
	 */
	@RequestMapping(value = "/verify-add", method = RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
	public AccountData addressVerify(final ExtAddressForm extAddressForm, final BindingResult bindingResult)
			throws UserAddressVerifyIntegrationException
	{
		Map<String, String> messageLabelMap = new HashMap<>();
		extAccountAddressValidator.validate(extAddressForm, bindingResult);

		//convert address form to address data
		final ExtAddressData addressData = extAddressDataUtil.convertToAddressData(extAddressForm);

		final UserAddressVerifyResponseDTO userAddressVerifyResponseDTO = userFacade.getUserAddressVerifyResponseDTO(addressData);

		//convert  to ExtAddressData
		final ExtAddressData suggestedAddress = extAddressDataUtil.convertToSuggestedAddressDto(userAddressVerifyResponseDTO,
				addressData);

		final AccountData accountData = new AccountData();

		accountData.setSuggestedAddress(suggestedAddress);
		accountData.setUserAddress(addressData);
		messageLabelMap = extCustomMessageUtils.setCheckoutShippingMessages();
		accountData.setMessageLabels(messageLabelMap);

		return accountData;
	}

	/**
	 * Method to remove address from address book.
	 *
	 * @param addressId
	 * @param redirectModel
	 *
	 * @return accountData
	 */
	@Override
	@RequestMapping(value = "/remove-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeAddress(@RequestParam(value = "addressid", required = false) final String addressId,
			final RedirectAttributes redirectModel)
	{
		final ExtAddressData extAddressData = new ExtAddressData();
		extAddressData.setId(addressId);

		if (StringUtils.isNotBlank(extAddressData.getId()))
		{

			try
			{
				final boolean success = userFacade.removeUserAddress(extAddressData);
				if (success)
				{
					GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS, "text.accountaddressbook.deleted.success.msg.text");
				}
				else
				{
					GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS, "text.accountaddressbook.deleted.failure.msg.text");
				}
			}
			catch (final ModelSavingException | UserAddressIntegrationException exception)
			{
				LOGGER.error("Exception occurred while removing the address", exception);
				GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS, "text.accountaddressbook.deleted.failure.msg.text");
			}
		}

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	/**
	 * Method to set current address with addressid as Default Address in the Customer's profile.
	 *
	 * @param addressCode
	 * @param redirectModel
	 *
	 * @return viewPage to be rendered in the frontend.
	 *
	 */
	@Override
	@RequestMapping(value = "/set-default-address", method = RequestMethod.POST)
	@RequireHardLogIn
	public String setDefaultAddress(@RequestParam("addressid") final String addressCode, final RedirectAttributes redirectModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setDefaultAddress(true);
		addressData.setVisibleInAddressBook(true);
		addressData.setId(addressCode);

		try
		{
			userFacade.setDefaultAddress(addressData);
			GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS,
					"text.accountaddressbook.preferredaddress.success.msg.text");
		}
		catch (final Exception exception)
		{
			LOGGER.error("Exception occurred while setting default address", exception);
			GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS,
					"text.accountaddressbook.preferredaddress.failure.msg.text");
		}

		return REDIRECT_TO_ADDRESS_BOOK_PAGE;
	}

	/**
	 * Controller for viewing My favorites for the user
	 *
	 * @param model
	 *           the Model object.
	 *
	 * @return pagename the String object.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + MY_FAVORITES_CMS_PAGE, method = RequestMethod.GET)
	@RequireHardLogIn
	public String myFavorites(final Model model) throws CMSItemNotFoundException
	{
		try
		{
			final List<String> favProducts = extCustomerFacade.getMyFavoriteFromPO();
			ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
			if (!CollectionUtils.isEmpty(favProducts))
			{
				final PageableData pageableData = new PageableData();
				pageableData.setSort(StringUtils.EMPTY);
				final SearchStateData searchState = new SearchStateData();
				final SearchQueryData searchQueryData = new SearchQueryData();
				searchQueryData.setValue(StringUtils.EMPTY);
				searchState.setQuery(searchQueryData);
				sessionService.setAttribute("favoriteProducts", favProducts);
				searchPageData = encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
				final List<ProductData> products = searchPageData.getResults();
				products.forEach(product -> product.setFavorite(true));
				searchPageData.setResults(extCustomerFacade.getMyFavProductOrderByDate(products));
				model.addAttribute(MY_FAV_PRODUCTS, searchPageData);

			}
			else
			{
				model.addAttribute(MY_FAV_PRODUCTS, searchPageData);
			}
			extCustomerFacade.updateMyFavorite(favProducts);
		}
		catch (final Exception exception)
		{
			LOGGER.error("Error occurred :", exception);
			setAccountPageNotificationUpdateKey(model, MY_FAVORITES_MESSAGE_VALUE_KEY);
		}
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(MY_FAVORITES_TITLE_TEXT));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_FAVORITES_CMS_PAGE));
		myFavortiesPageSetUp(model);
		return getViewForPage(model);
	}



	/**
	 * Controller for adding product to My favorite for the user.
	 *
	 * @param code
	 *           the product code
	 * @param name
	 *           the product name
	 * @return String
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/addToMyFavorite", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addToMyFavorite(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.addToMyFavorite(code, (name == null ? code : name));
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, ADD);
	}

	/**
	 * Controller for removing product from My favorite for the user.
	 *
	 * @param code
	 *           the product code
	 * @param name
	 *           the product name
	 * @return pagename the String object.
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/removeFromMyFavorite", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeFromMyFavorite(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.removeFromMyFavorite(code, name);
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, REMOVE);
	}

	/**
	 * Method to edit the user's address.
	 *
	 * @param extAddressData
	 *
	 * @return accountData
	 */
	private AccountData editAddress(final ExtAddressData extAddressData)
	{
		final AccountData accountData = new AccountData();
		String addEditAddressSuccessFailureMessageKey = StringUtils.EMPTY;
		try
		{
			final boolean success = userFacade.editUserAddress(extAddressData);
			addEditAddressSuccessFailureMessageKey = ADDREES_EDITED_SUCCESS_MESSAGE_VALUE_KEY;
			if (!success)
			{
				addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_EDIT_ADDRESS_ERROR_CODE
						+ SimonCoreConstants.INTEGRATION_ERROR_CODE_400 + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			}
		}
		catch (final ModelSavingException modelSavingException)
		{
			LOGGER.error("ModelSavingException occurred while editing the address", modelSavingException);
			addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_EDIT_ADDRESS_ERROR_CODE
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_400 + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			accountData.setFailure(addEditAddressSuccessFailureMessageKey);
		}
		catch (final UserAddressIntegrationException ex)
		{
			LOGGER.error("Exception occurred while editing the address", ex);
			addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_EDIT_ADDRESS_ERROR_CODE + ex.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			accountData.setFailure(addEditAddressSuccessFailureMessageKey);
		}

		sessionService.setAttribute(ADD_EDIT_ADDRESS_SUCCESS_FAILURE_MESSAGE_KEY, addEditAddressSuccessFailureMessageKey);
		return accountData;
	}

	/**
	 * Method to add the user's address.
	 *
	 * @param extAddressData
	 *
	 * @return accountData
	 */
	private AccountData addAddress(final ExtAddressData extAddressData)
	{
		final AccountData accountData = new AccountData();
		String addEditAddressSuccessFailureMessageKey = StringUtils.EMPTY;
		try
		{
			final boolean success = userFacade.addUserAddress(extAddressData);

			addEditAddressSuccessFailureMessageKey = ADDREES_ADDED_SUCCESS_MESSAGE_VALUE_KEY;
			if (!success)
			{
				addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_ADD_ADDRESS_ERROR_CODE
						+ SimonCoreConstants.INTEGRATION_ERROR_CODE_400 + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			}
		}

		catch (final ModelSavingException modelSavingException)
		{
			LOGGER.error("ModelSavingException occurred while editing the address", modelSavingException);
			addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_ADD_ADDRESS_ERROR_CODE
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_400 + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			accountData.setFailure(addEditAddressSuccessFailureMessageKey);
		}
		catch (final UserAddressIntegrationException ex)
		{
			LOGGER.error("UserAddressIntegrationException occurred while editing the address", ex);
			addEditAddressSuccessFailureMessageKey = SimonCoreConstants.INTEGRATION_USER_ADD_ADDRESS_ERROR_CODE + ex.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			accountData.setFailure(addEditAddressSuccessFailureMessageKey);
		}

		sessionService.setAttribute(ADD_EDIT_ADDRESS_SUCCESS_FAILURE_MESSAGE_KEY, addEditAddressSuccessFailureMessageKey);
		return accountData;
	}

	/**
	 * Populates the profile form with customerdata
	 *
	 * @param customerData
	 * @param updateProfileForm
	 */
	private void populateAccountUpdateForm(final CustomerData customerData, final AccountUpdateForm updateProfileForm)
	{
		updateProfileForm.setFirstName(customerData.getFirstName());
		updateProfileForm.setLastName(customerData.getLastName());
		updateProfileForm.setEmail(customerData.getDisplayUid());
		updateProfileForm.setZipCode(customerData.getZipcode());
		updateProfileForm.setCountry(customerData.getCountry());
		if (null != customerData.getBirthMonth())
		{
			updateProfileForm.setBirthMonth(customerData.getBirthMonth().getCode());
		}
		updateProfileForm.setBirthYear(customerData.getBirthYear());
		updateProfileForm.setGender(customerData.getGender().getCode());
		if (null != customerData.getPrimaryMall())
		{
			updateProfileForm.setPrimaryMall(customerData.getPrimaryMall().getCode());
		}
	}

	/**
	 * Method to get listOfCustomerAlternateMallsIds.
	 *
	 * @param customerData
	 *
	 * @return listOfString
	 *
	 */
	private List<String> getListOfCustomerAlternateMallsIds(final CustomerData customerData)
	{
		final List<String> alternateMallIdList = new ArrayList<>();
		if (customerData.getAlternateMalls() != null)
		{

			for (final MallData mallData : customerData.getAlternateMalls())
			{
				alternateMallIdList.add(mallData.getCode());
			}
		}
		return alternateMallIdList;
	}

	/**
	 * This method is called for displaying saved designer of the profile.
	 *
	 * @param model
	 * @return pagename the String object.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + MY_DESIGNERS_CMS_PAGE_LABEL, method = RequestMethod.GET)
	@RequireHardLogIn
	public String showDesigners(final Model model) throws CMSItemNotFoundException
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final List<DesignerData> designerDatas = new ArrayList<>();
		try
		{
			final boolean status = extCustomerFacade.getAllFavouriteDesigners(designerDatas);
			if (!status)
			{
				setAccountPageNotificationUpdateKey(model, MY_DESIGNERS_MESSAGE_VALUE_KEY);
			}
		}
		catch (final DuplicateUidException duplicateUidException)
		{
			LOGGER.error("designer Update Failed while fetching and saving designerData: ", duplicateUidException);
		}
		if (CollectionUtils.isNotEmpty(currentCustomerData.getMyDesigners()) && CollectionUtils.isNotEmpty(designerDatas))
		{
			model.addAttribute("designerList", currentCustomerData.getMyDesigners());
			model.addAttribute("activeDesignerList", CommonUtils.getDesignerOrderByName(designerDatas));
			model.addAttribute("addDesigners", TEXT_ACCOUNT_MYDESIGNERS_ADDMOREBUTTON);
		}
		else
		{
			model.addAttribute("noSavedDesignerTitle", TEXT_ACCOUNT_MYDESIGNERS_NOSAVEDTITLE);
			model.addAttribute("noSavedDesignerData", TEXT_ACCOUNT_MYDESIGNERS_NOSAVEDDATA);
			model.addAttribute(ACCOUNT_DETAILS_USERNAME_KEY, currentCustomerData.getFirstName());
			model.addAttribute("addDesigners", TEXT_ACCOUNT_MYDESIGNERS_ADDBUTTON);
		}
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, TEXT_ACCOUNT_MYDESIGNERS_TITLE);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, TEXT_ACCOUNT_MYDESIGNERS_HOTOADD);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_DESIGNERS_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_DESIGNERS_CMS_PAGE_LABEL));
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_DESIGNERS_CMS_PAGE_LABEL);
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, MY_DESIGNERS_CMS_PAGE_LABEL);
		model.addAttribute(TEXT_ACCOUNT_CMS1_TITLE_LABEL, TEXT_ACCOUNT_FEATUREDDESIGNERS_TITLE);
		model.addAttribute(PAGEID, MY_DESIGNERS_CMS_PAGE_LABEL);
		model.addAttribute(PAGETYPE, MY_DESIGNERS_CMS_PAGE_LABEL);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		getGlobalMessage(model);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_MYDESIGNERS_TITLE));
		return getViewForPage(model);
	}

	/**
	 * This method is called for displaying saved deals of the profile.
	 *
	 * @param model
	 * @return pagename the String object.
	 * @throws CMSItemNotFoundException
	 * @throws DuplicateUidException
	 */
	@RequestMapping(value = SLASH + MY_DEALS_CMS_PAGE_LABEL, method = RequestMethod.GET)
	@RequireHardLogIn
	public String showDeals(final Model model) throws CMSItemNotFoundException, DuplicateUidException
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final List<DealData> dealDatas = new ArrayList<>();
		try
		{
			final boolean status = extCustomerFacade.getAllFavoriteDeals(currentCustomerData, dealDatas);
			if (!status)
			{
				setAccountPageNotificationUpdateKey(model, MY_DEALS_MESSAGE_VALUE_KEY);
			}
		}
		catch (final DuplicateUidException duplicateUidException)
		{
			LOGGER.error("Deals Update Failed while fetching and saving dealData: ", duplicateUidException);
		}
		if (CollectionUtils.isNotEmpty(currentCustomerData.getMyDeals()) && CollectionUtils.isNotEmpty(dealDatas))
		{
			final List<String> dealIds = new ArrayList<>();
			for (final DealData dealData : dealDatas)
			{
				dealIds.add(dealData.getCode());
			}
			extCustomerFacade.updateFavDeals(currentCustomerData, dealDatas);
			model.addAttribute("dealsList", dealDatas);
			model.addAttribute("viewDeals", TEXT_ACCOUNT_MYDEALS_ADDBUTTON);
		}
		else
		{
			model.addAttribute("noSavedDealTitle", TEXT_ACCOUNT_MYDEALS_NOSAVEDTITLE);
			model.addAttribute("noSavedDealData", TEXT_ACCOUNT_MYDEALS_NOSAVEDDATA);
			model.addAttribute(ACCOUNT_DETAILS_USERNAME_KEY, currentCustomerData.getFirstName());
			model.addAttribute("viewDeals", TEXT_ACCOUNT_MYDEALS_VIEWBUTTON);
		}
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, TEXT_ACCOUNT_MYDEALS_TITLE);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, TEXT_ACCOUNT_MYDEALS_HOTOADD);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_DEALS_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_DEALS_CMS_PAGE_LABEL));
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_DEALS_CMS_PAGE_LABEL);
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, MY_DEALS_CMS_PAGE_LABEL);
		model.addAttribute(PAGEID, MY_DEALS_CMS_PAGE_LABEL);
		model.addAttribute(PAGETYPE, MY_DEALS_CMS_PAGE_LABEL);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		getGlobalMessage(model);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_MYDEALS_TITLE));
		return getViewForPage(model);
	}

	/**
	 * This method is used set success or error messages into model.
	 *
	 * @param model
	 */
	private void getGlobalMessage(final Model model)
	{
		final List confMessageList = (List) model.asMap().get(ACC_CONF_MSGS);
		if (CollectionUtils.isNotEmpty(confMessageList))
		{
			final GlobalMessage message = (GlobalMessage) confMessageList.get(0);
			if (message != null)
			{
				setAccountPageNotificationUpdateKey(model, message.getCode());
			}

		}
	}

	/**
	 * This method to set accountPageNotificationUpdateKey to display success or error message.
	 *
	 * @param model
	 * @param messageKey
	 */
	private void setAccountPageNotificationUpdateKey(final Model model, final String messageKey)
	{
		model.addAttribute(ACCOUNT_PAGE_NOTIFICATION_UPDATE_KEY, messageKey);
		if (messageKey != null && (messageKey.contains("error") || messageKey.contains("fail")))
		{
			model.addAttribute(ACCOUNT_PAGE_NOTIFICATION_ERROR_KEY, "errorIcon");
		}
	}

	/**
	 * This method is called for saving selected designer of the profile.
	 *
	 * @param redirectModel
	 * @param request
	 *
	 * @return JSON String object.
	 */
	@ResponseBody
	@RequestMapping(value = "/add-multiDesigners", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addDesignersFromModal(final RedirectAttributes redirectModel, final HttpServletRequest request)
	{
		final String[] designerIds = request.getParameterValues(CHECKED_ROWS);
		final boolean addStatus = extCustomerFacade.updateDesignersToProfile(designerIds);
		if (addStatus)
		{
			GlobalMessages.addFlashMessage(redirectModel, ACC_CONF_MSGS, TEXT_ACCOUNT_MY_DESIGNERS_CONFIRMATION_UPDATED, null);
			return RETURN_RESPONSE_SUCCESS;

		}
		else
		{
			return RETURN_RESPONSE_FAILURE;
		}



	}

	/**
	 * This method will fetch all designer from the database.it will exclude the user saved designers from the list.
	 *
	 * @return pagename the String object.
	 */
	@RequestMapping(value = "/get-designers", method = RequestMethod.GET)
	@RequireHardLogIn
	@ResponseBody
	public DesignerDataList getAllDesigners()
	{
		final DesignerDataList designerDataList = new DesignerDataList();
		designerDataList.setAllDesignerList(extCustomerFacade.fetchAllDesignersExceptCustomerFavDesigners());
		return designerDataList;

	}

	/**
	 * @return the configurationService
	 */
	public ConfigurationService getConfigurationService()
	{
		return configurationService;
	}

	/**
	 * @return the extUpdateProfileValidator
	 */
	public ExtUpdateProfileValidator getExtUpdateProfileValidator()
	{
		return extUpdateProfileValidator;
	}

	/**
	 * @param extUpdateProfileValidator
	 *           the extUpdateProfileValidator to set
	 */
	public void setExtUpdateProfileValidator(final ExtUpdateProfileValidator extUpdateProfileValidator)
	{
		this.extUpdateProfileValidator = extUpdateProfileValidator;
	}


	/**
	 * Method to view payment data
	 *
	 * @param success
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + PAYMENT_DETAILS_CMS_PAGE, method = RequestMethod.GET)
	@RequireHardLogIn
	public String viewPaymentDetails(@RequestParam(value = "success", required = false) final String success, final Model model)
			throws CMSItemNotFoundException
	{
		final List<AddressData> billingAddressList = userFacade.getBillingAddressList(Boolean.TRUE);
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_DIV_CLASS_KEY, PAYMENT_PAGE_SPECIFIC_DIV_CLASS_VALUE);
		model.addAttribute("billingAddressList", billingAddressList);
		model.addAttribute(STATE_DATA_LIST, extCheckoutFacade.getAllStates(model));
		model.addAttribute("paymentInfoData", userFacade.getCCPaymentInfos(true));
		model.addAttribute("spreedlyEnvKey", configurationService.getConfiguration().getString(SimonWebConstants.SPREEDLY_ENV_KEY));
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, PAYMENT_DETAILS_SPECIFIC_HEADER_MESSAGE_VALUE_KEY);
		storeCmsPageInModel(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAYMENT_DETAILS_CMS_PAGE));
		model.addAttribute(BREADCRUMBS_ATTR,
				accountBreadcrumbBuilder.getBreadcrumbs(PAYMENT_DETAILS_SPECIFIC_HEADER_MESSAGE_VALUE_KEY));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(PAGEID, PAYMENT_DETAILS_CMS_PAGE);
		model.addAttribute(CMS_PARAGRAPH_CLASS, PAYMENT_DETAILS_CMS_PARAGRAPH_CSS);
		model.addAttribute(PAGETYPE, PAYMENT_DETAILS_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		model.addAttribute(ENABLE_SPREEDLY_CALLS, !configurationService.getConfiguration().getBoolean(STUB_SPREEDLY_CALLS));
		setNotificationKey(success, model);
		return getViewForPage(model);
	}


	/**
	 * Sets the notification key.
	 *
	 * @param success
	 *           the success
	 * @param model
	 *           the model
	 */
	private void setNotificationKey(final String success, final Model model)
	{
		if (Boolean.TRUE.toString().equalsIgnoreCase(success))
		{
			model.addAttribute(ACCOUNT_PAGE_NOTIFICATION_UPDATE_KEY, PAYMENT_METHOD_ADDED_SUCCESS_MESSAGE_VALUE_KEY);
		}
		else if (Boolean.FALSE.toString().equalsIgnoreCase(success))
		{
			final String setPaymentErrorMessage = sessionService.getAttribute(SET_PAYMENT_ERROR_MESSAGE);
			if (null != setPaymentErrorMessage)
			{
				setAccountPageNotificationUpdateKey(model, setPaymentErrorMessage);
				sessionService.removeAttribute(SET_PAYMENT_ERROR_MESSAGE);
			}
		}
		getGlobalMessage(model);
	}

	/**
	 * method to add new payment data
	 *
	 * @param paymentInfoForm
	 * @return String
	 */
	@RequestMapping(value = "/payment-details", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public String setPaymentDetails(@ModelAttribute("paymentInfoForm") final PaymentInfoForm paymentInfoForm)
	{

		String setPaymentErrorMessage = StringUtils.EMPTY;
		boolean success = false;

		if (paymentInfoForm.getSecurityCode() != null)
		{
			try
			{
				extCheckoutFacade.verifyToken(paymentInfoForm.getSecurityCode(), true, false);
				success = userFacade.setNewPaymentInfo(setPaymentInfoAddress(paymentInfoForm));
			}
			catch (final CartCheckOutIntegrationException e)
			{
				LOGGER.error(e.getMessage(), e);
				setPaymentErrorMessage = SimonCoreConstants.INTEGRATION_USER_ADDRESS_PAYMENT_ERROR_CODE + e.getCode()
						+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			}
			catch (final ModelSavingException exception)
			{
				LOGGER.error(exception.getMessage(), exception);
			}
			catch (final UserAddressIntegrationException ex)
			{
				LOGGER.error(ex.getMessage(), ex);
				setPaymentErrorMessage = SimonCoreConstants.INTEGRATION_USER_ADDRESS_PAYMENT_ERROR_CODE + ex.getCode()
						+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			}
		}

		if (success)
		{
			return RETURN_RESPONSE_SUCCESS_PAYMENT;
		}
		else
		{
			if (paymentInfoForm.getSecurityCode() != null)
			{
				try
				{
					userFacade.callRemovePaymentMethodToken(paymentInfoForm.getSecurityCode());
				}
				catch (final CartCheckOutIntegrationException e)
				{
					LOGGER.error(e.getMessage(), e);
					setPaymentErrorMessage = SimonCoreConstants.INTEGRATION_USER_REMOVE_ADDRESS_ERROR_CODE + e.getCode()
							+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
					sessionService.setAttribute(SET_PAYMENT_ERROR_MESSAGE, setPaymentErrorMessage);
					return RETURN_RESPONSE_FAILURE_PAYMENT;
				}
			}
			sessionService.setAttribute(SET_PAYMENT_ERROR_MESSAGE, setPaymentErrorMessage);
			return RETURN_RESPONSE_FAILURE_PAYMENT;
		}
	}

	/**
	 * @param paymentInfoForm
	 * @return
	 */
	private CCPaymentInfoData setPaymentInfoAddress(final PaymentInfoForm paymentInfoForm)
	{
		final CCPaymentInfoData extPaymentInfoData = fillPaymentInfoData(paymentInfoForm);
		final AddressData addressData = setAddress(paymentInfoForm);
		extPaymentInfoData.setBillingAddress(addressData);
		return extPaymentInfoData;
	}

	/**
	 * populates the address.
	 *
	 * @param paymentInfoForm
	 *           the payment info form
	 * @return the address
	 */
	private AddressData setAddress(final PaymentInfoForm paymentInfoForm)
	{
		AddressData addressData;
		if (StringUtils.isNotEmpty(paymentInfoForm.getBillingAddId()))
		{
			addressData = userFacade.getAddress(paymentInfoForm.getBillingAddId());
		}
		else
		{
			addressData = convertToAddressData(paymentInfoForm);
		}
		addressData.setBillingAddress(Boolean.TRUE);
		addressData.setShippingAddress(Boolean.FALSE);
		return addressData;
	}

	/**
	 * Convert to address data.
	 *
	 * @param addressForm
	 *           the address form
	 * @return the address data
	 */
	private AddressData convertToAddressData(final PaymentInfoForm paymentInfoForm)
	{
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final AddressData addressData = new AddressData();
		addressData.setFirstName(currentCustomerData.getFirstName());
		addressData.setLastName(currentCustomerData.getLastName());
		addressData.setLine1(paymentInfoForm.getLine1());
		addressData.setLine2(paymentInfoForm.getLine2());
		addressData.setTown(paymentInfoForm.getTown());
		addressData.setPostalCode(paymentInfoForm.getPostalCode());
		final CountryData countryData = getI18NFacade().getCountryForIsocode(US_ISO);
		addressData.setCountry(countryData);
		final RegionData regionData = getI18NFacade().getRegion(US_ISO, paymentInfoForm.getState());
		addressData.setRegion(regionData);

		return addressData;

	}

	/**
	 * populates info data.
	 *
	 * @param paymentInfoForm
	 *           the payment info form
	 * @return the CC payment info data
	 */
	private CCPaymentInfoData fillPaymentInfoData(final PaymentInfoForm paymentInfoForm)
	{
		final CCPaymentInfoData extPaymentInfoData = new CCPaymentInfoData();
		extPaymentInfoData.setPaymentToken(paymentInfoForm.getSecurityCode());
		extPaymentInfoData.setAccountHolderName(paymentInfoForm.getNameOnCard());
		extPaymentInfoData.setCardNumber(paymentInfoForm.getCardNumber());
		extPaymentInfoData.setLastFourDigits(paymentInfoForm.getLastFourDigits());
		extPaymentInfoData.setCardType(paymentInfoForm.getCardType());
		extPaymentInfoData.setExpiryMonth(paymentInfoForm.getExpirationMonth());
		extPaymentInfoData.setExpiryYear(paymentInfoForm.getExpirationYear());
		extPaymentInfoData.setSaveInAccount(Boolean.TRUE);
		extPaymentInfoData.setDefaultPaymentInfo(paymentInfoForm.isDefaultPayment());
		return extPaymentInfoData;
	}

	/**
	 * favorite page data.
	 *
	 * @param model
	 */
	private void myFavortiesPageSetUp(final Model model)
	{
		model.addAttribute(ACCOUNT_DETAILS_USERNAME_KEY, extCustomerFacade.getCurrentCustomer().getFirstName());
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, MY_FAVORITES_TITLE_TEXT);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, "myFavorites.how.to.add.text");
		model.addAttribute(ACCOUNT_DETAILS_NO_SAVED_DATA_KEY, "myFavorites.no.saved.Data.text");
		model.addAttribute(ACCOUNT_DETAILS_MY_FAVORITES_DETAILS, "myFavorites.details.text");
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_FAVORITES_CMS_PAGE);
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, MY_FAVORITES_CMS_PAGE);
		model.addAttribute(PAGEID, MY_FAVORITES_CMS_PAGE);
		model.addAttribute(PAGETYPE, MY_FAVORITES_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		model.addAttribute(TEXT_ACCOUNT_CMS1_TITLE_LABEL, TEXT_ACCOUNT_TRENDINGNOW_TITLE);
	}


	protected ProductSearchPageData<SearchStateData, ProductData> encodeSearchPageData(
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		final SearchStateData currentQuery = searchPageData.getCurrentQuery();

		if (currentQuery != null)
		{
			try
			{
				final SearchQueryData query = currentQuery.getQuery();
				final String encodedQueryValue = XSSEncoder.encodeHTML(query.getValue());
				query.setValue(encodedQueryValue);
				currentQuery.setQuery(query);
				searchPageData.setCurrentQuery(currentQuery);
				searchPageData.setFreeTextSearch(XSSEncoder.encodeHTML(searchPageData.getFreeTextSearch()));
			}
			catch (final UnsupportedEncodingException unsupportedEncodingException)
			{
				LOGGER.debug("Error occured during Encoding the Search Page data values", unsupportedEncodingException);
			}
		}
		return searchPageData;
	}

	/**
	 * Controller for removing designer from My designers for the user.
	 *
	 * @param code
	 *           the designer code
	 * @param name
	 *           the designer name
	 * @return pagename the String object.
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/remove-designer", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeFromMyDesigners(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.removeFromMyDesigners(code);
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, REMOVE);
	}

	/**
	 * Controller for adding designer from My designers for the user.
	 *
	 * @param code
	 *           the designer code
	 * @param name
	 *           the designer name
	 * @return pagename the String object.
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/add-designer", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addFavoriteMyDesigners(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.addToMyDesigners(code, name);
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, ADD);
	}

	/**
	 * Controller for removing store from My store for the user.
	 *
	 * @param code
	 *           the shop code
	 * @param name
	 *           the shop name
	 * @return pagename the String object.
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/remove-store", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeFromMyStores(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.removeFromMyStores(code);
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, REMOVE);
	}

	/**
	 * Controller for removing designer from My designers for the user.
	 *
	 * @param code
	 *           the designer code
	 * @param name
	 *           the designer name
	 * @return pagename the String object.
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/add-store", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addFavoriteMyStores(final String code, final String name) throws JSONException
	{
		final boolean status = extCustomerFacade.addToMyStores(code, name);
		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, ADD);
	}

	@Override
	@RequestMapping(value = "/remove-payment-method", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removePaymentMethod(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		String removePaymentErrorMessage = StringUtils.EMPTY;
		boolean status = false;

		try
		{
			userFacade.removePaymentMethodToken(paymentMethodId);
		}
		catch (final CartCheckOutIntegrationException | UserAddressIntegrationException e)
		{
			LOGGER.error(e.getMessage(), e);
			removePaymentErrorMessage = SimonCoreConstants.INTEGRATION_USER_REMOVE_ADDRESS_ERROR_CODE + e.getMessage()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
			return redirectRemovePaymentError(redirectAttributes, removePaymentErrorMessage);
		}

		final AddressModel paymentInfoAddressModel = userFacade.getCCPaymentInfoAddressModel(paymentMethodId);

		if (paymentInfoAddressModel == null)
		{
			return redirectRemovePaymentError(redirectAttributes, "payment.address.method.not.found");
		}

		try
		{
			status = userFacade.removeCCPaymentInfoUserAddress(paymentInfoAddressModel);
		}
		catch (final UserAddressIntegrationException e)
		{
			status = false;
			LOGGER.error("Error occured during remove the address payment {} ", e);
			removePaymentErrorMessage = SimonCoreConstants.INTEGRATION_USER_REMOVE_ADDRESS_ERROR_CODE + e.getCode()
					+ SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE;
		}
		if (status)
		{
			return super.removePaymentMethod(paymentMethodId, redirectAttributes);
		}
		else
		{
			return redirectRemovePaymentError(redirectAttributes, removePaymentErrorMessage);
		}
	}

	/**
	 * This method searches for the orders placed by the user and will return the search result either No-Orders or
	 * Order-List
	 *
	 * @param page
	 *           This attribute is used to pass the page number to be displayed from the {@link PageableData}
	 * @param showMode
	 *           This attribute defines whether the display will be in the form of pagination or the complete search
	 *           results will be displayed at once
	 * @param sortCode
	 *           This attribute defines the default sort mode for the sorting mechanism
	 * @param model
	 *           This attribute will hold all the relevant data to be displayed on the page
	 * @return String Will return the page URL
	 */
	@Override
	@RequestMapping(value = "/orders", method = RequestMethod.GET)
	@RequireHardLogIn
	public String orders(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
		final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
		if (CollectionUtils.isNotEmpty(searchPageData.getResults()))
		{
			populateModel(model, searchPageData, showMode);
			storeCmsPageInModel(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ORDER_HISTORY_CMS_PAGE));
		}
		else
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_ORDER_HISTORY_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(NO_ORDER_HISTORY_CMS_PAGE));
		}
		model.addAttribute(PAGEID, ORDER_HISTORY_CMS_PAGE);
		model.addAttribute(PAGETYPE, ORDER_HISTORY_CMS_PAGE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("text.account.orderHistory"));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, MY_ORDERS_PAGE_SPECIFIC_HEADER_MESSAGE_VALUE_KEY);
		return getViewForPage(model);
	}

	/**
	 * This method will search for the order details depending upon the order code passed to it and will return the JSON
	 * for the order
	 *
	 * @param orderCode
	 *           This attribute holds the value for the order number for which the order details are to be fetched
	 * @param model
	 *           This attribute will hold all the relevant data to be displayed on the page
	 * @param redirectModel
	 *
	 * @return String
	 */
	@Override
	@RequestMapping(value = ORDER_DETAIL_CMS_PAGE + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String order(@PathVariable("orderCode") final String orderCode, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final OrderData orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
		model.addAttribute("orderDetails", orderDetails);
		return "pages/account/orderDetails";
	}

	/**
	 * This method supports the functionality of Order History page and returns the page with the Order Lists for the
	 * particular user along with the pagination
	 *
	 * @param page
	 *           This attribute is used to pass the page number to be displayed from the {@link PageableData}
	 * @param showMode
	 *           This attribute defines whether the display will be in the form of pagination or the complete search
	 *           results will be displayed at once
	 * @param sortCode
	 *           This attribute defines the default sort mode for the sorting mechanism
	 * @param model
	 *           This attribute will hold all the relevant data to be displayed on the page
	 * @return SearchPageData<OrderHistoryData>
	 */
	@RequireHardLogIn
	@RequestMapping(value = "/get-orders", method = RequestMethod.GET)
	public String getOrderHistoryData(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model)
	{
		final PageableData pageableData = createPageableData(page, 5, sortCode, showMode);
		final SearchPageData<OrderHistoryData> searchPageData = orderFacade.getPagedOrderHistoryForStatuses(pageableData);
		populateModel(model, searchPageData, showMode);
		return "pages/account/accountOrderList";
	}

	/**
	 * @param redirectAttributes
	 * @return
	 */
	private String redirectRemovePaymentError(final RedirectAttributes redirectAttributes, final String removePaymentErrorMessage)
	{
		if (StringUtils.isNotEmpty(removePaymentErrorMessage))
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER, removePaymentErrorMessage);
		}
		return REDIRECT_TO_PAYMENT_INFO_PAGE;
	}

	/**
	 * controller for premium outlets page
	 *
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + PREMIUM_OUTLETS + "*", method = RequestMethod.GET)
	public String showPremiumOutlets(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(PREMIUM_OUTLETS));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PREMIUM_OUTLETS));
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, HOW_TO_ADD_FAVORITES);
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, MY_PREMIUM_OUTLETS);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_FAVORITES_CMS_PAGE);
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, PREMIUM_OUTLETS);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(MY_PREMIUM_OUTLET));
		model.addAttribute(PAGEID, PREMIUM_OUTLETS);
		model.addAttribute(PAGETYPE, PREMIUM_OUTLETS);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return getViewForPage(model);

	}

	/**
	 * controller for my centers page
	 *
	 * @param model
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + MY_CENTER, method = RequestMethod.GET)
	public String showMyCenter(final Model model) throws CMSItemNotFoundException
	{
		String currentUserPrimaryCenter = StringUtils.EMPTY;
		final MallData primaryMallData = extCustomerFacade.getCustomerPrimaryMallData();
		if (null != primaryMallData && StringUtils.isNotEmpty(primaryMallData.getName()))
		{
			currentUserPrimaryCenter = primaryMallData.getName();
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_CENTER));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_CENTER));
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_KEY, HOW_TO_ADD_MY_CENTER);
		model.addAttribute(ACCOUNT_DETAILS_HOW_TO_ADD_CSS_KEY, MY_CENTER);
		model.addAttribute(ACCOUNT_DETAILS_TITLE_KEY, currentUserPrimaryCenter);
		model.addAttribute(ACCOUNT_DETAILS_PAGE_LEVEL_CSS_KEY, MY_CENTER);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(MY_CENTER_BREADCRUMB));
		model.addAttribute(PAGEID, MY_CENTER);
		model.addAttribute(PAGETYPE, MY_CENTER);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return getViewForPage(model);

	}

	/**
	 * @param model
	 * @return viewName
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + EMAIL_PREFERENCES, method = RequestMethod.GET)
	public String showEmailPreferences(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(EMAIL_PREFERENCES));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(EMAIL_PREFERENCES));
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, TEXT_ACCOUNT_EMAIL_PREFERENCES);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_EMAIL_PREFERENCES));
		model.addAttribute(PAGEID, EMAIL_PREFERENCES);
		model.addAttribute(PAGETYPE, EMAIL_PREFERENCES);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
		final boolean optedInEmail = currentCustomerData.isOptedInEmail();
		model.addAttribute(SimonCoreConstants.OPTED_IN_EMAIL, optedInEmail);
		getGlobalMessage(model);
		return getViewForPage(model);
	}

	/**
	 * @description Method use to update the email preferences / newsletter subscription in the hybris and po.com
	 * @method updateEmailPreference
	 * @param accountUpdateForm
	 * @param bindingResult
	 * @param model
	 * @param redirectAttributes
	 * @return returnAction
	 */
	@RequestMapping(value = SLASH + UPDATE_EMAIL_PREFERENCES, method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateEmailPreference(@ModelAttribute("updateUserForm") final AccountUpdateForm accountUpdateForm,
			final BindingResult bindingResult, final Model model, final RedirectAttributes redirectAttributes)
	{
		final String returnAction = REDIRECT_TO_EMAIL_PREFERENCES;
		String emailPreferencesUpdateMsgKey = EMAIL_PREFERENCES_OPTED_UPDATE_SUCCESS;
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, EMAIL_PREFERENCES_UPDATE_FAILED);
		}
		else
		{
			try
			{
				final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
				currentCustomerData.setOptedInEmail(accountUpdateForm.isOptedInEmail());
				extCustomerFacade.updateProfileInformation(currentCustomerData, true);

				if (accountUpdateForm.isOptedInEmail())
				{
					emailPreferencesUpdateMsgKey = EMAIL_PREFERENCES_UNOPTED_UPDATE_SUCCESS;
				}
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, emailPreferencesUpdateMsgKey);
			}
			catch (final UserIntegrationException userIntegrationException)
			{
				LOGGER.error("Error occured when updating the opted in email (email preferences) : ", userIntegrationException);
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, EMAIL_PREFERENCES_UPDATE_FAILED);
			}
			catch (final DuplicateUidException duplicateUidException)
			{
				LOGGER.error("Error occured when updateing customer data: ", duplicateUidException);
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, EMAIL_PREFERENCES_UPDATE_FAILED);
			}
		}

		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_EMAIL_PREFERENCES));
		return returnAction;
	}

	/**
	 * @param code
	 * @param name
	 * @return page name
	 * @throws JSONException
	 */

	@ResponseBody
	@RequestMapping(value = "/add-deal", method = RequestMethod.POST)
	@RequireHardLogIn
	public String addFavoriteDeal(final String code, final String name) throws JSONException
	{
		boolean status = false;

		if (StringUtils.isNotBlank(code) && StringUtils.isNotBlank(name))
		{
			status = extCustomerFacade.addFavDeal(code, name);
		}

		LOGGER.debug("ExtAccountPageController ::: addFavoriteDeal :: code : {} , name : {}, status : {} ", code, name, status);

		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, ADD);
	}


	/**
	 * @param code
	 * @param name
	 * @return page name
	 * @throws JSONException
	 */
	@ResponseBody
	@RequestMapping(value = "/remove-deal", method = RequestMethod.POST)
	@RequireHardLogIn
	public String removeFavoriteDeal(final String code, final String name) throws JSONException
	{
		boolean status = false;

		if (StringUtils.isNotBlank(code))
		{
			status = extCustomerFacade.removeFavDeal(code);
		}

		LOGGER.debug("ExtAccountPageController ::: removeFavoriteDeal :: code : {} , name : {}, status : {} ", code, name, status);

		return extCustomerFacade.createJsonForFavouriteUnFavourite(code, name, status, REMOVE);
	}


	/**
	 * @param model
	 * @return String View
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + MY_SIZE, method = RequestMethod.GET)
	@RequireHardLogIn
	public String showMySize(final Model model) throws CMSItemNotFoundException
	{

		try
		{
			final List<String> mySizeIds = extCustomerFacade.fetchMyFavSizesFromExtService();
			extCustomerFacade.updateMySize(mySizeIds);


			model.addAttribute(BASE_CATEGORY_NAME_TO_SUBCATEGORY_DATA_LIST_MAP,
					simonCategoryNavigationFacade.getCategoryforMySize());

			model.addAttribute(SUBCATEGORY_CODE_TO_SIZE_LIST_MAP, extCustomerFacade.getAllSizes());


			List<SizeData> mySizeDataList = new ArrayList<>();
			if (!CollectionUtils.isEmpty(mySizeIds))
			{
				mySizeDataList = extCustomerFacade.getMySizes(mySizeIds);
				model.addAttribute(MY_SIZES, mySizeDataList);

			}
			else
			{
				model.addAttribute(MY_SIZES, mySizeDataList);
			}
			extCustomerFacade.updateMySize(mySizeIds);

		}
		catch (final Exception exception)
		{
			LOGGER.error("Error occurred :", exception);
			setAccountPageNotificationUpdateKey(model, MY_SIZES_MESSAGE_VALUE_KEY);
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_SIZE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_SIZE));
		model.addAttribute(ACCOUNT_PAGE_SPECIFIC_HEADER_MESSAGE_KEY, TEXT_ACCOUNT_MY_SIZES);
		model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_MY_SIZES));
		model.addAttribute(PAGEID, MY_SIZE);
		model.addAttribute(PAGETYPE, MY_SIZE);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		getGlobalMessage(model);
		return getViewForPage(model);
	}

	/**
	 * Controller for adding size to My Size for the user.
	 *
	 * @param updateSizeForm
	 * @param redirectAttributes
	 * @return String
	 */
	@RequestMapping(value = "/updateMySize", method = RequestMethod.POST)
	@RequireHardLogIn
	public String updateMySize(@ModelAttribute("updateSizeForm") final UpdateSizeForm updateSizeForm,
			final RedirectAttributes redirectAttributes)
	{

		try
		{
			final boolean addStatus = extCustomerFacade.addToMySize(updateSizeForm.getOldCode(), updateSizeForm.getCode(),
					updateSizeForm.getName());

			if (addStatus)
			{
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, MY_SIZE_UPDATE_SUCCESS, null);
			}
			else
			{
				GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, MY_SIZE_UPDATE_ERROR, null);
			}
		}
		catch (final UserFavouritesIntegrationException ex)
		{
			LOGGER.error("Exception Occured while updating MY Size:", ex);

			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, MY_SIZE_UPDATE_ERROR, null);

		}

		return REDIRECT_TO_MY_SIZE;
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.NOINDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
