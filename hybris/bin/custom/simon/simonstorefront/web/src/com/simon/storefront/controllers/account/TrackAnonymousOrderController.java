package com.simon.storefront.controllers.account;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.forms.TrackOrderForm;
import com.simon.storefront.util.ExtWebUtils;


/**
 * This controller class is using for Order tracking when Guest User try to track any order from hybris database.
 */
@Controller
@RequestMapping("/customer-service")
public class TrackAnonymousOrderController extends AbstractPageController
{
	private static final Logger LOGGER = Logger.getLogger(TrackAnonymousOrderController.class);

	private static final String SLASH = "/";
	private static final String BREADCRUMBS_ATTR = "breadcrumbs";
	private static final String RESULT_STATUS = "STATUS";
	private static final String PAGEID = "pageId";
	private static final String PAGE_NAME = "track-order";
	private static final String TRACK_NO_RESULT_MESSAGE_PREFIX = "trackNoResultMessagePrefix";
	private static final String TRACK_NO_RESULT_MESSAGE_SUFFIX = "trackNoResultMessageSuffix";
	private static final String TRACK_NO_RESULT_MESSAGE_LOCALIZED_PREFIX_STRING = "message.track.order.no.result.prefix";
	private static final String TRACK_NO_RESULT_MESSAGE_LOCALIZED_SUFFIX_STRING = "message.track.order.no.result.suffix";
	private static final String ORDER_DETAIL_CMS_PAGE = "/track-order/";
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String REDIRECT_TO_ORDER_HISTORY_PAGE = REDIRECT_PREFIX + "/my-account/orders";

	@Resource
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource
	private ExtCheckoutFacade extCheckoutFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "extCustomMessageUtils")
	private ExtCustomMessageUtils extCustomMessageUtils;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	/**
	 * Method to get the Track Order CMS page.
	 *
	 * @param model
	 *
	 * @return String the view page name.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = SLASH + "track-order", method = RequestMethod.GET)
	public String getTrackOrderPage(final Model model) throws CMSItemNotFoundException
	{
		final Boolean redirectLoginTrackOrder = sessionService.getAttribute(SimonCoreConstants.REDIRECT_LOGIN_TRACK_ORDER);
		sessionService.removeAttribute(SimonCoreConstants.REDIRECT_LOGIN_TRACK_ORDER);

		if (null != redirectLoginTrackOrder && redirectLoginTrackOrder)
		{
			return REDIRECT_TO_ORDER_HISTORY_PAGE;
		}
		else
		{
			model.addAttribute("trackOrderForm", new TrackOrderForm());
			storeCmsPageInModel(model, getContentPageForLabelOrId(PAGE_NAME));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(PAGE_NAME));
			model.addAttribute(BREADCRUMBS_ATTR, accountBreadcrumbBuilder.getBreadcrumbs("Track Order"));

			return getViewForPage(model);
		}
	}


	/**
	 * Method is used to searching order into the Hybris database for guest user and user entered the Order Id and email
	 * Id
	 *
	 * @param trackOrderForm
	 *
	 * @param model
	 *
	 * @return Map which content the AbstractOrderData object.
	 *
	 */
	@ResponseBody
	@RequestMapping(value = SLASH + "order-details", method = RequestMethod.POST)
	public Map<String, Object> searchOrderForGuestUser(@ModelAttribute("trackOrderForm") final TrackOrderForm trackOrderForm,
			final Model model)
	{
		final Map<String, Object> trackMyOrderMap = new HashMap<>();
		OrderHistoryData orderHistoryData = null;

		if (StringUtils.isNotBlank(trackOrderForm.getOrderId()) && StringUtils.isNotBlank(trackOrderForm.getEmailId()))
		{
			orderHistoryData = extCheckoutFacade.getGuestOrderForTracking(trackOrderForm.getOrderId(), trackOrderForm.getEmailId());
		}

		if (orderHistoryData != null)
		{
			LOGGER.debug("Order Details for Guest User::Order Not Found when finding the order model in the database");
			trackMyOrderMap.put(RESULT_STATUS, "SUCCESS");
			trackMyOrderMap.put("TRACKORDER", orderHistoryData);
			trackMyOrderMap.put("messageLabels", extCustomMessageUtils.setTrackOrderMessages());
		}
		else
		{
			trackMyOrderMap.put(RESULT_STATUS, "ERROR");
			final String trackNoResultMessagePrefix = getMessageSource().getMessage(TRACK_NO_RESULT_MESSAGE_LOCALIZED_PREFIX_STRING,
					null, getI18nService().getCurrentLocale());
			final String trackNoResultMessageSuffix = getMessageSource().getMessage(TRACK_NO_RESULT_MESSAGE_LOCALIZED_SUFFIX_STRING,
					null, getI18nService().getCurrentLocale());
			trackMyOrderMap.put(TRACK_NO_RESULT_MESSAGE_PREFIX, trackNoResultMessagePrefix);
			trackMyOrderMap.put(TRACK_NO_RESULT_MESSAGE_SUFFIX, trackNoResultMessageSuffix);

		}
		trackMyOrderMap.put("OrderId", trackOrderForm.getOrderId());
		trackMyOrderMap.put("Email", trackOrderForm.getEmailId());
		model.addAttribute(PAGEID, PAGE_NAME);
		model.addAttribute("pageType", PAGE_NAME);
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);

		model.addAttribute("RESULT", trackMyOrderMap);
		return trackMyOrderMap;
	}

	/**
	 * This method will search for the order details depending upon the order code passed to it and will return the JSON
	 * for the order
	 *
	 * @param orderCode
	 *           This attribute holds the value for the order number for which the order details are to be fetched
	 * @param model
	 *           This attribute will hold all the relevant data to be displayed on the page
	 *
	 * @return String
	 */

	@RequestMapping(value = ORDER_DETAIL_CMS_PAGE + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String order(@PathVariable("orderCode") final String orderCode, final Model model) throws CMSItemNotFoundException
	{
		try
		{
			final OrderData orderDetails = orderFacade.getOrderDetailsForCodeWithoutUser(orderCode);
			model.addAttribute("orderDetails", orderDetails);
		}
		catch (final UnknownIdentifierException e)
		{
			LOGGER.error("Order not found for requested order code", e);
		}

		return "pages/account/orderDetails";
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.NOINDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
