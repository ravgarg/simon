package com.simon.storefront.controllers.cms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.cms.AbstractCMSComponentController;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.model.CustomShopsComponentModel;
import com.simon.facades.account.data.StoreDataList;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.customer.ExtShopFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.storefront.controllers.SimonControllerConstants;


/**
 * Controller for CMS CustomShopsComponent
 */
@Controller("CustomShopsComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CustomShopsComponent)
public class CustomShopsComponentController extends AbstractCMSComponentController<CustomShopsComponentModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomShopsComponentController.class);
	@Resource(name = "shopFacade")
	private ExtShopFacade shopFacade;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	/**
	 * prepares shops data list{@link StoreDataList} provided in the component{@link CustomShopsComponentModel}
	 *
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param model
	 *           of type {@link Model}
	 * @param component
	 *           of type {@link CustomShopsComponentModel}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CustomShopsComponentModel component)
	{
		final StoreDataList storeDataList = new StoreDataList();
		shopFacade.loadCustomShops(component, storeDataList);
		try
		{
			if (extCustomerFacade.checkIfUserIsLoggedIn())
			{
				final CustomerData currentCustomerData = extCustomerFacade.getCurrentCustomer();
				extCustomerFacade.getAllFavouriteStores(currentCustomerData);
				shopFacade.updateFavCustomShops(currentCustomerData, storeDataList);
				setIfSavedStoresFound(currentCustomerData, model);
			}
		}
		catch (final DuplicateUidException e)
		{
			LOGGER.error("Stores Update Failed while fetching and saving storeData: ", e);
		}
		model.addAttribute("storeList", storeDataList);
		model.addAttribute("alignment", component.getAlignment().getCode());
		model.addAttribute("pageType", "storedirectory");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
	}

	/**
	 * creates the view jsp for the component
	 *
	 * @component of type {@link CustomShopsComponentModel}
	 */
	@Override
	protected String getView(final CustomShopsComponentModel component)
	{
		return ControllerConstants.Views.Cms.ComponentPrefix + StringUtils.lowerCase(CustomShopsComponentModel._TYPECODE);
	}

	/**
	 * Sets the model attribute if saved stores found.
	 *
	 * @param currentCustomerData
	 *           the current customer data
	 * @param model
	 *           the model
	 */
	private void setIfSavedStoresFound(final CustomerData currentCustomerData, final Model model)
	{
		if (!CollectionUtils.isEmpty(currentCustomerData.getMyStores()))
		{
			model.addAttribute("savedFav", "true");
		}

	}
}
