/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.storefront.forms;

public class PlaceOrderForm
{
	private String spreedlyToken;
	private boolean termsCheck;
	private String securityCode;
	private String nameOnCard;
	private String cardType;
	private String lastFourDigits;
	private String cardNumber;
	private String expirationMonth;
	private String expirationYear;
	private boolean saveInAccount;
	private boolean useShippingAddressAsBilling;
	private String line1;
	private String line2;
	private String town;
	private String state;
	private String postalCode;
	private String paymentInfoId;
	private String billingAddId;
	private String defaultCardForNextTime;

	public String getLine1()
	{
		return line1;
	}

	public void setLine1(final String line1)
	{
		this.line1 = line1;
	}

	public String getLine2()
	{
		return line2;
	}

	public void setLine2(final String line2)
	{
		this.line2 = line2;
	}

	public String getTown()
	{
		return town;
	}

	public void setTown(final String town)
	{
		this.town = town;
	}

	public String getState()
	{
		return state;
	}

	public void setState(final String state)
	{
		this.state = state;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(final String postalCode)
	{
		this.postalCode = postalCode;
	}

	public boolean isSaveInAccount()
	{
		return saveInAccount;
	}

	public void setSaveInAccount(final boolean saveInAccount)
	{
		this.saveInAccount = saveInAccount;
	}



	public boolean isUseShippingAddressAsBilling()
	{
		return useShippingAddressAsBilling;
	}

	public void setUseShippingAddressAsBilling(final boolean useShippingAddressAsBilling)
	{
		this.useShippingAddressAsBilling = useShippingAddressAsBilling;
	}

	public String getExpirationMonth()
	{
		return expirationMonth;
	}

	public void setExpirationMonth(final String expirationMonth)
	{
		this.expirationMonth = expirationMonth;
	}

	public String getExpirationYear()
	{
		return expirationYear;
	}

	public void setExpirationYear(final String expirationYear)
	{
		this.expirationYear = expirationYear;
	}

	public String getCardType()
	{
		return cardType;
	}

	public void setCardType(final String cardType)
	{
		this.cardType = cardType;
	}

	public String getLastFourDigits()
	{
		return lastFourDigits;
	}

	public void setLastFourDigits(final String lastFourDigits)
	{
		this.lastFourDigits = lastFourDigits;
	}

	public String getCardNumber()
	{
		return cardNumber;
	}

	public void setCardNumber(final String cardNumber)
	{
		this.cardNumber = cardNumber;
	}

	public String getNameOnCard()
	{
		return nameOnCard;
	}

	public void setNameOnCard(final String nameOnCard)
	{
		this.nameOnCard = nameOnCard;
	}

	public String getSpreedlyToken()
	{
		return spreedlyToken;
	}

	public void setSpreedlyToken(final String spreedlyToken)
	{
		this.spreedlyToken = spreedlyToken;
	}

	public String getSecurityCode()
	{
		return securityCode;
	}

	public void setSecurityCode(final String securityCode)
	{
		this.securityCode = securityCode;
	}

	public boolean isTermsCheck()
	{
		return termsCheck;
	}

	public void setTermsCheck(final boolean termsCheck)
	{
		this.termsCheck = termsCheck;
	}

	public String getPaymentInfoId()
	{
		return paymentInfoId;
	}

	public void setPaymentInfoId(final String paymentInfoId)
	{
		this.paymentInfoId = paymentInfoId;
	}

	public String getBillingAddId()
	{
		return billingAddId;
	}

	public void setBillingAddId(final String billingAddId)
	{
		this.billingAddId = billingAddId;
	}

	public String getDefaultCardForNextTime()
	{
		return defaultCardForNextTime;
	}

	public void setDefaultCardForNextTime(final String defaultCardForNextTime)
	{
		this.defaultCardForNextTime = defaultCardForNextTime;
	}


}
