package com.simon.storefront.tags;



import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.simon.core.exceptions.SystemException;
import com.simon.facades.message.CustomMessageFacade;


/**
 * This class is used to get the messageText for the given code{@link SimonMessageModel}. If the messageText for the
 * given code is not available in db, the code itself is returned.
 */
public class MessageTag extends TagSupport
{

	private static final long serialVersionUID = 1L;
	private static final String ERROR_MSG = "Unable to render message text for message code : ";
	private static final Logger LOG = LoggerFactory.getLogger(MessageTag.class);

	private String code;

	private Object arguments;

	private String var;

	private String text;

	@Override
	public int doStartTag() throws JspException
	{
		if (code == null)
		{
			code = "NONE";
		}
		final WebApplicationContext appContext = WebApplicationContextUtils
				.getRequiredWebApplicationContext(pageContext.getServletContext());
		/** {CustomMessageFacade} to retrieve the messageText from message code. */
		final CustomMessageFacade messageFacade = (CustomMessageFacade) appContext.getBean("customMessageFacade");
		final JspWriter out = pageContext.getOut();
		Object[] argumentsArray = null;
		try
		{
			String message = messageFacade.getMessageTextForCode(code);
			if (!StringUtils.isEmpty(this.arguments))
			{
				argumentsArray = resolveArguments(this.arguments);
			}
			if (!(ObjectUtils.isEmpty(argumentsArray)))
			{
				final MessageFormat messageFormat = new MessageFormat(message);
				message = messageFormat.format(argumentsArray);
			}

			if (var != null)
			{
				pageContext.setAttribute(var, message);
			}
			else if (StringUtils.isEmpty(message))
			{
				out.print(code);
			}
			else
			{
				out.print(message);
			}

		}
		catch (final IOException exception)
		{
			LOG.error(ERROR_MSG + code, exception);
		}
		catch (final SystemException exception)
		{
			LOG.error(ERROR_MSG + code, exception);
			try
			{
				if (text != null)
				{
					out.print(text);
				}
				else
				{
					out.print(code);
				}
			}
			catch (final IOException e)
			{
				LOG.error(ERROR_MSG + code, e);
			}
		}
		return SKIP_BODY;
	}

	protected Object[] resolveArguments(final Object argumentsVal) throws JspException
	{
		Object[] objArray = null;
		if (argumentsVal instanceof String)
		{
			final String[] stringArray = StringUtils.delimitedListToStringArray((String) argumentsVal, ",");
			if (stringArray.length == 1)
			{
				final Object argument = stringArray[0];
				if ((argument != null) && (argument.getClass().isArray()))
				{
					return ObjectUtils.toObjectArray(argument);
				}
				objArray = new Object[]
				{ argumentsVal };
			}

			final Object[] argumentsArray = new Object[stringArray.length];
			int counter = 0;
			while (true)
			{
				argumentsArray[counter] = stringArray[counter];
				++counter;
				if (counter >= stringArray.length)
				{
					objArray = argumentsArray;
					break;
				}
			}
		}
		objArray = checkArgNature(argumentsVal, objArray);

		return objArray;
	}

	private Object[] checkArgNature(final Object argumentsVal, final Object[] objArray)
	{
		Object[] newArray = objArray;
		if (null != argumentsVal)
		{
			newArray = StringUtils.delimitedListToStringArray(String.valueOf(argumentsVal), ",");
		}
		if (argumentsVal instanceof Object[])
		{
			newArray = (Object[]) argumentsVal;
		}
		if (argumentsVal instanceof Collection)
		{
			newArray = ((Collection) argumentsVal).toArray();
		}
		return newArray;
	}

	@Override
	public int doEndTag() throws JspException
	{
		return EVAL_PAGE;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public void setArguments(final Object arguments)
	{
		this.arguments = arguments;
	}

	public void setVar(final String var)
	{
		this.var = var;
	}

	public void setText(final String text)
	{
		this.text = text;
	}


}
