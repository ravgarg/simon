package com.simon.storefront.filters.cms;

import de.hybris.platform.acceleratorcms.data.CmsPageRequestContextData;
import de.hybris.platform.acceleratorcms.services.CMSPageContextService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.generated.storefront.filters.cms.CMSSiteFilter;


/**
 * This ExtCMSSiteFilter class overrides the CMSSiteFilter.
 */
public class ExtCMSSiteFilter extends CMSSiteFilter
{
	private static final Logger log = LoggerFactory.getLogger(ExtCMSSiteFilter.class);
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "cmsPageContextService")
	private CMSPageContextService cmsPageContextService;
	private static final String COOKIE_NAME = "betaSession";
	private static final String BETA_LOGIN_URL = "/beta-login";
	private static final int MAX_AGE = -1;

	@Override
	protected void doFilterInternal(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
			final FilterChain filterChain) throws ServletException, IOException
	{
		final String requestURL = httpRequest.getRequestURL().toString();

		final CmsPageRequestContextData cmsPageRequestContextData = cmsPageContextService
				.initialiseCmsPageContextForRequest(httpRequest);

		// check whether exits valid preview data
		if (cmsPageRequestContextData.getPreviewData() == null)
		{
			// process normal request (i.e. normal browser non-cmscockpit request)
			if (processNormalRequest(httpRequest, httpResponse))
			{
				final boolean whiteListingFlag = configurationService.getConfiguration().getBoolean("simon.whitelisting.fallback");
				final String redirectUrl = configurationService.getConfiguration().getString("simon.whitelisting.beta.url");
				final String domainURL = (String) configurationService.getConfiguration().getProperty("website.simon.https");
				if (!whiteListingFlag)
				{
					// proceed filters normally
					filterChain.doFilter(httpRequest, httpResponse);
				}
				else if (redirectUrl.equals(httpRequest.getRequestURI()) || BETA_LOGIN_URL.equals(httpRequest.getRequestURI()))
				{
					log.info("redirectWhileURLContainsBeta else if block");
					redirectWhileURLContainsBeta(httpRequest, httpResponse, filterChain, MAX_AGE);
				}
				else
				{
					log.info("redirectionOnBasisOfBetaCookie else");
					final Cookie[] cookies = httpRequest.getCookies();
					if (null == cookies)
					{
						createNewCookieAndRedirect(httpRequest, httpResponse, MAX_AGE, redirectUrl, domainURL);
						return;
					}
					else
					{
						boolean betaCookieExist = false;
						for (final Cookie cookie : cookies)
						{
							betaCookieExist = redirectionOnBasisOfBetaCookie(httpRequest, httpResponse, filterChain, betaCookieExist,
									cookie, redirectUrl, domainURL);
						}
						if (!betaCookieExist)
						{
							createNewCookieAndRedirect(httpRequest, httpResponse, MAX_AGE, redirectUrl, domainURL);
							return;
						}
					}
				}
			}
		}
		else if (StringUtils.contains(requestURL, PREVIEW_TOKEN))
		{
			final String redirectURL = processPreviewRequest(httpRequest, cmsPageRequestContextData);

			// redirect to computed URL
			if (redirectURL.charAt(0) == '/')
			{
				final String contextPath = httpRequest.getContextPath();
				final String encodedRedirectUrl = httpResponse.encodeRedirectURL(contextPath + redirectURL);
				httpResponse.sendRedirect(encodedRedirectUrl);
			}
			else
			{
				final String encodedRedirectUrl = httpResponse.encodeRedirectURL(redirectURL);
				httpResponse.sendRedirect(encodedRedirectUrl);
			}

			// next filter in chain won't be invoked!!!
		}
		else
		{
			if (httpRequest.getSession().isNew())
			{
				processPreviewData(httpRequest, cmsPageRequestContextData.getPreviewData());
			}
			// proceed filters
			filterChain.doFilter(httpRequest, httpResponse);
		}
	}

	private boolean redirectionOnBasisOfBetaCookie(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
			final FilterChain filterChain, final boolean betaCookieExist, final Cookie cookie, final String redirectUrl,
			final String domainURL) throws IOException, ServletException
	{
		log.info("redirectionOnBasisOfBetaCookie start");
		boolean cookieExist = betaCookieExist;
		if (COOKIE_NAME.equals(cookie.getName()))
		{
			if ("yes".equals(cookie.getValue()))
			{
				filterChain.doFilter(httpRequest, httpResponse);
			}
			else if ("no".equals(cookie.getValue()))
			{
				httpRequest.getSession(false).invalidate();
				final String encodedRedirectUrl = httpResponse.encodeRedirectURL(domainURL + redirectUrl);
				httpResponse.sendRedirect(encodedRedirectUrl);
			}
			cookieExist = true;
		}
		return cookieExist;
	}

	private void redirectWhileURLContainsBeta(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
			final FilterChain filterChain, final int maxAge) throws IOException, ServletException
	{
		log.info("redirectWhileURLContainsBeta start");
		final Cookie[] cookies = httpRequest.getCookies();
		if (null == cookies)
		{
			createNewCookie(httpResponse, maxAge);
			filterChain.doFilter(httpRequest, httpResponse);
		}
		else
		{
			boolean betaCookieExist = false;
			for (final Cookie cookie : cookies)
			{
				if (COOKIE_NAME.equals(cookie.getName()))
				{
					betaCookieExist = true;
					break;
				}
			}
			if (!betaCookieExist)
			{
				createNewCookie(httpResponse, maxAge);
			}
			filterChain.doFilter(httpRequest, httpResponse);
		}
	}

	private void createNewCookieAndRedirect(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
			final int maxAge, final String redirectUrl, final String domainURL) throws IOException
	{
		log.info("createNewCookieAndRedirect start");
		createNewCookie(httpResponse, maxAge);
		httpRequest.getSession(false).invalidate();
		final String encodedRedirectUrl = httpResponse.encodeRedirectURL(domainURL + redirectUrl);
		log.info(encodedRedirectUrl);
		httpResponse.sendRedirect(encodedRedirectUrl);
	}

	private void createNewCookie(final HttpServletResponse httpResponse, final int maxAge)
	{
		final Cookie betaCookie = new Cookie(COOKIE_NAME, "no");
		betaCookie.setMaxAge(maxAge);
		betaCookie.setSecure(true);
		httpResponse.addCookie(betaCookie);
	}

}
