package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.PromotionResultData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.CustomerGender;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.enums.Months;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.generated.storefront.controllers.pages.CheckoutController;
import com.simon.integration.exceptions.AuthLoginIntegrationException;
import com.simon.integration.exceptions.UserIntegrationException;
import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.forms.ShopperRegistrationForm;
import com.simon.storefront.util.ExtWebUtils;


/**
 * This class extends CheckoutController and used for checkout functionality.
 */
@Controller
@RequestMapping(value = "/checkout")
public class ExtCheckoutController extends CheckoutController
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtCheckoutController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";

	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "order-confirmation";
	private static final String CONTINUE_URL_KEY = "continueUrl";


	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;

	@Resource(name = "extRegistrationValidator")
	private Validator extRegistrationValidator;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * This method is used to navigate to the order confirmation page for logged in user.
	 *
	 * @param orderCode
	 *           is the order code to place the order.
	 * @param model
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param redirectModel
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		sessionReset();
		return processOrderCode(orderCode, model, request, redirectModel);
	}

	/**
	 * @param formData
	 * @param bindingResult
	 * @return String
	 * @throws UserIntegrationException
	 */
	@ResponseBody
	@RequestMapping(value = "/create-account", method = RequestMethod.POST)
	public String createAccount(final ShopperRegistrationForm formData, final BindingResult bindingResult)
			throws UserIntegrationException
	{
		String status = "OC_SUCCESS_3";
		final String userPswd = sessionService.getAttribute("userPassword");
		if (StringUtils.isNotEmpty(userPswd))
		{
			formData.setPwd(userPswd);
		}
		sessionService.removeAttribute("userPassword");
		final RegisterData data = prepareShopperRegistrationData(formData);
		try
		{
			extCustomerFacade.registerGuestCustomer(data, formData.getOrdercode());
		}
		catch (final AuthLoginIntegrationException exception)
		{
			LOG.error("registration failed: ", exception);
			status = "OC_Error_1";
		}
		catch (final UserIntegrationException exception)
		{
			LOG.error("Error Message : {}",
					configurationService.getConfiguration().getString(SimonCoreConstants.INTEGRATION_USER_CENTER_ERROR_CODE
							+ exception.getCode() + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE));
			LOG.error("registration failed: ", exception);
			status = "OC_Error_1";
		}
		catch (final DuplicateUidException duplicateUidException)
		{
			LOG.error("Duplicate email registration failed: ", duplicateUidException);
			status = "OC_Error_2";
		}
		return status;
	}

	/**
	 * This method is used to prepare data from registration Form.
	 *
	 * @param formData
	 * @return
	 */
	private RegisterData prepareShopperRegistrationData(final ShopperRegistrationForm formData)
	{
		final RegisterData data = new RegisterData();
		data.setFirstName(formData.getFirstName());
		data.setLastName(formData.getLastName());
		data.setLogin(formData.getEmail());
		data.setGender(CustomerGender.NOT_DISCLOSED.getCode());
		data.setBirthYear(SimonWebConstants.BIRTH_YEAR);
		data.setBirthMonth(Months.JAN.getCode());
		data.setCountry(formData.getCountry());
		data.setZipcode(formData.getZip());
		data.setPassword(formData.getPwd());
		return data;
	}

	/**
	 * This method is used for order confirmation for Guest user.It validates the guest register validator form and
	 * process the request.
	 *
	 * @param form
	 *           is the guestRegisterForm which contains orderCode, password and its UID.
	 * @param bindingResult
	 *           of type {@link BindingResult}
	 * @param model
	 *           is the model attribute.
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param response
	 *           of type {@link HttpServletResponse}
	 * @param redirectModel
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String orderConfirmation(final GuestRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getGuestRegisterValidator().validate(form, bindingResult);
		return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel);
	}

	/**
	 * This method is to process the request form of guest user request form.
	 *
	 * @param form
	 *           is the guestRegisterForm which contains orderCode, password and its UID.
	 * @param bindingResult
	 *           of type {@link BindingResult}
	 * @param model
	 *           is the model attribute.
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param response
	 *           of type {@link HttpServletResponse}
	 * @param redirectModel
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	protected String processRegisterGuestUserRequest(final GuestRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return processOrderCode(form.getOrderCode(), model, request, redirectModel);
		}
		try
		{
			getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode());
			getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
			getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
		}
		catch (final DuplicateUidException e)
		{
			// User already exists
			LOG.warn("guest registration failed: [{}]", e);
			model.addAttribute(new GuestRegisterForm());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"guest.checkout.existingaccount.register.error", new Object[]
					{ form.getUid() });
			return REDIRECT_PREFIX + request.getHeader("Referer");
		}

		return REDIRECT_PREFIX + "/";
	}

	/**
	 * This method is used to process orderCode and then return the url string to land.
	 *
	 * @param orderCode
	 *           is the order code to place the order.
	 * @param model
	 *           is the model attribute.
	 * @param request
	 *           of type {@link HttpServletRequest}
	 * @param redirectModel
	 *           of type {@link RedirectAttributes}
	 * @return - a URL to the page to load.
	 *
	 * @throws CMSItemNotFoundException
	 */
	@Override
	protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		OrderData orderDetails;

		try
		{
			orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
		}
		catch (final UnknownIdentifierException exception)
		{
			LOG.warn("Attempted to load an order confirmation that does not exist or is not visible. Redirect to home page.");
			LOG.error("Exception" + exception);
			return REDIRECT_PREFIX + ROOT;
		}

		if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : orderDetails.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
				entry.setProduct(product);
			}
		}

		model.addAttribute("orderCode", orderCode);
		model.addAttribute("userPassword", sessionService.getAttribute("userPassword"));
		model.addAttribute("orderData", orderDetails);
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
		model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
		model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);

		final Optional<PromotionResultData> optional = orderDetails.getAppliedOrderPromotions().stream()
				.filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).findFirst();
		if (optional.isPresent())
		{
			final PromotionResultData giveAwayCouponPromotion = optional.get();
			final List<CouponData> giftCoupons = giveAwayCouponPromotion.getGiveAwayCouponCodes();
			model.addAttribute("giftCoupon", giftCoupons.get(0));

			orderDetails.getAppliedOrderPromotions().removeIf(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes()));
		}

		processEmailAddress(model, orderDetails);

		final String continueUrl = getSessionService().getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		final ContentPageModel cmsPage = getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, cmsPage);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		if (isResponsive())
		{
			return getViewForPage(model);
		}

		return SimonControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
	}

	/**
	 *
	 * @return - boolean true of false to tell is this experience is responsive or not.
	 */
	protected boolean isResponsive()
	{
		return ResponsiveUtils.isResponsive();
	}

	/**
	 * @return - this method returns the session service.
	 */
	@Override
	public SessionService getSessionService()
	{
		return super.getSessionService();
	}

	/**
	 * This method process the email and address based on user is logged in or not.
	 *
	 * @param model
	 *           - is the model attribute.
	 * @param orderDetails
	 *           - is order details which gives information of guest user.
	 */
	@Override
	protected void processEmailAddress(final Model model, final OrderData orderDetails)
	{
		String uid;

		if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm"))
		{
			final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
			guestRegisterForm.setOrderCode(orderDetails.getGuid());
			uid = orderDetails.getPaymentInfo().getBillingAddress().getEmail();
			guestRegisterForm.setUid(uid);
			model.addAttribute(guestRegisterForm);
		}
		else
		{
			uid = orderDetails.getUser().getUid();
		}
		model.addAttribute("email", uid);
	}

	/**
	 * This method reset the session.
	 */
	protected void sessionReset()
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.NOINDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
