package com.simon.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.facades.cart.ExtCartFacade;
import com.simon.generated.storefront.controllers.pages.checkout.steps.MultiStepCheckoutController;
import com.simon.integration.exceptions.CartCheckOutIntegrationException;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * ExtMultiStepCheckoutController extends MultiStepCheckoutController which provides the basic functionality for
 * checkout.
 */
@Controller
@RequestMapping(value = "/checkout/multi")
public class ExtMultiStepCheckoutController extends MultiStepCheckoutController
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtMultiStepCheckoutController.class);
	private static final String MULTI = "multi";
	private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart";

	@Resource
	private ExtCartFacade cartFacade;

	/**
	 * This method validates the cart and will redirect the user to cart page if validation fails. In success scenario an
	 * ASYNC call to ESB will be triggered. If the connection to ESB fails, user will be navigated to cart page else the
	 * shipping panel will get loaded.
	 *
	 * @param model
	 * @param redirectAttributes
	 *
	 * @throws CMSItemNotFoundException
	 * @throws CommerceCartModificationException
	 *
	 * @return String
	 *
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = MULTI)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes)
			throws CMSItemNotFoundException, CommerceCartModificationException
	{
		String redirectUrl = REDIRECT_CART_URL;
		if (!validateCart(redirectAttributes))
		{
			try
			{
				cartFacade.createTwoTapCart();
				redirectUrl = getCheckoutStep().nextStep();
			}
			catch (final CartCheckOutIntegrationException cartCheckOutIntegrationException)
			{
				LOG.error(cartCheckOutIntegrationException.getMessage(), cartCheckOutIntegrationException);
				GlobalMessages.addFlashMessage(redirectAttributes, SimonWebConstants.JMS_ERROR,
						getMessageSource().getMessage("integration.jms.exception.error.message", null,
								"Something went wrong in communicating with the store. Please retry.",
								getI18nService().getCurrentLocale()),
						null);
			}
		}
		return redirectUrl;
	}

}
