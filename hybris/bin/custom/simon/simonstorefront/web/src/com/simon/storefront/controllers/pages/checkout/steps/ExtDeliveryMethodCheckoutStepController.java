package com.simon.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.facades.checkout.data.ExtCheckoutData;
import com.simon.facades.checkout.data.ShippingPriceCall;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.generated.storefront.controllers.pages.checkout.steps.DeliveryMethodCheckoutStepController;


/**
 * Extended controller for {@link DeliveryMethodCheckoutStepController} that will handle the custom functionalities on
 * Review and Select Shipping Method panel.
 */
@Controller
@RequestMapping(value = "/checkout/multi/delivery-method")
public class ExtDeliveryMethodCheckoutStepController extends DeliveryMethodCheckoutStepController
{

	@Resource
	private ExtCheckoutFacade checkoutFacade;

	/**
	 * This method supports the functionality where user is selecting the delivery methods per retailer and clicks on
	 * continue button.
	 *
	 * @param retailerId
	 * @param shippingMethod
	 *
	 * @return ExtCheckoutData
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	@ResponseBody
	public ExtCheckoutData doSelectDeliveryMode(final String retailerId, final String shippingMethod)
	{
		final ExtCheckoutData extCheckoutData = checkoutFacade.getCheckoutData();
		checkoutFacade.setDeliveryMethodForRetailer(retailerId, shippingMethod);
		return checkoutFacade.getCheckoutDataForReview(ShippingPriceCall.ALL.toString(), extCheckoutData);
	}

}
