/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateQuoteCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.address.AddressVerificationFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.util.JsonUtils;
import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.facades.checkout.data.ExtCheckoutData;
import com.simon.facades.message.utils.ExtCustomMessageUtils;
import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.facades.user.ExtUserFacade;
import com.simon.generated.storefront.controllers.pages.checkout.steps.DeliveryAddressCheckoutStepController;
import com.simon.integration.exceptions.UserAddressIntegrationException;
import com.simon.integration.users.address.verify.exceptions.UserAddressVerifyIntegrationException;
import com.simon.storefront.checkout.form.ExtAddressForm;
import com.simon.storefront.checkout.util.ExtAddressDataUtil;
import com.simon.storefront.checkout.validation.ExtCheckoutAddressValidator;
import com.simon.storefront.constant.SimonWebConstants;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtWebUtils;


/**
 * Class used for different operations on DeliveryAddress during Checkout steps by extending
 * {@link AbstractCheckoutStepController}.
 *
 */
@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class ExtDeliveryAddressCheckoutStepController extends DeliveryAddressCheckoutStepController
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtDeliveryAddressCheckoutStepController.class);

	private static final String MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL_LOWER_CASE = "multistep-checkout-summary";
	private static final String DELIVERY_ADDRESS = "delivery-address";
	private static final String SHOW_SAVE_TO_ADDRESS_BOOK_ATTR = "showSaveToAddressBook";
	private static final String STUB_SPREEDLY_CALLS = "simon.spreedly.validate.card.browser.call.enable.stub.flag";
	private static final String ADD_SHIPPING_ADDRESS_ERROR = "place.checkout.order.shipping.address";

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "multiStepCheckoutBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "addressVerificationFacade")
	private AddressVerificationFacade addressVerificationFacade;

	@Resource(name = "userFacade")
	private ExtUserFacade userFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource(name = "extCheckoutAddressValidator")
	private ExtCheckoutAddressValidator extCheckoutAddressValidator;

	@Resource(name = "extAddressDataUtil")
	private ExtAddressDataUtil extAddressDataUtil;
	@Resource(name = "extCustomMessageUtils")
	private ExtCustomMessageUtils extCustomMessageUtils;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "checkoutFacade")
	private ExtCheckoutFacade checkoutFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * Check if the configuration is to disable the calls to the Spreedly environment for performance monitioring
	 *
	 * @return boolean
	 */
	@ModelAttribute(name = "enableSpreedlyCalls")
	public boolean checkIfSpreedlyIsDisabled()
	{
		return !configurationService.getConfiguration().getBoolean(STUB_SPREEDLY_CALLS);
	}

	/**
	 * Method is invoked once user enter into checkout steps, This method actually populate common model attributes by
	 * using {@link CartData} and populate values in {@link AddressForm}
	 *
	 * @param model
	 * @param redirectAttributes
	 *           of type {@link RedirectAttributes}
	 * @return the view that will be responsible for rendering the outcome.
	 */
	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateQuoteCheckoutStep
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		checkoutFacade.setDeliveryAddressIfAvailable();
		final CartData cartData = checkoutFacade.getCheckoutCart();

		populateCommonModelAttributes(model, cartData, new ExtAddressData(), new ExtAddressData());
		return SimonControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	/**
	 * Method is invoked once user enter into checkout steps, This method actually is responsible for verify the address
	 * via easy post. It actually check if Delivery Address is valid or not.
	 *
	 * @param addressForm
	 *           the address form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 *           the model
	 * @return the string
	 */
	@RequestMapping(value = "/verify-add", method = RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
	public Object addressVerify(final ExtAddressForm addressForm, final BindingResult bindingResult, final Model model)
	{
		final CartData cartData = checkoutFacade.getCheckoutCart();
		ExtAddressData suggestedAddress = null;
		ExtCheckoutData checkoutData = null;
		boolean addressVerify = true;
		extCheckoutAddressValidator.validate(addressForm, bindingResult);

		//convert address form to address data
		final ExtAddressData addressData = extAddressDataUtil.convertToAddressData(addressForm);
		try
		{
			suggestedAddress = extAddressDataUtil
					.convertToSuggestedAddressDto(userFacade.getUserAddressVerifyResponseDTO(addressData), addressData);
		}
		catch (final UserAddressVerifyIntegrationException e)
		{
			addressVerify = false;
			LOGGER.error("UserAddressVerifyIntegrationException:::", e);
		}
		checkoutData = checkoutFacade.getCheckoutData(model, cartData, addressData, suggestedAddress);
		checkoutData.setAddressVerify(addressVerify);

		return checkoutData;
	}

	/**
	 * Method is invoked once user enter into checkout steps, This method actually is responsible for adding the address
	 * using {@link CartData} and value populated in {@link AddressForm}. It actually check if Delivery Address is
	 * already present , it will replace it with the new one and make it default Delivery Address.
	 *
	 * @param addressForm
	 *           of type {@link ExtAddressForm} used to store the details for Delivery Address.
	 * @param bindingResult
	 *           of type {@link BindingResult}
	 * @param model
	 *           of type {@link Model}
	 * @return the view that will be responsible for rendering the outcome.
	 * @throws DuplicateUidException
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
	public Object add(final ExtAddressForm addressForm, final BindingResult bindingResult, final Model model)
			throws DuplicateUidException
	{
		final CustomerType customerType = checkoutCustomerStrategy.getCurrentUserForCheckout().getType();
		//this is required to convert anonymous user to guest user
		if (null == customerType)
		{
			getCustomerFacade().createGuestUserForAnonymousCheckout(addressForm.getEmailId(),
					getMessageSource().getMessage("text.guest.customer", null, getI18nService().getCurrentLocale()));

		}

		extCheckoutAddressValidator.validate(addressForm, bindingResult);
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			return SimonControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		ExtCheckoutData checkoutdata = new ExtCheckoutData();
		boolean addressAdd = true;
		final ExtAddressData suggestedAddress = null;
		final ExtAddressData userAddress = null;

		//convert address form to address data
		final ExtAddressData newAddress = extAddressDataUtil.convertToAddressData(addressForm);
		processAddressVisibilityAndDefault(addressForm, newAddress);

		boolean isAddAddress = false;

		if (null != customerType && customerType.equals(CustomerType.REGISTERED))
		{
			try
			{
				isAddAddress = addAddress(newAddress);
			}
			catch (final UserAddressIntegrationException ex)
			{
				LOGGER.error("UserAddressIntegrationException occurred while adding address", ex);
				checkoutdata
						.setErrorMessage(Localization.getLocalizedString(SimonCoreConstants.INTEGRATION_USER_ADD_ADDRESS_ERROR_CODE
								+ ex.getCode() + SimonCoreConstants.INTEGRATION_ERROR_CODE_MESSAGE));
			}
		}
		else
		{
			userFacade.addAddress(newAddress);
		}

		//CHECKING REGISTER USER AND ADDING SHIPPING ADDRESS AT PO.COM
		if (!checkoutCustomerStrategy.isAnonymousCheckout() && !isAddAddress)
		{
			addressAdd = false;
			final CartData cartData = checkoutFacade.getCheckoutCart();
			checkoutdata = checkoutFacade.getCheckoutData(model, cartData, userAddress, suggestedAddress);
			checkoutdata.setAddressAdded(addressAdd);
			checkoutdata.setErrorMessage(Localization.getLocalizedString(ADD_SHIPPING_ADDRESS_ERROR));
			return checkoutdata;
		}
		// Set the new address as the selected checkout delivery address
		addressAdd = checkoutFacade.setDeliveryAddress(newAddress);
		final CartData cartData = checkoutFacade.getCheckoutCart();
		checkoutdata = checkoutFacade.getCheckoutData(model, cartData, userAddress, suggestedAddress);
		checkoutdata.setAddressAdded(addressAdd);
		checkoutdata.setShippingAddresses(getSupportedDeliveryAddresses());
		setBillingAndPaymentInfo(checkoutdata);
		if (addressAdd)
		{
			checkoutFacade.checkAdditionalCartInfo(checkoutdata);
		}
		return checkoutdata;
	}

	/**
	 * This Method synced user address to PO.com and save in db too.
	 *
	 * @param extAddressData
	 *
	 * @return boolean
	 */
	private boolean addAddress(final ExtAddressData extAddressData) throws UserAddressIntegrationException
	{
		boolean result = false;
		try
		{
			result = userFacade.addUserAddress(extAddressData);
		}
		catch (final ModelSavingException modelSavingException)
		{
			LOGGER.error("ModelSavingException occurred while saving the address", modelSavingException);
		}

		return result;
	}

	/**
	 * get all Supported Delivery Addresses checkout
	 *
	 * @return addressList
	 */
	private List<AddressData> getSupportedDeliveryAddresses()
	{
		List<AddressData> addressList = new ArrayList<>();
		final CustomerType customerType = checkoutCustomerStrategy.getCurrentUserForCheckout().getType();
		if (!checkoutCustomerStrategy.isAnonymousCheckout() && !customerType.equals(CustomerType.GUEST))
		{
			addressList = (List<AddressData>) checkoutFacade.getSupportedDeliveryAddresses(true);
		}
		return addressList;
	}


	/**
	 * Method is invoked once user edit address from checkout. This method actually is return json data for checkout data
	 * and updated delivery addresses
	 *
	 * @param model
	 * @return Object
	 */
	@RequestMapping(value = "/edit-address", method = RequestMethod.GET)
	@ResponseBody
	@RequireHardLogIn
	public Object edit(final Model model)
	{
		final CartData cartData = checkoutFacade.getCheckoutCart();
		final ExtCheckoutData checkoutdata = checkoutFacade.getCheckoutData(model, cartData);
		checkoutdata.setShippingAddresses(getSupportedDeliveryAddresses());
		return checkoutdata;
	}

	/**
	 * Method is invoked once user select address from address drop down. This method actually is responsible for adding
	 * the address in cartdata
	 *
	 * @param addressId
	 *
	 * @param model
	 *           of type {@link Model}
	 * @return checkoutData
	 */
	@RequestMapping(value = "/selected-address", method = RequestMethod.POST)
	@ResponseBody
	@RequireHardLogIn
	public Object addSavedAddress(@RequestParam final String addressId, final Model model)

	{
		final ExtAddressData suggestedAddress = null;
		final ExtAddressData userAddress = null;
		boolean addressAdd = false;
		final AddressData newAddress = userFacade.getAddress(addressId);
		// Set the new address as the selected checkout delivery address
		addressAdd = checkoutFacade.setDeliveryAddress(newAddress);
		final CartData cartData = checkoutFacade.getCheckoutCart();
		final ExtCheckoutData checkoutData = checkoutFacade.getCheckoutData(model, cartData, userAddress, suggestedAddress);
		checkoutData.setAddressAdded(addressAdd);
		setBillingAndPaymentInfo(checkoutData);
		checkoutFacade.checkAdditionalCartInfo(checkoutData);
		return checkoutData;
	}

	/**
	 * This is the method to process address visibility and set the default address.
	 *
	 * @param addressForm
	 *           of type {@link ExtAddressForm} used to store the details for Delivery Address.
	 * @param newAddress
	 *           of type {@link AddressData} used to store the details for Delivery Address.
	 */
	protected void processAddressVisibilityAndDefault(final ExtAddressForm addressForm, final AddressData newAddress)
	{
		if (addressForm.getSaveInAddressBook() != null)
		{
			newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
			if (addressForm.getSaveInAddressBook().booleanValue() && userFacade.isAddressBookEmpty())
			{
				newAddress.setDefaultAddress(true);
			}
		}
		else if (checkoutCustomerStrategy.isAnonymousCheckout())
		{
			newAddress.setDefaultAddress(true);
			newAddress.setVisibleInAddressBook(true);
		}
	}

	/**
	 * This method populates the common model attributes for shipping page.In this we are passing to json object to jsp
	 * page which contains cartdata and other details in the json.
	 *
	 * @param model
	 * @param cartData
	 * @param addressForm
	 *           of type {@link ExtAddressForm} used to store the details for Delivery Address.
	 * @throws CMSItemNotFoundException
	 */
	protected void populateCommonModelAttributes(final Model model, final CartData cartData, final ExtAddressData addressData,
			final ExtAddressData suggestedAddress) throws CMSItemNotFoundException
	{
		final ExtCheckoutData extCheckoutData = checkoutFacade.getCheckoutData(model, cartData, addressData, suggestedAddress);
		extCheckoutData.setShippingAddresses(getSupportedDeliveryAddresses());
		model.addAttribute("cartDataJson", JsonUtils.getJSONStringFromObject(extCheckoutData));
		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
		model.addAttribute("addressFormEnabled", Boolean.valueOf(checkoutFacade.isNewAddressEnabledForCart()));
		model.addAttribute("removeAddressEnabled", Boolean.valueOf(checkoutFacade.isRemoveAddressEnabledForCart()));
		model.addAttribute(SHOW_SAVE_TO_ADDRESS_BOOK_ATTR, Boolean.TRUE);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, resourceBreadcrumbBuilder.getBreadcrumbs(getBreadcrumbKey()));
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("pageType", "checkout");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL_LOWER_CASE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL_LOWER_CASE));
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	@Override
	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}

	/**
	 * @param selectedShippingMethod
	 * @return ExtCheckoutData
	 */
	@RequestMapping(value = "/estimate-cart", method = RequestMethod.GET)
	@ResponseBody
	@RequireHardLogIn
	public ExtCheckoutData estimateCart(final String selectedShippingMethod)
	{
		ExtCheckoutData extCheckoutData = checkoutFacade.getCheckoutData();
		checkoutFacade.estimateAndValidateCart(extCheckoutData, selectedShippingMethod);
		extCheckoutData = checkoutFacade.getCheckoutDataForReview(selectedShippingMethod, extCheckoutData);

		return extCheckoutData;
	}

	/**
	 * This method use to set billing and payment information in cart json
	 *
	 * @param ExtCheckoutData
	 * @throws UserAddressIntegrationException
	 */
	private void setBillingAndPaymentInfo(final ExtCheckoutData extCheckoutData)
	{
		final CustomerType customerType = checkoutCustomerStrategy.getCurrentUserForCheckout().getType();

		if (null != customerType && customerType.equals(CustomerType.REGISTERED))
		{
			try
			{
				extCheckoutData.setPaymentInfos(checkoutFacade.getCCPaymentInfoFromExternalSystem());
			}
			catch (final UserAddressIntegrationException ex)
			{
				LOGGER.error("UserAddressIntegrationException occurred while fetching payment", ex);
			}
		}
		else
		{
			extCheckoutData.setPaymentInfos(getUserFacade().getCCPaymentInfos(true));
		}
		extCheckoutData.setSpreedlyEnvKey(configurationService.getConfiguration().getString(SimonWebConstants.SPREEDLY_ENV_KEY));
		extCheckoutData.setBillingAddresses(userFacade.getBillingAddressList(Boolean.TRUE));
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.NOINDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}
}
