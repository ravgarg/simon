package com.simon.storefront.forms;

public class TrackOrderForm
{
	private String orderId;
	private String emailId;

	public String getOrderId()
	{
		return orderId;
	}

	public void setOrderId(final String orderId)
	{
		this.orderId = orderId;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(final String emailId)
	{
		this.emailId = emailId;
	}


}
