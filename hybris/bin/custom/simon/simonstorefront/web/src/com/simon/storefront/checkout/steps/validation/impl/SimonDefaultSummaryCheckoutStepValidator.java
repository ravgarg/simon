/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.storefront.checkout.steps.validation.impl;

import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.commercefacades.order.data.CartData;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.facades.order.ExtCheckoutFacade;
import com.simon.generated.storefront.checkout.steps.validation.impl.DefaultSummaryCheckoutStepValidator;


/**
 * Validator class used to validate Simon checkout Steps by extending {@link DefaultSummaryCheckoutStepValidator},
 * implementing different methods to validate at different level during Checkout journey.
 *
 */
public class SimonDefaultSummaryCheckoutStepValidator extends DefaultSummaryCheckoutStepValidator
{

	private static final String PICKUP = "pickup";

	private static final String CHECKOUT_MULTI_PAYMENT_DETAILS_NOTPROVIDED = "checkout.multi.paymentDetails.notprovided";

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSummaryCheckoutStepValidator.class);

	@Resource(name = "checkoutFacade")
	private ExtCheckoutFacade checkoutFacade;

	@Override
	public ValidationResults validateOnEnter(final RedirectAttributes redirectAttributes)
	{
		final ValidationResults cartResult = checkCartAndDelivery(redirectAttributes);
		if (cartResult != null)
		{
			return cartResult;
		}

		final ValidationResults paymentResult = checkPaymentMethodAndPickup(redirectAttributes);
		if (paymentResult != null)
		{
			return paymentResult;
		}

		return super.validateOnEnter(redirectAttributes);
	}

	/**
	 * Method used to validate payment method and pickup details , its an overriden method of
	 * {@link DefaultSummaryCheckoutStepValidator#checkPaymentMethodAndPickup()} for using {@link ExtCheckoutFacade}.
	 *
	 * @param redirectAttributes
	 *           of type {@link RedirectAttributes}
	 * @return {@link ValidationResults} is returned as a validation status of the end result.
	 */
	@Override
	protected ValidationResults checkPaymentMethodAndPickup(final RedirectAttributes redirectAttributes)
	{

		if (!checkoutFacade.hasPaymentInfo())
		{
			GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.INFO_MESSAGES_HOLDER,
					CHECKOUT_MULTI_PAYMENT_DETAILS_NOTPROVIDED);
			return ValidationResults.REDIRECT_TO_PAYMENT_METHOD;
		}
		final CartData cartData = checkoutFacade.getCheckoutCart();

		if (!checkoutFacade.hasShippingItems())
		{
			cartData.setDeliveryAddress(null);
		}

		if (!checkoutFacade.hasPickUpItems() && PICKUP.equals(cartData.getDeliveryMode().getCode()))
		{
			return ValidationResults.REDIRECT_TO_PICKUP_LOCATION;
		}
		LOGGER.info("Null Value returned.");
		return null;
	}
}
