package com.simon.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.simon.core.dto.ProductDetailsData;
import com.simon.facades.solr.product.page.SolrProductPageFacade;


/**
 * The Class SolrProductPageController.
 */
@Controller
@RequestMapping(value = "/**/ps")
public class SolrProductPageController extends AbstractSearchPageController
{

	/** The solr product page facade. */
	@Autowired
	private SolrProductPageFacade solrProductPageFacade;

	/**
	 * Gets the product display page.
	 *
	 * @param baseProductCode
	 *           the base product code
	 * @return the product display page
	 */
	@RequestMapping(value = "/{productCode:.*}", method = RequestMethod.GET)
	public @ResponseBody ProductDetailsData getProductDisplayPage(@PathVariable("productCode") final String baseProductCode)
	{
		ProductDetailsData prouctDetails = null;
		if (StringUtils.isNotBlank(baseProductCode))
		{
			final PageableData pageableData = createPageableData(0, 0, null, ShowMode.Page);
			final SearchStateData searchState = new SearchStateData();
			final SearchQueryData searchQueryData = new SearchQueryData();
			searchQueryData.setValue(baseProductCode);
			searchState.setQuery(searchQueryData);
			prouctDetails = solrProductPageFacade.getProductDetails(searchState, pageableData);
		}
		return prouctDetails;
	}
}
