package com.simon.storefront.controllers;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.storefront.data.MetaElementData;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController.SearchResultsData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController.ShowMode;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessage;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.LoginForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.ContentPageType;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.TrendModel;
import com.simon.core.services.DesignerService;
import com.simon.facades.account.data.DealData;
import com.simon.facades.account.data.DealDataType;
import com.simon.facades.account.data.StoreData;
import com.simon.facades.cms.ExtPageFacade;
import com.simon.facades.customer.ExtCustomerFacade;
import com.simon.facades.deal.ExtDealFacade;
import com.simon.facades.favorite.data.FavoriteTypeEnum;
import com.simon.generated.storefront.controllers.ControllerConstants;
import com.simon.generated.storefront.controllers.pages.DefaultPageController;
import com.simon.storefront.breadcrumb.impl.ExtContentPageBreadcrumbBuilder;
import com.simon.storefront.util.ExtWebUtils;


/**
 * This ExtDefaultPageController class overrides the DefaultPageController functionality.
 */
@Controller
public class ExtDefaultPageController extends DefaultPageController
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtDefaultPageController.class);
	private static final String ATOZDESIGNERS_PAGE_ID = "atozdesigners";
	private static final String STORE_DATA = "storeData";
	private static final String COOKIE_NAME = "betaSession";

	@Resource(name = "extContentPageBreadcrumbBuilder")
	private ExtContentPageBreadcrumbBuilder extContentPageBreadcrumbBuilder;
	@Resource
	private SessionService sessionService;
	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;
	@Resource(name = "extCustomerFacade")
	private ExtCustomerFacade extCustomerFacade;
	@Resource(name = "extCmsPageFacade")
	private ExtPageFacade extCmsPageFacade;
	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;
	@Resource(name = "extStoreConverter")
	private Converter<ShopModel, StoreData> extStoreConverter;
	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "defaultDesignerService")
	private DesignerService defaultDesignerService;
	@Resource(name = "httpSessionRequestCache")
	private HttpSessionRequestCache httpSessionRequestCache;
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "extDealFacade")
	private ExtDealFacade extDealFacade;

	private static final String ERROR_CMS_PAGE = "notFound";
	private static final String ATOZ_DESIGNER_PAGE = REDIRECT_PREFIX + "/atozdesigners";
	private static final String DESIGNER_CODE = "designerCode";
	private static final String DESIGNER = "designer";
	private static final String STORE_NAME = "storeName";
	private static final String STORE_PAGE = "store";
	private static final String STORE_ID = "storeId";
	private static final String DEAL_CODE = "dealCode";
	private static final String PRODUCT_DEAL_CODE = "productDealCode";
	private static final String IS_FAV_STORE = "isFavStore";
	private static final String SEARCH_PAGE_DATA = "searchPageData";
	private static final String IS_FAV_DESIGNER = "isFavDesigner";
	private static final String SEARCH_PAGE_SIZE = "storefront.search.pageSize";
	private static final String TREND_NAME = "trendName";
	private static final String TREND_CODE = "trendCode";
	private static final String PAGETYPE = "pageType";
	private static final String CATNAME = "categoryName";
	private static final String IS_FAV_DEAL = "isFavDeal";
	private static final String DESIGNER_LIST_PAGE = "designerListPage";
	private static final String PROMO_INLINE_BANNER = "promoInLineBanner";
	private static final String HERO_BANNER = "heroBanner";
	private static final String SEO_LINKS = "seoLinks";
	private static final String ERROR_MESSAGE = "errorMessage";
	private static final String ACC_CONF_MSGS = "accConfMsgs";
	private static final String COLON = ":";

	/**
	 * This method used to display designer product data to designer Listing page.
	 *
	 * @param designerCode
	 *           the designer code
	 * @param searchQuery
	 *           the search query
	 * @param sortCode
	 *           the sort code
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param model
	 *           the model
	 * @return viewPage
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/designer", method = RequestMethod.GET)
	public String fillDesignerListData(@RequestParam(value = "code", required = false) final String code,
			@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "sort", required = false) final String sortCode, final HttpServletRequest request,
			final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		String designerCode = code;
		if (StringUtils.isEmpty(designerCode))
		{
			final String[] params = searchQuery.split(COLON);
			if (params.length > 0)
			{
				designerCode = params[0];
			}
		}
		final ContentPageModel pageForRequest = getContentPageForLabelOrId(DESIGNER_LIST_PAGE);
		final ContentPageModel a2zPage = getContentPageForLabelOrId(ATOZDESIGNERS_PAGE_ID);

		if (null != pageForRequest && StringUtils.isNotEmpty(designerCode))
		{
			final DesignerModel designer = defaultDesignerService.getDesignerForCode(designerCode);
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.DESIGNERS.name());
			if (null != designer)
			{
				String pageURL;
				ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
				if (StringUtils.isNotEmpty(code))
				{
					searchPageData = populateDataForDesigner(designerCode, sortCode, model, pageForRequest, designer);
				}
				else
				{
					searchPageData = populateDataForDesigner(searchQuery, sortCode, model, pageForRequest, designer);
				}

				model.addAttribute(PAGETYPE, "designerlisting");
				model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
				model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
				if (CollectionUtils.isNotEmpty(searchPageData.getResults()))
				{
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pageForRequest, Optional.of(a2zPage)));
					model.addAttribute(PROMO_INLINE_BANNER, designer.getPromoInlineBanner());
					model.addAttribute(HERO_BANNER, designer.getHeroBanner());
					model.addAttribute(SEO_LINKS, designer.getSeoLinks());
					pageURL = getViewForPage(pageForRequest);
				}
				else
				{
					final List<ProductModel> productList = designer.getProducts().stream()
							.filter(p -> ShopState.OPEN.getCode().equals(p.getShop().getState().getCode())).collect(Collectors.toList());
					if (CollectionUtils.isEmpty(productList))
					{
						pageURL = ATOZ_DESIGNER_PAGE;
					}
					else
					{
						pageURL = getViewForPage(pageForRequest);
					}

				}
				return pageURL;
			}
		}
		return showErrorNotFoundPage(model, response);
	}

	/**
	 * This method used to display designer product data to designer Listing page.
	 *
	 * @param sortCode
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return viewPage
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/feature-designer/*", method = RequestMethod.GET)
	public String fillFeatureDesignerListData(@RequestParam(value = "sort", required = false) final String sortCode,
			final HttpServletRequest request, final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		String pageURL;
		if (null != pageForRequest)
		{
			final Collection<DesignerModel> designerList = pageForRequest.getDesigner();
			if (CollectionUtils.isNotEmpty(designerList))
			{
				final DesignerModel designer = designerList.iterator().next();
				extCustomerFacade.updateFavorite(FavoriteTypeEnum.DESIGNERS.name());
				if (null != designer)
				{
					final ProductSearchPageData<SearchStateData, ProductData> searchPageData = populateDataForDesigner(
							designer.getCode(), sortCode, model, pageForRequest, designer);
					if (CollectionUtils.isNotEmpty(searchPageData.getResults()))
					{
						model.addAttribute(PAGETYPE, "designerlisting");
						model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS,
								SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
						model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
						model.addAttribute(PROMO_INLINE_BANNER, designer.getPromoInlineBanner());
						model.addAttribute(HERO_BANNER, designer.getHeroBanner());
						model.addAttribute(SEO_LINKS, designer.getSeoLinks());
						pageURL = getViewForPage(pageForRequest);
					}
					else
					{
						final List<ProductModel> productList = designer.getProducts().stream()
								.filter(p -> ShopState.OPEN.getCode().equals(p.getShop().getState().getCode()))
								.collect(Collectors.toList());
						if (CollectionUtils.isEmpty(productList))
						{
							pageURL = ATOZ_DESIGNER_PAGE;
						}
						else
						{
							pageURL = getViewForPage(pageForRequest);
						}
					}
					return pageURL;
				}
			}
		}
		return showErrorNotFoundPage(model, response);
	}

	/**
	 * This method used to display designer Landing Page on the basis of retailer active/not active.
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return viewPage
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/designer/landing/*", method = RequestMethod.GET)
	public String fillDesignerLandingData(@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		if (null != pageForRequest)
		{
			final Collection<DesignerModel> designers = pageForRequest.getDesigner();
			if (CollectionUtils.isNotEmpty(designers))
			{
				final DesignerModel designerModel = designers.iterator().next();
				final ProductSearchPageData<SearchStateData, ProductData> searchPageData = populateDataForDesigner(searchQuery,
						sortCode, model, pageForRequest, designerModel);

				if (CollectionUtils.isNotEmpty(searchPageData.getResults()))
				{
					model.addAttribute(PAGETYPE, "designerlanding");
					model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
					model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
					return getViewForPage(pageForRequest);
				}

			}
		}
		return ATOZ_DESIGNER_PAGE;
	}


	/**
	 * This method used to display the load more json data on listing page.Or if some filter is applied it will gives the
	 * results.
	 *
	 * @param searchQuery
	 * @param listingId
	 * @param page
	 * @param showMode
	 * @param sortCode
	 * @param request
	 * @return viewPage
	 */
	@ResponseBody
	@RequestMapping(value = "/designer/**/results", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonSearchResults(@RequestParam(value = "code", required = false) final String code,
			@RequestParam(value = "q", required = false) final String searchQuery, @RequestParam("listingId") final String listingId,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model,
			final HttpServletRequest request)
	{

		String designerCode = code;
		if (StringUtils.isEmpty(designerCode))
		{
			designerCode = searchQuery;
		}

		final String[] params = designerCode.split(COLON);
		if (params.length > 0)
		{
			designerCode = params[0];
		}

		final ExtResultsData<ProductData> searchResultsData = new ExtResultsData<>();
		sessionService.setAttribute(DESIGNER_CODE, designerCode);

		ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
		if (StringUtils.isNotEmpty(code))
		{
			searchPageData = performSearch(code, page, showMode, sortCode, getSearchPageSize());
		}
		else
		{
			searchPageData = performSearch(searchQuery, page, showMode, sortCode, getSearchPageSize());
		}

		if (null != searchPageData)
		{
			searchResultsData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			searchResultsData.setPagination(searchPageData.getPagination());
			searchResultsData.setLoggedInUser(extCustomerFacade.checkIfUserIsLoggedIn());
			searchResultsData.setFavSuccessMessage(getLocalizedString(SimonCoreConstants.FAV_ADDED_MESSAGE));
		}
		return searchResultsData;
	}

	/**
	 * This is the method to land on Beta page. This is using for beta login.
	 *
	 * @param loginError
	 * @param model
	 * @param request
	 * @param response
	 * @param session
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/beta", method = RequestMethod.GET)
	public String getBetaPage(@RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		LOGGER.info("getBetaPage start");
		final List errorMessages = (List) model.asMap().get(ACC_CONF_MSGS);
		if (CollectionUtils.isNotEmpty(errorMessages))
		{
			final GlobalMessage message = (GlobalMessage) errorMessages.get(0);
			if (message != null)
			{
				model.addAttribute(ERROR_MESSAGE, message.getCode());
			}

		}
		final LoginForm loginForm = new LoginForm();
		model.addAttribute(loginForm);
		model.addAttribute("enableCaptcha", sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA));
		model.addAttribute("disablePassword",
				configurationService.getConfiguration().getBoolean("simon.whitelisting.password.disable"));
		model.addAttribute("pageType", "beta-login");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);

		final boolean whiteListingFlag = configurationService.getConfiguration().getBoolean("simon.whitelisting.fallback");
		boolean betaCookieExist = false;
		if (whiteListingFlag)
		{
			final Cookie[] cookies = request.getCookies();
			if (null != cookies)
			{
				for (final Cookie cookie : cookies)
				{
					if (COOKIE_NAME.equals(cookie.getName()) && "yes".equals(cookie.getValue()))
					{
						betaCookieExist = true;
						break;
					}
				}
			}
		}
		if (betaCookieExist || !whiteListingFlag)
		{
			return REDIRECT_PREFIX + "/";
		}
		final ContentPageModel pageForRequest = getContentPageForLabelOrId("beta-login");
		storeCmsPageInModel(model, pageForRequest);
		return getViewForPage(model);
	}

	/**
	 * This method after successful validation allow user to access simon site otherwise redirect to beta content page.
	 *
	 * @param formData
	 * @param model
	 * @param request
	 * @param response
	 * @param bindingResult
	 * @param redirectAttributes
	 * @return if valid user allow the simon site otherwise to beta page.
	 */
	@RequestMapping(value = "/beta-login", method = RequestMethod.POST)
	public String betaAccountLogin(@ModelAttribute("loginData") final LoginForm formData, final HttpServletRequest request,
			final HttpServletResponse response, final BindingResult bindingResult, final RedirectAttributes redirectAttributes)
	{
		LOGGER.info("betaAccountLogin start");
		final boolean validUser = extCmsPageFacade.betaLoginProcess(formData.getJ_username(), formData.getJ_password(), request,
				response);
		if (!validUser)
		{
			LOGGER.info("betaAccountLogin not a valid user");
			GlobalMessages.addFlashMessage(redirectAttributes, ACC_CONF_MSGS, messageSource
					.getMessage("text.beta.user.authentication.message", new Object[] {}, getI18nService().getCurrentLocale()));
			return REDIRECT_PREFIX + "/beta";
		}
		return REDIRECT_PREFIX + "/";
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page,
			final ShowMode showMode, final String sortCode, final int pageSize)
	{
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		return productSearchFacade.textSearch(searchState, pageableData);
	}

	protected PageableData createPageableData(final int pageNumber, final int pageSize, final String sortCode,
			final ShowMode showMode)
	{
		final PageableData pageableData = new PageableData();
		pageableData.setCurrentPage(pageNumber);
		pageableData.setSort(sortCode);

		if (ShowMode.All == showMode)
		{
			pageableData.setPageSize(getSearchPageSize());
		}
		else
		{
			pageableData.setPageSize(pageSize);
		}
		return pageableData;
	}

	public static class ExtResultsData<RESULT> extends SearchResultsData<RESULT>
	{
		private boolean loggedInUser;
		private String favSuccessMessage;

		public String getFavSuccessMessage()
		{
			return favSuccessMessage;
		}

		public void setFavSuccessMessage(final String favSuccessMessage)
		{
			this.favSuccessMessage = favSuccessMessage;
		}

		public boolean isLoggedInUser()
		{
			return loggedInUser;
		}

		public void setLoggedInUser(final boolean loggedInUser)
		{
			this.loggedInUser = loggedInUser;
		}
	}

	/**
	 * Method to get localized label from locale properties.
	 *
	 * @param key
	 *           the key
	 * @return Localized title string.
	 */
	protected String getLocalizedString(final String key)
	{
		return Localization.getLocalizedString(key);
	}

	@RequestMapping(value = "/store/*", method = RequestMethod.GET)
	public String fillStoreListData(@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		final ContentPageModel storePage = getContentPageForLabelOrId("storesDirectory");
		if (pageForRequest != null)
		{
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.RETAILERS.name());
			final Collection<ShopModel> shopList = pageForRequest.getShops();
			final String pageLabel = pageForRequest.getLabel();
			if (CollectionUtils.isNotEmpty(shopList))
			{
				final ShopModel shopModel = shopList.iterator().next();
				if (ShopState.OPEN.equals(shopModel.getState()))
				{
					sessionService.setAttribute(STORE_PAGE, shopModel.getId());
					model.addAttribute(STORE_NAME, shopModel.getName());
					model.addAttribute(STORE_ID, shopModel.getId());
					model.addAttribute(IS_FAV_STORE, extCustomerFacade.isFavStore(shopModel.getId()));
					storeCmsPageInModel(model, pageForRequest);
					setUpMetaDataForContentPage(model, pageForRequest);
					model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pageForRequest, Optional.of(storePage)));
					final ProductSearchPageData<SearchStateData, ProductData> searchPageData = extCmsPageFacade
							.searchListingPageData(searchQuery, sortCode, model, pageForRequest, pageLabel);
					model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
					model.addAttribute(PAGETYPE, "storelisting");
					model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS,
							SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
					model.addAttribute(CATNAME, shopModel.getName().replaceAll(SimonControllerConstants.APPOSTROPHY_CONSTANT,
							SimonControllerConstants.ASCII_APPOSTROPHY_CONSTANT));
					return getViewForPage(pageForRequest);
				}
				else
				{
					return REDIRECT_PREFIX + "/404";
				}
			}
		}
		return showErrorNotFoundPage(model, response);
	}

	/**
	 * Displayes all the products which belong to a single deal
	 *
	 * @param searchQuery
	 *           the deal code
	 * @param page
	 * @param showMode
	 * @param sortCode
	 * @param searchText
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/deal/*", method = RequestMethod.GET)
	public String fillDealListData(@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		if (pageForRequest != null)
		{
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.OFFERS.name());
			final List<DealModel> dealList = (List<DealModel>) pageForRequest.getDeal();
			if (CollectionUtils.isNotEmpty(dealList))
			{
				final DealModel dealModel = dealList.get(0);
				if (ShopState.OPEN.equals(dealModel.getShop().getState()))
				{
					final DealData dealData = extCustomerFacade.getDealData(dealModel);
					if (dealData.getDealUrlType().equalsIgnoreCase(DealDataType.PRODUCT_PROMOTION_DEAL_URL.name())
							|| dealData.getDealUrlType().equalsIgnoreCase(DealDataType.PRODUCT_DEAL_URL.name()))
					{
						setDealVariableInSession(dealModel);
						model.addAttribute("dealName", dealModel.getName());
						model.addAttribute(DEAL_CODE, dealModel.getCode());
						model.addAttribute(CATNAME, dealModel.getName().replaceAll(SimonControllerConstants.APPOSTROPHY_CONSTANT,
								SimonControllerConstants.ASCII_APPOSTROPHY_CONSTANT));
						storeCmsPageInModel(model, pageForRequest);
						setUpMetaDataForContentPage(model, pageForRequest);
						model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pageForRequest));
						model.addAttribute(IS_FAV_DEAL, extCustomerFacade.isFavDeal(dealModel.getCode()));
						final ProductSearchPageData<SearchStateData, ProductData> searchPageData = extCmsPageFacade
								.searchListingPageData(searchQuery, sortCode, model, pageForRequest, pageForRequest.getLabel());
						model.addAttribute(PAGETYPE, "deallisting");
						model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS,
								SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
						model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
						return getViewForPage(pageForRequest);
					}
					else
					{

						return REDIRECT_PREFIX + dealData.getDealUrl();
					}

				}
			}
		}
		return showErrorNotFoundPage(model, response);

	}

	/**
	 * Method to give json response to load more functionality
	 *
	 * @param searchQuery
	 * @param listingId
	 * @param page
	 * @param showMode
	 * @param sortCode
	 * @param request
	 * @param model
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@ResponseBody
	@RequestMapping(value = "/deal/**/results/", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonDealResults(@RequestParam("q") final String searchQuery,
			@RequestParam("listingId") final String listingId, @RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException
	{
		final ExtResultsData<ProductData> searchResultsData = new ExtResultsData<>();
		final DealModel dealModel = extDealFacade.getDealForCode(listingId);
		setDealVariableInSession(dealModel);
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, siteConfigService.getInt("storefront.search.pageSize", 0));
		if (null != searchPageData)
		{
			searchResultsData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			searchResultsData.setPagination(searchPageData.getPagination());
			searchResultsData.setLoggedInUser(extCustomerFacade.checkIfUserIsLoggedIn());
			searchResultsData.setFavSuccessMessage(getLocalizedString(SimonCoreConstants.FAV_ADDED_MESSAGE));
		}
		return searchResultsData;
	}

	@ResponseBody
	@RequestMapping(value = "/store/**/results/", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonStoreResults(@RequestParam("q") final String searchQuery,
			@RequestParam("listingId") final String listingId, @RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		final ExtResultsData<ProductData> searchResultsData = new ExtResultsData<>();
		sessionService.setAttribute(STORE_PAGE, listingId);
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, siteConfigService.getInt(SEARCH_PAGE_SIZE, 0));
		if (null != searchPageData)
		{
			searchResultsData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			searchResultsData.setPagination(searchPageData.getPagination());
			searchResultsData.setLoggedInUser(extCustomerFacade.checkIfUserIsLoggedIn());
			searchResultsData.setFavSuccessMessage(getLocalizedString(SimonCoreConstants.FAV_ADDED_MESSAGE));
		}
		return searchResultsData;
	}


	private String showErrorNotFoundPage(final Model model, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		// No page found - display the notFound page with error from controller
		storeCmsPageInModel(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));

		model.addAttribute(WebConstants.MODEL_KEY_ADDITIONAL_BREADCRUMB,
				resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.not.found"));
		GlobalMessages.addErrorMessage(model, "system.error.page.not.found");

		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		model.addAttribute(PAGETYPE, "error");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
	}

	protected int getSearchPageSize()
	{
		return getSiteConfigService().getInt(SEARCH_PAGE_SIZE, 0);
	}



	/**
	 * @param request
	 * @param response
	 * @param model
	 * @return @String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/store/deals/*", method = RequestMethod.GET)
	public String getStoreDeals(final HttpServletRequest request, final HttpServletResponse response, final Model model)
			throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		if (pageForRequest != null)
		{
			extCustomerFacade.updateFavorite(FavoriteTypeEnum.OFFERS.name());
			final Collection<ShopModel> shopList = pageForRequest.getShops();
			if (CollectionUtils.isNotEmpty(shopList))
			{
				final ShopModel shopModel = shopList.iterator().next();
				if (ShopState.OPEN.equals(shopModel.getState()))
				{
					final StoreData storeData = new StoreData();
					extStoreConverter.convert(shopModel, storeData);
					storeData.setFavorite(extCustomerFacade.isFavStore(shopModel.getId()));
					model.addAttribute(STORE_DATA, storeData);
					model.addAttribute(CATNAME, shopModel.getName().replaceAll(SimonControllerConstants.APPOSTROPHY_CONSTANT,
							SimonControllerConstants.ASCII_APPOSTROPHY_CONSTANT));
					storeCmsPageInModel(model, pageForRequest);
					setUpMetaDataForContentPage(model, pageForRequest);
					model.addAttribute(PAGETYPE, "storedeal");
					model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
					return getViewForPage(pageForRequest);
				}
				else
				{
					return REDIRECT_PREFIX + "/";
				}
			}
		}
		return showErrorNotFoundPage(model, response);
	}


	/**
	 * Fill trend list data.
	 *
	 * @param searchQuery
	 *           the search query
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @param model
	 *           the model
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/trend/*", method = RequestMethod.GET)
	public String fillTrendListData(@RequestParam(value = "q", required = false) final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final HttpServletRequest request,
			final HttpServletResponse response, final Model model) throws CMSItemNotFoundException
	{
		final ContentPageModel pageForRequest = getContentPageForRequest(request);
		if (pageForRequest != null)
		{
			final String pageLabel = pageForRequest.getLabel();
			final Collection<TrendModel> trendList = pageForRequest.getTrend();
			if (CollectionUtils.isNotEmpty(trendList))
			{
				extCustomerFacade.updateFavorite(FavoriteTypeEnum.PRODUCTS.name());
				final TrendModel trend = trendList.iterator().next();
				model.addAttribute(TREND_CODE, trend.getCode());
				model.addAttribute(TREND_NAME, trend.getName());
				model.addAttribute(CATNAME, trend.getName().replaceAll(SimonControllerConstants.APPOSTROPHY_CONSTANT,
						SimonControllerConstants.ASCII_APPOSTROPHY_CONSTANT));
				sessionService.setAttribute(TREND_CODE, trend.getCode());
				final ProductSearchPageData<SearchStateData, ProductData> searchPageData = extCmsPageFacade
						.searchListingPageData(searchQuery, sortCode, model, pageForRequest, pageLabel);

				if (null != searchPageData && null != searchPageData.getResults()
						&& CollectionUtils.isEmpty(searchPageData.getResults()))
				{

					final Collection<ProductModel> products = trend.getProducts();
					final long productCount = products.stream()
							.filter(t -> (null != t.getShop() && ShopState.OPEN.getCode().equals(t.getShop().getState().getCode())))
							.count();
					if (productCount <= 0)
					{
						return showErrorNotFoundPage(model, response);
					}
				}

				storeCmsPageInModel(model, pageForRequest);
				setUpMetaDataForContentPage(model, pageForRequest);
				model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pageForRequest));
				model.addAttribute(PAGETYPE, "trendlisting");
				model.addAttribute(SEARCH_PAGE_DATA, searchPageData);
				model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_ECONOMIC);
				return getViewForPage(pageForRequest);
			}
		}
		return showErrorNotFoundPage(model, response);
	}

	/**
	 * Json trend results.
	 *
	 * @param searchQuery
	 *           the search query
	 * @param listingId
	 *           the listing id
	 * @param page
	 *           the page
	 * @param showMode
	 *           the show mode
	 * @param sortCode
	 *           the sort code
	 * @return the search results data
	 */
	@ResponseBody
	@RequestMapping(value = "/trend/**/results/", method = RequestMethod.GET)
	public SearchResultsData<ProductData> jsonTrendResults(@RequestParam("q") final String searchQuery,
			@RequestParam("listingId") final String listingId, @RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode)
	{
		final ExtResultsData<ProductData> searchResultsData = new ExtResultsData<>();
		sessionService.setAttribute(TREND_CODE, listingId);
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, siteConfigService.getInt(SEARCH_PAGE_SIZE, 0));
		if (null != searchPageData)
		{
			searchResultsData.setResults(extCustomerFacade.populateMyFavorite(searchPageData.getResults()));
			searchResultsData.setPagination(searchPageData.getPagination());
			searchResultsData.setLoggedInUser(extCustomerFacade.checkIfUserIsLoggedIn());
			searchResultsData.setFavSuccessMessage(getLocalizedString(SimonCoreConstants.FAV_ADDED_MESSAGE));
		}
		return searchResultsData;
	}

	/**
	 * @param searchQuery
	 * @param sortCode
	 * @param model
	 * @param pageForRequest
	 * @param designer
	 * @return
	 */
	private ProductSearchPageData<SearchStateData, ProductData> populateDataForDesigner(final String searchQuery,
			final String sortCode, final Model model, final ContentPageModel pageForRequest, final DesignerModel designerModel)
	{

		sessionService.setAttribute(DESIGNER_CODE, designerModel.getCode());
		model.addAttribute(CATNAME, designerModel.getName().replaceAll(SimonControllerConstants.APPOSTROPHY_CONSTANT,
				SimonControllerConstants.ASCII_APPOSTROPHY_CONSTANT));
		model.addAttribute(DESIGNER, designerModel);
		model.addAttribute(IS_FAV_DESIGNER, extCustomerFacade.isFavDesigner(designerModel.getCode()));
		storeCmsPageInModel(model, pageForRequest);
		setUpMetaDataForContentPage(model, pageForRequest);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, getBreadcrumbs(pageForRequest));
		return extCmsPageFacade.searchListingPageData(searchQuery, sortCode, model, pageForRequest, pageForRequest.getLabel());
	}

	/**
	 * Gets the breadcrumbs for designer layout and designer landing.
	 *
	 * @param pageForRequest
	 *           the page for request
	 * @return the List of {@link Breadcrumb}
	 */
	private List<Breadcrumb> getBreadcrumbs(final ContentPageModel pageForRequest)
	{

		if (CollectionUtils.isNotEmpty(pageForRequest.getDesigner()))
		{
			final DesignerModel designer = pageForRequest.getDesigner().iterator().next();
			final Optional<ContentPageModel> page = designer.getContentPage().stream()
					.filter(p -> ContentPageType.LANDINGPAGE.equals(p.getPageType())).findFirst();
			return getBreadcrumbs(pageForRequest, page);
		}
		else if (CollectionUtils.isNotEmpty(pageForRequest.getDeal()))
		{
			final DealModel deal = pageForRequest.getDeal().iterator().next();
			final Optional<ContentPageModel> page = deal.getContentPage().stream()
					.filter(p -> ContentPageType.LANDINGPAGE.equals(p.getPageType())).findFirst();
			return getBreadcrumbs(pageForRequest, page);


		}
		else if (CollectionUtils.isNotEmpty(pageForRequest.getTrend()))
		{
			final TrendModel trend = pageForRequest.getTrend().iterator().next();
			final Optional<ContentPageModel> page = trend.getContentPage().stream()
					.filter(p -> ContentPageType.LANDINGPAGE.equals(p.getPageType())).findFirst();
			return getBreadcrumbs(pageForRequest, page);
		}
		return Collections.emptyList();

	}

	/**
	 * This method will provide List of breadcrumb on the basis of provided pages.
	 *
	 * @param pageForRequest
	 * @param breadcrumbs
	 * @param page
	 */
	private List<Breadcrumb> getBreadcrumbs(final ContentPageModel pageForRequest, final Optional<ContentPageModel> page)
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		breadcrumbs.add(new Breadcrumb("/", getLocalizedString(SimonCoreConstants.HOME_BREAD_CRUMB), null));
		if (page.isPresent())
		{
			breadcrumbs.add(new Breadcrumb(page.get().getLabel(), page.get().getName(), null));
		}
		if (pageForRequest.getPageType() == ContentPageType.LISTINGPAGE)
		{
			breadcrumbs.add(new Breadcrumb("#", pageForRequest.getName(), null));
		}
		return breadcrumbs;
	}

	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		model.addAttribute(SimonCoreConstants.CANONICAL_URL, setCanonicalInCmsPageInModel(model, cmsPage));
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if (null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.FOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			setMetaData(model, pageForRequest);
		}
	}

	/**
	 * Gets the breadcrumbs for designer layout and designer landing.
	 *
	 * @param model
	 * @param cmsPage
	 * @return String
	 */
	private String setCanonicalInCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		String result = "";

		if (cmsPage instanceof ContentPageModel)
		{
			result = StringUtils.isNotEmpty(((ContentPageModel) cmsPage).getCanonicalTag())
					? ((ContentPageModel) cmsPage).getCanonicalTag() : SimonCoreConstants.NOT_APPLICABLE;
		}
		return result;
	}

	/**
	 * This Method set variable in session on basic if promotion type.
	 *
	 * @param dealModel
	 */
	private void setDealVariableInSession(final DealModel dealModel)
	{
		if (CollectionUtils.isEmpty(dealModel.getPromotionSourceRule()) && CollectionUtils.isNotEmpty(dealModel.getProducts()))
		{
			sessionService.setAttribute(PRODUCT_DEAL_CODE, dealModel.getCode());
		}
		else
		{
			sessionService.setAttribute(DEAL_CODE, dealModel.getCode());
		}
	}

	protected void setMetaData(final Model model, final ContentPageModel pageForRequest)
	{
		final List<MetaElementData> metadata = new LinkedList<>();
		final StringBuilder meta = new StringBuilder();
		if (StringUtils.isNotEmpty((meta.append(pageForRequest.getIndex().toString().toLowerCase()).append(",")
				.append(pageForRequest.getFollow().toString().toLowerCase())).toString()))
		{
			final MetaElementData element = new MetaElementData();
			element.setName("robots");
			element.setContent(meta.toString());
			model.addAttribute("metatags", metadata);
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, meta.toString());
		}
		super.setUpMetaData(model, null, null);
	}

}
