package com.simon.storefront.builder.impl;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.generated.storefront.controllers.pages.ProductPageController;


/**
 * This class is used to create the breadcrumbs for the Product details page.
 */
public class ExtProductBreadcrumbBuilder extends ProductBreadcrumbBuilder
{
	private static final Logger LOG = LoggerFactory.getLogger(ProductPageController.class);

	/**
	 * Returns a list of breadcrumbs for the valid supercategories hiearchy for the given product.
	 *
	 * @param productCode
	 *           represents the unique product code of the product.
	 * @return breadcrumbs which is a list of {@link Breadcrumb} for the given product.
	 */
	@Override
	public List<Breadcrumb> getBreadcrumbs(final String productCode)
	{
		LOG.debug("preparing breadcrumb for {}", productCode);
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Collection<CategoryModel> categoryModels = new ArrayList<>();
		Breadcrumb last;

		final ProductModel baseProductModel = getBaseProduct(productModel);
		last = getProductBreadcrumb(baseProductModel);
		categoryModels.addAll(baseProductModel.getSupercategories());

		breadcrumbs.add(last);

		while (CollectionUtils.isNotEmpty(categoryModels))
		{
			final CategoryModel toDisplay = processCategoryModels(categoryModels);
			categoryModels.clear();
			if (toDisplay != null)
			{
				breadcrumbs.add(getCategoryBreadcrumb(toDisplay));
				categoryModels.addAll(toDisplay.getSupercategories());
			}
		}
		Collections.reverse(breadcrumbs);
		return breadcrumbs;
	}

	/**
	 * Process category models list and returns the valid supercategory for which the breadcrumb {@link Breadcrumb} will
	 * be created in calling method. Very first category of type {@link MerchandizingCategoryModel} will be returned.
	 *
	 * @param categoryModels
	 *           hold the list of {@link CategoryModel} out of which one will be sent back.
	 * @return the category model which is the valid {@link MerchandizingCategoryModel} for which the breadcrumb will be
	 *         created
	 */
	protected CategoryModel processCategoryModels(final Collection<CategoryModel> categoryModels)
	{
		CategoryModel categoryToDisplay = null;

		for (final CategoryModel categoryModel : categoryModels)
		{
			if (categoryModel instanceof MerchandizingCategoryModel)
			{
				if (categoryToDisplay == null && CollectionUtils.isNotEmpty(categoryModel.getSupercategories()))
				{
					categoryToDisplay = categoryModel;
				}
				if (getBrowseHistory().findEntryMatchUrlEndsWith(categoryModel.getCode()) != null)
				{
					break;
				}
			}
		}
		return categoryToDisplay;
	}

}
