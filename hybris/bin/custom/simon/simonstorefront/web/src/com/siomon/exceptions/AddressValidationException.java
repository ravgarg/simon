package com.siomon.exceptions;

/**
 * This exception will be thrown when address validation fails
 */
public class AddressValidationException extends Exception
{

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 2469048504736124426L;

	/** Exception code. */
	private final String code;


	/**
	 * Instantiates a new AddressValidationException exception.
	 *
	 * @param message
	 *           the message
	 */
	public AddressValidationException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new AddressValidationException exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public AddressValidationException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}


	/**
	 * Instantiates a new AddressValidationException exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public AddressValidationException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}




	/**
	 * Instantiates a new integration exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public AddressValidationException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Instantiates a new integration exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 *
	 *
	 */
	public AddressValidationException(final String message, final Throwable cause)
	{
		super(message, cause);
		this.code = "";
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}


}
