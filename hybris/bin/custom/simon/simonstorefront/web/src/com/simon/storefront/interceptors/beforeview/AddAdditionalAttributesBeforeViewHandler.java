package com.simon.storefront.interceptors.beforeview;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.interceptors.BeforeViewHandler;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.storefront.util.CSRFTokenManager;


/**
 * AddAdditionalAttributesBeforeViewHandler adds additional attributes in model and view before page load
 */
public class AddAdditionalAttributesBeforeViewHandler implements BeforeViewHandler
{
	private static final String USER_LOGIN_FORGOTPSWD_URL = "user.login.forgotpassword.url";
	private static final String FORGOT_PSWD_LINK = "forgotPasswordLink";

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource
	private SessionService sessionService;



	/**
	 * Adds {@link PageTemplateModel#getPageStyleId()} as a model attribute named <code>pageStyleId</code>
	 */
	@Override
	public void beforeView(final HttpServletRequest request, final HttpServletResponse response, final ModelAndView modelAndView)
	{
		final AbstractPageModel requestedPage = (AbstractPageModel) modelAndView.getModel()
				.get(AbstractPageController.CMS_PAGE_MODEL);
		if (null != requestedPage)
		{
			modelAndView.addObject("pageStyleId", requestedPage.getMasterTemplate().getPageStyleId());
		}
		if (!userService.isAnonymousUser(userService.getCurrentUser()))
		{
			modelAndView.addObject("isLoggedIn", true);
			modelAndView.addObject("userName", userService.getCurrentUser().getName());
			setIfUserHasProfileAttributesDefaulted(modelAndView);
		}
		modelAndView.addObject("captchaKey", String.valueOf(configurationService.getConfiguration().getString("captcha.sitekey")));
		modelAndView.addObject(FORGOT_PSWD_LINK, configurationService.getConfiguration().getString(USER_LOGIN_FORGOTPSWD_URL));
		final String sessionCsrfToken = getTokenForSession(request);
		modelAndView.addObject(CSRFTokenManager.CSRF_PARAM_NAME, sessionCsrfToken);
	}


	/**
	 * user see a notification every time i sign into SPO.com, if the following attributes have default values in my
	 * profile:
	 *
	 * If Birth Month/Year has 01/1900 If Gender has "Unspecified"
	 *
	 * @param modelAndView
	 *
	 *
	 */
	protected void setIfUserHasProfileAttributesDefaulted(final ModelAndView modelAndView)
	{

		final Boolean loginProfileCompletedMessageCode = sessionService
				.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE);
		sessionService.removeAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE);
		setLoginSuccessCodeForView(modelAndView, loginProfileCompletedMessageCode);
		if (null != loginProfileCompletedMessageCode && loginProfileCompletedMessageCode)
		{
			final UserModel userModel = userService.getCurrentUser();
			if (userModel instanceof CustomerModel)
			{
				final CustomerModel customerModel = (CustomerModel) userModel;
				if (customerModel.getProfileCompleted() == null || !customerModel.getProfileCompleted().booleanValue())
				{
					modelAndView.addObject(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE, Boolean.TRUE);
				}
			}
		}
	}

	/**
	 * This method is used to set the login success flag for analytics.
	 *
	 * @param modelAndView
	 * @param loginProfileCompletedMessageCode
	 */
	private void setLoginSuccessCodeForView(final ModelAndView modelAndView, final Boolean loginProfileCompletedMessageCode)
	{
		final Boolean loginSuccess = sessionService.getAttribute(SimonCoreConstants.LOGIN_SUCCESS_MESSAGE_CODE);
		sessionService.removeAttribute(SimonCoreConstants.LOGIN_SUCCESS_MESSAGE_CODE);
		if ((null != loginProfileCompletedMessageCode && loginProfileCompletedMessageCode)
				|| (null != loginSuccess && loginSuccess))
		{
			modelAndView.addObject(SimonCoreConstants.LOGIN_SUCCESS_MESSAGE_CODE, Boolean.TRUE);
		}
	}

	/**
	 * This method is used to get Token for Session
	 *
	 * @param request
	 * @return token
	 */
	protected String getTokenForSession(final HttpServletRequest request)
	{
		return CSRFTokenManager.getTokenForSession(request.getSession());
	}
}
