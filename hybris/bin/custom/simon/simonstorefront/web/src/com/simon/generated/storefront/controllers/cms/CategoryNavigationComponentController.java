package com.simon.generated.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.CategoryNavigationComponentModel;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.cms.NavigationDTO;
import com.simon.facades.cms.SimonCategoryNavigationFacade;
import com.simon.generated.storefront.controllers.ControllerConstants;


/**
 * Category Navigation Controller for Category Navigation CMS Component. This controller is used to return a DTO that
 * contains all the navigation nodes.
 */
@Controller("CategoryNavigationComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CategoryNavigationComponent)
public class CategoryNavigationComponentController
		extends AbstractAcceleratorCMSComponentController<CategoryNavigationComponentModel>
{

	@Resource
	private SimonCategoryNavigationFacade simonCategoryNavigationFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CategoryNavigationComponentModel component)
	{
		final List<Map<NavigationDTO, List<NavigationDTO>>> navigationDTO = simonCategoryNavigationFacade
				.getNavigationNodes(component);
		model.addAttribute("topNavComponent", navigationDTO);
		if (SimonCoreConstants.LISTING_PAGE_ID.equalsIgnoreCase(component.getNavigationPageType().getCode()))
		{
			model.addAttribute(SimonCoreConstants.LISTING_PAGE_ID, true);
		}
		else
		{
			model.addAttribute(SimonCoreConstants.LISTING_PAGE_ID, false);
		}

	}

	@Override
	protected String getView(final CategoryNavigationComponentModel component)
	{
		if (SimonCoreConstants.PLP_PAGE_ID.equalsIgnoreCase(component.getNavigationPageType().getCode())
				|| SimonCoreConstants.CLP_PAGE_ID.equalsIgnoreCase(component.getNavigationPageType().getCode())
				|| SimonCoreConstants.LISTING_PAGE_ID.equalsIgnoreCase(component.getNavigationPageType().getCode()))
		{
			return ControllerConstants.Views.Cms.ComponentPrefix + SimonCoreConstants.PLP_LEFT_NAVIGATION_PAGE_NAME;
		}

		return super.getView(component);
	}
}
