package com.simon.storefront.renderer;

import de.hybris.platform.acceleratorcms.component.renderer.CMSComponentRenderer;
import de.hybris.platform.cms2.jalo.contents.components.CMSParagraphComponent;
import de.hybris.platform.cms2.model.contents.components.CMSParagraphComponentModel;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;



/**
 * The Class CMSParagraphComponentRenderer overrides the OOTB renderer for ${@link CMSParagraphComponent} . It is used
 * to render paragraph component content without the div tag.
 */
public class CMSParagraphComponentRenderer implements CMSComponentRenderer<CMSParagraphComponentModel>
{

	/**
	 * This method is used to render the CMS paragraph Component content.
	 *
	 * @param pageContext
	 *           of type {@link PageContext} used to create the HTML for the component.
	 * @param component
	 *           of type {@link CMSParagraphComponentModel} provides the content for the paragraph to be displayed.
	 * @throws ServletException
	 *            {@link ServletException} signals that ServletException has occurred.
	 * @throws IOException
	 *            of type {@link IOException} Signals that an I/O exception has occurred.
	 */
	@Override
	public void renderComponent(final PageContext pageContext, final CMSParagraphComponentModel component)
			throws ServletException, IOException
	{
		final JspWriter out = pageContext.getOut();
		out.write(component.getContent() == null ? StringUtils.EMPTY : component.getContent());

	}
}
