package com.simon.storefront.controllers.pages.login;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.generated.storefront.controllers.pages.LoginPageController;
import com.simon.storefront.controllers.SimonControllerConstants;
import com.simon.storefront.util.ExtWebUtils;


/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@RequestMapping(value = "/login")
public class ExtLoginPageController extends LoginPageController
{
	@Resource
	private SessionService sessionService;

	@Resource(name = "extWebUtils")
	private ExtWebUtils extWebUtils;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Override
	protected String getView()
	{
		return SimonControllerConstants.Views.Pages.Account.GlobalLoginPage;
	}

	/**
	 * @description The overridden method use to validate the login credentials and redirect to the login page when user
	 *              enter the something wrong credentials
	 * @method doLogin
	 *
	 */
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public String doLogin(@RequestHeader(value = "referer", required = false) final String referer,
			@RequestParam(value = "error", defaultValue = "false") final boolean loginError, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final HttpSession session)
			throws CMSItemNotFoundException
	{
		if (loginError && !StringUtils.contains(referer, "/login?"))
		{
			storeReferer(referer, request, response);
		}
		if (loginError)
		{
			model.addAttribute("message", sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_MESSAGE_CODE));
		}

		model.addAttribute("enableCaptcha", sessionService.getAttribute(SimonCoreConstants.LOGIN_ERROR_ENABLE_CAPTCHA));
		model.addAttribute("pageType", "login");
		model.addAttribute(SimonControllerConstants.CONTENT_TYPE_ANALYTICS, SimonControllerConstants.CONTENT_TYPE_INFO);
		final ContentPageModel pageForRequest = getContentPageForLabelOrId("login");
		storeCmsPageInModel(model, pageForRequest);
		return getDefaultLoginPage(loginError, session, model);
	}

	/**
	 * Overriding method to set values for robots.txt
	 *
	 * @param model
	 * @param cmsPage
	 */
	@Override
	protected void storeCmsPageInModel(final Model model, final AbstractPageModel cmsPage)
	{
		super.storeCmsPageInModel(model, cmsPage);
		if (null != cmsPage)
		{
			final ContentPageModel pageForRequest = (ContentPageModel) cmsPage;
			boolean flag = false;

			if (null == pageForRequest.getIndex())
			{
				pageForRequest.setIndex(Index.INDEX);
				flag = true;
			}
			if ( null == pageForRequest.getFollow())
			{
				pageForRequest.setFollow(Follow.NOFOLLOW);
				flag = true;
			}
			if (flag)
			{
				modelService.save(pageForRequest);
			}
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, extWebUtils.getMetaInfo(pageForRequest));
		}
	}

}
