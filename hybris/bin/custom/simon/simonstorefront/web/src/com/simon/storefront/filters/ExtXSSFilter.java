package com.simon.storefront.filters;

import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.web.DefaultXSSFilterConfig;
import de.hybris.platform.servicelayer.web.XSSFilter;
import de.hybris.platform.servicelayer.web.XSSMatchAction;
import de.hybris.platform.util.config.ConfigIntf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.simon.storefront.filters.request.wrapper.ExtXSSRequestWrapper;
import com.simon.storefront.filters.value.translator.ExtXSSValueTranslatorImpl;

import atg.taglib.json.util.JSONObject;


/**
 * Custom XSS filter to exclude some specific urls from the XSS Filter and also add XSS filtering on the json payload in
 * the request
 *
 * @see de.hybris.platform.servicelayer.web.XSSFilter#doFilter(javax.servlet. ServletRequest,
 *      javax.servlet.ServletResponse, javax.servlet.FilterChain)
 */
public class ExtXSSFilter extends XSSFilter
{

	private static final int CONSTANT_400 = 400;

	private static final Logger LOG = Logger.getLogger(ExtXSSFilter.class);

	public static final String INIT_PARAM_BYPASS_XSS_KEY = "bypassXSSForHeaders";
	public static final String CONFIG_PARAM_PREFIX = "xss.filter.";
	public static final String SIMON_XSS_EXCLUDE_URL = "simon.xss.filter.exclude.url";
	public static final String SIMON_XSS_EXCLUDE_URL_SEPERATOR = "simon.xss.filter.exclude.url.seperator";

	public static final String CONFIG_RULE_PREFIX_REGEXP = "xss\\.filter\\.rule\\..*(?i)";
	public static final String CONFIG_HEADER_PREFIX_REGEXP = "xss\\.filter\\.header\\..*(?i)";
	public static final String PIPE_SEPARATOR = "|";
	protected static final String CONFIG_HEADER_PREFIX = "xss.filter.header.";
	public static final String CONFIG_ENABLED = "xss.filter.enabled";
	public static final String CONFIG_ACTION = "xss.filter.action";
	private volatile boolean enabled;
	private List<Pattern> patterns;
	private Map<String, String> headers;
	private XSSValueTranslator lazyValueTranslator;
	private XSSMatchAction actionOnPatternMatch;
	private String webroot;
	private XSSFilterConfig config;
	private static final String[] EMPTY = new String[0];
	private boolean bypassXSSForHeaders;

	/**
	 * Initialize from the filter config
	 */
	@Override
	public void init(final FilterConfig filterConfig) throws ServletException
	{
		this.webroot = filterConfig.getServletContext().getContextPath();
		this.bypassXSSForHeaders = null == filterConfig.getInitParameter(INIT_PARAM_BYPASS_XSS_KEY) ? false
				: Boolean.parseBoolean(filterConfig.getInitParameter(INIT_PARAM_BYPASS_XSS_KEY));
		this.initFromConfig(new DefaultXSSFilterConfig(filterConfig));
	}

	/**
	 * Initialize from the filter config
	 */
	@Override
	protected void initFromConfig(final XSSFilterConfig config)
	{
		this.config = config;
		this.actionOnPatternMatch = config.getActionOnMatch();
		this.initPatternsAndHeaders(this.config.isEnabled(), this.config.getPatternDefinitions(), this.config.getHeadersToInject());
		config.registerForConfigChanges(this);
	}

	/**
	 * Intialize the patterns from the property config files that will be used for the XSS filtering
	 */
	@Override
	protected void initPatternsAndHeaders(final boolean enabled, final Map<String, String> patternDefinitions,
			final Map<String, String> headers)
	{
		if (enabled)
		{
			this.patterns = this.compilePatterns(patternDefinitions);
			if (MapUtils.isNotEmpty(headers))
			{
				this.headers = new LinkedHashMap<String, String>();
				for (final Map.Entry<String, String> e : headers.entrySet())
				{
					this.headers.put(e.getKey().replaceAll("xss.filter.header.", ""), e.getValue());
				}
			}
			else
			{
				this.headers = Collections.emptyMap();
			}
			this.lazyValueTranslator = new ExtXSSValueTranslatorImpl(this.patterns, this.bypassXSSForHeaders);
			this.enabled = true;
		}
		else
		{
			this.patterns = Collections.emptyList();
			this.headers = Collections.emptyMap();
			this.enabled = false;
		}
	}

	/**
	 * Reload the patterns if there is any change in the config
	 */
	@Override
	public void reloadOnConfigChange()
	{
		final String infoBefore = this.getSetupInfo();
		this.actionOnPatternMatch = this.config.getActionOnMatch();
		this.initPatternsAndHeaders(this.config.isEnabled(), this.config.getPatternDefinitions(), this.config.getHeadersToInject());
		final String infoAfter = this.getSetupInfo();
		LOG.warn("XSS filter setup changed for " + this.webroot + " from " + infoBefore + " to " + infoAfter + " witch action "
				+ this.actionOnPatternMatch);
	}

	/**
	 * Compile the patterns fetched from the properties files and create a list of patterns out of it
	 */
	@Override
	protected List<Pattern> compilePatterns(final Map<String, String> rules)
	{
		final List<Pattern> ret = new ArrayList<Pattern>(1);
		final StringBuilder exprBuffer = new StringBuilder();
		for (final Map.Entry<String, String> entry : rules.entrySet())
		{
			final String expr = entry.getValue();
			if (StringUtils.isNotBlank(expr))
			{
				if (exprBuffer.length() != 0)
				{
					exprBuffer.append(PIPE_SEPARATOR);
				}
				exprBuffer.append(expr);
			}
		}
		try
		{
			ret.add(Pattern.compile(exprBuffer.toString()));
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error("Error parsing xss filter rule ", e);
		}
		return ret;
	}

	/**
	 * Return the setup configuration info
	 */
	@Override
	protected String getSetupInfo()
	{
		return "enabled:" + this.enabled + "#patterns:"
				+ ((null == this.patterns) ? "<n/a>" : new StringBuilder().append(this.patterns.size()).toString());
	}

	/**
	 * Custom filter to perform the XSS filtering only if the endpoint url is not in the XSS filtering exluded list
	 */
	@Override
	public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain)
			throws IOException, ServletException
	{
		final ConfigIntf cfg = Registry.getMasterTenant().getConfig();
		final String servletPath = ((HttpServletRequest) servletRequest).getServletPath();
		boolean matched = false;
		final StringTokenizer excludeUrlTokenizer = new StringTokenizer(cfg.getString(SIMON_XSS_EXCLUDE_URL, StringUtils.EMPTY),
				cfg.getString(SIMON_XSS_EXCLUDE_URL_SEPERATOR, StringUtils.EMPTY));
		while (excludeUrlTokenizer.hasMoreTokens())
		{
			final String exludeUrl = excludeUrlTokenizer.nextToken();
			if (servletPath.contains(exludeUrl))
			{
				matched = true;
				break;
			}
		}
		if (matched || !this.enabled)
		{
			filterChain.doFilter(servletRequest, servletResponse);
		}
		else
		{
			this.injectHeaders(servletResponse);
			this.processPatternsAndDoFilter(servletRequest, servletResponse, filterChain);
		}
	}

	/**
	 * Inject headers into the response
	 *
	 * @param servletResponse
	 */
	private void injectHeaders(final ServletResponse servletResponse)
	{
		if (MapUtils.isNotEmpty(this.headers) && servletResponse instanceof HttpServletResponse)
		{
			final HttpServletResponse resp = (HttpServletResponse) servletResponse;
			for (final Map.Entry<String, String> me : this.headers.entrySet())
			{
				resp.setHeader(me.getKey(), me.getValue());
			}
		}
	}

	/**
	 * Perform the XSS filtering on the basis of the patterns on the request headers, parameters, and the json payload.
	 */
	@Override
	protected void processPatternsAndDoFilter(final ServletRequest servletRequest, final ServletResponse servletResponse,
			final FilterChain filterChain) throws IOException, ServletException
	{
		if (CollectionUtils.isNotEmpty(this.patterns))
		{
			final HttpServletRequest req = (HttpServletRequest) servletRequest;
			switch (this.actionOnPatternMatch)
			{
				case REJECT:
					rejectReqForValidReqParams(req, filterChain, servletResponse);
					break;
				case STRIP:
					filterChain.doFilter(new ExtXSSRequestWrapper(req, this.lazyValueTranslator), servletResponse);
					break;
				default:
					throw new UnsupportedOperationException(
							"Match action type " + this.actionOnPatternMatch + " is not yet implemented.");
			}
		}
	}

	private void rejectReqForValidReqParams(final HttpServletRequest req, final FilterChain filterChain,
			final ServletResponse servletResponse) throws IOException, ServletException
	{
		final Map<String, String[]> originalParameterMap = req.getParameterMap();
		final Map<String, String[]> parameters = ((ExtXSSValueTranslatorImpl) this.lazyValueTranslator)
				.translateParameters(originalParameterMap);
		if (originalParameterMap.equals(parameters))
		{
			final Map<String, String[]> originalHeaderMap = getValueMapFromHeaders(req);
			final Map<String, String[]> translatedHeaders = ((ExtXSSValueTranslatorImpl) this.lazyValueTranslator)
					.translateHeaders(originalHeaderMap);
			if (originalHeaderMap.equals(translatedHeaders))
			{
				filterChain.doFilter(req, servletResponse);
				return;
			}
		}
		this.setRejectResponseCodes((HttpServletResponse) servletResponse);
	}

	/**
	 * Set the reject response codes if the request is not valid
	 */
	@Override
	protected void setRejectResponseCodes(final HttpServletResponse httpResponse) throws IOException
	{
		httpResponse.setStatus(CONSTANT_400);
		httpResponse.sendError(CONSTANT_400);
	}

	/**
	 * Destroy method to remove the XSS Filtering configuration at the run time
	 */
	@Override
	public void destroy()
	{
		if (this.config != null)
		{
			this.config.unregisterConfigChangeListener();
			this.config = null;
		}
		this.patterns = null;
		this.enabled = false;
	}

	/**
	 * Return the headers value map from the request
	 *
	 * @param request
	 * @return
	 */
	public static Map<String, String[]> getValueMapFromHeaders(final HttpServletRequest request)
	{

		Map<String, String[]> headerValueMap = null;
		final Enumeration<String> headerEnum = request.getHeaderNames();
		while (headerEnum.hasMoreElements())
		{
			final String header = headerEnum.nextElement();
			final Enumeration<String> valuesEnum = request.getHeaders(header);
			if (valuesEnum != null)
			{
				if (headerValueMap == null)
				{
					headerValueMap = initializeLinkedHashMap();
				}
				final List<String> valueList = Collections.list(valuesEnum);
				headerValueMap.put(header,
						valueList.isEmpty() ? ExtXSSFilter.EMPTY : valueList.toArray(instantiateValueListString(valueList)));
			}
		}
		return (headerValueMap == null) ? Collections.emptyMap() : headerValueMap;
	}

	/**
	 * Method to instantiate valueList list of String.
	 *
	 * @param valueList
	 *           the List object.
	 *
	 * @return the array of String.
	 *
	 */
	private static String[] instantiateValueListString(final List<String> valueList)
	{
		return new String[valueList.size()];
	}

	/**
	 * Method to initialize LinkedHashMap.
	 *
	 * @return the Map object.
	 */
	private static Map<String, String[]> initializeLinkedHashMap()
	{
		return new LinkedHashMap<>();
	}

	/**
	 * custom interface for the translator
	 *
	 */
	public interface XSSValueTranslator
	{
		JSONObject translateJsonObject(final JSONObject p0);

		Map<String, String[]> translateParameters(Map<String, String[]> originalNoCaseInsensitiveMap);

		Map<String, String[]> translateHeaders(Map<String, String[]> headerValueMap);
	}
}
