package com.simon.storefront.forms;

public class UpdateSizeForm
{
	private String code;
	private String name;
	private String oldCode;

	public String getCode()
	{
		return code;
	}

	public void setCode(final String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	public String getOldCode()
	{
		return oldCode;
	}

	public void setOldCode(final String oldCode)
	{
		this.oldCode = oldCode;
	}




}
