package com.simon.storefront.validator.form;

import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.RegistrationValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.simon.storefront.forms.ShopperRegistrationForm;


/**
 * This is the class to validate Shopper registration form at server site.
 *
 */
@Component("extRegistrationValidator")
public class ExtRegistrationValidator extends RegistrationValidator
{
	private static final String PWRD_FIELD = "pwd";

	/**
	 * Email Regex
	 */
	public static final Pattern EMAIL_REGEX = Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b");

	/**
	 * PASSWORD_REGEX
	 */
	public static final Pattern PASSWORD_REGEX = Pattern.compile(
			"((((?=.*[A-Z])(?=.*[~!@#$&*%])(?=.*[0-9]))|((?=.*[~!@#$&*%])(?=.*[0-9])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[~!@#$&*%])(?=.*[a-z]))).{8,15})");

	@Override
	public boolean supports(final Class<?> aClass)
	{
		return ShopperRegistrationForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final ShopperRegistrationForm registerForm = (ShopperRegistrationForm) object;

		final String firstName = registerForm.getFirstName();
		final String lastName = registerForm.getLastName();
		final String email = registerForm.getEmail();
		final String pwd = registerForm.getPwd();
		final String checkPwd = registerForm.getCheckPwd();
		final String country = registerForm.getCountry();
		final int birthYear = registerForm.getBirthyear();
		final String birthmonth = registerForm.getBirthmonth();
		final String primaryMall = registerForm.getPrimaryMall();

		if (StringUtils.isBlank(primaryMall))
		{
			errors.rejectValue("primaryMall", "register.primary.mall.invalid");
		}
		validateName(errors, firstName, "firstName", "register.firstName.invalid");
		validateName(errors, lastName, "lastName", "register.lastName.invalid");
		validateCountry(errors, country, "country", "register.country.invalid");
		validateName(errors, birthmonth, "birthmonth", "register.birthmonth.invalid");
		validatebirth(errors, birthYear, "birthyear", "register.birthyear.invalid");

		if (StringUtils.length(firstName) + StringUtils.length(lastName) > 255)
		{
			errors.rejectValue("lastName", "register.name.invalid");
			errors.rejectValue("firstName", "register.name.invalid");
		}

		validateEmail(errors, email);
		validatePassword(errors, pwd);
		comparePasswords(errors, pwd, checkPwd);

	}

	protected void validateCountry(final Errors errors, final String country, final String propertyName, final String property)
	{
		if (StringUtils.isBlank(country))
		{
			errors.rejectValue(propertyName, property);
		}

	}

	protected void validatebirth(final Errors errors, final int monthYear, final String propertyName, final String property)
	{
		if (monthYear <= 0)
		{
			errors.rejectValue(propertyName, property);
		}

	}

	@Override
	protected void validatePassword(final Errors errors, final String pwd)
	{
		if (StringUtils.isEmpty(pwd) || (StringUtils.length(pwd) < 8 || StringUtils.length(pwd) > 16))
		{
			errors.rejectValue(PWRD_FIELD, "register.pwd.invalid");
		}
		else if (!validatePasswordCategory(pwd))
		{
			errors.rejectValue(PWRD_FIELD, "register.pwd.invalid.category");
		}
	}

	/**
	 * This method is used to validate password like Passwords must be between 6 and 15 characters long and contain
	 * characters from three of the following four categories: Uppercase characters, Lowercase characters, Numeric:
	 * 0-9,Non-alphanumeric: ex. ~!@#$%
	 *
	 * @param pwd
	 * @return boolean
	 */
	public boolean validatePasswordCategory(final String pwd)
	{
		final Matcher matcher = PASSWORD_REGEX.matcher(pwd);
		return matcher.matches();
	}




}
