package com.simon.storefront.filters.value.translator;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import atg.taglib.json.util.JSONArray;
import atg.taglib.json.util.JSONException;
import atg.taglib.json.util.JSONObject;


/**
 * Custom value translator for incorporating the methods to perform XSS filtering on the basis of given parameters
 */
public class ExtXSSValueTranslatorImpl implements com.simon.storefront.filters.ExtXSSFilter.XSSValueTranslator
{

	private static final Logger LOG = Logger.getLogger(ExtXSSValueTranslatorImpl.class);
	private final List<Pattern> patterns;
	private final boolean bypassXSSForHeaders;

	public ExtXSSValueTranslatorImpl(final List<Pattern> patterns, final boolean bypassXSSForHeaders)
	{
		this.patterns = patterns;
		this.bypassXSSForHeaders = bypassXSSForHeaders;
	}

	/**
	 * Return the translated Map of the parametes
	 *
	 * @see com.simon.storefront.servicelayer.web.ExtXSSFilter.XSSValueTranslator# translateParameters(java.util.Map)
	 */
	@Override
	public Map<String, String[]> translateParameters(final Map<String, String[]> original)
	{
		return this.stringValuesFromMap(original);
	}

	/**
	 * Return the translated Map of the headers
	 *
	 * @see com.simon.storefront.servicelayer.web.ExtXSSFilter.XSSValueTranslator# translateHeaders(java.util.Map)
	 */
	@Override
	public Map<String, String[]> translateHeaders(final Map<String, String[]> original)
	{
		if (this.bypassXSSForHeaders)
		{
			return original;
		}
		return this.stringValuesFromMap(original);
	}

	/**
	 * Return the translated JsonObject of the given orignal json payload
	 *
	 * @see com.simon.storefront.servicelayer.web.ExtXSSFilter.XSSValueTranslator#
	 *      translateJsonObject(com.google.gson.JsonObject)
	 */
	@Override
	public JSONObject translateJsonObject(final JSONObject original)
	{
		return this.validJsonFromJsonObject(original);
	}

	private Map<String, String[]> stringValuesFromMap(final Map<String, String[]> original)
	{
		if (original != null)
		{
			Map<String, String[]> copy = null;
			for (final Map.Entry<String, String[]> entry : original.entrySet())
			{
				copy = stripXSSFromOriginalValues(original, copy, entry);
			}
			return (copy == null) ? original : copy;
		}
		return original;
	}

	/**
	 * Process the form value one by one and perform XSS filtering if required
	 *
	 * @param original
	 *           of type {@link Map}
	 * @param copy
	 *           of type {@link Map}
	 * @return type {@link Map }
	 */
	private Map<String, String[]> stripXSSFromOriginalValues(final Map<String, String[]> original,
			final Map<String, String[]> copy, final Map.Entry<String, String[]> entry)
	{
		Map<String, String[]> newMap = copy;
		final String[] originalValues = entry.getValue();
		final String[] strippedValues = this.stripXSS(originalValues);
		if (strippedValues != originalValues)
		{
			if (newMap == null)
			{
				newMap = initializeLinkedHashMap(original);
			}
			newMap.put(entry.getKey(), strippedValues);
		}
		return newMap;
	}

	/**
	 * Method to initialize LinkedHashMap
	 *
	 * @param original
	 *           the Map object.
	 *
	 * @return Map map object containing key strings, value array of strings.
	 */
	private Map<String, String[]> initializeLinkedHashMap(final Map<String, String[]> original)
	{
		return new LinkedHashMap<>(original);
	}

	/**
	 * Validate the JSON Object and perform the XSS filtering on the object
	 *
	 * @param original
	 * @return
	 */
	private JSONObject validJsonFromJsonObject(final JSONObject original)
	{
		if (original != null)
		{
			return processJsonObject(original);
		}
		return original;

	}

	/**
	 * Process the Json object by iterating over all the values of the keys and perform XSS filtering if required
	 *
	 * @param json
	 * @return
	 */
	private JSONObject processJsonObject(final JSONObject json)
	{
		final Iterator<?> jsonKeys = json.keys();
		while (jsonKeys.hasNext())
		{
			final String jsonKey = (String) jsonKeys.next();
			try
			{
				final Object jsonObj = json.get(jsonKey);
				if (jsonObj instanceof JSONObject)
				{
					json.put(jsonKey, processJsonObject(json.getJSONObject(jsonKey)));
				}
				else if (jsonObj instanceof JSONArray)
				{
					json.put(jsonKey, processJsonArray((JSONArray) jsonObj));
				}
				else if (jsonObj instanceof String)
				{
					json.put(jsonKey, stripXSS((String) jsonObj));
				}
			}
			catch (final JSONException e)
			{
				LOG.error("Exception occured while processing JsonObject in Xss", e);
			}
		}
		return json;
	}

	/**
	 * Process the Json array by iterating over all the values of the keys and perform XSS filtering if required
	 *
	 * @param jsonArray
	 * @return
	 */
	private JSONArray processJsonArray(final JSONArray jsonArray)
	{
		final Iterator<?> arrayElement = jsonArray.iterator();
		while (arrayElement.hasNext())
		{
			Object jsonObj = arrayElement.next();
			if (jsonObj instanceof JSONObject)
			{
				jsonObj = processJsonObject((JSONObject) jsonObj);
			}
			else if (jsonObj instanceof JSONArray)
			{
				jsonObj = processJsonArray((JSONArray) jsonObj);
			}
			else if (jsonObj instanceof String)
			{
				jsonObj = stripXSS((String) jsonObj);
			}
		}
		return jsonArray;
	}

	/**
	 * Strip the array of the String values by using the XSS rules
	 *
	 * @param values
	 * @return
	 */
	private String[] stripXSS(final String[] values)
	{
		if (values == null || values.length == 0)
		{
			return values;
		}
		String[] copyValues = null;
		int i = 0;
		for (final String vOrig : values)
		{
			final String vStripped = this.stripXSS(vOrig);
			if (!vStripped.equalsIgnoreCase(vOrig))
			{
				if (copyValues == null)
				{
					final int length = values.length;
					copyValues = initializeStringArray(length);
					System.arraycopy(values, 0, copyValues, 0, length);
				}
				copyValues[i] = vStripped;
			}
			++i;
		}
		return (copyValues == null) ? values : copyValues;

	}

	/**
	 * Method to initialize string arary.
	 *
	 * @param length
	 *           integer value.
	 *
	 * @return Array of string values.
	 */
	private String[] initializeStringArray(final int length)
	{
		return new String[length];
	}

	/**
	 * Method to perform the XSS filtering on the given string values on the basis of the patterns
	 *
	 * @param value
	 */
	private String stripXSS(final String value)
	{
		String stripXSSValue = value;
		if (stripXSSValue != null && stripXSSValue.length() > 0)
		{
			final Matcher matcher = Pattern.compile(StringUtils.EMPTY).matcher(stripXSSValue);
			for (final Pattern scriptPattern : this.patterns)
			{
				stripXSSValue = matcher.usePattern(scriptPattern).replaceAll(StringUtils.EMPTY);
			}
		}
		return stripXSSValue;
	}

}
