var theme = window.theme || {};
(function(theme) {

	// TODO make this come from a sys.prop
	var SPEEDY_KEY= 'BB7A7NdGv1cwqNeCj30lphwP47L';
	var INPUT_STYLE = 'background-color: rgb(255, 255, 255); background-image: none; border-bottom-color: rgb(204, 204, 204); border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; border-bottom-style: solid; border-bottom-width: 1px; border-image-outset: 0px; border-image-repeat: stretch; border-image-slice: 100%; border-image-source: none; border-image-width: 1; border-left-color: rgb(204, 204, 204); border-left-style: solid; border-left-width: 1px; border-right-color: rgb(204, 204, 204); border-right-style: solid; border-right-width: 1px; border-top-color: rgb(204, 204, 204); border-top-left-radius: 0px; border-top-right-radius: 0px; border-top-style: solid; border-top-width: 1px; box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset; box-sizing: border-box; color: rgb(25, 33, 43); cursor: auto; display: block; font-family: "Open Sans", Helvetica, Arial, sans-serif; font-size: 14px; font-stretch: normal; font-style: normal; font-variant-caps: normal; font-variant-ligatures: normal; font-variant-numeric: normal; font-weight: normal; height: 32px; letter-spacing: normal; line-height: 20px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; margin-top: 0px; padding-bottom: 6px; padding-left: 12px; padding-right: 12px; padding-top: 6px; text-align: start; text-indent: 0px; text-rendering: auto; text-shadow: none; text-size-adjust: 100%; text-transform: none; transition-delay: 0s, 0s; transition-duration: 0.15s, 0.15s; transition-property: border-color, box-shadow; transition-timing-function: ease-in-out, ease-in-out; user-select: text; width: 100%; word-spacing: 0px; writing-mode: horizontal-tb; -webkit-appearance: none; -webkit-rtl-ordering: logical; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); -webkit-border-image: none;';

	var $formToSubmit;

	theme.initSpreedly = function() {
		var spreedlyAttMap = {
			'number': 'spreedlyCardNumber',
			'cvv': 'spreedlyCVV'
		};

		//initialize the Spreedly IFrame API


		var options =  {
			'numberEl': 'spreedlyCardNumber',
			'cvvEl': 'spreedlyCCV'
		};

		var handlePaymentToken = function (token, formData) {
			var $form = $formToSubmit;

			$('input[name=spreedlyToken]', $form).val(token);
			$('input[name=card_nameOnCard]', $form).val(formData.full_name);
			$('input[name=card_cardType]', $form).val(formData.card_type);
			$('input[name=card_lastFourDigits]', $form).val(formData.last_four_digits);
			$('input[name=card_expirationMonth]', $form).val(formData.month);
			$('input[name=card_expirationYear]', $form).val(formData.year);
			$('input[name=card_cardNumber]', $form).val(formData.number);

			$form.submit();

		};

		Spreedly.on('paymentMethod', handlePaymentToken);

		Spreedly.on('errors', function(errors) {
			var $messageEl = $('#errors .alert');
			var errorBorder = '1px solid red';
			var html = '';
			for(var i = 0; i < errors.length; i++) {
				var error = errors[i];
				if(error['attribute']) {
					var $masterFormElement = $('#'+spreedlyAttMap[error['attribute']]).parents('.form-group');
					if($masterFormElement.length > 0) {
						$masterFormElement.addClass('has-error');
					} else {
						Spreedly.setStyle(error['attribute'], 'border: ' + errorBorder + ';');
					}
				}
				html += error['message'] + '<br/>';
			}
			$messageEl.html(html);
			$messageEl.parent().show();
			$formToSubmit.unblock();

		});

		Spreedly.on('ready', function() {
			Spreedly.setLabel('number', 'Card');
			Spreedly.setLabel('cvv', 'CVV');
			Spreedly.setStyle('number', INPUT_STYLE);
			Spreedly.setStyle('cvv', INPUT_STYLE)
		});

		Spreedly.init(SPEEDY_KEY,options);
	}

	theme.getSpreedlyToken = function(spreedlyArgs, form) {
		$formToSubmit = form;
		Spreedly.tokenizeCreditCard(spreedlyArgs);
	}



})(theme);

ACC.checkout = {

	_autoload: [
		"bindCheckO",
		"bindForms",
		"bindSavedPayments",
		"bindSpreedly"
	],

	bindSpreedly:function(){
		if($("#currentCheckoutStep").val() === 'review') {
			theme.initSpreedly();


			$(document).on("click", ".js-place-order", function (e) {
				ACC.common.blockFormAndShowProcessingMessage($(this));

				var $form = $(e.currentTarget).parents('form');

				e.preventDefault();

				var requiredFields = {};
				var $details = $('#paymentInfoDetails');

				requiredFields['full_name'] = $('input[name=card_nameOnCard]', $details).val();
				requiredFields['month'] = $('select[name=card_expirationMonth] :selected', $details).val();
				requiredFields['year'] = $('select[name=card_expirationYear] :selected', $details).val();

				theme.getSpreedlyToken(requiredFields, $form)

			});

		}

		$(document).on("click", ".submit_silentOrderPostForm", function (e) {
			var form = document.getElementById('silentOrderPostForm');
			ACC.common.blockFormAndShowProcessingMessage($(this));
			form.submit();
		});


	},

	bindForms:function(){

		$(document).on("click","#addressSubmit",function(e){
			e.preventDefault();
			$('#addressForm').submit();
		})

		$(document).on("click","#deliveryMethodSubmit",function(e){
			e.preventDefault();
			$('#selectDeliveryMethodForm').submit();
		})

	},

	bindSavedPayments:function(){
		$(document).on("click",".js-saved-payments",function(e){
			e.preventDefault();

			var title = $("#savedpaymentstitle").html();

			$.colorbox({
				href: "#savedpaymentsbody",
				inline:true,
				maxWidth:"100%",
				opacity:0.7,
				//width:"320px",
				title: title,
				close:'<span class="glyphicon glyphicon-remove"></span>',
				onComplete: function(){
				}
			});
		})
	},

	bindCheckO: function ()
	{
		var cartEntriesError = false;

		// Alternative checkout flows options
		$('.doFlowSelectedChange').change(function ()
		{
			if ('multistep-pci' == $('#selectAltCheckoutFlow').val())
			{
				$('#selectPciOption').show();
			}
			else
			{
				$('#selectPciOption').hide();

			}
		});



		$('.js-continue-shopping-button').click(function ()
		{
			var checkoutUrl = $(this).data("continueShoppingUrl");
			window.location = checkoutUrl;
		});

		$('.js-create-quote-button').click(function ()
		{
			$(this).prop("disabled", true);
			var createQuoteUrl = $(this).data("createQuoteUrl");
			window.location = createQuoteUrl;
		});


		$('.expressCheckoutButton').click(function()
		{
			document.getElementById("expressCheckoutCheckbox").checked = true;
		});

		$(document).on("input",".confirmGuestEmail,.guestEmail",function(){

			var orginalEmail = $(".guestEmail").val();
			var confirmationEmail = $(".confirmGuestEmail").val();

			if(orginalEmail === confirmationEmail){
				$(".guestCheckoutBtn").removeAttr("disabled");
			}else{
				$(".guestCheckoutBtn").attr("disabled","disabled");
			}
		});

		$('.js-continue-checkout-button').click(function ()
		{
			var checkoutUrl = $(this).data("checkoutUrl");

			cartEntriesError = ACC.pickupinstore.validatePickupinStoreCartEntires();
			if (!cartEntriesError)
			{
				var expressCheckoutObject = $('.express-checkout-checkbox');
				if(expressCheckoutObject.is(":checked"))
				{
					window.location = expressCheckoutObject.data("expressCheckoutUrl");
				}
				else
				{
					var flow = $('#selectAltCheckoutFlow').val();
					if ( flow == undefined || flow == '' || flow == 'select-checkout')
					{
						// No alternate flow specified, fallback to default behaviour
						window.location = checkoutUrl;
					}
					else
					{
						// Fix multistep-pci flow
						if ('multistep-pci' == flow)
						{
							flow = 'multistep';
						}
						var pci = $('#selectPciOption').val();

						// Build up the redirect URL
						var redirectUrl = checkoutUrl + '/select-flow?flow=' + flow + '&pci=' + pci;
						window.location = redirectUrl;
					}
				}
			}
			return false;
		});

	}

};
