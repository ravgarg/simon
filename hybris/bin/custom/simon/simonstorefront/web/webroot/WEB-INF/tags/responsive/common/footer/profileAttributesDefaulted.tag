<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty loginProfileCompletedMessageCode && loginProfileCompletedMessageCode eq 'true'}">
<div class="modal fade login-modal-popup" id="globalPopup" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><spring:theme code="text.login.profile.attributes.default.header" /> {<span class="name">${userName}</span>}!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="checkout-leaving-container clearfix">
                    <p><spring:theme code="text.login.profile.attributes.default.thank.create.account" /></p>
                    <p><spring:theme code="text.login.profile.attributes.default.profile.information" /></p>
                    <div class="leaving-btn">
                      <button data-url="/en/my-account/update-profile" type="button" class="btn btn-redirect"><spring:theme code="text.login.profile.attributes.default.ok" /></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</c:if>