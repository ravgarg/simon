<div class="modal fade checkout-leaving-modal" id="checkoutLeavingModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Leaving Checkout</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="checkout-leaving-container clearfix">
                    <p>Are you sure you want to leave checkout?</p>
                    <div class="leaving-btn">
                      <button type="button" class="btn secondary-btn black" data-dismiss="modal">Yes I'm Sure</button>
                      <button type="button" class="btn plum">No, Stayin Checkout</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>