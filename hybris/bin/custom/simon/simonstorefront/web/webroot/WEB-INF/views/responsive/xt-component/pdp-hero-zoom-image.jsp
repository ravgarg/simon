<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section class="product-images-container">
	<div class="gallery-carousel">	
		<div class="slick-top-prev"><span class="icon"></span></div>
		<div class="carousel-stage" 
			id="galleryCarousel">

			<c:forEach var="i" begin="1" end="5">
			<div class="item">
				<picture					
					data-product-img='/_ui/responsive/simon-theme/images/1-1x.jpg'
					data-zoom-img-src="/_ui/responsive/simon-theme/images/1-zoom.jpg">
					<%-- source will contain mobile thumbnail image --%>
					<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/1-mob-thumb.jpg"/>
					<img 
						class="thumbnail img-responsive tail-hide" 
						alt="product thumbnail" 
						src='/_ui/responsive/simon-theme/images/1-thumb.jpg'/>
				</picture>
			</div>
			<div class="item">
				<picture
					data-product-img='/_ui/responsive/simon-theme/images/2-1x.jpg'
					data-zoom-img-src="/_ui/responsive/simon-theme/images/2-zoom.jpg">
					<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/2-mob-thumb.jpg"/>
					<img 
						class="thumbnail img-responsive tail-hide" 
						alt="product thumbnail" 
						src='/_ui/responsive/simon-theme/images/2-thumb.jpg'/>
				</picture>
			</div>
			</c:forEach>
		</div>
		<div class="slick-bottom-next"><span class="icon"></span></div>
			
	</div>
	<div class="product-image">
		<div class='zoom' id="pdpimage"></div>
		<a href="javascript:void(0);" class="zoom-cross"></a>
		<a href="javascript:void(0);" class="zoom-plus-icon"></a>
	</div>
</section>
