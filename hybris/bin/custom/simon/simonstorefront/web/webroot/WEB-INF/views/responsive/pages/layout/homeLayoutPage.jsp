<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<template:page pageTitle="homepage">

	<div class="owl-carousel analytics-target" id="homepageCarousel">
		<cms:pageSlot position="csn_HeroSlot" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>

		<div class="container">
			<div class="custom-component">
				<div class="row">
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="csn_ContentSpotsASlot" var="feature" element="div">
							<div class="img-container">
								<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>
			
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="csn_ContentSpotsBSlot" var="feature" element="div">
							<div class="img-container">
								<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>
			
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="csn_ContentSpotsCSlot" var="feature" element="div">
							<div class="img-container">
								<cms:component component="${feature}"/>
							</div>	
						</cms:pageSlot>
					</div>
				</div>
			</div>

		<cms:pageSlot position="csn_Promo1Slot" var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
		</div>	


	<div class="container">
		<div class="trending-now common-heading">
				<cms:pageSlot position="csn_TrendingNowSlot" var="feature" element="div" class="span-24">
					<cms:component component="${feature}" />
				</cms:pageSlot>
		</div>	
	</div>
	<div class="container">
		<cms:pageSlot position="csn_Promo2Slot" var="feature" element="div" class="span-24 section5 cms_disp-img_slot">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
	<div class="container">
		<div class="style-deals common-heading">
				<cms:pageSlot position="csn_StyleDealsSlot" var="feature" >
				    <cms:component component="${feature}" />
				</cms:pageSlot>
		</div>	
	</div>

	<div class="container">
		<cms:pageSlot position="csn_Promo3Slot" var="feature" element="div" class="span-24 section5 cms_disp-img_slot">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
	<div class="social-shop">
		<div class="container">
			<div class="row">
				<div class="social-shop-containers  col-xs-12 col-md-8">
					<div class="row">
						<div id="carousalFooter">
							<cms:pageSlot position="csn_SocialShopSlot" var="feature">
								<div class="social-shop-content item col-xs-12 col-md-4">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<cms:pageSlot position="csn_SocialShopTextSlot" var="feature" >
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>			
	</div>	
	<cms:pageSlot position="csn_TextContentSlot" var="feature" element="div" class="span-24 section5 cms_disp-img_slot">
		<cms:component component="${feature}" />
	</cms:pageSlot>

</template:page>

