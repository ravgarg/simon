
<div class="email-preferences row">
	<div class="email-content col-md-12">
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
		<ul>
			<li>Lorem ipsum dolor sit amet</li>
			<li>Consectetur adipiscing elit. Fusce pellentes</li>
			<li>Risus in enim porta aliquam aenean at felis.</li>
		</ul>
		<div class="row">
	    	<label class="checkboxes col-md-12">
	           <span class="sm-input-group field-float">
	              <input type="checkbox" name="confirm">
	           </span>
	           Unsubscribe from Shop Premium Outlet emails
	        </label>
	       	<div class="submit-btn col-md-6 col-xs-12">
	       		<button class="btn">Save</button>
	       	</div>
	    </div>
    </div>
</div>