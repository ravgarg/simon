/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define([ 'jquery'], 
	function($) {
    'use strict';
    var myPremiumOutlets = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.paintFirstimage();
        },
        initVariables: function() {

        },
        initAjax: function() {},
        initEvents: function() {},
        paintFirstimage:function(){
        	
        }
	}
      
    $(function() {
    	myPremiumOutlets.init();
    });

    return myPremiumOutlets;
});