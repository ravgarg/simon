<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<c:if test="${not empty storeList and not empty storeList.allStoresList}">
	<input type="hidden" data-fav-url="true" 
		value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />
	<c:choose>
		<c:when test="${alignment eq 'LeftAligned'}">
			<a href="#subMenuL1Id11" class="accordion-menu"
				data-toggle="collapse"><spring:theme
							code="text.account.myStores.store.text" /></a>
			<ul class="level-1 stores collapse in" id="subMenuL1Id11">
				<c:forEach items="${storeList.allStoresList}" var="shop">
					<c:set var="shopURL">
						<c:url value="${shop.contentPageLabel}"/>
					</c:set>
					<li><a href="${shopURL}">${shop.name}</a></li>
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			<div class="current-trends stores">
				<h3 class="page-heading major">
					<spring:theme code="text.account.myStores.allstores.text" />
				</h3>
				<div class="row margin-zero">
					<c:forEach items="${storeList.allStoresList}" var="shop">
						<c:choose>
							<c:when test="${not empty shop.backgroundImage}">
								<c:set var="backgroundImage" value="${shop.backgroundImage}" />
							</c:when>
							<c:otherwise>
								<c:set var="backgroundImage"
									value="/_ui/responsive/simon-theme/images/no-image.jpg" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.mobileBackgroundImage}">
								<c:set var="mobileBackgroundImage" value="${shop.mobileBackgroundImage}" />
							</c:when>
							<c:otherwise>
								<c:set var="mobileBackgroundImage"
									value="/_ui/responsive/simon-theme/images/store-no-image-mobile.png" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.logo}">
								<c:set var="logo" value="${shop.logo}" />
							</c:when>
							<c:otherwise>
								<c:set var="logo" value="" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.mobileLogo}">
								<c:set var="mobileLogo" value="${shop.mobileLogo}" />
							</c:when>
							<c:otherwise>
								<c:set var="mobileLogo" value="" />
							</c:otherwise>
						</c:choose>
						<div class="store col-md-4 col-xs-12">
							<div class="store-content">
								<div class="box-background">
								<a href="${shop.contentPageLabel}">
																		<picture>
									<source media="(max-width: 767px)"
										srcset="${mobileBackgroundImage}">
									<source media="(min-width: 768px) and (max-width: 991px)"
										srcset="${backgroundImage}">
									<img class="img-responsive" alt="${altTextBGImage}" style=""
										src="${backgroundImage}"> </picture>
								</a>
								</div>
								<div class="fav-the-store add-to-favorites">
											<input type="hidden" value="${shop.id}" class="base-code" /> 
											<input type="hidden" value="${shop.name}" class="base-name" />
											<input type="hidden" value="RETAILERS" class="base-type" /> 	
																		 
										<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
											<a href="javascript:void(0);" 
												data-fav-product-code="${shop.id}"
												data-fav-name="${shop.name}" data-fav-type="RETAILERS" 
												class="icon-fav-blank fav-icon login-modal openLoginModal analytics-loginFlyOut" 
												data-analytics='{"event":{"sub_type":"login_initiation","type":"login"},"login": {"stage": "login_start"}}' 
												data-satellitetrack="login">
											</a>
										</sec:authorize>
										<div class="analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="my_stores" data-analytics-linkplacement="my_stores" data-analytics-component="my_stores">
											<input type="hidden" value="${shop.id}" class="base-code analytics-base-code" /> 
											<input type="hidden" value="${shop.name}" class="base-name analytics-base-name" />
											<input type="hidden" value="RETAILERS" class="base-type" /> 
											<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
												<a href="javascript:void(0);" class="mark-favorite fav-icon <c:if test='${shop.favorite}'>marked</c:if>"></a>
											</sec:authorize>
										</div>
								</div>
								<div class="center-content">
								<a href="${shop.contentPageLabel}">
									<picture>
										<source media="(max-width: 767px)" srcset="${logo}">
										<source media="(min-width: 768px) and (max-width: 991px)"
											srcset="${mobileLogo}">
										<img class="img-responsive" alt="${altTextMobileLogo}" style="" src="${logo}">
									</picture>
								</a>
								</div>
								<div class="shop-now">
									<a class="analytics-genericLink analytics-storeClick" data-storeval="${shop.name}" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="shop-now" data-analytics-linkname="shop-now" href="${shop.contentPageLabel}"><spring:theme code="text.account.myStores.shopnow.text" /></a>
								</div>
							</div>
						</div>

					</c:forEach>

				</div>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>
