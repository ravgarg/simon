<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
	
	<form:form action="cartAction" method="post" id="cartForm">
   </form:form> 
	<div class="cart-heading">
		<span class="cart-bag">${cartData.messageLabels.myShoppingBagLabel}</span><span
			class="cart-item total-item-number">(<span>${cartData.totalUnitCount}</span> Items)
		</span>
	</div>
	<div class="cart-condition col-md-8">${cartData.messageLabels.itemPriceLabel}</div>

	