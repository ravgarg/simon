<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<template:page pageTitle="${pageTitle}">
	<div class="container analytics-myPages">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="breadcrumbs col-md-3 hide-mobile">
						<c:forEach items="${breadcrumbs}" var="breadcrumbs"
							varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}"><c:if test="${status.last}">
									<span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}</a>
						</c:forEach>
					</div>
					<c:if test="${not empty accountPageNotificationUpdateKey}">
						<div class="col-md-9">
						<c:choose>
							<c:when test="${not empty accountPageNotificationErrorKey}">
							<div class="error-msg analytics-formSubmitFailure"> 
											<spring:theme code="${accountPageNotificationUpdateKey}" />
										</div>
							</c:when>
							<c:otherwise>
							<div class="profile-update-msg analytics-formSubmitSuccess"> 
											<spring:theme code="${accountPageNotificationUpdateKey}" />
										</div>
							</c:otherwise>
						
						</c:choose>
									
						</div>
					</c:if>

					
				</div>
			</div>

			<cms:pageSlot position="csn_leftNavigation-accountPage" var="feature"
				element="div" class="account-section-content">
				<cms:component component="${feature}" />
			</cms:pageSlot>

			<div class="col-md-9">
			<div class="${pageLevelClass} page-container favorite-container myoffers-container">
				<button
					class="col-xs-12 btn secondary-btn black major hide-desktop my-outlets" data-toggle="modal" data-target="#myoutlets">
					<spring:theme code="myPremiumOutletKey" />
				</button>
				
				<h1 class="page-heading fav-icon"><spring:theme code="${titleKey}" />
					<c:choose>
						<c:when test='${"my-center" eq pageId}'>
	       					<a href="/my-account/update-profile" class="add-deals hidden-xs analytics-genericLink" data-analytics-eventsubtype="how-to-add-favourite" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>||how-to-add-favourite" data-analytics-linkname="how-to-add-favourite">
								<spring:theme code="${howToAddKey}"/>
							</a>
	    				</c:when> 
    				<c:otherwise>   
						<a href="#" class="add-deals hidden-xs analytics-genericLink" data-analytics-eventsubtype="how-to-add-favourite" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>||how-to-add-favourite" data-analytics-linkname="how-to-add-favourite" data-toggle="modal" data-target="#moadl1Content" data-key="${favPopupMappingClass}">
							<spring:theme code="${howToAddKey}"/>
						</a>
					</c:otherwise>
					</c:choose>
				</h1>



					<div class="myfavorites-container">

						<cms:pageSlot position="csn_myAccountTitleContentSlot"
							var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						<c:choose>
							<c:when test='${"my-center" eq pageId}'>
		       					<a href="/my-account/update-profile" class="add-deals visible-xs analytics-genericLink" data-analytics-eventsubtype="how-to-add-favourite" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>||how-to-add-favourite" data-analytics-linkname="how-to-add-favourite">
									<spring:theme code="${howToAddKey}"/>
								</a>
		    				</c:when> 
		    				<c:otherwise>   
								<a href="#" class="add-deals visible-xs" data-toggle="modal" data-target="#moadl1Content" data-key="${pageLevelClass}"><spring:theme code="${howToAddKey}" /></a>
							</c:otherwise>
						</c:choose>
						
								
					  <c:if test="${not empty myFavProduts}">
							<product:productTilesListing searchPageData="${myFavProduts}" />
						</c:if>
						<c:choose>
							<c:when test="${pageLevelClass eq 'my-premium-outlets' or pageLevelClass eq 'my-center'}">
								<div class="myoutlets-container analytics-favoriteCenter">
									<div class="outlet-items">
										<div class="row">
											<cms:pageSlot position="csn_noSavedDetailContentSlot" var="feature">
												<div class="outlet-items-detail col-xs-12 col-md-4">
												<cms:component component="${feature}" />
												</div>
											</cms:pageSlot>
										</div>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<cms:pageSlot position="csn_noSavedDetailContentSlot" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
						
								<div class="current-trends analytics-featuredcomponents clearfix" data-analytics-storetype="featuredcomponent">
									<c:if test="${not empty cmsComponent1titleKey}">
										<h3 class="major page-heading">
											<spring:theme code="${cmsComponent1titleKey}" />
										</h3>
									</c:if>
									<c:if test="${cmsPage.uid ne 'my-deals'}">
                              			<cms:pageSlot position="csn_myAccountCMSManagedContentSlot1"
											var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
                              	 </c:if>
										
								</div>
							</c:otherwise>
						</c:choose>

						<c:if test="${not empty cmsComponent2titleKey}"> <div class="current-trends analytics-featureStores comming-soon row" data-analytics-storetype="comming-soon"> <h3 class="major page-heading"><spring:theme
						code="${cmsComponent2titleKey}" /></h3><div class="row"></c:if>
						
							<cms:pageSlot position="csn_myAccountCMSManagedContentSlot2"
								var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						
						<c:if test="${not empty cmsComponent2titleKey}"> </div> </div></c:if>
					</div>
					
					
				</div>
		</div>
	</div>
	</div>
	
	
	<div class="modal fade modal-list" id="myoutlets" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title">My Premium Outlets</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<cms:pageSlot position="csn_leftNavigation-accountPage" var="feature"
						element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>		
	</div>
</template:page>