<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

  <!-- Start: Mobile Category Browse model-->
	<div class="modal fade plp-filters-modal" id="mBrowseCategories" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><spring:theme code='plp.filter.category' /></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body clearfix">
					<div id="mbrowseContent" class="plp-filters"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="plp-filters show-mobile">
	<div class="browse-categories">
		<button 
			id="mBrowseCategoriesBtn"
			class="btn block black secondary-btn" 
			type="button"
			data-toggle="modal" 
			data-dismiss="modal" 
			data-target="#mBrowseCategories">
			<spring:theme code="text.clp.browse.categories" />
		</button>
	</div>
    </div>