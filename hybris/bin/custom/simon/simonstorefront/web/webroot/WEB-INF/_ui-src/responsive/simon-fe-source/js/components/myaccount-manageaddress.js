/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'utility', 'templates/shippingEasypost.tpl', 'checkoutShipping', 'ajaxFactory', 'templates/myAccountAddEditAddress.tpl', 'templates/confirmationTemplate.tpl','masking','analytics'], function($, utility, easypostFormTemplate, checkoutShipping, ajaxFactory, myAccountAddEditAddressTemplate, confirmationTemplate,masking,analytics) {
    'use strict';
    var cache;
    var myaccountAddress = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.usphoneValidation();
            //Expand Left Navigation 'My Acount' Section if user lands on any My Account page
            if($('.left-menu-container .left-menu .active').length) {
            	$('.left-menu-container .left-menu').find('.active').parent('.level-1').siblings('.accordion-menu').trigger('click');
            }
        },

        initVariables: function() {
            cache = {
                $document : $(document),
                $addressForm : $('#addressForm'),
                $resourceBundle : $('#resourceBundle').data('label'),
                $savedAddressesNumber : $('.address').length,
                $analyticsError : []
            }
        },

        initAjax: function() {},

        initEvents: function() {
            cache.$document.on('click','#addAddress', myaccountAddress.addressformSubmit);
            cache.$document.on('click','.addEditAddress', myaccountAddress.openAddEditAddressModal);
            cache.$document.on('click','.delete-address-link', myaccountAddress.openConfirmationModal);
            cache.$document.on('click','.edit-delete-address-box input[name="defaultAddress"]', myaccountAddress.makeDefaultAddress);
            cache.$document.on('click','#mAccountBtn', myaccountAddress.openMobileLeftNavigationModal);

            //Loop for calculating height of Saved Addresses and setting max height in each row of address

            for (var i = 0; i < cache.$savedAddressesNumber; i = i+3) {
                var firstElementHeight = $('.address').eq(i).outerHeight(),
                    secondElementHeight = $('.address').eq(i+1).outerHeight(),
                    thirdelementHeight = $('.address').eq(i+2).outerHeight(),
                    maxHeight = Math.max(firstElementHeight, secondElementHeight, thirdelementHeight);

                $('.address .address-border').eq(i).outerHeight(maxHeight);
                $('.address .address-border').eq(i+1).outerHeight(maxHeight);
                $('.address .address-border').eq(i+2).outerHeight(maxHeight);
            }
        },
        
        addressformSubmit :function(event){
        	event.preventDefault();
        	if($('#addressForm').parsley().validate() === true){
        		myaccountAddress.validateAddress(event);
        	}
        	else{
        		if($('#analyticsSatelliteFlag').val()==='true'){
					// PAYMENT PANEL - SPREEDLY ERRORS - setTimeout - formsubmit
					jQuery.unique(cache.$analyticsError);
					
					var $param = {
							formname : '#addressBookModal form.analytics-formValidation',
							formerror : cache.$analyticsError.join('|')
						};
					analytics.formSubmitValidation($param);
				 }
        	}
        	
        	
        },
        usphoneValidation: function() {
            $("#us-phoneno-validation,[data-role='phoneNumberValidation']").mask("(999) 999-9999");
            $("#us-phoneno-validation,[data-role='phoneNumberValidation']").on("blur", function() {
                var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );
                if( last.length === 5 ) {
                    var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );
                    var lastfour = last.substr(1,4);
                    var first = $(this).val().substr( 0, 9 );
                    $(this).val( first + move + '-' + lastfour );
                }
            });
        },
        openAddEditAddressModal: function(e){
                var data = cache.$resourceBundle;

                $('#myAccountVerifyAddressModal').modal('hide');
                data.addressLabel= cache.$resourceBundle.addnewaddressLabel;
                data.buttonLabel= cache.$resourceBundle.addAddressButtonLabel;
                data.firstName = "";
                data.lastName= "";
                data.line1= "";
                data.line2= "";
                data.town= "";
                data.postalCode= "";
                data.phone= "";
                data.CSRFToken = $('input[name=CSRFToken]').val();
                
                if($(e.target).data('action') === 'editaddress'){
                    $.extend(data, $(e.target).data('address'));
                    data.addressLabel= cache.$resourceBundle.editAddressLabel;
                    data.buttonLabel = cache.$resourceBundle.editButtonLabel;
                }

                $('#addressBookModal').modal('show').find('.modal-content').html(myAccountAddEditAddressTemplate(data));
                utility.floatingLabelsInit();
        },

        validateAddress:function(e){
                e.preventDefault();
                if($('#addressForm .defaultAddressCheckbox').prop('checked')) {
                    $('#addressForm input[name=defaultAddress]').val(true);
                }

                var formData = $('#addressForm').serialize();
                var options = {
                    'methodType': 'POST',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': '/my-account/verify-add',
                    'isShowLoader': false,
                    'cache' : true
                }
                
                ajaxFactory.ajaxFactoryInit(options, function(response){
                    $("#addressBookModal").modal('hide');
                    $('#myAccountVerifyAddressModal').modal('show').find('.modal-content').html(easypostFormTemplate(response));
                    $('#myAccountVerifyAddressModal').on('click','button.useThisAddress', myaccountAddress.saveAddress);
                    $('#myAccountVerifyAddressModal').on('click','.suggestedAddress', myaccountAddress.suggestedAddress);
                    $('#myAccountVerifyAddressModal').on('click','.userEntered', myaccountAddress.userEntered);
                });
                
                return false;
        },

        suggestedAddress: function(){
            $('.edit-address-btn').addClass('hide');
            $('.suggest-address-btn').removeClass('hide');
            $('.userEntered').removeAttr('checked');
            $('.suggestedAddress').attr('checked');
        },

        userEntered: function(){
            $('.edit-address-btn').removeClass('hide');
            $('.suggest-address-btn').addClass('hide');
            $('.userEntered').attr('checked');
            $('.suggestedAddress').removeAttr('checked');
        },

        saveAddress:function(e){
                e.preventDefault();
                
                $('#suggestedAddress input[name=CSRFToken], #userEnteredAddress input[name=CSRFToken]').val($('input[name=CSRFToken]').val());

                $('#suggestedAddress input[name=defaultAddress], #userEnteredAddress input[name=defaultAddress]').val($('#addressForm input[name=defaultAddress]').val());

                var formData;

                if($('input[class="suggestedAddress"]:checked').attr('class') === "suggestedAddress") {
                    formData = $('#suggestedAddress').serialize();
                }

                if($('input[class="userEntered"]:checked').attr('class') === "userEntered") {
                    formData = $('#userEnteredAddress').serialize();
                }

                var $url = '/my-account/addedit-address',
					options = {
						'methodType': 'POST',
						'dataType': 'JSON',
						'methodData': formData,
						'url': $url,
						'isShowLoader': false,
						'cache' : true
					};
				
                ajaxFactory.ajaxFactoryInit(options, function(response){
						if($('#analyticsSatelliteFlag').val()==='true'){							
							if(response.status === undefined){
								analytics.analyticsAJAXFailureHandler('add_new_address',response.messageLabels,'CE');
							}else{
								var $data = {
									formname : 'add_new_address',
									formnametype : 'address_book',
									type : 'registration',
									stage : 'add_new_address_completion',
									satelliteTrack : 'registration'
								};
								analytics.analyticsAJAXSuccessHandler($data);
							}
						}
                        var url = window.location.href.replace(/\?.+/, '');

                        window.location.href = url;
                }, '','add_new_address'); 
                e.stopImmediatePropagation();
        },

        openConfirmationModal:function(e){
            $('.myaccount .address-book .addressBookModal.confirmationModal').remove();
            var data = cache.$resourceBundle;
            $(this).parents('.address-box-margin').append(confirmationTemplate({
                data: data,
                addressId: $(e.target).data('addressdata').addressId,
                addressName: $(e.target).data('addressdata').address,
                CSRFToken: $('input[name=CSRFToken]').val()
            }));
            $('#confirmDeleteModal').modal('show');
        },

        makeDefaultAddress:function(e){
            var addressId = $(e.target).data("addressid"),
            	CSRFToken = $('input[name=CSRFToken]').val();

            var options = {
                'methodType': 'POST',
                'dataType': 'JSON',
                'methodData': {"addressid" : addressId, "CSRFToken": CSRFToken},
                'url': '/my-account/set-default-address',
                'isShowLoader': false,
                'cache' : true
            }
            ajaxFactory.ajaxFactoryInit(options, function(){            	

            }); 
        },
        
        openMobileLeftNavigationModal:function() {
        	$('#mAccount').html($('.myaccount .account-section-content').html());
        	$('.myaccount .account-section-content').first().remove();
        	$('#mAccount .left-menu-container').removeClass('show-desktop');
        }
        
    }
      
    $(function() {
        myaccountAddress.init();
    });

    return myaccountAddress;
});