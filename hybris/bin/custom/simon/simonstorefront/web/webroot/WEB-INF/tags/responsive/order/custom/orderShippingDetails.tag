<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="shipping-heading major"><spring:theme code='order.confirmation.shipping.address.txt'/></div>
		<div class="shipping-address"><p class="margin-bottom-zero">${order.deliveryAddress.firstName}&nbsp;${order.deliveryAddress.lastName}</p><p class="margin-bottom-zero"> ${order.deliveryAddress.line1}</p><p class="margin-bottom-zero">${order.deliveryAddress.line2}</p><p class="margin-bottom-zero" data-analytics-state="${order.deliveryAddress.region.isocodeShort}" data-analytics-zip="${order.deliveryAddress.postalCode}">${order.deliveryAddress.town},&nbsp;${order.deliveryAddress.region.isocodeShort},&nbsp;${order.deliveryAddress.postalCode}</p>

			</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="payment-heading major"><spring:theme code='order.confirmation.payment.method.txt'/></div>
		<div class="payment-method" data-analytics-payment="${order.paymentInfo.cardType}">${order.paymentInfo.cardType}</div>
	</div>
</div>