<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<ul class="nav navbar-nav myaccount-dropdown111 navbar-right support-menu">
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle help-link"><spring:theme code="header.link.support"/></a>
        <ul class="dropdown-menu">
            <c:forEach items="${feature.links}" var="subMenu">        
                        <c:url value="${subMenu.url}" var="linkUrl" />
	                    <li><a href="${linkUrl}">${subMenu.name}</a></li>
           </c:forEach>
        </ul>
    </li>
</ul>

