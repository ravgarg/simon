﻿
<div class="container">
	<div class="the-designer-shop active">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/confirmation-image2@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/confirmation-image.png">
			</picture>
			<div class="content-container">
				<h1 class="display-medium major">SEE WHAT'S</h1>
				 <div class="designer-banner-text">TRENDING NOW</div>
				 <span class="shopnow major">DISCOVER MORE <span>›</span></span> 
			</div>
		</a>
	</div>
</div>
