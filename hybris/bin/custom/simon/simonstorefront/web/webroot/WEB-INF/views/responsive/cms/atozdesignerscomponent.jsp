<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="analytics-pageContentLoad"
	data-analytics-pagetype="${fn:toLowerCase(pageType)}"
	data-analytics-subtype="${fn:toLowerCase(pageType)}"
	data-analytics-contenttype="${content_type}">
	<c:set var="designerSet" value="${activeDesignerList}" scope="request"></c:set>
	<c:choose>
		<c:when test="${not empty activeDesignerList}">
			<div class="custom-heading-links-component">
				<h3 class="major heading-border">
					<spring:theme code="text.account.myDesigners.title" />
				</h3>
				<div class="analytics-favoriteDesigners" data-analytics-length="designers">
					<jsp:include page="../pages/account/displayMyDesigners.jsp" />
				</div>
			</div>
		</c:when>
	</c:choose>
	<div class="designers-pagination-wrapper">
		<nav>
			<ul class="pagination">
				<c:forEach var="letter" items="${alphabetMap}">
					<c:choose>
						<c:when test="${not empty letter.value}">
							<li><a href="#${letter.key}${letter.key}"
								data-pag-target="${letter.key}${letter.key}">${letter.key}</a></li>
						</c:when>
						<c:otherwise>
							<li><a class="disabled">${letter.key}</a></li>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</ul>
		</nav>

		<div class="designer-content-wrapper favorite-container analytics-designerAZ">
			<input type="hidden" data-fav-url="true"
				value='{"add":"/my-account/add-designer", "remove":"/my-account/remove-designer" }' />
			<div class="designer-container">
				<c:forEach var="letter" items="${alphabetMap}">
					<div class="links-component">
						<h3 id="${letter.key}${letter.key}" class="major">${letter.key}</h3>
							<c:forEach var="designerItem" items="${letter.value}">
								<div class="col-md-4 custom-links add-to-favorites">
									<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
									<input type="hidden" value="${designerItem.name}" class="base-name analytics-base-name" /> 
										<a href="javascript:void(0);"
											data-fav-product-code="${designerItem.code}"
											data-fav-name="${designerItem.name}"
											data-fav-type="DESIGNERS"
											class="favor_icons openLoginModal"></a>
										<a href="/${designerItem.bannerUrl}" class="analytics-genericLink analytics-designerClick" data-designerval="${designerItem.name}" data-analytics-eventtype="designer_a_z_click" data-analytics-eventsubtype="my_designer_a_z" data-analytics-linkplacement="my_designer_a_z" data-analytics-linkname="${designerItem.name}">${designerItem.name}</a>
									</sec:authorize>
									<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
									<div class="analytics-addtoFavorite" data-analytics-pagestyle="myaccountfavorite" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="my_designer_a_z" data-analytics-linkplacement="my_designer_a_z" data-analytics-component="designer">
									
									<input type="hidden" value="${designerItem.code}" class="base-code analytics-base-code" /> 
									<input type="hidden" value="${designerItem.name}" class="base-name analytics-base-name" /> 
									<input type="hidden" value="DESIGNERS" class="base-type" />
									
										<a href="javascript:void(0);"
											class="markFav mark-favorite favor_icons <c:if test='${designerItem.favorite}'>marked</c:if>" ></a>
										<a href="/${designerItem.bannerUrl}" class="analytics-genericLink analytics-designerClick" data-designerval="${designerItem.name}" data-analytics-eventtype="designer_a_z_click" data-analytics-eventsubtype="my_designer_a_z" data-analytics-linkplacement="my_designer_a_z" data-analytics-linkname="${designerItem.name}">${designerItem.name}</a>
									</div>
									</sec:authorize>
								</div>
							</c:forEach>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>