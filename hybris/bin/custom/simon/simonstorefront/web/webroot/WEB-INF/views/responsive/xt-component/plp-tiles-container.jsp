<div class="row product-container product-list">
    <div class="item-tile item-responsive col-md-4 col-xs-6">
        <jsp:include page="global-product-tile.jsp"></jsp:include>
    </div>
    <div class="item-tile item-responsive col-md-4 col-xs-6">
        <jsp:include page="global-product-tile.jsp"></jsp:include>
    </div>

    <div class="item-tile item-responsive col-md-4 col-xs-6">
        <jsp:include page="global-product-tile.jsp"></jsp:include>
    </div>

    <div class="item-tile item-responsive col-md-4 col-xs-6">
        <jsp:include page="global-product-tile.jsp"></jsp:include>
    </div>

    <div class="item-tile item-responsive col-md-4 col-xs-6">
        <jsp:include page="global-product-tile.jsp"></jsp:include>
    </div>
</div>

<div class="row load-more-btn-wrapper">
    <button id="loadMore" class="btn btn-primary load-more block">Load More</button>
</div>