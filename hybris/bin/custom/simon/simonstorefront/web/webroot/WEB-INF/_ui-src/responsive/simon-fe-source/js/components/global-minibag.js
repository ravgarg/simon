/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'templates/minicart.tpl', 'handlebars', 'ajaxFactory', 'utility', 'hbshelpers', 'viewportDetect', 'favorite','analytics'], 
    function($, miniCartTemplate, handlebars, ajaxFactory, utility, hbshelpers, viewportDetect, favorite,analytics) {
    'use strict';
    var cache = {};
    var globalMinibag = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.initAjax();
            this.initMiniBagAjax();
            this.initBlurScreen();
        },
        initVariables: function() {
            cache = {
                $document: $(document),
                $window: $(window),
                $minbagIcon: $('.minicart'),
                $miniBag: $('.mini-cart'),
                $megaMenuBlur: $('.mega-menu-blur'),
                $megaMenuArea: $('.mini-cart-area'),
                $minbagContainer: $('#miniCartItems'),
                $dropDown: $('.mini-cart.dropdown'),
                $APIdataUrl: $('#miniCartItems').attr("data-url"),
                $APIDataCountUrl: $('#miniCartItems').attr("data-counturl"),
                $isMinicartOpen :false,
                $isHover:false
            }
        },
        initAjax: function() {},
        initEvents: function() {
             cache.$document.on('mouseover', '.minicart',function(e){
                if(!cache.$isMinicartOpen){
                    globalMinibag.initMiniBagAjax(e);
                    cache.$isMinicartOpen =true;					
                }
             });
             
             $('document').on('click', '.trending-now-btn',function(e){
            	 var navigateToUrl = $(this).data('url');
            	 window.location.href = navigateToUrl;
             });
        },
        initBlurScreen: function() {
            cache.$miniBag.on('hide.bs.dropdown', function() {
                utility.pageScreenBlurOnHeaderLinks('hide');
            });
            cache.$miniBag.on('show.bs.dropdown', function() {
                utility.pageScreenBlurOnHeaderLinks('show');
            });
            cache.$megaMenuArea.on('click', function(e) {
                e.stopPropagation();
            });
           
        },
        initMiniBagAjax: function(e) {
            var options = {
                'methodType': 'GET',
                'dataType': 'JSON',
                'url': cache.$APIdataUrl,
                'isShowLoader': false,
                'isRequireLoader': false,   
                'cache': true
            }
            if(typeof e !== 'undefined' && e.type === 'mouseover'){
                ajaxFactory.ajaxFactoryInit(options, globalMinibag.initMiniBag, '','minibag_hover');
                cache.$isHover = true;
            }else{
            	options.url = cache.$APIDataCountUrl;
            	ajaxFactory.ajaxFactoryInit(options, function(response){
            		if(response>0){
            			cache.$minbagContainer.html(miniCartTemplate());
	            		$('.minicart span').html(response);
	            		$('.minicart').addClass('active');
	                    cache.$isHover = false;
            		}
            	});
            	
            }
        },
        initMiniBag: function(response) {
            function addScroll(){
                var heightofdiv = window.innerHeight - ($('.js-mainHeader').height() - $('.simon-md-mega-menu').height());
                heightofdiv = heightofdiv - 177;
                var currentHeight = $('.item-cart').height()-177;
                if (currentHeight > heightofdiv) {
                    $('.scrollablediv').css('height', heightofdiv-10 + 'px');
                }
            }

            var viewPort = viewportDetect.lastClass;
            cache.$minbagContainer.html(miniCartTemplate(response));
            if (response.totalUnitCount > 0) {
                $('.minicart span').html(response.totalUnitCount);
                $('.minicart').addClass('active');
            }
            else{
            	$('.minicart span').html('');
            	$('.minicart').removeClass('active');
            }

            $('.itemcount').on('click', function() {
                $(this).next('.product-container').collapse('toggle');
            });
            cache.$dropDown.on('show.bs.dropdown', function() {
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown(400, addScroll);
            });
            cache.$dropDown.on('hide.bs.dropdown', function(e) {
                e.preventDefault();
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp(400, function() {
                    cache.$dropDown.removeClass('open');
                    cache.$dropDown.find('.dropdown-toggle').attr('aria-expanded', 'false');
                });
            });

            if(viewPort === 'large'){
                $('.navbar .mini-cart').hover(function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown(400, addScroll);
                    utility.pageScreenBlurOnHeaderLinks('show');
                }, function() {
                    $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
                    utility.pageScreenBlurOnHeaderLinks('hide');
                    cache.$isMinicartOpen =false;
                });
            }

            $('.product-container').on('show.bs.collapse hidden.bs.collapse', function() {
                $(this).prev('.itemcount').toggleClass('plus-icon');
            })
            $('.mini-cart-area').css('display','block');
            $('.mini-cart-area').stop(true, true).slideDown(400, addScroll);
            //$('.mini-cart-area').first().stop(true, true).delay(250).slideDown(400, addScroll);
            //$(this).next('.product-container').collapse('toggle');
            //$(this).find('.mini-cart-area').first().stop(true, true).slideDown(400, addScroll);
            addScroll();
            $('.tooltip-init').tooltip();
            $('.product-container').on('click.markFav', 'a.mark-favorite', favorite.markFav);
            $('.product-container').on('click','a.openLoginModal',utility.openGlobalLoginModal);
            if( cache.$isHover && response){
				if($('#analyticsSatelliteFlag').val()==='true'){
					try {
						analytics.analyticsOnPageMiniBagHover(response);
					} catch(e) {
						analytics.analyticsAJAXFailureHandler('minibag_hover','something went wrong','CE');
						return;
					}
				}
            }
        }
    };
    $(function() {
        globalMinibag.init();
    });
    return globalMinibag;
});