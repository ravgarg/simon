<div class="pdp-overview-container">
	<div class="pdp-overview-content">
		<div class="price-container">
			<div class="crossed-price">$89.76 - $80.10</div>
			<div class="actual-price">$55.76 - $65.10 
				<span class="tooltip-init tooltip-icon" data-placement = "top" data-html="true" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00"></span>
   			</div>
			<div class="off">Up to 20% Off</div>
		</div>

		<div class="regular-tall-container major">
			<a href="#"><span>Regular</span>$45.33</a>
			<a href="#" class="active"><span>Tall</span>$45.33</a>
		</div>
		
		<div class="select-size-container">
			<button class="btn secondary-btn black major hide-desktop show-mobile" data-toggle="modal" data-dismiss="modal" data-target="#selectSizeMobile">Selected Size: 32</button>
			<div class="size-links show-desktop hide-mobile">
				<h6>Select Size <span class="size-guide-link"><a data-href="/GlobalSizeGuidePage" data-dismiss="modal" data-target="#sizeGuideModal" id="pdpSizeChartLink">Size Guide</a></span></h6>
				<div id="sizePalette" class="size-palette clearfix">
					<a href="#">30</a>
					<a href="#" class="active">31</a>
					<a href="#">32</a>
					<a href="#">33</a>
					<a href="#" class="crossed">34</a>
					<a href="#">36</a>
					<a href="#">38</a>
					<a href="#">40</a>
					<a href="#">42</a>
					<a href="#">46</a>
					<a href="#">50</a>
				</div>
			</div>
		</div>

		<div class="select-style-container">
			<h6>Select Hem Style</h6>
			<a href="#">With Cuffs</a>
			<a href="#" class="active">Without Cuffs</a>
			<a href="#">Unfinished</a>
		</div>

		<div class="list-dropdown">
			<select>
				<option>
					Select Inseam Length
				</option>
			</select>
		</div>

		<div class="select-color">
			<h6>Select Color: <span id="colorName">Taupe</span></h6>
			<div id="colorPalette" class="color-palette clearfix" >
				<span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
				<span class="color-switches active" data-color-name="Taupe"  style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
				<span class="color-switches" data-color-name="gray" style="background: url('/_ui/responsive/simon-theme/images/color-dark-blue.png');"></span>
			</div>
		</div>

		<div class="select-quantity">
			<h6>Select Quantity</h6>
			<div class="plus-minus">
				<a href="javascript:void(0);" class="decrease"></a>
				<input type="text" class="quantity" value="1" min="1" max="99"/>
				<a href="javascript:void(0);" class="increase disabled"></a>
			</div>
		</div>

		<div class="add-to-bag">
			<p class="price-disclaimer">Prices online may vary from in store</p>
			<button class="btn plum major" data-toggle="modal" data-dismiss="modal" data-target="#justAddedModal">ADD TO BAG</button>
		</div>

		<div class="add-to-favorites">
			<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#globalLoginModal">Add to My Favorites</a>
		</div>

		<!--<div class="add-to-favorites">
			<a href="#" class="added">Add to My Favorites</a>
		</div>-->
	</div>

	<div class="pdp-overview-accordion">
		<div class="panel-group" id="accordion">
		    <div class="panel panel-default">
	             <h6 class="panel-title">
	                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1"><i class="glyphicon glyphicon-minus"></i>Details</a>
	            </h6>
		        <div id="panel1" class="panel-collapse collapse in">
		            <div class="content">
						<img src="/_ui/responsive/simon-theme/images/pdp-description-logo.png" alt="">
						<ul>
							<li>By using this site, you agree</li>
							<li>By using this site, you agree</li>
							<li>By using this site, you agree</li>
							<li>By using this site, you agree</li>
						</ul>
					</div>
		        </div>
		    </div>

		    <div class="panel panel-default">
	            <h6 class="panel-title">
	                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2"><i class="glyphicon glyphicon-plus"></i>Shipping & Returns Information</a>
	            </h6>
		        <div id="panel2" class="panel-collapse collapse">
		            <div class="content">
						ds
					</div>
		        </div>
		    </div>
		</div>
	</div>
	<div class="footer-social">
        <section>
           <h6>Share abc</h6>
           <a href="https://www.facebook.com/sharer/sharer.php?u=https://scontent.xx.fbcdn.net/v/t31.0-8/13735769_1138596126187164_3106035068213495774_o.jpg&amp;oh=fd305eea3e1dd624ee05d72aef73158b&amp;oe=5A21A57E" title="Facebook" alt="Facebook">
               <img src="/_ui/responsive/simon-theme/icons/black-facebook.svg" >
           </a>
           <a href="https://twitter.com/simonpropertygp" title="Twitter" alt="Twitter">
               <img src="/_ui/responsive/simon-theme/icons/black-twitter.svg" >
           </a>
           <a href="https://www.youtube.com/user/SimonPropertyGroup" title="Instagram" alt="Instagram">
               <img src="/_ui/responsive/simon-theme/icons/black-instagram.svg" >
           </a>
           <a href="http://www.simon.com/foundatsimon" title="YouTube" alt="Youtube">
               <img src="/_ui/responsive/simon-theme/icons/black-youtube.svg">
           </a>
           <a href="https://instagram.com/simonpropertygroup" title="Pinterest" alt="Pinterest">
               <img src="/_ui/responsive/simon-theme/icons/black-pinterest.svg">
           </a>
        </section>
    </div>
</div>