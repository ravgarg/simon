<button class="btn secondary-btn black" data-toggle="modal" data-target="#shareViaEmail">
										share via email
									</button>

<!-- Share via Email Modal Window -->
<div class="modal fade" id="shareViaEmail" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title">Share Via Email</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body share-via-email">		
					<form action="${action}" method="post" modelAttribute="" class="sm-form-validation bt-flabels js-flabels captchaValidation" data-parsley-validate="validate">									
						<p class="sub-title">Love it? Share it?</p>
						<p>Spread the love by sharing your favorite styles, deals and more.</p>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="sm-input-group">
									  <label class="field-label">Email Address<span class="field-error" id="error-shareEmail"></span></label>
								      <input type="email" class="form-control" placeholder="Your Email" name="email" data-parsley-errors-container="error-shareEmail" type="email" data-parsley-type="email" data-parsley-error-message="Please enter your email address" required>
								</div>
			                </div>
			                <div class="col-xs-12 col-md-12">
								<div class="sm-input-group toEmail">
									  <label class="field-label">To Email<span class="field-error" id=""></span></label>
								      <input type="email" class="form-control" placeholder="To Email" name="to-email" data-parsley-errors-container="" type="email" data-parsley-type="email" data-parsley-error-message="" required>
								      <span class="msg">Enter up to 5 addresses, separate with a comma.</span>
								</div>
			                </div>
			                <div class="col-xs-12 col-md-12">
								<div class="sm-input-group message-row">
									<label>Message<span class="field-error" id=""></span></label>
									<textarea class="form-control" placeholder="Hello, Check this out from Shop Premium Outlets. More savings than ever!" rows="5"></textarea>
								</div>
							</div>
							<c:if test="${not empty enableCaptcha && enableCaptcha eq 'true'}">
				                <div class="col-xs-12 col-md-12 captchaContainer">
									<div class="sm-input-group">
									<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
									<div id="captcha" class="has-error"><spring:theme code="login.customer.captcha.error.msg" /></div>
									</div>
								</div>
							</c:if>
			                <div class="col-xs-12 col-md-12">
			                	<div class="sm-input-group">
			                	<label class="custom-checkbox">
			                		<input type="checkbox" name="" class="sr-only" id="termAndPolicy"/><i class="i-checkbox"></i>
			                		I agree to the Terms of Use and Privacy Policy.
			                	</label>									                				
                				<p>Don't worry, we're not signing anyone up for anything-this is a one-time email.</p>                					                				
                				</div>								
							</div>			                
			                <div class="submit-btn col-md-12 col-xs-12">
								<button class="btn" type="submit">
									Send Message
								</button>
							</div>
						</div>
					</form>		
					
				</div>
			</div>
		</div>		
	</div>