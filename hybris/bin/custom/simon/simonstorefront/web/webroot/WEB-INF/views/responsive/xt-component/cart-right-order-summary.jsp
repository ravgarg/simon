<div class="cart-order-summary">
    <div class="bag-summary hide-mobile">
       Bag Summary
       <a href="#">
          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
       </a>
    </div>
    <div class="view-bag-summary hide-desktop pad-r-mobile pad-l-mobile">View Bag Summary</div>
    <jsp:include page="cart-order-summary.jsp"></jsp:include>
</div>