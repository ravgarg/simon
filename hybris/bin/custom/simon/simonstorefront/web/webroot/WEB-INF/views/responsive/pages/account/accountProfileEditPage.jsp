<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<div class="myprofile">
	<button
		class="col-xs-12 btn secondary-btn black major hide-desktop my-account"><spring:theme code="text.account.profile.myaccount.myprofile"/></button>
	<c:set var="profileCompleted" value = "${not empty isProfileCompleted && isProfileCompleted eq 'true'}" />
	<c:url value="/my-account/update-profile" var="updateProfileActionUrl" />
		<form:form action="${updateProfileActionUrl}" method="post" commandName="updateProfileForm" id="updatePassword" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-analytics-type="registration" data-analytics-eventtype="my_account" data-analytics-eventsubtype="my_account_update" data-analytics-formtype="my_account" data-analytics-formname="my_account_update" data-satellitetrack="registration"  data-parsley-validate="validate">	
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<h5 class="panel-title">
						<a class="accordion-toggle major" data-toggle="collapse"
							data-parent="#accordion" href="#panel1" aria-expanded="true"><spring:theme code="text.account.profile.myaccount.header"/></a>
					</h5>
					<div id="panel1" class="panel-collapse collapse in"
						aria-expanded="true">
						<div class="collapsed-content">
							<div class="row form-details">
								<div class="col-md-12">
									<p class="instruction-msg"><spring:theme code="text.account.profile.myaccount.header.instruction"/></p>
								</div>
								<div class="col-xs-12 col-md-12">
									<div class="sm-input-group">
										<label class="field-label" for="accountEmailAddress"><spring:theme code="text.account.profile.myaccount.email"/> <span class="field-error" id="error-j_guestEmailId"></span></label>
										<input type="email" class="form-control" id="accountEmailAddress"
											placeholder="<spring:theme code="text.account.profile.myaccount.email"/>"
											value="${updateProfileForm.email}" name="email" data-parsley-errors-container="#error-j_guestEmailId" data-parsley-type="email" data-parsley-error-message="Please enter your email address" readonly>
									</div>
								</div>
								<div class=" col-md-12 col-xs-12 hide update-password">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<div class="sm-input-group">
												<label class="field-label" for="newpswd"><spring:theme code="register.new.customer.pwd.placeHolder.current" /> <span class="field-error" id="error-j_newpswd"></span></label>
												<input 
													type="password"
													autocomplete="off"
													class="form-control" 
													placeholder="<spring:theme code="register.new.customer.pwd.placeHolder.current" />" 
													name="currentPassword" 
													data-parsley-errors-container="#error-j_newpswd" 
													id="newpswd"
													data-parsley-required-message="Please enter a password" 
													data-parsley-pwdstrength-message="Please enter a valid password"
													data-parsley-pwdstrength 
													 />
											</div>
										</div>
									</div>
									<div class="row">	
										<div class="col-xs-12 col-md-6">
											<div class="sm-input-group">
												<label class="field-label" for="registration-pswd"><spring:theme code="register.new.customer.confirm.pwd.placeHolder" /> <span class="field-error" id="error-j_pswd"></span></label>
												<input 
													type="password"
													autocomplete="off" 
													class="form-control" 
													placeholder="<spring:theme code="updatePwd.pwd" />" 
													name="password" 
													data-parsley-errors-container="#error-j_pswd" 
													id="registration-pswd"
													data-parsley-required-message="Please enter a password" 
													data-parsley-pwdstrength-message="Please enter a valid password"
													data-parsley-pwdstrength 
													 />
											</div>
										</div>
										<div class="col-xs-12 col-md-6">
											<div class="sm-input-group">
												<label class="field-label" for="cnfrm-pswd"><spring:theme code="register.new.customer.confirm.pwd.placeHolder.reconfirm" /> <span class="field-error" id="error-j_confirmPswd"></span></label>
												<input 
													type="password"
													autocomplete="off"
													class="form-control" id="cnfrm-pswd"
													placeholder="<spring:theme code="register.new.customer.confirm.pwd.placeHolder.reconfirm" />" 
													name="confirmPassword" 
													data-parsley-errors-container="#error-j_confirmPswd" 
													data-parsley-required-message="<spring:theme code="register.new.customer.confirmpwd.error" />"
													data-parsley-equalto-message="Password & confirm password should match" 
													data-parsley-equalto="#registration-pswd" 
													 />
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12 change-password">
									<label for="changePassCheckbox" class="custom-checkbox"><input type="checkbox" name="" class="sr-only" id="changePassCheckbox"/><em class="i-checkbox"></em><spring:theme code="text.account.profile.change.password"/></label> 
									<input type="hidden" name="passwordUpdate" value="false"/>
								</div>
								<div class="col-md-12 col-xs-12 hide update-password">
									<div class="instructions">
										<p><spring:theme code="text.account.profile.change.password.instructions"/></p>
										<ul>
											<li><spring:theme code="text.account.profile.change.password.instruction.uppercase"/></li>
											<li><spring:theme code="text.account.profile.change.password.instruction.numeric"/></li>
											<li><spring:theme code="text.account.profile.change.password.instruction.lowercase"/></li>
											<li><spring:theme code="text.account.profile.change.password.instruction.nonalphanumeric"/></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="form-fields">
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group">
											<label class="field-label" for="accountFirstName"><spring:theme code="text.account.profile.myaccount.password.firstname"/> <span class="field-error" id="error-firstName"></span></label>
											<input type="text" class="form-control" name="firstName" id="accountFirstName"
												placeholder="<spring:theme code="text.account.profile.myaccount.password.firstname"/>"
												value="${updateProfileForm.firstName}" data-parsley-errors-container="#error-firstName" data-parsley-error-message="Please enter your first name" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group">
											<label class="field-label" for="accountlastName"><spring:theme code="text.account.profile.myaccount.password.lastname"/> <span class="field-error" id="error-j_guestLastName"></span></label>
											<input type="text" class="form-control" name="lastName" id="accountlastName"
												placeholder="<spring:theme code="text.account.profile.myaccount.password.lastname"/>"
												value="${updateProfileForm.lastName}" data-parsley-errors-container="#error-j_guestLastName" data-parsley-error-message="Please enter your last name" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6 zipcode">
										<div class="sm-input-group">
											<label class="field-label" for="txtZip"><spring:theme code="text.account.profile.myaccount.password.zipcode"/> <span class="field-error" id="error-j_guestZipCode"></span></label>
											<input type="text" id="txtZip" class="form-control" name="zipCode"
												placeholder="<spring:theme code="text.account.profile.myaccount.password.zipcode"/>" value="${updateProfileForm.zipCode}" data-parsley-errors-container="#error-j_guestZipCode" data-parsley-error-message="Please enter your ZIP or postal code" data-parsley-required="" data-parsley-pattern="^(?=.*[1-9].*)[0-9]{5}?$" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu field-float">
                                			<label class="field-label" for="countries"><spring:theme code="text.account.profile.myaccount.password.country"/> <span class="field-error" id="error-j_country"></span></label>
											<select class="form-control" name="country" id="countries" data-parsley-errors-container="#error-j_country" data-parsley-error-message="Please select your country" required>
												
												<c:forEach items="${countries}" var="country">
													<c:choose>
															<c:when test="${country.isocode eq updateProfileForm.country}">
				                                          		<option value="${country.isocode}" selected>${country.name}</option>
				                                          	</c:when>
					                                          <c:otherwise>
					                                           		<option value="${country.isocode}" >${country.name}</option>
					                                          </c:otherwise> 
													</c:choose>
												</c:forEach>
											</select>
										</div>
									</div>
									
									
									
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu field-float">
                                			<label class="field-label" for="accountBirthMonth"><spring:theme code="text.account.profile.myaccount.password.birthmonth"/> <span class="field-error" id="error-j_birthmonth"></span></label>
                                			<select class="form-control" name="birthMonth" id="accountBirthMonth" data-parsley-errors-container="#error-j_birthmonth" data-parsley-error-message="Please select your birth month" required>
											 <option value="" <c:if test="${profileCompleted eq 'false'}">selected</c:if>><spring:theme code="text.account.profile.myaccount.password.birthmonth"/></option>												
												<c:forEach items="${birthMonthList}" var="month">
													<c:choose>
															<c:when test="${month.code eq updateProfileForm.birthMonth}">
				                                          		<option value="${month.code}" <c:if test="${profileCompleted eq 'true'}">selected</c:if>>${month.name}</option>
				                                          	</c:when>
					                                          <c:otherwise>
					                                           		<option value="${month.code}" >${month.name}</option>
					                                          </c:otherwise> 
													</c:choose>
												</c:forEach>
											</select>	
                                			</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu field-float">
                                			<label class="field-label" for="accountBirthYear"><spring:theme code="text.account.profile.myaccount.password.birthyear"/> <span class="field-error" id="error-j_birthyear"></span></label>
											 <select class="form-control" name="birthYear" id="accountBirthYear" data-parsley-errors-container="#error-j_birthyear" data-parsley-error-message="Please select your birth year" required>
												<option value="" <c:if test="${profileCompleted eq 'false'}">selected</c:if>><spring:theme code="text.account.profile.myaccount.password.birthyear"/></option>
												<c:forEach var="year" begin="${startYear}" end="${endYear}"
													step="1">
													<c:choose>
															<c:when test="${year eq updateProfileForm.birthYear}">
				                                          		<option value="${year}" <c:if test="${profileCompleted eq 'true'}">selected</c:if>>${year}</option>
				                                          	</c:when>
					                                          <c:otherwise>
					                                           		<option value="${year}" >${year}</option>
					                                          </c:otherwise> 
													</c:choose>
												</c:forEach>
											</select>											
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu field-float">
                                			<label class="field-label" for="accountgender"><spring:theme code="text.account.profile.myaccount.password.gender"/> <span class="field-error" id="error-j_gender"></span></label>
											   <select class="form-control" name="gender" id="accountgender" data-parsley-errors-container="#error-j_gender" data-parsley-error-message="Please select your gender" required>
												<option value="" <c:if test="${profileCompleted eq 'false'}">selected</c:if>><spring:theme code="text.account.profile.myaccount.password.gender"/></option>
												<c:forEach items="${genderList}" var="gender">
													<c:choose>
															<c:when test="${gender.code eq updateProfileForm.gender}">
				                                          		<option value="${gender.code}" <c:if test="${profileCompleted eq 'true'}">selected</c:if>>${gender.name}</option>
				                                          	</c:when>
					                                          <c:otherwise>
					                                           		<option value="${gender.code}" >${gender.name}</option>
					                                          </c:otherwise> 
													</c:choose>
												</c:forEach>
											</select>   	
										</div>
									</div>
								</div>
								<div class="clearfix row">
									<div class="update-profile col-md-12 small-text"><spring:theme code="text.account.profile.myaccount.save.message"/></div>
									<div class="submit-btn col-md-12 col-xs-12">
										<button class="btn" type="submit" id="updateForm"><spring:theme code="register.new.customer.button.update" /></button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</form:form>


</div>