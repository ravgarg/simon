<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="breadcrumbs">
	<c:if test="${not empty breadcrumbs}">
		<c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
			<c:choose>
				<c:when test="${breadcrumb.url eq '#'}">
					<a href="#" <c:if test="${status.last}">class="active"</c:if>>${fn:escapeXml(breadcrumb.name)}</a>
					<c:if test="${!status.last}"><span class="arrow-fwd"></span></c:if>
				</c:when>
				<c:otherwise>
					<c:url value="${breadcrumb.url}" var="breadcrumbUrl"/>
					<a href="${breadcrumbUrl}" <c:if test="${status.last}">class="active"</c:if>>${fn:escapeXml(breadcrumb.name)}</a>
					<c:if test="${!status.last}"><span class="arrow-fwd"></span></c:if>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</c:if>
</div>
