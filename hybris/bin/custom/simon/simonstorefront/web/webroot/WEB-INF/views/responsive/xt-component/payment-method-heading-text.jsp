<div class="heading-text">
	<div class="payment-method-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. You don't have any saved payment methods. Save your preferred credit card information to make your next checkout experience faster.</div>
	<div class="not-saved-payment-text">You don't have any saved payment methods. Save your preferred credit card information to make your next checkout experience faster.</div>
	<jsp:include page="payment-method-tile.jsp"></jsp:include>
	<div class="security-reason">* For security reasons, you can only add or delete saved payment methods.</div>
	<button type="button" class="btn add-new-payment-method" data-toggle="modal" data-target="#paymentMethodModal">Add New Payment Method</button>
</div>