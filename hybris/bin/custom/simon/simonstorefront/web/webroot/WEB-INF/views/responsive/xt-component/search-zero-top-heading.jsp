<div class="search-zero-result">
    <h1 class="no-result">No results found for</h1>
    <h1 class="search-term">“{Search Term}”</h1>
</div>
<div class="btn-category hide-desktop">
    <button class="btn secondary-btn black">Browse Categories</button>
</div>

