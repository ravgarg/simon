<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

<div class="designer-landing">
    <div class="container">
       <div class="row">
            <div class="col-md-12 hide-mobile">
                <jsp:include page="clp-breadcurmb.jsp"></jsp:include>
            </div>
       </div>
    </div>
    <div class="col-md-12 no-pad designer-carousal">
        <jsp:include page="hero-carousal.jsp"></jsp:include>
    </div>
    <div class="container">
        <div class="row">
            <aside class="col-md-3 left-menu-container show-desktop">
                <jsp:include page="plp-left-nav.jsp"></jsp:include>
            </aside>
            <div class="col-md-9">
                <div class="btn-category"><button class="btn secondary-btn black hide-desktop">Browse Categories</button></div>
                <jsp:include page="designer-listing-shop.jsp"></jsp:include>
                <jsp:include page="clp-trending-now.jsp"></jsp:include>
                <jsp:include page="designer-landing-shop.jsp"></jsp:include>
                <jsp:include page="style-deals.jsp"></jsp:include>
                <jsp:include page="designers-deals.jsp"></jsp:include>
            </div>
        </div>
    </div>
    <jsp:include page="social-shop.jsp"></jsp:include>
	
	<jsp:include page="footer-content-slot.jsp"></jsp:include>
</div>


