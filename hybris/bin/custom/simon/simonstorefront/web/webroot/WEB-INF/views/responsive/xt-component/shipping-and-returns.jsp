<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="trends-landing-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="trends-landing-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<h1 class="page-heading major"> Shipping, Returns & Privacy Policy </h1>
			<jsp:include page="shipping-returns-heading-desc.jsp"></jsp:include>			
			<div class="shipping-and-returns">								
				
				<form>
					<div class="no-store-selected">		
						<div class="row">
							<div class="col-xs-12 col-md-6 sm-input-group select-menu">
								<select class="form-control" name="store" id="">
									<option>Select a store</option>
									<option>store 1</option>
									<option>store 2</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<p>Select a Store or a brand to view their shipping, returns & privacy information.</p>
							</div>
						</div>							
					</div>
				</form>
							
			</div>			
		</div>
	</div>
</div>
