define(['hbshelpers', 'handlebars', 'ajaxFactory', 'utility', 'cartcheckoutordesummary', 'templates/checkoutReviewOrder.tpl','myaccountManageAddress','globalCustomComponents','viewportDetect','analytics'],
    function (hbshelpers, handlebars, ajaxFactory, utility, cartcheckoutordesummary, checkoutReviewOrderTemplate, myaccountManageAddress,globalCustomComponents,viewportDetect,analytics) {
        'use strict';
        var cache;
        var checkoutReviewOrder = {
            init: function () {
                this.initVariables();
                this.initEvents();
                this.initAjax();
                this.reviewItemsAccordian();
                if($('#nosavedPaymentmethod').length === 0){
                    this.flotLabel();
                }
                
            },
            initVariables: function () {
                cache = {
                    $document: $(document),
                    $reviewOrderContainer: $('.review-order'),
                    $orderSummaryContainer: $('.order-Summary-Container'),
                    methodType: '',
                    $response: $("#cartData").length >0 ? $.parseJSON($("#cartData").val()) : '',
                    $itemContainer: $('.cart-checkout-items'),
                    $checkoutBreadcrumb : $('.checkout-breadcrumb')
                }
            },
            initAjax: function () {
            },
            initEvents: function () {
                cache.$document.on('click', '.cart-checkout-items-order-details button.reviewOrder', checkoutReviewOrder.submitReviewOrder);
                cache.$document.on('change', '.cart-checkout-items-order-details select#shippingMethod', checkoutReviewOrder.selectShippingMethod);
                cache.$document.on('click','.viewReturnPolicy',utility.openReturnPrivacyPolicyModal);
                cache.$document.on('click', '.cart-checkout-items-order-details select', checkoutReviewOrder.getMethodTypePrice),
                $('.sm-input-group input').on('keydown', checkoutReviewOrder.flotLabel),
                $('.sm-input-group input').on('change', checkoutReviewOrder.flotLabel),
                $('.sm-input-group select').on('change', checkoutReviewOrder.flotLabel);
            },
            
            flotLabel:function () {
                var $input;
                $input = $(this);
                if ($input.is("input")) {
                    setTimeout(function () {
                        if ($input.val()) {
                            $input.closest('.sm-input-group').addClass('field-float');
                        } else {
                            $input.closest('.sm-input-group').removeClass('field-float');
                        }
                    }, 1);
                }  
            },
            paintPage: function (response) {
                $(".cart-checkout-items-order-details select").prop("disabled", false);
                $('.tooltip-init').tooltip();
                cache.methodType = response.cartData.shippingPriceCall;
                cache.$response = response;
                response.openReviewPanel = true;
                $('#revieworder').html(checkoutReviewOrderTemplate(response));
                $("#panel2").collapse('toggle');
                response.isCheckout = true;
                cartcheckoutordesummary.paintordersummary(cache.$orderSummaryContainer, response);
                $('.tooltip1-init').tooltip();
            },
            submitReviewOrder: function () {
                cache.$response.openReviewPanel = false;
                cache.$response.showTicker = true;
                $('#revieworder').html(checkoutReviewOrderTemplate(cache.$response));
                $("#panel2").collapse('toggle');
                if (cache.$response.userloggedin) {
                    $("#panel4").collapse('toggle');
					if($('#analyticsSatelliteFlag').val()==='true'){
                        analytics.analyticsOnPagePaymentClick(true);
                    }
                    $('#payment .panel-title .statement').removeClass('hide');
                    $('#payment .panel-title .statement').addClass('hide-mobile');
                    $('.placeorderBtn').removeClass('inactive');
                    $('.placeorderBtn').removeAttr("disabled");
                } else {
                    $("#panel3").collapse('toggle');
                    if($('#analyticsSatelliteFlag').val()==='true'){
                        analytics.analyticsOnPageSaveInformationClick('onload');
                    }
					
                }
				if(viewportDetect.lastClass === 'large'){
    				globalCustomComponents.rightRailAnchoredReviewOrder();
    				window.scrollTo(500, 600);
    			}
                $('.close-tab').toggleClass('hide');


                // Checkout Breadcrumb
                cache.$checkoutBreadcrumb.find('li:nth-child(2)').addClass('checked').removeClass('active').css('cursor', 'pointer');
                cache.$checkoutBreadcrumb.find('li:last-child').addClass('active');
                cache.$checkoutBreadcrumb.find('li').each(function() {
                  $(this).css('pointer-events','');
                });
            },
            selectShippingMethod: function (e) {
                var RetailerId = $(this).data('retailerid');
                var shippingMethod = $(this).val();
                var formData = {
                    retailerId: RetailerId,
                    shippingMethod: shippingMethod
                }
                var options = {
                    'methodType': 'GET',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': $("#cartData").data('select'),
                    'isShowLoader': false,
                    'cache': true
                }
                ajaxFactory.ajaxFactoryInit(options, checkoutReviewOrder.paintPage);
				e.stopImmediatePropagation();
				return false;
            },
            getMethodTypePrice: function (e) {
                if (cache.methodType !== 'ALL') {
                    var options = {
                        'methodType': 'GET',
                        'dataType': 'JSON',
                        'methodData': {"selectedShippingMethod": cache.methodType},
                        'url': $("#cartData").data('estimatecart'),
                        'isShowLoader': false,
                        'cache': true
                    }
                    $(".cart-checkout-items-order-details select").prop("disabled", true);
                    ajaxFactory.ajaxFactoryInit(options, checkoutReviewOrder.paintPage);
                    e.stopImmediatePropagation();
				    return false;
                }
            },
            reviewItemsAccordian: function () {
                cache.$document.on('click', '.view-items', function () {
                    var that = $(this);
                    that.next('div').collapse('toggle');
                    if(that.next('div').attr("aria-expanded") !== 'false'){
                        that.addClass('minus-icon')
                        $('.close-tab').removeClass('hide');
                        $('.view-tab').addClass('hide');
                       
                    } else {
                        that.removeClass('minus-icon');
                        $('.close-tab').addClass('hide');
                        $('.view-tab').removeClass('hide');                        
                    }
                });
                $('.cart-checkout-items').on('show.bs.collapse hidden.bs.collapse', function () {
                    $(this).prev('.view-items').toggleClass('minus-icon');
                });
            }

        };
        return checkoutReviewOrder;
    });