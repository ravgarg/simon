<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order/custom"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="totalItemCount" value="" />

<div class="cart-checkout-items-order-details" data-analytics-item-count="${totalItemCount}">
	<c:forEach items="${order.retailerInfoData}" var="retailerInfoData">
		<div class="cart-checkout-items analytics-orderretailerList">
		
			<div class="confirm-retailer major" data-analytics-shippingmethod="${retailerInfoData.appliedShippingMethod.name}" data-analytics-retailerdiscount="${not empty retailerInfoData.promotionalDiscount ? retailerInfoData.promotionalDiscount.value : 0}" data-analytics-retailername="${retailerInfoData.retailerName}" data-analytics-retaileritem="${retailerInfoData.totalUnitCount}">
				${retailerInfoData.retailerName} (${retailerInfoData.totalUnitCount})
			</div>
			<c:set var="totalItemCount" value="${totalItemCount + retailerInfoData.totalUnitCount}" />
			<div class="cart-items-container">
				<div class="return-information">
					<a class="viewReturnPolicy" data-href="/contentPolicy/${retailerInfoData.productDetails.get(0).shopId}">
						<spring:theme code='order.confirmation.shipping.return.policy.txt' />
					</a>
				</div>
				<div class="checkout-item-heading">
					<div class="row">
						<div class="col-md-2 col-xs-2">
							<div class="item-heading"><spring:theme code='order.confirmation.items.txt' /></div>
						</div>
						<div class="col-md-offset-6 col-md-2 col-xs-offset-6 col-xs-2">
							<div class="price-heading hide-mobile"><spring:theme code='order.confirmation.items.price.txt'/></div>
							</div>
						<div class="col-md-2 col-xs-2">
							<div class="total-heading"><spring:theme code='order.confirmation.subtotal.txt'/></div>
							</div>
						</div>
					</div>
				<c:forEach items="${retailerInfoData.productDetails}" var="entryData">
					<c:set var="altText" value="${entryData.product.baseOptions[0].selected.baseProductDesignerName}&nbsp;${entryData.product.variantValues.Color}&nbsp;${entryData.product.name}" />
					<div class="row clearfix row-pad-btm analytics-cartItem">
						<div class="col-md-2 col-xs-2 prodImage">
							<div class="imgContainer">
								<a href="${entryData.product.url}">
									<c:choose>
										<c:when test="${not empty entryData.product.images}">
											<c:forEach items="${entryData.product.images}" var="images">
												<c:if test="${images.format eq 'thumbnail' }">
													<a href="#"><img class="img-width" alt="${altText}" src="${images.url}"></a>
												</c:if>
											</c:forEach>
										</c:when>
										<c:otherwise>
											<img class="img-width" alt="${altText}" src="/_ui/responsive/simon-theme/images/no-image-105x160.jpg">
										</c:otherwise>
									</c:choose>
								</a>
							</div>
							<c:set var = "itemCode" value=""/>
								<c:set var = "itemType" value=""/>
								<c:set var = "itemDescription" value=""/>
								<c:if test="${not empty entryData.itemPromotionList}">
								<c:forEach items="${entryData.itemPromotionList}" var="itemPromotionList" varStatus="status">
									<c:choose>
									<c:when test="${not status.last}">
										<c:set var = "itemCode" value="${itemCode}${itemPromotionList.code}>"/>
										<c:set var = "itemType" value="${itemType}${itemPromotionList.type}>"/>
										<c:set var = "itemDescription" value="${itemDescription}${itemPromotionList.description}>"/>
									</c:when>
									<c:otherwise>
										<c:set var = "itemCode" value="${itemCode}${itemPromotionList.code}"/>
										<c:set var = "itemType" value="${itemType}${itemPromotionList.type}"/>
										<c:set var = "itemDescription" value="${itemDescription}${itemPromotionList.description}"/>
									</c:otherwise>
									</c:choose>
								</c:forEach>
								</c:if>
		
						</div>
						<div class="col-md-6 col-xs-8 row-pad-l">
							<div class="item-description" data-analytics-item-designer="${entryData.product.baseOptions[0].selected.baseProductDesignerName}" data-analytics-item-id="${entryData.product.code}">
								<a href="${entryData.product.url}" data-analytics-item-name="${entryData.product.name}">${entryData.product.name}</a>
							</div>
							<div class="item-promotion-spacing" data-analytics-item-offercode="${itemCode}" data-analytics-item-offertype="${itemType}" data-analytics-item-offerdesc="${itemDescription}">
								<c:forEach items="${order.appliedProductPromotions}" var="productPromotion">
									<c:forEach items="${productPromotion.consumedEntries}" var="consumedEntry">
										<c:if test="${consumedEntry.orderEntryNumber eq entryData.entryNumber}">
											${productPromotion.description}
										</c:if>
									</c:forEach>
								</c:forEach>
							</div>
							<div class="hide-desktop">
								<div class="item-price" data-analytics-item-price-actual="${entryData.msrpValue.value}">${entryData.msrpValue.formattedValue}</div>
								<div class="item-revised-price">
									<span data-analytics-item-price="${entryData.saleValue.value}">${entryData.saleValue.formattedValue}</span>&nbsp;
									<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true"
										class="tooltip-init simon-logo" data-toggle="tooltip" title="MSRP: ${entryData.msrpValue.formattedValue}<br>
																									List Price: ${entryData.listValue.formattedValue} <br>
																									Sale Price: ${entryData.saleValue.formattedValue}" alt="Info">
								</div>
								<div class="item-discount">${entryData.youSave.value}<spring:theme code='order.confirmation.percentage.off.txt' /></div>
							</div>
							<div class="item-qty" data-analytics-item-quantity="${entryData.quantity}">
								<spring:theme code='order.confirmation.quantity.txt' />&nbsp;${entryData.quantity}
							</div>
							<c:forEach items="${entryData.product.variantValues}" var="entry">
								<div class="item-color" data-analytics-${entry.key}="${entry.value}">${entry.key}: ${entry.value}</div>
							</c:forEach>
						</div>
						<div class="col-md-2 col-xs-2">
							<div class="hide-mobile">
								<c:if test="${not empty entryData.msrpValue.value}">
									<span class="item-price strike" data-analytics-item-strike-through="${entryData.msrpValue.value}">${entryData.msrpValue.formattedValue}</span>
								</c:if>
								<c:if test="${empty entryData.msrpValue.value}">
									<c:if test="${not empty entryData.saleValue.value}">
									<span class="item-price strike" data-analytics-item-strike-through="${entryData.listValue.value}">${entryData.listValue.formattedValue}</span>
									</c:if>
								</c:if>
								<div class="item-revised-price">
									<c:if test="${entryData.promotionApplicable}">
										<c:choose>
											<c:when test="${empty entryData.saleValue.value}">
												<input type="hidden" class="price_beforediscount"
													value="${entryData.basePrice.value}" />
											</c:when>
											<c:otherwise>
												<input type="hidden" class="price_beforediscount"
													value="${entryData.saleValue.value}" />
											</c:otherwise>
										</c:choose>
										<span data-analytics-item-price="${entryData.yourPrice.value}">${entryData.yourPrice.formattedValue}</span>&nbsp;
										<c:if test="${not empty entryData.msrpValue.value and not empty entryData.saleValue.value}">
											<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true"
												class="tooltip-init simon-logo" data-toggle="tooltip" title='<spring:theme code="order.details.msrp.text"/>${entryData.msrpValue.formattedValue}<br>
																												<spring:theme code="order.details.list.price.text"/>${entryData.listValue.formattedValue}<br>
																												<spring:theme code="order.details.your.price.text"/>${entryData.yourPrice.formattedValue}<br>
																												<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="Info">
										</c:if>
										<c:if test="${empty entryData.msrpValue.value}">
											<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true"
												class="tooltip-init simon-logo" data-toggle="tooltip" title='<spring:theme code="order.details.your.price.text"/>${entryData.yourPrice.formattedValue}<br>
																											<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="Info">
										</c:if>
									</c:if>
									<c:if test="${not entryData.promotionApplicable}">
										<c:if test="${not empty entryData.saleValue.value}">
											<span data-analytics-item-price="${entryData.saleValue.value}">${entryData.saleValue.formattedValue}</span>&nbsp;
										</c:if>
										<c:if test="${empty entryData.saleValue.value}">
											<span data-analytics-item-price="${entryData.listValue.value}">${entryData.listValue.formattedValue}</span>&nbsp;
										</c:if>
										<c:if test="${not empty entryData.msrpValue.value and not empty entryData.saleValue.value}">
											<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true"
												class="tooltip-init simon-logo" data-toggle="tooltip" title='<spring:theme code="order.details.msrp.text"/>${entryData.msrpValue.formattedValue}<br>
																											<spring:theme code="order.details.list.price.text"/>${entryData.listValue.formattedValue}<br>
																											<spring:theme code="order.details.sale.price.text"/>${entryData.saleValue.formattedValue}<br>
																											<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="Info">
										</c:if>
									</c:if>
								</div>
								<div class="item-discount">${entryData.savingsMessage}</div>
							</div>
						</div>
						<div class="col-md-2 col-xs-2">
							<div class="subtotal">${entryData.totalPrice.formattedValue}</div>
						</div>
					</div>
				</c:forEach>
				<div class="row">
					<div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
						<div class="price-container-left">
							<div class="price-calculation-container">
								<div class="price-top-spacing">
									<span class="estimated-subtotal"><spring:theme code='order.confirmation.estimated.subtotal.txt' /></span>
									<span class="estimated-sub-price" data-analytics-retailer-exclusive="${retailerInfoData.estimatedSubtotal.value}">
										${retailerInfoData.estimatedSubtotal.formattedValue}
									</span>
								</div>
								<div class="ship-top-spacing">
									<span class="estimated-shipping"><spring:theme code='order.confirmation.estimated.shipping.txt' /></span>
									<span class="estimated-shipping-price" data-analytics-retailer-shipping="${retailerInfoData.estimatedShipping.value}">
										${retailerInfoData.estimatedShipping.formattedValue}
									</span>
								</div>
								<div class="choose-method" data-analytics-retailer-shipping_method="${retailerInfoData.selectedShippingMethod}">
									<a>${retailerInfoData.selectedShippingMethod}</a>
								</div>
								<div class="tax-top-spacing">
									<span class="estimated-tax"><spring:theme code='order.confirmation.estimated.items.tax.txt' /></span>
									<span class="estimated-tax-price" data-analytics-retailer-tax="${retailerInfoData.estimatedTax.value}">
										${retailerInfoData.estimatedTax.formattedValue}
									</span>
								</div>
								<div class="last-call-spacing">
									<span class="last-call-retailer"><spring:theme code='order.confirmation.retailer.name.txt' arguments="${retailerInfoData.retailerName}" /></span>
									<span class="last-call-price" data-analytics-retailer-gross-total="${retailerInfoData.estimatedTotal.value}">
										${retailerInfoData.estimatedTotal.formattedValue}
									</span>
								</div>
								
								<c:set var = "retailerCode" value=""/>
								<c:set var = "retailerType" value=""/>
								<c:set var = "retailerDescription" value=""/>
								<c:if test="${not empty retailerInfoData.retailerPromotionList}">
								<c:forEach items="${retailerInfoData.retailerPromotionList}" var="retailerPromotionList" varStatus="status">
									<c:choose>
									<c:when test="${not status.last}">
										<c:set var = "retailerCode" value="${retailerCode}${retailerPromotionList.code}>"/>
										<c:set var = "retailerType" value="${retailerType}${retailerPromotionList.type}>"/>
										<c:set var = "retailerDescription" value="${retailerDescription}${retailerPromotionList.description}>"/>
									</c:when>
									<c:otherwise>
										<c:set var = "retailerCode" value="${retailerCode}${retailerPromotionList.code}"/>
										<c:set var = "retailerType" value="${retailerType}${retailerPromotionList.type}"/>
										<c:set var = "retailerDescription" value="${retailerDescription}${retailerPromotionList.description}"/>
									</c:otherwise>
									</c:choose>
								</c:forEach>
								</c:if>
								<div class="you-save-spacing" data-analytics-retailer-offercode="${retailerCode}" data-analytics-retailer-offertype="${retailerType}" data-analytics-retailer-offerdesc="${retailerDescription}">
									<span class="you-save"><spring:theme code='order.confirmation.you.save.txt' /></span>
									<span class="you-save-price" data-analytics-retailer-discount="${retailerInfoData.saving.value}">
										${retailerInfoData.saving.formattedValue}
									</span>
								</div>
							
															
								<c:forEach items="${retailerInfoData.promoMessages}" var="promoMessage">
									<div class="retailer-promotion-spacing">${promoMessage}</div>
								</c:forEach>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:forEach>
	<div class="order-summary-bottom">
		<order:orderSummaryBottom order="${orderData}" />
	</div>
</div>

<!--Shipping, Return and Policy modal start-->
<div class="modal fade" id="shippingReturnModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><spring:theme code="return.policy.message" /></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="privacy-content scrollablediv scroll-container"></div>
		</div>
	</div>
</div>
<!--Shipping, Return and Policy modal ends-->
<!--order Summary Help Modal start-->
<div class="modal fade orderSummaryHelpModal" id="orderSummaryHelpModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><spring:theme code="text.checkout.estimated.tooltip.title" /></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body clearfix">
				<p class="tax-message"><spring:theme code="text.checkout.estimated.tooltip.message1" /></p>
				<p class="tax-message">
					<spring:theme code="text.checkout.estimated.tooltip.message2" />
				</p>
				<div>
					<button class="btn secondary-btn close-wdt" data-dismiss="modal"><spring:theme code="text.checkout.estimated.tooltip.button.label"/></button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--order Summary Help Modal end-->