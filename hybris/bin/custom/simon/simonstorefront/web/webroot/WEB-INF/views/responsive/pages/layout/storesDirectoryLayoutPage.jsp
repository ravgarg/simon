<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<template:page pageTitle="${pageTitle}">

	<div class="container">
	<div class="analytics-pageContentLoad" data-analytics-pagetype="store-directory" data-analytics-subtype="store-directory" data-analytics-contenttype="informational">
		<div class="row">
			<div class="breadcrumbs col-md-3 hide-mobile">
				<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
				<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
			</div>
			<div class="col-md-9 hide" id="errorMsgContainer">
				<div class="error-msg analytics-formSubmitFailure"></div>
			</div>
		</div>
		<div class="row">
			<aside class="col-md-3 left-menu-container show-desktop">
				<ul class="left-menu level-0 list-unstyled">
					<li class="has-level-1 active"><cms:pageSlot
							position="csn_shopDirectoryLeftNavigationSlot" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot></li>
				</ul>
			</aside>
			<div class="col-md-9">
				
				<div class="my-stores page-container favorite-container">
					<h1 class="page-heading"><spring:theme
								code="text.account.myStores.store.text" />
								<a href="" class="add-deals hide-mobile" data-toggle="modal" data-key="my-stores" data-target="#moadl1Content"><spring:theme
								code="text.account.myStores.howToAdd" /></a>
					</h1>
					<div class="myfavorites-container my-store list">
						<cms:pageSlot position="csn_shopDirectoryTitleSlot" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						<a href="" class="how-to-add-store hide-desktop" data-toggle="modal" data-key="my-stores" data-target="#moadl1Content"><spring:theme
									code="text.account.myStores.howToAdd" /></a>
						<button class="col-xs-12 btn secondary-btn black major hide-desktop my-account" data-toggle="modal" data-target="#category">categories</button>
						<cms:pageSlot position="csn_shopDirectoryMainAreaSlot" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="category" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title major">Categories</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<aside class="col-md-3 left-menu-container">
						<ul class="left-menu level-0 list-unstyled">
							<li class="has-level-1 active"><cms:pageSlot
									position="csn_shopDirectoryLeftNavigationSlot" var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot></li>
						</ul>
					</aside>
				</div>
			</div>
		</div>	
		</div>	
	</div>
</template:page>