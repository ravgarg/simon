<div class="container">
	<jsp:include page="checkout-breadcrumb.jsp"></jsp:include>
	<div class="row">
		<div class="col-xs-12 col-md-8 spacing-right">
			<jsp:include page="checkout-guest-page.jsp"></jsp:include>
		</div>
		
		<div class="col-xs-12 col-md-4 order-summary-right hide-mobile">
            <jsp:include page="checkout-right-order-summary.jsp"></jsp:include>
        </div>
	</div>
</div>

<jsp:include page="checkout-verify-address-modal.jsp"></jsp:include>
<jsp:include page="checkout-leaving-modal.jsp"></jsp:include>
<jsp:include page="checkout-shipping-return-information.jsp"></jsp:include>