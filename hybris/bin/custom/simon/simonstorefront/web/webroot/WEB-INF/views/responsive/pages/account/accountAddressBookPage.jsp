<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="row analytics-addressBook">
	 <c:set var = "stateListSize" value = "${fn:length(stateList)}"/>
	 
	<div class="col-md-12">
		<button type="button" id="mAccountBtn" class="btn secondary-btn black my-account hide-desktop col-xs-12" data-toggle="modal" data-dismiss="modal" data-target="#mAccountModal"> <spring:theme code="header.link.account" /></button>
		
		<div class="modal fade plp-filters-modal" id="mAccountModal" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Account</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					
					<div class="modal-body clearfix">
						<div id="mAccount" class=""></div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="heading-text">

			<c:choose>
				<c:when test="${empty addressData}">

					<div class="shipping-add-text">
						<spring:theme code="text.accountaddressbook.shipping.address.text" />
					</div>

					<div class="not-saved-add-text">
						<spring:theme
							code="text.accountaddressbook.shipping.address.notsaved.text" />
					</div>

				</c:when>
				<c:otherwise>

					<div class="shipping-add-text">
						<spring:theme
							code="text.accountaddressbook.shipping.address.saveandmanage.text" />
					</div>
					
				</c:otherwise>
			</c:choose>
			
			<div class="edit-delete-address-box">
				<div class="row">
				
				<c:forEach items="${addressData}" var="address">
					<div class="col-md-4 address">
						<div class="address-border">
							<div class="address-box-margin">
								<div class="preferred-address-text">
									<div> ${address.firstName} &nbsp; ${address.lastName} </div>
									 <div> ${address.line1} &nbsp; ${address.line2} </div>
									 <div> ${address.town} ,&nbsp;  ${address.region.isocodeShort} &nbsp; ${address.postalCode}</div> 
									<div>${address.phone}</div>
								</div>
								
								<div class="sm-input-group">
								
									<c:choose>
										<c:when test="${address.defaultAddress eq true}">
											<input type="radio" id="preferredAddress" name="defaultAddress" data-addressid="${address.id}" checked>
										</c:when>
										
										<c:otherwise>
											<input type="radio" id="preferredAddress" class="analytics-genericLink" data-analytics-eventtype="address-book"  data-analytics-eventsubtype="preferred-address" data-analytics-linkplacement="address-book" data-analytics-linkname="preferred-address" name="defaultAddress" data-addressid="${address.id}">
										</c:otherwise>
									</c:choose>
									
									<label for="preferredAddress"><spring:theme
										code="text.accountaddressbook.preferredaddress.text" /></label>
								</div>
								
								<div class="edit-delete-links clearfix"> 
									<a href="#" class="edit-border addEditAddress analytics-genericLink" data-analytics-eventtype="address-book"  data-analytics-eventsubtype="edit-address" data-analytics-linkplacement="address-book" data-analytics-linkname="edit-address" data-action="editaddress" data-address='{"addressId": "${address.id}", "firstName" :"${address.firstName}", "lastName": "${address.lastName}", "line1": "${address.line1}", "line2": "${address.line2}", "town": "${address.town}", "region": {"isocode": "${address.region.isocode}"}, "postalCode": "${address.postalCode}", "phone": "${address.phone}", "defaultAddress": ${address.defaultAddress}}' data-toggle="modal">
										<spring:theme
										code="text.accountaddressbook.edit.text" /></a> 
									
									<a href="javascript:void(0)" class="delete-padding delete-address-link" data-addressdata='{"address" : "", "addressId" : "${address.id}"}'><spring:theme
										code="text.accountaddressbook.delete.text" /></a>
								</div>
							</div>
						</div>
					</div>
				</c:forEach>
				</div>
			</div>
			
			<button type="button" class="btn add-new-address addEditAddress col-xs-12 col-md-3 analytics-genericLink" data-analytics-eventsubtype="add-address_initiation" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-address" data-analytics-linkname="add-address"
				data-toggle="modal" data-target="#addressBookModal"><spring:theme code="text.accountaddressbook.addnewaddress.text" /></button>
		<form:form>
			<input type="hidden" id="resourceBundle" data-label='{
				"addnewaddressLabel": "<spring:theme code="text.accountaddressbook.addnewaddress.text" />",
				"optionalfieldsLabel": "<spring:theme code="text.accountaddressbook.optionalfields.text" />",
				"stateLabel": "<spring:theme code="text.accountaddressbook.state.label.text" />",
				"makedefaultaddressLabel": "<spring:theme code="text.accountaddressbook.makedefaultaddress.text" />",
				"addAddressButtonLabel": "<spring:theme code="text.accountaddressbook.add.button.text" />",
				"cancelButtonText": "<spring:theme code="text.accountaddressbook.cancel.button.text" />",
				
				"editAddressLabel":"<spring:theme code="text.accountaddressbook.edit.address.text" />",
				"editButtonLabel" : "<spring:theme code="text.accountaddressbook.edit.address.button.save.text" />",
				
				"firstNameLabel" : "<spring:theme code="text.accountaddressbook.firstname.text" />",
				"lastNameLabel" : "<spring:theme code="text.accountaddressbook.lastname.text" />",
				"addressLabel" : "<spring:theme code="text.accountaddressbook.address.text" />",
				"defaultAddressLabel" : "<spring:theme code="text.accountaddressbook.address.text" />",
				"cityLabel" : "<spring:theme code="text.accountaddressbook.city.placeholder.text" />",
				"zipLabel" : "<spring:theme code="text.accountaddressbook.zipcode.placeholder.text" />",
				"phoneLabel" : "<spring:theme code="text.accountaddressbook.phonenumber.placeholder.text" />",
				"stateList" : [
					<c:forEach items="${stateList}" var="state" varStatus="index">
					 {"isocode":"${state.stateCode}", "isocodeShort":"${state.isocodeShort}", "name":"${state.stateName}"} <c:if test="${stateListSize > index.index+1}">, </c:if>
					</c:forEach> 
							 ],
							 
				"deleteConfirmationModalTitle" : "<spring:theme code="text.accountaddressbook.delete.address.text" />",
				"deleteConfirmationModalMsg" : "<spring:theme code="text.accountaddressbook.deleteaddress.confirmmsg.text" />",
				"deleteConfirmationModalDeleteBtnLabel" : "<spring:theme code="text.accountaddressbook.delete.button.text" />",
				"deleteConfirmationModalCancelBtnLabel" : "<spring:theme code="text.accountaddressbook.cancel.button.text" />",
				
				"deleteAddressURL": "/my-account/remove-address",
				"countryISO" : "${countryIsocode}"
			}'>
		</form:form>
		</div>
		
		<div class="modal fade addressBookModal" id="addressBookModal"
			role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

				</div>
			</div>
		</div>
	
		<div class="modal fade addressBookModal" id="addressBookDeleteModal"
			role="dialog">
			<div class="modal-dialog">
				<div class="modal-content delete-modal-width">
					<div class="modal-header">
						<h5 class="modal-title"><spring:theme code="text.accountaddressbook.delete.address.text" /></h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close"></button>
					</div>
					<div class="modal-body clearfix">
						<p class="confirm-msg"><spring:theme code="text.accountaddressbook.deleteaddress.confirmmsg.text" />  </p>
						<div class="address-buttons">
							<button class="btn btn-add"><spring:theme code="text.accountaddressbook.delete.button.text" /></button>
							<button class="btn secondary-btn black" data-dismiss="modal"><spring:theme code="text.accountaddressbook.cancel.button.text" /></button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade checkout-verify-address-modal" id="myAccountVerifyAddressModal" role="dialog">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            
		        </div>
		    </div>
		</div>

	</div>
</div>