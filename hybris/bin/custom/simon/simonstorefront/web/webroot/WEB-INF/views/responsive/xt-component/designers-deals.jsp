	<div class="designer-deals clearfix">
		<div class="row">
			<div class="designer-deals-container col-xs-12 col-md-8">
				<h2 class="major">designer deals</h2>
				<h6>New Arrivals up to 40% off</h6>
				<a href="#" class="major">SHOP NOW&nbsp;<span>&rsaquo;</span></a>
			</div>
			<div class="designer-deals-container  designer-deals-img col-xs-12 col-md-4">
				<picture>
					<!--[if IE 9]><video class="hide"><![endif]-->
					<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/plp-image1@2x.png">
					<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/plp-image1.png">
				</picture>
				<span><a target="_self" href="" class="major hide-mobile">SHOP NOW<span>&rsaquo;</span></a></span>
			</div>
		</div>
	</div>
