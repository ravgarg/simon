;var com_hybrisconvert_addon_f = function() {

    Number.prototype.PadLeft = function (length, digit) {
        var str = '' + this;
        while (str.length < length) {
            str = (digit || '0') + str;
        }
        return str;
    };
    
    var api = {

        getAdminServiceCode : function() {
            if(window.com_hybrisconvert_addon_global && window.com_hybrisconvert_addon_global.cyAdminServiceCode) {
                return com_hybrisconvert_addon_global.cyAdminServiceCode;
            } else {
            	return '';
            }
        },

        getDataServiceCode : function() {
            if(window.com_hybrisconvert_addon_global && window.com_hybrisconvert_addon_global.cyDataServiceCode) {
                return com_hybrisconvert_addon_global.cyDataServiceCode;
            } else {
            	return '';
            }
        },

        getBrowseData : function() {
            
            // Browse stores 3 items in tfi. Should it store more at some point, we still only return 3 by
            // setting mbp here to 3. Differing numbers in tfi versus the mbp value below implies a requirement
            // on the items in tfi being ordered according to their score.
            var mbp = 3;

            var bd = {};
            bd.focusedProductID = '';
            bd.browseData = [];
            bd.currentProductID = (function() {
                if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon && ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']) {
                    var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']);
                    if (product) {
                        return product.properties.ItemID;
                    }
                }
                return '';
            })();

            try {
                common.testStorage();
            }
            catch(e) {
                return bd;
            }

            try {
                var tfi_s = localStorage.getItem(browse.constants.topFocusItems);
                common.debug("getBrowseData: tfi = " +tfi_s);
                var tfi = $.parseJSON(tfi_s);
                if (tfi) {
                    var p;
                    var i = 1;
                    var current_max = 0;
                    for (var psku in tfi) {
						if( !tfi.hasOwnProperty( psku ) ){ continue; } 
                        if (i > mbp) break;
                        p = {};
                        p.productID = psku;
                        bd.browseData.push(p);
                        if (tfi[psku] > current_max) {
                            bd.focusedProductID = psku;
                            current_max = tfi[psku];
                        }
                        ++i;
                    }
                }
                else {
                    common.debug("getBrowseData: tfi is null after parse");
                }
            }
            catch(e) {
                common.debug("getBrowseData: error parsing tfi: " +e);
            }

            return bd;
        }
    };

    var common = {
    		
        MAX_COOKIE_EXPIRY : (60*60*24*365*2),
        _debug : false,
        
        testStorage : function() {
            if (typeof(Storage) === "undefined") {
                throw new UserException("no storage");
            }
        },

        debug : function(s) {
            if (common._debug === true) {
                window.console && console.log(s);
            }
        },

        merge: function (destination, source) {
            for (var property in source) {
                if (source.hasOwnProperty(property)) {
                    destination[property] = source[property];
                }
            }
            return destination;
        },
        
        parseBreadcrumbs : function(generalData, delimiter){
          if (generalData.properties.ItemCategoryPath && generalData.properties.ItemCategoryPath.breadcrumbs){
          	var breadcrumbs = generalData.properties.ItemCategoryPath.breadcrumbs;
          	var itemCategoryPath = "";
          	for (var k=0; k<breadcrumbs.length; k++){
          		if (k>0){
          			itemCategoryPath+=delimiter;
          		}
          		itemCategoryPath+=breadcrumbs[k].code;
          	}
          	return itemCategoryPath;
          }
        },

        getTimeAndTZ : function() {
            var d = new Date();
            var t = d.getTime();
            var tz = d.getTimezoneOffset();
            return t+"~"+tz;
        },

        getCookie : function(cn) {
            var name = cn + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; ++i) {
                var c = ca[i];
                while (c.charAt(0)==' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return decodeURIComponent(c.substring(name.length));
                }
            }
            return "";
        },

         setCookie : function(cn, cv, ma) {
             var mas = "";
             var ds = "";
             var domain = com_hybrisconvert_addon_global.cyDomain;
             if (domain) {
                 ds = ";domain=" +domain;
             }
             if (ma) {
                 //'expires' is obsolete, but IE does not support max-age and all browsers support expires
                 var date = new Date();
                 date.setTime(date.getTime()+(ma*1000));
                 mas = ";expires=" +date.toUTCString();
             }
             document.cookie = cn+'='+encodeURIComponent(cv)+";path=/"+ds+mas;
        }
    };

    var hybrisconvert = {
        constants : {
            VersionValue : 'hc/1',
            SIDcookie : '__cy_d',
            PageTypeProduct : 'PRODUCT',
            PageTypeOrderConfirmation : 'ORDERCONFIRMATION',
            PageTypeProductSearch : 'PRODUCTSEARCH',
            PageTypeCategory : 'CATEGORY',
            PageTypeCart : 'CART',
            Path : '/abandonment2/WE/seewhy.nogif/',
            treatmentCookieName:'__cy_abcookie'
        },

        addBrowseProduct : function(product) {

            if (!product.properties.ItemMasterID || product.properties.ItemMasterID.length == 0) {
                product.properties.ItemMasterID = product.properties.ItemID;
            }

            browse.addProduct(product);
        },

        productBrowsed : function() {
        	if (window.com_hybrisconvert_addon_global && typeof com_hybrisconvert_addon_global.mode !== 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
        	    hybrisconvert.m0_productBrowsed();
        	}
        	else {
        	    hybrisconvert.m1_productBrowsed();
        	}
        },
        
        m1_productBrowsed : function() {
            if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon && window.com_hybrisconvert_addon_global) {
                var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']);                
                if (product) {
                	
    				var itemID = product.properties.ItemID;
    				var itemMasterID = product.properties.ItemMasterID;
                    
    				hybrisconvert.addBrowseProduct(product);

                    if (itemMasterID && itemID !== itemMasterID) {
                        browse.productVariantSelect(itemMasterID);
                    }
                }
            }
        },
        
        m0_productBrowsed : function() {
            if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon && window.com_hybrisconvert_addon_global) {
                var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']);                
                if (product) {
                	var action = 'PV';
                    if (product.properties.ItemMasterID && product.properties.ItemID !== product.properties.ItemMasterID) {
                        action = 'VS';
                    }
                    if (!product.properties.ItemMasterID || product.properties.ItemMasterID.length == 0) {
                        product.properties.ItemMasterID = product.properties.ItemID;
                    }
    				s_browse.sendEvent(product, action);
                    try {
                        localStorage.setItem(s_browse.constants.lastProduct, JSON.stringify({'product' : product, 'dwell' : '0'}));
                    }
                    catch(err){}
                }
            }
        },

        m1_productReviewed : function() {
            var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductReviewed']);
            if (product) {                
              if (!product.properties.ItemMasterID || product.properties.ItemMasterID.length == 0) {
                product.properties.ItemMasterID = product.properties.ItemID;
              }
              
                
              if (!browse.hasRecord('p', product.properties.ItemMasterID)) {
                  hybrisconvert.addBrowseProduct(product);
              }
              // User's rating is available in cyActionProductReviewed.ReviewScore. productReviewed function does not accept that
              // as a parameter so no point retrieving it. All reviews, regardless of score, are regarded as positive.
              browse.productReviewed(product.properties.ItemMasterID);
            }
        },

        m0_productReviewed : function() {
            var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductReviewed']);
            if (product) {
                var action = 'PR';
                s_browse.sendEvent(product, action);
            }
        },
        
        bindBrowseListeners : function() {
            $(".a2a_i").on("click", function(event) {
                if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon && ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']) {
                    var product = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']);
                    if (product) {
                      if (!product.properties.ItemMasterID || product.properties.ItemMasterID.length == 0) {
                        product.properties.ItemMasterID = product.properties.ItemID;
                      }

                        
                      if (typeof com_hybrisconvert_addon_global.mode != 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
                          var action = 'PS';
                          s_browse.sendEvent(product, action);
                      }
                      else {
                          browse.productSocialLike(product.properties.ItemMasterID);
                      }
                    }
                }
            });
        },

        generateUUID : function() {
            // Generate an RFC 4122 compliant version 4 UUID
            return 'NNNNNNNN-NNNN-4NNN-XNNN-NNNNNNNNNNNN'.replace(/[NX]/g, function(c) {
                var rn = Math.floor(Math.random()*16);
                if (c == 'N') {
                    v = rn;
                }
                else {
                    // 0 <= rn < 16, but RFC 4122 requires that the 2 most sig bits be 1 and 0
                    v = (rn&0x3|0x8);
                }
                return v.toString(16);
            }).toUpperCase();
        },

        getSessionID : function() {
            var sid = common.getCookie(hybrisconvert.constants.SIDcookie);
            if (sid === "") {
                sid = hybrisconvert.generateUUID();
            }
            common.setCookie(hybrisconvert.constants.SIDcookie, sid, common.MAX_COOKIE_EXPIRY);
            return sid;
        },

        sendEvent : function(p) {
            var method;
            var qs;
            var url = com_hybrisconvert_addon_global.cyTargetURL;
            var sc = com_hybrisconvert_addon_global.cyDataServiceCode;
            var sed1 = com_hybrisconvert_addon_global.cyStaticEventData01;
            var sed2 = com_hybrisconvert_addon_global.cyStaticEventData02;

            if (ACC.addons.hybrisconvertaddon['cyActionEmailCaptured']){
                var emailInformation = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionEmailCaptured']);
                var uid = emailInformation.properties.EmailAddress || '';
                var fn = emailInformation.properties.FirstName || '';
            }
            var treatmentCookie = common.getCookie(hybrisconvert.constants.treatmentCookieName);
            if (treatmentCookie){
            	p['Custom11'] = treatmentCookie;
            }

            var sid = hybrisconvert.getSessionID();
            if (url && sc) {
                if (uid) {
                    p['UserID'] = uid;
                }
                if (fn) {
                    p['Custom1'] = fn;
                }
                method = window.location.protocol.toLowerCase().indexOf('https') >= 0 ? "https://" : (p['UserID'] ? 'https://' : 'http://');
                if (url.match(/^\/\//)) {
                    url = url.replace(/^\/\/(.*)/, method+'$1');
                }
                else if (!url.match(/^(http:\/\/|https:\/\/)/)) {
                    url = method+url;
                }
                else {
                    url = url.replace(/^http:\/\/(.*)/, method+'$1');
                }
                
                if (typeof com_hybrisconvert_addon_global.mode != 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
                	if (typeof com_hybrisconvert_addon_global.yaasTenant != 'undefined' && com_hybrisconvert_addon_global.yaasTenant.length > 0) {
                		p['Custom6'] = com_hybrisconvert_addon_global.yaasTenant;
                	}
                    if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon['cyActionGeneral']) {
                        var g = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionGeneral']);
                        if (g && g.properties && typeof g.properties.ServerContext != 'undefined') {
                            var hsid = g.properties.ServerContext;
                            if (hsid) {
                                p['Custom7'] = hsid;
                            }
                        }
                    }
                }
                
				var m;
				try {
				    m = common.getCookie(browse.constants.labelOpCode);
                    if (typeof com_hybrisconvert_addon_global.mode != 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
                        if (m && m==='cart') {
                            p['Custom9'] = 'cart0';
                        }
                        else {
                            p['Custom9'] = 'Browse0';
                        }
                    }
                    else {
                        if (m && m==='cart') {
                            p['Custom9'] = 'cart';
                        }
                        else {
                            p['Custom9'] = 'Browse';
                        }
                    }
				}
				catch(e){}
				
                qs = '?Event=WebEvent&CustomerCode=' +sc+ '&SessionID=' +encodeURIComponent(sid);
                qs += '&Version='+encodeURIComponent(hybrisconvert.constants.VersionValue);
                qs += '&ClientTimeAndTZ='+common.getTimeAndTZ();
                qs += '&DefaultPageName='+encodeURIComponent(window.location.pathname);
                qs += '&Referrer='+encodeURIComponent(window.document.referrer);
                qs += '&Server='+encodeURIComponent(window.document.domain);
                if (sed1) {
                    qs += '&sed1='+encodeURIComponent(sed1);
                }
                if (sed2) {
                    qs += '&sed2='+encodeURIComponent(sed2);
                }

                for (var n in p) {
                    if (p.hasOwnProperty(n)) {
                        qs += '&' + n + '=' + encodeURIComponent(p[n]);
                    }
                }

                var i = document.createElement("img");
                i.width=1; i.height=1; i.border=0;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(i, s);
                i.src = url + hybrisconvert.constants.Path + Math.random().toString().slice(2) + qs;
                i.style.cssText = 'display:none;';
            }
        },

        formatBasketLine : function(i, l) {
            var s = i.toString();
            if (s.length < l) {
                s = ('00' + s).slice(-l);
            }
            return 'CYBK' + s;
        },

        getBasketAppend : function() {
            var ba = '1';
            try {
                var m = common.getCookie(browse.constants.labelOpCode);
                if (!m) {
                    ba = '0';
                }
            }
            catch(err){}
            return ba;
        },

        isQuickOrder : function() {
        	if(ACC.quickorder) {
        	   var quickOrderRegex=/^.*quickOrder$/;
        	   if(quickOrderRegex.test(window.location.pathname)) {
        		   return true;
        	   }
        	}
        	return false;
        },

        addToCart : function(hybrisconvertCart) {
            var atc = hybrisconvertCart['cyActionAddToCart'];
            if (atc) {
            	var basketProducts = atc.properties.Products;
                for(var b=0; b<basketProducts.length; b++) {
                	var ba = hybrisconvert.getBasketAppend();
                    browse.terminate();
                    var p = {};
                	var s = hybrisconvert.formatBasketLine(1, 3);
                	var product = basketProducts[b].properties;

	                p[s+'ItemID'] = product.ItemID;
	                if (product.ItemMasterID && product.ItemMasterID.length > 0){
	                    p[s+'ItemMasterID'] = product.ItemMasterID;
	                } else {
	                    p[s+'ItemMasterID'] = product.ItemID;
	                }
	                p[s+'ItemMasterAlwaysPresent'] = 'true';
	                p[s+'ItemName'] = product.ItemName;
	                p[s+'ItemDesc'] = product.ItemDesc;
	                p[s+'ItemPrice'] = product.ItemPrice;
	                p[s+'ItemPriceCurrency'] = product.ItemPriceCurrency;
	                p[s+'ItemQuantity'] = product.ItemQuantity;
	                p[s+'ItemPageURL'] = product.ItemPageURL;
	                p[s+'ItemImageURL'] = product.ItemImageURL;
	                p[s+'ItemStockLevel'] = product.ItemStockLevel;
	                //Only store the first category
	                /**
	                 * Loop around the categories and output them individually
	                 */
	                if (product.ItemTotalCategories){
	                    for (var i=0; i<product.ItemTotalCategories; i++){
	                        var index = i+1;
	                        p[s+'ItemCategory_'+index.PadLeft(2, 0)]=product.ItemCategories[i].code;
	                    }
	                }
	                /**
	                 * Loop around the facets and output them individually
	                 */
	                if (product.ItemTotalFacets){
	                    for (var j=0; j<product.ItemTotalFacets; j++){
	                        var facetPropertyName = s+"ItemFacet_"+product.ItemFacets[j].code;
	                        for (var k=0; k<product.ItemFacets[j].values.length; k++){
	                            var index = k+1;
	                            var facetValuepropertyName = facetPropertyName+"_"+index.PadLeft(2, 0);
	                            p[facetValuepropertyName]=product.ItemFacets[j].values[k];
	                        }
	                    }
	                }
	
	                p[s+'ItemReviewScore'] = product.ItemReviewScore;
	            

	                if (typeof com_hybrisconvert_addon_global.mode != 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
	                	p['Custom8'] = 'CA';
	                }
	                p['FunnelLevel'] = '4';
	                p['Value'] = atc.properties.CartValue;
	                p['ReturnToLink'] = atc.properties.ReturnToLink;
	                p['BasketAppend'] = ba;
	
	                if(!hybrisconvert.isQuickOrder()) {
	                   var parsedBreadcrumbs = common.parseBreadcrumbs(hybrisconvertCart['cyActionGeneral'], browse.constants.delimiter);
	                   if (parsedBreadcrumbs){
	                	   p['ItemCategoryPath'] = parsedBreadcrumbs;
	                   }
	                }
	                hybrisconvert.sendEvent(p);
                }
            }
        },

        orderPlaced : function() {

            browse.terminate();

            var p = {};
            p['FunnelLevel'] = '7';

            if (ACC.addons.hybrisconvertaddon['cyActionOrderPlaced']){
              var order = JSON.parse(ACC.addons.hybrisconvertaddon['cyActionOrderPlaced']);
              var on = order.properties.OrderNumber;
              var ov = order.properties.OrderValue;
            }

            p['OrderNumber'] = on;
            p['Value'] = ov;

            hybrisconvert.sendEvent(p);
        },

        emailCapture : function() {
            var qs = location.search ? location.search : '';
            if (qs.charAt(0) == '?') {
                qs = qs.substring(1);
            }

            if (qs.length > 0) {
                qs = qs.replace(/\+/g, ' ');
                var qsp = qs.split(/[&;]/g);

                for (var i = 0; i < qsp.length; i++) {
                    var kvp = qsp[i].split('=');
                    var v = kvp.length > 1 ? decodeURIComponent(kvp[1]) : '';
                    if (v.match(/\S+@\S+\.\S+/)) {
                        var p = {};
                        p['Custom1'] = 'Guest';
                        p['UserID'] = v;
                        p['FunnelLevel'] = '0';
                        hybrisconvert.sendEvent(p);
                        break;
                    }
                }
            }
        },

        emailLoginCapture : function() {
            var p = {};
            p['FunnelLevel'] = '0';
            hybrisconvert.sendEvent(p);
        },

        bindToAddToCartForm: function () {
            var addToCartForm = $('.add_to_cart_form');
            if (addToCartForm.length) {
                addToCartForm.ajaxForm({
                    success: function(cartResult, statusText, xhr, formElement) {
                        hybrisconvert.addToCart(cartResult.hybrisconvertCart);
                        ACC.product.displayAddToCartPopup(cartResult, statusText, xhr, formElement);
                    }
                });
            }
        },

        bindToAddToCartStorePickUpForm: function () {
           $(document).on("click",".js-pickup-in-store-button",function(e){
              var addToCartStorePickUpForm = $('#colorbox #add_to_cart_storepickup_form');
              addToCartStorePickUpForm.ajaxFormUnbind();
              addToCartStorePickUpForm.ajaxForm({
                 success: function(response, status, xhr, element){
                    hybrisconvert.addToCart(response.hybrisconvertCart);
                    ACC.product.displayAddToCartPopup(response, status, xhr, element);
                 }
              });
          });
        },

        bindToQuickOrder: function () {
           if(ACC.quickorder) {
	           var addToCartHandler = function() {
	              $.ajax({
	                   url: ACC.quickorder.$quickOrderContainer.data('quickOrderAddToCartUrl'),
	                   type: 'POST',
	                   dataType: 'json',
	                   contentType: 'application/json',
	                   data: ACC.quickorder.getJSONDataForAddToCart(),
	                   async: false,
	                   success: function (response) {
	                       hybrisconvert.addToCart(response.hybrisconvertCart);
	                       ACC.quickorder.handleAddToCartSuccess(response);
	                   },
	                   error: function (jqXHR, textStatus, errorThrown) {
	                       
	                   }
	               });
	           };
	           $('#js-add-to-cart-quick-order-btn-top, #js-add-to-cart-quick-order-btn-bottom').unbind("click");
	           var addToCartButton = $('#js-add-to-cart-quick-order-btn-top, #js-add-to-cart-quick-order-btn-bottom');
	           addToCartButton.on("click", addToCartHandler);
           }
        }
    };

    var s_browse = {
        constants  : {
            'lastProduct' : 'com.hybrisconvert.lastProduct'
        },
        
        sendEvent : function(product, action) {
            try {
                common.testStorage();
                var m = common.getCookie(browse.constants.labelOpCode);
                if (m && m === "cart") {
                    return;
                }
                var s = hybrisconvert.formatBasketLine(1, 3);
                var r = browse._createRecord(product);
                var e = browse._createBasketItem(r, s, true);
                e['FunnelLevel'] = '3';
                e['BasketAppend'] = '0';
                e['Custom8'] = action;
                
                hybrisconvert.sendEvent(e);
            }
            catch(err){}
        },
        
        dwellStart : function() {
        	try {
                common.testStorage();
                s_browse.pageOpenTime = new Date().getTime();
                var lp = localStorage.getItem(s_browse.constants.lastProduct);
                if (lp) {
                    var olp = JSON.parse(lp);
                    if (olp && olp.dwell && olp.product) { 
                        var action = 'PD|'+olp.dwell;
                        s_browse.sendEvent(olp.product, action);
                    }
                    localStorage.removeItem(s_browse.constants.lastProduct);
                }
        	}
        	catch (err){}
        },
        
        _unload : function() {
        	// update the LS record with the time this page has been open
            try {
        	    common.testStorage();
        	    var dwell = new Date().getTime() - s_browse.pageOpenTime;
                var lp = localStorage.getItem(s_browse.constants.lastProduct);
                if (lp) {
                    var olp = JSON.parse(lp);
                    if (olp) {
                        olp.dwell = (dwell / 1000).toFixed(2);
                        localStorage.setItem(s_browse.constants.lastProduct, JSON.stringify(olp));
                    }
                }
            }
            catch(err){}
        }
    };
    
    var browse = {

        _cysku : null,
        constants : {
            "topFocusItems":"com.hybrisconvert.b.tfi",
            "currentsku":"com.hybrisconvert.b.cysku",
            "topFocusCategories":"tfc",
            "sku":"sku",
            "productname":"name",
            "productItemId":"productItemId",
            "productMasterId":"productMasterId",
            "productdescription":"description",
            "productURL":"productURL",
            "imageURL":"imageURL",
            "price":"price",
            "recommendations":"recommendations",
            "isVariant":"isVariant",
            "masterSKU":"masterSKU",
            "category":"com.hybrisconvert.b.category",
            "reviewScore":"reviewScore",
            "reviewSummary":"reviewSummary",
            "reviewLink":"reviewLink",
            "averageEngagementTimeSecs":18,
            "score":"score",
            "scoreThreshold":6.0,
            "ditchThreshold":0.0005,
            "scorematrix":"com.hybrisconvert.b.sm",
            "activityCount":"count",
            "activityDwell":"dwell",
            "activityLastViewed":"lastViewed",
            "activityBookmark":"bookmark",
            "activitySocialMark":"social",
            "activityFacebookShare":"share",
            "activityPrint":"print",
            "activityEmail":"email",
            "activityReview":"review",
            "activityTweet":"tweet",
            "activityVariantSelect":"variant",
            "labelOpCode":"com.hybrisconvert.b.opCode",
            "activityDwellSF":10,
            "maximumInterestPeriod":86400,      //one day
            "skus":"com.hybrisconvert.b.skus",
            "categories":"com.hybrisconvert.b.categories",
            "category_property":"categories",
            "facets_property":"facets",
            "stockLevel_property":"stockLevel",
            "item_category_path_property" : "itemCategoryPath",
            "delimiter" : "**CYD**"
        },

        weights : {
            "count":1,
            "dwell":1,
            "bookmark":3,
            "social":5,
            "share":5,
            "tweet" : 5,
            "email" : 10,
            "print" : 15,
            "review" : 10,
            "variant":2
        },

        addRecommendation : function(sku, recommendationSKU, name, imageURL, productURL) {
            try {
                common.testStorage();
                if (browse.hasRecord('p', sku))
                {
                    var record = browse._getRecord('p', sku);
                    var recommend = record[browse.constants.recommendations];
                    if (!recommend.hasOwnProperty(recommendationSKU))
                    {
                        var item = {};
                        item[browse.constants.productname] = name;
                        item[browse.constants.imageURL] = imageURL;
                        item[browse.constants.productURL] = productURL;
                        recommend[recommendationSKU] = item;
                        browse._writeField('p', sku, record);
                    }
                }
            }
            catch(err){}
        },

        _checkCategory : function(category) {
            //check that category is in list
            var categories;
            categories_s = localStorage.getItem(browse.constants.category);
            if (!categories_s) {
                categories = {};
            }
            else {
                categories = JSON.parse(categories_s);
            }

            if (!categories.hasOwnProperty(category)) {
                categories[category] = 1;
            }
            else {
                categories[category]++;
            }

            localStorage.setItem(browse.constants.category, JSON.stringify(categories));
        },

        addProduct : function(product) {
            try {
                common.testStorage();

                if (!browse.hasRecord('p', product.properties.ItemMasterID)) {
                    var r = browse._createRecord(product);
                    browse._writeField('p', product.properties.ItemMasterID, r);
                }

                var scorematrix = {};
                var scorematrix_s = localStorage.getItem(browse.constants.scorematrix);
                if (scorematrix_s) {
                    scorematrix = JSON.parse(scorematrix_s);
                }

                var d = new Date();

                if (scorematrix[product.properties.ItemMasterID]) {
                    var scorecard = scorematrix[product.properties.ItemMasterID];

                    scorecard[browse.constants.activityCount] = scorecard[browse.constants.activityCount]+1;
                    scorecard[browse.constants.activityLastViewed] = d.getTime();
                }
                else {
                    var scorecard = {};
                    scorecard[browse.constants.activityCount] = 1;
                    scorecard[browse.constants.activityDwell] = 0;
                    scorecard[browse.constants.activityBookmark] = 0;
                    scorecard[browse.constants.activitySocialMark] = 0;
                    scorecard[browse.constants.activityFacebookShare] = 0;
                    scorecard[browse.constants.activityTweet] = 0;
                    scorecard[browse.constants.activityEmail] = 0;
                    scorecard[browse.constants.activityPrint] = 0;
                    scorecard[browse.constants.activityReview] = 0;
                    scorecard[browse.constants.activityLastViewed] = d.getTime();
                    scorecard[browse.constants.activityVariantSelect] = 0;
                    scorecard[browse.constants.score] = 0;

                    scorematrix[product.properties.ItemMasterID] = scorecard;
                }

                localStorage.setItem(browse.constants.scorematrix, JSON.stringify(scorematrix));

                browse._cysku = product.properties.ItemMasterID;
                localStorage.setItem(browse.constants.currentsku, product.properties.ItemMasterID);

                for (var i=0; i<product.properties.ItemCategories.length; i++) {
                    browse._checkCategory(product.properties.ItemCategories[i].code);
                }
            }
            catch(err){}
        },

        productVariantSelect : function(sku) {
            try {
                common.testStorage();
                browse._process(sku, browse.constants.activityVariantSelect);
            }
            catch(err){}
        },

        productSocialLike : function(sku) {
            try {
                common.testStorage();
                browse._process(sku, browse.constants.activitySocialMark);
            }
            catch(err){}
        },

        productReviewed : function(sku) {
            try {
                common.testStorage();
                browse._process(sku, browse.constants.activityReview);
            }
            catch(err){}
        },

        _productPageDwell : function(sku, dwell) {
            var scorematrix_s = localStorage.getItem(browse.constants.scorematrix);
            if (scorematrix_s) {
                var scorematrix = JSON.parse(scorematrix_s);
                var scorecard = scorematrix[sku];
                if (scorecard) {
                    var os = scorecard[browse.constants.activityDwell];
                    var ns = browse._dwellScore(dwell);
                    if (ns > os) {
                        scorecard[browse.constants.activityDwell] = ns;
                    }
                    browse._propagateScores(scorematrix, scorecard);
                }
            }
        },

        _process : function(sku, field) {
            var scorematrix_s = localStorage.getItem(browse.constants.scorematrix);
            if (scorematrix_s) {
                var scorematrix = JSON.parse(scorematrix_s);
                var scorecard = scorematrix[sku];
                if (scorecard) {
                    scorecard[field] = 1;
                    browse._propagateScores(scorematrix, scorecard);
                }
            }
        },

        _propagateScores : function(scorematrix, scorecard) {
            var d = new Date();
            var t = d.getTime();
            browse._rescore(scorematrix, t);

            browse._extractCurrentFocusProducts(scorematrix);
            scorecard[browse.constants.activityLastViewed] = t;
            localStorage.setItem(browse.constants.scorematrix, JSON.stringify(scorematrix));
        },

        _rescore : function(scorematrix, t) {
            try
            {
                for (var sku in scorematrix) {
                    if (scorematrix.hasOwnProperty(sku)) {
                        var scorecard = scorematrix[sku];
                        var secs = (t - scorecard[browse.constants.activityLastViewed]) / 1000;
                        var td = browse._timedecayScore(secs);
                        var ws = browse._weightedScore(scorecard,secs);
                        //high ws should dominate the score.  However, if the majority of ws is made of
                        //dwell, then this domination will diminish faster than td
                        var s = td * ws;
                        if (scorecard[browse.constants.score]==0) {
                            if (s > browse.constants.scoreThreshold) {
                                scorecard[browse.constants.score] = s;
                            }
                        }
                        else {
                            if (s < browse.constants.ditchThreshold) {
                                delete scorematrix[sku];
                            }
                            else {
                                scorecard[browse.constants.score] = s;
                            }
                        }
                    }
                }
            }
            catch(err){}
        },

        _weightedScore : function(scorecard,secs) {
            var score = 1;
            score = score + scorecard[browse.constants.activityCount];
            score = score + browse.weights.bookmark * scorecard[browse.constants.activityBookmark];
            score = score + browse.weights.social * scorecard[browse.constants.activitySocialMark];
            score = score + browse.weights.share * scorecard[browse.constants.activityFacebookShare];
            score = score + browse.weights.tweet * scorecard[browse.constants.activityTweet];
            score = score + browse.weights.email * scorecard[browse.constants.activityEmail];
            score = score + browse.weights.print * scorecard[browse.constants.activityPrint];
            score = score + browse.weights.review * scorecard[browse.constants.activityReview];
            score = score + browse.weights.variant * scorecard[browse.constants.activityVariantSelect];

            var activityScore = Math.log(score * 10.0) / Math.log(2);
            var dwellScore = browse._dwellScore(secs);
            return Math.ceil(activityScore*dwellScore);
        },

        _dwellScore : function(dwell) {
            if (dwell >= browse.constants.averageEngagementTimeSecs) {
                return 6.0*Math.exp(-dwell/10000);
            }
            else {
                var v = Math.exp(dwell * 0.15 / 5.0);
                return v;
            }
        },

        _timedecayScore : function(secs) {
            var dd = secs/86400;
            var s = Math.exp(-0.005*dd*dd);
            return s;
        },

        _sendEvent : function() {
            var m = common.getCookie(browse.constants.labelOpCode);
            if (m && m === "cart") {
                return;
            }

            var records = JSON.parse(localStorage.getItem(browse.constants.topFocusItems));
            var categories_s = localStorage.getItem(browse.constants.category);
            var categories = JSON.parse(categories_s);

            var scorematrix;
            var scorematrix_s = localStorage.getItem(browse.constants.scorematrix);
            if (scorematrix_s) {
                scorematrix = JSON.parse(scorematrix_s);
            }

            var key;
            var catcount = 0;
            for (key in categories) {
                if (categories.hasOwnProperty(key)) {
                    catcount++;
                }
            }
            var scores = {};
            var prods = {};
            var prodcount = 0;
            if (scorematrix && records) {
                for (key in records) {
					if( !records.hasOwnProperty( key ) ){ continue; } 
                    prods[key] = browse._getRecord('p', key);
                    var sc = scorematrix[key];
                    scores[key] = sc['score'];
                    prodcount++;
                }
            }

            browse._sendBrowseEvent(prodcount, prods, scores, catcount, categories);
        },

        _extractCurrentFocusProducts : function(scorematrix) {
            var records = {};
            var oldrecords = {};
            var upload = false;

            if (localStorage.getItem(browse.constants.topFocusItems)) {
                oldrecords = JSON.parse(localStorage.getItem(browse.constants.topFocusItems));
            }

            for (key in scorematrix) {
				if( !scorematrix.hasOwnProperty( key ) ){ continue; } 
                browse._processScoreCard(key, scorematrix[key], records);
            }

            localStorage.setItem(browse.constants.topFocusItems, JSON.stringify(records));
            localStorage.setItem(browse.constants.scorematrix, JSON.stringify(scorematrix));

            for (key in records){
                if (!oldrecords.hasOwnProperty(key)) {
                    upload = true;
                    break;
                }
            }

            if (upload) {
                browse._sendEvent();
            }
        },

        _processScoreCard : function(key, scorecard, records) {
            var upload = false;

            if (browse._objectSize(records) > 0) {
                if (browse._objectSize(records) < 3) {
                    records[key] = scorecard.score;
                    if (scorecard.score > browse.constants.scoreThreshold) {
                        upload = true;
                    }
                }
                else if (records.hasOwnProperty(key)) {
                    if (records[key] < scorecard.score) {
                        records[key] = scorecard.score;
                    }
                }
                else {
                    var smallestkey = "none";
                    var smallestvalue = 1000;

                    for (var i in records) {
                        if (records.hasOwnProperty(i)) {
                            if (records[i] < scorecard.score) {
                                if (records[i] < smallestvalue) {
                                    smallestkey = i;
                                    smallestvalue = records[i];
                                }
                            }
                        }
                    }

                    //if candidate found, replace it here
                    if (smallestkey !== "none") {
                        delete records[smallestkey];
                        records[key] = scorecard.score;
                        upload = true;
                    }
                }
            }
            else {
                if (scorecard.score > browse.constants.scoreThreshold) {
                    records[key] = scorecard.score;
                    upload = true;
                }
            }

            return upload;
        },

        _objectSize : function(obj) {
            var size = 0, key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) size++;
            }
            return size;
        },

        hasRecord : function(type, item)
        {
            var rb = false;
            try
            {
                common.testStorage();
                var lsType;
                if (type == 'p') {
                    lsType = browse.constants.skus;
                }
                else if (type == 'c') {
                    lsType = browse.constants.categories;
                }
                var i = localStorage.getItem(lsType);
                if (i) {
                    var items = JSON.parse(i);
                    var r = items[item];
                    if (r) {
                        rb = true;
                    }
                }
            }
            catch(err) {}

            return rb;
        },

        _createRecord : function(product) {
        	
            var record = {};
            record[browse.constants.productname] = product.properties.ItemName;
            record[browse.constants.productdescription] = product.properties.ItemDesc;
            record[browse.constants.imageURL] = product.properties.ItemImageURL;
            record[browse.constants.productURL] = product.properties.ItemPageURL;
            record[browse.constants.price] = product.properties.ItemPrice;
            record[browse.constants.reviewScore] = product.properties.ItemReviewScore;
            record[browse.constants.reviewSummary] = product.properties.ItemReviewSummary;
            record[browse.constants.reviewLink] = product.properties.ItemReviewLink;
            record[browse.constants.stockLevel_property] = product.properties.ItemStockLevel;

            // Loop around the categories and store them in an array
            if (product.properties.ItemTotalCategories){
                var categoryArray = new Array();
                for (var i=0; i<product.properties.ItemTotalCategories; i++){
                    categoryArray.push(product.properties.ItemCategories[i].code);
                }
                record[browse.constants.category_property] = categoryArray;    
            }            
            
            // Loop around the facets and for every facet store all of its values against its key (essentially a Map)
            if (product.properties.ItemTotalFacets){
                var facets = {};
                for (var j=0; j<product.properties.ItemTotalFacets; j++){
                    var facetValueArray = new Array();
                    for (var k=0; k<product.properties.ItemFacets[j].values.length; k++){
                        facetValueArray.push(product.properties.ItemFacets[j].values[k]);
                    }
                    facets[product.properties.ItemFacets[j].code] = facetValueArray;
                }
                record[browse.constants.facets_property] = facets;
            }
            record[browse.constants.recommendations] = {};
            
            record[browse.constants.productItemId] = product.properties.ItemID;
            record[browse.constants.productMasterId] = product.properties.ItemMasterID;
            
            var parsedBreadcrumbs = common.parseBreadcrumbs(JSON.parse(ACC.addons.hybrisconvertaddon['cyActionGeneral']), browse.constants.delimiter);
            if (parsedBreadcrumbs) {
            	record[browse.constants.item_category_path_property] = parsedBreadcrumbs;
            }

            return record;
        },

        terminate : function() {
            try {
                common.testStorage();

                localStorage.removeItem(browse.constants.skus);
                localStorage.removeItem(browse.constants.categories);
                localStorage.removeItem(browse.constants.category);
                localStorage.removeItem(browse.constants.topFocusItems);
                localStorage.removeItem(browse.constants.scorematrix);
                localStorage.removeItem('com.hybrisconvert.b.edc');
                localStorage.removeItem('com.hybrisconvert.b.lastsku');

                common.setCookie(browse.constants.labelOpCode, "cart");
             }
            catch(err){}
        },

        _createBasketItem: function(record, s, includeReviewScore) {
            
            var p = {};
            
            p[s+'ITEMID'] = record['productItemId'];            
            p[s+'ITEMMASTERALWAYSPRESENT'] = 'true';
            p[s+'ITEMPRICE'] = record['price'];
            p[s+'ITEMNAME'] = record['name'];
            p[s+'ITEMIMAGEURL'] = record['imageURL'];
            p[s+'ITEMDESC'] = record['description'];
            p[s+'ITEMPAGEURL'] = record['productURL'];
            p[s+'ITEMMASTERID'] = record['productMasterId'];
            p[s+'ITEMSTOCKLEVEL'] = record['stockLevel'];
            
            if (record['categories']) {
                var categories = record['categories']; 
                for (var i=0; i<categories.length; i++) {
                    var index = i+1;
                    p[s+'ITEMCATEGORY_'+index.PadLeft(2, 0)] = categories[i];
                }
            }

            if (record['facets']) {
                var facets = record['facets'];
                for (var key in facets) {
					if( !facets.hasOwnProperty( key ) ){ continue; } 
                    var facetName = "ITEMFACET_"+key.toUpperCase();
                    var facetValues = facets[key];
                    for (var j=0; j<facetValues.length; j++) {
                        var jIndex = j+1;
                        var facetValueName = facetName+"_"+jIndex.PadLeft(2, 0)
                        p[s+facetValueName] = facetValues[j];
                    }
                }
            }
            
            if (includeReviewScore) {
                p[s+'ITEMREVIEWSCORE'] = record['reviewScore'];
                p[s+'ITEMREVIEWSUMMARY'] = record['reviewSummary'];
                p[s+'ITEMLink'] = record['reviewLink'];
                top = false;
            }
            
            var recommendations = record['recommendations'];
            
            var append = 0;
            for (var rkey in recommendations) {
                if (recommendations.hasOwnProperty(rkey)) {
                    var r = recommendations[rkey];
                    prepend = 'R' + append++;
                    p[s+prepend+'ITEMID'] = rkey;
                    p[s+prepend+'ITEMNAME'] = r[browse.constants.productname];
                    p[s+prepend+'ITEMIMAGEURL'] = r[browse.constants.imageURL];
                    p[s+prepend+'ITEMPAGEURL'] = r[browse.constants.productURL];
                }
            }
            
            if (record['itemCategoryPath']) {
            	p[s+'ITEMCATEGORYPATH'] = record['itemCategoryPath']; 
            }
            
            return p;
        },
        
        _sendBrowseEvent : function(numproducts, products, scores, numcategories, categories) {
            try
            {
                var count = 0;
                var p;
                var e = {};
                var record;
                var s;
                for (var key in products) {
                    if (products.hasOwnProperty(key)) {
                        ++count;
                        record = products[key];
                        s = hybrisconvert.formatBasketLine(count, 3);
                        p = browse._createBasketItem(record, s, top);
                        e[s+'ITEMBROWSESCORE'] = scores[key];
                        e = common.merge(e, p);
                    }
                }

                e['Custom14'] = numcategories;
                e['Custom13'] = numproducts;
                e['FunnelLevel'] = '3';
                e['BasketAppend'] = '0';

                hybrisconvert.sendEvent(e);
            }
            catch(err){}
        },

        _getRecord : function(type, item)
        {
            var rs = {};
            var i;
            if (type == 'c') {
                i = localStorage.getItem(browse.constants.categories);
            }
            else if (type == 'p') {
                i = localStorage.getItem(browse.constants.skus);
            }
            if (i) {
                var items = JSON.parse(i);
                rs = items[item];
            }
            return rs;
        },

        _writeField : function(type, key, val)
        {
            var items;
            var lsKey;
            if (type == 'p') {
                lsKey = browse.constants.skus;
            }
            else if (type == 'c') {
                lsKey = browse.constants.categories;
            }
            var items_s = localStorage.getItem(lsKey);
            if (!items_s) {
                items = {};
            }
            else {
                items = JSON.parse(items_s);
            }
            items[key] = val;
            localStorage.setItem(lsKey, JSON.stringify(items));
        },

        // Start EDC Additions
        cyDwellStart : function() {
            try {
                common.testStorage();
                browse._cypageOpen = new Date().getTime();
                var t = localStorage.getItem("com.hybrisconvert.b.edc");
                var s = localStorage.getItem("com.hybrisconvert.b.lastsku");
                if (s) {
                    browse._productPageDwell(s, t);
                }
            }
            catch(err){}
        },

        _cyDwellTime : function() {
            browse._cypageClose = new Date().getTime();
            browse._cytime = browse._cypageClose - browse._cypageOpen;
        },

        _unload : function() {
            try {
                browse._cyDwellTime();
                browse._cysku = localStorage.getItem(browse.constants.currentsku);
                if (browse._cysku) {
                    localStorage.setItem('com.hybrisconvert.b.edc', (browse._cytime / 1000));
                    localStorage.setItem('com.hybrisconvert.b.lastsku', browse._cysku);
                }
                localStorage.removeItem(browse.constants.currentsku);
            }
            catch(err){}
        }
        // End EDC Additions
    };


    $(document).ready(function ()
    {
        if (window.ACC && ACC.addons && ACC.addons.hybrisconvertaddon && window.com_hybrisconvert_addon_global) {

        	if (typeof com_hybrisconvert_addon_global.mode !== 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
        	    s_browse.dwellStart();
        	}
        	else {
                browse.cyDwellStart();
        	}

            hybrisconvert.bindToAddToCartForm();
            hybrisconvert.bindToAddToCartStorePickUpForm();
            hybrisconvert.bindToQuickOrder();

            if (!ACC.addons.hybrisconvertaddon['cyActionEmailCaptured'] && com_hybrisconvert_addon_global.cyUseURLEmail === 'true') {
                hybrisconvert.emailCapture();
            }

            if (ACC.addons.hybrisconvertaddon['cyActionProductBrowsed']) {
            	if (typeof com_hybrisconvert_addon_global.mode !== 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
            	    hybrisconvert.m0_productBrowsed();
            	}
            	else {
                    hybrisconvert.m1_productBrowsed();
            	}
            }

            if (ACC.addons.hybrisconvertaddon['cyActionProductReviewed']) {
            	if (typeof com_hybrisconvert_addon_global.mode !== 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
                    hybrisconvert.m0_productReviewed();
            	}
            	else {
                    hybrisconvert.m1_productReviewed();
            	}
            }

            if (ACC.addons.hybrisconvertaddon['cyActionOrderPlaced']) {
                hybrisconvert.orderPlaced();
            }
            else if (ACC.addons.hybrisconvertaddon['cyActionEmailCaptured']) {
                hybrisconvert.emailLoginCapture();
            }

            hybrisconvert.bindBrowseListeners();

            // EDC
            if (typeof com_hybrisconvert_addon_global.mode !== 'undefined' && com_hybrisconvert_addon_global.mode == 0) {
                if (window.addEventListener) {
                    window.addEventListener('beforeunload', s_browse._unload, false);
                }
                else if (window.attachEvent) {
                    window.attachEvent('onbeforeunload', s_browse._unload);
                }
            }
            else {
                if (window.addEventListener) {
                    window.addEventListener('beforeunload', browse._unload, false);
                }
                else if (window.attachEvent) {
                    window.attachEvent('onbeforeunload', browse._unload);
                }
            }
        }
    });

    return {
        common: common,
        hybrisconvert: hybrisconvert,
        browse: browse,
        s_browse : s_browse,
        api : api
    };
}();