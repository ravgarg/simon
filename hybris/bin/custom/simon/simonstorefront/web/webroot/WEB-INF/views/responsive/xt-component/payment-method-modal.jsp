<div class="modal fade payment-method-modal" id="paymentMethodModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Payment Method</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="card-details">CREDIT CARD DETAILS</div>
                <div class="cards-container clearfix">
                    <div class="cards pull-left">
                        <img src="/_ui/responsive/simon-theme/icons/visa.svg" class="simon-logo" />
                        <img src="/_ui/responsive/simon-theme/icons/mastercard.svg" class="simon-logo" />
                        <img src="/_ui/responsive/simon-theme/icons/amex.svg" class="simon-logo" />
                    </div>
                    <div class="secure-info small-text pull-right hidden-xs">
                        Your information is secure <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                    </div>
                </div>
                <div class="optional-fields">*Indicates optional field</div>
                <form>
                    <div class="payment-method-fields">
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "First Name">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Last Name">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "card number">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-7 col-md-4">
                              <div class = "sm-input-group list-dropdown">
                                 <select class = "form-control major">
                                    <option value="Month">Exp. Month</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-4">
                              <div class = "sm-input-group list-dropdown">
                                 <select class = "form-control major">
                                    <option value="Year">Exp. Year</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "CVV">
                                <div class="cvv">
                                    <img height="10px" src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                </div>
                              </div>
                           </div>
                           <div class="antivirus-icon col-xs-2 col-md-2 show-desktop">
                                <img title="" alt="" src="/_ui/responsive/simon-theme/icons/mc-afee-fpo.png">
                           </div>
                        </div>
                        <div class="row show-mobile margin-btm">
                            <div class="secure-info small-text col-xs-7 col-xs-offset-1">
                                 Your information is secure <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                            </div>
                            <div class="antivirus-icon col-xs-2">
                                <img title="" alt="" src="/_ui/responsive/simon-theme/icons/mc-afee-fpo.png">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-12">
                                <div class="billing-add major">Billing Address</div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Address">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Apt / Floor / Suite*">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "City">
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-3">
                              <div class = "sm-input-group list-dropdown">
                                 <select class = "form-control major">
                                    <option value="State">State</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "ZIP Code">
                              </div>
                           </div>
                        </div>
                        <div class="clearfix">
                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm" class="custom-checkbox"/>
                              </div>
                              <span class="make-default-text">Make Default Address</span>
                           </label>
                        </div>
                        <div class="payment-method-buttons">
                            <button class="btn btn-add">ADD</button>
                            <button class="btn secondary-btn black" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>