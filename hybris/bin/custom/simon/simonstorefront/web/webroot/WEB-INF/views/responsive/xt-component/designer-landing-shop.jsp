<div class="designer-shop">
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="img-container">
                <a href="#">
                    <!--image width should not be less than 480px-->
                    <img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/dlp-image@2x.jpg" alt="">
                    <h4 class="major">Designer Dress Shop<span>&rsaquo;</span></h4>
                </a>
            </div>
            <div class="designer-text">
                <div class="major"><span class="designer">DESIGNER</span><span class="edit">EDIT:</span></div>
                <div class="designer-collection">3000+ of your favorite designers. All in one place.
                Shop this week's edit of designer collections including
                <a>Calvin Klein</a>, <a>Polo Ralph Lauren</a>, <a>Tommy Hilfiger</a>, <a>Michael Kors</a> & more.</div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="img-container">
                <a href="#">
                <!--image width should not be less than 480px-->
                <img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/dlp-image2@2x.jpg" alt="">
                <h4 class="major">Designer Dress Shop<span>&rsaquo;</span></h4>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="img-container">
                <a href="#">
                <!--image width should not be less than 480px-->
                    <img class="img-responsive img-hover-effect img-margin-top" src="/_ui/responsive/simon-theme/images/dlp-image3@2x.jpg" alt="">
                    <h4 class="major">Designer Dress Shop<span>&rsaquo;</span></h4>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="img-container">
                <a href="#">
                <!--image width should not be less than 480px-->
                <img class="img-responsive img-hover-effect img-margin img-margin-top" src="/_ui/responsive/simon-theme/images/dlp-image4@2x.jpg" alt="">
                <h4 class="major">Designer Dress Shop<span>&rsaquo;</span></h4>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="img-container">
                <a href="#">
                <!--image width should not be less than 480px-->
                <img class="img-responsive img-hover-effect img-margin-top" src="/_ui/responsive/simon-theme/images/dlp-image5@2x.jpg" alt="">
                <h4 class="major">Designer Dress Shop<span>&rsaquo;</span></h4>
                </a>
            </div>
        </div>
    </div>
</div>
