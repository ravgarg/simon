<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${not empty designer}">
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/add-designer", "remove":"/my-account/remove-designer" }' />
	<div class="col-md-4 col-xs-12 custom-links add-to-favorites">
		<div class="analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite"  data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|featured_designer" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|featured_designer" data-analytics-component="featured_designer">				
			<input type="hidden" value="${designer.code}" class="base-code analytics-base-code" /> 
			<input type="hidden" value="${designer.name}" class="base-name analytics-base-name" /> 
			<a href="javascript:void(0);" class="mark-favorite favor_icons <c:if test="${designer.favorite}">marked</c:if>"></a>
		</div>
		<a class="analytics-genericLink analytics-designerClick" data-designerval="${designer.name}" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkname="${designer.name}" href="/${designer.bannerUrl}">${designer.name}</a><br>
	</div>
</c:if>