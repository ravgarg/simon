<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}" id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}" id="totalNumberOfResults" />				
				
				<c:forEach items="${searchPageData.facets}" var="facets"
					varStatus="status">
				<c:if test="${facets.code eq 'categoryPath'}">
					<ul class="left-menu level-0 list-unstyled analytics-plpleftNav" data-analytics-eventtype="side_navigation" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|side_navigation" data-satellitetrack="link_track">
							<c:forEach var="facetValue" items="${facets.values}"> 
								<li
									<c:choose><c:when test="${facetValue.selected and fn:length(facetValue.childNode) gt 0}">class="active has-level-1"</c:when><c:when test="${facetValue.selected}">class="active"</c:when><c:otherwise>class="has-level-1"</c:otherwise>  </c:choose>>
									<a data-toggle="collapse" class="accordion-menu collapsed" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(facetValue.name))}" href="#${facetValue.code}">${fn:escapeXml(facetValue.name)}</a>
									<c:if test="${fn:length(facetValue.childNode) gt 0}">
										<ul class="level-1 collapse" id="${facetValue.code}">
											<!-- List of L3 Nodes -->
											<c:forEach var="child1" items="${facetValue.childNode }">
												<li
													<c:choose><c:when test="${child1.selected and fn:length(child1.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child1.selected}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>
													<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child1.name))}" href="<c:url value="${child1.query.url }"/>">${fn:escapeXml(child1.name)}</a>
													 <c:if
														test="${fn:length(child1.childNode) gt 0}">
														<ul class="level-2 collapse" id="${child1.code }">
															<!-- List of L4 Nodes -->
															<c:forEach var="child2" items="${child1.childNode }">
																<li
																	<c:choose><c:when test="${child2.selected and fn:length(child2.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child2.selected}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>
																	<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child2.name))}" href="<c:url value="${child2.query.url }"/>">${fn:escapeXml(child2.name) }</a>
																	<c:if test="${fn:length(child2.childNode) gt 0}">
																		<ul class="level-2 collapse" id="${child2.code }">
																			<!-- List of L5 Nodes -->
																			<c:forEach var="child3" items="${child2.childNode }">
																				<li
																					<c:choose><c:when test="${child3.selected and fn:length(child3.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child3.selected}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>
																					<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child3.name))}"href="<c:url value="${child3.query.url }"/>">${fn:escapeXml(child3.name) }</a>
																					 <c:if
																						test="${fn:length(child3.childNode) gt 0}">
																						<ul class="level-2 collapse" id="${child3.code }">
																							<c:forEach var="child4"
																								items="${child3.childNode }">
																								<li
																									<c:if test="${child4.selected}">class="active" </c:if>><a
																									href="<c:url value="${child4.query.url }"/>">${fn:escapeXml(child4.name) }</a></li>
																							</c:forEach>
																						</ul>
																					</c:if>
																				</li>
																			</c:forEach>
																		</ul>
																	</c:if>
																</li>
															</c:forEach>
														</ul>
													</c:if>
												</li>
											</c:forEach>
										</ul>
									</c:if>
								</li>
							</c:forEach>


						</ul>
						</c:if>
						</c:forEach>