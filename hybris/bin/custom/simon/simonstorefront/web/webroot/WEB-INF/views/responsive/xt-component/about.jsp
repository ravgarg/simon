<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>
<div class="static-page">
	<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="trends-landing-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="trends-landing-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-12 static-page-container">
			<h1 class="page-heading major"> About </h1>
			<jsp:include page="about-heading-desc.jsp"></jsp:include>
			
			<jsp:include page="about-hero-container.jsp"></jsp:include>
			
			<h4>Lorem Ipsum</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec elit at nibh faucibus posuere sed sed massa. Suspendisse potenti. Vivamus venenatis gravida magna, non interdum lectus cursus luctus. Maecenas lacus sapien, pretium sit amet justo et, ornare venenatis diam. Proin eget rutrum libero. Vivamus aliquet sem ex, scelerisque suscipit urna mattis vel. Pellentesque fermentum sapien diam, dictum dignissim diam maximus id. Phasellus accumsan rutrum ex a aliquam. Nunc sollicitudin gravida lacus, quis ornare orci vestibulum id. Integer placerat tempor egestas. Maecenas ultrices lacus eu enim hendrerit congue. Duis arcu ante, auctor nec dignissim eu, viverra eget purus. Morbi pulvinar enim a magna varius dignissim. Suspendisse eros leo, sagittis ac ullamcorper nec, tempor vel elit.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec elit at nibh faucibus posuere sed sed massa. Suspendisse potenti. Vivamus venenatis gravida magna, non interdum lectus cursus luctus. Maecenas lacus sapien, pretium sit amet justo et, ornare venenatis diam. Proin eget rutrum libero. Vivamus aliquet sem ex, scelerisque suscipit urna mattis vel. Pellentesque fermentum sapien diam, dictum dignissim diam maximus id. Phasellus accumsan rutrum ex a aliquam. Nunc sollicitudin gravida lacus, quis ornare orci vestibulum id. Integer placerat tempor egestas. Maecenas ultrices lacus eu enim hendrerit congue. Duis arcu ante, auctor nec dignissim eu, viverra eget purus. Morbi pulvinar enim a magna varius dignissim. Suspendisse eros leo, sagittis ac ullamcorper nec, tempor vel elit.</p>
			
			<jsp:include page="trend-shop.jsp"></jsp:include>	
			
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec elit at nibh faucibus posuere sed sed massa. Suspendisse potenti. Vivamus venenatis gravida magna, non interdum lectus cursus luctus. Maecenas lacus sapien, pretium sit amet justo et, ornare venenatis diam. Proin eget rutrum libero. Vivamus aliquet sem ex, scelerisque suscipit urna mattis vel. Pellentesque fermentum sapien diam, dictum dignissim diam maximus id. Phasellus accumsan rutrum ex a aliquam. Nunc sollicitudin gravida lacus, quis ornare orci vestibulum id. Integer placerat tempor egestas. Maecenas ultrices lacus eu enim hendrerit congue. Duis arcu ante, auctor nec dignissim eu, viverra eget purus. Morbi pulvinar enim a magna varius dignissim. Suspendisse eros leo, sagittis ac ullamcorper nec, tempor vel elit.</p>
			
			<div class="about-banner-container">
				<h2 class="major">Lorem Ipsum</h2>
				<p>Lorem ipsum dolor consecetur elicit velir au et pusanao roleum uspean al divelaor</p>
			</div>
			
			<h4>Lorem Ipsum</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec elit at nibh faucibus posuere sed sed massa. Suspendisse potenti. Vivamus venenatis gravida magna, non interdum lectus cursus luctus. Maecenas lacus sapien, pretium sit amet justo et, ornare venenatis diam. Proin eget rutrum libero. Vivamus aliquet sem ex, scelerisque suscipit urna mattis vel. Pellentesque fermentum sapien diam, dictum dignissim diam maximus id. Phasellus accumsan rutrum ex a aliquam. Nunc sollicitudin gravida lacus, quis ornare orci vestibulum id. Integer placerat tempor egestas. Maecenas ultrices lacus eu enim hendrerit congue. Duis arcu ante, auctor nec dignissim eu, viverra eget purus. Morbi pulvinar enim a magna varius dignissim. Suspendisse eros leo, sagittis ac ullamcorper nec, tempor vel elit.</p>
			
			
		</div>
	</div>
	</div>	
</div>