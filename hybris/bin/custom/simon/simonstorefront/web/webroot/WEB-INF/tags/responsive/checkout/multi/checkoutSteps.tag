<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ attribute name="checkoutSteps" required="true" type="java.util.List" %>
<%@ attribute name="progressBarId" required="true" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="checkout-breadcrumb">
	<ul>
	 <c:forEach items="${checkoutSteps}" var="checkoutStep" varStatus="status">
            <c:url value="${checkoutStep.url}" var="stepUrl"/>
             <c:choose>
                 <c:when test="${checkoutStep.progressBarId eq 'deliveryAddress'}">
                    <c:set scope="page"  var="activeCheckoutStepNumber"  value="${checkoutStep.stepNumber}"/>
                   <li class=" active"><span>Shipping</span></li>
                </c:when>
                <c:when test="${checkoutStep.progressBarId eq 'deliveryMethod'}">
                    <li class=""><span>Review Order</span></li>
                </c:when>
                <c:when test="${checkoutStep.progressBarId eq 'paymentMethod'}">
                    <li class=""><span>Payment</span></li>
                </c:when>
            </c:choose>
     </c:forEach>
	</ul>
</div>






					
					
					
