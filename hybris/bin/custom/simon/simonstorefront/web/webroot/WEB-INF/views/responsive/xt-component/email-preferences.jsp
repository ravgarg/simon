
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<jsp:include page="email-breadcurmb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="myorders-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">
				<jsp:include page="email-top-heading.jsp"></jsp:include>
				<jsp:include page="email-preferences-content.jsp"></jsp:include>
			</div>
		</div>
	</div>