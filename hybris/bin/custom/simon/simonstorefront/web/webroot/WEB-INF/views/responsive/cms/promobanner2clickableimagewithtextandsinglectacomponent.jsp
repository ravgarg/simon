<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:choose>
	<c:when test="${link.target eq 'NEWWINDOW'}">
    	<c:set var="target" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="target" value="_self" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>


<div class="designer-deals-section clearfix">
	<div class="designer-deals analytics-data" data-analytics='{
			  "event": {
				"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promoBanner",
				"sub_type": "${fn:toLowerCase(component.itemtype)}"
			  },
			  "banner": {
				"click": {
				  "id": "${fn:toLowerCase(component.uid)}"
				}
			  },
	     "link": {
	    "placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promo_click",
	    "name": "<c:choose>
					<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>"
	  }
			}'>
		<div class="">
			<div class="designer-deals-container designer-deals-info col-xs-12 col-md-7" data-satellitetrack="internal_click"  data-analytics='{"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promoBanner","name": "${fn:toLowerCase(link.linkName)}"}'>
				<h2 class="major">${titleText}</h2>
				<h6>${subheadingText}</h6>
				<c:if test="${not empty link && not empty link.linkName}" >
				<c:choose>
					<c:when test="${not empty link.url}">
					<a href="${link.url}" class="visible-xs">${link.linkName}&nbsp;<span>&rsaquo;</span></a>
					</c:when>
					<c:otherwise>
					<span class="visible-xs"> ${link.linkName}&nbsp;<span>&rsaquo;</span></span>
					</c:otherwise>
				</c:choose>
				</c:if>
		
			</div>
			<div class="designer-deals-container designer-deals-img col-xs-12 col-md-3">			
				<picture>
					<!--[if IE 9]><video class="hide"><![endif]-->
					<source media="(max-width: 991px)" srcset="${mobileImage}">
					<!--[if IE 9]></video><![endif]-->
					<img class="img-responsive" alt="${backgroundImage.imageDescription}" style="" src="${desktopImage}">
				</picture>
			</div>
			<div class="designer-deals-container designer-deals-shopnow col-xs-12 col-md-2" data-satellitetrack="internal_click"  data-analytics='{"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promoBanner","name": "${fn:toLowerCase(fn:replace(link.linkName," ","_"))}"}'>
				<c:if test="${not empty link && not empty link.linkName}" >
				<c:choose>
					<c:when test="${not empty link.url}">
					<a target=${target} href="${link.url}" class="hidden-xs">${link.linkName}&nbsp;<span>&rsaquo;</span></a>					
					</c:when>
					<c:otherwise>
					<span class="hidden-xs">${link.linkName}&nbsp;<span>&rsaquo;</span></span>
					</c:otherwise>
				
				</c:choose>
				</c:if>	
			</div>
		</div>
	</div>
</div>