<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="row">
	<div class="col-md-12">
		<input type="hidden" data-fav-url="true"
			value='{"add":"/my-account/add-designer", "remove":"/my-account/remove-designer" }' />
		<c:forEach items="${designerSet}" var="designer">
			<div class="col-md-4 col-xs-12 custom-links add-to-favorites">
				<div class="analytics-addtoFavorite add-to-favorites" data-analytics-pagetype="myaccountfavorite"  data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|designer" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|designer" data-analytics-component="designer">				
					<input type="hidden" value="${designer.code}" class="base-code analytics-base-code" /> 
					<input type="hidden" value="${designer.name}" class="base-name analytics-base-name" /> 
					<a href="javascript:void(0);" class="mark-favorite favor_icons marked"></a>
				</div>
				<a href="/${designer.bannerUrl}">${designer.name}</a><br>
			</div>
		</c:forEach>
	</div>
</div>