/**
 * @function : rlutility
 * @description : use this for global tiles carousel functionality
 */
define(['jquery','slickCarousel','viewportDetect','ajaxFactory'], function($, slickCarousel, viewportDetect,ajaxFactory){
    'use strict';
    var globalCustomComponents = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.initAjax();
            this.homepageCarousel();
        },
        initVariables: function() {},
        initAjax: function() {},
        initEvents: function() {},

        heroCarousel: function(elem, options) {
                elem.slick(options);
        },
        
        submitShareEmailMyDeal: function(e){
        	e.preventDefault();
        	if ($('#shareEmailForm').parsley().isValid()) {
            	if($('.captchaValidation').length){
                    var response = grecaptcha.getResponse();
                    if(response.length===0){
                    	$('#captcha').show(); 
                        return false;
                    }else{   
                    	$('#captcha').hide();
                    } 
                }
          	 var formData = {
                   CSRFToken :$("input[name=CSRFToken]").val(),
                   fromEmail :$('#shareViaEmail').find('#fromEmail').val(),
                   toEmail :$('#shareViaEmail').find('#toEmail').val(),
                   comments :$('#shareViaEmail').find('#emailMsg').val(),
                   productId : $("input[name=productCode]").val(),
                   dealId : $("input[name=selectedDealID]").val()
                   }
                    var options = {
                            'methodType': 'POST',
                            'dataType': 'JSON',
                            'methodData':formData,
                            'url': $("#shareEmailForm").data('emailurl'),
                            'isShowLoader': false,
                            'cache' : true
                        }
                 ajaxFactory.ajaxFactoryInit(options, function(){
                        
                       	 $('#shareViaEmail').modal('hide');
                        
                 });
           }
        	else
        		$('#shareEmailForm').submit();
        },
        socialShopCarousel: function(elem) {
            var viewPort = viewportDetect.lastClass;
            if (viewPort === 'small') {
                elem.slick({
                    arrows: false,
                    dots: false,
                    slidesToShow: 1.3,
                    slidesToScroll: 1,
                    initialSlide: 0,
                    draggable: true,
                    infinite: false,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: true
                });
            }
        },
		rightRailAnchoredReviewOrder: function(){
			var $orderSummaryRight = $('.anchoredRightRail');
			if($orderSummaryRight.length > 0){
				var topValue = 758;
				$orderSummaryRight
					.stop(false, false)
					.animate({
						top: topValue
					}, 0);
			}
		},
		rightRailAnchored: function(){
			var $orderSummaryRight = $('.anchoredRightRail');
			
			if($orderSummaryRight.length > 0){
				var originalY = $orderSummaryRight.offset().top,
					topMargin = 10;

				$(window).on('scroll', function() {
					var scrollTop = $(window).scrollTop(),
						footerTop = ($('footer').position().top),
						scrollFlagValue = 550;
					
					if( $('header').hasClass('navbar-fixed-top:visible') ){
						scrollFlagValue = 700;
					}
					var containerOffset = $orderSummaryRight.closest('.container').next('.container');
					if( containerOffset.length > 0){						
						footerTop = ($(containerOffset).position().top);
					} 
					footerTop = footerTop.toFixed();
					
					if( (scrollTop+scrollFlagValue) < footerTop ){
						
						var topValue =  scrollTop < originalY ? 0 : scrollTop - originalY + topMargin;
						if( $('header').hasClass('navbar-fixed-top') ){
							topValue = topValue + $('header').height()
						}
						$orderSummaryRight
							.stop(false, false)
							.animate({
								top: topValue
							}, 0);
					}
					
				});
			}
		}
    };
    
    return globalCustomComponents;
});