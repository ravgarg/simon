<div class="account-info">
   <div class="row">
      <div class="col-xs-12 col-md-6 mid-align">
         <p class="mid-align-content">You are currently checking out as a guest.</p>
      </div>
      <div class="col-xs-12 col-md-3 mid-align">
         <a href="#">Have an account?</a>
      </div>
      <div class="col-xs-12 col-md-3 checkout-signin">
         <button class="btn secondary-btn black block">Sign In</button>
      </div>
   </div>
</div>



<div class="checkout-shipping-container">
   <div class="panel-group" id="accordion">
       
      <div class="panel panel-default">
            <h5 class="panel-title major">
               <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1">1. Shipping Information <span class="pull-right statement hidden-xs">We don't ship outside the United States</span></a>
            </h5>
           <div id="panel1" class="panel-collapse collapse in">
               <div class="collapsed-content">
                  
                  <form>
                     <div class="shipping-fields">
                        <div class="mandatory-text">
                        	<span class="note visible-xs">We don't ship outside the United States</span>
                        </div>
                        <div class="mandatory-text small-text">
                           *Indicates optional field
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "First Name">
                              </div>
                           </div>
                           <div class="col-xs-4 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "M.I*">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "Last Name">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "Address 1">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "APT / FLOOR / SUITE*">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "City">
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-3">
                              <div class = "sm-input-group select-menu">
                                 <select class = "form-control">
                                    <option value="State">State</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "ZIP Code">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "Emal Address">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "Phone Number">
                              </div>
                           </div>
                        </div>

                        <div class="clearfix pull-left disclaimer-copy">
                           
                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm" / >
                              </div>
                              Use Shipping Address for Billing
                           </label>
                           
                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm">
                              </div>
                              Subscribe to Shop Premium Outlets newsletter. You can unsubscribe at any time.
                              
                           </label>
                           <p class="sign-up-policy">Please refer to our <a href="#">Privacy Policy</a>, <a href="#">Terms of Use</a>, or <a href="#">Contact Us</a>.</p>
                        </div>
                        <button type="button" class="btn" data-toggle="modal" data-dismiss="modal" data-target="#checkoutVerifyAddressModal">save & continue</button>

                     </div>



                     <div class="shipping-fields-edit">
                        <h6>SHIP TO <a href="#">Edit</a></h6>
                        <p class="editable-text">Lisa Simone<br />
                           18 Bull path Close<br />
                           East Hampton, NY 11937<br /><br />

                           lisa.simone@gmail.com<br />
                           (972) 602-8383
                        </p>
                     </div>


                  </form>
               </div>
           </div>
       </div>
<div style="clear:both;"></div>
       <div class="panel panel-default">
            <h5 class="panel-title major">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2">2. Review order & Shipping Method</a>
            </h5>
           <div id="panel2" class="panel-collapse collapse">
               <div class="collapsed-content">
                  
                  <div class="cart-checkout-items-order-details">
                      


                      <div class="cart-checkout-items">
                          <div class="confirm-retailer major">Last Call (2)</div>
                          <div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading hide-mobile">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Asymmetric-Peplum Midi Dress</a></div>
                                   <div class="additional">
                                     Additional 20% Off
                                   </div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="item-color">Size: 2</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-12">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$360.00</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">--</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Taxes</span>
                                         <span class="estimated-tax-price">--</span>
                                      </div>
                                      <div class="tax-calculation">
                                         <a herf="#">Tax calculated during Checkout</a>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Last Call Estimated Total</span>
                                         <span class="last-call-price">$360.00</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$80.00</span>
                                      </div>

                                      <div class="shipping-method">
                                        <div class="major small-text tag">Select shipping method for last call</div>
                                        <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Premium Shipping - $7.50</option>
                                          </select>
                                        </div>
                                        <a class="viewReturnPolicy" data-href="/contentPolicy/2023">View Shipping, Returns & Privacy Information</a>
                                      </div>
                                   </div>
                                   </div>
                                </div>
                             </div>

                          </div>
                       </div>









                      <div class="cart-checkout-items">
                          <div class="confirm-retailer major">Last Call (2)</div>
                          <div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading hide-mobile">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Asymmetric-Peplum Midi Dress</a></div>
                                   <div class="additional">
                                     Additional 20% Off
                                   </div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="item-color">Size: 2</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-12">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$360.00</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">--</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">--</span>
                                      </div>
                                      <div class="tax-calculation">
                                         <a herf="#">Tax calculated during Checkout</a>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Last Call Estimated Total</span>
                                         <span class="last-call-price">$360.00</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$80.00</span>
                                      </div>

                                      <div class="shipping-method">
                                        <div class="major small-text tag">Select shipping method for last call</div>
                                        <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Premium Shipping - $7.50</option>
                                          </select>
                                        </div>
                                        <a class="viewReturnPolicy" data-href="/contentPolicy/2023">View Shipping, Returns & Privacy Information</a>
                                      </div>
                                   </div>
                                   </div>
                                </div>
                             </div>

                          </div>
                       </div>


                          <div class="cart-checkout-allitems">
                              <div class="row">
                                <div class="col-md-12 col-xs-12">
                                     <div class="price-container-left cart-checkout-items">
                                      <div class="price-calculation-container">
                                        <div class="price-top-spacing">
                                           <span class="estimated-subtotal">Estimated Order Subtotal</span>
                                           <span class="estimated-sub-price">$360.00</span>
                                        </div>
                                        <div class="ship-top-spacing">
                                           <span class="estimated-shipping">Combined Shipping Charges</span>
                                           <span class="estimated-shipping-price">$16.00</span>
                                        </div>
                                      
                                        <div class="tax-top-spacing">
                                           <span class="estimated-tax">Estimated Taxes</span>
                                           <span class="estimated-tax-price">--</span>
                                        </div>
                                        <div class="last-call-spacing">
                                           <span class="last-call-retailer"> Estimated Order Total</span>
                                           <span class="last-call-price">$360.00</span>
                                        </div>
                                        <div class="you-save-spacing">
                                           <span class="you-save">You Saved</span>
                                           <span class="you-save-price">$80.00</span>
                                        </div>
                                        
                                     </div>
                                   </div>
                                </div>
                             </div>
                          </div>

                        <button type="button" class="btn">save &amp; continue</button>


                  </div>

               </div>
           </div>
       </div>

       <div class="panel panel-default">
            <h5 class="panel-title major">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3">3. Save Your Information 
                  <span>(Optional)</span>
                  <span class="tick"></span>
                </a>
            </h5>
           <div id="panel3" class="panel-collapse collapse">
               <div class="collapsed-content">
                  <div class="save-information-container">
                  <p class="account-created">Great! Next time you checkout it'll be quick.</p>
                  <p class="create-account">You have chosen not to create an account. Changed your mind? <a href="#">Create an Account</a></p>
                     <div class="row">
                        <div class="col-xs-12 col-md-6 pull-right">
                           <p>Simple provide a password to create a Shop
                              Premium Outlets account then you can:
                              <ul>
                                 <li>Check out faster</li>
                                 <li>Track your orders</li>
                                 <li>Receive special offers & more</li>
                              </ul>
                           </p>
                        </div>
                        <div class="col-xs-12 col-md-6 pull-left">
                           <div class="sm-input-group">
                              <input type="text" class="form-control" placeholder="PASSWORD - MINIMUM OF 6 CHARACTERS">
                           </div>
                           <div class="sm-input-group">
                              <input type="password" class="form-control" autocomplete="off" placeholder="CONFIRM PASSWORD">
                           </div>
                           <div class="saveskip-btn">
                              <button class="btn">Save & Continue</button>
                              <button class="btn secondary-btn black">Skip</button>
                           </div>
                           
                        </div>
                     </div>
                  </div>
               </div>
           </div>
       </div>

       <div class="panel panel-default">
            <h5 class="panel-title major payment">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel4">4. Payment <span class="pull-right statement hidden-xs">Credit or Debit Card <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00"></span></a>
            </h5>
           <div id="panel4" class="panel-collapse collapse">
               <div class="collapsed-content">
                    <form>
                     <div class="shipping-fields payment-container">
                        <div class="cards-container clearfix">
                        	<span class="pull-left statement visible-xs">Credit or Debit Card <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00"></span>
                            <div class="cards pull-left">
                                <img src="/_ui/responsive/simon-theme/icons/visa.svg" class="simon-logo" />
                                <img src="/_ui/responsive/simon-theme/icons/mastercard.svg" class="simon-logo" />
                                <img src="/_ui/responsive/simon-theme/icons/amex.svg" class="simon-logo" />
                            </div>
                            <div class="secure-info small-text pull-right hidden-xs">
                                Your information is secure <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                            </div>
                        </div>
                        <div class="mandatory-text small-text">
                           *Indicates optional field
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "FIRST NAME">
                              </div>
                           </div>
      
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "LAST NAME">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "CARD NUMNBER">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-6 col-md-4 list-dropdown float-none">
                              <div class = "sm-input-group select-menu">
                                 <select class = "form-control">
                                    <option value="State">EXP. MONTH</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-4 list-dropdown float-none">
                              <div class = "sm-input-group select-menu">
                                 <select class = "form-control">
                                    <option value="State">EXP. YEAR</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-4 float-none">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "CVV">
                                 <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo cvv-tooltip" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                              </div>
                           </div>
                        </div>
						<div class="clearfix visible-xs secure-section">                              
                            <div class="secure-info pull-left">
                                Your information is secure <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                            </div>
                            <div class=" pull-right">
                            	<img src="/_ui/responsive/simon-theme/icons/mc-afee-fpo.png" class="security-certification" />
                            	
                            </div>
						</div>
                        <div class="clearfix disclaimer-copy">

                        <div class="antivirus-icon">
                          <img title="" alt=""
                          src="/medias/mcafee-icon.png?context=bWFzdGVyfGltYWdlc3w0MjY0fGltYWdlL3BuZ3xpbWFnZXMvaGIxL2g1YS84Nzk2MjA0NzkzODg2LnBuZ3w1ZTcwYWY2NjE5MGZkYmE2NTc0Y2EwMzBjZGZjYzFjN2IwZTJiM2I1OWJmZmFlYTJjNGUxYWIwNmFhMGIwOWUz">
                        </div>

                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm" / >
                              </div>
                              Save Card and Make Default Payment Method
                           </label>
                           
                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm">
                              </div>
                              Use Shipping Address for Billing
                           </label>
                        </div>

                        <h6 class="major payment-subheading">Billing Address</h6>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "ADDRESS 1">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "APT / FLOOR / SUITE*">
                              </div>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "CITY">
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-3 list-dropdown">
                              <div class = "sm-input-group select-menu">
                                 <select class = "form-control">
                                    <option value="STATE">State</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-6 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control" placeholder = "ZIP CODE">
                              </div>
                           </div>
                        </div>

                          <div class="order-summary-right cart-order-summary">
                              <jsp:include page="checkout-right-order-confirmation-summary.jsp"></jsp:include>
                          </div>
                  
                     </div>

                     
                  </form>
               </div>
           </div>
       </div>

   </div>
</div>