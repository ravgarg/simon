<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="component" required="true" type="com.simon.core.model.ContentSlotClickableImageWithSingleCTAComponentModel" %>

<c:if test="${not empty component }"> 
		<c:choose>
			<c:when test="${component.link.target.code eq 'newWindow'}">
		    	<c:set var="target" value="_blank" />
			</c:when>
		    <c:otherwise>
				<c:set var="target" value="_self" />
			</c:otherwise>
		</c:choose>
		
		<c:set var="desktopImage" value="${component.backgroundImage.desktopImage.url}" />
		<c:choose>
			<c:when test="${not empty component.backgroundImage.mobileImage}">
		    	<c:set var="mobileImage" value="${component.backgroundImage.mobileImage.url}" />
			</c:when>
		    <c:otherwise>
				<c:set var="mobileImage" value="${desktopImage}" />
			</c:otherwise>
		</c:choose>
		<c:if test="${isAuthenticatedMyCenter}"> 
		<c:set var="myCenterClass" value="loggedInUser" />
		</c:if>
		<c:if test="${empty component.shape || component.shape eq 'SQUARE'}" >
		          <div class="item-tile item-responsive col-md-4 col-xs-6 analytics-data" data-satellitetrack="internal_click"  data-analytics='{"event": {"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content-spot","sub_type": "${fn:toLowerCase(component.getItemtype())}"},
								  "banner": {
									"click": {
									  "id": "${fn:toLowerCase(component.getUid())}"
									}
								  },"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content_spot","name": "<c:choose>
				<c:when test="${not empty component.getName()}">${fn:toLowerCase(fn:replace(component.getName()," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.getUid())}</c:otherwise></c:choose>"}}'>
						<a target=${target} href="${component.link.url}">
						
							<!--image width should not be less than 480px-->
							<picture>
								<source media="(max-width: 991px)" srcset="${mobileImage}"/>
								<img class="img-responsive img-hover-effect ${myCenterClass}" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}"/>
							</picture>
							<c:if test="${not empty component.link && not empty component.link.linkName}" >
								<h4 class="major">${component.link.linkName}</h4>
							</c:if>
						</a>
					</div>
		</c:if>
		
		<c:if test="${component.shape eq 'RECTANGLE'}" > 
		    <div class="item-tile item-responsive col-md-4 col-xs-6 analytics-data" data-satellitetrack="internal_click"  data-analytics='{"event": {"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content-spot","sub_type": "${fn:toLowerCase(component.getItemtype())}"},
								  "banner": {
									"click": {
									  "id": "{fn:toLowerCase(component.id)}"
									}
								  },"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content_spot","name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.getUid())}</c:otherwise></c:choose>"}}'>				
				<a target=${target} href="${component.link.url}">
					<picture>
						<source media="(max-width: 991px)" srcset="${mobileImage}"/>
						<img class="img-responsive img-hover-effect" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}">
					</picture>	
					<c:if test="${not empty component.link && not empty component.link.linkName}" >
						<h4 class="major">${component.link.linkName}</h4>
					</c:if>
				</a>
			</div>	
		</c:if>	
</c:if>
	