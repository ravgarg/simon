<div class="modal fade checkout-shipping-return-modal" id="shippingReturnInformation" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Shipping, Returns & Privacy Info</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
            
            		
				<div class="shipping-return-container clearfix">
					
					<h6 class="subheading">(Retailer Name) Shipping</h6>
					<p>Your order will be shipped to you directly by (Retailer Name)</p>
					
					<div class="shipping-data">
						<div class="row">
							<div class="col-xs-8">
								<h6>No Rush Shipping</h6>
								<p class="small-text shipping-time">(3-5 Business Days)</p>
							</div>
							<!-- <div class="col-xs-4"><h6 class="pull-right plum-text">$3</h6></div> -->
						</div>

						<div class="row">
							<div class="col-xs-8">
								<h6>Premium Shipping</h6>
								<p class="small-text shipping-time">(1-2 Business Days)</p>
							</div>
<!-- 							<div class="col-xs-4"><h6 class="pull-right plum-text">$3</h6></div>
 -->						</div>
					</div>
					
					<p class="small-text">Shipping prices available during checkout</p>

					<hr />

					<h6 class="subheading">(Retailer Name) Return Policy</h6>
					<p>Your order will be able to return item directly to (Retailer Name).</p>

					<div class="policy-info">
						<p>Return Period: 30 Days</p>
						<p>Cose: $9.50</p>
						<p>Method: UPS or FedEx</p>	
					</div>

					<hr />

					<h6 class="subheading">Privacy Policy</h6>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p><p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>

                    <div class="shipping-return-btn">
                      <button type="button" class="btn" data-dismiss="modal">Close</button>
                    </div>

                </div>

            
                
            </div>
        </div>
    </div>
</div>