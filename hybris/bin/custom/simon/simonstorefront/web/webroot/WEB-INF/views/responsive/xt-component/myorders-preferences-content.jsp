
<div class="myorder-details-container row">
	<div class="col-md-12">
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
		<div class="myorder-details">
			<div class="row hidden-xs order-detail-col">
				<div class="col-md-3 col-sm-12 date-bold">Order Date</div>
				<div class="col-md-5 col-sm-12 date-bold">Retailers</div>
				<div class="col-md-2 col-sm-12 date-bold">Total Price</div>
				<div class="col-md-2 col-sm-12 date-bold">Order Details</div>
			</div>
			<div class="row order-detail-row">
				<div class="col-md-3 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Date</div>
					<div class="date-detail">March 8, 2016</div>
				</div>
				<div class="col-md-5 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Retailers</div>
					<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Total Price</div>
					<div class="price">$514.39</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Details</div>
					<div class="underline"><a href="#myordersModal" data-toggle="modal" data-target="#myordersModal">4DSF45FSN56</a></div>
					<div class="status shipped">Shipped</div>
				</div>
			</div>
			<div class="row order-detail-row">
				<div class="col-md-3 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Date</div>
					<div class="date-detail">February 12, 2016</div>
				</div>
				<div class="col-md-5 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Retailers</div>
					<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Total Price</div>
					<div class="price">$212.45</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Details</div>
					<div class="underline">3FWB52ABR41</div>
					<div class="status processing">Processing</div>
				</div>
			</div>
			<div class="row order-detail-row">
				<div class="col-md-3 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Date</div>
					<div class="date-detail">December 18, 2015</div>
				</div>
				<div class="col-md-5 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Retailers</div>
					<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Total Price</div>
					<div class="price">$181.25</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Details</div>
					<div class="underline">8HBS14RJM12</div>
					<div class="status cancel">Cancelled</div>
				</div>
			</div>
			<div class="row order-detail-row">
				<div class="col-md-3 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Date</div>
					<div class="date-detail">December 13, 2015</div>
				</div>
				<div class="col-md-5 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Retailers</div>
					<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Total Price</div>
					<div class="price">$247.83</div>
				</div>
				<div class="col-md-2 col-sm-12 order-detail-com">
					<div class="visible-xs date-bold">Order Details</div>
					<div class="underline">7YKL17CYW23</div>
					<div class="status processing">Processing</div>
				</div>
			</div>
		</div>
		<div class="row">
	       	<div class="submit-btn col-md-12 col-xs-12">
	       		<button class="btn">Start Shopping</button>
	       	</div>
	    </div>
    </div>
</div>
<jsp:include page="order-details.jsp"></jsp:include> 