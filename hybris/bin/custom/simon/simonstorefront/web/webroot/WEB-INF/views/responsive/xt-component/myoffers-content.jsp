<div class="myoffers-container">
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.</p>
	<span class="add-deals visible-xs">How to Add Deals</span>
	<div class="deal-area-container">
		<!-- no-deals area -->
		<div class="deal-area">
			<p class="deal-details">Lisa, you have no deals saved</p>
			<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip.</p>
			<button class="btn">View Deals</button>
		</div>
		<!-- all-deals area -->
		<div class="deal-details-container row">
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="padding-bottom: 20px;">
		<div class="col-md-6 col-xs-12">
			<button class="btn" data-toggle="modal" data-target="#moadl1Content">Modal 1</button>
		</div>
		<div class="col-md-6 col-xs-12">
			<button class="btn" data-toggle="modal" data-target="#moadl2Content">Modal 2</button>
		</div>
	</div>
	<!-- Modal 1 -->
	<div class="modal fade" id="moadl1Content" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title">Offer Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="barcode">
						<img src="/_ui/responsive/simon-theme/images/barcode.png" />
						<button class="btn">Back to offer details</button>
						<button class="btn secondary-btn black">Print</button>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<!-- Modal 2 -->
	<div class="modal fade" id="moadl2Content" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title">Offer Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="modal-container">
						<div class="offer-full-details">
							<a href="#">
								<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
							</a>
							<p class="free-shipping">Up to 75% Off Last Minute Gifts + Free Shipping</p>
							<p>Valid Oct. 7-10</p>
						</div>
						<div class="offer-details-container">
							May not be combined with Student, Military or Corporate Membership discounts. Discount may not be applied toward taxes, shipping and handling, monogramming or engraving, alterations or personalization. Not valid on charitable items or sample sale merchandise. Limit one per customer per store per day. If you return some or all merchandise, the dollar value of this promotion is not refunded or credited back to your account. Void where prohibited by law. No cash value except where prohibited, then the cash value is 1/100 cent.
						</div>
						<button class="btn">Shop now</button>
						<button class="btn secondary-btn black">Use In-store</button>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>