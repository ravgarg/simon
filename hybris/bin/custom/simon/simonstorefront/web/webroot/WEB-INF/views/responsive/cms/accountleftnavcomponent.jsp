<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:set value="${component.styleClass}" var="navigationClass" />

<aside class="col-md-3 col-xs-12 left-menu-container">
	<div class="account-section-header">
		<ul class="left-menu level-0 list-unstyled analytics-plpleftNav" data-analytics-eventtype="side_navigation" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|side_navigation" data-satellitetrack="link_track">
			<c:forEach items="${component.navigationNode.children}" var="topLevelChild">
				<c:choose>
					<c:when test="${not empty topLevelChild.children }">
						<li class="has-level-1"><a data-analytics-leftlinkname="${topLevelChild.title}" href="#${topLevelChild.uid}" class="accordion-menu collapsed" data-toggle="collapse">${topLevelChild.title}</a>
							<ul class="level-1 collapse" id="${topLevelChild.uid}">
								<c:forEach items="${topLevelChild.children}" var="nestedChild">	
								<c:set value="" var="page" />
									<c:choose>
										<c:when test='${fn:contains(nestedChild.entries.get(0).item.url, "/my-account/")}'>
											  <c:set value='${fn:substringAfter(nestedChild.entries.get(0).item.url, "/my-account/")}' var="page" />
										</c:when>
										<c:otherwise>
											
											<c:if test='${fn:contains(nestedChild.entries.get(0).item.url, "/customer-service/")}'>
											    <c:set value='${fn:substringAfter(nestedChild.entries.get(0).item.url, "/customer-service/")}' var="page" />
											     <c:set value='${cmsPage.uid}' var="pageId" />
											</c:if>	
											
										</c:otherwise>
									</c:choose>				
									<c:choose>
									
										<c:when test="${pageId == page}">
											<li class="active has-level-2"><cms:component component="${nestedChild.entries.get(0).item}" /></li>
										</c:when>
										<c:otherwise>
											<li class="has-level-2"><cms:component component="${nestedChild.entries.get(0).item}" /></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul></li>
					</c:when>
					<c:otherwise>
						<c:forEach items="${topLevelChild.entries}" var="entry">
						 <c:set value='' var="page" />
							      <c:choose>
										<c:when test='${fn:contains(entry.item.url, "/my-account/")}'>
											  <c:set value='${fn:substringAfter(entry.item.url, "/my-account/")}' var="page" />
										</c:when>
										<c:otherwise>
											
											<c:if test='${fn:contains(entry.item.url, "/customer-service/")}'>
											    <c:set value='${fn:substringAfter(entry.item.url, "/customer-service/")}' var="page" />
											    <c:set value='${entry.item.linkName}' var="pageId" />
											</c:if>		
										</c:otherwise>
									</c:choose>
							<c:choose>
								<c:when test="${pageId == page}">
									<li class="active"><cms:component component="${entry.item}" evaluateRestriction="true" /></li>
								</c:when>
								<c:otherwise>
									<li><cms:component component="${entry.item}" evaluateRestriction="true" /></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>
</aside>