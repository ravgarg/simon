<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" data-fav-url="true"
	value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />
	 
	<div class="store col-md-4">
	    <div class="box-background">
	         <picture>
	            <source media="(max-width: 991px)" srcset="${featuredStoreData.mobileBackgroundImage}"/>
	            <img 
	                class="img-responsive" 
	                alt="${featuredStoreData.altTextBGImage}" 
	                src='${featuredStoreData.backgroundImage}'/>
	        </picture> 
	    </div>
	    <div class="fav-the-store add-to-favorites analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="retailer" data-analytics-linkplacement="retailer" data-analytics-component="featured_stores">
		    <input type="hidden" value="${featuredStoreData.id}" class="base-code analytics-base-code" /> 
			<input type="hidden" value="${featuredStoreData.name}" class="base-name analytics-base-name" />
	        <a href="javascript:void(0);" class="mark-favorite fav-icon <c:if test="${featuredStoreData.favorite eq 'true'}">marked</c:if>"></a> 
	    </div>
	    <div class="center-content">
	        <picture>
	            <source media="(max-width: 991px)" srcset="${featuredStoreData.mobileLogo}"/>
	            <img 
	                class="img-responsive" 
	                alt="${featuredStoreData.altTextLogo}" 
	                src="${featuredStoreData.logo}"/>
	        </picture> 
	        
	    </div>
	    <div class="shop-now">
	        <a class="analytics-genericLink analytics-storeClick" data-storeval="${featuredStoreData.name}" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="shop-now" data-analytics-linkname="shop-now" href="${featuredStoreData.contentPageLabel}">${featuredStoreData.name}</a>
	    </div>
	</div>
