<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>

<ul class="nav navbar-nav myaccount-dropdown navbar-right">
    <li class="dropdown">
    <sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
    	<c:set var="favClass" value="fav-icon marked"></c:set>
    </sec:authorize>
    <c:set var="myPremiumOutlets"> <spring:theme code="text.account.premiumoutlets.link.text"/></c:set>
        <a href="#" data-toggle="dropdown" class="${favClass} favor_icons dropdown-toggle"><spring:theme code="header.link.favorites"/></a>
        <ul class="dropdown-menu analytics-favoriteNavigation" data-analytics='{ "event": { "type": "favor_navigation","sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" }, "link": {"placement": "header_favor|favor_navigation" }}' data-satellitetrack="link_track">
        
            <c:forEach items="${feature.links}" var="subMenu">        
                        <c:url value="${subMenu.url}" var="linkUrl" />
	                    <li data-analytics-linkname="${subMenu.name}">
	                    	<c:choose>
  								<c:when test="${isLoggedIn}">				  								
	  								<a href="${linkUrl}" data-url="${linkUrl}" >${subMenu.name}</a>		
	  							</c:when>
	  							<c:otherwise>
	  							<c:choose>
	  								<c:when test="${linkUrl eq myPremiumOutlets}">				  								
	  								<a href="${linkUrl}" data-url="${linkUrl}" >${subMenu.name}</a>
	  							</c:when>
	  							<c:otherwise>
	  								<a href="javascript:void(0)" data-url="${linkUrl}" class="openLoginModalInNavigation">${subMenu.name}</a>
	  								</c:otherwise>
	  							</c:choose>	
								</c:otherwise>
							</c:choose>
	                    </li>
           </c:forEach>
        </ul>
    </li>
</ul>