<div class="designers-pagination-wrapper">
	<nav> 
		<ul class="pagination"> 
			<li class="active"><a href="#all">#</a></li> 
			<li><a href="#AA" data-pag-target="AA">A</a></li> 
			<li><a href="#BB" data-pag-target="BB">B </a></li> 
			<li><a href="#CC" data-pag-target="CC">C</a></li> 
			<li><a href="#DD" data-pag-target="DD">D</a></li> 
			<li><a href="#EE" data-pag-target="EE">E</a></li>
			<li><a href="#FF" data-pag-target="FF">F</a></li>
			<li><a href="#GG" data-pag-target="GG`">G</a></li>
			<li><a href="#HH" data-pag-target="HH">H</a></li>
			<li><a href="#II" data-pag-target="II">I</a></li>
			<li><a href="#JJ" data-pag-target="JJ">J</a></li>
			<li><a href="#KK" data-pag-target="KK">K</a></li>
			<li><a href="#LL" data-pag-target="LL">L</a></li>
			<li><a href="#MM" data-pag-target="MM">M</a></li>
			<li><a href="#NN" data-pag-target="NN">N</a></li>
			<li><a href="#OO" data-pag-target="OO">O</a></li>
			<li><a href="#PP" data-pag-target="PP">P</a></li>
			<li><a href="#QQ" data-pag-target="QQ">Q</a></li>
			<li><a href="#RR" data-pag-target="RR">R</a></li>
			<li><a href="#SS" data-pag-target="SS">S</a></li>
			<li><a href="#TT" data-pag-target="TT">T</a></li>
			<li><a href="#UU" data-pag-target="UU">U</a></li>
			<li><a href="#VV" data-pag-target="VV">V</a></li>
			<li><a href="#WW" data-pag-target="WW">W</a></li>
			<li><a href="#XX" data-pag-target="XX">X</a></li>
			<li><a href="#YY" data-pag-target="YY">Y</a></li>
			<li><a href="#ZZ" data-pag-target="ZZ">Z</a></li>
		</ul> 
	</nav>
	<div class="designer-content-wrapper">
		<div class="designer-container">
			<div class="custom-heading-links-component">	
				<h3 id="all" class="major">#</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
			<div class="custom-heading-links-component">		
				<h3 id="AA" class="major">A</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
			
			<div class="custom-heading-links-component">		
				<h3 id="BB" class="major">B</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
	
			<div class="custom-heading-links-component">		
				<h3 id="CC" class="major">C</h3>
				<div class="row">
					<div class="col-md-4 custom-links">
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a><br>
						<a href="#" class="favourite-icon">Designer Name</a>
					</div>
				</div>	
			</div>
			
	
			<div class="custom-heading-links-component">		
				<h3 id="DD" class="major">D</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>		
			</div>
			
	
			<div class="custom-heading-links-component">		
				<h3 id="EE" class="major">E</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
				
		
			<div class="custom-heading-links-component">		
				<h3 id="FF" class="major">F</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
		
			<div class="custom-heading-links-component">		
				<h3 id="GG" class="major">G</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
	
			<div class="custom-heading-links-component">		
				<h3 id="HH" class="major">H</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			
	
			<div class="custom-heading-links-component">		
				<h3 id="II" class="major">I</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			

			<div class="custom-heading-links-component">		
				<h3 class="major" id="JJ">J</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			

			<div class="custom-heading-links-component">		
				<h3 id="YY" class="major">Y</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
			

			<div class="custom-heading-links-component">		
				<h3 id="ZZ" class="major">Z</h3>
				<div class="row add-to-favorites">
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
					<div class="col-md-4 custom-links">
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a><br>
						<a href="javascript:void(0);" class="mark-favorite favor_icons"></a>
						<a href="#">Designer Name</a>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>