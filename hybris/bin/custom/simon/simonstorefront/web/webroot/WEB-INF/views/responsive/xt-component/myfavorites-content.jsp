<div class="myfavorites-container">
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.</p>
	<a href="#" class="add-deals visible-xs" data-toggle="modal" data-target="#moadl1Content">How to Add Favorites</a>
	<div class="deal-area-container">
		<!-- no-favorites area -->
		<div class="deal-area">
			<p class="deal-details">Lisa, you have no favorites saved</p>
			<p>My Favorites are a great way to keep track of your most-loved items and share with friends. Just click the heart next to your favorite items to create your own personalized mall and receive special offers.</p>
			<button class="btn">Shop Trends</button>
			<button class="btn">Shop Designers</button>
			<button class="btn">Shop Deals</button>
		</div>
	</div>
	<!-- favorites area -->
	<div class="deal-area-container">
		<jsp:include page="plp-tiles-container.jsp"></jsp:include>
	</div>
	<jsp:include page="mytrendings.jsp"></jsp:include>
	<!-- Modal 1 -->
	<div class="modal fade" id="moadl1Content" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title">How to Add Favorites</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<div class="add-favorites-container">
						<div class="add-favorites">
							<p>Personalize your shopping experiences by saving your favorite items, designers and stores to your Favorites list.You can view all your favorites by clicking My Favorites at the top of the site page. Simply create an account and let the <span class="fav-icon"></span>ing begin.</p> 
							<p class="sign-in">Already have an account? <a href="#">Sign In</a></p>
						</div>
						<div class="panel panel-default">
							<h5 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#panel1">My Favorite Items</a>
							</h5>
							<div class="panel-collapse collapse" id="panel1">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<ul>
											<li>Add items as easily as you shop by simply clicking the <span class="fav-icon"></span>in the top corner of your desired product.</li>
											<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
										</ul>
										<a href="#" class="add-favorite-link">
											Start Adding Your Favorite Items Now >
										</a>
									</div>
									<div class="col-md-4 col-xs-12">
										<img src="/_ui/responsive/simon-theme/images/illustration.png" />									
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<h5 class="panel-title">
								<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#panel2">My Designers</a>
							</h5>
							<div class="panel-collapse collapse" id="panel2">
								<div class="row">
									<div class="col-md-8 col-xs-12">
										<ul>
											<li>Add items as easily as you shop by simply clicking the <span class="fav-icon"></span>in the top corner of your desired product.</li>
											<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</li>
										</ul>
										<a href="#" class="add-favorite-link">
											Start Adding Your Favorite Items Now >
										</a>
									</div>
									<div class="col-md-4 col-xs-12">
										<img src="/_ui/responsive/simon-theme/images/illustration.png" />									
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>