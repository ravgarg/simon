
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<li class="simon-header-search">
							<span class="search-icon"></span>
							<span class="menu-close search-close-icon"></span>
<form name="search_form_${component.uid}" class="analytics-headerSearch" method="get"
		action="/search" data-parsley-validate="validate">
	<input required type="text" name="text" title="search" id="searchInput" autocomplete="off" placeholder="<spring:theme code='search.box.component.placeholder' />" >
</form>
</li>
