<div class="container">
	<div class="row">
		<div class="col-md-12">
			<jsp:include page="myoffers-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="myoffers-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<jsp:include page="myoffers-top-heading.jsp"></jsp:include>
			<jsp:include page="myoffers-content.jsp"></jsp:include>
		</div>
	</div>
</div>