define(['hbshelpers', 'handlebars', 'ajaxFactory',
  'templates/plpGlobalProductTile.tpl','analytics'],
  function (hbshelpers, handlebars, ajaxFactory, gloabalLoadmoreTemplate, analytics) {
    'use strict';
    var isBusy = false,
        firstPage = $('#currentPage').val(),
        currentPage = Number(firstPage),
        numberOfPages = $('#numberOfPages').val(),
        $listingId = $('#listingId').val(),
        cache = {
          $document: $(document),
          $loadmore: $('.load-more-btn-wrapper')
        },
      loadMore = {
        init: function () {
          this.initEvents();
        },
        initEvents: function () {
          this.updatePageonScroll();
          cache.$loadmore.on('click', '#loadMore', loadMore.getAjaxpageination);  
        },
        getAjaxpageination: function () {
          var nextPage = currentPage+1;
          if(nextPage >= 3){
            $('#loadMore').show();
            $(window).off('scroll.loadmore');

            $('#loadMore').hover(function(){
            	$(this).css('background','black');
            })
          }


          var url = '',
              plpurl = window.location.pathname,
              urlpath = plpurl.substr(1, url.lastIndexOf('/')),
              plpurlsplit = plpurl.split('_'),
              finalurl = plpurlsplit[plpurlsplit.length - 1];
          if ((numberOfPages && (nextPage >= numberOfPages)) || isBusy){
            return false;
          }

          isBusy = true;
          if($('body').hasClass('page-searchListPage')){
            // Search page
            var searchQuery = window.location.search;
            if (searchQuery.indexOf('?text=') < 0) {
              url = window.location.pathname + '/results' + searchQuery + '&page=' + nextPage + '&listingId=' + $listingId;
            } else {
              var searchText = $('#searchText').val();
              url = window.location.pathname + '/results?q=' + searchText + '&page=' + nextPage;
              }
          }else{
            // PLP page
            if (!Boolean(searchText)) {
              var query = window.location.search;
              if (!Boolean(query)) {
                      query = '?q=%3Arelevance';
              }
              url = urlpath + finalurl + '/results' + query + '&page=' + nextPage + '&listingId=' + $listingId;
            }
          }
          var options = {
            'methodType': 'GET',
            'dataType': 'JSON',
            'url': url,
            'isRequireLoader': false,
            'hasLoadMore': true,
            'cache': false,
            success: function () {
              $('#loadMore').show();
            }
          }
          $("#loadMore").hide();
          ajaxFactory.ajaxFactoryInit(options, function (response) {
            $('#loadMore').show();
            if(!numberOfPages){
              numberOfPages = response.pagination.numberOfPages;
              }

            if (response.results === "") {
              $("#loadMore").hide();
            }
            
            
            if (response.pagination.numberOfPages !== 1){
			  var analyticsProductCount = $('.product-container.product-list .analytics-item').length;
              $('.product-container').append(gloabalLoadmoreTemplate(response));
              $('.tooltip-init').tooltip();
			  if($('#analyticsSatelliteFlag').val()==='true'){
				analytics.analyticsOnPageLoadMoreClick(analyticsProductCount);
			  }
              currentPage = response.pagination.currentPage;
              if (response.pagination.currentPage === 3) {
                $("#loadMore").show();
              }
              $(document).trigger('initPLPColorSwatchCarousel');
              
            }    
            if (response.pagination.numberOfPages === response.pagination.currentPage +1) {
              $("#loadMore").addClass('hidden');
            }
            isBusy = false;
          });

          return false;
        },
        updatePageonScroll: function () {
          var loading = false,
              scrollHeight = $("footer").height();

          $(window)
            .off('scroll.loadmore')
            .on('scroll.loadmore', function () {
              if (!loading && $('.trends-landing-page').length<1 && ($(window).scrollTop() - ($(document).height() - $(window).height()) <= 5) - scrollHeight && ($(window).scrollTop() - ($(document).height() - $(window).height()) >= -5) - scrollHeight) {
                loadMore.getAjaxpageination();
              }
             });
        }
      };
    $(function () {
      loadMore.init();
    });
    return loadMore;
  });