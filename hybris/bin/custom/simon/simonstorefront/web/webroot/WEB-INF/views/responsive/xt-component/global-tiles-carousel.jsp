      <div class="global-tile-carousel">
        <div class="row">
            <div class="col-md-12">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active major">
                        <a href="#youMayLike" role="tab" data-toggle="tab">You may also like</a>
                    </li>

                    <li role="presentation" class="major">
                        <a href="#moreFromRetailer" role="tab" data-toggle="tab">More from this Retailer</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="youMayLike">
                        <div class="row">
                            <div id="globalTilesCarousel">
                                <div class="item col-md-3">
                                    <img src="/_ui/responsive/simon-theme/icons/black-favorites-light.svg" class="favorite-right simon-logo">
                                    <a href="#">
                                        <picture>
                                             <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
                                            <source media="(max-width: 767px)"  srcset="/_ui/responsive/simon-theme/images/bags.jpg">
                                            <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/bags.jpg">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img class="img-responsive" alt="" style="" src="/_ui/responsive/simon-theme/images/bags.jpg">
                                        </picture>
                                    </a>
                                    <div class="item-name"><a href="">Last Call</a></div>
                                    <div class="item-discount"><a href="">Longchamp</a></div>
                                    <div class="item-retailer"><a href="">1 Color</a></div>
                                    <div class="item-color"><a href="">'Large Le Pliage' Tote</a></div>
                                    <div class="item-discrition">$200.00</div>
                                    <div class="item-price">$160.00<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "bottom" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00"></div>
                                    <div class="item-revised-price">20% Off</div>
                                </div>
                                <div class="item col-md-3">
                                    <img src="/_ui/responsive/simon-theme/icons/black-favorites-light.svg" class="favorite-right simon-logo">
                                    <a href="#">
                                        <picture>
                                            <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
                                            <source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/neckless.jpg">
                                            <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/neckless.jpg">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img class="img-responsive" alt="" style="" src="/_ui/responsive/simon-theme/images/neckless.jpg">
                                        </picture>
                                    </a>
                                    <div class="item-name"><a href="">Nordstrom Rack</a></div>
                                    <div class="item-discount"><a href="">Rebecca Minkoff</a></div>
                                    <div class="item-retailer"><a href="">1 Color</a></div>
                                    <div class="item-color"><a href="">Crystal Detail Fan Pendant Necklace</a></div>
                                    <div class="item-discrition">$57.94</div>
                                    <div class="item-price">$28.97<img src="/_ui/responsive/simon-theme/icons/black-question.svg" class="simon-logo"></div>
                                    <div class="item-revised-price">50% Off</div>

                                </div>
                                <div class="item col-md-3">
                                    <img src="/_ui/responsive/simon-theme/icons/black-favorites-light.svg" class="favorite-right simon-logo">
                                    <a href="#">
                                        <picture>
                                             <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
                                            <source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/dress.jpg">
                                            <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/dress.jpg">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img class="img-responsive" alt="" style="" src="/_ui/responsive/simon-theme/images/dress.jpg">
                                        </picture>
                                    </a>
                                    <div class="item-name"><a href="">Last Call</a></div>
                                    <div class="item-discount"><a href="">Halston Heritage</a></div>
                                    <div class="item-retailer"><a href="">1 Color</a></div>
                                    <div class="item-color"><a href="">Strapless Structured Dress</a></div>
                                    <div class="item-discrition">$233.00</div>
                                    <div class="item-price">$116.50<img src="/_ui/responsive/simon-theme/icons/black-question.svg" class="simon-logo"></div>
                                    <div class="item-revised-price">50% Off</div>
                                </div>
                                <div class="item col-md-3">
                                  <img src="/_ui/responsive/simon-theme/icons/black-favorites-light.svg" class="favorite-right simon-logo">
                                  <a href="#">
                                    <picture>
                                         <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
                                        <source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/scarf.jpg">
                                        <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/scarf.jpg">
                                        <!--[if IE 9]></video><![endif]-->
                                        <img class="img-responsive" alt="" style="" src="/_ui/responsive/simon-theme/images/scarf.jpg">
                                    </picture>
                                  </a>
                                    <div class="item-name"><a href="">Gap Factory</a></div>
                                    <div class="item-discount"><a href=""></a></div>
                                    <div class="item-retailer"><a href="">2 Colors</a></div>
                                    <div class="item-color"><a href="">Print Fringe Scarf</a></div>
                                    <div class="item-discrition">$19.86</div>
                                    <div class="item-price">$13.90
                                     <img src="/_ui/responsive/simon-theme/icons/black-question.svg" class="simon-logo">
                                    </div>
                                    <div class="item-revised-price">30% Off</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="moreFromRetailer">
                    </div>
                </div>
            </div>
        </div>
      </div>
