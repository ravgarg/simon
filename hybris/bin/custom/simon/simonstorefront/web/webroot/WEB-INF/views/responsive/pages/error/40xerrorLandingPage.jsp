<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 

<template:page pageTitle="${pageTitle}">
<c:choose><c:when test="${not empty message && not empty name}">
<c:set var="message" value="${message}"/>
<c:set var="name" value="${name}"/>
</c:when><c:otherwise>
<c:set var="message" value="Something went wrong"/>
<c:set var="name" value="Exception"/>
</c:otherwise></c:choose> 
	<div class="error500 analytics-errorPage" data-analytics-pagetype="500error" data-analytics-subtype="500error" data-analytics-contenttype="informational" data-analytics-errortype="error" data-analytics-errorsubtype="500error" data-analytics-errormsg="${message}" data-analytics-errorname="${name}">
	<header>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="sitelogo">
							<cms:pageSlot position="CSN_40xSitesLogoSection" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<div class="col-md-8 hide-mobile">
				
					 <ul class="links major">
					 	<cms:pageSlot position="CSN_40xErrorPageExternalLinkSection" var="feature">
							<li><cms:component component="${feature}"/></li>
						</cms:pageSlot>
					</ul> 
				</div>
			</div>
		</div>
	</header>

	<div class="error500-container">
		<picture>
			<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/image500@2x.jpg">
			<img class="img-responsive" src="/_ui/responsive/simon-theme/images/image500.jpg" alt="<spring:theme code='system.error.internal.server.error'/>" />
		</picture>
		<div class="container error500-content">
			<div class="row">
				<div class="col-xs-6 col-md-5">
				<cms:pageSlot position="CSN_40xErrorMainMessageSection" var="feature">
						<cms:component component="${feature}" element="div" class="span-24 section1 cms_disp-img_slot"/>
				</cms:pageSlot>
				<cms:pageSlot position="CSN_40xErrorSubMessageSection" var="feature">
					<cms:component component="${feature}" element="div" class="span-24 section1 cms_disp-img_slot"/>
				</cms:pageSlot>
					<!-- <h1 class="display-xl major">We're <span>sorry</span></h1>
					<h4>Our website is currenty unavailable due to scheduled maintenance. We regret any inconvenience.</h4>
					<h4>Please check back soon.</h4> -->
				</div>
			</div>
		</div>

		<div class="analytics-footer"><div class="container">
			<div class="row error500-footer-link clearfix">
	            <div class="col-md-5 col-xs-12 footer-social">
	            <section class="clearfix">
	           	 	<cms:pageSlot position="CSN_40xSocialSection" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					</section>
	                <div class="footer-terms">
	                <cms:pageSlot position="CSN_40xErrorpageCopyRightMessageSection" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
		             
		            </div>
	            </div>
	            
         	</div>
		</div>
	</div>
	</div>
</div>
</template:page>