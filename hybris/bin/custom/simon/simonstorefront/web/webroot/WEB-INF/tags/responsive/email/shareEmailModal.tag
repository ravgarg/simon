<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="modal fade" id="shareViaEmail" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					<spring:theme code="text.share.email.modal.title" />
				</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close"></button>
			</div>
			<div class="modal-body share-via-email">
				<c:url value="/share-email/send-email" var="actionUrl"/>
				<form:form action="${actionUrl}" data-emailurl="${actionUrl}" id="shareEmailForm"
					class="sm-form-validation bt-flabels js-flabels captchaValidation"
					data-parsley-validate="validate">
					<input type="hidden" name="dealId" id="dealId"/>
					<p class="sub-title">
						<spring:theme code="text.share.email.modal.subtitle" />
					</p>
					<p>
						<spring:theme code="text.share.email.modal.subtitle.content" />
					</p>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="sm-input-group">
								<label class="field-label" for="fromEmail"><spring:theme
										code="text.share.email.modal.your.email" /><span
									class="field-error" id="error-shareEmail">
								</span>
								</label> 
								<input type="email" class="form-control" placeholder="Your Email"
									name="fromEmail" id="fromEmail"
									data-parsley-errors-container="#error-shareEmail"
									data-parsley-error-message="<spring:theme code="text.share.email.modal.your.email.error.span.msg" />"
									required>
							</div>
						</div>
						<div class="col-xs-12 col-md-12">
							<div class="sm-input-group toEmail">
								<label class="field-label" for="toEmail"><spring:theme
										code="text.share.email.modal.to.email" />
									<span class="field-error" id="error-toemail"></span></label>
									 <input type="text"
									class="form-control" placeholder="To Email" name="toEmail"
									id="toEmail"
									data-parsley-errors-container="#error-toemail"
									data-parsley-required
									data-parsley-emailstrength-message="Only 5 emails allowed"
									data-parsley-emailstrength
									data-parsley-required-message="<spring:theme code="text.share.email.modal.your.email.error.span.msg" />"
									required>
							</div>
							<p class="email-info">Enter up to 5 addresses, separate with a comma.</p>
						</div>
						<div class="col-xs-12 col-md-12">
							<div class="sm-input-group message-row">
								<label for="emailMsg"><spring:theme
										code="text.share.email.modal.message.label" /><span
									class="field-error" id=""></span></label>
								<textarea class="form-control txtarealowercase" id="emailMsg" rows="5"></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-md-12 captchaContainer">
							<div class="sm-input-group">
								<div class="g-recaptcha" id="rcaptcha"
									data-sitekey="${captchaKey}"></div>
								<div id="captcha" class="has-error">
									<spring:theme code="login.customer.captcha.error.msg" />
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-12">
							<div class="sm-input-group">
							<label class="custom-checkbox" for="termAndPolicy">
							<input type="checkbox" data-parsley-required="true" data-parsley-trigger="click" 
							class="sr-only" id="termAndPolicy"/><em
									class="i-checkbox"></em> 
										<spring:theme code="text.share.email.modal.iagree.msg" />
										 <a href="<spring:theme code="text.share.email.modal.termofuse.href" />" target="_new" class="link"><spring:theme code="text.share.email.modal.termofuse.msg" /></a> <spring:theme code="text.share.email.modal.and.msg" /> 
										 <a href="<spring:theme code="text.share.email.modal.privacy.href" />" target="_new" class="link"><spring:theme code="text.share.email.modal.privacy.msg" /></a>.
								</label>
								<p>
									<spring:theme code="text.share.email.modal.policy.msg" />
								</p>
							</div>
						</div>
						<div class="submit-btn col-md-12 col-xs-12">
							<button class="btn btnShareEmail" type="button">
								<spring:theme
									code="text.share.email.modal.button.name.sendMessage" />
							</button>
						</div>
					</div>
				</form:form>

			</div>
		</div>
	</div>
</div>