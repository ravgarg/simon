<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

	<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="trends-landing-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="trends-landing-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<h1 class="page-heading major"> Trend Shops </h1>
			<jsp:include page="trends-heading-desc.jsp"></jsp:include>			
			<div class="store-main-carousel">
			<jsp:include page="trends-landing-hero-carousal.jsp"></jsp:include>
			</div>
			<jsp:include page="trends-landing-content.jsp"></jsp:include>	
			
		</div>
	</div>
	</div>	
	<jsp:include page="social-shop.jsp"></jsp:include>
	<jsp:include page="footer-content-slot.jsp"></jsp:include>	
