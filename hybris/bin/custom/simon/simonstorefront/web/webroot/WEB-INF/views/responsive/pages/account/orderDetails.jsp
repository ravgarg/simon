<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix = "fn" uri = "http://java.sun.com/jsp/jstl/functions"%>

<c:set var="order" value="${orderDetails}" />
<div class="modal-header">
	<h5 class="modal-title">
		<spring:theme code="order.details.header.text" />
	</h5>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
</div>
<div class="modal-body clearfix">
	<div class="order-detail-container">
		<div class="content-space">
			<div class="myorder-details">
				<div class="major">
					<spring:theme code="order.details.premium.order.number.text" />
				</div>
				<div class="normal-text order-number" data-analytics-ordercode="${order.code}">${order.code}</div>
			</div>
			<div class="myorder-details">
				<div class="major order-date"><spring:theme code="order.details.order.date.text" /></div>
				<div class="normal-text"><fmt:formatDate value="${order.created}" dateStyle="long" type="date" /></div>
			</div>
		</div>
		<div class="content-space">
			<div class="myorder-details">
				<div class="major"><spring:theme code="order.details.shipping.address.text" /></div>
				<div class="normal-text shipping-address">
					<p>${order.deliveryAddress.firstName}&nbsp;${order.deliveryAddress.lastName}</p>
					<p>${order.deliveryAddress.line1}&nbsp;${order.deliveryAddress.line2}</p>
					<p data-analytics-state="${order.deliveryAddress.region.isocodeShort}" data-analytics-zip="${order.deliveryAddress.postalCode}">${order.deliveryAddress.town},&nbsp;${order.deliveryAddress.region.isocodeShort}&nbsp;${order.deliveryAddress.postalCode}</p>
				</div>
			</div>
			<div class="myorder-details">
				<div class="major"><spring:theme code="order.details.payment.method.text" /></div>
				<div class="normal-text payment-method" data-analytics-payment="${fn:toUpperCase(order.paymentInfo.cardType)}">${fn:toUpperCase(order.paymentInfo.cardType)}</div>
			</div>
		</div>
		<c:forEach items="${order.retailerInfoData}" var="retailer">
			<div class="content-space last-call analytics-orderretailerList">
				<div class="major confirm-retailer" data-analytics-shippingmethod="${retailer.appliedShippingMethod.name}" data-analytics-retailername="${retailer.retailerName}" data-analytics-retailerdiscount="${not empty retailer.promotionalDiscount ? retailer.promotionalDiscount.value : 0}" data-analytics-retaileritem="${retailer.totalUnitCount}">${retailer.retailerName} (${retailer.totalUnitCount})</div>
				<div class="normal-text">
					<div class="return-information">
						<a class="viewReturnPolicy" href="/customer-service/shipping-returns" target="_blank"><spring:theme code="order.details.shipping.return.policy.text" /></a>
					</div>
					<div class="order-track">
						<div class="bold-txt">
							<spring:theme code="order.details.retailer.track.order.text" />
						</div>
						<div>
							<c:if test="${not empty retailer.orderTrackingList}">

								<c:forEach items="${retailer.orderTrackingList}"
									var="trackOrder">
									<div class="customer-service">
										<c:choose>
											<c:when test="${not empty trackOrder.trackingNumber}">
												<c:choose>
													<c:when test="${not empty trackOrder.carrierUrl}">
														<a href="${trackOrder.carrierUrl}" target="_blank">${trackOrder.trackingNumber}</a>
													</c:when>
													<c:otherwise>
														${trackOrder.trackingNumber}
												</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<a href="${retailer.customerServiceLink}" target="_blank"><spring:theme
														code="order.details.track.order.customer.service.text" /></a>
											</c:otherwise>
										</c:choose>

									</div>
									</br/>
								</c:forEach>
							</c:if>
							<c:if test="${empty retailer.orderTrackingList}">
								<a href="${retailer.customerServiceLink}" target="_blank"><spring:theme
										code="order.details.track.order.customer.service.text" /></a>
							</c:if>
						</div>
					</div>

					<div class="order-track">
					</div>
					<div class="order-track">
						<div class="bold-txt"><spring:theme code="order.details.retailer.order.number.text" /></div>
						<div>
							<c:choose>
								<c:when test="${not empty retailer.retailerOrderNumber}">${retailer.retailerOrderNumber}</c:when>
								<c:otherwise>--</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="cart-items-container checkout-accordian">
						<div class="return-information"><spring:theme code="order.details.column.header.items.text" /></div>
						<div class="checkout-item-heading">
							<div class="row">
								<div class="col-md-offset-8 col-md-2 col-xs-1 hide-mobile">
									<div class="price-heading hide-mobile"><spring:theme code="order.details.column.header.item.price.text" /></div>
								</div>
								<div class="col-md-2 col-xs-2 col-xs-offset-9 subtotal-margin">
									<div class="total-heading"><spring:theme code="order.details.column.header.subtotal.text" /></div>
								</div>
							</div>
						</div>
						<c:forEach items="${retailer.productDetails}" var="entryData">
							
							<div class="row clearfix row-pad-btm analytics-cartItem">
								<div class="col-md-2 col-xs-4">
									<c:if test="${entryData.product.purchasable}"><a href="${entryData.product.url}"></c:if>
										<c:choose>
											<c:when test="${not empty entryData.product.images}">
												<c:forEach items="${entryData.product.images}" var="images">
										         	<c:if test="${images.format eq 'thumbnail' }">
										                  <img class="img-width" alt="${entryData.product.baseOptions[0].selected.baseProductDesignerName}&nbsp;${entryData.product.variantValues.Color}&nbsp;${entryData.product.name}" src="${images.url}">
										         	</c:if>
									         	</c:forEach>
											</c:when>
											<c:otherwise>
													<img class="img-width" alt="${entryData.product.baseOptions[0].selected.baseProductDesignerName}&nbsp;${entryData.product.variantValues.Color}&nbsp;${entryData.product.name}" src="/_ui/responsive/simon-theme/images/no-image-105x160.jpg">
											</c:otherwise>
										</c:choose>
									<c:if test="${entryData.product.purchasable}"></a></c:if>
								</div>
								
								<div class="col-md-6 col-xs-8 row-pad-l">
									<c:set var = "itemCode" value=""/>
									<c:set var = "itemType" value=""/>
									<c:set var = "itemDescription" value=""/>
									<c:if test="${not empty entryData.itemPromotionList}">					
										<c:forEach items="${entryData.itemPromotionList}" var="itemPromotionData" varStatus="status">
										<c:choose>																				
										<c:when test="${not status.last}">
											<c:set var = "itemCode" value="${itemCode}${itemPromotionData.code}>"/>
											<c:set var = "itemType" value="${itemType}${itemPromotionData.type}>"/>
											<c:set var = "itemDescription" value="${itemDescription}${itemPromotionData.description}>"/>
										</c:when>
										<c:otherwise>
											<c:set var = "itemCode" value="${itemCode}${itemPromotionData.code}"/>
											<c:set var = "itemType" value="${itemType}${itemPromotionData.type}"/>
											<c:set var = "itemDescription" value="${itemDescription}${itemPromotionData.description}"/>
										</c:otherwise>
										</c:choose>	
										</c:forEach>
									</c:if>
									<div class="item-promotion-spacing" data-analytics-item-offercode="${itemCode}" data-analytics-item-offertype="${itemType}" data-analytics-item-offerdesc="${itemDescription}"></div>
									<c:if test="${entryData.product.purchasable}">
										<div class="item-description" data-analytics-item-designer="${entryData.product.baseOptions[0].selected.baseProductDesignerName}" data-analytics-item-id="${entryData.product.code}">
											<a href="${entryData.product.url}" data-analytics-item-name="${entryData.product.name}">${entryData.product.name}</a>
										</div>
									</c:if>
									<c:if test="${!entryData.product.purchasable}">
										<div class="item-description-inactive">
											<a href="javascript:void(0)" data-analytics-item-name="${entryData.product.name}">${entryData.product.name}</a>
										</div>
									</c:if>
									<div class="order-track" data-analytics-orderstatus="${entryData.status}">
										<c:if test='${entryData.status eq "PROCESSING"}'>
											<div class="status processing" >
												<spring:theme code="order.details.order.entry.status.processing" />
											</div>
										</c:if>
										<c:if test='${entryData.status eq "CANCELLED"}'>
											<div class="status cancelled">
												<spring:theme code="order.details.order.entry.status.cancelled" />
											</div>
										</c:if>
										<c:if test='${entryData.status eq "CONTACT_RETAILER"}'>
											<div class="status completed">
												<spring:theme code="order.details.order.entry.status.contact.retailer" />
											</div>
										</c:if>
										<c:if test='${entryData.status eq "ORDER_SENT_TO_RETAILER"}'>
											<div class="status processing">
												<spring:theme code="order.details.order.entry.status.order.sent.to.retailer" />
											</div>
										</c:if>
										<c:if test='${entryData.status eq "SHIPPED"}'>
											<div class="status completed">
												<spring:theme code="order.details.order.entry.status.shipped" />
											</div>
										</c:if>
										<c:if test='${entryData.status eq "RETURNED"}'>
											<div class="status completed">
												<spring:theme code="order.details.order.entry.status.returned" />
											</div>
										</c:if>
									</div>
									<div class="hide-desktop">
										<c:if test="${not empty entryData.msrpValue.value}">
											<div class="item-price" data-analytics-item-price-actual="${entryData.msrpValue.value}">${entryData.msrpValue.formattedValue}</div>
										</c:if>
										<div class="item-revised-price">
											<c:if test="${not empty entryData.saleValue.value}">
												${entryData.saleValue.formattedValue}
												<c:if test="${not empty entryData.msrpValue.value}">
													<img src="/_ui/responsive/simon-theme/icons/black-question.svg"
														data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" 
														data-original-title="<spring:theme code="order.details.msrp.text" /> ${entryData.msrpValue.formattedValue}<br>
																			<spring:theme code="order.details.list.price.text" /> ${entryData.listValue.formattedValue}<br>
																			<spring:theme code="order.details.sale.price.text" /> ${product.saleValue.formattedValue}">
												</c:if>
											</c:if>
											<c:if test="${empty entryData.saleValue.value}">
												${entryData.listValue.formattedValue}
											</c:if>
										</div>
										<div class="subtotal hide-desktop">${entryData.totalPrice.formattedValue}</div>
										<div class="item-discount">${entryData.savingsMessage}</div>
									</div>
									<div class="item-qty" data-analytics-item-quantity="${entryData.quantity}">Quantity: ${entryData.quantity}</div>
									<c:forEach items="${entryData.product.variantValues}" var="entry">
										<div class="item-color" data-analytics-${entry.key}="${entry.value}">${entry.key}: ${entry.value}</div>
									</c:forEach>
								</div>
								<div class="col-md-2 col-xs-2">
									<div class="hide-mobile">
										<c:if test="${not empty entryData.msrpValue.value}">
											<span class="item-price strike" data-analytics-item-strike-through="${entryData.msrpValue.value}">${entryData.msrpValue.formattedValue}</span>
										</c:if>
										<c:if test="${empty entryData.msrpValue.value}">
											<c:if test="${not empty entryData.saleValue.value}">
												<span class="item-price strike" data-analytics-item-strike-through="${entryData.listValue.value}">${entryData.listValue.formattedValue}</span>
											</c:if>
										</c:if>
										<div class="item-revised-price">
											<c:if test="${entryData.promotionApplicable}">
									  <c:choose>
											<c:when test="${empty entryData.saleValue.value}">
												<input type="hidden" class="price_beforediscount"
													value="${entryData.basePrice.value}" />
											</c:when>
											<c:otherwise>
												<input type="hidden" class="price_beforediscount"
													value="${entryData.saleValue.value}" />
											</c:otherwise>
										</c:choose>
										<span data-analytics-item-price="${entryData.yourPrice.value}">${entryData.yourPrice.formattedValue}</span>&nbsp;
												<c:if test="${not empty entryData.msrpValue.value and not empty entryData.saleValue.value}">
													<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" 
														class="tooltip-init simon-logo" data-toggle = "tooltip" title = '<spring:theme code="order.details.msrp.text"/>${entryData.msrpValue.formattedValue}<br>
																														<spring:theme code="order.details.list.price.text"/>${entryData.listValue.formattedValue}<br>
																														<spring:theme code="order.details.your.price.text"/>${entryData.yourPrice.formattedValue}<br>
																														<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="">
												</c:if>
												<c:if test="${empty entryData.msrpValue.value}">
														<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" 
															class="tooltip-init simon-logo" data-toggle = "tooltip" title = '<spring:theme code="order.details.your.price.text"/>${entryData.yourPrice.formattedValue}<br>
																															<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="">
												</c:if>
											</c:if>
											<c:if test="${not entryData.promotionApplicable}">
												<c:if test="${not empty entryData.saleValue.value}">
													<span data-analytics-item-price="${entryData.saleValue.value}">${entryData.saleValue.formattedValue}</span>&nbsp;
												</c:if>
												<c:if test="${empty entryData.saleValue.value}">
													<span data-analytics-item-price="${entryData.listValue.value}">${entryData.listValue.formattedValue}</span>&nbsp;
												</c:if>
												<c:if test="${not empty entryData.msrpValue.value and not empty entryData.saleValue.value}">
													<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true"
														class="tooltip-init simon-logo" data-toggle="tooltip" title='<spring:theme code="order.details.msrp.text"/>${entryData.msrpValue.formattedValue}<br>
																													<spring:theme code="order.details.list.price.text"/>${entryData.listValue.formattedValue}<br>
																													<spring:theme code="order.details.sale.price.text"/>${entryData.saleValue.formattedValue}<br>
																													<spring:theme code="order.details.you.saved.text"/>${entryData.youSave.formattedValue}' alt="">
												</c:if>
											</c:if>
										</div>
										<div class="item-discount">${entryData.savingsMessage}</div>
									</div>
								</div>
								<div class="col-md-2 col-xs-2 hide-mobile">
									<div class="subtotal">${entryData.totalPrice.formattedValue}</div>
								</div>
							</div>
						</c:forEach>
						<div class="row">
							<div class="col-md-offset-2 col-md-10 col-xs-8 col-xs-offset-4">
								<div class="price-container-left">
									<div class="price-calculation-container">
										<div class="price-top-spacing">
											<span class="estimated-subtotal"><spring:theme code="order.details.retailer.estimated.subtotal.text" /></span>
											<span class="estimated-sub-price" data-analytics-retailer-exclusive="${retailer.estimatedSubtotal.value}">${retailer.estimatedSubtotal.formattedValue}</span>
										</div>
										<div class="ship-top-spacing">
											<span class="estimated-shipping"><spring:theme code="order.details.retailer.estimated.shipping.text" /></span>
											<span class="estimated-shipping-price" data-analytics-retailer-shipping="${retailer.estimatedShipping.value}">${retailer.estimatedShipping.formattedValue}</span>
										</div>
										<div class="choose-method" data-analytics-retailer-shipping_method="${retailer.selectedShippingMethod}">
											<a href="#">${retailer.selectedShippingMethod}</a>
										</div>
										<div class="tax-top-spacing">
											<span class="estimated-tax"><spring:theme code="order.details.retailer.estimated.tax.text" /></span>
											<span class="estimated-tax-price" data-analytics-retailer-tax="${retailer.estimatedTax.value}">${retailer.estimatedTax.formattedValue}</span>
										</div>
										<div class="last-call-spacing">
											<span class="last-call-retailer"><spring:theme code="order.details.retailer.estimated.total.text" arguments="${retailer.retailerName}" /></span>
											<span class="last-call-price" data-analytics-retailer-gross-total="${retailer.estimatedTotal.value}">${retailer.estimatedTotal.formattedValue}</span>
										</div>
										
										<c:set var = "retailerCode" value=""/>
										<c:set var = "retailerType" value=""/>
										<c:set var = "retailerDescription" value=""/>
										<c:if test="${not empty retailer.retailerPromotionList}">
										<c:forEach items="${retailer.retailerPromotionList}" var="retailerPromotionList" varStatus="status">
											<c:choose>
											<c:when test="${not status.last}">
												<c:set var = "retailerCode" value="${retailerCode}${retailerPromotionList.code}>"/>
												<c:set var = "retailerType" value="${retailerType}${retailerPromotionList.type}>"/>
												<c:set var = "retailerDescription" value="${retailerDescription}${retailerPromotionList.description}>"/>
											</c:when>
											<c:otherwise>
												<c:set var = "retailerCode" value="${retailerCode}${retailerPromotionList.code}"/>
												<c:set var = "retailerType" value="${retailerType}${retailerPromotionList.type}"/>
												<c:set var = "retailerDescription" value="${retailerDescription}${retailerPromotionList.description}"/>
											</c:otherwise>
											</c:choose>
										</c:forEach>
										</c:if>
										<div class="you-save-spacing" data-analytics-retailer-offercode="${retailerCode}" data-analytics-retailer-offertype="${retailerType}" data-analytics-retailer-offerdesc="${retailerDescription}">
											<span class="you-save"><spring:theme code="order.details.retailer.you.saved.text" /></span>
											<span class="you-save-price" data-analytics-retailer-discount="${retailer.saving.value}">${retailer.saving.formattedValue}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
		<div class="order-details-all-items">
			<div class="cart-checkout-allitems order-summary-bottom">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div class="price-container-left cart-checkout-items">
							<div class="price-calculation-container">
								<div class="price-top-spacing">
									<span class="estimated-subtotal"><spring:theme code="order.details.estimated.order.subtotal.text" /></span>
									<span class="estimated-sub-price" data-analytics-exclusive="${order.subTotal.value}">${order.subTotal.formattedValue}</span>
								</div>
								<div class="ship-top-spacing">
									<span class="estimated-shipping"><spring:theme code="order.details.combined.shipping.charges" /></span>
									<span class="estimated-shipping-price" data-analytics-shipping="${order.deliveryCost.value}">${order.deliveryCost.formattedValue}</span>
								</div>

								<div class="tax-top-spacing">
									<span class="estimated-tax"><spring:theme code="order.details.estimated.taxes.text" /></span>
									<span class="estimated-tax-price" data-analytics-tax="${order.totalTax.value}">${order.totalTax.formattedValue}</span>
									<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true"
										class="tooltip-init simon-logo" data-toggle = "tooltip" 
										title = "<p>
													<spring:theme code="order.details.estimated.taxes.para1.text" />
												</p>
												<p>
													<spring:theme code="order.details.estimated.taxes.para2.text" />
												</p>"
										alt="">
								</div>
								<div class="last-call-spacing">
									<span class="last-call-retailer"><spring:theme code="order.details.estimated.order.total" /></span>
									<span class="last-call-price" data-analytics-gross-total="${order.totalPrice.value}">${order.totalPrice.formattedValue}</span>
								</div>
								<div class="you-save-spacing">
									<span class="you-save"><spring:theme code="order.details.you.saved.text" /></span>
									<span class="you-save-price" data-analytics-discount="${order.totalDiscounts.value}">${order.totalDiscounts.formattedValue}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cart-help-container">
				<div class="help-info"><spring:theme code="order.details.helpful.information.text" /></div>
				<div class="information"><spring:theme code="order.details.helpful.information.content.text" /></div>
				<div class="question">
					<spring:theme code="order.details.question.text" /><a href="/customer-service/contact-us" target="_blank"><spring:theme code="order.details.contact.us.text" /></a>
				</div>
				<div class="more-details">
					<a href="/customer-service/shipping-returns" target="_blank" class="analytics-genericLink" data-analytics-eventtype="link_click|policy_links" data-analytics-eventsubtype="view_shipping_method" data-analytics-linkplacement="orderdetail" data-analytics-linkname="policy_links" data-analytics-satellitetrack="link_track"><spring:theme code="order.details.shipping.methods.charges.details.text" /></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-xs-12 close-popup">
					<button type="button" class="btn" data-dismiss="modal"><spring:theme code="order.details.close.button.text" /></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer show-mobile">
	<button class="btn plum">Update Item</button>
</div>