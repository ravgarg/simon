<div class="create-account-container privacy-wall">
   	<form action="submit"  class="sm-form-validation bt-flabels analytics-registerForm js-flabels captchaValidation" data-parsley-validate="validate">
       <div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="sm-input-group">
					<label class="field-label" for="newCustomerEmail"><spring:theme code="register.new.customer.email.placeHolder" /> <span class="field-error" id="error-j_guestEmailId"></span></label>
					<input type="email" class="form-control" id="newCustomerEmail" placeholder="Email Address" name="email" data-parsley-errors-container="#error-j_guestEmailId" type="email" data-parsley-type="email" data-parsley-error-message="Please enter your email address" required>
				</div>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="sm-input-group">
					<label class="field-label" for="user-pswd"><spring:theme code="register.new.customer.pwd.placeHolder" /> <span class="field-error" id="error-j_pswd"></span></label>
					<input 
						type="password" 
						autocomplete="off"
						class="form-control" 
						placeholder="Password" 
						name="pwd" 
						data-parsley-errors-container="#error-j_pswd" 
						id="user-pswd"
						data-parsley-required-message="Please enter a password" 
						data-parsley-required 
						data-parsley-pwdstrength-message="Please enter a valid password"
						data-parsley-pwdstrength />
				</div>
			</div>
		</div>
		<div class="row">				
			<div class="col-xs-12 col-md-6 captchaContainer">
				<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
				<div id="captcha" class="has-error"><spring:theme code="register.new.customer.captcha.error.msg" /></div>
			</div>
			
			<div class="terms-conditions col-md-6 col-xs-12 small-text hidden-xs">
				128-bit SSL Bank-level Security
			</div>	
		</div>
		<div class="row">
			<div class="submit-btn col-md-6 col-xs-12">
				<button class="btn">Submit</button>
			</div>
			<div class="terms-conditions col-md-6 col-xs-12 small-text visible-xs">
				128-bit SSL Bank-level Security
			</div>	
		</div>		
      </form>
     </div>