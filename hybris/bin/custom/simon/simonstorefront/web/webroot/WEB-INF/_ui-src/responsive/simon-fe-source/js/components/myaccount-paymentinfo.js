define(['hbshelpers', 'handlebars', 'utility', 'ajaxFactory', 'spreedly', 'viewportDetect','analytics'],
    function(hbshelpers, handlebars, utility, ajaxFactory, spreedly, viewportDetect,analytics) {
        'use strict';
        var cache;
        var myaccountPaymentInfo = {
            init: function() {
				$('.addaddress-section').hide();
                this.initVariables();
                this.initEvents();
                this.initAjax();
             
                this.spreedlyinit();
				
            },
            initVariables: function() {
                cache = {
                    $document : $(document),
                    $window : $(window),
                    $analyticsError : [],
                    $accformvalidated : true
                }
            },
            initAjax: function() {
				
			},
            initEvents: function() {
                cache.$document.on('change',"select[name='address-list']", myaccountPaymentInfo.addAddressDropdown),
                cache.$document.on('click',"input[id='preferredCard']", myaccountPaymentInfo.addToPreferredCard),
                cache.$document.on('click',".delete-payment", myaccountPaymentInfo.deletePaymentModal)
            },
			deletePaymentModal: function(){				
				$('input[name="paymentInfoId"]').val( $(this).data('paymentid') );
				var paymentMethodType = $(this).data("payment-method-type");
				$('#paymentMethodDeleteModal .confirm-msg').find('.paymentMethodType').html(paymentMethodType);
			},
			addToPreferredCard: function(){		
				
				$(this).closest('form').submit();
			},
			addAddressDropdown: function(){
				
				var $this = $(this),
					dropVal = $this.val();
					
				if( dropVal === '' ){
					$('.addaddress-section').clone().appendTo( $('#addAddressContainer') );
					$('#addAddressContainer').find('.addaddress-section').show();
				}else{
					$('#addAddressContainer').empty();
					$('.addaddress-section').hide();
				}
			},
			spreedlyinit:function(){
				Spreedly.init();
				
				Spreedly.on('paymentMethod', function(token, pmData) {
					  var tokenField = document.getElementById("account_method_token");
					  tokenField.setAttribute("value", token);
					  $('#cardType').val(pmData.card_type);
					  $('#cardNumber').val(pmData.number);
					  $('#lastFourDigitCard').val(pmData.last_four_digits);
                });
				
				Spreedly.on('errors', function(errors) {
                  var messageEl = document.getElementById('errors');
                  var errorBorder = "1px solid red";
                  for(var i = 0; i < errors.length; i++) {
                    var error = errors[i];
                    if(error["attribute"]) {
                      var masterFormElement = document.getElementById(error["attribute"]);
                      if(masterFormElement) {
                        masterFormElement.style.border = errorBorder
                      } else {
                        Spreedly.setStyle(error["attribute"], "border: " + errorBorder + ";");
                      }
					  cache.$analyticsError.push(error.message);
                    }
                    messageEl.innerHTML = "";
                    messageEl.innerHTML += error["message"] + "<br/>";
                  }
				  
				 
                });
				
				Spreedly.on('validation', function(inputProperties) {
                    if(!inputProperties["validCvv"]) {
                        cache.$accformvalidated = false;
                        var errorBorder = "1px solid #dd0b0b";
                        Spreedly.setStyle('cvv', "border: " + errorBorder + ";");
                        $('#checkoutPaymentMsgContainer').find('.error-desc').html($('#cvvLengthValidationError').val());
                        cache.$analyticsError.push('invalid_CVV');	
                        cache.$analyticsError.join('|')
                        return false;
                    }else{
                        cache.$accformvalidated = true;
                    }
					
                });
				
				Spreedly.on('ready', function() {
					Spreedly.setFieldType('text')
					Spreedly.setNumberFormat('prettyFormat');
					Spreedly.setStyle('number','border: 1px solid #B4B2B0; height:39px;font-size:13.3px; padding: 6px 12px;');
					Spreedly.setStyle('cvv', 'height: 32px; border-radius: none; border: 1px solid #B4B2B0; padding: .65em 1em; font-size: 91%;');
					if(viewportDetect.lastClass === 'small'){
						Spreedly.setStyle('cvv', 'width: 82%;');
						Spreedly.setStyle('number','width: 91%;');
					}else{
						Spreedly.setStyle('cvv', 'width: 48%;');
						Spreedly.setStyle('number','width: 94%;');
					}
					Spreedly.setPlaceholder('number', 'CARD NUMBER');
					Spreedly.setPlaceholder('cvv', 'CVV'); 
					
                });
				
				
				$('#addPaymentMethod').click(function(event){
                    event.preventDefault();
                    $(this).prop('disabled', true);
                    Spreedly.validate();
                    var normalBorder = "1px solid #ccc";

                   // These are the fields whose values we want to transfer *from* the
                   // master form *to* the payment frame form. Add the following if
                   // you're displaying the address:
                   // ['address1', 'address2', 'city', 'state', 'zip', 'country']
                   var paymentMethodFields = ['first_name', 'last_name', 'month', 'year'],
                   options = {};
                   for(var i = 0; i < paymentMethodFields.length; i++) {
                     var field = paymentMethodFields[i];

                     // Reset existing styles (to clear previous errors)
                     var fieldEl = document.getElementById(field);
                     fieldEl.style.border = normalBorder;

                     // add value to options
                     options[field]  = fieldEl.value
                   }

                   // Reset frame styles
                 
                   // Tokenize!
                   if($('#enableSpreedlyCalls').val() === 'true'){
                       Spreedly.tokenizeCreditCard(options,function(){

                       });
                   }
                   setTimeout(function() {
                     //your code to be executed after 1 second
                       var formData;
                       if($('#enableSpreedlyCalls').val() === 'true'){
                           formData = {
                               "securityCode" : $("input[name=account_method_token]").val(),
                               "nameOnCard":$("#first_name").val()+ " " + $("#last_name").val(),
                               "lastFourDigits":$('#lastFourDigitCard').val(),
                               "cardType":$('#cardType').val(),
                               "cardNumber":$('#cardNumber').val(),
                               "expirationMonth":$('#month').val(),
                               "expirationYear":$('#year').val(),
                               "billingAddId":$('#address-list').val(),
                               "CSRFToken" :$("input[name=CSRFToken]").val(),
                               "line1": $('#billingAddress1').val(),
                               "line2": $('#billingAddress2').val(),
                               "town": $('#billingCity').val(),
                               "state": $('#billingState').val(),
                               "postalCode": $('#zipCode').val(),
                               "defaultPayment":$('#defaultPayment').is(":checked")
                           }
                        } else {
                   		formData = {
                   			// START Setting up temporary values for parameters used for payment mocking
                   			"securityCode" : "5DSRBHMmp9NfDvXSvgORuNCbkVt",
                               "lastFourDigits":"1111",
                               "cardType":"visa",
                               "cardNumber":"XXXX-XXXX-XXXX-1111",
                               // END Setting up temporary values for parameters used for payment mocking
                               "nameOnCard":$("#first_name").val()+ " " + $("#last_name").val(),
                               "expirationMonth":$('#month').val(),
                               "expirationYear":$('#year').val(),
                               "billingAddId":$('#address-list').val(),
                               "CSRFToken" :$("input[name=CSRFToken]").val(),
                               "line1": $('#billingAddress1').val(),
                               "line2": $('#billingAddress2').val(),
                               "town": $('#billingCity').val(),
                               "state": $('#billingState').val(),
                               "postalCode": $('#zipCode').val(),
                               "defaultPayment":$('#defaultPayment').is(":checked")
                       	}
                   	}
                          
                        var objectOptions = {
                            'methodType': 'POST',
                            'dataType': 'JSON',
                            'methodData':formData,
                            'url': '/my-account/payment-details',
                            'isShowLoader': false,
                            'cache' : true
                        }

                        if($('#account_method_token').val()  && cache.$accformvalidated){
                       	     ajaxFactory.ajaxFactoryInit(objectOptions, function(response){
								 if(response==='success=true') {
									if($('#analyticsSatelliteFlag').val()==='true'){
										var $data = {
												formname : 'payment_method',
												formnametype : 'payment_method',
												type : 'registration',
												stage : 'payment_method_completion',
												satelliteTrack : 'registration'
											};
										analytics.analyticsAJAXSuccessHandler($data);
									}
									var url = window.location.href.replace(/\?.+/, ''),
									successUrl = url + "?success=true";
									window.location.href = successUrl;
								}else {
									if($('#analyticsSatelliteFlag').val()==='true'){
										analytics.analyticsAJAXFailureHandler('payment_method','something went wrong','CE');
									}
									var failurl = window.location.href.replace(/\?.+/, ''),
									failureUrl = failurl + "?success=false";
									window.location.href = failureUrl;
								}
							   
							 }, '','payment_method');
                            }else{
							 if($('#analyticsSatelliteFlag').val()==='true'){
								// PAYMENT PANEL - SPREEDLY ERRORS - setTimeout - formsubmit
								jQuery.unique(cache.$analyticsError);
								
								var $param = {
										formname : '#paymentMethodModal form.analytics-formValidation',
										formerror : cache.$analyticsError.join('|')
									};
								analytics.formSubmitValidation($param);
							 }
						 }
                       
                   }, 4000);
				})
				
			}

        };
        return myaccountPaymentInfo;
    });

	