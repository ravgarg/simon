<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<div class="myoffers-container">
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.</p>
	<span class="add-deals visible-xs">How to Add Deals</span>
	<div class="owl-carousel" id="homepageCarousel">
		<cms:pageSlot position="csn_HeroSlot" var="feature">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>
	<div class="deal-area-container">
		<!-- all-deals area -->
		<div class="deal-details-container row">
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12">
				<div class="deal-block">
					<span class="icon-fav-blank"></span>
					<a href="#">
						<img src="/_ui/responsive/simon-theme/images/logo-slot.png" />
					</a>
					<div class="gift-details">
						Up to 75% Off Last Minute Gifts
					</div>
					<div>
						Valid Oct. 7-10 Online & In-Store
					</div>
				</div>
			</div>
		</div>
	</div>
</div>