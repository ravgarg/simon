<div class="modal fade plp-filters-modal" id="plpFiltersModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Filter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="plp-filters">
                    <jsp:include page="plp-filters-common.jsp"></jsp:include>
                </div>
            </div>
        </div>
    </div>
</div>