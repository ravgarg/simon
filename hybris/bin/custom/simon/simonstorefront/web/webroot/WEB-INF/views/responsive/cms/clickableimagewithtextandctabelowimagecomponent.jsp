<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<c:set var="imageDestinationLink" value="${backgroundImage.destinationLink}" />
<c:choose>
	<c:when test="${destinationLink.target eq 'NEWWINDOW'}">
    	<c:set var="imageDestinationTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="imageDestinationTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${fontColor eq 'BLACK'}">
    	<c:set var="fontColor" value="black-text" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>

<div class="style-items col-xs-12 col-md-4 analytics-data" data-analytics='{
  "event": {
    "type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|style_deals",
    "sub_type": "${fn:toLowerCase(component.itemtype)}"
  },
  "banner": {
    "click": {
      "id": "${fn:toLowerCase(component.uid)}"
    }},"link": {
    "placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|style_deals",
    "name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>"
    }
}'>
	<div class="style-content-container">
		<div class="style-container ${fontColor}">
		<c:choose>
			<c:when test="${not empty imageDestinationLink && imageDestinationLink ne '#'}">
			<a target="${imageDestinationTarget}" href="${ycommerce:getUrlForCMSLinkComponent(imageDestinationLink)}">
				<picture>
					<source media="(max-width: 991px)" srcset="${mobileImage}">
					<img class="js-responsive-image11" alt="${backgroundImage.imageDescription}" style="" src="${desktopImage}">
				</picture>
			</a>
			</c:when>
			<c:otherwise>
			<picture>
					<source media="(max-width: 991px)" srcset="${mobileImage}">
					<img class="js-responsive-image11" alt="${backgroundImage.imageDescription}" style="" src="${desktopImage}">
				</picture>
			</c:otherwise>
		</c:choose>

			<h5 class="major">
			<c:choose>
				<c:when test="${not empty titleText.url}">
				<a href="${titleText.url}">${titleText.linkName}</a>
				</c:when>
				<c:otherwise>
				${titleText.linkName}
				</c:otherwise>
			</c:choose>
			</h5>
			<div data-satellitetrack="internal_click"  data-analytics='{"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|style_deals","name": "${fn:toLowerCase(fn:replace(subheadingText.linkName," ","_"))}"}'>
			<p>
			<c:choose>
				<c:when test="${not empty subheadingText.url}">
				<a href="${subheadingText.url}">${subheadingText.linkName}</a>
				</c:when>
				<c:otherwise>
				${subheadingText.linkName}
				</c:otherwise>
			</c:choose>
			</p></div>
			<p>${promoText}</p>
		</div>
	</div>
</div>