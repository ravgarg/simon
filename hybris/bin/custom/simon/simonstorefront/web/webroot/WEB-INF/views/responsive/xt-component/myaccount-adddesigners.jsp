<div class="custom-heading-links-component">
	<h4>Lisa, you have no designers saved</h4>
	<!-----------EITHER BELOW DIV WILL COME-------------->
	<div class="clp-des">
		My Designers are a great way to keep track of your most-loved stores and share with friends. Just click the heart next to your favorite designers to create your own personalized mall and receive special offers.
	</div>
	<!---------------OR THIS----------------------------->
	<div class="row">
		<div class="col-md-4 custom-links">
			<a href="#" class="favourite-icon fill">Designer Name</a><br>
			<a href="#" class="favourite-icon fill">Designer Name</a>
		</div>
		<div class="col-md-4 custom-links">
			<a href="#" class="favourite-icon fill">Designer Name</a><br>
			<a href="#" class="favourite-icon fill">Designer Name</a>
		</div>
		<div class="col-md-4 custom-links">
			<a href="#" class="favourite-icon fill">Designer Name</a><br>
			<a href="#" class="favourite-icon fill">Designer Name</a>
		</div>
	</div>
	<!-------------------------------------------------->
	<div class="btn-category"><button class="btn black btn-width major" data-toggle="modal" data-target="#addDesignersModal">Add Designers</button></div>
</div>




		<div class="modal fade adddesigners-modal" id="addDesignersModal"	role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">addDesignersModal</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body clearfix">
						
							<form name="" method="get" action="" class="sm-form-validation designers-search">
								<input type="text" name="text" title="" class="form-control" id="" autocomplete="off" placeholder="Search Designer By Name">
							</form>
							<div class="designers-listing wrapper">
								<ul class="scroll-container">
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
									<li>
										<label class="custom-checkbox"><input type="checkbox" name="" value="" > <i class="i-checkbox"></i>Designer Name</label>
									</li>
								</ul>
							</div>
						
					</div>
					<div class="modal-footer clearfix">
						<button class="btn btn-add pull-left btn-width">Add Designers</button>
						<button class="btn secondary-btn black pull-left btn-width" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	
		
		 <!--confirmation  modal-->
       <div class="modal fade" id="removeAddressConfirmation" role="dialog">
              <div class="modal-dialog">
                     <div class="modal-content">
                         <div class="modal-header">
                             <h5 class="modal-title">Confirmation</h5>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                         </div>
                           <div class="privacy-content">
                                  Leaving My Designers will save the changes you have made. Are you sure?                          
                                  
                           </div>
                           <div class="btn-container clearfix cart-modal-footer">
                              <button class="btn plum major btn-width" id="removeEntry" data-action="remove">Save Changes</button>
                              <button class="btn secondary-btn black major btn-width"  data-dismiss="modal" aria-label="Close">Cancel</button>
                            </div>
                           
              </div>
       </div>
       </div>
       <!--//confirmation modal-->