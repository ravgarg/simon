<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<c:if test="${empty myFavProduts}">			
<div class="myarea-container">
<h4>${userName}, <spring:theme code="${noSavedDataKey}" /></h4>
		<p><spring:theme code="${myFavoriteDetails}" /></p>
</div>
</c:if>
