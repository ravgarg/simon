<div class="heading-text">
	<div class="shipping-add-text">Save your preferred shipping addresses to make your next checkout experience faster. Suspendisse hendrerit eget orci a egestas. Proin mollis tellus vitae ex auctor congue. Phasellus ante erat, tempus vulputate augue quis, ultrices viverra dui.</div>
	<div class="not-saved-add-text">You have not yet saved an address. Save your shipping addresses to make your next checkout experience faster.</div>
	<jsp:include page="address-book-edit-delete-tile.jsp"></jsp:include>
	<button type="button" class="btn add-new-address col-xs-12 col-md-3" data-toggle="modal" data-target="#addressBookModal">Add New Address</button>
</div>