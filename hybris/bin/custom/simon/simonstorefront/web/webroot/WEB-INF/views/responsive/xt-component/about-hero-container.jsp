<div class="static-hero-container">
	<picture>
		<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/clp-mobile-image@2x.png">
		<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/plp-image.png">
	</picture>
	<div class="content-container">
		<h1 class="major">Lorem Ipsum<br/> Dolor Sit Amet</h1>
		<h5>Lorem ipsum dolor sit amet, consecur adipiscing elit</h5>
	</div>
</div>