<div class="container">
	<div class="the-designer-shop">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/checkout-image2@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/checkout-image2.png">
			</picture>
		</a>
		<div class="content-container">
			<h1 class="display-medium major">Lorem Ipsum <br>Dolor Sit Amet</h1>
			 <a href="#"><span class="shopnow major">SHOP NOW <span>&rsaquo;</span></span></a>
		</div>
	</div>
</div>
