<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty searchPageData.results}">
	<div class="myorder-details-container">
		<div class="myorder-btn-container myorder-order">
			<div class="row">
				<div class="col-md-3 col-xs-12">
					<div class="myorder-margin">
						<a href="${redirectToUrl}" class="analytics-genericLink" data-analytics-eventtype="continue_shopping" data-analytics-eventsubtype="myorder_page" data-analytics-linkplacement="myorder_page|shopnow" data-analytics-linkname="shopnow" data-analytics-satellitetrack="link_track">
							<button class="btn btn-primary block">${ctaText}</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
<c:if test="${empty searchPageData.results}">
	<div class="myorder-details-container row">
			<p class="no-order"><spring:theme code="order.history.no.orders.text" /></p>
	</div>
	<div class="myorder-details-container row">
		<div class="myorder-btn-container-no-order">
			<div class="row">
				<div class="col-md-3 col-xs-12">
					<div class="myorder-margin-no-order">
						<a href="${redirectToUrl}" class="analytics-genericLink" data-analytics-eventtype="continue_shopping" data-analytics-eventsubtype="myorder_page" data-analytics-linkplacement="myorder_page|shopnow" data-analytics-linkname="shopnow" data-analytics-satellitetrack="link_track">
							<button class="btn btn-primary block">${ctaText}</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>