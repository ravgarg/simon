<div class="order-summary-price-container pad-r-mobile pad-l-mobile collapse">
    <div class="price-top-spacing">
        <span class="estimated-subtotal">Estimated Order Subtotal</span>
        <span class="estimated-sub-price">$360.00</span>
    </div>
    <div class="ship-top-spacing">
        <span class="estimated-shipping">Combined Shipping Charges</span>
        <span class="estimated-shipping-price">--</span>
    </div>
    <div class="tax-top-spacing">
        <span class="estimated-tax">
            Estimated Taxes
            <a href="#">
                <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
            </a>
        </span>
        <span class="estimated-tax-price">--</span>
    </div>
    <div class="last-call-spacing">
        <span class="last-call">Estimated Order Total</span>
        <span class="last-call-price">$360.00</span>
    </div>
    <div class="you-save-spacing">
        <span class="you-save">You Saved</span>
        <span class="you-save-price">$80.00</span>
    </div>
</div>
<div class="btn-border-top">
     <div class="btn-padding"><button class="btn plum">Proceed to Checkout</button></div></div>
<div class="cart-help-container hide-mobile">
   <div class="help-info">Helpful Information</div>
   <div class="information">Please note Simon places orders on your behalf for
            each retailer you shopped at; you will see these as
            separate transactions on your credit card
            statement.
   </div>
   <div class="question">Question?<a href="#">Contact Us</a></div>
   <div class="more-details"><a href="#">See Shipping Methods & Charges for More Details</a></div>
</div>