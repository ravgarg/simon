<div class="account-info">
   <div class="row">
      <div class="col-xs-12">
         Welcome back, Ms. Simon
      </div>
   </div>
</div>



<div class="checkout-shipping-container">
   <div class="panel-group" id="accordion">
       
      <div class="panel panel-default">
            <h5 class="panel-title major">
               <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1">1. Shipping Information <span class="pull-right statement">We don't ship outside the United States</span></a>
            </h5>
           <div id="panel1" class="panel-collapse collapse in">
               <div class="collapsed-content">
                  
                  <form>
                    <div class="shipping-fields">
                        <div class="shipping-address">
                          <div class="shipping-method">
                            <div class="major small-text tag">
                              Shipping Address
                            </div>
                          <div class="sm-input-group select-menu">
                            <select class="form-control">
                              <option value="">Home - 18 Bull Path Close, East Hampton, NY 11937</option>
                            </select>
                          </div>
                          </div>
                        <button type="button" class="btn">save &amp; continue</button>
                        </div>
                    </div>

                    <div class="shipping-fields-edit">
                        <h6>SHIP TO <a href="#">Edit</a></h6>
                        <p class="editable-text">Lisa Simone<br>
                           18 Bull path Close<br>
                           East Hampton, NY 11937<br><br>

                           lisa.simone@gmail.com<br>
                           (972) 602-8383
                        </p>
                     </div>
                    

                  </form>
               </div>
           </div>
       </div>
<div style="clear:both;"></div>
       <div class="panel panel-default">
            <h5 class="panel-title major">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2">2. Review order & Shipping Method</a>
            </h5>
           <div id="panel2" class="panel-collapse collapse">
               <div class="collapsed-content">
                  
                  <div class="cart-checkout-items-order-details">
                      


                      <div class="cart-checkout-items">
                          <div class="confirm-retailer major">Last Call (2)</div>
                          <div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading hide-mobile">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="edit-item-bag"><a href="#checkoutLeavingModal" data-toggle="modal" data-dismiss="modal" data-target="#checkoutLeavingModal">Edit Item in Bag</a></div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Asymmetric-Peplum Midi Dress</a></div>
                                   <div class="additional">
                                     Additional 20% Off
                                   </div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="item-color">Size: 2</div>
                                   <div class="edit-item-bag"><a href="#checkoutLeavingModal" data-toggle="modal" data-dismiss="modal" data-target="#checkoutLeavingModal">Edit Item in Bag</a></div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-12">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$360.00</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">--</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">--</span>
                                      </div>
                                      <div class="tax-calculation">
                                         <a herf="#">Tax calculated during Checkout</a>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Last Call Estimated Total</span>
                                         <span class="last-call-price">$360.00</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$80.00</span>
                                      </div>

                                      <div class="shipping-method">
                                        <div class="major small-text tag">Select shipping method for last call</div>
                                        <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Premium Shipping - $7.50</option>
                                          </select>
                                        </div>
                                        <a class="viewReturnPolicy" data-href="/contentPolicy/2023">View Shipping, Returns &amp; Privacy Information</a>
                                      </div>
                                   </div>
                                   </div>
                                </div>
                             </div>

                          </div>
                       </div>









                      <div class="cart-checkout-items">
                          <div class="confirm-retailer major">Last Call (2)</div>
                          <div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading hide-mobile">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="edit-item-bag"><a href="#checkoutLeavingModal" data-toggle="modal" data-dismiss="modal" data-target="#checkoutLeavingModal">Edit Item in Bag</a></div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Asymmetric-Peplum Midi Dress</a></div>
                                   <div class="additional">
                                     Additional 20% Off
                                   </div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                   <div class="item-color">Size: 2</div>
                                   <div class="edit-item-bag"><a href="#checkoutLeavingModal" data-toggle="modal" data-dismiss="modal" data-target="#checkoutLeavingModal">Edit Item in Bag</a></div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                    <div class="subtotal hide-mobile">$200.00</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-12">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$360.00</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">--</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">--</span>
                                      </div>
                                      <div class="tax-calculation">
                                         <a herf="#">Tax calculated during Checkout</a>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Last Call Estimated Total</span>
                                         <span class="last-call-price">$360.00</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$80.00</span>
                                      </div>

                                      <div class="shipping-method">
                                        <div class="major small-text tag">Select shipping method for last call</div>
                                        <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Premium Shipping - $7.50</option>
                                          </select>
                                        </div>
                                        <a class="viewReturnPolicy" data-href="/contentPolicy/2023">View Shipping, Returns &amp; Privacy Information</a>
                                      </div>
                                   </div>
                                   </div>
                                </div>
                             </div>

                          </div>
                       </div>




                             <div class="row">
                                <div class="col-md-12 col-xs-12">
                                     <div class="price-container-left cart-checkout-items">
                                      <div class="price-calculation-container">
                                        <div class="price-top-spacing">
                                           <span class="estimated-subtotal">Estimated Order Subtotal</span>
                                           <span class="estimated-sub-price">$360.00</span>
                                        </div>
                                        <div class="ship-top-spacing">
                                           <span class="estimated-shipping">Combined Shipping Charges</span>
                                           <span class="estimated-shipping-price">$16.00</span>
                                        </div>
                                      
                                        <div class="tax-top-spacing">
                                           <span class="estimated-tax">Estimated Taxes</span>
                                           <span class="estimated-tax-price">--</span>
                                        </div>
                                        <div class="last-call-spacing">
                                           <span class="last-call-retailer"> Estimated Order Total</span>
                                           <span class="last-call-price">$360.00</span>
                                        </div>
                                        <div class="you-save-spacing">
                                           <span class="you-save">You Saved</span>
                                           <span class="you-save-price">$80.00</span>
                                        </div>
                                        <button type="button" class="btn">save &amp; continue</button>
                                     </div>
                                   </div>
                                </div>
                             </div>




                  </div>

               </div>
           </div>
       </div>



       <div class="panel panel-default">
            <h5 class="panel-title major">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3">3. Payment <span class="pull-right statement">Credit or Debit Card <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00"></span></a>
            </h5>
           <div id="panel3" class="panel-collapse collapse">
               <div class="collapsed-content">
                    <form>
                     <div class="shipping-fields payment-container">
                        <div class="major small-text tag">
                          Payment Method
                        </div>
                        <div class="row">
                           <div class="col-xs-12">
                              <div class = "sm-input-group select-menu">
                                 <select class = "form-control">
                                    <option value="STATE">Visa ending in 7571</option>
                                 </select>
                              </div>
                           </div>
                          </div>
                          <div class="row">
                           <div class="col-xs-6 col-md-3">
                              <div class="sm-input-group">
                                 <input type="text" class="form-control" placeholder="CVV">
                                 <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo cvv-tooltip" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                              </div>
                           </div>
                          </div>

                          <div class="order-summary-right cart-order-summary">
                              <jsp:include page="checkout-right-order-confirmation-summary.jsp"></jsp:include>
                          </div>
                  
                     </div>

                     
                  </form>
               </div>
           </div>
       </div>

   </div>
</div>