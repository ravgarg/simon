define('pages/store', ['jquery','slickCarousel','globalCustomComponents', 'viewportDetect', 'analytics'],
    function($,slickCarousel,globalCustomComponents, viewportDetect, analytics) {

        'use strict';
        var cache;
        var store = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.homepageCarousal();
                this.storeExpandText();
                if($('#analyticsSatelliteFlag').val()==='true'){
                    analytics.analyticsOnPageLoad();
                }
				
            },
            initVariables: function() {
                 cache = {
                    $document : $(document),
                    $homepageCarousel : $('#homepageCarousel'),
                    $pageContainer: $('.page-container'),
                    $truncated: $('.truncated')
                }
            },
            initEvents: function() {
                $(document).on('click','.btnShareEmail',globalCustomComponents.submitShareEmailMyDeal);
                cache.$document.on('click','#category', store.openMobileLeftNavigationModal);
                cache.$document.on('click', '#dealModal', store.setDealModalID)
                cache.$document.on('click', '.accordion-toggle', function() {
                    if($(this).hasClass('minus-icon')){
                        $(this).removeClass('minus-icon');
                    }
                    else{
                        $(this).addClass('minus-icon');
                    }
                });
               cache.$document.on('click','.deal-block',store.dealBlock);
               cache.$pageContainer.on('click', '.add-deals', function() {
                    if($(this).hasClass('minus-icon')){
                        $(this).removeClass('minus-icon');
                    }else{
                        $(this).addClass('minus-icon');
                    }
                    
                    var key = $(this).data('key'),
                        keyId = '#' + key,
                        target = $(this).parents('.page-container').find(keyId);
                        target.removeClass('collapsed').addClass('minus-icon');
                        target.parent('.panel-title').next().addClass('in');
                });
               cache.$document.on('click','#deals .accordion-menu',store.accordionMenu);
            },
            accordionMenu: function(e){
                e.stopPropagation();
                if($(this).hasClass('collapsed')) {
                    $(this).removeClass('collapsed');
                    $(this).next('ul').addClass('in');
                } else {
                    $(this).addClass('collapsed');
                    $(this).next('ul').removeClass('in');
                }
            },
            setDealModalID: function(){
            	$("input[name=selectedDealID]").val($(this).data('dealval'));
            },
            dealBlock:function(){                 
                var $imgURL=$(this).data('url'),
                    $name = $(this).data('name'),
                    $validity = $(this).data('validity'),
                    $desc = $(this).data('desc'),
                    $shopNow = $(this).data('shopnow'),
                    $type = $(this).data('type');
                $('#modalContent').find('#imgURL img').attr('src',$imgURL);
                $('#modalContent').find('#name').text($name);
                $('#modalContent').find('#validity span').text($validity);
                $('#modalContent').find('#desc').text($desc); 
                $('#modalContent').find('.shop-now').attr('href',$shopNow);
                if($type==='INSTORE'){
                    $('#modalContent').find('.shop-now').hide();
                    $('#modalContent').find('.instore').show();
                }
                if($type==='ONLINE'){
                    $('#modalContent').find('.shop-now').show();
                    $('#modalContent').find('.instore').hide();
                }
                if($type==='BOTH'){
                    $('#modalContent').find('.shop-now').show();
                    $('#modalContent').find('.instore').show();
                }
                
                // Social media
                var imageUrl = window.location.protocol+'//'+window.location.host+$imgURL,
                $twitter = $('#modalContent').find('#TWITTER');
                $twitter.attr('href', 'https://twitter.com/share?url='+window.location.href+'&amp;text='+$name+', '+$desc.replace('%', ' percent'));
                $twitter.attr('target','_blank');

                var $facebook =  $('#modalContent').find('#FACEBOOK');
                $facebook.attr('href', 'https://www.facebook.com/sharer/sharer.php?u='+window.location.href);
                $facebook.attr('target','_blank');

                var $pinterest =  $('#modalContent').find('#PINTEREST');
                $pinterest.attr('href', 'http://pinterest.com/pin/create/button/?url='+window.location.href+'&amp;media=' +imageUrl+ '&amp;description='+$name+', '+$desc);
                $pinterest.attr('target','_blank');
            },
            homepageCarousal:function(){
                cache.$homepageCarousel.find('.tail-hide').removeClass('tail-hide');
                var options = {
                    autoplay: false,
                    autoplaySpeed: 3000,
                    slidesToShow: 1,
                    infinite: true,
                    arrows: false,
                    dots: true,
                    draggable: true,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: true,
                    prevArrow: '<button class="slick-prev"></button>',
                    nextArrow: '<button class="slick-next"></button>',
                    responsive: [
                        {
                            breakpoint: 991,
                            settings: {
                                arrows: false
                            }
                        }
                    ]
                }
                globalCustomComponents.heroCarousel(cache.$homepageCarousel, options);
            },
            
          //this is used to expand plp categories link
            storeExpandText: function () {
                var viewPort = viewportDetect.lastClass;
                if(viewPort === 'small') {
                    var maxLength = 35;
                    cache.$truncated.each(function(){
                        var myStr = $(this).text();
                        if($.trim(myStr).length > maxLength){
                            var newStr = myStr.substring(0, maxLength);
                            var removedStr = myStr.substring(maxLength, $.trim(myStr).length+2);
                            $(this).empty().html(newStr);
                            $(this).append('<a href="javascript:void(0);" class="read-more"><span class="color-black">... </span>See More +</a>');
                            $(this).append('<span class="more-text">' + removedStr + '</span>');
                        }
                    });
                    cache.$truncated.find(".read-more").on('click', function(){
                        $(this).siblings(".more-text").toggle();
                        $(this).remove();
                    });
                }
            },
            openMobileLeftNavigationModal: function(){
                $('#deals .left-menu-container').removeClass('show-desktop');
            }
        };

        $(function() {
            store.init();
        });
    });