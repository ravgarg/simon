<div class="style-deals common-heading">
	<div class="row">
		<h2 class="heading major">Style Deals</h2>
		<div class="style-items col-xs-12 col-md-4">
			<div class="style-content-container">
				<div class="style-container ">
					<a target="_self" href="">
						<picture>
							<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/logo-slot@2x.png">
							<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/logo-slot.png">
						</picture>
					</a>
					<h5 class="major"><a href="#">Enjoy up to 60% off</a></h5>
					<p class="margin-btm"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></p>
					<p>Sale Ends 08/09/17</p>
				</div>
			</div>
		</div>
		<div class="style-items col-xs-12 col-md-4">
			<div class="style-content-container">
				<div class="style-container ">
					<a target="_self" href="">
						<picture>
							<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/logo-slot1@2x.png">
							<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/logo-slot1.png">
						</picture>
					</a>
					<h5 class="major"><a href="#">Enjoy up to 60% off</a></h5>
					<p class="margin-btm"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></p>
					<p>Sale Ends 08/09/17</p>
				</div>
			</div>
		</div>
		<div class="style-items col-xs-12 col-md-4">
			<div class="style-content-container">
				<div class="style-container">
					<a target="_self" href="">
						<picture>
							<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/logo-slot2@2x.png">
							<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/logo-slot2.png">
						</picture>
					</a>
					<h5 class="major"><a href="#">enjoy up to 60% off</a></h5>
					<p class="margin-btm"><a href="#">Lorem Ipsum is simply dummy text of the printing and typesetting industry</a></p>
					<p>Sale Ends 08/09/17</p>
				</div>
			</div>
		</div>
	</div>
</div>
	