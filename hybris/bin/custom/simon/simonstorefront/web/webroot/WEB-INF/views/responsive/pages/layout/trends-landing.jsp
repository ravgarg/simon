<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
<div class="analytics-pageContentLoad" data-analytics-pagetype="trend-landing" data-analytics-subtype="trend-landing" data-analytics-contenttype="informational">
	<div class="container">
		<div class="row">
	
			<div class="col-md-12 breadcrumbs hide-mobile">
				<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a> <span
					class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
			</div>
	
			<cms:pageSlot position="csn_leftNavigationForTrend" var="feature"
				element="div" class="account-section-content">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<div class="col-md-9 analytics-rightContainer"  data-analytics-pagelisttype="trend-landing" data-analytics-pagelistname="${cmsPage.name}">
				
				<div class="page-container trends-landing-page">
					<h1 class="page-heading major">
						<cms:pageSlot position="csn_HeadingSlotForTrend" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</h1>


					<div class="myfavorites-container">
					
						<div class="show-more">
							<cms:pageSlot position="csn_HeadingAndDescriptionSlotForTrend" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>						
						</div>
						<div class="owl-carousel" id="homepageCarousel">
							<cms:pageSlot position="csn_HeroSlotForTrend" var="feature">
								<div class="img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
						<div class="btn-category hide-desktop">							
							
							<button class="btn secondary-btn black"  data-toggle="modal" data-target="#trendsShop">
								<spring:theme code="text.trend.browse.categories" />
							</button>

						</div>
						
						<div class="current-trends">
							<cms:pageSlot position="csn_ManagedSpotsSlotForTrend"
								var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</div>
					</div>
						
				</div>
			</div>
		</div>
	</div>
	
	<div class="social-shop">
		<div class="container">
			<div class="row">
				<div class="social-shop-containers  col-xs-12 col-md-8">
					<div class="row">
						<div id="carousalFooter">
							<cms:pageSlot position="csn_SocialShopSlotForTrend"
								var="feature">
								<div class="social-shop-content item col-xs-12 col-md-4">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<cms:pageSlot position="csn_SocialShopTextSlotForTrend"
					var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>
	</div>
	<div>
		<cms:pageSlot position="csn_TextContentSlot" var="feature"
			element="div" class="span-24 section5 cms_disp-img_slot">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
	
	
	<div class="modal fade" id="trendsShop" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title"><spring:theme code="text.trend.browse.categories" /></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<cms:pageSlot position="csn_leftNavigationForTrend" var="feature"
						element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>		
	</div>
</div>	
</template:page>




