/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define('pages/orderConfirmation',['jquery','checkoutShipping', 'ajaxFactory','hbshelpers', 'handlebars', 'utility','viewportDetect','globalCustomComponents','analytics'],
    function($, checkoutShipping,ajaxFactory,hbshelpers, handlebars, utility,viewportDetect,globalCustomComponents,analytics) {
        'use strict';
        var cache;
        var orderConfirmation = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.initAjax();
                this.checkoutCreateAccount();
                this.checkoutOrderSummaryAccordian();
                utility.floatingLabelsInit();
            },
            initVariables: function() { 
                cache = {
                    $document: $(document),
                    $priceContainer : $('.order-summary-price-container-confirm')
                }
            },
            initAjax: function() {},
            initEvents: function() {
                cache.$document.on('click','.btn-create-account button.btn-crt-account',orderConfirmation.accountCreateOnOrderConfirmation);
                cache.$document.on('click','.viewReturnPolicy',utility.openReturnPrivacyPolicyModal);
            },
            accountCreateOnOrderConfirmation : function () {
                utility.floatingLabelsInit();
                if($('.checkoutCreateAccount').parsley().validate() === false){
                    return false;
                }
                else{
                    if ($("input[name=skip]").val() === "true") {
                        var formData = {
                            "CSRFToken": $("input[name=CSRFToken]").val(),
                            "firstName": $("input[name=firstName]").val(),
                            "lastName": $("input[name=lastName]").val(),
                            "zip": $("input[name=zip]").val(),
                            "email": $("input[name=email]").val(),
                            "country": $("input[name=country]").val(),
                            "ordercode": $("input[name=ordercode]").val(),
                            "pwd" : $('#password').val()
                        }
                        var objectOptions = {
                            'methodType': 'POST',
                            'dataType': 'JSON',
                            'methodData': formData,
                            'url': '/checkout/create-account',
                            'isShowLoader': true,
                            'cache': true
                        }
                        $(".confirmation-create-account").hide();
                        $('#showLoader').addClass("loader");
                        ajaxFactory.ajaxFactoryInit(objectOptions, function (response) {
                            $('#showLoader').removeClass("loader");
                            if(response === "OC_Error_1"){
                                $(".account-already-exist").removeClass('hide');
								analytics.analyticsAJAXFailureHandler('orderconfirmation_signin','Registration failed','CE');
                            }
                            if(response === "OC_Error_2"){
                                $(".account-created-error").removeClass('hide');
								analytics.analyticsAJAXFailureHandler('orderconfirmation_signin','Duplicate email registration failed','CE');
                            }
                            if(response === "OC_SUCCESS_3"){
								var $data = {
										formname : 'registration_form_page',
										formnametype : 'registration_form',
										type : 'registration',
										stage : 'completion',
										satelliteTrack : 'registration'
									};
								analytics.analyticsAJAXSuccessHandler($data);
                                $(".account-created-heading").removeClass('hide');
                            }
							
                        }, '','orderconfirmation_signin'); 
                    }
                }

            },
            checkoutCreateAccount : function () {
                if ($("input[name=skip]").val() === "false") {
                    var formData = {
                        "CSRFToken": $("input[name=CSRFToken]").val(),
                        "firstName": $("input[name=firstName]").val(),
                        "lastName": $("input[name=lastName]").val(),
                        "zip": $("input[name=zip]").val(),
                        "email": $("input[name=email]").val(),
                        "country": $("input[name=country]").val(),
                        "ordercode": $("input[name=ordercode]").val(),
                    }
                    var objectOptions = {
                        'methodType': 'POST',
                        'dataType': 'JSON',
                        'methodData': formData,
                        'url': '/checkout/create-account',
                        'isShowLoader': true,
                        'cache': true
                    }
                    $('#showLoader').addClass("loader");
                    ajaxFactory.ajaxFactoryInit(objectOptions, function (response) {
                        $('#showLoader').removeClass("loader");
                        if(response === "OC_Error_1"){
                            $(".account-already-exist").removeClass('hide'); 
                        }
                        if(response === "OC_Error_2"){
                            $(".account-created-error").removeClass('hide');
                        }
                        if(response === "OC_SUCCESS_3"){
                            $(".account-created-heading").removeClass('hide');
                        }
                    });
                }
            },
            checkoutOrderSummaryAccordian: function() {
                if (cache.$document.width() < 992) {
                    cache.$priceContainer.addClass('collapse');
                    cache.$document.on('click', '.view-bag-summary', function() {
                        $(this).next('div').collapse('toggle');
                    });
                    cache.$priceContainer.on('show.bs.collapse hidden.bs.collapse', function() {
                        $(this).prev('.view-bag-summary').toggleClass('minus-icon');
                    })
                }
            }
            
        }

        $(function() {
            orderConfirmation.init();
			if(viewportDetect.lastClass === 'large'){
				globalCustomComponents.rightRailAnchored();
			}
        });

        return orderConfirmation;
    });