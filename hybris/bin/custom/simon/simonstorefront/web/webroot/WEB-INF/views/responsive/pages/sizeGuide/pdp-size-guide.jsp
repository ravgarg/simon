<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<div class="modal-header">
    <h5 class="modal-title">Size Guide</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
</div>
<div class="size-guide">
    <cms:pageSlot position="CSN_GSG_MainAreaSection" var="feature">
    <cms:component component="${feature}"/>
    </cms:pageSlot>
</div>
<div class="modal-footer clearfix">
    <button class="btn pull-left" data-dismiss="modal">Close</button>
</div>
