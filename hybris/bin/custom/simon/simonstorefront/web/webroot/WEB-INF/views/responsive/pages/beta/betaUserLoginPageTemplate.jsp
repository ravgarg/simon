<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/mobile/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<template:page pageTitle="${pageTitle}">
<div class="container create-account analytics-pageContentLoad" data-analytics-pagetype="beta-login" data-analytics-subtype="beta-login" data-analytics-contenttype="informational">
    <div class="container">	
		<c:if test="${not empty errorMessage}">
				<div class="error-msg analytics-formSubmitFailure"> <spring:theme code="${errorMessage}" />
				</div>
		</c:if>  	
	</div>
	<cms:pageSlot position="csn_HeroBetaSlot" var="comp" element="div" class="shopperRegistrationPage">
		<cms:component component="${comp}" element="div" class="shopperRegistrationPage-component"/>
	</cms:pageSlot>
<div class="create-account-container privacy-wall">
   	<form action="/beta-login" method="post" modelAttribute="loginData" class="sm-form-validation bt-flabels js-flabels captchaValidation analytics-formValidation" data-analytics-type="betalogin" data-analytics-eventtype="betalogin" data-analytics-eventsubtype="betalogin" data-analytics-formtype="betalogin_form" data-analytics-formname="betalogin_form" data-satellitetrack="betalogin" data-parsley-validate="validate">
       <div class="row">
			<div class="col-xs-12 col-md-12">
				<div class="sm-input-group">
					<label class="field-label" for="newCustomerEmail"><spring:theme code="register.new.customer.email.placeHolder" /> <span class="field-error" id="error-j_guestEmailId"></span></label>
					<input type="email" class="form-control" id="newCustomerEmail" placeholder="Email Address" name="j_username" data-parsley-errors-container="#error-j_guestEmailId" type="email" data-parsley-type="email" data-parsley-error-message="<spring:theme code="register.new.customer.email.error" />" required>
				</div>
			</div>
			<c:if test="${disablePassword eq false}">
				<div class="col-xs-12 col-md-12">
					<div class="sm-input-group">
						<label class="field-label" for="user-pswd"><spring:theme code="register.new.customer.pwd.placeHolder" /> <span class="field-error" id="error-j_pswd"></span></label>
						<input type="password" autocomplete="off" class="form-control" placeholder="Password" name="j_password" data-parsley-errors-container="#error-j_pswd" 
							id="user-pswd"
							data-parsley-required-message="<spring:theme code="text.login.field.error.message.password" />" 
							data-parsley-required 
							data-parsley-pwdstrength-message="<spring:theme code="text.login.field.error.message.valid.password" />"
							data-parsley-pwdstrength />
					</div>
				</div>
			</c:if>
			
		</div>
		<div class="row">				
			<div class="col-xs-12 col-md-6 captchaContainer">
				<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
				<div id="captcha" class="has-error"><spring:theme code="register.new.customer.captcha.error.msg" /></div>
			</div>
			
			<div class="terms-conditions col-md-6 col-xs-12 small-text hidden-xs">
				<spring:theme code="register.new.customer.ssl.security" />
			</div>	
		</div>
		<div class="row">
			<div class="submit-btn col-md-6 col-xs-12 analytics-genericLink" data-analytics-eventsubtype="beta-login" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|beta-login" data-analytics-linkname="beta-login">
				<button class="btn analytics-genericLink" data-analytics-eventsubtype="betalogin" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|beta-login" data-analytics-linkname="betaloginclick"><spring:theme code="register.new.customer.submit.btn" /></button>
			</div>
			<div class="terms-conditions col-md-6 col-xs-12 small-text visible-xs">
				<spring:theme code="register.new.customer.ssl.security" />
			</div>	
		</div>		
		<input type="hidden" name="CSRFToken" value="${CSRFToken}">
      </form>
     </div>
    </div>
</template:page>