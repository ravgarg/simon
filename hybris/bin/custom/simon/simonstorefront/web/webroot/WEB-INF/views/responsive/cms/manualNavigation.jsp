<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<!-- Manual Left Navigation Starts -->

<c:forEach items="${topNavComponent}" var="L1childnodes">
	<c:forEach items="${L1childnodes}" var="L1childnode">
		<c:set value="${L1childnode.key}" var="childNodeValue" />
		<!-- L1 NODE -->
		<c:set value="${L1childnode.value}" var="childNodesL2" />
		<!-- LIST OF L2 N L3 NODES -->
		<c:choose>
			<c:when test="${fn:length(childNodesL2) gt 0}">
				<!-- check if L1 has L2 NODES -->
				<li class="has-level-1"> 
					<a href="#${childNodeValue.uid}" class="accordion-menu collapsed" data-toggle="collapse">${childNodeValue.name}</a>
					<ul class="level-1 collapse" id="${childNodeValue.uid}">
						<c:set var="sizeOfL3" value="1"></c:set>
						<c:set var="childNodes" value="0"></c:set>
						<c:forEach items="${childNodesL2}" var="childnodeL2">
							<!-- A PARTICULAR L2 OR L3 NODE -->
							<c:set var="l2URL"><c:url value="${childnodeL2.url}" /></c:set>
							<c:choose>
								<c:when test="${childnodeL2.target eq 'newWindow'}">
							    	<c:set var="childnodeL2Target" value="_blank" />
								</c:when>
							    <c:otherwise>
									<c:set var="childnodeL2Target" value="_self"/>
								</c:otherwise>
							</c:choose> 
							<c:choose>
								<c:when test="${childnodeL2.isIsL2()  and childnodeL2.getL2Size() gt 0}">
									<!-- check if current node is L2 and has L3 Nodes -->
									<c:set var="childNodes" value="${childnodeL2.getL2Size()+1}"></c:set>
									<li class="has-level-2">
										<a target="${childnodeL2Target}" href="#${childnodeL2.uid}" class="accordion-menu collapsed" data-toggle="collapse">${childnodeL2.name}</a>
										<ul class="level-2 collapse" id="${childnodeL2.uid }"></ul>
									</li>
								</c:when>
								<c:when test="${!childnodeL2.isIsL2() }">
									<!-- check if current Node is NOT L2 -->
									<c:choose>
										<c:when test="${sizeOfL3 eq childNodes}">
											<!-- check if current Node is last L3 node of L2 and its not View All -->
											<li><a target="${childnodeL2Target}" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(childnodeL2.name))}" href="${l2URL}">${childnodeL2.name}</a></li>
									    </c:when>
										<c:otherwise>
											<!-- L3 NODE -->
											<c:set var="sizeOfL3" value="${sizeOfL3+1}"></c:set>
											<li><a target="${childnodeL2Target}" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(childnodeL2.name))}" href="${l2URL}">${childnodeL2.name}</a></li>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<!-- if no L3 EXIST FOR L2 NODE -->
									<li><a target="${childnodeL2Target}" href="${l2URL}" class="collapsed" >${childnodeL2.name}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
				</li>
			</c:when>
			<c:otherwise>
				<!-- IF NO L2 EXIST FOR PARTICULAR L1 NODE -->
				<c:choose>
					<c:when test="${childNodeValue.target eq 'newWindow'}">
				    	<c:set var="childNodeValueTarget" value="_blank" />
					</c:when>
				    <c:otherwise>
						<c:set var="childNodeValueTarget" value="_self" />
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${childNodeValue.isLockVisible}">
						<li><a target="${childNodeValueTarget}" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(childNodeValue.name))}" href="${childNodeValue.url}">${childNodeValue.name}
								<span> &nbsp; </span>
						</a></li>
					</c:when>
					<c:when test="${childNodeValue.uid == pageId}">
						<li class="active"><a target="${childNodeValueTarget}" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(childNodeValue.name))}" href="${childNodeValue.url}">${childNodeValue.name}
						</a></li>
					</c:when>
					<c:otherwise>
						<li><a target="${childNodeValueTarget}" data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(childNodeValue.name))}" href="${childNodeValue.url}">${childNodeValue.name}</a>
						</li>
					</c:otherwise>
				</c:choose>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</c:forEach>
<!-- Manual Left Navigation Ends -->