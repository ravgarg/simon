<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String" %>
<%@ attribute name="action" required="true" type="java.lang.String" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<div class="col-xs-12 col-sm-5 col-sm-push-1 global-login-containers">
				<h3><spring:theme code="text.checkout.login.guest.title"/></h3>
				<p><spring:theme code="text.checkout.login.guest.description"/></p>
             
              <form:form action="${action}" method="post" commandName="guestForm">
                  
                     <ycommerce:testId code="guest_Checkout_button">
                           <div class="form-group">
                                  <button type="submit" class="btn guest-btn analytics-genericLink" data-analytics-eventsubtype="checkoutlogin" data-analytics-linkplacement="checkoutlogin" data-analytics-linkname="checkout_as_guest"><spring:theme code="${actionNameKey}"/></button>
                           </div>
                     </ycommerce:testId>
              
       </form:form>

       </div>





	