/**
 * @description global.js 
 */
require.config({
    waitSeconds: 140,
    paths: {
        // Libraries
        'bootstrap': '../bower_components/bootstrap/dist/js/bootstrap.min',
        'require': '../bower_components/requirejs/require',
        'jquery': '../bower_components/jquery/jquery.min',
        'parsley': '../bower_components/parsleyjs/dist/parsley.min',
        'handlebars': '../bower_components/handlebars/handlebars.min',
        'slickCarousel': 'helpers/slick.min',
        'customScrollbar': 'helpers/scrollBar',
        'picturefill': '../bower_components/picturefill/dist/picturefill.min',
        'imgZoom': '../bower_components/jquery-zoom/jquery.zoom.min',
        'masking':'../bower_components/jquery-mask-plugin/dist/jquery.mask.min',
        'bootstrap-select': '../bower_components/bootstrap-select/dist/js/bootstrap-select.min',
        //templates folder
        'templates': '../compiledTemplates',

        'async': '../bower_components/requirejs-plugins/src/async',

        // custom helpers
        'ajaxFactory': 'helpers/ajax.factory',
        'viewportDetect': 'helpers/viewport-detect',
        'utility': 'helpers/global-utility',
        'hbshelpers': 'helpers/handlebars-helpers',
        'matchMedia': 'helpers/matchMedia',
        'typeahead': 'helpers/typeahead',
        

        //mobile mega menu jQuery Plugin
        'mobileMegaMenu': 'helpers/mobile-mega-menu',

        //global components
        'globalFooter': 'components/global-footer',
        'globalHeader': 'components/global-header',
        'globalCustomComponents': 'components/global-custom-components',

        'cartcheckoutordesummary' : 'components/cart-checkout-order-summary',

        'qtySelector': 'components/global-qty-selector',


        // global minibag
        'globalMinibag': 'components/global-minibag',
        'scrollbar': '../bower_components/jquery-custom-scrollbar/jquery.custom-scrollbar',
        
        //megaMenu
        'globalMegaMenu': 'components/global-mega-menu',
        'globalTilesCarousel': 'components/global-tiles-carousel',

        // pdp components
        'productImagesCarousel': 'components/pdp-product-carousel-zoom',
        'pdpOverview': 'components/pdp-overview',
        'pdpAddtocart': 'components/pdp-addtocart',
        'hybrisJS' : 'hybrisconvertaddon',

        // checkout components
        'checkoutShipping': 'components/checkout-shipping',
        'orderConfirmation': 'components/orderConfirmation',
        'checkoutReviewOrder': 'components/checkout-revieworder',
        'spreedly' : 'iframe-v1.min',
        'myaccountProfileUpdate': 'components/myaccount-profile-update',
        'myDesigners': 'components/myDesigners',
        'login': 'components/login',
        'myaccountPaymentInfo': 'components/myaccount-paymentinfo',
        
        //my account components
        'myaccountManageAddress': 'components/myaccount-manageaddress',
        'myaccountSizes': 'components/myaccount-mysizes',
        'myaccountDesignersAZ': 'components/myaccount-designers-az',
        'myaccountOrderHistory': 'components/myaccount-order-history',
        'myPremiumOutlets' : 'components/myPremiumOutlets',
		
        //load more
        'loadMore': 'components/loadmore',

        // Favorite
        'favorite': 'components/global-favorite',
        
        // analytics
        'analytics' : 'analytics/analytics',
		
		//show-more content
		'showMore': 'components/global-show-more'
        
    },
    shim: {
        // define JS dependencies here, plugins (non-amd compliant) need this shim config
        'typeahead': {
            deps: ['bootstrap']
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'globalCustomComponents': {
            deps:['jquery','slickCarousel']
        },
        'matchMedia': {
            deps: ['jquery']
        },
        'globalMegaMenu': {
            deps: ['mobileMegaMenu']
        },
        'globalMinibag': {
            deps: ['jquery']
        },
        'viewportDetect': {
            deps: ['matchMedia']
        },
        'mobileMegaMenu': {
            deps: ['jquery']
        },
        'hybrisJS': {
            deps: ['jquery']
        },
        'handlebars': {
            deps: ['jquery']
        },
        'require': {
            deps: ['jquery']
        },
        'parsley': {
            deps: ['jquery']
        },
        'imgZoom': {
            deps: ['jquery']
        },
        'slickCarousel': {
            deps: ['jquery']
        },
        'scrollbar': {
            deps: ['jquery']
        },
        'myPremiumOutlets':{
        	deps: ['jquery']
        },
        'spreedly': {
            deps: ['jquery']
        },
        'analytics':{
        	deps: ['jquery']
        },
        'bootstrap-select':{
            deps:['bootstrap']
        }
        
    }
});


/**
 * @function Global Module Loader
 * @description : use this for any global functionality
 */
define('global', ['jquery', 'bootstrap', 'utility', 'favorite', 'parsley', 'globalMegaMenu', 
      'mobileMegaMenu', 'globalFooter', 'globalHeader', 'globalTilesCarousel','globalMinibag','hybrisJS','showMore','analytics','bootstrap-select'],
    function($, bootstrap, utility, favorite,showMore) {
        'use strict';
        var simonGlobal = {
            init: function() {
                this.initVariables();
                this.initAjax();
                this.initCurrentPage();
                utility.initModal();
                utility.imgToSvg();
                utility.floatingLabelsInit();
                utility.selectedDropDownColor();
                this.initEvents();
                this.tooltip();
                this.accordion();
                this.popOver(); 
                favorite.init();
                this.loginPopup();
            },
            
            initCurrentPage: function() {
                var pages = $('script[data-page]');
                pages.each(function() {
                    var el = $(this);
                    var list = el.attr('data-page').split(' ');
                    require(list,function(){
                    	setTimeout(function() {
	                    	try {
		            			var s = document.createElement('script');
		                        s.src = document.getElementById('analyticsURL').value;
		                        s.type = "text/javascript";
		                        s.async = false;     
		                        document.getElementsByTagName('head')[0].appendChild(s);
		            		}
		            		catch(err) {
		            		    return;
		            		}
                    	},2000);
                    });
                });
            },
            initVariables: function() {},
            initEvents: function() {
                //Border For Dropdown when it is in open state or value has been selected

                $('html,body').on('click','select',function(){
                    $(this).addClass("selectBorder");
                });
                
                $('html,body').on('blur', 'select',function(){
            		$(this).removeClass("selectBorder");
                });

            },
            initAjax: function() {},
            tooltip: function() {
                $('.tooltip-init').tooltip();
            },
            popOver: function(){
                $('.popover-init').tooltip({"trigger":"click"});
            },
            accordion: function() {
                var selectHeading = $('.panel > .panel-collapse');
                $(function ($) {
                    selectHeading.on('show.bs.collapse hidden.bs.collapse', function () {
                        $(this).prev().find('.glyphicon').toggleClass('glyphicon-plus glyphicon-minus');
                    })
                });
            },
            loginPopup: function() {
                //Login Success Popup
                var currentUrl = window.location.href;
                currentUrl = currentUrl.split('/en');
                var loginModal = $('#globalPopup');
                if(loginModal.length) {
                    loginModal.modal('show');
                    loginModal.on('click','.btn-redirect', function() {
                        var dataUrl = $(this).data('url');
                        window.location.href = currentUrl[0] + dataUrl;
                    });
                }
            }
        }

        $(function() {
            simonGlobal.init();
        });

        return simonGlobal;
    });