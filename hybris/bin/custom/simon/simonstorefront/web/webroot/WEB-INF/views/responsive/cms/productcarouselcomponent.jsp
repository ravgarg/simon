<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<input type="hidden" data-fav-url="true"
	value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
<div class="product-recomandation product-container">
<c:if test="${not empty searchPageData and not empty searchPageData.results}">
	<c:choose>
	<c:when test="${not empty title}">
		<c:set var="carouselTitle" value="${title}"/>
	</c:when>
	<c:otherwise>
		<c:set var="carouselTitle"><spring:theme code="text.product.recommendations.title" /></c:set>
	</c:otherwise>
	</c:choose>
	<div class="global-tile-carousel carousel-section">
		<div class="clearfix">
			<div class="col-md-12">
				<!-- Nav tabs -->
					<ul class="nav nav-tabs recommendation-heading" role="tablist">
						<c:choose>
							<c:when test="${pageType eq 'PRODUCT' }">
								<li><cms:pageSlot position="csn_RecommendedProductSlot"
										var="feature" element="li">
										<cms:component component="${feature}" />
									</cms:pageSlot></li>
							</c:when>
							<c:otherwise>
								<li role="presentation" class="active major"><a
									href="#youMayLike" role="tab" data-toggle="tab">${carouselTitle}</a></li>
							</c:otherwise>
						</c:choose>
					</ul>
					<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="youMayLike">
							<div id="globalTilesCarousel" class="analytics-productList analytics-productRecommended" data-satellitetrack="product_listing" 
								data-analytics-eventtype="product_click"  
								data-analytics-eventsubtype='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|recommended_products'
								data-analytics-listype='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|recommended_products'
								data-analytics-ecommerce-clickaction="recommended_prod_click"
								data-analytics-listname="recommended_products"
								data-analytics-linkplacement='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>'>

								<c:forEach items="${searchPageData.results}" var="product"
									varStatus="status">
									<c:if test="${fn:length(product.variantOptions)>0}">
										<c:set var="InStockProductIdentified" value="false" />
										<c:set var="variantProduct"
											value="${product.variantOptions.get(0)}" />
										 <c:set var="altText" value="${variantProduct.baseProductDesignerName}&nbsp;${variantProduct.colorName}&nbsp;${variantProduct.baseProductName}"/> 
										<c:if test="${InStockProductIdentified eq false}">
											<c:set var="InStockProductIdentified"
												value="${variantProduct.inStockFlag}" />
												
											<c:if test="${InStockProductIdentified}">
												<div class="item-tile col-md-3 analytics-item" 
													data-analytics-productposition="${status.count}" 
													data-analytics-productoutofstock='<c:choose><c:when test="${InStockProductIdentified eq false}">out_of_stock</c:when><c:otherwise>in_stock</c:otherwise></c:choose>' 
													data-analytics-linkname="${fn:escapeXml(variantProduct.baseProductName)}">
												
												<div class="item">
													<input type="hidden" value="${variantProduct.baseProductCode}" class="base-product-code" /> 
													<input type="hidden" value="${variantProduct.baseProductName}" class="base-product-name" />
													<c:if test="${not empty categoryName}">
														<input type="hidden" value="${categoryName}" class="base-product-category" />
													</c:if> 
													<!-- Fav-icon For anonymous user -->
													<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
														<a href="javascript:void(0);"
															data-fav-product-code="${variantProduct.baseProductCode}"
															data-fav-name="${variantProduct.baseProductName}" data-fav-type="PRODUCTS"
															class="fav-icon login-modal openLoginModal"></a>
													</sec:authorize>
													<!-- Fav-icon For logged in user -->
													<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
													<div class="analytics-addtoFavorite" data-analytics-pagestyle="plp" data-analytics-pagetype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product" data-analytics-component="product">
														<a href="javascript:void(0);"
															class="mark-favorite fav-icon <c:if test='${product.favorite}'>marked</c:if>">
															<i class="tooltip-init" data-placement="bottom"
															data-html="true" data-toggle="tooltip"
															title="<spring:theme code="text.pdp.addedtofavorite" />"></i>
														</a>
													</div>
													</sec:authorize>
													<a
														href="<c:url value='${variantProduct.baseProductUrl}?colorID=${variantProduct.colorID}'/>">
														<picture> <c:choose>
															<c:when
																test="${fn:length(variantProduct.variantImage) > 0}">

																<c:forEach var="media"
																	items="${variantProduct.variantImage}">
																	<c:choose>
																		<c:when test="${media.format eq 'Desktop'}">
																			<c:choose>
																				<c:when test="${not empty media.url}">
																					<c:set var="desktopImageUrl" value="${media.url}" />
																				</c:when>
																				<c:otherwise>
																					<c:set var="desktopImageUrl"
																						value="/_ui/responsive/simon-theme/images/no-image.png" />
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<c:if test="${ empty media.format}">
																				<c:set var="desktopImageUrl"
																					value="/_ui/responsive/simon-theme/images/no-image.png" />
																			</c:if>

																		</c:otherwise>
																	</c:choose>
																	<c:choose>
																		<c:when test="${media.format eq 'Mobile'}">
																			<c:choose>
																				<c:when test="${not empty media.url}">
																					<c:set var="mobileImageUrl" value="${media.url}" />
																				</c:when>
																				<c:otherwise>
																					<c:set var="mobileImageUrl"
																						value="/_ui/responsive/simon-theme/images/no-image.png" />
																				</c:otherwise>
																			</c:choose>
																		</c:when>
																		<c:otherwise>
																			<c:if test="${ empty media.format}">
																				<c:set var="mobileImageUrl"
																					value="/_ui/responsive/simon-theme/images/no-image.png" />
																			</c:if>

																		</c:otherwise>
																	</c:choose>
																</c:forEach>
																<!--[if IE 9]><video class="hide img-responsive"><![endif]-->
																<source media="(max-width: 767px)" srcset="${mobileImageUrl}">
							            						<source media="(min-width: 768px)" srcset="${desktopImageUrl}">
																<!--[if IE 9]></video><![endif]-->
																<img class="img-responsive"
																 alt="${altText}"
																 style=""
																	src="${desktopImageUrl}">
															</c:when>
															<c:otherwise>
																<source media="(max-width: 767px)"
																	srcset="/_ui/responsive/simon-theme/images/no-image.png">
																<source
																	media="(min-width: 768px) and (max-width: 991px)"
																	srcset="/_ui/responsive/simon-theme/images/no-image.png">
																<!--[if IE 9]></video><![endif]-->
																<img class="img-responsive"
																 alt="${altText}"
																  style=""
																	src="/_ui/responsive/simon-theme/images/no-image.png">
															</c:otherwise>
														</c:choose> </picture>
													</a>
													<c:choose>
														<c:when test="${not empty variantProduct.baseProductDesignerName  && not empty variantProduct.baseProductRetailerName && (fn:toUpperCase(fn:trim(variantProduct.baseProductDesignerName))  eq fn:toUpperCase(fn:trim(variantProduct.baseProductRetailerName))) }">
															<c:choose>
																<c:when test="${not empty variantProduct.baseProductRetailerURL}">
																 <div class="item-name gray-color analytics-retailer" data-satellitetrack="product_listing" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}"><a href="${variantProduct.baseProductRetailerURL}" >${variantProduct.baseProductRetailerName}</a></div> 
																</c:when>
																<c:otherwise>
																<div class="item-name gray-color analytics-retailer" data-satellitetrack="product_listing" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}">${variantProduct.baseProductRetailerName}</div>
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															<c:choose>
																<c:when test="${not empty variantProduct.baseProductDesignerURL}">
															  		 <div class="item-name gray-color analytics-designer" data-analytics-designer="${fn:toLowerCase(variantProduct.baseProductDesignerName)}"><a href="${variantProduct.baseProductDesignerURL}">${variantProduct.baseProductDesignerName}</a></div>
															  		 </c:when>
																   <c:otherwise>
																    <div class="item-name gray-color analytics-designer" data-analytics-designer="${fn:toLowerCase(variantProduct.baseProductDesignerName)}">${variantProduct.baseProductDesignerName}</div>
																   </c:otherwise>
															   </c:choose>
															   <c:choose>
															   <c:when test="${not empty variantProduct.baseProductRetailerURL}">
															  	 <div class="item-name gray-color analytics-retailer" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}"><a href="${variantProduct.baseProductRetailerURL}" >${variantProduct.baseProductRetailerName}</a></div>
															   </c:when>
															   <c:otherwise>
															    <div class="item-name gray-color analytics-retailer" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}">${variantProduct.baseProductRetailerName}</div>
															   </c:otherwise>
															   </c:choose>
														    
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${fn:length(product.variantOptions) > 0}">
															<c:set var="colorCount"
																value="${fn:length(product.variantOptions)}" />
														</c:when>
														<c:otherwise>
															<c:set var="colorCount" value="" />
														</c:otherwise>
													</c:choose>
													<div class="item-retailer">
														<a href="">${colorCount} Color</a>
													</div>
													<!-- Color Owl Carousal Starts -->
													<div class="color-switches-carousal">
														<c:forEach items="${product.variantOptions}" var="variant"
															varStatus="status">
															<div class="item">
																<div class="color-palette clearfix">
																	<c:choose>
																		<c:when test="${variant.inStockFlag}">
																			<a
																				href="<c:url value='${variant.baseProductUrl}?colorID=${variant.colorID}'/>"
																				class="color-switches"
																				data-color-name="${variant.colorName}"
																				style="background: url('${variant.swatchImage}');"></a>
																		</c:when>
																		<c:otherwise>
																			<a
																				href="<c:url value='${variant.baseProductUrl}?colorID=${variant.colorID}'/>"
																				class="color-switches not-available"
																				data-color-name="${variant.colorName}"
																				style="background: url('${variant.swatchImage}');"></a>
																		</c:otherwise>
																	</c:choose>


																</div>
															</div>
														</c:forEach>
													</div>
													<!-- Color Owl Carousal Ends -->

													<div class="item-color">
														<a
															href="<c:url value='${variantProduct.baseProductUrl}?colorID=${variantProduct.colorID}'/>">${variantProduct.baseProductName}</a>
													</div>
													<c:if test="${ not empty product.applicablePriceRange}">
														<div class="item-discrition">${product.applicablePriceRange}
														</div>
													</c:if>
													<c:if test="${ not empty product.plumPriceRange}">
														<div class="item-price">
															<span>${product.plumPriceRange}</span>
															<c:if
																test="${not  empty variantProduct.variantPrice['MSRP'].left and not  empty variantProduct.variantPrice['LIST'].left and not  empty variantProduct.variantPrice['SALE'].left}">
																<a href="javascript:void(0);" data-placement="top"
																	data-html="true" data-toggle="tooltip"
																	data-original-title="${product.msrpPriceRange} </br>${product.listPriceRange} </br>${product.salePriceRange}"
																	class="tooltip-init tooltip-icon"></a>
															</c:if>
														</div>
													</c:if>
													<c:if test="${ not empty product.percentageOffRange}">
														<div class="item-revised-price">
															${product.percentageOffRange}</div>
													</c:if>
												</div>
												</div>
											</c:if>
										</c:if>
									</c:if>
								</c:forEach>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</c:if>
</div>