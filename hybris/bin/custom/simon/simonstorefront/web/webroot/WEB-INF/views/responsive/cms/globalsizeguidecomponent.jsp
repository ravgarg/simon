<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="list-dropdown">
<form>
<select id="sizeChartCategories">
    <option value="default" selected hidden>SELECT A SIZE GUIDE CATEGORY</option>
    <c:forEach items="${mainheadings}" var="heading">
<c:if test="${not empty heading.title}">
	<option value="${heading.uid}">
	 ${heading.title}
   </option>  
</c:if>    
   <c:forEach items="${heading.subheadings}" var="subheading">
   <c:if test="${not empty subheading.title}">
   	<option value="${heading.uid}-${subheading.uid}" class="subheading">
       - ${subheading.title}
	</option>
	</c:if>  
    </c:forEach>
</c:forEach>
</select>
</form>
</div>

<div class="scroll-container default-skin clearfix" id="scrollContainer">
<c:forEach items="${mainheadings}" var="heading">
<div id="${heading.uid}">
<div class="size-guide-content">
<c:if test="${not empty heading.title}">
<h6>${heading.title}</h6>
</c:if>
<c:if test="${not empty heading.description}">
<p>${heading.description}</p>
</c:if>
<c:forEach items="${heading.subheadings}" var="subheading">
 <div id="${heading.uid}-${subheading.uid}">
        <div class="size-guide-content">
        <c:if test="${not empty subheading.title}">
           <h6>${subheading.title}</h6>
           </c:if>
           <c:if test="${not empty subheading.description}">
           <p>${subheading.description}</p>
           </c:if>
           </div>
           </div>
</c:forEach>
</div>
</div>
</c:forEach>
</div>