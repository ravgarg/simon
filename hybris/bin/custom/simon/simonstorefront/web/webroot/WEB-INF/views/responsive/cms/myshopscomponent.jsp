<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not empty accountPageNotificationUpdateKey}">
	<input type="hidden" id="errorMessage"
		value="<spring:theme code="${accountPageNotificationUpdateKey}" />&nbsp;
		       <spring:theme code="text.account.myStores.title" />" />
</c:if>
<c:if test="${not empty storeList}"><input type="hidden" data-fav-url="true" 
		value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />
	<c:choose>
		<c:when test="${alignment eq 'LeftAligned'}">
			<a href="#subMenuL1Id10" class="accordion-menu"
				data-toggle="collapse"><spring:theme code="${titleKey}" /></a>
			<ul class="level-1 my-stores collapse in" id="subMenuL1Id10">
				<c:forEach items="${storeList}" var="shop">
					<li><a href="${shop.contentPageLabel}">${shop.name}</a></li>
				</c:forEach>
			</ul>
		</c:when>
		<c:otherwise>
			<div class="my-store has-stores">
				<h2 class="page-sub-heading"><spring:theme code="${titleKey}" /></h2>
				<div class="current-trends clearfix analytics-featureStores" data-analytics-length="stores" data-analytics-storetype="mystores">
					<c:forEach items="${storeList}" var="shop">
						<c:choose>
							<c:when test="${not empty shop.backgroundImage}">
								<c:set var="backgroundImage" value="${shop.backgroundImage}" />
							</c:when>
							<c:otherwise>
								<c:set var="backgroundImage"
									value="/_ui/responsive/simon-theme/images/store-no-image-desktop.png" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.mobileBackgroundImage}">
								<c:set var="mobileBackgroundImage"
									value="${shop.mobileBackgroundImage}" />
							</c:when>
							<c:otherwise>
								<c:set var="mobileBackgroundImage"
									value="/_ui/responsive/simon-theme/images/store-no-image-mobile.png" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.logo}">
								<c:set var="logo" value="${shop.logo}" />
							</c:when>
							<c:otherwise>
								<c:set var="logo" value="" />
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty shop.mobileLogo}">
								<c:set var="mobileLogo" value="${shop.mobileLogo}" />
							</c:when>
							<c:otherwise>
								<c:set var="mobileLogo" value="" />
							</c:otherwise>
						</c:choose>
						<div class="store col-md-4 col-xs-12">
							<div class="store-content">
								<div class="box-background">
									<picture>
									<source media="(max-width: 767px)"
										srcset="${mobileBackgroundImage}">
									<source media="(min-width: 768px) and (max-width: 991px)"
										srcset="${backgroundImage}">
									<img class="img-responsive" alt="" style=""
										src="${backgroundImage}"> </picture>
								</div>
								<div class="fav-the-store add-to-favorites analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="retailer" data-analytics-linkplacement="retailer" data-analytics-component="stores">
									<input type="hidden" value="${shop.id}" class="base-code analytics-base-code" /> <input
										type="hidden" value="${shop.name}" class="base-name analytics-base-name" /> <a
										href="javascript:void(0);"
										class="mark-favorite fav-icon marked"><span class="hide"> fav</span></a>
								</div>
								<div class="center-content">
									<picture>
										<source media="(max-width: 767px)" srcset="${logo}">
										<source media="(min-width: 768px) and (max-width: 991px)"
											srcset="${mobileLogo}">
										<img class="img-responsive" alt="" style="" src="${logo}">
									</picture>

								</div>
								<div class="shop-now">
									<a href="${shop.contentPageLabel}" data-storeval="${shop.name}" class="analytics-genericLink analytics-storeClick" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="shop-now" data-analytics-linkname="shop-now"><spring:theme
											code="text.account.myStores.shopnow.text" /><span class="hide"> fav</span></a>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</c:if>