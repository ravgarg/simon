<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>
<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="clp-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="plp-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<jsp:include page="designer-listing-top-heading.jsp"></jsp:include>
			<jsp:include page="clp-heading-desc-and-links.jsp"></jsp:include>
			<jsp:include page="designer-listing-shop.jsp"></jsp:include>
			<jsp:include page="plp-tiles-container.jsp"></jsp:include>
		</div>
	</div>
</div>
