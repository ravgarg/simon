<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="main-menu mobile-mega-menu">
            <div class="mobile-sidebar-wrapper">
				<a href="#" class="active">
				<span class="shop">
					<img src="/_ui/responsive/simon-theme/icons/black-shop.svg">
				</span>
				<label> Shop </label>
				</a>
				<a href="#">
				<span class="login">
					<img src="/_ui/responsive/simon-theme/icons/black-login.svg">
				</span>
				<label> Login </label>
				</a>
				
				<a href="#">
				<span class="favor">
					<img src="/_ui/responsive/simon-theme/icons/black-favorites-light.svg">
				</span>
				<label> Favorites </label>
				</a>

				<a href="#">
				<span class="find-a-center">
					<img src="/_ui/responsive/simon-theme/icons/black-find-center.svg">
				</span>
				<label> Find a Center </label>
				</a>

				<a href="#">
				<span class="support">
					<img src="/_ui/responsive/simon-theme/icons/black-support.svg">
				</span>
				<label> Support </label>
				</a>
			</div>
			<nav >
              <ul>
                <li><a href="#" class="mymobilemenu">Stores</a>
                  <ul>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li>
                    	<a href="#" class="open-accordian">Privacy Policy</a>
                    	<div class="mobile-accordian collapse">
                    		<a href="#">Accordian Link 1</a>
                    		<a href="#">Accordian Link 2</a>
                    		<a href="#">Accordian Link 3</a>
                    		<a href="#">Accordian Link 4</a>
                    	</div>
                    </li>
                    <li><a  href="#">Additional Details</a></li>
                  </ul>
                </li>
                <li><a href="#" class="mymobilemenu">Designers</a>
                  <ul>
                    <li><a href="#">Help</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">What Ever</a></li>
                  </ul>
                </li>
                <li><a href="#" class="mymobilemenu">Trends</a>
                	<ul>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a  href="#">Additional Details</a></li>
                  </ul>
                </li>
                <li><a href="#" class="mymobilemenu">Deals</a>
                  <ul>
                    <li><a href="#">Cardiovascular</a></li>
                    <li><a href="#">Vascular</a></li>
                    <li><a href="#">Orthopedics</a></li>
                    <li><a href="#">Women's Health</a></li>
                    <li><a href="#">Cancer</a></li>
                    <li><a href="#">Cardiovascular</a></li>
                    <li><a href="#">Vascular</a></li>
                    <li><a href="#">Orthopedics</a></li>
                    <li><a href="#">Women's Health</a></li>
                    <li><a href="#">Cardiovascular</a></li>
                    <li><a href="#">Vascular</a></li>
                    <li><a href="#">Orthopedics</a></li>
                    <li><a href="#">Cardiovascular</a></li>
                    <li><a href="#">Vascular</a></li>
                    <li><a href="#">Orthopedics</a></li>
                    <li><a href="#">Women's Health</a></li>
                  </ul>
                </li>
                <li><a href="#" class="mymobilemenu">Health &amp; Wellness</a>
                	<ul>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a  href="#">Additional Details</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>

<div class="container simon-md-mega-menu">
	<nav class="navbar">
		<div class="container">
			<div class="row">
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="dropdown top-menu-open hide-mobile">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle">
							<span class="md-open-icon menu-open pull-left"></span>
							<span class="md-open-icon menu-close pull-left"></span>
							<span class="menu-label-shop pull-left">Shop</span>
							<span class="menu-label-close pull-left">Close</span>
						</a>
						<ul class="dropdown-menu desktop-mega-menu">
							<li class="grid-demo">
								<div class="col-sm-3 menu-level-1">
									<ul>
										<li><a href="#" data-cat-id="#category-1">Stores</a></li>
										<li><a href="#" data-cat-id="#category-2">Designers</a></li>
										<li><a href="#" data-cat-id="#category-3">Trends</a></li>
										<li><a href="#" data-cat-id="#category-4">Deals</a></li>
										<li><a href="#" data-cat-id="#category-5">Women</a></li>
										<li><a href="#" data-cat-id="#category-6">Men</a></li>
										<li><a href="#" data-cat-id="#category-7">Kids</a></li>
										<li><a href="#" data-cat-id="#category-8">Shoes</a></li>
										<li><a href="#" data-cat-id="#category-9">Handbags</a></li>
										<li><a href="#" data-cat-id="#category-10">Jewelry & Accessories</a></li>
										<li><a href="#" data-cat-id="#category-11">Home</a></li>
										<li><a href="#" data-cat-id="#category-12">Gifts</a></li>
									</ul>
								</div>
								<div class="col-sm-9 menu-borderleft">
									<div class="row clearfix">
										<div id="category-1" class="desktop-cat-panel">
											<div class="mega-menu-links clearfix">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">Cole Haan</a>
														</li>
														<li class="cat-heading">
															<a href="#">Last Call</a>
														</li>
														<li class="cat-heading">
															<a href="#">Sketchers</a>
														</li>
														<li class="cat-heading">
															<a href="#">US Polo</a>
														</li>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
													<ul>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
														<li>
															<a href="#">Store Name</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="row designer-deals clearfix">
												 <div class="designer-deals-container col-xs-12 col-md-4">
													 <p class="minor major">designer deals</p>
													 <h6>New Arrivals up to 40% off</h6>
													 <a href="#" class="major">shop now <span>&rsaquo;</span></a>
												 </div>
												 <div class="designer-deals-container col-xs-12 col-md-8">
												 <picture>
												 <!--[if IE 9]><video class="hide"><![endif]-->
													 <source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/image@2x.jpg 2x">
													 <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/image@2x.jpg">
												 <!--[if IE 9]></video><![endif]-->
														<img alt="" style="" src="/_ui/responsive/simon-theme/images/promo-banner.png">
													 	</picture>
												 		<a href="#" class="major">shop now <span>&rsaquo;</span></a>
												 </div>
											</div>
										</div>
										<div id="category-2" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS A-Z</a>
														</li>
														<li class="cat-heading">
															<a href="#">WOMEN'S DESIGNER</a>
														</li>
														<li class="cat-heading">
															<a href="#">MEN'S DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
														<li class="cat-heading">
															<a href="#">MY DESIGNERS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">FEATURED</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
												<div class="col-sm-3">
												    <ul>
														<li class="cat-heading">
															<a href="#">DESIGNERS EDIT</a>
														</li>
														<li>
															<a href="#">Dresses</a>
														</li>
														<li>
															<a href="#">Shoes</a>
														</li>
														<li>
															<a href="#">Handbags</a>
														</li>
														<li>
															<a href="#">Jeans</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
												
											</div>
										</div>
										<div id="category-3" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">MEN'S TRENDS</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">WOMEN'S TRENDS</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">Designer Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>

													</ul>
													
												</div>																								
											</div>

										</div>
										<div id="category-4" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">BABY</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIRLS</a>
														</li>
														<li class="cat-heading">
															<a href="#">BOYS</a>
														</li>
														<li class="cat-heading">
															<a href="#">THE TOY SHOP</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">CATEGORIES</a>
														</li>
														<li>
															<a href="#">Storewide</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">FEATURED OFFERS</a>
														</li>
														<li>
															<a href="#">Featured Offer</a>
														</li>
														<li>
															<a href="#">Featured Offer</a>
														</li>
														<li>
															<a href="#">Featured Offer</a>
														</li>
														<li>
															<a href="#">Featured Offer</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">WOMEN</a>
														</li>
														<li>
															<a href="#">Clothing</a>
														</li>
														<li>
															<a href="#">Shoes</a>
														</li>
														<li>
															<a href="#">Handbags</a>
														</li>
														<li>
															<a href="#">Accessories</a>
														</li>
														<li>
															<a href="#">Fragrance</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>													
													</ul>				
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">MEN</a>
														</li>
														<li>
															<a href="#">Clothing</a>
														</li>
														<li>
															<a href="#">Shoes</a>
														</li>
														<li>
															<a href="#">Handbags</a>
														</li>
														<li>
															<a href="#">Accessories</a>
														</li>
														<li>
															<a href="#">Fragrance</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>	
													</ul>
												</div>
											</div>
										</div>
										<div id="category-5" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">SHOES</a>
														</li>
														<li class="cat-heading">
															<a href="#">HANDBAGS</a>
														</li>
														<li class="cat-heading">
															<a href="#">ACCESSORIES</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING NOW</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TREND SHOPS</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">CLOTHING</a>
														</li>
														<li>
															<a href="#">Tops</a>
														</li>
														<li>
															<a href="#">Bottoms</a>
														</li>
														<li>
															<a href="#">Dresses</a>
														</li>
														<li>
															<a href="#">Jackets & Coats</a>
														</li>
														<li>
															<a href="#">Active Wear</a>
														</li>
														<li>
															<a href="#">Swimwaer</a>
														</li>
														<li>
															<a href="#">Sleepwear</a>
														</li>
														<li>
															<a href="#">Intimate Apparel</a>
														</li>
														<li>
															<a href="#">Petites</a>
														</li>
														<li>
															<a href="#">Plus Sizes</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">FRAGRANCE</a>
														</li>
														<li>
															<a href="#">Fragrance</a>
														</li>
														<li>
															<a href="#">Skin Care</a>
														</li>
														<li>
															<a href="#">Bath & Body</a>
														</li>
														<li>
															<a href="#">Hair Care</a>
														</li>
														<li>
															<a href="#">Makeup</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
											</div>
										</div>
										<div id="category-6" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">GROOMING & COLOGNE</a>
														</li>														
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING NOW</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TREND SHOPS</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">CLOTHING</a>
														</li>
														<li>
															<a href="#">Tops</a>
														</li>
														<li>
															<a href="#">Bottoms</a>
														</li>
														<li>
															<a href="#">Suits</a>
														</li>
														<li>
															<a href="#">Jackets & Blazers</a>
														</li>
														<li>
															<a href="#">Coats</a>
														</li>
														<li>
															<a href="#">Active Wear</a>
														</li>
														<li>
															<a href="#">Swimwaer</a>
														</li>
														<li>
															<a href="#">Sleepwear</a>
														</li>
														<li>
															<a href="#">Underwear</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">ACCESSORIES</a>
														</li>
														<li>
															<a href="#">Ties</a>
														</li>
														<li>
															<a href="#">Watches & Jewelry</a>
														</li>
														<li>
															<a href="#">Scarves, Hats & Gloves</a>
														</li>
														<li>
															<a href="#">Belts</a>
														</li>
														<li>
															<a href="#">Ballets & Bags</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>

													
												</div>
											</div>
										</div>
										<div id="category-7" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">THE TOY SHOP</a>
														</li>														
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">BABY</a>
														</li>
														<li>
															<a href="#">Baby Girl(0-24 months)</a>
														</li>
														<li>
															<a href="#">Baby Boy(0-24 months)</a>
														</li>
														<li>
															<a href="#">Newborn Shop</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">GIRL</a>
														</li>
														<li>
															<a href="#">Toddler Girls(2T-5T)</a>
														</li>
														<li>
															<a href="#">Girls 2-6X</a>
														</li>
														<li>
															<a href="#">Girls 7-16</a>
														</li>													
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">BOYS</a>
														</li>
														<li>
															<a href="#">Toddler Boys(2T-5T)</a>
														</li>
														<li>
															<a href="#">Boys 2-7</a>
														</li>
														<li>
															<a href="#">Boys 8-20</a>
														</li>													
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">SHOES</a>
														</li>
														<li>
															<a href="#">Girls</a>
														</li>
														<li>
															<a href="#">Boys</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">ACCESSORIES</a>
														</li>
														<li>
															<a href="#">Girls</a>
														</li>
														<li>
															<a href="#">Boys</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													
												</div>
											</div>
										</div>
										<div id="category-8" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIRLS SHOES</a>
														</li>
														<li class="cat-heading">
															<a href="#">BOYS SHOES</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING NOW</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING SHOPS</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">WOMEN'S SHOES</a>
														</li>
														<li>
															<a href="#">Booties</a>
														</li>
														<li>
															<a href="#">Boots</a>
														</li>
														<li>
															<a href="#">Comforts</a>
														</li>
														<li>
															<a href="#">Evening & Bridal</a>
														</li>
														<li>
															<a href="#">Flats</a>
														</li>
														<li>
															<a href="#">Pumps</a>
														</li>
														<li>
															<a href="#">Sandals</a>
														</li>
														<li>
															<a href="#">Slippers</a>
														</li>
														<li>
															<a href="#">Sneakers</a>
														</li>
														<li>
															<a href="#">Wedges</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">MEN'S SHOES</a>
														</li>
														<li>
															<a href="#">Dress</a>
														</li>
														<li>
															<a href="#">Sneakers</a>
														</li>
														<li>
															<a href="#">Oxfords & Lace-Ups</a>
														</li>
														<li>
															<a href="#">Boots & Chukkas</a>
														</li>
														<li>
															<a href="#">Drivers</a>
														</li>
														<li>
															<a href="#">Sandals</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>	
												</div>
											</div>
										</div>
										<div id="category-9" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">WALLETS</a>
														</li>
														<li class="cat-heading">
															<a href="#">WRISTLETS</a>
														</li>
														<li class="cat-heading">
															<a href="#">GIFTS & GIFT CARDS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING NOW</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING SHOPS</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">SHOP ALL BAGS</a>
														</li>
														<li>
															<a href="#">Bagpacks</a>
														</li>
														<li>
															<a href="#">Bucket Bags</a>
														</li>
														<li>
															<a href="#">Clutches & Evening Bags</a>
														</li>
														<li>
															<a href="#">Crossbody & Messengers Bags</a>
														</li>
														<li>
															<a href="#">Hobo Bags</a>
														</li>
														<li>
															<a href="#">Satchels</a>
														</li>
														<li>
															<a href="#">Shoulder Bags</a>
														</li>
														<li>
															<a href="#">Tote Bags</a>
														</li>
														<li>
															<a href="#">Travel & Cases</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div id="category-10" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">NEW ARRIVALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DESIGNERS</a>
														</li>
														<li class="cat-heading">
															<a href="#">DEALS</a>
														</li>
														<li class="cat-heading">
															<a href="#">BELTS</a>
														</li>
														<li class="cat-heading">
															<a href="#">HATS & GLOVES</a>
														</li>
														<li class="cat-heading">
															<a href="#">SCARVES & WRAPS</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">

													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING NOW</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TRENDING SHOPS</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">Trend Name</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">FASHION JEWELRY</a>
														</li>
														<li>
															<a href="#">Earing</a>
														</li>
														<li>
															<a href="#">Bracelets</a>
														</li>
														<li>
															<a href="#">Necklaces</a>
														</li>
														<li>
															<a href="#">Rings</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>

														<ul>
														<li class="cat-heading">
															<a href="#">ACCESSORIES</a>
														</li>
														<li>
															<a href="#">Girls</a>
														</li>

														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">MEN'S ACCESSORIES</a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">TECH ACCESSORIES & CASES</a>
														</li>
													</ul>		
												</div>
											</div>
										</div>
										<div id="category-11" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">GAMES & ENTERTAINMENT</a>
														</li>
														<li class="cat-heading">
															<a href="#">PETS ACCESSORIES</a>
														</li>
														<li class="cat-heading">
															<a href="#">KIDS ROOM DECOR</a>
														</li>
														<li class="cat-heading">
															<a href="#">GARMENTS CARE & HOME CLEANING</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
												    <ul>
														<li class="cat-heading">
															<a href="#">BED & BATH</a>
														</li>
														<li>
															<a href="#">Bath</a>
														</li>
														<li>
															<a href="#">Bedding</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">DINING & ENTERTAINMENT</a>
														</li>
														<li>
															<a href="#">Dinnerware</a>
														</li>
														<li>
															<a href="#">Drinkware</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
														<li>
															<a href="#">Flatware</a>
														</li>
														<li>
															<a href="#">Barware</a>
														</li>
														<li>
															<a href="#">Table Linens & Accessories</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>

												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">HOME DECORE</a>
														</li>
														<li>
															<a href="#">Decrative Throw Pillows</a>
														</li>
														<li>
															<a href="#">Home Fragrances & Candles</a>
														</li>
														<li>
															<a href="#">Frame & Wall Art</a>
														</li>
														<li>
															<a href="#">Jackets & Coats</a>
														</li>
														<li>
															<a href="#">Vases, Bowls & Trays</a>
														</li>
														<li>
															<a href="#">Rugs</a>
														</li>
														<li>
															<a href="#">Lighting</a>
														</li>
														<li>
															<a href="#">Furniture</a>
														</li>
														<li>
															<a href="#">Storage & Organization</a>
														</li>
														<li>
															<a href="#">Jewelry Boxes</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">KITCHEN</a>
														</li>
														<li>
															<a href="#">Coffee Makers & Tea Kettles</a>
														</li>
														<li>
															<a href="#">Cookware & Bakeware</a>
														</li>
														<li>
															<a href="#">Cutley</a>
														</li>
														<li>
															<a href="#">Prep & Tools</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>
													<ul>
														<li class="cat-heading">
															<a href="#">OFFICE & STATIONARY</a>
														</li>
														<li>
															<a href="#">Desk Decore & Stationary</a>
														</li>
														<li>
															<a href="#">View All <span>&rsaquo;</span></a>
														</li>
													</ul>

												</div>
											</div>
										</div>
										<div id="category-12" class="desktop-cat-panel">
											<div class="mega-menu-links">
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">GIFT CARDS</a>
														</li>
														<li>
															<a href="#">LUXURY GIFTS</a>
														</li>
														<li>
															<a href="#">GIFTS UNDER $100</a>
														</li>
														<li>
															<a href="#">GIFTS UNDER $50</a>
														</li>
														<li>
															<a href="#">GIFTS FOR HER</a>
														</li>
														<li>
															<a href="#">GIFTS FOR HIM</a>
														</li>
													</ul>
												</div>
												<div class="col-sm-3">
													<ul>
														<li class="cat-heading">
															<a href="#">FEATURED SHOPS</a>
														</li>
														<li>
															<a href="#">Anniversary</a>
														</li>
														<li>
															<a href="#">Baby Shower</a>
														</li>
														<li>
															<a href="#">Birtday</a>
														</li>
														<li>
															<a href="#">Thank You</a>
														</li>
														<li>
															<a href="#">Wedding</a>
														</li>
													</ul>
													
												</div>																								
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
					<li class="simon-header-search">
						<span class="menu-close search-close-icon">
						</span>
						<cms:pageSlot position="csn_SearchBoxCompSlot" var="feature">
                			<cms:component component="${feature}"/>
            			</cms:pageSlot>
					</li>
					<li class="pull-right menu-fav-links hide-mobile"><a href="login" class="header-login">Login</a></li>
	            		<cms:pageSlot position="csn_FavoritesSlot" var="feature" element="li" class="pull-right menu-fav-links hide-mobile">
	                			<cms:component component="${feature}"/>
	            		</cms:pageSlot>
				</ul>
			</div>
		</div>
		</div>
	</nav>
</div>