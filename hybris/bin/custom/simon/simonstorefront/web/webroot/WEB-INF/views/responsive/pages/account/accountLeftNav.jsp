<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<aside class="col-md-3 left-menu-container show-desktop">
	<div class="account-section-header">
		<ul class="left-menu level-0 list-unstyled">
			<li><a href="#">My Premium Outlets</a></li>
			<li><a href="#">My Center</a></li>
			<li><a href="#">My Stores</a></li>
			<li><a href="#">My Favorites</a></li>
			<li><a href="#">My Offers</a></li>
			<li><a href="#">My Designers</a></li>
			<li><a href="#">My Sizes</a></li>
			<li class="has-level-1"><a href="#subMenuL1Id10"
				class="collapsed" data-toggle="collapse">My Account</a>
				<ul class="level-1 collapse" id="subMenuL1Id10">
					<li class="active"><a href="#">My Profile</a></li>
					<li><a href="#">My Orders</a></li>
					<li><a href="#">Address Book</a></li>
					<li><a href="#">Payment Method</a></li>
					<li><a href="#">Email Preferences</a></li>
				</ul></li>
		</ul>
	</div>
</aside>