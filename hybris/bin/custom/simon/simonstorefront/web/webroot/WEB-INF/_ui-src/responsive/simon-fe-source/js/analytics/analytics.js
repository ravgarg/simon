/**
* @function : rlutility
* @description : use this for global email subscription modal functionality
*/
define(['jquery'], 
    function($ ) {
        'use strict';
        var cache;
        var analytics = {
            init: function() {
                this.initVariables();
                this.initEvents();
                
               
            },
            initVariables: function() {
                cache = {
                    $document : $(document),
                    $window :$(window),
					$cartData : {},
					$pdpData : {},
					$miniBagData : {},
					$orderData : {},
					$myOrdersList : []
                }
            },
            initAjax: function() {},
            initEvents: function() {
            	if($('#analyticsSatelliteFlag').val()==='true'){
					//******************* Generic Link *****************//
					cache.$document.off('click.genericClick').on('click.genericClick','.analytics-genericLink', analytics.genericButtonOnClick); 
					cache.$document.find('.mini-cart-area').off('click.miniBaggenericClick').on('click.miniBaggenericClick','.analytics-genericLink',analytics.genericButtonOnClick);
					cache.$document.off('click.shippingReturn').on('click.shippingReturn','.analytics-shippingReturn a', analytics.genericButtonOnClick);
					
					//******************* Component Link *****************//
					cache.$document.off('click.componentClick').on('click.componentClick','.analytics-data', analytics.analyticsOnPageComponentClick); 
					
					
					//******************* PLP EVENTS *****************//
					cache.$document.off('click.plpSort').on('click.plpSort','.analytics-plpSorting li a',analytics.analyticsOnPageSortingClick);
					cache.$document.find('.analytics-plpFiltering').off('click.plpFilter').on('click.plpFilter','.analytics-filterApplyBtn',analytics.analyticsOnPageFilteringClick);
					cache.$document.find('.analytics-plpcolorSwatches').off('click.plpColorSwatches').on('click.plpColorSwatches','.color-palette a',analytics.analyticsOnPageColorSwatchesClick);
					cache.$document.off('click.plpLeftNav').on('click.plpLeftNav','.analytics-plpleftNav li a',analytics.analyticsOnPageLeftNavClick);  

					// ********** PRODUCT CLICK *****************//
					cache.$document.off('click.productClick').on('click.productClick','.analytics-item', analytics.analyticsOnPageProductClick);  

					//********** HERO CAROUSEL EVENTS *****************//
					cache.$document.off('click.heroCarousel').on('click.heroCarousel','.analytics-heroCarousel',analytics.analyticsOnPageHeroCarouselClick);
					
					//********** PDP EVENTS *****************//
					cache.$document.off('click.pdpSocialIcon').on('click.pdpSocialIcon','.analytics-socialIcon a',analytics.analyticsOnPageSocialIconClick);
					cache.$document.off('click.pdpalternateImage').on('click.pdpalternateImage','.analytics-alternateImage .item',analytics.analyticsOnPageAdditionalImageClick);
					cache.$document.off('click.pdpCheckoutbtn').on('click.pdpCheckoutbtn','.analytics-checkOutBtn',analytics.analyticsOnPageCheckoutClick);
					
					//********** MINIBAG EVENTS *****************//
					cache.$document.on('click', '.minicart',function(){
						$('.minicart').trigger('mouseover');
					});
					cache.$document.off('click.minilinkclick').on('click.minilinkclick','.analytics-miniBagLink',analytics.analyticsOnPageMiniBagEmptyLinkClick);
					cache.$document.find('.mini-cart-area').off('click.miniCheckout').on('click.miniCheckout','.analytics-checkOutBtn',analytics.analyticsOnPageCheckoutClick);
            	
					//********** HEADER & FOOTER EVENTS *****************//
					cache.$document.find('.analytics-footer').off('click.footer').on('click.footer','a, .btn',analytics.analyticsOnPageFooterClick);
					cache.$document.off('click.newsletter').on('click.newsletter','.analytics-newsletter',analytics.analyticsOnPageNewsletterClick);
			
					
					//********** LOGIN FLYOUT *****************//
					cache.$document.find('.nav, .already-exist-msg').off('click.loginFlyOut').on('click.loginFlyOut','.analytics-loginFlyOut',analytics.analyticsOnPageLoginFlyoutClick);
					cache.$document.find('.nav').off('click.logoutFlyOut').on('click.logoutFlyOut','.analytics-logout',analytics.analyticsOnPageLogoutClick);
					
					
					//********** GLOBAL FORM VALIDATION **********//
					cache.$document.on('click','#addressBookModal form.analytics-formValidation input[type=submit],#paymentMethodModal form.analytics-formValidation input[type=submit]',analytics.formSubmitValidation);
					
					cache.$document.on('submit','#globalLoginModal form.analytics-formValidation',analytics.formSubmitValidation);
					cache.$document.find('form.analytics-formValidation').on('click','.btn, .submit-btn .btn, .btn-create-account .btn-crt-account',analytics.formSubmitValidation);			
					//cache.$document.off('click.guestCheckout').on('click.guestCheckout','.analytics-guestCheckout',analytics.guestCheckoutButtonClick);
					
					//********** MYACCOUNT -> MY SIZE **********//
					$('.analytics-mysize .list-group-item a.btn').on('click',analytics.analyticsOnMySizeItemClick);
				}
            },
			
			analyticsOnPageShippingClick: function(flag,description){
				var $data = {
						"event":{
							"type" : "checkout",
							"sub_type" : 'step1_shipping_panel'
						},
						"ecommerce":{
							"action": "checkout|step1_shipping_panel",
							"checkout_stage": {
								"step": "stage1|shipping_panel"
						},
							"checkout_error": {}
						},
						"link": {
							"placement" : "checkout_step1",
							"name" : "shipping_panel"
						}
					},
					$satelliteTrack = 'checkout';
				delete $data.ecommerce["checkout_error"];
				try {
					delete digital_data.ecommerce["checkout_error"];
				}
				catch(err) {
				    
				}
				if(flag === false){
					$data.event.type = "checkout_error";
					$data.ecommerce.action = "checkout_error|step1_shipping_panel";
					
					var $errorField = [], $errors = 'something went wrong';
					if(description){
						$errors = description;
					}else{ 
						$('[data-analytic-checkouttab="shipping-address"]').find('.sm-input-group.has-error').each(function(){
							if( $(this).find('input').length > 0 ){
								$errorField.push($(this).find('input').attr('name'));
							}
							if( $(this).find('select').length > 0 ){
								$errorField.push($(this).find('select').attr('id'));
							}						
						})
						if( $errorField.length > 0 ){
							$errors = $errorField.join('|');
						}
					}
					$data.ecommerce.checkout_error = {
						"step" : $errors
					};
				}
				
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
			
			analyticsOnPageReviewClick: function(flag){
				var $data = {
						"event":{
							"type" : "checkout",
							"sub_type" : 'step2_review_order'
						},
						"ecommerce":{
							"action": "checkout|step2_review_order",
							"checkout_stage": {
								"step": "stage2|review_order"
						},
							"checkout_error": {}
						},
						"link": {
							"placement" : "checkout_step2",
							"name" : "review_panel"
						}
					},
					$satelliteTrack = 'checkout';
				
				delete $data.ecommerce["checkout_error"];
				try {
					delete digital_data.ecommerce["checkout_error"];
				}
				catch(err) {
				    
				}
				if(flag === 'onload'){					
					$.extend(true, digital_data, $data);
				
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
				}else{
					if(flag === false){
						$data.event.type = "checkout_error";
						$data.ecommerce.action = "checkout_error|step2_review_order";
						
						$data.ecommerce.checkout_error = {
							"step" : 'review_order'
						};
					}
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					} 
				}
				
			},
			
			analyticsOnPageSaveInformationClick: function(flag){
				
				var $data = {
						"form_error" : {},
						"event":{
							"type"      : "guest_checkout_initiation",
							"sub_type"  : "registration"
						},
						"ecommerce":{
							"action": "checkout|step_save_information"
						}
					},
					$satelliteTrack = 'registration';
				
				delete $data["form_error"];
				delete digital_data["form_error"];
				
				if(flag === 'onload'){
					$data.registration = {
						stage : 'initiation',
						name   : 'checkout_registration_form',
						type : 'registration_form'
					};
					
					if( digital_data.registration === undefined ){
						$.extend(true, digital_data, $data);
						
						//once data is pushed in the data layer, trigger the following function//
						if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
							_satellite.track($satelliteTrack);
						}					
					}
				}else{
					if(flag === 'skip'){
						$data.event.type = 'guest_checkout_registration_skip';
						$data.registration = {
							stage : 'registration_skip',
							name   : 'checkout_registration_form',
							type : 'registration_form'
						};
					}else if(flag === false){
						$data.event.type = 'guest_checkout_registration_failure';
						$data.ecommerce.action = "checkout_error|step_save_information";
						$data.form_error = {
								type  : 'registration_form',
								name   : 'checkout_registration_form',
								fields : "password"
						};
						$data.registration = {
							stage : 'error',
							name   : 'checkout_registration_form',
							type : 'registration_form'
						};
						$satelliteTrack = "form_error";
						
					}else if(flag === true){
						$data.event.type = 'guest_checkout_registration_completion';
						$data.registration = {
							stage : 'completion',
							name   : 'checkout_registration_form',
							type : 'registration_form'
						};
					}

					$.extend(true, digital_data, $data);
				
					//once data is pushed in the data layer, trigger the following function//
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
				}
				
			},
			
			analyticsOnPagePaymentClick: function(flag,description){
				var $data = {
						"event":{
							"type" : "checkout",
							"sub_type" : 'step3_payment_panel'
						},
						"ecommerce":{
							"action": "checkout|step3_payment_panel",
							"checkout_stage": {
								"step": "stage3|payment_panel"
						},
							"checkout_error": {}
						},
						"link": {
							"placement" : "checkout_step3",
							"name" : "place_order"
						}
					},
					$satelliteTrack = 'checkout';
					
				delete $data.ecommerce["checkout_error"];
				delete digital_data["form_error"];
				try {
					delete digital_data.ecommerce["checkout_error"];
				}
				catch(err) {
				    
				}
				var $errorField = [], $errors = 'something went wrong';
				if( flag === 'submit' ){
					var form = $("#payment-form");
					
					form.parsley().validate();
						
					if(form.parsley().isValid() === false || description){
						flag = false;
					}
					
				}
				if( flag === false ){
					$data.event.type = "checkout_error";
					$data.ecommerce.action = "checkout_error|step3_payment_panel";
					
					if(description){
						$errors = description;
					}
					
					$('[data-analytic-checkouttab="payment"]').find('.sm-input-group.has-error:visible').each(function(){
						if( $(this).find('input').length > 0 ){
							$errorField.push($(this).find('input').attr('id'));
						}
						if( $(this).find('select').length > 0 ){
							$errorField.push($(this).find('select').attr('id'));
						}
					})
					if( $errorField.length > 0 ){
						$errors += $errorField.join('|');
					}
					
					$data.ecommerce.checkout_error = {
						"step" : $errors
					};
					
				}
				
				$.extend(true, digital_data, $data);
			
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
				}
					
				
			},
			
			analyticsOnPageNewsletterClick: function(){
				var $data = {
					"event" : {
						"type" : "newsletter_signup_initiation",
						"sub_type" : "newsletter"						
					},
					"newsletter" : {
						"stage" : "newsletter_signup_initiation"
					}
				},
				$satelliteTrack = "newsletter";;
				
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				} 
				
			},			
			
			analyticsOnPageLogoutClick: function(e){
				e.stopPropagation();
				var $satelliteTrack = "login", 
					$data = {
						event:{
						  sub_type : "my_account_overlay",
						  type : "log_out"
						},
						login:{
							stage: "log_out",
							name :   "my_account_overlay",
							type : 'my_account_overlay'
						}
					};
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);	
					}
						
			},
			
			analyticsOnPageLoginFlyoutClick: function($this){				
				delete digital_data["form_error"];
				var $loginName = 'login_popup';
				
				if($this.type === 'click'){
					$this = $(this);
					$loginName = 'login_flyout';
				}
				
				var $satelliteTrack = "login",
					$data = {
						event: {
							type : 'global_navigation_login_click',
							sub_type : 'login'
						},
						login: {
							stage : 'initiation',
							name : $loginName,
							type : 'login_form'
					}
					};
				
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
					} 
							
			},
			
			formSubmitValidation: function(response){
				var form = '',
					errorArr = [],
					flag = true;
				if(response.formname){
					form =  $(response.formname);
					if(response.formerror){
						errorArr.push(response.formerror);
						flag = 'error';
					}
				}else{
					form = $(this).closest('.analytics-formValidation');
				}				
				var	$satelliteTrack = form.data('satellitetrack'),
					$eventType = form.data('analytics-eventtype'),
					$eventSubType = form.data('analytics-eventsubtype'),
					$type = form.data('analytics-type'),
					$data = {
						"event": {
							"type" : $eventType,
							"sub_type" : ''
						}
					}, // flag added to skip satallite track on success of some pages on form validation
					$skipSatelliteTrackValidForm = ['login_flyout','checkout_login_form_page','login_popup','login_overlay_form','loginpage','registration_form_page','orderconfirmation_registration_form','newsletter_popup','order_tracking','add_new_address','add_payment_method','my_account_update'];
				
				
				delete $data["form_error"];
				delete digital_data["form_error"];
				form.parsley().validate();
				
			
				if (form.parsley().isValid() && flag !== 'error'){
					$data.event.sub_type = $eventSubType+'_completion';
					$data[$type] = {
						stage : form.data('analytics-formname')+'_completion',
						name : form.data('analytics-formname')
					};
					
					if($data[$type].name === 'email-preferences' && $('#optedInEmail').is(':checked') === true){
						$data.event.sub_type = $eventSubType+'_opt_out';
						$data[$type].stage = form.data('analytics-formname')+'_opt_out';
					}
					
					if($.inArray($data[$type].name, $skipSatelliteTrackValidForm) !== -1) {
						flag = false;
					}
					
					if( $data.event.type === 'premium_outlet_center' ){
						var $secondaryCenter = [];
						$('#drpCenters').find('li .item-padding').each(function(){
							if( $(this).find('input').is(':checked') ){
								$secondaryCenter.push(  $.trim($(this).find('label').text()) );
							}
						})
						$data.premium_outlet = {
							  primary_center: $( 'select[name="primaryMall"] option:selected' ).text(),
							  secondary_center: $secondaryCenter.join(' | ')
						};
					}
					
					if(form.hasClass('captchaValidation')){
						var response = grecaptcha.getResponse();
						if(response.length===0){
							flag = 'error';
							errorArr.push('You can not leave Captcha Code empty');
						}
					}
				}else{
					flag = 'error';
				}
				if(flag === 'error'){
					$(form).find('.sm-input-group.has-error:visible').each(function(){
						if( $(this).find('input').length > 0 ){
							errorArr.push($(this).find('input').attr('id'));
						}
						if( $(this).find('select').length > 0 ){
							errorArr.push($(this).find('select').attr('id'));
						}						
					});
					
					$data.event.sub_type = $eventSubType+'_failure';
					$data.form_error = {
							type : form.data('analytics-formtype'),
							name : form.data('analytics-formname'),
							fields : errorArr.join('|')
						
					};
					$data[$type] = {
						stage : 'error',
						name : form.data('analytics-formname'),
						type : form.data('analytics-formtype')
					};
						
					$satelliteTrack = 'form_error';
				}
				if(flag){
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function:// 
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
				}
			},
			
			analyticsAJAXSuccessHandler: function($param){
				var $data = {};
				
				if($param.ecommerce){
					$data.ecommerce = {};
					$data.ecommerce[$param.type] = $param.favoriteList;
				}else if($param.type){
					$data[$param.type] = {
						stage : $param.stage,
						name : $param.formname,
						type : $param.formnametype
					};
					if($param.zipcode){
						$data[$param.type].zip_code = $param.zipcode;
					}
				}
				$.extend(true, digital_data, $data);
				
				if($param.satelliteTrack){
					//Once the data is pushed to the Data Layer, trigger the following function:// 
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($param.satelliteTrack);
					}
				}
			},
			
			analyticsAJAXFailureHandler: function(eventSubType,errorMessage,errorType,errorCode,linkURL){
				// AJAX FAILURE HANDLER
				if(!errorType){
					errorType = 'system error';
				}
				if(!eventSubType){
					eventSubType = digital_data.event.sub_type;
				}
				
				var $data = {
						event: {
							sub_type : eventSubType+'_error'
						},
						error: {
							type : errorType,
							message   : errorMessage
						}
					},
					$satelliteTrack = 'error_track';
				if(errorCode){
					$data.error.errorcode = errorCode;
				}
				if(linkURL){
					$data.error.intendedurl = linkURL;
				}
				
				$.extend(true, digital_data, $data);
					
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
			},
			
			trackingOrderFormSuccess: function(response){
				var $data = {
						ecommerce :{
							order_info: []
						},
						order_tracking:{
							stage : "order_tracking_completion",
							name : "order_tracking"
						},
						order_tracking_status: "no_result"   
					},
					$satelliteTrack = 'order_tracking';
				
				if(response.TRACKORDER){
					var $retailer = [];
					$(response.TRACKORDER.retailers).each(function(key,value){
						$retailer.push(value);
					})
					var d = new Date(response.TRACKORDER.placed),
						locale = "en-us",
						$orderDate = d.toLocaleString(locale, {month: "long"})+' '+d.getDate()+ ','+ d.getFullYear();
					var ordersData = {
							id : response.TRACKORDER.code,
							retailer : $retailer.join(' | '),
							order_date : $orderDate,
							price : response.TRACKORDER.total.value,
							status : response.TRACKORDER.statusDisplay
						};
					$data.ecommerce.order_info.push( ordersData );
				}
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function:// 
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
			},
			
			analyticsOnPageRecommendedProductClick: function(){
				var $this = $(this),
					$parent = $this.closest('.analytics-productRecommended'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$data = $parent.data('analytics'),
					$recommendedProduct = $this.data('analytics'); 
				
				$data.ecommerce = {
					recommended_prod_click : {
							id : $this.find('.base-product-code').val(),
							name : $this.find('.base-product-name').val(),											
							position : $recommendedProduct.ecommerce.prod_click.position,
							retailer : $this.find('.item-name.analytics-retailer').data('analytics-retailer'),
							designer : $this.find('.item-name.analytics-designer').data('analytics-designer')
					} 
				};
				
				if( $this.find('.base-product-category').length > 0 ){
					$data.ecommerce.recommended_prod_click.category = $this.find('.base-product-category').val();
				} 
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
			
			analyticsOnPageFooterClick: function(){
				var $this = $(this),
					$data = {
						"event":{
							"type" : "global_navigation",
							"sub_type" : 'footer_links'
						},
						"component":{
							"type" : "footer"
						},
						"link": {
							"placement" : "side_links|footer_navigation"
						}
					},
					$satelliteTrack = 'link_track';
				
				if( $this.closest('div').hasClass('footer-social') ){
					$data.event.type = 'link_click|social_icons';
					$data.component.name = $this.data('analytics-linkname');
					$data.link.placement = 'social_icon|footer_navigation';
				}
				
				if(typeof $this.data('analytics-linkname') !== 'undefined'){
					$data.link.name = $this.data('analytics-linkname');					
				}
				
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
			
			analyticsOnPageHeaderNavClick: function($param){
				var $this = $($param);
				
				if(typeof $this.data('analytics-linkname') !== 'undefined'){
					var	$parent = $this.closest('.analytics-headerNavigation'),
						$satelliteTrack = $parent.data('satellitetrack'),
						$data = {
							event : {
								type : $parent.data('analytics-eventtype'),
								sub_type : $parent.data('analytics-eventsubtype')
							},
							link : {
								placement : $parent.data('analytics-linkplacement'),
								name : $this.data('analytics-leftlinkname')
							}
						};
					
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
				}
					
				}
			},
			
			analyticsOnPageHeaderSearchListClick : function(productId,productName){
				
				var $searchTerm = $('form.analytics-headerSearch').find('input[name="text"]').val(),
					$satelliteTrack = 'internal_search',
					$data = {
						event: {
							type : 'internal_search',
							sub_type : 'full_query_search_term'
						},
						ecommerce : {
							search : {
								term : $searchTerm,
								type : 'full_query_search'
							}
						}
					};
				if(typeof productId !== 'undefined'){
					$data.event.sub_type = "auto_suggested_search_term";
					$data.ecommerce.search.type = 'auto_suggested';
					$data.ecommerce.search.term = productName.toLowerCase();
					$data.ecommerce.prod_info = {
											id : productId,
											name : productName
										};
				}
				
				$.extend(true, digital_data, $data);
					
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				} 
				
			},
			
            genericButtonOnClick: function(){
				
				var $this = $(this),
					$eventType = 'link_click',
					$satelliteTrack = 'link_track',
					$eventSubType = $this.data('analytics-eventsubtype'),
					$linkPlacement = $this.data('analytics-linkplacement'),
					$linkName = $this.data('analytics-linkname');
					
				if($this.closest('div').hasClass('analytics-shippingReturn') === true){
					var $parent = $this.closest('div.analytics-shippingReturn');
						$eventType = $parent.data('analytics-eventtype');
						$eventSubType = $parent.data('analytics-eventsubtype');
						$linkPlacement = $parent.data('analytics-linkplacement');
						$linkName = $parent.data('analytics-linkname');
						$satelliteTrack = $parent.data('analytics-satellitetrack');
				}else{					
				if($this.data('analytics-eventtype')){
					$eventType = $this.data('analytics-eventtype');
				}
				if($this.data('analytics-satellitetrack')){
					$satelliteTrack = $this.data('analytics-satellitetrack');
				}
				}
				var	$options  = {
						 event: { 
							 type      : $eventType, 
								sub_type :  $eventSubType
						  },			
						   link: { 
								   placement :  $linkPlacement,            
								   name      :  $linkName   
						   }
					  };
				if( $this.hasClass('analytics-dealClick') ){
					$options.ecommerce = {
						deal_click : {
							code : $this.data('dealval')
						}
					}
				}
				if( $this.hasClass('analytics-storeClick') ){
					$options.ecommerce = {
						store_click : {
							name : $this.data('storeval')
						}
					}
				}
				if( $this.hasClass('analytics-designerClick') ){
					$options.ecommerce = {
						designer_click : {
							name : $this.data('designerval')
						}
					}
				}
				
            	 $.extend(true, digital_data, $options); 
				 
	             //Once the data is pushed to the Data Layer, trigger the following function://  				
	             if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
	             _satellite.track($satelliteTrack);
	             } 				
	             
            },
             
            analyticsOnPageHeroCarouselClick: function(){
				analytics.analyticsOnPageComponentClick($(this));
			},
			
			analyticsOnPageLeftNavClick: function(){
				var $this = $(this);
				if( $this.hasClass('accordion-menu') === false ){
					var $parent = $this.closest('.analytics-plpleftNav'),
						$satelliteTrack = $parent.data('satellitetrack'),
						$data = {
							event : {
								type : $parent.data('analytics-eventtype'),
								sub_type : $parent.data('analytics-eventsubtype')
							},
							link : {
								placement : $parent.data('analytics-linkplacement'),
								name : $this.data('analytics-leftlinkname')
							}
						};
					
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
					} 
					
					
				}
			},
			
			analyticsOnPageSortingClick: function(){
				var $parent = $(this).closest('.analytics-plpSorting'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$data = $parent.data('analytics'),
					$this = $(this).closest('li').data('value');
				
				
				$.extend(true, digital_data, $data);
				
				var $param = { 
							ecommerce:{ 
								sort:{ 
									name: $this
								} 
							}
						};
				
				$.extend(true, digital_data, $param);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				} 
				
				
			},
			
			analyticsOnPageFilteringClick: function(){
				var $this = $(this),
					$parent = $this.closest('.analytics-plpFiltering'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$data = $parent.data('analytics'),
					$filter='';				
				
			
				$.extend(true, digital_data, $data);
				
				$parent.find('.panel').each(function(){
					var $currentTitle = $(this).find('.panel-title').clone().children().remove().end().text();
					var $currentData = [];
					$(this).find('.multiselect-item.active, .line-item.active').each(function(){
						$currentData.push(($(this).text()).trim());
					});
					if( $currentData.length > 0 ){
						$currentTitle = $currentTitle+':'+$currentData.join(',');
						$filter = $filter+$currentTitle+'|';
					}
				});
				
				var $param = { 
							ecommerce:{ 
								filter:{ 
									name: $filter.slice(0,-1)
								} 
							}
						};
				
				$.extend(true, digital_data, $param);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
				
			},
			
			analyticsOnPageColorSwatchesClick: function(e){
				e.stopPropagation();
				var $parent = $(this).closest('.analytics-plpcolorSwatches'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$productParent = $(this).closest('.item-tile');
				var $param = {
							event: {
								type : 'product_click',
								sub_type : 'colour_swatches'
							},
							ecommerce:{ 
								colour_swatches_click:{ 
									id : $productParent.find('.base-product-code').val(),
									name : $productParent.find('.base-product-name').val(),											
									position : $productParent.index() + 1,
									attribute : 'color:'+$productParent.find('.analytics-colorattribute').data('analytics-colorattribute')
								} 
							},
							link: {
								placement: $('.analytics-plpList').data('analytics-linkplacement'), 
								name: "colour_swatches"
							}
						};
				
				$.extend(true, digital_data, $param);
				
				//Once the data is pushed to the Data Layer, trigger the following function://  
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
			
			analyticsOnPageBrandNameClick: function(){
				var $this = $(this),
					$data = $this.data('analytics'),
					$satelliteTrack = $this.data('satellitetrack');
					
				
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
					} 
					
			},
			
			analyticsOnPageSocialIconClick: function(){
				var $this = $(this),
					$parent = $this.closest('.analytics-socialIcon'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$name = $this.attr('id');
					
				var $componentName = '';
					if( $parent.data('analytics-componenttype') === 'product' ){
						$componentName = digital_data.ecommerce.item[0].name;
					}else if( $parent.data('analytics-componenttype') === 'deal' ){
						$componentName = $('p.free-shipping').html();
					}
					var $param = {
							event: {
								type : 'link_click|social_icons',
								sub_type : $parent.data('analytics-eventsubtype')
							},
							component: {
								type : $parent.data('analytics-componenttype'),
								name : $componentName
							},
							link: {                
								placement : $parent.data('analytics-linkplacement'),
								name :  $name   
							}
						};
			
					$.extend(true, digital_data, $param);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
					}
					
			},
			
			analyticsOnPageCheckoutClick: function(){
				var $this = $(this),
					$satelliteTrack = $this.data('satellitetrack');
					
					var $data ={
							event: {
								type : $this.data('analytics-eventtype'),
								sub_type : $this.data('analytics-eventsubtype')
							},
							ecommerce: {
								proceed_checkout:{
									type : $this.data('analytics-ecommercetype')
								}
							},
							link: {
								placement : $this.data('analytics-linkplacement'),
								name :  $this.data('analytics-linkname')
							}
						};
					
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
					
			},
			
			analyticsOnPageProductVariantClick: function($this){
				
				var $parent = $this.closest('.analytics-productVariant'),
					$data = $parent.data('analytics'),
					$satelliteTrack = $parent.data('satellitetrack'),
                    colorId = $('.color-palette').find('span.active').data('color-id'),
					$items = [];
				
					var skuKey = $('#skuKey').val();
				if( skuKey !== '' ){
					
					$.each(cache.$pdpData.ecommerce.colordetails,function(key,value){
						if( key === colorId ){
							$.each(cache.$pdpData.ecommerce.colordetails[key].productDimensionsData,function(key1,value1){
								if( skuKey === value1.id ){
									$items.push(value1);
							}
					});
						}
					});
					$data.ecommerce.item = [];
					$data.ecommerce.action = 'prod_attribute_change';
					$.extend(true, $data.ecommerce.item, $items);
					
					$.extend(true, digital_data, $data);
						
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
				}
					
				}
			},
			
			analyticsOnPageContinueShoppingClick: function(){
				var $this = $(this),
					$data = $this.data('analytics'),
					$satelliteTrack = $this.data('satellitetrack');
					
				
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
					
			},
			
			analyticsOnPageAdditionalImageClick: function(){
				var $this = $(this),
					$parent = $this.closest('.analytics-alternateImage'),
					$data = $parent.data('analytics'),
					$satelliteTrack = $parent.data('satellitetrack');
					
					$.extend(true, digital_data, $data);
					
					//Once the data is pushed to the Data Layer, trigger the following function://  
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					} 
					
			},
			
			analyticsOnPageAddToBagClick: function(){
				var skuKey = $('#skuKey').val();
				if( skuKey !== '' ){	
				var $data = cache.$document.find('.add-to-bag button').data('analytics'),
					$satelliteTrack = 'add_bag',
					$option = {
						event:{
							type : 'add_bag',
							sub_type : $data.event.sub_type
						},
						ecommerce : {
							action : 'add_bag',
							bag : {
								type : 'sub-bag'
							},
							item : []							
						},
						link : {
							placement : $data.link.placement,
							name : $data.link.name
						}
					};
					var $productCode = $('input[name="productCodePost"]').val(),
						flag = false;

					$.each(cache.$pdpData.ecommerce.colordetails,function(key,value){
						$.each(cache.$pdpData.ecommerce.colordetails[key].productDimensionsData,function(key1,value1){
							if( $productCode === value1.id ){
								$option.ecommerce.item.push(value1);
									$option.ecommerce.item[0].quantity = $('#quantity').val();
								flag = true;
								return false;
							}
						});
						if(flag === true){
							return false;
						}
					});
					
					$.extend(true, digital_data, $option);
					
					 //Once the data is pushed to the Data Layer, trigger the following function://  				
						if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
						} 				
						
				}
			},
			
			analyticsOnPageMiniBagEmptyLinkClick: function(){
				var $this = $(this),
					$data = $this.data('analytics'),
					$satelliteTrack = $this.data('satellitetrack');
				
				$.extend(true, digital_data, $data);
					
				 //Once the data is pushed to the Data Layer, trigger the following function://  				
				 if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);				
				 }				
								
			},
			
			analyticsOnPageBtnFavoriteClick: function($this){
				event.stopPropagation();

				//var $this = $(this),
				var $parent = $this.closest('.analytics-addtoFavorite'),
					$data = {
							event :{
								type : '',
								sub_type : $parent.data('analytics-eventsubtype')
							},
							ecommerce:{
								favorite:{}
							},
							link:{
								placement : $parent.data('analytics-linkplacement'),
								name : ''
							}
					},
					$pageType = $parent.data('analytics-pagetype'),
					$pageStyle = $parent.data('analytics-pagestyle'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$favorite ,
					$param = {},
					$flag = false,
					$productCode = '';
					
					if(!$satelliteTrack){
						$satelliteTrack = 'favoriting';
					}
					
					// Load More- do not have updated pagetype so need to add custom check
					if($this.closest('.analytics-data').hasClass('analytics-loadMoreData')){
						$pageStyle = 'plp';
						if($('#searchText').length === 1){
							$pageType = 'productsearch';
							$data.event.sub_type = 'productsearch |'+$parent.data('analytics-component');
							$data.link.placement = 'productsearch |'+$parent.data('analytics-component');
						}else if($('.page-productList').length === 1){
							$pageType === 'category';
							$data.event.sub_type = 'category |'+$parent.data('analytics-component');
							$data.link.placement = 'category |'+$parent.data('analytics-component');
						}
					}
					if( $this.hasClass('mark-favorite') ){
						$flag = true;
						if($pageStyle){
							
							if($pageType === 'myaccountfavorite'){
								// My Account- Designers A_Z
								$param = {
									id : $parent.find('.analytics-base-code').val(),
									name : $parent.find('.analytics-base-name').val()
								}
							}else {
								$param = {
									id : $this.closest('.item-tile').find('.base-product-code').val(),
									name : $this.closest('.item-tile').find('.base-product-name').val(),
									retailer : $this.closest('.item-tile').find('.item-name.analytics-retailer').data('analytics-retailer'),
									designer : $this.closest('.item-tile').find('.item-name.analytics-designer').data('analytics-designer')
								}
							}
						}else{
							if($pageType === 'productsearch' || $pageType === 'category' || $pageType === 'my-favorites' || $pageType === 'storelisting'){
								$param = {
									id : $this.closest('.item-tile').find('.base-product-code').val(),
									name : $this.closest('.item-tile').find('.base-product-name').val(),
									retailer : $this.closest('.item-tile').find('.item-name.analytics-retailer').data('analytics-retailer'),
									designer : $this.closest('.item-tile').find('.item-name.analytics-designer').data('analytics-designer')
								}
							}else if($pageType === 'sub_bag'){
								// minicart data
								$productCode = $parent.data('analytics-productid');
								$.each(cache.$miniBagData.ecommerce.item,function(key,value){	
									
									if( value.id === $productCode ){
										
										$param = {
											id : value.id,
											name : value.name,
											retailer : value.retailer,
											designer : value.designer
										}
									}
								});
								
							}else if($pageType === 'shipping_bag'){
								// CART - page	
								$productCode = $parent.data('analytics-productcode');
								$.each(cache.$cartData.ecommerce.item,function(key,val){
									if( cache.$cartData.ecommerce.item.hasOwnProperty(key) ){
										$.each( cache.$cartData.ecommerce.item[key],function(key1,val1){
											if(val1 === $productCode){
												$param.id = cache.$cartData.ecommerce.item[key].id;
												$param.name = cache.$cartData.ecommerce.item[key].name;
												$param.retailer = cache.$cartData.ecommerce.item[key].retailer;
												$param.designer = cache.$cartData.ecommerce.item[key].designer;
											}
										});
									}
								});
							}else if($pageType === 'prod_view'){
								// PDP - page
								$productCode = $('input[name="productCodePost"]').val();

								
								$.each(cache.$pdpData.ecommerce.colordetails,function(key,value){
								   
									$.each(cache.$pdpData.ecommerce.colordetails[key].productDimensionsData,function(key1,value1){
										if( $productCode === value1.id ){
											$param = value1;
										}
									});

								});
								
							}else if($pageType === 'myaccountfavorite'){
								// My Account- Stores, Designers
								$param = {
									id : $parent.find('.analytics-base-code').val(),
									name : $parent.find('.analytics-base-name').val()
								}
							}else if($pageType === 'add_bagpdp'){
								// PDP- Add to Bag Modal Favourite
								$data.event.sub_type = $parent.data('analytics-eventsubtype');
								$data.link.placement = $parent.data('analytics-linkplacement');
									$param = {
										id : $parent.find('.analytics-base-code').val(),
										name : $parent.find('.analytics-base-name').val()
								}
							}
						}
					}
					if( $flag === true ){					
						if( $this.hasClass('marked') ){
							$favorite = 'marked_favorite';
						}else{
							$favorite = 'marked_unfavorite';
						}
						$data.event.type = $favorite;
						$param.type = $favorite;						
						$param.component = $parent.data('analytics-component');						
						$data.ecommerce.favorite = $param;
						$data.link.name = $favorite;
						
						$.extend(true, digital_data, $data); 
						 
						 //Once the data is pushed to the Data Layer, trigger the following function://  				
						 if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);
					}
						
					}
			},
			
			analyticsOnPageLoadMoreClick : function(productCount){
								
				var $satelliteTrack = 'product_impression',
					$plpList = $('.analytics-plpList'),
					$param = { 
						event: {
							type: $plpList.data('analytics-eventsubtype'),
							sub_type: "additional_products_impressions"
						},
						ecommerce:{ 
							 addtional_prod:{ 
								start_index: productCount + 1
							},
							item: []
						},
						link: {
							placement: $plpList.data('analytics-linkplacement'),
							name : "load_more_products"
						}
					},
					$productCount = 1;
				
				$.extend(true, digital_data, $param);
				
				$('.analytics-plpList .analytics-item').each(function(){
					
						var $this = $(this),
							productData = {
								id : $this.find('.base-product-code').val(),
								name : $this.find('.base-product-name').val(),											
								position : $productCount,
								retailer : $this.find('.item-name.analytics-retailer').data('analytics-retailer'),
								designer : $this.find('.item-name.analytics-designer').data('analytics-designer'),
								out_of_stock : $this.data('analytics-productoutofstock'),
								attribute : 'color:'+$this.find('.analytics-colorattribute').data('analytics-colorattribute')
								
							};
						if( $this.find('.base-product-category').length > 0 ){
							productData.category = $this.find('.base-product-category').val();
						}
						if( $(this).closest('.analytics-plpList').length > 0 ){	
							var $plpList = $('.analytics-plpList');
							if( $plpList.data('analytics-filtername') ){
								$param.ecommerce.filter = {
									name : $plpList.data('analytics-filtername')
								}
							}
							$param.ecommerce.sort = {
								name : $plpList.data('analytics-sortname')
							}
							
						}
						$param.ecommerce.item.push( productData );
						$productCount++;
					
				});
				
				$.extend(true, digital_data, $param);
				
				//Once the data is pushed to the Data Layer, trigger the following function:// 
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);					
				}
				
				
			},
			
			fetchMyOrders: function($satelliteTrack){
				console.log($satelliteTrack);
				var ordersData = {};
				cache.$myOrdersList = [];
				cache.$document.find('.analytics-myOrders .order-detail-row').each(function(){
					var $this = $(this),
						retailerData = [];
					
					$this.find('.retailer-name').each(function(){
						retailerData.push($(this).text());
					});						
					ordersData = {
						id : $this.find('.orderDetails').data('analytics-ordercode'),
						retailer : retailerData.join(','),
						order_date : $this.find('.date-detail').data('analytics-orderdate'),
						price : $this.find('.price').data('analytics-orderprice')
					};
					cache.$myOrdersList.push( ordersData );
				});
				if($satelliteTrack){
					digital_data.event.type = $satelliteTrack;
					digital_data.ecommerce.order_info = cache.$myOrdersList;
					if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
						_satellite.track($satelliteTrack);					
					}
				}
			},
			
			updateCartData: function($data){
				
				var $param = {
						event : {
							type : 'change_quantity',
							sub_type : 'shopping_bag'
						},
						ecommerce: {},
						link: {
							placement : 'shopping_bag',
							name : 'change_quantity'
						}
					},
					$satelliteTrack = 'shopping_bag';
				
				if($data.type === 'remove'){
					$param.event.type = 'remove_product';
					$param.ecommerce = {
						remove: {}
					};
					$param.link.name = 'product_remove';
					$satelliteTrack = 'product_removal';
					
					$.each(cache.$cartData.ecommerce.item,function(key,val){
						if( cache.$cartData.ecommerce.item.hasOwnProperty(key) ){
							$.each( cache.$cartData.ecommerce.item[key],function(key1,val1){
								if(val1 === $data.productcode){
									$param.ecommerce.remove = cache.$cartData.ecommerce.item[key];	
									// removed from global cartData array
									cache.$cartData.ecommerce.item.splice(key,1);
									digital_data.ecommerce.item = cache.$cartData.ecommerce.item;
								 }
							});
						}
					});
				}else{
					$param.ecommerce.action = 'change_quantity';
					$param.ecommerce.quantity = {
						id : $data.productcode,
						name : $data.productname,
						quantity : $data.productquantity
					};
					$.each(cache.$cartData.ecommerce.item,function(key,value){
						if(value.id === $data.productcode){
							value.quantity = $data.productquantity;
						}
					});
					$.each(digital_data.ecommerce.item,function(key,value){
						if(value.id === $data.productcode){
							value.quantity = $data.productquantity;
						}
					});
				}
				$.extend(true, digital_data, $param);
				//Once the data is pushed to the Data Layer, trigger the following function:// 
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
				}

			},
			
			fetchCartData: function($data){
				if( $data.length > 0 ){
				$.each($data,function(key,value){
						// retailer details
						var $prefix = $data[key],
							$subtotal = ($prefix.estimatedSubtotal)?$prefix.estimatedSubtotal.value:0,
							$estimated = ($prefix.estimatedTotal)?$prefix.estimatedTotal.value:0,
							$saving = ($prefix.saving)?$prefix.saving.value:0,
							$tax = ($prefix.estimatedTax)?$prefix.estimatedTax:0,
							$shipping = ($prefix.estimatedShipping)?$prefix.estimatedShipping.value:'',
							$shippingMethod = ($prefix.appliedShippingMethod)?$prefix.appliedShippingMethod.name:'',
							$retailerdiscount = ($prefix.promotionalDiscount)?$prefix.promotionalDiscount.value:0,
							$retailerInfo = {
								'name' : $prefix.retailerName,
								'item' : $prefix.totalUnitCount,
								'revenue' : {
									'exclusive'      : $subtotal,
									'gross_total'    : $estimated,
									'tax'            : $tax,
									'discount'       : $saving,
									'retailer_discount': $retailerdiscount
								},
								'offer' : {}
							};
							if($shipping){
								$retailerInfo.shipping = $shipping;
							}
							if($shippingMethod){
								$retailerInfo.shipping_method = $shippingMethod;
							}
							if($prefix.retailerPromotionList && $prefix.retailerPromotionList.length > 0){								
								var $retailerPromotionsTypeArr = [],
								    $retailerPromotionsCodeArr = [],
								    $retailerPromotionsDescArr = [];
								
								$.each($prefix.retailerPromotionList,function(k,v){
									$retailerPromotionsTypeArr.push(v.type);
									$retailerPromotionsCodeArr.push(v.code);
									$retailerPromotionsDescArr.push(v.description);
								});
								
								$retailerInfo.offer = {
									'type' : $retailerPromotionsTypeArr.join(' > '),
									'code' : $retailerPromotionsCodeArr.join(' > '),
									'description' : $retailerPromotionsDescArr.join(' > ')
								};
							}
						cache.$cartData.ecommerce.retailer.push($retailerInfo);
					
						// products items details
						var $productArr = $prefix["productDetails"];
						$.each($productArr,function(i,j){
							var $stock = ( typeof $productArr[i].product.stock.stockLevelStatus.code !== 'undefined' )?$productArr[i].product.stock.stockLevelStatus.code:'',
								$strike = 0,
								$actual = 0,
								$productSize = '',
								$productStyle = '',
								$productFit = '',
								$productColor = "colour  : "+$productArr[i].product.variantValues.Color,
								$pricebeforediscount = 0;
								
							if($productArr[i].promotionApplicable){
								$actual = $productArr[i].yourPrice.value;
								if( $productArr[i].msrpValue ){
									$strike = $productArr[i].msrpValue.value;
							}
							else{
								if($productArr[i].saleValue){
									$strike = $productArr[i].listValue.value;
								}
							}
								if($productArr[i].saleValue){
									$pricebeforediscount = parseFloat($productArr[i].saleValue.value);
								}
								else{
									$pricebeforediscount = parseFloat($productArr[i].basePrice.value);
								}
								
								
							}
							else{
								if( $productArr[i].msrpValue ){
									$strike = $productArr[i].msrpValue.value;
									if($productArr[i].saleValue){
										$actual = $productArr[i].saleValue.value;
								}else{
										$actual = $productArr[i].listValue.value;
									}
								}else{
									if($productArr[i].saleValue){
										$strike = $productArr[i].listValue.value;
										$actual = $productArr[i].saleValue.value;
									}else{
										$actual = $productArr[i].listValue.value;
									}
								}
								
							}
								
							if($productArr[i].product.variantValues.Size){$productSize = "|size : "+$productArr[i].product.variantValues.Size;}
							if($productArr[i].product.variantValues.Style){$productStyle ="|style : "+$productArr[i].product.variantValues.Style;}
							if($productArr[i].product.variantValues.Fit){$productFit = "|size_type : "+$productArr[i].product.variantValues.Fit;}

							// product details
							var $productInfo = {
									'id' : $productArr[i].product.code,
									'attribute' : $productColor+$productSize+$productFit+$productStyle,
									'name' : $productArr[i].product.name,
									'retailer' : $prefix.retailerName,
									'quantity' : $productArr[i].quantity,
									'out_of_stock' : $stock, 
									'favorite_status': $productArr[i].product.favorite,
									'offer' : {},
									'price' : { 'strike_through' : $strike, 'actual' : $actual ,'price_beforediscount' :$pricebeforediscount}
								};
							if($productArr[i].product.baseDesignerName){
								$productInfo.designer = $productArr[i].product.baseDesignerName;
							}
							if($productArr[i].itemPromotionList && $productArr[i].itemPromotionList.length > 0){
								var $itemPromotionsTypeArr = [],
									$itemPromotionsCodeArr = [],
									$itemPromotionsDescArr = [];
								
								if($productArr.length) {
									$.each($productArr[i].itemPromotionList,function(k,v){
										$itemPromotionsTypeArr.push(v.type);
										$itemPromotionsCodeArr.push(v.code);
										$itemPromotionsDescArr.push(v.description);
									});
								
									$productInfo.offer = {
										'type' : $itemPromotionsTypeArr.join(' > '),
										'code' : $itemPromotionsCodeArr.join(' > '),
										'description' :$itemPromotionsDescArr.join(' > ')
									};
								}
							}
							cache.$cartData.ecommerce.item.push($productInfo);
						});
				});
				}
			},
			
			analyticsOnPageCartLoad: function(){
				var $cartInfo = $('#cartData').val();
				if($cartInfo) {
				    try {
						var $data = $.parseJSON($cartInfo);
				    } catch(e) {
						analytics.analyticsAJAXFailureHandler('shipping_bag','something went wrong','CE');
				        return;
				    }
				}
				
				
				cache.$cartData = {
						ecommerce: {
							action : 'view_shopping_bag',
							bag: {
								type : 'shipping_bag'
							},
							item: [],
							retailer: []
						}
					};
				analytics.fetchCartData( $data.retailerInfoData );

			},
			
			analyticsOnPagePDPLoad: function(){
				var $pdpInfo = $('#analyticsDataPDP').val();
				if($pdpInfo) {
				    try {
				       var $data = $.parseJSON($pdpInfo);
				    } catch(e) {
						analytics.analyticsAJAXFailureHandler('pdp','something went wrong','CE');
				        return;
				    }
				}
					
				var $productStatus = '',
					$productId = '',
					$productSize = '',
					$productStyle = '',
					$productColor = '',
					$productFit = '',
					$selectedColor = $('.color-palette').find('span.active').data('color-id'),
					categoryName = cache.$document.find('input[type="hidden"].base-product-category').val();
					
					cache.$pdpInfo = {
							ecommerce: {
								action : 'prod_view',
								item: [{									
										id: $productId,
										name: $data.productTitle,
										attribute: '',
										out_of_stock : $productStatus,
										retailer: $data.retailerName,
										designer: $data.designerName,
										category: categoryName,
										quantity: '1',
										price : {}
								}]
							}
						};
					
					cache.$document.find('.analytics-productData').attr('data-analytics',JSON.stringify(cache.$pdpInfo));
					$.extend(true, digital_data, cache.$pdpInfo);
					
					cache.$pdpData = {
							ecommerce: {
								colordetails:{}
							}
						};
						
					$.each($data.colordetails, function(key,value){
						$.each($data.colordetails[key]['productDimensionsData'], function(key1,value1){        
								
								var $prefix = $data.colordetails[key]['productDimensionsData'][key1],
									$outofstock = ($prefix.variantInStore === true)?false:true,
									$strike = 0,
									$actual = 0;
								
									if( $prefix.msrpValue ){
										$strike = $prefix.msrpValue.value;
										if($prefix.saleValue){
											$actual = $prefix.saleValue.value;
									}else{
											$actual = $prefix.listValue.value;
										}
									}else{
										if($prefix.saleValue){
											$strike = $prefix.listValue.value;
											$actual = $prefix.saleValue.value;
										}else{
											$actual = $prefix.listValue.value;
										}
									}
								var	$prodAttribute = {
												id : $prefix.sku,
												name : $prefix.name,
												retailer: $prefix.retailerName,
												designer: $prefix.designerName,
												category: categoryName,
												component : "product",
												out_of_stock: $outofstock,
												quantity: '1',
												price : { strike_through : $strike, actual : $actual }
										};
									var obj = {};
									obj[key] = {
										productDimensionsData:[ $prodAttribute ]
									};
										if($prefix.dimensions.size){$productSize = "|size : "+$prefix.dimensions.size.swatches[0].valueCategoryName;}
										if($prefix.dimensions.style){$productStyle ="|style : "+$prefix.dimensions.style.swatches[0].valueCategoryName;}
										$productColor = "colour  : "+$prefix.colorName;
										if($prefix.dimensions.fit){$productFit = "|size_type : "+$prefix.dimensions.fit.swatches[0].valueCategoryName;}
										
									$prodAttribute.attribute = $productColor+$productSize+$productFit+$productStyle;
										
									if( cache.$pdpData.ecommerce.colordetails.hasOwnProperty(key) ){
										cache.$pdpData.ecommerce.colordetails[key].productDimensionsData.push($prodAttribute);
									}else{
										$.extend(true, cache.$pdpData.ecommerce.colordetails, obj);
									}
									
									
									if( $selectedColor === $prefix.colorId ){
										$productStatus = $outofstock;
										$productId = $prefix.sku;
										
										digital_data.ecommerce.item[0].id = $productId;
										digital_data.ecommerce.item[0].out_of_stock = $productStatus;
										digital_data.ecommerce.item[0].attribute = $productColor+$productSize+$productFit+$productStyle;
										if(digital_data.ecommerce.item[0].price){
										digital_data.ecommerce.item[0].price = {
											strike_through : $strike, 
												actual : $actual
										};
									}
										
									}
						});
					});
			},
			
			analyticsOnPageCheckoutLoad: function(){
				var $cartData = $('#cartData').val();
				if($cartData) {
				    try {
				      var $data = $.parseJSON($cartData);
				    } catch(e) {
						analytics.analyticsAJAXFailureHandler('checkout','something went wrong','CE');
				        return;
				    }
				}
				
				
				cache.$cartData = {
					ecommerce: {
						action : 'checkout',
						checkout_stage: {
							step : 'stage1|shipping_panel'
						},
						item: [],
						retailer:[]
					}
				};
				analytics.fetchCartData( $data.cartData.retailerInfoData );
			},
			
			analyticsOnOrderConfirmation: function(){
					cache.$orderData = {
						ecommerce: {
							action: 'purchase',
							item : [],
							order : {},
							retailer : []
						}
					};
					var $orderModal = $('#myordersModal'), 
						$orderCode = $('.order-number').data('analytics-ordercode'),
						$orderState = $('.shipping-address').find('p:eq(3)').data('analytics-state'),
						$orderZip = $('.shipping-address').find('p:eq(3)').data('analytics-zip');
						
					if( $orderModal.length > 0 ){
						cache.$orderData.ecommerce.action = 'my_order_page_view';
						$orderCode = $orderModal.find('.order-number').data('analytics-ordercode'),
						$orderState = $orderModal.find('.shipping-address p:eq(2)').data('analytics-state'),
						$orderZip = $orderModal.find('.shipping-address p:eq(2)').data('analytics-zip');
						
					}
					cache.$orderData.ecommerce.order = {
						id : $orderCode,
						state : $orderState,
						zip : $orderZip,
						item_count : '0',
						revenue : {
							exclusive : $('.order-summary-bottom .estimated-sub-price').data('analytics-exclusive'),
							gross_total : $('.order-summary-bottom .last-call-price').data('analytics-gross-total'),
							shipping : $('.order-summary-bottom .estimated-shipping-price').data('analytics-shipping'),
							tax : $('.order-summary-bottom .estimated-tax-price').data('analytics-tax'),
							discount : $('.order-summary-bottom .you-save-price').data('analytics-discount')
						},
						payment : $('.payment-method').data('analytics-payment')
					}
					var retailerData = {}, itemCount = 0;
					
					cache.$document.find('.analytics-orderretailerList').each(function(){
						var $this = $(this),
							$retailerName = $this.find('.confirm-retailer').data('analytics-retailername');
						
						itemCount = itemCount + $this.find('.confirm-retailer').data('analytics-retaileritem');
						retailerData = {
							name : $retailerName,
							item : $this.find('.confirm-retailer').data('analytics-retaileritem'),
							shippingmethod : $this.find('.confirm-retailer').data('analytics-shippingmethod'),
							revenue : {
								exclusive : Number(parseFloat($this.find('.estimated-sub-price').data('analytics-retailer-exclusive')).toFixed(2)),
								gross_total : Number(parseFloat($this.find('.last-call-price').data('analytics-retailer-gross-total')).toFixed(2)),
								shipping : Number(parseFloat($this.find('.estimated-shipping-price').data('analytics-retailer-shipping')).toFixed(2)),
								tax : Number(parseFloat($this.find('.estimated-tax-price').data('analytics-retailer-tax')).toFixed(2)),
								discount : Number(parseFloat($this.find('.you-save-price').data('analytics-retailer-discount')).toFixed(2)),
								retailer_discount : Number(parseFloat($this.find('.confirm-retailer').data('analytics-retailerdiscount')).toFixed(2))
							},
							offer : {}
						};
						if($this.find('.choose-method').data('analytics-retailer-shipping_method')){
							retailerData.shipping_method = $this.find('.choose-method').data('analytics-retailer-shipping_method');
						}
						
						if($this.find('.you-save-spacing').data('analytics-retailer-offercode')){
							retailerData.offer.code = $this.find('.you-save-spacing').data('analytics-retailer-offercode')
						}
						if($this.find('.you-save-spacing').data('analytics-retailer-offertype')){
							retailerData.offer.type = $this.find('.you-save-spacing').data('analytics-retailer-offertype');
						}
						if($this.find('.you-save-spacing').data('analytics-retailer-offerdesc')){
							retailerData.offer.description = $this.find('.you-save-spacing').data('analytics-retailer-offerdesc');
						}
						cache.$orderData.ecommerce.retailer.push(retailerData);
						
						$this.find('.analytics-cartItem').each(function(){
							var $this = $(this),
							itemData = {
								id : $this.find('.item-description').data('analytics-item-id'),
								name : $this.find('.item-description a').data('analytics-item-name'),
								retailer : $retailerName,
								quantity : $this.find('.item-qty').data('analytics-item-quantity'),
								offer : {},
								price : {
									strike_through : parseFloat(($this.find('.hide-mobile .item-price').data('analytics-item-strike-through'))?$this.find('.hide-mobile .item-price').data('analytics-item-strike-through'):0),
									actual : $this.find('.hide-mobile .item-revised-price span').data('analytics-item-price'),
									price_beforediscount : parseFloat(($this.find('.price_beforediscount').val())?$this.find('.price_beforediscount').val():0)
								}
							}
	
							if($this.find('.item-description').data('analytics-item-designer')){
								itemData.designer = $this.find('.item-description').data('analytics-item-designer');
							}
							if($this.find('.item-promotion-spacing').data('analytics-item-offercode')){
								itemData.offer.code = $this.find('.item-promotion-spacing').data('analytics-item-offercode');
							}
							if($this.find('.item-promotion-spacing').data('analytics-item-offertype')){
								itemData.offer.type = $this.find('.item-promotion-spacing').data('analytics-item-offertype');
							}
							if($this.find('.item-promotion-spacing').data('analytics-item-offerdesc')){
								itemData.offer.description = $this.find('.item-promotion-spacing').data('analytics-item-offerdesc');
							}
							
							if( $orderModal.length > 0 ){
								itemData.status = $this.find('.order-track').data('analytics-orderstatus');
							}
							cache.$orderData.ecommerce.item.push(itemData);
							
						});
							
					});
					
					cache.$orderData.ecommerce.order.item_count = itemCount;
			},
			
			getOrderDetails: function(){
				var $satelliteTrack = 'order_tracking',
					$param = {
						event: {
							type : 'order_details'
						},
						ecommerce: {}
					};
				
				analytics.analyticsOnOrderConfirmation();
				$param.ecommerce = cache.$orderData.ecommerce;
				$param.ecommerce.action = 'my_order_page_view';
				
				$.extend(true, digital_data, $param);
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
			
			analyticsOnMySizeItemClick: function(){
				$('#editSizeModal .modal-content#'+$(this).attr('id')).find('form').attr({
					'data-formtype' : $.trim($(this).html().toLowerCase()), 
					'data-formgroup' : $(this).closest('.tab-pane').attr('id'), 
					'data-formcategory' : $(this).closest('.list-group-item').find('h6').html()});
			},
			
			mySizeFormSubmit: function(param){
				var $data = {
					'preference' : $(param).find('.radio-content input[type="radio"]:checked').val(),
					'group' : $(param).data('formgroup'), 
					'category' : $(param).data('formcategory')
				},$param = {};
				if( $(param).data('formtype') === 'edit' ){
					delete digital_data['add_size'];
					$param.edit_size = $data;
				}else if( $(param).data('formtype') === 'add' ){
					delete digital_data['edit_size'];
					$param.add_size = $data;
				}
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					$.extend(true, digital_data, $param);
				}
				
				//Once the data is pushed to the Data Layer, trigger the following function:// 
				_satellite.track("my_size");
			},
			
			analyticsOnPageLoad: function(){
				var $param = {
						status:{ 
							datalayerloaded: false
						},
						banner:{
							view: []
						},
						ecommerce:{
						    list : {},
							item: [],
							recommended_prod_impression: [],
							filter : {}
						}
					},
					$pageType = '';
				
				/*********** PAGE LOAD FUNCTION CALLS MULTIPLE TIMES- so added if condition ****************/	
				if(typeof digital_data.status === 'undefined'){
					var $pageLoad = cache.$document.find('.analytics-pageContentLoad');
					if( $pageLoad.length > 0 ){
							$pageType =  $pageLoad.data('analytics-pagetype');
							$param.page = {
								type : 		$pageLoad.data('analytics-pagetype'),
								content_type : $pageLoad.data('analytics-contenttype')
							}					
							$param.event = {
								sub_type : $pageLoad.data('analytics-subtype')
							}
					}
					var $errorPage = cache.$document.find('.analytics-errorPage');
					if($errorPage.length > 0){
						$param.event = {
							type : $errorPage.data('analytics-errortype'),
							sub_type : $errorPage.data('analytics-errorsubtype')
						}
						$param.error = {
							type : $errorPage.data('analytics-errorname'),
							message : $errorPage.data('analytics-errormsg')
						}
						
						if($errorPage.data('analytics-pagetype')){
							$param.page = {
								type : 		$errorPage.data('analytics-pagetype'),
								content_type : $errorPage.data('analytics-contenttype')
							};
						}
						
					}else if(cache.$document.find('.analytics-trackOrder').length > 0){
						$param.order_tracking = {
							stage : "track_order_initiation"
						};
					}else if(cache.$document.find('.analytics-emailPreferences').length > 0){
						$param.newsletter = {
							stage : "newsletter_initiation"
						};
					}else if( cache.$document.find('#analyticsDataPDP').length > 0 ){				
						cache.$pdpData = {};
						analytics.analyticsOnPagePDPLoad();
					}else if( cache.$document.find('.analytics-cartData').length > 0 ){			
						cache.$cartData = {};	
						analytics.analyticsOnPageCartLoad();
						$param.ecommerce = cache.$cartData.ecommerce;
					}else if( cache.$document.find('.analytics-checkoutData').length > 0 ){
						cache.$cartData = {};
						digital_data.event.sub_type = "step1_shipping_panel";
						analytics.analyticsOnPageCheckoutLoad();
						$param.ecommerce = cache.$cartData.ecommerce;
					}else if( cache.$document.find('.analytics-orderData').length > 0 ){
						$param.ecommerce.action = 'purchase';
						
						analytics.analyticsOnOrderConfirmation();
						$param.ecommerce = cache.$orderData.ecommerce;
						
					}else if( $('#orderDetailPageCount').length > 0 ){
						$param.ecommerce.order_info = [];
						analytics.fetchMyOrders();
						$param.ecommerce.order_info = cache.$myOrdersList;
					}else if( $('.analytics-mysize').length > 0 ){
						$param.ecommerce.size = [];
						var sizesData = {};
								
						$('.mysizes-tabs li').each(function(){
							var $this = $(this);					
							$('.mysizes-tabscontent').find('.tab-pane#Women .list-group').each(function(){							
								sizesData = {
									category : $(this).find('h6').html(),
									group : $this.find('a').data('analyticstabs'),
									preference : $(this).find('p').html()
								};
								$param.ecommerce.size.push(sizesData);
						});
						});					
					}else if( $('.analytics-dealsLanding .deal-area-container').length > 0 ){
						$param.ecommerce.deal = [];
						var dealData = {};
						cache.$document.find('.analytics-deals .deal-details-list').each(function(){
							dealData = {
								'code' : $(this).find('input[type="hidden"].analytics-base-code').val(),
								'retailer' : $(this).find('.gift-details:eq(0)').data('analytics-dealretailer'),
								'discount' : $(this).find('.gift-details:eq(1)').data('analytics-dealdiscount')
							};
							$param.ecommerce.deal.push( dealData );
						});
						
					}
					/**************** FAVORITE STORES ITERATION ********************/
					var listFlag;
					if( cache.$document.find('.analytics-favoriteStores').length > 0 ){
						listFlag = cache.$document.find('.analytics-favoriteStores').data('analytics-length');
							var storesData = {};
						$param.ecommerce.favorite_retailers = [];
						if(listFlag === 'stores'){
							cache.$document.find('.analytics-favoriteStores .store').each(function(){
								storesData = {
									type : 'favorite',
									component : 'stores',
									id : $(this).find('input[type="hidden"].analytics-base-code').val(),
									name : $(this).find('input[type="hidden"].analytics-base-name').val()
								};
								$param.ecommerce.favorite_retailers.push( storesData );
							});
						}
					}
					/**************** FEATURE STORES ITERATION ********************/
					$param.ecommerce.stores = [];
					if( cache.$document.find('.analytics-featureStores').length > 0 ){
						var featureStoreData = {};
						cache.$document.find('.analytics-featureStores').each(function(){
							var $this = $(this);
							if( $this.find('.store').length > 0 ){
								$this.find('.store').each(function(){
									featureStoreData = {
										name : $(this).find('.analytics-storeClick').data('storeval'),
										type : $this.data('analytics-storetype')
									},
									$param.ecommerce.stores.push(featureStoreData);
								});
							}
						});					
					}
					/**************** FAVORITE DESIGNERS ITERATION ********************/
					if( cache.$document.find('.analytics-favoriteDesigners').length > 0 ){
						listFlag = cache.$document.find('.analytics-favoriteDesigners').data('analytics-length');
							var	designersData = {};
						$param.ecommerce.favorite_designers = [];
						if(listFlag === 'designers'){
							cache.$document.find('.analytics-favoriteDesigners .custom-links').each(function(){
								designersData = {
									type : 'favorite',
									component : 'designers',
									id : $(this).find('input[type="hidden"].analytics-base-code').val(),
									name : $(this).find('input[type="hidden"].analytics-base-name').val()
								};
								$param.ecommerce.favorite_designers.push( designersData );
							});
							
						}
					}
					/**************** FAVORITE DEALS ITERATION ********************/
					if( cache.$document.find('.analytics-favoriteDeals').length > 0 ){
						listFlag = cache.$document.find('.analytics-favoriteDeals').data('analytics-length');
							var	dealsData = {};
						$param.ecommerce.favorite_deals = [];
						if(listFlag === 'deals'){
							cache.$document.find('.analytics-favoriteDeals .deal-details-list').each(function(){
								dealsData = {
									type : 'favorite',
									component : 'deals',
									id : $(this).find('input[type="hidden"].analytics-base-code').val(),
									name : $(this).find('input[type="hidden"].analytics-base-name').val()
								};
								$param.ecommerce.favorite_deals.push( dealsData );
							});
						}
					}
					/**************** PRODUCT RECOMMENDATION ********************/
					if(cache.$document.find('.analytics-productRecommended').length > 0){
						$param.ecommerce.recommended_prod_impression = [];
						$('.analytics-productRecommended .analytics-item').each(function( ) {
							var $this = $(this),
								productData = {
										id : $this.find('.base-product-code').val(),
										name : $this.find('.base-product-name').val(),											
										position : $this.data('analytics-productposition'),
										retailer : $this.find('.item-name.analytics-retailer').data('analytics-retailer'),
										designer : $this.find('.item-name.analytics-designer').data('analytics-designer'),
										out_of_stock : $this.data('analytics-productoutofstock')
									};
								if( $this.find('.base-product-category').length > 0 ){
									productData.category = $this.find('.base-product-category').val();
								}
							
							$param.ecommerce.recommended_prod_impression.push( productData );
						});
					}

					$param.ecommerce.designer = [];
					if(cache.$document.find('.analytics-designerAZ').length > 0 ){					
						var designerData = {};
						$('.designer-container .links-component').each(function(){
							if($(this).find('.custom-links').length > 0){
								$(this).find('.custom-links').each(function(){
									designerData = {
										name: $(this).find('input[type="hidden"].base-name').val()
									};
									$param.ecommerce.designer.push(designerData);
								});
							}
						});
					}
					/**************** FEATURE COMPONENTS ********************/
					if(cache.$document.find('.analytics-featuredcomponents').length > 0 ){
						var featureData = {};
						cache.$document.find('.analytics-featuredcomponents').each(function(){
							var $this = $(this);
							if( $this.find('.store').length > 0 ){
								$this.find('.store').each(function(){
									featureData = {
										name : $(this).find('.analytics-storeClick').data('storeval'),
										type : $this.data('analytics-storetype')
									},
									$param.ecommerce.stores.push(featureData);
								});
							}
							if( $this.find('.custom-links').length > 0 ){
								$this.find('.custom-links').each(function(){
									featureData = {
										name : $(this).find('.analytics-designerClick').data('designerval'),
										type : $this.data('analytics-storetype')
									},
									$param.ecommerce.designer.push(featureData);
								});
							}
						});
					}
					/**************** ONLOAD: COMPONENT ITERATION ********************/
					if( $('.analytics-data').length > 0 ){
						$('.analytics-data').each(function(){
							var $this = $(this),
								$data = $this.data('analytics');
							
							if( $data ){
								
								if( $data.hasOwnProperty('banner') ){
									$param.banner.view.push({
										'placement': $pageType+$data.link.placement,
										'name' : $data.link.name,
										'id' : $data.banner.click.id
									});
								}
							}
						});					
					} 
					/**************** PLP ITERATION ********************/
					if($('.analytics-plpList').length > 0){
						if( $('.analytics-plpList .analytics-item').length === 0 ){
							$param.ecommerce.list = {
								type : digital_data.page.type,
								name : digital_data.page.section_1
							};
						}else{
							$('.analytics-plpList .analytics-item').each(function( ) {
								var $this = $(this),
									productData = {
												id : $this.closest('.item').find('.base-product-code').val(),
												name : $this.closest('.item').find('.base-product-name').val(),											
												position : $this.data('analytics-productposition'),
												retailer : $this.find('.item-name.analytics-retailer').data('analytics-retailer'),
												designer : $this.find('.item-name.analytics-designer').data('analytics-designer'),
											out_of_stock : $this.data('analytics-productoutofstock'),
												attribute : 'color:'+$this.find('.analytics-colorattribute').data('analytics-colorattribute')
											};
										if( $this.find('.base-product-category').length > 0 ){
											productData.category = $this.find('.base-product-category').val();
										}
								
								$param.ecommerce.item.push( productData );
							});
							var $plpList = $('.analytics-plpList');
											$param.ecommerce.addtional_prod = {
												start_index : 0
											};
							if( $plpList.data('analytics-ecommerce-searchterm') ){
								$param.ecommerce.search = {
									term : $plpList.data('analytics-ecommerce-searchterm'),
									type : $plpList.data('analytics-ecommerce-searchtype'),
									count : $('#productItem-count').html()
								};
										}
							$param.ecommerce.list = {
								type : $plpList.data('analytics-listype'),
								name : $plpList.data('analytics-listname')
							};
							if( $plpList.data('analytics-filtername') ){
								$param.ecommerce.filter = {
									name : $plpList.data('analytics-filtername')
									}
							}
							$param.ecommerce.sort = {
								name : $plpList.data('analytics-sortname')
							}							
									
							$param.ecommerce.action = $plpList.data('analytics-ecommerce-action');
								}
							}
					
					/**************** ONLOAD: LIST TYPE HANDLING ********************/
					if($(document).find('.col-md-9.analytics-rightContainer').data('analytics-pagelisttype')){
						$param.ecommerce.list.type = $(document).find('.col-md-9.analytics-rightContainer').data('analytics-pagelisttype');
					}
					/**************** ONLOAD: LIST NAME HANDLING ********************/
					if($(document).find('.col-md-9.analytics-rightContainer').data('analytics-pagelistname')){
						$param.ecommerce.list.name = $(document).find('.col-md-9.analytics-rightContainer').data('analytics-pagelistname');
					}
						
					/**************** ONLOAD: form submit handling ********************/
					var $formType = '', $formSubmitted = '', $formFailFlag = false, $formSuccessFlag = false, $formnameType ='', $pageLoadError = false;
					
					if( analytics.urlParam('registration') === "success" ){
						$formSuccessFlag = true;
						$formType = 'registration';
						$formSubmitted = 'registration_form_page';
						$formnameType = 'registration_form';
					}else if( cache.$document.find('.analytics-formSubmitSuccess').length > 0){
						$formSuccessFlag = true;
						if( cache.$document.find('form.analytics-myPageFormSubmit').length > 0 ){
							// ***********************Form success handled by AJAX*********************** //
							$formSuccessFlag = false;
						}else if( cache.$document.find('.analytics-addressBook').length > 0 ){
							$formType = 'registration';
							$formSubmitted = 'add_new_address';
							$formnameType = $formSubmitted;
						}else{
							// formtype & formSubmitted - values will change once MyAccount & Update Center data will available dynamically
							$formType = cache.$document.find('form.analytics-formValidation').data('analytics-type');
							$formSubmitted = cache.$document.find('form.analytics-formValidation').data('analytics-formname');
							$formnameType = $formSubmitted;
						}
					}
					if($('#analyticsLoginSuccessFlag').val()){
						$formSuccessFlag = true;
						$formType = 'login';
						$formSubmitted = 'loginform';
						$formnameType = 'login_form';
					}
					if($formSuccessFlag){
						$param.event = {
							sub_type : 'login'
						};
						$param[$formType] = {
							stage : 'completion',
							name : $formSubmitted,
							type : $formnameType
						};
					}
					if(cache.$document.find('.analytics-registerForm').length > 0){
						$formSubmitted = cache.$document.find('.register__section form.analytics-formValidation').data('analytics-formname');
						$formnameType = 'registration_form';
						$param.registration = {
							stage : "initiation",
							name : $formSubmitted,
							type : $formnameType
						};
						digital_data.event.sub_type = 'registration';
						if( analytics.urlParam('registration') === "failure" ){		
							$formFailFlag = true;
							$formType = cache.$document.find('.register__section form.analytics-formValidation').data('analytics-type');
							
						}
					}else if(cache.$document.find('.analytics-loginPage').length > 0){
						$formSubmitted = 'loginform_page';
						$formnameType = 'login_form';
						$param.login = {
							stage : "initiation",
							name : $formSubmitted,
							type: $formnameType
						};
						if( analytics.urlParam('error') === "true" ){
							$formFailFlag = true;
							$formType = 'login';
						}
					}else if(cache.$document.find('.analytics-checkoutLogin').length > 0){
						$formSubmitted = 'checkout_login_form_page';
						$formnameType = 'login_form';
						$param.login = {
							stage : "initiation",
							name : $formSubmitted,
							type: $formnameType
						};
						digital_data.event.sub_type = 'login';
						if( analytics.urlParam('error') === "true" || $('span.has-error').length > 0){
							$formFailFlag = true;
							$formType = 'login';
						}
					}else if( cache.$document.find('.analytics-formSubmitFailure').length > 0){
							$formFailFlag = true;
							if( cache.$document.find('.analytics-myPages').length > 0 ){
								// ***********************Form error handled by AJAX - My Designers & My Stores*********************** //
								/* ***********************
									ESB DOWN ERROR ON PAGE LOAD
									MY DESIGNERS,MY DEALS , MY FAVORITES, MY STORES,MY SIZES							
								*********************** */
								$formFailFlag = false;
								$pageLoadError = true;
								
							}else if( cache.$document.find('.analytics-addressBook').length > 0 ){
								$formType = 'registration';
								$formSubmitted = 'add_new_address';
								$formnameType = $formSubmitted;
							}else{
								// formtype & formSubmitted - values will change once MyAccount & Update Center data will available dynamically
								$formType = cache.$document.find('form.analytics-formValidation').data('analytics-type');
								$formSubmitted = cache.$document.find('form.analytics-formValidation').data('analytics-formname');
								$formnameType = $formSubmitted;
							}
					}
					// ***********************Form submit error handling*********************** //
					if($formFailFlag){
						$param.event = {
							type : $formType,
							sub_type : $formType+'_failure'
						};
						$param[$formType] = {
							stage : 'error',
							name : $formSubmitted,
							type : $formnameType
						};
						$param.form_error = {
							type : $.trim(cache.$document.find('.analytics-formSubmitFailure').text()),
							name : $formSubmitted
						};
					}
					// ***********************ESB down error handling*********************** //
					if($pageLoadError){
						$param.error = {
							type : 'CE',
							message : $.trim(cache.$document.find('.analytics-formSubmitFailure').text())
						}
						$param.esbdown = true;
					}
					$.extend(true, digital_data, $param);
					digital_data.status.datalayerloaded = true;
				}
            },
			
			analyticsOnPageMiniBagHover: function(response){
			
				var $data = response,
					$satelliteTrack = "mini_bag",
					$param = {
						"event": {
							"type": "mini_bag_overlay",
							"sub_type": "mini_bag"
						},
					"ecommerce":{
						"bag":{
							"type" : "mini_bag"
						},
						"item": []
					},
						"link" : {
							"placement": "mini_bag", 
							"name": "mini_bag_overlay"
						}
				}, productArray = [];
				
				cache.$miniBagData = {
					ecommerce: {
						item:[]
					}
				};
				if(typeof $data === 'undefined'){
					$param.link = $('.analytics-miniBagEmpty').data('analytics');
				}else{
					if( $data.retailerInfoData !== null ){
						if($data.retailerInfoData.length > 0){	
							$.each($data.retailerInfoData,function(key,value){	
								$.each($data.retailerInfoData[key],function(key1,value1){
									
									 if( key1 === "productDetails" ){
										var $productArr = $data.retailerInfoData[key][key1];
										$.each($productArr,function(i,j){
											
											var $attribute = [],
												$strike = 0,
												$actual = 0;
												
											if( $productArr[i].msrpValue ){
												$strike = $productArr[i].msrpValue.value;
												if($productArr[i].saleValue){
													$actual = $productArr[i].saleValue.value;
											}else{
													$actual = $productArr[i].listValue.value;
												}
											}else{
												if($productArr[i].saleValue){
													$strike = $productArr[i].listValue.value;
													$actual = $productArr[i].saleValue.value;
												}else{
													$actual = $productArr[i].listValue.value;
												}
											}
											
											$.each($productArr[i].product.variantValues,function(m,n){
												$attribute.push(m+':'+n);
											});
											productArray.push({
												'id' : $productArr[i].product.code,
												'name' : $productArr[i].product.name,
												'price' : { 'strike_through' : $strike, 'actual' : $actual },
												'retailer' : $data.retailerInfoData[key].retailerName,
												'designer' : $productArr[i].baseDesignerName,
												'quantity' : $productArr[i].quantity,
												'favorite_status' : $productArr[i].favorite
											});
										});
									}
								});
							});
						}else{
							$param.link = $('.analytics-miniBagEmpty').data('analytics');
						}
					}else{
						$param.link = $('.analytics-miniBagEmpty').data('analytics');
					}				
				}
				
				if(productArray.length > 0){
					$param.ecommerce.item = productArray;
					cache.$miniBagData.ecommerce.item = $param.ecommerce.item;
				}else{
					cache.$miniBagData.ecommerce.item = [];
				}
				$.extend(true, digital_data, $param);
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
				_satellite.track($satelliteTrack);
				}
				
			},
             
            analyticsOnPageProductClick: function(event){
				var $this = $(this),
					$parent = $this.closest('.analytics-productList'),
					$satelliteTrack = $parent.data('satellitetrack'),
					$prod_click = $parent.data('analytics-ecommerce-clickaction'),
					$prod_position = $(this).index(),
					$data = {
						event:{
						  sub_type : $parent.data('analytics-eventtype'),
						  type     : $parent.data('analytics-eventsubtype')
						},
						ecommerce : {
							action: $parent.data('analytics-ecommerce-clickaction'),
							list:{
								type : $parent.data('analytics-listype'),
								name : $parent.data('analytics-lisname')
							}
						},
						link:{
							placement : $parent.data('analytics-linkplacement'),
							name : $this.data('analytics-linkname')
						}
					};
					$data.ecommerce[$prod_click] = {
						id : $this.closest('.item').find('.base-product-code').val(),
						name : $this.closest('.item').find('.base-product-name').val(),											
						position : ($prod_position+1),
						retailer : $this.find('.item-name.analytics-retailer').data('analytics-retailer'),
						designer : $this.find('.item-name.analytics-designer').data('analytics-designer'),
						out_of_stock : $this.data('analytics-productoutofstock')
					}
				
				if( $this.find('.base-product-category').length > 0 ){
					$data.ecommerce[$prod_click].category = $this.find('.base-product-category').val();
				} 
				$.extend(true, digital_data, $data);
				
				//Once the data is pushed to the Data Layer, trigger the following function://
				if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					_satellite.track($satelliteTrack);
				}
				
			},			
			
            analyticsOnPageComponentClick: function(e){
				
            	var $this = e;
				if( e.type === 'click' ){
					$this = $(this);
				}
				 
            	var $param = $this.data('analytics'),
					$satelliteTrack = $this.data('satellitetrack');
            

            	var $options = {};
				
				if( $param.hasOwnProperty('event') ){
					var $event = {
							event : {
								type     : $param.event.type,
								sub_type : $param.event.sub_type
							}
						};
					$options.event = $event.event;
				}
				if( $param.hasOwnProperty('banner') ){
					var $promoBanner = {
							banner:{
								click :{
									id  :  $param.banner.click.id
								}
							}
						};
					$options.banner = $promoBanner.banner;
				}
				if( $param.hasOwnProperty('carousel') ){
					var $carousel = {
							carousel:{
								click :{
									id  :  $param.carousel.click.id,
									placement  :  $param.carousel.click.placement,
									name  :  $param.carousel.click.name
								}
							}
						};
					$options.carousel = $carousel.carousel;
				}
	         	
				var $links = {
						link: { 
							placement   :  $param.link.placement,                  
							name        :  $param.link.name   
						}
					}
				
				$options.link = $links.link;
        
         		$.extend(true, digital_data, $options);
         		
                //Once the data is pushed to the Data Layer, trigger the following function:// 
                if($('#analyticsSatelliteFlag').val()==='true' && typeof _satellite != "undefined"){
					if($satelliteTrack){
						_satellite.track($satelliteTrack);
					}else{
						_satellite.track("internal_click");
					}
                }
            	 
            	 
            },
						
			urlParam: function(name){
				var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href), 
					$return= "";

				if( results !== null ){
					$return= results[1];
				}
				return $return;
			}
		}
        $(function() {
            analytics.init();
			
        });

        return analytics;
    });