<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="error-container analytics-errorPage" data-analytics-pagetype="page_not_found" data-analytics-subtype="page_not_found" data-analytics-contenttype="informational" data-analytics-errortype="error" data-analytics-errorsubtype="404error" data-analytics-errormsg="page_not_found" data-analytics-errorname="404error">
	    <div class="error-title-container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<cms:pageSlot position="CSN_404ErrorMainMessageSection" var="feature">
						<cms:component component="${feature}" element="div" class="span-24 section1 cms_disp-img_slot"/>
					</cms:pageSlot>
				</div>
			</div>
			<!-- <div class="row">
				<div class="col-xs-12 col-md-12"><h3>Page not found or no longer exists.</h3></div>
			</div> -->
			<div class="row">
				<div class="col-xs-12 col-md-12">
	 				<cms:pageSlot position="CSN_404ErrorSubMessageSection" var="feature">
						<cms:component component="${feature}" element="div" class="span-24 section1 cms_disp-img_slot"/>
					</cms:pageSlot>
	            </div>
			</div>
	    </div>
	    <!--error page custom component similar to homepage-->
		<div class="error-image-container">
	      
			<div class="custom-component">
				<div class="row">
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="CSN_404ErrorpageImagelinkASection" var="feature" >
							<div class="img-container">
								<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>
			
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="CSN_404ErrorpageImagelinkBSection" var="feature" >
							<div class="img-container">
								<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>
			
					<div class="columns col-xs-12 col-md-4">
						<cms:pageSlot position="CSN_404ErrorpageImagelinkCSection" var="feature" >
							<div class="img-container">
								<cms:component component="${feature}"/>
							</div>	
						</cms:pageSlot>
					</div>
				</div>
			</div>
	
		</div>
					
	    <!--error page custom component similar to homepage-->
	</div>
	</div>

</template:page>