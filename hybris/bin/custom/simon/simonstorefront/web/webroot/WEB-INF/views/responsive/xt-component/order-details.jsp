<div class="modal fade order-detail-container-modal" id="myordersModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="order-detail-container">
	                <div class="content-space">
						<div class="myorder-details">
							<div class="major">Simon Premium Outlets Order Number</div>
							<div class="normal-text ">4DSF45FSN56</div>
						</div>
						<div class="myorder-details">
							<div class="major">Order Date</div>
							<div class="normal-text">March 8, 2016</div>
						</div>
					</div>
	                <div class="content-space">
						<div class="myorder-details">
							<div class="major">Shipping Address</div>
							<div class="normal-text">
								<p>Lisa Simone</p>
								<p>18 Bull Path Close</p>
								<p>East Hampton, NY 11937</p>
							</div>
						</div>
						<div class="myorder-details">
							<div class="major">Payment Method</div>							
							<div class="normal-text">VISA ending in 7581</div>
						</div>
					</div>
	                <div class="content-space last-call">
						<div class="major">Last Call (2)</div>
						<div class="normal-text">
							<div class="return-information"><a class="viewReturnPolicy" data-href="/contentPolicy/testShop1">View Shipping, Returns &amp; Privacy Information</a></div>
							<div class="order-track">
								<div class="bold-txt">Track Order</div>
								<div class="customer-service">Contact Customer Service</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Status</div>
								<div class="status shipped">Shipped</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Number</div>
								<div>--</div>
							</div>
							<div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-1 hide-mobile"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2 col-xs-offset-9 "><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                      </div>
                                      <div class="subtotal hide-desktop">$160.00</div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Red</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$200.00</div>
                                      <div class="item-revised-price">$160.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2 hide-mobile">
                                    <div class="subtotal">$160.00</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description">
                                   		<a href="#">Asymmetric-Peplum Midi Dress</a>
	                                   <div class="item-additional-price">
	                                     Additional 20% Off
	                                   </div>
                                   </div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$250.00</div>
                                      <div class="item-revised-price">$200.00
                                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                      </div>
                                      <div class="subtotal hide-desktop">$200.00</div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Coral Red</div>
                                   <div class="item-color">Size: 2</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$250.00</div>
                                      <div class="item-revised-price">$200.00
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2 hide-mobile">
                                    <div class="subtotal">$200.00</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-8 col-xs-offset-4">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$360.00</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">7.50%</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">$28.32</span>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Last Call Estimated Total</span>
                                         <span class="last-call-price">$395.82</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$80.00</span>
                                      </div>
                                   	</div>
                                   </div>
                                </div>
                             </div>

                          </div>
						</div>
					</div>
	                <div class="content-space last-call">
						<div class="major">Gap Factory (1)</div>
						<div class="normal-text">
							<div class="return-information"><a class="viewReturnPolicy" data-href="/contentPolicy/testShop1">View Shipping, Returns &amp; Privacy Information</a></div>
							<div class="order-track">
								<div class="bold-txt">Track Order</div>
								<div class="customer-service">Not shipped yet</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Status</div>
								<div class="status processing">Processing</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Number</div>
								<div>1234ABCD1234</div>
							</div>
							<div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-1 hide-mobile"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2 col-xs-offset-9 "><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Print Fringe Scarf</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$19.99</div>
                                      <div class="item-revised-price">$15.99
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                      </div>
                                      <div class="subtotal hide-desktop">$15.99</div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Blue</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$19.99</div>
                                      <div class="item-revised-price">$15.99
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2 hide-mobile">
                                    <div class="subtotal">$15.99</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-8 col-xs-offset-4">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">$15.99</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">$3.00</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">$2.01</span>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Gap factory Estimated Total</span>
                                         <span class="last-call-price">$21.00</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">$1.00</span>
                                      </div>
                                   	</div>
                                   </div>
                                </div>
                             </div>

                          </div>
						</div>
					</div>
	                <div class="content-space last-call final-estimation">
						<div class="major">Nordstrom Rack (2)</div>
						<div class="normal-text">
							<div class="return-information"><a class="viewReturnPolicy" data-href="/contentPolicy/testShop1">View Shipping, Returns &amp; Privacy Information</a></div>
							<div class="order-track">
								<div class="bold-txt">Track Order</div>
								<div class="customer-service">--</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Status</div>
								<div class="status cancel">Cancelled - Retailer Out of Stock</div>
							</div>
							<div class="order-track">
								<div class="bold-txt">Order Number</div>
								<div>1234ABCD4321</div>
							</div>
							<div class="cart-items-container checkout-accordian">
                             <div class="return-information">Items</div>
                             <div class="checkout-item-heading">
                                <div class="row">
                                   <div class="col-md-offset-8 col-md-2 col-xs-1 hide-mobile"><div class="price-heading hide-mobile">Item Price</div></div>
                                   <div class="col-md-2 col-xs-2 col-xs-offset-9 "><div class="total-heading">Subtotal</div></div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Sterling Silver Diamond & Bar Charm Double Drop Necklace - 0.08 ctw</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$36.21</div>
                                      <div class="item-revised-price">$28.97
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                      </div>
                                      <div class="subtotal hide-desktop">$28.97</div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Crystal</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$36.21</div>
                                      <div class="item-revised-price">$28.97
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2 hide-mobile">
                                    <div class="subtotal">$28.97</div>
                                </div>
                             </div>
                             <div class="row clearfix row-pad-btm">
                                <div class="col-md-2 col-xs-4">
                                   <a href="#">
                                      <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
                                   </a>
                                </div>
                                <div class="col-md-6 col-xs-8 row-pad-l">
                                   <div class="item-description"><a href="#">Women's Retro Composite Frame Sunglasses</a></div>
                                   <div class="hide-desktop">
                                      <div class="item-price">$112.46</div>
                                      <div class="item-revised-price">$89.97
                                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                      </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                   <div class="subtotal hide-desktop">$89.97</div>
                                   <div class="item-qty">Quantity: 1</div>
                                   <div class="item-color">Color: Havana-Light</div>
                                </div>
                                <div class="col-md-2 col-xs-2">
                                  <div class="hide-mobile">
                                      <div class="item-price">$112.46</div>
                                      <div class="item-revised-price">$89.97
                                       <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
                                    </div>
                                      <div class="item-discount">20% Off</div>
                                   </div>
                                </div>
                                <div class="col-md-2 col-xs-2 hide-mobile">
                                    <div class="subtotal">$89.97</div>
                                </div>
                             </div>

                             <div class="row">
                                <div class="col-md-offset-2 col-md-10 col-xs-8 col-xs-offset-4">
                                   <div class="price-container-left">
                                    <div class="price-calculation-container">
                                      <div class="price-top-spacing">
                                         <span class="estimated-subtotal">Estimated Subtotal</span>
                                         <span class="estimated-sub-price">--</span>
                                      </div>
                                      <div class="ship-top-spacing">
                                         <span class="estimated-shipping">Estimated Shipping</span>
                                         <span class="estimated-shipping-price">--</span>
                                      </div>
                                      <div class="choose-method">
                                         <a herf="#">Premium Shipping</a>
                                      </div>
                                      <div class="tax-top-spacing">
                                         <span class="estimated-tax">Estimated Tax</span>
                                         <span class="estimated-tax-price">--</span>
                                      </div>
                                      <div class="last-call-spacing">
                                         <span class="last-call-retailer">Nordstrom Rack Estimated Total</span>
                                         <span class="last-call-price">--</span>
                                      </div>
                                      <div class="you-save-spacing">
                                         <span class="you-save">You Saved</span>
                                         <span class="you-save-price">--</span>
                                      </div>
                                   	</div>
                                   </div>
                                </div>
                             </div>

                          </div>
						</div>
						<div class="cart-checkout-allitems">
                              <div class="row">
                                <div class="col-md-12 col-xs-12">
                                     <div class="price-container-left cart-checkout-items">
                                      <div class="price-calculation-container">
                                        <div class="price-top-spacing">
                                           <span class="estimated-subtotal">Estimated Order Subtotal</span>
                                           <span class="estimated-sub-price">$516.35</span>
                                        </div>
                                        <div class="ship-top-spacing">
                                           <span class="estimated-shipping">Combined Shipping Charges</span>
                                           <span class="estimated-shipping-price">$16.00</span>
                                        </div>
                                      
                                        <div class="tax-top-spacing">
                                           <span class="estimated-tax">Estimated Taxes</span>
                                           <span class="estimated-tax-price">$75.43</span>
                                        </div>
                                        <div class="last-call-spacing">
                                           <span class="last-call-retailer"> Estimated Order Total</span>
                                           <span class="last-call-price">$607.78</span>
                                        </div>
                                        <div class="you-save-spacing">
                                           <span class="you-save">You Saved</span>
                                           <span class="you-save-price">$94.00</span>
                                        </div>
                                        
                                     </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          <div class="cart-help-container">
							<div class="help-info">Helpful Information</div>
								<div class="information">Please note Simon places orders on your behalf for
								each retailer you shopped at; you will see these as
								separate transactions on your credit card
								statement.
								</div>
								<div class="question">Question?<a href="#">Contact Us</a></div>
							<div class="more-details">
								<a href="#">See Shipping Methods &amp; Charges for More Details</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-xs-12 close-popup">
								<button type="button" class="btn">close</button>
							</div>
						</div>
					</div>
				</div>
            </div>
            <div class="modal-footer show-mobile">
                 <button class="btn plum">Update Item</button>
            </div>
        </div>
    </div>
</div>