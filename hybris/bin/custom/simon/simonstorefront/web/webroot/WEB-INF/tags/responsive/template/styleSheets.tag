<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>


<c:choose>
	<c:when test="${pageStyleId != null}">
<!-- customized PLD CSS start-->
	<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/_ui/responsive/simon-theme/css/global.css"/>
	<c:choose>
	<c:when test="${pageStyleId != 'error'}">
		<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/_ui/responsive/simon-theme/css/${pageStyleId}.css"/>	
	</c:when>
	</c:choose>
	
<!-- customized PLD CSS ends-->
	</c:when>
	<c:otherwise>
<!-- Out of the box -->
<%-- Theme CSS files --%>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" />
<c:choose>
	<c:when test="${wro4jEnabled}">
		<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/wro/all_responsive.css" />
		<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/wro/${themeName}_responsive.css" />
		<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/wro/addons_responsive.css" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style.css"/>
		<c:forEach items="${addOnCommonCssPaths}" var="addOnCommonCss">
			<link rel="stylesheet" type="text/css" media="all" href="${addOnCommonCss}"/>
		</c:forEach>
	</c:otherwise>
</c:choose>

<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
</c:forEach>

<%-- Theme CSS files ends --%>

	<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/_ui/responsive/simon-theme/css/global.css"/>
	<link rel="stylesheet" type="text/css" media="all" href="${contextPath}/_ui/responsive/simon-theme/css/homepage.css"/>

</c:otherwise>
</c:choose>




<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />