<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:choose>
	<c:when test="${heroBanner.fontColor eq 'WHITE'}">
    	<c:set var="fontColor" value="active" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>
<c:set var="imageDestinationLink" value="${heroBanner.backgroundImage.destinationLink}" />
<c:set var="backgroundImageItem" value="${heroBanner.backgroundImage}" />
<c:set var="desktopImage" value="${backgroundImageItem.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty backgroundImageItem.mobileImage}">
    	<c:set var="mobileImage" value="${backgroundImageItem.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<div class="container">
	<div class="the-designer-shop ${fontColor}">
		<a href="${imageDestinationLink.url}">
			<picture>
				<source media="(max-width: 991px)" srcset="${mobileImage}">
				<img class="img-responsive" src="${desktopImage}" alt="${heroBanner.backgroundImage.imageDescription}">
			</picture>
		</a>
		<div class="content-container">
			<h1 class="display-medium major">${heroBanner.titleText}</h1>
			<div class="designer-banner-text">${heroBanner.subheadingText}</div>
			<c:if test="${not empty heroBanner.link && not empty heroBanner.link.linkName}" >								
				<c:choose>
					<c:when test="${heroBanner.link.target eq 'NEWWINDOW'}">
				    	<c:set var="target" value="_blank" />
					</c:when>
				    <c:otherwise>
						<c:set var="target" value="_self" />
					</c:otherwise>
				</c:choose>	
				<a target=${target} href="${heroBanner.link.url}">
					<span class="shopnow major">${heroBanner.link.linkName}&nbsp;<span>&rsaquo;</span></span>
				</a>
			</c:if>
		</div>
	</div>
</div>
