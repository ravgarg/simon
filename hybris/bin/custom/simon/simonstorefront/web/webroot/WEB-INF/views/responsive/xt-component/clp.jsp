<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>
<div class="category-landing">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<jsp:include page="clp-breadcurmb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="plp-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">
				<jsp:include page="clp-top-heading.jsp"></jsp:include>
				<jsp:include page="clp-heading-desc-and-links.jsp"></jsp:include>
				<div class="clp-main-carousel">
					<jsp:include page="clp-hero-carousal.jsp"></jsp:include>
				</div>
				<jsp:include page="clp-trending-now.jsp"></jsp:include>
				<jsp:include page="homepage-promotion-banner.jsp"></jsp:include>
				<jsp:include page="the-designer-shop.jsp"></jsp:include>
				<jsp:include page="trending-now.jsp"></jsp:include>
				<jsp:include page="clp-the-active-shop.jsp"></jsp:include>
				<jsp:include page="style-deals.jsp"></jsp:include>
				<jsp:include page="designers-deals.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<jsp:include page="social-shop.jsp"></jsp:include>
</div>