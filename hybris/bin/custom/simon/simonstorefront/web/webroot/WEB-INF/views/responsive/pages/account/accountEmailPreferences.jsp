<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="email-preferences row analytics-emailPreferences">
	<div class="email-content col-md-12">
		<p><spring:theme code="account.email.preferences.paragraph.notintoit.text" /></p>
		
		<div class="row">
			<c:url value="/my-account/update-email-preferences" var="updateEmailPreferenceActionUrl" />
				<form:form action="${updateEmailPreferenceActionUrl}" class="analytics-formValidation" data-analytics-type="newsletter" data-analytics-eventtype="newsletter" data-analytics-eventsubtype="email-preferences" data-analytics-formtype="newsletter" data-analytics-formname="email-preferences" data-satellitetrack="newsletter" method="post" commandName="updateUserForm" id="updateEmailPreferences">	
				<label class="checkboxes col-md-12" for="optedInEmail"> 
					<span class="sm-input-group field-float">
					<c:choose>
						<c:when test="${optedInEmail eq false}">
							<input type="checkbox" name="optedInEmail" id="optedInEmail">	
						</c:when>
						<c:otherwise>
							<input type="checkbox" name="optedInEmail" id="optedInEmail" checked="checked">
						</c:otherwise>
					</c:choose> 
					
				</span> <spring:theme code="account.email.preferences.unsubscribe.from.shop.text" />
				</label>
				<div class="submit-btn col-md-6 col-xs-12">
					<button class="btn analytics-genericLink" data-analytics-eventsubtype="email-preference" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|email-preference" data-analytics-linkname="email-preference" type="submit" id="updateForm"><spring:theme code="account.email.preferences.button.save" /></button>
				</div>
			</form:form>
		</div>
	</div>
</div>