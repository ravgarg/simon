<div class="container">
	<div class="row">
		<div class="col-md-12">
			<jsp:include page="myprofile-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="myprofile-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<jsp:include page="myprofile-top-heading.jsp"></jsp:include>
			<jsp:include page="myprofile-content.jsp"></jsp:include>
		</div>
	</div>
</div>