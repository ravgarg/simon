define(['hbshelpers','handlebars',  'ajaxFactory', 
        'templates/pdpJustAddedModal.tpl','analytics'],
function(hbshelpers,  handlebars,ajaxFactory, 
        pdpJustAddedmodal, analytics) {
    'use strict';
    var cache = {
            $document : $(document)
        },
        pdpAddtocart = {
            init: function() {
                this.initEvents();
                
            },          
            initEvents: function() {     
                cache.$document.on('click','.addtocart',pdpAddtocart.addtocartModal)
                
            },

            addtocartModal:function(event){
                event.preventDefault();
                    var modalUrl        = $(this).data('apiurl'),
                        CSRFToken       = $("input[name=CSRFToken]").val(),
                        qty             = $("input[name=qty]").val(),
                        productCodePost = $("input[name=productCodePost]").val();
                    var formData ={
                        "productCodePost": productCodePost,
                        "qty": qty,
                        "CSRFToken":CSRFToken
                    }
                     var options = {
                             'methodType': 'POST',
                             'dataType': 'JSON',
                             'url': modalUrl,
                             'methodData': formData,
                             'isShowLoader': false,
                             'cache' : true
                         }
                         ajaxFactory.ajaxFactoryInit(options, function(response){
                            if(response.statusCode!=='success'){
                                $('#error').empty().text(response.errorMessage).show();
                                $('#error').removeClass('hide');
								analytics.analyticsAJAXFailureHandler('addtobag',response.errorMessage,'CE');
                                return false;
                            }else{
								if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.analyticsOnPageAddToBagClick();
								}
							}
                            $('#error').hide();
                            $('#justAddedModal').modal('show').find('.just-added-items').html(pdpJustAddedmodal(response));
                            $('.minicart').addClass('active');
                            $('.minicart span').html(response.itemCount);

                            $('.tooltip-init').tooltip();

                         }, '','addtobag');
            }
            
        };
    $(function() {
            pdpAddtocart.init();
        });
   return pdpAddtocart;
});