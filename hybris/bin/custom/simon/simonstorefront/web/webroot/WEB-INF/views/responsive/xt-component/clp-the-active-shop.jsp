
	<div class="the-designer-shop">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/banner-image1@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/banner-image1.png">
			</picture>
			<div class="content-container">
				<h1 class="display-medium major">the active shop</h1>
				 <div class="designer-banner-text">Calvin Klein, Tommy Hilfinger & More</div>
				 <span class="shopnow major">shop now <span>&rsaquo;</span></span> 
			</div>
		</a>
	</div>
