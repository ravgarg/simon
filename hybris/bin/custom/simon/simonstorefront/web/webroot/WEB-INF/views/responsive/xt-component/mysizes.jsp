<div class="mysizes">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<jsp:include page="my-sizes-breadcrumb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="plp-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">
				<jsp:include page="my-sizes-top-heading.jsp"></jsp:include>
				
				<div class="page-context-links">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.
				</div>
				  
				<!--- 
					Mobile view is not done which is accoridan view .... 
					reference url :- https://www.npmjs.com/package/bootstrap-responsive-tabs 
				---->
				<jsp:include page="my-sizes-tabs.jsp"></jsp:include>
			</div>

			
	</div>
	</div>
</div>