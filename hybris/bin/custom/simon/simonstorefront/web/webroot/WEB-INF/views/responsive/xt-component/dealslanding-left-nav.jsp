<ul class="left-menu level-0 list-unstyled">
    <li><a href="#">Featured Deals</a></li>
    <li><a href="#">New Arrivals</a></li>
    <li><a href="#">Designers</a></li>
    <li><a href="#">Baby</a></li>
    <li><a href="#">Girls</a></li>
    <li><a href="#">Boys</a></li>
    <li><a href="#">The Toy Shop</a></li>
    <li class="has-level-1">
        <a href="#subMenuL1Id10" class="collapsed" data-toggle="collapse">Categories</a>
        <ul class="level-1 collapse" id="subMenuL1Id10">
            <li><a href="#">Categories</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">Categories</a></li>
        </ul>
    </li>
    <li class="has-level-1">
        <a href="#subMenuL1Id10" class="collapsed" data-toggle="collapse">Women</a>
        <ul class="level-1 collapse" id="subMenuL1Id10">
            <li><a href="#">Women</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Women</a></li>
            <li><a href="#">Women</a></li>
        </ul>
    </li>
    <li class="has-level-1">
        <a href="#subMenuL1Id10" class="collapsed" data-toggle="collapse">Men</a>
        <ul class="level-1 collapse" id="subMenuL1Id10">
            <li><a href="#">Men</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Men</a></li>
            <li><a href="#">Men</a></li>
        </ul>
    </li>
</ul>