<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="enum" uri="http://simon.com/tld/enum"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
		<c:forEach items="${links}" var="link" varStatus="ctr">


			<a class="btn analytics-genericLink" data-analytics-eventsubtype="${fn:toLowerCase(link.linkName)}" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|${fn:toLowerCase(link.linkName)}" data-analytics-linkname="${fn:toLowerCase(link.linkName)}" type="button" target="${target}" href="${link.url}">
				<c:if test="${not empty link && not empty link.linkName}">

					<c:choose>
						<c:when test="${link.target eq 'NEWWINDOW'}">
							<c:set var="target" value="_self" />
						</c:when>
						<c:otherwise>
							<c:set var="target" value="_blank" />
						</c:otherwise>
					</c:choose>
					${link.linkName}
				</c:if>



			</a>
		</c:forEach>




