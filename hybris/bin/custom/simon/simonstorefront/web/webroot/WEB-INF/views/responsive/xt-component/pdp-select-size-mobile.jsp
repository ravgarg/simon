<div class="modal fade" id="selectSizeMobile" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content just-added">
            <div class="modal-header">
                <h5 class="modal-title">Select Size</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
			<form>
				<div class="just-added-items">
						
					<div class="select-size-container">
						<div class="size-links">
							<h6>Selected Size: <span>32</span><span class="size-guide-link"><a href="#" data-toggle="modal" data-dismiss="modal" data-target="#sizeGuideModal">Size Guide</a></span></h6>
							<div id="sizePaletteMobile" class="size-palette clearfix">
								<a href="#">30</a>
								<a href="#" class="active">31</a>
								<a href="#">32</a>
								<a href="#">33</a>
								<a href="#" class="crossed">34</a>
								<a href="#">36</a>
								<a href="#">38</a>
								<a href="#">40</a>
								<a href="#">42</a>
								<a href="#">46</a>
								<a href="#">50</a>
							</div>
						</div>
					</div>

				</div>
			</form>

			<div class="modal-footer clearfix">
                <button class="btn pull-left" data-dismiss="modal">Close</button>
            </div>
			
            
        </div>
    </div>
</div>