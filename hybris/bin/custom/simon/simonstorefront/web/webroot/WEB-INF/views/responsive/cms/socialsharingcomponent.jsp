<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${endpoint == 'EMAIL'}">
		<a data-analytics-linkname="${media.altText}" href="#shareViaEmail" data-toggle="modal" title="" id="${endpoint}">
			<img src="${media.url}" alt="${media.altText}">
		</a>
	</c:when>
	<c:otherwise>
		<a data-analytics-linkname="${media.altText}" href="${urlLink}" title="" data-href="${urlLink}" id="${endpoint}"> <img
			src="${media.url} " alt="${media.altText}">
		</a>
	</c:otherwise>
</c:choose>