<div class="confirm">
    <div class="hide-mobile">
        <jsp:include page="checkout-the-active-shop.jsp"></jsp:include>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4 order-summary-right">
                 <jsp:include page="checkout-confirmation-right-order-summary.jsp"></jsp:include>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="confirmation-heading">
                    <div class="thank-you">Thank you.</div>
                    <div class="order-number">Order #1337 processing at Shop Premium Outlets</div>
                    <div class="order-receipt">A confirmation receipt will be sent to lisa.simone@gmail.com from Shop Premium Outlets and the store(s) where you placed your order.</div>
                </div>
                <div class="account-created-heading">
                    <div class="account-success-text">Account Created Successfully</div>
                    <div class="account-success-confirm">Your account for lisa.simone@gmail.com has been created. Your order has been saved to your account.</div>
                </div>
                <div class="account-already-exist">
                    <div class="already-exist-text">An Account Associated with that Email Already Exists</div>
                    <div class="already-exist-msg">An account for lisa.simone@gmail.com already exists. To access your account please <a href="#">login</a>.</div>
                </div>
                <div class="confirmation-create-account">
                    <div class="create-account-heading">Create an Account</div>
                    <div class="create-account-msg">Create an account for lisa.simone@gmail.com from Shop Premium Outlets and the store(s) where you placed your order.</div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12 sm-input-group">
                            <input type="text" class="form-control" name="password" placeholder="ENTER PASSWORD" title="ENTER PASSWORD">
                        </div>
                        <div class="col-md-6 col-xs-12 sm-input-group">
                            <input type="password" class="form-control" name="confirmpassword" autocomplete="off" placeholder="CONFIRM PASSWORD" title="CONFIRM PASSWORD">
                        </div>
                        <div class="col-md-12 col-xs-12 btn-create-account"><button class="btn col-md-3 col-xs-12">CREATE ACCOUNT</button></div>
                    </div>
                </div>
                <div class="shipping-payment-method">
                   <div class="row">
                      <div class="col-md-4 col-xs-6">
                        <div class="shipping-heading major">
                            Shipping Address
                         </div>
                        <div class="shipping-address">
                            Lisa Simone
                            18 Bull Path Close
                            East Hampton, NY 11937
                         </div>
                      </div>
                      <div class="col-md-offset-1 col-md-4 col-xs-12">
                        <div class="payment-heading major">
                            Payment Method
                        </div>
                        <div class="payment-method">
                            VISA ending in 7581
                        </div>
                      </div>
                   </div>
                </div>
                <jsp:include page="checkout-item-details.jsp"></jsp:include>
            </div>
        </div>
    </div>
    <div class="hide-mobile">
         <jsp:include page="checkout-active-shop-bottom.jsp"></jsp:include>
    </div>
</div>