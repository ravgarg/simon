<div class="mysizes-container tab-accordian">
	<ul class="nav nav-tabs major show-desktop">
		<li class="active"><a href="#women" data-toggle="tab">Women</a></li>
		<li><a href="#men" data-toggle="tab">Men</a></li>
		<li><a href="#kids" data-toggle="tab">Kids</a></li>
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="women">
			<div class="panel panel-default">
				<div class="panel-heading hide-desktop">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent=".tab-pane" href="#collapseWomen">Women<span class="pull-right"><i class="glyphicon glyphicon-minus"></i></span></a>
						
					</h4>
				</div>
				<div id="collapseWomen" class="panel-collapse collapse in">
					<div class="panel-body">
						<ul class="list-group item-listing">
							<li class="list-group-item row">
								<div class="col-md-10 col-xs-12 no-pad">
									<h6 class="list-group-item-heading">Clothing Sizes</h6>
									<p class="list-group-item-text">XXS - 2 (US), 32 1/2 (UK), 24 (IT), 35 (FR), 155/82A (Jeans)</p>
								</div>
								<div class="col-md-2 col-xs-12 no-pad">
									<a href="#" class="btn secondary-btn btn-width black major" data-toggle="modal" data-target="#editSizeModal"> Edit</a>
								</div>
							</li>

							<li class="list-group-item row">
								<div class="col-md-10 col-xs-12 no-pad">
									<h6 class="list-group-item-heading">Shoes</h6>
								</div>
								<div class="col-md-2 col-xs-12 no-pad">
									<a href="#" class="btn btn-width major" data-toggle="modal" data-target="#editSizeModal"> Add</a>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="men">
			<div class="panel panel-default">
				<div class="panel-heading hide-desktop">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent=".tab-pane" href="#collapseMen">Men<span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></a>
						
					</h4>
				</div>
				<div id="collapseMen" class="panel-collapse collapse">
					<div class="panel-body">
					Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. 
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane" id="kids">
			<div class="panel panel-default">
				<div class="panel-heading hide-desktop">
					<h4 class="panel-title">
						<a data-toggle="collapse" data-parent=".tab-pane" href="#collapseKids">Kids <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></a>
						
					</h4>
				</div>
				<div id="collapseKids" class="panel-collapse collapse">
					<div class="panel-body">
					Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. 
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>



<div class="modal fade editsize-modal" id="editSizeModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Clothing Size</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body clearfix">
				<div class="size-guide-content">
					<h6>Women's Size Guide</h6>									
				</div>
				
				
				
				<div class="fixedTable row" id="mysizes-data">
					<header class="fixedTable-header col-xs-12">
						<table class="table">
							<thead>
								<tr>
									<td>XXS</td>
									<td>XS</td>
									<td>S</td>
									<td>M</td>
									<td>L</td>
									<td>XL</td>
									<td>XXL</td>
									<td>XXXL</td>
								</tr>
							</thead>
						</table>
					</header>
					<aside class="fixedTable-sidebar col-xs-2">
						<table class="table">
							<tbody>
								<tr><td>US</td> </tr>
								<tr><td>UK</td> </tr>
								<tr><td>IT</td> </tr>
								<tr><td>FR</td> </tr>
								<tr><td>Jeans</td> </tr>
								<tr><td>Pref</td> </tr>	
								<tr><td>US</td> </tr>
								<tr><td>UK</td> </tr>
								<tr><td>IT</td> </tr>
								<tr><td>FR</td> </tr>
								<tr><td>Jeans</td> </tr>
								<tr><td>Pref</td> </tr>	
							</tbody>
						</table>
				  </aside>
					<div class="fixedTable-body col-xs-10">
						<table class="table size-table">
							<tbody>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2/1</td>
									<td>4/4</td>
									<td>56/5</td>
									<td>86/5</td>
									<td>106/5</td>
									<td>126/5</td>
									<td>146/5</td>
									<td>166/5</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr>
									<td>2</td>
									<td>4</td>
									<td>6</td>
									<td>8</td>
									<td>10</td>
									<td>12</td>
									<td>14</td>
									<td>16</td>
								</tr>
								<tr class="radio-content">
									<td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td>
									<td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td>
									<td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td><td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td><td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td><td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td><td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td><td>
										<div class="custom-radio"><input type="radio" id="" name=""></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
				
			</div>
			<div class="modal-footer clearfix">
				<button class="btn btn-add pull-left btn-width">Save</button>
				<button class="btn secondary-btn black pull-left btn-width" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>

<script>


</script>