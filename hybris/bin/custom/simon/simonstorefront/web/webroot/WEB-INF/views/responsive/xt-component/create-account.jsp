
    <div class="container create-account">
		<a href="#" class="banner">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/hero-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/hero-image2.png">
			</picture>
			<div class="img-caption">
				<h1>Lorem Ipsum</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
		</a>
    	<div class="create-account-container">
	    	<form action="submit">
		       <div class="panel-group" id="accordion">       
			      <div class="panel panel-default">
						<h5 class="panel-title">
						    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">Step 1: Create an Account</a>
						</h5>
						<div id="panel1" class="panel-collapse collapse in" aria-expanded="true" style="">
						    <div class="collapsed-content">
							    <div class="row">
					                <div class="col-xs-12 col-md-4">
					                	<div class="instructions">
						                	<p>An email will be sent to the email address that you enter once this form is completed to complete the registration process.</p>
						                	<p>Passwords must be between 6 and 15 characters long and contain characters from three of the following four categories:</p>
						                	<ul>
						                		<li>Uppercase characters</li>
						                		<li>Lowercase characters</li>
						                		<li>Numeric: 0-9</li>
						                		<li>Non-alphanumeric: ex. ~!@#$%</li>
						                	</ul>
					                	</div>
					                </div>
					                <div class="col-xs-12 col-md-8">
							             <div class="row">
							                <div class="col-xs-12 col-md-12">
							                   <div class="sm-input-group">
							                      <input type="text" class="form-control" placeholder="Email Address">
							                   </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group">
							                      <input type="text" class="form-control" placeholder="Password">
							                   </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group">
							                      <input type="password" autocomplete="off" class="form-control" placeholder="Confirm Password">
							                   </div>
							                </div>
							             </div>	
							             <div class="clearfix">
							             	<label class="checkboxes">
				                              <span class="sm-input-group pull-left field-float">
				                                 <input type="checkbox" name="confirm">
				                              </span>
				                              Yes, I am 18 years of age or older and agree to receive electronic messages from Simon Property Group regarding events, sales, store openings and other information about Simon shopping centers. You may withdraw your consent anytime. Please refer to our <a href="#">Privacy Policy</a>, <a href="#">Terms of Use</a>, or <a href="#">Contact Us</a>.
				                           </label>
							             </div>						             
					                </div>
							     </div>
						    </div>
						</div>
			       </div>
			       <div class="panel panel-default">
						<h5 class="panel-title">
						    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2" aria-expanded="true">Step 2: Tell Us About Yourself</a>
						</h5>
						<div id="panel2" class="panel-collapse collapse in" aria-expanded="true" style="">
						    <div class="collapsed-content">
							    <div class="row">
					                <div class="col-xs-12 col-md-4">
					                	<div class="instructions">
						                	<p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
					                	</div>
					                </div>
					                <div class="col-xs-12 col-md-8">
							             <div class="row">
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group">
							                      <input type="text" class="form-control reqiured" placeholder="First Name">
							                   </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group">
							                      <input type="text" class="form-control" placeholder="Last name">
							                   </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group">
							                      <input type="text" class="form-control" placeholder="zip code">
							                   </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group select-menu">
		                                          <select class="form-control">
		                                            <option value="">United States</option>
		                                          </select>
		                                        </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group select-menu">
		                                          <select class="form-control">
		                                            <option value="">Birth Month</option>
		                                          </select>
		                                        </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group select-menu">
		                                          <select class="form-control">
		                                            <option value="">Birth Year</option>
		                                          </select>
		                                        </div>
							                </div>
							                <div class="col-xs-12 col-md-6">
							                   <div class="sm-input-group select-menu">
		                                          <select class="form-control">
		                                            <option value="">Gender</option>
		                                          </select>
		                                        </div>
							                </div>
							             </div>	
							             <div class="clearfix row">
							             	<div class="submit-btn col-md-6 col-xs-12">
							             		<button class="btn">Submit</button>
							             	</div>
							             	<div class="terms-conditions col-md-6 col-xs-12 small-text">
												128-bit SSL Bank-level Security
											</div>
							             </div>						             
					                </div>
							     </div>
						    </div>
						</div>
			       </div>
		       </div>
	       </form>
       </div>
    </div>