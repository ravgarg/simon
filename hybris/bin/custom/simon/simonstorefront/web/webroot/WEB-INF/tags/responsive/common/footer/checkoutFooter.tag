<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<footer>
<div class="analytics-footer">
	<div class="container">
		
		<div class="footer-top-border">
			<div class="row xs-row clearfix">
				
				<div class="checkout-footer-icons clearfix">
				<div class="icons">
					<cms:pageSlot position="csn_PaymentMethodSlot" var="feature">
					<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				
				<div class="antivirus-icon">
					<cms:pageSlot position="csn_McAfeeSlot" var="feature">
					<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				</div>

				<cms:pageSlot position="csn_CustomerServiceSlot" var="feature">
				<cms:component component="${feature}" />
				</cms:pageSlot>

				
				
			</div>
			
			<div class="footer-terms">
				<cms:pageSlot position="csn_CopyRightSlot" var="feature">
				<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
	</div>		
		</footer>