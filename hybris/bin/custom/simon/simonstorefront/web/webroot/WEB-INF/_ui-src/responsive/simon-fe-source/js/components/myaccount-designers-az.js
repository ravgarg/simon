/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'utility', 'ajaxFactory','viewportDetect' ], function($, utility, ajaxFactory, viewportDetect) {
    'use strict';
    var cache;
    var myaccountDesignersAZ = {
        init: function() {
            this.initVariables();
            this.initEvents();
        },

        initVariables: function() {
            cache = {
                $document : $(document),
				$designerPagination : $(".designers-pagination-wrapper .pagination")
            }
        },

        initAjax: function() {},

        initEvents: function() {
			cache.$designerPagination.on('click','a', myaccountDesignersAZ.onClickPagination);
			
		},
		onClickPagination: function(e){
				
				var scrollToTop,
					targetDiv = $(this).attr('data-pag-target');
				if(targetDiv === ''){
					e.preventDefault();
				}else{
						scrollToTop = $('header').height();

					$('html,body').animate({
						scrollTop: parseInt($('#' + targetDiv).offset().top - scrollToTop)
					}, 1000);
				}
		},
		fixedDesignersPagination: function(){
			var $element = $('.designers-pagination-wrapper').find('.pagination');
			if($element.length){
				var originalY = $element.offset().top;

				var topMargin = 10;

				$(window).on('scroll', function() {
					var scrollTop = $(window).scrollTop(),
						footerTop = ($('footer').position().top);
					
					if( (scrollTop+950) < footerTop ){
								
						$element.stop(false, false).animate({
							top: scrollTop < originalY ? 0 : scrollTop - originalY + topMargin
						}, 0);
					}
				});
			}
			
		}
	 }
      
    $(function() {
        myaccountDesignersAZ.init();		
		if(viewportDetect.lastClass === 'small'){
			myaccountDesignersAZ.fixedDesignersPagination();
		}
    });

    return myaccountDesignersAZ;
});