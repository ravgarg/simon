<div class="social-shop">
	<div class="container">
		<div class="row">
			<div class="social-shop-containers  col-xs-12 col-md-8" >
				<div class="row">
					<div id="carousalFooter">
						<div class="social-shop-containers item  col-xs-12 col-md-4">
							<picture>
								<!--[if IE 9]><video class="hide"><![endif]-->
								<source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/image-1@2x.jpg 2x">
								<source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/image-1@2x.jpg">
								<!--[if IE 9]></video><![endif]-->
								<img alt="" style="" src="/_ui/responsive/simon-theme/images/image-1.jpg">
							</picture>
						</div>
						<div class="social-shop-containers item col-xs-12 col-md-4">
							<picture>
								<!--[if IE 9]><video class="hide"><![endif]-->
								<source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/image-2@2x.jpg 2x">
								<source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/image-2@2x.jpg">
								<!--[if IE 9]></video><![endif]-->
								<img alt="" style="" src="/_ui/responsive/simon-theme/images/image-2.jpg">
							</picture>
						</div>
						<div class="social-shop-containers item col-xs-12 col-md-4">
							<picture>
								<!--[if IE 9]><video class="hide"><![endif]-->
								<source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/new-image-3@2x.jpg 2x">
								<source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/new-image-3@2x.jpg">
								<!--[if IE 9]></video><![endif]-->
								<img alt="" style="" src="/_ui/responsive/simon-theme/images/new-image-3.jpg">
							</picture>
						</div>
					</div>
				</div>
			</div>
			<div class="social-shop-containers social-shop-content col-xs-12 col-md-4">
				<h4 class="major main-heading">#socialshop</h4>
				<h4 class="sub-heading">Join our style community</h4>
				<p>Show us your style & <a href="#">Upload a Photo</a> from your Facebook, Instagram, or device.</p>
				<a href="#" class="btn secondary-btn black major">the social shop</a>
			</div>
		</div>
	</div>
</div>