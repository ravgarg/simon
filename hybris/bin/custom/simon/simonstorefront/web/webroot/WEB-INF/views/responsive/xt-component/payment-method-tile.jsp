<div class="payment-method-box">
    <div class="row">
      <div class="col-md-4">
        <div class="payment-method-border">
           <div class="payment-method-box-margin">
            <div class="card-details">VISA ending in 7581</div>
            <div class="card-exp">02/2025</div>
             <div class="preferred-card-address">
                Lisa Simone
                18 Bull Path Close
                East Hampton, NY 11937
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress" name="" checked="">
                <label for="preferredAddress">Preferred Card</label>
             </div>
             <a href="#" data-toggle="modal" data-target="#paymentMethodDeleteModal">Delete</a>
           </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="payment-method-border">
           <div class="payment-method-box-margin">
            <div class="card-details">VISA ending in 7581</div>
            <div class="card-exp">02/2025</div>
             <div class="preferred-card-address">
                Lisa Simone
                18 Bull Path Close
                East Hampton, NY 11937
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress1" name="" checked="">
                <label for="preferredAddress1">Preferred Card</label>
             </div>
             <a href="#" data-toggle="modal" data-target="#paymentMethodDeleteModal">Delete</a>
           </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="payment-method-border">
           <div class="payment-method-box-margin">
            <div class="card-details">VISA ending in 7581</div>
            <div class="card-exp">02/2025</div>
             <div class="preferred-card-address">
                Lisa Simone
                18 Bull Path Close
                East Hampton, NY 11937
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress2" name="" checked="">
                <label for="preferredAddress2">Preferred Card</label>
             </div>
             <a href="#" data-toggle="modal" data-target="#paymentMethodDeleteModal">Delete</a>
           </div>
        </div>
      </div>
    </div>
</div>