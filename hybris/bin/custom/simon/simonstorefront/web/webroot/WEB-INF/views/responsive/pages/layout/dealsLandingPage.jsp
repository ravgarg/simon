<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<template:page pageTitle="${pageTitle}">
	<div class="container">
	<div class="analytics-pageContentLoad" data-analytics-pagetype="deal-landing" data-analytics-subtype="deal-landing" data-analytics-contenttype="informational">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<div class="row">
					<div class="breadcrumbs">
						<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
						<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
					</div>
				</div>
			</div>
			<cms:pageSlot position="csn_dealsLanding_LeftNavigationSlot"
				var="feature" element="div" class="account-section-content">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<section class="col-md-9 analytics-rightContainer myoffers-container deals-landing" data-analytics-pagelisttype="deal-landing" data-analytics-pagelistname="${cmsPage.name}">

					<div class="my-favorites page-container">
						<a href="#"
							class="add-deals hidden-xs" data-toggle="modal"
							data-target="#moadl1Content" data-key="my-deals"><spring:theme
									code="text.account.deals.howToAdd"></spring:theme></a>
						<div class="myfavorites-container">

							<cms:pageSlot position="csn_dealsLanding_pageTitleSlot"
								var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>

							<a class="add-deals hide-desktop" data-toggle="modal"
								data-target="#moadl1Content" href="#" data-key="my-deals"><spring:theme
									code="text.account.deals.howToAdd"></spring:theme></a>
							<div class="page-context-links"></div>
						</div>

						<div class="owl-carousel" id="homepageCarousel">
							<cms:pageSlot position="csn_dealsLanding_heroCarouselSlot"
								var="feature">
								<div class="img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
	
						<div class="btn-category hide-desktop categories-btn">
	
							<button class="btn secondary-btn black" data-toggle="modal"
								data-target="#deals" id="category">
								<spring:theme code="text.clp.browse.categories" />
							</button>
	
						</div>
	
						<cms:pageSlot position="csn_dealsLanding_dealsTilesSlot"
							var="feature" element="div" class="account-section-content analytics-dealsLanding">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					
					</div>
			</section>
		</div>
	</div>
<div class="modal fade" id="deals" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
						<h5 class="modal-title major">Categories</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<cms:pageSlot position="csn_dealsLanding_LeftNavigationSlot"
						var="feature" element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
		</div>		
	</div>

</template:page>