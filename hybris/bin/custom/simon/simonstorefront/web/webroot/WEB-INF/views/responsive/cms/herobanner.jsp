<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:set var="imageDestinationLink"
	value="${storeData.heroBanner.backgroundImage.destinationLink}" />
<c:set var="backgroundImageItem"
	value="${storeData.heroBanner.backgroundImage}" />
<c:set var="desktopImage"
	value="${backgroundImageItem.desktopImage.url}" />

<c:choose>
	<c:when test="${not empty backgroundImageItem.mobileImage}">
		<c:set var="mobileImage"
			value="${backgroundImageItem.mobileImage.url}" />
	</c:when>
	<c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${storeData.heroBanner.fontColor eq 'WHITE'}">
		<c:set var="fontColor" value="active" />
	</c:when>
	<c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${storeData.heroBanner.alignment eq 'LeftAligned'}">
		<c:set var="style" value="left" />
	</c:when>
	<c:otherwise>
		<c:set var="style" value="" />
	</c:otherwise>
</c:choose>

<div class="the-designer-shop ${style}  ${fontColor}">
	<a href="${imageDestinationLink.url}"> <picture>
		<source media="(max-width: 991px)" srcset="${mobileImage}">
		<img class="img-responsive" src="${desktopImage}" alt="${storeData.heroBanner.backgroundImage.imageDescription}"> </picture>
	</a>
	<div class="content-container">
		<h1 class="display-medium major">${storeData.heroBanner.titleText}</h1>
		<div class="designer-banner-text">${storeData.heroBanner.subheadingText}</div>
		<c:if test="${not empty storeData.heroBanner.linkText}">
			<c:choose>
				<c:when test="${heroBanner.linkTarget eq 'NEWWINDOW'}">
					<c:set var="target" value="_blank" />
				</c:when>
				<c:otherwise>
					<c:set var="target" value="_self" />
				</c:otherwise>
			</c:choose>
			<a target=${target } href="${storeData.heroBanner.linkUrl}"> <span
				class="shopnow major">${storeData.heroBanner.linkText}&nbsp;<span>&rsaquo;</span></span>
			</a>
		</c:if>
	</div>
</div>
