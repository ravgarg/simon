<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:url value="/checkout/multi/summary/placeOrder" var="placeOrderUrl"/>
<spring:url value="/checkout/multi/termsAndConditions" var="getTermsAndConditionsUrl"/>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">


<jsp:body>
<input id="currentCheckoutStep" type="hidden" name="currentCheckoutStep" value="review" />
<div class="row">
    <div class="col-sm-6">
    	<div class="checkout-headline">
    		<span class="glyphicon glyphicon-lock"></span>
            <spring:theme code="checkout.multi.secure.checkout" />
        </div>
		<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<ycommerce:testId code="checkoutStepFour">
                <div id="paymentInfoDetails" class="checkout-review">


                    <div class="global-alerts" id="errors" style="display:none">

                        <div class="alert alert-danger "></div>
                    </div>


                    <div class="form-group">

                        <label class="control-label " for="card_nameOnCard">
                            Name On Card</label>

                        <input id="card_nameOnCard" name="card_nameOnCard" class="text form-control" type="text" value="">
                    </div>

                    <div class="form-group">
                        <label class="control-label ">
                            Card Number</label>
                        <div id="spreedlyCardNumber"></div>
                    </div>

                    <fieldset id="cardDate" class="form-group">
                        <label for="" class="control-label"><spring:theme code="payment.expiryDate"/></label>
                        <div class="row">
                            <div class="col-xs-6">


                                <select name="card_expirationMonth" class="form-control">
                                    <c:forEach items="${months}" var="month">
                                        <option value="${month.code}">${month.name}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <div class="col-xs-6">
                                <select name="card_expirationYear" class="form-control">
                                    <c:forEach items="${expiryYears}" var="expiryYear">
                                        <option value="${expiryYear.code}">${expiryYear.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </fieldset>

                    <div class="form-group row">
                        <div class="col-xs-6">
                            <label class="control-label ">
                                CVV</label>
                            <div id="spreedlyCCV"></div>
                        </div>
                    </div>


                </div>
				<div class="checkout-review hidden-xs">
                    <div class="checkout-order-summary">
                        <multi-checkout:orderTotals cartData="${cartData}" showTaxEstimate="${showTaxEstimate}" showTax="${showTax}" subtotalsCssClasses="dark"/>
                    </div>
                </div>
                <div class="place-order-form hidden-xs">
                    <form:form action="${placeOrderUrl}" id="placeOrderForm1" commandName="placeOrderForm">
                        <input type="hidden" name="card_nameOnCard" value="" />
                        <input type="hidden" name="card_cardType" value="" />
                        <input type="hidden" name="card_lastFourDigits" value="" />
                        <input type="hidden" name="card_cardNumber" value="" />
                        <input type="hidden" name="card_expirationMonth" value="" />
                        <input type="hidden" name="card_expirationYear" value="" />
                        <input type="hidden" name="spreedlyToken" value="" />
                        <div class="checkbox">
                            <label> <form:checkbox id="Terms1" path="termsCheck" />
                                <spring:theme code="checkout.summary.placeOrder.readTermsAndConditions" arguments="${getTermsAndConditionsUrl}" text="Terms and Conditions"/>
                            </label>
                        </div>

                        <button id="placeOrder" type="submit" class="btn js-place-order btn-primary btn-place-order btn-block">
                            <spring:theme code="checkout.summary.placeOrder" text="Place Order"/>
                        </button>
                    </form:form>
                </div>
			</ycommerce:testId>
		</multi-checkout:checkoutSteps>
    </div>

    <div class="col-sm-6">
		<multi-checkout:checkoutOrderSummary cartData="${cartData}" showDeliveryAddress="true" showPaymentInfo="true" showTaxEstimate="true" showTax="true" />
	</div>

    <div class="col-sm-12 col-lg-12">
        <br class="hidden-lg">
        <cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
            <cms:component component="${feature}"/>
        </cms:pageSlot>
    </div>
</div>
</jsp:body>
</template:page>