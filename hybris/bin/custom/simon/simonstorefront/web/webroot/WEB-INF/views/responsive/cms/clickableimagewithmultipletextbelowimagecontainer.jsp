<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<div class="row">
<c:forEach items="${trendingNowList}" var="trendingNowComponent" varStatus="status">

<c:set var="imageDestinationLink" value="${trendingNowComponent.backgroundImage.destinationLink}" />
<c:choose>
	<c:when test="${imageDestinationLink.target eq 'NEWWINDOW'}">
    	<c:set var="imageDestinationTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="imageDestinationTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${trendingNowComponent.titleText.target eq 'NEWWINDOW'}">
    	<c:set var="titleTextTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="titleTextTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${trendingNowComponent.subheadingText.target eq 'NEWWINDOW'}">
    	<c:set var="subheadingTextTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="subheadingTextTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${trendingNowComponent.fontColor eq 'WHITE'}">
    	<c:set var="fontColor" value="white-text" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${trendingNowComponent.backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty trendingNowComponent.backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${trendingNowComponent.backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<div class="trending-items col-xs-12 col-md-4 analytics-data ${fontColor}" data-analytics='{
		  "event": {
			"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|${fn:toLowerCase(component.itemtype)}",
			"sub_type": "${fn:toLowerCase(trendingNowComponent.itemtype)}"
		  },
		  "banner": {
			"click": {
			  "id": "${fn:toLowerCase(trendingNowComponent.uid)}"
			}
		  },"link": {
			"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|trending_now",
			"name": "<c:choose>
				<c:when test="${not empty trendingNowComponent.name}">${fn:toLowerCase(fn:replace(trendingNowComponent.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(trendingNowComponent.uid)}</c:otherwise></c:choose>"
		  }
		}'>
	<c:choose>
		<c:when test="${not empty imageDestinationLink}">
		<a target="${imageDestinationTarget}" href="${ycommerce:getUrlForCMSLinkComponent(imageDestinationLink)}">
		<picture>
			  <source media="(max-width: 767px)" srcset="${mobileImage}">
			  <source media="(min-width: 768px) and (max-width: 991px)" srcset="${desktopImage}">
			  <img class="img-responsive img-hover-effect" alt="${trendingNowComponent.backgroundImage.imageDescription}" src="${desktopImage}" >
		</picture>
		</a>
		</c:when>
		<c:otherwise>
		<picture>
			  <source media="(max-width: 767px)" srcset="${mobileImage}">
			  <source media="(min-width: 768px) and (max-width: 991px)" srcset="${desktopImage}">
			  <img class="img-responsive img-hover-effect" alt="${trendingNowComponent.backgroundImage.imageDescription}" src="${desktopImage}" >
		</picture>
		</c:otherwise>
	</c:choose>
	
	<c:choose>
	<c:when test="${not empty trendingNowComponent.titleText.url}">
	<a target="${titleTextTarget}" href="${trendingNowComponent.titleText.url}">
		<h4 class="major">${trendingNowComponent.titleText.linkName}</h4>
	</a>
	</c:when>
	<c:otherwise>
	<h4 class="major">${trendingNowComponent.titleText.linkName}</h4>
	</c:otherwise>
</c:choose>
	
	<c:choose>
	<c:when test="${not empty trendingNowComponent.subheadingText.url}">
	<a target="${subheadingTextTarget}" href="${trendingNowComponent.subheadingText.url}">
		<h6>${trendingNowComponent.subheadingText.linkName}</h6>
	</a>
	</c:when>
	<c:otherwise>
	<h6>${trendingNowComponent.subheadingText.linkName}</h6>
	</c:otherwise>
</c:choose>
	
	
</div>
      <c:choose>
			<c:when test="${(status.count % 3)==0}">
				<c:if test="${(status.count)==fn:length(trendingNowList)}">
					</div>
				</c:if>
				<c:if test="${(status.count)<fn:length(trendingNowList)}">
					</div>
					<div class="row">
				</c:if>		
			</c:when>
			<c:otherwise>
				<c:if test="${(status.count)==fn:length(trendingNowList)}">
		   			</div>
				</c:if>
			</c:otherwise>
		</c:choose>
   </c:forEach>
