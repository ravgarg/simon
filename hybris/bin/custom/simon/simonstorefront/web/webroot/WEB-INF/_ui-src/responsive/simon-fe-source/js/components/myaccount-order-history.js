/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'ajaxFactory', 'utility', 'analytics'], function($, ajaxFactory, utility, analytics) {
    'use strict';
    var cache;
    var myaccountOrderHistory = {
        init: function() {
            this.initVariables();
            this.initEvents();
            utility.floatingLabelsInit();
            if($('.left-menu-container .left-menu .active').length) {
            	$('.left-menu-container .left-menu').find('.active').parent('.level-1').siblings('.accordion-menu').trigger('click');
            }
        },
        initVariables: function() {
            cache = {
                $document : $(document),
                count : 1,
                numbers: $('.myaccount .pagination>li').length - 7,
                $orderdetailrow: $('.myorder-details-container .order-detail-row'),
                $loadmorebtn: $('.load-more')
            }
        },
        initAjax: function() {},
        initEvents: function() {   
            cache.$document.on('click',".myorder-details-container .load-more", myaccountOrderHistory.loadMore);
            $(document).ready(function(){
                $('.orderDetails').click(function(e) {
                    var url = 'order/'+e.currentTarget.text;
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "html",
                        success: function (data) {
                        	$('#myordersModal').find('.modal-content').html(data);
                        	
							$("#myordersModal").on("shown.bs.modal", function () { 
								if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.getOrderDetails();
								}
							}).modal('show');
                        	$('.tooltip-init').tooltip();
                        }
                    });
                    e.stopImmediatePropagation();
                    return false;
                });
            });
          
            //hide load more
            if(parseInt($('input#orderDetailPageCount').val()) > 1) {
                cache.$loadmorebtn.css('visibility','visible');
            }
            $('div.account-section-header').find('li.active').find('a').attr('href','javascript:void(0)');
            $('.order-detail-row').each(function( index ) {
                if (index%2 === 0){
                    $(this).addClass("row-color-gray");
                }
            });
        },
        loadMore: function(e){
                var url = $(this).data('url');
                url = url+'?page='+cache.count;
                $.ajax({
                    url: url,
                    data: {
                        txtsearch: $('#appendedInputButton').val()
                    },
                    type: "GET",
                    dataType: "html",
                    success: function (data) {
                    var $myorderdetails = $('.myorder-details-container .myorder-details');
                        cache.count++;
                        $myorderdetails.append($.parseHTML(data));
                        myaccountOrderHistory.initEvents();
                        if(parseInt($('input#orderDetailPageCount').val()) === cache.count){
                            cache.$loadmorebtn.css('visibility','hidden');
                        }
						if($('#analyticsSatelliteFlag').val()==='true'){
							analytics.fetchMyOrders('loadmore_orders');
						}
                    }
                });
                e.stopImmediatePropagation();
                return false;
        },
    }
    $(function() {
        myaccountOrderHistory.init();
    });

    return myaccountOrderHistory;
});