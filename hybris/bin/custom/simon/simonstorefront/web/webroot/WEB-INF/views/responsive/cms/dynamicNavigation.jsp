<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<!-- Dynamic Left Navigation Starts -->
<c:forEach var="L2NavigationNode" items="${navigationData }">
<c:if test="${L2NavigationNode.show}">
	   <c:set var="l2AccordianShow" value="false"/>
       <c:forEach var="l3ChildNode" items="${L2NavigationNode.childNode}">
              <c:if test="${l3ChildNode.show and l2AccordianShow eq 'false'}">
                     <c:set var="l2AccordianShow" value="true"/>
              </c:if>
       </c:forEach>
       <li
		<c:choose><c:when test="${L2NavigationNode.id eq searchPageData.categoryCode and fn:length(L2NavigationNode.childNode) gt 0}">class="active has-level-1"</c:when><c:when test="${L2NavigationNode.id eq searchPageData.categoryCode}">class="active"</c:when><c:otherwise>class="has-level-1"</c:otherwise>  </c:choose>>

		<c:choose>
			<c:when test="${fn:length(L2NavigationNode.childNode) gt 0 and l2AccordianShow eq 'true'}">
				<a href="#${L2NavigationNode.id}" class="accordion-menu collapsed"
					data-toggle="collapse">${L2NavigationNode.name }</a>
				<ul class="level-1 collapse" id="${L2NavigationNode.id}">
					<!-- List of L3 Nodes -->
					<c:forEach var="child1" items="${L2NavigationNode.childNode }">
					<c:if test="${child1.show}">
						<li
							<c:choose><c:when test="${child1.id eq searchPageData.categoryCode and fn:length(child1.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child1.id eq searchPageData.categoryCode}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>

							<c:choose>
								<c:when test="${fn:length(child1.childNode) gt 0}">
									<a href="#${child1.id }" class="accordion-menu collapsed"
										data-toggle="collapse">${child1.name }</a>
									<ul class="level-2 collapse" id="${child1.id }">
										<!-- List of L4 Nodes -->
										<c:forEach var="child2" items="${child1.childNode }">
										<c:if test="${child2.show}">
											<li
												<c:choose><c:when test="${child2.id eq searchPageData.categoryCode and fn:length(child2.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child2.id eq searchPageData.categoryCode}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>
												<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child2.name))}" href="<c:url value="${child2.url }"/>">${child2.name }</a>
												<c:if test="${fn:length(child2.childNode) gt 0}">
													<ul class="level-2 collapse" id="${child2.id }">
														<!-- List of L5 Nodes -->
														<c:forEach var="child3" items="${child2.childNode }">
														<c:if test="${child3.show}">
															<li
																<c:choose><c:when test="${child3.id eq searchPageData.categoryCode and fn:length(child3.childNode) gt 0}">class="active has-level-2"</c:when><c:when test="${child3.id eq searchPageData.categoryCode}">class="active"</c:when><c:otherwise>class="has-level-2" </c:otherwise> </c:choose>>
																<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child3.name))}" href="<c:url value="${child3.url }"/>">${child3.name }</a>
																<c:if test="${fn:length(child3.childNode) gt 0}">
																	<ul class="level-2 collapse" id="${child3.url }">
																		<c:forEach var="child4" items="${child3.childNode }">
																		<c:if test="${child4.show}">
																			<li
																				<c:if test="${child4.url eq searchPageData.categoryCode}">class="active" </c:if>><a
																				data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child4.name))}" href="<c:url value="${child4.url }"/>">${child4.name }</a></li>
																		</c:if>
																		</c:forEach>
																	</ul>
																</c:if>
															</li>
															</c:if>
														</c:forEach>
													</ul>
												</c:if>
											</li>
											</c:if>
										</c:forEach>
									</ul>
								</c:when>
								<c:otherwise>
									<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(child1.name))}" href="<c:url value="${child1.url }"/>">${child1.name }</a>
								</c:otherwise>
							</c:choose>
						</li>
						</c:if>
					</c:forEach>
				</ul>
			</c:when>
			<c:otherwise>
				<a data-analytics-leftlinkname="${fn:toLowerCase(fn:escapeXml(L2NavigationNode.name))}" href="<c:url value="${L2NavigationNode.url}" />">${L2NavigationNode.name }</a>
			</c:otherwise>
		</c:choose>
	</li>
	</c:if>
</c:forEach>
<!-- Dynamic Left Navigation Ends -->