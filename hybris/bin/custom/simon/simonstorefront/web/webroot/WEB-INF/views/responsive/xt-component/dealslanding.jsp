<div class="container">
	<div class="row">
		<div class="col-md-12">
			<jsp:include page="dealslanding-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="dealslanding-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<jsp:include page="dealslanding-top-heading.jsp"></jsp:include>
			<jsp:include page="dealslanding-content.jsp"></jsp:include>
		</div>
	</div>
</div>