<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" id="designerList" value="${allDesignerlist}"/>
<div class="myarea-container">
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/add-designer", "remove":"/my-account/remove-designer" }' />
		
		<c:set var="buttonText" value="" scope="page" />
		<c:choose>
			<c:when test="${not empty designerList && not empty activeDesignerList}">
				<c:set var="designerSet" value="${activeDesignerList}" scope="request"/>
				<div class="analytics-favoriteDesigners" data-analytics-length="designers">
				<jsp:include page="displayMyDesigners.jsp"></jsp:include>
				</div>
				<div class="btn-category">
					<button class="btn black btn-width major callDesignerList analytics-genericLink" data-analytics-eventsubtype="add-designers" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-designers" data-analytics-linkname="add-designers" data-toggle="modal"
						data-target="#addDesignersModal" data-url="<c:url value = "/my-account/get-designers"/>">
						<spring:theme code="${addDesigners}" />
					</button>
				</div>
			</c:when>
			<c:otherwise>
				<div class="analytics-favoriteDesigners" data-analytics-length="nodesigners">
					<h4>${userName},
						<spring:theme code="${noSavedDesignerTitle}" />
					</h4>
					<p>
						<spring:theme code="${noSavedDesignerData}" />
					</p>
					<button class="btn black btn-width major callDesignerList analytics-genericLink" data-analytics-eventsubtype="add-designers" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-designers" data-analytics-linkname="add-designers" data-toggle="modal"
						data-target="#addDesignersModal" data-url="<c:url value = "/my-account/get-designers"/>">
						<spring:theme code="${addDesigners}" />
					</button>
				</div>
			</c:otherwise>
		</c:choose>
</div>

	<div class="modal fade adddesigners-modal" id="addDesignersModal" role="dialog">
		<div class="modal-dialog">
			<form:form method="post" commandName="addDesignerForm"  class="sm-form-validation designers-search analytics-myPageFormSubmit" data-analytics-formtype="add_designers" data-analytics-formname="add_designers">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title"><spring:theme code="text.account.myDesigners.addDesigner.title" /></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body clearfix">
						<div class="designers-listing-container">
						<label for="designerdetailssearch" class="hide"><spring:theme code="text.account.myDesigners.designer.search" /></label>
							<input type="text" name="search-filter" title="" class="form-control" id="designerdetailssearch" autocomplete="off" placeholder="<spring:theme code="text.account.myDesigners.designer.search" />">
							<div class="designers-listing wrapper">
								<div class="scroll-container">
									<ul class="searchList" id="searchList">
									
									</ul>	
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer clearfix">
						<button class="btn add-to-fav-btn btn-add pull-left btn-width disabled analytics-genericLink" data-analytics-eventsubtype="my-designer" data-analytics-linkplacement="my-designer" data-analytics-linkname="add designer" disabled><spring:theme code="text.account.myDesigners.addDesigner.button" /></button>
						<button class="btn secondary-btn black pull-left btn-width" data-dismiss="modal"><spring:theme code="basket.save.cart.action.cancel" /></button>
					</div>
				</div>
			</form:form>
		</div>
	</div>

