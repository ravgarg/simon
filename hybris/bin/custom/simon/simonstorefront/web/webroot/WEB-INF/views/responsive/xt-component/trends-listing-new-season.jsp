<div class="the-designer-shop left active trends-listing-new-season">
	<a href="#">
		<picture>
			<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/trends-listing-new-seasonMob.png">
			<img class="img-responsive" src="/_ui/responsive/simon-theme/images/trends-listing-new-season.png">
		</picture>
		<div class="content-container">
			<h1 class="display-medium major">New season blues by barneys warehouse</h1>
			<div class="designer-banner-text">
				We officially love blue this season.
				From baby hues to cobalt, shop our favorite
				cool blue on the hotlist this season.
			</div>			
		</div>
	</a>
</div>