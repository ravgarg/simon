<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">
	<div class="container">	
		<c:if test="${not empty errorMessage}">
				<div class="error-msg analytics-formSubmitFailure"> <spring:theme code="${errorMessage}" />
				</div>
		</c:if>  	
	</div>
	<cms:pageSlot position="csn_heroBannerSlot" var="comp" element="div" class="shopperRegistrationPage">
		<cms:component component="${comp}" element="div" class="shopperRegistrationPage-component"/>
	</cms:pageSlot>
	<div class="container create-account">
	
		 <div class="create-account-container analytics-registerForm">
			 <cms:pageSlot position="csn_mainShopperRegistrationSlot" var="comp" element="div" class="mainpage">
				<cms:component component="${comp}" element="div" class="shopperRegistrationPage-component"/>
			</cms:pageSlot>
		</div>
	</div>
</template:page>