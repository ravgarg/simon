<ul class="left-menu level-0 list-unstyled">
    <li class="has-level-1 active">
        <a href="#subMenuL1Id10" class="accordion-menu collapsed" data-toggle="collapse">My Stores</a>
        <ul class="level-1 my-stores collapse" id="subMenuL1Id10">
            <li><a href="#">Polo Ralph Lauraen</a></li>
            <li><a href="#">Nordstrom Rack</a></li>
            <li><a href="#">Last Call</a></li>
        </ul>
        
        <a href="#subMenuL1Id11" class="accordion-menu collapsed" data-toggle="collapse">Stores</a>
        <ul class="level-1 stores collapse" id="subMenuL1Id11">
            <li><a href="#">Store Name</a></li>
            <li><a href="#">Store Name</a></li>
            <li><a href="#">Store Name</a></li>
            <li><a href="#">Store Name</a></li>
            <li><a href="#">Store Name</a></li>
            <li><a href="#">Store Name</a></li>
        </ul>
    </li>
</ul>