define(['jquery','analytics'],
    function($,analytics) {
        var $body = $('body');
        var isRequireLoader = true,
        	hasLoadMore = false;
        var ajaxFactory = {
            ajaxFactoryInit: function(options, successCallback, errorCallback, errorPage) {
                // Retreiving response from the service
                var ajaxdataType, ajaxtype, ajaxUrl, ajaxData, ajaxCallback, $parentElem, asyncType, isShowLoader, cache;
                if (!errorCallback) {
                    errorCallback = function(jqXHR, textStatus, errorThrown) {
                        ajaxFactory.showError(jqXHR.status, textStatus, errorThrown);
						
						if($('#analyticsSatelliteFlag').val()==='true'){
							if(errorPage && ajaxUrl !== '#'){								
								analytics.analyticsAJAXFailureHandler(errorPage,errorThrown,'SE',jqXHR.status,ajaxUrl);
							}
						}
                    };
                }
                if (typeof options.asyncType !== 'undefined') {
                    asyncType = options.asyncType;
                } else {
                    asyncType = true;
                }

                if (typeof options.dataType !== 'undefined') {
                    ajaxdataType = options.dataType;
                } else {
                    ajaxdataType = 'json';
                }

                if (typeof options.methodType !== 'undefined') {
                    ajaxtype = options.methodType;
                } else {
                    ajaxtype = 'POST';
                }

                if (typeof options.url !== 'undefined') {
                    ajaxUrl = options.url;
                } else {
                    ajaxUrl = '#';
                }

                if (typeof options.methodData !== 'undefined') {
                    ajaxData = options.methodData;
                } else {
                    ajaxData = '';
                }

                if (typeof options.callback !== 'undefined') {
                    ajaxCallback = options.callback;
                } else {
                    ajaxCallback = '';
                }

                if (typeof options.parentElem !== 'undefined') {
                    $parentElem = options.parentElem;
                } else {
                    $parentElem = $body;
                }

                if (typeof options.isShowLoader !== 'undefined') {
                    isShowLoader = options.isShowLoader;
                } else {
                    isShowLoader = true;
                }
                
                if (typeof options.hasLoadMore !== 'undefined') {
                	hasLoadMore = options.hasLoadMore;
                } else {
                	hasLoadMore = false;
                }

                if (typeof options.isRequireLoader !== 'undefined') {
                    isRequireLoader = options.isRequireLoader;
                } else {
                    isRequireLoader = true;
                }
                if (typeof options.isShowloaderMsg !== 'undefined') {
                	isShowloaderMsg = options.isShowloaderMsg;
                } else {
                	isShowloaderMsg = false;
                }
                if (typeof options.cache !== 'undefined') {
                    cache = options.cache;
                } else {
                    cache = false;
                }
				
				if( typeof errorPage === 'undefined' ){
					errorPage = '';
				}

                $.ajax({
                    url: ajaxUrl,
                    type: ajaxtype,
                    cache: cache,
                    async: asyncType,
                    beforeSend: function() {
                        ajaxFactory.ajaxHandler.ajaxStart($parentElem, isShowLoader);
                    },
                    complete: function() {
                        ajaxFactory.ajaxHandler.ajaxStop($parentElem, isShowLoader);
                    },
                    dataType: ajaxdataType,
                    data: ajaxData,
                    success: successCallback,
                    error: function(status, textStatus, errorThrown) {
                        errorCallback(status, textStatus, errorThrown);
                    },
                    jsonpCallback: ajaxCallback
                });
            },
            showError: function() {
                //use these params to display errors - status, textStatus, errorThrown
            	$('body').removeClass('page-overlay');
            },
            ajaxHandler: {
                ajaxStart: function() {
                    ////use these param to start loader - $elem, isShowLoader
                    if(isRequireLoader){
                        $('body').addClass('page-overlay');
                    }
                    if(isShowloaderMsg){
                    	$('.loaderMsg').html($('#checkoutSpinner').val());
                    }
                    if(hasLoadMore) {
                    	$(".load-more-btn-wrapper .load-more-loader").remove();
                        $(".load-more-btn-wrapper").append('<span class="load-more-loader"></span>');
                    }
                },
                ajaxStop: function() {
                    //use these param to stop loader - $elem, isShowLoader
                	$('body').removeClass('page-overlay');
                	$('.loaderMsg').html('');
                	$(".load-more-btn-wrapper .load-more-loader").remove();
                }
            }
        };
        return {
            ajaxFactory: ajaxFactory,
            httpRequest: ajaxFactory.httpRequest,
            ajaxFactoryInit: ajaxFactory.ajaxFactoryInit
        };
    });