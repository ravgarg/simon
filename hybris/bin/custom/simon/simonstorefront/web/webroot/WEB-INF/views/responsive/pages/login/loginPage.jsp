<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="vip-club-signin">
	<div class="container">
		<c:if test="${not empty enableCaptcha && enableCaptcha eq 'true'}">
		<c:set var="captchaValidation" value="captchaValidation"/>
		</c:if>
		<form action="/j_spring_security_check"
			method="post" modelAttribute="registerFormData"
			class="sm-form-validation bt-flabels js-flabels ${captchaValidation} analytics-formValidation" data-analytics-type="login" data-analytics-eventtype="login" data-analytics-eventsubtype="login" data-analytics-formtype="login_form" data-analytics-formname="loginform_page" data-analytics-formtype="loginform" data-satellitetrack="login" data-parsley-validate="validate">
			<div class="signin-box">
				<h5 class="major">
					<spring:theme code="text.login.signInBox.header.vipclub" />
					<span><spring:theme
							code="text.login.signInBox.header.signin" /></span>
				</h5>
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<div class="sm-input-group">
							<label class="field-label" for="email-address"><spring:theme
									code="text.login.field.label.email.address" /> <span
								class="field-error" id="error-j_guestEmailId"></span></label> <input
								type="email" class="form-control" id="email-address"
								placeholder="<spring:theme code="text.login.field.place.holder.email.address" />"
								name="j_username"
								data-parsley-errors-container="#error-j_guestEmailId"
								type="email" data-parsley-type="email"
								data-parsley-error-message="<spring:theme code="text.login.field.error.message.email.address" />"
								required>
						</div>
					</div>
					<div class="col-xs-12 col-md-12">
						<div class="sm-input-group">
							<label class="field-label" for="login-pswd"><spring:theme
									code="text.login.field.label.password" /> <span
								class="field-error" id="error-j_pswd"></span></label> <input
								type="password" class="form-control" id="login-pswd" autocomplete="off"
								placeholder="<spring:theme code="text.login.field.place.holder.password" />"
								name="j_password" data-parsley-errors-container="#error-j_pswd"
								data-parsley-required-message="<spring:theme code="text.login.field.error.message.password" />"
								data-parsley-required required />

						</div>
					</div>
					
					<c:if test="${not empty enableCaptcha && enableCaptcha eq 'true'}">
						<div class="col-xs-12 col-md-6 captchaContainer">
							<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
							<div id="captcha" class="has-error"><spring:theme code="login.customer.captcha.error.msg" /></div>
						</div>
					</c:if>
					<div class="submit-btn col-md-12 col-xs-12">
						<button class="btn white" type="submit">
							<spring:theme code="text.login.field.button.sign.in" />
						</button>
					</div>
					<div class="bottom-section col-md-12 col-xs-12">
						<ul>
							<li><a href="/join-now" class="analytics-genericLink" data-analytics-eventsubtype="login_overlay" data-analytics-linkplacement="login_overlay" data-analytics-linkname="join-now"><spring:theme
										code="text.login.field.href.join.vipclub" /></a></li>
							<li><a href="${forgotPasswordLink}"><spring:theme
										code="text.login.field.href.forgot.password" /></a></li>
						</ul>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
