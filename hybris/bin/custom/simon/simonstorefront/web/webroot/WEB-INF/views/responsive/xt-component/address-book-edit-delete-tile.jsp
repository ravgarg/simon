<div class="edit-delete-address-box">
    <div class="row">
      <div class="col-md-4">
        <div class="address-border">
           <div class="address-box-margin">
             <div class="preferred-address-text">
             Lisa Simone 18 Bull Path Close
             East Hampton, NY 11937
             (972) 602-8283
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress" name="" checked="">
                <label for="preferredAddress">Preferred Address</label>
             </div>
             <div class="edit-delete-links">
                <a href="#" class="edit-border" data-toggle="modal" data-target="#addressBookModal1">Edit</a>
                <a href="#" class="delete-padding" data-toggle="modal" data-target="#addressBookDeleteModal">Delete</a>
             </div>
           </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="address-border">
           <div class="address-box-margin">
             <div class="preferred-address-text">
             Lisa Simone 18 Bull Path Close
             East Hampton, NY 11937
             (972) 602-8283
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress1" name="" checked="">
                <label for="preferredAddress1">Preferred Address</label>
             </div>
             <div class="edit-delete-links">
                <a href="#" class="edit-border" data-toggle="modal" data-target="#addressBookModal1">Edit</a>
                <a href="#" class="delete-padding" data-toggle="modal" data-target="#addressBookDeleteModal">Delete</a>
             </div>
           </div>
        </div>
      </div>
     <div class="col-md-4">
        <div class="address-border">
           <div class="address-box-margin">
             <div class="preferred-address-text">
             Lisa Simone 18 Bull Path Close
             East Hampton, NY 11937
             (972) 602-8283
             </div>
             <div class="custom-radio">
                <input type="radio" id="preferredAddress2" name="" checked="">
                <label for="preferredAddress2">Preferred Address</label>
             </div>
             <div class="edit-delete-links">
                <a href="#" class="edit-border" data-toggle="modal" data-target="#addressBookModal1">Edit</a>
                <a href="#" class="delete-padding" data-toggle="modal" data-target="#addressBookDeleteModal">Delete</a>
             </div>
           </div>
        </div>
      </div>
    </div>
</div>