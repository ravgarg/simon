<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<template:page pageTitle="${pageTitle}">
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}" id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}" id="totalNumberOfResults" />
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
	<div class="container">
		<article class="row">
			<div class="col-md-12 breadcrumbs hide-mobile">
					<c:if test="${not empty breadcrumbs}">
						<c:forEach items="${breadcrumbs}" var="breadcrumb"
							varStatus="status">
							<c:choose>
								<c:when test="${breadcrumb.url eq '#'}">
									<a href="#" <c:if test="${status.last}">class="active"</c:if>>${fn:escapeXml(breadcrumb.name)}</a>
									<c:if test="${!status.last}">
										<span class="arrow-fwd"></span>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:url value="${breadcrumb.url}" var="breadcrumbUrl" />
									<a href="${breadcrumbUrl}" 
										<c:if test="${status.last}">class="active"</c:if>>${fn:escapeXml(breadcrumb.name)}</a>
									<c:if test="${!status.last}">
										<span class="arrow-fwd"></span>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
				</div>

				<!-- breadcurmb Ends -->

			<cms:pageSlot position="csn_plp_leftNavigationSlot" var="feature" view="desktop" >
				<cms:component component="${feature}"/>
			</cms:pageSlot>

		<section class="col-md-9">
			<!-- Heading -->
			<h1 class="page-heading major ">${categoryName} </h1>
			
			<!-- Description and Links Starts -->
			<div class="page-context-links">
				${categoryDescription}
				<div class="clp-des-links">
					<c:forEach items="${seoLinks}" var="seoLink" varStatus="status">
						<a href="<c:choose><c:when test="${not empty seoLink}">/${ycommerce:getUrlForCMSLinkComponent(seoLink)}</c:when><c:otherwise>'#'</c:otherwise></c:choose>" title="${seoLink.linkName}">${seoLink.linkName}</a>
						<c:if test="${!status.last}"><span>&rsaquo;</span></c:if>
					</c:forEach>
				</div>
			</div> 			
			<!-- Description and Links Ends -->

			<!-- Designer shop banner starts -->
			<c:if test="${not empty heroBanner}">
			<div class="row plp-top-banner">
				<jsp:include page="../../cms/categoryherobanner.jsp"></jsp:include>
			</div></c:if>
			<!-- Designer shop banner end -->
			
			<!-- Filters Starts -->
			<product:productFilters searchPageData="${searchPageData}" />
			<!-- Filters END -->
			<!-- Product Tiles Starts -->
			<product:productTilesListing searchPageData="${searchPageData}" />
			<!-- Product Tiles END -->
			<div class="row load-more-btn-wrapper">
				<button id="loadMore" class="btn btn-primary load-more block"><spring:theme code='plp.loadmore.text' /></button>
			</div>
				<!-- Product Tiles Ends -->

			</section>
		</article>
	</div>

</template:page>