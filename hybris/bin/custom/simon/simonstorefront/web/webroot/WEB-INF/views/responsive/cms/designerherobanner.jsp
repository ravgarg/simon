<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="enum" uri="http://simon.com/tld/enum"%>
<%@ taglib prefix="fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<c:set var="imageDestinationLink" value="${heroBanner.backgroundImage.destinationLink}" />
<c:choose>
	<c:when test="${imageDestinationLink.target eq 'NEWWINDOW'}">
    	<c:set var="imageDestinationTarget" value="_self" />
	</c:when>
    <c:otherwise>
		<c:set var="imageDestinationTarget" value="_blank" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${heroBanner.backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty heroBanner.backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${heroBanner.backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${heroBanner.alignment eq 'CENTERALIGNED'}">
    	<c:set var="alignment" value="center-align" />
	</c:when>
    <c:otherwise>
		<c:set var="alignment" value="" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${heroBanner.fontColor eq 'WHITE'}">
    	<c:set var="fontColor" value=" white-text" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>

<enum:enum hybrisEnum="${heroBanner.noOfCTA}"/>

<div
	class="tail-hide item ${alignment} ${fontColor} analytics-heroCarousel" data-satellitetrack="internal_click"  data-analytics='{"event": {
"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|hero_carousel",
"sub_type": "${fn:toLowerCase(component.itemtype)}"
},              
 "carousel":{
                  "click": {
                                         "placement" :  "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|hero_carousel",
                                         "name"     :  "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>",
                                         "id"       :  "${fn:toLowerCase(component.uid)}"
                                        }
               },
  "link": {
    "placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|hero_carousel",
    "name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>"
  }}'>
  <c:choose>
		<c:when test="${not empty imageDestinationLink}">
			<a target="${imageDestinationTarget}"
			href="${ycommerce:getUrlForCMSLinkComponent(imageDestinationLink)}"> 
			<picture>
			<source media="(max-width: 991px)" srcset="${mobileImage}">
			<img class="img-responsive" src="${desktopImage}" alt="${backgroundImage.imageDescription}"> </picture>
			</a>
		</c:when>
		<c:otherwise>
			<picture>
			<source media="(max-width: 991px)" srcset="${mobileImage}">
			<img class="img-responsive" src="${desktopImage}" alt="${backgroundImage.imageDescription}"> </picture>
		</c:otherwise>
	</c:choose>
	
	<div class="container carousel-content">
		<h1 class="display-large major">${heroBanner.titleText}</h1>
		<h2 class="hide"></h2>
		<h3 class="hide"></h3>
		<h4 class="hide"></h4>
		<h5>${heroBanner.subheadingText}</h5>
		<h5 class="major">
			<c:forEach items="${heroBanner.links}" var="link" varStatus="ctr">
				<c:if test="${not empty value && ctr.count <= value}">

					<c:if test="${not empty link && not empty link.linkName}">
						<c:choose>
							<c:when test="${link.target eq 'NEWWINDOW'}">
								<c:set var="target" value="_blank" />
							</c:when>
							<c:otherwise>
								<c:set var="target" value="_self" />
							</c:otherwise>
						</c:choose>
						<div data-satellitetrack="internal_click"
							data-analytics='{"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|hero_carousel","name": "${fn:toLowerCase(fn:replace(link.linkName," ","_"))}"
  					}'>
  					<c:choose>
						<c:when test="${not empty link.url}">
						<a target=${target } href="${link.url}">${link.linkName}&nbsp;<span>&rsaquo;</span></a>
						</c:when>
						<c:otherwise>
						${link.linkName}&nbsp;<span>&rsaquo;</span>
						</c:otherwise>
					</c:choose>
							
						</div>
					</c:if>
				</c:if>
			</c:forEach>
		</h5>
	</div>
</div>