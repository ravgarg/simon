<div class="error500">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="sitelogo">
						<img src="/_ui/responsive/simon-theme/images/simon-logo.svg" class="simon-logo logo-desktop">
						<img src="/_ui/responsive/simon-theme/images/logo_mobile.svg" class="simon-logo logo-mobile">
					</div>
				</div>
				<div class="col-md-8 show_desktop hide_mobile">
					<ul class="links major">
						<li><a href="#">investors</a></li>
						<li><a href="#">career</a></li>
						<li><a href="#">tourism</a></li>
						<li><a href="#">syf</a></li>
						<li><a href="#">blog</a></li>
						<li><a href="#">contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>

	<div class="error500-container">
		<picture>
			<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/image500@2x.jpg">
			<img class="img-responsive" src="/_ui/responsive/simon-theme/images/image500.jpg">
		</picture>
		<div class="container error500-content">
			<div class="row">
				<div class="col-xs-6 col-md-5">
					<h1 class="display-xl major">We're <span>sorry</span></h1>
					<h4>Our website is currenty unavailable due to scheduled maintenance. We regret any inconvenience.</h4>
					<h4>Please check back soon.</h4>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row error500-footer-link clearfix">
	            <div class="col-md-5 col-xs-12 footer-social">
	                   <a href="https://www.facebook.com/SimonPropertyGroup" title="Facebook" alt="Facebook">
	                <section class="clearfix">
	                       <img src="/_ui/responsive/simon-theme/icons/black-facebook.svg" >
	                   </a>
	                   <a href="https://twitter.com/simonpropertygp" title="Twitter" alt="Twitter">
	                       <img src="/_ui/responsive/simon-theme/icons/black-twitter.svg" >
	                   </a>
	                   <a href="https://www.youtube.com/user/SimonPropertyGroup" title="Instagram" alt="Instagram">
	                       <img src="/_ui/responsive/simon-theme/icons/black-instagram.svg" >
	                   </a>
	                   <a href="http://www.simon.com/foundatsimon" title="YouTube" alt="Youtube">
	                       <img src="/_ui/responsive/simon-theme/icons/black-youtube.svg">
	                   </a>
	                   <a href="https://instagram.com/simonpropertygroup" title="Pinterest" alt="Pinterest">
	                       <img src="/_ui/responsive/simon-theme/icons/black-pinterest.svg">
	                   </a>
	                </section>
	                <div class="footer-terms">
		                Copyright@ 1999-2017, Simon Property Group, L.P.All Right Reserved.
		            </div>
	            </div>
	            
         	</div>
		</div>

	</div>
</div>

