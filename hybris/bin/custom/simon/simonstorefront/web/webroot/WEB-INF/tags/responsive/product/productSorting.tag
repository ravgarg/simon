<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="col-md-4 select-box dropdown custom-dropdown">
	<div class="sortby hide-mobile">Sort By</div>
	<button class="dropdown-toggle hide-mobile" type="button" data-toggle="dropdown">
		<span class="label-text">
			<c:forEach items="${searchPageData.sorts}" var="sort">
				<c:if test="${sort.selected}">
					${fn:escapeXml(sort.name)}
				</c:if>
			</c:forEach>
		</span>
		<span class="caret-icon"></span>
	</button>

		<ul class="dropdown-menu analytics-plpSorting" data-analytics='{"event": {"type": "product_sorting","sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>"},"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>", "name": "product_sorting"}}' data-satellitetrack="product_filter">
			<c:forEach items="${searchPageData.sorts}" var="sort">
			<li data-value="${fn:escapeXml(sort.code)}" ${sort.selected? 'selected="selected"' : ''}>
				<a href="#"> 
					<c:choose>
						<c:when test="${not empty sort.name}">
	                    	${fn:escapeXml(sort.name)}
	                    </c:when>
						<c:otherwise>
							<spring:theme code="${themeMsgKey}.sort.${sort.code}" />
						</c:otherwise>
					</c:choose>
				</a>
			</li>
		</c:forEach>
	</ul>
</div>

<input type="hidden" name="sort" value="" />

<c:catch var="errorException">
	<spring:eval expression="searchPageData.currentQuery.query" var="dummyVar" />
	<%-- This will throw an exception is it is not supported --%>
	<input type="hidden" name="q" value="${searchPageData.currentQuery.query.value}" />
</c:catch>

<c:if test="${supportShowAll}">
	<ycommerce:testId code="searchResults_showAll_link">
		<input type="hidden" name="show" value="Page" />
	</ycommerce:testId>
</c:if>

<c:if test="${supportShowPaged}">
	<ycommerce:testId code="searchResults_showPage_link">
		<input type="hidden" name="show" value="All" />
	</ycommerce:testId>
</c:if>

<c:if test="${not empty additionalParams}">
	<c:forEach items="${additionalParams}" var="entry">
		<input type="hidden" name="${fn:escapeXml(entry.key)}" value="${fn:escapeXml(entry.value)}" />
	</c:forEach>
</c:if>