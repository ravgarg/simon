<div class="modal fade in" id="globalLoginModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <form>
                    <jsp:include page="global-login-content.jsp"></jsp:include>
                </form>
            </div>
        </div>
    </div>
</div>