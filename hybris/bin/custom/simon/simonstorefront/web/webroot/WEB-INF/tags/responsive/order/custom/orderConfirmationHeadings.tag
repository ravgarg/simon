<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="confirmation-heading">
    <div class="thank-you text-transform: capitalize"><spring:theme code='order.confirmation.salutation.txt'/>
    	<span><c:if test="${isLoggedIn}">&nbsp${order.deliveryAddress.firstName}.</c:if></span>
    </div>
    <div class="order-number" data-analytics-ordercode="${order.code}"><spring:theme code="order.confirmation.txt" arguments="${order.code}"/></div>
    <div class="order-receipt"><spring:theme code='order.confirmation.receipt.txt' arguments="${order.deliveryAddress.email}"/></div>
</div>