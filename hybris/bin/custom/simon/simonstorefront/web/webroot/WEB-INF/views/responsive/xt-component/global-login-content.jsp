<div class="sign-up-content">
    <div class="size-guide-content">
            <h6 class="major">Already a VIP Club Member?</h6>
            <p>Sign in now.</p>
        </div>
    <div class="sm-form-control">
       <div class="col-md-7 navbar-login">
              <div class="row">
                <div class="sm-input-group">
                  <input type="text" class="form-control" name="emailaddress" placeholder="EMAIl ADDRESS" title="EMAIl ADDRESS">
                </div>
                <div class="sm-input-group">
                  <input type="text" class="form-control" name="PASSWORD" placeholder="PASSWORD" title="PASSWORD">
                </div>
                </div>
              </div>
              <div class="clearfix links-container">
                    <label class="custom-checkbox">
                        <input type="checkbox" name="confirm" class="sr-only"/>
                        <i class="i-checkbox"></i>                    
                    </label>
                    <span>Stay Logged In</span>
                     
                    <a href="#" class="forgot-password pull-right">Forgot Password?</a>
              </div>
        <button class="btn">Login</button>
    </div>
    <div class="size-guide-content">
                <h6>VIP SHOPPERS CLUB</h6>
                <p>Join the VIP Club for free access to exclusive deals online and in stores at a center near you!</p>
            </div>
    <button class="btn secondary-btn black">Join Now</button>
</div>