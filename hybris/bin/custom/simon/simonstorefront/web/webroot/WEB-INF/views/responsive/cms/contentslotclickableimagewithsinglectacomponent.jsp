<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:if test="${not empty component }">
		<c:choose>
			<c:when test="${component.link.target eq 'newWindow'}">
		    	<c:set var="target" value="_blank" />
			</c:when>
		    <c:otherwise>
				<c:set var="target" value="_self" />
			</c:otherwise>
		</c:choose>
		
		<c:set var="desktopImage" value="${component.backgroundImage.desktopImage.url}" />
		<c:choose>
			<c:when test="${not empty component.backgroundImage.mobileImage}">
		    	<c:set var="mobileImage" value="${component.backgroundImage.mobileImage.url}" />
			</c:when>
		    <c:otherwise>
				<c:set var="mobileImage" value="${desktopImage}" />
			</c:otherwise>
		</c:choose>
		
		<c:if test="${isAuthenticatedMyCenter}"> 
		<c:set var="myCenterClass" value="loggedInUser" />
		</c:if>
    		
		<c:if test="${empty component.shape || component.shape eq 'SQUARE'}" >
		          <div class="analytics-data my-center-block" data-satellitetrack="internal_click"  data-analytics='{"event": {"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content-spot","sub_type": "${fn:toLowerCase(component.itemType)}"},
								  "banner": {
									"click": {
									  "id": "${fn:toLowerCase(component.id)}"
									}
								  },"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content_spot","name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.id)}</c:otherwise></c:choose>"}}'>
						<c:choose>
							<c:when test="${not empty component.link.url}">
							<a target=${target} href="${component.link.url}">
						
							<!--image width should not be less than 480px-->
							<picture>
								<source media="(max-width: 991px)" srcset="${mobileImage}"/>
								<img class="img-responsive img-hover-effect ${myCenterClass}" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}"/>
							</picture>
							<c:if test="${not empty component.link && not empty component.link.linkName}" >
								<h4 class="major">${component.link.linkName}</h4>
							</c:if>
						</a>
							</c:when>
							<c:otherwise>
							<!--image width should not be less than 480px-->
							<picture>
								<source media="(max-width: 991px)" srcset="${mobileImage}"/>
								<img class="img-responsive img-hover-effect ${myCenterClass}" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}"/>
							</picture>
							<c:if test="${not empty component.link && not empty component.link.linkName}" >
								<h4 class="major">${component.link.linkName}</h4>
							</c:if>
							</c:otherwise>
						</c:choose>
						
					</div>
		</c:if>
		
		<c:if test="${component.shape eq 'RECTANGLE'}" > 
		    <div class="analytics-data" data-satellitetrack="internal_click"  data-analytics='{"event": {"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content-spot","sub_type": "${fn:toLowerCase(component.itemType)}"},
								  "banner": {
									"click": {
									  "id": "${fn:toLowerCase(component.id)}"
									}
								  },"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|content_spot","name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.id)}</c:otherwise></c:choose>"}}'>				
				<c:choose>
					<c:when test="${not empty component.link.url}">
					<a target=${target} href="${component.link.url}">
					<picture>
						<source media="(max-width: 991px)" srcset="${mobileImage}"/>
						<img class="img-responsive img-hover-effect" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}">
					</picture>	
					<c:if test="${not empty component.link && not empty component.link.linkName}" >
						<h4 class="major">${component.link.linkName}</h4>
					</c:if>
				</a>
					</c:when>
					<c:otherwise>
					<picture>
						<source media="(max-width: 991px)" srcset="${mobileImage}"/>
						<img class="img-responsive img-hover-effect" src="${desktopImage}" alt="${component.backgroundImage.imageDescription}">
					</picture>	
					<c:if test="${not empty component.link && not empty component.link.linkName}" >
						<h4 class="major">${component.link.linkName}</h4>
					</c:if>
					</c:otherwise>
				</c:choose>
				
			</div>	
		</c:if>	
</c:if>
	