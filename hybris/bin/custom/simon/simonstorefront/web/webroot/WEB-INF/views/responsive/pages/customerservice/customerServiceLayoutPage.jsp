<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<template:page pageTitle="${pageTitle}">
<div class="analytics-pageContentLoad" data-analytics-pagetype="${fn:toLowerCase(cmsPage.name)}" data-analytics-subtype="${fn:toLowerCase(cmsPage.name)}" data-analytics-contenttype="informational">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="breadcrumbs col-md-3 hide-mobile">
						<a href="/"><spring:theme
								code="search.page.breadcrumb.home" /></a> <span class="arrow-fwd"></span>
						<a href="#" class="active">${cmsPage.name}</a>
					</div>
				</div>
			</div>

			<aside class="col-md-3 left-menu-container show-desktop">
				<cms:pageSlot position="csn_CustomerServicePageLeftNavigationSlot"
					var="feature">
					<cms:component component="${feature}" element="div"
						class="span-24 section1 cms_disp-img_slot" />
				</cms:pageSlot>
			</aside>
			<div class="col-md-9">
			  <div class="myorder-details-container row">
			  <div class="col-md-12">
				<div class="btn-category show-mobile">
					<div class="browse-categories">
						<button 
							id="mBrowseCategoriesBtn"
							class="btn block black secondary-btn analytics-genericLink" data-analytics-eventsubtype="contact-us" data-analytics-linkplacement="${fn:toLowerCase(cmsPage.name)}|contact-us" data-analytics-linkname="contact-us" 
							type="button"
							data-toggle="modal" 
							data-dismiss="modal" 
							data-target="#contact-us">
							<spring:theme code="text.footer.customer.service.btn" />
						</button>
					</div>
				</div>
				
				<cms:pageSlot position="csn_CustomerServicePageFirstContentSlot"
					var="feature">
					<cms:component component="${feature}" element="h1"
						class="page-heading" />
				</cms:pageSlot>

				<cms:pageSlot position="csn_CustomerServicePageSecondContentSlot"
					var="feature">
					<cms:component component="${feature}" element="div"
						class="span-24 section1 cms_disp-img_slot" />
				</cms:pageSlot>
				
				<cms:pageSlot position="csn_CustomerServicePageThirdContentSlot"
					var="feature">
					<div class="row haveanaccount">
					<div class="col-md-7 col-xs-12">
					<cms:component component="${feature}" element="div"
						class="span-24 section1 cms_disp-img_slot" />
					</div>
					<!-- have an account -->
					<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">	
					<!-- Global Login Modal Starts Here -->
					 <user:globalLoginModal />
					<!-- Global Login Modal Ends Here -->	
					<div class="col-md-3 col-xs-12 haveacc">
						<spring:theme code="track.order.form.have.an.account.text" />
					</div>
					<div class="col-md-2 col-xs-12 btn-style">
	       				<button class="btn secondary-btn black openLoginModal" data-fav-name="track-order"><spring:theme code="track.order.form.signin.text" /></button>
	       			</div>	
	       			</sec:authorize>
	       			<!-- end have an account -->				
					</div>	
				</cms:pageSlot>
				
				<cms:pageSlot position="csn_CustomerServicePageFourthContentSlot"
					var="feature">
					<cms:component component="${feature}" element="div"
						class="span-24 section1 cms_disp-img_slot" />
				</cms:pageSlot>

				<cms:pageSlot position="csn_CustomerServicePageFifthContentSlot"
					var="feature">
					<cms:component component="${feature}" element="div"
						class="span-24 section1 cms_disp-img_slot" />
				</cms:pageSlot>

			</div>
           </div>
          </div>
		</div>
	</div>

	<div class="modal fade" id="contact-us" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><spring:theme code="text.footer.customer.service.btn" /></h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<cms:pageSlot position="csn_CustomerServicePageLeftNavigationSlot"
						var="feature">
						<cms:component component="${feature}" element="div"
							class="span-24 section1 cms_disp-img_slot" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</div>
</template:page>