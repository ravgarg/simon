
    <div class="myprofile">
		<a href="#" class="banner">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/hero-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/hero-image2.png">
			</picture>
			<div class="img-caption">
				<h1>Lorem Ipsum</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
		</a>
    	<div class="myprofile-container">
	    	<form action="submit">
		       <div class="panel-group" id="accordion">       
			      <div class="panel panel-default">
						<h5 class="panel-title">
						    <a class="accordion-toggle major" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true">Lorem Ipsum</a>
						</h5>
						<div id="panel1" class="panel-collapse collapse in" aria-expanded="true">
						    <div class="collapsed-content">
							    <div class="row">
							    	<div class="col-md-12"><p class="instruction-msg">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p></div>
					                <div class="col-xs-12 col-md-12">
					                   <div class="sm-input-group">
					                      <input type="text" class="form-control" placeholder="Email Address">
					                   </div>
					                </div>
					                <div class="col-xs-12 col-md-6">
					                   <div class="sm-input-group">
					                      <input type="text" class="form-control" placeholder="Password">
					                   </div>
					                </div>
					                <div class="col-xs-12 col-md-6">
					                   <div class="sm-input-group">
					                      <input type="password" autocomplete="off" class="form-control" placeholder="Confirm Password">
					                   </div>
					                </div>
					             <div class="col-md-12">
					             	<label class="custom-checkbox"><input type="checkbox" class="sr-only" id="changePassCheckbox"/><i class="i-checkbox"></i>Change Password</label> 
					             </div>	
					                <div class="col-md-12">
					                	<div class="instructions">
						                	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						                	<ul>
						                		<li>Uppercase characters</li>
						                		<li>Numeric: 0-9</li>
						                		<li>Lowercase characters</li>
						                		<li>Non-alphanumeric: ex. ~!@#$%</li>
						                	</ul>
					                	</div>
					                </div>					             
							     </div>
							     
				                <div class="form-fields">
						             <div class="row">
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group">
						                      <input type="text" class="form-control reqiured" placeholder="First Name">
						                   </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group">
						                      <input type="text" class="form-control" placeholder="Last name">
						                   </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group">
						                      <input type="text" class="form-control" placeholder="zip code">
						                   </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group select-menu">
	                                          <select class="form-control">
	                                            <option value="">United States</option>
	                                          </select>
	                                        </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group select-menu">
	                                          <select class="form-control">
	                                            <option value="">Birth Month</option>
	                                          </select>
	                                        </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group select-menu">
	                                          <select class="form-control">
	                                            <option value="">Birth Year</option>
	                                          </select>
	                                        </div>
						                </div>
						                <div class="col-xs-12 col-md-6">
						                   <div class="sm-input-group select-menu">
	                                          <select class="form-control">
	                                            <option value="">Gender</option>
	                                          </select>
	                                        </div>
						                </div>
						             </div>	
						             <div class="clearfix row">
						             	<div class="update-profile col-md-12 small-text">
											Saves changes to your account by selecting update
										</div>
						             	<div class="submit-btn col-md-12 col-xs-12">
						             		<button class="btn">Update</button>
						             	</div>
						             </div>						             
				                </div>
						    </div>
						</div>
			       </div>      
			      <div class="panel panel-default next-section">
						<h5 class="panel-title">
						    <a class="accordion-toggle major" data-toggle="collapse" data-parent="#accordion" href="#panel2" aria-expanded="true">My Center</a>
						</h5>
						<div id="panel2" class="panel-collapse collapse in" aria-expanded="true">
						    <div class="collapsed-content">
							    <div class="row">
							    	<div class="col-md-12">
							    		<p class="instruction-msg">Please select your most frequently visited Premium Outlet centers so that we can provide you with relevant VIP deals and coupons.</p>
							    		<p class="instruction-msg">Idque option torquatos usu cu, mei in voluptua elaboraret. Eu atqui dicant adversarium mea, cu scripta fierent sea.</p>
							    	</div>	
					                <div class="col-md-12">
					                   <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Primary Center</option>
                                          </select>
                                        </div>
					                </div>	
					                <div class="col-md-12">
					                   <div class="sm-input-group select-menu">
                                          <select class="form-control">
                                            <option value="">Select One / Several Centers</option>
                                          </select>
                                        </div>
					                </div>			             
							     </div>	
					             <div class="clearfix row">
					             	<div class="update-profile col-md-12 small-text">
										Saves changes to your account by selecting update
									</div>
					             	<div class="submit-btn col-md-2 col-xs-12">
					             		<button class="btn">Update</button>
					             	</div>
					             	<div class="terms-conditions col-md-10 col-xs-12 small-text">
					             		<div class="faqs">VIP FAQs</div>
					             		<div class="security">128-bit SSL Bank-level Security</div>
									</div>
					             </div>	
						    </div>
						</div>
			       </div>
		       </div>
	       </form>
       </div>
       
		<a href="#" class="banner">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/hero-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/hero-image2.png">
			</picture>
			<div class="img-caption">
				<h1>Lorem Ipsum</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
			</div>
		</a>
		<div class="other-links">
			<ul>
				<li><a href="#">Email Unsubscribe</a></li>
			</ul>
		</div>
    </div>