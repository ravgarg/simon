<div class="container">
    <div class="empty-bag">Your Shopping Bag Is Empty</div>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <jsp:include page="global-tiles-carousel.jsp"></jsp:include>
        </div>
        <div class="col-md-12 col-xs-12">
            <jsp:include page="trending-now.jsp"></jsp:include>
        </div>
    </div>
</div>