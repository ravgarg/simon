/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'mobileMegaMenu', 'utility', 'analytics'], function($, mobileMegaMenu, utility, analytics) {
    'use strict';
    var cache = {};
    var globalMegaMenu = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.initAjax();
            this.megaMenuNevigation();
            this.initMobileMenu();
            this.leftMobileMenu();
        },
        initVariables: function() {
            cache = {
                $menuLevel1Ancor: $('.menu-level-1 a'),
                $desktopMegaMenu: $('.desktop-mega-menu'),
                $desktopCatPanel: $('.desktop-cat-panel'),
                $megaMenuBanner: $('.mega-menu-banner'),
                $navbar: $('#navbar'),
                $topMenuOpen: $('.top-menu-open'),
                $mainMenu: $('.main-menu'),
                $mobileMenuAccordian : $('.mobile-accordian'),
                $document : $(document),
                $navLoginFlyout : $('.login-area'),
                $mobileSidebarWrapper : $('.mobile-sidebar-wrapper'),
                $back : $('.back'),
                $mainMenuToggle : $('.main-menu-toggle')                
            }
        },
        initAjax: function() {},
        initEvents: function() {
            cache.$document.on('click', '.search-icon', globalMegaMenu.searchIcon);
            cache.$document.on('keydown', '.menu-level-1 a', globalMegaMenu.handleLeftToRightArrow);
            cache.$document.on('keydown', '.desktop-cat-panel a', globalMegaMenu.handleRightToLeftArrow);
            //init calculate height function.
            this.calculateNavHeight();
        },
        searchIcon: function(){
			if($('#analyticsSatelliteFlag').val()==='true'){
				analytics.analyticsOnPageHeaderSearchListClick();
			}
            $("form[name='search_form_cm_searchBox']").submit();
        },
        handleRightToLeftArrow: function(event){
            if (event.keyCode === 37) {
                // left arrow key pressed, focus to 1st level category 
                cache.$menuLevel1Ancor.eq(0).focus();
            }
        },
        handleLeftToRightArrow: function(event){
            if (event.keyCode === 39) {
                // right arrow key pressed, focus to 2nd level categories link
                $($(this).data('cat-id')).find('a').eq(0).focus();
            }
        },
        initMobileMenu: function() {
            cache.$mainMenu.mobileMegaMenu({
                changeToggleText: true,
                enableWidgetRegion: true,
                prependCloseButton: true,
                stayOnActive: true,
                toogleTextOnClose: '<span class="mobile-close-menu"></span>',
                menuToggle: 'main-menu-toggle'
            });
            $("li.mymobilemenu > ul").css("display","none");
            cache.$document.on('click', '.open-accordian', function(e) {
                e.preventDefault();
                $(this).next('.mobile-accordian').collapse('toggle');
            });

            cache.$mobileMenuAccordian.on('show.bs.collapse hidden.bs.collapse', function() {
                $(this).prev('a').toggleClass('minus-icon');
            });
        },
        // Mobile left column menu
        leftMobileMenu: function() {
            // first shop link active
            cache.$mainMenuToggle.click(function() {
                $('body').toggleClass('stopScroll');
                cache.$mainMenu.find('.mobile-sidebar-wrapper a:first-child').toggleClass('active');
                $('.mobile-sidebar-wrapper a:first-child').on('click', function() {
                    $(this).parents(cache.$mainMenu).find('nav #main').show();
                    $(this).parents(cache.$mainMenu).removeClass('newbg');
                });
                    
            });

            cache.$mobileSidebarWrapper.find('> a').on('click', function(e) {
                e.preventDefault();
                var $this = $(this),
                    links = 'ul.nav, .nav-login-flyout, #shop';
                cache.$mobileSidebarWrapper.find('a').removeClass('active');
                if($this.next(links).css('display') === 'block'){
                    $this.removeClass('active');
                    $this.parents(cache.$mainMenu).find('.mobile-sidebar-wrapper + nav').children().show();
                    cache.$mainMenu.find(links).hide();
                    $this.next(links).parents('.main-menu').removeClass('newbg');
                } else {
                    $this.addClass('active');
                    $this.parents(cache.$mainMenu).find('.mobile-sidebar-wrapper + nav').children().hide();
                    cache.$mainMenu.find(links).hide();
                    $this.next(links).show();
                    $this.next(links).find('ul.dropdown-menu').addClass('is-in-view open-slide');
                    $this.next(links).parents('.main-menu').addClass('newbg');
                }         
            });
        },
        megaMenuNevigation: function() {
            cache.$menuLevel1Ancor.on('mouseover focus', function() {
                var catId = $(this).data('cat-id');
                $(catId).removeClass('addScroll');
                cache.$menuLevel1Ancor.removeClass('active');
                $(this).addClass('active');
                cache.$desktopMegaMenu.addClass('white-bg');
                cache.$desktopCatPanel.hide();
                cache.$megaMenuBanner.show();
                
                //Adding scroll to the right header section if content height is greater than the viewport
                if($(catId).height() > 518) {
                	$(catId).addClass('addScroll'); 
                }
                $(catId).show();
            });
            cache.$topMenuOpen.on('hide.bs.dropdown', function() {
                cache.$desktopMegaMenu.removeClass('white-bg');
                cache.$desktopCatPanel.hide();
                cache.$menuLevel1Ancor.removeClass('active');
                cache.$navbar.removeClass('navbarborder');
                cache.$megaMenuBanner.hide();
                utility.pageScreenBlurOnHeaderLinks('hide');

            });
            cache.$topMenuOpen.on('show.bs.dropdown', function() {
                cache.$navbar.addClass('navbarborder');
                utility.pageScreenBlurOnHeaderLinks('show');
            });
            cache.$navLoginFlyout.on('hide.bs.dropdown', function() {
                utility.pageScreenBlurOnHeaderLinks('hide');
            });
            cache.$navLoginFlyout.on('show.bs.dropdown', function() {
                utility.pageScreenBlurOnHeaderLinks('show');
            });
            cache.$desktopMegaMenu.on('click', function(e) {
				analytics.analyticsOnPageHeaderNavClick(event.target);
                e.stopPropagation();
            })
            $(document).on('click', '.second-level-menu a', function(e) {
                e.stopImmediatePropagation();
            })
        },

        //this function is used to calculate top navigation height.
        calculateNavHeight:function () {
            $(document).ready(function () {
                var height = 0;
                var count = 0;
                $('.menu-level-1').find("li").each(function () {
                    if (height === 0) {
                        height = $(this).height();
                    } else {
                        height = height + $(this).height();
                    }
                    ++count;
                });
                if(count > 10){
                    $('.menu-borderleft').css('min-height', 440 + 'px');
                    $('.menu-level-1').css('height', 440 + 'px');
                    $('.menu-level-1').css('overflow','auto');
                }else{
                    $('.menu-borderleft').css('min-height', height + 20 + 'px');
                }
            });
        }
    };
    $(function() {
        globalMegaMenu.init();
    });
    return globalMegaMenu;
});