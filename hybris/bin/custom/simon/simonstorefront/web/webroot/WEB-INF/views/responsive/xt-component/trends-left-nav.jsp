<ul class="left-menu level-0 list-unstyled">
    <li><a href="#">New Arrivals</a></li>
    <li><a href="#">Designers</a></li>
    <li><a href="#">Deals</a></li>
    <li><a href="#">Shoes</a></li>
    <li><a href="#">Handbags</a></li>
    <li><a href="#">Jewelry & Accessories</a></li>
    <li><a href="#">Gifts & Gift Cards</a></li>
    
    <li class="has-level-1">
        <a href="#subMenuL1Id10" class="collapsed" data-toggle="collapse">Clothing</a>
        <ul class="level-1 collapse" id="subMenuL1Id10">
            <li><a href="#">All Clothing</a></li>
            <li><a href="#">Tops</a></li>
            <li >
                <a href="#subMenuL2Id3">Bottoms</a>
                <ul class="level-2 collapse" id="subMenuL2Id3">
                    <li><a href="#">Day</a></li>
                    <li><a href="#">Evening</a></li>
                    <li><a href="#">Petites</a></li>
                    <li><a href="#">Active</a></li>
                </ul>
            </li>
            <li >
                <a href="#subMenuL2Id4">Dresses</a>
                <ul class="level-2" id="subMenuL2Id4">
                    <li><a href="#">All Dresses</a></li>
                    <li class="active"><a href="#">Bridal</a></li>
                    <li><a href="#">Day</a></li>
                    <li><a href="#">Evening</a></li>
                    <li><a href="#">Petites</a></li>
                    <li><a href="#">Active</a></li>
                </ul>
            </li>
            <li><a href="#">Jackets & Coats</a></li>
            <li><a href="#">Active Wear</a></li>
            <li><a href="#">Swimwear</a></li>
            <li><a href="#">Sleepwear</a></li>
            <li><a href="#">Intimate Apparel</a></li>
            <li><a href="#">Petites</a></li>
            <li><a href="#">Plus Sizes</a></li>
        </ul>
    </li>
    <li class="has-level-1">
        <a href="#subMenuL11" class="collapsed" data-toggle="collapse">Beauty & Fragrance</a>
        <ul class="level-1 collapse" id="subMenuL11">
            <li><a href="#">Day</a></li>
            <li><a href="#">Evening</a></li>
            <li><a href="#">Petites</a></li>
            <li><a href="#">Active</a></li>
        </ul>
    </li>
</ul>