<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<template:page pageTitle="${pageTitle}">
<input type="hidden" data-fav-url="true" value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
<input type="hidden" data-updateapi="/cart/updateMultiD" id="cartData" class="analytics-cartData" value='${fn:replace(cartDataJSON, "'", "&apos;")}'/>
	<div class="container">
		<c:if test="${not empty cartData}">
			<div id="messageContainer" class="hide">
				<div class="item-removed">
					<c:forEach items="${cartData.globalFlashMsg}" var="globalFlashMsg">
						<span class="icon-removed"></span>
						<span class="remove-desc">${globalFlashMsg.message}</span>
					</c:forEach>
				</div>
			</div>
		</c:if>
		<c:choose>
			<c:when test="${not empty cartData && not empty cartData.retailerInfoData}">
				<div>
					<div class="breadcrumbs hide-mobile">
						<a href="/" class="analytics-genericLink" data-analytics-eventtype="continue_shopping" data-analytics-eventsubtype="shopping_bag" data-analytics-linkplacement="shopping_bag" data-analytics-linkname="continue_shopping" data-satellitetrack="link_track">
							<span class="back-to"></span>
							<span>${cartData.messageLabels.continueUrlLabel}</span>
						</a>
					</div>
					<div class="row">
						<cms:pageSlot position="csn_topContentSlot-cartPage" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
						<div class="col-xs-12 col-md-4 order-summary-right anchoredRightRail">
							<div class="cart-order-summary">
								<div class="ordersummaryContainer"></div>
								<div class="btn-padding">
									<form action="/cart/checkout/" method="get">
										<button class="btn plum analytics-checkOutBtn" data-analytics-eventtype="proceed_checkout_top_right" data-analytics-eventsubtype="shopping_bag" data-analytics-ecommercetype="shopping_bag" data-analytics-linkplacement="shopping_bag|top_right" data-analytics-linkname="proceed_checkout" data-satellitetrack="proceed_checkout"><spring:theme code="cart.button.text.proceed.to.checkout" /></button>
									</form>
								</div>


								<div class="cart-help-container hide-mobile">
									<div class="help-info">${cartData.messageLabels.helpFulInfoLabel}</div>
									<div class="information">${cartData.messageLabels.helpFullInfoMessageLabel}</div>
									<div class="question">
										${cartData.messageLabels.questionLabel}?
										<cms:pageSlot position="csn_contactUsContentSlot-cartPage" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
									<div class="more-details analytics-shippingReturn" data-analytics-eventtype="link_click|policy_links" data-analytics-eventsubtype="view_shipping_method" data-analytics-linkplacement="cartpage" data-analytics-linkname="policy_links" data-analytics-satellitetrack="link_track">
										<cms:pageSlot position="csn_shippingContentSlot-cartPage" var="feature">
											<cms:component component="${feature}" />
										</cms:pageSlot>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
							<div id="cartEnteries"></div>
							<div id="cartbottomOrderSummary">
								<div class="order-summary-bottom">
									<div class="ordersummaryContainer"></div>
									<div class="btn-padding">
										<form action="/cart/checkout/" method="get">
											<button class="btn plum analytics-checkOutBtn" data-analytics-eventtype="proceed_checkout_bottom" data-analytics-eventsubtype="shopping_bag" data-analytics-ecommercetype="shopping_bag" data-analytics-linkplacement="shopping_bag|bottom" data-analytics-linkname="proceed_checkout" data-satellitetrack="proceed_checkout"><spring:theme code="cart.button.text.proceed.to.checkout" /></button>
										</form>
									</div>
									<div class="cart-help-container">
										<div class="help-info">${cartData.messageLabels.helpFulInfoLabel}</div>
										<div class="information">${cartData.messageLabels.helpFullInfoMessageLabel}</div>
										<div class="question">
											${cartData.messageLabels.questionLabel}?
											<cms:pageSlot position="csn_contactUsContentSlot-cartPage" var="feature">
												<cms:component component="${feature}" />
											</cms:pageSlot>
										</div>
										<div class="more-details analytics-shippingReturn" data-analytics-eventtype="link_click|policy_links" data-analytics-eventsubtype="view_shipping_method" data-analytics-linkplacement="cartpage" data-analytics-linkname="policy_links" data-analytics-satellitetrack="link_track">
											<cms:pageSlot position="csn_shippingContentSlot-cartPage" var="feature">
												<cms:component component="${feature}" />
											</cms:pageSlot>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="row">
					<cms:pageSlot position="csn_emptyCartMiddleContentSlot-cartPage" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<div class="col-xs-12 col-md-8">
						<div id="cartEnteries"></div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<!--policy modal-->
	<div class="modal fade" id="shippingReturnModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<spring:theme code="return.policy.message" />
					</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="privacy-content scrollablediv scroll-container"></div>
			</div>
		</div>
	</div>
	<!--policy modal-->

	<!--confirmation  modal-->
	<div class="modal fade analytics-removeProductWrapper"
		id="removeConfirmation" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><spring:theme code="cart.button.text.remove.item" /></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="privacy-content">
					<spring:theme code="cart.label.text.remove.ask.confirmation" />
					<input type="hidden" name="removeEntryNumber" id="removeEntryNumber" value="">
					<input type="hidden" name="removeProductCode" class="analytics-removeProductCode" id="removeProductCode" value="">
				</div>
				<div class="btn-container clearfix cart-modal-footer">
					<button class="btn plum major analytics-removeCartProduct" data-analytics='{ "event": { "type": "remove_product","sub_type": "shopping_bag" }, "link": {"placement": "shopping_bag", "name": "product_remove" }}'
						data-satellitetrack="product_removal" id="removeEntry" data-action="remove"><spring:theme code="cart.button.text.confirm.remove" /></button>
					<button class="btn secondary-btn black major" data-dismiss="modal" aria-label="Close"><spring:theme code="cart.button.text.decline.remove" /></button>
				</div>
			</div>
		</div>
	</div>
	<!--confirmation modal-->
</template:page>