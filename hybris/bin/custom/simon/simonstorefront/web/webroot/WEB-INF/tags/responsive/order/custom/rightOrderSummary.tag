<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="cart-order-summary">
	<div class="bag-summary hide-mobile">
		<spring:theme code='order.confirmation.summery.txt' />
	</div>
	<div class="summary-border-top"></div>
	<div class="view-bag-summary hide-desktop pad-r-mobile pad-l-mobile">
		<spring:theme code='order.confirmation.view.summery.txt' />
	</div>
	<div class="order-summary-price-container-confirm">
		<div class="price-top-spacing">
			<span class="estimated-subtotal"><spring:theme code='order.confirmation.estimated.order.txt' /></span>
			<span class="estimated-sub-price">${order.subTotal.formattedValue}</span>
		</div>
		<div class="ship-top-spacing">
			<span class="estimated-shipping"><spring:theme code='order.confirmation.estimated.shipping.charge.txt' /></span>
			<span class="estimated-shipping-price">
				<c:choose>
					<c:when test="${!empty order.deliveryCost.value}">
						${order.deliveryCost.formattedValue}
					</c:when>
					<c:otherwise>
						--
					</c:otherwise>
				</c:choose>
			</span>
		</div>
		<div class="tax-top-spacing">
			<span class="estimated-tax">
				<spring:theme code='order.confirmation.estimated.tax.txt' />
				
				<a href="#" data-toggle="modal" data-target="#orderSummaryHelpModal">
					<img src="/_ui/responsive/simon-theme/icons/black-question.svg" class="tooltip-init simon-logo" alt="black-question.svg">
				</a>
				
			</span>
			<span class="estimated-tax-price">
				<c:choose>
					<c:when test="${!empty order.totalTax.value}">
						${order.totalTax.formattedValue}
					</c:when>
					<c:otherwise>
						--
					</c:otherwise>
				</c:choose>
			</span>
		</div>
		<div class="last-call-spacing">
			<span class="last-call"><spring:theme code='order.confirmation.estimated.order.total.txt' /></span>
			<span class="last-call-price">${order.totalPrice.formattedValue}</span>
		</div>
		<div class="you-save-spacing">
			<span class="you-save"><spring:theme code='order.confirmation.you.save.txt' /></span>
			<span class="you-save-price">
				<c:choose>
					<c:when test="${!empty order.totalDiscounts.value}">
						${order.totalDiscounts.formattedValue}
					</c:when>
					<c:otherwise>
						--
					</c:otherwise>
				</c:choose>
			</span>
		</div>
	</div>
	<div class="border-btm"></div>
	<div class="cart-help-container hide-mobile">
		<div class="help-info"><spring:theme code='checkout.right.summary.helpful.information.text'/></div>
		<div class="information"><spring:theme code='checkout.right.summary.helpful.information.content.text'/></div>
		<div class="question">
			<spring:theme code='checkout.right.summary.question.text'/>
		</div>
		<div class="more-details">
			<a href="#" class="analytics-genericLink" data-analytics-eventtype="link_click|policy_links" data-analytics-eventsubtype="view_shipping_method" data-analytics-linkplacement="ordersummary" data-analytics-linkname="policy_links" data-analytics-satellitetrack="link_track"><spring:theme code='checkout.right.summary.shipping.methods.charges.details.text'/></a>
		</div>
	</div>
</div>