<div class="modal fade addressBookModal" id="addressBookModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add New Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <p class="optional-fields">*Indicates optional field</p>
                <form>
                    <div class="address-fields">
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "First Name">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Last Name">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Address">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "APT / FLOOR / SUITE*">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "City">
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-3">
                              <div class = "sm-input-group list-dropdown">
                                 <select class = "form-control major">
                                    <option value="State">State</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-xs-7 col-md-2">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "ZIP Code">
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-12 col-md-7">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Emal Address">
                              </div>
                           </div>
                           <div class="col-xs-12 col-md-5">
                              <div class = "sm-input-group">
                                 <input type = "text" class = "form-control major" placeholder = "Phone Number">
                              </div>
                           </div>
                        </div>
                        <div class="clearfix">
                           <label class="checkboxes">
                              <div class="sm-input-group pull-left">
                                 <input type="checkbox" name="confirm" class="custom-checkbox"/>
                              </div>
                              <span class="make-default-text">Make Default Address</span>
                           </label>
                        </div>
                        <div class="address-buttons">
                            <button class="btn btn-add">ADD</button>
                            <button class="btn secondary-btn black" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>