
<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="searchPageData" required="true"
	type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div
	class="col-md-5 page-container select-box dropdown dropdown-accordion"
	data-accordion="#accordion">
	<div class="product-count hide-mobile"> <spring:theme code='plp.displaying.text' /> 
	    <span id="productItem-count">${searchPageData.pagination.totalNumberOfResults}</span> 
	     <c:choose>
			 <c:when test="${searchPageData.pagination.totalNumberOfResults gt 1}">
				<spring:theme code='search.page.count.items.text' />
			 </c:when>
				<c:otherwise>
					<spring:theme code='search.page.count.item.text' />
				</c:otherwise>
		</c:choose>  
    </div>
	<button class="dropdown-toggle hide-mobile" type="button" data-toggle="dropdown">
		<c:set var="filterSize" value="${searchPageData.breadcrumbs.size()}" />
  		<c:set var="baseProductRetailerNameCount" value="0" />
		<c:set var="baseProductDesignerNameCount" value="0" />
  		<c:forEach items="${searchPageData.breadcrumbs}" var="breadcrumb">
    	<c:if test="${breadcrumb.getFacetCode() eq 'categoryPath' }"> 
    	 <c:set var="filterSize" value="${searchPageData.breadcrumbs.size()-1}" />
    	 </c:if>
     	<c:if test="${breadcrumb.getFacetCode() eq 'baseProductRetailerName' }"><c:set var="baseProductRetailerNameCount" value="${baseProductRetailerNameCount+1}" /> </c:if>
        <c:if test="${breadcrumb.getFacetCode() eq 'baseProductDesignerName' }"><c:set var="baseProductDesignerNameCount" value="${baseProductDesignerNameCount+1}" /> </c:if>
    	</c:forEach>
		<spring:theme code='plp.filter.heading' /> 
	    <c:if test="${filterSize >0 }"> <span>(${filterSize}) </span></c:if> 
	 	<span class="caret-icon"></span>
	</button>
	
	<ul class="dropdown-menu analytics-plpFiltering" data-analytics='{"event": {"type": "product_filter","sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>"},"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>", "name": "product_filter"}}' data-satellitetrack="product_filter"  role="menu" aria-labelledby="dLabel">
		<li>
			<div class="panel-group" id="accordion">
				
					<c:if test="${not empty searchPageData.breadcrumbs}">
						<c:url value="${searchPageData.currentQuery.clearAllUrl}"
							var="clearALlQueryUrl" />
							<div class="clear-all hide-desktop">
								<a href="${clearALlQueryUrl}" class="clearAllFilter">Clear All Filters</a>
							</div>
					</c:if>
				
				<c:forEach items="${searchPageData.facets}" var="facets"
					varStatus="status">
					  <c:set var="sectedFacetCount" value="0" />
                        <c:forEach items="${searchPageData.breadcrumbs}" var="breadcrumb">
                        <c:if test="${breadcrumb.getFacetCode() eq facets.code }">
                         <c:set var="sectedFacetCount" value="${sectedFacetCount+1}" /> 
                        </c:if>
                       </c:forEach>
												
					<c:if test="${!(facets.code eq 'baseProductRetailerName' || facets.code eq 'baseProductDesignerName' || facets.code eq 'categoryPath')}">
					<div class="panel multiselect-filter" data-facet="${facets.code}">
					<c:set var="clearFacetHide" value="hide"/>
						<div class="panel-heading">
						<c:forEach items="${facets.values}" var="facetValue">
						<c:if test="${facetValue.selected}">
						<c:set var="clearFacetHide" value=""/>
						</c:if>
						</c:forEach>
								<a class="panel-title collapsed" href="#collapse${status.index}"
									data-toggle="collapse" data-parent="#accordion">${facets.name}&nbsp;<span class="filter-count"><c:if test="${sectedFacetCount gt 0}">(${sectedFacetCount})</c:if></span></a><a class="clear-facets ${clearFacetHide}" href="javascript:void(0);"><spring:theme
											code='plp.clear.text' /></a>
							</div>
							
							<div class="panel-collapse collapse" id="collapse${status.index}">
								<div class="panel-body">
											<c:if test="${facets.displayFacetType eq 'SQUAREIMAGE'}" >
											<div class="select-color">
											<c:set var="count" value="0" />
											<c:forEach items="${facets.values}" var="facetValue"
												varStatus="loop">
												<c:set var="selected" value="" />
												<c:if test="${facetValue.selected}">
													<c:set var="selected" value="active" />
													<c:set var="count" value="${count+1 }" />
												</c:if>
												<c:url value="${facetValue.query.url}"
													var="facetValueQueryUrl" />
												<div class="color-palette-item multiselect-item ${selected}"
													data-code="${fn:escapeXml(facetValue.code)}">
													<c:choose>
													<c:when test="${not empty facetValue.name }">
													<a href="#" data-href="${facetValueQueryUrl}"
														class="color-switches"
														data-color-name="${fn:escapeXml(facetValue.code)}"
														style="background: url(${facetValue.name});"></a> <span>${fn:escapeXml(facetValue.code)}</span>
												    </c:when>
													<c:otherwise>
													<a href="#" data-href="${facetValueQueryUrl}"
														class="color-switches"
														data-color-name="${fn:escapeXml(facetValue.code)}"
														style="background: url('/_ui/responsive/simon-theme/images/question-mark.jpg');"></a> <span>${fn:escapeXml(facetValue.code)}</span>
												    </c:otherwise>	
												   </c:choose> 
												</div>
											</c:forEach>
											</div>
										</c:if>
										
										
										<c:if test="${facets.displayFacetType eq 'SQUARE'}" >
										<div class="size-palette clearfix">
												<c:set var="count" value="0" />
										<c:forEach items="${facets.values}" var="facetValue">
											<c:set var="selected" value="" />
											<c:if test="${facetValue.selected}">
												<c:set var="selected" value="active" />
												<c:set var="count" value="${count+1 }" />
											</c:if>
											<c:url value="${facetValue.query.url}"
												var="facetValueQueryUrl" />

											<a href="#" data-href="${facetValueQueryUrl}"
												data-code="${fn:escapeXml(facetValue.name)}"
												class="auto-size multiselect-item ${selected}">${fn:escapeXml(facetValue.name)}</a>

										</c:forEach>
										</div>
											</c:if>
										<c:if test="${facets.displayFacetType eq 'CHECKBOX'}" >
												<c:forEach items="${facets.values}" var="facetValue" varStatus="counterSel">
														<c:set var="selected" value="" />
														<c:if test="${facetValue.selected}">
															<c:set var="selected" value="active" />
															
														</c:if>
														<c:url value="${facetValue.query.url}"
															var="facetValueQueryUrl" />										
															<c:if test="${ not facetValue.favoriteStore}">
																<div class="dynamic-listing-filters">
											<ul id="searchList" class="searchList">
												<li><label for="checkbox-collapse-${countCollapse.index}" data-href="${facetValueQueryUrl}"
													data-code="${fn:escapeXml(facetValue.code)}"
													class="auto-size multiselect-item ${selected} custom-checkbox"><input id="checkbox-collapse-${countCollapse.index}"
														type="checkbox" name="checkedRows" value="" <c:if test="${facetValue.selected}">
												checked="true"
												</c:if>>
													<em class="i-checkbox"></em><span>${fn:escapeXml(facetValue.name)}</span></label>
												</li>
											</ul>
										</div>
															</c:if>
													</c:forEach>
											
											</c:if>
										
									<a class="btn black apply-btn analytics-filterApplyBtn" href="#" data-href=""> <span><spring:theme
												code='plp.apply.text' /></span> <span class="filter-count"><c:if
												test="${count gt 0 }">(${count })</c:if></span>
									</a>
								</div>
							</div>
					</div>
				</c:if>
					
					<c:if test="${facets.code eq 'baseProductRetailerName'}">

						<div class="panel multiselect-filter"
							data-facet="baseProductRetailerName">
							<c:set var="clearFacetHide" value="hide"/>
							<div class="panel-heading">
							<c:forEach items="${facets.values}" var="facetValue">
								<c:if test="${facetValue.selected}">
									<c:set var="clearFacetHide" value=""/>
								</c:if>
							</c:forEach>
								<a class="panel-title collapsed" href="#collapseStore"
									data-toggle="collapse" data-parent="#accordion"><spring:theme
										code='plp.filters.store' />&nbsp;<span class="filter-count"><c:if test="${baseProductRetailerNameCount gt 0}">(${baseProductRetailerNameCount})</c:if></span></a><a class="clear-facets ${clearFacetHide}" href="javascript:void(0);"><spring:theme
											code='plp.clear.text' /></a>
							</div>
							<div class="panel-collapse collapse" id="collapseStore">
								<div class="panel-body">
									


									<div class="designers-listing-container">
										<label for="search-filter-designer" class="hide"><spring:theme code='plp.filters.searchbyname.text' /></label>
										<input type="text" id="search-filter-designer" name="search-filter" title="" class="form-control"
											 autocomplete="off" placeholder="<spring:theme
											code='plp.filters.searchbyname.text' />">

										<div class="designers-listing wrapper">
											<div class="scroll-container">
												<h6>
													<spring:theme code='plp.filters.store.mystores' />
												</h6>

												<c:choose>
													<c:when test="${isLoggedIn}">
														<c:set var="haveFavoriteStores" value="false" />
														<ul id="searchList" class="searchList">
															<c:set var="count" value="0" />
															<c:forEach items="${facets.values}" var="facetValue" varStatus="counter">
																<c:set var="selected" value="" />
																<c:if test="${facetValue.selected}">
																	<c:set var="selected" value="active" />
																	<c:set var="count" value="${count+1 }" />
																</c:if>
																<c:url value="${facetValue.query.url}"
																	var="facetValueQueryUrl" />
																<c:if test="${facetValue.favoriteStore}">
																	<c:set var="haveFavoriteStores" value="true" />

																	<li><label for="checkbox-fav-store-${counter.index}" data-href="${facetValueQueryUrl}"
																		data-code="${fn:escapeXml(facetValue.name)}"
																		class="auto-size multiselect-item ${selected} custom-checkbox"><input id="checkbox-fav-store-${counter.index}"
																			type="checkbox" name="checkedRows" value="" <c:if test="${facetValue.selected}">
																	 checked="true"
																	 </c:if>>
																			<em class="i-checkbox"></em><span>${fn:escapeXml(facetValue.name)}</span></label>
																	</li>

																</c:if>
															</c:forEach>
														</ul>
														
														<c:if test="${ not haveFavoriteStores}">
															<p><spring:theme code='plp.filters.store.nostore.text' />
															 <a href="#" class="add-deals hidden-xs analytics-genericLink" data-analytics-eventsubtype="how-to-add-favourite" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|how-to-add-favourite" data-analytics-linkname="how-to-add-favourite"
																	data-toggle="modal" data-target="#moadl1Content"
																	data-key="my-stores"> <spring:theme
																	code='plp.filters.howtofavorite' /></a>
															</p>
														</c:if>
													</c:when>
													<c:otherwise>
														<p>
														<a href="javascript:void(0);"
															class="login-modal openLoginModal" data-toggle="modal"
															data-target="#globalLoginModal"><spring:theme code='plp.filters.login' /></a>
															
															&nbsp;
															<spring:theme code='plp.filters.store.login.text' />
														</p>
													</c:otherwise>
												</c:choose>
												
												<h6>
													<a href="javascript:void(0)" class="checkAll selectAll">
														<spring:theme code='plp.filters.all.text' />
													</a>
												</h6>
												<ul id="searchList" class="searchList">
													<c:forEach items="${facets.values}" var="facetValue" varStatus="counterSel">
														<c:set var="selected" value="" />
														<c:if test="${facetValue.selected}">
															<c:set var="selected" value="active" />
															
														</c:if>
														<c:url value="${facetValue.query.url}"
															var="facetValueQueryUrl" />										
															<c:if test="${ not facetValue.favoriteStore}">
																<li><label for="checkbox-custom-${counterSel.index}" data-href="${facetValueQueryUrl}"
																	data-code="${fn:escapeXml(facetValue.name)}"
																	class="auto-size multiselect-item ${selected} custom-checkbox"><input id="checkbox-custom-${counterSel.index}"
																	type="checkbox" name="checkedRows" value=""  <c:if test="${facetValue.selected}">
																	 checked="true"
																	 </c:if>> <em
																	class="i-checkbox"></em><span>${fn:escapeXml(facetValue.name)}</span></label>
																</li>
															</c:if>
													</c:forEach>
												</ul>
											</div>
										</div>
									</div>
									<a class="btn black apply-btn analytics-filterApplyBtn" href="#" data-href=""> <span><spring:theme
												code='plp.apply.text' /> </span> <span class="filter-count"></span>
									</a>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${facets.code eq 'baseProductDesignerName'}">
						<div class="panel multiselect-filter"
							data-facet="baseProductDesignerName">
							<c:set var="clearFacetHide" value="hide"/>
							<div class="panel-heading">
														<c:forEach items="${facets.values}" var="facetValue">
								<c:if test="${facetValue.selected}">
									<c:set var="clearFacetHide" value=""/>
								</c:if>
							</c:forEach>
								<a class="panel-title collapsed" href="#collapseDesigner"
									data-toggle="collapse" data-parent="#accordion"><spring:theme
										code='plp.filters.designer' />&nbsp;<span
									class="filter-count"><c:if test="${baseProductDesignerNameCount gt 0}">(${baseProductDesignerNameCount})</c:if></span></a><a class="clear-facets ${clearFacetHide}" href="javascript:void(0);"><spring:theme
											code='plp.clear.text' /></a>
							</div>
							<div class="panel-collapse collapse" id="collapseDesigner">
								<div class="panel-body">
									<div class="designers-listing-container">
									<label for="search-filter-designer-coll" class="hide"><spring:theme code='plp.filters.searchbyname.text' /></label>
										<input type="text" id="search-filter-designer-coll" name="search-filter" title="" class="form-control"
											 autocomplete="off" placeholder="<spring:theme	code='plp.filters.searchbyname.text' />">

										<div class="designers-listing wrapper">
											<div class="scroll-container">
												<h6>
													<spring:theme code='plp.filters.designer.mydesigners' />
												</h6>

												<c:choose>
													<c:when test="${isLoggedIn}">
														<c:set var="haveFavoriteStores" value="false" />
														
														<ul id="searchList" class="searchList">
														<c:set var="count" value="0" />
															<c:forEach items="${facets.values}" var="facetValue" varStatus="counterDesign">
																<c:set var="selected" value="" />
																<c:if test="${facetValue.selected}">
																	<c:set var="selected" value="active" />
																	<c:set var="count" value="${count+1 }" />
																</c:if>
																<c:url value="${facetValue.query.url}"
																	var="facetValueQueryUrl" />
																<c:if test="${facetValue.favoriteDesigner}">
																
																	<c:set var="haveFavoriteStores" value="true" />
																	<li><label for="checkbox-design-${counterDesign.index}" data-href="${facetValueQueryUrl}"
																		data-code="${fn:escapeXml(facetValue.name)}"
																		class="auto-size multiselect-item ${selected} custom-checkbox"><input id="checkbox-design-${counterDesign.index}"
																			type="checkbox" name="checkedRows" value=""  <c:if test="${facetValue.selected}">
																	 checked="true"
																	 </c:if>>
																			<em class="i-checkbox"></em><span>${fn:escapeXml(facetValue.name)}</span></label>
																	</li>
																</c:if>
															</c:forEach>
														</ul>
														<c:if test="${ not haveFavoriteStores}">
															<p>
																<spring:theme
																	code='plp.filters.designer.nodesigner.text' />

																<a href="#" class="add-deals hidden-xs analytics-genericLink" data-analytics-eventsubtype="how-to-add-favourite" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|how-to-add-favourite" data-analytics-linkname="how-to-add-favourite"
																	data-toggle="modal" data-target="#moadl1Content"
																	data-key="my-designers"> <spring:theme
																		code='plp.filters.howtofavorite' /></a>
															</p>


														</c:if>
													</c:when>
													<c:otherwise>
														<p>
															<a href="javascript:void(0);"
															class="login-modal openLoginModal" data-toggle="modal"
															data-target="#globalLoginModal"><spring:theme code='plp.filters.login' /></a>
															&nbsp;
															<spring:theme code='plp.filters.designer.login.text' />
														</p>
													</c:otherwise>
												</c:choose>
												<h6>
													<a href="javascript:void(0)" class="checkAll"><spring:theme code='plp.filters.all.text' /></a>
												</h6>
												<ul id="searchList" class="searchList">
													<c:forEach items="${facets.values}" var="facetValue" varStatus="countdesign">
														<c:set var="selected" value="" />
														<c:if test="${facetValue.selected}">
															<c:set var="selected" value="active" />
														</c:if>
														<c:url value="${facetValue.query.url}"
															var="facetValueQueryUrl" />
														<c:if test="${ not facetValue.favoriteDesigner}">
														
															<li><label for="checkbox-sel-design-${countdesign.index}" data-href="${facetValueQueryUrl}"
																data-code="${fn:escapeXml(facetValue.name)}"
																class="auto-size multiselect-item ${selected} custom-checkbox"><input id="checkbox-sel-design-${countdesign.index}"
																	type="checkbox" name="checkedRows" value=""										
																	<c:if test="${facetValue.selected}">
																	 checked="true"
																	 </c:if>> <em
																	class="i-checkbox"></em><span>${fn:escapeXml(facetValue.name)}</span></label>
															</li>
														</c:if>
													</c:forEach>
												</ul>
											</div>
										</div>
									</div>
									<a class="btn black apply-btn analytics-filterApplyBtn" href="#" data-href=""> <span><spring:theme
												code='plp.apply.text' /> </span> <span class="filter-count"></span>
									</a>
								</div>
							</div>
						</div>
					</c:if>
				</c:forEach>
			</div>
		</li>
	</ul>
	<!--  START:- my favorites component  -->
	<div class="myfavorites-container">
	
		<cms:pageSlot position="csn_howtoFavoritesDetailContentSlot"
			var="feature">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
	<!--  END  my favorites component  -->
</div>
