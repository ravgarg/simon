		<div class="custom-component">
		<div class="row">
			<div class="columns col-xs-12 col-md-4">
				
				<div class="img-container">
					<a href="#">
						<!--image width should not be less than 480px-->
						<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/1LoremIpsum@2x.jpg" alt="">
						<h4 class="major">Lorem Ipsum <span>&rsaquo;</span></h4>
					</a>
				</div>
			</div>

			<div class="columns col-xs-12 col-md-4">
				<div class="img-container">
					<a href="#">
						<!--image width should not be less than 480px-->
						<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/2LoremIpsum@2x.jpg" alt="">
						<h4 class="major">Lorem Ipsum <span>&rsaquo;</span></h4>
					</a>
				</div>
			</div>


			<div class="columns col-xs-12 col-md-4">
				<div class="img-container">
					<a href="#">
						<!--image width should not be less than 480px-->
						<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/3LoremIpsum@2x.jpg" alt="">
						<h4 class="major">Lorem Ipsum <span>&rsaquo;</span></h4>
					</a>
				</div>
			</div>

			</div>

		</div>
