<div class="cart-order-summary">
	<div class="bag-summary hide-mobile">
	Order Summary
		<a href="#">
			<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
		</a>
		<span class="view-bag">View My Bag (5)</span>
	</div>

	<div class="view-bag-summary hide-desktop pad-r-mobile pad-l-mobile">
		View Bag Summary
	</div>
	    
	<div class="order-summary-price-container pad-r-mobile pad-l-mobile collapse">
	    <div class="price-top-spacing">
	        <span class="estimated-subtotal">Estimated Order Subtotal</span>
	        <span class="estimated-sub-price">$360.00</span>
	    </div>
	    <div class="ship-top-spacing">
	        <span class="estimated-shipping">Combined Shipping Charges</span>
	        <span class="estimated-shipping-price">--</span>
	    </div>

	    <div class="tax-top-spacing">
	        <span class="estimated-tax">Estimated Taxes
		        <a href="#">
					<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
				</a>
			</span>
	        <span class="estimated-tax-price">--</span>
	    </div>
	    
	    <div class="last-call-spacing">
	        <span class="last-call">Estimated Order Total</span>
	        <span class="last-call-price">$360.00</span>
	    </div>
	    <div class="you-save-spacing">
	        <span class="you-save">You Saved</span>
	        <span class="you-save-price">$80.00</span>
	    </div>
	</div>
	

	<jsp:include page="checkout-right-order-confirmation-summary.jsp"></jsp:include>



</div>