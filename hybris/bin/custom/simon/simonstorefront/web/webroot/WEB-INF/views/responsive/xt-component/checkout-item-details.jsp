<div class="cart-checkout-items-order-details">
   <div class="cart-checkout-items">
      <div class="confirm-retailer major">Last Call (2)</div>
      <div class="cart-items-container">
         <div class="return-information"><a href="#">View Shipping, Returns & Privacy Information</a></div>
         <div class="checkout-item-heading">
            <div class="row">
               <div class="col-md-2 col-xs-2">
                  <div class="item-heading">Items</div>
               </div>
               <div class="col-md-offset-6 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
               <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-2">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
               </a>
            </div>
            <div class="col-md-6 col-xs-8 row-pad-l">
               <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
               <div class="hide-desktop">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                      <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                  </div>
                  <div class="item-discount">20% Off</div>
               </div>
               <div class="item-qty">Quantity: 1</div>
               <div class="item-color">Color: Red</div>
            </div>
            <div class="col-md-2 col-xs-2">
              <div class="hide-mobile">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                   <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                </div>
                  <div class="item-discount">20% Off</div>
               </div>
            </div>
            <div class="col-md-2 col-xs-2">
                <div class="subtotal">$200.00</div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-2">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
               </a>
            </div>
            <div class="col-md-6 col-xs-8 row-pad-l">
               <div class="item-description"><a href="#">Asymmetric-Peplum Midi Dress</a></div>
               <div class="hide-desktop">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                  <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                  </div>
                  <div class="item-discount">20% Off</div>
               </div>
               <div class="item-qty">Quantity: 1</div>
               <div class="item-color">Color: Red</div>
            </div>
            <div class="col-md-2 col-xs-2">
              <div class="hide-mobile">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                   <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                </div>
                  <div class="item-discount">20% Off</div>
               </div>
            </div>
            <div class="col-md-2 col-xs-2">
                <div class="subtotal">$200.00</div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-container-left">
                  <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$360.00</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Premium Shipping</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Tax</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$360.00</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$80.00</span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="cart-checkout-items">
      <div class="confirm-retailer major">Gap Factory (1)</div>
      <div class="cart-items-container">
         <div class="return-information"><a href="#">View Shipping, Returns & Privacy Information</a></div>
         <div class="checkout-item-heading">
            <div class="row">
               <div class="col-md-2 col-xs-2">
                  <div class="item-heading">Items</div>
               </div>
               <div class="col-md-offset-6 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
               <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-2">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-3.jpg">
               </a>
            </div>
            <div class="col-md-6 col-xs-8 row-pad-l">
               <div class="item-description"><a href="#">Print Fringe Scarf</a></div>
               <div class="hide-desktop">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                  <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                  </div>
                  <div class="item-discount">20% Off</div>
               </div>
               <div class="item-qty">Quantity: 1</div>
               <div class="item-color">Color: Red</div>
            </div>
            <div class="col-md-2 col-xs-2">
              <div class="hide-mobile">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                   <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                </div>
                  <div class="item-discount">20% Off</div>
               </div>
            </div>
            <div class="col-md-2 col-xs-2">
                <div class="subtotal">$200.00</div>
            </div>
         </div>
          <div class="row">
            <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-container-left">
                  <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$360.00</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Premium Shipping</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Tax</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$360.00</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$80.00</span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="cart-checkout-items">
      <div class="confirm-retailer major">Nordstrom Rack (2)</div>
      <div class="cart-items-container">
         <div class="return-information"><a href="#">View Shipping, Returns & Privacy Information</a></div>
         <div class="checkout-item-heading">
            <div class="row">
               <div class="col-md-2 col-xs-2">
                  <div class="item-heading">Items</div>
               </div>
               <div class="col-md-offset-6 col-md-2 col-xs-offset-6 col-xs-2"><div class="price-heading hide-mobile">Item Price</div></div>
               <div class="col-md-2 col-xs-2"><div class="total-heading">Subtotal</div></div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-2">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-4.jpg">
               </a>
            </div>
            <div class="col-md-6 col-xs-8 row-pad-l">
               <div class="item-description"><a href="#">Sterling Silver Diamond & Bar Charm Double Drop Necklace - 0.08 ctw</a></div>
               <div class="hide-desktop">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                  <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                  </div>
                  <div class="item-discount">20% Off</div>
               </div>
               <div class="item-qty">Quantity: 1</div>
               <div class="item-color">Color: Red</div>
            </div>
            <div class="col-md-2 col-xs-2">
              <div class="hide-mobile">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                   <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                </div>
                  <div class="item-discount">20% Off</div>
               </div>
            </div>
            <div class="col-md-2 col-xs-2">
                <div class="subtotal">$200.00</div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-2">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-5.jpg">
               </a>
            </div>
            <div class="col-md-6 col-xs-8 row-pad-l">
               <div class="item-description"><a href="#">Women's Retro Composite Frame Sunglasses</a></div>
               <div class="hide-desktop">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                  <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                  </div>
                  <div class="item-discount">20% Off</div>
               </div>
               <div class="item-qty">Quantity: 1</div>
               <div class="item-color">Color: Red</div>
            </div>
            <div class="col-md-2 col-xs-2">
              <div class="hide-mobile">
                  <div class="item-price">$200.00</div>
                  <div class="item-revised-price">$160.00
                   <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                </div>
                  <div class="item-discount">20% Off</div>
               </div>
            </div>
            <div class="col-md-2 col-xs-2">
                <div class="subtotal">$200.00</div>
            </div>
         </div>
          <div class="row">
            <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-container-left">
                  <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$360.00</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Premium Shipping</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Tax</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$360.00</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$80.00</span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="order-summary-bottom">
         <jsp:include page="checkout-confirmation-order-summary.jsp"></jsp:include>
   </div>
</div>