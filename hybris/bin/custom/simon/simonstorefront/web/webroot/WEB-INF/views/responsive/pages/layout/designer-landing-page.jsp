<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
	<div class="container designer-landing-page">
		<div class="col-md-12 breadcrumbs hide-mobile">
			<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a> <span
				class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
		</div>
	</div>
	<div class="owl-carousel" id="homepageCarousel">
		<cms:pageSlot position="csn_HeroSlotForDesigner" var="feature">
			<div class="img-container">
				<cms:component component="${feature}" />
			</div>
		</cms:pageSlot>
	</div>
	<div class="container">
		<div class="row">
				<cms:pageSlot position="csn_leftNavigationForDesigner" var="feature"
					element="div" class="account-section-content">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				
					<div class="btn-category hide-desktop col-md-9">
						<button class="btn secondary-btn black">
							<spring:theme code="text.clp.browse.categories" />
						</button>
					</div>
			<div class="col-md-9 analytics-rightContainer" data-analytics-pagelisttype="${pageType}" data-analytics-pagelistname="${categoryName}">
				<div class="designer-spacing">
					<cms:pageSlot position="csn_Promo1SlotForDesigner" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="custom-heading-links-component category-list">
					<h3 class="major heading-border">
						<spring:theme code="text.clp.trending.now" />
					</h3>

					<div class="clp-des-links">
						<cms:pageSlot position="csn_TrendingNowLinksSlot" var="feature">
							<cms:component component="${feature}" />
							<span>&rsaquo;</span>
						</cms:pageSlot>
					</div>

				</div>
				<div class="custom-component">
					<div class="row">
						<div class="columns col-xs-12 col-md-6 designer-edit">
							<cms:pageSlot position="csn_ContentSpotsASlotForDesigner"
								var="feature" element="div">
								<div class="img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>

						<div class="columns col-xs-12 col-md-6">
							<cms:pageSlot position="csn_ContentSpotsBSlotForDesigner"
								var="feature" element="div">
								<div class="img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>

						<div class="columns col-xs-12 col-md-6">
							<cms:pageSlot position="csn_ContentSpotsCSlotForDesigner"
								var="feature" element="div">
								<div class="img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<div class="style-deals common-heading">
					<div class="row">
						<cms:pageSlot position="csn_StyleDealsSlotForDesigner"
							var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
				</div>
				<cms:pageSlot position="csn_Promo2SlotForDesigner" var="feature"
					element="div" class="span-24 section5 cms_disp-img_slot">
					<cms:component component="${feature}" />
				</cms:pageSlot>

			</div>
		</div>
	</div>
	<div class="social-shop">
		<div class="container">
			<div class="row">
				<div class="social-shop-containers  col-xs-12 col-md-8">
					<div class="row">
						<div id="carousalFooter">
							<cms:pageSlot position="csn_SocialShopSlotForDesigner"
								var="feature">
								<div class="social-shop-content item col-xs-12 col-md-4">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<cms:pageSlot position="csn_SocialShopTextSlotForDesigner"
					var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>
	</div>
	<div>
		<cms:pageSlot position="csn_TextContentSlot" var="feature"
			element="div" class="span-24 section5 cms_disp-img_slot">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>
	<!--  favorites component model start -->
		<div class="myfavorites-container">
			<cms:pageSlot position="csn_howtoFavoritesDetailContentSlot" var="feature">
					<cms:component component="${feature}" />
			</cms:pageSlot>			
		</div>
<!-- favorites component model  END  -->
</template:page>




