<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" id="storeList" value="${allStoreslist}" />

	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />

	<c:set var="buttonText" value="" scope="page" />
	<c:choose>
		<c:when test="${not empty storeList}">
		<div class="current-trends analytics-favoriteStores" data-analytics-length="stores">
			<div class="row">
				<c:forEach items="${storeList}" var="shop">
					<c:choose>
						<c:when test="${not empty shop.backgroundImage}">
							<c:set var="backgroundImage" value="${shop.backgroundImage}"/>
						</c:when>
						<c:otherwise>
							<c:set var="backgroundImage" value="/_ui/responsive/simon-theme/images/store-no-image-desktop.png"/>
						</c:otherwise> 
					</c:choose>
					<c:choose>
						<c:when test="${not empty shop.mobileBackgroundImage}">
							<c:set var="mobileBackgroundImage" value="${shop.mobileBackgroundImage}"/>
						</c:when>
						<c:otherwise>
							<c:set var="mobileBackgroundImage" value="/_ui/responsive/simon-theme/images/store-no-image-mobile.png"/>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty shop.logo}">
							<c:set var="logo" value="${shop.logo}"/>
						</c:when>
						<c:otherwise>
							<c:set var="logo" value=""/>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${not empty shop.mobileLogo}">
							<c:set var="mobileLogo" value="${shop.mobileLogo}"/>
						</c:when>
						<c:otherwise>
							<c:set var="mobileLogo" value=""/>
						</c:otherwise>
					</c:choose>
					
					<div class="store col-md-4 col-xs-12">
						<div class="store-content">
							<div class="box-background">
							<picture>
									 <source media="(max-width: 767px)" srcset="${mobileBackgroundImage}">
									  <source media="(min-width: 768px) and (max-width: 991px)" srcset="${backgroundImage}">
									  <img class="img-responsive" alt="${shop.altTextBGImage}" style="" src="${backgroundImage}" >
								</picture>
							</div>
							
							<div class="fav-the-store add-to-favorites analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="|stores"  data-analytics-linkplacement="|stores" data-analytics-component="stores">
								<input type="hidden" value="${shop.id}" class="base-code analytics-base-code" /> 
								<input type="hidden" value="${shop.name}" class="base-name analytics-base-name" /> 
								<a href="javascript:void(0);" class="mark-favorite fav-icon marked"><span class="hide"> fav</span></a>
							</div>
							
							<div class="center-content">
							
								<picture>
								
									 <source media="(max-width: 767px)" srcset="${logo}">
									  <source media="(min-width: 768px) and (max-width: 991px)" srcset="${mobileLogo}">
									  <img class="img-responsive" alt="${shop.altTextLogo}" style="" src="${logo}" >
								</picture>

							</div>
							<div class="shop-now">
								<a class="analytics-genericLink analytics-storeClick" data-storeval="${shop.name}" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="shop-now" data-analytics-linkname="shop-now" href="${shop.contentPageLabel}"><spring:theme code="text.account.myStores.shopnow.text" /></a>
							</div>
						</div>
					</div>

				</c:forEach>
			</div>
		
			<div class="btn-category">
				<button class="btn black btn-width major callStoreList analytics-genericLink" data-analytics-eventsubtype="add-store" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-store" data-analytics-linkname="add-store"
					data-toggle="modal" data-target="#addStoresModal"
					data-url="/my-account/get-stores">
					<spring:theme code="${addMoreStores}" />
				</button>
			</div>
		</div>
		</c:when>
		<c:otherwise>
		<div class="myarea-container analytics-favoriteStores" data-analytics-length="nostores">
			<div class="not-saved-text">
				<h4>${userName},
					<spring:theme code="${noSavedDataKey}" />
				</h4>
				<div class="subheading-text">
					<spring:theme code="${noSavedStoreData}" />
				</div>
			</div>
			<div class="btn-category">
				<button class="btn black btn-width major callStoreList  analytics-genericLink" data-analytics-eventsubtype="add-store" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-store" data-analytics-linkname="add-store"
					data-toggle="modal" data-target="#addStoresModal"
					data-url="/my-account/get-stores">
					<spring:theme code="${addStores}" />
				</button>
			</div>
		</div>
		</c:otherwise>
	</c:choose>

<div class="modal fade adddesigners-modal" id="addStoresModal"
	role="dialog">
	<div class="modal-dialog">
		<form:form  commandName="addStoreForm"
			class="sm-form-validation designers-search analytics-myPageFormSubmit" data-analytics-formtype="add_stores" data-analytics-formname="add_stores">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<spring:theme code="text.account.myStores.addStores.title" />
					</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close"></button>
				</div>
				<div class="modal-body clearfix">
					<div class="designers-listing-container">	
					<label for="storesdetailssearch" class="hide"><spring:theme code="text.account.myStores.stores.search" /></label>					
						<input type="text" name="search-filter" title="" class="form-control"
							id="storesdetailssearch" autocomplete="off"
							placeholder="<spring:theme code="text.account.myStores.stores.search" />">
						<div class="designers-listing wrapper">
							<div class="scroll-container">
								<ul class="searchList" id="searchList">
								
								</ul>	
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer clearfix">
					<button class="btn addToFavStore btn-add pull-left btn-width disabled analytics-genericLink" data-analytics-eventsubtype="my-stores" data-analytics-linkplacement="my-stores" data-analytics-linkname="add store" disabled>
						<spring:theme code="text.account.myStores.addStores.button" />
					</button>
					<button class="btn secondary-btn black pull-left btn-width"
						data-dismiss="modal">
						<spring:theme code="basket.save.cart.action.cancel" />
					</button>
				</div>
			</div>
		</form:form>
	</div>
</div>
