<div class="plp-filters trends-listing-filters hide-mobile">
    <div class="row filters">
        <div class="col-xs-12 product-count">Displaying 203 Items</div>   
        
        <jsp:include page="plp-filters-common.jsp"></jsp:include>

        <div class="col-md-4 clear-all">
            <a 
                href="javascript:void(0);" 
                id="clearAllFilter">Clear All</a>
        </div>
        <div class="col-md-3 select-box dropdown custom-dropdown">
           <button 
                class="dropdown-toggle" 
                type="button" 
                data-toggle="dropdown">Sort by
                <span class="caret-icon"></span>                
            </button>
            <ul class="dropdown-menu">
                <li><a href="#">Price up</a></li>
                <li><a href="#">Price down</a></li>
                <li><a href="#">Relevance</a></li>
            </ul>
        </div>
    </div>
</div>



<div class="plp-filters show-mobile">
    <div class="browse-categories">
            <button 
                id="mBrowseCategories"
                class="btn block black secondary-btn" 
                type="button">Browse Categories
            </button>
        </div>
    <div class="row filters">        
        <div class="col-xs-6">
            <button 
                id="mFilter"
                class="btn block black secondary-btn" 
                type="button"  data-toggle="modal" data-dismiss="modal" data-target="#plpFiltersModal">Filter (8)
            </button>
        </div>
        <div class="col-xs-6">
            <button 
                id="mShortBy" 
                class="btn block black secondary-btn" 
                type="button">Sort by  
            </button>
        </div>
        <div class="col-xs-12 product-count">Displaying 203 Items</div>
    </div>    
</div>
<jsp:include page="plp-filters-modal.jsp"></jsp:include>