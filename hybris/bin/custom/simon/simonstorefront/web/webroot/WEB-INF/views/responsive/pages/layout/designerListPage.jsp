
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<template:page pageTitle="${pageTitle}">
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}"
	id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}"
	id="totalNumberOfResults" />
	<input type="hidden" data-listing-url="true"
		value='{"add":"/my-account/add-designer", "remove":"/my-account/remove-designer" }' />
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<div class="row">
					<div class="breadcrumbs">
						<c:forEach items="${breadcrumbs}" var="breadcrumbs" varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}"><c:if test="${!status.first}"><span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}</a>
						</c:forEach>
					</div>
					</div>
				</div>	
			
			<cms:pageSlot position="csn_designer_productLeftRefinements"
				var="feature" element="div" class="account-section-content">
				<cms:component component="${feature}" />
			</cms:pageSlot>
			<!-- breadcurmb Ends -->
			<div class="col-md-9">
					<c:if test="${not empty designer.code}">
					<div class="add-to-favorites storefront-heading pageHeading">
						<input type="hidden" value="${designer.code}" id="listingId" class="base-product-code base-code analytics-base-code" name="${designer.code}" /> 
							<input type="hidden" value="${designer.name}" class="base-product-name base-name analytics-base-name" />
							
						
						<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
							<h1 class="page-heading major">${designer.name}</h1>
							<a href="javascript:void(0);" data-fav-product-code="${designer.code}" data-fav-name="${designer.name}" data-fav-type="DESIGNERS"
							class="listfav favor_icons <c:if test="${isFavDesigner}">marked</c:if> login-modal openLoginModal analytics-loginFlyOut" data-analytics='{"event":{"sub_type":"login_initiation","type":"login"},"login": {"stage": "login_start"}}' data-satellitetrack="login"></a>
						</sec:authorize>
					
						<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
						<div class="analytics-addtoFavorite" data-analytics-pagestyle="plp" data-analytics-pagetype="myaccountfavorite" data-analytics-eventsubtype="designerlisting|product" data-analytics-linkplacement="designerlisting|product" data-analytics-component="designer">
							<input type="hidden" value="DESIGNERS" class="base-type" /> 
							<a href="javascript:void(0);" title="" class="listfav icon-fav-blank mark-favorite fav-icon <c:if test='${isFavDesigner}'>marked</c:if> "></a>
						</div>
							<h1 class="page-heading major" title="">${designer.name}</h1>
						</sec:authorize>
						
						
					</div>
					</c:if>
					<div class="show-more summer-txt">
						${designer.description}
					</div>
					<c:forEach items="${seoLinks}" var="seoLink" varStatus="status">
							<a href="<c:choose><c:when test="${not empty seoLink}">/${ycommerce:getUrlForCMSLinkComponent(seoLink)}</c:when><c:otherwise>'#'</c:otherwise></c:choose>" title="${seoLink.linkName}">${seoLink.linkName}</a>
							<c:if test="${!status.last}"><span>&rsaquo;</span></c:if>
						</c:forEach>
					<div class="designer-listing-banner">
						<div class="the-designer-shop left active">
							<c:if test="${not empty heroBanner}">
								<jsp:include page="../../cms/designerherobanner.jsp"></jsp:include>
							</c:if>
						</div>
					</div>
	
						<!-- Filters Starts -->
						<product:productFilters searchPageData="${searchPageData}" />
						<!-- Filters END -->
						<!-- Product Tiles Starts -->
						<product:productTilesListing searchPageData="${searchPageData}" />
						<!-- Product Tiles END -->
			
						<c:if test="${fn:length(searchPageData.results)<=0}">
					    <div><spring:theme code="search.no.results" /></div>
					    </c:if>
					    <div class="row load-more-btn-wrapper">
							<button id="loadMore" class="btn btn-primary load-more block"><spring:theme code='plp.loadmore.text' /></button>
					    </div>
				</div>
			</div>
		</div>
	</div>
</template:page>