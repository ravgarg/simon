<div class="custom-heading-links-component">
	<div class="btn-category hide-desktop">
		<button class="btn secondary-btn black">Browse Categories</button>
	</div>
	<h3 class="major heading-border"> Trending Now </h3>

	<div class="row">
		<div class="col-md-4 custom-links">
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a><br>
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a>
		</div>
		<div class="col-md-4 custom-links">
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a><br>
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a>
		</div>
		<div class="col-md-4 custom-links">
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a><br>
			<a href="#">Lorem ipsum dolor sit amet <span>&rsaquo;</span></a>
		</div>
	</div>	
</div>
