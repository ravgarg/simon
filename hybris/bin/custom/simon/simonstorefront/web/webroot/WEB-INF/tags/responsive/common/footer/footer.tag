<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="footer" 	tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<footer>
<div class="analytics-footer">
        <div class="join-vip-club">
           <cms:pageSlot position="csn_JoinVipClubSlot" var="feature">
            <cms:component component="${feature}"/>
          </cms:pageSlot>
          </div>
    <div class="container">
          <div class="row">
           <div class="col-xs-12 visible-xs visible-sm footer-email-signup" >
	            <cms:pageSlot position="csn_FooterNewsLetterSlot" var="feature">
	              <cms:component component="${feature}"/>
	            </cms:pageSlot>
	            <button class="btn secondary-btn black btn-width" data-toggle="modal" data-target="#emailModal">EMAIL SIGN UP</button>
           </div>
                
          </div>
          

                <div class="row footer-link-wrapper clearfix">
                      <div class="col-md-8 col-xs-12 footer-social">
                          <section>
                           <cms:pageSlot position="csn_SocialSlot" var="feature">
                  <cms:component component="${feature}"/>
                 </cms:pageSlot>
                          </section>
                      </div>
                      <div class="col-md-4 hidden-xs">
                          <div class="find-outlet pull-right">
                          <cms:pageSlot position="csn_FindAPremiumOutletSlot" var="feature">
                  <cms:component component="${feature}"/>
                </cms:pageSlot>
                              <!-- <a href="#" title="Find A Simon Shopping Center Near You">
                                  FIND A SIMON CENTER
                              </a> -->
                          </div>
                      </div>
                </div>

          


          <div class="footer-top-border">
                <div class="row xs-row clearfix">
                <!-- Commenting this code because we will use this code in the next beta-2 release and wokr on the story -->
                <%-- <div class="col-md-4 footer-emai-signup hidden-xs pull-right">
            <cms:pageSlot position="csn_FooterNewsLetterSlot" var="feature">
              <cms:component component="${feature}"/>
            </cms:pageSlot>
                       <!--  <p>Sign up with your email address to receive our newsletter for the latest trends, style inspiration and special offers.</p> -->
                        <button class="btn secondary-btn black analytics-newsletter" data-toggle="modal" data-target="#emailModal">EMAIL SIGN UP</button>
                    </div> --%>





                	<cms:pageSlot position="csn_GlobalFooterSlot" var="feature">
						<cms:component component="${feature}"/>
					</cms:pageSlot>
					

          
          



                </div>

                <div class="footer-terms">
                		 <cms:pageSlot position="csn_TermsOfUseSlot" var="feature">
						   <cms:component component="${feature}"/>
					    </cms:pageSlot>
                </div>
          </div>
    </div>



    <div class="footer-cookies hide" id="footerCookiesComponent">
       <div class="container">
        <div class="footer-cookies-about"><spring:theme code="text.footer.about.cookeis" /></div>
        <div class="row">
            <div class="col-md-10 footer-cookies-term">
            	 <cms:pageSlot position="csn_FooterCookiesBannerSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot> 
            </div>
            <div class="col-md-2">
                 <button class="btn secondary-btn white" id="acceptCookieBtn"><spring:theme code="text.footer.accept.button.name" /></button>
            </div>
        </div>
      </div>
    </div>
</div>
    <!-- Modal -->
    <div class="modal fade" id="emailModal" role="dialog">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><spring:theme code="text.footer.modal.header.shopPremium" /></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="analytics-formValidation" data-analytics-type="newsletter" data-analytics-eventtype="newsletter" data-analytics-eventsubtype="newsletter" data-analytics-formtype="newsletter" data-analytics-formname="newsletter_popup" data-satellitetrack="newsletter">
                        <div class="modal-body clearfix">
                            <div class="sign-up-content">
                             <cms:pageSlot position="SimonFooterNewsLetterContentSlot" var="feature">
						   		<cms:component component="${feature}"/>
					    	</cms:pageSlot>
                                <p class="heading-margin"><spring:theme code="text.footer.modal.headding.signup" /></p>
                                <div class="sm-form-control">

                                <div class="col-md-7">
                                  <div class="row">
                                    <div class="sm-input-group">
                                    <label for="footerEmail" class="hide"><spring:theme code="text.footer.modal.email.label" /></label>
                                      <input type="text" class="form-control" id="footerEmail" name="emailaddress" placeholder="<spring:theme code="text.footer.modal.email.label" />" title="<spring:theme code="text.footer.modal.email.label" />">
                                    </div>
                                    <div class="sm-input-group">
                                     <label for="footerConfirmEmail" class="hide"><spring:theme code="text.footer.modal.confirmemail.label" /></label>
                                      <input type="text" class="form-control" id="footerConfirmEmail" name="confirmemailaddress" placeholder="<spring:theme code="text.footer.modal.confirmemail.label" />" title="<spring:theme code="text.footer.modal.confirmemail.label" />">
                                    </div>
                                    </div>
                                  </div>
                                    <div class="clearfix pull-left disclaimer-copy">
                                      <label class="custom-checkbox" for="age_confirm">
                                      <span class="hide"><spring:theme code="text.footer.modal.confirmation"/></span>
                                        <input type="checkbox" name="confirm" class="sr-only" id="age_confirm"/>
                                        <em class="i-checkbox"></em>                    
                                      </label>
                                      <span><spring:theme code="text.footer.modal.confirm.msg" /></span>
                                        <p class="sign-up-policy"><spring:theme code="text.footer.modal.viewour" /> <a href="<spring:theme code="text.footer.modal.privacy.policy.href" />"><spring:theme code="text.footer.modal.privacy.policy" /></a>, <a href="<spring:theme code="text.footer.modal.termofuse.href" />"><spring:theme code="text.footer.modal.termofuse" /></a>, or <a href="<spring:theme code="text.footer.modal.contactus" />"><spring:theme code="text.footer.modal.contactus" /></a>. <spring:theme code="text.footer.modal.unsubscribe.msg" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer clearfix submit-btn">
                            <button class="btn secondary-btn black" data-dismiss="modal"><spring:theme code="text.footer.cancel.button.name" /></button>
                            <button class="btn"><spring:theme code="text.footer.submit.button.name" /></button>
                        </div>
                    </form>
                </div>
        </div>
    </div>

   <footer:profileAttributesDefaulted/>
</footer>