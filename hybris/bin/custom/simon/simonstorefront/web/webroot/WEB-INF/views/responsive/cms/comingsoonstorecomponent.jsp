<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="store col-md-4">
    <div class="box-background">
        <picture>
            <source media="(max-width: 991px)" srcset="${mobileShopBackgroundImage.url}"/>
            <img 
                class="img-responsive" 
                alt="${desktopShopBackgroundImage.altText}" 
                src="${desktopShopBackgroundImage.url}" />
        </picture>
    </div>
    
    <div class="center-content">
        <picture>
            <source media="(max-width: 991px)" srcset="${mobileLogoImage.url}"/>
            <img 
                class="img-responsive" 
                alt="${desktopLogoImage.altText}" 
                src="${desktopLogoImage.url}"/>
        </picture>
    </div>
    <div class="shop-now">
        <a class="analytics-genericLink analytics-storeClick" data-storeval="${fn:toLowerCase(component.uid)}" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="shop-now" data-analytics-linkname="shop-now" href="${shopNowLink.url}">${comingSoonText}</a>
    </div>
</div>
