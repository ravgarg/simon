<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true"
	type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<form action="${action}" method="post" modelAttribute="registerFormData" class="sm-form-validation bt-flabels js-flabels captchaValidation analytics-formValidation" data-analytics-type="registration" data-analytics-eventtype="registration" data-analytics-eventsubtype="registration" data-analytics-formtype="registration_form" data-analytics-formname="registration_form_page" data-satellitetrack="registration" data-parsley-validate="validate">
	<div class="panel-group" id="accordion">       
		<div class="panel panel-default">
			<h5 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel1" aria-expanded="true"><spring:theme code="register.new.customer.step1.create" /></a>
			</h5>
			<div id="panel1" class="panel-collapse collapse in" aria-expanded="true">
				<div class="collapsed-content">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<div class="instructions">
								<p><spring:theme code="register.new.customer.pwd.desc" /></p>
								<ul>
									<li><spring:theme code="register.new.customer.uppercase.char" /></li>
									<li><spring:theme code="register.new.customer.lowercase.char" /></li>
									<li><spring:theme code="register.new.customer.numeric.char" /></li>
									<li><spring:theme code="register.new.customer.alphanumeric.char" /></li>
								</ul>
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
								<div class="row">
								<div class="col-xs-12 col-md-12">
									<div class="sm-input-group">
										<label class="field-label" for="newCustomerEmail"><spring:theme code="register.new.customer.email.placeHolder" /> <span class="field-error" id="error-j_guestEmailId"></span></label>
										<input type="email" class="form-control" id="newCustomerEmail" placeholder="<spring:theme code="register.new.customer.email.placeHolder" />" name="email" data-parsley-errors-container="#error-j_guestEmailId" type="email" data-parsley-type="email" data-parsley-error-message="<spring:theme code="register.new.customer.email.error" />" required>
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="sm-input-group">
										<label class="field-label" for="user-pswd"><spring:theme code="register.new.customer.pwd.placeHolder" /> <span class="field-error" id="error-j_pswd"></span></label>
										<input 
											type="password" 
											autocomplete="off"
											class="form-control" 
											placeholder="<spring:theme code="register.new.customer.pwd.placeHolder" />" 
											name="pwd" 
											data-parsley-errors-container="#error-j_pswd" 
											id="user-pswd"
											data-parsley-required-message="Please enter a password" 
											data-parsley-required 
											data-parsley-pwdstrength-message="Please enter a valid password"
											data-parsley-pwdstrength />
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<div class="sm-input-group">
										<label class="field-label" for="cnfrm-pswd"><spring:theme code="register.new.customer.confirm.pwd.placeHolder" /> <span class="field-error" id="error-j_confirmPswd"></span></label>
										<input 
											type="password"
											autocomplete="off"
											class="form-control" 
											id="cnfrm-pswd" required
											placeholder="<spring:theme code="register.new.customer.confirm.pwd.placeHolder" />" 
											name="checkPwd" 
											data-parsley-errors-container="#error-j_confirmPswd" 
											data-parsley-required-message="<spring:theme code="register.new.customer.confirmpwd.error" />" 
											data-parsley-required
											data-parsley-equalto-message="Password & confirm password should match" 
											data-parsley-equalto="#user-pswd" />
									</div>
								</div>
								</div>	
								<div class="clearfix sm-input-group input-check">
									<label class="custom-checkbox" for="changePassCheckbox">
										<input type="checkbox" data-parsley-required="true" data-parsley-trigger="click" 
										class="sr-only" required id="changePassCheckbox"/>
										<em class="i-checkbox"></em><spring:theme code="register.new.customer.confirm.msg" />
										<a href="<spring:theme code="register.new.customer.confirm.privacy.policy.herf" />"> 
										<spring:theme code="register.new.customer.confirm.privacy.policy" /></a>, 
										<a href="<spring:theme code="register.new.customer.confirm.termofuse" />">
										<spring:theme code="register.new.customer.confirm.termofuse" /></a>, or 
										<a href="<spring:theme code="register.new.customer.confirm.contactus.herf" />">
										<spring:theme code="register.new.customer.confirm.contactus" /></a>.
									</label> 
								</div>						             
						</div>
						</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<h5 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel2" aria-expanded="true"><spring:theme code="register.new.customer.step2.tellUs" /></a>
			</h5>
			<div id="panel2" class="panel-collapse collapse in" aria-expanded="true">
				<div class="collapsed-content">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<div class="instructions">
								<p><spring:theme code="register.new.customer.instruction.tellmeabout" /></p>
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
								<div class="row">
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group">
											<label class="field-label" for="first-name"><spring:theme code="register.new.customer.firstname.placeHolder" /> <span class="field-error" id="error-firstName"></span></label>
											<input type="text" class="form-control reqiured" id="first-name" placeholder="<spring:theme code="register.new.customer.firstname.placeHolder" />" name="firstName" data-parsley-errors-container="#error-firstName" data-parsley-error-message="<spring:theme code="register.new.customer.firstName.error" />" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group">
											<label class="field-label" for="last-name"><spring:theme code="register.new.customer.lastname.placeHolder" /> <span class="field-error" id="error-lastName"></span></label>
											<input type="text" class="form-control" id="last-name" placeholder="<spring:theme code="register.new.customer.lastname.placeHolder" />" name="lastName" data-parsley-errors-container="#error-lastName" data-parsley-error-message="<spring:theme code="register.new.customer.lastname.error" />" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6 zipcode">
										<div class="sm-input-group">
											<label class="field-label" for="txtZip"><spring:theme code="register.new.customer.zipcode.placeHolder" /> <span class="field-error" id="error-zip"></span></label>
											<input type="text" id="txtZip" class="form-control" placeholder="<spring:theme code="register.new.customer.zipcode.placeHolder" />" name="zip" data-parsley-errors-container="#error-zip" data-parsley-error-message="<spring:theme code="register.new.customer.zip.error" />"  data-parsley-required="" data-parsley-pattern="^(?=.*[1-9].*)[0-9]{5}?$" required>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu">
											<label class="field-label" for="countries"><spring:theme code="register.new.customer.country" /> <span class="field-error" id="error-country"></span></label>
											<select class="form-control" name="country" id="countries" data-parsley-errors-container="#error-country" data-parsley-error-message="<spring:theme code="register.new.customer.country.error" />" required>
												<c:forEach items="${countryList}" var="country">
												<option value="${country.isocode}">${country.name}</option>
												</c:forEach> 
											
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu">
											<label class="field-label" for="birthmonth"><spring:theme code="register.new.customer.birthMonth" /> <span class="field-error" id="error-birthmonth"></span></label>
											<select class="form-control" name="birthmonth" id="birthmonth" data-parsley-errors-container="#error-birthmonth" data-parsley-error-message="<spring:theme code="register.new.customer.birthmonth.error" />" required>
											<option value=""><spring:theme code="register.new.customer.birthMonth" /></option>
											<c:forEach items="${birthMonthList}" var="birthmonth">
											<option value="${birthmonth.code}">${birthmonth.name}</option>
											</c:forEach>
											
											
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu">
											<label class="field-label" for="birthyear"><spring:theme code="register.new.customer.birthYear" /> <span class="field-error" id="error-birthyear"></span></label>
											<select class="form-control" name="birthyear" id="birthyear" data-parsley-errors-container="#error-birthyear" data-parsley-error-message="<spring:theme code="register.new.customer.birthyear.error" />" required>
											<option value=""><spring:theme code="register.new.customer.birthYear" /></option>
												<c:forEach var="year" begin="${startYear}" end="${endYear}" step="1">
												<option value="${year}"><c:out value="${year}" /></option>
											</c:forEach> 
												
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-md-6">
										<div class="sm-input-group select-menu">
											<label class="field-label" for="gender"><spring:theme code="register.new.customer.gender" /> <span class="field-error" id="error-j_gender"></span></label>
											<select class="form-control" name="gender" id="gender" data-parsley-errors-container="#error-j_gender" data-parsley-error-message="<spring:theme code="register.new.customer.gender.error" />" required>
											
												<c:forEach items="${genderList}" var="gender">
												<option value="${gender.code}">${gender.name}</option>
												</c:forEach> 
											
											</select>
										</div>
									</div>
								</div>						             
						</div>
						</div>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<h5 class="panel-title">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#panel3" aria-expanded="true"><spring:theme code="register.my.center.heading" /></a>
			</h5>
			<div id="panel3" class="panel-collapse collapse in" aria-expanded="true">
				<div class="collapsed-content">
					<div class="row">
						<div class="col-xs-12 col-md-4">
							<div class="instructions">
								<p><spring:theme code="register.my.center.subheading.one" /></p>
							</div>
						</div>
						<div class="col-xs-12 col-md-8">
							<p class="optional-fields"><spring:theme code="register.my.center.optional" /></p>
							<div class="list-dropdown-my-profile">
								<div class="sm-input-group">
									<label class="field-label" for="selectPrimaryMall">
										<spring:theme code="text.account.profile.myaccount.myprofile.primary.center" /> <span class="field-error" id="error-primaryMall"></span>
									</label>
									<select class="form-control" name="primaryMall" id="selectPrimaryMall" data-parsley-errors-container="#error-primaryMall" data-parsley-error-message="<spring:theme code="register.primary.mall.invalid"/>" required>
									<option value="" selected><spring:theme code="register.my.center.select.primary.center"/></option>
									<c:forEach items="${mallList}" var="mall">
										<option value="${mall.code}">${mall.name}</option>
									</c:forEach>
									</select>
								</div>
							</div>
							<div class="select-box dropdown dropdown-accordion centersDropdown" data-accordion="#accordion">
								<div class="several-centers"><spring:theme code="register.primary.center.select" /></div>
								<button class="dropdown-toggle centers" type="button"
									data-toggle="dropdown">
									<spring:theme code="text.account.profile.myaccount.myprofile.primary.center.selectacenter" />&nbsp;(<span class="centerCount"></span>)
									<span class="caret-icon"></span>
								</button>
								<ul class="dropdown-menu" id="drpCenters" role="menu" aria-labelledby="dLabel">
									<li>
									<c:forEach items="${mallList}" var="mall" varStatus="counter">
									<div class="item-padding" id="${mall.code}">
									<c:set var="mallCode" value="${mall.code}"/>
										<span class="line-item"> 
											<a href="#" class="custom-checkbox" data-value="${mall.code}" tabIndex="-1"> 				
												<label for="sr-checkbox-${counter.index}" class="hide">Mall Checkbox</label>
												<input type="checkbox" value="${mall.code}" class="sr-only" id="sr-checkbox-${counter.index}"/>
												<em class="i-checkbox"></em>${mall.name}
											</a>
										</span>
									</div>
								</c:forEach>
									</li>
								</ul>					
							</div>
							<c:if test="${not empty captchaEnabledForRegister && captchaEnabledForRegister eq 'true'}">
									<div class="row">
										<div class="col-xs-12 col-md-6 captchaContainer">
											<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
											<div id="captcha" class="has-error"><spring:theme code="register.new.customer.captcha.error.msg" /></div>
										</div>
									</div>
							</c:if>								
							<div class="clearfix row">
								<div class="submit-btn col-md-6 col-xs-12">
									<button class="btn"><spring:theme code="register.new.customer.submit.btn" /></button>
								</div>
								<div class="terms-conditions col-md-6 col-xs-12 small-text">
									<spring:theme code="register.new.customer.ssl.security" />
								</div>
							</div>					             
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" name="alternateMallIds" id="alternateMallIds" value=""/>
	<input type="hidden" name="CSRFToken" value="${CSRFToken}">
	<input type="hidden" name="captchaEnabled" id="captchaEnabled" value="${not empty captchaEnabledForRegister && captchaEnabledForRegister eq 'true'}"/>
</form>