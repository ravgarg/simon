/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery'], function($) {
    'use strict';
    var myaccountSizes = {
        init: function() {
            this.initVariables();
            this.initEvents();
			if( $('#mysizes-data').length > 0 ){
				this.initTables();
			}
		},

        initTables : function(){
            myaccountSizes.fixedTable($('#mysizes-data'));
        },

        fixedTable : function(el) {
			var $body, $header, $sidebar;
			$body = $(el).find('.fixedTable-body');
			$sidebar = $(el).find('.fixedTable-sidebar table');
			$header = $(el).find('.fixedTable-header table');
			return $($body).scroll(function() {
				$($sidebar).css('margin-top', -$($body).scrollTop());
				return $($header).css('margin-left', -$($body).scrollLeft());
			});
		},

        initVariables: function() {

        },

        initAjax: function() {},

        initEvents: function() {
			
		}
	 }
      
    $(function() {
        myaccountSizes.init();
    });

    return myaccountSizes;
});