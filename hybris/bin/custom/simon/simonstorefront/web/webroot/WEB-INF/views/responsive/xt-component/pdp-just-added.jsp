<div class="modal fade" id="justAddedModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content just-added">
            <div class="modal-header">
                <h5 class="modal-title">Just Added</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
			
				<div class="just-added-items">
					
				</div>
		</div>
    </div>
</div>