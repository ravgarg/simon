/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['hbshelpers', 'handlebars', 'jquery', 'ajaxFactory', 'utility', 'templates/mydesignerListModal.tpl', 'templates/mystoreListModal.tpl'], 
	function(hbshelpers, handlebars, $, ajaxFactory, utility, mydesignerListModalTemplate, mystoreListModalTemplate) {
    'use strict';
    var cache;
    var myDesigners = {
        init: function() {
            this.initVariables();
            this.initEvents();
        },
        initVariables: function() {
            cache = {
                $document : $(document),
                $designersSearch: $('.designers-search')
            }
        },
        initAjax: function() {},
        initEvents: function() {
            cache.$document.on('keyup','input[name="search-filter"]', myDesigners.myDesignersSearch); 
            cache.$document.on('click','.callDesignerList', myDesigners.callDesignerList); 
            cache.$document.on('click','.callStoreList', myDesigners.callStoreList); 
			
        },
        myDesignersSearch: function(){
			
			var $this = $(this),
				val = $this.val(),
				str = '';

			$(this).closest('.designers-listing-container').find(".searchList li").show();
			if( val !== '' ){				
				$(this).closest('.designers-listing-container').find(".searchList li").each(function(){
					str = $(this).text().toLowerCase();

					if( str.search( val.toLowerCase() ) === -1  ){
						$(this).hide();
					}else{
						$(this).show();
					}
				});

			}

        },
		
        callDesignerList: function(){
            var options = {
                'methodType': 'GET',
                'dataType': 'JSON',
                'url': $(this).data('url'),
                'isShowLoader': false,
				'cache': true
            }
            ajaxFactory.ajaxFactoryInit(options, function(data){
                $("#addDesignersModal").find('.searchList').html(mydesignerListModalTemplate(data));
                $("#addDesignersModal").modal('show');

                if(cache.$designersSearch.length>0){
                    var checkboxes = $(".designers-search input[type='checkbox']");
                    checkboxes.on('click',function () {
                        if (checkboxes.is(':checked')) {
                            $('.btn-add').removeClass('disabled').removeAttr('disabled');
                        } else {
                            $('.btn-add').addClass('disabled').attr('disabled');
                        }
                    });
                }
            });
        },
		
        callStoreList: function(){
            var options = {
                'methodType': 'GET',
                'dataType': 'JSON',
                'url': $(this).data('url'),
                'isShowLoader': false,
				'cache': true
            }
            $('body').addClass("page-overlay");
            ajaxFactory.ajaxFactoryInit(options, function(data){
                $("#addStoresModal").find('.searchList').html(mystoreListModalTemplate(data));
                $("#addStoresModal").modal('show');
                
                if(cache.$designersSearch.length>0){
                    var checkboxes = $(".designers-search input[type='checkbox']");
                    checkboxes.on('click',function () {
                        if (checkboxes.is(':checked')) {
                            $('.btn-add').removeClass('disabled').removeAttr('disabled');
                        } else {
                            $('.btn-add').addClass('disabled').attr('disabled');
                        }
                    });
                }
                $('body').removeClass("page-overlay");
            });
        }
    }
      
    $(function() {
        myDesigners.init();
    });

    return myDesigners;
});