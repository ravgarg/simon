define('pages/pdp', ['hbshelpers','pdpOverview','pdpAddtocart', 'globalTilesCarousel'],
    function(hbshelpers, pdpOverview) {
        'use strict';
        var pdp = {
            init: function() {
                pdpOverview.init();
            }
        };

        $(function() {
            pdp.init();
        });
    });