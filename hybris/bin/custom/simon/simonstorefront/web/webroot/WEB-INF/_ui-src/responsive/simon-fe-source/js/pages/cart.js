/**
* @function : rlutility
* @description : use this for global email subscription modal functionality
*/

define('pages/cart', ['jquery','templates/cartPageTemplate.tpl','templates/cartmessages.tpl','handlebars','slickCarousel','viewportDetect','ajaxFactory','cartcheckoutordesummary','templates/shippingReturn.tpl','globalCustomComponents','analytics','utility'], 
    function($, cartPageTemplate, cartpagemessageTemplate,handlebars,slickCarousel, viewportDetect,ajaxFactory,cartcheckoutordesummary,shippingReturnTemplate,globalCustomComponents,analytics,utility) {

        'use strict';
        var cache;
        var cartpage = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.initAjax();
				if($('#analyticsSatelliteFlag').val()==='true'){
                    analytics.analyticsOnPageLoad();
                }
                this.cartOrderSummaryAccordian();
                this.cartItemsAccordian();
                this.editItemCarousel();
                this.cartwithitems();
            },

            initVariables: function() {
                cache = {
                    $document : $(document),
                    $window :$(window),
                    $priceContainer : $('.order-summary-price-container'),
                    $itemContainer : $('.cart-items-container'),
                    $editItemCarousel :$("#editItemCarousel"),
                    $orderSummaryeElem :$('.ordersummaryContainer'),					
                    $msgContainer : $('#messageContainer'),
                    $cartUpdate: false,
					$analyticsProductCode: '',
					$analyticsProductName : '',
					$analyticsCartUpdateFlag : '',
					$analyticsProductQuantity : ''
                }
            },
            initAjax: function() {},
            initEvents: function() {
                cache.$document.on('click','#removeEntry',cartpage.removeUpdateItem);
                cache.$document.on('click','.item-remove',cartpage.removeItemConfirmation);
                cache.$document.on('change','.txt-qty',cartpage.removeUpdateItem);
                cache.$document.on('click','.viewReturnPolicy',utility.openReturnPrivacyPolicyModal);
             },
			
            qtyCheckValidation: function () {
                var max = parseInt($(this).attr('max'));
                var min = parseInt($(this).attr('min'));
                if ($(this).val() > max) {
                    $(this).val(max);
                }
                else if ($(this).val() < min) {
                    $(this).val(min);
                }  
            },
            removeItemConfirmation: function(){
                 var targetElm = $(this);
                $('#removeEntryNumber').val(targetElm.data('entrynumber'))
                $('#removeProductCode').val(targetElm.data('productcode'))
               $('#removeConfirmation').modal();
            },
            removeUpdateItem:function(){
                
                var targetElm = $(this)
                var CSRFToken = $("input[name=CSRFToken]").val();
                var quanity = 0;
                var entrynumber = $('#removeEntryNumber').val();
                var productcode = $('#removeProductCode').val();
                
                $('#removeConfirmation').modal('hide');
                if(targetElm.data('action')!=='remove'){
                    if(targetElm.val() < 1){
                         targetElm.val(1);
                    }
                    else if(targetElm.val()>parseInt($(this).attr('max'))){
                        targetElm.val($('.item-qty span').html());
                        targetElm.parent().parent().find('.errortext').show();
                        return false;
                    }
                    if(targetElm.val() < 1){
                        quanity = 1;
                    }else{
                        quanity = targetElm.val();
                    }
                    entrynumber = targetElm.data('entrynumber');
                    productcode = targetElm.data('productcode');
					
					cache.$analyticsProductQuantity = quanity;
					cache.$analyticsProductName = targetElm.data('productname');
                }
				cache.$analyticsProductCode = productcode;
				cache.$analyticsCartUpdateFlag = targetElm.data('action');
                var formData ={
                    "entryNumber": entrynumber,
                    "productCode": productcode,
                    "quantity":quanity,
                    "CSRFToken":CSRFToken
                }
                var options = {
                    'methodType': 'POST',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': $('#cartData').data('updateapi'),
                    'isShowLoader': false,
                    'cache' : true
                }
                $('.errortext').hide();
                cache.$cartUpdate = true;
                ajaxFactory.ajaxFactoryInit(options, cartpage.initcartpage,'','cartpage');
            },
            
            cartOrderSummaryAccordian: function() {
                if (cache.$document.width() < 992) {
                    cache.$priceContainer.collapse('hide');
                   
                    cache.$document.on('click', '.view-bag-summary', function() {
                        $('.view-bag-summary').toggleClass('minus-icon');
                        $(this).next('div').collapse('toggle');
                    });
                    cache.$priceContainer.on('show.bs.collapse hidden.bs.collapse', function() {
                        $(this).prev('.view-bag-summary').toggleClass('minus-icon');
                    })
                    
                }
            },
            cartItemsAccordian: function () {
                cache.$itemContainer.collapse('show');
                cache.$document.on('click', '.last-call', function() {
                    $(this).next('div').collapse('toggle');
                });
                cache.$itemContainer.on('show.bs.collapse hidden.bs.collapse', function() {
                    $(this).prev('.last-call').toggleClass('plus-icon');
                });
            },
            editItemCarousel : function () {
                cache.$editItemCarousel.slick({
                    autoplay: true,
                    autoplaySpeed: 3000,
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    draggable: true,
                    infinite: false,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: true
                });
            },

            cartwithitems:function(){
                if($('#cartData').length>0){
                    var response= $('#cartData').val();
                    response = $.parseJSON(response);
                    cartpage.initcartPageonLoad(response);
                }
            },
            
            initcartPageonLoad: function(response){
            	$('html, body').animate({ scrollTop: 0 }, '100')
                cache.$msgContainer.show();
                $('#cartEnteries').html(cartPageTemplate(response));
                cartcheckoutordesummary.paintordersummary(cache.$orderSummaryeElem, response);
                $('.cart-item span').html(response.totalUnitCount);
            },

            initcartpage: function(response){
                    $('html, body').animate({ scrollTop: 0 }, '100')
                    cache.$msgContainer.show();
                    $('#cartEnteries').html(cartPageTemplate(response));
                    cartcheckoutordesummary.paintordersummary(cache.$orderSummaryeElem, response);
                    if(cache.$cartUpdate || response.globalFlashMsg){
                    	cache.$msgContainer.removeClass('hide');
                    	cache.$msgContainer.html(cartpagemessageTemplate(response));
                    }
					
                    if($('#analyticsSatelliteFlag').val()==='true'){
						if(cache.$cartUpdate){
							var $param = {
									productcode : cache.$analyticsProductCode,
									productname : cache.$analyticsProductName,
									productquantity : cache.$analyticsProductQuantity,
									type : cache.$analyticsCartUpdateFlag
								};
								analytics.updateCartData( $param );
						}
                    }
					
                    $('.cart-item span').html(response.totalUnitCount);
                    
                    setTimeout(function(){
                        cache.$msgContainer.fadeOut('slow', function(){
                            if(!response.retailerInfoData || response.retailerInfoData.length===0){
                                location.reload();
                                return false;
                            }
                        });
                        
                        if(!response.retailerInfoData || response.retailerInfoData.length===0){
                            location.reload();
                            return false;
                        }
                        
                    }, 6000)
                    
                    $('.cart-items-container').on('show.bs.collapse hidden.bs.collapse', function() {
                        $(this).prev('.last-call').toggleClass('plus-icon');
                    });
                    $('.order-summary-price-container').on('show.bs.collapse hidden.bs.collapse', function() {
                        $(this).prev('.view-bag-summary').toggleClass('minus-icon');
                    })

                    $('.tooltip-init').tooltip();
                    utility.openReturnPrivacyPolicyModal();
                    cache.$cartUpdate = false;
            }
            
        }

        $(function() {
            cartpage.init();
			if(viewportDetect.lastClass === 'large'){
				globalCustomComponents.rightRailAnchored();
			}
        });

        return cartpage;
    });