<div class="item">
    <span class="fav-icon"></span>
    <a href="#">
        <picture>
            <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
            <source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/dress.jpg">
            <source media="(min-width: 768px) and (max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/dress.jpg">
            <!--[if IE 9]></video><![endif]-->
            <img class="img-responsive" alt="" style="" src="/_ui/responsive/simon-theme/images/dress.jpg">
        </picture>
    </a>
    <div class="item-name"><a href="">Last Call</a></div>
    <div class="item-discount"><a href="">Halston Heritage</a></div>
    <div class="item-retailer"><a href="">1 Color</a></div>

    <!-- Color Owl Carousal Starts -->
    <div class="color-switches-carousal">
        <div class="item">  
            <div class="color-palette clearfix">
                <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
            </div>
        </div>
        <div class="item">
            <div class="color-palette clearfix">
                <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
            </div>
        </div>
        <div class="item">
            <div class="color-palette clearfix">
                <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
            </div>
        </div>
        <div class="item">
            <div class="color-palette clearfix">
                <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
            </div>
        </div>
        <div class="item">
            <div class="color-palette clearfix">
                <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
            </div>
        </div>
    </div>
    <!-- Color Owl Carousal Ends -->

            
    <div class="item-color"><a href="">Strapless Structured Dress</a></div>
    <div class="item-discrition">$233.00</div>
    <div class="item-price">$116.50<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="" data-original-title="MSRP: $56.00 <br> List Price: $19.95 <br> Sale Price: $1.95"></div>
    <div class="item-revised-price">50% Off</div>
</div>