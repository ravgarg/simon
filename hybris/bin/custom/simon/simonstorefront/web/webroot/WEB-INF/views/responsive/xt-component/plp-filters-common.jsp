<div class="col-md-5 select-box dropdown dropdown-accordion" data-accordion="#accordion">
    <button 
        class="dropdown-toggle" 
        type="button" 
        data-toggle="dropdown">Filter (8)
        <span class="caret-icon"></span>                
    </button>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <li>
          <div class="panel-group" id="accordion">
            
            <div class="panel">
              <div class="panel-heading">
                  <a class="panel-title" href="#collapse1" data-toggle="collapse" data-parent="#accordion">Size</a>
              </div>
              <div class="panel-collapse collapse" id="collapse1">
                <div class="panel-body">
                    <div class="size-palette clearfix">
                        <a href="#">30</a>
                        <a href="#" class="active">31</a>
                        <a href="#">32</a>
                        <a href="#">33</a>
                        <a href="#" class="crossed">34</a>
                        <a href="#">36</a>
                        <a href="#">38</a>
                        <a href="#">40</a>
                    </div>
                </div>
              </div>
              <button class="btn secondary-btn black" data-toggle="modal" data-target="#emailModal">Apply (2)</button>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <span class="plp-clear">Clear</span>
                <a class="panel-title" href="#collapse2" data-toggle="collapse" data-parent="#accordion">Color (2) </a>
              </div>
              <div class="panel-collapse collapse" id="collapse2">
                <div class="panel-body">
                    <div class="select-color">
                      <div id="colorPalette" class="color-palette clearfix" >
                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
                          Black
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-black.png');"></span>
                          Black
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                        <div class="color-palette-item active">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                        <div class="color-palette-item">
                          <span class="color-switches" data-color-name="black" style="background: url('/_ui/responsive/simon-theme/images/color-brown.png');"></span>
                          Brown
                        </div>

                      </div>
                    </div>
                </div>
              </div>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <a class="panel-title" href="#collapse3" data-toggle="collapse" data-parent="#accordion">Size Type</a>
              </div>
              <div class="panel-collapse collapse" id="collapse3">
                <div class="panel-body">
                    <div class="size-palette size-type clearfix">
                        <a href="#">Regular</a>
                        <a href="#" class="active">Tall</a>
                        <a href="#">Plus Size</a>
                        <a href="#" class="crossed">Petite</a>
                    </div>
                </div>
              </div>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <a class="panel-title" href="#collapse4" data-toggle="collapse" data-parent="#accordion">Designer</a>
              </div>
              <div class="panel-collapse collapse" id="collapse4">
                <div class="panel-body">
                    <div class="designers-container">
                      <div class="sm-input-group">
                          <input type="text" class="form-control" placeholder="SEARCH BY NAME">
                      </div>
                      <h6>My Designers</h6>
                      <p class="plp-login"><a href="#">Log in</a> to view your favorite designers.</p>

                      <h6>All</h6>
                      <div class="checkboxes-container">
                        <div class="checkboxes">
                            <div class="sm-input-group pull-left">
                              <input type="checkbox" name="confirm">
                            </div>
                            7 for all Mankind
                          </div>
                          <div class="checkboxes">
                            <div class="sm-input-group pull-left">
                              <input type="checkbox" name="confirm">
                            </div>
                            AG Adriano Goldschmied
                          </div>
                          <div class="checkboxes">
                            <div class="sm-input-group pull-left">
                              <input type="checkbox" name="confirm">
                            </div>
                            Alice + Olive
                        </div>

                      </div>
                      

                    </div>
                </div>
              </div>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <a class="panel-title" href="#collapse5" data-toggle="collapse" data-parent="#accordion">Store</a>
              </div>
              <div class="panel-collapse collapse" id="collapse5">
                <div class="panel-body">
                  <a href="#">Item a</a><br>
                  <a href="#">Item b</a><br>
                  <a href="#">Item c</a>
                </div>
              </div>
            </div>

            <div class="panel">
              <div class="panel-heading">
                <a class="panel-title" href="#collapse6" data-toggle="collapse" data-parent="#accordion">Category</a>
              </div>
              <div class="panel-collapse collapse" id="collapse6">
                <div class="panel-body">
                  <a href="#">Item a</a><br>
                  <a href="#">Item b</a><br>
                  <a href="#">Item c</a>
                </div>
              </div>
            </div>

          </div>
        </li>
    </ul>
    
</div>
