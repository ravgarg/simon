<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/responsive/nav/breadcrumb"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%-- <link rel="stylesheet" type="text/css" media="all" href="/_ui/addons/hybrisconvertaddon/desktop/common/css/hybrisconvertaddon.css"/> --%>
<template:page pageTitle="${pageTitle}">
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
		<c:if test="${not empty productCategory}">
			<input type="hidden" value="${productCategory}" class="base-product-category" />
		</c:if>
	<div class="container">
	<cms:pageSlot position="csn_breadcrumbCompSlot" var="feature" element="div">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	<div class="row">
	<input type="hidden" class="" id="analyticsDataPDP">
	<input type="hidden" id="analyticsPDPCategory" value="">
		<div class="analytics-productData" data-satellitetrack="product_listing" data-analytics=''>
			<div class="col-xs-12 col-md-8">
				<div id="productHeader">
				</div>
				<div class="pdp-overview-container hide-desktop">
					<div class="pdp-overview-content top">
						<div class="price-container">						
						</div>
					</div>
				</div>
				<section class="product-images-container analytics-alternateImage" data-analytics='{"event": {"type": "link_click","sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product_alternate_image"},"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>", "name": "alternate_product_images"}}' data-satellitetrack="link_track">
					<div class="gallery-carousel">	
						<div class="slick-top-prev"><span class="icon"></span></div>
						<div class="carousel-stage" id="galleryCarousel"></div>
						<div class="slick-bottom-next"><span class="icon"></span></div>
							
					</div>
					<div class="product-image">
						<div class='zoom' id="pdpimage"></div>
						<a href="javascript:void(0);" class="zoom-cross">&nbsp;</a>
						<a href="javascript:void(0);" class="zoom-plus-icon">&nbsp;</a>
					</div>			
				</section>
			</div>
			<!--PDP Overview Section Start-->
			<div class="col-xs-12 col-md-4">
				<div class="pdp-overview-container">
					<div class="pdp-overview-content">
						<div class="price-container">
						</div>
						<div class="analytics-productVariant" data-analytics='{"event": {"type": "prod_view","sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product_attribute"}, "ecommerce" : { "action": "prod_attribute_change" },"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>", "name": "attribute_change"}}' data-satellitetrack="pdp_prod_view">
							<div data-apiurl="/ps/${product.code}" id="colorPalette" class="select-color">							
							</div>
							<div class="clearfix" id="productFilter">
							</div>
						</div>
						<div class="select-quantity">
							<h6>Select Quantity</h6>
							<div class="plus-minus">
								<div class="hide" id="error"></div>
								<a href="javascript:void(0);" class="decrease"></a>
								<label for="quantity" class="hide">Quantity</label>
								<input type="text" class="quantity" id="quantity" name="qty" value="1" min="${minimumStockQuantity}" max="${maximumStockQuantity}"/>
								<a href="javascript:void(0);" class="increase disabled"></a>
							</div>
						</div>

							<div class="add-to-bag">
								<input type="hidden" name="productCodePost" id="skuKey">
								<button class="btn plum major disabled addtocart analytics-addToBag" 
									data-analytics='{ "event": { "sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add_product" }, "link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>", "name": "add_bag" }}' id="addToCartBtn" data-oos-msg="Item no longer available"
									data-addtobag="ADD TO BAG"
									data-apiurl="/cart/add" disabled><spring:theme code="text.pdp.addedtobag" /></button>
							</div>
							
							<div class="buyFromStore hide">
								<button  class="btn plum major  buyfromStorebtn"> </button>
							</div>

							<div class="add-to-favorites pdppagefavContainer addfavs">
								<input type="hidden" value="${product.code}" class="base-code" name="productCode" />
								<input type="hidden" value="${product.baseProduct}" class="base-code-base" name="productBaseCode" />
								<input type="hidden" value="${product.name}" class="base-name" />
								<!-- For anonymous user -->
								<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
									<a href="javascript:void(0);" 
									data-fav-product-code="${product.code}"
									data-fav-name="${product.name}" data-fav-type="PRODUCTS"
												class="fav-icon login-modal openLoginModal"><spring:theme code="text.pdp.addtofavorite" /></a> 
								</sec:authorize>
								<!-- For logged in user -->
								<div class="analytics-addtoFavorite" data-analytics-pagetype="prod_view" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|pdp" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|pdp" data-analytics-component="product">
									<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
										<a href="javascript:void(0);"
											class="mark-favorite fav-icon <c:if test='${product.favorite}'>marked</c:if>"
											data-add-to-fav	="<spring:theme code="text.pdp.addtofavorite" />"
											data-added-to-fav="<spring:theme code="text.pdp.addedtofavorite" />">
											<c:choose>
											<c:when test="${product.favorite}"><spring:theme code="text.pdp.addedtofavorite" /></c:when>
											<c:otherwise><spring:theme code="text.pdp.addtofavorite" /></c:otherwise>
											</c:choose>	 
										</a>
									</sec:authorize>	
								</div>
							</div>						
						</div>

						<div class="xs-row">
							<div class="pdp-overview-accordion">
								<div class="panel-group" id="accordion">
									<div class="panel panel-default">
										<h6 class="panel-title">
											<a class="accordion-toggle collapsed" data-toggle="collapse"
												data-parent="#accordion" href="#baseProductDetail"><em
												class="glyphicon glyphicon-plus"></em>Details</a>
										</h6>
										<div id="baseProductDetail" class="panel-collapse collapse"></div>
									</div>

									<div class="panel panel-default">
										<h6 class="panel-title">
											<a class="accordion-toggle collapsed" data-toggle="collapse"
												data-parent="#accordion" href="#panel2"><em
												class="glyphicon glyphicon-plus"></em>Shipping &amp; Returns
												Information</a>
										</h6>
										
										<div id="panel2" class="panel-collapse collapse">
											<div class="return-information">
											
											<a class="viewpdpReturnPolicy viewReturnPolicy analytics-genericLink" data-analytics-eventtype="link_click|policy_links" data-analytics-eventsubtype="view_shipping_method" data-analytics-linkplacement="pdp" data-analytics-linkname="policy_links" data-analytics-satellitetrack="link_track" data-href="/contentPolicy/"><spring:theme code="order.details.shipping.return.policy.text" /></a>
											
										</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="xs-row">
							<div class="footer-social clearfix analytics-socialIcon" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|social_icons" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-componenttype="product" data-analytics-componentname="" data-satellitetrack="link_track">
								<section>
									<h6>Share</h6>
									<cms:pageSlot position="csn_pdp_socialSlot" var="feature">
										<cms:component component="${feature}" />
									</cms:pageSlot>
								</section>
							</div>
						</div>
						<div class="xs-row">
							<div class="price-may-vary">Prices online may vary from in
								store. Products online may not be available in stores.</div>
							</div>
					</div>
			</div>
			<!--pdp overview section ends-->
			<form:form action="pdpAction" method="post" id="addtoCartForm">

			</form:form>

		</div>

		
		
		
		</div>
		<c:if test="${not empty searchPageData}">
			<div class="row">
				<jsp:include page="../../cms/productcarouselcomponent.jsp"></jsp:include>
			</div>
		</c:if>
		
	</div>
	
	<!--size chart modal-->
	<div class="modal fade" id="sizeGuideModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
	</div>
	<!--//size chart modal-->

	<!-- just added Modal start -->
	<div class="modal fade" id="justAddedModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content just-added">
				<div class="modal-header">
					<h5 class="modal-title"><spring:theme code="text.pdp.addedtoyourbag" /></h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close"></button>
				</div>
				<div class="just-added-items"></div>
			</div>
		</div>
	</div>
	
	<!-- just added modal ends here  -->
	
	 <!--Shipping, Return and Policy modal start-->
       <div class="modal fade" id="shippingReturnModal" role="dialog">
             <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><spring:theme code="return.policy.message" /></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="privacy-content scrollablediv scroll-container">
                               
                        </div>
             		</div>
      		 </div>
       </div>
    <!--Shipping, Return and Policy modal ends-->
   
</template:page>