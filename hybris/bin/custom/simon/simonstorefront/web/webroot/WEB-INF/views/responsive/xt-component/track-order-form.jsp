
<div class="myorder-details-container row">
	<div class="col-md-12">
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
		
		<div class="row">
	       	<div class="col-md-8 col-xs-12">
	       		Did you know you can track all your orders automatically and in one place in the My Account Portal? Don't have an account? <a href="#">Create an account here</a>
	       	</div>
	       	<div class="col-md-2 col-xs-12">
	       		Have an account? 
	       	</div>
	       	<div class="col-md-2 col-xs-12">
	       		<button class="btn secondary-btn black">SIGN In</button>
	       	</div>
	    </div>

		
	    <div class="row">
	    	<div class="order-main-container">

	    		<!-- first form -->
	    		<div class="track-order-form">
					<div class="col-md-6 col-xs-12">
			       		Check your order status by entering your email address and your Shop Premium Outlets Order number on your Confirmation Email.
			       	</div>

			       	<div class="col-md-6 col-xs-12">
			       		<form method="post" action="/customer-service/order-details" id="orderTrack">   
							<div class="sm-input-group">
								<input type="text" id="trackNumber" class="form-control" name="ordernumber" placeholder="Shop Premium Outlets Order Number">
							</div>
							<div class="sm-input-group">
								<input type="text" class="form-control" name="password" placeholder="PASSWORD" title="PASSWORD" id="password">
							</div>

							<div class="submit-btn col-md-12 col-xs-12">
					       		<button class="btn">Check My Order</button>
					       	</div>
						</form>
			       	</div>
		       	</div>


				
				<!-- Listing -->
				<div class="track-order-listing">
					<h5>Result for <span class="text-bold">“00000000000000000000”</span> and <span class="text-bold">“lisa.simone@gmail.com”</span></h5>

					<div class="myorder-details">
						<div class="row hidden-xs order-detail-col">
							<div class="col-md-3 col-sm-12 date-bold">Order Date</div>
							<div class="col-md-5 col-sm-12 date-bold">Retailers</div>
							<div class="col-md-2 col-sm-12 date-bold">Total Price</div>
							<div class="col-md-2 col-sm-12 date-bold">Order Details</div>
						</div>

						<div class="row order-detail-row">
							<div class="col-md-3 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Order Date</div>
								<div class="date-detail">March 8, 2016</div>
							</div>
							<div class="col-md-5 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Retailers</div>
								<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
							</div>
							<div class="col-md-2 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Total Price</div>
								<div class="price">$514.39</div>
							</div>
							<div class="col-md-2 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Order Details</div>
								<div class="underline"><a href="#myordersModal" data-toggle="modal" data-target="#myordersModal">4DSF45FSN56</a></div>
								<div class="status shipped">Shipped</div>
							</div>
						</div>
						
						<div class="row order-detail-row">
							<div class="col-md-3 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Order Date</div>
								<div class="date-detail">December 13, 2015</div>
							</div>
							<div class="col-md-5 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Retailers</div>
								<div>Saks Off 5th, Banana Republic Factory, Nieman Marcus Last Call</div>
							</div>
							<div class="col-md-2 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Total Price</div>
								<div class="price">$247.83</div>
							</div>
							<div class="col-md-2 col-sm-12 order-detail-com">
								<div class="visible-xs date-bold">Order Details</div>
								<div class="underline">7YKL17CYW23</div>
								<div class="status processing">Processing</div>
							</div>
						</div>
					</div>
					<div class="row">
				       	<div class="submit-btn col-md-12 col-xs-12">
				       		<button class="btn">Start Shopping</button>
				       	</div>
				    </div>
				</div>

				
				<!-- Error -->
				<div class="track-order-error">
				<h5>No results found for <span class="text-bold">“00000000000000000000”</span> and <span class="text-bold">“lisa.simone@gmail.com”</span></h5>
					<p>
						Check your order status by entering your email address and your Shop Premium Outlets Order number on your Confirmation Email. To track a retailer specific order, visit the retailer's website directly and search using the retailer specific order number. 
					</p>
					
					<div class="row">
				       	<div class="submit-btn col-md-12 col-xs-12">
				       		<button class="btn">Look Up Another Order</button>
				       	</div>
				    </div>
				</div>


		       	<div id="trackOrderResult"></div>

	       	</div>
	    </div>




		<!-- <div class="row">
			       	<div class="submit-btn col-md-12 col-xs-12">
			       		<button class="btn">Start Shopping</button>
			       	</div>
			    </div> -->
    </div>
</div>
