<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<template:page pageTitle="${pageTitle}">
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}"
	id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}"
	id="totalNumberOfResults" />
<input type="hidden" data-listing-url="true" value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />

 	<div class="category-landing">
		<div class="container">
			<div class="row">
				<div class="col-md-12 hide-mobile">
					<div class="breadcrumbs">
						<c:forEach items="${breadcrumbs}" var="breadcrumbs" varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}"><c:if test="${!status.first}"><span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}</a>
						</c:forEach>
					</div>
				</div>

				<cms:pageSlot position="csn_storeListing_productLeftRefinements"
					var="feature" element="div">
					<cms:component component="${feature}" />
				</cms:pageSlot>

				<div class="col-md-9 store-listing storefront">
				 <div class="add-to-favorites storefront-heading">
					<input type="hidden" value="${storeId}" id="listingId" class="base-code" />
					<input type="hidden" value="${storeName}" class="base-name" />
					<!-- For anonymous user -->
					<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
					<a href="javascript:void(0);" data-fav-product-code="${storeId}" data-fav-name="${storeName}" data-fav-type="RETAILERS"
						class="listfav fav-icon favor_icons  login-modal openLoginModal header-heart"></a>
					</sec:authorize>
					<!-- For logged in user -->	
					<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
					<a href="javascript:void(0);" data-fav-product-code="${storeId}" data-fav-name="${storeName}" data-fav-type="RETAILERS"
						class="listfav  header-heart fav-icon favor_icons <c:if test="${isFavStore}">marked</c:if> login-modal mark-favorite"></a>
					</sec:authorize>
					<h1 class="page-heading major">${storeName}</h1>
				  </div>
				  
				  <div class="page-context-links">
						<span class="truncated"> <cms:pageSlot
								position="csn_storeListing_pageDescriptionSlot" var="feature"
								element="div">
								<cms:component component="${feature}" />
							</cms:pageSlot>
						</span>
					</div>

					<div id="homepageCarousel" class="owl-carousel">
						<cms:pageSlot position="csn_storeListing_heroSlot" var="feature">
							<div class="img-container">
							<cms:component component="${feature}" />
							</div>
						</cms:pageSlot>
					</div>

                 
					
					<div class="custom-component mystore-list">
						<div class="row">
							<cms:pageSlot position="csn_storeListing_promo1Slot" var="feature"
								element="div">
								<div class="col-md-4 col-xs-12 img-container">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
 					<product:mobilebrowsecategories />
			 		<!-- Filters Starts -->
					<product:storeListingproductFilters searchPageData="${searchPageData}" />
					<!-- Filters END -->
					<!-- Product Tiles Starts -->
					<product:productTilesListing searchPageData="${searchPageData}" />
					<!-- Product Tiles END -->
					<c:if test="${fn:length(searchPageData.results)<=0}">
					 <div><spring:theme code="search.no.results" /></div>
					</c:if>
					<div class="row load-more-btn-wrapper">
						<button id="loadMore" class="btn btn-primary load-more block"><spring:theme code='plp.loadmore.text' /></button>
					</div>
					<!-- Product Tiles Ends -->
				</div>
			</div>
 		</div>
	</div>
	<!--  favorites component model start -->
		<div class="myfavorites-container">
			<cms:pageSlot position="csn_howtoFavoritesDetailContentSlot" var="feature">
					<cms:component component="${feature}" />
			</cms:pageSlot>			
		</div>
<!-- favorites component model  END  -->
</template:page>