<div class="owl-carousel" id="homepageCarousel">
	
	<div class="item">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/hero-image@2x.jpg">
				<img class="img-responsive" src="/_ui/responsive/simon-theme/images/hero-image.jpg">
			</picture>
		</a>
		<div class="container carousel-content">
			<h1 class="display-large major">now shop online</h1>
			<h5>Shop our curated edit of this season's standout trends.</h5>
			<h5 class="major"><a href="#">shop women <span>&rsaquo;</span></a> <a href="#">shop men <span>&rsaquo;</span></a> <a href="#">shop designers<span>&rsaquo;</span></a></h5>
		</div>
	</div>

	<div class="item">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/hero-image@2x.jpg">
				<img class="img-responsive" src="/_ui/responsive/simon-theme/images/hero-image.jpg">
			</picture>
		</a>
		<div class="container carousel-content">
			<h1 class="display-large major">now shop online</h1>
			<h5 class="major"><a href="#">shop women <span>&rsaquo;</span></a> <a href="#">shop men <span>&rsaquo;</span></a> <a href="#">shop  <span>&rsaquo;</span></a></h5>
		</div>
	</div>

	<div class="item center-align">
		<a href="#">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/new-hero-image1@2x.jpg">
				<img class="img-responsive" src="/_ui/responsive/simon-theme/images/new-hero-image1.jpg">
			</picture>
		</a>
		<div class="container carousel-content">
			<h1 class="display-largemajor">now shop online</h1>
			<h5>Shop our curated edit of this season's standout trends.</h5>
			<h5 class="major"><a href="#">shop women <span>&rsaquo;</span></a> <a href="#">shop men <span>&rsaquo;</span></a> <a href="#">shop designers<span>&rsaquo;</span></a></h5>
		</div>
	</div>	
</div>