<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<div class="col-xs-12 col-sm-5 global-login-containers analytics-checkoutLogin">
	<h1></h1>
	<h2></h2>
	<h3><spring:theme code="login.title" /></h3>
	<form id="loginForm" class="bt-flabels js-flabels sm-form-validation  analytics-formValidation" data-parsley-validate="validate" data-analytics-type="login" data-analytics-eventtype="login" data-analytics-eventsubtype="login" data-analytics-formtype="login_form" data-analytics-formname="checkout_login_form_page" data-analytics-formtype="loginform" data-satellitetrack="login" action="/checkout/j_spring_security_check" method="post" data-parsley-validate>
	
			<c:if test="${not empty message}">
				<span class="has-error analytics-formSubmitFailure"> <spring:theme code="${message}" /></span>
			</c:if>
			<div class="sm-input-group">
				<label class="field-label" for="loginEmail"><spring:theme code="text.account.profile.myaccount.email"/> <span class="field-error" id="error-j_guestEmailId1"></span></label>
				<input type="email" class="form-control" id="loginEmail"
					placeholder="<spring:theme code="text.account.profile.myaccount.email"/>"
					value="${updateProfileForm.email}" name="j_username" data-parsley-errors-container="#error-j_guestEmailId1" data-parsley-type="email" data-parsley-error-message="<spring:theme code="login.checkout.customer.email.error.msg"/>" required>
			</div>
			<div class="sm-input-group">
				<label class="field-label" for="login-pswd"><spring:theme code="register.new.customer.pwd.placeHolder" /> <span class="field-error" id="error-j_pwd"></span></label>
				<input 
					type="password"
					autocomplete="off"
					class="form-control" 
					placeholder="<spring:theme code="register.new.customer.pwd.placeHolder" />" 
					name="j_password" 
					data-parsley-errors-container="#error-j_pwd" 
					id="login-pswd"
					data-parsley-required-message="<spring:theme code="login.checkout.customer.pwd.error.msg" />" 
					data-parsley-required 
					data-parsley-pwdstrength-message="<spring:theme code="login.checkout.customer.pwd.strength.error.msg" />"
					data-parsley-pwdstrength 
					required />
			</div>
		<div class="clearfix links-container">
			
			<ycommerce:testId code="login_forgotPassword_link">
			<a href="${forgotPasswordLink}" class="forgot-password"><spring:theme code="login.link.forgottenPwd" /></a>
			</ycommerce:testId>
		</div>
		<c:if test="${not empty enableCaptcha && enableCaptcha eq 'true'}">
		<div class="row">
			<div class="col-xs-12 col-md-6 captchaContainer">
				<div class="g-recaptcha" id="rcaptcha" data-sitekey="${captchaKey}"></div>
				<div id="captcha" class="has-error"><spring:theme code="login.customer.captcha.error.msg" /></div>
			</div>
		</div>
		</c:if>
		<ycommerce:testId code="loginAndCheckoutButton">
		<button type="submit" class="btn btn-primary plum"><spring:theme code="text.login.checkout.loginCheckout.btn" /></button>
		</ycommerce:testId>
	</form>
</div>