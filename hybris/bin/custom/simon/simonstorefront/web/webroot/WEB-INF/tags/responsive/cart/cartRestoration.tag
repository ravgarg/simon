<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:url value="/cart" var="cartUrl" />

<c:set var="textPos" value="left" scope="request"/>
<c:if test="${not showModifications}">
	<c:set var="textPos" value="center" scope="request"/>
</c:if>

<c:if test="${not empty restorationErrorMsg}">
	<div class="alert negative">
		<spring:theme code="basket.restoration.${restorationErrorMsg}" />
	</div>
</c:if>