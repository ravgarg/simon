<div class="mydesigners">
<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="my-designers-breadcrumb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="plp-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			
			<button class="btn secondary-btn black btn-width hide-desktop major my-premium-outlet">My premium outlet</button>
			
			<jsp:include page="my-designers-top-heading.jsp"></jsp:include>
			<a class="page-heading-link show-desktop" href="">How to Add Designers</a>
			<div class="page-context-links">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.
			</div>
			<a class="page-heading-link hide-desktop" href="">How to Add Designers</a>
			
			<jsp:include page="myaccount-adddesigners.jsp"></jsp:include>
			
			<jsp:include page="mydesigners-featured-heading.jsp"></jsp:include>

			<jsp:include page="myaccount-mydesigners-shop.jsp"></jsp:include>
			
		</div>
	   </div>
	</div>
</div>