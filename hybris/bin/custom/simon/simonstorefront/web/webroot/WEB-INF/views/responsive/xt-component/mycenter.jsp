<div class="mycenter"><div class="container" >
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<jsp:include page="my-center-breadcrumb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="plp-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">				
				<button class="btn secondary-btn black btn-width hide-desktop major my-premium-outlet">My premium outlet</button>
				
				<jsp:include page="my-center-top-heading.jsp"></jsp:include>
				<a class="page-heading-link show-desktop" href="">Change my center</a>
				<div class="page-context-links">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.
				</div>
				<a class="page-heading-link hide-desktop" href="">Change my center</a>
				<div class="trending-now common-heading">
					<div class="row">
						<!--this cms component already developed and used in homepage and clp-->
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
						<div class="trending-items col-xs-12 col-md-4">
							<a target="" href="">
								<img class="img-responsive img-hover-effect" src="/_ui/responsive/simon-theme/images/shot-08-1278-02@2x.jpg" alt="">
							</a>
							<a target="${titleTextTarget}" href="${titleText.url}">
								<h4 class="major">Local Center Homepage</h4>
							</a>
						</div>
						
					
						
					
				
						
					</div>
				</div>
			</div>

	</div>
</div></div>