define('pages/plp', ['hbshelpers','analytics', 'slickCarousel', 'viewportDetect','globalCustomComponents','utility','loadMore','myDesigners'],
    function(hbshelpers, analytics, slickCarousel, viewportDetect,globalCustomComponents, utility,loadMore,myDesigners) {

        'use strict';
        var cache,
        	FAV_TOOLTIP_TIMEOUT = 3000;;
        var plp = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.clpCarousal();
                this.plpSearchMobileCarousal();
                if(viewportDetect.lastClass === 'small'){
                    this.colorSwitchesMobile();
                }else{
                    this.colorSwitchesCarousal();
                    plp.openActiveLink();
                }
                this.plpFilter();
                if($('#analyticsSatelliteFlag').val()==='true'){
                    analytics.analyticsOnPageLoad();
                }
				globalCustomComponents.socialShopCarousel(cache.$carousalFooter);

                plp.updateSearchClearFilter();
                plp.browseCategories();
				
				
				if($('.left-menu-container .left-menu .active').length) {
	            	$('.left-menu-container .left-menu').find('.active').find('.level-1.collapse').addClass('in');
	            }
            },
            initVariables: function() {
                 cache = {
                    $document : $(document),
                    $carousalFooter : $('#carousalFooter'),
                    $homepageCarousel : $('#homepageCarousel'),
                    $plpSearchMobileCarousal : $('.initCarouselMobile'),
                    $dropdownAccordion : $('.dropdown-accordion'),
                    $colorSwitchesCarousal : $('.color-switches-carousal'),
                    $dropdownMenu : $('ul.dropdown-menu'),
                    isSearchPage: $('body').hasClass('page-searchListPage'),
                    $pageContainer: $('.page-container'),
                    $truncated: $('.truncated')
                }
            },
            initEvents: function() {
                $(document).on('initPLPColorSwatchCarousel', function(){
                    plp.colorSwitchesCarousal();
                    $('.fav-icon .tooltip-init').tooltip();
                    $('.fav-icon .tooltip-init')
                    .on('show.bs.tooltip', plp.favTooltip);
                });
				$(document).find('.add-deals').on('click', function() {
					$( $(this).attr('data-target') ).modal('show'); 
                    if($(this).hasClass('minus-icon')){
                        $(this).removeClass('minus-icon');
                    }else{
                        $(this).addClass('minus-icon');
                    }
                    
                    var key = $(this).data('key'),
                        keyId = '#' + key,
                        target = $(this).parents('.page-container').find(keyId);
                        target.removeClass('collapsed').addClass('minus-icon');
						target.parent('.panel-title').next().addClass('in');
                   
                });
				$(document).find('.login-modal').on('click', function() {
					$( $(this).attr('data-target') ).modal('show'); 
				});
                //init expand text
            	$(document).on('click','.openLoginModal',utility.openGlobalLoginModal);
                plp.plpExpandText();                
                $('#sortForm, #mSortForm')
                	.off('click.sortForm')
                	.on('click.sortForm', '.dropdown-menu li', function(){
                		var $form = $(this).parents('form');
                		var newVal = $(this).data('value');
                		$form.find('[name="sort"]').val(newVal);
                		$form.submit();
                    });
                
                $('#mBrowseCategories')
                    .off('shown.bs.modal')
                    .on('shown.bs.modal',plp.openActiveLinkOnModal)
                    .off('hide.bs.modal')
                    .on('hide.bs.modal', plp.collapseLeftNav);

                // auto hide tooltip on fav added
                $('.fav-icon .tooltip-init')
                    .on('show.bs.tooltip', plp.favTooltip);
                
                // Bind events for multiselect filters
                $('.multiselect-filter')
                    .off('click.multiselect-filter')
                    .on('click.multiselect-filter', '.multiselect-item', plp.multiselectFilter)
                    .on('click.multiselect-filter', '.clear-facets', plp.clearFacets);

                $('.designers-listing-container').on('click','.checkAll', plp.checkAll); 
               
            },
            checkAll: function(e){
                e.preventDefault();
                var $this=$(this);

                if($this.hasClass('selectAll')){
                    $this.parent().next().find('input:checkbox').prop('checked', true);
                    $this.removeClass('selectAll');
                }
                else{                    
                    $this.parent().next().find('input:checkbox').prop('checked', false);
                    $this.addClass('selectAll');
                }
            },
            
            clpCarousal:function(){
				var $arrow = false;
					if( $('.designer-landing-page').length === 1 ){
						$arrow = true;
					}
				
                var options = {
                    autoplay: false,
                    autoplaySpeed: 3000,
                    slidesToShow: 1,
                    arrows: $arrow,
                    infinite: true,
                    dots: true,
                    draggable: true,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: false,
                    prevArrow: '<button class="slick-prev"></button>',
                    nextArrow: '<button class="slick-next"></button>',
                    responsive: [
                        {
                            breakpoint: 991,
                            settings: {
                                arrows: false
                            }
                        }
                    ]
                }
                globalCustomComponents.heroCarousel(cache.$homepageCarousel, options);
                cache.$homepageCarousel.find('.tail-hide').removeClass('tail-hide');
            },

            // Tiles Carousal
            colorSwitchesCarousal:function(){
                var swatchesCarousal = $('.color-switches-carousal');
                var options = {
                    autoplay: false,
                    autoplaySpeed: 3000,
                    slidesToShow: 4.5,
                    arrows: true,
                    infinite: false,
                    dots: false,
                    draggable: true,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    singleItem: true,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: true,
                    prevArrow: '<button class="slick-prev"></button>',
                    nextArrow: '<button class="slick-next"></button>',
                    responsive: [
                        {
                            breakpoint: 991,
                            settings: {
                                arrows: false
                            }
                        }
                    ]
                }
                
                swatchesCarousal.not(".slick-initialized").slick(options);

               // Hide first arrow of carousal
                swatchesCarousal.find('.slick-prev').hide();
                swatchesCarousal.find('.slick-next').on('click', function() {
                    $(this).closest(swatchesCarousal).find('.slick-prev').show();                     
                });
            },

            // Tiles Carousal
            colorSwitchesMobile:function(){
                if ( (viewportDetect.lastClass === 'small') && ($("div").hasClass('color-switches-carousal')) ) {
                        // Adding class for mobile
                    $('.color-switches-carousal').addClass('color-switches-mobile');
                    $('.color-switches-carousal').append("<span class='count'></span>");  
                    $('.color-switches-carousal').each(function() {
                            var itemLength = $(this).find('.item').length;
                            
                            if(itemLength>4) {
                            	$(this).find('.count').html('+'+(itemLength-4));
                            }
                        });
                }
            },

            // Tiles Filter Accordion
            plpFilter: function() {
                // Collapse accordion every time dropdown is shown
                cache.$dropdownAccordion.on('show.bs.dropdown', function () {
                  var accordion = $(this).find($(this).data('accordion'));
                  accordion.find('.panel-collapse.in').collapse('hide');    
                });

                // Prevent dropdown to be closed when we click on an accordion link
                cache.$dropdownAccordion
                    .off('click.dropdownAccordion')
                    .on('click.dropdownAccordion', 'a[data-toggle="collapse"]', function (event) {
                        event.preventDefault();
                        event.stopPropagation();
                        $($(this).data('parent')).find('.panel-collapse.in').collapse('hide');
                        $($(this).attr('href')).collapse('show');
                    });

                // Stopping Filter dropdown to close
                cache.$dropdownMenu
                    .off('click.dropdownMenu')
                    .on('click.dropdownMenu', '.panel-body', function(event) {
                        event.stopPropagation();
                    });



                // Mobile viewport - expand collapse
                    var viewPort = viewportDetect.lastClass;
                    if(viewPort === 'small') {
                    $('.plp-filters-modal .panel-title').on('click', function() {
                        $(this).closest('.panel').find('.panel-collapse').toggleClass('in').css('height','inherit');
                        $(this).closest('.panel').siblings().find('.panel-collapse').removeClass('in');
                    });
                }

            },

            //this is used to expand plp categories link
            plpExpandText: function () {
                var viewPort = viewportDetect.lastClass;
                if(viewPort === 'small') {
                    var maxLength = 35;
                    cache.$truncated.each(function(){
                        var myStr = $(this).text();
                        if($.trim(myStr).length > maxLength){
                            var newStr = myStr.substring(0, maxLength);
                            var removedStr = myStr.substring(maxLength, $.trim(myStr).length+2);
                            $(this).empty().html(newStr);
                            $(this).append('<a href="javascript:void(0);" class="read-more"><span class="color-black">... </span>See More +</a>');
                            $(this).append('<span class="more-text">' + removedStr + '</span>');
                        }
                    });
                    cache.$truncated.find(".read-more").on('click', function(){
                        $(this).siblings(".more-text").toggle();
                        $(this).remove();
                    });
                }
            },

            //this is used to init carousal on plp and search page
            plpSearchMobileCarousal:function() {
                var viewPort = viewportDetect.lastClass;
                if(viewPort === 'small') {
                    cache.$plpSearchMobileCarousal.slick({
                        autoplay: true,
                        autoplaySpeed: 3000,
                        slidesToShow: 2,
                        arrows: false,
                        dots: true
                    });
                }
            },
            openActiveLink: function(){
                var $parents = $('.left-menu .active:first')
                                    .parentsUntil('.left-menu')
                                    .filter('li.has-level-1, li.has-level-2');
                
                $('.left-menu .active.has-level-2').children('.collapse').collapse('show');
                if(cache.isSearchPage){
                	$('.left-menu .active.has-level-1').children('.collapse').collapse('show');
                }
                $parents.each(function(){
                    $(this).children('.collapse').collapse('show');
                });
            },
            updateSearchClearFilter: function(){
                if(!cache.isSearchPage || typeof $('.clearAllFilter')[0] === 'undefined'){
                    return false;
                }

                var textVal = $("#searchText").val() || utility.getParamValueFromUrl(param),
                    newUrl;
                
                newUrl = window.location.pathname + '?text='+textVal;
                $('.clearAllFilter').attr("href", newUrl);
            },
            browseCategories:function(){
                if(viewportDetect.lastClass === 'small'){
                    var $desktopContent = $('.left-menu-container').html(),
                        $mobBrowseCategories = $('<aside>').addClass('col-md-3');

                    $('.left-menu-container').empty();
                    $mobBrowseCategories.html($desktopContent);
                    $('#mbrowseContent').html($mobBrowseCategories);                        
                }
            },
            collapseLeftNav: function(){
                $('#mbrowseContent .has-level-1 .in.collapse').collapse('hide');
            },
            openActiveLinkOnModal: function () {
                if(/^\s*$/.test($('#mbrowseContent').html()) === true){
                    plp.browseCategories();
                }                        
                plp.openActiveLink();
            },
            favTooltip: function () {
                var $this = $(this);
                setTimeout(function(){
                    $this.tooltip('hide');
                }, FAV_TOOLTIP_TIMEOUT);
            },
            multiselectFilter: function(event){
				var target = $( event.target );
				if ( target.is( "a" ) ) {
					event.preventDefault();
				}
               
                var $this = $(this),
                    $parent = $this.parents('.multiselect-filter:first'),
                    $applyBtn = $parent.find('.apply-btn'),
                    $selectedItems,
                    count,
                    url,
                    actionOnFacet = 'remove';
                $this.toggleClass('active');

                $selectedItems = $parent.find('.multiselect-item.active');
                count = $selectedItems.length;
                $parent.find('.filter-count').html( (count > 0) ? '('+count+')' : '');

                if($this.hasClass('active')){
                    actionOnFacet = 'add';
                    $this.find('input[type="checkbox"]').prop('checked', true);
                }
                else {
                	$this.find('input[type="checkbox"]').prop('checked', false);
                }
                var newQ = plp.updateSpecificFacet(actionOnFacet, $parent, $parent.data('facet'), $this.data('code'));

                // enable disable Apply button
                if(count > 0){
                    url = ($applyBtn.data('href') || window.location.pathname)+plp.rebuildFacetUrl($parent, newQ);
                    $applyBtn.removeClass('disabled');
                    $parent.find('.clear-facets').removeClass('hide');
                }else{
                    url = ($applyBtn.data('href') || window.location.pathname)+plp.rebuildFacetUrl($parent, newQ);
                    $parent.find('.clear-facets').addClass('hide');
                }
                
                // update apply Btn href
                $applyBtn.attr('href', url);
                return false;
            },
            updateSpecificFacet: function(action, $elem, facet, value){
                // Assuming all the facet values will be unique
            	var searchTermWithPlus = decodeURIComponent($('input[name=q]:first').val()),
            		q= searchTermWithPlus.replace(/\+/g, ' '),
                    qArray = q.split(':'),
                    valIndex = qArray.indexOf(value);

               if($elem.data('curr-q') && $elem.data('curr-q') !== ''){
                    q = $elem.data('curr-q');
                    qArray = q.split(':');
                    valIndex = qArray.indexOf(value);
                }
               
                if(action === 'add'){
                    // Do nothing if unique facet value exists
                    if(valIndex > 0 ){
                        $elem.data('curr-q', q);
                        return encodeURIComponent(q);
                    }
                    // Add key and value form even and odd position respectively
                    qArray.push(facet, value);
                }else if(action === 'remove'){
                    // value will be at odd position & key at even
                    if(valIndex < 1 ){
                        $elem.data('curr-q', q);
                        return encodeURIComponent(q);
                    }
                    // remove key and value form even and odd position respectively
                    qArray.splice(valIndex-1, 2);
                }
                
                q = qArray.join(':');
                $elem.data('curr-q', q);
                return encodeURIComponent(q);
            },
            rebuildFacetUrl: function($elem, newQ){
                var queryArray = window.location.search.split('&'), 
                    index = -1;
                if($elem.data('curr-query') && $elem.data('curr-query') !== ''){
                    queryArray = $elem.data('curr-query').split('&');
                }
                // search for 'q='string
                queryArray.forEach(function(item, i){
                    if(item.indexOf('q=') >= 0){
                        index = i;                    
                    } 
                });
                
                // put q at 0th index if it at zero or not exist
                if(index === 0){
                    newQ = '?q=' + newQ;
               }else if(index === -1){
                    index = 0;
                    newQ = '?q=' + newQ;
               }else{
                    newQ = 'q=' + newQ;
                }

                if(newQ !== ''){
                    queryArray[index] = newQ;
                }else{
                    queryArray.splice(index, 1);
                }
                
                $elem.data('curr-query',queryArray.join('&'));
                return queryArray.join('&');
            },
            clearFacets: function(){
                event.preventDefault();
                var $this = $(this),
                    $parent = $this.parents('.multiselect-filter:first'),
                    $applyBtn = $parent.find('.apply-btn');
                
                // Remove class form multiselect-item
                $parent.find('.multiselect-item.active').removeClass('active');
                $parent.find('.filter-count').html('');                
                $applyBtn.addClass('disabled');
                
                // update apply Btn href
                $applyBtn.attr('href', '#');


                var q = decodeURIComponent($('input[name=q]:first').val()),
                    qArray = q.split(':'),
                    facet = $parent.data('facet'),
                    valIndex = qArray.indexOf(facet);

                while(valIndex !== -1){
                    // remove key and value form even and odd position respectively
                    qArray.splice(valIndex, 2);
                    valIndex = qArray.indexOf(facet)
                };
                
                $parent.removeData('curr-q');                
                var newQ = encodeURIComponent(qArray.join(':')),
                    url;

                $this.addClass('hide');
                
                // enable disable Apply button
                url = window.location.pathname + plp.rebuildFacetUrl($parent, newQ);
                window.location = url;
            }

        };

        $(function() {
            plp.init();
        });
    });