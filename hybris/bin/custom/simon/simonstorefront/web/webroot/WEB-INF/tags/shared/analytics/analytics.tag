<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>

<script type="text/javascript" src="${sharedResourcePath}/js/analyticsmediator.js"></script>
<script src="//assets.adobedtm.com/5010ad1f2b0ac8b34825d876147a0d2c1cc8b33e/satelliteLib-98a41f95cc97511cf169b8e885aca719c1f49a0a-staging.js"></script>
<analytics:googleAnalytics/>
<analytics:jirafe/>