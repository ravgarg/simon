<div class="deals-listing">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">			
				<jsp:include page="deals-listing-breadcrumb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="plp-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">	
				<jsp:include page="deals-listing-top-heading.jsp"></jsp:include>
				<div class="page-context-links">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.
				</div>
				<jsp:include page="deals-listing-shop.jsp"></jsp:include>
				
				<!-------------- DEALS LISTING list same as DEALS LANDING -------------------->
			</div>
		</div>
	</div>
</div>