define('pages/customerServicePage', ['jquery', 'ajaxFactory','viewportDetect', 'utility', 'handlebars', 'templates/trackorder.tpl','templates/shippingReturn.tpl','analytics'],
    function($, ajaxFactory, viewportDetect, utility, handlebars, trackordertemplate,shippingReturnTemplate,analytics) {

        'use strict';
        var cache;
        var customerServicePage = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.trackOrderOnLoad();
                utility.floatingLabelsInit();
                $('ul#CustomerServiceNode').find('li.active').find('a').attr('href','javascript:void(0)');
				        if($('#analyticsSatelliteFlag').val()==='true'){
                  analytics.analyticsOnPageLoad();
                }
				        
            },
            initVariables: function() {
                 cache = {
            		 $document : $(document),
                     $orderTrack : $('#orderTrack'),
                     $trackorderform : $('.track-order-form'),
                     $trackorderlisting : $('.track-order-listing'),
                     $trackordererror : $('.track-order-error'),
                     $errorheading : $('.error-heading'),
                     $trackorderlistingbtns : $('.track-order-listing-btns'),
                     $truncated: $('.truncated')

                }
            },
            initEvents: function() {
                cache.$orderTrack.on('submit', customerServicePage.trackOrderFunc);
                customerServicePage.retrunPageExpandText();
				if($('.modal-list').length > 0){
                    $('.modal-list .accordion-menu').click(function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        if($(this).hasClass('collapsed')) {
                            $(this).removeClass('collapsed');
                            $(this).next('ul').addClass('in');
                        } else {
                            $(this).addClass('collapsed');
                            $(this).next('ul').removeClass('in');
                        }
                    }); 
                }
                $('#shippingReturns').on('changed.bs.select', function () {
                       $('#divShippingReturn').addClass('field-float');
                       $('#noStoreSelected').addClass('pad-top'); 
                       $('.filter-option').addClass('option-color');
                        var formData = {
                            CSRFToken :$("input[name=CSRFToken]").val(),
                            retailerId :$(this).val()
                            }
                             var options = {
                                     'methodType': 'POST',
                                     'dataType': 'JSON',
                                     'methodData':formData,
                                     'url': $(".shipping-return-page").data('url'),
                                     'isShowLoader': false,
                                     'cache' : true
                                 }
                          ajaxFactory.ajaxFactoryInit(options, function(response){
                              $('.privacy-info').hide();
                              $('.selected-store-container').html(shippingReturnTemplate(response));
                          });
                  });
                
                cache.$document.on('click','.orderDetails',function(e){
                	var url = 'track-order/'+$(this).data('guid');
                    $.ajax({
                        url: url,
                        type: "GET",
                        dataType: "html",
                        success: function (data) {
                        	$('#myordersModal').find('.modal-content').html(data);
							$("#myordersModal").on("shown.bs.modal", function () {
								if($('#analyticsSatelliteFlag').val()==='true'){								
									analytics.getOrderDetails();
								}
							}).modal('show');
                        }
                    });
                    e.stopImmediatePropagation();
                    return false;
                	
                });
            },
            trackOrderOnLoad: function(){
            	cache.$trackorderlisting.hide();
                cache.$trackordererror.hide();
                cache.$errorheading.hide();
                cache.$trackorderlistingbtns.hide();
            },
            trackOrderFunc: function(){

	            	var that = $(this),
	                url = that.attr('action'),
	                methodType = that.attr('method'),
	                formData = {
	                    orderId : that.find($('#trackNumber')).val(),
	                    emailId : that.find($('#email')).val(),
	                    CSRFToken :$("input[name=CSRFToken]").val()
	                },
	                options = {
	                    'methodType': methodType,
	                    'dataType': 'JSON',
	                    'methodData': formData,
	                    'url': url,
	                    'isShowLoader': false,
	                    'cache' : true
	                }
	 
	            ajaxFactory.ajaxFactoryInit(options, function(response){
	                $('.track-order-form').hide();
	                $('#trackOrderResult').html(trackordertemplate(response));
					if($('#analyticsSatelliteFlag').val()==='true'){
						analytics.trackingOrderFormSuccess(response);
					}
	                $('#trackOrderResult').show();
	                if($('.error-heading').length > 0) {
	                    $('.track-order-error').show();
                        $('.track-order-listing-btns').hide();
	                } else {
                        $('.track-order-listing-btns').show();
                    }
                  $('.tooltip-init').tooltip();
	            })
	            
	            return false;
            },
             //this is used to expand plp categories link
             retrunPageExpandText: function () {
                var viewPort = viewportDetect.lastClass;
                if(viewPort === 'small') {
                    var maxLength = 5;
                    cache.$truncated.each(function(){
                        var myStr = $(this).text();
                        if($.trim(myStr).length > maxLength){
                            var newStr = myStr.substring(0, maxLength);
                            var removedStr = myStr.substring(maxLength, $.trim(myStr).length+2);
                            $(this).empty().html(newStr);
                            $(this).append('<a href="javascript:void(0);" class="read-more"><span class="color-black">... </span>See More +</a>');
                            $(this).append('<span class="more-text">' + removedStr + '</span>');
                        }
                    });
                    cache.$truncated.find(".read-more").on('click', function(){
                        $(this).siblings(".more-text").toggle();
                        $(this).remove();
                    });
                }
            },
        }


        $(function() {            
            customerServicePage.init();
        });
        return customerServicePage;
    });