<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ attribute name="dealsDataList" required="true" type="java.util.List" %>


<c:if test="${not empty dealsDataList}">
<div class="deal-area-container favorite-container myoffers-container deals-padding">
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/add-deal", "remove":"/my-account/remove-deal" }' />
	<!-- all-deals area -->
		<div class="deal-details-container row analytics-deals">
		<input type="hidden" name="selectedDealID" value=""/>
		<c:forEach var="deal" items="${dealsDataList}" varStatus="status">
			<c:choose>
			
				<c:when test="${empty deal.imageURL}">
					<c:set var="url"
						value="/_ui/responsive/simon-theme/images/no-image.jpg"></c:set>
				</c:when>
				<c:otherwise>
					<c:set var="url" value="${deal.imageURL}"></c:set>
				</c:otherwise>
			</c:choose>
			<div class="col-md-4 col-xs-12 deal-details-list">
				<c:url value="${deal.dealUrl}" var="dealUrl"/>
				<div class="deal-block item-tile" data-url="${url}"
					data-name="${deal.name}" data-validity="${deal.validity}"
					data-desc="${deal.description}" data-code="${deal.code}"
					data-shopnow="${dealUrl}" data-type="ONLINE">
					<div class="item">

						<!-- Fav-icon For Anonymous user -->
						<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
							<span data-fav-product-code="${deal.code}" data-fav-name="${deal.name}" data-fav-type="OFFERS" class="icon-fav-blank fav-icon login-modal openLoginModal tooltip-init">
							</span>
						</sec:authorize>

						<!-- Fav-icon For logged in user -->
						<div class="analytics-addtoFavorite" data-analytics-pagetype="myaccountfavorite"  data-analytics-eventsubtype="my_deals"  data-analytics-linkplacement="my_deals" data-analytics-component="deals">
							<input type="hidden" value="${deal.code}" class="base-product-code analytics-base-code" name="${deal.code}" /> 
							<input type="hidden" value="${deal.name}" class="base-product-name analytics-base-name" />
						<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
							<c:choose>
									<c:when test="${deal.favorite}">
										<a href="#" class="icon-fav-blank mark-favorite fav-icon marked tooltip-init" title="" data-placement="bottom" data-toggle="tooltip"><span class="hide"> fav</span></a>
									</c:when>
									<c:otherwise>
										<a href="#" class="icon-fav-blank mark-favorite fav-icon"><span class="hide"> fav</span></a>
									</c:otherwise>
								</c:choose>
						</sec:authorize>
						</div>
						<a href="#" data-toggle="modal" data-target="#modalContent" class="img-link analytics-genericLink analytics-dealClick" data-analytics-eventtype="view_deals_modal" data-analytics-eventsubtype="deals_landing_page" data-analytics-linkplacement="view_deals_modal" data-analytics-linkname="view_deal_modal" data-analytics-satellitetrack="link_track" id="dealModal" data-dealval="${deal.code}"> <c:choose>
								<c:when test="${empty deal.imageURL}">
									<img
										src="/_ui/responsive/simon-theme/images/no-image.jpg"  alt="no-image"/>

								</c:when>
								<c:otherwise>
									<img src="${deal.imageURL}" alt="${deal.altText}"/>
								</c:otherwise>
							</c:choose>
						</a>
						<div class="gift-details" data-analytics-dealname="${deal.name}" data-analytics-dealretailer="${deal.retailerName}">${deal.name}</div>
						<div class="gift-details" data-analytics-dealdiscount="${deal.description}">${deal.description}</div>
						<div>
							<spring:theme code="page.storefront.deals.validity.prefix" />
							${deal.validity} 
						</div>
						<div>
							<c:choose>
								<c:when test="${deal.storeType eq 'BOTH' }">
									<spring:theme code="page.storefront.deals.storeType" />
								</c:when>
								<c:otherwise>
									${deal.storeType}
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		<c:choose>
			<c:when test="${(status.count % 3)==0}">
				<c:if test="${(status.count)==fn:length(dealsDataList)}">
					</div>
				</c:if>
				<c:if test="${(status.count)<fn:length(dealsDataList)}">
					</div>
					<div class="deal-details-container row ">
				</c:if>		
			</c:when>
			<c:otherwise>
				<c:if test="${(status.count)==fn:length(dealsDataList)}">
		   			</div>
				</c:if>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	<div class="modal fade" id="modalContent" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">
						<spring:theme code="text.offer.details.store" />
					</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close"></button>
				</div>
				
				<div class="modal-body">
					<div class="modal-container">
						<div class="offer-full-details">
							<a href="#" id="imgURL">
								<img src="" alt="blank" />
							</a>
							<p class="free-shipping" id="name"></p>
							<p id="validity">
								<spring:theme code="page.storefront.deals.validity.prefix" />
								<span></span>
							</p>
						</div>
						<div class="offer-details-container" id="desc"></div>
                           <div class="sitelogo footer-social clearfix analytics-socialIcon" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|social_icons" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-componenttype="deal" data-analytics-componentname="" data-satellitetrack="link_track" data-name="name" data-title="title">
                               <h6><spring:theme code="page.storefront.deals.share.text" /></h6>
                               <c:choose>
	                               <c:when test="${cmsPage.uid eq 'my-deals'}">
	                               		<cms:pageSlot position="csn_myAccountCMSManagedContentSlot1" var="feature">
	                                      <cms:component component="${feature}" />
	                                	</cms:pageSlot>
	                               </c:when>
	                               <c:otherwise>
	                             			<cms:pageSlot position="csn_dealsLanding_socialSlot" var="feature">
	                                             <cms:component component="${feature}" />
	                             		 	</cms:pageSlot>
	                               </c:otherwise>
                               </c:choose>
                           </div>
						<a href="#" id="imgURL" class="btn shop-now analytics-genericLink" data-analytics-eventtype="shop_now_button_deals_modal" data-analytics-eventsubtype="deals_landing_page" data-analytics-linkplacement="deals|shopnow" data-analytics-linkname="shopnow" data-analytics-satellitetrack="link_track"> <spring:theme
								code="text.account.myStores.shopnow.text" />
						</a>
						<button class="btn secondary-btn black instore">
							<spring:theme code="text.use.in.store" />
						</button>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</c:if>
