define(['ajaxFactory','utility','analytics'],
    function(ajaxFactory,utility,analytics) {
        'use strict';
        var cache = {},
            favorite = {
                init: function() {
                    this.initVariable();
                    this.initEvents();
                    this.checkForError();
                },
                initVariable: function(){
                    if(typeof $('input[data-fav-url]').val() !== 'undefined') {
                        cache.favUrl = JSON.parse($('input[data-fav-url]').val());
						
						if($('.favorite-container').length) {
							if($('.favorite-container input[data-fav-url]').length){
								cache.favUrl = JSON.parse($('.favorite-container input[data-fav-url]').val());
							}
						}	
                    }
                    if (typeof ($('input[data-listing-url]').val()) != 'undefined') {
                        cache.listFavUrl = JSON.parse($('input[data-listing-url]').val());
                    }
                    if (typeof ($('input[data-store-favurl]').val()) != 'undefined') {
                        cache.storeFavUrl = JSON.parse($('input[data-store-favurl]').val());
                    }
                    cache.submitforms = true;
                    cache.delay  = 1000;
                },
                
                checkForError:function(){
                	if($('#errorMessage').length>0){
                		$('#errorMsgContainer').removeClass('hide'); 
                		$('.error-msg').html($('#errorMessage').val());
                	}
                	else{
                		$('#errorMsgContainer').addClass('hide');
                	}
                },
                favAjax: function(type, code, name,urlSelector) {
                    var options = {
                        'methodType': 'POST',
                        'dataType': 'text',
                        'methodData': {
								CSRFToken :ACC.config.CSRFToken, 
								code: code,
								name: name
							},
                        'url': urlSelector[type],
                        'cache' : false 
                    }
                    ajaxFactory.ajaxFactoryInit(options, function(response){
						try {
							response = JSON.parse(response);
						} catch(e) {
							analytics.analyticsAJAXFailureHandler('addtofavorite','something went wrong','CE');
							return;
						} 
						
						
						
                    	/*$('input.base-code').filter(function(){return this.value==response.code})*/
                    	var $elem =$('input.base-product-code').filter(function(){return this.value==response.code}),
							$this = $elem.parents('.item-tile').find('.mark-favorite');
						var actionStatus = response.action+'-'+response.status;
                    	if($('.just-added-items').length){
                    		$this = $(this);
                    	}
                    	if('undefined' === typeof $elem[0]){
                    		$elem = $('input.base-code').filter(function(){return this.value==response.code});
                    	}
                    	if('undefined' === typeof $this[0]){
                    		$this = $elem.parent('.add-to-favorites').find('.mark-favorite')
                    	}
                    	
                    	if($('#justAddedModal').hasClass('in')){
                    		$this = $('#justAddedModal a.mark-favorite');
						}
						
                    	if($('input.base-code').filter(function(){return this.value==response.code}).parent('.add-to-favorites').hasClass('pdppagefavContainer')){
                    		$this = $('input.base-code').filter(function(){return this.value==response.code}).parent('.addfavs').find('.mark-favorite');
                    	}
                    	
                    	//Condition for Designer Listing Page
                    	if($('.my-designers').length){
                    		$this = $('input.base-code').filter(function(){return this.value==response.code}).parents('.add-to-favorites').find('.mark-favorite');
                    	}
                    	
                    	switch(actionStatus){
                    		case 'add-success':
                    			$this.addClass('marked');
                    			if($('#justAddedModal').hasClass('in')){
									$('#justAddedModal a.mark-favorite').addClass('marked');
								}
								
                    			if($this.parents('.deal-details-container').find('.item').length || $this.parents('.just-added-items').length) {
                    				$this.attr('data-toggle','tooltip');
                    				$this.attr('data-placement','bottom');
                    				$this.attr('title','Added to My Favorites');
                    				$this.tooltip('hide');
                    				$this.addClass('tooltip-init');
                    				$this.tooltip('enable');
								}
								if($this.parents('.item-cart').length){
									$this.find('.favorite-text').html('Added')
								}
								else{
									if($('.add-to-favorites .favorite-text').length)
										$this.find('.favorite-text').html($this.data('added-to-fav'))
									else 	
										$this.data('added-to-fav') && $this.html($this.data('added-to-fav'));
								}
								try {
									analytics.analyticsOnPageBtnFavoriteClick($this);
								}catch(e) {
									return;
								}
                    			break;
                    		case 'remove-success':
                    			$this.removeClass('marked');
                    			if($('#justAddedModal').hasClass('in')){
									$('#justAddedModal a.mark-favorite').removeClass('marked');
								}
                    			if($this.parents('.deal-details-container').find('.item').length || $this.parents('.item-cart').length || $this.parents('.just-added-items').length) {
                    				$this.tooltip('disable');
                    			}
                    			
								$this.children('.tooltip-init').tooltip('hide');
								if($this.parents('.item-cart').length){
									$this.find('.favorite-text').html('Add to My Favorites');
								}else{
									if($('.add-to-favorites .favorite-text').length)
										$this.find('.favorite-text').html($this.data('add-to-fav'));
									else
										$this.data('add-to-fav') && $this.html($this.data('add-to-fav'));
								}
								try {
									analytics.analyticsOnPageBtnFavoriteClick($this);
								}catch(e) {
									return;
								}
                    			break;
                    		case 'add-fail':
                    		case 'remove-fail':
                    			// open pop up
                				break;
                    		default: 
                    			break;
                    	}
                        if(!$this.hasClass('marked')){
                            $this.tooltip('hide');
                        }
                    }, '','addtofavorite');
                },  
                initEvents: function() { 
                    $(document)
                        .off('click.markFav')
                        .on('click.markFav', '.mark-favorite', favorite.markFav);
                    $(document).on('click', '.openLoginModal',utility.openGlobalLoginModal);
                    $('.openLoginModalInNavigation').on('click', favorite.favSubNavClick);
                    $('button.add-to-fav-btn').on('click',favorite.submitDesignerFavs);
                    $('button.addToFavStore').on('click',favorite.submitStoreFavs);
                },
                
                
                submitStoreFavs :function(event){
                	event.stopPropagation();
                	if(cache.submitforms){
                		cache.submitforms = false;
                		$.ajax({
                		      type: 'POST',
                		      url: "/my-account/add-stores/",
                		      datatype: "JSON",
                		      data :$('#addStoreForm').serialize(),
                		      success: function(response) {
                		        setTimeout(function() {
                		        	if(response=='true'){
										if($('#analyticsSatelliteFlag').val() === 'true'){
											var $favoriteList = [];
												$('#searchList').find('li').each(function(){											
													if( $(this).find('input').is(':checked') ){
														$favoriteList.push( { 
															id : $(this).find('input').val(), 
															name : $(this).find('span').text(),
															type : 'favorite',
															component : 'retailer'
														}); 
													
													}
												});
											var $data = {
													ecommerce : true,
													type : 'favorited_stores',
													favoriteList : $favoriteList,
													satelliteTrack : 'my_stores'
												};
											analytics.analyticsAJAXSuccessHandler($data);
										}
                            			window.location.href = '/my-account/my-stores';
                            		 }else{
										if($('#analyticsSatelliteFlag').val()==='true'){
											analytics.analyticsAJAXFailureHandler('my-stores','something went wrong','CE');
										}
									 }
                		        }, cache.delay);
                		      },
							  error: function(status, textStatus, errorThrown) {
								  if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.analyticsAJAXFailureHandler('my-stores',errorThrown,'SE',jqXHR.status,ajaxUrl);
								  }
							  }
                		    });
                		    return false;
                	}
                },
                submitDesignerFavs: function(event){
                	if(cache.submitforms){
                		cache.submitforms = false;
                		$.ajax({
                		      type: 'POST',
                		      url: "/my-account/add-multiDesigners/",
                		      datatype: "JSON",
                		      data :$('#addDesignerForm').serialize(),
                		      success: function(response) {
                		        setTimeout(function() {
                		        	if(response=='true'){
										if($('#analyticsSatelliteFlag').val() === 'true'){
											var $favoriteList = [];
												$('#searchList').find('li').each(function(){											
													if( $(this).find('input').is(':checked') ){
														$favoriteList.push( { 
															id : $(this).find('input').val(), 
															name : $(this).find('span').text(),
															type : 'favorite',
															component : 'designer'
														}); 
													
													}
												});
											var $data = {
													ecommerce : true,
													type : 'favorited_designers',
													favoriteList : $favoriteList,
													satelliteTrack : 'my_designers'
												};
											analytics.analyticsAJAXSuccessHandler($data);
										}
                            			window.location.href = '/my-account/my-designers';
                            		 }else{
										if($('#analyticsSatelliteFlag').val()==='true'){
											analytics.analyticsAJAXFailureHandler('add-designers','something went wrong','CE');
										}
									 }
                		        }, cache.delay);
                		      },
							  error: function(status, textStatus, errorThrown) {
								  if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.analyticsAJAXFailureHandler('add-designers',errorThrown,'SE',jqXHR.status,ajaxUrl);
								  }
							  }
                		    });
                		    return false;
                   
                	}
                },
                
                favSubNavClick :function(event){
                	$.ajax({
           	         url: $(this).data('url'),
           	         data: {},
           	         type: "GET",
           	         beforeSend: function(xhr){xhr.setRequestHeader('viewType', 'ajax');},
           	         success: function() {
           	        	event.stopPropagation();
           	        	var favType = $(this).data('fav-type'), 
           	        		favId   = $(this).data('fav-product-code'),
           	        		favName = $(this).data('fav-name'),
           	        		redirectURL = $(this).data('url');
           	        	$('#globalLoginModal').find('#favType').val(favType);
           	        	$('#globalLoginModal').find('#favId').val(favId);
           	        	$('#globalLoginModal').find('#favName').val(favName);
           	        	$('#globalLoginModal').find('#redirectURL').val(redirectURL);
						$('#globalLoginModal').on("shown.bs.modal", function () {
							if($('#analyticsSatelliteFlag').val()==='true'){
								try {
									analytics.analyticsOnPageLoginFlyoutClick($(this));
								}catch(e) {
									return;
								}	
							}
						}).modal('show');
           	         }
           	      });
                	event.stopImmediatePropagation();
                },
                markFav: function(event){
                    event.preventDefault();
                    var $this = $(this),
						code,name,urlSelector;
                    
                    code = $this.parents('.item-tile').find('.base-product-code').val() || 
                                        $this.closest('.add-to-favorites').find('.base-code').val() ||
                                        $this.parent('.leftprodarea').find('.base-code').val();
                    
                    name = $this.parents('.item-tile').find('.base-product-name').val() || 
                    					$this.closest('.add-to-favorites').find('.base-name').val() ||
                    					$this.parent('.leftprodarea').find('.base-name').val();
                    
                    if($this.hasClass('listfav')){
                    	urlSelector = cache.listFavUrl;
                    }
                    else{
                    	if($this.hasClass('pageHeadingfav')){
                    		urlSelector = cache.storeFavUrl;
                    	}
                    	else{
                    		urlSelector = cache.favUrl;
                    	}
                    	
                    }
                    if(!$this.hasClass('marked')){
                        // Fav is added
                        favorite.favAjax('add', code, name,urlSelector);
                    }else{
                        // Remove fav
                        favorite.favAjax('remove', code, name,urlSelector);                        
                    }
                    
                    

                }
            };
    
        $(function() {
        	favorite.init();
        });
       return favorite;
    });