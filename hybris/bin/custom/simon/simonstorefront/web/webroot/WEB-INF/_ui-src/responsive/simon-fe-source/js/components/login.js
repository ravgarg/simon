/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'utility'], function($, utility) {
    'use strict';
    var login = {
        init: function() {
            this.initVariables();
            this.initEvents();
            utility.floatingLabelsInit();
            this.floatingLabelsInit();
            
        },
        initVariables: function() {

        },
        initAjax: function() {},
        initEvents: function() {

        },
        floatingLabelsInit:  function() {
        	window.Parsley.options.requiredMessage = 'The field cannot be empty';
        	function floatingLabel(onload) {
                var $input;
                $input = $(this);
                if (onload) {
                $.each($('.sm-input-group input'), function(index, value) {
                    var $current_input;
                    $current_input = $(value);
                    if ($current_input.val()) {
                    $current_input.closest('.sm-input-group').addClass('field-float');
                    }
                });
                }
                
                if($input.is( "input")){
                    setTimeout(function() {
                    
                        if ($input.val()) {
                            $input.closest('.sm-input-group').addClass('field-float');
                        } else {
                            $input.closest('.sm-input-group').removeClass('field-float');
                        }
                    }, 1);
                }
            }
        
            $('.sm-input-group input').on('keydown', floatingLabel);
            $('.sm-input-group input').on('change', floatingLabel);        
        
            if($('.sm-form-validation1').length){
                $('.sm-form-validation1').parsley();
                $('.sm-form-validation1').parsley().on('form:error', function() {
                    $.each(this.fields, function(key, field) {
                        if (field.validationResult !== true) {
                            field.$element.closest('.sm-input-group').addClass('has-error');
                        }
                    });
                });
                    
                $('.sm-form-validation1').parsley().on('field:validated', function() {
                    if (this.validationResult === true) {
                    this.$element.closest('.sm-input-group').removeClass('has-error');
                    } else {
                    this.$element.closest('.sm-input-group').addClass('has-error');
                    var parsleyError = true;
                    return parsleyError;
                    }
                });
            }
            floatingLabel(true);
        }
    }
      
    $(function() {
        login.init();
    });

    return login;
});