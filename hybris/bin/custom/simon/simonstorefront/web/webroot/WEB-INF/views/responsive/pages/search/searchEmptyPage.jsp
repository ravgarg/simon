<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<template:page pageTitle="${pageTitle}">

	<div class="container">
		<article class="row">
		
			<c:url value="/" var="homePageUrl" />
			<cms:pageSlot position="SideContent" var="feature" element="div" class="side-content-slot cms_disp-img_slot searchEmptyPageTop">
				<cms:component component="${feature}" element="div" class="no-space yComponentWrapper searchEmptyPageTop-component"/>
			</cms:pageSlot>
				
			<div class="col-md-12 hide-mobile">
				<div class="breadcrumbs">
					<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
					<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
				</div>
		
				<!-- breadcurmb Ends -->
			</div>
			
			
			
			<cms:pageSlot position="csn_eslp_leftNavigationSlot" var="comp" >
				<cms:component component="${comp}" />
			</cms:pageSlot>
			
			<div class="search-empty col-md-9">
				<div class="search-zero-result analytics-data" data-analytics='{"ecommerce": {"search": {
                        "term"   : "${fn:toLowerCase(searchPageData.freeTextSearch)}",
                        "type"   : "full_query_search",
                        "count"  : "0" }, 
				"list" :{"type" : "search_result_listing_page","name" : "${fn:toLowerCase(searchPageData.freeTextSearch)}"
								}}}'>
					<div class="no-result">
						<spring:theme code="search.no.results" arguments="${searchPageData.freeTextSearch}"/> 
					</div>
					
				</div>
			
				<div class="btn-category hide-desktop">
				    <button class="btn secondary-btn black"><spring:theme code="text.browse.categories.nosearchresult" /></button>
				</div>
		</div>
	
	</article>
</div>
</template:page>