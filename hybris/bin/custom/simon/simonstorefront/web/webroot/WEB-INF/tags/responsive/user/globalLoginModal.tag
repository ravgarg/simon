<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <div class="modal fade in" id="globalLoginModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><spring:theme code="text.global.login.modal.title" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <form action="/j_spring_security_check" method="post" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-parsley-validate="validate" data-analytics-type="login" data-analytics-eventtype="login" data-analytics-eventsubtype="login" data-analytics-formtype="login_form" data-analytics-formname="login_popup" data-analytics-formtype="loginform" data-satellitetrack="login" data-parsley-validate="validate">
	                  <div class="sign-up-content">
						    <div class="size-guide-content">
						            <h6 class="major"><spring:theme code="text.global.login.modal.header.vipclub" /></h6>
						            <p><spring:theme code="text.global.login.modal.signupnow" /></p>
						    </div>
						    <div class="sm-form-control">
						       <div class="col-md-12 navbar-login">
						              <div class="row">
						                <div class="sm-input-group">
						                  <label class="field-label" for="login_email"><spring:theme code="text.global.login.modal.email.placeholder" /><span class="field-error" id="error-j_guestEmailId1"></span></label>
						                  <input type="email" class="form-control" id="login_email" name="j_username" data-parsley-errors-container="#error-j_guestEmailId1" type="email" data-parsley-type="email" data-parsley-error-message="<spring:theme code="text.global.login.modal.email.error.msg" />" required placeholder="<spring:theme code="text.global.login.modal.email.placeholder" /> " title="<spring:theme code="text.global.login.modal.email.placeholder" />">
										<input type="hidden" id="favType" name="favType"/>
										<input type="hidden" id="favId" name="favId"/>		
										<input type="hidden" id="favName" name="favName"/>
										<input type="hidden" id="redirectURL" name="redirectURL"/>						                
						                </div>
						                <div class="sm-input-group">
						                  <label class="field-label" for="login_password"><spring:theme code="text.login.field.label.password" /><span class="field-error" id="error-j_pswd1"></span></label>
						                  <input type="password" class="form-control" id="login_password" name="j_password" autocomplete="off" data-parsley-errors-container="#error-j_pswd1" data-parsley-required-message="<spring:theme code="text.global.login.modal.pwd.error.msg" />" data-parsley-required required placeholder="<spring:theme code="text.global.login.modal.pwd.placeholder" />" title="<spring:theme code="text.global.login.modal.pwd.placeholder" />">
						                </div>
						                </div>
						              </div>
						              <div class="clearfix links-container">
						                    					                     
						                    <a href="${forgotPasswordLink}" class="forgot-password"><spring:theme code="text.global.login.modal.forgot.password" /></a>
						              </div>
						        <button type="submit" class="btn"><spring:theme code="text.global.login.modal.button.name" /></button>
						    </div>
					  </div>
                </form>
                
                <div class="shoppers-club-section">
				    <div class="size-guide-content">
				                <h6><spring:theme code="text.global.login.modal.vip.shopper.club" /></h6>
				                <p><spring:theme code="text.global.login.modal.join.vip.msg" /></p>
				    </div>
				    <button class="btn secondary-btn black analytics-genericLink" data-analytics-eventsubtype="login_overlay" data-analytics-linkplacement="login_overlay" data-analytics-linkname="join-now" onclick="location.href='/join-now'"><spring:theme code="text.global.login.modal.join.now" /></button>
				</div>
            </div>
        </div>
    </div>
</div>