<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
<input type="hidden" name="CSRFToken" value="${CSRFToken}">
<input type="hidden" id="enableSpreedlyCalls" value="${enableSpreedlyCalls}"/>
<input type="hidden" data-easypost="/checkout/multi/delivery-address/verify-add"
 data-addressadd="/checkout/multi/delivery-address/selected-address" 
 data-updateapi="/checkout/multi/delivery-address/add"
 data-editapi ="/checkout/multi/delivery-address/edit-address" 
 data-estimatecart="/checkout/multi/delivery-address/estimate-cart"
 data-select="/checkout/multi/delivery-method/select"  
 id="cartData" class="analytics-checkoutData" value='${fn:replace(cartDataJson, "'", "&apos;")}'/>
 <input type ="hidden" id="checkoutSpinner" value='<spring:theme code="txt.spinner.checkout.estimated.msg" />'/>
<input type="hidden" id="countryIsoCode" name="countryIsoCode" value='${countryIsoCode}'/>
<input type="hidden" id="cvvLengthValidationError" name="cvvLengthValidationError" value='<spring:message code="payment.cvv.length.invalid" text="CVV length not valid for entered card"/>'/>
<div class="container">
     <multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}"/>
	     <div class="row">
	      <div class="col-xs-12 col-md-8 spacing-right">
	      	<multi-checkout:shipmentItems />
	      </div>
          <div class="col-xs-12 col-md-4 hide-mobile order-summary-right anchoredRightRail">
              <div class="cart-order-summary">
                    <div class="order-Summary-Container"></div>
                    <jsp:include page="../../../xt-component/checkout-right-order-confirmation-summary.jsp"></jsp:include>
                </div>
          </div>
	     </div>
</div>
<jsp:include page="../../../xt-component/checkout-verify-address-modal.jsp"></jsp:include>
<div class="modal fade checkout-leaving-modal" id="checkoutLeavingModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><spring:theme code="checkout.leaving.header" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="checkout-leaving-container clearfix">
                    <p><spring:theme code="checkout.leaving.message" /></p>
                    <div class="leaving-btn">
                      <form action="/cart/" method="get" class="confirmForm">
                      	<button type="submit" class="btn secondary-btn black  confirmleavingcheckout"><spring:theme code="checkout.leaving.message.yes" /></button>
                      </form>
                      <button type="button" class="btn plum" id="notLeavingCheckout" data-dismiss="modal"><spring:theme code="checkout.leaving.message.no" /></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade checkout-leaving-modal" id="inventoryFail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><spring:theme code="product.out.of.stock.error.message.lable" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="checkout-leaving-container clearfix">
                    <p><spring:theme code="product.out.of.stock.error.message" /></p>
                    <div class="leaving-btn">
                      <form action="/cart/" method="get" class="confirmForm">
                      	<button type="submit" class="btn  black  confirmleavingcheckout"><spring:theme code="product.out.of.stock.error.button" /></button>
                      </form>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade checkout-leaving-modal" id="callEstimateFail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">&nbsp;</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <div class="checkout-leaving-container clearfix">
                    <p><spring:theme code="txt.spinner.checkout.estimated.error.msg" /></p>
                    <div class="leaving-btn">
                      <form action="/cart/" method="get" class="confirmForm">
                      	<button type="submit" class="btn  black  confirmleavingcheckout"><spring:theme code="product.out.of.stock.error.button" /></button>
                      </form>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade orderSummaryHelpModal" id="orderSummaryHelpModal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">&nbsp;</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body clearfix">
				<p class="tax-message"><spring:theme code="text.checkout.estimated.tooltip.message1" /></p>
				<p class="tax-message">
					<spring:theme code="text.checkout.estimated.tooltip.message2" />
				</p>
				<div>
					<button class="btn secondary-btn close-wdt" data-dismiss="modal"><spring:theme code="text.checkout.estimated.tooltip.button.label"/></button>
				</div>
			</div>
		</div>
	</div>
</div>


<jsp:include page="../../../xt-component/checkout-shipping-return-information.jsp"></jsp:include>

</template:page>
