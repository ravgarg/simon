<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<template:page pageTitle="${pageTitle}">
	<jsp:include page="../../xt-component/hero-carousal.jsp"></jsp:include> 
	<jsp:include page="../../xt-component/homepage-promotion-banner.jsp"></jsp:include> 
	<jsp:include page="../../xt-component/the-designer-shop.jsp"></jsp:include>
	<jsp:include page="../../xt-component/trending-now.jsp"></jsp:include>
	<jsp:include page="../../xt-component/the-active-shop.jsp"></jsp:include>
	<jsp:include page="../../xt-component/style-deals.jsp"></jsp:include>
	<jsp:include page="../../xt-component/designers-deals.jsp"></jsp:include>
	<jsp:include page="../../xt-component/social-shop.jsp"></jsp:include>
	<jsp:include page="../../xt-component/footer-content-slot.jsp"></jsp:include>
</template:page>
