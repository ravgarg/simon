<div class="login-container">
	<div class="vip-club-signin">
		<div class="container">
			<form action="${action}" method="post" modelAttribute="registerFormData" class="sm-form-validation bt-flabels js-flabels" data-parsley-validate="validate">
				<div class="signin-box">
					<h5 class="major">The VIP Club <span>Sign In</span></h5>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</p>
					<div class="row">
		               <div class="col-xs-12 col-md-12">
		                  <div class="sm-input-group">
		                  		<label class="field-label">Email Address<span class="field-error" id="error-j_guestEmailId"></span></label>
		                        <input type="email" class="form-control" placeholder="Email Address" name="email" data-parsley-errors-container="#error-j_guestEmailId" type="email" data-parsley-type="email" data-parsley-error-message="Please enter your email address" required>
		                  </div>
		               </div>
		               <div class="col-xs-12 col-md-12">
		                  <div class="sm-input-group">
		                  		<label class="field-label">Password<span class="field-error" id="error-j_pswd"></span></label>
		                        <input type="password" autocomplete="off" class="form-control" placeholder="Password" name="pwd" data-parsley-errors-container="#error-j_pswd" data-parsley-error-message="Please enter a password" required>
		                  </div>
		               </div>
		               <div class="col-xs-12 col-md-12">
		                 <label class="custom-checkbox"><input type="checkbox" class="sr-only" id="changePassCheckbox"/><i class="i-checkbox"></i>Stay Logged In</label>
		               </div>
		               <div class="submit-btn col-md-12 col-xs-12">
		            		<button class="btn white">Sign In</button>
		            	</div>
		            	<div class="bottom-section col-md-12 col-xs-12">
			            	<ul>
			            		<li><a href="#">Join the VIP Club</a></li>
			            		<li><a href="#">Forgot Password</a></li>
			            	</ul>
			            	<p>Nibh euismod tincidunt ut laoreet dolore magna.</p>
		            	</div>
		            </div>
				</div>
			</form>
		</div>
	</div>
	<div class="exclusive-container">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-xs-12">
					<a href="#"> 
						<img src="/_ui/responsive/simon-theme/images/gif.png" />
					</a>
				</div>
				<div class="col-md-7 col-xs-12">
					<div class="online-offers">
						<h5>Get Exclusive Online Offers</h5>
						<p>The VIP Shopper Club is a membership-based club for our savviest shoppers. Members have access to the exclusive online offers not available to the public and also receive periodic center updates. With no cost for membership, theres nothing holding you back from getting access to the best outlet offers. Sign up now!</p>
						<div class="row">
							<div class="col-md-6 col-xs-12">
								<div class="list-heading">Special Offers & Perks</div>
								<ul>
				            		<li>Labore et dolore magna aliquab</li>
				            		<li>Nisi ut aliquip ex ea commodo</li>
				            		<li>Deserunt mollit anim id</li>
				            	</ul>
							</div>
							<div class="col-md-6 col-xs-12">
								<div class="list-heading">Online Convenience</div>
								<ul>
				            		<li>Labore et dolore magna aliquab</li>
				            		<li>Nisi ut aliquip ex ea commodo</li>
				            		<li>Deserunt mollit anim id</li>
				            	</ul>
							</div>
						</div>
		               <div class="shopper-club col-md-12 col-xs-12">
		            		<button class="btn">Join The VIP Shoppers Club</button>
		            	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>