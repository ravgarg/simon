<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/responsive/common/header"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="email" tagdir="/WEB-INF/tags/responsive/email"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:master pageTitle="${pageTitle}">
	<jsp:attribute name="pageCss">
		<jsp:invoke fragment="pageCss" />
	</jsp:attribute>

	<jsp:attribute name="pageScripts">
		<jsp:invoke fragment="pageScripts" />
	</jsp:attribute>
	
	<jsp:body>
	<c:if test="${!ispunchout}">
	<c:choose>
			<c:when test="${pageStyleId == 'checkout'}">
	     		<header:checkoutHeader hideHeaderLinks="${hideHeaderLinks}" />
			</c:when>
			<c:when test="${pageStyleId == 'orderConfirmation'}">
	     		<header:header hideHeaderLinks="${hideHeaderLinks}" />
			</c:when>
			<c:otherwise>
	       		<c:if
					test="${cmsPageRequestContextData.page.uid != 'ErrorPage500'}">
					<header:header hideHeaderLinks="${hideHeaderLinks}" />
				</c:if>
			</c:otherwise> </c:choose>
			
			<div>
				
				<cart:cartRestoration />
				<jsp:doBody />
			</div>
			<c:choose>
			 <c:when test="${pageStyleId == 'checkout'}">
	           <footer:checkoutFooter />
	         </c:when>
			 <c:otherwise>
				 <c:if test="${cmsPageRequestContextData.page.uid !='ErrorPage40x' && cmsPageRequestContextData.page.uid != 'ErrorPage500' && cmsPageRequestContextData.page.uid != 'beta-login'}">
					<footer:footer />
			     </c:if>
			 </c:otherwise> 
			</c:choose>
	  <c:if test="${cmsPageRequestContextData.page.uid != 'beta-login'}">
	    <!-- Global Login Modal Starts Here -->
		<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
			 <user:globalLoginModal />
		</sec:authorize>
		<!-- Global Login Modal Ends Here -->
		<!-- Share Email Modal Starts Here -->
		 <email:shareEmailModal />
		<!-- Share Email Modal Starts Here -->
		</c:if>
		</c:if>
	</jsp:body>

</template:master>
