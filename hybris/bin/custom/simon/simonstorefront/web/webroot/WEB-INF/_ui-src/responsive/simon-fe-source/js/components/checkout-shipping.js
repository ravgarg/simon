define(['hbshelpers', 'handlebars', 'utility', 'ajaxFactory','templates/checkoutShipping.tpl', 'cartcheckoutordesummary','templates/checkoutUserData.tpl','templates/shippingForm.tpl','templates/shippingEasypost.tpl','checkoutReviewOrder','templates/checkoutReviewOrder.tpl','templates/checkoutSaveInfo.tpl','templates/checkoutPayment.tpl','templates/checkoutShippingAddress.tpl', 'spreedly','customScrollbar','myaccountManageAddress','viewportDetect','globalCustomComponents','templates/checkoutpaymentexistingCVV.tpl','masking','analytics'],
    function(hbshelpers, handlebars, utility, ajaxFactory, checkoutShippingTemplate, cartcheckoutordesummary,checkoutUserDetailsTemplate,shippingForm,easypostFormTemplate,checkoutReviewOrder,checkoutReviewOrderTemplate,checkoutSaveInfoTemplate,checkoutPaymentTemplate, shippingaddressDisplayTemplate,spreedly,customScrollbar,myaccountManageAddress,viewportDetect,globalCustomComponents,existingCVVtemplate,masking,analytics) {
        'use strict';
        var cache;
        var checkoutShipping = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.initAjax();
                this.getCartData();
                utility.floatingLabelsInit();
                this.initScroll();
                if(viewportDetect.lastClass === 'large'){
    				globalCustomComponents.rightRailAnchored();
    			}
                	
                },
            initVariables: function() {
                cache = {
                    $document : $(document),
                    $window : $(window),
                    $checkoutStepsContainer : $('.checkout-shipping-container'),
                    $orderSummaryContainer : $('.order-Summary-Container'),
                    $accountInfo : $('.account-info'),
                    $response : $("#cartData").length >0 ? $.parseJSON($("#cartData").val()) : '',
                    $addressAddFlag : true,
                    $formvalidated : true,
                    $formvalidatedspreedly : true,
                    $formvalidatedloggedInuser : true,
                    $checkoutBreadcrumb : $('.checkout-breadcrumb'),
					          $analyticsError : []
                }
            },
            initAjax: function() {},
            initEvents: function() {
                cache.$document.on('click','.shipping-fields button.guestUserSaveShipping',checkoutShipping.submitshippingformdataGuest);
                cache.$document.on('click','.shipping-fields button.loggedUserSaveShipping',checkoutShipping.submitshippingformdatalogged);
                cache.$document.on('change','.shipping-fields select#shippingFormAddress',checkoutShipping.checkforAddressForm);

                cache.$document.on('change','.useBillingCheckboxContainer :checkbox',checkoutShipping.showHideBillingForm);
                
                cache.$document.on('click','#saveInfo button.skipBtn',checkoutShipping.redirectToPayment);
                cache.$document.on('click','.saveskip-btn button.saveBtn',checkoutShipping.saveUserInfo);
                cache.$document.on('change','#paymentDropdown',checkoutShipping.changePaymentDropdown);
                cache.$document.on('change','#billingDropdown',checkoutShipping.changeBillingDropdown);
                cache.$document.on('click','#editShippingAddress',checkoutShipping.editShippingAddress);
                cache.$document.on('click','.viewReturnPolicy',utility.openReturnPrivacyPolicyModal);
                //"Place Order" button in Right section on Checkout page
                cache.$document.on('click','.placeorderBtn',function() {
                	checkoutShipping.placeOrderbtnClicked();
                });
                cache.$document
                  .off('click.paymentForm')
                  .on('click.paymentForm', '#token_init', checkoutShipping.generateToken)
                  .on('click.paymentForm', '.useBillingCheckboxContainer input[name=confirm]', checkoutShipping.toggleBillingValidation);

                $(document).ready(function($) {
                  $('#checkoutMsgContainer').addClass('hide');
                  if (window.history && window.history.pushState) {

                    $(window).on('popstate', function() {
                      $('#checkoutLeavingModal').modal('show')
                      $('#checkoutLeavingModal').on('click','#notLeavingCheckout',function(){
                        window.history.pushState('forward', null, '?#forward');
                      })
                    });

                    window.history.pushState('forward', null, '?#forward');
                    $('.selectpicker').selectpicker();
                  }
            });
            $("#phonemask,[data-role='phoneNumberValidation']").mask("(999) 999-9999");
            },
            generateToken: function(){
              utility.floatingLabelsInit();
              $('.error-and-msg #errors').empty();
              $(this).submit();
              if($('#analyticsSatelliteFlag').val()==='true'){
                setTimeout(function() {
                  // PAYMENT PANEL - SPREEDLY ERRORS - setTimeout - formsubmit
                  jQuery.unique(cache.$analyticsError);
                  analytics.analyticsOnPagePaymentClick('submit',cache.$analyticsError.join('|'));						  
                }, 4000);
			        }
            },
            toggleBillingValidation: function(){
              var $billingAddress = $('.billingContainer [data-parsley-errors-container]');
              if($(this).prop('checked')){
                $billingAddress.prop('required', false);
              }else{
                $billingAddress.prop('required', true);
              }
            },
            initScroll: function () {
                var $scrollBar = $('#shippingReturnInformation .modal-body .shipping-return-container');
                $scrollBar.addClass('sb-container');
                $scrollBar.scrollBox();
            },
            changePaymentDropdown:function(){
              var $existingCVV = $('.existingCVV'),
                $nosavedPaymentmethod = $('.nosavedPaymentmethod'),
                $disclaimerCopy = $('.disclaimer-copy'),
                $headingContainer = $('.heading_container'),
                $cardsContainer = $('.cards-container'),
                $shippingMethod = $('.shipping-method');
      
              if($(this).val()==='addNewPayment')
              {
                $existingCVV.addClass('hide');
                $nosavedPaymentmethod.removeClass('hide');
                $disclaimerCopy.removeClass('hide');
                $cardsContainer.removeClass('hide');
                $shippingMethod.addClass('hide');
                $existingCVV.empty();
                checkoutShipping.spreedlyinit();
                $(this).closest('form').find('.mandatory-text').removeClass('hide');
                $('.heading_container').slideDown("slow")
                      }
                      else{
                $(this).closest('form').find('.mandatory-text').addClass('hide');
                $cardsContainer.addClass('hide');
                $shippingMethod.removeClass('hide');
                $existingCVV.removeClass('hide');
                $nosavedPaymentmethod.addClass('hide');
                $disclaimerCopy.addClass('hide');
                $headingContainer.addClass('hide');
                $existingCVV.html(existingCVVtemplate());
                checkoutShipping.spreedlyinit();
                $('.heading_container').slideUp("slow");
              }
            },

            changeBillingDropdown:function(){
              if($(this).val()==='addNewBilling')
              {
                $('.billingContainer').slideDown("slow");
              }
              else{
                $('.billingContainer').slideUp("slow");
              }
            },

            redirectToPayment:function(){
              $('#saveInfoTick').removeClass('hide');
              $('.placeorderBtn').removeClass('inactive');
              $('.placeorderBtn').removeAttr("disabled");
              $('.save-information-container').addClass('hide');
              $('.saveinforendered').removeClass('hide');
              if($('#analyticsSatelliteFlag').val()==='true'){
				analytics.analyticsOnPageSaveInformationClick('skip');
                analytics.analyticsOnPagePaymentClick(true);
              }
			  
              $('#panel4').collapse('toggle');
              $('#payment .panel-title .statement').addClass('hide');
              window.scrollTo(500, 700);
              checkoutShipping.spreedlyinit();
            },

            showHideBillingForm:function(){ 	
            	if($('#useShipping').is(":checked")){
            		$('.heading_container, .billingContainer').slideUp("slow");
            		if(!$('#billingDropdown').is(":visible")){
            		  $('.billingContainer').slideUp("slow");
            		  $("#billingDropdown").val($("#billingDropdown option:first").val());
            		}
            		
            	}
            	else{
            		$('.heading_container').slideDown("slow");
            		if(!$('#billingDropdown').is(":visible") || $("#billingDropdown").val()=='addNewBilling'){
            		  $('.billingContainer').slideDown("slow");
            		}
            	}	
            },

            editShippingAddress:function(){
                var formData = {
                  CSRFToken :$("input[name=CSRFToken]").val()
                }
                var options = {
                    'methodType': 'GET',
                    'dataType': 'JSON',
                    'methodData':formData,
                    'url': $("#cartData").data('editapi'),
                    'isShowLoader': false,
                    'cache' : true
                }
                ajaxFactory.ajaxFactoryInit(options, function(response){
					if(response.cartData.deliveryAddress != null && response.userloggedin){
						$('.placeorderBtn').addClass('inactive');
						$('.placeorderBtn').attr("disabled","true");
						if(response.cartData.deliveryAddress.visibleInAddressBook){
							cache.$checkoutStepsContainer.html(checkoutShippingTemplate(response));
						}
						else{
							cache.$checkoutStepsContainer.html(shippingForm(response));
						}
					}else{
					  cache.$checkoutStepsContainer.html(checkoutShippingTemplate(response));
					  analytics.analyticsOnPageShippingClick(true);
					}
                  
                  cache.$accountInfo.html(checkoutUserDetailsTemplate(response));
                  cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate(response));
                  if(!response.userloggedin){
                    cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate(response));
                  }
                  cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(response));
				  
                  cache.$addressAddFlag = true;
                  $('.selectpicker').selectpicker();
                },'','checkout_shipping_information');
                
            },
            
            spreedlyinit:function(){
              Spreedly.init();
                Spreedly.on('paymentMethod', function(token, pmData) {
                  var tokenField = document.getElementById("payment_method_token");
                  tokenField.setAttribute("value", token);
                  $('#cardType').val(pmData.card_type);
                  $('#cardNumber').val(pmData.number);
                  $('#lastFourDigitCard').val(pmData.last_four_digits);
                  // Normally would now submit the form..
                  Spreedly.setStyle('number','width: 90%;  border: 1px solid #B4B2B0; height:36.9px;font-size:13.3px; padding: 6px 12px 6px 21px;');
                });

                Spreedly.on('errors', function(errors) {
                	cache.$formvalidatedloggedInuser = true;
                	cache.$formvalidatedspreedly = true;
                	cache.$formvalidated = false;
                    var errorBorder = "2px solid #dd0b0b";
                    for(var i = 0; i < errors.length; i++) {
                      var error = errors[i];
                      if(error["attribute"]) {
                        var masterFormElement = document.getElementById(error["attribute"]);
                        if(masterFormElement) {
                          masterFormElement.style.border = errorBorder
                        } else {
                          Spreedly.setStyle(error["attribute"], "border: " + errorBorder + ";");
                        }
						            cache.$analyticsError.push(error.message);
                      }
                      
                    }
                    
                  });

                  Spreedly.on('validation', function(inputProperties) {
                    if(!inputProperties["validCvv"]) {
                        cache.$formvalidated = false;
                        var errorBorder = "2px solid #dd0b0b";
                        Spreedly.setStyle('cvv', "border: " + errorBorder + ";");
                        $('#checkoutPaymentMsgContainer').removeClass('hide');
                        $('#checkoutPaymentMsgContainer').find('.error-desc').html($('#cvvLengthValidationError').val());
						            cache.$analyticsError.push($('#cvvLengthValidationError').val());						
                        return false;
                    }else{
                    	cache.$formvalidated = true;
                    }
					
                });

                Spreedly.on('ready', function() {
                  Spreedly.setFieldType('text')
                  Spreedly.setNumberFormat('prettyFormat');
                  if(viewportDetect.lastClass === 'large'){
                    Spreedly.setStyle('number','width: 90%;  border: 1px solid #B4B2B0; height:36.9px;font-size:13.3px; padding: 6px 12px 6px 21px;');
                    Spreedly.setStyle('cvv', 'width: 80%; height: 32px; border-radius: 0; border: 1px solid #B4B2B0; padding: .65em 1em; font-size: 91%;');
                  }else{
                    Spreedly.setStyle('number','width: 88%;  border: 1px solid #B4B2B0; height:36.9px;font-size:13.3px; padding: 6px 12px 6px 21px;');
                    Spreedly.setStyle('cvv', 'width: 79%; height: 32px; border-radius: 0; border: 1px solid #B4B2B0; padding: .65em 1em; font-size: 91%; outline: none;');
                  }
                  
                  //For Iphone 7
                  if(screen.width<400) {
                	  $('.payment-cvv').css('width','58.33%');
                	  Spreedly.setStyle('cvv', 'width: 74%');
                  }
                  
                  Spreedly.setPlaceholder('number', 'CARD NUMBER');
                  Spreedly.setPlaceholder('cvv', 'CVV'); 
                  if(!$('.nosavedPaymentmethod').is(":visible") && $('.existingCVV').length>0){
                	  Spreedly.setFieldType('text');
                      Spreedly.setNumberFormat('prettyFormat');
                	  Spreedly.setStyle('cvv', 'width: 78%; height: 32px; border-radius: 0; border: 1px solid #B4B2B0; padding: .65em 1em; font-size: 91%;');
                	  Spreedly.setRecache($('#paymentDropdown').find('option:selected').attr('rel') , {
                      'card_type' :$('#paymentDropdown').find('option:selected').data('cardtype')
                    });
                	  Spreedly.on("recache", function(token, paymentMethod) {
                      cache.$formvalidated = true;
                	});
                  }
                });
                
                $( ".placeOrderBtn" ).unbind("click").click(function( event ) {
                	$(this).submit(function() {
            			return false;
            		});
                    event.preventDefault();
                    checkoutShipping.placeOrderbtnClicked();
                });
            },
            placeOrderbtnClicked : function(){
                Spreedly.validate();
                // check for validation first 
                if($('.billingContainer').is(':visible') && $('#payment-form').parsley().validate() === false){
                	cache.$formvalidatedspreedly = true; 						
                   return false;
                 }
                // check for if user is looged in and placing order for existing saved payment method
                if(!$('.nosavedPaymentmethod').is(":visible") && cache.$formvalidatedloggedInuser){
                	 Spreedly.recache();
                	 if(cache.$formvalidated){
                		 checkoutShipping.placeOrderCallafterSpreedly()
                	 }
                	 cache.$formvalidatedloggedInuser = false;
                	return false;
                 }
                 var normalBorder = "1px solid #ccc";

                // These are the fields whose values we want to transfer *from* the
                // master form *to* the payment frame form. Add the following if
                // you're displaying the address:
                // ['address1', 'address2', 'city', 'state', 'zip', 'country']
                var paymentMethodFields = ['first_name', 'last_name', 'month', 'year'],
                options = {};
                for(var i = 0; i < paymentMethodFields.length; i++) {
                  var field = paymentMethodFields[i];

                  // Reset existing styles (to clear previous errors)
                  var fieldEl = document.getElementById(field);
                  fieldEl.style.border = normalBorder;

                  // add value to options
                  options[field]  = fieldEl.value
                }

                // Reset frame styles
				
                // Tokenize!
                if($('#enableSpreedlyCalls').val() ==='true'){
                    Spreedly.tokenizeCreditCard(options);
                    cache.$formvalidatedspreedly = false;
                }
				
				
                setTimeout(function() {
                  //your code to be executed after 1 second
                    var formData;
                    var formDataBilling;
                    if($('#enableSpreedlyCalls').val() === 'true'){
                        formData = {
                                "spreedlyToken" : $("input[name=payment_method_token]").val(),
                                "saveInAccount" :$('#saveCard').is(":checked"),
                                "nameOnCard":$("#first_name").val() +' '+ $("#last_name").val(),
                                "lastFourDigits":$('#lastFourDigitCard').val(),
                                "cardType":$('#cardType').val(),
                                "cardNumber":$('#cardNumber').val(),
                                "expirationMonth":$('#month').val(),
                                "expirationYear":$('#year').val(),
                                "useShippingAddressAsBilling" : $('#useShipping').is(":checked"),
                                "defaultCardForNextTime":$('#defaultCard').is(":checked"),
                                "CSRFToken" :$("input[name=CSRFToken]").val()
                        }
                    } else {
                        formData = {
                                // START Setting up temporary values for parameters used for payment mocking
                        		"spreedlyToken" : "5DSRBHMmp9NfDvXSvgORuNCbkVt",
                                "lastFourDigits":"1111",
                                "cardType":"visa",
                                "cardNumber":"XXXX-XXXX-XXXX-1111",
                                // END Setting up temporary values for parameters used for payment mocking
                                "saveInAccount" :$('#saveCard').is(":checked"),
                                "nameOnCard":$("#first_name").val() +' '+ $("#last_name").val(),
                                "expirationMonth":$('#month').val(),
                                "expirationYear":$('#year').val(),
                                "useShippingAddressAsBilling" : $('#useShipping').is(":checked"),
                                "defaultCardForNextTime":$('#defaultCard').is(":checked"),
                                "CSRFToken" :$("input[name=CSRFToken]").val()
                        }
                  }
                  if($('.billingContainer').is(":visible")){
                    formDataBilling ={
                      "line1": $('#billingAddress1').val(),
                      "line2": $('#billingAddress2').val(),
                      "town": $('#billingCity').val(),
                      "state": $('#billingState').val(),
                      "postalCode": $('#zipCode').val()
                    }
                    
                  }else{
                    formDataBilling ={
                      "billingAddId": $('#billingDropdown').val()
                    }
                  }
                  $.extend(formData,formDataBilling);
                       
                     var objectOptions = {
                         'methodType': 'POST',
                         'dataType': 'JSON',
                         'methodData':formData,
                         'url': '/checkout/multi/summary/placeOrder',
                         'isShowLoader': false,
                         'cache' : true
                     }
                 if(cache.$formvalidated){
                     ajaxFactory.ajaxFactoryInit(objectOptions, function(response){
                       if(response.code==='ERROR_CODE_3'){
							if($('#analyticsSatelliteFlag').val()==='true'){
								analytics.analyticsOnPagePaymentClick(false,'inventoryFail');
							}
							$('#inventoryFail').modal('show');
                       } 
                       if(response.description){
							$('#checkoutPaymentMsgContainer').removeClass('hide');
							$('#checkoutPaymentMsgContainer').find('.error-desc').html(response.description);
						    if($('#analyticsSatelliteFlag').val()==='true'){
								analytics.analyticsOnPagePaymentClick(false,response.description);
							}
                          window.scrollTo(500, 1000);
                          return false;
                        }
                        if(response.confirmationPageUrl){
                        	try {
                        		analytics.analyticsOnPagePaymentClick(true);
            				}
            				catch(err) {
            				    
            				}
                          window.location.href = window.location.origin + response.confirmationPageUrl;
                        } 
                     },'','checkout_placeorder');
                     cache.$formvalidated = false;
                  }
                }, 4000);
            
            },
            
            placeOrderCallafterSpreedly:function(){
            	var formData = {
                        "paymentInfoId" : $("#paymentDropdown").val(),
                        "CSRFToken" :$("input[name=CSRFToken]").val(),
                        "spreedlyToken" : $('#paymentDropdown').find('option:selected').attr('rel')
                      }
                    var formDataBilling;
                if($('.billingContainer').is(":visible")){
                  formDataBilling ={
                    "line1": $('#billingAddress1').val(),
                    "line2": $('#billingAddress2').val(),
                    "town": $('#billingCity').val(),
                    "state": $('#billingState').val(),
                    "postalCode": $('#zipCode').val()
                  }
                
              }else{
                formDataBilling ={
                  "billingAddId": $('#billingDropdown').val()
                }
              }
              $.extend(formData,formDataBilling);
                   
                    var objectOptions = {
                       'methodType': 'POST',
                       'dataType': 'JSON',
                       'methodData':formData,
                       'url': '/checkout/multi/summary/placeOrder',
                       'isShowLoader': false,
                       'cache' : true
                    }

                    if(cache.$formvalidated){
                        ajaxFactory.ajaxFactoryInit(objectOptions, function(response){
                          cache.$formvalidated = false;
                          if(response.code==='ERROR_CODE_3'){
								if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.analyticsOnPagePaymentClick(false,'inventoryFail');
								}
								$('#inventoryFail').modal('show');
                          }
                          if(response.description){
								$('#checkoutPaymentMsgContainer').removeClass('hide');
								$('#checkoutPaymentMsgContainer').find('.error-desc').html(response.description);
								if($('#analyticsSatelliteFlag').val()==='true'){
									analytics.analyticsOnPagePaymentClick(false,response.description);
								}
                            return false;
                          }
                          if(response.confirmationPageUrl){
							  analytics.analyticsOnPagePaymentClick(true);
                        	  window.location.href = window.location.origin + response.confirmationPageUrl;
                          }
                        },'','checkout_placeorder');
                        cache.$formvalidated = false;
                       }
              return false;
            },

            getCartData:function(){
                if($('#cartData').length>0){
                    checkoutShipping.initCheckoutPage(cache.$response);
                    checkoutShipping.initCheckoutOrderSummary(cache.$response);
                }
                
               
            },

            checkforAddressForm:function(){
                if($(this).val()==='addNewAddr' && $('#cartData').length){
                    cache.$response.loggedInUsermanuallyChange = false;
                    $('#loggedUserShippingContainer').html(shippingForm(cache.$response));
                    $('.shippingformContainer').removeClass('hide');
                    $('.shippingformContainer').closest('form').find('.mandatory-text').removeClass('hide');
                 }
                else{
                    $('.shippingformContainer').addClass('hide');
                    $('.shippingformContainer').closest('form').find('.mandatory-text').addClass('hide');
                }
                utility.floatingLabelsInit();
                $('.selectpicker').selectpicker('refresh');
            },

            submitshippingformdatalogged:function(){
                var formData = {
                        addressId : $('#shippingFormAddress').val(),
                        CSRFToken :$("input[name=CSRFToken]").val()

                }

                var options = {
                    'methodType': 'POST',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': $("#cartData").data('addressadd'),
                    'isShowLoader': false,
                    'cache' : true
                    
                }
            
                ajaxFactory.ajaxFactoryInit(options, function(response){
                  if(!response.addressAdded){
                    $(document).ready(function ($) {
                      $('#checkoutMsgContainer').removeClass('hide');
                        $('#checkoutMsgContainer').addClass('show');
                        $('#checkoutMsgContainer').animate({scrollTop: 0}, '100');
                        $('#checkoutMsgContainer').find('.error-desc').html(response.errorMessage);
                    });
                    cache.$addressAddFlag = true;
					
					if($('#analyticsSatelliteFlag').val()==='true'){
                      analytics.analyticsOnPageShippingClick(false,response.errorMessage);
                    }
					
                    window.scrollTo(500, 10);
                    return false;
                  }
                  if(response.addressAdded){
                      // Checkout Breadcrumb starts
                      cache.$checkoutBreadcrumb.find('li:first-child').addClass('checked').removeClass('active').css('cursor', 'pointer');
                    }
                  if(response.errorMessage==='ERROR_CODE_3'){
					if($('#analyticsSatelliteFlag').val()==='true'){  
						analytics.analyticsOnPageShippingClick(false,'inventoryFail');
					}
                    $('#inventoryFail').modal('show');
                    return false;
                  } 

                    cache.$checkoutStepsContainer.html(shippingaddressDisplayTemplate(response));
                    $('#checkoutMsgContainer').addClass('hide');
                    cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate());
                    if(!response.userloggedin){
                      cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate(response));
                    }
                    var date = new Date(),
                	year = date.getFullYear(),
                	count,
                	arryear = [];
                    arryear.push(year);
                    for(count=0;count<10; count++){
                    	year = year+1;
                    	arryear.push(year);
                    }
                    response.expyear = arryear;
                    cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(response));
                    checkoutShipping.spreedlyinit();
                    analytics.analyticsOnPageReviewClick(true);
                    $('#revieworder').html(checkoutReviewOrderTemplate(response));
                    checkoutShipping.getReviewOrderData(response.cartData.shippingPriceCall); 
                      
                });
            },

            submitshippingformdataGuest:function(e){
                e.preventDefault();
                if($('.shippingformContainer').is(":visible")){
                  if($('#guestUserAddressForm').parsley().validate() === false){
                    if($('#analyticsSatelliteFlag').val()==='true'){
                      analytics.analyticsOnPageShippingClick(false);
                    }
                    window.scrollTo(500, 10);
					          return false;
                  }
                }
                else{
                  checkoutShipping.submitshippingformdatalogged();
                  return false;
                }
                var shippingFormelem = $('.shippingaddressForm');
                var saveInAddressBook = true;
                if($('#saveInAddressBook').length){
                    saveInAddressBook = shippingFormelem.find('#saveInAddressBook').is(":checked");
                }
                var formData = {
                        titleCode : '',
                        firstName : $.trim(shippingFormelem.find('#firstName').val()),
                        middleName: $.trim(shippingFormelem.find('#middleName').val()),
                        lastName : $.trim(shippingFormelem.find('#lastName').val()),
                        line1 : $.trim(shippingFormelem.find('#address1').val()),
                        line2 : $.trim(shippingFormelem.find('#address2').val()),
                        townCity : $.trim(shippingFormelem.find('#city').val()),
                        regionIso : $.trim(shippingFormelem.find('#state').val()),
                        postcode : $.trim(shippingFormelem.find('#zipcode').val()),
                        countryIso : $("input[name=countryIsoCode]").val() ,
                        saveInAddressBook : saveInAddressBook,
                        shippingAddress : true,
                        billingAddress : shippingFormelem.find('.isBillingAddress').is(":checked"),
                        phone : (shippingFormelem.find('#phoneNumber').val()).replace(/[- )(]/g,''),
                        emailId  : shippingFormelem.find('#emailId').val(),
                        CSRFToken :$("input[name=CSRFToken]").val()

                }
                var options = {
                    'methodType': 'POST',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': $("#cartData").data('easypost'),
                    'isShowLoader': false,
                    'cache' : true
                }
                
                ajaxFactory.ajaxFactoryInit(options, function(response){
                  cache.$response = response;
                    if(response.suggestedAddress.verification){
                        $('#checkoutVerifyAddressModal').modal('show').find('.modal-content').html(easypostFormTemplate(response));
                        $('#checkoutVerifyAddressModal').on('click','button.useThisAddress , button.addEditAddress',function(){
                            if(cache.$addressAddFlag){
                              if($(this).hasClass("addEditAddress")){
                                checkoutShipping.submitshippingform(cache.$response,'edit');
                                }else{
                                  checkoutShipping.submitshippingform(cache.$response,'usethisaddress');
                                  cache.$addressAddFlag = false;
                                }
                            }
                            
                        });

                    $('#checkoutVerifyAddressModal').on('click','.suggestedAddress', function(){
                      $('.edit-address-btn').addClass('hide');
                      $('.suggest-address-btn').removeClass('hide');
                      $('.userEntered').removeAttr('checked');
                      $('.suggestedAddress').attr('checked');
                    });
                    $('#checkoutVerifyAddressModal').on('click','.userEntered',function(){
                      $('.edit-address-btn').removeClass('hide');
                      $('.suggest-address-btn').addClass('hide');
                      $('.userEntered').attr('checked');
                      $('.suggestedAddress').removeAttr('checked');
                    });
                    }
                    
                    else{
                    	$('.shipping-error-msg').removeClass('hide');
                        $('#checkoutMsgContainer').addClass('show');
                        window.scrollTo(500, 10);
                        $('#checkoutMsgContainer').animate({scrollTop: 0}, '100');
                        $('#checkoutMsgContainer').find('.error-desc').html(response.suggestedAddress.errorMessage);
                        return false;
                    }
                });
            },

            submitshippingform:function(response,formbutton){
                $('#checkoutVerifyAddressModal').modal('hide');
                response.returnedUser=true;
                if($('input[name=selectaddress]:checked').val()==='suggestedAddress'){
                    response.suggestedAddressFlag=true;
                }
                else{
                    response.suggestedAddressFlag=false;
                }
                cache.$checkoutStepsContainer.html(checkoutShippingTemplate(response));
                if(formbutton==='edit'){
                  cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate());
                  if(!response.userloggedin){
                    cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate());
                  }
                  cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(cache.$response));
                  return false;
                }
                var shippingFormelem = $('.shippingaddressForm');
                var saveInAddressBook = false;
                if($('#saveInAddressBook').length){
                    saveInAddressBook = shippingFormelem.find('#saveInAddressBook').is(":checked");
                }
				
                var formData = {
                        titleCode : '',
                        firstName : $.trim(shippingFormelem.find('#firstName').val()),
                        middleName: $.trim(shippingFormelem.find('#middleName').val()),
                        lastName : $.trim(shippingFormelem.find('#lastName').val()),
                        line1 : $.trim(shippingFormelem.find('#address1').val()),
                       line2 : $.trim(shippingFormelem.find('#address2').val()),
                        townCity : $.trim(shippingFormelem.find('#city').val()),
                        regionIso : $.trim(shippingFormelem.find('#state').val()),
                        postcode : $.trim(shippingFormelem.find('#zipcode').val()),
                        countryIso : $.trim($("input[name=countryIsoCode]").val()) ,
                        saveInAddressBook : saveInAddressBook,
                        shippingAddress : true,
                        billingAddress : shippingFormelem.find('.isBillingAddress').is(":checked"),
                        phone : $.trim(shippingFormelem.find('#phoneNumber').val()),
                        emailId  : $.trim(shippingFormelem.find('#emailId').val()),
                        CSRFToken :$("input[name=CSRFToken]").val()

                }

                var options = {
                    'methodType': 'POST',
                    'dataType': 'JSON',
                    'methodData': formData,
                    'url': $("#cartData").data('updateapi'),
                    'isShowLoader': false,
                    'isShowloaderMsg' : true,
                    'cache' : true
                }
                ajaxFactory.ajaxFactoryInit(options, checkoutShipping.addShippingAddress);
            },

            addShippingAddress:function(response){
            	$('body').addClass('page-overlay');
            	cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate(response));
            	if(!response.userloggedin){
                    cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate(response));
                  }
            	 cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(response));
	              if(!response.addressAdded){
	                $(document).ready(function ($) {
	                  $('.shipping-error-msg').removeClass('hide');
	                    $('#checkoutMsgContainer').addClass('show');
	                    window.scrollTo(500, 10);
	                    $('#checkoutMsgContainer').animate({scrollTop: 0}, '100');
	                    $('#checkoutMsgContainer').find('.error-desc').html(response.errorMessage);
	                    if($('#analyticsSatelliteFlag').val()==='true'){
	                      analytics.analyticsOnPageShippingClick(false,response.errorMessage);
	                    }
						
	                 });
	                cache.$addressAddFlag = true;
	                return false;
	              }
              
	              if(!response.createCartResponseReceived){
	                  	$('#callEstimateFail').modal('show');
	                  	return false;
	              }
	
	              if(response.errorMessage==='ERROR_CODE_3'){
						$('#inventoryFail').modal('show');
						if($('#analyticsSatelliteFlag').val()==='true'){
							  analytics.analyticsOnPageShippingClick(false,'inventoryFail');
						  }
						return false;
	              }
	              if($('#analyticsSatelliteFlag').val()==='true'){
	            	  var $data = {
						formname : 'shippingaddressForm'
					};
				
					// SAVE ADDRESS - SUCCESS
					if($('#saveInAddressBook').length){
						if($('#saveInAddressBook').is(":checked") === true){
							$data.type = 'registration';
							$data.stage = 'adding_new_address_success';
							$data.zipcode = $('#zipcode').val();
							analytics.analyticsAJAXSuccessHandler($data);
						}
					}
				
	              }
                cache.$checkoutStepsContainer.html(shippingaddressDisplayTemplate(response));
                $('#checkoutMsgContainer').addClass('hide');
                cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate());
				
				// review- success
				if($('#analyticsSatelliteFlag').val()==='true'){
				  analytics.analyticsOnPageReviewClick('onload');
				}
				
                if(!response.userloggedin){
                  cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate(response));
                }
                var date = new Date(),
                	year = date.getFullYear(),
                	count,
                	arryear = [];
                arryear.push(year);
                for(count=0;count<10; count++){
                	year = year+1;
                	arryear.push(year);
                }
                response.expyear = arryear;
                cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(response));
                checkoutShipping.spreedlyinit();


                // Checkout Breadcrumb starts
                cache.$checkoutBreadcrumb.find('li:first-child').addClass('checked').removeClass('active').css('cursor', 'pointer');
                cache.$checkoutBreadcrumb.find('li:nth-child(2)').addClass('active');
                cache.$checkoutBreadcrumb.find('li').each(function() {
                  $(this).css('pointer-events','');
                });

                // Breadcrumb click function
                function breadcrumbClicked() {
                  cache.$checkoutBreadcrumb.find('li').each(function() {
                    $(this).removeClass('active checked');
                  });
                  $(this).addClass('active');
                }

                function breadcrumbDisabled() {
                  cache.$checkoutBreadcrumb.find('li').each(function() {
                    $(this).css('pointer-events','none');
                  });
                }

                cache.$checkoutBreadcrumb.on('click','li:first-child', function() {
                  breadcrumbClicked.call($(this));
                  checkoutShipping.editShippingAddress();
                  breadcrumbDisabled.call($(this));
                });

                cache.$checkoutBreadcrumb.on('click','li:nth-child(2)', function() {
                  breadcrumbClicked.call($(this));
                  breadcrumbDisabled.call($(this));
                });
                // Checkout Breadcrumb ends
                
                $('#revieworder').html(checkoutReviewOrderTemplate(response));
                checkoutShipping.getReviewOrderData(response.cartData.shippingPriceCall);
            },

            initCheckoutPage:function(response){
              if(response.cartData.deliveryAddress != null && response.userloggedin){
                     if(response.cartData.deliveryAddress.visibleInAddressBook){
                           cache.$checkoutStepsContainer.html(checkoutShippingTemplate(response));
                     }
                     else{
                           cache.$checkoutStepsContainer.html(shippingForm(response));
                     }
              }
              else{
                cache.$checkoutStepsContainer.html(checkoutShippingTemplate(response));
              }
                cache.$accountInfo.html(checkoutUserDetailsTemplate(response));
                cache.$checkoutStepsContainer.append(checkoutReviewOrderTemplate(response));
                if(!response.userloggedin){
                  cache.$checkoutStepsContainer.append(checkoutSaveInfoTemplate(response));
                }
                cache.$checkoutStepsContainer.append(checkoutPaymentTemplate(response));
                $('.tooltip-init').tooltip();
                
            },
            initCheckoutOrderSummary: function (response) {
                response.isCheckout=true;
                cartcheckoutordesummary.paintordersummary(cache.$orderSummaryContainer, response);
            },

            getReviewOrderData: function(response){
            	$('body').addClass('page-overlay');
                var options = {
                    'methodType': 'GET',
                    'dataType': 'JSON',
                    'methodData': {"selectedShippingMethod": response},
                    'url': $("#cartData").data('estimatecart'),
                    'isRequireLoader': true,
                    'isShowloaderMsg' : true,
                    'cache' : true,
                    
                }
                setTimeout(function(){
                	ajaxFactory.ajaxFactoryInit(options, checkoutShipping.initReviewOrder,'','checkout_revieworder');
                },3)
            },

            initReviewOrder:function(response) {
                if (response.loadReviewPanel) {
                    checkoutReviewOrder.init();
                    checkoutReviewOrder.paintPage(response);
                    $('#checkoutMsgContainer').addClass('hide');
                } else {
                    $(document).ready(function ($) {
                    	$('#callEstimateFail').modal('show');

						          if($('#analyticsSatelliteFlag').val()==='true'){
                        analytics.analyticsOnPageReviewClick(false);
                      }
                    })
                }
                
            },

            saveUserInfo:function () {
                utility.floatingLabelsInit();
                if($('.saveInfoForm').parsley().validate() === false && $('#analyticsSatelliteFlag').val()==='true'){
					analytics.analyticsOnPageSaveInformationClick(false);
                    return false;
                }
                else{
                  var formData = {
                    CSRFToken :$("input[name=CSRFToken]").val(),
                    userPassword :$('#password').val()
                    }
                     var options = {
                             'methodType': 'POST',
                             'dataType': 'JSON',
                             'methodData':formData,
                             'url': '/checkout/multi/summary/save-information',
                             'isShowLoader': false,
                             'cache' : true
                         }
                  ajaxFactory.ajaxFactoryInit(options, function(response){
                      $('#saveInfoTick').removeClass('hide');
                      if($('#analyticsSatelliteFlag').val()==='true'){
						analytics.analyticsOnPageSaveInformationClick(true);
						analytics.analyticsOnPagePaymentClick(true);
                      }
					  $('#panel4').collapse('toggle');
                      $('#payment .panel-title .statement').addClass('hide');
                      $('.placeorderBtn').removeClass('inactive');
                      $('.placeorderBtn').removeAttr("disabled");
                      $('.save-information-container').addClass('hide');
                      $('.saveinforendered').removeClass('hide');
                      $('.saveinforendered').html(response);
                  },'','checkout_saveinformation');
                }
                window.scrollTo(500, 700);
                checkoutShipping.spreedlyinit();
            }
        };
        
        return checkoutShipping;
    });

