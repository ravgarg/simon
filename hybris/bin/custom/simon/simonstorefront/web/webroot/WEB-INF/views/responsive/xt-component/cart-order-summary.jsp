<div class="order-summary-price-container pad-r-mobile pad-l-mobile collapse">
    <div class="price-top-spacing">
        <span class="estimated-subtotal">Estimated Order Subtotal</span>
        <span class="estimated-sub-price">$360.00</span>
    </div>
    <div class="ship-top-spacing">
        <span class="estimated-shipping">Estimated Combined Shipping</span>
        <span class="estimated-shipping-price">--</span>
    </div>
    <div class="choose-method">
        <a herf="#">Choose method in Checkout</a>
    </div>
    <div class="tax-top-spacing">
        <span class="estimated-tax">Estimated Combined Tax</span>
        <span class="estimated-tax-price">--</span>
    </div>
    <div class="tax-calculation">
        <a herf="#">Tax calculated during Checkout</a>
    </div>
    <div class="last-call-spacing">
        <span class="last-call">Estimated Bag Total</span>
        <span class="last-call-price">$360.00</span>
    </div>
    <div class="you-save-spacing">
        <span class="you-save">You Saved</span>
        <span class="you-save-price">$80.00</span>
    </div>
</div>
<div class="btn-border-top">
     <div class="btn-padding"><button class="btn plum">Proceed to Checkout</button></div></div>
<div class="cart-help-container hide-mobile">
   <div class="help-info">Helpful Information</div>
   <div class="information">Please note Simon places orders on your behalf for
            each retailer you shopped at; you will see these as
            separate transactions on your credit card
            statement.
   </div>
   <div class="question">Question?<a href="#">Contact Us</a></div>
   <div class="more-details"><a href="#">See Shipping Methods & Charges for More Details</a></div>
</div>