

<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>



<div class="container">
	<article class="row">
		<div class="col-md-12 hide-mobile"> 
			<jsp:include page="clp-breadcurmb.jsp"></jsp:include>
		</div>

		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="plp-left-nav.jsp"></jsp:include>
		</aside>
		<section class="col-md-9">
			<h1 class="page-heading major"> Trend Shops </h1>
			<jsp:include page="trends-heading-desc.jsp"></jsp:include>
			<div class="xs-row">
				<jsp:include page="trends-listing-new-season.jsp"></jsp:include>
			</div>
			<jsp:include page="trends-listing-filters.jsp"></jsp:include>
			<jsp:include page="plp-tiles-container.jsp"></jsp:include>
			
		</section>
	</article>
</div>

