<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="global-login">
			<div class="row">
				<cms:pageSlot position="csn_LeftCheckoutLoginContentSlot"
					var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<cms:pageSlot position="csn_RightCheckoutLoginContentSlot"
					var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>

			</div>
		</div>
	</div>

</template:page>