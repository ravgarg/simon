<div class="owl-carousel" id="homepageCarousel">
	<div class="item tail-hide the-designer-shop left">
		<a target="_self" href="javascript:void(0);">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/clp-mobile-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/plp-image.png">
			</picture>
			<div class="content-container">
				<h1 class="display-large major">Discover The season</h1>
				<h5 class="designer-banner-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
			</div>
		</a>
		
	</div>

	<div class="item tail-hide the-designer-shop left">
		<a target="_self" href="javascript:void(0);">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/clp-mobile-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/plp-image.png">
			</picture>
			
			<div class="content-container">
				<h1 class="display-large major">Discover The season</h1>
				<h5 class="designer-banner-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
			</div>
		</a>
	</div>
	<div class="item tail-hide the-designer-shop left">
		<a target="_self" href="javascript:void(0);">
			<picture>
				<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/clp-mobile-image@2x.png">
				<img alt="" class="img-responsive" src="/_ui/responsive/simon-theme/images/plp-image.png">
			</picture>
			
			<div class="content-container">
				<h1 class="display-large major">Discover The season</h1>
				<h5 class="designer-banner-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
			</div>
		</a>
	</div>
</div>
