<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<template:page pageTitle="${pageTitle}">
	<div class="vip-club-signin-container analytics-loginPage">
		<div class="container">
			<c:if test="${not empty message}">
				<div class="error-msg analytics-formSubmitFailure"> ${message}
				</div>
			</c:if>
		</div>

			<div class="login-container">
				<cms:pageSlot position="csn_login_SignInSlot" var="comp"
					element="div" class="loginPage">
					<cms:component component="${comp}" element="div"
						class="loginPage-component" />
				</cms:pageSlot>

				<div class="exclusive-container">
					<div class="container">
						<div class="row">
							<div class="col-md-5 col-xs-12">
								<cms:pageSlot position="csn_login_LoginBottomFeatureSlot"
									var="feature">
									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
							<cms:pageSlot position="csn_login_LoginBottomParagraphSlot"
								var="paragraph">
								<cms:component component="${paragraph}" />
							</cms:pageSlot>
						</div>
					</div>
				</div>
			</div>

	</div>
</template:page>