<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<c:set var="imageDestinationLink" value="${backgroundImage.destinationLink}" />
<c:choose>
	<c:when test="${destinationLink.target eq 'NEWWINDOW'}">
    	<c:set var="imageDestinationTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="imageDestinationTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${fontColor eq 'WHITE'}">
    	<c:set var="fontColor" value="active" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${alignment eq 'LEFTALIGNED'}">
    	<c:set var="style" value="left " />
	</c:when>
    <c:otherwise>
		<c:set var="style" value="" />
	</c:otherwise>
</c:choose>

	<div class="the-designer-shop ${style}  ${fontColor} analytics-data" data-analytics='{
		  "event": {
			"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promo_click",
			"sub_type": "${fn:toLowerCase(component.itemtype)}"
		  },
		  "banner": {
			"click": {
			  "id": "${fn:toLowerCase(component.uid)}"
			}
		  },
     "link": {
    "placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promo_click",
    "name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>"
  }
		}'>
		<c:choose>
		<c:when test="${not empty imageDestinationLink}">
		<a target=${imageDestinationTarget} href="${ycommerce:getUrlForCMSLinkComponent(imageDestinationLink)}">
    		<c:choose>
    		<c:when test="${not empty mobileImage or not empty desktopImage}">
			<picture>
				<source media="(max-width: 991px)" srcset="${mobileImage}">
				<img class="img-responsive" src="${desktopImage}" alt="${backgroundImage.imageDescription}">
			</picture>
			</c:when>
			</c:choose>
		</a>
		</c:when>
		<c:otherwise>
		<c:choose>
    		<c:when test="${not empty mobileImage or not empty desktopImage}">
			<picture>
				<source media="(max-width: 991px)" srcset="${mobileImage}">
				<img class="img-responsive" src="${desktopImage}" alt="${backgroundImage.imageDescription}">
				<div class="content-container">
			<h1 class="display-medium major">${titleText}</h1>
			<div class="designer-banner-text">${subheadingText}</div>
			<c:if test="${not empty link && not empty link.linkName}">
				<c:choose>
					<c:when test="${link.target eq 'NEWWINDOW'}">
				    	<c:set var="target" value="_blank" />
					</c:when>
				    <c:otherwise>
						<c:set var="target" value="_self" />
					</c:otherwise>
				</c:choose>	
				<div data-satellitetrack="internal_click"  data-analytics='{"link": {"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|promo1","name": "${fn:toLowerCase(fn:replace(link.linkName," ","_"))}"}'>
				<c:choose>
					<c:when test="${not empty link.url}">
						<a target=${target} href="${link.url}">
						<span class="shopnow major">${link.linkName}&nbsp;<span>&rsaquo;</span></span>
						</a>
					</c:when>
					<c:otherwise>
						<span class="shopnow major">${link.linkName}&nbsp;<span>&rsaquo;</span></span>
					</c:otherwise>
				</c:choose>
				
				</div>
			</c:if>
		</div>
			</picture>
			</c:when>
			</c:choose>
		</c:otherwise>
		</c:choose>
    	
		
	</div>

