<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec"	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="deal" tagdir="/WEB-INF/tags/responsive/deal" %>
<template:page pageTitle="${pageTitle}">
<input type="hidden" data-store-favurl="true" 
		value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />

	<div class="deals-listing">
		<div class="container">
			<article class="row">
				<div class="col-md-12 hide-mobile">
					<div class="row">
						<div class="breadcrumbs">
							<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
							<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
						</div>
					</div>
					<!-- breadcurmb Ends -->
				</div>
				<cms:pageSlot position="csn_leftNavigationStorefrontDealsPage"
					var="feature" element="div" class="account-section-content">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				<section class="col-md-9 myoffers-container">
				
				<div class="fav-the-store add-to-favorites pageHeading">
				<input type="hidden" value="${storeData.id}" class="base-code" /> 
				<input type="hidden" value="${storeData.name}" class="base-name" />
				<input type="hidden" value="RETAILERS" class="base-type" /> 
				<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
					<h1 class="page-heading major"><a href="javascript:void(0);" 
						data-fav-product-code="${storeData.id}"
						data-fav-name="${storeData.name}" data-fav-type="RETAILERS" 
						class="icon-fav-blank fav-icon login-modal openLoginModal analytics-loginFlyOut" 
						data-analytics='{"event":{"sub_type":"login_initiation","type":"login"},"login": {"stage": "login_start"}}' 
						data-satellitetrack="login">${storeData.name}
					</a></h1>
				</sec:authorize>
				<input type="hidden" value="${storeData.id}" class="base-code" /> 
				<input type="hidden" value="${storeData.name}" class="base-name" />
				<input type="hidden" value="RETAILERS" class="base-type" /> 
				<input type="hidden" value="${storeData.id}" class="base-product-code analytics-base-code" name="${storeData.id}" /> 
				<input type="hidden" value="${storeData.name}" class="base-product-name analytics-base-name" />
				<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
					<a href="javascript:void(0);" title="" class="pageHeadingfav icon-fav-blank mark-favorite fav-icon <c:if test='${storeData.favorite}'>marked</c:if> "></a>
					<h1 class="page-heading major" title="">${storeData.name}</h1>
				</sec:authorize>
				</div>
				
					<cms:pageSlot position="csn_pageDescriptionStorefrontDealsPage"
						var="feature" element="div">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					<c:if test="${not empty storeData.heroBanner}">
						<div class="plp-top-banner">
							<jsp:include page="../../cms/herobanner.jsp"></jsp:include>
						</div>
					</c:if>
					 <deal:dealtilelisting dealsDataList="${storeData.deals}" /> 
				</section>
			</article>
		</div>
	</div>
</template:page>




