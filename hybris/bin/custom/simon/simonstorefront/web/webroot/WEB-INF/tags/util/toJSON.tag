<%@tag import="org.slf4j.LoggerFactory"%>
<%@tag import="org.codehaus.jackson.map.ObjectMapper"%>
<%@ tag trimDirectiveWhitespaces="true" display-name="serialize an object to JSON" pageEncoding="UTF-8" language="java" %>
<%@ attribute name="source" required="true" type="java.lang.Object"%>
<%@ attribute name="catchException" type="java.lang.Boolean"%>
<%
    String result = null;
    try {
        ObjectMapper mapper = new ObjectMapper();
        result = mapper.writeValueAsString(source);
    }catch(Exception e){
        if( ! Boolean.FALSE.equals(catchException)){
            LoggerFactory.getLogger("toJson.tag").error("can't convert to json", e);
        } else {
            throw e;
        }
    }
    if(result != null){
        jspContext.getOut().append(result);
    }
%>