<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<input type="hidden" id="dealsList" value="${dealsList}"/>
	<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/add-deal", "remove":"/my-account/remove-deal" }' />
		<c:choose>
			<c:when test="${not empty dealsList}">
				<c:set var="dealsList" value="${dealsList}" scope="request"/>
				<div class="analytics-favoriteDeals" data-analytics-length="deals">
					<jsp:include page="displayMyDeals.jsp"></jsp:include>
				</div>
				<div class="btn-category">
					<button class="btn black btn-width major callDesignerList analytics-genericLink" data-analytics-eventsubtype="view-deal" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|view-deal" data-analytics-linkname="view-deal" OnClick=" location.href='<c:url value = "/deals"/>' ">
						<spring:theme code="${viewDeals}" />
				</button>
				</div>
			</c:when>
			<c:otherwise>
			<div class="analytics-favoriteDeals" data-analytics-length="nodeals">
				<h4>${userName},
					<spring:theme code="${noSavedDealTitle}" />
				</h4>
				<p>
					<spring:theme code="${noSavedDealData}" />
				</p>
				<button class="btn black btn-width major callDesignerList" OnClick=" location.href='<c:url value = "/deals"/>' ">
						<spring:theme code="${viewDeals}" />
				</button>
			</div>
			</c:otherwise>
		</c:choose>
				
