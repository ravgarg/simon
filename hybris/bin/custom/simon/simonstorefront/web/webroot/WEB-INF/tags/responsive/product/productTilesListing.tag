<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
 <%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
 <%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
		<c:set var="facetValue" value="" />
		<c:forEach items="${searchPageData.breadcrumbs}" var="filterFacet" varStatus="status">
			<c:if test="${not empty filterFacet}"><c:set var="facetValue" value = "${facetValue}${fn:toLowerCase(filterFacet.facetName)}:${fn:toLowerCase(filterFacet.facetValueName)}"/></c:if>
			<c:if test="${not status.last}"><c:set var="facetValue" value = "${facetValue}|"/></c:if>
		</c:forEach>
		<div class="row product-container product-list analytics-productList analytics-plpList" 
			data-analytics-eventtype="product_click" 
			data-analytics-eventsubtype='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>' 
			<c:if test="${not empty searchPageData.freeTextSearch}"> 
				data-analytics-ecommerce-searchterm="${fn:toLowerCase(searchPageData.freeTextSearch)}" 
				data-analytics-ecommerce-searchtype="full_query_search"
			</c:if> 
			data-analytics-listype='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>' 
			data-analytics-listname='<c:choose><c:when test="${not empty categoryName}">${fn:toLowerCase(categoryName)}</c:when><c:otherwise>${fn:toLowerCase(searchPageData.freeTextSearch)}</c:otherwise></c:choose>' 
			data-analytics-ecommerce-action="prod_impression"
			data-analytics-ecommerce-clickaction="prod_click"
			data-analytics-filtername='<c:if test="${not empty facetValue}">${fn:escapeXml(facetValue)}</c:if>' 
			data-analytics-sortname='<c:forEach items="${searchPageData.sorts}" var="sort"><c:if test="${not empty sort && sort.selected}">${fn:toLowerCase(sort.name)}</c:if></c:forEach>' 
			data-analytics-linkplacement='<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>' 
			data-satellitetrack="product_listing">	
			    <c:forEach items="${searchPageData.results}" var="product" varStatus="status">  
					 <c:if test="${fn:length(product.variantOptions)>0}">
			    	 	<c:set var="InStockProductIdentified" value="false"/>
			    	 	<c:set var="variantProduct" value="${product.variantOptions.get(0)}"/>
			    	 	 <c:set var="altText" value="${variantProduct.baseProductDesignerName}&nbsp;${variantProduct.colorName}&nbsp;${variantProduct.baseProductName}"/> 
										
					   	<c:if test="${InStockProductIdentified eq false}">	
					   		<c:set var="InStockProductIdentified" value="${variantProduct.inStockFlag}"/>	
					   		 <c:if test="${status.count == 7}">
					   		 	<c:choose>
					   		 		<c:when test="${(cmsPage.uid eq 'productList') or (cmsPage.uid eq 'designerListPage')}">
					   		 			 <product:promoinlinebanner component="${promoInLineBanner}" />
					   		 		</c:when>
					   		 		<c:otherwise>
					   		 			<cms:pageSlot position="csn_promoInLineSlot" var="feature">
							     			 <c:if test="${not empty feature}">
												<cms:component component="${feature}" element="div" class="item-tile item-responsive col-md-4 col-xs-6" />
											</c:if>
								 		</cms:pageSlot>
					   		 		</c:otherwise>
					   		 	</c:choose>
							</c:if>
					</c:if>
				<div
					class="item-tile item-responsive col-md-4 col-xs-6" data-satellitetrack="product_listing" data-analytics-productposition="${status.count}" data-analytics-productoutofstock='<c:choose><c:when test="${InStockProductIdentified eq false}">true</c:when><c:otherwise>false</c:otherwise></c:choose>' data-analytics-linkname="${fn:escapeXml(variantProduct.baseProductName)}">
					           	 <div class="item">
					           	 	<input type="hidden" value="${variantProduct.baseProductCode}" class="base-product-code" />
							   		<input type="hidden" value="${variantProduct.baseProductName}" class="base-product-name" />
									<c:if test="${not empty categoryName && pageType eq 'CATEGORY'}">
										<input type="hidden" value="${categoryName}" class="base-product-category" />
									</c:if> 
							   		<!-- Fav-icon For anonymous user -->
									<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
										<a href="javascript:void(0);" data-fav-product-code="${variantProduct.baseProductCode}"
											  data-fav-name="${variantProduct.baseProductName}" data-fav-type="PRODUCTS"
											class="fav-icon login-modal openLoginModal">
											<span class="hide"> fav</span>
											</a> 
									</sec:authorize>
									<!-- Fav-icon For logged in user -->
									<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
									<div class="analytics-addtoFavorite" data-analytics-pagestyle="<c:if test="${not empty pageStyleId}">${pageStyleId}</c:if>" data-analytics-pagetype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|product" data-analytics-component="product">
										<a href="javascript:void(0);" data-fav-product-code="${variantProduct.baseProductCode}"
											  data-fav-name="${variantProduct.baseProductName}" data-fav-type="PRODUCTS" class="mark-favorite fav-icon <c:if test='${product.favorite}'>marked</c:if>">
		                                    <em  class="tooltip-init" 
	                                            data-placement = "bottom" 
	                                            data-html="true" 
	                                            data-toggle = "tooltip"
	                                            title = "<spring:theme code="text.pdp.addedtofavorite" />"></em>
	                                     </a>
									</div>
									</sec:authorize>
							    	 <a class="analytics-colorattribute analytics-item" data-satellitetrack="product_listing" data-analytics-productposition="${status.count}" data-analytics-productoutofstock='<c:choose><c:when test="${InStockProductIdentified eq false}">true</c:when><c:otherwise>false</c:otherwise></c:choose>' data-analytics-linkname="${fn:escapeXml(variantProduct.baseProductName)}" data-analytics-colorattribute="${variantProduct.colorID}" href="<c:url value='${variantProduct.baseProductUrl}?colorID=${variantProduct.colorID}'/>">
							    	 
							    	 
							    	 
							    	 <div class="imgContainer"> 
							    	 
							    	 
							    	 
							    	 
							        <picture>
							        <c:choose>
							        	<c:when test="${fn:length(variantProduct.variantImage) > 0}">
							        	
							        		<c:forEach var="media" items="${variantProduct.variantImage}">
		                                        <c:choose>
		                                            <c:when test="${media.format eq 'Desktop'}">
			                                            <c:choose>
			                                            	<c:when test="${not empty media.url}">
			                                            			<c:set var="desktopImageUrl" value="${media.url}"/>
			                                            	</c:when>
			                                            	<c:otherwise>
			                                            			<c:set var="desktopImageUrl" value="/_ui/responsive/simon-theme/images/no-image-200x305.jpg"/>
			                                            	</c:otherwise>
			                                            </c:choose>
		                                              </c:when>
		                                              <c:otherwise>
		                                              		<c:if test="${ empty media.format}">
		                                              				<c:set var="desktopImageUrl" value="/_ui/responsive/simon-theme/images/no-image-200x305.jpg"/>
		                                              		</c:if>
		                                               		
		                                               </c:otherwise>
		                                        </c:choose>
		                                         <c:choose>
		                                                 <c:when test="${media.format eq 'Mobile'}">
		                                                 	<c:choose>
		                                                 		<c:when test="${not empty media.url}">
		                                                 			<c:set var="mobileImageUrl" value="${media.url}"/>
		                                                 		</c:when>
		                                                	 	<c:otherwise>
		                                                 			<c:set var="mobileImageUrl" value="/_ui/responsive/simon-theme/images/no-image-200x305.jpg"/>
		                                                 		</c:otherwise>
		                                                 	</c:choose>
		                                               </c:when>
		                                              <c:otherwise>
		                                              		<c:if test="${ empty media.format}">
		                                              				<c:set var="mobileImageUrl" value="/_ui/responsive/simon-theme/images/no-image-200x305.jpg"/>
		                                              		</c:if>
		                                               		
		                                               </c:otherwise>
		                                        </c:choose>
		                                    </c:forEach> 
							           		 <!--[if IE 9]><video class="hide img-responsive"><![endif]-->
							            	<source media="(max-width: 767px)" srcset="${mobileImageUrl}">
							            	<source media="(min-width: 768px)" srcset="${desktopImageUrl}">
							            	<!--[if IE 9]></video><![endif]-->
							            	<img class="img-responsive"  
							            	alt="${altText}"
							            	  style="" src="${desktopImageUrl}" >
							       		</c:when>
							       		<c:otherwise>
							       		<source media="(max-width: 767px)" srcset="/_ui/responsive/simon-theme/images/no-image-200x305.jpg">
							            	<source media="(min-width: 768px)" srcset="/_ui/responsive/simon-theme/images/no-image-200x305.jpg">
							            	<!--[if IE 9]></video><![endif]-->
							            	<img class="img-responsive" 
							            	alt="${altText}"
							            	style="" src="/_ui/responsive/simon-theme/images/no-image-200x305.jpg" >
							       		</c:otherwise>
							       		</c:choose>
							        </picture>
							        
							        </div>
							        
							        
							        
							        
							        
							        
							    </a>
								<c:if test="${ not empty product.potentialPromotions && not empty product.potentialPromotions.get(0).description}">
										<div class="itemLevelPromotion"> ${product.potentialPromotions.get(0).description}</div>
								</c:if>
								<c:choose>
								<c:when test="${not empty variantProduct.baseProductDesignerName  && not empty variantProduct.baseProductRetailerName && (fn:toUpperCase(fn:trim(variantProduct.baseProductDesignerName))  eq fn:toUpperCase(fn:trim(variantProduct.baseProductRetailerName))) }">
										<c:choose>
											<c:when test="${not empty variantProduct.baseProductRetailerURL}">
											 <div class="item-name gray-color analytics-retailer" data-satellitetrack="product_listing" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}"><a href="${variantProduct.baseProductRetailerURL}">${variantProduct.baseProductRetailerName}</a></div>
											</c:when>
											<c:otherwise>
											 <div class="item-name gray-color analytics-retailer" data-satellitetrack="product_listing" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}">${variantProduct.baseProductRetailerName}</div>
											</c:otherwise>
										</c:choose>
										
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${not empty variantProduct.baseProductDesignerURL}">
										<div class="item-name gray-color  analytics-designer" data-satellitetrack="product_listing" data-analytics-designer="${fn:toLowerCase(variantProduct.baseProductDesignerName)}"><a href="${variantProduct.baseProductDesignerURL}">${variantProduct.baseProductDesignerName}</a></div>
										</c:when>
										<c:otherwise>
										<div class="item-name gray-color  analytics-designer" data-satellitetrack="product_listing" data-analytics-designer="${fn:toLowerCase(variantProduct.baseProductDesignerName)}">${variantProduct.baseProductDesignerName}</div>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${not empty variantProduct.baseProductRetailerURL}">
									 	 <div class="item-name gray-color analytics-retailer" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}"><a href="${variantProduct.baseProductRetailerURL}">${variantProduct.baseProductRetailerName}</a></div>
										</c:when>
										<c:otherwise>
										<div class="item-name gray-color analytics-retailer" data-analytics-retailer="${fn:toLowerCase(variantProduct.baseProductRetailerName)}">${variantProduct.baseProductRetailerName}</div>
										</c:otherwise>
									</c:choose>
									  
								</c:otherwise>
								</c:choose>
							  	<c:choose>
									<c:when test="${fn:length(product.variantOptions) > 0}">
										<c:set var="colorCount" value="${fn:length(product.variantOptions)}"/>
									</c:when>
									<c:otherwise>
										<c:set var="colorCount" value=""/>
									</c:otherwise>	
								</c:choose>
							    <div class="item-retailer"><a href="">${colorCount} Color</a></div>   
							    <!-- Color Owl Carousal Starts -->
							    <div class="color-switches-carousal analytics-plpcolorSwatches" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-satellitetrack="product_listing">
							    	<c:forEach items="${product.variantOptions}" var="variant" varStatus="status">  
							    		<div class="item">  
							            	<div class="color-palette clearfix">
							            	<c:choose>
							            		<c:when test="${variant.inStockFlag}">
							            			<c:choose>
							            				<c:when test="${not empty variant.swatchImage}">
							            					<a  href="<c:url value='${variant.baseProductUrl}?colorID=${variant.colorID}'/>" class="color-switches" data-color-name="${variant.colorName}" style="background: url('${variant.swatchImage}');"><span class="hide"> fav</span></a>
							            				</c:when>
							            				<c:otherwise>
							            					<a  href="<c:url value='${variant.baseProductUrl}?colorID=${variant.colorID}'/>" class="color-switches" data-color-name="${variant.colorName}" style="background: url('/_ui/responsive/simon-theme/images/question-mark.jpg');background-size: cover;"><span class="hide"> fav</span></a>
							            				</c:otherwise>
							            			</c:choose>
							            		</c:when>
							            		<c:otherwise>
							            			<a  href="<c:url value='${variant.baseProductUrl}?colorID=${variant.colorID}'/>" class="color-switches not-available" data-color-name="${variant.colorName}" style="background: url('${variant.swatchImage}');"><span class="hide"> fav</span></a>
							            		</c:otherwise>
							            	</c:choose>
							               	 
							          	  			
							          	  </div>
							        	</div>
							    	</c:forEach>
							    </div>
							    <!-- Color Owl Carousal Ends -->  
							    <div class="item-color"><a class="analytics-genericLink" data-analytics-eventtype="brand_link" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|brand_link" data-analytics-linkname="${variantProduct.baseProductName}"  data-satellitetrack="link_track" href="<c:url value='${variantProduct.baseProductUrl}?colorID=${variantProduct.colorID}'/>">${variantProduct.baseProductName}</a></div>
							    <c:if test="${ not empty product.applicablePriceRange}">
								    	<div class="item-discrition">${product.applicablePriceRange}  </div>
			 					</c:if>
								<c:if test="${ not empty product.plumPriceRange}">
								    		<div class="item-price">
								    			<span>${product.plumPriceRange}</span>
								    			 <c:if test="${not  empty variantProduct.variantPrice['MSRP'].left and not  empty variantProduct.variantPrice['LIST'].left and not  empty variantProduct.variantPrice['SALE'].left}">
								    			<a href="javascript:void(0);" 
								    				data-placement="top" 
								    				data-html="true" 
								    				data-toggle="tooltip" 
													data-original-title="${product.msrpPriceRange} </br>${product.listPriceRange} </br>${product.salePriceRange}" 
								    				class="tooltip-init tooltip-icon"></a>	
								    			</c:if>
											</div>
								</c:if>			 
								<c:if test="${ not empty product.percentageOffRange}">
								    		<div class="item-revised-price">  ${product.percentageOffRange}</div>
								</c:if>
							</div>
					    </div>
			    	</c:if>
			    	
			    </c:forEach>				
			</div>	