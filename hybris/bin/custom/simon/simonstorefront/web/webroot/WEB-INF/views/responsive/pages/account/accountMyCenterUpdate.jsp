<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url value="/my-account/update-profile" var="updateMyCenterUrl" />
<form:form action="${updateMyCenterUrl}" method="post" commandName="updateProfileForm" id="UpdateMyCenterForm" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-analytics-type="registration" data-analytics-eventtype="premium_outlet_center" data-analytics-eventsubtype="my_account" data-analytics-formtype="premium_outlet_center" data-analytics-formname="premium_outlet_center" data-satellitetrack="registration" data-parsley-validate="validate">	
<div class="panel panel-default next-section">
	<h5 class="panel-title">
		<a class="accordion-toggle major" data-toggle="collapse"
			data-parent="#accordion" href="#panel2" aria-expanded="true"><spring:theme code="text.account.profile.myaccount.mycenter" /></a>
	</h5>
	<div id="panel2" class="panel-collapse collapse in"
		aria-expanded="true">
		<div class="collapsed-content">
			<div class="row">
				<div class="col-md-12">
					<p class="instruction-msg line1"><spring:theme code="text.account.profile.myaccount.mycenter.heading" /></p>
				</div>
								<div class="col-md-12 list-dropdown-my-profile">
									<div class="sm-input-group">
						<div class="primary-centers"><spring:theme code="text.account.profile.myaccount.myprofile.primary.center" /></div>
						<label class="hide" for="selectPrimaryMall">Primary Mall</label>
						<select class="form-control" name="primaryMall" id="selectPrimaryMall">
						<c:forEach items="${mallList}" var="mall">
							<c:choose>
							
								<c:when test="${mall.code eq customerData.primaryMall.code}">
									<option value="${mall.code}" selected>${mall.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${mall.code}">${mall.name}</option>
								</c:otherwise>
								
							</c:choose>
							
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="col-md-12 select-box dropdown dropdown-accordion centersDropdown" 
				data-accordion="#accordion">
				<div class="several-centers"><spring:theme code="text.account.profile.myaccount.myprofile.primary.center.select" /></div>
					<button class="dropdown-toggle centers" type="button"
						data-toggle="dropdown">
						<spring:theme code="text.account.profile.myaccount.myprofile.primary.center.selectacenter" />&nbsp;(<span class="centerCount">${fn:length(customerAlternateMallList)}</span>)
						<span class="caret-icon"></span>
					</button>
					<ul class="dropdown-menu" id="drpCenters" role="menu" aria-labelledby="dLabel">
						<li>
							<c:set var="customerMallCount" value="0"/>
							<c:forEach items="${mallList}" var="mall">
									<div class="item-padding" id="${mall.code}">
									<c:set var="mallCode" value="${mall.code}"/>
										<span class="line-item"> 
											<a href="#" class="custom-checkbox" data-value="${mall.code}" tabIndex="-1"> 				
													<c:set var="mallChecked" value="false" />
													<c:forTokens items="${customerAlternateMallList}" delims="," var="mCode"> 
														<c:if test="${fn:contains(mCode, mallCode)}">
															<c:set var="mallChecked" value="true" />
															<c:set var="customerMallCount" value="${customerMallCount+1 }"/>
														</c:if>
													</c:forTokens>
														<label class="hide" for="${mall.code}-id">${mall.name} </label>
														<c:choose>
														<c:when test="${mallChecked eq true}">
															<input type="checkbox" checked value="${mall.code}" id="${mall.code}-id"
																class="sr-only" />
														</c:when>
														<c:otherwise>
															<input type="checkbox" value="${mall.code}" class="sr-only" id="${mall.code}-id" />
														</c:otherwise>
														</c:choose>
												
												<em class="i-checkbox"></em>${mall.name}
											</a>
										</span>
									</div>
								</c:forEach>
							</li>
					</ul>	
									
				</div>
			</div>
			<input type="hidden" name="updateCenter" value="true"/>
			<input type="hidden" name="alternateMallIds" id="alternateMallIds" value=""/>
			
			<div class="clearfix row">
				<div class="update-profile col-md-12 small-text update-msg"><spring:theme code="text.account.profile.myaccount.save.message"/></div>
				<div class="submit-btn col-md-2 col-xs-12">
					<button class="btn" type="submit" id="updateMyCenter"><spring:theme code="register.new.customer.button.update" /></button>
				</div>
				<div class="terms-conditions col-md-10 col-xs-12 small-text">
				
				    <div class="faqs"><a href="<spring:theme code="text.account.profile.myaccount.vip.faq.href" />" class="link"><spring:theme code="text.account.profile.myaccount.vip.faq" /></a> </div>
					<div class="security"><spring:theme code="text.account.profile.myaccount.ssl"/></div>
				</div>
			</div>
		</div>
	</div>
</div>
</form:form>