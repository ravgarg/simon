<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<c:set var="imageDestinationLink" value="${backgroundImage.destinationLink}" />
<c:choose>
	<c:when test="${imageDestinationLink.target eq 'NEWWINDOW'}">
    	<c:set var="imageDestinationTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="imageDestinationTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${titleText.target eq 'NEWWINDOW'}">
    	<c:set var="titleTextTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="titleTextTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${subheadingText.target eq 'NEWWINDOW'}">
    	<c:set var="subheadingTextTarget" value="_blank" />
	</c:when>
    <c:otherwise>
		<c:set var="subheadingTextTarget" value="_self" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${fontColor eq 'WHITE'}">
    	<c:set var="fontColor" value="white-text" />
	</c:when>
    <c:otherwise>
		<c:set var="fontColor" value="" />
	</c:otherwise>
</c:choose>

<c:set var="desktopImage" value="${backgroundImage.desktopImage.url}" />
<c:choose>
	<c:when test="${not empty backgroundImage.mobileImage}">
    	<c:set var="mobileImage" value="${backgroundImage.mobileImage.url}" />
	</c:when>
    <c:otherwise>
		<c:set var="mobileImage" value="${desktopImage}" />
	</c:otherwise>
</c:choose>

<div class="trending-items col-xs-12 col-md-4 analytics-data ${fontColor}" data-analytics='{
		  "event": {
			"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|${fn:toLowerCase(component.itemtype)}",
			"sub_type": "${fn:toLowerCase(component.itemtype)}"
		  },
		  "banner": {
			"click": {
			  "id": "${fn:toLowerCase(component.uid)}"
			}
		  },"link": {
			"placement": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|trending_now",
			"name": "<c:choose>
				<c:when test="${not empty component.name}">${fn:toLowerCase(fn:replace(component.name," ","_"))}</c:when><c:otherwise>${fn:toLowerCase(component.uid)}</c:otherwise></c:choose>"
		  }
		}'>
	<c:choose>
		<c:when test="${not empty imageDestinationLink}">
			<a target="${imageDestinationTarget}" href="${ycommerce:getUrlForCMSLinkComponent(imageDestinationLink)}">
				<img class="img-responsive img-hover-effect" src="${desktopImage}" alt="${backgroundImage.imageDescription}">
			</a>
		</c:when>
		<c:otherwise>
			<img class="img-responsive img-hover-effect" src="${desktopImage}" alt="${backgroundImage.imageDescription}">
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${not empty titleText.url}">
		<a target="${titleTextTarget}" href="${titleText.url}">
		<h4 class="major">${titleText.linkName}</h4>
		</a>
		</c:when>
		<c:otherwise>
		<h4 class="major">${titleText.linkName}</h4>
		</c:otherwise>
	</c:choose>
	
	
	<c:choose>
		<c:when test="${not empty subheadingText.url}">
		<a target="${subheadingTextTarget}" href="${subheadingText.url}">
		<h6>${subheadingText.linkName}</h6>
		</a>
		</c:when>
		<c:otherwise>
		<h6>${subheadingText.linkName}</h6>
		</c:otherwise>
	</c:choose>
	
	
</div>