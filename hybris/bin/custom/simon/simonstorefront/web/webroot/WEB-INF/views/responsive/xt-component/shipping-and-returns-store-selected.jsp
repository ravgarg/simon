<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="trends-landing-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="trends-landing-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<h1 class="page-heading major"> Shipping, Returns & Privacy Policy </h1>
			<jsp:include page="shipping-returns-heading-desc.jsp"></jsp:include>			
			<div class="shipping-and-returns">								
				
				<form>
					<div class="selected-store-container">					
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<div class="sm-input-group select-menu">
									<label class="field-label">
										Select a store
										<span class="field-error" id=""></span>
									</label>
									<select class="form-control" name="store" id="">
										<option>Select a store</option>
										<option>store 1</option>
										<option>store 2</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="shipping-detail">
									<h6>Nordstrom Rack Shipping</h6>
									<p>Your order will be shipped to you directly by Nordstrom Rack</p>
									
									<h6>No Rush Shipping</h6>
									<p class="shipping-copy">Lorem ipsum dolor sit amet</p>
									
									<h6>Premium Shipping</h6>
									<p class="shipping-copy">Lorem ipsum dolor sit amet</p>
									
									<div class="shipping-msg">Shipping prices available during checkout</div>
									
								</div>
								<div class="return-policy">
									<h6>Returns Policy</h6>
									<p>You will be able to return your item directly to {Retailer Name}.</p>
									Return Period: 30 Days <br/>
									Cost: $9.50 <br/>
									Method: UPS or FedEx <br/>
								</div>
								<div class="privacy-policy">
									<h6>Privacy Policy</h6>
									<p>Lorem ipsum dolor sit amet, scripta minimum an quo, ea has purto erant homero, et duo agam consequat. Eu vide regione albucius usu, noster dicunt est in. Illum definiebas instructior at nam. Id nam erant scaevola democritum.</p>
									<p>Idque option torquatos usu cu, mei in voluptua elaboraret. Eu atqui dicant adversarium mea, cu scripta fierent sea, mel cu altera expetenda.</p>
								</div>
							</div>
						</div>																	
					</div>
				</form>
						
			</div>			
		</div>
	</div>
</div>
