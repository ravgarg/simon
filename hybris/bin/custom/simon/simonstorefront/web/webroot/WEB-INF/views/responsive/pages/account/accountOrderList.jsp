<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<c:forEach items="${searchPageData.results}" var="order">
	<div class="row order-detail-row">
		<div class="col-md-3 col-sm-12 order-detail-com">
			<div class="col-xt-12 date-bold hidden-sm visible-xs"><spring:theme code="order.history.order.date.text" /></div>
			<div class="date-detail" data-analytics-orderdate='<fmt:formatDate value="${order.placed}" dateStyle="long" type="date" />'>
				<fmt:formatDate value="${order.placed}" dateStyle="long" type="date" />
			</div>
		</div>
		<div class="col-md-4 col-sm-12 order-detail-com">
			<div class="col-xt-12 date-bold hidden-sm visible-xs"><spring:theme code="order.history.retailers.text" /></div>
			<div>
				<c:forEach var="retailer" items="${order.retailers}" varStatus="loop">
					<span class="retailer-name" data-analytics-retailername="${retailer}">${retailer}</span>
					<c:if test="${!loop.last}">, </c:if>
				</c:forEach>
			</div>
		</div>
		<div class="col-md-2 col-sm-12 order-detail-com">
			<div class="col-xt-12 date-bold hidden-sm visible-xs"><spring:theme code="order.history.total.price.text" /></div>
			<div class="price" data-analytics-orderprice="${order.total.value}">${order.total.formattedValue}</div>
		</div>
		<div class="col-md-3 col-sm-12 order-detail-com myorder-tooltip padding-left-15">
			<div class="col-xt-12 date-bold hidden-sm visible-xs"><spring:theme code="order.history.order.details.text" /></div>
			<div class="underline order-number">
				<a href="#" class="orderDetails" data-analytics-ordercode="${order.code}">${order.code}</a>
			</div>
		</div>
	</div>
</c:forEach>
<jsp:include page="orderDetailsPopUp.jsp" />