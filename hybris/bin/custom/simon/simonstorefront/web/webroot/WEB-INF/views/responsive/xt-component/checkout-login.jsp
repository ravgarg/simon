<div class="container">
	<div class="global-login">
		<div class="row">
			<div class="col-xs-12 col-sm-5 col-sm-push-6 global-login-containers">
				<h3>New or Guest Customers</h3>
				<p>During Checkout you can create an account to track your orders, save items to your favorites list and more.</p>
				<button class="btn guest-btn">checkout as a guest</button>
			</div>
			<div class="col-xs-12 col-sm-5 col-sm-pull-5 global-login-containers">
				<h3>Returning Customer</h3>
				<form>
					<div class="sm-input-group">
						<input type="text" class="form-control" name="emailaddress" placeholder="EMAIl ADDRESS" title="EMAIl ADDRESS">
					</div>
					<div class="sm-input-group">
						<input type="text" class="form-control" name="password" placeholder="PASSWORD" title="PASSWORD">
					</div>

					<div class="clearfix links-container">
						<label class="custom-checkbox">
							<input type="checkbox" name="confirm" class="sr-only"/>
							<i class="i-checkbox"></i>                    
						</label>
                    
						<span class="stay-loggedin">Stay Logged In</span>
						<a href="#" class="forgot-password pull-right">Forgot Password</a>
					</div>
					<button class="btn plum">Log in & checkout</button>
				</form>
			</div>
			
		</div>
	</div>
</div>