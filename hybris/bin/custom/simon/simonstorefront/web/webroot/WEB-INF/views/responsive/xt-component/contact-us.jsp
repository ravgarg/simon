<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

	<div class="container">
	<div class="row">
		<div class="col-md-12 hide-mobile">
			<jsp:include page="trends-landing-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="trends-landing-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<h1 class="page-heading major"> Contact Us </h1>
			<jsp:include page="contact-us-heading-desc.jsp"></jsp:include>			
			<div class="contact-us-container">
				<div class="row address-container">
					<div class="col-xs-12 col-md-4">
						<h6>Corporate Headquarters</h6>
						<address>
							<p>Simon Property Group, Inc. <br/>
							225 West Washington Street <br/>
							Indianapolis, Indiana 46204</p>
						<address>
					</div>
					<div class="col-xs-12 col-md-4">
						<h6>General Information</h6>
						<p>							
							+1 (317) 636-1600							
						<p>
					</div>
					<div class="col-xs-12 col-md-4">
						<h6>Learn More</h6>
						<p>
							<a href="#" title="About Simon">About Simon</a> <br/>
							<a href="#" title="View Map">View Map</a>
						</p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 btn-contact">
						<p>Any questions? We�re here to help.</p>
						<a class="btn" title="Contact Us">Contact Us</a>
					</div>
				</div>
				
			</div>			
		</div>
	</div>
	</div>	
