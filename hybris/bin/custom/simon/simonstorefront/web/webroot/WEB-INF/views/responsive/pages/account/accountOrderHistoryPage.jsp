<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination"%>

<spring:url value="/my-account/order/" var="orderDetailsUrl" />

<div class="myorder-details-container row">
	<input type="hidden" id="orderDetailPageCount" value="${searchPageData.pagination.numberOfPages}"  />
	<div class="col-md-12">
		<div class="myorder-details analytics-myOrders">
			<div class="row hidden-xs order-detail-col">
				<div class="col-md-3 col-sm-12 date-bold"><spring:theme code="order.history.order.date.text" /></div>
				<div class="col-md-4 col-sm-12 date-bold"><spring:theme code="order.history.retailers.text" /></div>
				<div class="col-md-2 col-sm-12 date-bold"><spring:theme code="order.history.total.price.text" /></div>
				<div class="col-md-3 col-sm-12 date-bold myorder-tooltip padding-left-15">
					<spring:theme code="order.history.order.details.text" />
					<img width="12" src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo trackorder-tooltip" data-toggle="tooltip" title='Simon Premium Outlets Order Number' alt="" data-original-title="">
				</div>
			</div>			
			<jsp:include page="accountOrderList.jsp" />			
		</div>
		<div class="row">
			<div class="submit-btn col-md-offset-3 col-md-9 col-xs-12">
				<button class="btn secondary-btn black load-more" data-url="/my-account/get-orders">
					<spring:theme code="order.history.load.more.button.text" />
				</button>
			</div>
		</div>
	</div>
</div>

