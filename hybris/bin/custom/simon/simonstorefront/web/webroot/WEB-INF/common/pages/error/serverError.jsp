<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Server Error</title>
    </head>
    <body>
		<c:redirect url="/500ErrorPage"/>
    </body>
</html>
