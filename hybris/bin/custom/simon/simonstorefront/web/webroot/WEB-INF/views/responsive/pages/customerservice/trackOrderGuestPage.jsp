<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	    <div class="row">
	    	<div class="order-main-container">	    		
	    		<!-- first form -->
	    		<div class="track-order-form">
					<div class="col-md-6 col-xs-12">
			       		<p><spring:theme code="track.order.form.text" /></p>
			       	</div>

			       	<div class="col-md-6 col-xs-12 track-order-section analytics-trackOrder">
			       		<form:form method="post" action="/customer-service/order-details" id="orderTrack" commandName="trackOrderForm" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-analytics-type="order_tracking" data-analytics-eventtype="order_tracking" data-analytics-eventsubtype="order_tracking" data-analytics-formtype="order_tracking" data-analytics-formname="order_tracking" data-satellitetrack="order_tracking" data-parsley-validate="validate">

							<div class="sm-input-group">
								<label class="field-label" for="trackNumber"><spring:theme code="order.details.retailer.order.number.text" /><span class="field-error" id="error-orderNumber"></span></label>
								<input path="orderId" id="trackNumber" class="form-control reqiured" name="ordernumber" placeholder="<spring:theme code="text.track.order.order.number.place.holder" />" 
								data-parsley-errors-container="#error-orderNumber" 
								data-parsley-error-message="<spring:theme code="text.track.order.order.number.error.msg" />" required />
							</div>


							<div class="sm-input-group">
								<label class="field-label"><spring:theme code="text.account.profile.myaccount.email" /><span class="field-error" id="error-j_guestEmailId"></span></label>
								<input path="emailId"  class="form-control reqiured" name="email" placeholder="<spring:theme code="text.account.profile.myaccount.email" />" title="EMAIL ADDRESS" id="email" 
								data-parsley-errors-container="#error-j_guestEmailId"
								type="email" 
								data-parsley-type="email" 
								data-parsley-error-message="<spring:theme code="register.new.customer.email.error" />" required />
							</div>

							<div class="submit-btn col-md-12 col-xs-12">
					       		<button class="btn"><spring:theme code="track.order.form.check.my.order.btn" /></button>
					       	</div>
						</form:form>
						</div>
		       	</div>
				
				<!-- Result Display Table -->
				<div id="trackOrderResult"></div>
				
				
				<!-- Result Buttons -->
				<div class="track-order-listing-btns">
					<div class="row">
				       	<div class="submit-btn col-md-12 col-xs-12">
				       		<a href="${cmsPage.label}" class="btn analytics-genericLink" data-analytics-eventtype="order_tracking" data-analytics-eventsubtype="check_additional_order" data-analytics-linkplacement="order_tracking" data-analytics-linkname="check_additional_order"><spring:theme code="track.order.form.check.another.order.btn" /></a>
				       	</div>
				    </div>
				    <div class="row customercare">
				    	 <cms:pageSlot position="csn_CustomerServicePageSixthContentSlot" var="feature">
              					<cms:component component="${feature}"/>
           				 </cms:pageSlot>
				    </div>
				</div>


				<!-- Error Display -->
				<div class="track-order-error">
				<p><spring:theme code="track.order.result.fail.text" /></p> 
                     
					<div class="row">
				       	<div class="submit-btn col-md-12 col-xs-12">
				       		<a href="${cmsPage.label}" class="btn analytics-genericLink" data-analytics-eventtype="order_tracking" data-analytics-eventsubtype="lookup_another_order" data-analytics-linkplacement="order_tracking" data-analytics-linkname="lookup_another_order"><spring:theme code="track.order.form.look.another.btn" /></a>
				       	</div>
				    </div>
				    <div class="row customercare">
				    	 <cms:pageSlot position="csn_CustomerServicePageSixthContentSlot" var="feature">
              					<cms:component component="${feature}"/>
           				 </cms:pageSlot>
				    </div>
				</div>
	       	</div>
	    </div>