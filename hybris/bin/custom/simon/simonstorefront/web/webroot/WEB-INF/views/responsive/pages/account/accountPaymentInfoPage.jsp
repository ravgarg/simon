<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="noBorder" value="" />
<c:if test="${not empty paymentInfoData}">
	<c:set var="noBorder" value="no-border" />
</c:if>
<c:choose>
	<c:when test="${not empty paymentInfoData}">
		<div class="account-paymentdetails account-list">
			<div class="account-cards card-select">
					<div class="payment-method-box">
						<div class="row">
						<c:forEach items="${paymentInfoData}" var="paymentInfo">
								<div class="col-md-4">
									<div class="payment-method-border">
										<div class="payment-method-box-margin">
											<div class="card-details"><span>${paymentInfo.cardType}</span>
												ending in ${paymentInfo.lastFourDigits}</div>
											<div class="card-exp">${paymentInfo.expiryMonth}/${paymentInfo.expiryYear}</div>
											<div class="preferred-card-address"><c:out value="${paymentInfo.billingAddress.firstName}  ${paymentInfo.billingAddress.lastName}" /><br>
												<c:out value="${paymentInfo.billingAddress.line1} ${paymentInfo.billingAddress.line2}" /><br>
												<c:out value="${paymentInfo.billingAddress.town}, ${paymentInfo.billingAddress.region.isocodeShort} ${paymentInfo.billingAddress.postalCode}" />
												</div>
											<div class="sm-input-group">
												<c:url value="/my-account/set-default-payment-details" var="preferredPaymentdetailsUrl" />
													<form:form action="${preferredPaymentdetailsUrl}" method="post" commandName="paymentInfoId" data-parsley-validate="validate">
													<c:choose>
														<c:when test="${paymentInfo.defaultPaymentInfo}">
														<input type=hidden name="paymentInfoId" id="paymentInfoId" value="${paymentInfo.id}">
														<input type="radio" id="preferredCard" checked >
														</c:when>
														<c:otherwise>
														<input type=hidden name="paymentInfoId" value="${paymentInfo.id}">
														<input type="radio" id="preferredCard" >
														</c:otherwise>
													</c:choose>
														<label for="preferredCard"><spring:theme code="text.account.profile.preferred.card" /></label>
													</form:form>
											</div>
											<a href="#" data-toggle="modal" data-paymentid="${paymentInfo.id}" class="delete-payment"
												data-target="#paymentMethodDeleteModal" data-payment-method-type="${paymentInfo.cardType}-${paymentInfo.lastFourDigits}"><spring:theme code="text.account.profile.card.delete" /></a>
										</div>
									</div>
								</div>
								
					</c:forEach>
						<div class="modal fade payment-method-delete-modal" id="paymentMethodDeleteModal" role="dialog">
							<div class="modal-dialog">
								<div class="modal-content delete-modal-width">
									<div class="modal-header">
									<h5 class="modal-title"><spring:theme code="text.account.profile.card.delete.header" /></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-body clearfix">
									<p class="confirm-msg"><span><spring:theme code="text.account.profile.card.delete.confirm.first" /></span><span class="paymentMethodType"></span><span><spring:theme code="text.account.profile.card.delete.confirm.last" /></span></p>
									<div class="payment-method-buttons">
									<c:url value="/my-account/remove-payment-method" var="paymentdetailsRemoveUrl" />
										<form:form action="${paymentdetailsRemoveUrl}" method="post" commandName="paymentInfoId" id="PaymentInfoDelete" data-parsley-validate="validate">
											<button class="btn btn-add analytics-genericLink" data-analytics-eventtype="payment_method" data-analytics-eventsubtype="delete_payment_method" data-analytics-linkplacement="payment_method" data-analytics-linkname="delete_payment_method"><spring:theme code="text.account.profile.card.delete.button" /></button>
											<button class="btn secondary-btn black" data-dismiss="modal"><spring:theme code="text.account.profile.card.cancel.delete.button" /></button>
											<input type="hidden" name="paymentInfoId" value=""/>
										</form:form>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button type="button" class="btn add-new-payment-method analytics-genericLink" data-analytics-eventsubtype="add-paymentMethod" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|add-paymentMethod" data-analytics-linkname="add-paymentMethod" data-toggle="modal" data-target="#paymentMethodModal"><spring:theme code="text.account.profile.card.add.new.button" /></button>
				</div>
			</div>
		</div>
	</c:when>
	<c:otherwise>
		<div class="account-section-content content-empty">
			<div class="not-saved-payment-text"><spring:theme code="text.account.profile.no.card" /></div>
			<button type="button" class="btn add-new-payment-method"
				data-toggle="modal" data-target="#paymentMethodModal"><spring:theme code="text.account.profile.card.add.new.button" /></button>
		</div>
	</c:otherwise>
</c:choose>
<div class="modal fade payment-method-modal" id="paymentMethodModal"
	role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><spring:theme code="text.account.profile.card.add.new.header" /></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body clearfix">
				<c:url value="/my-account/payment-details" var="paymentdetailsUrl" />
				<form:form action="${paymentdetailsUrl}" method="post" commandName="paymentInfoForm" id="PaymentInfo" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-analytics-type="registration" data-analytics-eventtype="my_account" data-analytics-eventsubtype="add_payment_method" data-analytics-formtype="add_payment_method" data-analytics-formname="add_payment_method" data-satellitetrack="registration" data-parsley-validate="validate">
                     <input type="hidden" name="account_method_token" id="account_method_token" value="" />
                     <input type="hidden" name="cardType" id="cardType" value="" />
                     <input type="hidden" name="cardNumber" id="cardNumber" value="" />
                     <input type="hidden" name="lastFourDigitCard" id="lastFourDigitCard" value="" />
                     <input type="hidden" id="enableSpreedlyCalls" value="${enableSpreedlyCalls}"/>
					<div class="card-details"><spring:theme code="text.account.profile.card.add.details" /></div>
					<div class="cards-container clearfix">
						<div class="cards pull-left">
							<img alt="visa" src="/_ui/responsive/simon-theme/icons/visa.svg" class="simon-logo" /> 
							<img alt="mastercard" src="/_ui/responsive/simon-theme/icons/mastercard.svg" class="simon-logo" /> 
							<img alt="amex" src="/_ui/responsive/simon-theme/icons/amex.svg" class="simon-logo" />
						</div>
						<div class="secure-info small-text pull-right hidden-xs"><spring:theme code="text.account.profile.card.add.secure.msg" />
						</div>
					</div>
					<div class="optional-fields"></div>
					<div class="payment-method-fields">
						<div class="row">
							<div class="col-xs-12 col-md-7">
								<div class = "sm-input-group">
                                     <label class="field-label" for="firstName"><spring:theme code="text.account.profile.card.add.firstName" /><span class="field-error" id="error-firstName"></span></label>
                                   <input 
                                    type="text" 
                                    class="form-control  spf-input-text" 
                                    placeholder="<spring:theme code="text.account.profile.card.add.firstName" />" 
                                    id="first_name" 
                                    name="firstName"
                                    data-parsley-errors-container="#error-firstName" 
                                    data-parsley-error-message="First Name error" 
                                    required />
                                 
                                  </div>
							</div>
							<div class="col-xs-12 col-md-5">
								<div class = "sm-input-group">
                                    <label class="field-label" for="last_name"><spring:theme code="text.account.profile.card.add.lastName" /><span class="field-error" id="error-lastName"></span></label>
                                     <input 
                                      type = "text" 
                                      class = "form-control spf-input-text" 
                                      name="lastName"
                                      id="last_name" 
                                      placeholder = "<spring:theme code="text.account.profile.card.add.lastName" />" 
                                      data-parsley-errors-container="#error-lastName" 
                                      data-parsley-error-message="Last Name error" 
                                      required />
                                  </div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-7">
								<div class="sm-input-group">
									<div id="spreedly-number-test" class="spf-field-group spf-number spf-field-cc">
                                                </div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-7">
								<div class="row">
									<div class="col-xs-7 col-md-6">
										<div class = "sm-input-group list-dropdown">
											<label class="field-label" for="month"><spring:theme code="text.account.profile.card.add.exp.month" /><span class="field-error" id="error-expmonth"></span></label>
											<select id="month" data-parsley-errors-container="#error-expmonth" required>
												  <option value="" selected="" hidden=""><spring:theme code="text.account.profile.card.add.exp.month" /></option>
												  <option value="01">01</option>
												  <option value="02">02</option>
												  <option value="03">03</option>
												  <option value="04">04</option>
												  <option value="05">05</option>
												  <option value="06">06</option>
												  <option value="07">07</option>
												  <option value="08">08</option>
												  <option value="09">09</option>
												  <option value="10">10</option>
												  <option value="11">11</option>
												  <option value="12">12</option>
											</select>
										</div>
									</div>
									<div class="col-xs-7 col-md-6">
										<div class = "sm-input-group list-dropdown">
											<label class="field-label" for="year"><spring:theme code="text.account.profile.card.add.exp.year" /><span class="field-error" id="error-expyear"></span></label>
											<select id="year" data-parsley-errors-container="#error-expyear" required>
												  <option value="" selected=""><spring:theme code="text.account.profile.card.add.exp.year" /></option>
												  <option value="2017">2017</option>
												  <option value="2018">2018</option>
												  <option value="2019">2019</option>
												  <option value="2020">2020</option>
												  <option value="2021">2021</option>
												  <option value="2022">2022</option>
												  <option value="2023">2023</option>
												  <option value="2024">2024</option>
												  <option value="2025">2025</option>
												  <option value="2026">2026</option>
											  		<option value="2027">2027</option>
											  		<option value="2028">2028</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-7 col-md-5">
								<div class="sm-input-group">									
									<div id="spreedly-cvv-test" class="spf-field-group spf-verification_value spf-field-cc"></div>
                                 	<img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo cvv-tooltip" data-toggle="tooltip" title="" alt="black-question" data-original-title="">
									<div class="antivirus-icon show-desktop">
										<img title="" alt="mc-afee-fpo" src="/_ui/responsive/simon-theme/icons/mc-afee-fpo.png">
									</div>
								</div>
							</div>
							
						</div>
						<div id="message"></div>
	      				<div id="errors"></div>
						<script id="spreedly-iframe" data-environment-key="${spreedlyEnvKey}" data-number-id="spreedly-number-test" data-cvv-id="spreedly-cvv-test">
					    </script>
						<div class="row show-mobile margin-btm">
							<div class="secure-info small-text col-xs-7 col-xs-offset-1"><spring:theme code="text.account.profile.card.add.secure.msg"/><img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement="top" data-html="true" class="tooltip-init simon-logo" data-toggle="tooltip" title="" alt="black-question" data-original-title="MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00">
							</div>
							<div class="antivirus-icon col-xs-2">
								<img title="" alt="mc-afee-fpo"
									src="/_ui/responsive/simon-theme/icons/mc-afee-fpo.png">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<div class="billing-add major"><spring:theme code="text.account.profile.card.add.billingadd.header"/></div>
							</div>
						</div>
						
						<div class="selectaddress-section">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<div class = "sm-input-group list-dropdown">
									<label class="hide" for="address-list">Address List</label>
										 <select name="address-list" id="address-list" class = "form-control major">
										 <c:forEach items="${billingAddressList}" var="billingAddress">
											<option value="${billingAddress.id}"><c:out value="${billingAddress.line1}, ${billingAddress.line2}, ${billingAddress.region.isocodeShort} ${billingAddress.postalCode}" /></option>
										</c:forEach>
											<option value=""><spring:theme code="text.account.profile.card.add.new.billing.address"/></option>
										 </select>
									</div>
								</div>
							</div>
						 </div>
						 <c:choose>
						<c:when test="${empty billingAddressList}">
							<div class="row">
								<div class="col-xs-12 col-md-7">
									<div class="sm-input-group">
									<label class="field-label" for="billingAddress1"><spring:theme code="text.account.profile.card.add.billingadd.address" /><span class="field-error" id="error-line1"></span></label>
										<input type="text" data-parsley-errors-container="#error-line1" required class="form-control major" id="billingAddress1" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.address"/>" name="line1">
									</div>
								</div>
								<div class="col-xs-12 col-md-5">
									<div class="sm-input-group">
										<label class="field-label hide" for="billingAddress2">Address 2<span class="field-error"></span></label>
										<input type="text" class="form-control major" id="billingAddress2" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.apt"/>"  name="line2">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-md-7">
									<div class="sm-input-group">
									<label class="field-label" for="billingCity"><spring:theme code="text.account.profile.card.add.billingadd.city"/><span class="field-error" id="error-townCity"></span></label>
										<input type="text" data-parsley-errors-container="#error-townCity" required  class="form-control major" id="billingCity" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.city"/>" name="town">
									</div>
								</div>
								<div class="col-xs-12 col-md-5">		
									<div class="row">
										<div class="col-xs-7 col-md-6">
											<div class="sm-input-group list-dropdown">
											<label class="field-label" for="billingState"><spring:theme code="text.account.profile.card.add.billingadd.region"/><span class="field-error" id="error-regioniso"></span></label>
												<select class="form-control major" id="billingState" name="state" data-parsley-errors-container="#error-regioniso" required>
													<option value=""><spring:theme code="text.account.profile.card.add.billingadd.state"/></option>
													<c:forEach items="${stateList}" var="state" varStatus="index">
														<option value="${state.stateCode}">${state.isocodeShort}</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-xs-7 col-md-6">
											<div class="sm-input-group">
												<label class="field-label" for="zipCode"><spring:theme code="text.account.profile.card.add.billingadd.zip"/><span class="field-error" id="error-postcode"></span></label>
												<input type="text" name="postalCode" id="zipCode" data-parsley-errors-container="#error-postcode" data-parsley-error-message="Invalid"  data-parsley-required="" data-parsley-pattern="^(?=.*[1-9].*)[0-9]{5}?$" required class="form-control major" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.zip"/>">
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:when>
						</c:choose>
						<div id="addAddressContainer"></div>
						
						<div class="clearfix">
							<label class="checkboxes" for="defaultPayment">
								<div class="sm-input-group pull-left">
									<input type="checkbox" name="confirm" id="defaultPayment" class="custom-checkbox" value="true" />
								</div> <span class="make-default-text"><spring:theme code="text.account.profile.card.add.make.default"/></span>
							</label>
						</div>
						<div class="payment-method-buttons submit-btn">
							<button class="btn btn-add" id="addPaymentMethod" type="submit"><spring:theme code="text.account.profile.card.add.button"/></button>
							<button class="btn secondary-btn black" data-dismiss="modal"><spring:theme code="text.account.profile.card.add.cancel"/></button>
						</div>
					</div>
					
				</form:form>

				<div class="addaddress-section">
					<div class="row">
						<div class="col-xs-12 col-md-7">
							<div class="sm-input-group">
								<label class="field-label" for="billingAddress1">Address<span class="field-error" id="error-billingAddress1"></span></label>
								<input type="text" class="form-control major" id="billingAddress1" data-parsley-errors-container="#error-billingAddress1" required placeholder="<spring:theme code="text.account.profile.card.add.billingadd.address"/>" name="line1">
							</div>
						</div>
						<div class="col-xs-12 col-md-5">
							<div class="sm-input-group">
								<label class="field-label hide" for="billingAddress2">Address 2<span class="field-error"></span></label>
								<input type="text" class="form-control major" id="billingAddress2" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.apt"/>"  name="line2">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-7">
							<div class="sm-input-group">
							<label class="field-label" for="billingCity">City<span class="field-error" id="error-billingCity"></span></label>
							<input type="text" class="form-control major" id="billingCity" data-parsley-errors-container="#error-billingCity" required placeholder="<spring:theme code="text.account.profile.card.add.billingadd.city"/>" name="town">
							</div>
						</div>
						<div class="col-xs-12 col-md-5">
							<div class="row">
								<div class="col-xs-7 col-md-6">
									<div class="sm-input-group list-dropdown">
										<label class="field-label" for="billingState">State<span class="field-error" id="error-billingState"></span></label>
										<select class="form-control major" id="billingState" name="state" data-parsley-errors-container="#error-billingState" required>
											<c:forEach items="${stateList}" var="state" varStatus="index">
												<option value="${state.stateCode}">${state.stateName}</option>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="col-xs-7 col-md-6">
									<div class="sm-input-group">
										<label class="field-label" for="zipCode"><spring:theme code="text.account.profile.card.add.billingadd.zip"/><span class="field-error" id="error-postcode"></span></label>
												<input type="text" name="postalCode" id="zipCode" data-parsley-errors-container="#error-postcode" data-parsley-error-message="Invalid"  data-parsley-required="" data-parsley-pattern="^(?=.*[1-9].*)[0-9]{5}?$" required class="form-control major" placeholder="<spring:theme code="text.account.profile.card.add.billingadd.zip"/>">
									</div>
								</div>
							</div>	
						</div>		
					</div>
				</div>


			</div>
		</div>
	</div>
</div>