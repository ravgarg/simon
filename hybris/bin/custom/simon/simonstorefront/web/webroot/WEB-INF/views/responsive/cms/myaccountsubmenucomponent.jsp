<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<ul class="nav navbar-nav myaccount-dropdown navbar-right">
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle"><spring:theme code="header.link.account"/></a>
        <ul class="dropdown-menu">
            <c:forEach items="${feature.links}" var="subMenu">
                            
                <c:choose>
                    <c:when test="${subMenu.linkName eq 'Logout'}">
                    	<c:url value="${subMenu.url}" var="logouturl" />
						<div class="logoutBtn analytics-logout">
							<a class="btn" href="${logouturl}">${subMenu.name}</a>
						</div>					
                    </c:when>
	  		         <c:otherwise>
	  		         	<c:url value="${subMenu.url}" var="linkUrl" />
	                    <li><a href="${linkUrl}">${subMenu.name}</a></li>
	  		         </c:otherwise>
               </c:choose>
           </c:forEach>
        </ul>
    </li>
</ul>

