<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order/custom"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<template:page pageTitle="${pageTitle}">
<c:set var = "skip" value = "false"/>
<c:if test="${empty userPassword}">
	<c:set var = "skip" value = "true"/>
</c:if>
<input type="hidden" name="skip" value="${skip}">
<input type="hidden" name="CSRFToken" value="${CSRFToken}">
<input type="hidden" name="firstName" value="${orderData.deliveryAddress.firstName}">
<input type="hidden" name="lastName" value="${orderData.deliveryAddress.lastName}">
<input type="hidden" name="zip" value="${orderData.paymentInfo.billingAddress.postalCode}">
<input type="hidden" name="email" value="${orderData.deliveryAddress.email}">
<input type="hidden" name="country" value="${orderData.paymentInfo.billingAddress.country.isocode}">
<input type="hidden" class="analytics-orderData" name="ordercode" value="${orderCode}">

	<div class="confirm">
		<div class="container">
			<cms:pageSlot position="csn_OrderPromo1Slot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4 order-summary-right anchoredRightRail">
					<order:rightOrderSummary order="${orderData}" />
				</div>
				<div class="col-xs-12 col-md-8">
					<order:orderConfirmationHeadings order="${orderData}" />
					<div class="account-created-heading hide">
						<div class="account-success-text">
							<spring:theme code='order.confirmation.account.created.successfully.txt' />
						</div>
						<div class="account-success-confirm">
							<spring:theme code="order.confirmation.account.created.successfully.msg" arguments="${orderData.deliveryAddress.email}" />
						</div>
					</div>
					<div class="account-already-exist hide">
						<div class="already-exist-text">
							<spring:theme code='order.confirmation.account.already.exist.txt' />
						</div>
						<div class="already-exist-msg">
							<spring:theme code="order.confirmation.account.already.exist.msg" arguments="${orderData.deliveryAddress.email}" />
							&nbsp <a href="/login" class="analytics-genericLink" data-analytics-eventsubtype="orderconfirmation_signin" data-analytics-linkplacement="orderconfirmation_signin" data-analytics-linkname="signin"><spring:theme code="login.login"/></a>.
						</div>
					</div>
					<div class="account-created-error hide">
						<div class="already-exist-text">
							<spring:theme code='order.confirmation.integration.exception.txt' />
						</div>
						<div class="already-exist-msg">
							<spring:theme code="order.confirmation.integration.exception.msg" />&nbsp<a href="#" class="analytics-genericLink" data-analytics-eventsubtype="orderconfirmation_signin" data-analytics-linkplacement="orderconfirmation_signin" data-analytics-linkname="signin">login</a>.
						</div>
					</div>
					<div class="loader-background">
						<div id="showLoader"></div>
					</div>
					<c:if test="${empty userPassword and !isLoggedIn}">
						<div class="confirmation-create-account">
							<div class="create-account-heading">
								<spring:theme code='order.confirmation.create.account.txt' />
							</div>
							<div class="create-account-msg">
								<spring:theme code="order.confirmation.create.account.msg" arguments="${orderData.deliveryAddress.email}" />
							</div>
							<form class="sm-form-validation checkoutCreateAccount analytics-formValidation" data-analytics-type="registration" data-analytics-eventtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-eventsubtype="registration" data-analytics-formtype="registration_form" data-analytics-formname="orderConfirmation_registrationForm" data-satellitetrack="registration" id="checkoutCreateAccount" data-parsley-validate="validate">
								<div class="row">
									<div class="col-md-6 col-xs-12 sm-input-group">
										<label class="field-label" for="password">Password <span class="field-error" id="error-password"></span></label>
										<input type="password" id="password" name="password" autocomplete="off" class="form-control" data-parsley-errors-container="#error-password" required
											data-parsley-pwdstrength-message="Please enter a valid password" data-parsley-pwdstrength placeholder="ENTER PASSWORD">
									</div>
									<div class="col-md-6 col-xs-12 sm-input-group">
										<label class="field-label" for="confirmPassword">confirm password <span class="field-error" id="error-confirmPassword"></span></label>
										<input type="Password" id="confirmPassword" name="confirmPassword" autocomplete="off" class="form-control" data-parsley-equalto-message="Passwords do not match"
											data-parsley-equalto="#password" data-parsley-errors-container="#error-confirmPassword" required placeholder="CONFIRM PASSWORD">
									</div>
									<div class="col-md-12 col-xs-12 btn-create-account">
										<button type="button" class="btn col-md-3 col-xs-12 btn-crt-account">
											<spring:theme code='order.confirmation.create.account.btn' />
										</button>
									</div>
								</div>
							</form>
						</div>
					</c:if>
					<div class="shipping-payment-method">
						<order:orderShippingDetails order="${orderData}" />
					</div>
					<order:orderItemsDetails order="${orderData}" />
				</div>
			</div>
		</div>
		<div class="container bottom-hero-image">
			<cms:pageSlot position="csn_OrderPromo2Slot" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</div>
	</div>
</template:page>