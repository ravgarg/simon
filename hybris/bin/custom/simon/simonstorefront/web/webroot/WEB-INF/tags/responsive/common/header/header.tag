<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<c:set var="compCount" value="0"/>
<cms:pageSlot position="csn_HeaderMessageSlot" var="feature"> 
                			<c:set var="compCount" value="${compCount+1}"/>
            		</cms:pageSlot>
<header class="js-mainHeader nav-down">
<!--top promo ticker start-->


<c:if test="${compCount>0}">
<div class="top-header-promo hide clearfix" id="topHeaderPromo">
	<div class="container">
	<div class="row">
		<div class="col-sm-1 col-xs-3 no-pad-r">
			<c:if test="${compCount >1}">
			<span class="arrow-back" id="promoTickerBack">
			</span> 
			<span class="promonavi">
				<span class="ticker-showing">1</span>
				<span>/</span>
				<span class="total-ticker">${compCount}</span>
			</span>
			<span class="arrow-right" id="promoTickerFwd">
			</span>
			</c:if>
		</div>
		<div class="col-sm-10 col-xs-7 no-pad top-promo-text">
					<cms:pageSlot position="csn_HeaderMessageSlot" var="feature"> 
                			<cms:component component="${feature}"/>
            		</cms:pageSlot>
		</div>
		<div class="col-sm-1 col-xs-2 no-pad-l">
			<span class="promo-close" id="promoTickerClose">
			</span>
		</div>
	</div>
	</div>
</div>
</c:if>
<!--top promo ticker ends-->
<div class="modal-backdrop fade mega-menu-blur"></div>
	<%-- a hook for the my account links in desktop/wide desktop--%>
	
	<div class="navbar navigation navigation--middle js-navigation--middle"
		role="navigation">

		
		<div class="container">
			<div class="row clearfix">
				<div class="col-xs-2 show-mobile">
				<c:if test="${cmsPageRequestContextData.page.uid != 'beta-login'}">
					<a class="main-menu-toggle" href="javascript:void(0)"> <span class="white-line"></span>
					<span class="hide"> main menu</span>
					</a>
			     </c:if>
					
				</div>
				<div class="col-md-6 col-xs-8">
					<cms:pageSlot position="csn_SitesLogoSlot" var="feature">
                		<cms:component component="${feature}"/>
            		</cms:pageSlot> 
				</div>
				<div class="col-md-6 col-xs-2 mobile-mini-bag">
					<div class="nav__right">
						<ul class="nav top-right-links">
						
						<c:choose>
							<c:when test="${pageContext.request.getParameter('name') != 'checkout'}">
						<cms:pageSlot position="csn_HeaderLinksCompSlot" var="feature">
                			<cms:component component="${feature}" element="li" class="hide-mobile"/>
            			</cms:pageSlot> 
						<cms:pageSlot position="csn_MiniCartCompSlot" var="feature" element="li" class="mini-cart dropdown">
                			<cms:component component="${feature}" ></cms:component>
            			</cms:pageSlot>
            				</c:when>	
            				<c:otherwise>
								<li class="secure-shopping major">SECURE SHOPPING</li>
							</c:otherwise>
							</c:choose>
						</ul>
					</div>
				</div>
			</div>
			
		</div>
	</div>

<!!--condition added for checkout-->
<c:choose>
	<c:when test="${pageContext.request.getParameter('name') != 'checkout'}">
	<cms:pageSlot position="csn_NavigationBarCompSlot" var="feature" view="desktop" >
	  <cms:component component="${feature}"/>
	</cms:pageSlot>
	</c:when>	
</c:choose>
</header>
