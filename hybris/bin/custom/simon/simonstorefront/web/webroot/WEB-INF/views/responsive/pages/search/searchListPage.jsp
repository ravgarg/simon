
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}" id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}" id="totalNumberOfResults" />
<input type="hidden" data-fav-url="true"
		value='{"add":"/my-account/addToMyFavorite", "remove":"/my-account/removeFromMyFavorite" }' />
	<div class="container category-landing">
		<article class="row">
			<div class="col-md-12 hide-mobile">
				<div class="breadcrumbs">
					<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
					<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
				</div>

				<!-- breadcurmb Ends -->
			</div>

			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="../../cms/facetDynamicNavigation.jsp"></jsp:include>
			</aside>


			<section class="col-md-9">

				<div class="search-result">
					<h3 class="result-for">
						<spring:theme code="search.page.searchText" />
					</h3>
					<h3 class="search-term">${searchPageData.freeTextSearch}</h3>
					<input type="hidden" value="${searchPageData.freeTextSearch}" id="searchText"/>
					<nav:searchSpellingSuggestion
						spellingSuggestion="${searchPageData.spellingSuggestion}" />
				</div>
				<!-- <div class="btn-category hide-desktop">
					<button class="btn secondary-btn black">
						<spring:theme code="text.clp.browse.categories" />
					</button>
				</div> -->

				<!-- Filters Starts -->
				<product:productFilters searchPageData="${searchPageData}" />
				<!-- Filters END -->
				<!-- Product Tiles Starts -->
				<product:productTilesListing searchPageData="${searchPageData}" />
				<!-- Product Tiles END -->
				<div class="row load-more-btn-wrapper">
					<button id="loadMore" class="btn btn-primary load-more block"><spring:theme code='plp.loadmore.text' /></button>
				</div>
				<!-- Product Tiles Ends -->

			</section>
		</article>
	</div>
</template:page>
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}" id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}" id="totalNumberOfResults" />