<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>


<spring:url value="/my-account/update-profile" var="updateProfileUrl" />
<spring:url value="/my-account/update-password" var="updatePasswordUrl" />
<spring:url value="/my-account/update-email" var="updateEmailUrl" />
<spring:url value="/my-account/address-book" var="addressBookUrl" />
<spring:url value="/my-account/payment-details" var="paymentDetailsUrl" />
<spring:url value="/my-account/orders" var="ordersUrl" />

<template:page pageTitle="${pageTitle}">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="breadcrumbs col-md-3 hide-mobile">
						<c:forEach items="${breadcrumbs}" var="breadcrumbs" varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}">
								<c:if test="${status.last}">
									<span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}
							</a>
						</c:forEach>
					</div>

					<c:if test="${not empty accountPageNotificationUpdateKey}">
						<div class="col-md-9 col-xs-12">
						<c:choose>
							<c:when test="${not empty accountPageNotificationErrorKey}">
							<div class="error-msg analytics-formSubmitFailure"> 
											<spring:theme code="${accountPageNotificationUpdateKey}" />
										</div>
							</c:when>
							<c:otherwise>
							<div class="profile-update-msg analytics-formSubmitSuccess"> 
											<spring:theme code="${accountPageNotificationUpdateKey}" />
										</div>
							</c:otherwise>
						
						</c:choose>
									
						</div>
					</c:if>

				</div>
			</div>

			<cms:pageSlot position="csn_leftNavigation-accountPage" var="feature" element="div" class="account-section-content">
				<cms:component component="${feature}" />
			</cms:pageSlot>

			<div class="col-md-9">
				<button class="col-xs-12 btn secondary-btn black major hide-desktop my-account"><spring:theme code="text.account.yourAccount" /></button>
				
				<div class="${accountPageSpecificDivClass}">
					<h1 class="page-heading major">
						<spring:theme code="${accountPageHeaderMessageKey}" /> 
					</h1>
					
				<cms:pageSlot position="csn_PageDescription-accountPage" var="feature" element="div" class="account-section-content">
					<div class="payment-method-text">
						<cms:component component="${feature}" />
					</div>
				</cms:pageSlot>
					
				<div class="myaccount-banner">
					<cms:pageSlot position="csn_firstContentSlot-accountPage" var="feature" element="div"  class="account-section-content ${cmsParagraphClass} }">
						<!-- <div class="myaccount-banner"> -->
							<cms:component component="${feature}" />
						<!-- </div> -->
					</cms:pageSlot>

					<cms:pageSlot position="csn_secondContentSlot-accountPage" var="feature" element="div" class="accountPageBottomContent">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
					<cms:pageSlot position="csn_thirdContentSlot-accountPage" var="feature" element="div" class="accountPageBottomContent">
						<div class="myaccount-banner bottom-banner">
							<cms:component component="${feature}" />
						</div>
					</cms:pageSlot>

					<cms:pageSlot position="csn_fourthContentSlot-accountPage" var="feature" element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>

				</div>
			</div>
		</div>
	</div>
</template:page>