<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<template:page pageTitle="${pageTitle}">
	<div class="category-landing">
		<div class="container">
			<div class="row">
				<div class="col-md-12 hide-mobile">
					<jsp:include page="../../xt-component/clp-breadcurmb.jsp"></jsp:include>
				</div>
				<cms:pageSlot position="csn_clp_leftNavigationSlot" var="feature" view="desktop" >
	  				<cms:component component="${feature}"/>
				</cms:pageSlot>
				<div class="col-md-9 analytics-rightContainer" data-analytics-pagelisttype="${pageType}" data-analytics-pagelistname="${categoryName}">
				
					<cms:pageSlot position="csn_clp_PageTitleTextSlot" var="feature">
							<cms:component component="${feature}"/>
					</cms:pageSlot>
				
					<div class="page-context-links">
						<cms:pageSlot position="csn_clp_categorSEOTextSlot" var="feature">
							<cms:component component="${feature}"/>
						</cms:pageSlot>
						<div class="clp-des-links">
							<cms:pageSlot position="csn_clp_categorySEOLinkSlot" var="feature"  >
								<cms:component component="${feature}"  />
								<span>&rsaquo;</span>
							</cms:pageSlot>
						</div>
					
					</div> 
					<div class="clp-main-carousel">
						<div class="owl-carousel" id="homepageCarousel">
							<cms:pageSlot position="csn_clp_heroSlot" var="feature">
								<cms:component component="${feature}"/>
							</cms:pageSlot>
						</div>
					</div>
					<div class="custom-heading-links-component">
						<div class="btn-category hide-desktop">
							<button class="btn secondary-btn black"><spring:theme code="text.clp.browse.categories"/></button>
						</div>
						
						<cms:pageSlot position="csn_clp_trendingNowHeading" var="feature">
									<cms:component component="${feature}"/>
							</cms:pageSlot>
			
						<div class="page-context-links">
							<div class="clp-des-links">
								<cms:pageSlot position="csn_clp_trendingNowSlot" var="feature">
										<cms:component component="${feature}"/>
										<span>&rsaquo;</span>
								</cms:pageSlot>
							</div>							
						</div>
					</div>
					<div class="trending-now common-heading row">
							<cms:pageSlot position="csn_clp_contentSpotsSlot" var="feature">
								<cms:component component="${feature}"/>
							</cms:pageSlot>
					</div>
					<div class="clp-container">
						<cms:pageSlot position="csn_clp_promo1Slot" var="feature">
								<cms:component component="${feature}"/>
						</cms:pageSlot>
					</div>
				
					<div class="trending-now common-heading">
					
							<cms:pageSlot position="csn_clp_trendShopsSlotHeading" var="feature">
									<cms:component component="${feature}"/>
							</cms:pageSlot>
							
							<cms:pageSlot position="csn_clp_trendShopsSlot" var="feature">
								<cms:component component="${feature}"/>
							</cms:pageSlot>
						
			
						<div class="clp-container">
							<cms:pageSlot position="csn_clp_promo2Slot" var="feature">
									<cms:component component="${feature}"/>
							</cms:pageSlot>
						</div>
				
						<div class="style-deals common-heading">
							<div class="row">
								<cms:pageSlot position="csn_clp_styleDeals" var="feature">
									<cms:component component="${feature}"/>
								</cms:pageSlot>
							</div>
						</div>
				
						<div class="clp-container">
							<cms:pageSlot position="csn_clp_promo3Slot" var="feature">
									<cms:component component="${feature}"/>
							</cms:pageSlot>
						</div>
				
				</div>
			</div>
		</div>
	</div>
	<div class="social-shop">
		<div class="container">
			<div class="row">
				<div class="social-shop-containers  col-xs-12 col-md-8" >
					<div class="row">
						<div id="carousalFooter">
							<cms:pageSlot position="csn_clp_SocialShopSlot" var="feature">
								<div class="social-shop-content item col-xs-12 col-md-4">
									<cms:component component="${feature}" />
								</div>
							</cms:pageSlot>
						</div>
					</div>
				</div>
				<cms:pageSlot position="csn_clp_SocialShopTextSlot" var="feature" >
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</div>
		</div>			
	</div>	
	<cms:pageSlot position="csn_TextContentSlot" var="feature" element="div" class="span-24 section5 cms_disp-img_slot">
		<cms:component component="${feature}" />
	</cms:pageSlot>
	</div>
</template:page>