<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="btn-border-top">
	<div>
		<button class="btn plum inactive placeorderBtn" disabled="true">
			<spring:theme code="checkout.right.summary.place.order.button.text" />
		</button>
	</div>
</div>

<div class="terms-conditions small-text">
	<spring:theme code="checkout.right.summary.accept.term.text" />
</div>

<div class="cart-help-container hide-mobile">
	<div class="help-info"><spring:theme code="checkout.right.summary.helpful.information.text" /></div>
	<div class="information"><spring:theme code="checkout.right.summary.helpful.information.content.text" /></div>
	<div class="question"><spring:theme code="checkout.right.summary.question.text" /></div>
	<div class="more-details">
		<a href="#"><spring:theme code="checkout.right.summary.shipping.methods.charges.details.text" /></a>
	</div>
