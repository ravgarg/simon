<div class="modal fade addressBookModal" id="addressBookDeleteModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content delete-modal-width">
            <div class="modal-header">
                <h5 class="modal-title">Delete Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <p class="confirm-msg">Are you sure you want to delete {address} from your saved addresses?</p>
                 <div class="address-buttons">
                    <button class="btn btn-add">delete</button>
                    <button class="btn secondary-btn black" data-dismiss="modal">CANCEL</button>
                 </div>
            </div>
        </div>
    </div>
</div>