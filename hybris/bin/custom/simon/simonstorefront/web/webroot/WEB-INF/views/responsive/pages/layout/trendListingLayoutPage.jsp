<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
<c:set var="pagination" value="${searchPageData.pagination }" />
<input type="hidden" value="${pagination.pageSize}" id="pageSize" />
<input type="hidden" value="${pagination.currentPage}" id="currentPage" />
<input type="hidden" value="${pagination.numberOfPages}" id="numberOfPages" />
<input type="hidden" value="${pagination.totalNumberOfResults}" id="totalNumberOfResults" />
<input type="hidden" data-listing-url="true" value='{"add":"/my-account/add-store", "remove":"/my-account/remove-store" }' />
 	<div class="category-landing">
		<div class="container">
			<div class="row">
				<div class="col-md-12 hide-mobile">
					<div class="breadcrumbs">
					   <c:forEach items="${breadcrumbs}" var="breadcrumbs" varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}"><c:if test="${!status.first}"><span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}</a>
						</c:forEach>
					</div>
				</div>
				<input type="hidden" value="${trendCode}" id="listingId" class="base-code" />
				<cms:pageSlot position="csn_TrendListing_productLeftRefinements"
					var="feature" element="div">
					<cms:component component="${feature}" />
				</cms:pageSlot>
 
				<div class="col-md-9 trends-listing-container">
 	 			  
 	 			  <div class="add-to-favorites storefront-heading">
					 	<h1 class="page-heading major">${trendName}</h1>
				  </div>
				  <div class="show-more">
						<cms:pageSlot
								position="csn_TrendListing_pageDescriptionSlot" var="feature"
								element="div">
								<cms:component component="${feature}" />
							</cms:pageSlot>
					 
					</div>

					<div class="trends-listing-new-season">
						<cms:pageSlot position="csn_TrendListing_heroSlot" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>
					
			 		<!-- Filters Starts -->
					<product:productFilters searchPageData="${searchPageData}" />
					<!-- Filters END -->
					<!-- Product Tiles Starts -->
					<product:productTilesListing searchPageData="${searchPageData}" />
					<c:if test="${fn:length(searchPageData.results)<=0}">
					 <div><spring:theme code="search.mobile.no.results" /></div>
					</c:if>
					<!-- Product Tiles END -->
					<div class="row load-more-btn-wrapper">
						<button id="loadMore" class="btn btn-primary load-more block"><spring:theme code='plp.loadmore.text' /></button>
					</div>
					<!-- Product Tiles Ends -->
				</div>
			</div>
 		</div>
	</div>
</template:page>