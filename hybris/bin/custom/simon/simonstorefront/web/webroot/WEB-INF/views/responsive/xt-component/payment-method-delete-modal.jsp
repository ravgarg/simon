<div class="modal fade payment-method-delete-modal" id="paymentMethodDeleteModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content delete-modal-width">
            <div class="modal-header">
                <h5 class="modal-title">Delete Payment Method</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body clearfix">
                <p class="confirm-msg">Are you sure you would like to delete {payment method name} from your saved payment methods?</p>
                 <div class="payment-method-buttons">
                    <button class="btn btn-add">delete</button>
                    <button class="btn secondary-btn black" data-dismiss="modal">CANCEL</button>
                 </div>
            </div>
        </div>
    </div>
</div>