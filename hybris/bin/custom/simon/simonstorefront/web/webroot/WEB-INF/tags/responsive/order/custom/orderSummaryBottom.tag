<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>



<div
	class="order-summary-price-container pad-r-mobile pad-l-mobile collapse">
	<div class="price-top-spacing" >
		<span class="estimated-subtotal"><spring:theme code='order.confirmation.estimated.order.txt' /></span> <span
			class="estimated-sub-price" data-analytics-exclusive="${order.subTotal.value}">${order.subTotal.formattedValue}</span>
	</div>
	<div class="ship-top-spacing">
		<span class="estimated-shipping"><spring:theme code='order.confirmation.estimated.shipping.charge.txt'/></span> <span
			class="estimated-shipping-price" data-analytics-shipping="${order.deliveryCost.value}">${order.deliveryCost.formattedValue}</span>
	</div>
	<div class="tax-top-spacing">
		<span class="estimated-tax"> <spring:theme code='order.confirmation.estimated.tax.txt' />
		</span> <span class="estimated-tax-price" data-analytics-tax="${order.totalTax.value}">${order.totalTax.formattedValue}</span>
	</div>
	<div class="last-call-spacing">
		<span class="last-call"><spring:theme code='order.confirmation.estimated.order.total.txt' /></span> <span
			class="last-call-price" data-analytics-gross-total="${order.totalPrice.value}">${order.totalPrice.formattedValue}</span>
	</div>
	<div class="you-save-spacing">
		<span class="you-save"><spring:theme code='order.confirmation.you.save.txt' /></span> <span class="you-save-price" data-analytics-discount="${order.totalDiscounts.value}">${order.totalDiscounts.formattedValue}</span>
	</div>
</div>
