<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="shipping & Return">
<input type="hidden" name="CSRFToken" value="${CSRFToken}"> 
<div class="shipping-return-page" data-url="/select-retailer">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<div class="breadcrumbs col-md-12 hide-mobile">
						<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
						<c:forEach items="${breadcrumbs}" var="breadcrumbs"
							varStatus="status">
							<c:set var="url">
								<c:url value="${breadcrumbs.url}" />
							</c:set>
							<a href="${url}"><c:if test="${status.last}">
									<span class="arrow-fwd"></span>
								</c:if>${breadcrumbs.name}</a>
						</c:forEach>
					</div>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<cms:pageSlot position="csn_ShippingReturnsPageLeftNavigationSlot" var="feature">
					<cms:component component="${feature}" element="div" class="span-24 section1 cms_disp-img_slot" />
				</cms:pageSlot>
			</aside>
			<div class="col-md-9">
			
			  <cms:pageSlot position="csn_ShippingReturnsFirstContentSlot" var="feature">
			    <cms:component component="${feature}" element="h1" class="page-heading major" />   
			  </cms:pageSlot>
			  			    
				<div class="page-context-links shippingnreturns-desc">
				<cms:pageSlot position="csn_ShippingReturnsSecondContentSlot"
					var="feature">
					<cms:component component="${feature}" element="span" uid="ellipsisText"
						class="ellipsis-text show-more" />
				</cms:pageSlot>
				</div>
				
				<div class="btn-category show-mobile">
					<div class="browse-categories">
						<button id="mBrowseCategoriesBtn"class="btn block black secondary-btn" type="button" data-toggle="modal" data-dismiss="modal" data-target="#contact-us">
						<spring:theme code="text.footer.customer.service.btn" />
						</button>
					</div>
				</div>	
				
				<div class="shipping-and-returns">								
					<form class="sm-form-validation">
						<div class="no-store-selected" id="noStoreSelected">		
							<div class="row">
								<div class="col-xs-12 col-md-12" id="divShippingReturn">
									<label class="field-label"><spring:theme code='shipping.return.select.a.store.txt'/></label>
									<select class="selectpicker" id="shippingReturns" title="<spring:theme code='shipping.return.select.a.store.txt'/>" data-live-search="true" 
									 data-live-search-placeholder="Search" tabindex="-98">
										<c:if test="${isLoggedIn}">
											<optgroup label="<spring:theme code='shipping.return.my.store.txt'/>">
												<c:forEach items="${userRetailerMap}" var="userRetailer">
													<option value="${userRetailer.value}">${userRetailer.key}</option>
												</c:forEach>
											</optgroup>
										</c:if>	
										<optgroup label="<spring:theme code='shipping.return.all.store.txt'/>">
											<c:forEach items="${retailerMap}" var="retailer">
												<option value="${retailer.value}">${retailer.key}</option>
											</c:forEach>
										</optgroup> 							
									</select>
								</div>
							</div>
							
							<div class="row">
								<div class="col-xs-12">
								   	<cms:pageSlot position="csn_ShippingReturnsThirdContentSlot" var="feature">
					                    <cms:component component="${feature}" element="p" 
						                   class="privacy-info" />
				                    </cms:pageSlot>
									<div class="selected-store-container">

									</div>	
								</div>
							</div>							
						</div>
					</form>
								
				</div>			
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-list" id="contact-us" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title"><spring:theme code="text.footer.customer.service.btn" /></h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<cms:pageSlot position="csn_ShippingReturnsPageLeftNavigationSlot"
						var="feature">
						<cms:component component="${feature}" element="div"
							class="span-24 section1 cms_disp-img_slot" />
					</cms:pageSlot>
				</div>
			</div>
		</div>
	</div>
</template:page>