/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'ajaxFactory', 'typeahead','globalMegaMenu', 'viewportDetect', 'analytics', 'utility'], function($, ajaxFactory, typeahead,globalMegaMenu, viewportDetect, analytics, utility) {
    'use strict';
    var cache;
    var labels = [];
	var map = {};
    var globalHeader = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.initAjax();
            this.initSearchEvent();
            this.setMobileMenuPosition();
            this.fixedHeader();
            this.checkPromoBanner();
			if(cache.$document.find('.analytics-errorPage, .search-zero-result').length > 0){
				analytics.analyticsOnPageLoad();
			}
            if($('#searchInput').length){
                $('#searchInput').on('focus',function(){
                    var $this = $(this);
                    $this.data('placeholder',$this.prop('placeholder'));
                    $this.removeAttr('placeholder')
                }).on('blur',function(){
                    var $this = $(this);
                    $this.prop('placeholder',$this.data('placeholder'));
                });
            }
            this.headerHeightCalc();
        },
        initVariables: function() {
            cache = {
                $document : $(document),
                $searchInput: $('#searchInput'),
                $blurScreenDiv : $('.mega-menu-blur'),
                $headerSearchContainer: $('.simon-header-search'),
                $topHeaderPromo : $('#topHeaderPromo'),
                $mobileMegaMenu: $('.mobile-mega-menu'),
                $tickerCounter : 0,
                $window: $(window),
                $headerContainer : $('.js-mainHeader'),
                $body: $('body'),
                $megaMenuBar : $('.simon-md-mega-menu')
            }
        },
        initAjax: function() {},
        initEvents: function() {
            cache.$document.on('searchSuggestion', globalHeader.suggestionOverlay);
            cache.$document.on('click', '#promoTickerBack', 'back', globalHeader.initTopPromoTicker);
            cache.$document.on('click', '#promoTickerFwd', 'fwd', globalHeader.initTopPromoTicker);
            cache.$document.on('click', '#promoTickerClose', globalHeader.closePromoTicker);
            cache.$searchInput.bind('typeahead:select', function(ev, suggestion) {
            });
            cache.$document.on('keyup', '#searchInput', globalHeader.typeAheadKeyup);
        },
        typeAheadKeyup: function(e){			
            if(e.target.value.length < 3){
                $('.simon-header-search .analytics-headerSearch ul li').remove();
                $('.simon-header-search .analytics-headerSearch ul').addClass('hide');
            }
        },
        fixedHeader: function(){
                // Hide Header on on scroll down
                var didScroll;
                var lastScrollTop = 0;
                var delta = 5;
                var navbarHeight = $('header').outerHeight();
                globalHeader.headerHeightCalc();
                $(window).scroll(function(event){
                    didScroll = true;
                });

                setInterval(function() {
                    if (didScroll) {
                        hasScrolled();
                        didScroll = false;
                    }
                }, 0);

                function hasScrolled() {
                    var st = $(window).scrollTop();
                    
                    // Make sure they scroll more than delta
                    if(Math.abs(lastScrollTop - st) <= delta)
                        return;
                    
                    // If they scrolled down and are past the navbar, add class .nav-up.
                    // This is necessary so you never see what is "behind" the navbar.
                    if (st > lastScrollTop && st > navbarHeight){
                        // Scroll Down
                        $('header').removeClass('nav-down').addClass('nav-up');
                    }else{
                        // Scroll Up
                        if(st + $(window).height() < $(document).height()) {
                            $('header').removeClass('nav-up').addClass('nav-down');
                    }
                }
                    
                    lastScrollTop = st;
                }
        },
        headerHeightCalc: function(){
            var viewPort = viewportDetect.lastClass;

            //Header for Checkout Page
            if(window.location.href.indexOf("checkout") > -1 && window.location.href.indexOf("orderConfirmation") == -1) {
            	(viewPort === 'large') ?  $('header').css('height','74px') :  $('header').css('height','64px');
                (viewPort === 'large') ?  $('.nav-up').css('top','-74px') :  $('.nav-up').css('top','-64px');
                (viewPort === 'large') ?  $('header').next('div').css('margin-top','74px') :  $('header').next('div').css('margin-top','64px');
            }
            //Header for Other Pages including Order Confirmation
            else {
            if($('.top-header-promo').is(":visible")){
                (viewPort === 'large') ?  $('header').css('height','182px') :  $('header').css('height','170px');
                (viewPort === 'large') ?  $('.nav-up').css('top','-182px') :  $('.nav-up').css('top','-170px');
                    (viewPort === 'large') ?  $('header').next('div').css('margin-top','182px') :  $('header').next('div').css('margin-top','170px');
                    }else{
                (viewPort === 'large') ?  $('header').css('height','142px') :  $('header').css('height','116px');
                (viewPort === 'large') ?  $('.nav-up').css('top','-142px') :  $('.nav-up').css('top','-116px');
                    (viewPort === 'large') ?  $('header').next('div').css('margin-top','142px') :  $('header').next('div').css('margin-top','116px');
                    }
                }
        },
        setMobileMenuPosition: function(){
            var marginTop = cache.$headerContainer.height();
            cache.$mobileMegaMenu.css('top', marginTop-68+'px');
        },
        closePromoTicker: function(){
            
            cache.$topHeaderPromo.slideUp( 200, function() {
            	utility.setCookie('promoBannerCookie', 'true');
                $(this).addClass('hide');
                globalHeader.headerHeightCalc();
            });
            globalHeader.setMobileMenuPosition();
        },
        //Set cookie for Promo Banner - once closed, it will only be displayed in new browser
        checkPromoBanner: function() {
        	if(utility.getCookie('promoBannerCookie') === null){
        		$('#topHeaderPromo').removeClass('hide');
            }
        },
        initTopPromoTicker: function(elm){
            var promoItems = cache.$topHeaderPromo.find('p');

            if(elm.data === 'back'){
                cache.$tickerCounter--;
            }else{
                cache.$tickerCounter++;
            }

            if(promoItems.length <= cache.$tickerCounter){
                cache.$tickerCounter = 0;
            }
            if(cache.$tickerCounter < 0){
                cache.$tickerCounter = promoItems.length-1;
            }
            cache.$topHeaderPromo.find('.ticker-showing').text(cache.$tickerCounter+1);
            promoItems.hide();
            promoItems.eq(cache.$tickerCounter).show();
        },
        initSearchSuggestionsAjax: function(query, process) {
        	var options = {
                'methodType': 'GET',
                'methodData': {'term': query},
                'dataType': 'JSON',
                'url': '/search/autocomplete/SearchBox',
                'isShowLoader': false,
                'cache' : true
            }
            ajaxFactory.ajaxFactoryInit(options, function(response){
                //empty array for autocomplete
				if(response.products.length === 0){
					if($('#analyticsSatelliteFlag').val()==='true'){
					    analytics.analyticsOnPageHeaderSearchListClick();
					}
				}
                var newIndex = -1;
                labels = [];
                $.each(response.suggestions, function(index, data){
                    labels.push({name: data.term});
                });
            	$.each(response.products, function(index, data){
            		map[data.name] = data;
                    labels.push({name: data.name});
                });
                $.each(labels, function(index, data){
                    if(labels[index].name === "See All Results"){
                        newIndex = index
                    }
                });
                if(newIndex !== -1){
                  labels.splice(newIndex, 1);
                }
                labels.splice(9, 0, {name: "See All Results"});
                if(labels.length === 1 && labels[0].name === "See All Results"){
                    labels = [];
                }
                return process(labels);
            });
        },
        initSearchEvent: function(){
			
            cache.$searchInput.typeahead({
                source: function(query, process){
                    $('.simon-header-search .analytics-headerSearch ul').removeClass('hide');
                    globalHeader.initSearchSuggestionsAjax(query, process);
                },
                select: function() {
                    var a = this.$menu.find(".active").data("value");
                    if (this.$element.data("active", a),
                    this.autoSelect || a) {
                        var b = this.updater(a);
                        if(b.name === "See All Results"){
                            b = $('#searchInput').val();
                            b || (b = ""),
                            this.$element.val(this.displayText(b) || b).change(),
                            this.afterSelect(b)
                        }else{
                            b || (b = ""),
                            this.$element.val(this.displayText(b) || b).change()
                        }
                    }else{
                        b = $('#searchInput').val();
                        b || (b = ""),
                        this.$element.val(this.displayText(b) || b).change(),
                        this.afterSelect(b)
                    }
                    return this.hide()
                },
                updater: function(item) {
                    if(item.name !== "See All Results"){
                    	if(map[item.name] !== undefined){
	                        analytics.analyticsOnPageHeaderSearchListClick(map[item.name].code,map[item.name].name);
	                        window.location.href=window.location.origin + map[item.name].url;
                    	}else{
                    		analytics.analyticsOnPageHeaderSearchListClick(null,item.name);
	                        window.location.href=window.location.origin + '/search?text=' + item.name;
                    	}
                    }
                    return item;
                }, 
                afterSelect: function(item){
                    $('form[name=search_form_cm_searchBox]').submit();
					if($('#analyticsSatelliteFlag').val()==='true'){
					    analytics.analyticsOnPageHeaderSearchListClick();
					}
                },
                matcher: function(a) {
                    var b = this.displayText(a);
                    return b;
                },
                minLength: 3,
                items: 10,
                autoSelect: false
             })
        },
        suggestionOverlay: function(e, val){
            var marginTop = cache.$headerContainer.height();
            if(val === 'hide'){
                cache.$blurScreenDiv.removeClass('in menu-is-open');
                cache.$headerSearchContainer.removeClass('dropdown-shown');
            }else{
                cache.$blurScreenDiv.addClass('in menu-is-open');
                cache.$headerSearchContainer.addClass('dropdown-shown');
            }
            cache.$blurScreenDiv.css({
                height: cache.$document.height()-marginTop+'px',
                top: marginTop+'px',
            });
        }
    }
      
    $(function() {
        globalHeader.init();
    });

    return globalHeader;
});