<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true"%>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true"%>
<%@ attribute name="pageCss" required="false" fragment="true"%>
<%@ attribute name="pageScripts" required="false" fragment="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="header"
	tagdir="/WEB-INF/tags/responsive/common/header"%>

<header class="js-mainHeader">
	<div class="navbar navigation navigation--middle js-navigation--middle"
		role="navigation">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-6 col-xs-5">
					<cms:pageSlot position="csn_SitesLogoSlot" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
				<div class="col-md-6 col-xs-7 mobile-mini-bag checkout-header">
					<cms:pageSlot position="csn_SecureLoginSlot" var="feature">
						<cms:component component="${feature}" />
					</cms:pageSlot>
				</div>
			</div>

		</div>
	</div>
</header>