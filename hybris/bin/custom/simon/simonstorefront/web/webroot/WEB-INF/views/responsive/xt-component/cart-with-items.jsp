<div class="cart-checkout-items-order-details">
   <div class="cart-checkout-items">
      <div class="last-call major">Last Call (2)</div>
      <div class="cart-items-container collapse">
         <div class="return-information"><a href="#">View Shipping & Returns Information</a></div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-3">
               <span class="icon-out-stock"></span>
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-1.jpg">
               </a>
            </div>
            <div class="col-md-10 col-xs-9">
               <div class="col-md-7 col-xs-12 no-pad-r">
                  <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                  <div class="out-stock-description">Out Of Stock</div>
                  <div>
                     <span class="item-price">$200.00</span>
                     <span class="item-revised-price">$160.00
                           <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                     </span>
                  </div>
                  <div class="item-discount">20% Off</div>
                  <div class="item-qty">Quantity: 1</div>
                  <div class="item-color">Color: Red</div>
                  <div class="qty-section">
                     <input type="text" title="qty" class="txt-qty" value="1"/>
                  </div>
                  <div class="item-price-qty-wise">$200.00</div>
               </div>
               <div class="col-md-5 col-xs-12 no-pad-r">
                  <div class="edit-remove-container">
                     <a class="item-edit" href="#editCartItemModal" data-toggle="modal" data-target="#editCartItemModal">Edit</a>
                     <a class="item-remove" href="#">Remove</a>
                     <div class="add-to-my-fav">
                        <a href="#">
                           <span class="icon-fav-blank"></span>
                           <span class="fav-text">Add to My Favorites</span>
                        </a>
                     </div>
                     <div class="added-to-fav hide">
                        <span class="icon-fav-added"></span>
                        <span class="added-text">Added!</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-3">
               <a href="#">
                   <img  class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-2.jpg">
               </a>
            </div>
            <div class="col-md-10 col-xs-9">
               <div class="col-md-7 col-xs-12 no-pad-r">
                  <div class="item-description"><a href="#">'Large Le Pliage' Tote</a></div>
                  <div>
                     <span class="item-price">$200.00</span>
                     <span class="item-revised-price">$160.00
                           <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                     </span>
                  </div>
                  <div class="item-discount">20% Off</div>
                  <div class="item-qty">Quantity: 1</div>
                  <div class="item-color">Color: Red</div>
                  <div class="qty-section">
                     <input type="text" title="qty" class="txt-qty" value="1"/>
                  </div>
                  <div class="item-price-qty-wise">$200.00</div>
               </div>
               <div class="col-md-5 col-xs-12 no-pad-r">
                  <div class="edit-remove-container">
                     <a class="item-edit" href="#editCartItemModal" data-toggle="modal" data-target="#editCartItemModal">Edit</a>
                     <a class="item-remove" href="#">Remove</a>
                     <div class="add-to-my-fav hide">
                        <a href="#">
                           <span class="icon-fav-blank"></span>
                           <span class="fav-text">Add to My Favorites</span>
                        </a>
                     </div>
                     <div class="added-to-fav">
                        <span class="icon-fav-added"></span>
                        <span class="added-text">Added!</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-container-left">
                  <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$360.00</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Choose method in Checkout</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Shipping</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="tax-calculation">
                     <a herf="#">Tax calculated during Checkout</a>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$360.00</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$80.00</span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="cart-checkout-items">
      <div class="last-call major">Gap Factory (1)</div>
      <div class="cart-items-container collapse">
         <div class="return-information"><a href="#">View Shipping & Returns Information</a></div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-3">
               <a href="#">
               <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-3.jpg">
               </a>
            </div>
            <div class="col-md-10 col-xs-9">
               <div class="col-md-7 col-xs-12 no-pad-r">
                  <div class="item-description"><a href="#">Print Fringe Scarf</a></div>
                  <div>
                     <span class="item-price">$19.99</span>
                     <span class="item-revised-price">$15.99
                          <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                     </span>
                  </div>
                  <div class="item-discount">20% Off</div>
                  <div class="item-qty">Quantity: 1</div>
                  <div class="item-color">Color: Blue</div>
                  <div class="qty-section">
                     <input type="text" title="qty" class="txt-qty" value="1"/>
                  </div>
                  <div class="item-price-qty-wise">$200.00</div>
               </div>
               <div class="col-md-5 col-xs-12 no-pad-r">
                  <div class="edit-remove-container">
                     <a class="item-edit" href="#editCartItemModal" data-toggle="modal" data-target="#editCartItemModal">Edit</a>
                     <a class="item-remove" href="#">Remove</a>
                     <div class="add-to-my-fav">
                        <a href="#">
                           <span class="icon-fav-blank"></span>
                           <span class="fav-text">Add to My Favorites</span>
                        </a>
                     </div>
                     <div class="added-to-fav">
                        <span class="icon-fav-added"></span>
                        <span class="added-text">Added!</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-container-left">
                     <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$16.98</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Choose method in Checkout</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Shipping</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="tax-calculation">
                     <a herf="#">Tax calculated during Checkout</a>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$16.98</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$2.00</span>
                  </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="cart-checkout-items">
      <div class="last-call major">Nordstrom Rack (2)</div>
      <div class="cart-items-container collapse">
         <div class="return-information"><a href="#">View Shipping & Returns Information</a></div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-3">
               <a href="#">
                  <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-4.jpg">
               </a>
            </div>
            <div class="col-md-10 col-xs-9">
               <div class="col-md-7 col-xs-12 no-pad-r">
                  <div class="item-description"><a href="#">Sterling Silver Diamond & Bar Charm Double Drop Necklace - 0.08 ctw</a></div>
                  <div>
                     <span class="item-price">$36.21</span>
                     <span class="item-revised-price">$28.97
                           <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                     </span>
                  </div>
                  <div class="item-discount">20% Off</div>
                  <div class="item-qty">Quantity: 1</div>
                  <div class="item-color">Color: Crystal</div>
                  <div class="qty-section">
                     <input type="text" title="qty" class="txt-qty" value="1"/>
                  </div>
                  <div class="item-price-qty-wise">$200.00</div>
               </div>
               <div class="col-md-5 col-xs-12 no-pad-r">
                  <div class="edit-remove-container">
                     <a class="item-edit" href="#editCartItemModal" data-toggle="modal" data-target="#editCartItemModal">Edit</a>
                     <a class="item-remove" href="#">Remove</a>
                     <div class="add-to-my-fav">
                        <a href="#">
                           <span class="icon-fav-blank"></span>
                           <span class="fav-text">Add to My Favorites</span>
                        </a>
                     </div>
                     <div class="added-to-fav">
                        <span class="icon-fav-added"></span>
                        <span class="added-text">Added!</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row clearfix row-pad-btm">
            <div class="col-md-2 col-xs-3">
               <a href="#">
               <img class="img-width" alt="" src="/_ui/responsive/simon-theme/images/cart-image-5.jpg">
               </a>
            </div>
            <div class="col-md-10 col-xs-9">
               <div class="col-md-7 col-xs-12 no-pad-r">
                  <div class="item-description"><a href="#">Women's Retro Composite Frame Sunglasses</a></div>
                  <div>
                     <span class="item-price">$112.46</span>
                     <span class="item-revised-price">$89.97
                           <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                     </span>
                  </div>
                  <div class="item-discount">20% Off</div>
                  <div class="item-qty">Quantity: 1</div>
                  <div class="item-color">Color: Havana-Light</div>
                  <div class="qty-section">
                     <input type="text" title="qty" class="txt-qty" value="1"/>
                  </div>
                  <div class="item-price-qty-wise">$200.00</div>
               </div>
               <div class="col-md-5 col-xs-12 no-pad-r">
                  <div class="edit-remove-container">
                     <a class="item-edit" href="#editCartItemModal" data-toggle="modal" data-target="#editCartItemModal">Edit</a>
                     <a class="item-remove" href="#">Remove</a>
                     <div class="add-to-my-fav">
                        <a href="#">
                           <span class="icon-fav-blank"></span>
                           <span class="fav-text">Add to My Favorites</span>
                        </a>
                     </div>
                     <div class="added-to-fav">
                        <span class="icon-fav-added"></span>
                        <span class="added-text">Added!</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="price-container-left">
               <div class="col-md-offset-2 col-md-10 col-xs-offset-3 col-xs-9">
               <div class="price-calculation-container">
                  <div class="price-top-spacing">
                     <span class="estimated-subtotal">Estimated Subtotal</span>
                     <span class="estimated-sub-price">$118.94</span>
                  </div>
                  <div class="ship-top-spacing">
                     <span class="estimated-shipping">Estimated Shipping</span>
                     <span class="estimated-shipping-price">--</span>
                  </div>
                  <div class="choose-method">
                     <a herf="#">Choose method in Checkout</a>
                  </div>
                  <div class="tax-top-spacing">
                     <span class="estimated-tax">Estimated Shipping</span>
                     <span class="estimated-tax-price">--</span>
                  </div>
                  <div class="tax-calculation">
                     <a herf="#">Tax calculated during Checkout</a>
                  </div>
                  <div class="last-call-spacing">
                     <span class="last-call-retailer">Last Call Estimated Total</span>
                     <span class="last-call-price">$118.94</span>
                  </div>
                  <div class="you-save-spacing">
                     <span class="you-save">You Saved</span>
                     <span class="you-save-price">$13.00</span>
                  </div>
               </div>
            </div>
            </div>
         </div>
      </div>
   </div>
   <div class="order-summary-bottom">
         <jsp:include page="cart-order-summary.jsp"></jsp:include>
   </div>
   <!-- Modal -->
   <jsp:include page="cart-edit-item-modal.jsp"></jsp:include>
</div>