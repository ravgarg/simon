<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer"
	tagdir="/WEB-INF/tags/responsive/common/footer"%>

<c:if test="${component.visible}">
	<c:forEach items="${component.navigationNode.children}" var="childLevel1">
		<c:forEach items="${childLevel1.children}" step="${component.wrapAfter}" varStatus="i">
		 <div class="col-md-2 simon-footer-links">
			<c:if test="${component.wrapAfter > i.index}">
				<h6 class="mobile-footer-accordian">${childLevel1.title}</h6>
			</c:if>
				<ul class="mobile-acc-links collapse">
					<c:forEach items="${childLevel1.children}" var="childLevel2"
						begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
						<c:forEach items="${childLevel2.entries}" var="childlink">
							<li data-analytics-linkname="${childlink.item}"><cms:component component="${childlink.item}" evaluateRestriction="true"/></li>
						</c:forEach>
					</c:forEach>
				</ul>
		  </div>
		</c:forEach>
	</c:forEach>
</c:if>