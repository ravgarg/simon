<div class="plp-search-tile">
    <h3 class="major">Trending Items</h3>
    <div class="row product-container product-list initCarouselMobile">
        <div class="item-tile col-md-4 col-xs-6">
            <jsp:include page="global-product-tile.jsp"></jsp:include>
        </div>
        <div class="item-tile col-md-4 col-xs-6">
            <jsp:include page="global-product-tile.jsp"></jsp:include>
        </div>
        <div class="item-tile col-md-4 col-xs-6">
            <jsp:include page="global-product-tile.jsp"></jsp:include>
        </div>
     </div>
</div>
