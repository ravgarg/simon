<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<template:page pageTitle="${pageTitle}">
<c:set var="pageId" value="cm_designersatoz" scope="request"></c:set>
	<div class="designers-az" data-analytics-pagetype="atozdesigners" data-analytics-subtype="atozdesigners" data-analytics-contenttype="informational">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="breadcrumbs col-md-3 hide-mobile">
							<a href="/"><spring:theme code="search.page.breadcrumb.home" /></a>
							<span class="arrow-fwd"></span> <a href="#" class="active">${cmsPage.name}</a>
						</div> 
					</div>
				</div>

				<cms:pageSlot position="csn_leftNavigation_atozDesignersPage"
					var="feature" element="div" class="account-section-content">
					<cms:component component="${feature}" />
				</cms:pageSlot>
				
				<div class="col-md-9">
					<div class="my-designers page-container">
					<h1 class="page-heading major"><spring:theme code="text.designers.a.to.z"></spring:theme><a href="#" class="add-deals hidden-xs" data-toggle="modal" data-target="#moadl1Content" data-key="my-designers"><spring:theme code="text.account.myDesigners.howToAdd"></spring:theme></a></h1>
					<div class="myfavorites-container">
						<a class="add-deals hide-desktop" data-toggle="modal"
							data-target="#moadl1Content" href="#" data-key="my-designers"><spring:theme code="text.account.myDesigners.howToAdd"></spring:theme></a>
						<div class="page-context-links"></div>
						<cms:pageSlot position="csn_aToZDesignersTitleContentSlot"
							var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>

					<cms:pageSlot position="csn_promoBannerContentSlot" var="feature"
						element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					
					<cms:pageSlot position="csn_designersListingSlot_aToZDesigners" var="feature"
						element="div" class="account-section-content">
						<cms:component component="${feature}" />
					</cms:pageSlot>
					
					
					

				</div>
				</div>
			</div>
		</div>
	</div>
</template:page>