<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<!-- Left Navigation Starts -->
<aside class="col-md-3 left-menu-container show-desktop">
	<ul class="left-menu level-0 list-unstyled analytics-plpleftNav" data-analytics-eventtype="side_navigation" data-analytics-eventsubtype="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|side_navigation" data-satellitetrack="link_track">
		<c:choose>
			<c:when test="${ListingPage}">
				<jsp:include page="facetDynamicNavigation.jsp"></jsp:include>
				<jsp:include page="manualNavigation.jsp"></jsp:include>
			</c:when>
			<c:otherwise>
				<jsp:include page="manualNavigation.jsp"></jsp:include>
				<jsp:include page="dynamicNavigation.jsp"></jsp:include>
			</c:otherwise>
		</c:choose>
	</ul>
</aside>

<!-- Left Navigation Ends -->