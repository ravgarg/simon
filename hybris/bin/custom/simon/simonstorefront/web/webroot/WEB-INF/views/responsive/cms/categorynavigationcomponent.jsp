<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/responsive/user"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.ArrayList"%>
<div class="main-menu mobile-mega-menu">
	<div class="mobile-sidebar-wrapper">
		<a href="#" class="active" data-id="main">
			<span class="shop"></span>
			<label> <spring:theme code="text.shop.online.title"/></label>
		</a>
		
		<c:choose>
			<c:when test="${isLoggedIn}">
				<a href="#" data-id="myAccount"> 
					<span class="login"></span> 
					<label><spring:theme code="header.link.account"/></label>
				</a>
				<ul id="myAccount" class="dropdown-menu nav navbar-nav myaccount-dropdown navbar-right">
				    <li class="dropdown">
				    <a href="#" data-toggle="dropdown" class=" favor_icons dropdown-toggle" aria-expanded="false"><spring:theme code="header.link.account"/></a>
				        <ul class="dropdown-menu">
				           <li><a href="/my-account/update-profile"><spring:theme code="text.account.yourAccount"/></a></li>
				           <li><a href="/my-account/orders"><spring:theme code="order.history.header.text"/></a></li>
				           <li><a href="/my-account/address-book"><spring:theme code="text.account.addressBook"/></a></li>
				           <li><a href="/my-account/payment-details"><spring:theme code="text.account.paymentDetails"/></a></li>
				           <li><a href="/my-account/email-preferences"><spring:theme code="text.account.email.preferences"/></a></li>
				           <div class="logoutBtn analytics-logout">
								<a class="btn" href="/logout"><spring:theme code="header.link.logout"/></a>
							</div>
				        </ul>
				    </li>
				</ul>
			</c:when>
			<c:otherwise>
				<a href="#" data-id="login"> 
					<span class="login"></span> 
					<label> <spring:theme code="text.login.signInBox.header.signin"/> </label>
				</a>
				<div id="login" class="dropdown-menu nav-login-flyout">
					<div class="login-top-border"></div>							
						<div class="sign-up-content">
							<div class="size-guide-content">
								<h6 class="major"><spring:theme code="text.loginFlyout.signInBox.header.vipclub"/></h6>
								<p><spring:theme code="text.loginFlyout.signInBox.header.sign.in.now"/></p>
							</div>
							<form action="/j_spring_security_check"
									method="post" class="sm-form-validation bt-flabels js-flabels  analytics-formValidation" data-analytics-type="login" data-analytics-eventtype="login" data-analytics-eventsubtype="login" data-analytics-formtype="login_form" data-analytics-formname="login_overlay_form" data-analytics-formtype="loginform" data-satellitetrack="login"
									data-parsley-validate="validate" id="login-flyout2">
								<div class="sm-form-control">
									<div class="col-md-7 navbar-login">
										<div class="row">
											<div class="sm-input-group">
												<label class="field-label" for="mail"><spring:theme code="text.login.field.label.email.address"/> <span class="field-error" id="error_guestEmailId"></span></label> <input
														type="email" class="form-control" id="mail"
														placeholder="Email Address"
														name="j_username"
														data-parsley-errors-container="#error_guestEmailId"
														type="email" data-parsley-type="email"
														data-parsley-error-message="Please enter your valid email address"
														required />
											</div>
											<div class="sm-input-group">
												<label class="field-label" for="pswd"><spring:theme code="text.login.field.label.password"/> <span
														class="field-error" id="error_pswd2"></span></label> <input
														type="password" autocomplete="off" class="form-control"
														placeholder="Password" id="pswd"
														name="j_password" data-parsley-errors-container="#error_pswd2"
														data-parsley-required-message="Please enter a password"
														data-parsley-required
														data-parsley-pwdstrength-message="Please enter a valid password"
														data-parsley-pwdstrength required />
											</div>
										</div>
									</div>
									<div class="clearfix links-container">
										<a href="https://testpremiumoutlets.simon-ops.com/vip/forgot-password"
											class="forgot-password pull-left"><spring:theme code="text.loginFlyout.signInBox.header.forgot.password"/></a>
									</div>
									<button class="btn"><spring:theme code="text.loginFlyout.signInBox.header.login"/></button>
								</div>
							</form>
							<div class="size-guide-content">
								<h6><spring:theme code="text.loginFlyout.signInBox.header.vip.shoppers.club"/></h6>
								<p><spring:theme code="text.loginFlyout.signInBox.header.join.vip.club.paragraph"/></p>
							</div>
							<button class="btn secondary-btn black analytics-genericLink" data-analytics-eventsubtype="login_overlay" data-analytics-linkplacement="login_overlay" data-analytics-linkname="join-now"
								onclick="location.href='/join-now'"><spring:theme code="text.loginFlyout.signInBox.header.join.now"/></button>
						</div>
				</div>
			</c:otherwise>
		</c:choose>

		<a href="#" data-id="favorite"> 
			<span class="favor"></span> 
			<label> <spring:theme code="header.link.favorites"/> </label>
		</a> 
		<ul id="favorite" class="dropdown-menu nav navbar-nav myaccount-dropdown navbar-right">
		    <li class="dropdown">
		    <a href="#" data-toggle="dropdown" class=" favor_icons dropdown-toggle" aria-expanded="false"><spring:theme code="header.link.favorites"/></a>
		        <ul class="dropdown-menu">
		           <li><a href="/my-account/my-premium-outlets"><spring:theme code="myPremiumOutletKey"/></a></li>
		           <li><a href="/my-account/my-center"><spring:theme code="text.account.mycenter.title"/></a></li>
		           <li><a href="/my-account/my-stores"><spring:theme code="text.account.myStores.title"/></a></li>
		           <li><a href="/my-account/my-favorites"><spring:theme code="myFavorites.title.text"/></a></li>
		           <li><a href="/my-account/my-designers"><spring:theme code="text.account.myDesigners.title"/></a></li>
		           <li><a href="/my-account/my-deals"><spring:theme code="text.account.myDeals.title"/></a></li>
		           <li><a href="/my-account/my-size"><spring:theme code="text.account.my.sizes"/></a></li>
		        </ul>
		    </li>
		</ul>

		<a href="#"> 
			<span class="find-a-center"></span> 
			<label> <spring:theme code="text.find.a.center.title"/> </label>
		</a> 
		<a href="#" data-id="support"> 
			<span class="support">
			</span> <label> <spring:theme code="header.link.support"/> </label>
		</a>
		<ul id="support" class="dropdown-menu nav navbar-nav myaccount-dropdown navbar-right">
		    <li class="dropdown">
		        <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true"><spring:theme code="header.link.support"/></a>
		        <ul class="dropdown-menu">
		            <li><a href="#"><spring:theme code="text.customer.care.title"/></a></li>
		           <li><a href="#"><spring:theme code="text.shipping.and.returns.title"/></a></li>
		           <li><a href="/faq"><spring:theme code="text.faqs.title"/></a></li>
		           <li><a href="/contact-us"><spring:theme code="text.contact.us.title"/></a></li>
		           <li><a href="/customer-service/track-order"><spring:theme code="text.track.my.order.title"/></a></li>
		           </ul>
		    </li>
		</ul>

	</div>
	

	<nav>
	
		<ul id="main">
			<c:forEach items="${topNavComponent}" var="L1childnodes">
				<c:forEach items="${L1childnodes}" var="L1childnode">
				<c:set value="${L1childnode.key}" var="childNodeValue" />
					<c:choose>
						<c:when test="${childNodeValue.target eq 'newWindow'}">
							<c:set var="childNodeValueTarget" value="_blank" />
						</c:when>
						<c:otherwise>
							<c:set var="childNodeValueTarget" value="_self" />
						</c:otherwise>
					</c:choose>
					<li class="mymobilemenu"><a target="${childNodeValueTarget}" href="#" class="mymobilemenu">${childNodeValue.name}</a>
							<ul>
							<c:set value="${L1childnode.value}" var="childNodesL2" />
								<c:set var="level2Size" value="0" 	/>
								<c:set var="counter" value="0" 	/>
								<c:forEach items="${childNodesL2}" var="childnodeL2">
		                           <c:set var="l2URL">
										<c:url value="${childnodeL2.url}" />
									</c:set>
									<c:choose>
										<c:when test="${childnodeL2.target eq 'newWindow'}">
									    	<c:set var="childnodeL2Target" value="_blank" />
										</c:when>
									    <c:otherwise>
											<c:set var="childnodeL2Target" value="_self" />
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${childnodeL2.isIsL2()}">
											<c:set var="counter" value="0" 	/>
											<li class="cat-heading">
											<c:choose>
												<c:when test="${childnodeL2.getL2Size() gt 0}">
													<a  target="${childnodeL2Target}" class="open-accordian" href="${l2URL} ">${childnodeL2.name}</a>
													<c:set var="level2Size" value="${childnodeL2.getL2Size()+1}" 	/>
													<div class="mobile-accordian collapse">
													
												</c:when>
												<c:otherwise>
													<a  target="${childnodeL2Target}" href="${l2URL} ">${childnodeL2.name}</a>
													<c:set var="level2Size" value="0" 	/>
													</li>
												</c:otherwise>
											</c:choose>
										</c:when>
										<c:otherwise>
											<c:set var="counter" value="${counter+1}" 	/>
											<a target="${childnodeL2Target}" href="${l2URL}">${childnodeL2.name}</a>
											<c:choose>
												<c:when test="${level2Size eq counter}">
													</div></li>
												</c:when>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</ul></li>
			</c:forEach>
			</c:forEach>
		</ul>

	</nav>
</div>
<div class="container simon-md-mega-menu">
	<nav class="navbar">
		<div class="">
			<div class="row">
				<div id="navbar" class="navbar-collapse collapse analytics-headerNavigation" data-analytics-eventtype="global_navigation" data-analytics-eventsubtype="header_navigation_dropdown" data-analytics-linkplacement="header_logo|header_navigation" data-satellitetrack="link_track">
					<ul class="nav navbar-nav">
						<li class="dropdown top-menu-open hide-mobile">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle">
								<span class="md-open-icon menu-open pull-left"></span>
								<span class="md-open-icon menu-close pull-left"></span>
								<span class="menu-label-shop pull-left"><spring:theme code="text.shop.online.title"/></span>
								<span class="menu-label-close pull-left"><spring:theme code="text.close.title"/></span>
							</a>
							<ul class="dropdown-menu desktop-mega-menu">
								<li class="grid-demo">
									<div class="col-sm-3 menu-level-1">
										<ul>
											<c:forEach items="${topNavComponent}" var="L1childnodes">
												<c:forEach items="${L1childnodes}" var="L1childnode">
													<c:set value="${L1childnode.key}" var="childNodeValue" />
													<c:choose>
														<c:when test="${childNodeValue.target eq 'newWindow'}">
													    	<c:set var="childNodeValueTarget" value="_blank" />
														</c:when>
													    <c:otherwise>
															<c:set var="childNodeValueTarget" value="_self" />
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${empty childNodeValue.url}">
															<li>
																<a target="${childNodeValueTarget}" data-analytics-linkname="" href="javascript:void(0)" data-cat-id="#${childNodeValue.uid}">${childNodeValue.name}</a>
															</li>
														</c:when>
														<c:otherwise>
															<li>
																<a target="${childNodeValueTarget}" data-analytics-linkname="${childNodeValue.name}" href="<c:url value = "${childNodeValue.url}"/>" data-cat-id="#${childNodeValue.uid}">${childNodeValue.name}</a>
															</li>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:forEach>
										</ul>
									</div>
									<div class="col-sm-9 menu-borderleft">
										<div class="row clearfix">

											<c:forEach items="${topNavComponent}" var="nodesList">
												<c:forEach items="${nodesList}" var="nodeMap">
													<c:set var="l1Node" value="${nodeMap.key}" />
													<div id="${l1Node.uid}" class="desktop-cat-panel">
														<div class="mega-menu-links clearfix">
															<div class="clearfix">
																<c:set var="totalcatCount" value="0" />
																<c:set var="childNodes" value="${nodeMap.value}" />
																	<div class="col-sm-3">
																<c:forEach items="${childNodes}" var="childNode">
																		<c:set var="l2URL">
																			<c:url value="${childNode.url}" />
																		</c:set>
																		<ul>
																			<c:choose>
																				<c:when test="${childNodeValue.target eq 'newWindow'}">
																			    	<c:set var="childnodeL2Target" value="_blank" />
																				</c:when>
																			    <c:otherwise>
																					<c:set var="childNodeTarget" value="_self" />
																				</c:otherwise>
																			</c:choose>
																			<c:choose>
																				<c:when test="${childNode.isIsL2()}">
																				<c:choose>
																				<c:when test="${empty l2URL}">
																					<li class="cat-heading"> 
																					${childNode.name}
																					</li>
																				</c:when>
																				<c:otherwise>
																				     <li class="cat-heading"> 
																						<a target="${childNodeTarget}" data-analytics-linkname="${childNode.name}" href="${l2URL}">${childNode.name}</a>
																					</li>
																				</c:otherwise>
																					</c:choose>
																				</c:when>
																				<c:otherwise>
																					<li>
																						<a target="${childNodeTarget}" data-analytics-linkname="${childNode.name}" href="<c:url value ="${childNode.url}"/>">${childNode.name}</a>
																					</li>
																				</c:otherwise>
																			</c:choose>
																			<c:set var="totalcatCount" value="${totalcatCount+1}" />
																			
																			<c:if test="${totalcatCount eq 15}">
																				</ul>
																				</div>
																				<div class="col-sm-3">
																				<ul>
																				<c:set var="totalcatCount" value="0" />
																			</c:if>
																		</ul>
																</c:forEach>
																	</div>
															</div>
														</div>
													<c:if test="${not empty l1Node.promoBanner}">
													<div class="clearfix mega-menu-promo">
													<c:set var="subheadingText" value="${l1Node.promoBanner.subheadingText}"  scope="request"></c:set>
													<c:set var="titleText" value="${l1Node.promoBanner.titleText}"  scope="request"></c:set>
													<c:set var="link" value="${l1Node.promoBanner.link}"  scope="request"></c:set>
													<c:set var="backgroundImage" value="${l1Node.promoBanner.backgroundImage}"  scope="request"></c:set>
												    <jsp:include page="promobanner2clickableimagewithtextandsinglectacomponent.jsp"></jsp:include>
											        </div>
											        </c:if>
													</div>
												</c:forEach>
											</c:forEach>
											
										</div>
									</div>
								</li>
							</ul>
						</li>
						
							<cms:pageSlot position="csn_SearchBoxCompSlot" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>

							<li class="menu-fav-links login-area hide-mobile dropdown">
							<cms:pageSlot position="csn_FavoritesSlot" var="feature">  								
									<cms:component component="${feature}" />
								</cms:pageSlot>	
							</li>
							
							<li class="pull-right menu-fav-links login-area hide-mobile dropdown">
							<c:choose>
  								<c:when test="${isLoggedIn}">				  								
  								<cms:pageSlot position="csn_myAccountSubmenuContentSlot" var="feature">  								
									<cms:component component="${feature}" />
								</cms:pageSlot>		
  							</c:when>
  							<c:otherwise>
  								<a href="#" class="header-login dropdown-toggle analytics-loginFlyOut" data-toggle="dropdown"><spring:theme code="header.mobile.link.login"/></a>
								<div class="dropdown-menu nav-login-flyout">
								<div class="login-top-border"></div>
									<user:loginFlyout />
								</div>
							</c:otherwise>
							</c:choose>
							</li>
					</ul>
				</div>
			</div>
		</div>
	</nav>
</div>