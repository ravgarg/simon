<div class="modal fade" id="sizeGuideModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Size Guide</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>

                <div class="size-guide">
                    <div class="list-dropdown">
                        <select>
                            <option>
                                Select Inseam Length
                            </option>
                        </select>
                    </div>
                    
                    <div class="scroll-container">
                        <div class="size-guide-content">
                            <h6>Women's Size Guide</h6>
                            <p>SEO copy lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <p><a href="#">Click here</a> for measuring instructions.<br />
                            All measurments are in inches unless otherwise indicated.</p>
                        </div>

                        <div class="size-guide-content">
                            <h6>Classic Size Guide</h6>
                            <table border="0" cellpadding="0" cellspacing="0" class="size-table">
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>P</td>
                                        <td colspan="2">S</td>
                                        <td colspan="2">M</td>
                                        <td colspan="2">L</td>
                                        <td>XL</td>
                                    </tr>
                                    <tr>
                                        <td>Size</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>8</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <td>Bust</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>8</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <td>Waist</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>8</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <td>Hip</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>8</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                    <tr>
                                        <td>China</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>8</td>
                                        <td>10</td>
                                        <td>12</td>
                                        <td>14</td>
                                        <td>16</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer clearfix">
                        <button class="btn pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>

                
            </form>
        </div>
    </div>
</div>