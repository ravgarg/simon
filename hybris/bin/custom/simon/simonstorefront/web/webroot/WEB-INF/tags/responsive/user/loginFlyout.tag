<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<div class="sign-up-content">
	<div class="size-guide-content">
		<h6 class="major"><spring:theme code="text.loginFlyout.signInBox.header.vipclub" /></h6>
		<p><spring:theme code="text.loginFlyout.signInBox.header.sign.in.now" /></p>
	</div>
	<form action="/j_spring_security_check"
			method="post" class="sm-form-validation bt-flabels js-flabels analytics-formValidation" data-parsley-validate="validate" data-analytics-type="login" data-analytics-eventtype="login" data-analytics-eventsubtype="login" data-analytics-formtype="login_form" data-analytics-formname="login_flyout" data-analytics-formtype="loginform" data-satellitetrack="login"
			data-parsley-validate="validate" id="login-flyout">
		<div class="sm-form-control">
			<div class="col-md-7 navbar-login">
				<div class="row">
					<div class="sm-input-group">
						<label class="field-label" for="e-mail"><spring:theme code="text.login.field.label.email.address" /> <span class="field-error" id="error_loginguestMailId"></span></label> <input
								type="email" class="form-control" id="e-mail"
								placeholder="<spring:theme code="text.login.field.place.holder.email.address" />"
								name="j_username"
								data-parsley-errors-container="error_loginguestMailId"
								type="email" data-parsley-type="email"
								data-parsley-error-message="<spring:theme code="text.login.field.error.message.email.address" />"
								required />
					</div>
					<div class="sm-input-group">
						<label class="field-label" for="pswrd"><spring:theme
									code="text.login.field.label.password" /> <span
								class="field-error" id="error_loginpswd"></span></label> <input
								type="password" autocomplete="off" class="form-control" id="pswrd"
								placeholder="<spring:theme code="text.login.field.place.holder.password" />"
								name="j_password" data-parsley-errors-container="#error_loginpswd"
								data-parsley-required-message="<spring:theme code="text.login.field.error.message.password" />"
								data-parsley-required
								data-parsley-pwdstrength-message="<spring:theme code="text.login.field.error.message.valid.password" />"
								data-parsley-pwdstrength required />
					</div>
				</div>
			</div>
			<div class="clearfix links-container">
				<a href="${forgotPasswordLink}"
					class="forgot-password pull-left"><spring:theme code="text.loginFlyout.signInBox.header.forgot.password" /></a>
			</div>
			<button class="btn"><spring:theme code="text.loginFlyout.signInBox.header.login" /></button>
		</div>
	</form>
	<div class="size-guide-content">
		<h6><spring:theme code="text.loginFlyout.signInBox.header.vip.shoppers.club" /></h6>
		<p><spring:theme code="text.loginFlyout.signInBox.header.join.vip.club.paragraph" /></p>
	</div>
	<button class="btn secondary-btn black analytics-genericLink" data-analytics-eventsubtype="login_overlay" data-analytics-linkplacement="login_overlay" data-analytics-linkname="join-now"
		onclick="location.href='/join-now'"><spring:theme code="text.loginFlyout.signInBox.header.join.now" /></button>
</div>