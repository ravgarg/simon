<div class="container">
	<div class="row">
		<div class="col-md-12">
			<jsp:include page="myfavorites-breadcurmb.jsp"></jsp:include>
		</div>
		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="myfavorites-left-nav.jsp"></jsp:include>
		</aside>
		<div class="col-md-9">
			<jsp:include page="myfavorites-top-heading.jsp"></jsp:include>
			<jsp:include page="myfavorites-content.jsp"></jsp:include>
		</div>
	</div>
</div>