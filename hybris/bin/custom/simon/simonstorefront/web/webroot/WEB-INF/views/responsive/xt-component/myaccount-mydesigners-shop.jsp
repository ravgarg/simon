<div class="the-designer-shop">
		<a href="#">
			<picture>
			<source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/HalfYearlySaleMob.png">
			<img class="img-responsive" src="/_ui/responsive/simon-theme/images/1designShop.jpg">
		</picture>
			<div class="content-container">
				<h1 class="display-medium major">the designer shop</h1>
				 <div class="designer-banner-text">Calvin Klein, Tommy Hilfinger & More</div>
				 <span class="shopnow major">shop now <span>&rsaquo;</span></span> 
			</div>
		</a>
	</div>