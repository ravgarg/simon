define(['ajaxFactory','utility','viewportDetect'],
    function(ajaxFactory,utility,viewportDetect) {
        'use strict';
        var cache = {
				$document: $(document),
				$showmore: $('.show-more'),
				$showTotalChar : 40, 
				$showChar : "See More +", 
				$hideChar : "See Less -"
			},
            showMore = {
                init: function() {
                    this.initVariable();
                    this.initEvents();
                },
                initVariable: function(){
                    
                },                
                
                initEvents: function() { 
				
					if(viewportDetect.lastClass === 'small'){
						cache.$showmore.each(function() {
							var content = $(this).text();
							if (content.length > cache.$showTotalChar) {
								var con = content.substr(0, cache.$showTotalChar);
								var hcon = content.substr(cache.$showTotalChar, content.length - cache.$showTotalChar);
								var txt= con +  '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + cache.$showChar + '</a></span>';
								$(this).html(txt);
							}
						});
						
						$(document)
							.off('click.showmoretext')
							.on('click.showmoretext','.showmoretxt',showMore.showMoreTextHandler);
					}
					
                },
				showMoreTextHandler: function(e){
					e.preventDefault();
					
					if ($(this).hasClass("show-less")) {
						$(this).removeClass("show-less");
						$(this).text(cache.$showChar);
					} else {
						$(this).addClass("show-less");
						$(this).text(cache.$hideChar);
					}
					$(this).parent().prev().toggle();
					$(this).prev().toggle();
					
					return false;
				}
            };
    
        $(function() {
        	showMore.init();
        });
       return showMore;
    });