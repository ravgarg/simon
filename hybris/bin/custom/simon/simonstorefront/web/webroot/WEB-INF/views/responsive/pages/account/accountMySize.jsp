<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="page-context-links">
	<p>
		<spring:theme code="text.account.profile.size.sample.content" />
	</p>
</div>


<div class="mysizes-container tab-accordian analytics-mysize analytics-myPages">
	<ul class="nav nav-tabs mysizes-tabs major show-desktop">
		<c:forEach var="entry" items="${categoryToCategoryMap}">
			<li><a href="#${entry.key}" data-analyticstabs="${entry.key}" data-toggle="tab">${entry.key}</a></li>
		</c:forEach>
	</ul>


	<div class="tab-content mysizes-tabscontent">
		<c:forEach var="entry" items="${categoryToCategoryMap}">
			<div class="tab-pane" id="${entry.key}">
				<div class="show-mobile heading-mobile"><a href="#${entry.key}" data-toggle="tab">${entry.key}</a></div>
				<div class="panel panel-default">
					<div class="panel-body">
						<c:forEach var="subCategoryData" items="${entry.value}">

							<c:set var="subCategorySizeDataList"
								value="${categoryCodeToSizeMap.get(subCategoryData.code)}" />

							<c:set var="selectedSizeData" value=""></c:set>
							<c:forEach var="sizeData" items="${ subCategorySizeDataList}">

								<c:if test="${sizeData.isSelected eq true}">
									<c:set var="selectedSizeData" value="${sizeData}"></c:set>

								</c:if>
							</c:forEach>


							<ul class="list-group item-listing">
								<li class="list-group-item row">
									<div class="col-md-10 col-xs-12 no-pad">
										<h6 class="list-group-item-heading">${subCategoryData.name['en']}</h6>
										<c:if test="${not empty selectedSizeData}">
											<p class="list-group-item-text"> <c:if test="${not empty selectedSizeData.operatorSize}">${selectedSizeData.operatorSize} - </c:if> ${selectedSizeData.value}
												<spring:theme code="text.account.profile.size.country.code.bracket" />
											</p>
										</c:if>
									</div>

									<div class="col-md-2 col-xs-12 no-pad">

										<c:choose>
											<c:when test="${empty selectedSizeData}">

												<a href="#" class="btn btn-width major analytics-genericLink" data-analytics-eventsubtype="${subCategoryData.code}" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|${subCategoryData.code}" data-analytics-linkname="${subCategoryData.code}" data-toggle="modal"
													data-target="#editSizeModal" id="${subCategoryData.code}">
													<spring:theme code="text.account.profile.size.add.button" />
												</a>
											</c:when>
											<c:otherwise>

												<a href="#" class="btn secondary-btn btn-width black major edit-btn  analytics-genericLink" data-analytics-eventsubtype="${subCategoryData.code}" data-analytics-linkplacement="<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>|${subCategoryData.code}" data-analytics-linkname="${subCategoryData.code}"
													data-toggle="modal" data-target="#editSizeModal"
													id="${subCategoryData.code}" selectedCode="12"> <spring:theme
														code="text.account.profile.size.edit.button" />

												</a>
											</c:otherwise>
										</c:choose>
									</div>

								</li>
							</ul>
						</c:forEach>
					</div>
				</div>

			</div>
		</c:forEach>
	</div>
</div>



<div class="modal fade editsize-modal" id="editSizeModal" role="dialog">
	<div class="modal-dialog">

		<c:forEach var="entry" items="${categoryCodeToSizeMap}">
		
		<c:set var="selectedSizeDataCode" value=""></c:set>
		<c:forEach var="sizeData" items="${entry.value}">
	
			<c:if test="${sizeData.isSelected eq true}">
				<c:set var="selectedSizeDataCode" value="${sizeData.code}"></c:set>
			</c:if>
		</c:forEach>
		

			<div class="modal-content" id="${entry.key}">
			<c:url value="/my-account/updateMySize" var="updateMySizeUrl" />
			<form method="post" action="${updateMySizeUrl}" id="mySizeModal">
				<div class="modal-header">

					<c:forEach var="subCategory"
						items="${categoryToCategoryMap.get(entry.value.get(0).baseCategoryName)}">
						<c:if test="${subCategory.code eq entry.key }">
							<h5 class="modal-title">${subCategory.name['en']}&nbsp<spring:theme code="text.account.profile.size" /></h5>
						</c:if>
					</c:forEach>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body clearfix">
					<div class="size-guide-content">
						<h6>${entry.value.get(0).baseCategoryName} <spring:theme code="text.account.profile.size.guides.content" /></h6>
					</div>

					<div class="fixedTable row" id="mysizes-data">
						<aside class="fixedTable-sidebar col-xs-2">
							<table class="table">
								<tbody>
                                   <c:set var="operatorSize" value="${entry.value.get(0).operatorSize}"></c:set>
                                   <c:choose>
                                   <c:when test="${empty operatorSize}">
                                   <tr>
                                          <td class="noopratorsizes"></td>
                                   </tr>
                                          <tr>
                                                 <td><spring:theme code="text.account.profile.size.country.code" /></td>
                                          </tr>
                                          <tr>
                                                 <td><spring:theme code="text.account.profile.size.pref" /></td>
                                          </tr>
                                   </c:when>
                                   <c:otherwise>
                                   <tr>
                                          <td class="opratorsizes"></td>
                                   </tr>
                                          <tr>
                                                 <td><spring:theme code="text.account.profile.size.country.code" /></td>
                                          </tr>
                                          <tr>
                                                 <td><spring:theme code="text.account.profile.size.pref" /></td>
                                          </tr>
                                   </c:otherwise>
                                   </c:choose>  
                                   </tbody>

							</table>
						</aside>
						<div class="fixedTable-body col-xs-10">
							<table class="table size-table">
								<tbody>
							
									<tr>
										<c:forEach var="sizeData" items="${entry.value}">
											<td>${sizeData.operatorSize}</td>
										</c:forEach>
									</tr>
									<tr>
										<c:forEach var="sizeData" items="${entry.value}">
											<td>${sizeData.value}</td>
										</c:forEach>
									</tr>

									<tr class="radio-content">

										<c:forEach var="sizeData" items="${entry.value}">

											<td>
												<div class="sm-input-group sizeRadio">
													<input type="radio" id="${sizeData.code}" name="name"  <c:if test="${sizeData.isSelected eq true}">checked="checked"</c:if> value="${sizeData.value}">
												</div>
											</td>

										</c:forEach>
									</tr>
								</tbody>
							</table>
						</div>
					</div>


				</div>
				
				<div class="modal-footer clearfix">
				<input type="hidden" name="oldCode" value="${selectedSizeDataCode}">
				<input type="hidden" name="CSRFToken" value="${CSRFToken}">
				<input type="hidden" name="code">
					<button class="btn btn-add pull-left btn-width save-data analytics-genericLink" data-analytics-eventsubtype="my-size" data-analytics-linkplacement="my-size" data-analytics-linkname="save" data-url="${updateMySizeUrl}"><spring:theme code="text.account.profile.size.save" /></button>
					<button class="btn secondary-btn black pull-left btn-width"
						data-dismiss="modal"><spring:theme code="text.account.profile.size.add.cancel" /></button>
				</div>
		
				</form>
			</div>

		</c:forEach>

	</div>
</div>
