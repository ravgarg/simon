<p class="info-text"><span class="more">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</span></p>
<a href="" class="how-to-add-store hide-desktop ">How to Add Stores</a>

<section class="right-section">
    <button class="btn block secondary-btn black show-mobile">Categories</button>

    <!-- Start First section: with 0 Stores -->
    <!-- <div class="my-store no-stores">
        <h1 class="page-heading">
            <span>My Stores</span>
        </h1>
        <p class="about-store">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.</p>
        <h4>Lisa, you have no stores saved</h4>
        <p class="user-msg">My Stores are a great way to keep track of your most-loved stores and share with friends.</p>
        <div class="store-holder clearfix hide">           
            do not create this div if Stroes are empty
        </div>
        <button class="btn add-store">Add Stores</button>
    </div> -->
    <!-- End First section: with 0 Stores -->

    <!-- Start First section: With Stores -->
    <div class="my-store has-stores">
        <h1 class="page-heading">
            <span>My Stores</span>
        </h1>
        <!-- <p class="about-store">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce pellentes risus in enim porta aliquam aenean at.</p>
        <p class="user-msg">My Stores are a great way to keep track of your most-loved stores and share with friends.</p> -->

        <div class="store-holder clearfix">
            <%-- will loop this component: Start --%>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon active"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <%-- will loop this component: End --%>

            <%-- Repeated above box: Start --%>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-3-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-3-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <%-- Repeated above box: End --%>            
        </div>
        <!-- <button class="btn add-store">Add Stores</button> -->
    </div>
    <!-- End First section: With Stores -->

    <!-- Start 2nd section: Featured Stores -->
    <div class="my-store feature-store">
        <h2 class="section-heading">
            <span>All Stores</span>
        </h2>
        <div class="store-holder clearfix">
            <%-- will loop this component: Start --%>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon active"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <%-- will loop this component: End --%>

            <%-- Repeated above box: Start --%>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-3-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-3-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-2-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-2.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <div class="store col-md-4">
                <div class="box-background">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-mob.jpg"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1.jpg'/>
                    </picture>
                </div>
                <div class="fav-the-store">
                    <span class="fav-icon"></span>
                </div>
                <div class="center-content">
                    <picture>
                        <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/store-1-logo.png"/>
                        <img 
                            class="img-responsive" 
                            alt="Store Name" 
                            src='/_ui/responsive/simon-theme/images/store-1-logo.png'/>
                    </picture>
                </div>
                <div class="shop-now">
                    <a href="#">Shop Now</a>
                </div>
            </div>
            <%-- Repeated above box: End --%>            
        </div>
    </div>
    <!-- End 2nd section: Featured Stores -->
</section>





<!-- ---------------------------------------------- Show More/Less Functionality - TO BE REMOVED AFTER INTEGRATION ----------------------------------------------------------- -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>

		$(document).ready(function() {
		    // Configure/customize these variables.
		    var showChar = 33;  // How many characters are shown by default
		    var ellipsestext = "...";
		    var moretext = "See More +";
		    var lesstext = "See Less -";
		    
		
		    $('.more').each(function() {
		        var content = $(this).html();
		 
		        if(content.length > showChar) {
		 
		            var c = content.substr(0, showChar);
		            var h = content.substr(showChar, content.length - showChar);
		 
		            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
		 
		            $(this).html(html);
		        }
		 
		    });
		 
		    $(".morelink").click(function(){
		        if($(this).hasClass("less")) {
		            $(this).removeClass("less");
		            $(this).html(moretext);
		        } else {
		            $(this).addClass("less");
		            $(this).html(lesstext);
		        }
		        $(this).parent().prev().toggle();
		        $(this).prev().toggle();
		        return false;
		    });
		});

</script>

