<div class="payment-method">
	<div class="container">
		<div class="row">
			<div class="col-md-12 hide-mobile">
				<jsp:include page="payment-method-breadcrumb.jsp"></jsp:include>
			</div>
			<aside class="col-md-3 left-menu-container show-desktop">
				<jsp:include page="plp-left-nav.jsp"></jsp:include>
			</aside>
			<div class="col-md-9">
				<button type="button" class="btn secondary-btn black my-account hide-desktop col-xs-12">My Account</button>
				<jsp:include page="payment-method-top-heading.jsp"></jsp:include>
				<jsp:include page="payment-method-heading-text.jsp"></jsp:include>
				<jsp:include page="payment-method-modal.jsp"></jsp:include>
				<jsp:include page="payment-method-delete-modal.jsp"></jsp:include>
			</div>
		</div>
	</div>
</div>