<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ attribute name="pageTitle" required="false" rtexprvalue="true" %>
<%@ attribute name="metaDescription" required="false" %>
<%@ attribute name="metaKeywords" required="false" %>
<%@ attribute name="pageCss" required="false" fragment="true" %>
<%@ attribute name="pageScripts" required="false" fragment="true" %>
<%@ attribute name="breadcrumbs" required="false" type="java.util.List"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/shared/analytics" %>
<%@ taglib prefix="addonScripts" tagdir="/WEB-INF/tags/responsive/common/header" %>
<%@ taglib prefix="generatedVariables" tagdir="/WEB-INF/tags/shared/variables" %>
<%@ taglib prefix="debug" tagdir="/WEB-INF/tags/shared/debug" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="htmlmeta" uri="http://hybris.com/tld/htmlmeta"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="checkout" tagdir="/WEB-INF/tags/responsive/checkout" %>
<%@ taglib prefix="analytics" tagdir="/WEB-INF/tags/responsive/analytics"%>
<!DOCTYPE html>
<html lang="${currentLanguage.isocode}">
<head>
	<title>
		${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : 'Shop Premium Outlet'}
	</title>
	
	<%-- Meta Content --%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">

	<%-- Additional meta tags --%>
	<htmlmeta:meta items="${metatags}"/>
	<c:if test="${canonicalUrl != 'NA'}"><link rel="canonical" href="${canonicalUrl}" /></c:if>
	
	<%-- Favourite Icon --%>
	<spring:theme code="img.favIcon" text="/" var="favIconPath"/>
	<link rel="apple-touch-icon" sizes="57x57" href="${originalContextPath}${favIconPath}/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="${originalContextPath}${favIconPath}/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="${originalContextPath}${favIconPath}/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="${originalContextPath}${favIconPath}/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="${originalContextPath}${favIconPath}/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="${originalContextPath}${favIconPath}/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="${originalContextPath}${favIconPath}/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="${originalContextPath}${favIconPath}/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="${originalContextPath}${favIconPath}/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="${originalContextPath}${favIconPath}/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="${originalContextPath}${favIconPath}/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${originalContextPath}${favIconPath}/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="${originalContextPath}${favIconPath}/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="${originalContextPath}${favIconPath}/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	
	<%-- <spring:theme code="img.favIcon" text="/" var="favIconPath"/>
    <link rel="shortcut icon" type="image/x-icon" media="all" href="${originalContextPath}${favIconPath}" /> --%>

	<%-- CSS Files Are Loaded First as they can be downloaded in parallel --%>
	<template:styleSheets/>

	<%-- Inject any additional CSS required by the page --%>
	<jsp:invoke fragment="pageCss"/>
	<generatedVariables:generatedVariables/>
	<script type="text/javascript">
	 window.digital_data ={
		  "event": {
			"type": "page_load",
			"sub_type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>"
		  },
		  "page": {
			"type": "<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>",
			"content_type": "<c:if test="${not empty content_type}">${fn:toLowerCase(content_type)}</c:if>",
			"domain": "${domain}",
			<c:choose><c:when test="${not empty breadcrumbs}"><c:forEach items="${breadcrumbs}" var="breadcrumb" varStatus="status">
			"section_${status.count}":"${fn:escapeXml(breadcrumb.name)}",
			</c:forEach></c:when><c:otherwise>"section_1":"<c:if test="${not empty pageType}">${fn:toLowerCase(pageType)}</c:if>",</c:otherwise></c:choose>
			"path": "${path}",
			"language": "${currentLanguage.isocode}",
			"title": "${not empty pageTitle ? pageTitle : not empty cmsPage.title ? cmsPage.title : 'Accelerator Title'}"
		  },
		  "user": {
			"status": "<c:choose><c:when test="${isLoggedIn}">register_user</c:when><c:otherwise>guest</c:otherwise></c:choose>",
			"id": "<c:choose><c:when test="${isLoggedIn}">${user}</c:when><c:otherwise>0</c:otherwise></c:choose>"
		  }
		};
	 
	</script>
	
	<input type="hidden" id="analyticsSatelliteFlag" value="${jalosession.tenant.config.getParameter('analytics.satellite.enabled.Flag')}"/>
	
</head>
<c:set var="styleId" scope="session" value="${pageStyleId}${' '}"/>

<body class="${styleId} ${pageBodyCssClasses} ${cmsPageRequestContextData.liveEdit ? ' yCmsLiveEdit' : ''} language-${currentLanguage.isocode}">
	<input type="hidden" id="analyticsLoginSuccessFlag" value="${loginSuccessMessageCode}" />
	<%-- Inject the page body here --%>
	<jsp:doBody/>
	<form name="accessiblityForm">
		<input type="hidden" id="accesibility_refreshScreenReaderBufferField" name="accesibility_refreshScreenReaderBufferField" value=""/>
	</form>
	<div id="ariaStatusMsg" class="skip" role="status" aria-relevant="text" aria-live="polite"></div>

	<%-- Load JavaScript required by the site --%>
	<template:javaScript/>
	<input type="hidden" id ="analyticsURL" value = "${scriptUrl}"/>

	<%-- Inject any additional JavaScript required by the page --%>
	<jsp:invoke fragment="pageScripts"/>

	<%-- Inject CMS Components from addons using the placeholder slot--%>
	<addonScripts:addonScripts/>

</body>

<debug:debugFooter/>

</html>

