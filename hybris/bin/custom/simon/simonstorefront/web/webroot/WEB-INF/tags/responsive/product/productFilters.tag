<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="searchPageData" required="true" type="de.hybris.platform.commerceservices.search.pagedata.SearchPageData" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


				<!-- Filters Starts -->
				<div class="plp-filters hide-mobile">
					<div class="row filters">
						
							<product:productFilterDropDown searchPageData="${searchPageData}" />					

						<div class="col-md-3 clear-all">
							<c:if test="${not empty searchPageData.breadcrumbs}">

								<c:url value="${searchPageData.currentQuery.clearAllUrl}"
									var="clearALlQueryUrl" />
									<c:if test="${cmsPage.uid eq 'designerListPage'}">
										<c:set var="clearALlQueryUrl" value="${clearALlQueryUrl}?q=${designer.code}" />
									</c:if>
								<a href="${clearALlQueryUrl}" class="clearAllFilter"><spring:theme
										code='plp.facet.clear.all' /> </a>

							</c:if>
						</div>

						<form id="sortForm" name="sortForm1" method="get" action="#">
							<product:productSorting searchPageData="${searchPageData}" />
						</form>
					</div>
				</div>


				<div class="plp-filters show-mobile">
					<div class="browse-categories">
						<button 
							id="mBrowseCategoriesBtn"
							class="btn block black secondary-btn" 
							type="button"
							data-toggle="modal" 
							data-dismiss="modal" 
							data-target="#mBrowseCategories">
							<spring:theme code="text.clp.browse.categories" />
						</button>
					</div>
					<div class="row filters">
							<div class="col-xs-6">
			            <button 
			                id="mFilter"
			                class="btn block black secondary-btn" 
			                type="button"  
											data-toggle="modal" 
											data-dismiss="modal" 
											data-target="#plpFiltersModal"><spring:theme code='plp.filter.text' />
			            </button>
			        </div>
			        <div class="col-xs-6">
			            <button 
			                id="mSortBy" 
			                class="btn block black secondary-btn" 
											type="button"
											data-toggle="modal" 
											data-dismiss="modal" 
											data-target="#mSortByModal"><spring:theme code='plp.sortby.text' />
			            </button>
			        </div>
							<div class="col-xs-12 product-count"><spring:theme 
								code="search.page.count.display.text" />&nbsp;${searchPageData.pagination.totalNumberOfResults}&nbsp;<spring:theme 
								code="search.page.count.items.text" />
						</div>
					</div>
				</div>

				<!-- Modal for mobile starts -->
				<div class="modal fade plp-filters-modal" id="plpFiltersModal"
					role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><spring:theme code='plp.filter.text'/></h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close"></button>
							</div>
							<div class="modal-body clearfix">
								<div class="plp-filters">
									<product:productFilterDropDown searchPageData="${searchPageData}" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Modal for mobile ends -->

			<!-- Start: Mobile sort by filter-->
			<div class="modal fade plp-filters-modal" id="mSortByModal" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
									<h5 class="modal-title"><spring:theme code='plp.sortby.text' /></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body clearfix">
								<div class="plp-filters">
									<form id="mSortForm" name="sortForm2" method="get" action="#">
										<product:productSorting searchPageData="${searchPageData}" />
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>				
				<!-- End: Mobile sort by filter-->
				<!-- Start: Mobile Category Browse model-->
				<div class="modal fade plp-filters-modal" id="mBrowseCategories" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title"><spring:theme code='plp.filter.category' /></h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
							</div>
							<div class="modal-body clearfix">
								<div id="mbrowseContent" class="plp-filters"></div>
							</div>
						</div>
					</div>
				</div>
