/**
* @function : rlutility
* @description : use this for global email subscription modal functionality
*/
define('pages/myaccount', ['jquery','slickCarousel', 'templates/shippingEasypost.tpl','handlebars','viewportDetect','ajaxFactory', 'myaccountProfileUpdate', 'myaccountManageAddress', 'login','myaccountSizes','myaccountPaymentInfo','myDesigners','myaccountDesignersAZ','myPremiumOutlets', 'myaccountOrderHistory','globalCustomComponents','analytics'], 
    function($, slickCarousel, easypostFormTemplate,handlebars, viewportDetect,ajaxFactory,myaccountProfileUpdate,myaccountManageAddress,login,myaccountSizes,myaccountPaymentInfo,myDesigners,myaccountDesignersAZ,myPremiumOutlets, myaccountOrderHistory, globalCustomComponents,analytics) {
        'use strict';
        var cache;
        var myaccount = {
            init: function() {
                this.initVariables();
                this.initEvents();
                this.initAjax();    
				if($('.payment-method').length > 0){
					myaccountPaymentInfo.init();
                }
                if(viewportDetect.lastClass === 'small'){
                    this.colorSwitchesMobile();
                }else{
                    this.colorSwitchesCarousal();
                }
				myaccountSizes.init();
				
				myaccountDesignersAZ.init();
                myaccountOrderHistory.init();
                this.mysize();
				if($('#analyticsSatelliteFlag').val()==='true'){
                    analytics.analyticsOnPageLoad();
                }
				
            },

            initVariables: function() {
                cache = {
                    $captchaValidation : $('.captchaValidation'),
                    $rcaptcha : $('#rcaptcha'),
                    $captcha : $('#captcha'),
                    $document: $(document),
                    $pageContainer: $('.page-container'),
                    $myfavoritesContainer: $('.myfavorites-container'),
                    $colorSwitchesCarousal : $('.color-switches-carousal'),
                    $mysizesContainer : $('.mysizes-container'),
                    $editSizeModal : $('#editSizeModal'),
                    $saveBtn : $('#editSizeModal .save-data')
                }
            },
            initAjax: function() {
                
            },
            initEvents: function() {
                if($('.modal-list').length > 0){
                    $('.modal-list .accordion-menu').click(function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        if($(this).hasClass('collapsed')) {
                            $(this).removeClass('collapsed');
                            $(this).next('ul').addClass('in');
                        } else {
                            $(this).addClass('collapsed');
                            $(this).next('ul').removeClass('in');
                        }
                    }); 
                }
				
				if($('.deal-block').length > 0){
					cache.$document.on('click','.deal-block',myaccount.dealBlock);
				}
                $('.nav .dropdown-menu  a:not(a[href="#"])').on('click', function() {
                    self.location = $(this).attr('href');
                });
                cache.$document.on('click', '.accordion-toggle', function() {
                    if($(this).hasClass('minus-icon')){
                        $(this).removeClass('minus-icon');
                    }
                    else{
                        $(this).addClass('minus-icon');
                    }
                });
                cache.$pageContainer.on('click', '.add-deals', function() {
                    if($(this).hasClass('minus-icon')){
                        $(this).removeClass('minus-icon');
                    }else{
                        $(this).addClass('minus-icon');
                    }
                    
                    var key = $(this).data('key'),
                        keyId = '#' + key,
                        target = $(this).parents('.page-container').find(keyId);
                        target.removeClass('collapsed').addClass('minus-icon');
						target.parent('.panel-title').next().addClass('in');
                   
                });
                if(cache.$captchaValidation.length>0){
                    $(document).on('submit','.captchaValidation',myaccount.validateCaptcha);  
                }              
                // adding password strength validator 
                window.ParsleyValidator.addValidator('pwdstrength', 
                    function(value) {
                        var patt = /(^(((?=.*[A-Z])(?=.*[~!@#$&*%])(?=.*[0-9]))|((?=.*[~!@#$&*%])(?=.*[0-9])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[~!@#$&*%])(?=.*[a-z]))).{8,15}$)/g;
                        var pwdstrength = value.match(patt) || [];
                        return pwdstrength.length >= 1;
                    }, 32);
                
                $(document).on('click','.btnShareEmail',globalCustomComponents.submitShareEmailMyDeal);

                    $(document).ready(function() {
                        cache.$document.on('click', '.has-level-1', function() {
                            $('.level-1').collapse('toggle');
                        });
                    });
            },
			dealBlock:function(){                 
				var $imgURL=$(this).data('url'),
					$name = $(this).data('name'),
					$validity = $(this).data('validity'),
					$desc = $(this).data('desc'),
				$shopNow = $(this).data('shopnow');
				$('#modalContent').find('#imgURL img').attr('src',$imgURL);
				$('#modalContent').find('#name').text($name);
				$('#modalContent').find('#validity').append($validity);
				$('#modalContent').find('#desc').text($desc);
				$('#modalContent').find('.shop-now').attr('href',$shopNow);		
				// Social media
                var imageUrl = window.location.protocol+'//'+window.location.host+$imgURL,
                $twitter = $('#modalContent').find('#TWITTER');
                $twitter.attr('href', 'https://twitter.com/share?url='+window.location.href+'&amp;text='+$name+', '+$desc.replace('%', ' percent'));
                $twitter.attr('target','_blank');

                var $facebook =  $('#modalContent').find('#FACEBOOK');
                $facebook.attr('href', 'https://www.facebook.com/sharer/sharer.php?u='+window.location.href);
                $facebook.attr('target','_blank');

                var $pinterest =  $('#modalContent').find('#PINTEREST');
                $pinterest.attr('href', 'http://pinterest.com/pin/create/button/?url='+window.location.href+'&amp;media=' +imageUrl+ '&amp;description='+$name+', '+$desc);
                $pinterest.attr('target','_blank');
            },
            validateCaptcha: function(){
                if(cache.$rcaptcha.length){
                    var response = grecaptcha.getResponse();
                    if(response.length===0){
                        cache.$captcha.show(); 
                        return false;
                    }else{   
                        cache.$captcha.hide();
                        return true;
                    } 
                }
            },
             // Tiles Carousal
             colorSwitchesCarousal:function(){
                var swatchesCarousal = $('.color-switches-carousal');
                var options = {
                    autoplay: false,
                    autoplaySpeed: 3000,
                    slidesToShow: 4.5,
                    arrows: true,
                    infinite: false,
                    dots: false,
                    draggable: true,
                    pauseOnHover: true,
                    swipe: true,
                    touchMove: true,
                    speed: 300,
                    singleItem: true,
                    cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
                    adaptiveHeight: true,
                    prevArrow: '<button class="slick-prev"></button>',
                    nextArrow: '<button class="slick-next"></button>',
                    responsive: [
                        {
                            breakpoint: 991,
                            settings: {
                                arrows: false
                            }
                        }
                    ]
                }
                
                swatchesCarousal.not(".slick-initialized").slick(options);

               // Hide first arrow of carousal
                swatchesCarousal.find('.slick-prev').hide();
                swatchesCarousal.find('.slick-next').on('click', function() {
                    $(this).closest(swatchesCarousal).find('.slick-prev').show();                     
                });
            },
             // Tiles Carousal
            colorSwitchesMobile:function(){
                if ( (viewportDetect.lastClass === 'small') && ($("div").hasClass('color-switches-carousal')) ) {
                        // Adding class for mobile
                    $('.color-switches-carousal').addClass('color-switches-mobile');
                    $('.color-switches-carousal').append("<span class='count'></span>");  
                    $('.color-switches-carousal').each(function() {
                            var length = $(this).find('.item').length;
                            $('.count').html('+'+(length-3));
                        });
                }
            },
            
            mysize: function() {
                cache.$mysizesContainer.find('.nav-tabs li:first').addClass('active');
                cache.$mysizesContainer.find('.tab-content .tab-pane:first').addClass('active');            
                
                // popup opening as per id
                cache.$mysizesContainer.find('.list-group-item .btn').click(function(e){
                    var currentId = e.target.id;
                    $('.modal-content').hide();
                    $('.modal-content#'+currentId).show();
                });

                // save data call
                 cache.$saveBtn.click(function(){
                    var $this = $(this),
                        $radioValue=$this.closest('form').find('input[type=radio][name=name]:checked').attr('id');
                        $('input[name="code"]').val($radioValue);
					analytics.mySizeFormSubmit( $this.parents('#mySizeModal') );
                    $this.parents('#mySizeModal').serialize().submit();
                });

            },
            
            submitShareEmailMyDeal: function(e){
            	e.preventDefault();
            	if ($('#shareEmailForm').parsley().isValid()) {
                	if(cache.$rcaptcha.length){
                        var response = grecaptcha.getResponse();
                        if(response.length===0){
                            cache.$captcha.show(); 
                            return false;
                        }else{   
                            cache.$captcha.hide();
                        } 
                    }
              	 var formData = {
                       CSRFToken :$("input[name=CSRFToken]").val(),
                       fromEmail :$('#shareViaEmail').find('#fromEmail').val(),
                       toEmail :$('#shareViaEmail').find('#toEmail').val(),
                       comments :$('#shareViaEmail').find('#emailMsg').val(),
                       productId : $("input[name=productCode]").val(),
                       dealId : $("input[name=dealId]").val()
                       }
                        var options = {
                                'methodType': 'POST',
                                'dataType': 'JSON',
                                'methodData':formData,
                                'url': $("#shareEmailForm").data('emailurl'),
                                'isShowLoader': false,
                                'cache' : true
                            }
                     ajaxFactory.ajaxFactoryInit(options, function(){
                          
                           	 $('#shareViaEmail').modal('hide');
                          
                     });
               }
            	else
            		$('#shareEmailForm').submit();
            }
        }

        $(function() {
            myaccount.init();
        });

        return myaccount;
    });
