<div class="designer-listing-banner">
    <div class="the-designer-shop left active">
        <a href="#">
            <picture>
                <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/dlp-image2@2x.png">
                <img class="img-responsive" src="/_ui/responsive/simon-theme/images/dlp-image2.png">
            </picture>
            <div class="content-container">
                <picture>
                    <source media="(max-width: 991px)" srcset="/_ui/responsive/simon-theme/images/dlp-image@2x.png">
                    <img class="img-responsive" src="/_ui/responsive/simon-theme/images/dlp-image.png">
                </picture>
                <h1 class="display-medium major">Diane Von Furstenburg</h1>
                <div class="designer-banner-text">New Season Collection up to 40% Off</div>
                <span class="shopnow major">shop now <span>&rsaquo;</span></span>
            </div>
        </a>
    </div>
</div>