/**
 * @function : rlutility
 * @description : use this for global email subscription modal functionality
 */
define(['jquery', 'ajaxFactory', 'utility'], function($, ajaxFactory, utility) {
    'use strict';
    var cache;
    var myaccountProfileUpdate = {
        init: function() {
            this.initVariables();
            this.initEvents();
            this.multiselectToggle();
            utility.floatingLabelsInit();
        },
        initVariables: function() {
            cache = {
                $document : $(document),
                $updatePassword : $('#updatePassword'),
                $updatePasswordSection : $('#updatePasswordSection'),
                $formDetails : $('.form-details'),
                $options: []
            }
        },
        initAjax: function() {},
        initEvents: function() {
            cache.$formDetails.on('click','.custom-checkbox', myaccountProfileUpdate.passwordToggle);
            cache.$updatePassword.on('click','#updateForm', myaccountProfileUpdate.updateForm);
            cache.$document.on('change', '#countries', myaccountProfileUpdate.changeCountry);
            cache.$document.on('change', '#selectPrimaryMall', myaccountProfileUpdate.changePrimaryMall);
        $( document ).ready(function() {
            $("#drpCenters li div").each(function(idx, ele){
               if($('#'+ele.id).find('input[type=checkbox]').is(':checked')){
                    cache.$options.push(ele.id);
                    $("#selectPrimaryMall option[value=" + ele.id + "]").hide();
                    $("#alternateMallIds").val(cache.$options.toString());
               }
            })
            if(!($('#countries').val() === 'US' || $('#countries').val() === 'CA')){
                var $zipCode = $('#txtZip');
                $zipCode.closest('.zipcode').addClass('hide').val('');
            }
        });          
        },
        changePrimaryMall: function(e){
            $("#drpCenters li div").each(function(idx,ele){
                if(e.currentTarget.value === ele.id){
                    $('#'+ele.id).hide();
                }else{
                    $('#'+ele.id).show();
                }
            })
        },
        passwordToggle: function(){
            if($('#changePassCheckbox').is(':checked') === true){
                $('.update-password').removeClass('hide');
                $('.update-password').find('input[type=password]').attr('data-parsley-required','');
                $('#updatePassword input[name=passwordUpdate]').val(true);
            }else{
                $('.update-password').addClass('hide');
                $('.update-password').find('input[type=password]').removeAttr('data-parsley-required','');
                $('#updatePassword input[name=passwordUpdate]').val(false);
            } 
        },
        changeCountry: function(){
            var $country = $(this).val(),
                $zipCode = $('#txtZip');
            if($country==='US' || $country==='CA'){
                $zipCode.closest('.zipcode').removeClass('hide').val('');
            }else{
                $zipCode.closest('.zipcode').addClass('hide').val('');
                $zipCode.attr('data-parsley-required', 'false');
                $zipCode.removeAttr('required');
                $zipCode.removeAttr('data-parsley-pattern');
            }
			
			// Country specific Regex
            var regex = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/;
            $zipCode.closest('.sm-input-group').removeClass('has-error');
            $zipCode.val('');
            if($country === 'US'){
                var usRegex = '^(?=.*[1-9].*)[0-9]{5}?$';
                $zipCode.attr('data-parsley-pattern',usRegex);
            } else {
                $zipCode.attr('data-parsley-pattern',regex);
            }
        },       
        multiselectToggle: function(){
            $( '.dropdown-menu a' ).on( 'click', function( event ) {
            var $target = $( event.currentTarget ),
                val = $target.attr( 'data-value' ),
                $inp = $target.find( 'input' ),
                idx;
            if ( ( idx = cache.$options.indexOf( val ) ) > -1 ) {
                cache.$options.splice( idx, 1 );
                $("#selectPrimaryMall option[value=" + val + "]").show();
                setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            } else {
                cache.$options.push( val );
                $("#selectPrimaryMall option[value=" + val + "]").hide();
                setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            }
            $( event.target ).blur();
            $("#alternateMallIds").val(cache.$options.toString());
            return false;
            });
            $( '.dropdown-toggle' ).on( 'click', function() {
                var $thisForm = $(this).closest('form');
                $("#drpCenters li div").each(function(idx,ele){
                    if($thisForm.find('#selectPrimaryMall').val() === ele.id){
                        $('#'+ele.id).hide();
                        return false;
                    }
                })
            });
        }
    }
      
    $(function() {
        myaccountProfileUpdate.init();
    });

    return myaccountProfileUpdate;
});