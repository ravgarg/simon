<jsp:include page="pdp-size-guide.jsp"></jsp:include>
<jsp:include page="pdp-select-size-mobile.jsp"></jsp:include>
<jsp:include page="global-login-modal.jsp"></jsp:include>

<div class="container">
	<article class="row">
		<div class="col-md-12">
			<jsp:include page="clp-breadcurmb.jsp"></jsp:include>
		</div>

		<aside class="col-md-3 left-menu-container show-desktop">
			<jsp:include page="plp-left-nav.jsp"></jsp:include>
		</aside>
		<section class="col-md-9">
			<jsp:include page="search-zero-top-heading.jsp"></jsp:include>
			<jsp:include page="plp-search-tile-with-heading.jsp"></jsp:include>
			<jsp:include page="plp-search-tile-with-heading1.jsp"></jsp:include>
		</section>
	</article>
</div>

