<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>
					
<div class="hide-desktop mobile-logo">
              <a data-analytics-linkname="${media.altText}" href="${urlLink}"> <img src="${mediaForMobile.url}" class="simon-logo" alt="${media.altText}"/></a>
</div>
<div class="sitelogo hide-mobile">
              <a data-analytics-linkname="${media.altText}" href="${urlLink}"><img src="${media.url}" class="simon-logo" alt="${media.altText}"/></a>
</div>
