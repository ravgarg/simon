<div class="modal fade no-pad-r" id="editCartItemModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="fixed-content modal-body clearfix">
                <div class="edit-item-container">
                    <div class="col-md-5">
                        <div class="show-mobile retailer-pad-btm">
                            <div class="edit-last-call">
                            <a href="#" class="last-call">Last Call</a>
                            <a href="#" class="retailer">Jay Godfrey</a>
                            </div>
                            <div class="item-description">Asymmetric-Peplum Midi Dress</div>
                            <div class="item-price">$250.00</div>
                            <div class="item-revised-price">$200.00
                               <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                            </div>
                            <div class="item-discount">20% Off</div>
                        </div>
                        <div id="editItemCarousel" class="edit-modal-image">
                            <img alt="" src="/_ui/responsive/simon-theme/images/cart-image-6.jpg">
                            <img alt="" src="/_ui/responsive/simon-theme/images/cart-image-6.jpg">
                            <img alt="" src="/_ui/responsive/simon-theme/images/cart-image-6.jpg">
                        </div>
                    </div>
                     <div class="col-md-7">
                       <div class="show-desktop">
                            <div class="edit-last-call">
                                 <a href="#" class="last-call">Last Call</a>
                                 <a href="#" class="retailer">Jay Godfrey</a>
                            </div>
                            <div class="item-description">Asymmetric-Peplum Midi Dress</div>
                            <div class="item-price">$250.00</div>
                            <div class="item-revised-price">$200.00
                                 <img src="/_ui/responsive/simon-theme/icons/black-question.svg" data-placement = "top" data-html="true" class="tooltip-init simon-logo" data-toggle = "tooltip" title = "MSRP: $234.00 <br> List Price: $200.00 <br> Sale Price: $122.00" alt="">
                            </div>
                            <div class="item-discount">20% Off</div>
                       </div>
                        <div class="select-color">Select Color</div>
                        <div class="item-colors clearfix">
                            <button type="button" style="background:#FF0000 url(orange.jpg)" class="color-swatches">Black</button>
                            <button type="button" class="color-swatches">Tomato</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                             <button type="button" class="color-swatches">Magenta</button>
                            <button type="button" class="color-swatches">Pink</button>
                        </div>
                        <div class="select-size">Select Size</div>
                        <div class="item-sizes clearfix">
                            <button type="button" class="btn-size">00</button>
                            <button type="button" class="btn-size">0</button>
                            <button type="button" class="btn-size">2</button>
                            <button type="button" class="btn-size">4</button>
                            <button type="button" class="btn-size">6</button>
                            <button type="button" class="btn-size">8</button>
                            <button type="button" class="btn-size">10</button>
                            <button type="button" class="btn-size">12</button>
                            <button type="button" class="btn-size">14</button>
                        </div>
                        <div class="update-item hide-mobile">
                           <button class="btn plum">Update Item</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer show-mobile">
                 <button class="btn plum">Update Item</button>
            </div>
        </div>
    </div>
</div>