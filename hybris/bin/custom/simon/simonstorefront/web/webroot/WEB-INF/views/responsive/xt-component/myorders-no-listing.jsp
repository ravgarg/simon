
<div class="myorder-details-container row">
	<div class="col-md-12">
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>

		<p class="no-order">You have not placed any order yet.</p>
		
		<div class="row">
	       	<div class="submit-btn col-md-12 col-xs-12">
	       		<a href="#" class="btn" role="button">Start Shopping</a>
	       	</div>
	    </div>
    </div>
</div>