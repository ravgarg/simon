package com.simon.storefront.interceptors.beforeview;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.PageTemplateModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.servlet.ModelAndView;

import com.simon.core.constants.SimonCoreConstants;


/**
 * AddAdditionalAttributesBeforeViewHandlerTest, unit test for {@link AddAdditionalAttributesBeforeViewHandler}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class AddAdditionalAttributesBeforeViewHandlerTest
{

	@InjectMocks
	@Spy
	private AddAdditionalAttributesBeforeViewHandler addAdditionalAttributesBeforeViewHandler;
	@Mock
	private UserService userService;
	@Mock
	private ModelAndView modelAndView;
	@Mock
	private PageTemplateModel pageTemplateModel;
	@Mock
	private AbstractPageModel abstractPageModel;
	private UserModel user;
	@Mock
	private ConfigurationService configurationService;

	private Configuration configuration;
	@Mock
	private SessionService sessionService;

	@Mock
	private CustomerModel customerModel;

	@Mock
	private UserModel userModel;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		when(pageTemplateModel.getPageStyleId()).thenReturn("homestyle");
		when(abstractPageModel.getMasterTemplate()).thenReturn(pageTemplateModel);
		final Map<String, Object> model = new HashMap<>();
		model.put(AbstractPageController.CMS_PAGE_MODEL, abstractPageModel);
		when(modelAndView.getModel()).thenReturn(model);
		Mockito.doReturn("Token").when(addAdditionalAttributesBeforeViewHandler).getTokenForSession(null);
		user = new UserModel();
		when(userService.getCurrentUser()).thenReturn(user);
		configuration = Mockito.mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn((configuration));
	}

	/**
	 * Verify page style id added in model and view correctly.
	 */
	@Test
	public void verifyPageStyleIdAddedInModelAndViewCorrectlyWithAnonymousUser()
	{
		when(userService.isAnonymousUser(user)).thenReturn(true);
		addAdditionalAttributesBeforeViewHandler.beforeView(null, null, modelAndView);
		verify(modelAndView).addObject("pageStyleId", "homestyle");
		verify(modelAndView, times(0)).addObject("isLoggedIn", true);
	}

	@Test
	public void verifyPageStyleIdAddedInModelAndViewCorrectlyWithDifferentUser()
	{
		when(userService.isAnonymousUser(user)).thenReturn(false);
		addAdditionalAttributesBeforeViewHandler.beforeView(null, null, modelAndView);
		verify(modelAndView).addObject("pageStyleId", "homestyle");
		verify(modelAndView, times(1)).addObject("isLoggedIn", true);
	}


	@Test
	public void setInSessionIfUserHasProfileAttributesDefaultedWhenNoMessage()
	{
		when(sessionService.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE)).thenReturn(Boolean.FALSE);
		addAdditionalAttributesBeforeViewHandler.setIfUserHasProfileAttributesDefaulted(modelAndView);
		assertFalse(customerModel.getProfileCompleted().booleanValue());

	}

	@Test
	public void setInSessionIfUserHasProfileAttributesDefaulted()
	{
		when(sessionService.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE)).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(customerModel);
		when(customerModel.getProfileCompleted()).thenReturn(Boolean.FALSE);
		addAdditionalAttributesBeforeViewHandler.setIfUserHasProfileAttributesDefaulted(modelAndView);
		assertFalse(customerModel.getProfileCompleted().booleanValue());

	}

	@Test
	public void setInSessionIfUserHasProfileAttributesDefaultedsCompleteProfileTrue()
	{
		when(sessionService.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE)).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(customerModel);
		when(customerModel.getProfileCompleted()).thenReturn(Boolean.TRUE);
		addAdditionalAttributesBeforeViewHandler.setIfUserHasProfileAttributesDefaulted(modelAndView);
		assertTrue(customerModel.getProfileCompleted());

	}


	@Test
	public void setInSessionIfUserHasProfileAttributesDefaultedWhenUserModelIsNotCustModel()
	{
		when(sessionService.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE)).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(userModel);
		when(customerModel.getProfileCompleted()).thenReturn(true);
		addAdditionalAttributesBeforeViewHandler.setIfUserHasProfileAttributesDefaulted(modelAndView);
		assertTrue(customerModel.getProfileCompleted());
	}

	@Test
	public void setInSessionIfUserHasProfileAttributesDefaultedProfileCompletedIsNull()
	{
		when(sessionService.getAttribute(SimonCoreConstants.LOGIN_PROFILE_COMPLETED_MESSAGE_CODE)).thenReturn(Boolean.TRUE);
		when(userService.getCurrentUser()).thenReturn(customerModel);
		when(customerModel.getProfileCompleted()).thenReturn(false);
		addAdditionalAttributesBeforeViewHandler.setIfUserHasProfileAttributesDefaulted(modelAndView);
		assertFalse(customerModel.getProfileCompleted().booleanValue());
	}


}
