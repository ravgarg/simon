package com.simon.storefront.validator.form;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;

import com.simon.core.constants.GeneratedSimonCoreConstants.Enumerations.CustomerGender;
import com.simon.core.enums.Months;
import com.simon.storefront.forms.AccountUpdateForm;


@UnitTest
public class ExtUpdateProfileValidatorTest
{
	@InjectMocks
	private ExtUpdateProfileValidator extProfileValidator;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration config;
	@Mock
	private Errors errors;
	private AccountUpdateForm updateProfileForm;
	private List<Object> list;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		updateProfileForm = new AccountUpdateForm();
		updateProfileForm.setBirthMonth(Months.FEB.getCode());
		updateProfileForm.setGender(CustomerGender.FEMALE);
		Mockito.doNothing().when(errors).rejectValue(Matchers.anyString(), Matchers.anyString());
		Mockito.when(configurationService.getConfiguration()).thenReturn(config);
	}


	@Test
	public void testValidateWithValidValues()
	{

		final CountryData country = new CountryData();
		country.setIsocode("US");
		updateProfileForm.setBirthYear(1992);
		updateProfileForm.setFirstName("firstName");
		updateProfileForm.setLastName("firstName");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setEmail("test@gmail.com");
		updateProfileForm.setZipCode("2333");
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setPassword("Test@123");
		updateProfileForm.setConfirmPassword("test");
		list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateWithInValidValues()
	{


		final CountryData country = new CountryData();
		updateProfileForm.setBirthYear(0);
		updateProfileForm.setFirstName(
				"firstNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("false");
		updateProfileForm.setConfirmPassword("Test@123");
		updateProfileForm.setPassword("Test@123");
		list = new ArrayList<>();
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(5)).rejectValue(Matchers.anyString(), Matchers.anyString());

	}


	@Test
	public void testValidateWithInValidValuesAndValidCountry()
	{


		final CountryData country = new CountryData();
		country.setIsocode("US");

		updateProfileForm.setBirthYear(0);
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setEmail("test");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setPassword("test1234");
		updateProfileForm.setConfirmPassword("test");
		final List<Object> list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(9)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateWithInValidValuesAndIsPasswordUpdateAndNullPassword()
	{


		final CountryData country = new CountryData();
		country.setIsocode("US");
		updateProfileForm.setBirthYear(0);
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setEmail("test");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setConfirmPassword("test");
		final List<Object> list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(8)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}


	@Test
	public void testValidateWithInValidValuesAndIsPasswordUpdateAndPasswordLengthLessThan6()
	{


		final CountryData country = new CountryData();
		country.setIsocode("US");
		updateProfileForm.setBirthYear(0);
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setEmail("test");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setConfirmPassword("test");
		updateProfileForm.setPassword("test");
		final List<Object> list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(8)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateWithInValidValuesAndIsPasswordUpdateAndPasswordLengthGreaterThan16()
	{
		final CountryData country = new CountryData();
		country.setIsocode("US");
		updateProfileForm.setBirthYear(0);
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setEmail("test");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setConfirmPassword("test");
		updateProfileForm.setPassword("test1234567890111");
		final List<Object> list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(9)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateWithInValidValuesAndIsPasswordUpdateAndConfirmPasswordNull()
	{

		final CountryData country = new CountryData();
		country.setIsocode("US");
		updateProfileForm.setBirthYear(0);
		updateProfileForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		updateProfileForm.setEmail("test");
		updateProfileForm.setCountry(country.getIsocode());
		updateProfileForm.setPasswordUpdate("true");
		updateProfileForm.setPassword("test1234567890111");
		final List<Object> list = new ArrayList<>();
		list.add("US");
		Mockito.when(config.getList("simonstorefront.account.countries.zipcode.valid")).thenReturn(list);
		extProfileValidator.validate(updateProfileForm, errors);
		verify(errors, times(9)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testSupports()
	{
		Assert.assertTrue(extProfileValidator.supports(AccountUpdateForm.class));
	}
}
