package com.simon.storefront.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.enums.Follow;
import com.simon.core.enums.Index;
import com.simon.facades.account.data.DesignerData;


/**
 * Test Class for ExtSiteThemeResolverUtils
 */
@UnitTest
public class ExtWebUtilsTest
{
	@Spy
	@InjectMocks
	private ExtWebUtils ExtWebUtils;
	@Mock
	private UiExperienceService uiExperienceService;
	@Mock
	private CMSSiteService cmsSiteService;
	@Mock
	private ConfigurationService configurationService;

	final List<String> albhabetsList = new ArrayList<>();

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		for (int i = 'A'; i <= 'Z'; i++)
		{
			albhabetsList.add(Character.toString((char) i));
		}
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test Method For clickablAlbhabeteNavigation Method
	 */
	@Test
	public void clickablAlbhabeteNavigationTest()
	{
		Map<String, Set<DesignerData>> clickableAlbhabetsMap = new HashMap<>();
		final List<DesignerData> designerList = new ArrayList<>();
		final DesignerData data = new DesignerData();
		final DesignerData data1 = new DesignerData();
		data.setName("Test");
		data1.setName("1Test");
		designerList.add(data);
		designerList.add(data1);
		clickableAlbhabetsMap = ExtWebUtils.getAlphabeticallyPartionedDesignerDataMap(designerList);
		assertEquals(clickableAlbhabetsMap.size(), 27);
	}

	@Test
	public void populateCategoryForAnalyticsTest()
	{
		final List<Breadcrumb> breadCrumbs = new ArrayList<>();
		breadCrumbs.add(new Breadcrumb("/", "Home", null));
		breadCrumbs.add(new Breadcrumb("/men", "Men", null));
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getProperty("navigation.home.category.name")).thenReturn("Home");
		assertEquals(ExtWebUtils.populateCategoryForAnalytics(breadCrumbs), "Men");
	}

	/**
	 * Test Method For setRobots Method
	 */
	@Test
	public void getRobotsTest()
	{
		final ContentPageModel contentPageModel = new ContentPageModel();
		contentPageModel.setFollow(Follow.FOLLOW);
		contentPageModel.setIndex(Index.NOINDEX);
		ExtWebUtils.getMetaInfo(contentPageModel);
		Assert.assertEquals(contentPageModel.getIndex().toString(), "NOINDEX");
	}
}
