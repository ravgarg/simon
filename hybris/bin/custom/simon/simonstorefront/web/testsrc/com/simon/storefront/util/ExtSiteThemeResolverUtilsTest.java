package com.simon.storefront.util;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.uiexperience.UiExperienceService;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commerceservices.enums.UiExperienceLevel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Test Class for ExtSiteThemeResolverUtils
 */
@UnitTest
public class ExtSiteThemeResolverUtilsTest
{
	@Mock
	private UiExperienceService uiExperienceService;
	@Mock
	private CMSSiteService cmsSiteService;
	@InjectMocks
	private ExtSiteThemeResolverUtils extsiteThemeResolverUtils;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}

	/**
	 * Test Method for resolveThemeForCurrentSite
	 */
	@Test
	public void resolveThemeForCurrentSiteTest()
	{


		final CMSSiteModel cmssite = Mockito.mock(CMSSiteModel.class);
		Mockito.when(uiExperienceService.getUiExperienceLevel()).thenReturn(UiExperienceLevel.DESKTOP);
		Mockito.when(cmsSiteService.getCurrentSite()).thenReturn(cmssite);
		extsiteThemeResolverUtils.resolveThemeForCurrentSite();
		Assert.assertNotNull(extsiteThemeResolverUtils.resolveThemeForCurrentSite());
	}

	/**
	 * Test Method for resolveThemeForCurrentSite to test case of null cmssite
	 */
	@Test
	public void resolveThemeForCurrentSiteForNullCMSSiteTest()
	{


		Mockito.when(uiExperienceService.getUiExperienceLevel()).thenReturn(UiExperienceLevel.DESKTOP);
		Mockito.when(cmsSiteService.getCurrentSite()).thenReturn(null);
		extsiteThemeResolverUtils.resolveThemeForCurrentSite();
		Assert.assertNull(extsiteThemeResolverUtils.resolveThemeForCurrentSite());
	}

	/**
	 * Test for resolveThemeForCurrentSite to test case of UI experience is null
	 */
	@Test
	public void resolveThemeForCurrentSiteForNullUIExperienceTest()
	{
		Mockito.when(uiExperienceService.getUiExperienceLevel()).thenReturn(null);
		final CMSSiteModel cmssite = Mockito.mock(CMSSiteModel.class);
		Mockito.when(cmsSiteService.getCurrentSite()).thenReturn(cmssite);
		extsiteThemeResolverUtils.resolveThemeForCurrentSite();
		Assert.assertNotNull(extsiteThemeResolverUtils.resolveThemeForCurrentSite());
	}
}
