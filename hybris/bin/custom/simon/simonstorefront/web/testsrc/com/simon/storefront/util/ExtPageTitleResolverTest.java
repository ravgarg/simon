package com.simon.storefront.util;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.context.MessageSource;

import com.simon.core.model.DesignerModel;
import com.simon.storefront.constant.SimonWebConstants;


/**
 * Test Class for ExtPageTitleResolverTest
 */
@UnitTest
public class ExtPageTitleResolverTest
{
	@InjectMocks
	@Spy
	private ExtPageTitleResolver extPageTitleResolver;
	@Mock
	private ProductService productService;
	@Mock
	private CommerceCategoryService commerceCategoryService;
	@Mock
	private I18NService i18NService;
	@Mock
	private ProductModel productModel;
	@Mock
	private CategoryModel categoryModel;
	@Mock
	private CategoryModel categoryModel2;
	@Mock
	private DesignerModel designer;
	@Mock
	private MessageSource messageSource;

	private static final String EN = Locale.ENGLISH.getLanguage();

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Test Method for resolveThemeForCurrentSite
	 */
	@Test
	public void resolveProductPageTitle()
	{
		final Locale locale = new Locale("en");
		final String catCode = "m300000_m300500_m300600_m300605";

		final List<CategoryModel> path = new ArrayList<CategoryModel>();
		path.add(categoryModel);


		given(i18NService.getCurrentLocale()).willReturn(locale);

		Mockito.when(productService.getProductForCode("testProduct")).thenReturn(productModel);
		Mockito.when(categoryModel.getName()).thenReturn("Test Category");
		Mockito.when(productModel.getName()).thenReturn("Test Product");
		Mockito.when(productModel.getProductDesigner()).thenReturn(designer);
		Mockito.when(designer.getName()).thenReturn("Cole Hann");
		Mockito.when(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT, locale))
				.thenReturn("SOP");
		Mockito.when(commerceCategoryService.getCategoryForCode("m300605")).thenReturn(categoryModel);
		Assert.assertNotNull(extPageTitleResolver.resolveProductPageTitle("testProduct", catCode));
	}

	/**
	 * Test Method for resolveCategoryPageTitle
	 */
	@Test
	public void resolveCategoryPageTitle()
	{
		final Locale locale = new Locale("en");
		final String catCode = "m300000_m300500_m300600_m300605";

		final List<CategoryModel> path = new ArrayList<CategoryModel>();
		path.add(categoryModel);


		given(i18NService.getCurrentLocale()).willReturn(locale);

		Mockito.when(categoryModel.getSupercategories()).thenReturn(path);
		Mockito.when(categoryModel.getCode()).thenReturn("m100000");
		Mockito.doReturn(path).when(extPageTitleResolver).getCategoryPathForCategory(categoryModel);
		Mockito.doReturn("Men's").when(extPageTitleResolver).buildCatCodePathString(path);
		Mockito.when(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT, locale))
				.thenReturn("SOP");
		Mockito.when(commerceCategoryService.getCategoryForCode("m300605")).thenReturn(categoryModel);
		Assert.assertNotNull(extPageTitleResolver.resolveCategoryPageTitle(categoryModel));
	}

	/**
	 * Test Method for resolveCategoryPageTitle
	 */
	@Test
	public void resolveContentPageTitle()
	{
		final String title = "Contact Us";
		final Locale locale = new Locale("en");

		Mockito.when(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT, locale))
				.thenReturn("SOP");
		Assert.assertNotNull(extPageTitleResolver.resolveContentPageTitle(title));
	}

	/**
	 * Test Method for resolveSearchTextPageTitle
	 */
	@Test
	public void resolveSearchTextPageTitle()
	{
		final String title = "Contact Us";
		final Locale locale = new Locale("en");

		Mockito.when(messageSource.getMessage(SimonWebConstants.TITLE_CONSTANT, null, SimonWebConstants.TITLE_CONSTANT, locale))
				.thenReturn("SOP");
		Assert.assertNotNull(extPageTitleResolver.resolveSearchTextPageTitle(title));
	}

	/**
	 * Test Method for resolveSearchTextPageTitle
	 */
	@Test
	public void buildCatCodePathStringTest()
	{
		final List<CategoryModel> path = new ArrayList<CategoryModel>();
		path.add(categoryModel);
		path.add(categoryModel2);
		Mockito.when(categoryModel.getCode()).thenReturn("m100000");
		Mockito.when(categoryModel2.getCode()).thenReturn("m100000");
		Mockito.when(categoryModel2.getName()).thenReturn("Test Category");
		Assert.assertNotNull(extPageTitleResolver.buildCatCodePathString(path));
	}
}
