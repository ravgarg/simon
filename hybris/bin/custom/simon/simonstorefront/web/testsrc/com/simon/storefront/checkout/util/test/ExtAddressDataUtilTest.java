package com.simon.storefront.checkout.util.test;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.checkout.data.ExtAddressData;
import com.simon.integration.users.address.verify.dto.UserAddressResponseVerificationDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerificationsDTO;
import com.simon.integration.users.address.verify.dto.UserAddressVerifyResponseDTO;
import com.simon.storefront.checkout.form.ExtAddressForm;
import com.simon.storefront.checkout.util.ExtAddressDataUtil;


/**
 * Contains tests for ExtAddressDataUtil.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtAddressDataUtilTest
{
	@InjectMocks
	private ExtAddressDataUtil extAddressDataUtil;

	@Mock
	private ExtAddressForm addressForm;

	@Mock
	private I18NFacade i18NFacade;

	@Mock
	private RegionData regionData;

	@Mock
	private UserAddressVerifyResponseDTO userAddressVerifyResponseDTO;

	@Mock
	private UserAddressResponseVerificationDTO userAddressResponseVerificationDTO;

	@Mock
	private ExtAddressData extAddressData;

	@Mock
	private UserService userService;

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 *
	 */
	@Test
	public void testconvertToAddressData()
	{
		Mockito.when(addressForm.getCountryIso()).thenReturn("US");
		Mockito.when(addressForm.getRegionIso()).thenReturn("RegionISO");
		final CustomerModel user = new CustomerModel();
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		Mockito.when(i18NFacade.getRegion("US", "RegionISO")).thenReturn(regionData);
		final AddressData addressData = extAddressDataUtil.convertToAddressData(addressForm);
		Assert.assertEquals(addressData.isShippingAddress(), Boolean.TRUE);
	}

	/**
	 *
	 */
	@Test
	public void testconvertToSuggestedAddressDto()
	{
		Mockito.when(userAddressVerifyResponseDTO.getZip()).thenReturn("1000-10");
		Mockito.when(userAddressVerifyResponseDTO.getCountry()).thenReturn("US");
		Mockito.when(userAddressVerifyResponseDTO.getState()).thenReturn("NY");
		final ExtAddressData addressData = extAddressDataUtil.convertToSuggestedAddressDto(userAddressVerifyResponseDTO,
				extAddressData);
		Assert.assertEquals(addressData.isShippingAddress(), Boolean.TRUE);
	}


	@Test
	public void testconvertToAddressDataWithMissingISOData()
	{
		Mockito.when(addressForm.getCountryIso()).thenReturn(null);
		Mockito.when(addressForm.getRegionIso()).thenReturn(null);
		Mockito.when(addressForm.getEmailId()).thenReturn("email");
		final CustomerModel user = new CustomerModel();
		Mockito.when(userService.getCurrentUser()).thenReturn(user);
		final AddressData addressData = extAddressDataUtil.convertToAddressData(addressForm);
		Assert.assertEquals(addressData.isShippingAddress(), Boolean.TRUE);
	}

	@Test
	public void testconvertToSuggestedAddressDtoWithEmptyData()
	{
		Mockito.when(userAddressVerifyResponseDTO.getZip()).thenReturn("100010");
		final UserAddressVerificationsDTO verifications = new UserAddressVerificationsDTO();
		final UserAddressResponseVerificationDTO delivery = new UserAddressResponseVerificationDTO();
		delivery.setSuccess(true);
		verifications.setDelivery(delivery);
		Mockito.when(userAddressVerifyResponseDTO.getVerifications()).thenReturn(verifications);
		final ExtAddressData addressData = extAddressDataUtil.convertToSuggestedAddressDto(userAddressVerifyResponseDTO,
				extAddressData);
		Assert.assertEquals(addressData.isShippingAddress(), Boolean.TRUE);
	}

	@Test
	public void testconvertToSuggestedAddressDtoWithNullUserAddressVerifyDTO()
	{
		final ExtAddressData addressData = extAddressDataUtil.convertToSuggestedAddressDto(null, extAddressData);
		Assert.assertNull(addressData.getFirstName());
	}
}
