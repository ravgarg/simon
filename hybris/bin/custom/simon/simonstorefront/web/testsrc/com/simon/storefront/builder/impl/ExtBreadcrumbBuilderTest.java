package com.simon.storefront.builder.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.resolver.ExtCategoryModelUrlResolver;


@UnitTest
public class ExtBreadcrumbBuilderTest
{
	@InjectMocks
	ExtBreadcrumbBuilder extBreadcrumbBuilder;

	@Mock
	private CommerceCategoryService commerceCategoryService;

	@Mock
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Mock
	private CategoryModel category;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testBreadcrumbsCorrectlyFormed()
	{
		final String url = "m300000_m300500_m300501";
		when(commerceCategoryService.getCategoryForCode(Matchers.anyString())).thenReturn(category);
		when(extCategoryModelUrlResolver.resolveCategoryPath(category, url)).thenReturn("url");
		final List<Breadcrumb> breadcrumbs = extBreadcrumbBuilder.getBreadcrumbs(url);
		assertEquals(breadcrumbs.size(), 4);
	}

}
