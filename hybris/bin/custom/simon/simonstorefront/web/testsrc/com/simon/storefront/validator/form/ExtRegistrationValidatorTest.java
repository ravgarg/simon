package com.simon.storefront.validator.form;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.validation.Errors;

import com.simon.storefront.forms.ShopperRegistrationForm;


@UnitTest
public class ExtRegistrationValidatorTest
{
	@InjectMocks
	ExtRegistrationValidator extRegistrationValidator;
	@Mock
	private Errors errors;
	private ShopperRegistrationForm registerForm;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(errors).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testSupports()
	{
		Assert.assertTrue(extRegistrationValidator.supports(ShopperRegistrationForm.class));
	}

	@Test
	public void testValidateWithValidData()
	{
		registerForm = new ShopperRegistrationForm();
		registerForm.setFirstName("firstName");
		registerForm.setLastName("lastName");
		registerForm.setEmail("email@gmail.com");
		registerForm.setPwd("Ankur12345");
		registerForm.setCheckPwd("Ankur12345");
		registerForm.setCountry("US");
		registerForm.setBirthyear(1992);
		registerForm.setBirthmonth("08");
		registerForm.setPrimaryMall("123");
		extRegistrationValidator.validate(registerForm, errors);
		verify(errors, times(0)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateWithNameLenghtInvalid()
	{
		registerForm = new ShopperRegistrationForm();
		registerForm.setFirstName(
				"firstNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersfirstNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		registerForm.setLastName(
				"LastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersLastNameAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255CharactersAndExtraCharactersAddedToIncreaseTheLengthGreaterThan255Characters");
		registerForm.setEmail("email@gmail.com");
		registerForm.setPwd("Ankur12345");
		registerForm.setCheckPwd("Ankur12345");
		registerForm.setCountry("US");
		registerForm.setBirthyear(1992);
		registerForm.setBirthmonth("08");
		extRegistrationValidator.validate(registerForm, errors);
		verify(errors, times(5)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateEmptyPassword()
	{
		extRegistrationValidator.validatePassword(errors, "");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}


	@Test
	public void testValidatePasswordLengthLessThanEight()
	{
		extRegistrationValidator.validatePassword(errors, "abc");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidatePasswordLengthGreaterThanSixteen()
	{
		extRegistrationValidator.validatePassword(errors, "abcdefghijklmnopqrstuvwxyz");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidatePasswordWithValidLengthInvalidPassword()
	{
		extRegistrationValidator.validatePassword(errors, "abcdefghi");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidatePasswordWithValidPassword()
	{
		extRegistrationValidator.validatePassword(errors, "Ankur12345");
		verify(errors, times(0)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}


	@Test
	public void testValidateCountryWithValidData()
	{
		extRegistrationValidator.validateCountry(errors, "US", "country", "register.country.invalid");
		verify(errors, times(0)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidateCountryWithEmptyCountry()
	{
		extRegistrationValidator.validateCountry(errors, "", "country", "register.country.invalid");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidatebirthWithValidData()
	{
		extRegistrationValidator.validatebirth(errors, 1992, "birthyear", "register.birthyear.invalid");
		verify(errors, times(0)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}

	@Test
	public void testValidatebirthWithInValidData()
	{
		extRegistrationValidator.validatebirth(errors, -1992, "birthyear", "register.birthyear.invalid");
		verify(errors, times(1)).rejectValue(Matchers.anyString(), Matchers.anyString());
	}
}
