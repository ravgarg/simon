package com.simon.storefront.builder.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistory;
import de.hybris.platform.acceleratorstorefrontcommons.history.BrowseHistoryEntry;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.MerchandizingCategoryModel;

import junit.framework.Assert;


/**
 * This class tests the methods of {@link ExtProductBreadcrumbBuilder}.
 */
@UnitTest
public class ExtProductBreadcrumbBuilderTest
{
	@InjectMocks
	ExtProductBreadcrumbBuilder extProductBreadcrumbBuilder;
	@Mock
	private UrlResolver<ProductModel> productModelUrlResolver;
	@Mock
	private UrlResolver<CategoryModel> categoryModelUrlResolver;
	@Mock
	private BrowseHistory browseHistory;
	@Mock
	private ProductService productService;
	@Mock
	private ProductModel productModel;
	private final Collection<CategoryModel> supCategories = new ArrayList<>();
	@Mock
	private VariantProductModel variantModel;
	@Mock
	private MerchandizingCategoryModel category1;
	@Mock
	private MerchandizingCategoryModel category2;
	@Mock
	private CategoryModel category3;
	@Mock
	private VariantCategoryModel varCategoryModel;
	private final List<CategoryModel> supCategories2 = new ArrayList<>();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(productModelUrlResolver.resolve(productModel)).thenReturn("/home/p/n001");
		when((productModel.getName())).thenReturn("demo Product");
		when((productModel.getCode())).thenReturn("demoProduct");
		when(productService.getProductForCode("demoProduct")).thenReturn(productModel);
	}

	@Test
	public void testgetBreadcrumbsWhenBaseProductSentAndNoSuperCategoriesFound()
	{
		//when
		when(productModel.getSupercategories()).thenReturn(supCategories);


		//then
		Assert.assertEquals("/home/p/n001", extProductBreadcrumbBuilder.getBreadcrumbs(productModel).get(0).getUrl());
	}

	@Test
	public void testgetBreadcrumbsWhenVariantProductSentAndNoSuperCategoriesFound()
	{
		//when
		when(productService.getProductForCode("demoProduct1")).thenReturn(variantModel);
		when(variantModel.getCode()).thenReturn("demoProduct1");
		when(productModel.getSupercategories()).thenReturn(supCategories);
		when(variantModel.getBaseProduct()).thenReturn(productModel);

		//then
		Assert.assertEquals("/home/p/n001", extProductBreadcrumbBuilder.getBreadcrumbs(variantModel).get(0).getUrl());
	}

	@Test
	public void testgetBreadcrumbsWhenBaseProductSentAndNoValidSuperCategoriesFound()
	{
		//given
		supCategories.add(varCategoryModel);

		//when
		when(productModel.getSupercategories()).thenReturn(supCategories);
		when(variantModel.getBaseProduct()).thenReturn(productModel);

		Assert.assertEquals("/home/p/n001", extProductBreadcrumbBuilder.getBreadcrumbs(productModel).get(0).getUrl());
	}

	@Test
	public void testgetBreadcrumbsWhenBaseProductSentAndValidSuperCategoriesFound()
	{
		//given
		final List<CategoryModel> supCategories3 = new ArrayList<>();
		supCategories3.add(category3);
		supCategories.add(category1);
		supCategories2.add(category2);
		final BrowseHistoryEntry browsingHistory = new BrowseHistoryEntry("", "");

		//when
		when(productModel.getSupercategories()).thenReturn(supCategories);
		when(variantModel.getBaseProduct()).thenReturn(productModel);
		when(category1.getSupercategories()).thenReturn(supCategories2);
		when(category1.getCode()).thenReturn("Pants");
		when(category1.getName()).thenReturn("Pants");
		when(category2.getCode()).thenReturn("Clothing");
		when(category2.getName()).thenReturn("Clothing");
		when(category2.getSupercategories()).thenReturn(supCategories3);
		when(categoryModelUrlResolver.resolve(category1)).thenReturn("/home/clothing/pants");
		when(categoryModelUrlResolver.resolve(category2)).thenReturn("/home/clothing");
		when(browseHistory.findEntryMatchUrlEndsWith("Clothing")).thenReturn(browsingHistory);

		//then
		final List<Breadcrumb> breadcrumbs = extProductBreadcrumbBuilder.getBreadcrumbs(productModel);
		Assert.assertEquals("/home/clothing", breadcrumbs.get(0).getUrl());
		Assert.assertEquals("/home/clothing/pants", breadcrumbs.get(1).getUrl());
		Assert.assertEquals("/home/p/n001", breadcrumbs.get(2).getUrl());
	}

}
