package com.simon.storefront.checkout.steps.validation;

import de.hybris.bootstrap.annotations.UnitTest;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;

import com.simon.storefront.checkout.form.ExtAddressForm;
import com.simon.storefront.checkout.validation.ExtCheckoutAddressValidator;


/**
 * Contains tests for ExtCheckoutAddressValidator.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCheckoutAddressValidatorTest
{

	@InjectMocks
	private ExtCheckoutAddressValidator extCheckoutAddressValidator;

	@Mock
	private Errors errors;

	private ExtAddressForm addressForm;

	/**
	 * Setting up resources for tests.
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		addressForm = new ExtAddressForm();
		addressForm.setCountryIso("US");
		addressForm.setFirstName("firstname");
		addressForm.setLastName("lastname");
		addressForm.setLine1("line1");
		addressForm.setTownCity("towncity");
		addressForm.setPostcode("21456");
		addressForm.setRegionIso("regioniso");
	}

	/**
	 * Method to test 'validate' method in Positive case.
	 *
	 */
	@Test
	public void testValidate_Positive_Data_Case()
	{

		extCheckoutAddressValidator.validate(addressForm, errors);

		Assert.assertEquals("US", addressForm.getCountryIso());
		Assert.assertEquals("21456", addressForm.getPostcode());
	}

	/**
	 * Method to test 'validate' method in Empty & NULL case.
	 *
	 */
	@Test
	public void testValidate_Empty_Null_Data_Case()
	{
		addressForm.setCountryIso(StringUtils.EMPTY);
		addressForm.setFirstName(StringUtils.EMPTY);
		addressForm.setRegionIso(null);

		extCheckoutAddressValidator.validate(addressForm, errors);

		Assert.assertEquals(StringUtils.EMPTY, addressForm.getCountryIso());
		Assert.assertEquals(StringUtils.EMPTY, addressForm.getFirstName());
	}

}

