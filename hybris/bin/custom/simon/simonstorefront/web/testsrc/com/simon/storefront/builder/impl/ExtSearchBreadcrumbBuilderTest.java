package com.simon.storefront.builder.impl;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * This class tests the methods of {@link ExtSearchBreadcrumbBuilder}.
 */
@UnitTest
public class ExtSearchBreadcrumbBuilderTest
{
	private static final String CAT_CODE = "catCode";

	@Spy
	@InjectMocks
	private ExtSearchBreadcrumbBuilder extSearchBreadcrumbBuilder;

	@Mock
	private ProductSearchPageData<SearchStateData, ProductData> searchPageData;

	@Mock
	private ProductSearchPageData<SearchStateData, ProductData> searchPageData2;

	private List<Breadcrumb> breadcrumbs;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		final Breadcrumb breadcrumb1 = new Breadcrumb("catURL1", "catName1", "linkClass");
		final Breadcrumb breadcrumb2 = new Breadcrumb("catURL2", "catName2", "linkClass");

		breadcrumbs = new ArrayList<>();
		breadcrumbs.add(breadcrumb1);
		breadcrumbs.add(breadcrumb2);

		doReturn(breadcrumbs).when(extSearchBreadcrumbBuilder).callSuperMethod(CAT_CODE, searchPageData);
		doReturn(new ArrayList<Breadcrumb>()).when(extSearchBreadcrumbBuilder).callSuperMethod(CAT_CODE, searchPageData2);
	}

	@Test
	public void testGetBreadcrumbsWithValidData()
	{
		final List<Breadcrumb> breadcrumbs = extSearchBreadcrumbBuilder.getBreadcrumbs(CAT_CODE, searchPageData);
		Assert.assertTrue(CollectionUtils.isNotEmpty(breadcrumbs));
		Assert.assertEquals(2, breadcrumbs.size());
	}

	@Test
	public void testGetBreadcrumbsWithEmptyBreadcrumbData()
	{
		final List<Breadcrumb> breadcrumbs = extSearchBreadcrumbBuilder.getBreadcrumbs(CAT_CODE, searchPageData2);
		Assert.assertTrue(CollectionUtils.isEmpty(breadcrumbs));
	}

}
