package com.simon.storefront.controllers.pages;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorservices.data.RequestContextData;
import de.hybris.platform.acceleratorservices.store.data.UserLocationData;
import de.hybris.platform.acceleratorservices.storefront.util.PageTitleResolver;
import de.hybris.platform.acceleratorservices.util.SpringHelper;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController.ShowMode;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.pages.CategoryPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.facades.product.OfferFacade;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.facades.navigation.LeftNavigationCategoryFacade;
import com.simon.storefront.controllers.pages.ExtCategoryPageController.CategorySearchEvaluator;



/**
 *
 * Junit test for SimonCategoryPageController
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCategoryPageControllerTest
{

	private final Model page = Mockito.spy(new BindingAwareModelMap());

	@InjectMocks
	private final ExtCategoryPageController simonCategoryPageController = Mockito.spy(new ExtCategoryPageController());

	@Mock
	CategoryPageModel categoryPageModel;

	private ShowMode showMode;
	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;
	@Mock
	private CommerceCategoryService commerceCategoryService;
	@Mock
	private CategoryModel category;

	@Mock
	private UrlResolver<CategoryModel> categoryModelUrlResolver;

	@Mock
	private CMSPageService cMSPageService;

	@Mock
	private SiteConfigService siteConfigService;
	@Mock
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Mock
	private SessionService sessionService;
	@Mock
	private SearchPageData<?> searchPageData;
	@Mock
	private PaginationData paginationData;
	@Mock
	private ProductCategorySearchPageData<SearchStateData, ProductData, CategoryData> productCategorySearchPageData;
	@Mock
	private CategorySearchEvaluator categorySearch;
	@Mock
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;
	@Mock
	private Breadcrumb breadcrumb;
	@Mock
	private CustomerLocationService customerLocationService;
	@Mock
	private UserLocationData userLocationData;

	@Mock
	private PageTitleResolver pageTitleResolver;
	@Mock
	private RequestContextData requestContextData;
	@Mock
	private HttpSession session;
	@Mock
	private ServletContext servletContext;
	@Mock
	private WebApplicationContext appContext;
	@Mock
	private ProductData productData;
	@Mock
	private OfferFacade offerFacade;
	@Mock
	private OfferData offerData;
	@Mock
	private SearchQueryData searchQueryData;
	@Mock
	private LeftNavigationCategoryFacade leftNavigationCategoryFacade;
	@Mock
	private Pair<String, String> categoryPair;
	@Mock
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;



	private final String categoryStr = "category";
	private final String categoryPage = "categoryPage";
	private final String categoryPageUrl = "categoryPageUrl";
	private final String categoryPageCategory = "categoryPagecategory";
	private final String categoryCode = "111111";
	private final String sortCode = "111111";
	private final String query = "q";


	/**
	 * @throws CMSItemNotFoundException
	 */

	/**
	 * test case for performSearchAndGetResultsPage
	 *
	 * @throws UnsupportedEncodingException
	 */
	@Test
	public void performSearchAndGetResultsPageTest() throws UnsupportedEncodingException
	{
		Mockito.when(commerceCategoryService.getCategoryForCode(categoryCode)).thenReturn(category);
		Mockito.when(simonCategoryPageController.getCategoryModelUrlResolver().resolve(category)).thenReturn(categoryStr);
		Mockito.when(request.getContextPath()).thenReturn(categoryPage);
		Mockito.when(request.getRequestURI()).thenReturn(categoryPageUrl);
		Mockito.when(response.encodeURL(categoryPageCategory)).thenReturn(categoryPageCategory);
		Mockito.when(siteConfigService.getInt("storefront.search.pageSize", 0)).thenReturn(1);
		Mockito.when(simonCategoryPageController.getProductSearchFacade()).thenReturn(productSearchFacade);
		Mockito.when(searchPageData.getPagination()).thenReturn(paginationData);
		Mockito.when(searchPageData.getPagination().getNumberOfPages()).thenReturn(2);
		Mockito.when(categorySearch.getSearchPageData()).thenReturn(productCategorySearchPageData);
		Mockito.when(leftNavigationCategoryFacade.getL1AndLeafCategoryCode(categoryCode)).thenReturn(categoryPair);
		Mockito.when(categoryPair.getLeft()).thenReturn("Left");
		Mockito.when(categoryPair.getRight()).thenReturn("Right");
		Mockito.when(request.getContextPath()).thenReturn("Right");
		Mockito.when(response.encodeURL("Right" + "Right")).thenReturn("Right");
		Mockito.when(extCategoryModelUrlResolver.resolveCategoryPath(null, categoryCode)).thenReturn("Right");
		@SuppressWarnings("static-access")
		final String redirection = simonCategoryPageController.category(categoryCode, query, 0, showMode.All, sortCode, page,
				request, response);

		Assert.assertTrue("redirect url is not category", redirection.equals("redirect:Right"));
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test
	public void performSearchAndGetResultsPageProductGridPageTest() throws UnsupportedEncodingException, NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{

		Mockito.when(simonCategoryPageController.getSimonCategoryPage(category)).thenReturn(categoryPageModel);
		Mockito.when(commerceCategoryService.getCategoryForCode(categoryCode)).thenReturn(category);
		Mockito.when(simonCategoryPageController.getCategoryModelUrlResolver().resolve(category)).thenReturn(categoryStr);
		Mockito.when(request.getContextPath()).thenReturn(categoryPage);
		Mockito.when(request.getRequestURI()).thenReturn(categoryPageCategory);
		Mockito.when(response.encodeURL(categoryPageCategory)).thenReturn(categoryPageCategory);
		Mockito.when(siteConfigService.getInt("storefront.search.pageSize", 0)).thenReturn(1);
		Mockito.when(simonCategoryPageController.getProductSearchFacade()).thenReturn(productSearchFacade);
		Mockito.when(searchPageData.getPagination()).thenReturn(paginationData);
		Mockito.when(searchPageData.getPagination().getNumberOfPages()).thenReturn(2);
		Mockito.when(simonCategoryPageController.getSimonCategorySearchEvaluator(categoryCode, query, 0, showMode.All, sortCode,
				categoryPageModel)).thenReturn(categorySearch);
		Mockito.when(categorySearch.getSearchPageData()).thenReturn(productCategorySearchPageData);
		Mockito.when(productCategorySearchPageData.getPagination()).thenReturn(paginationData);
		Mockito.when(paginationData.getNumberOfPages()).thenReturn(2);
		Mockito.when(simonCategoryPageController.getSearchBreadcrumbBuilder()).thenReturn(searchBreadcrumbBuilder);
		final List<Breadcrumb> breadcrumbList = new ArrayList<>();
		breadcrumbList.add(breadcrumb);
		Mockito.when(searchBreadcrumbBuilder.getBreadcrumbs(categoryCode, productCategorySearchPageData))
				.thenReturn(breadcrumbList);
		Mockito.when(simonCategoryPageController.getCustomerLocationService()).thenReturn(customerLocationService);
		Mockito.when(customerLocationService.getUserLocation()).thenReturn(userLocationData);
		Mockito.doNothing().when(simonCategoryPageController).doUpdatePageTitle(category, page);
		Mockito.when(pageTitleResolver.resolveCategoryPageTitle(category)).thenReturn("pageTitleResolver");
		Mockito.when(session.getServletContext()).thenReturn(servletContext);
		Mockito.when(request.getSession()).thenReturn(session);
		Mockito.when(WebApplicationContextUtils.getWebApplicationContext(servletContext)).thenReturn(appContext);
		Mockito.when(WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext)).thenReturn(appContext);
		Mockito.when(SpringHelper.getSpringBean(request, "requestContextData", RequestContextData.class, true))
				.thenReturn(requestContextData);
		Mockito.when(categorySearch.getCategoryPage()).thenReturn(categoryPageModel);

		final List<ProductData> productDataList = new ArrayList<>();
		productDataList.add(productData);
		Mockito.when(productCategorySearchPageData.getResults()).thenReturn(productDataList);

		final List<OfferData> offerDataList = new ArrayList<>();
		offerDataList.add(offerData);
		Mockito.when(offerFacade.getOffersForProductCode(null)).thenReturn(offerDataList);
		Mockito.when(searchQueryData.getValue()).thenReturn(null);
		Mockito.when(leftNavigationCategoryFacade.getL1AndLeafCategoryCode(categoryCode)).thenReturn(categoryPair);
		Mockito.when(categoryPair.getLeft()).thenReturn("Left");
		Mockito.when(categoryPair.getRight()).thenReturn("Right");
		Mockito.when(request.getContextPath()).thenReturn("Right");
		Mockito.when(response.encodeURL("Right" + "Right")).thenReturn("Right");
		Mockito.when(extCategoryModelUrlResolver.resolveCategoryPath(null, categoryCode)).thenReturn("Right");


		final String redirection = simonCategoryPageController.category(categoryCode, query, 0, showMode.All, sortCode, page,
				request, response);
		Assert.assertTrue("redirect url is not productGridPage", redirection.equals("redirect:Right"));
	}

}
