package com.simon.storefront.checkout.steps.validation.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorfacades.flow.CheckoutFlowFacade;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.simon.facades.order.ExtCheckoutFacade;


/**
 * Test Class for {@link SimonDefaultSummaryCheckoutStepValidator}
 */
@UnitTest
public class SimonDefaultSummaryCheckoutStepValidatorTest
{
	private static final String OTHER = "Other";

	private static final String PICKUP = "pickup";

	@InjectMocks
	@Spy
	SimonDefaultSummaryCheckoutStepValidator simonDefaultSummaryCheckoutStepValidator;

	@Mock
	ExtCheckoutFacade extCheckoutFacade;

	@Mock
	RedirectAttributes redirectAttributes;

	@Mock
	CheckoutFlowFacade checkoutFlowFacade;

	@Mock
	CheckoutFacade checkoutFacade;

	@Mock
	CartData cartData;

	@Mock
	DeliveryModeData deliveryModelData;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(simonDefaultSummaryCheckoutStepValidator.getCheckoutFlowFacade()).thenReturn(checkoutFlowFacade);
		when(simonDefaultSummaryCheckoutStepValidator.getCheckoutFacade()).thenReturn(checkoutFacade);
	}

	/**
	 * tests {@link SimonDefaultSummaryCheckoutStepValidator#validateOnEnter(RedirectAttributes)} when checkout flow does
	 * not have a valid cart.
	 */
	@Test
	public void testValidateOnEnterWhenCartResultIsNotNull()
	{
		when(checkoutFlowFacade.hasValidCart()).thenReturn(Boolean.FALSE);
		assertEquals(ValidationResults.REDIRECT_TO_CART,
				simonDefaultSummaryCheckoutStepValidator.validateOnEnter(redirectAttributes));
	}

	/**
	 * tests {@link SimonDefaultSummaryCheckoutStepValidator#validateOnEnter(RedirectAttributes)} when checkout flow has
	 * a valid cart but no information about delivery address, delivery mode and payment.
	 */
	@Test
	public void testValidateOnEnterWhenCartResultNullPaymentResultNotNull()
	{
		when(checkoutFlowFacade.hasValidCart()).thenReturn(Boolean.TRUE);
		when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(Boolean.FALSE);
		when(checkoutFlowFacade.hasNoDeliveryMode()).thenReturn(Boolean.FALSE);
		when(extCheckoutFacade.hasPaymentInfo()).thenReturn(Boolean.FALSE);
		assertEquals(ValidationResults.REDIRECT_TO_PAYMENT_METHOD,
				simonDefaultSummaryCheckoutStepValidator.validateOnEnter(redirectAttributes));
	}

	/**
	 * tests {@link SimonDefaultSummaryCheckoutStepValidator#validateOnEnter(RedirectAttributes)} when checkout flow has
	 * a valid cart , payment info, information about shipping items and pick-up items but no information about delivery
	 * address and delivery mode.
	 */
	@Test
	public void testValidateOnEnterWhenCartAndPaymentResultBothNull()
	{
		when(extCheckoutFacade.getCheckoutCart()).thenReturn(cartData);
		when(checkoutFlowFacade.hasValidCart()).thenReturn(Boolean.TRUE);
		when(checkoutFlowFacade.hasNoDeliveryAddress()).thenReturn(Boolean.FALSE);
		when(checkoutFlowFacade.hasNoDeliveryMode()).thenReturn(Boolean.FALSE);
		when(extCheckoutFacade.hasPaymentInfo()).thenReturn(Boolean.TRUE);
		when(checkoutFacade.hasShippingItems()).thenReturn(Boolean.TRUE);
		when(checkoutFacade.hasPickUpItems()).thenReturn(Boolean.TRUE);

		when(checkoutFacade.hasPickUpItems()).thenReturn(Boolean.TRUE);

		when(cartData.getDeliveryMode()).thenReturn(deliveryModelData);

		when(deliveryModelData.getCode()).thenReturn("pickup");


		assertEquals(ValidationResults.REDIRECT_TO_PICKUP_LOCATION,
				simonDefaultSummaryCheckoutStepValidator.validateOnEnter(redirectAttributes));
	}

	/**
	 * tests {@link SimonDefaultSummaryCheckoutStepValidator#checkPaymentMethodAndPickup(RedirectAttributes)} when
	 * delivery mode is pickup.
	 */
	@Test
	public void testCheckPaymentMethodAndPickupWhenDeliveryModeIsPickup()
	{
		when(extCheckoutFacade.getCheckoutCart()).thenReturn(cartData);
		when(extCheckoutFacade.hasPaymentInfo()).thenReturn(Boolean.TRUE);
		when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		when(checkoutFacade.hasShippingItems()).thenReturn(Boolean.FALSE);
		when(checkoutFacade.hasPickUpItems()).thenReturn(Boolean.FALSE);
		when(cartData.getDeliveryMode()).thenReturn(deliveryModelData);
		when(deliveryModelData.getCode()).thenReturn(PICKUP);
		assertEquals(ValidationResults.REDIRECT_TO_PICKUP_LOCATION,
				simonDefaultSummaryCheckoutStepValidator.checkPaymentMethodAndPickup(redirectAttributes));
	}

	/**
	 * tests {@link SimonDefaultSummaryCheckoutStepValidator#checkPaymentMethodAndPickup(RedirectAttributes)} when
	 * delivery mode is not pickup.
	 */
	@Test
	public void testCheckPaymentMethodAndPickupWhenDeliveryModeNotPickup()
	{
		when(extCheckoutFacade.getCheckoutCart()).thenReturn(cartData);
		when(extCheckoutFacade.hasPaymentInfo()).thenReturn(Boolean.TRUE);
		when(checkoutFacade.getCheckoutCart()).thenReturn(cartData);
		when(checkoutFacade.hasShippingItems()).thenReturn(Boolean.FALSE);
		when(checkoutFacade.hasPickUpItems()).thenReturn(Boolean.FALSE);
		when(cartData.getDeliveryMode()).thenReturn(deliveryModelData);
		when(deliveryModelData.getCode()).thenReturn(OTHER);
		assertEquals(null, simonDefaultSummaryCheckoutStepValidator.checkPaymentMethodAndPickup(redirectAttributes));
	}

}
