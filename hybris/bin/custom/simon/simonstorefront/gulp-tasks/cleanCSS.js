var gulp = require('gulp');
var clean = require('gulp-clean');
 
gulp.task('cleanCSS', function () {
    return gulp.src(['./web/webroot/_ui/responsive/simon-theme/css/'], 
	{read: false})
        .pipe(clean({force: true}));
});