var gulp = require('gulp');
var clean = require('gulp-clean');
 
gulp.task('cleanTemplates', function () {
    return gulp.src(['./web/webroot/_ui/responsive/simon-theme/templates/',
                     './web/webroot/_ui/responsive/simon-theme/compiledTemplates/'], 
	{read: false})
        .pipe(clean({force: true}));
});