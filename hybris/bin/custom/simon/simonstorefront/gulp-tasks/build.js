({
	appDir : "../web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/js",
	baseUrl : ".",
	dir : "../web/webroot/_ui/responsive/simon-theme/js",
	mainConfigFile : "../web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/js/global.js",
	modules : [
	{
		name : "global",
		include : [ 'jquery' ]
	},{
		name : "pages/homepage",
		exclude : [ 'global' ]
	},{
		name : "pages/pdp",
		exclude : [ 'global' ]
	},{
		name : "pages/cart",
		exclude : [ 'global' ]
	},{
		name : "pages/plp",
		exclude : [ 'global' ]
	},{
		name : "pages/myaccount",
		exclude : [ 'global' ]
	},{
		name : "pages/checkout",
		exclude : [ 'global' ]
	},{
		name : "pages/orderConfirmation",
		exclude : [ 'global' ]
	},{
		name : "pages/store",
		exclude : [ 'global' ]
	},{
		name : "pages/customerServicePage",
		exclude : [ 'global' ]
	}],
	findNestedDependencies : true,
	removeCombined : true
});