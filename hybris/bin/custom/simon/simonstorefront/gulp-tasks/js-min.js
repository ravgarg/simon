var gulp = require('gulp');
var jsmin = require('gulp-uglify');
 
gulp.task('js-min',['amdOptimizeProd'], function () {
		return gulp.src('./web/webroot/_ui/responsive/simon-theme/js/*.js')
        .pipe(jsmin())
        .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/js/'));
});