var gulp = require('gulp');
var clean = require('gulp-clean');
 
gulp.task('cleanJS', function () {
    return gulp.src(['./web/webroot/_ui/responsive/simon-theme/js/'], 
	{read: false})
        .pipe(clean({force: true}));
});