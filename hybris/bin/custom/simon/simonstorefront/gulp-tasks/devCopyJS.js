var gulp = require('gulp');

//copy src files from dev to build folder
gulp.task('devCopyJS',['cleanJS'], function() {
	gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/bower_components/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/bower_components/'));
    gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/icons/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/icons/'));
    gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/fonts/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/fonts/'));
    gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/images/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/images/'));
	gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/json/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/json/'));

    return gulp.src("./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/js/**/")
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/js/'));
});
