var gulp = require('gulp');
var defineModule = require('gulp-define-module');
var handlebars = require('gulp-handlebars');

gulp.task('handlebarsCompile', ['cleanTemplates'], function(cb) {
  // Load templates from the templates/ folder relative to where gulp was executed
  return gulp.src('./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/templates/**/*.html')
    // Compile each Handlebars template source file to a template function
    .pipe(handlebars())
    // Define templates as AMD modules
    .pipe(defineModule('amd'))
    // Write the output into the templates folder
    .pipe(gulp.dest('./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/compiledTemplates/'));
});

gulp.task('handlebarsCompileDev', ['cleanTemplates'], function(cb) {
	  // Load templates from the templates/ folder relative to where gulp was executed
	  return gulp.src('./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/templates/**/*.html')
	    // Compile each Handlebars template source file to a template function
	    .pipe(handlebars())
	    // Define templates as AMD modules
	    .pipe(defineModule('amd'))
	    // Write the output into the templates folder
	    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/compiledTemplates/'));
	});