var gulp = require('gulp'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	rename = require('gulp-rename');
	
//SCSS to CSS generation
gulp.task('sass',['cleanCSS'], function(cb) {
    gulp.src('./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/scss/**/*.scss')
    	.pipe(sourcemaps.init())
    	.pipe(sass({outputStyle: 'compressed'}))
    	.pipe(sourcemaps.write('css-sourcemaps'))
        .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/css'));
    return gulp.src('./web/webroot/WEB-INF/_ui-src/responsive/simon-fe-source/scss/global.scss')
    .pipe(sourcemaps.init())
	.pipe(sass({outputStyle: 'compressed'}))
    .pipe(rename('global-IE.css'))
    .pipe(sourcemaps.write('css-sourcemaps'))
    .pipe(gulp.dest('./web/webroot/_ui/responsive/simon-theme/css/'));
});