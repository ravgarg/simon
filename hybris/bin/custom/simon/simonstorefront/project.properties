# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# you can put key/value pairs here.
# Use Config.getParameter(..) to retrieve the values during runtime.

# Specifies the location of the spring context file putted automatically to the global platform application context.
simonstorefront.application-context=simonstorefront-spring.xml

# Specifies the location of the acceleratorstorefrontcommons spring context file
simonstorefront.additionalWebSpringConfigs.acceleratorstorefrontcommons=classpath:/acceleratorstorefrontcommons/web/spring/acceleratorstorefrontcommons-spring.xml

# Specifies the location of the addonsupport web spring context file
simonstorefront.additionalWebSpringConfigs.addonsupport=classpath:/addonsupport/web/spring/addonsupport-b2c-web-spring.xml

# Specify how long resource messages should be cached.
# Set to -1 to never expire the cache.
# Suggested values:
#   Development: 1
#   Production: -1
storefront.resourceBundle.cacheSeconds=1

# Turn on test IDs for selenium and smoke tests (not for production)
#simonstorefront.testIds.enable=true

# Show debug info in the frontend pages
# Suggested values:
#   Development: true
#   Production:  false
storefront.show.debug.info=false

# Flag the tomcat JSessionID cookie as httpOnly
simonstorefront.tomcat60.context.useHttpOnly=true

# Prevent injection of JSESSIONID into URLs
simonstorefront.tomcat60.context.disableURLRewriting=true

# URLs that do not need CSRF verification
csrf.allowed.url.patterns=/[^/]+(/[^?]*)+(sop/response)$,/[^/]+(/[^?]*)+(merchant_callback)$,/[^/]+(/[^?]*)+(hop/response)$,/[^/]+(/[^?]*)+(language)$,/[^/]+(/[^?]*)+(currency)$

#csrf.allowed.url.patterns=^https?://orderpagetest.ic3.com+(/[^?]*)$

# Enable storefront static resources client side caching.
# Suggested values:
#   Development: no-cache,must-revalidate
#   Production: public,max-age=600
# In development this will prevent these resources from being cached.
# In production this will cache these static resources for only 10 minutes
# after which the client will check if the resource has changed using the
# resource's ETAG. This is required as the resource file contents could change
# without the URL changing.
#storefront.staticResourceFilter.response.header.Cache-Control=public,max-age=600
storefront.staticResourceFilter.response.header.Cache-Control=no-cache,must-revalidate


# Enable media client side caching for 1 year. This is fine because the media
# URLs change each time there is a data change within the media
mediafilter.response.header.Cache-Control=public,max-age=31536000



###########
# Context root for the storefront.
# By default this is set to the extension name.
# The XXX.webroot controls the actual context path for the extension's web module.
# The storefrontContextRoot is used to set the context path in the impexed data.
# Both of these values should be changed together.
# If you want to change these values then it is suggested that you override both of
# them in your config/local.properties file rather than changing the value here.
# If you want to remap the the context path to the root site on the web container
# then you must set these values to be blank, do not set them to be '/'
simonstorefront.webroot=/simonstorefront
storefrontContextRoot=/simonstorefront

###########
# Qualified website storefront URLs for each CMSWebSite.
# By default the storefront is accessed via a localhost URL.
# If you are running multiple websites from a single storefront them you
# may want to use different hostnames to access each website. In that case
# the system needs to know the hostnames that you have chosen to use.
# The fully qualified URL that customers will use to access the website is
# also required for asynchronous processes like email content creation.
# During development it is likely that you will either use localhost if you
# only have one website or you will use XXXX.local where XXXX is the site uid.
# To use a hostname like XXXX.local you will need to setup a mapping in your
# local system DNS hosts file. See: http://en.wikipedia.org/wiki/Hosts_(file)
# Using a scheme like XXXX.local also allows for XXXX.uat and XXXX.test to
# support multiple environments.
# In production the fully qualified URL to the website should be used.
# Also note that the port numbers should not be specified if they are the
# default values.

# For example you may setup a URL mapping as follows:
# NOTE you should define your mapping in your config/local.properties rather
# than here in the extension's properties file.
website.simon-uk.http=http://simon-uk.local:9001
website.simon-uk.https=https://simon-uk.local:9002
website.simon-de.http=http://simon-de.local:9001
website.simon-de.https=https://simon-de.local:9002
website.electronics.http=http://electronics.local:9001
website.electronics.https=https://electronics.local:9002

###########
# Qualified media URLs for each CMSWebSite used to load images and other media
media.simon-uk.http=http://simon-uk.local:9001
media.simon-uk.https=https://simon-uk.local:9002
media.simon-de.http=http://simon-de.local:9001
media.simon-de.https=https://simon-de.local:9002
media.electronics.http=http://electronics.local:9001
media.electronics.https=https://electronics.local:9002


# Google API key
# For information on generating your API Key: https://developers.google.com/maps/documentation/javascript/tutorial#api_key
#googleApiKey=

#version of the google map api
googleApiVersion=3.7

# This is the (XML format) reverse geocoding URL for version 3 of the
# Google Maps JavaScript API
google.geocoding.reverse.url=https://maps.googleapis.com/maps/api/geocode/xml

# Google Analytics Tracking IDs. Can be host specific, as listed below.
google.analytics.tracking.id=your_google_analytics_tracking_id

# Google Analytics Tracking IDs properties for the local system
#google.analytics.tracking.id.electronics.local=
#google.analytics.tracking.id.simon-uk.local=
#google.analytics.tracking.id.simon-de.local=

# Google Analytics Tracking IDs properties for the cloud
#google.analytics.tracking.id.electronics.cloud=
#google.analytics.tracking.id.simon-uk.cloud=
#google.analytics.tracking.id.simon-de.cloud=

# Google Analytics Tracking IDs properties for the qaserver
#google.analytics.tracking.id.electronics.qaserver=
#google.analytics.tracking.id.simon-uk.qaserver=
#google.analytics.tracking.id.simon-de.qaserver=


# Jirafe properties
#jirafe.api.url=your_url
#jirafe.api.token=your_api_token
#jirafe.app.id=your_api_id
#jirafe.version=your_version
#jirafe.data.url=your_data_url

# Default site ID
#jirafe.site.id=your_site_id

# Jirafe properties for the local system
#jirafe.site.id.electronics.local=
#jirafe.site.id.usd.electronics.local=
#jirafe.site.id.jpy.electronics.local=
#jirafe.site.id.eur.electronics.local=
#jirafe.site.id.simon-uk.local=
#jirafe.site.id.simon-de.local=

# Jirafe properties for the cloud
#jirafe.site.id.electronics.cloud=
#jirafe.site.id.usd.electronics.cloud=
#jirafe.site.id.jpy.electronics.cloud=
#jirafe.site.id.eur.electronics.cloud=
#jirafe.site.id.simon-uk.cloud=
#jirafe.site.id.simon-de.cloud=

# Jirafe properties for the qaserver
#jirafe.site.id.electronics.qaserver=
#jirafe.site.id.usd.electronics.qaserver=
#jirafe.site.id.jpy.electronics.qaserver=
#jirafe.site.id.eur.electronics.qaserver=
#jirafe.site.id.simon-uk.qaserver=
#jirafe.site.id.simon-de.qaserver=

############# Hosted Order Page settings ###############
########################################################
##### Common Properties
#hop.post.url=https://orderpagetest.ic3.com/hop/orderform.jsp
hop.post.url=/acceleratorservices/hop-mock

#sop.post.url=https://orderpagetest.ic3.com/hop/ProcessOrder.do
#sop.post.url=/acceleratorservices/sop-mock/process
sop.post.url=/en/checkout/multi/payment-method/spreedly/process


#These keys are valid only for MultiStep Checkout and can be commented out or set to false

site.pci.strategy=SOP
#site.pci.strategy=HOP
#hop.pci.strategy.enabled=false
#hop.pci.strategy.enabled.simon-uk=true
#hop.pci.strategy.enabled.simon-de=true
#hop.pci.strategy.enabled.electronics=true

### Remove the comment below to show the hosted order pre-post debug page (can be done on a per-site basis)
#hop.debug.mode=true
#hop.debug.mode.simon-uk=true

################### Per Site Properties #############################
# These CyberSource properties can be set on a per site basis.      #
# Simply append the name of the CMS site on the end of the property #
#####################################################################

# Default values

hop.cybersource.testCurrency=USD
hop.cybersource.merchantID=your_merchant_id
hop.cybersource.sharedSecret=your_shared_secret
hop.cybersource.serialNumber=your_serial_number
hop.cybersource.setupFee=0

# Site specific overrides
hop.cybersource.testCurrency.simon-uk=GBP
hop.cybersource.testCurrency.simon-de=EUR
hop.cybersource.testCurrency.electronics=USD

######## CyberSource Hosted Order Page Properties
##### Appearance Configuration
### URL for an image to display in the background of the order page, 
##  such as https://www.example.com/images/background.gif.
##  Note: Make sure to use a secure (HTTPS) URL so that your customers'
##  web browser does not display a security warning.
#hop.cybersource.appearance.backgroundImageURL.simon-uk=

## Use one of these values:
##  - blue, standard (Gray), gray, green, orange, red, custom
## If you choose custom, the following four fields become required.
hop.cybersource.appearance.colorScheme=blue
hop.cybersource.appearance.colorScheme.simon-uk=orange

## Enable these properties if you choose "custom" for [hop.cybersource.appearance.colorScheme].
#hop.cybersource.appearance.barColor.simon-uk=#37B837
#hop.cybersource.appearance.barTextColor.simon-uk=#FFFFFF
#hop.cybersource.appearance.messageBoxBackgroundColor.simon-uk=#BBF1BB
#hop.cybersource.appearance.requiredFieldColor.simon-uk=#000000
###


###########
# Search results page size configuration per store:
# Set to zero to load the default page size
storefront.search.pageSize.Desktop=20
# setting for mobile
storefront.search.pageSize.Mobile=20
# site specific settings
#storefront.search.pageSize.simon-uk.Mobile=20
#storefront.search.pageSize.simon-de.Mobile=20
#storefront.search.pageSize.electronics.Mobile=20

###########
# StoreLocator results page size configuration per store:
# Set to zero to load the default page size
storefront.storelocator.pageSize.Desktop=10
# setting for mobile
storefront.storelocator.pageSize.Mobile=5


###########
# Configuration for showing checkout flow options on the cart page
# The accelerator has a strategy to select the checkout flow for a site. This strategy
# can be overridden by selecting a different strategy on the cart page. This may be useful
# during development while the desired strategy is being developed.
# For production this should be disabled. If no configuration is specified then FALSE is assumed.
# The configuration below is set to enable checkout flow selection for all sites.
# The configuration can also be overridden per site.
storefront.show.checkout.flows=true
# Specify configuration for a single site
#storefront.show.checkout.flows.electronics=true

###########
# CSS and JS files compression into one using wro4j project 
# (https://github.com/wro4j/wro4j) is disabled by default.
# Here you can enable it on site/frontend level
storefront.wro4j.enabled=false
#Definition of which countries regions should be attached to the address
resolve.country.regions=US,CA

#Number of pagination results for PickupInStore UI search
pointofservice.display.search.results.count=100

#Fallback taxcode is returned by DefaultTaxCodeStrategy when it cannot find taxCode for product and taxArea
#Different value can be configure for different base store by adding base store name at the end of property name
externaltax.fallbacktaxcode=PC040100
externaltax.fallbacktaxcode.electronics=P0000000
externaltax.fallbacktaxcode.simon-uk=PC040100
externaltax.fallbacktaxcode.simon-de=PC040100

#Number of pagination results to display
pagination.number.results.count=5

############XSS FILTER SECURITY SETTINGS###############
xss.filter.enabled=true
simonstorefront.xss.filter.rule.src1=(?ims)[\\s\r\n]+src[\\s\r\n]*=[\\s\r\n]*\\\'(.*?)\\\'
simonstorefront.xss.filter.rule.src2=(?ims)[\\s\r\n]+src[\\s\r\n]*=[\\s\r\n]*\\\"(.*?)\\\"
simonstorefront.xss.filter.rule.src3=(?ims)[\\s\r\n]+src[\\s\r\n]*=*(.*?)>
simonstorefront.xss.filter.rule.braces=(?i)<(.*?)>
#######################################################

#https://jira.spring.io/browse/SPR-9014
spring.beaninfo.ignore=true

# Checkout URL pattern
simonstorefront.checkout.url.pattern=(^https://.*/checkout/.*)

#############################
### Weblogic specific properties
## Define the tld libs required to be copied to the WEB-INF of the generated war 
## when packaging into an ear
#############################
simonstorefront.weblogic.copy-tld-files=spring.tld,spring-form.tld,security.tld

###########
# Defines the number of entries displayed in the mini cart when the add to cart button is clicked  
simonstorefront.storefront.minicart.shownProductCount=3


##########
# Add mime support for svg format
mediatype.by.fileextension.svg=image/svg+xml

#########
# Defines the minimum and maximum number of rows for quick order
simonstorefront.quick.order.rows.min=3
simonstorefront.quick.order.rows.max=25

##########
# Defines maximum size in bytes of the upload request that takes into account additional parameters,
# such as CSRF token (please note that the size of the upload request seems to vary between browsers)
import.csv.max.upload.size.bytes=11240
# Defines maximum size in bytes of the uploaded CSV file
import.csv.file.max.size.bytes=10240

##########
# When restoring a saved cart, one copy of the restored saved cart can be kept. The name of this copied/(cloned) cart is
# the original saved cart name + copy#. This property set the regex for the name suffix of #.
commerceservices.clone.savecart.name.regex=(\\s+\\d*)$

##########
# Defines whether we want to update the uploading saved carts, which show the message of "CSV file is importing..."
refresh.uploading.saved.cart=true
# Defines the interval of the requests for refreshing uploading saved cart
refresh.uploading.saved.cart.interval=5000

##########
# Quotes #
##########
# number of comments to show initially (integer)
quote.pagination.numberofcomments=4
# The minimum amount required to be able to initiate a quote
quote.request.initiation.threshold=25000

########Defines the Year start and end in the Account Pages dropdown 
simonstorefront.account.birthyear.start=1907
simonstorefront.account.countries.zipcode.valid=US,CA
user.register.page.captcha.enabled=true

########### My Account : My Center ##############
mycenter.page.montreal.mall.updated.url.stores=store_listing
mycenter.page.montreal.mall.updated.url.deals=sales
mycenter.page.montreal.mall.updated.url.travel-here=travel_tourism

##### PERFORMANCE TEST SPECIFIC PROPERTIES##########
simon.spreedly.validate.card.browser.call.enable.stub.flag=false

##### REQUIRED FIELDS VALIDATION CODE STUB PROPERTIES##########
simon.twotap.validate.required.field.call.enable.stub.flag=true

##### VERIFY CONFIGURABLE PARAMETR FLAG PROPERTIES##########
simon.checkout.verify.configurable.field.flag=true

##### Designer SOLR Results page size #########
storefront.designer.result.pageSize=10000

############## XSS filter entries ##################
############XSS FILTER SECURITY SETTINGS###############
simonstorefront.xss.filter.rule.simon1=<(.*?)script(.*?)>(.*?)</(.*?)script(.*?)>
simonstorefront.xss.filter.rule.simon2=<(.*?)script(.*?)>(.*?)</(.*?)script(.*?)>
simonstorefront.xss.filter.rule.simon3=<(.*?)script(.*?)>(.*?)</(.*?)script(.*?)>
simonstorefront.xss.filter.rule.simon4=</script(.*?)>
simonstorefront.xss.filter.rule.simon5=<script(.*?)>
simonstorefront.xss.filter.rule.simon6=eval\\((.*?)\\)
simonstorefront.xss.filter.rule.simon7=expression\\((.*?)\\)
simonstorefront.xss.filter.rule.simon8=onload(.*?)=
simonstorefront.xss.filter.rule.simon9=<(\\s*)(/\\s*)?script(\\s*)> 
simonstorefront.xss.filter.rule.simon10=%3Cscript%3E
simonstorefront.xss.filter.rule.simon11=%3Cscript%3E
simonstorefront.xss.filter.rule.simon12=alert%28
simonstorefront.xss.filter.rule.simon13=document(.*)\\.(.*)cookie
simonstorefront.xss.filter.rule.simon14=eval(\\s*)\\(
simonstorefront.xss.filter.rule.simon15=setTimeout(\\s*)\\(
simonstorefront.xss.filter.rule.simon16=setInterval(\\s*)\\(
simonstorefront.xss.filter.rule.simon17=execScript(\\s*)\\(
simonstorefront.xss.filter.rule.simon18=(?i)javascript(?-i):
simonstorefront.xss.filter.rule.simon19=(?i)onclick(?-i)
simonstorefront.xss.filter.rule.simon20=(?i)ondblclick(?-i)
simonstorefront.xss.filter.rule.simon21=(?i)onmouseover(?-i)
simonstorefront.xss.filter.rule.simon22=(?i)onmousedown(?-i)
simonstorefront.xss.filter.rule.simon23=(?i)onmouseup(?-i)
simonstorefront.xss.filter.rule.simon24=(?i)onmousemove(?-i)
simonstorefront.xss.filter.rule.simon25=(?i)onmouseout(?-i)
simonstorefront.xss.filter.rule.simon26=(?i)onmouseout(?-i)
simonstorefront.xss.filter.rule.simon27=(?i)onfocus(?-i)
simonstorefront.xss.filter.rule.simon28=(?i)onblur(?-i)
simonstorefront.xss.filter.rule.simon29=(?i)onkeypress(?-i)
simonstorefront.xss.filter.rule.simon30=(?i)onkeypress(?-i)
simonstorefront.xss.filter.rule.simon31=(?i)onkeydown(?-i)
simonstorefront.xss.filter.rule.simon32=(?i)onload(?-i)
simonstorefront.xss.filter.rule.simon33=(?i)onreset(?-i)
simonstorefront.xss.filter.rule.simon34=(?i)onselect(?-i)
simonstorefront.xss.filter.rule.simon35=(?i)onsubmit(?-i) 
simonstorefront.xss.filter.rule.simon37=(?i)onunload(?-i)
simonstorefront.xss.filter.rule.simon38=%2522
simonstorefront.xss.filter.rule.simon39=%253e
simonstorefront.xss.filter.rule.simon40=%253d
simonstorefront.xss.filter.rule.simon41=%2529
simonstorefront.xss.filter.rule.simon42=%253c
simonstorefront.xss.filter.rule.simon43=%2529
simonstorefront.xss.filter.rule.simon44=<img%20src
simonstorefront.xss.filter.rule.simon45=<img
simonstorefront.xss.filter.rule.simon46=onbegin%3d
simonstorefront.xss.filter.rule.simon47=(?i)onbegin(?-i)
simonstorefront.xss.filter.rule.simon48=style%3d
simonstorefront.xss.filter.rule.simon49=style=
simonstorefront.xss.filter.rule.simon50=\\+and\\+
simonstorefront.xss.filter.rule.simon51=\\+or\\+
simonstorefront.xss.filter.rule.equals=(?i)\\=(?i)
simonstorefront.xss.filter.rule.doublequotes=(?i)\\"(?i)
simonstorefront.xss.filter.rule.openparentheses=(?i)\\((?i)
simonstorefront.xss.filter.rule.closeparentheses=(?i)\\)(?i)
simonstorefront.xss.filter.rule.lessthan=(?i)\\<(?i)
simonstorefront.xss.filter.rule.greaterthan=(?i)\\>(?i)
simonstorefront.xss.filter.rule.parentheses=(?i)\\((.*?)\\)(?i)
simonstorefront.xss.filter.rule.quotes=(?i)\\"(.*?)\\"(?i)
# XSS Filter
simon.xss.filter.exclude.url=/j_spring_security_check
simon.xss.filter.exclude.url.seperator=,

# BTG
storefront.btg.enabled=false
