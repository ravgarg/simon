package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.fulfilmentprocess.actions.order.CreateMarketplaceConsignmentsAction;
import com.simon.core.services.ExtMiraklOrderService;
import com.simon.core.services.RetailerService;


/**
 *
 */
public class ExtCreateMarketplaceConsignmentsAction extends CreateMarketplaceConsignmentsAction
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtCreateMarketplaceConsignmentsAction.class);

	@Resource
	private RetailerService retailerService;
	@Resource
	private ExtMiraklOrderService miraklOrderService;

	/**
	 * A loop has been introduced in this method and some other changes have been done in order to sent separate orders
	 * to mirakl for each retailer in a hybris order
	 */
	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws RetryLaterException
	{
		final OrderModel order = orderProcessModel.getOrder();
		final List<String> retailers = retailerService.getAllRetailersFromOrder(order);
		for (final String retailer : retailers)
		{
			final MiraklCreatedOrders miraklCreatedOrders = miraklOrderService.loadCreatedOrdersForRetailer(order, retailer);
			if (miraklCreatedOrders == null)
			{
				LOG.info("No marketplace orders stored within order {}. Skipping marketplace consignments creation..",
						order.getCode());
				break;
			}

			final Set<MarketplaceConsignmentModel> consignments = marketplaceConsignmentService.createMarketplaceConsignments(order,
					miraklCreatedOrders);

			LOG.info("{} marketplace consignments successfully created for order {}", consignments.size(), order.getCode());
		}

	}

}
