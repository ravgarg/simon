package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;

import java.util.Collection;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.mirakl.hybris.fulfilmentprocess.actions.order.SubprocessesCompletedAction;


/**
 * Through this class completed will be complete sub process
 */
public class ExtSubprocessesCompletedAction extends SubprocessesCompletedAction
{
	private static final Logger LOG = Logger.getLogger(ExtSubprocessesCompletedAction.class);

	private static final String PROCESS_MSG = "Process: ";

	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		LOG.info(PROCESS_MSG + process.getCode() + " is checking for  " + process.getConsignmentProcesses().size()
				+ " subprocess results");

		final OrderModel order = process.getOrder();
		final Collection<ConsignmentProcessModel> consignmentProcesses = process.getConsignmentProcesses();

		if (CollectionUtils.isNotEmpty(consignmentProcesses))
		{
			consignmentProcesses.stream();

			final Optional<ConsignmentProcessModel> atleastOneConsProcessNotDone = consignmentProcesses.stream()
					.filter(consProcess -> !consProcess.isDone()).findFirst();
			final boolean allConsProcessNotDone = consignmentProcesses.stream().allMatch(consProcess -> !consProcess.isDone());

			if (allConsProcessNotDone)
			{
				LOG.info(PROCESS_MSG + process.getCode() + " found all subprocesses incomplete");
				order.setDeliveryStatus(DeliveryStatus.NOTSHIPPED);
				save(order);
				return Transition.NOK;
			}
			else if (atleastOneConsProcessNotDone.isPresent())
			{
				LOG.info(PROCESS_MSG + process.getCode() + " found subprocess " + atleastOneConsProcessNotDone.get().getCode()
						+ " incomplete -> wait again!");
				order.setDeliveryStatus(DeliveryStatus.PARTSHIPPED);
				save(order);
				return Transition.NOK;
			}
		}

		LOG.info(PROCESS_MSG + process.getCode() + " found all subprocesses complete");
		order.setDeliveryStatus(DeliveryStatus.SHIPPED);
		save(order);
		return Transition.OK;
	}

}
