package com.simon.fulfilmentprocess.actions.consignment;

import static com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State.SHIPPING;
import static com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State.WAITING_ACCEPTANCE;
import static com.mirakl.hybris.core.enums.MarketplaceConsignmentPaymentStatus.SUCCESS;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.task.RetryLaterException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State;
import com.mirakl.client.mmp.domain.order.state.MiraklOrderStatus;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.model.MarketplaceConsignmentProcessModel;
import com.mirakl.hybris.core.order.services.TakePaymentService;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.simon.core.service.mirakl.MarketPlaceService;


/**
 * This Action Class is executed upon logisticOrderUpdate.
 *
 */
public class ProcessLogisticOrderUpdateAction extends AbstractAction<MarketplaceConsignmentProcessModel>
{

	private static final Logger LOG = LoggerFactory.getLogger(ProcessLogisticOrderUpdateAction.class);

	protected MarketplaceConsignmentService marketplaceConsignmentService;

	protected TakePaymentService takePaymentService;

	protected Populator<MiraklOrder, MarketplaceConsignmentModel> updateConsignmentPopulator;

	protected MiraklMarketplacePlatformFrontApi miraklApi;

	@Resource
	private MarketPlaceService marketPlaceService;

	@Resource
	private ConfigurationService configurationService;

	private static final String PENDING_ACCEPTANCE_TO_SHIPPING = "acceptance.order.in.order.process";

	protected Map<State, Transition> stateToTransition = ImmutableMap.of(State.CLOSED, Transition.CLOSED, State.REFUSED,
			Transition.REFUSED, State.CANCELED, Transition.CANCELED, State.SHIPPING, Transition.SHIPPING, State.SHIPPED,
			Transition.SHIPPED);

	@Override
	public String execute(final MarketplaceConsignmentProcessModel consignmentProcess) throws RetryLaterException, Exception
	{
		return String.valueOf(executeAction(consignmentProcess));
	}

	/**
	 * This method is used to executeAction and make a call to processPayment method to valid/invalid order Payment.
	 *
	 * @param consignmentProcess
	 * @return Transition
	 * @throws RetryLaterException
	 * @throws Exception
	 */
	public Transition executeAction(final MarketplaceConsignmentProcessModel consignmentProcess)
			throws RetryLaterException, Exception
	{
		final MarketplaceConsignmentModel consignment = (MarketplaceConsignmentModel) consignmentProcess.getConsignment();
		final MiraklOrder miraklOrder = marketplaceConsignmentService.loadConsignmentUpdate(consignment);

		if (miraklOrder == null)
		{
			LOG.warn("No stored update was found for consignment {}", consignment.getCode());
			return Transition.WAIT;
		}

		if (!consignment.isLastUpdateProcessed())
		{
			LOG.info("Processing a received update for consignment {}", consignment.getCode());
			processConsignmentUpdate(consignment, miraklOrder);
			saveConsignment(consignment);
		}

		final MiraklOrderStatus miraklStatus = miraklOrder.getStatus();

		//		Here calling Mirakl API OR21, it will update the Order status from Pending Acceptance to Shipping in progress status
		final boolean pendingToShipping = configurationService.getConfiguration().getBoolean(PENDING_ACCEPTANCE_TO_SHIPPING);
		if (pendingToShipping && WAITING_ACCEPTANCE.equals(miraklStatus.getState()))
		{
			marketPlaceService.acceptOrRejectOrder(consignment, true);
		}

		if (SHIPPING.equals(miraklStatus.getState()) && !(SUCCESS.equals(consignment.getPaymentStatus())))
		{
			marketPlaceService.processPayment(consignment);
			consignment.setPaymentStatus(SUCCESS);
			getModelService().save(consignment);
		}
		return nextTransition(miraklStatus);
	}

	protected void processConsignmentUpdate(final MarketplaceConsignmentModel consignment, final MiraklOrder miraklOrder)
	{
		updateConsignmentPopulator.populate(miraklOrder, consignment);
		consignment.setLastUpdateProcessed(true);
	}

	protected void saveConsignment(final MarketplaceConsignmentModel consignment)
	{
		getModelService().saveAll(consignment.getConsignmentEntries());
		getModelService().save(consignment);
		final List<ReturnRequestModel> returnRequests = ((OrderModel) consignment.getOrder()).getReturnRequests();
		if (CollectionUtils.isNotEmpty(returnRequests))
		{
			getModelService().saveAll(returnRequests);
			for (final ReturnRequestModel returnRequest : returnRequests)
			{
				getModelService().saveAll(returnRequest.getReturnEntries());
			}
		}
	}

	protected Transition nextTransition(final MiraklOrderStatus miraklStatus)
	{
		final Transition transition = stateToTransition.get(miraklStatus.getState());
		return (transition != null) ? transition : Transition.WAIT;
	}

	protected enum Transition
	{
		WAIT, CANCELED, CLOSED, REFUSED, SHIPPING, SHIPPED;

		private static final Set<String> stringValues = Sets
				.newHashSet(Iterables.transform(Arrays.asList(values()), Functions.toStringFunction()));

		public static Set<String> getStringValues()
		{
			return stringValues;
		}
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

	/**
	 * @param marketplaceConsignmentService
	 */
	@Required
	public void setMarketplaceConsignmentService(final MarketplaceConsignmentService marketplaceConsignmentService)
	{
		this.marketplaceConsignmentService = marketplaceConsignmentService;
	}

	/**
	 * @param takePaymentService
	 */
	@Required
	public void setTakePaymentService(final TakePaymentService takePaymentService)
	{
		this.takePaymentService = takePaymentService;
	}

	/**
	 * @param miraklApi
	 */
	@Required
	public void setMiraklApi(final MiraklMarketplacePlatformFrontApi miraklApi)
	{
		this.miraklApi = miraklApi;
	}

	/**
	 * @param updateConsignmentPopulator
	 */
	@Required
	public void setUpdateConsignmentPopulator(final Populator<MiraklOrder, MarketplaceConsignmentModel> updateConsignmentPopulator)
	{
		this.updateConsignmentPopulator = updateConsignmentPopulator;
	}

}
