package com.simon.fulfilmentprocess.constants;

@SuppressWarnings("PMD")
public class SimonFulfilmentProcessConstants extends GeneratedSimonFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "simonfulfilmentprocess";
	
	private SimonFulfilmentProcessConstants()
	{
		//empty
	}
	
	
}
