package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService.Nothing;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.tx.Transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.simon.facades.share.email.ShareEmailFacade;


/**
 * This action class is used to Share email for Order confirmation
 */
public class ExtSendOrderCompletedNotificationAction extends AbstractProceduralAction<OrderProcessModel>
{

	private static final Logger LOG = Logger.getLogger(ExtSendOrderCompletedNotificationAction.class);

	@Resource(name = "shareEmailFacade")
	private ShareEmailFacade shareEmailFacade;

	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ImpersonationService impersonationService;

	@Override
	public void executeAction(final OrderProcessModel orderProcessModel) throws Exception
	{
		final OrderModel orderModel = orderProcessModel.getOrder();
		LOG.info("Order Model code." + orderModel.getCode());
		final ImpersonationContext context = new ImpersonationContext();
		final Collection<CatalogVersionModel> list = new ArrayList<>();
		list.add(orderModel.getEntries().get(0).getProduct().getCatalogVersion());
		context.setCatalogVersions(list);
		context.setCurrency(orderModel.getCurrency());
		context.setSite(orderModel.getSite());
		context.setLanguage(commonI18NService.getLanguage(Locale.ENGLISH.getLanguage()));

		LOG.info("catalog version." + orderModel.getEntries().get(0).getProduct().getCatalogVersion());

		impersonationService.executeInContext(context,
				new ImpersonationService.Executor<ImpersonationService.Nothing, ImpersonationService.Nothing>()
				{
					@Override
					public Nothing execute() throws Nothing
					{

						Transaction.current().enableDelayedStore(false);
						final boolean emailSentFlag = shareEmailFacade
								.getOrderDetailsToSharedOrderConfirmationOrRejectionEmail(orderModel, "CONFIRM");

						if (emailSentFlag)
						{
							LOG.info("Order Confirmation Email successfully sent.");
						}
						else
						{
							LOG.info("Order Confirmation Email not sent successfully.");
						}
						return null;
					}

				});

	}

}
