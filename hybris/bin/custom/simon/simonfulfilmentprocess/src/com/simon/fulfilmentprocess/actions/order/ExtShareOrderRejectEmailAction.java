package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService.Nothing;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractProceduralAction;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.tx.Transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.facades.share.email.ShareEmailFacade;


/**
 * This action class is used to Share email for Order Rejection
 */
public class ExtShareOrderRejectEmailAction extends AbstractProceduralAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtShareOrderRejectEmailAction.class);

	@Resource(name = "shareEmailFacade")
	private ShareEmailFacade shareEmailFacade;

	@Resource
	private CommonI18NService commonI18NService;
	@Resource
	private ImpersonationService impersonationService;

	@Override
	public void executeAction(final OrderProcessModel process) throws Exception
	{
		final OrderModel orderModel = process.getOrder();

		LOG.info("Order Model code." + process.getCode());
		final ImpersonationContext context = new ImpersonationContext();
		final Collection<CatalogVersionModel> list = new ArrayList<>();
		list.add(orderModel.getEntries().get(0).getProduct().getCatalogVersion());
		context.setCatalogVersions(list);
		context.setCurrency(orderModel.getCurrency());
		context.setSite(orderModel.getSite());
		context.setLanguage(commonI18NService.getLanguage(Locale.ENGLISH.getLanguage()));

		LOG.info("catalog version." + orderModel.getEntries().get(0).getProduct().getCatalogVersion());

		impersonationService.executeInContext(context,
				new ImpersonationService.Executor<ImpersonationService.Nothing, ImpersonationService.Nothing>()
				{
					@Override
					public Nothing execute() throws Nothing
					{

						Transaction.current().enableDelayedStore(false);
						final boolean shareEmailFlag = shareEmailFacade
								.getOrderDetailsToSharedOrderConfirmationOrRejectionEmail(process.getOrder(), "REJECT");
						if (shareEmailFlag)
						{
							LOG.debug("Order Rejection Email is send for order {}", process.getOrder().getCode());
						}
						else
						{
							LOG.error("Exception occured while sharing Order rejection Email for order {} ",
									process.getOrder().getCode());
						}
						return null;
					}

				});
	}

}
