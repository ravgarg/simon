package com.simon.fulfilmentprocess.impl;

import de.hybris.platform.core.model.order.OrderModel;

import com.mirakl.hybris.fulfilmentprocess.impl.DefaultCheckOrderService;


/**
 * ExtSimonCheckOrderServiceImpl created only for override the method from CheckOrderService interface
 */
public class ExtCheckOrderServiceImpl extends DefaultCheckOrderService
{

	@Override
	public boolean check(final OrderModel order)
	{
		boolean status = true;
		if (!order.getCalculated().booleanValue() || order.getEntries().isEmpty() || order.getPaymentInfo() == null)
		{
			status = false;
		}
		return status;
	}
}
