package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;
import de.hybris.platform.servicelayer.event.EventService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.domain.order.create.MiraklOfferNotShippable;
import com.mirakl.hybris.core.fulfilment.events.NotShippableOffersEvent;
import com.mirakl.hybris.fulfilmentprocess.actions.order.CreateMarketplaceOrderAction;
import com.simon.core.services.ExtMiraklOrderService;


/**
 * This action class is used to Export Order To Mirakl
 */
public class ExtCreateMarketplaceOrderAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(CreateMarketplaceOrderAction.class);

	@Resource
	private ExtMiraklOrderService miraklOrderService;

	@Resource
	private EventService eventService;

	@Override
	public final String execute(final OrderProcessModel process) throws Exception
	{
		return executeAction(process).toString();
	}


	/**
	 * By this method creating Market place orders and calling OR011
	 *
	 * @param orderProcessModel
	 * @return Transition
	 */
	public Transition executeAction(final OrderProcessModel orderProcessModel)
	{
		final OrderModel order = orderProcessModel.getOrder();

		if (CollectionUtils.isNotEmpty(order.getMarketplaceEntries()))
		{
			final List<MiraklCreatedOrders> marketplaceOrders = miraklOrderService.createMarketplaceOrdersForRetailers(order);
			if (marketplaceOrders != null)
			{
				for (final MiraklCreatedOrders marketplaceOrder : marketplaceOrders)
				{
					handleNotShippableOffers(marketplaceOrder, order);
				}
				return Transition.OK;
			}
		}

		LOG.info("Order Export to Mirakl:: No marketplace entries within order {}. Skipping call to OR01..", order.getCode());
		return Transition.NOK;

	}

	protected void handleNotShippableOffers(final MiraklCreatedOrders marketplaceOrders, final OrderModel orderModel)
	{
		final List<MiraklOfferNotShippable> notShippableOffers = marketplaceOrders.getOffersNotShippable();
		if (CollectionUtils.isNotEmpty(notShippableOffers))
		{
			eventService.publishEvent(new NotShippableOffersEvent(notShippableOffers, orderModel));
		}
	}


	/**
	 * Making Transition status
	 *
	 */
	public enum Transition
	{
		OK, NOK;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
