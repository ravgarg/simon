package com.simon.fulfilmentprocess.actions.order;

import org.apache.log4j.Logger;

import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.task.RetryLaterException;

public class SimonPaymentAction extends AbstractSimpleDecisionAction<OrderProcessModel> {
	private static final Logger LOG = Logger.getLogger(SimonPaymentAction.class);

	@Override
	public de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition executeAction(
			OrderProcessModel order) throws RetryLaterException, Exception {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Simon uses Spreedly/TwoTap checkout.  PaymentAction is deligated to Retailer's site");
		}
		return Transition.OK;
	}
}
