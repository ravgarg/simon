/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import com.simon.generated.fulfilmentprocess.actions.order.CheckAuthorizeOrderPaymentAction;


/**
 * This action implements payment authorization using {@link CreditCardPaymentInfoModel}. Any other payment model could
 * be implemented here, or in a separate action, if the process flow differs.
 */
public class SimonCheckAuthorizeOrderPaymentAction extends CheckAuthorizeOrderPaymentAction
{
	@Override
	public Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		if (order != null)
		{
			if (order.getPaymentInfo() instanceof InvoicePaymentInfoModel
					|| order.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
			{
				return Transition.OK;
			}
			else
			{
				return assignStatusForOrder(order);
			}
		}
		return Transition.NOK;
	}

}
