package com.simon.fulfilmentprocess.actions.order;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractAction;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 * Through this action class if order is accepted on TT then process will continue either if rejected then process stop
 * here.
 */
public class ExtOrderAcceptAndRejectAction extends AbstractAction<OrderProcessModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtOrderAcceptAndRejectAction.class);


	@Override
	public final String execute(final OrderProcessModel process) throws Exception
	{
		return executeAction(process).toString();
	}

	/**
	 * Check two tap order status is confirm or not
	 *
	 * @param process
	 * @return
	 */
	protected Transition executeAction(final OrderProcessModel process)
	{
		final OrderModel order = process.getOrder();

		if (OrderStatus.REJECTED.equals(order.getStatus()))
		{
			LOG.debug("This order is not accepted", order.getCode());
			return Transition.REJECTED;
		}
		return Transition.ACCEPTED;
	}


	/**
	 * Making Transition status
	 *
	 */
	public enum Transition
	{
		ACCEPTED, REJECTED;

		public static Set<String> getStringValues()
		{
			final Set<String> res = new HashSet<>();
			for (final Transition transitions : Transition.values())
			{
				res.add(transitions.toString());
			}
			return res;
		}
	}

	@Override
	public Set<String> getTransitions()
	{
		return Transition.getStringValues();
	}

}
