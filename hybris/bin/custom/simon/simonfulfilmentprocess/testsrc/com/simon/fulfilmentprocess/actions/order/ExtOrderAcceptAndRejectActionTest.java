package com.simon.fulfilmentprocess.actions.order;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.fulfilmentprocess.actions.order.ExtOrderAcceptAndRejectAction.Transition;


@UnitTest
public class ExtOrderAcceptAndRejectActionTest
{

	@InjectMocks
	private ExtOrderAcceptAndRejectAction extOrderAcceptAndRejectAction;

	@Mock
	private OrderProcessModel process;

	@Mock
	private OrderModel order;

	@Mock
	private OrderStatus orderStatus;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void executeActionWhenOrderRejected()
	{
		when(process.getOrder()).thenReturn(order);
		when(order.getStatus()).thenReturn(orderStatus.REJECTED);
		assertEquals(Transition.REJECTED, extOrderAcceptAndRejectAction.executeAction(process));
	}

	@Test
	public void executeActionWhenOrderAccepted()
	{
		when(process.getOrder()).thenReturn(order);
		when(order.getStatus()).thenReturn(orderStatus.CHECKED_VALID);
		assertEquals(Transition.ACCEPTED, extOrderAcceptAndRejectAction.executeAction(process));
	}

}
