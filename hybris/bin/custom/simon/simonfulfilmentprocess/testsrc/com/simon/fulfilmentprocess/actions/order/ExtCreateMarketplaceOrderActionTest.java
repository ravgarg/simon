package com.simon.fulfilmentprocess.actions.order;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.event.EventService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.domain.order.create.MiraklOfferNotShippable;
import com.mirakl.hybris.core.order.services.MiraklOrderService;
import com.simon.fulfilmentprocess.actions.order.ExtCreateMarketplaceOrderAction.Transition;


@UnitTest
public class ExtCreateMarketplaceOrderActionTest
{
	@Spy
	@InjectMocks
	private ExtCreateMarketplaceOrderAction extCreateMarketplaceOrderAction;

	@Mock
	private AbstractOrderEntryModel abstractOrderEntryModel;

	@Mock
	private MiraklOfferNotShippable miraklOfferNotShippable;

	@Mock
	private MiraklCreatedOrders marketplaceOrders;

	@Mock
	private MiraklOrderService miraklOrderService;

	@Mock
	private EventService eventService;

	private List<AbstractOrderEntryModel> entryList;

	private List<MiraklOfferNotShippable> notShippableOffers;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExecuteActionWithOK() throws Exception
	{
		final OrderModel order = new OrderModel();
		final AbstractOrderEntryModel abstractOrderEntryModel = mock(AbstractOrderEntryModel.class);
		entryList = new ArrayList<>();
		entryList.add(abstractOrderEntryModel);
		order.setEntries(entryList);
		final OrderProcessModel process = new OrderProcessModel();
		process.setOrder(order);
		notShippableOffers = new ArrayList<>();
		when(miraklOrderService.createMarketplaceOrders(order)).thenReturn(marketplaceOrders);
		when(marketplaceOrders.getOffersNotShippable()).thenReturn(notShippableOffers);
		doNothing().when(extCreateMarketplaceOrderAction).handleNotShippableOffers(marketplaceOrders, order);
		assertEquals(Transition.NOK.toString(), extCreateMarketplaceOrderAction.execute(process));
	}


	@Test
	public void testExecuteActionWithNOK() throws Exception
	{
		final OrderModel order = new OrderModel();
		final OrderProcessModel process = new OrderProcessModel();
		process.setOrder(order);
		entryList = new ArrayList<>();
		notShippableOffers = new ArrayList<>();
		doNothing().when(extCreateMarketplaceOrderAction).handleNotShippableOffers(marketplaceOrders, order);
		assertEquals(Transition.NOK.toString(), extCreateMarketplaceOrderAction.execute(process));

	}




}
