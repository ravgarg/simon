package com.simon.fulfilmentprocess.actions.order;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.InvoicePaymentInfoModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SimonCheckAuthorizeOrderPaymentActionTest
{

	@InjectMocks
	@Spy
	private SimonCheckAuthorizeOrderPaymentAction simonCheckAuthorizeOrderPaymentAction;
	@Mock
	private OrderProcessModel process;

	@Mock
	private OrderModel order;
	@Mock
	private InvoicePaymentInfoModel invoicePaymentInfoModel;
	@Mock
	private CreditCardPaymentInfoModel creditCardPaymentInfoModel;

	@Test
	public void executeActionWhenOrderModelIsNull()
	{
		assertEquals(Transition.NOK, simonCheckAuthorizeOrderPaymentAction.executeAction(process));
	}

	@Test
	public void executeActionWhenOrderModelIsNotNull()
	{


		when(process.getOrder()).thenReturn(order);
		assertEquals(Transition.NOK, simonCheckAuthorizeOrderPaymentAction.executeAction(process));

	}


	@Test
	public void executeActionWhenPaymentInfoIsInstanceofInvoicePaymentInfoModel()
	{


		when(process.getOrder()).thenReturn(order);
		when(order.getPaymentInfo()).thenReturn(invoicePaymentInfoModel);
		assertEquals(Transition.OK, simonCheckAuthorizeOrderPaymentAction.executeAction(process));

	}

	@Test
	public void executeActionWhenPaymentInfoIsInstanceofCreditCardPaymentInfoModel()
	{
		when(process.getOrder()).thenReturn(order);
		when(order.getPaymentInfo()).thenReturn(creditCardPaymentInfoModel);
		assertEquals(Transition.OK, simonCheckAuthorizeOrderPaymentAction.executeAction(process));

	}

}
