package com.simon.fulfilmentprocess.actions.order;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.share.email.ShareEmailFacade;
import com.simon.fulfilmentprocess.actions.order.ExtCreateMarketplaceOrderAction.Transition;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtSendOrderCompletedNotificationActionTest
{
	@InjectMocks
	private ExtSendOrderCompletedNotificationAction sendOrderCompletedNotificationAction;

	@Mock
	private ShareEmailFacade shareEmailFacade;
	@Mock
	private ImpersonationService impersonationService;
	@Mock
	private CommonI18NService commonI18NService;

	private List<AbstractOrderEntryModel> entries;
	private AbstractOrderEntryModel entryModel;
	private ProductModel productModel;
	private CatalogVersionModel catalogVersion;
	private OrderProcessModel orderProcessModel;
	private OrderModel order;

	/**
	 *
	 */
	@Before
	public void setUp()
	{
		orderProcessModel = new OrderProcessModel();
		order = new OrderModel();
		entries = new ArrayList<AbstractOrderEntryModel>();
		entryModel = new AbstractOrderEntryModel();
		productModel = new ProductModel();
		catalogVersion = new CatalogVersionModel();
		catalogVersion.setVersion("Online");
		productModel.setCatalogVersion(catalogVersion);
		entryModel.setProduct(productModel);
		entries.add(entryModel);
		order.setCode("code");
		orderProcessModel.setOrder(order);
		order.setEntries(entries);
	}

	/**
	 * @throws Exception
	 */

	@Test
	public void testExecuteActionWithOK() throws Exception
	{
		final String transition = sendOrderCompletedNotificationAction.execute(orderProcessModel);
		assertEquals(Transition.OK.toString(), transition);
	}
}
