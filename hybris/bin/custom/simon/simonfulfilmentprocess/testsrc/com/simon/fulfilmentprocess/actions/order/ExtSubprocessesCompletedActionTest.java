package com.simon.fulfilmentprocess.actions.order;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.DeliveryStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.ordersplitting.model.ConsignmentProcessModel;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction.Transition;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.order.state.MiraklOrderStatus;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;


@UnitTest
public class ExtSubprocessesCompletedActionTest
{

	@InjectMocks
	private ExtSubprocessesCompletedAction action;

	@Mock
	private OrderProcessModel process;
	@Mock
	private OrderModel order;
	@Mock
	private ConsignmentProcessModel consignmentProcessModel;
	@Mock
	private MarketplaceConsignmentModel marketplaceConsignment;

	private ArrayList<ConsignmentProcessModel> consignmentProcesses;

	@Mock
	private MiraklOrder miraklOrder;

	@Mock
	private MiraklOrderStatus miraklOrderStatus;

	@Mock
	private ModelService modelService;



	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		consignmentProcesses = new ArrayList<>();
		consignmentProcesses.add(consignmentProcessModel);
	}

	@Test
	public void executeActionProcessCompleted()
	{
		when(process.getCode()).thenReturn("code");
		when(process.getOrder()).thenReturn(order);
		when(process.getConsignmentProcesses()).thenReturn(consignmentProcesses);
		when(consignmentProcessModel.isDone()).thenReturn(true);
		doNothing().when(order).setDeliveryStatus(DeliveryStatus.SHIPPED);
		doNothing().when(modelService).save(Mockito.any());
		action.executeAction(process);
		assertEquals(Transition.OK, action.executeAction(process));

	}

	@Test
	public void executeActionProcessNotShipped()
	{
		when(process.getCode()).thenReturn("code");
		when(process.getOrder()).thenReturn(order);
		when(process.getConsignmentProcesses()).thenReturn(consignmentProcesses);
		when(consignmentProcessModel.isDone()).thenReturn(false);
		doNothing().when(order).setDeliveryStatus(DeliveryStatus.NOTSHIPPED);
		doNothing().when(modelService).save(Mockito.any());
		action.executeAction(process);
		assertEquals(Transition.NOK, action.executeAction(process));

	}

	@Test
	public void executeActionProcessPartshipped()
	{
		when(process.getCode()).thenReturn("code");
		when(process.getOrder()).thenReturn(order);
		when(process.getConsignmentProcesses()).thenReturn(consignmentProcesses);
		when(consignmentProcessModel.isDone()).thenReturn(false);
		doNothing().when(order).setDeliveryStatus(DeliveryStatus.PARTSHIPPED);
		doNothing().when(modelService).save(Mockito.any());
		action.executeAction(process);
		assertEquals(Transition.NOK, action.executeAction(process));

	}


}
