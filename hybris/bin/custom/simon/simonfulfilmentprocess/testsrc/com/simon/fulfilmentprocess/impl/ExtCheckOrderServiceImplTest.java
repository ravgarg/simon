package com.simon.fulfilmentprocess.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.model.AdditionalCartInfoModel;


/**
 * JUnit test suite for {@link ExtCheckOrderServiceImplTest}
 */
@UnitTest
public class ExtCheckOrderServiceImplTest
{

	@InjectMocks
	@Spy
	private final ExtCheckOrderServiceImpl checkOrderServiceImpl = new ExtCheckOrderServiceImpl();

	@Mock
	private OrderModel order;

	@Mock
	private AdditionalCartInfoModel additionalCartInfoModel;

	/**
	 * set up
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * checking order Model require object
	 */
	@Test
	public void testCheckDeliveryOptionsTrue()
	{
		final AbstractOrderEntryModel abstractOrderEntry = mock(AbstractOrderEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		abstractOrderEntryList.add(abstractOrderEntry);
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.TRUE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(paymentInfoModel);
		assertTrue(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueAndOrderCalculatedIsFalse()
	{
		final AbstractOrderEntryModel abstractOrderEntry = mock(AbstractOrderEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		abstractOrderEntryList.add(abstractOrderEntry);
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.FALSE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(paymentInfoModel);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueEntriesIsNotEmpty()
	{
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.TRUE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(paymentInfoModel);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueAndPaymentInfoIsNotNull()
	{
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.FALSE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(paymentInfoModel);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueAndPaymentInfoIsNull()
	{
		final AbstractOrderEntryModel abstractOrderEntry = mock(AbstractOrderEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		abstractOrderEntryList.add(abstractOrderEntry);
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.TRUE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(null);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueAndOrderCalculatedIsFalseAndPaymentInfoIsNull()
	{
		final AbstractOrderEntryModel abstractOrderEntry = mock(AbstractOrderEntryModel.class);
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		abstractOrderEntryList.add(abstractOrderEntry);
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.FALSE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(null);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueEntriesIsNotEmptyAndPaymentInfoIsNull()
	{
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.TRUE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(null);
		assertFalse(checkOrderServiceImpl.check(order));

	}

	@Test
	public void testCheckDeliveryOptionsTrueAndAndPaymentInfoIsNull()
	{
		final List<AbstractOrderEntryModel> abstractOrderEntryList = new ArrayList<>();
		final PaymentInfoModel paymentInfoModel = mock(PaymentInfoModel.class);
		Mockito.when(order.getCalculated()).thenReturn(Boolean.FALSE);
		Mockito.when(order.getEntries()).thenReturn(abstractOrderEntryList);
		Mockito.when(order.getPaymentInfo()).thenReturn(null);
		assertFalse(checkOrderServiceImpl.check(order));

	}




}
