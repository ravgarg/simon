package com.simon.fulfilmentprocess.actions.order;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.task.RetryLaterException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;



@UnitTest
public class SimonPaymentActionTest
{

	@Spy
	@InjectMocks
	private SimonPaymentAction simonPaymentAction;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExecuteAction() throws RetryLaterException, Exception
	{

		final OrderProcessModel process = new OrderProcessModel();
		Assert.assertEquals("OK", simonPaymentAction.execute(process));
	}

}
