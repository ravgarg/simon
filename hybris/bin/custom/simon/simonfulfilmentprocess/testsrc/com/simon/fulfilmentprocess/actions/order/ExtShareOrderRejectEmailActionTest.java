package com.simon.fulfilmentprocess.actions.order;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.facades.share.email.ShareEmailFacade;


@UnitTest
public class ExtShareOrderRejectEmailActionTest
{
	@InjectMocks
	private ExtShareOrderRejectEmailAction extShareOrderRejectEmailAction;

	@Mock
	private ShareEmailFacade shareEmailFacade;

	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private ImpersonationService impersonationService;
	@Mock
	private EventService eventService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testExecuteActionWithOK() throws Throwable
	{
		final OrderProcessModel orderProcess = mock(OrderProcessModel.class);
		final OrderModel orderModel = mock(OrderModel.class);
		when(orderProcess.getOrder()).thenReturn(orderModel);
		when(orderModel.getCode()).thenReturn("Orde123");

		final List<AbstractOrderEntryModel> orderEnrty = new ArrayList<>();
		final AbstractOrderEntryModel entryModal = mock(AbstractOrderEntryModel.class);
		orderEnrty.add(entryModal);
		final ProductModel product = mock(ProductModel.class);
		when(entryModal.getProduct()).thenReturn(product);
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(product.getCatalogVersion()).thenReturn(catalogVersion);
		when(orderModel.getEntries()).thenReturn(orderEnrty);
		extShareOrderRejectEmailAction.execute(orderProcess);
		verify(impersonationService, atLeastOnce()).executeInContext(any(), any());

	}
}
