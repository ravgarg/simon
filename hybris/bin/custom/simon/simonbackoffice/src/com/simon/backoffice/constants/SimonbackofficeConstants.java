/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.backoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SimonbackofficeConstants extends GeneratedSimonbackofficeConstants
{
	public static final String EXTENSIONNAME = "simonbackoffice";

	private SimonbackofficeConstants()
	{
		super();
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
