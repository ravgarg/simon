package com.simon.core.search.converters.populator;

import de.hybris.platform.commercefacades.search.converters.populator.FacetPopulator;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;



/**
 * populate DisplayFacetType attribute
 */
public class ExtFacetPopulator extends FacetPopulator
{
	/**
	 * populate DisplayFacetType attribute
	 */
	@Override
	public void populate(final FacetData source, final FacetData target)
	{
		super.populate(source, target);
		target.setDisplayFacetType(source.getDisplayFacetType());
	}
}
