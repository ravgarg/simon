package com.simon.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simon.core.dao.DesignerDao;
import com.simon.core.model.DesignerModel;


/**
 * The Class DefaultDesignerDao. DB based default implementation of {@link DesignerDao}
 */
public class DefaultDesignerDao extends DefaultGenericDao<DesignerModel> implements DesignerDao
{
	private static final String FIND_ALL_DESIGNERS_FOR_CODES = "SELECT {" + DesignerModel.PK + "} FROM {" + DesignerModel._TYPECODE
			+ "} WHERE {" + DesignerModel.CODE + "} IN (?codes)";

	/**
	 * Instantiates a new default designer dao.
	 */
	public DefaultDesignerDao()
	{
		super("Designer");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DesignerModel findDesignerByCode(final String code)
	{
		final List<DesignerModel> resList = find(Collections.singletonMap("code", code));
		return resList.isEmpty() ? null : resList.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DesignerModel> findListOfDesignerForCodes(final List<String> codes)
	{
		validateParameterNotNull(codes, "codes must not be null");
		final Map<String, List<String>> queryParams = new HashMap<>();
		queryParams.put("codes", codes);
		final SearchResult<DesignerModel> result = getFlexibleSearchService().search(FIND_ALL_DESIGNERS_FOR_CODES, queryParams);
		return result.getResult();

	}
}
