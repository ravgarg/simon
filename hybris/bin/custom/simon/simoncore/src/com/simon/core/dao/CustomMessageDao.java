package com.simon.core.dao;


import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.Collection;

import com.simon.core.exceptions.SystemException;


/**
 * This interface fetches messagetext corresponding to message code and catalog version provided.
 */
public interface CustomMessageDao extends Dao
{
	/**
	 * This method returns messageText for given code and given catalogVersion.
	 *
	 */
	String getMessageTextForCode(final String messageCode, Collection<CatalogVersionModel> catalogVersions) throws SystemException;

}
