package com.simon.core.product.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductIdentificationStrategy;


/**
 * This Class is use to identifying the Product based on unqiue attribute in Feed in The Simon Context that field is SKU
 */
public class SimonProductIdentificationStrategy extends DefaultProductIdentificationStrategy
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void identifyProduct(final ProductImportData data) throws ProductImportException
	{
		final ProductModel productResolvedByShopSku = data.getProductResolvedByShopSku();
		if (productResolvedByShopSku != null)
		{
			data.setIdentifiedProduct(productResolvedByShopSku);
		}
	}

}
