package com.simon.core.services.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.dao.ErrorLogDao;
import com.simon.core.services.ErrorLogService;


/**
 * This service is use to fetch data from ErrorLog
 *
 */
public class ErrorLogServiceImpl implements ErrorLogService
{
	@Resource
	private ErrorLogDao errorLogDao;

	/**
	 * This method use to fetch list of product codes based on last end time of cronjob
	 */
	@Override
	public List<String> getProductCodes(final Date lastEndTime)
	{
		return errorLogDao.getProductCodes(lastEndTime);

	}
}
