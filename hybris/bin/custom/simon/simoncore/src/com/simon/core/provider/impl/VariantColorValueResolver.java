package com.simon.core.provider.impl;


import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;


/**
 * This class helps in indexing details or VariantColor into InputDocument
 */
public class VariantColorValueResolver extends VariantCategoryValueResolver
{
	/**
	 * This method fetches VariantColor from ResolverContext, if not null, gets the required value and adds to
	 * InputDocument
	 *
	 * @throws FieldValueProviderException,
	 *            Exception is thrown if there is any problem while adding data to index or expected value is not
	 *            fetched, given the attribute has optional set to false.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Collection<CategoryModel>, Object> resolverContext) throws FieldValueProviderException
	{
		boolean hasVariantValueCategory = false;
		String attributeValueInUpperCase = null;
		final Collection<CategoryModel> variantValueCategories = resolverContext.getData();
		if (CollectionUtils.isNotEmpty(variantValueCategories))
		{
			final String attributeName = getAttributeName(indexedProperty);
			final String propertyName = getPropertyName(indexedProperty);
			final Object attributeValue = getAttributeValue(variantValueCategories, attributeName, propertyName);
			if (null != attributeValue)
			{
				attributeValueInUpperCase = ((String) attributeValue).toUpperCase();
			}
			hasVariantValueCategory = filterAndAddFieldValues(document, batchContext, indexedProperty, attributeValueInUpperCase,
					resolverContext.getFieldQualifier());

		}
		if (!hasVariantValueCategory)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}
}
