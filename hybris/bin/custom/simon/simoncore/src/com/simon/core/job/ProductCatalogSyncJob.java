package com.simon.core.job;

import de.hybris.platform.commerceservices.setup.SetupSyncJobService;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import com.simon.core.constants.SimonCoreConstants.Catalog;


/**
 * A job, responsible to Synchronize ProductCatalog
 *
 */
public class ProductCatalogSyncJob extends AbstractJobPerformable<CronJobModel>
{
	@Resource
	private SetupSyncJobService setupSyncJobService;

	/**
	 * method responsible for Synchronizing ProductCatalog
	 *
	 * @params: CronJobModel
	 *
	 * @return PerformResult
	 */
	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		/**
		 * Implemented based on AbstractDataImportService > synchronizeProductCatalog
		 */
		setupSyncJobService.createProductCatalogSyncJob(Catalog.PRODUCT_CATALOG_CODE);
		return setupSyncJobService.executeCatalogSyncJob(Catalog.PRODUCT_CATALOG_CODE);
	}
}
