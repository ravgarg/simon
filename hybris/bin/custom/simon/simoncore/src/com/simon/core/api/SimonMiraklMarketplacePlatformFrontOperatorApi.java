package com.simon.core.api;

import com.mirakl.client.mmp.core.MiraklMarketplacePlatformFrontOperatorApi;
import com.mirakl.client.mmp.front.request.order.accept.MiraklAcceptOrderRequest;
import com.simon.core.api.mirakl.dto.SimonMiraklShipOrderRequest;
import com.simon.core.api.mirakl.dto.SimonMiraklTrackingOrderRequest;

/**
 * This is the interface used to make
 * front/Operator API calls of Mirakl.
 */
public interface SimonMiraklMarketplacePlatformFrontOperatorApi extends MiraklMarketplacePlatformFrontOperatorApi{
	
	/**
	 * Method is used to call Mirakl API OR21, it will update the Order
	 * <code>acceptance/Rejection</code> status depending on Retailer action.
	 * 
	 * @param request
	 *            object {@link MiraklAcceptOrderRequest} used for Mirakl Order
	 *            Acceptance/Rejection
	 * @return void
	 */
	 void acceptOrRejectOrder(MiraklAcceptOrderRequest request);
	
	 /**
	 * Method is used to call OR24 API, to update the
	 * <code>shipping state</code> within Mirakl.
	 * 
	 * @param request
	 *            object {@link SimonMiraklShipOrderRequest} used for Mirakl
	 *            shipping order status update.
	 * @return void
	 */
	 void shipOrder(SimonMiraklShipOrderRequest request);
	
	 /**
	 * Method is used to call OR23 API, to update the
	 * <code>carrier tracking details</code> within Mirakl.
	 * 
	 * @param request
	 *            object (@link SimonMiraklTrackingOrderRequest} used for Mirakl Order Tracking.
	 * @return void
	 */
	 void trackOrder(SimonMiraklTrackingOrderRequest request);

}
