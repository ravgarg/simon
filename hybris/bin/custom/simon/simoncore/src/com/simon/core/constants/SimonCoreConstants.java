package com.simon.core.constants;

@SuppressWarnings(
{ "javadoc", "deprecation", "squid:UnusedPrivateMethod" })
public class SimonCoreConstants extends GeneratedSimonCoreConstants
{
	public static final String EXTENSIONNAME = "simoncore";
	public static final String CART = "cart";
	public static final String SHOP = "Shop";

	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";

	public static final int TWOTAP_MAX_STATUS_CHECK = 40;
	public static final String TWOTAP_DEFAULT_CALLBACK_HOST = "twotap.default.callback.host";
	public static final String TWOTAP_ORDER_ID_PARM = "orderId";
	public static final String TWOTAP_PURCHASE_CALLBACK_URL_UPDATED = "/simonwebservices/twotap/updated";
	public static final String TWOTAP_PURCHASE_CALLBACK_URL_CONFIRMED = "/simonwebservices/twotap/confirmed";
	public static final String TWOTAP_CREATE_CART_URL = "https://api.twotap.com/v1.0/cart";
	public static final String TWOTAP_CART_STATUS_URL = "https://api.twotap.com/v1.0/cart/status";
	public static final String TWOTAP_CART_ESTIMATE_URL = "https://api.twotap.com/v1.0/cart/estimates";
	public static final String TWOTAP_CART_PURCHASE_URL = "https://api.twotap.com/v1.0/purchase";
	public static final String TWOTAP_CART_PURCHASE_CONFIRM_URL = "https://api.twotap.com/v1.0/purchase/confirm";
	public static final String TWOTAP_PRIVATE_TOKEN = "8c79e983779570381d770cb314a4afc0c76e2a1c473d884432";
	public static final String TWOTAP_PUBLIC_TOKEN = "a3015d0e9a880bdd368d12d41fa099";

	public static final String TWOTAP_STATUS_STILL_PROCESSING = "still_processing";
	public static final String TWOTAP_STATUS_DONE = "done";

	public static final String TWOTAP_PURCHASE_URL = "https://api.twotap.com/v1.0/purchase";

	public static final String SPREEDLY_RECEIVER_TOKEN = "WBjd4m4cr9m4ZP6JQWkt8tFK9S2";
	public static final String SPREEDLY_PAYMENT_METHOD = "https://core.spreedly.com/v1/payment_methods.json";
	public static final String SPREEDLY_RESTFUL_WS_URL = "https://core.spreedly.com";
	public static final String SPREEDLY_RESTFUL_WS_USERNAME = "BB7A7NdGv1cwqNeCj30lphwP47L";
	public static final String SPREEDLY_RESTFUL_WS_PASS = "uq4WW689cHBY85fyWTLZv4j69WyzgwX40Ob2JHrMAiQQDXeeAuJA0N7LfMF3o0xr";
	public static final String SPREEDLY_PROVISION_RECEIVER = "https://core.spreedly.com/v1/receivers.json";


	public static final double ZERO_DOUBLE = 0.0;
	public static final int ZERO_INT = 0;
	public static final int HUNDRED_INT = 100;
	public static final int TWO_INT = 2;
	public static final int FIRST_INT = 1;
	public static final int THREE_INT = 3;
	public static final int FOUR_INT = 4;
	public static final int FIVE_INT = 5;
	public static final int SIX_INT = 6;
	public static final int SEVEN_INT = 7;
	public static final int EIGHT_INT = 8;
	public static final int NINE_INT = 9;
	public static final int TEN_INT = 10;
	public static final int ELEVEN_INT = 11;
	public static final int TWELVE_INT = 12;
	public static final int THIRTEEN_INT = 13;
	public static final int FOURTEEN_INT = 14;
	public static final int FIFTEEN_INT = 15;
	public static final int NINETEEN_INT = 19;
	public static final int TWENTY_TWO = 22;
	public static final char COLON = ':';
	public static final String PLUS_SIGN = "+";
	public static final String DOUBLE_ZERO = "00";


	public static final String CATEGORY_CODE_PATH = "categoryCodePath";
	public static final String CATEGORY_CODE_URL_PARAM = "/catcodes/";
	public static final String REDIRECT = "redirect:";
	public static final String LOGIN_ERROR_MESSAGE_CODE = "loginErrorMessageCode";
	public static final String LOGIN_PROFILE_COMPLETED_MESSAGE_CODE = "loginProfileCompletedMessageCode";
	public static final String LOGIN_SUCCESS_MESSAGE_CODE = "loginSuccessMessageCode";
	public static final String REDIRECT_LOGIN_TRACK_ORDER = "redirectLoginTrackOrder";
	public static final String LOGIN_ERROR_ENABLE_CAPTCHA = "loginErrorEnableCaptcha";
	public static final String RELEVANCE = ":relevance";
	public static final String OPTED_IN_EMAIL = "optedInEmail";
	public static final String VARIANT_CATEGORY_RESOLVER = "variantCategoryValueResolver";
	public static final String PRODUCT_TYPE = "simonProductType";
	public static final String ATTRIBUTE = "attribute";
	public static final String VALUE = "value";
	public static final String NAME = "Name";
	public static final String CODE = "code";
	public static final String ID = "Id";
	public static final String UNAPPROVED = "Unapproved";
	public static final String UNAPPROVED_PRODUCTIDS = "unapprovedProductIds";
	public static final String OOS_PRODUCTIDS = "oosProductIds";
	public static final String ERROR_CODE3 = "ERROR_CODE_3";
	public static final String QUANTITY = "quantity";



	public class Catalog
	{
		public static final String PRODUCT_CATALOG_CODE = "simonProductCatalog";
		public static final String PRODUCT_IMAGE_CATALOG_CODE = "simonImageCatalog";
		public static final String STAGED = "Staged";
		public static final String ONLINE = "Online";
		public static final String DEFAULT = "Default";

		private Catalog() //NOSONAR
		{
			//empty
		}

	}

	public static final String PERCENT_SAVING_MESSAGE = "percent.saving.message";

	public static final String PERCENT_SAVING_PRE_MESSAGE = "percent.saving.pre.message";

	public static final String MSRP_STRING = "MSRP Price: ";

	public static final String LIST_STRING = "List Price: ";

	public static final String SALE_STRING = "Sale Price: ";

	public static final String PRODUCT_COLOR_TITLE = "color.label";

	public static final String PRODUCT_LIST_PRICE = "LIST";

	public static final String PRODUCT_SALE_PRICE = "SALE";

	public static final String PRODUCT_MSRP = "MSRP";

	public static final String OPTIONAL_PARAM = "optional";

	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
	/** The Constant for PIPE delimiter. */
	public static final String PIPE = "|";
	/** Media format for default image */
	public static final String MEDIA_FORMAT_DEFAULT = "580Wx884H*2";
	/** Media format for mobile image */
	public static final String MEDIA_FORMAT_MOBILE = "325Wx495H";
	/** Media format for thumbnail image */
	public static final String MEDIA_FORMAT_THUMBNAIL = "150Wx229H";
	/** Media format for zoom image */
	public static final String MEDIA_FORMAT_ZOOM = "580Wx884H*2";
	/** Media format for cart/checkout thumbnail image */
	public static final String MEDIA_FORMAT_CART = "150Wx229H";
	public static final String DESKTOP_PRODUCT_IMAGE_FORMAT = "325Wx495H";
	public static final String MOBILE_PRODUCT_IMAGE_FORMAT = "150Wx229H";

	/** constant for underscore(_) */
	public static final String UNDERSCORE = "_";

	public static final String LOWSTOCK = "basket.page.message.update.reducedNumberOfItemsAdded.lowStock";
	public static final String NOSTOCK = "basket.page.message.update.reducedNumberOfItemsAdded.noStock";
	public static final String UNAVAILABLE = "basket.page.message.retailer.now.inactive";
	public static final String UPDATE_SUCCESSFULLY = "basket.page.message.update";
	public static final String REMOVE_SUCCESSFULLY = "basket.page.message.remove";




	public static final String DESKTOP_PRODUCT_IMAGE_TYPE = "Desktop";
	public static final String MOBILE_PRODUCT_IMAGE_TYPE = "Mobile";
	public static final String INVENTORY_STOCK_LEVEL_MESSAGE = "basket.page.insufficient.inventory.stock.level.message";

	public static final String INTEGRATION_ERROR_CODE_MESSAGE = ".message";
	public static final String INTEGRATION_ERROR_CODE_400 = "400";
	public static final String INTEGRATION_USER_UPDATE_PROFILE_ERROR_CODE = "integration.user.update.profile.errorcode.";
	public static final String INTEGRATION_USER_REGISTER_ERROR_CODE = "integration.user.register.errorcode.";
	public static final String INTEGRATION_USER_AUTH_LOGIN_ERROR_CODE = "integration.user.auth.login.errorcode.";
	public static final String INTEGRATION_USER_AUTH_TOKEN_ERROR_CODE = "integration.user.auth.token.errorcode.";
	public static final String INTEGRATION_USER_AUTH_TOKEN_CLIENT_CREDENTIALS_ERROR_CODE = "integration.user.auth.token.client.credentials.errorcode.";
	public static final String INTEGRATION_USER_LOGIN_ERROR_CODE = "integration.user.login.errorcode.";
	public static final String INTEGRATION_USER_ADD_ADDRESS_ERROR_CODE = "integration.user.add.address.errorcode.";
	public static final String INTEGRATION_USER_REMOVE_ADDRESS_ERROR_CODE = "integration.user.remove.address.errorcode.";
	public static final String INTEGRATION_USER_EDIT_ADDRESS_ERROR_CODE = "integration.user.edit.address.errorcode.";
	public static final String INTEGRATION_USER_ADDRESS_PAYMENT_ERROR_CODE = "integration.user.address.payment.errorcode.";
	public static final String INTEGRATION_USER_CENTER_ERROR_CODE = "integration.user.my.center.errorcode.";


	/** constant for DELIMITER(,) */
	public static final String CATEGORY_LABEL = "category.label";



	/** constant for DELIMITER(,) */
	public static final String DELIMITER = ",";
	/** constant for DOT(.) */
	public static final String DOT = ".";

	/** constant for CSV(csv) */
	public static final String CSV = "csv";
	/** constant for UTF8(UTF8) */
	public static final String UTF8 = "UTF-8";

	public static final String HYPHEN = "-";

	/** constant for forward slash */
	public static final String FORWARD_SLASH = "/";

	public static final String PLP_PAGE_ID = "plp";
	public static final String PLP_LEFT_NAVIGATION_PAGE_NAME = "leftnavigationplp";
	public static final String CLP_PAGE_ID = "clp";
	public static final String LISTING_PAGE_ID = "ListingPage";



	public static final String SPACE = " ";
	public static final String COLOR = "color";
	public static final String SIZE = "size";
	public static final String PIPE_DOUBLE = "||";

	public static final String COLOR_SWATCH_URL = "colorSwatchUrl";
	public static final String THUMBNAIL = "thumbnail";

	public static final String PERCENTAGE_SAVING = "PERCENT_SAVING";
	public static final Object DEFAULT_IMAGE_KEY = "default";
	public static final Object NORMALIZED_COLOR_CODE = "normalizedColorCode";
	public static final Object NORMALIZED_COLOR_URL = "normalizedColorURL";
	public static final String AGE_GROUP = "ageGroup";
	public static final String GENDER = "gender";
	public static final String VARIANT_VALUE_CATEGORY_CODE_PREFIX = "VVC";
	public static final String VARIANT_CATEGORY_CODE_PREFIX = "VC";
	public static final String PRODUCT_IMPORT_ACTIVE = "product.import.active";
	public static final String ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE = "order.acceptance.based.on.dollar";
	public static final String VALUE_FOR_ORDER_ACCEPTANCE = "dollar.value.for.order.acceptance";
	public static final String FAV_ADDED_MESSAGE = "text.pdp.addedtofavorite";
	public static final String HOME_BREAD_CRUMB = "text.account.breadcrumb.home";
	public static final String SOMETHING_WENT_WRONG_MESSAGE = "Something went Wrong... Please try after some time...";
	public static final String VALUE_FOR_ORDER_EXPORT = "order.spo.export.deposit";
	public static final String MOVE_ORDER_EXPORT_FROM_NFS_TO_SFTP_PATH = "order.export.from.nfs.to.sftp.path";
	public static final String ORDER_EXPORT_CSV_FILE_HEADER_FOR_RETAILER = "order.export.csv.file.header.for.retailer";
	public static final String ORDER_EXPORT_CSV_FILE_HEADER_FOR_BOT = "order.export.csv.file.header.for.bot";
	public static final String SPO_ORDER_FILE_NAME_FOR_RETAILER = "SPO_orders";
	public static final String SPO_ORDER_FILE_NAME_FOR_BOT = "BOT_orders";
	public static final String TIMESTAMP_FOR_RETAILER = "yyyyMMdd";
	public static final String TIMESTAMP_FOR_BOT = "yyyyMMdd_hhmmss";
	public static final String ORDER_EXPORT_SFTP_HOST = "orderexport.sftp.host";
	public static final String ORDER_EXPORT_SFTP_PRIVATE_KEY = "orderexport.sftp.private.key";
	public static final String ORDER_EXPORT_SFTP_USER = "orderexport.sftp.user";
	public static final String DOMAIN_URL = "domain";
	public static final String PATH = "path";
	public static final String BUY_FROM_TEXT = "buy.from";
	public static final String REDIRECT_TEXT_PRE = "redirect.text.pre";
	public static final String REDIRECT_TEXT_POST = "redirect.text.post";
	public static final String REDIRECT_CLICK_HERE = "click.here";
	public static final String CURRENTURL = "currentURL";
	public static final String CANONICAL_URL = "canonicalUrl";
	public static final String NOT_APPLICABLE = "NA";
	public static final String PRODUCT_CODE_LIST = "productCodes";
	public static final String DEFAULT = "default";
	public static final String MEDIA_CODE_SWATCH_SUFFIX = "SwatchImage";
	public static final String SCRIPT_URL = "scriptUrl";

	/**
	 * Inner Constant class for SOLR Indexed Properties name
	 */
	public class IndexedKeys
	{
		public static final String BASE_PRODUCT_CODE = "baseProductCode";
		public static final String BASE_PRODUCT_NAME = "baseProductName";
		public static final String INSTOCKFLAG = "inStockFlag";
		public static final String BASE_PRODUCT_DESCRIPTION = "baseProductDescription";
		public static final String VARIANT_PRODUCT_NAME = "name";
		public static final String VARIANT_PRODUCT_CODE = "code";
		public static final String VARIANT_PRODUCT_DESCRIPTION = "description";
		public static final String BASE_PRODUCT_DESIGNER_NAME = "baseProductDesignerName";
		public static final String BASE_PRODUCT_RETAILER_NAME = "baseProductRetailerName";
		public static final String BASE_PRODUCT_RETAILER_ID = "baseProductRetailerId";
		public static final String BASE_PRODUCT_RETAILER_LOGO = "baseProductRetailerLogo";
		public static final String VARIANT_PRODUCT_COLOR_ID = "colorId";
		public static final String VARIANT_PRODUCT_COLOR_NAME = "colorName";
		public static final String COLOR_SWATCH_IMAGE_URL = "colorSwatchImage";
		public static final String NORMALIZED_COLOR_CODE = "normalizedColorCode";


		public static final String VARIANT_PRODUCT_IMAGES = "images";
		public static final String VARIANT_CATEGORY_PATH = "variantCategoryPath";
		public static final String PRODUCT_PLUM_PRICE = "plumPrice";
		public static final String VARIANT_PRODUCT_PRICE = "variantPrice";
		public static final String VARIANT_CATEGORY_DETAIL = "variantCategoryDetail";
		public static final String CATEGORY_PATH = "categoryPath";
		public static final String PROMOTION_CODE = "promotionCode";
		public static final String PROMOTION_END_DATE = "promotionEndDate";
		public static final String PROMOTION_START_DATE = "promotionStartDate";
		public static final String PROMOTION_TYPE = "promotionType";
		public static final String PROMOTION_DESCRIPTION = "promotionDescription";
		public static final String PROMOTION_PRIORITY = "promotionPriority";
		public static final String BASE_PRODUCT_RETAILER_PUNCH_OUT = "retailerPunchOutFlag";
		public static final String VARIANT_PRODUCT_PUNCH_OUT_URL = "rawProductUrl";
		public static final String BASE_PRODUCT_DESIGNER_URL = "baseProductDesignerUrl";
		public static final String BASE_PRODUCT_RETAILER_URL = "baseProductRetailerUrl";

		private IndexedKeys() //NOSONAR
		{
			//empty
		}

	}

	/**
	 * Constant class for Shop attributes
	 */
	public class ShopCustomFields
	{
		public static final String PAYMENTMETHODS = "paymentmethods";
		public static final String PREFIX = "prefix";
		public static final String ORDER_EXPORT = "orderexport";

		private ShopCustomFields() //NOSONAR
		{
			//empty
		}

	}

	/**
	 * Constant class for ConsignmentEntry additional fields attributes
	 */
	public class ConsignmentEntryCustomFields
	{

		public static final String ORDER_LINE_STATUS = "order-line-status";
		public static final String SHIP_DATE = "ship-date";
		public static final String CARRIER_CODE = "carrier-code";
		public static final String CARRIER_NAME = "carrier-name";
		public static final String CARRIER_URL = "carrier-url";
		public static final String TRACKING_NUMBER = "tracking-number";
		public static final String CANCELLATION_REASON = "cancellation-reason";
		public static final String CANCELLATION_DATE = "cancellation-date";
		public static final String REFUND_REASON = "refund-reason";
		public static final String REFUND_DATE = "refund-date";
		public static final String PRODUCT_AMOUNT = "product-amount";
		public static final String SHIPPING_AMOUNT = "shipping-amount";
		public static final String TAX_AMOUNT = "tax-amount";

		private ConsignmentEntryCustomFields() //NOSONAR
		{
			//empty
		}

	}
}
