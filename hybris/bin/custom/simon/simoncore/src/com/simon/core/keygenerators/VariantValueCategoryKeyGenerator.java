package com.simon.core.keygenerators;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import com.simon.core.constants.SimonCoreConstants;


/**
 * VariantValueCategoryKeyGenerator generates Variant Value Category Code for given raw name
 */
public class VariantValueCategoryKeyGenerator implements KeyGenerator
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object generate()
	{
		throw new UnsupportedOperationException("Not supported, please call generateFor().");
	}

	/**
	 * Converts Variant Value Category Code to lower case and replaces all nbsp by hyphen.
	 *
	 * @param arg0
	 *           the arg 0
	 * @return the object
	 */
	@Override
	public Object generateFor(final Object arg0)
	{
		return (SimonCoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + "-"
				+ ((String) arg0).toLowerCase().replaceAll("\\s+", "-").replaceAll("/+", "-")).replaceAll("-+", "-");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		throw new UnsupportedOperationException("A reset of VariantValueCategoryKeyGenerator is not supported.");
	}

}
