package com.simon.core.dao;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * This DAO interface is used to retrieve order model
 */
public interface ExtOrderDao
{
	/**
	 * @description Method us to get the Order Model from the database based on the Order Id
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	OrderModel getOrderByOrderId(final String orderId);

	/**
	 * @description Method us to get the Versioned Order Model from the database based on the Order Id
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	OrderModel getVersionedOrderByOrderId(final String orderId);

	/**
	 * Returns orders placed in a day
	 *
	 * @return {@link List} of {@link OrderModel} - matched orders
	 */
	List<OrderModel> findOrdersToExport(int day, String startHours, String endHours);
}
