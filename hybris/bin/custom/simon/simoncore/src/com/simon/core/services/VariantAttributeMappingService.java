package com.simon.core.services;

import java.util.List;
import java.util.Map;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ControlType;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.model.VariantAttributeMappingModel;


/**
 * This interface fetches Variant Attribute Mapping corresponding to a ShopGateway/Target system and a shop.
 */
public interface VariantAttributeMappingService
{

	/**
	 * Fetches list of VariantAttributeMappingModel corresponding to a Shop and target system.
	 *
	 * @param shop
	 *           Shop Model
	 * @param shopGateway
	 *           Shop Gateway Model
	 * @return List of VariantAttributeMappingModel
	 */
	List<VariantAttributeMappingModel> getVariantAttributeMappingForShop(ShopModel shop, ShopGatewayModel shopGateway);

	/**
	 * Get list of VariantAttributeMappingModel corresponding to Shop gateway (i.e. target system)
	 *
	 * @param shopGateway
	 *           Shop Gateway Model {@link ShopGatewayModel}
	 * @return List of {@link VariantAttributeMappingModel}
	 *
	 * @throws IllegalArgumentException
	 *            if ShopGatewayModel is null
	 */
	List<VariantAttributeMappingModel> getVariantAttributeMapping(ShopGatewayModel shopGateway);

	/**
	 * Gets the variant attribute label map.
	 *
	 * @param shop
	 *           the shop
	 * @param shopGateway
	 *           the shop gateway
	 * @return the variant attribute label map
	 */
	Map<String, String> getVariantAttributeLabelMap(ShopModel shop, ShopGatewayModel shopGateway);

	/**
	 * Gets the variant attribute control type map.
	 *
	 * @param shop
	 *           the shop
	 * @param shopGateway
	 *           the shop gateway
	 * @return the variant attribute control type map
	 */
	Map<String, ControlType> getVariantAttributeControlTypeMap(ShopModel shop, ShopGatewayModel shopGateway);

	/**
	 * Gets the variant attribute label map.
	 *
	 * @param shop
	 *           the shop
	 * @param shopGateway
	 *           the shop gateway
	 * @return the variant attribute label map
	 */
	Map<String, String> getVarinatAttributeTwoTapLabelMap(ShopModel shop, ShopGatewayModel shopGateway);
}
