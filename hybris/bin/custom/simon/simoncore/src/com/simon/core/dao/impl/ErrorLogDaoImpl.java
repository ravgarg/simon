package com.simon.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ErrorLogDao;


/**
 * Use to fetch information from ErrorLog
 */
public class ErrorLogDaoImpl extends AbstractItemDao implements ErrorLogDao
{
	private static final String ERRORMESSAGE = "errorMessage";
	private static final String JOBLASTMODIFIEDTIME = "jobLastModifiedTime";

	/**
	 * Returns product codes
	 *
	 * @return {@link List} of {@link String} - matched products
	 */
	@Override
	public List<String> getProductCodes(final Date lastEndTime)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_PRODUCT_CODES_ERRORLOG);
		fQuery.addQueryParameter(ERRORMESSAGE, SimonCoreConstants.UNAPPROVED);
		fQuery.addQueryParameter(JOBLASTMODIFIEDTIME, lastEndTime);
		fQuery.setResultClassList(Arrays.asList(String.class));
		return getFlexibleSearchService().<String> search(fQuery).getResult();
	}

}
