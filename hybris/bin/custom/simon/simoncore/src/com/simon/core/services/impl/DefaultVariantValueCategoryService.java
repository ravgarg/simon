package com.simon.core.services.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import javax.annotation.Resource;

import com.simon.core.dao.VariantValueCategoryDao;
import com.simon.core.services.VariantValueCategoryService;


/**
 * DefaultVariantValueCategoryService, implements {@link VariantValueCategoryService} provides additional
 * implementations of operations to search {@code ProductModel}.
 */
public class DefaultVariantValueCategoryService implements VariantValueCategoryService
{
	@Resource
	private VariantValueCategoryDao variantValueCategoryDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantValueCategoryModel getTopSequenceVariantValueCategory(final VariantCategoryModel variantCategory)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("Variant Category", variantCategory);
		return variantValueCategoryDao.findTopSequenceVariantValueCategory(variantCategory);
	}

}
