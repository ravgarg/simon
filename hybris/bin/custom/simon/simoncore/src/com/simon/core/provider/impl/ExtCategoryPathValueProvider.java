package com.simon.core.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryPathValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;


/**
 * ExtCategoryPathValueProvider is used to return field values for the category paths for the variant categories which
 * are of given indexedProperty that are fetched from the base product based on the indexConfig.If no such variant
 * category is there then it returns an empty list.
 */
public class ExtCategoryPathValueProvider extends CategoryPathValueProvider
{

	/**
	 * This method is used to return field values for the category paths for the variant categories which are of given
	 * indexedProperty that are fetched from the base product based on the indexConfig.If no such variant category is
	 * there then it returns an empty list.
	 *
	 * @see de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryPathValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig,
	 *      de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		ProductModel baseProduct;
		if (model instanceof GenericVariantProductModel)
		{
			baseProduct = ((GenericVariantProductModel) model).getBaseProduct();
		}
		else
		{
			baseProduct = (ProductModel) model;
		}
		final Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig,
				indexedProperty, baseProduct);
		final List<CategoryModel> variantcategories = categories.stream()
				.filter(categoryModel -> categoryModel instanceof VariantCategoryModel).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(variantcategories))
		{
			final Collection<FieldValue> fieldValues = new ArrayList<>();

			final Set<String> categoryPaths = getCategoryPaths(variantcategories);
			fieldValues.addAll(createFieldValue(categoryPaths, indexedProperty));

			return fieldValues;
		}
		else
		{
			return Collections.emptyList();
		}
	}

}
