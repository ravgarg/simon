/**
 *
 */
package com.simon.core.exceptions;


/**
 * This <tt>class</tt> will be used for application specific {@link BusinessException}. This class represents the type
 * of Exception which are expected to occur and can be handled.
 *
 */
public class BusinessException extends Exception
{
	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99001L;

	/** Exception code. */
	private final String code;

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message
	 *           the message
	 */
	public BusinessException(final String message)
	{
		super(message);
		this.code = "";
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public BusinessException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public BusinessException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}

	/**
	 * Instantiates a new business exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public BusinessException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}

}
