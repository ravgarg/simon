package com.simon.core.mirakl.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.europe1.model.PriceRowModel;

import com.mirakl.client.mmp.domain.common.MiraklDiscount;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.simon.core.enums.PriceType;


/**
 * SellingPriceRowPopulator populates {@link PriceRowModel} used as selling price from {@link MiraklExportOffer}
 */
public class SellingPriceRowPopulator implements Populator<MiraklExportOffer, PriceRowModel>
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final MiraklExportOffer miraklExportOffer, final PriceRowModel target)
	{
		final MiraklDiscount discount = miraklExportOffer.getDiscount();
		target.setPrice(discount.getDiscountPrice().doubleValue());
		target.setStartTime(discount.getStartDate());
		target.setEndTime(discount.getEndDate());
		target.setPriceType(PriceType.SALE);
		target.setOfferId(miraklExportOffer.getId());
		target.setOfferSku(miraklExportOffer.getProductSku());
		target.setOfferStateCode(miraklExportOffer.getStateCode());
	}
}
