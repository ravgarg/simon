package com.simon.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.simon.core.dao.MallDao;
import com.simon.core.model.MallModel;


/**
 * The Class MallDaoImpl. DB based default implementation of {@link MallDao}
 */
public class MallDaoImpl extends DefaultGenericDao<MallModel> implements MallDao
{

	private static final String FIND_ALL_MALLS_FOR_CODES = "SELECT {" + MallModel.PK + "} FROM {" + MallModel._TYPECODE
			+ "} WHERE {" + MallModel.CODE + "} IN (?codes)";

	/**
	 * Instantiates a new default Mall dao.
	 */
	public MallDaoImpl()
	{
		super("Mall");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MallModel findMallByCode(final String code)
	{
		final List<MallModel> resList = this.find(Collections.singletonMap("code", code));
		return resList.isEmpty() ? null : resList.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<MallModel> findAllMalls()
	{
		return this.find();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<MallModel> findMallListForCode(final List<String> mallCodes)
	{
		validateParameterNotNull(mallCodes, "mallCodes must not be null");
		final Map<String, List<String>> queryParams = new HashMap<>();
		queryParams.put("codes", mallCodes);
		final SearchResult<MallModel> result = getFlexibleSearchService().search(FIND_ALL_MALLS_FOR_CODES, queryParams);
		return new HashSet<>(result.getResult());
	}
}
