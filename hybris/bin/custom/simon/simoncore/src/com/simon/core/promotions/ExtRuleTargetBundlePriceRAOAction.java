package com.simon.core.promotions;

import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.EntriesSelectionStrategyRPD;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.impl.RuleTargetBundlePriceRAOAction;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ExtRuleTargetBundlePriceRAOAction extends RuleTargetBundlePriceRAOAction
{

	@Override
	protected void performAction(final RuleActionContext context,
			final List<EntriesSelectionStrategyRPD> entriesSelectionStrategyRPDs, final BigDecimal amount)
	{
		this.validateSelectionStrategy(entriesSelectionStrategyRPDs, context);
		if (this.hasEnoughQuantity(entriesSelectionStrategyRPDs))
		{
			final int count = this.adjustStrategyQuantity(entriesSelectionStrategyRPDs, -1);
			final BigDecimal fixedPrice = amount.multiply(BigDecimal.valueOf(count));
			DiscountRAO discount = null;
			final Map selectedOrderEntryMap = this.getSelectedOrderEntryQuantities(entriesSelectionStrategyRPDs);
			final Set selectedOrderEntryRaos = this.getSelectedOrderEntryRaos(entriesSelectionStrategyRPDs, selectedOrderEntryMap);
			final BigDecimal currentPriceOfToBeDiscountedOrderEntries = this.getRuleEngineCalculationService()
					.getCurrentPrice(selectedOrderEntryRaos, selectedOrderEntryMap);
			if (currentPriceOfToBeDiscountedOrderEntries.compareTo(fixedPrice) > 0)
			{
				final RuleEngineResultRAO result = context.getRuleEngineResultRao();
				final CartRAO cartRAO = context.getCartRao();
				final BigDecimal discountAmount = this.getOrderUtils()
						.applyRounding(currentPriceOfToBeDiscountedOrderEntries.subtract(fixedPrice), cartRAO.getCurrencyIsoCode());
				discount = this.getRuleEngineCalculationService().addOrderLevelDiscount(cartRAO, true, discountAmount);
				this.setRAOMetaData(context, discount);
				this.consumeOrderEntries(selectedOrderEntryRaos, selectedOrderEntryMap, discount);
				result.getActions().add(discount);
				context.insertFacts(discount, discount.getConsumedEntries());
				final Iterator selectedOrderEntryRaosIterator = selectedOrderEntryRaos.iterator();

				while (selectedOrderEntryRaosIterator.hasNext())
				{
					final OrderEntryRAO selectedOrderEntry = (OrderEntryRAO) selectedOrderEntryRaosIterator.next();
					context.updateFacts(selectedOrderEntry);
				}

				context.updateFacts(cartRAO, result);
			}

			this.trackRuleGroupExecutions(context);
		}

		this.trackRuleExecution(context);
	}

}
