package com.simon.core.services;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.variants.model.VariantCategoryModel;


/**
 * VariantCategoryService service contains abstract methods for operations on {@link VariantCategoryModel}
 */
public interface VariantCategoryService extends CategoryService
{

	/**
	 * Gets the leaf variant category for catalog version. Returns leaf <code>VariantCategoryModel</code> for given
	 * <code>catalogVersion</code>.
	 *
	 * @param catalogVersion
	 *           the catalog version for which leaf variant category will be returned
	 * @return the variant category model
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is null
	 */
	VariantCategoryModel getLeafVariantCategory(CatalogVersionModel catalogVersion);
}
