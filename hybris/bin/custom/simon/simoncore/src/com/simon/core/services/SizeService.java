package com.simon.core.services;

import java.util.List;

import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.model.SizeModel;


/**
 * SzieService service for {@link SizeModel}. Contains abstract methods for operations on {@link SizeModel}
 */
public interface SizeService
{

	/**
	 * Gets the List of Sizes.
	 *
	 * @param sizeCodes
	 *
	 * @return the {@link List<SizeModel>}
	 */
	List<SizeModel> getAllSizesByCode(List<String> sizeCodes);

	/**
	 * Gets the Size for code. Returns matching Size for <code>code</code>
	 *
	 * @param sizeCode
	 *           the Size <code>code</code>
	 * @return the {@link SizeModel} for Size <code>code</code>
	 */
	SizeModel getSizeForId(String sizeCode);

	/**
	 * Gets the List of Sizes.
	 *
	 * @return the {@link List<SizeModel>}
	 */
	List<SizeModel> getAllSizes();


	/**
	 * Gets the Sequence number for given Sizes.
	 *
	 * @return the ProductSizeSequenceModel
	 */
	ProductSizeSequenceModel getSequenceForSizes(String sizeCode);

	/**
	 * Gets the Sequence numbers for all Sizes.
	 *
	 * @return the List<ProductSizeSequenceModel>
	 */
	List<ProductSizeSequenceModel> getAllSequencesForSizes();

}
