package com.simon.core.services.price;

import de.hybris.platform.product.PriceService;

import java.util.Map;

import com.simon.core.price.DetailedPriceInfo;


/**
 * Service for the different prices of a product.
 *
 * @spring.bean priceService
 */
public interface ExtPriceService extends PriceService
{

	/**
	 * This method provides utility to decide which price needs to be shown as strike through and what percent off user
	 * is getting.Here lp is the list price and sp is the sale price 
	 * 
	 * @param msrp
	 * @param lp
	 * @param sp
	 * @return isMsrpStrikeThough, isLpStrikeThrough, percentSaving
	 */
	Map<DetailedPriceInfo, Object> getDetailedPriceInformation(double msrp, double lp, double sp);
}
