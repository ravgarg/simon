package com.simon.core.caches;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;
import com.simon.core.services.VariantCategoryService;
import com.simon.core.services.VariantValueCategoryService;


/**
 * VariantValueCategoryPreProcessor creates new {@link VariantValueCategoryModel} and associated Media for Color type
 * variant category if present in product feed found during pre-processing.
 */
public class VariantValueCategoryPreProcessor extends AbstractPreProcessor<VariantValueCategoryModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(VariantValueCategoryPreProcessor.class);

	private static final int VALID_DELIMS = 2;

	@Autowired
	private VariantCategoryService variantCategoryService;

	@Autowired
	private VariantValueCategoryService variantValueCategoryService;

	@Autowired
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Autowired
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private UserService userService;

	@Autowired
	private MediaService mediaService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createKey(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		String key = null;
		final String[] delims = StringUtils.split(rawValue, "=");
		if (delims.length == VALID_DELIMS && StringUtils.isNotEmpty(delims[0]) && StringUtils.isNotEmpty(delims[1]))
		{
			final String variantCategoryCode = (String) variantCategoryKeyGenerator.generateFor(delims[0]);
			final String variantValueCategoryCode = (String) variantValueCategoryKeyGenerator.generateFor(delims[1]) + "-"
					+ variantCategoryCode;
			key = variantValueCategoryCode;
		}
		return key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantValueCategoryModel createValue(final String attribute, final String rawValue,
			final Map<String, String> productValues, final ProductImportFileContextData context)
	{
		VariantValueCategoryModel variantValueCategory;

		final String variantValueCategoryCode = createKey(attribute, rawValue, productValues, context);
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);

		final VariantValueCategoryModel existingVariantValueCategory = getVariantValueCategoryFromDatabase(catalogVersion,
				variantValueCategoryCode);
		if (existingVariantValueCategory != null)
		{
			variantValueCategory = existingVariantValueCategory;
		}
		else
		{
			variantValueCategory = createVariantValueCategory(variantValueCategoryCode, rawValue, catalogVersion);
		}

		return variantValueCategory;
	}

	/**
	 * Creates the variant category.
	 *
	 * @param variantValueCategoryCode
	 *           the variant value category code
	 * @param rawValue
	 *           the raw value
	 * @param catalogVersion
	 *           the catalog version
	 * @param productValues
	 *           the product values
	 * @return the variant category model
	 */
	private VariantValueCategoryModel createVariantValueCategory(final String variantValueCategoryCode, final String rawValue,
			final CatalogVersionModel catalogVersion)
	{
		LOGGER.info("Creating New Variant Value Category :{} ", variantValueCategoryCode);
		final VariantValueCategoryModel variantValueCategory = modelService.create(VariantValueCategoryModel.class);
		variantValueCategory.setCode(variantValueCategoryCode);
		variantValueCategory.setName(StringUtils.split(rawValue, "=")[1]);
		variantValueCategory.setCatalogVersion(catalogVersion);
		variantValueCategory.setAllowedPrincipals(Arrays.asList(userService.getUserGroupForUID("customergroup")));

		final String variantCategoryCode = getVariantCategoryCode(rawValue);
		final VariantCategoryModel variantCategory = getVariantCategoryFromDatabase(catalogVersion, variantCategoryCode);
		variantValueCategory.setSupercategories(Arrays.asList(variantCategory));

		final VariantValueCategoryModel topSequenceCategory = variantValueCategoryService
				.getTopSequenceVariantValueCategory(variantCategory);
		final int topSequence = topSequenceCategory != null ? topSequenceCategory.getSequence() : 0;
		variantValueCategory.setSequence(topSequence + 1);
		modelService.save(variantValueCategory);
		return variantValueCategory;
	}


	/**
	 * Gets the variant category from database.
	 *
	 * @param catalogVersion
	 *           the catalog version
	 * @param variantValueCategoryCode
	 *           the variant category code
	 * @return the variant category from database
	 */
	private VariantValueCategoryModel getVariantValueCategoryFromDatabase(final CatalogVersionModel catalogVersion,
			final String variantValueCategoryCode)
	{
		VariantValueCategoryModel variantValueCategory = null;
		try
		{
			final CategoryModel category = variantCategoryService.getCategoryForCode(catalogVersion, variantValueCategoryCode);
			if (category instanceof VariantValueCategoryModel)
			{
				variantValueCategory = (VariantValueCategoryModel) category;
				LOGGER.debug("Variant Value Category :{} Found", variantValueCategory.getCode());
			}
		}
		catch (final UnknownIdentifierException identifierException)
		{
			LOGGER.debug("Variant Value Category :{} Not Found:{}", variantValueCategoryCode, identifierException);
		}
		return variantValueCategory;
	}

	/**
	 * Gets the variant category from database.
	 *
	 * @param catalogVersion
	 *           the catalog version
	 * @param variantCategoryCode
	 *           the variant category code
	 * @return the variant category from database
	 */
	private VariantCategoryModel getVariantCategoryFromDatabase(final CatalogVersionModel catalogVersion,
			final String variantCategoryCode)
	{
		VariantCategoryModel variantCategory = null;
		try
		{
			final CategoryModel category = variantCategoryService.getCategoryForCode(catalogVersion, variantCategoryCode);
			if (category instanceof VariantCategoryModel)
			{
				variantCategory = (VariantCategoryModel) category;
				LOGGER.debug("Variant Category :{} Found", variantCategory.getCode());
			}
		}
		catch (final UnknownIdentifierException identifierException)
		{
			LOGGER.debug("Variant Category :{} Not Found:{}", variantCategoryCode, identifierException);
		}
		return variantCategory;
	}

	/**
	 * Gets the variant category code.
	 *
	 * @param rawValue
	 *           the raw value
	 * @return the variant category code
	 */
	private String getVariantCategoryCode(final String rawValue)
	{
		String key = null;
		final String[] delims = StringUtils.split(rawValue, "=");
		if (delims.length == VALID_DELIMS && StringUtils.isNotEmpty(delims[0]))
		{
			key = (String) variantCategoryKeyGenerator.generateFor(delims[0]);
		}
		return key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ValueType getValueType()
	{
		return ValueType.MULTIPLE;
	}
}
