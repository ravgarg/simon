package com.simon.core.mirakl.services;

import java.util.Date;

import com.simon.core.dto.OfferImportMetaData;
import com.simon.core.exceptions.OfferImportException;
import com.simon.core.mirakl.services.impl.ExtOfferImportServiceImplTest;


/**
 * The Interface ExtOfferImportService this defines operations to import offers and stocks.
 * <p>
 * Known implementation {@link ExtOfferImportServiceImplTest}
 */
public interface ExtOfferImportService
{

	/**
	 * This method will be used to import all the configured offers from mirakl into hybris.
	 * <p>
	 * Once the offers are imported, all the existing offers in hybris that are missing in the latest will be deleted to be
	 * in sync with mirakl.
	 * </p>
	 *
	 * @param missingOffersDeletionDate
	 *           Offers missing in Mirakl and modified before this date will be deleted
	 * @param offerImportMetaData
	 *           Meta data for offers contains meta information used during import
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	void importAllOffers(Date missingOffersDeletionDate, OfferImportMetaData offerImportMetaData) throws OfferImportException;

	/**
	 * This method will be used to import all the offers in mirakl system that are created after the last import happened.
	 * If the last import date is null, then all offers will be imported by default.
	 *
	 * @param lastImportDate
	 *           The last import date
	 * @param offerImportMetaData
	 *           Meta data for offers contains meta information used during import
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	void importOffersUpdatedSince(Date lastImportDate, OfferImportMetaData offerImportMetaData) throws OfferImportException;
}
