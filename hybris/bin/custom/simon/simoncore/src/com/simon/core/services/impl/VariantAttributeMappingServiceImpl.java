package com.simon.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ControlType;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.core.services.VariantAttributeMappingService;


/**
 * This class fetches Variant Attribute Mapping corresponding to a ShopGateway/Target system and a shop
 */
public class VariantAttributeMappingServiceImpl implements VariantAttributeMappingService
{

	@Resource
	private GenericDao<VariantAttributeMappingModel> variantAttributeMappingGenericDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VariantAttributeMappingModel> getVariantAttributeMappingForShop(final ShopModel shop,
			final ShopGatewayModel shopGateway)
	{
		validateParameterNotNullStandardMessage(ShopGatewayModel._TYPECODE, shopGateway);
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(VariantAttributeMappingModel.TARGETSYSTEM, shopGateway);
		paramMap.put(VariantAttributeMappingModel.SHOP, shop);
		return variantAttributeMappingGenericDao.find(paramMap);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<VariantAttributeMappingModel> getVariantAttributeMapping(final ShopGatewayModel shopGateway)

	{
		validateParameterNotNullStandardMessage(ShopGatewayModel._TYPECODE, shopGateway);
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(VariantAttributeMappingModel.TARGETSYSTEM, shopGateway);
		return variantAttributeMappingGenericDao.find(paramMap);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getVariantAttributeLabelMap(final ShopModel shop, final ShopGatewayModel shopGateway)
	{
		validateParameterNotNullStandardMessage(ShopGatewayModel._TYPECODE, shopGateway);
		validateParameterNotNullStandardMessage(ShopModel._TYPECODE, shop);
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(VariantAttributeMappingModel.TARGETSYSTEM, shopGateway);
		paramMap.put(VariantAttributeMappingModel.SHOP, shop);
		final List<VariantAttributeMappingModel> variantAttributesList = variantAttributeMappingGenericDao.find(paramMap);
		return variantAttributesList.stream().collect(Collectors.toMap(VariantAttributeMappingModel::getSourceAttribute,
				VariantAttributeMappingModel::getSourceAttributeLabel));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, ControlType> getVariantAttributeControlTypeMap(final ShopModel shop, final ShopGatewayModel shopGateway)
	{
		validateParameterNotNullStandardMessage(ShopGatewayModel._TYPECODE, shopGateway);
		validateParameterNotNullStandardMessage(ShopModel._TYPECODE, shop);
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(VariantAttributeMappingModel.TARGETSYSTEM, shopGateway);
		paramMap.put(VariantAttributeMappingModel.SHOP, shop);
		final List<VariantAttributeMappingModel> variantAttributesList = variantAttributeMappingGenericDao.find(paramMap);
		return variantAttributesList.stream().collect(
				Collectors.toMap(VariantAttributeMappingModel::getSourceAttribute, VariantAttributeMappingModel::getControlType));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, String> getVarinatAttributeTwoTapLabelMap(final ShopModel shop, final ShopGatewayModel shopGateway)
	{
		validateParameterNotNullStandardMessage(ShopGatewayModel._TYPECODE, shopGateway);
		validateParameterNotNullStandardMessage(ShopModel._TYPECODE, shop);
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put(VariantAttributeMappingModel.TARGETSYSTEM, shopGateway);
		paramMap.put(VariantAttributeMappingModel.SHOP, shop);
		final List<VariantAttributeMappingModel> variantAttributesList = variantAttributeMappingGenericDao.find(paramMap);
		return variantAttributesList.stream().collect(
				Collectors.toMap(VariantAttributeMappingModel::getSourceAttribute, VariantAttributeMappingModel::getTargetAttribute));
	}
}
