package com.simon.core.price;

import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.security.JaloSecurityException;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * compare price row based on PK, Minqtd, MatchValue, currency,net, start date and pricetype.
 *
 */
@SuppressWarnings("deprecation")
public class ExtPriceRowMatchComparator implements Comparator<PriceRow>, Serializable
{

	private static final long serialVersionUID = 1812769781908889659L;

	private static final String SALE = "SALE";

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtPriceRowMatchComparator.class);


	private Currency curr;
	private boolean net;
	private Unit unit;

	/**
	 * Constructor for ExtPriceRowMatchComparator
	 *
	 * @param curr
	 * @param net
	 * @param unit
	 */
	public ExtPriceRowMatchComparator(final Currency curr, final boolean net, final Unit unit)
	{
		this.curr = curr;
		this.net = net;
		this.unit = unit;
	}

	/**
	 * select price row based on MatchValue. which price row have MatchValue that row will preferred if both price row
	 * have and don't have MatchValue than price row selection based on currency.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	public int compare(final PriceRow row1, final PriceRow row2)
	{
		final int matchValue1 = row1.getMatchValueAsPrimitive();
		final int matchValue2 = row2.getMatchValueAsPrimitive();
		if (matchValue1 != matchValue2)
		{
			return matchValue2 - matchValue1;
		}
		else
		{
			return evaluatePriceRowByCurrency(row1, row2);
		}
	}


	/**
	 * select price row based on currency. if price row have currency then it will return and if both have currency or
	 * both don't have currency than price row selection based on net value.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRowByCurrency(final PriceRow row1, final PriceRow row2)
	{

		final boolean c1Match = getCurr().equals(row1.getCurrency());
		final boolean c2Match = getCurr().equals(row2.getCurrency());
		if (c1Match != c2Match)
		{
			return c1Match ? -1 : 1;
		}
		else
		{
			return evaluatePriceRowByNet(row1, row2);
		}

	}

	/**
	 * select price row based on net value. which price row have net value that should return if both are same than
	 * selection of price on start time.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRowByNet(final PriceRow row1, final PriceRow row2)
	{

		final boolean n1Match = isNet() == row1.isNetAsPrimitive();
		final boolean n2Match = isNet() == row2.isNetAsPrimitive();
		if (n1Match != n2Match)
		{
			return n1Match ? -1 : 1;
		}
		else
		{
			return evaluatePriceRowByUnit(row1, row2);
		}

	}

	/**
	 * select price row based on unit value. which price row have net value that should return if both are same than
	 * selection of price on Minqtd.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRowByUnit(final PriceRow row1, final PriceRow row2)
	{

		final boolean u1Match = getUnit().equals(row1.getUnit());
		final boolean u2Match = getUnit().equals(row2.getUnit());
		if (u1Match != u2Match)
		{
			return u1Match ? -1 : 1;
		}
		else
		{
			return evaluatePriceRowByMinqtd(row1, row2);
		}

	}


	/**
	 * select price row based on Minqtd (Scale price definition. A customer needs to order at least this number of units
	 * for this price to be effective. Default value is 1.) which price row have greater value of Minqtd would be
	 * preferred.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRowByMinqtd(final PriceRow row1, final PriceRow row2)
	{

		final long min1 = row1.getMinqtdAsPrimitive();
		final long min2 = row2.getMinqtdAsPrimitive();
		if (min1 != min2)
		{
			return min1 > min2 ? -1 : 1;
		}
		else
		{
			return evaluatePriceRowByStartTime(row1, row2);
		}
	}


	/**
	 * select price row based on start date. if both row have start date than selection of price based on price type.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRowByStartTime(final PriceRow row1, final PriceRow row2)
	{
		final boolean row1Range = row1.getStartTime() != null;
		final boolean row2Range = row2.getStartTime() != null;

		if (row1Range != row2Range)
		{
			return row1Range ? -1 : 1;
		}
		else
		{
			return evaluatePriceRow(row1, row2);
		}
	}

	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * @param row1
	 * @param row2
	 * @return int
	 */
	protected int evaluatePriceRow(final PriceRow row1, final PriceRow row2)
	{

		EnumerationValue row1price = null;
		try
		{
			@SuppressWarnings("rawtypes")
			final Map rowMap = row1.getAllAttributes();
			row1price = (EnumerationValue) rowMap.get(PriceRowModel.PRICETYPE);

		}
		catch (final JaloInvalidParameterException e)
		{
			LOGGER.error("Jalo Invalid Parameter Exception ", e);

		}
		catch (final JaloSecurityException e)
		{
			LOGGER.error("Jalo Security Exception ", e);

		}
		if (null != row1price && row1price.getCode().equals(SALE))
		{
			return -1;
		}
		else
		{
			return comparePK(row1, row2);
		}
	}

	protected int comparePK(final PriceRow row1, final PriceRow row2)
	{
		return row1.getPK().compareTo(row2.getPK());
	}

	/**
	 * @return curr
	 */
	public Currency getCurr()
	{
		return curr;
	}

	/**
	 * @return net
	 */
	public boolean isNet()
	{
		return net;
	}

	/**
	 * @return unit
	 */
	public Unit getUnit()
	{
		return unit;
	}


	/**
	 * @param curr
	 */
	public void setCurr(final Currency curr)
	{
		this.curr = curr;
	}

	/**
	 * @param net
	 */
	public void setNet(final boolean net)
	{
		this.net = net;
	}

	/**
	 * @param unit
	 */
	public void setUnit(final Unit unit)
	{
		this.unit = unit;
	}

}
