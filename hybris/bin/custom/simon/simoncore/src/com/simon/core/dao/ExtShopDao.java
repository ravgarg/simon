package com.simon.core.dao;

import java.util.List;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;


public interface ExtShopDao extends ShopDao
{


	/**
	 * Find list of store by code string. Returns {@link ShopModel} for given store <code>id</code>.
	 *
	 * @param storeIds
	 *
	 * @param objects
	 *           the store <code>id</code>
	 * @return the shop model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>id</code> is <code>null</code>
	 */
	List<ShopModel> findListOfShopsForCodes(String[] storeIds);

	/**
	 * This method return retailers list
	 *
	 * @return
	 */
	public List<ShopModel> getRetailerList();
}
