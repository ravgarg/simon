package com.simon.core.provider.impl;

import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ContentPageType;




/**
 * ShopDetailsValueResolver is used to filter and add field values for the shop associated with the base product of
 * given product. It checks whether the shop values is present. If not, then it checks whether the attribute is optional
 * or not.If not, then it throws the Field value provider exception.
 */
public class ShopDetailsValueResolver extends AbstractValueResolver<GenericVariantProductModel, ShopModel, Object>
{
	/**
	 * The Constant attribute parameter representing one of keys for IndexedProperty.
	 */
	public static final String ATTRIBUTE_PARAM = "attribute";
	/**
	 * The Constant representing default value for attribute key in IndexedProperty.
	 */
	public static final String ATTRIBUTE_PARAM_DEFAULT_VALUE = null;
	/**
	 * The Constant optional parameter representing one of keys for IndexedProperty.
	 */
	public static final String OPTIONAL_PARAM = "optional";
	/**
	 * The Constant representing default value for optional key in IndexedProperty.
	 */
	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;
	/**
	 * Catalog version
	 */
	public static final String CATALOG_VERSION = "Online";

	@Resource
	private ModelService modelService;
	@Resource
	private TypeService typeService;

	/**
	 * This method is used to filter and add field values for the shop associated with the base product of given product.
	 * It checks whether the shop values is present. If not, then it checks whether the attribute is optional or not.If
	 * not, then it throws the Field value provider exception.
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#addFieldValues(de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument,
	 *      de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 *      de.hybris.platform.solrfacetsearch.config.IndexedProperty, de.hybris.platform.core.model.ItemModel,
	 *      de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext)
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<ShopModel, Object> resolverContext) throws FieldValueProviderException
	{
		boolean hasShopValues = false;
		final ShopModel shop = resolverContext.getData();
		if (shop != null)
		{
			final String attributeName = getAttributeName(indexedProperty);
			final Object attributeValue = getAttributeValue(shop, attributeName);

			if (attributeValue instanceof Collection<?>)
			{
				final Collection<ContentPageModel> contentModels = (Collection<ContentPageModel>) attributeValue;
				if (CollectionUtils.isNotEmpty(contentModels))
				{
					for (final ContentPageModel contentModel : contentModels)
					{
						if (CATALOG_VERSION.equalsIgnoreCase(contentModel.getCatalogVersion().getVersion())
								&& ContentPageType.LISTINGPAGE.equals(contentModel.getPageType()))
						{
							hasShopValues = filterAndAddFieldValues(document, batchContext, indexedProperty, contentModel.getLabel(),
									resolverContext.getFieldQualifier());
						}
					}
				}
			}
			else
			{
				hasShopValues = filterAndAddFieldValues(document, batchContext, indexedProperty, attributeValue,
						resolverContext.getFieldQualifier());
			}

		}
		if (!hasShopValues)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}

	}

	/**
	 * This method is used to get attribute name by checking indexed property attribute. If it is not found then it
	 * fetches the indexed property's name.
	 *
	 * @param indexedProperty
	 * @return attributeName
	 */
	protected String getAttributeName(final IndexedProperty indexedProperty)
	{
		String attributeName = ValueProviderParameterUtils.getString(indexedProperty, ATTRIBUTE_PARAM,
				ATTRIBUTE_PARAM_DEFAULT_VALUE);

		if (attributeName == null)
		{
			attributeName = indexedProperty.getName();
		}

		return attributeName;
	}

	/**
	 * This method is used to get the attribute value for the attribute corresponding to the shop.
	 *
	 * @param designer
	 * @param attributeName
	 * @return value
	 */
	protected Object getAttributeValue(final ShopModel shop, final String attributeName)
	{
		Object value = null;

		if (StringUtils.isNotEmpty(attributeName))
		{
			value = getModelAttributeValue(shop, attributeName);
		}
		return value;

	}

	/**
	 * This method is used to fetch the attribute value from the model service depending upon the corresponding type has
	 * the corresponding attribute.
	 *
	 * @param designer
	 * @param attributeName
	 * @return value
	 */
	protected Object getModelAttributeValue(final ShopModel shop, final String attributeName)
	{
		Object value = null;

		final ComposedTypeModel composedType = typeService.getComposedTypeForClass(shop.getClass());
		if (typeService.hasAttribute(composedType, attributeName))
		{
			value = modelService.getAttributeValue(shop, attributeName);
		}

		return value;
	}



	/**
	 * This method is used to load data. It fetches the shop from the base product of the corresponding product.
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#loadData(de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 *      java.util.Collection, de.hybris.platform.core.model.ItemModel)
	 */
	@Override
	protected ShopModel loadData(final IndexerBatchContext batchContext, final Collection<IndexedProperty> indexedProperties,
			final GenericVariantProductModel model) throws FieldValueProviderException
	{
		return model.getBaseProduct().getShop();

	}


}
