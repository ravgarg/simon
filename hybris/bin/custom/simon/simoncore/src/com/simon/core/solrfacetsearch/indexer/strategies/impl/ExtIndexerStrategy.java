package com.simon.core.solrfacetsearch.indexer.strategies.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.impersonation.ImpersonationContext;
import de.hybris.platform.commerceservices.impersonation.ImpersonationService;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.promotionengineservices.model.CatForPromotionSourceRuleModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.config.IndexOperation;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.indexer.strategies.impl.DefaultIndexerStrategy;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.simon.core.dao.ExtPromotionSourceRuleDao;


/**
 *
 */
public class ExtIndexerStrategy extends DefaultIndexerStrategy
{
	private static final Logger LOG = Logger.getLogger(ExtIndexerStrategy.class);

	@Autowired
	private ImpersonationService impersonationService;

	@Autowired
	private UserService userService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@Autowired
	private ExtPromotionSourceRuleDao extPromotionSourceRuleDao;

	@Override
	protected List<PK> resolvePks() throws IndexerException
	{
		final List<PK> resolvedPKs = Lists.newArrayList(super.resolvePks());
		if (IndexOperation.UPDATE.equals(this.getIndexOperation()))
		{
			try
			{
				final List<PK> listOfProductsPKsToUpdate = impersonationService.executeInContext(createImpersonationContext(),
						new ImpersonationService.Executor<List<PK>, SolrServiceException>()
						{
							public List<PK> execute() throws SolrServiceException
							{
								return getPKsForUpdatedPromosOnCategories();
							}
						});
				resolvedPKs.addAll(listOfProductsPKsToUpdate);
			}
			catch (final SolrServiceException e)
			{
				LOG.error("Category Codes for which promotion is updated cannot be fetched due to some error", e);
			}
		}
		return resolvedPKs;
	}

	private List<PK> getPKsForUpdatedPromosOnCategories() throws SolrServiceException
	{
		final List<PK> listOfProductsPKsToUpdate = new ArrayList<>();
		final List<CatForPromotionSourceRuleModel> updatedCategoryPromoRelations = extPromotionSourceRuleDao
				.findUpdatedCategoriesForPromotionSourceRule(this.getIndexedType(), this.getFacetSearchConfig());
		if (CollectionUtils.isNotEmpty(updatedCategoryPromoRelations))
		{
			updatedCategoryPromoRelations.forEach(catPromoRel -> {
				final List<ProductModel> listOfProducts = new ArrayList<>();
				final List<ProductModel> listOfVariants = new ArrayList<>();
				final List<CategoryModel> categories = (List<CategoryModel>) categoryService
						.getCategoriesForCode(catPromoRel.getCategoryCode());
				if (CollectionUtils.isNotEmpty(categories))
				{
					categories.forEach(cat -> listOfProducts.addAll(productService.getProductsForCategory(cat)));
				}
				addAllProductsPks(listOfProductsPKsToUpdate, listOfProducts, listOfVariants);
			});
		}
		return listOfProductsPKsToUpdate;
	}

	/**
	 * Method to addAll Product's pk values.
	 *
	 * @param listOfProductsPKsToUpdate
	 * @param listOfProducts
	 * @param listOfVariants
	 */
	private void addAllProductsPks(final List<PK> listOfProductsPKsToUpdate, final List<ProductModel> listOfProducts,
			final List<ProductModel> listOfVariants)
	{
		if (CollectionUtils.isNotEmpty(listOfProducts))
		{
			listOfProducts.stream().forEach(p -> {
				if (p instanceof GenericVariantProductModel)
				{
					listOfVariants.add(p);
				}
				else
				{
					listOfVariants.addAll(p.getVariants());
				}
			});
			listOfProductsPKsToUpdate.addAll(listOfVariants.stream().map(ProductModel::getPk).collect(Collectors.toList()));
		}
	}

	private ImpersonationContext createImpersonationContext()
	{
		final ImpersonationContext impersonationContext = new ImpersonationContext();
		impersonationContext.setUser(userService.getAdminUser());
		return impersonationContext;
	}
}
