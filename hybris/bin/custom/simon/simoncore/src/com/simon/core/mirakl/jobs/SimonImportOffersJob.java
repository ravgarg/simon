package com.simon.core.mirakl.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.dto.OfferImportMetaData;
import com.simon.core.exceptions.OfferImportException;
import com.simon.core.mirakl.services.ExtOfferImportService;
import com.simon.hybris.core.model.SimonImportOffersCronJobModel;


/**
 * SimonImportOffersJob imports Offers and Stocks in system via {@link ExtOfferImportService}.
 */
public class SimonImportOffersJob extends AbstractJobPerformable<SimonImportOffersCronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SimonImportOffersJob.class);

	@Autowired
	private ExtOfferImportService extOfferImportService;

	/**
	 * Imports offers and stock via {@link ExtOfferImportService}. {@link SimonImportOffersCronJobModel#isFullImport()} is
	 * used to trigger full import. Full import is also triggered if
	 * {@link SimonImportOffersCronJobModel#getLastImportTime()} is null otherwise partial import is triggered.
	 * <p>
	 * {@link OfferImportException} if thrown from {@link ExtOfferImportService} during import results in cron job failure.
	 */
	@Override
	public PerformResult perform(final SimonImportOffersCronJobModel simonImportOffersCronJob)
	{
		LOGGER.info("Started a Offer import..");
		final Date lastImportTime = simonImportOffersCronJob.getLastImportTime();
		final Date jobStartTime = simonImportOffersCronJob.getStartTime();
		try
		{
			final OfferImportMetaData offerImportMetaData = new OfferImportMetaData();
			offerImportMetaData.setBatchSize(simonImportOffersCronJob.getBatchSize());
			offerImportMetaData.setNoOfWorkers(simonImportOffersCronJob.getNumberOfWorkers());
			offerImportMetaData.setSessionUser(simonImportOffersCronJob.getSessionUser());
			offerImportMetaData.setSessionCurrency(simonImportOffersCronJob.getSessionCurrency());
			offerImportMetaData.setSessionLanguage(simonImportOffersCronJob.getSessionLanguage());
			offerImportMetaData.setDeleteMissingOffers(simonImportOffersCronJob.getDeleteMissingOffers());

			if (simonImportOffersCronJob.isFullImport() || lastImportTime == null)
			{
				LOGGER.info("Performing a FULL offers import");
				extOfferImportService.importAllOffers(jobStartTime, offerImportMetaData);
			}
			else
			{
				LOGGER.info("Importing offers updated after [{}]", lastImportTime);
				extOfferImportService.importOffersUpdatedSince(lastImportTime, offerImportMetaData);
			}
		}
		catch (final OfferImportException e)
		{
			LOGGER.error("Exception occurred while importing offers", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		finally
		{
			simonImportOffersCronJob.setLastImportTime(jobStartTime);
			modelService.save(simonImportOffersCronJob);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}
