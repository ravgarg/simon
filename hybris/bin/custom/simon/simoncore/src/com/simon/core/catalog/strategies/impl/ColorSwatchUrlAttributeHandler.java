package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCoreAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.MediaKeyGenerator;
import com.simon.core.model.DesignerModel;


/**
 * ColorSwatchUrlAttributeHandler associates colorSwatchUrl present in feed as <code>swatch image  of color </code> on
 * imported products as <code>media</code> on Generic variant product.
 */
public class ColorSwatchUrlAttributeHandler extends DefaultCoreAttributeHandler
{

	private static final String VARIANT_ATTRIBUTES = "variantAttributes";
	private static final Logger LOGGER = LoggerFactory.getLogger(DesignerAttributeHandler.class);
	@Autowired
	private MediaService mediaService;

	@Autowired
	private MediaKeyGenerator mediaKeyGenerator;

	@Autowired
	private CatalogVersionService catalogVersionService;

	/**
	 * Fetches {@link DesignerModel} for swatchImage media code and associates it with product. In this first we create
	 * Code from Raw color variant by using mediaKeyGenerator and then fetch swatch media from Service.
	 *
	 *
	 *
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = attribute.getCoreAttribute();
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE,
				Catalog.DEFAULT);
		final ProductModel product = determineOwner(coreAttribute, data, context);
		if (product instanceof GenericVariantProductModel)
		{

			final GenericVariantProductModel genericVariantProduct = (GenericVariantProductModel) product;
			if (StringUtils.isNotEmpty(attribute.getValue()))
			{
				try
				{
					String colorSwatchMediaCode = (String) mediaKeyGenerator
							.generateFor(data.getRawProduct().getValues().get(VARIANT_ATTRIBUTES));
					if (StringUtils.isNotEmpty(colorSwatchMediaCode))
					{
						final String shopId = context.getShopId();
						colorSwatchMediaCode = new StringBuilder(shopId).append(SimonCoreConstants.UNDERSCORE)
								.append(colorSwatchMediaCode).toString();
						final MediaModel media = mediaService.getMedia(catalogVersion, colorSwatchMediaCode);
						genericVariantProduct.setSwatchColorMedia(media);
					}
				}
				catch (final Exception exception)
				{
					LOGGER.error("Exception Occurred In Getting/Creating Media : " + attribute.getValue() + " For Product : "
							+ genericVariantProduct.getCode(), exception);
				}
			}


		}
		else
		{
			LOGGER.error(
					"Exception Occurred when fetch product with which media need to associcate when fetch Product. Error due to configuration issue in MiraklCoreAttributeModel    ");
		}
	}

}
