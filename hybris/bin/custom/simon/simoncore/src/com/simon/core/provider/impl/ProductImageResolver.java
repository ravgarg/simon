package com.simon.core.provider.impl;


import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;

import com.simon.core.constants.SimonCoreConstants;


/**
 * This method fetches the images for the {@link GenericVariantProductModel} from ResolverContext, if not null, gets the
 * required value and adds to InputDocument
 *
 * @throws FieldValueProviderException,
 *            Exception is thrown if there is any problem while adding data to index or expected value is not fetched,
 *            given the attribute has optional set to false.
 */
public class ProductImageResolver extends AbstractValueResolver<GenericVariantProductModel, List<MediaContainerModel>, Object>
{

	private static final Logger LOG = Logger.getLogger(ProductImageResolver.class);

	/** The Constant ALTERNATE which is appended after format while indexing. */
	private static final String ALTERNATE = "alternate";

	/** The Constant DEFAULT which is appended after format while indexing. */
	private static final String DEFAULT = "default";
	/**
	 * stores the text mediaFormat.
	 */
	public static final String MEDIA_FORMAT_PARAM = "mediaFormat";
	/**
	 * stores default value for media format.
	 */
	public static final String MEDIA_FORMAT_PARAM_DEFAULT_VALUE = null;

	private static final String HYPHEN = "-";

	/**
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#addFieldValues(de.hybris.platform.
	 *      solrfacetsearch.indexer.spi.InputDocument, de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 *      de.hybris.platform.solrfacetsearch.config.IndexedProperty, T,
	 *      de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext)
	 *
	 *      adds the images for given sku into the solr indexes. throws {@link FieldValueProviderException} if it is not
	 *      able to index the property.
	 *
	 * @param document
	 *           of type {@link InputDocument} denotes the solr document of gven sku in which the property will be
	 *           indexed.
	 * @param batchContext
	 *           of type {@link IndexerBatchContext}
	 * @param indexedProperty
	 *           of type {@link IndexedProperty} denotes the property that is indexed in solr for gven sku.
	 * @param model
	 *           of type {@link model} is used to fetch the images from the database to index in solr.
	 * @param resolverContext
	 *           of type {@link ValueResolverContext} used to fetch images from {@link GenericVariantProductModel}.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<List<MediaContainerModel>, Object> resolverContext) throws FieldValueProviderException
	{
		final List<MediaContainerModel> medias = resolverContext.getData();
		final List<String> mediaURLs = new ArrayList<>();
		int count = 0;
		for (final MediaContainerModel mediaContainer : medias)
		{
			for (final MediaModel media : mediaContainer.getMedias())
			{
				populateMediaUrls(mediaURLs, media, count);
			}
			count++;
		}
		if (CollectionUtils.isNotEmpty(mediaURLs))
		{
			document.addField(indexedProperty, mediaURLs, resolverContext.getFieldQualifier());
		}
		else
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, SimonCoreConstants.OPTIONAL_PARAM,
					SimonCoreConstants.OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	/**
	 * used to fetch the images {@link List<MediaContainerModel>} from {@link GenericVariantProductModel}
	 *
	 * @param batchContext
	 *           of type {@link IndexerBatchContext}.
	 * @param indexedProperties
	 *           of type {@link Collection<IndexedProperty>}.
	 * @param product
	 *           of type {@link GenericVariantProductModel} used to fetch the medias for the product.
	 */
	@Override
	protected List<MediaContainerModel> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel product)
			throws FieldValueProviderException
	{
		final List<MediaContainerModel> galleryImages = new ArrayList<>(product.getGalleryImages());
		try
		{
			galleryImages.sort(new Comparator<MediaContainerModel>()
			{
				@Override
				public int compare(final MediaContainerModel mediaContainer1, final MediaContainerModel mediaContainer2)
				{
					return compareTo(mediaContainer1.getQualifier(), mediaContainer2.getQualifier());
				}
			});
		}
		catch (final NumberFormatException e)
		{
			LOG.debug("Incorrect Media container data ", e);
		}
		return galleryImages;
	}

	/**
	 * This method compares the qualifier of MediaContainerModel. MediaContainer qualifier format is something like
	 * COLH-<ShopID>-<SequenceNo>. First we get the sequence number from the qualifier, then do the comparison on this
	 * sequence number.
	 *
	 * @param mediaContainer1
	 * @param mediaContainer2
	 * @return
	 */
	private int compareTo(final String mediaContainer1, final String mediaContainer2)
	{
		int result = 0;
		final String sequenceNum1 = mediaContainer1.substring(mediaContainer1.lastIndexOf(HYPHEN) + 1);
		final String sequenceNum2 = mediaContainer2.substring(mediaContainer2.lastIndexOf(HYPHEN) + 1);

		if (StringUtils.isNotEmpty(sequenceNum1) && StringUtils.isNotEmpty(sequenceNum2))
		{
			final Integer firstNumber = Integer.valueOf(sequenceNum1);
			final Integer secondNumber = Integer.valueOf(sequenceNum2);
			result = firstNumber.compareTo(secondNumber);
		}
		return result;
	}

	/**
	 * Populate media urls which are indexed into the solr.
	 *
	 * @param mediaURLs
	 *           the media urls for the different format images
	 * @param count
	 *           the counter used to distinguish between different container entries
	 * @param media
	 *           the media {@link MediaModel} that stores the different format media urls.
	 */
	private void populateMediaUrls(final List<String> mediaURLs, final MediaModel media, final int count)
	{

		final String mediaURL = media.getURL();
		if (null != media.getMediaFormat() && StringUtils.isNotBlank(mediaURL) && null != mediaURLs)
		{
			if (mediaURLs.isEmpty() || count == 0)
			{
				mediaURLs.add(media.getMediaFormat().getQualifier().concat(SimonCoreConstants.UNDERSCORE).concat(DEFAULT)
						.concat(SimonCoreConstants.PIPE).concat(media.getURL()));
			}
			else
			{
				mediaURLs.add(media.getMediaFormat().getQualifier().concat(SimonCoreConstants.UNDERSCORE).concat(ALTERNATE)
						.concat(String.valueOf(count)).concat(SimonCoreConstants.PIPE).concat(media.getURL()));
			}
		}
	}

}
