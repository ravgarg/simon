package com.simon.core.exceptions;

import com.simon.core.mirakl.strategies.ProductFileImportPostProcessor;


/**
 * ProductFileImportPostProcessingException is thrown when any error occurs in product file import post processing
 * layer. <br>
 * It is thrown by implementations of {@link ProductFileImportPostProcessor}
 *
 * @see ProductFileImportPostProcessor
 */
public class ProductFileImportPostProcessingException extends Exception
{
	private static final long serialVersionUID = -4859335562964163148L;

	/**
	 * Instantiates a new product file import post processing exception.
	 *
	 * @param message
	 *           the message
	 * @param paramThrowable
	 *           the param throwable
	 */
	public ProductFileImportPostProcessingException(final String message, final Throwable paramThrowable)
	{
		super(message, paramThrowable);
	}
}
