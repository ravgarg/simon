package com.simon.core.strategies.impl;

import de.hybris.platform.commerceservices.strategies.impl.DefaultCheckoutCustomerStrategy;
import de.hybris.platform.core.model.user.CustomerModel;


/**
 * Implementation of {@link DefaultCheckoutCustomerStrategy} that resolves {@link CustomerModel}s from the user service.
 *
 */
public class ExtCheckoutCustomerStrategy extends DefaultCheckoutCustomerStrategy
{

	/**
	 * This method is used to give the current customer model,In case of guest checkout it will give the anonymous user
	 * in return.Otherwise it will return the current checkout user.
	 */
	@Override
	public CustomerModel getCurrentUserForCheckout()
	{
		if (isAnonymousCheckout())
		{
			final CustomerModel checkoutCustomer = (CustomerModel) getUserService()
					.getUserForUID(getCartService().getSessionCart().getUser().getUid());

			return checkoutCustomer;
		}
		return (CustomerModel) getUserService().getCurrentUser();
	}
}
