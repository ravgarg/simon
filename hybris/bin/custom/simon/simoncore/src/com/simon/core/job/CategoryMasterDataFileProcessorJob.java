package com.simon.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants;


/**
 * This class process master category CSV file and create CSV for Merchandising and mirakl category to category Hot
 * folder.
 */
public class CategoryMasterDataFileProcessorJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryMasterDataFileProcessorJob.class);

	private static final String ATTRIBUTE_SET_S_NAME = "ATTRIBUTESET%sNAME";
	private static final String ATTRIBUTE_SET_S_VALUE = "ATTRIBUTESET%sVALUE";

	private static final String PIPE_SEPERATOR = "|";
	private static final String COMMA = ",";
	private static final String SEMI_COLON = ";";

	private static final String INPUT_PATH = "category.master.feed.hotfolder.directory";
	private static final String PROCESSED_PATH = "category.master.feed.processed.directory";
	private static final String INPUT_FILE_NAME = "category.master.feed.input.filename";
	private static final String OUTPUT_PATH = "category.master.feed.hotfolder.inbound";
	private static final String MIRAKL_FILE_PREFIX = "category.master.feed.mirakl.category.file";
	private static final String MERCH_FILE_PREFIX = "category.master.feed.merchandizing.category.file";
	private static final String INPUT_FILE_ATTRIBUTESET_COUNT = "category.master.feed.headers.attributeset.count";

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * This perform method process Input Master category feed file and break into Merchandising category and Mirakl
	 * category CSV
	 */
	@Override
	public PerformResult perform(final CronJobModel model)
	{
		final String inputFilePath = configurationService.getConfiguration().getString(INPUT_PATH) + getFileSeparator()
				+ configurationService.getConfiguration().getString(INPUT_FILE_NAME) + SimonCoreConstants.DOT
				+ SimonCoreConstants.CSV;

		try (Reader inputFile = getFileReader(inputFilePath))
		{
			processMasterCategoryCSVFile(inputFile);
		}
		catch (final IOException e)
		{
			LOGGER.error("Exception Occurred While processing input Master Category Feed", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		final String outputFilePath = configurationService.getConfiguration().getString(PROCESSED_PATH) + getFileSeparator()
				+ configurationService.getConfiguration().getString(INPUT_FILE_NAME) + "_processed_" + new Date().getTime()
				+ SimonCoreConstants.DOT + SimonCoreConstants.CSV;
		moveFile(inputFilePath, outputFilePath);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * method is used for creating csv file after reading each line from feed file
	 *
	 * @param outPutLines
	 * @param outputCsvPath
	 * @param filePrefix
	 * @return
	 * @throws IOException
	 */
	protected File createCSVFile(final List<String> outPutLines, final String outputCsvPath, final String filePrefix)
			throws IOException
	{
		final StringBuilder outputFileBuilder = new StringBuilder();
		outputFileBuilder.append(outputCsvPath).append(getFileSeparator()).append(filePrefix).append(SimonCoreConstants.UNDERSCORE)
				.append(new Date().getTime()).append(SimonCoreConstants.DOT).append(SimonCoreConstants.CSV);
		final String outputFilePath = outputFileBuilder.toString();
		final File directory = new File(outputCsvPath);
		boolean directoryExist = true;
		if (!directory.exists())
		{
			directoryExist = directory.mkdirs();
		}
		if (directoryExist)
		{
			Files.write(Paths.get(outputFilePath), outPutLines, Charset.defaultCharset());
		}
		return new File(outputFilePath);
	}

	/**
	 * Move file from input file path to output file path
	 *
	 */
	protected void moveFile(final String inputFilePath, final String outPutFilePath)
	{
		try
		{
			final File directory = new File(configurationService.getConfiguration().getString(PROCESSED_PATH));
			boolean directoryExist = true;
			if (!directory.exists())
			{
				directoryExist = directory.mkdirs();
			}
			if (directoryExist)
			{
				Files.move(Paths.get(inputFilePath), Paths.get(outPutFilePath));
			}
		}
		catch (final IOException e)
		{
			LOGGER.error("IO Exception While moving feed file into processed folder:", e);

		}
	}

	/**
	 * Method to get record from CSV file
	 */
	protected CSVParser getRawCsvRecords(final Reader inputFile, final int attributeSetCount) throws IOException
	{
		final String[] header = prepareHeader(attributeSetCount);
		return CSVFormat.EXCEL.withHeader(header).withSkipHeaderRecord().parse(inputFile);
	}


	/**
	 * Prepare Header of CSV file
	 * 
	 * @param attributeSetCount
	 * @return
	 */
	private String[] prepareHeader(final int attributeSetCount)
	{
		final List<String> headersList = new ArrayList<>();
		headersList.add(Headers.ID);
		headersList.add(Headers.GOOGLEID);
		headersList.add(Headers.GOOGLEIDNAME);
		headersList.add(Headers.SPOID);
		headersList.add(Headers.SPONAME);
		headersList.add(Headers.REF_SPOPATH);

		for (int count = 1; count <= attributeSetCount; count++)
		{
			headersList.add(String.format(ATTRIBUTE_SET_S_NAME, count));
			headersList.add(String.format(ATTRIBUTE_SET_S_VALUE, count));
		}
		final String[] header = new String[headersList.size()];
		headersList.toArray(header);
		return header;
	}

	/**
	 * method create a new Object of CSVReader to process csv file in given format.
	 *
	 * @param file
	 * @param charset
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	protected InputStreamReader getFileReader(final String file) throws FileNotFoundException
	{
		return new InputStreamReader(new FileInputStream(file), Charset.forName(SimonCoreConstants.UTF8));
	}

	/**
	 * Method to process Master category CSV file by reading each line
	 *
	 * @param outPutLines
	 * @throws IOException
	 */
	private void processMasterCategoryCSVFile(final Reader inputFile) throws IOException
	{
		final int attributeSetCount = configurationService.getConfiguration().getInt(INPUT_FILE_ATTRIBUTESET_COUNT, 6);
		final Iterable<CSVRecord> records = getRawCsvRecords(inputFile, attributeSetCount);
		final Map<String, Set<String>> miraklMerchandizingCategoriesMap = new HashMap<>();
		final Map<String, Map<String, Set<String>>> merchandizingAdditionalAttributesMap = new HashMap<>();
		for (final CSVRecord record : records)
		{
			if (StringUtils.isNotEmpty(record.get(Headers.GOOGLEID)) && StringUtils.isNotEmpty(record.get(Headers.SPOID)))
			{
				populateMerchandizingAdditionalAttributesMap(merchandizingAdditionalAttributesMap, record, attributeSetCount);
				populateMiraklMerchandizingCategoriesMap(miraklMerchandizingCategoriesMap, record);
			}
		}

		LOGGER.debug("Merchandizing Additional Attributes Map Size {}", merchandizingAdditionalAttributesMap.size());

		final List<String> additionalAttributesOutputList = new ArrayList<>();
		for (final Map.Entry<String, Map<String, Set<String>>> entryMech : merchandizingAdditionalAttributesMap.entrySet())
		{
			String attributeValueNameSeparated;
			final StringBuilder categoryattributeValueNameSeparated = new StringBuilder();
			for (final Map.Entry<String, Set<String>> attributeValueNameEntry : entryMech.getValue().entrySet())
			{
				attributeValueNameSeparated = attributeValueNameEntry.getValue().stream().collect(Collectors.joining(COMMA));
				categoryattributeValueNameSeparated.append(attributeValueNameEntry.getKey()).append("->")
						.append(attributeValueNameSeparated).append(PIPE_SEPERATOR);
			}
			additionalAttributesOutputList.add(SEMI_COLON + entryMech.getKey() + SEMI_COLON + categoryattributeValueNameSeparated);
		}

		LOGGER.debug(" Mirakl Merchandizing Categories Map Size {} ", miraklMerchandizingCategoriesMap.size());
		final List<String> miraklMerchandizingCategoriesOutputList = new ArrayList<>();
		for (final Map.Entry<String, Set<String>> entry : miraklMerchandizingCategoriesMap.entrySet())
		{
			final String categoryCommaSeparated = entry.getValue().stream().collect(Collectors.joining(COMMA));
			miraklMerchandizingCategoriesOutputList.add(SEMI_COLON + entry.getKey() + SEMI_COLON + categoryCommaSeparated);
		}
		createOutputCSVFile(additionalAttributesOutputList, MERCH_FILE_PREFIX);
		createOutputCSVFile(miraklMerchandizingCategoriesOutputList, MIRAKL_FILE_PREFIX);
	}

	private void createOutputCSVFile(final List<String> outputList, final String prefix) throws IOException
	{
		createCSVFile(outputList, configurationService.getConfiguration().getString(OUTPUT_PATH),
				configurationService.getConfiguration().getString(prefix));
	}

	/**
	 * Method to create create Mirakl Id with MerchId Map
	 *
	 */
	private void populateMiraklMerchandizingCategoriesMap(final Map<String, Set<String>> miraklMerchandizingMap,
			final CSVRecord record)
	{
		final String miraklCategory = record.get(Headers.GOOGLEID);
		final String merchandizingCategory = record.get(Headers.SPOID);

		Set<String> merchandizingCategories;
		if (miraklMerchandizingMap.containsKey(miraklCategory))
		{
			merchandizingCategories = miraklMerchandizingMap.get(miraklCategory);
		}
		else
		{
			merchandizingCategories = new HashSet<>();
		}
		merchandizingCategories.add(merchandizingCategory);
		miraklMerchandizingMap.put(miraklCategory, merchandizingCategories);
	}

	/**
	 * Method to create create MerchId AdditionalAttribute Map
	 *
	 */
	private void populateMerchandizingAdditionalAttributesMap(
			final Map<String, Map<String, Set<String>>> merchCatgoryAditionalAttributeMap, final CSVRecord record,
			final int attributeSetCount)
	{
		final String merchId = record.get(Headers.SPOID);
		Map<String, Set<String>> additionalAttributeKeyValueMap;
		if (merchCatgoryAditionalAttributeMap.containsKey(merchId))
		{
			additionalAttributeKeyValueMap = merchCatgoryAditionalAttributeMap.get(merchId);
		}
		else
		{
			additionalAttributeKeyValueMap = new HashMap<>();
		}
		for (int counter = 1; counter <= attributeSetCount; counter++)
		{
			populateAdditionalAttributesMap(additionalAttributeKeyValueMap, record, counter);
		}
		merchCatgoryAditionalAttributeMap.put(merchId, additionalAttributeKeyValueMap);


	}

	/**
	 * Check And Put New AttributeNameValue Map
	 */
	private void populateAdditionalAttributesMap(final Map<String, Set<String>> additionalAttributeKeyValueMap,
			final CSVRecord record, final int counter)
	{
		final String attributeName = record.get(String.format(ATTRIBUTE_SET_S_NAME, counter));
		final String attributeValue = record.get(String.format(ATTRIBUTE_SET_S_VALUE, counter));
		if (StringUtils.isNotEmpty(attributeName) && StringUtils.isNotEmpty(attributeValue))
		{
			addEntryInMerchCategoryAdditionalMap(additionalAttributeKeyValueMap, attributeName, attributeValue);
		}
	}

	/**
	 * Add Value In Merchandising Category Additional Value Map
	 */
	private void addEntryInMerchCategoryAdditionalMap(final Map<String, Set<String>> additionalAttributeKeyValueMap,
			final String attributeName, final String attributeValue)
	{
		Set<String> attributeValues;
		if (additionalAttributeKeyValueMap.containsKey(attributeName))
		{
			attributeValues = additionalAttributeKeyValueMap.get(attributeName);
		}
		else
		{
			attributeValues = new HashSet<>();
		}
		attributeValues.add(attributeValue);
		additionalAttributeKeyValueMap.put(attributeName, attributeValues);
	}


	/**
	 * Method to return File separator
	 *
	 * @return
	 */
	private String getFileSeparator()
	{
		return System.getProperty("file.separator");
	}

	public class Headers
	{
		private Headers()
		{
			//empty
		}

		public static final String ID = "ID";
		public static final String GOOGLEID = "GOOGLEID";
		public static final String GOOGLEIDNAME = "GOOGLEIDNAME";
		public static final String SPOID = "SPOID";
		public static final String SPONAME = "SPONAME";
		public static final String REF_SPOPATH = "REF_SPOPATH";

	}
}
