package com.simon.core.mirakl.product.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.simon.core.mirakl.product.dao.MiraklRawProductDAO;


/**
 * Mirakl raw product dao class
 *
 */
public class MiraklRawProductDAOImpl implements MiraklRawProductDAO
{
	@Resource
	protected FlexibleSearchService flexibleSearchService;

	protected static final String RAW_PRODUCTS_FETCH_QUERY = "SELECT {mrp:" + MiraklRawProductModel.PK + "} FROM {"
			+ MiraklRawProductModel._TYPECODE + " AS mrp} WHERE {mrp:" + MiraklRawProductModel.IMPORTID + "} = ?"
			+ MiraklRawProductModel.IMPORTID;

	@Override
	public List<MiraklRawProductModel> fetchMiraklProductsForImport(final String importId)
	{
		final FlexibleSearchQuery rawProductsFetchQuery = new FlexibleSearchQuery(RAW_PRODUCTS_FETCH_QUERY,
				Collections.singletonMap(MiraklRawProductModel.IMPORTID, importId));
		final SearchResult<MiraklRawProductModel> rawProducts = flexibleSearchService.search(rawProductsFetchQuery);
		return rawProducts.getResult();
	}
}
