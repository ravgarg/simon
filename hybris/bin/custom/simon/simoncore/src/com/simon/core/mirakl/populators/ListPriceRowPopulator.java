package com.simon.core.mirakl.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.europe1.model.PriceRowModel;

import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.simon.core.enums.PriceType;


/**
 * ListPriceRowPopulator populates {@link PriceRowModel} used as list price from {@link MiraklExportOffer}
 */
public class ListPriceRowPopulator implements Populator<MiraklExportOffer, PriceRowModel>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final MiraklExportOffer miraklExportOffer, final PriceRowModel target)
	{
		if (miraklExportOffer.getDiscount() != null && miraklExportOffer.getDiscount().getOriginPrice() != null)
		{
			target.setPrice(miraklExportOffer.getDiscount().getOriginPrice().doubleValue());
		}
		else
		{
			target.setPrice(miraklExportOffer.getPrice().doubleValue());
		}
		target.setStartTime(miraklExportOffer.getAvailableStartDate());
		target.setEndTime(miraklExportOffer.getAvailableEndDate());
		target.setPriceType(PriceType.LIST);
		target.setOfferId(miraklExportOffer.getId());
		target.setOfferSku(miraklExportOffer.getProductSku());
		target.setOfferStateCode(miraklExportOffer.getStateCode());
		target.setLeadtimeToShip(miraklExportOffer.getLeadtimeToShip() != null ? miraklExportOffer.getLeadtimeToShip() : 0);
	}
}
