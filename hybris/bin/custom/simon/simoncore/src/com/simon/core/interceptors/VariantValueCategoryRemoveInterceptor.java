package com.simon.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Remove interceptor to remove thumbnail for variant value category being deleted.
 */
public class VariantValueCategoryRemoveInterceptor implements RemoveInterceptor<VariantValueCategoryModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(VariantValueCategoryRemoveInterceptor.class);

	/**
	 * On remove method to register elements to remove
	 */
	@Override
	public void onRemove(final VariantValueCategoryModel variantValueCategory, final InterceptorContext ctx)
			throws InterceptorException
	{
		if (null != variantValueCategory.getThumbnail())
		{
		LOGGER.debug("Registering Removal for VVC-Thumbnail {}:{} for Product {}",
				variantValueCategory.getThumbnail().getCode(), variantValueCategory.getThumbnail().getPk(),
				variantValueCategory.getCode());
		ctx.registerElementFor(variantValueCategory.getThumbnail(), PersistenceOperation.DELETE);
		}

	}

}
