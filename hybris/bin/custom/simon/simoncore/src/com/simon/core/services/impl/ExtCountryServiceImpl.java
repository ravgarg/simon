package com.simon.core.services.impl;

import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.simon.core.services.ExtCountryService;


/**
 * Implementation form ExtCountryService
 */
public class ExtCountryServiceImpl implements ExtCountryService
{
	@Resource
	private GenericDao<CountryModel> extGenericCountryDao;

	@Override
	public CountryModel getCountryForExternalId(final String externalId)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(CountryModel.EXTERNALID, externalId);
		final List<CountryModel> countryModels = extGenericCountryDao.find(params);

		if (isNotEmpty(countryModels) && countryModels.size() > 1)
		{
			throw new AmbiguousIdentifierException(format("Found more than one country having the External Code %s", externalId));
		}

		return isEmpty(countryModels) ? null : countryModels.get(0);
	}

}
