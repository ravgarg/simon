package com.simon.core.mirakl.populators;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Optional;

import com.mirakl.hybris.core.product.populators.ProductExportBrandPopulator;
import com.simon.core.model.DesignerModel;


/**
 * ExtProductExportBrandPopulator extends {@link ProductExportBrandPopulator} and exports
 * {@link ProductModel#getProductDesigner()} name as brand label.
 */
public class ExtProductExportBrandPopulator extends ProductExportBrandPopulator
{

	/**
	 * Exports {@link ProductModel#getProductDesigner()} name as brand label
	 */
	@Override
	protected String getBrandLabel(final ProductModel productModel)
	{
		return Optional.ofNullable(((GenericVariantProductModel) productModel).getBaseProduct().getProductDesigner())
				.map(DesignerModel::getName).orElse("");
	}

}
