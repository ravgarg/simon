package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.time.Instant;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCoreAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.constants.SimonCoreConstants.Catalog;


/**
 * MediaAttributeHandler, generic attribute handler for all media type attributes.
 * <p>
 * {@link MiraklCoreAttributeModel#TYPEPARAMETER} is used as an attribute qualifier used to determine on which attribute
 * media will be created or updated.
 * <p>
 * This does not download the media; it stores/updates the url in the media model.
 */
public class MediaAttributeHandler extends DefaultCoreAttributeHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(MediaAttributeHandler.class);
	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * Creates/Updates media on attribute specified by {@link MiraklCoreAttributeModel#TYPEPARAMETER}. This can be any
	 * attribute of type {@link MediaModel}
	 * <p>
	 * This does not downloads the media just stores/updated the url in the media model.
	 * <p>
	 * Media url is fetched from {@link AttributeValueData#getValue()}
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = attribute.getCoreAttribute();
		final String mediaUrl = attribute.getValue();
		final String attributeQualifier = attribute.getCoreAttribute().getTypeParameter();

		if (StringUtils.isNotEmpty(mediaUrl) && StringUtils.isNotEmpty(attributeQualifier))
		{
			final ProductModel product = determineOwner(coreAttribute, data, context);
			final MediaModel media = getOrCreateMedia(product, attribute, attributeQualifier);
			media.setURL(mediaUrl);
			LOGGER.debug("Updating Image : {} On Attribute : {} Product : {}", mediaUrl, attributeQualifier, product.getCode());
			markItemsToSave(data, media);
		}
	}

	/**
	 * Gets existing media object associated with product. If media object exists it is returned else a new media object
	 * is created.
	 *
	 * @param product
	 *           the product
	 * @param attribute
	 *           the attribute
	 * @param attributeQualifier
	 *           the attribute qualifier
	 * @return the or create media
	 */
	private MediaModel getOrCreateMedia(final ProductModel product, final AttributeValueData attribute,
			final String attributeQualifier)
	{
		MediaModel media;
		final Object mediaObject = modelService.getAttributeValue(product, attributeQualifier);
		if (mediaObject != null)
		{
			media = (MediaModel) mediaObject;
		}
		else
		{
			media = modelService.create(MediaModel.class);
			media.setCode(product.getCode() + "-" + attribute.getCode() + "-" + Instant.now().toEpochMilli());
			media.setCatalogVersion(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT));
			modelService.setAttributeValue(product, attributeQualifier, media);
		}
		return media;
	}
}
