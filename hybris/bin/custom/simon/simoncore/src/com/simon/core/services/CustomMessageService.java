package com.simon.core.services;

import com.simon.core.exceptions.SystemException;


/**
 * Service Interface to fetch messagetext from Dao with messageCode as input parameter
 */
public interface CustomMessageService
{
	/**
	 * This method gets the messageText for Code from {@link SimonMessageModel}.
	 *
	 */
	String getMessageForCode(final String messageCode) throws SystemException;
}
