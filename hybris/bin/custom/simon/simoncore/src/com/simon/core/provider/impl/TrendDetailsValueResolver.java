package com.simon.core.provider.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.model.TrendModel;




/**
 * TrendDetailsValueResolver is used to filter and add field values for the trend associated with the base product of
 * given product. It checks whether the trend values is present. If not, then it checks whether the attribute is
 * optional or not.If not, then it throws the Field value provider exception.
 */
public class TrendDetailsValueResolver extends AbstractValueResolver<GenericVariantProductModel, List<TrendModel>, Object>
{
	/**
	 * The Constant attribute parameter representing one of keys for IndexedProperty.
	 */
	public static final String ATTRIBUTE_PARAM = "attribute";
	/**
	 * The Constant representing default value for attribute key in IndexedProperty.
	 */
	public static final String ATTRIBUTE_PARAM_DEFAULT_VALUE = null;
	/**
	 * The Constant optional parameter representing one of keys for IndexedProperty.
	 */
	public static final String OPTIONAL_PARAM = "optional";
	/**
	 * The Constant representing default value for optional key in IndexedProperty.
	 */
	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	@Resource
	private ModelService modelService;
	@Resource
	private TypeService typeService;

	/**
	 * This method is used to filter and add field values for the trend associated with the base product of given
	 * product. It checks whether the trend values is present. If not, then it checks whether the attribute is optional
	 * or not.If not, then it throws the Field value provider exception.
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#addFieldValues(de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument,
	 *      de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 *      de.hybris.platform.solrfacetsearch.config.IndexedProperty, de.hybris.platform.core.model.ItemModel,
	 *      de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver.ValueResolverContext)
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<List<TrendModel>, Object> resolverContext) throws FieldValueProviderException
	{
		boolean hasTrendValues = false;
		final List<TrendModel> trend = resolverContext.getData();
		if (CollectionUtils.isNotEmpty(trend))
		{
			final List<String> trendListAttributeValue = new ArrayList<>();
			trend.forEach(t -> trendListAttributeValue.add(t.getCode()));
			hasTrendValues = filterAndAddFieldValues(document, batchContext, indexedProperty, trendListAttributeValue,
					resolverContext.getFieldQualifier());
		}
		if (!hasTrendValues)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}

	}

	/**
	 * This method is used to load data. It fetches the trend from the base product of the corresponding product.
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver#loadData(de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext,
	 *      java.util.Collection, de.hybris.platform.core.model.ItemModel)
	 */
	@Override
	protected List<TrendModel> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel model)
			throws FieldValueProviderException
	{
		return (List<TrendModel>) model.getBaseProduct().getTrends();

	}



}
