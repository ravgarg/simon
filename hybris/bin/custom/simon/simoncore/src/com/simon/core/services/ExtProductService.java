package com.simon.core.services;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.simon.core.enums.PriceType;


/**
 * The Interface ExtProductService, extends {@link ProductService} provides additional operations to search
 * {@code ProductModel}.
 */
public interface ExtProductService extends ProductService
{

	/**
	 * Gets the products for code. Returns list of <code>ProductModel</code> for gives <code>codes</code> and
	 * <code>catalogVersion</code>.
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @param productIdList
	 *           the codes products code to search
	 * @return the products for code, returns list of <code>ProductModel</code> found.
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code> or if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> getProductsForCodes(final CatalogVersionModel catalogVersion, final Set<String> productIdList);

	/**
	 * Get list of products having price row and product Approval status is check and Price type is list.
	 *
	 * @param priceType
	 *           the price type
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @return the checked products having price row
	 */
	List<GenericVariantProductModel> getCheckedProductsHavingPriceRow(final PriceType priceType,
			final CatalogVersionModel catalogVersion);

	/**
	 * This method returns all Generic Variant Products which have gallery image, and whose base Product is associated
	 * with atleast 1 Merchandizing category.
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @return returns list of GenericVariantProductModel
	 * @throws IllegalArgumentException
	 *            if the given catalogVersion is null
	 */
	List<GenericVariantProductModel> getUnApprovedProductsHavingImageAndMerchandizingCategory(CatalogVersionModel catalogVersion);

	/**
	 * This method returns true if Product is favorite which is associated with a customer.
	 *
	 * @param productModel
	 *           the product model
	 * @return boolean
	 */
	boolean isFavorite(ProductModel productModel);

	/**
	 * Gets the products for catalog version. Returns list of <code>ProductModel</code> for given
	 * <code>catalogVersion</code>.
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @return the products for code, returns list of <code>ProductModel</code> found.
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> getProductsForCatalogVersion(final CatalogVersionModel catalogVersion);

	/**
	 * Gets the products not associated to any merchandizing category for given catalog version. Returns list of
	 * <code>ProductModel</code> not associated to any merchandizing category for given <code>catalogVersion</code>.
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @return the products for code, returns list of <code>ProductModel</code> found.
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> getProductsWithoutMerchandizingCategory(CatalogVersionModel catalogVersion);

	/**
	 * This method use to make the product disabled
	 *
	 * @param productList
	 */
	void makeProductsDisabled(final Set<String> productList);

	/**
	 * Gets the products for catalog version and approval status. Returns list of <code>ProductModel</code> for given
	 * <code>catalogVersion</code> and <code>approvalStatus</code>.
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @param approvalStatus
	 *           the approval status for which products will be returned
	 * @return the products for code, returns list of <code>ProductModel</code> found.
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> or <code>approvalStatus</code> is null
	 */
	List<ProductModel> getProductsForCatalogVersion(final CatalogVersionModel catalogVersion,
			final ArticleApprovalStatus approvalStatus);

	/**
	 * This method will return the list of {@link GenericVariantProductModel} that have gone out of stock
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @param lastSuccessJobRun
	 * @return {@link List} of {@link GenericVariantProductModel}
	 */
	List<GenericVariantProductModel> getProductsWithZeroStock(CatalogVersionModel catalogVersion, Date lastSuccessJobRun);
}
