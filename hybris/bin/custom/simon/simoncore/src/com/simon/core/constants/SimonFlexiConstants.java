package com.simon.core.constants;

import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.constants.CategoryConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.link.LinkModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.CustomMessageModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.model.SizeModel;
import com.simon.generated.model.ErrorLogModel;



@SuppressWarnings(
{ "javadoc", "deprecation", "squid:S1192" })
public class SimonFlexiConstants
{

	private SimonFlexiConstants()
	{
		//empty
	}

	public static final String SUBCATEGORIES_OF_CATEGORIES_BY_CATEGORY_ID_QUERY = "select distinct{cat2cat.target} from " + "{"
			+ CategoryModel._TYPECODE + " as cat join " + CategoryConstants.Relations.CATEGORYCATEGORYRELATION + " as cat2cat on "
			+ "{cat." + CategoryModel.PK + "}=" + "{cat2cat." + LinkModel.SOURCE + "} join "
			+ CategoryConstants.Relations.CATEGORYPRODUCTRELATION + " as cat2prod on " + "{cat2cat." + LinkModel.TARGET + "}="
			+ "{cat2prod." + LinkModel.SOURCE + "}}" + " where {cat." + CategoryModel.CODE + "}=?categoryId";

	/*
	 * Flexi Query to fetch CustomMessageModel. With input parameters messageCode and catalog version
	 */
	public static final String MESSAGE_BY_CODE = "select {" + CustomMessageModel.PK + "} from {" + CustomMessageModel._TYPECODE
			+ "} where {" + CustomMessageModel.UID + "}=?code and {" + CustomMessageModel.CATALOGVERSION + "} in (?catalogVersions)";

	public static final String SELECT_PRICES_MODIFIED_BEFORE_DATE = "SELECT {o." + PriceRowModel.PK + "} FROM {"
			+ PriceRowModel._TYPECODE + " as o} WHERE {o." + PriceRowModel.MODIFIEDTIME + "} < ?" + PriceRowModel.MODIFIEDTIME;

	public static final String SELECT_STOCKS_MODIFIED_BEFORE_DATE = "SELECT {o." + StockLevelModel.PK + "} FROM {"
			+ StockLevelModel._TYPECODE + " as o} WHERE {o." + StockLevelModel.MODIFIEDTIME + "} < ?" + StockLevelModel.MODIFIEDTIME;


	public static final String SELECT_PRICES_FOR_PRODUCT_CODES = "SELECT {o." + PriceRowModel.PK + "} FROM {"
			+ PriceRowModel._TYPECODE + " as o} WHERE {o." + PriceRowModel.PRODUCTID
			+ "} IN (?prodIds) AND {o.priceType} = ?priceType";

	public static final String SELECT_PRODUCTS_FOR_CODES = "SELECT {p:" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE
			+ " AS p} WHERE {p:" + ProductModel.CODE + "} IN (?codes)" + " AND {p:" + ProductModel.CATALOGVERSION
			+ "}=?catalogVersion";


	public static final String TWOTAP_CART_BY_CART_ID_QUERY = "SELECT {pk} FROM {Cart} WHERE {Cart.code} = ?cartId";
	public static final String TWOTAP_CART_BY_PURCHASE_ID_QUERY = "SELECT {pk} FROM {ProxyCart} WHERE {purchaseId} = ?purchaseId";
	public static final String ORDER_BY_PURCHASE_ID_QUERY = "SELECT {oo.pk} FROM {ProxyCart as tt JOIN Order as oo ON {oo.proxyCart} = {tt.pk}}"
			+ "WHERE {tt.purchaseId} = ?purchaseId";
	public static final String CART_BY_CART_ID_QUERY = "SELECT {pk} FROM {Cart} WHERE {cartId} = ?cartId";

	public static final String SELECT_L1_CATEGORIES = "select {cat2cat." + LinkModel.TARGET + "} from {"
			+ CategoryConstants.Relations.CATEGORYCATEGORYRELATION + " as cat2cat} where {cat2cat." + LinkModel.SOURCE
			+ "} in ({{select {cat." + CategoryModel.PK + "} from {" + CategoryModel._TYPECODE + " as cat} where {cat."
			+ CategoryModel.CODE + "}=?categoryId and {cat." + CategoryModel.CATALOGVERSION + "} IN ({{SELECT {"
			+ CatalogVersionModel.PK + "} FROM {" + CatalogVersionModel._TYPECODE + "} WHERE {" + CatalogVersionModel.CATALOG
			+ "} IN ({{SELECT {" + CatalogModel.PK + "} FROM {" + CatalogModel._TYPECODE + "} WHERE {" + CatalogModel.ID + "} = '"
			+ SimonCoreConstants.Catalog.PRODUCT_CATALOG_CODE + "'}}) AND {" + CatalogVersionModel.VERSION + "} IN ('"
			+ SimonCoreConstants.Catalog.ONLINE + "')}})}})";

	public static final String FIND_ORDER_BY_ORDER_ID_QUERY = "SELECT {pk} FROM {Order} WHERE {Order.code} = ?orderId and {Order.versionID} is null";

	public static final String FIND_VERSIONED_ORDER_BY_ORDER_ID_QUERY = "SELECT {pk} FROM {Order} WHERE {Order.code} = ?orderId and {Order.versionID} is not null";

	public static final String FIND_TOP_VARIANTVAUECATEGORY_SEQUENCE = "select top 1 {vvc.pk} from {VariantValueCategory as vvc JOIN CategoryCategoryRelation as rel ON {rel.target}={vvc.pk} JOIN VariantCategory as vc ON {rel.source}={vc.pk}} where {vc.pk}=?variantCategory order by {vvc.sequence} DESC";

	public static final String SELECT_CHECKED_GVP_HAVING_PRICEROW = "SELECT {prod:" + GenericVariantProductModel.PK + "} FROM {"
			+ GenericVariantProductModel._TYPECODE + " AS prod join PriceRow as pr on {prod.code}={pr.productId}} WHERE  {prod:"
			+ GenericVariantProductModel.APPROVALSTATUS + "} = ?approvalStatus" + " AND {pr:" + PriceRowModel.PRICETYPE
			+ "} =?priceType AND {" + GenericVariantProductModel.CATALOGVERSION + "}=?catalogVersion";

	public static final String SELECT_UNAPPROVED_GVP_HAVING_IMAGE_AND_MERCH_CATEGORY = "select distinct {gvp.pk} from {MerchandizingCategory as merchCat JOIN CategoryProductRelation as CatProd ON {merchCat.pk}={CatProd.source} JOIN Product as prod ON {prod:pk}={CatProd.target} JOIN GenericVariantProduct as gvp on {gvp:baseProduct}={prod.pk}} WHERE {gvp.approvalStatus} IN ({{SELECT {pk} FROM {ArticleApprovalStatus AS aas} WHERE {aas:code} IN ('unapproved')}}) and {gvp.galleryImages} is not null and ({gvp.mpn} is not null or {gvp.gtin} is not null) and {gvp.catalogversion}=?catalogVersion";


	public static final String FIND_ALL_STORES_FOR_CODES = "SELECT {" + ShopModel.PK + "} " + " FROM {" + ShopModel._TYPECODE
			+ " AS  sh  " + "JOIN " + ShopState._TYPECODE + " AS ss ON {sh:" + ShopModel.STATE + "}={ss:pk" + "}} " + "WHERE {sh:"
			+ ShopModel.ID + "} IN (?ids) AND {ss.code} IN ('OPEN')";

	public static final String FIND_FAVORITES_BY_CUSTOMER = "select {p.modifiedtime}, {p.target} from {Customer2FavProductRel as p}  where {p.source} = ?customer order by {p.modifiedtime} desc";

	public static final String FIND_ALL_DESIGNERS_EXCEPT_SAVED = "SELECT {" + DesignerModel.PK + "} FROM {"
			+ DesignerModel._TYPECODE + "} WHERE {" + DesignerModel.PK
			+ "} NOT IN ({{SELECT {target} FROM {customer2DesignerRel} WHERE {source} = ?customer }}) AND {" + DesignerModel.ACTIVE
			+ "} = 1 ";

	public static final String FIND_ADDRESS_BY_DUPLICATE_FROM = "SELECT {" + AddressModel.PK + "} FROM {" + AddressModel._TYPECODE
			+ "} WHERE {" + AddressModel.DUPLICATEFROM + "}=?duplicateFromAddressId";

	public static final String FIND_ALL_STORES_EXCEPT_SAVED = "SELECT { sh:" + ShopModel.PK + "} " + " FROM {"
			+ ShopModel._TYPECODE + " AS  sh  " + "JOIN " + ShopState._TYPECODE + " AS ss ON {sh:" + ShopModel.STATE + "}={ss:pk"
			+ "}} " + "WHERE { sh:" + ShopModel.PK
			+ "} NOT IN ({{SELECT {target} FROM {customer2ShopRel} WHERE {source} = ?customer }}) AND {ss.code} IN ('OPEN')";

	public static final String FIND_ALL_DESIGNERS = "SELECT {" + DesignerModel.PK + "} FROM {" + DesignerModel._TYPECODE
			+ "} WHERE {" + DesignerModel.ACTIVE + "} = 1 ";


	public static final String FIND_DEALS_BY_CUSTOMER = "SELECT {d.target} FROM {Customer2DealRel as d} WHERE {d.source} = ?customer";

	public static final String FIND_DEALS_BY_IDS = "SELECT {" + DealModel.PK + "} " + " FROM {" + DealModel._TYPECODE + "} WHERE {"
			+ DealModel.CODE + "} IN (?codes) ";

	public static final String FIND_ALL_OPENED_DEALS = " SELECT {dl." + DealModel.PK + "} FROM {" + DealModel._TYPECODE
			+ "  AS dl JOIN " + ShopModel._TYPECODE + "  AS s ON {dl:" + DealModel.SHOP + "}={s." + ShopModel.PK + "} JOIN "
			+ ShopState._TYPECODE + "  AS ss ON {s:" + ShopModel.STATE + "}={ss:pk}" + "} WHERE {ss.code} IN ('OPEN')";

	public static final String FIND_OPENED_DEALS_WITH_CODE = " SELECT {dl." + DealModel.PK + "} FROM {" + DealModel._TYPECODE
			+ "  AS dl JOIN " + ShopModel._TYPECODE + "  AS s ON {dl:" + DealModel.SHOP + "}={s." + ShopModel.PK + "} JOIN "
			+ ShopState._TYPECODE + "  AS ss ON {s:" + ShopModel.STATE + "}={ss:pk}" + "} WHERE {ss.code} IN ('OPEN') AND {dl."
			+ DealModel.CODE + "} IN (?codes) ";

	public static final String FIND_ORDER_EXPORT = "SELECT DISTINCT  {O." + OrderModel.PK + "} FROM {" + OrderModel._TYPECODE
			+ " AS O  JOIN " + ConsignmentModel._TYPECODE + " AS C ON {O." + OrderModel.PK + "} = {C." + ConsignmentModel.ORDER
			+ " } JOIN " + ConsignmentEntryModel._TYPECODE + " AS E ON {C.PK} = {E.consignment} } WHERE to_char({O."
			+ OrderModel.MODIFIEDTIME
			+ "},'YYYY-MM-DD HH24:MM:SS') BETWEEN ?initialTime AND ?finalTime AND {O.versionID} is null and {O.ORIGINALVERSION } is null";

	public static final String SELECT_PRODUCTS_FOR_CATALOGVERSION = "SELECT {pk} FROM {Product!} WHERE {catalogVersion}=?catalogVersion";

	public static final String SELECT_PRODUCTS_FOR_CATALOGVERSION_APPROVAL_STATUS = "SELECT {pk} FROM {GenericVariantProduct!} WHERE {catalogVersion}=?catalogVersion AND {approvalStatus}=?approvalStatus";

	public static final String SELECT_PRODUCTS_WITH_ZERO_STOCK = "SELECT {pk} FROM {GenericVariantProduct!} WHERE {code} IN ({{SELECT {productCode} FROM {StockLevel} WHERE {available} = 0 AND {modifiedTime} > ?lastSuccessJobRun}}) AND {catalogVersion}=?catalogVersion";

	public static final String SELECT_PRODUCTS_MISSING_MERCH_CATEGORY = "SELECT {pk} FROM {Product!} WHERE {pk} NOT IN ({{SELECT DISTINCT ({prod.pk}) FROM {MerchandizingCategory AS cat JOIN AutoGenCategoriesToProduct AS rel ON {cat.pk}={rel.source} JOIN Product AS prod ON {prod:pk}={rel.target}} WHERE {prod.catalogVersion}=?catalogVersion}}) AND {catalogVersion}=?catalogVersion";

	public static final String FIND_ALL_SIZES = "SELECT {" + SizeModel.PK + "} FROM {" + SizeModel._TYPECODE + "} ";

	public static final String FIND_ALL_SIZES_FOR_CODES = "SELECT {" + SizeModel.PK + "} FROM {" + SizeModel._TYPECODE
			+ "} WHERE {" + SizeModel.CODE + "} IN (?codes)";

	public static final String FIND_SIZE_BY_SIZE_CODE_QUERY = "SELECT {pk} FROM {Size} WHERE {Size.code} = ?code";

	public static final String FIND_LEAF_VARIANT_CATEGORY = "SELECT {cat.PK} FROM {VariantCategory AS cat} WHERE NOT EXISTS ({{SELECT {pk} FROM {CategoryCategoryRelation AS rel JOIN VariantCategory AS sub ON {rel.target}={sub.PK}} WHERE {rel:source}={cat.pk} AND {sub.catalogVersion}={cat.catalogVersion} }}) AND {cat.catalogVersion} =?catalogVersion";

	public static final String FIND_SEQUENCE_FOR_SIZE = "SELECT {" + ProductSizeSequenceModel.PK + "} FROM {"
			+ ProductSizeSequenceModel._TYPECODE + "} WHERE LOWER({" + ProductSizeSequenceModel.SIZE + "}) = LOWER(?size)";

	public static final String FIND_PRODUCT_CODES_ERRORLOG = "SELECT DISTINCT {productIds} FROM {ErrorLog} WHERE {errorMessage}= ?errorMessage AND {"
			+ ErrorLogModel.CREATIONTIME + "} > ?jobLastModifiedTime";

	public static final String SELECT_PRODUCT_CODES = "SELECT {p:" + ProductModel.PK + "} FROM {" + ProductModel._TYPECODE
			+ " AS p} WHERE {p:" + ProductModel.CODE + "} IN (?codes)";

	public static final String FIND_ALL_SEQUENCE_FOR_SIZE = "SELECT {" + ProductSizeSequenceModel.PK + "} FROM {"
			+ ProductSizeSequenceModel._TYPECODE + "}";
	public static final String WHITELISTEDUSER_BY_EMAIL_QUERY = "SELECT {pk} FROM {WhitelistedUser} WHERE {WhitelistedUser.EMAILID} = ?email";
	public static final String FIND_ALL_MEDIA_CONTAINER_WHERE_VERSION_GT_ZERO = "SELECT {" + MediaContainerModel.PK
			+ " } FROM {MediaContainer} WHERE {version}!=0 and {" + MediaContainerModel.MODIFIEDTIME + "} < ?cleanupAgeDate";
}
