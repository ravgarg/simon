package com.simon.media.hotfolder.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.constants.CatalogConstants;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.mediaconversion.conversion.DefaultMediaConversionService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants.Catalog;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;


/**
 * delete old media if exist and create new.
 */
@SuppressFBWarnings
public class ExtMediaImpexConverter extends DefaultImpexConverter
{
	@Resource
	private DefaultMediaConversionService defaultMediaConversionService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private ModelService modelService;

	@Resource
	private ProductService productService;

	@Resource
	private UserService userService;

	@Resource
	private SessionService sessionService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtMediaImpexConverter.class);

	/*
	 * delete old media if exist and create new (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter#convert(java.util.
	 * Map, java.lang.Long)
	 */
	@Override
	public String convert(final Map<Integer, String> row, final Long sequenceId)
	{

		if (!MapUtils.isEmpty(row))
		{
			final String productId = row.get(0);
			try
			{
				final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
						Catalog.STAGED);
				sessionService.executeInLocalView(new SessionExecutionBody()
				{
					@Override
					public Object execute()
					{
						sessionService.setAttribute(CatalogConstants.SESSION_CATALOG_VERSIONS, catalogVersion);
						final ProductModel productModel = productService.getProductForCode(catalogVersion, productId);
						if (productModel != null && CollectionUtils.isNotEmpty(productModel.getGalleryImages()))
						{
							productModel.getGalleryImages().stream().forEach(mediaContainer -> {
								mediaContainer.setVersion((int) mediaContainer.getPk().getLongValue());
								modelService.save(mediaContainer);
							});
							productModel.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
							productModel.setGalleryImages(null);
							modelService.save(productModel);
						}
						return productModel;
					}
				}, userService.getAdminUser());
			}
			catch (final Exception ex)
			{
				LOGGER.error(productId + " Exception Occurred In Looking Up  Product OR Removing Medias ", ex);
			}
		}
		try
		{
			return super.convert(row, sequenceId);
		}
		catch (final Exception e)
		{
			LOGGER.error("Caught an Exception in convert.", e);
		}
		return "";
	}

}
