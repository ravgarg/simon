package com.simon.core.interceptors;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Remove interceptor to remove each and every media of Media container being removed
 */
public class MediaContainerRemoveInterceptor implements RemoveInterceptor<MediaContainerModel>
{

	private static final Logger LOGGER = LoggerFactory.getLogger(MediaContainerRemoveInterceptor.class);

	/**
	 * On remove method to register elements to remove
	 */
	@Override
	public void onRemove(final MediaContainerModel mediaContianer, final InterceptorContext ctx) throws InterceptorException
	{
		mediaContianer.getMedias().forEach(media -> {
			LOGGER.debug("Registering Removal for Media {}:{} for MediaContainer {}:{}", media.getCode(), media.getPk(),
					mediaContianer.getQualifier(), mediaContianer.getPk());
			ctx.registerElementFor(media, PersistenceOperation.DELETE);
		});

	}

}
