
package com.simon.core.services.impl;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.simon.core.dao.MallDao;
import com.simon.core.model.MallModel;
import com.simon.core.services.MallService;


/**
 * MallServiceImpl, default implementation of {@link MallService}.
 */
public class MallServiceImpl implements MallService
{

	/** The Mall dao. */
	@Resource(name = "mallDao")
	private MallDao mallDao;


	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public MallModel getMallForCode(final String code)
	{
		return mallDao.findMallByCode(code);
	}


	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public List<MallModel> getAllMalls()
	{
		return mallDao.findAllMalls();
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public Set<MallModel> getMallListForCode(final List<String> mallCodes)
	{
		return mallDao.findMallListForCode(mallCodes);
	}
}
