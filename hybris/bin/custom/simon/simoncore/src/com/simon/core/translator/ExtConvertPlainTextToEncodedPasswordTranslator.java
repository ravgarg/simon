package com.simon.core.translator;

import de.hybris.platform.core.Registry;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.translators.AbstractSpecialValueTranslator;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.persistence.security.PasswordEncoderFactory;

import org.apache.commons.lang.StringUtils;

import com.simon.core.jalo.WhitelistedUser;


/**
 *
 */
public class ExtConvertPlainTextToEncodedPasswordTranslator extends AbstractSpecialValueTranslator
{
	@Override
	public void performImport(final String cellValue, final Item processedItem) throws ImpExException
	{
		if (cellValue != null && !cellValue.trim().startsWith("<ignore>") && processedItem != null && processedItem.isAlive()
				&& processedItem instanceof WhitelistedUser)
		{
			final WhitelistedUser user = (WhitelistedUser) processedItem;
			if (cellValue.length() > 0)
			{
				final int pos = cellValue.indexOf(58);
				String encoding = pos != -1 ? cellValue.substring(0, pos).trim() : null;
				if (StringUtils.EMPTY.equals(encoding))
				{
					encoding = "*";
				}

				String password = pos != -1 ? cellValue.substring(pos + 1).trim() : cellValue;
				if (StringUtils.EMPTY.equals(password))
				{
					password = null;
				}

				final PasswordEncoderFactory factory = Registry.getCurrentTenant().getJaloConnection().getPasswordEncoderFactory();
				final String passwordEncoded = factory.getEncoder(encoding).encode(user.getEmailId(), password);
				user.setPassword(passwordEncoded);
				user.setPasswordEncoding(encoding);
			}

		}

	}

}
