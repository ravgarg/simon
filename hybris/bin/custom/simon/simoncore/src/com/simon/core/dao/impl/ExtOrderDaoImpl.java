package com.simon.core.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtOrderDao;
import com.simon.core.util.CommonUtils;


/**
 * This DAO class is used to retrieve order model
 */
public class ExtOrderDaoImpl extends AbstractItemDao implements ExtOrderDao
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtOrderDaoImpl.class);


	private static final String INITIAL_TIME = "initialTime";
	private static final String FINAL_TIME = "finalTime";

	/**
	 * @description Method us to get the Order Model from the database based on the Order Id
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	@Override
	public OrderModel getOrderByOrderId(final String orderId)
	{
		ServicesUtil.validateParameterNotNull(orderId, "order id cannot be null");
		OrderModel orderModel = null;

		try
		{
			final FlexibleSearchQuery fQuery = getFlexibleSearchQuery(SimonFlexiConstants.FIND_ORDER_BY_ORDER_ID_QUERY);
			fQuery.addQueryParameter("orderId", orderId);
			final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
			if (CollectionUtils.isNotEmpty(results))
			{
				orderModel = (OrderModel) results.get(0);
			}
		}
		catch (final RuntimeException exception)
		{
			LOG.error("Error occured when finding the order model in the database ", exception);
		}
		return orderModel;
	}

	/**
	 * @description Method us to get the Order Model from the database based on the Order Id
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	@Override
	public OrderModel getVersionedOrderByOrderId(final String orderId)
	{
		ServicesUtil.validateParameterNotNull(orderId, "order id cannot be null");
		OrderModel orderModel = null;

		try
		{
			final FlexibleSearchQuery fQuery = getFlexibleSearchQuery(SimonFlexiConstants.FIND_VERSIONED_ORDER_BY_ORDER_ID_QUERY);
			fQuery.addQueryParameter("orderId", orderId);
			final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
			if (CollectionUtils.isNotEmpty(results))
			{
				orderModel = (OrderModel) results.get(0);
			}
		}
		catch (final RuntimeException exception)
		{
			LOG.error("Error occured when finding the order model in the database ", exception);
		}
		return orderModel;
	}

	protected FlexibleSearchQuery getFlexibleSearchQuery(final String code)
	{
		return new FlexibleSearchQuery(code);
	}

	/**
	 * Returns orders placed in a day
	 *
	 * @return {@link List} of {@link OrderModel} - matched orders
	 */
	@Override
	public List<OrderModel> findOrdersToExport(final int day, final String startHours, final String endHours)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_ORDER_EXPORT);

		fQuery.addQueryParameter(INITIAL_TIME, CommonUtils.getStartDate(day, startHours));
		fQuery.addQueryParameter(FINAL_TIME, CommonUtils.getEndDate(day, endHours));

		return getFlexibleSearchService().<OrderModel> search(fQuery).getResult();
	}

}
