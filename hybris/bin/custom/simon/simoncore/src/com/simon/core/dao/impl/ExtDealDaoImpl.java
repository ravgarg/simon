package com.simon.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtDealDao;
import com.simon.core.model.DealModel;


/**
 * The Class DefaultDealDao. DB based default implementation of {@link ExtDealDao}
 */
public class ExtDealDaoImpl extends DefaultGenericDao<DealModel> implements ExtDealDao
{
	/**
	 * Instantiates a new default deal dao.
	 */
	public ExtDealDaoImpl()
	{
		super("Deal");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DealModel findDealByCode(final String code)
	{
		final List<DealModel> resList = find(Collections.singletonMap("code", code));
		return resList.isEmpty() ? null : resList.get(0);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DealModel> findAllDeals()
	{
		final SearchResult<DealModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_ALL_OPENED_DEALS);
		return result.getResult();
	}

	/**
	 * Find list of deal by code string. Returns {@link DealModel} for given deal <code>code</code>.
	 *
	 * @param String
	 *           the deal <code>code</code>
	 * @return the deal model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code>
	 */
	@Override
	public List<DealModel> findListOfDealForCodes(final List<String> codes)
	{
		validateParameterNotNull(codes, "codes must not be null");
		final Map<String, List<String>> queryParams = new HashMap<>();
		queryParams.put("codes", codes);
		final SearchResult<DealModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_OPENED_DEALS_WITH_CODE,
				queryParams);
		return result.getResult();

	}
}
