package com.simon.core.populators;

import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroupCommand;
import de.hybris.platform.util.localization.Localization;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.ImmutableMap;
import com.mirakl.hybris.facades.shop.ShopFacade;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.BaseColorsData;
import com.simon.core.dto.ColorDetailsData;
import com.simon.core.dto.ImagesData;
import com.simon.core.dto.ProductDetailsData;
import com.simon.core.dto.ProductDimensionsData;
import com.simon.core.dto.VariantCategoryData;
import com.simon.core.dto.VariantValueCategoryData;


/**
 * This class populates {@link ProductDetailsData} DTO from {@link SolrSearchResponse}
 *
 * @param <FACET_SEARCH_CONFIG_TYPE>
 * @param <INDEXED_TYPE_TYPE>
 * @param <INDEXED_PROPERTY_TYPE>
 * @param <INDEXED_TYPE_SORT_TYPE>
 * @param <ITEM>
 *           {@link SolrSearchResponse} Response which we get from Solr
 */
public class ProductDetailResponsePopulator<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, INDEXED_TYPE_SORT_TYPE, ITEM>
		implements
		Populator<SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult>, ProductDetailsData> //NOSONAR
{

	private static final String DECIMAL_FORMATOR = "#.00";

	/**
	 * This populator populates data from {@link Document} into {@link ProductDimensionsData}
	 */
	@Resource
	private ProductDimensionDataPopulator productDimensionDataPopulator;

	/**
	 * This class populates data from {@link Document} into Map of string and {@link VariantCategoryData}
	 */
	@Resource
	private VariantCategoryMapPopulator variantCategoryMapPopulator;

	/**
	 * This class populates data from {@link Document} into Map
	 */
	@Resource
	private ProductImageDataPopulator productImageDataPopulator;

	/**
	 * Commoni18N service to fetch current currecy symbol, to fetch price data from price factory
	 */
	@Resource
	private CommonI18NService commonI18NService;

	@Resource(name = "shopFacade")
	private ShopFacade shopFacade;

	@Resource
	private TypeService typeService;

	@Resource
	private EnumerationService enumerationService;


	/**
	 * The primary method of populator which populates {@link ProductDetailsData} DTO from {@link SolrSearchResponse}
	 * Data is populated {@link ColorDetailsData}, corresponding to each group in {@link SolrSearchResponse}
	 *
	 * ColorId selection is done only once. Selected colorId is set to colorid of the first group having atleast 1
	 * document where inStoreFlag is true
	 *
	 * @param source
	 *           {@link SolrSearchResponse} Response which is fetched from Solr.
	 *
	 * @param target
	 *           {@link ProductDetailsData} consisting of complete dataset
	 *
	 * @throws ConversionException
	 *            if document's field value is not able to cast in given type
	 */
	@Override
	public void populate(
			final SolrSearchResponse<FACET_SEARCH_CONFIG_TYPE, INDEXED_TYPE_TYPE, INDEXED_PROPERTY_TYPE, SearchQuery, INDEXED_TYPE_SORT_TYPE, SearchResult> source,
			final ProductDetailsData target)
	{
		if (null != source.getSearchResult() && CollectionUtils.isNotEmpty(source.getSearchResult().getGroupCommands()))
		{
			final SearchResultGroupCommand groupCommand = source.getSearchResult().getGroupCommands().get(0);
			if (CollectionUtils.isNotEmpty(groupCommand.getGroups()))
			{
				populatePunchOutDetails(target, groupCommand);

				final List<Double> plumPrices = new ArrayList<>();
				final Map<String, ColorDetailsData> colorDetails = new LinkedHashMap<>(source.getSearchResult().getPageSize());
				final List<BaseColorsData> baseColors = new ArrayList<>();
				String selectedColorId = StringUtils.EMPTY;
				String colorId;
				ColorDetailsData colorDetailsData;
				List<Double> plumPrice;
				final TreeSet<BigDecimal> strikeOffPrice = new TreeSet<>();
				final String currencySymbol = commonI18NService.getCurrentCurrency().getSymbol();
				int maxPercentOff = 0;
				boolean productsPresentInGroup = false;

				for (final SearchResultGroup group : groupCommand.getGroups())
				{
					final Map<String, Pair<ColorDetailsData, List<Double>>> productGroupData = populateColorsDataAndPlumPrice(group);

					colorId = productGroupData.keySet().iterator().next();
					colorDetailsData = productGroupData.values().iterator().next().getLeft();
					plumPrice = productGroupData.values().iterator().next().getRight();

					colorDetails.putAll(ImmutableMap.of(colorId, colorDetailsData));
					plumPrices.addAll(plumPrice);
					baseColors.add(populateBaseColorData(group));
					strikeOffPrice
							.addAll(colorDetailsData.getProductDimensionsData().stream().filter(pd -> pd.getStrikeOffPrice() != null)
									.map(pd -> pd.getStrikeOffPrice()).collect(Collectors.toList()));

					if (StringUtils.isEmpty(selectedColorId))
					{
						selectedColorId = populateSelectedColor(group);
						productsPresentInGroup = true;
					}
					maxPercentOff = getPercentSavingsForRangePrices(group, maxPercentOff);
				}
				setstrikeOffPriceRange(target, strikeOffPrice, currencySymbol);
				setPercentOffForPriceRange(target, maxPercentOff);
				setPlumPriceRange(target, plumPrices, currencySymbol);
				setBaseProductData(target, groupCommand);
				setPotentialPromotionData(target, groupCommand);
				//setting first color Id as default in case Product Exist in group and no variant is in stock
				if (StringUtils.isEmpty(selectedColorId) && productsPresentInGroup)
				{
					selectedColorId = (String) groupCommand.getGroups().get(0).getDocuments().get(0)
							.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID);
				}
				target.setSelectedColorName(selectedColorId);
				target.setColordetails(colorDetails);
				target.setBaseColors(baseColors);
			}
		}
	}

	/**
	 * Populate the punch out details for the product.
	 *
	 * @param groupCommand
	 *           {@link SearchResultGroupCommand} used to fetch the details from document.
	 *
	 * @param target
	 *           {@link ProductDetailsData} consisting of complete dataset
	 */
	private void populatePunchOutDetails(final ProductDetailsData target, final SearchResultGroupCommand groupCommand)
	{
		final Document firstDocument = groupCommand.getGroups().get(0).getDocuments().get(0);
		target.setBuyFrom(getLocalizedString(SimonCoreConstants.BUY_FROM_TEXT)
				+ firstDocument.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_NAME));
		target.setPunchOutFlag(null != firstDocument.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_PUNCH_OUT)
				? String.valueOf(Boolean.FALSE) : String.valueOf(Boolean.TRUE));
		target.setRedirectionTextPre(getLocalizedString(SimonCoreConstants.REDIRECT_TEXT_PRE));
		target.setRedirectionTextPost(getLocalizedString(SimonCoreConstants.REDIRECT_TEXT_POST));
		target.setClickHereText(getLocalizedString(SimonCoreConstants.REDIRECT_CLICK_HERE));
		final String url = (String) firstDocument.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PUNCH_OUT_URL);
		if (null != url)
		{
			final String[] urlString = url.split(SimonCoreConstants.FORWARD_SLASH);

			final List<String> urlList = Arrays.asList(urlString);
			if (urlList.size() > SimonCoreConstants.TWO_INT)
			{
				target.setDomain(urlList.get(SimonCoreConstants.TWO_INT));
			}
			target.setProductUrl(url);
		}
	}

	private void setPotentialPromotionData(final ProductDetailsData target, final SearchResultGroupCommand groupCommand)
	{
		final PromotionData potentialPromoData = new PromotionData();
		final List<SearchResultGroup> groups = groupCommand.getGroups();
		if (CollectionUtils.isNotEmpty(groups))
		{
			final List<Document> filteredDocuments = new ArrayList<>();
			groups.forEach(g -> {
				filteredDocuments.addAll(g.getDocuments().stream()
						.filter(doc -> StringUtils.isNotEmpty((String) doc.getFieldValue(IndexedKeys.PROMOTION_CODE)))
						.collect(Collectors.toList()));
			});
			if (CollectionUtils.isNotEmpty(filteredDocuments))
			{
				final Optional<Document> document = filteredDocuments.stream()
						.max(Comparator.comparing(fD -> Integer.parseInt((String) fD.getFieldValue(IndexedKeys.PROMOTION_PRIORITY))));
				if (isPromotionValid(document))
				{
					potentialPromoData.setCode((String) document.get().getFieldValue(IndexedKeys.PROMOTION_CODE));
					potentialPromoData.setDescription((String) document.get().getFieldValue(IndexedKeys.PROMOTION_DESCRIPTION));
					potentialPromoData.setPromotionType((String) document.get().getFieldValue(IndexedKeys.PROMOTION_TYPE));
					potentialPromoData.setEndDate((Date) document.get().getFieldValue(IndexedKeys.PROMOTION_END_DATE));
					potentialPromoData.setStartDate((Date) document.get().getFieldValue(IndexedKeys.PROMOTION_START_DATE));
				}
			}
		}
		target.setPotentialPromotionData(potentialPromoData);
	}

	private boolean isPromotionValid(final Optional<Document> document)
	{
		if (document.isPresent())
		{
			final Date currentDate = new Date();
			final Date startDate = (Date) document.get().getFieldValue(IndexedKeys.PROMOTION_START_DATE);
			final Date endDate = (Date) document.get().getFieldValue(IndexedKeys.PROMOTION_END_DATE);
			if (startDate != null && endDate != null)
			{
				return (currentDate.after(startDate) && currentDate.before(endDate)) ? true : false;
			}
			else if (startDate != null)
			{
				return currentDate.after(startDate) ? true : false;
			}
			else if (endDate != null)
			{
				return currentDate.before(endDate) ? true : false;
			}
			else
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Sets Percent Savings for a Price Range Values
	 *
	 * @param target
	 * @param maxPercentOff
	 */
	private void setPercentOffForPriceRange(final ProductDetailsData target, final int maxPercentOff)
	{
		if (maxPercentOff > 0)
		{
			final String percentOff = getLocalizedString(SimonCoreConstants.PERCENT_SAVING_PRE_MESSAGE) + maxPercentOff
					+ getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
			target.setPercentOff(percentOff);
		}
	}

	/**
	 * Returns maximum Percent Off for a Search Result Group
	 *
	 * @param group
	 * @param maxPercentOff
	 * @return maximum Percent Off for a Search Result Group
	 */
	private int getPercentSavingsForRangePrices(final SearchResultGroup group, final int maxPercentOff)
	{
		int maxPercOff = maxPercentOff;
		for (final Document document : group.getDocuments())
		{
			final Object variantProductPriceRaw = document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PRICE);
			if (variantProductPriceRaw != null)
			{
				final List<String> variantProductPrice = (List<String>) variantProductPriceRaw;
				maxPercOff = Math.max(maxPercOff, Integer.parseInt(
						(variantProductPrice).stream().filter(p -> p.contains(SimonCoreConstants.PERCENTAGE_SAVING)).findFirst()
								.orElse(SimonCoreConstants.PERCENTAGE_SAVING + SimonCoreConstants.PIPE + SimonCoreConstants.ZERO_INT)
								.split(Pattern.quote(SimonCoreConstants.PIPE))[1]));
			}
		}
		return maxPercOff;
	}

	private void setBaseProductData(final ProductDetailsData target, final SearchResultGroupCommand groupCommand)
	{
		target.setColorTitle(getLocalizedString(SimonCoreConstants.PRODUCT_COLOR_TITLE));
		final Document document = groupCommand.getGroups().get(0).getDocuments().get(0);
		target.setBaseProductCode((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_CODE));
		target.setBaseProductDesc((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESCRIPTION));
		target.setDesignerName((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME));
		target.setRetailerName((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_NAME));
		target.setDesignerUrl((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_URL));
		final Object retailerUrlList = document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_URL);
		if (retailerUrlList != null)
		{
			final List<String> retailerUrl = (List<String>) retailerUrlList;
			if (CollectionUtils.isNotEmpty(retailerUrl))
			{
				target.setRetailerUrl(retailerUrl.get(0));
			}
		}

		target.setRetailerLogo((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_LOGO));
		target.setProductTitle((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME));
		final String retailerId = (String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_ID);
		final String shippigReturnInfo = shopFacade.getShopForId(retailerId).getReturnPolicy();
		target.setRetailerShippingReturnInfo(shippigReturnInfo);
		target.setRetailerID((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_ID));
	}

	/**
	 * Method to get localized label from locale properties
	 *
	 * @return Localized title string.
	 */
	protected String getLocalizedString(final String key)
	{
		return Localization.getLocalizedString(key);
	}

	/**
	 * Sets price range for plum price
	 *
	 * @param target
	 * @param plumPrices
	 * @param currencySymbol
	 */
	private void setPlumPriceRange(final ProductDetailsData target, final List<Double> plumPrices, final String currencySymbol)
	{
		final List<Double> finalPrices = new ArrayList<>(new HashSet<>(plumPrices));
		finalPrices.removeAll(Collections.singleton(null));
		Collections.sort(finalPrices);
		if (finalPrices.size() > 1)
		{
			target.setPriceRange(currencySymbol + new DecimalFormat(DECIMAL_FORMATOR).format(finalPrices.get(0)) + "-"
					+ currencySymbol + new DecimalFormat(DECIMAL_FORMATOR).format(finalPrices.get(finalPrices.size() - 1)));
		}
		else if (finalPrices.size() == 1)
		{
			target.setPriceRange(currencySymbol + new DecimalFormat(DECIMAL_FORMATOR).format(finalPrices.get(0)));
		}
	}

	/**
	 * Sets strikeoff Price range in format
	 *
	 * @param target
	 * @param priceSet
	 * @param currencySymbol
	 */
	private void setstrikeOffPriceRange(final ProductDetailsData target, final TreeSet<BigDecimal> priceSet,
			final String currencySymbol)
	{
		String priceRangeString = null;
		final DecimalFormat decimalFormat = new DecimalFormat("#.00");
		if (CollectionUtils.isNotEmpty(priceSet))
		{
			if (checkIfNoPriceRange(priceSet))
			{
				priceRangeString = currencySymbol + decimalFormat.format(priceSet.first());
			}
			else
			{
				priceRangeString = currencySymbol + decimalFormat.format(priceSet.first()) + SimonCoreConstants.HYPHEN
						+ currencySymbol + decimalFormat.format(priceSet.last());
			}
			target.setStrikeOffPriceRange(priceRangeString);
		}
	}

	/**
	 * checks if range exists in price
	 *
	 * @param priceRange
	 * @return
	 */
	private boolean checkIfNoPriceRange(final TreeSet<BigDecimal> priceRange)
	{
		return priceRange.size() == 1 || priceRange.first().equals(priceRange.last());
	}


	/**
	 * This method checks all the documents {@link Document} of the group {@link SearchResultGroup} Returns the colorId
	 * of first document, where inStockFlag is set to true
	 *
	 * @param group
	 *           Represents a single group of GenericVariantProduct having same color
	 * @return selectedColorId
	 */
	private String populateSelectedColor(final SearchResultGroup group)
	{
		return (String) group.getDocuments().stream().filter(s -> ((Boolean) s.getFieldValue(IndexedKeys.INSTOCKFLAG))).findFirst()
				.map(s -> s.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID)).orElse("");
	}

	/**
	 * This method fetches first document from group and sets the data into {@link BaseColorsData}. As, every document in
	 * group will have same color Variant Category details, so information is fetched from first document only
	 *
	 * @param group
	 *           Represents a single group of GenericVariantProduct having same color
	 * @return {@link BaseColorsData} consisting of color VariantCategory details
	 */
	private BaseColorsData populateBaseColorData(final SearchResultGroup group)
	{
		final BaseColorsData baseColorsData = new BaseColorsData();
		baseColorsData.setColorId((String) group.getDocuments().get(0).getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID));
		baseColorsData.setColorName((String) group.getDocuments().get(0).getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME));
		final String colorUrl = (String) group.getDocuments().get(0).getFieldValue(IndexedKeys.COLOR_SWATCH_IMAGE_URL);
		baseColorsData.setSwatchImageUrl(colorUrl);

		return baseColorsData;
	}


	/**
	 * This method populates data from a single group {@link SearchResultGroup}. It internally calls 3 populators, which
	 * sets the data of {@link ColorDetailsData}
	 *
	 * {@link ProductImageDataPopulator} sets images
	 *
	 * {@link ProductDimensionDataPopulator} sets details of single GenericVariantProduct
	 *
	 * {@link VariantCategoryMapPopulator} sets VariantCategory and corresponding VariantValueCategory data
	 *
	 *
	 * @param group
	 *           Represents a single group of GenericVariantProduct having same color
	 *
	 * @return Map where key is ColorId and value is {@link ColorDetailsData} which consists of information belonging to
	 *         that color. LinkedHashMap is used to maintain the insertion order
	 */
	protected Map<String, Pair<ColorDetailsData, List<Double>>> populateColorsDataAndPlumPrice(final SearchResultGroup group)
	{
		final List<Double> plumPrices = new ArrayList<>();
		final Map<String, Pair<ColorDetailsData, List<Double>>> groupColorAndPricePair = new LinkedHashMap<>();

		final ColorDetailsData colorDetails = new ColorDetailsData();
		final Document firstDocument = group.getDocuments().get(0);
		colorDetails.setColorName((String) firstDocument.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME));


		final List<ProductDimensionsData> productDimensionList = new ArrayList<>();

		final Map<String, VariantCategoryData> variantCategoryMapMain = new LinkedHashMap<>();
		final Map<String, ImagesData> images = new HashMap<>();
		productImageDataPopulator.populate(firstDocument, images);
		colorDetails.setImages(images);

		for (final Document document : group.getDocuments())
		{

			plumPrices.add((Double) document.getFieldValue(IndexedKeys.PRODUCT_PLUM_PRICE));

			productDimensionList.add(populateProductDimensions(document));

			final Map<String, VariantCategoryData> variantCategoryMap = new LinkedHashMap<>();
			variantCategoryMapPopulator.populate(document, variantCategoryMap);

			for (final Entry<String, VariantCategoryData> variantCategory : variantCategoryMap.entrySet())
			{
				final List<VariantValueCategoryData> swatches = variantCategory.getValue().getSwatches();
				final String variantCategoryId = variantCategory.getKey();
				final String title = variantCategory.getValue().getTitle();
				final String controlType = variantCategory.getValue().getControlType();
				if (variantCategoryMapMain.containsKey(variantCategoryId))
				{
					final Predicate<VariantValueCategoryData> ifCategoryIdExist = valueCategory -> valueCategory.getValueCategoryId()
							.equals(swatches.get(0).getValueCategoryId());
					if (!variantCategoryMapMain.get(variantCategoryId).getSwatches().stream().filter(ifCategoryIdExist).findFirst()
							.isPresent())
					{
						variantCategoryMapMain.get(variantCategoryId).getSwatches().add(swatches.get(0));
					}
				}
				else
				{
					final VariantCategoryData variantCategoryData = new VariantCategoryData();
					variantCategoryData.setTitle(variantCategoryMap.values().iterator().next().getTitle());
					final List<VariantValueCategoryData> swatchesList = new ArrayList<>();
					swatchesList.add(swatches.get(0));

					variantCategoryData.setTitle(title);
					variantCategoryData.setSwatches(swatchesList);
					variantCategoryData.setControlType(controlType);
					variantCategoryMapMain.put(variantCategoryId, variantCategoryData);
				}
			}
		}

		variantCategoryMapMain.keySet().forEach(vmKey -> {
			List<VariantValueCategoryData> swatchesSortedList = variantCategoryMapMain.get(vmKey).getSwatches();
			swatchesSortedList = swatchesSortedList.stream().sorted(Comparator
					.comparing(VariantValueCategoryData::getValueCategorySequence, Comparator.nullsLast(Comparator.naturalOrder())))
					.collect(Collectors.toList());
			variantCategoryMapMain.get(vmKey).setSwatches(swatchesSortedList);
		});


		colorDetails.setProductDimensionsData(productDimensionList);
		colorDetails.setVariantData(variantCategoryMapMain);
		colorDetails
				.setInStore(productDimensionList.stream().filter(ProductDimensionsData::isVariantInStore).findFirst().isPresent());
		groupColorAndPricePair.put(productDimensionList.get(0).getColorId(),
				new ImmutablePair<ColorDetailsData, List<Double>>(colorDetails, plumPrices));
		return groupColorAndPricePair;

	}

	/**
	 * This method populates data from a single document {@link Document} to a {@link ProductDimensionsData} by calling
	 * {@link ProductDimensionDataPopulator}
	 *
	 * @param document
	 *           {@link Document} the solr document which is used to fetch the variant information
	 *
	 * @return productDimensions of type {@link ProductDimensionsData} which is populated using the document
	 *         {@link Document}
	 */
	protected ProductDimensionsData populateProductDimensions(final Document document)
	{
		final ProductDimensionsData productDimensions = new ProductDimensionsData();
		productDimensionDataPopulator.populate(document, productDimensions);
		return productDimensions;
	}

}
