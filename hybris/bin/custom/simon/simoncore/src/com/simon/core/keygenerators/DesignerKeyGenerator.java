package com.simon.core.keygenerators;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;


/**
 * DesignerKeyGenerator generates Designer Code for given raw name
 */
public class DesignerKeyGenerator implements KeyGenerator
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object generate()
	{
		throw new UnsupportedOperationException("Not supported, please call generateFor().");
	}

	/**
	 * Converts Raw Name Code to lower case and replaces all nbsp by hyphen and Append PD as prefix.
	 *
	 * @param arg0
	 *           the Raw Name
	 * @return the object
	 */
	@Override
	public Object generateFor(final Object arg0)
	{
		return "PD" + "-" + ((String) arg0).toLowerCase().replaceAll("\\s+", "-");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		throw new UnsupportedOperationException("A reset of DesignerKeyGenerator is not supported.");

	}

}
