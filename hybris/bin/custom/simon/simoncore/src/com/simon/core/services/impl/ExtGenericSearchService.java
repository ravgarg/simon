/**
 *
 */
package com.simon.core.services.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.GenericCondition;
import de.hybris.platform.core.GenericConditionList;
import de.hybris.platform.core.GenericFieldComparisonCondition;
import de.hybris.platform.core.GenericLiteralCondition;
import de.hybris.platform.core.GenericQuery;
import de.hybris.platform.core.GenericSearchField;
import de.hybris.platform.core.GenericSubQueryCondition;
import de.hybris.platform.core.GenericValueCondition;
import de.hybris.platform.core.Operator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.core.model.type.ViewTypeModel;
import de.hybris.platform.genericsearch.GenericSearchQuery;
import de.hybris.platform.genericsearch.impl.DefaultGenericSearchService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;




/**
 * The Class ExtGenericSearchService is used to modify OOTB search behavior for {@link GenericVariantProductModel} on
 * {@link CategoryModel} .
 *
 */
@SuppressWarnings(
{ "rawtypes", "deprecation", "unchecked", "squid:CallToDeprecatedMethod", "squid:S1188" })
public class ExtGenericSearchService extends DefaultGenericSearchService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultGenericSearchService.class);
	private static final long serialVersionUID = 1L;
	private transient SessionService sessionService;
	private transient FlexibleSearchService flexibleSearchService;
	private transient TypeService typeService;

	/**
	 * Overridden method to achieve search of GenericVariantProduct on Category. We are replacing OOTB search query by
	 * our custom query after checking the type and condition.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public SearchResult search(final GenericSearchQuery genericSearchQuery)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("genericSearchQuery", genericSearchQuery);
		ServicesUtil.validateParameterNotNullStandardMessage("genericSearchQuery.getQuery()", genericSearchQuery.getQuery());
		return sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Object execute()
			{
				final ComposedTypeModel initialType = typeService
						.getComposedTypeForCode(genericSearchQuery.getQuery().getInitialTypeCode());

				if (initialType instanceof ViewTypeModel)
				{
					throw new IllegalArgumentException("searching for ViewType is currently unimplemented");
				}
				else
				{
					final HashMap values = new HashMap();
					String query = genericSearchQuery.getQuery().toFlexibleSearch(values);
					final boolean categorySearch = isGenericProductSearchOnCategory(genericSearchQuery, query);
					if (categorySearch)
					{
						query = prepareNewQuery(genericSearchQuery, values);
					}
					LOG.debug("QUERY: " + query);
					LOG.debug("VALUES: " + values);
					final FlexibleSearchQuery fsq = new FlexibleSearchQuery(query);
					fsq.setNeedTotal(genericSearchQuery.isNeedTotal());
					fsq.setResultClassList(genericSearchQuery.getQuery().getResultClasses());
					fsq.addQueryParameters(values);
					fsq.setFailOnUnknownFields(genericSearchQuery.isFailOnUnknownFields());
					fsq.setLocale(genericSearchQuery.getLocale());
					fsq.setLanguage(genericSearchQuery.getLanguage());
					fsq.setCatalogVersions(genericSearchQuery.getCatalogVersions());
					fsq.setCount(genericSearchQuery.getCount());
					fsq.setStart(genericSearchQuery.getStart());
					return flexibleSearchService.search(fsq);
				}
			}
		});
	}

	/**
	 * This method is used to prepare a new query for Product if request is to fetch GenricVariantProduct on the basis of
	 * category.
	 *
	 * @param genericSearchQuery
	 * @param values
	 */
	private String prepareNewQuery(final GenericSearchQuery genericSearchQuery, final HashMap values)
	{
		final GenericQuery genericQuery = new GenericQuery(genericSearchQuery.getQuery().getInitialTypeCode());
		genericQuery.addCondition(updateConditions(genericSearchQuery.getQuery().getCondition()));
		final GenericSearchQuery newGenericSearchQuery = new GenericSearchQuery(genericQuery);
		newGenericSearchQuery.setCount(genericSearchQuery.getCount());
		newGenericSearchQuery.setNeedTotal(genericSearchQuery.isNeedTotal());
		newGenericSearchQuery.setCatalogVersions(genericSearchQuery.getCatalogVersions());
		newGenericSearchQuery.setDisableCaching(genericSearchQuery.isDisableCaching());
		newGenericSearchQuery.setDisableSearchRestrictions(genericSearchQuery.isDisableSearchRestrictions());
		newGenericSearchQuery.setDisableSpecificDbLimitSupport(genericSearchQuery.isDisableSpecificDbLimitSupport());
		newGenericSearchQuery.setFailOnUnknownFields(genericSearchQuery.isFailOnUnknownFields());
		newGenericSearchQuery.setLanguage(genericSearchQuery.getLanguage());
		newGenericSearchQuery.setLocale(genericSearchQuery.getLocale());
		newGenericSearchQuery.setSessionSearchRestrictions(genericSearchQuery.getSessionSearchRestrictions());
		return newGenericSearchQuery.getQuery().toFlexibleSearch(values);
	}

	/**
	 * Update conditions in query after fetching that .
	 *
	 * @param condition
	 *           the condition
	 * @return the generic condition
	 */
	private GenericCondition updateConditions(final GenericCondition condition)
	{
		final LinkedList finalConditions = Lists.newLinkedList();
		if (null != condition)
		{
			final List<GenericCondition> genericConditions = ((GenericConditionList) condition).getConditionList();
			for (final GenericCondition genericCondition : genericConditions)
			{
				if (genericCondition instanceof GenericSubQueryCondition
						&& ((GenericSubQueryCondition) genericCondition).getSubQuery() != null && "CategoryProductRelation"
								.equals(((GenericSubQueryCondition) genericCondition).getSubQuery().getInitialTypeCode()))
				{
					final GenericSubQueryCondition oldCondition = (GenericSubQueryCondition) genericCondition;
					finalConditions.add(GenericCondition.createSubQueryCondition(getSearchField(oldCondition.getField()), Operator.IN,
							getGenericSubQuery(oldCondition)));

				}
				else if (genericCondition instanceof GenericSubQueryCondition)
				{
					final GenericSubQueryCondition oldCondition = (GenericSubQueryCondition) genericCondition;
					finalConditions.add(GenericCondition.createSubQueryCondition(oldCondition.getField(), oldCondition.getOperator(),
							oldCondition.getSubQuery()));
				}
				else if (genericCondition instanceof GenericValueCondition)
				{
					final GenericValueCondition oldCondition = (GenericValueCondition) genericCondition;
					finalConditions
							.add(GenericCondition.createConditionForValueComparison(oldCondition.getField(), oldCondition.getOperator(),
									oldCondition.getValue(), oldCondition.getValueQualifier(), oldCondition.isCaseInsensitive()));
				}
				else if (genericCondition instanceof GenericFieldComparisonCondition)
				{
					final GenericFieldComparisonCondition oldCondition = (GenericFieldComparisonCondition) genericCondition;
					finalConditions.add(GenericCondition.createConditionForFieldComparison(oldCondition.getField(),
							oldCondition.getOperator(), oldCondition.getComparisonField(), oldCondition.isCaseInsensitive()));
				}
				else if (genericCondition instanceof GenericLiteralCondition)
				{
					final GenericLiteralCondition oldCondition = (GenericLiteralCondition) genericCondition;
					finalConditions.add(
							GenericCondition.createConditionForLiteralComparison(oldCondition.getField(), oldCondition.getOperator()));
				}
				else if (genericCondition instanceof GenericConditionList)
				{
					finalConditions.add(updateConditions(genericCondition));
				}

			}
		}
		if (condition.getOperator().equals(Operator.AND))
		{
			return GenericConditionList.and(finalConditions);
		}
		return GenericConditionList.or(finalConditions);
	}


	/**
	 * Gets the generic sub query.
	 *
	 * @param genericCondition
	 *           the generic condition
	 * @return the generic sub query
	 */
	private GenericQuery getGenericSubQuery(final GenericSubQueryCondition genericCondition)
	{
		final GenericQuery query = new GenericQuery(ProductModel._TYPECODE);
		final GenericSearchField pkField = new GenericSearchField(ProductModel._TYPECODE, ProductModel.PK);
		final GenericCondition condition = GenericCondition.createSubQueryCondition(pkField, genericCondition.getOperator(),
				genericCondition.getSubQuery());
		query.addCondition(condition);
		return query;
	}

	/**
	 * Get Search field instance by using the old field of previous query.
	 *
	 * @param oldField
	 * @return
	 */
	private GenericSearchField getSearchField(final GenericSearchField oldField)
	{
		GenericSearchField newField;
		if (null == oldField)
		{
			newField = null;
		}
		else
		{
			newField = new GenericSearchField(oldField.getTypeCode(), GenericVariantProductModel.BASEPRODUCT);
			newField.setFieldTypes(oldField.getFieldTypes());
			newField.setLanguagePK(oldField.getLanguagePK());
		}
		return newField;
	}


	/**
	 * This method is used to check whether this search request is for GenericVariantProduct on category.
	 *
	 * @param genericSearchQuery
	 * @param query
	 * @param categorySearch
	 * @return boolean
	 */
	private boolean isGenericProductSearchOnCategory(final GenericSearchQuery genericSearchQuery, final String query)
	{
		return ("GenericVariantProduct".equals(genericSearchQuery.getQuery().getInitialTypeCode())
				&& query.contains("CategoryProductRelation"));
	}

	/**
	 * Sets the session service.
	 *
	 * @param sessionService
	 *           the sessionService to set
	 */
	@Override
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	/**
	 * Sets the flexible search service.
	 *
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	@Override
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * Sets the type service.
	 *
	 * @param typeService
	 *           the typeService to set
	 */
	@Override
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}
}
