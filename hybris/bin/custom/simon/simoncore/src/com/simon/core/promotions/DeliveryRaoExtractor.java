
package com.simon.core.promotions;

import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DeliveryModeRAO;
import de.hybris.platform.ruleengineservices.rao.providers.RAOFactsExtractor;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.base.Preconditions;
import com.simon.promotion.rao.RetailerRAO;


/**
 * The instances of objects that are needed by the rule engine to evaluate a condition needs to be exposed in the rule
 * context. This class identifies the distinct {@link DeliveryModeRAO} that will be exposed in rule context.
 *
 */

public class DeliveryRaoExtractor implements RAOFactsExtractor
{
	private static final String EXPAND_DELIVERY_RAO = "EXPAND_DELIVERY_RAO";

	/**
	 * Expands {@link DeliveryModeRAO} in rule context.
	 *
	 * @param fact
	 *           Instance of {@link CartRAO}
	 * @return distinct set of {@link DeliveryModeRAO}
	 */

	public Set<DeliveryModeRAO> expandFact(final Object fact)
	{
		Preconditions.checkArgument(fact instanceof CartRAO, "CartRAO type is expected here");
		final HashSet<DeliveryModeRAO> facts = new HashSet<>();
		final CartRAO cartRAO = (CartRAO) fact;
		if (CollectionUtils.isNotEmpty(cartRAO.getRetailers()))
		{
			for (final Object delRal : cartRAO.getRetailers())
			{
				facts.add(((RetailerRAO) delRal).getDeliveryMode());
			}
		}

		return facts;
	}


	/**
	 * Gets the triggering option.
	 *
	 * @return the triggering option
	 */

	@Override
	public String getTriggeringOption()
	{
		return EXPAND_DELIVERY_RAO;
	}


	/**
	 * Checks if is default.
	 *
	 * @return true, if is default
	 */

	@Override
	public boolean isDefault()
	{
		return true;
	}


	/**
	 * Checks if is min option.
	 *
	 * @return true, if is min option
	 */
	@Override
	public boolean isMinOption()
	{
		return false;
	}

}

