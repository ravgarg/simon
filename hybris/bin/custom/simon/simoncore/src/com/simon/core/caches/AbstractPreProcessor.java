package com.simon.core.caches;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;


/**
 * AbstractPreProcessor is a cache based pre-processor. This maintains a cache map {@link #cacheMap} for every item that
 * is pre processed through this. Cache helps in re preprocessing for the same raw value.
 *
 * @param <T>
 *           any model that extends {@link ItemModel}
 */
public abstract class AbstractPreProcessor<T extends ItemModel> implements ProductFileImportPreProcessor
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPreProcessor.class);

	protected final Map<String, T> cacheMap = new ConcurrentHashMap<>();

	@Autowired
	protected ModelService modelService;

	private ValueType valueType = ValueType.SINGLE;

	/**
	 * Creates the key. Returns unique for a given <code>rawValue</code>. This key is checked again cache for pre
	 * processing of every line item to avoid re-preprocessing.
	 *
	 * @param attribute
	 *           the attribute
	 * @param rawValue
	 *           the raw value
	 * @param rawValues
	 *           the raw values
	 * @return the string
	 */
	abstract String createKey(final String attribute, final String rawValue, final Map<String, String> rawValues,
			final ProductImportFileContextData context);

	/**
	 * Creates the value. Returns T Model for given <code>rawValue</code>
	 *
	 * @param attribute
	 *           the attribute
	 * @param rawValue
	 *           the raw value
	 * @param rawValues
	 *           the raw values
	 * @return the t
	 */
	abstract T createValue(final String attribute, final String rawValue, final Map<String, String> rawValues,
			final ProductImportFileContextData context);

	/**
	 * Extract raw value.
	 *
	 * @param attribute
	 *           the attribute
	 * @param rawValues
	 *           the raw values
	 * @return the string
	 */
	protected String extractRawValue(final String attribute, final Map<String, String> rawValues)
	{
		return rawValues.get(attribute);
	}

	/**
	 * Cache based pre-processing. Cache is build in {@link #cacheMap} for every rawvalue that is pre-processed. <br>
	 * First raw value for given attribute is extracted using {@link #extractRawValue(String, Map)}<br>
	 * Unique key for this value is created using {@link #createKey(String, String, Map)} and checked in existing map<br>
	 * If given key does not exist new value is created for this key using {@link #createValue(String, String, Map)} and
	 * is ingested into the map.
	 * <p>
	 * Value type can be of 2 types determined by {@link ValueType}. For raw values of type {@link ValueType#SINGLE} are
	 * directly processed multiple type values {@link ValueType#MULTIPLE} are split and done sent for processing.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void preProcess(final String attribute, final Map<String, String> rawValues, final ProductImportFileContextData context)
	{
		final String rawValue = extractRawValue(attribute, rawValues);
		if (StringUtils.isNotEmpty(rawValue))
		{
			if (getValueType() == ValueType.SINGLE)
			{
				buildCache(attribute, rawValue, rawValues, context);
			}
			else
			{
				for (final String subRawValue : StringUtils.split(rawValue, "|"))
				{
					buildCache(attribute, subRawValue, rawValues, context);
				}
			}
		}
	}

	/**
	 * Builds the cache. First key is created using {@link #createKey(String, String, Map)} and checked against map. If
	 * key is present then no processing is done else value is created using {@link #createValue(String, String, Map)}
	 * and is ingested into the map.
	 *
	 * @param attribute
	 *           the attribute
	 * @param rawValue
	 *           the raw value
	 * @param rawValues
	 *           the raw values
	 */
	private void buildCache(final String attribute, final String rawValue, final Map<String, String> rawValues,
			final ProductImportFileContextData context)
	{
		final String key = createKey(attribute, rawValue, rawValues, context);
		if (StringUtils.isNotEmpty(key))
		{
			T t = cacheMap.get(key);
			if (t != null)
			{
				LOGGER.trace("Found Model Having Key : {} In Cache", key);
			}
			else
			{
				LOGGER.debug("Not Found Model Having Key {} In Cache. Creating new model", key);
				t = createValue(attribute, rawValue, rawValues, context);
				cacheMap.put(key, t);
			}
		}
		else
		{
			LOGGER.warn("Unable To Create Key For Attribute : {} Raw Value : {}", attribute, rawValue);
		}
	}

	/**
	 * Cleanup.
	 */
	public void cleanup()
	{
		cacheMap.clear();
	}

	/**
	 * Sets the value type.
	 *
	 * @param valueType
	 *           the new value type
	 */
	public void setValueType(final ValueType valueType)
	{
		this.valueType = valueType;
	}

	/**
	 * Gets the value type.
	 *
	 * @return the value type
	 */
	public ValueType getValueType()
	{
		return valueType;
	}
}
