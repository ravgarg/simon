package com.simon.core.provider.impl;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;


/**
 * This class helps in indexing details or VariantCategories into InputDocument
 */
public class BaseProductEnumResolver extends AbstractValueResolver<GenericVariantProductModel, ProductModel, Object>
{

	/**
	 * Attribute which maps to "attribute" ValueProviderParameter in impex. Fetches the attribute name to index.
	 */
	public static final String AGE_GROUP = "ageGroup";
	public static final String GENDER = "gender";
	public static final String ATTRIBUTE_PARAM = "attribute";
	/**
	 * The default value of "attribute", in case value is not fetched from model
	 *
	 */
	public static final String ATTRIBUTE_PARAM_DEFAULT_VALUE = null;
	/**
	 * Attribute which check whether indexed property is optional or not
	 */
	public static final String OPTIONAL_PARAM = "optional";
	/**
	 * The default value of optional attribute
	 */
	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	/**
	 * ModelService to fetch attribute values
	 */
	@Resource
	private ModelService modelService;
	/**
	 * TypeService to check whether a particular attribute belong to type specified.
	 */
	@Resource
	private TypeService typeService;

	/**
	 * This method fetches VariantValueCategories from ResolverContext, if not null, gets the required value and adds to
	 * InputDocument
	 *
	 * @throws FieldValueProviderException,
	 *            Exception is thrown if there is any problem while adding data to index or expected value is not
	 *            fetched, given the attribute has optional set to false.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<ProductModel, Object> resolverContext) throws FieldValueProviderException
	{
		final ProductModel product = resolverContext.getData();
		if (null != product)
		{
			final String attributeName = indexedProperty.getName();
			final String attributeValue = getAttributeValue(product, attributeName);
			document.addField(indexedProperty, attributeValue, resolverContext.getFieldQualifier());
		}
		else
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}



	/**
	 * Loads the data from {@link GenericVariantProductModel} into ResolverContext
	 */
	@Override
	protected ProductModel loadData(final IndexerBatchContext batchContext, final Collection<IndexedProperty> indexedProperties,
			final GenericVariantProductModel model) throws FieldValueProviderException
	{
		return model.getBaseProduct();

	}

	/**
	 * Gets the attribute value the provided attribute name.
	 *
	 * @param product
	 *           of type {@link ProductModel} used to fetch the attribute value
	 * @param attributeName
	 *           of type string defines the attribute name in ProductModel
	 * @param propertyName
	 *           the property name
	 * @return the attribute value of the attribute in {@link ProductModel}
	 */
	private String getAttributeValue(final ProductModel product, final String attributeName)
	{
		if (AGE_GROUP.equals(attributeName))
		{
			return null != product.getAgeGroup() ? product.getAgeGroup().getCode() : StringUtils.EMPTY;
		}
		else if (GENDER.equals(attributeName))
		{
			return null != product.getGender() ? product.getGender().getCode() : StringUtils.EMPTY;
		}
		return null;
	}


}
