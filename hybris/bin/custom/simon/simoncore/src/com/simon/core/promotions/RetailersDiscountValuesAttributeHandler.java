package com.simon.core.promotions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.simon.core.util.CommonUtils;


/**
 * The Class is {@link DynamicAttributeHandler} for retailersDiscountValuesInternal of {@link AbstractOrderModel}}.
 */
public class RetailersDiscountValuesAttributeHandler
		implements DynamicAttributeHandler<Map<String, List<DiscountValue>>, AbstractOrderModel>
{

	/**
	 * Gets the Map of DiscountValue for each retailer which represent retailer discounts from
	 * retailersDiscountValuesInternal after converting its value from string to collection.
	 *
	 * @param model
	 *           the model
	 * @return the list of retailer discounts applicable on cart
	 */
	@Override
	public Map<String, List<DiscountValue>> get(final AbstractOrderModel model)
	{
		final Map<String, String> values = model.getSubbagDiscountValuesInternal();
		final Map<String, List<DiscountValue>> retaileDiscounts = new HashMap<>();
		if (MapUtils.isNotEmpty(values))
		{
			values.keySet().stream().forEach(retailer -> retaileDiscounts.put(retailer,
					new ArrayList<>(DiscountValue.parseDiscountValueCollection(values.get(retailer)))));
		}
		return retaileDiscounts;
	}

	/**
	 * Converts the List of DiscountValue for each retailer to string and sets into retailersDiscountValuesInternal
	 *
	 * @param model
	 *           the model
	 * @param discountValues
	 *           the discount values
	 */
	@Override
	public void set(final AbstractOrderModel model, final Map<String, List<DiscountValue>> retailerDiscountValue)
	{
		if (MapUtils.isNotEmpty(retailerDiscountValue))
		{
			retailerDiscountValue.keySet().stream()
					.forEach(retailer -> setRetailerDiscount(retailer, retailerDiscountValue.get(retailer), model));
		}
		else
		{
			model.setSubbagDiscountValuesInternal(new HashMap<>());
		}
	}

	private void setRetailerDiscount(final String retailer, final List<DiscountValue> retailerDiscountValue,
			final AbstractOrderModel model)
	{
		final String newOne = DiscountValue.toString(retailerDiscountValue);
		final Map<String, String> retailerDiscountValueMap = CommonUtils.createMapFrom(model.getSubbagDiscountValuesInternal());
		retailerDiscountValueMap.put(retailer, newOne);
		model.setSubbagDiscountValuesInternal(retailerDiscountValueMap);
		model.setCalculated(Boolean.FALSE);
	}


}
