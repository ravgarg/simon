/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.core.services.order;

import de.hybris.platform.core.model.order.CartModel;


/**
 * This is a contract declares the method used for setting retailer specific delivery modes
 *
 */
public interface RetailerSubbagDeliveryModeStrategy
{
	/**
	 * This methods associates retailer sub-bag with the cart
	 *
	 * @param cartModel
	 * @param retailerId
	 * @param deliveryMode
	 * @param deliveryModeModel
	 * @return
	 */
	boolean setRetailerSubbagDeliveryMode(final CartModel cartModel, String retailerId, String deliveryMode);
}
