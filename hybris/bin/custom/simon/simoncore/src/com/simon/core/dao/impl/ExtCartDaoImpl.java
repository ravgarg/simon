package com.simon.core.dao.impl;

import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtCartDao;


/**
 * This DAO class is used to retrieve cart model
 */
public class ExtCartDaoImpl extends AbstractItemDao implements ExtCartDao
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModel getCartByCartId(final String cartId)
	{
		ServicesUtil.validateParameterNotNull(cartId, "Cart Id cannot be null");
		CartModel cart = null;

		final FlexibleSearchQuery fQuery = getFlexibleSearchQuery(SimonFlexiConstants.TWOTAP_CART_BY_CART_ID_QUERY);
		fQuery.addQueryParameter("cartId", cartId);

		final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
		if (CollectionUtils.isNotEmpty(results))
		{
			cart = (CartModel) results.get(0);
		}
		return cart;
	}

	protected FlexibleSearchQuery getFlexibleSearchQuery(final String code)
	{
		return new FlexibleSearchQuery(code);
	}

}
