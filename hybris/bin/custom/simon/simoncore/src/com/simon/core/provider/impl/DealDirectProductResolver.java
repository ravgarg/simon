package com.simon.core.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;


/**
 * Resolver to index Multi valued deal code which have direct product relation with deal.
 */
public class DealDirectProductResolver
		extends AbstractValueResolver<GenericVariantProductModel, Map<String, List<String>>, Object>
{

	@Resource
	private ExtDealService extDealService;



	/**
	 * Load data into context. Deals are fetched from dealService.
	 */
	@Override
	protected Map<String, List<String>> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel product)
			throws FieldValueProviderException
	{
		final List<DealModel> dealList = extDealService.findListOfDeals();
		final Map<String, List<String>> productDeal = new HashMap<>();
		for (final DealModel dealModel : dealList)
		{
			if (CollectionUtils.isNotEmpty(dealModel.getProducts()))
			{
				setProductAndAssociatedDealInMap(productDeal, dealModel);

			}

		}
		return productDeal;
	}


	/**
	 * This method get all deals associated with a product and put in map with key as product code.
	 * 
	 * @param productDeal
	 * @param dealModel
	 */
	private void setProductAndAssociatedDealInMap(final Map<String, List<String>> productDeal, final DealModel dealModel)
	{
		for (final ProductModel productModel : dealModel.getProducts())
		{

			if (productDeal.containsKey(productModel.getCode()))
			{
				productDeal.get(productModel.getCode()).add(dealModel.getCode());
			}
			else
			{
				final List<String> listOfDealCode = new ArrayList<>();
				listOfDealCode.add(dealModel.getCode());
				productDeal.put(productModel.getCode(), listOfDealCode);
			}

		}
	}



	/**
	 * Fetches deals from Context and indexed the deal Codes
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Map<String, List<String>>, Object> resolverContext) throws FieldValueProviderException
	{
		final Map<String, List<String>> productDealMap = resolverContext.getData();
		final List<String> dealCodes = productDealMap.get(model.getCode());
		filterAndAddFieldValues(document, batchContext, indexedProperty, dealCodes, resolverContext.getFieldQualifier());

	}


}
