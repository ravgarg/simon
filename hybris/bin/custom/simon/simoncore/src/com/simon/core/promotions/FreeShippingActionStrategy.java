/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.simon.core.promotions;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.daos.DeliveryModeDao;
import de.hybris.platform.promotionengineservices.action.impl.AbstractRuleActionStrategy;
import de.hybris.platform.promotionengineservices.action.impl.DefaultShippingActionStrategy;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderChangeDeliveryModeActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.ShipmentRAO;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This Strategy is used for giving free shipping to retailers sub-bag
 */
public class FreeShippingActionStrategy extends AbstractRuleActionStrategy<RuleBasedOrderChangeDeliveryModeActionModel>
{
	private static final double FREE_SHIPPING = 0.0d;
	private static final Logger LOG = LoggerFactory.getLogger(DefaultShippingActionStrategy.class);
	@Resource
	private DeliveryModeDao deliveryModeDao;

	public List<PromotionResultModel> apply(final AbstractRuleActionRAO action)
	{
		if (!(action instanceof ShipmentRAO))
		{
			LOG.error("cannot apply {}, action is not of type ShipmentRAO, but {}", this.getClass().getSimpleName(), action);
			return Collections.emptyList();
		}
		else
		{
			return applyPromotion(action);
		}
	}

	public void undo(final ItemModel item)
	{
		if (item instanceof RuleBasedOrderChangeDeliveryModeActionModel)
		{
			this.handleUndoActionMetadata((RuleBasedOrderChangeDeliveryModeActionModel) item);
			final RuleBasedOrderChangeDeliveryModeActionModel action = (RuleBasedOrderChangeDeliveryModeActionModel) item;
			final AbstractOrderModel order = action.getPromotionResult().getOrder();
			final String retailerId = action.getRetailer();
			final Map<String, DeliveryModeModel> retailersDeliveryModes = new HashMap<>();
			retailersDeliveryModes.putAll(order.getRetailersDeliveryModes());
			final Map<String, Double> retailersDeliveryCosts = new HashMap<>();
			retailersDeliveryCosts.putAll(order.getRetailersDeliveryCost());
			if (null != action.getReplacedDeliveryCost() && null != action.getReplacedDeliveryMode())
			{
				retailersDeliveryCosts.put(retailerId, order.getRetailersDeliveryCost().get(retailerId));
				retailersDeliveryModes.put(retailerId, order.getRetailersDeliveryModes().get(retailerId));
			}
			order.setRetailersDeliveryCost(retailersDeliveryCosts);
			order.setRetailersDeliveryModes(retailersDeliveryModes);
			performUndoInternal(action);
			this.getModelService().save(order);
		}

	}


	protected AbstractOrderModel performUndoInternal(final RuleBasedOrderChangeDeliveryModeActionModel action)
	{
		return super.undoInternal(action);
	}


	protected RuleBasedOrderChangeDeliveryModeActionModel getCreatedPromotionAction(final PromotionResultModel promoResult,
			final AbstractRuleActionRAO action)
	{
		return super.createPromotionAction(promoResult, action);
	}

	protected DeliveryModeModel getDeliveryModeForCode(final String code)
	{
		ServicesUtil.validateParameterNotNull(code, "Parameter code cannot be null");
		final List<DeliveryModeModel> deliveryModes = getDeliveryModeDao().findDeliveryModesByCode(code);
		return CollectionUtils.isNotEmpty(deliveryModes) ? (DeliveryModeModel) deliveryModes.get(0) : null;
	}

	protected DeliveryModeDao getDeliveryModeDao()
	{
		return this.deliveryModeDao;
	}

	/**
	 * Method to apply promotion and save it in DB using modelService.
	 *
	 * @param action
	 *
	 * @return listOfPromotionResultModel
	 */
	private List<PromotionResultModel> applyPromotion(final AbstractRuleActionRAO action)
	{
		final ShipmentRAO changeDeliveryMethodAction = (ShipmentRAO) action;
		if (!(changeDeliveryMethodAction.getAppliedToObject() instanceof CartRAO))
		{
			LOG.error("cannot apply {}, appliedToObject is not of type CartRAO, but {}", this.getClass().getSimpleName(),
					action.getAppliedToObject());
			return Collections.emptyList();
		}
		else
		{
			final PromotionResultModel promoResult = this.getPromotionActionService().createPromotionResult(action);
			if (promoResult == null)
			{
				LOG.error("cannot apply {}, promotionResult could not be created.", this.getClass().getSimpleName());
				return Collections.emptyList();
			}
			else
			{
				return applyPromotion(action, promoResult);
			}
		}
	}

	/**
	 * Method to apply promotion and save it in DB using modelService.
	 *
	 * @param action
	 * @param promoResult
	 *
	 * @return listOfPromotionResultModel
	 */
	private List<PromotionResultModel> applyPromotion(final AbstractRuleActionRAO action, final PromotionResultModel promoResult)
	{
		final AbstractOrderModel order = promoResult.getOrder();
		if (Objects.isNull(order))
		{
			LOG.error("cannot apply {}, order or cart not found: {}", this.getClass().getSimpleName(), order);
			if (this.getModelService().isNew(promoResult))
			{
				this.getModelService().detach(promoResult);
			}

			return Collections.emptyList();
		}
		else
		{
			final ShipmentRAO shipmentRAO = (ShipmentRAO) action;
			final DeliveryModeModel shipmentModel = getDeliveryModeForCode(shipmentRAO.getMode().getCode());
			if (shipmentModel == null)
			{
				LOG.error("Delivery Mode for code {} not found!", shipmentRAO.getMode());
				return Collections.emptyList();
			}
			else
			{
				return applyPromotion(action, promoResult, order, shipmentRAO, shipmentModel);
			}
		}
	}

	/**
	 * Method to apply promotion & save it in DB.
	 *
	 * @param action
	 * @param promoResult
	 * @param order
	 * @param shipmentRAO
	 * @param shipmentModel
	 *
	 * @return listOfPromotionResultModel
	 */
	private List<PromotionResultModel> applyPromotion(final AbstractRuleActionRAO action, final PromotionResultModel promoResult,
			final AbstractOrderModel order, final ShipmentRAO shipmentRAO, final DeliveryModeModel shipmentModel)
	{
		final String retailerId = shipmentRAO.getRetailer().getRetailerId();
		final Map<String, DeliveryModeModel> retailersDeliveryModes = new HashMap<>();
		retailersDeliveryModes.putAll(order.getRetailersDeliveryModes());
		final Map<String, Double> retailersDeliveryCosts = new HashMap<>();
		retailersDeliveryCosts.putAll(order.getRetailersDeliveryCost());
		final DeliveryModeModel shipmentModelToReplace = retailersDeliveryModes.get(retailerId);
		final Double deliveryCostToReplace = retailersDeliveryCosts.get(retailerId);
		retailersDeliveryCosts.put(retailerId, FREE_SHIPPING);
		retailersDeliveryModes.put(retailerId, shipmentModel);
		order.setRetailersDeliveryCost(retailersDeliveryCosts);
		order.setRetailersDeliveryModes(retailersDeliveryModes);
		promoResult.setRetailerID(retailerId);
		final RuleBasedOrderChangeDeliveryModeActionModel actionModel = getCreatedPromotionAction(promoResult, action);
		this.handleActionMetadata(action, actionModel);
		actionModel.setRetailer(retailerId);
		actionModel.setDeliveryMode(shipmentModel);
		actionModel.setDeliveryCost(BigDecimal.valueOf(FREE_SHIPPING));
		if (null != shipmentModelToReplace)
		{
			actionModel.setReplacedDeliveryMode(shipmentModelToReplace);
		}
		if (null != deliveryCostToReplace)
		{
			actionModel.setReplacedDeliveryCost(BigDecimal.valueOf(deliveryCostToReplace.doubleValue()));
		}
		this.getModelService().saveAll(new Object[]
		{ promoResult, actionModel, order });
		return Collections.singletonList(promoResult);
	}
}
