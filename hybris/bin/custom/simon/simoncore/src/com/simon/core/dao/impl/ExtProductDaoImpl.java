package com.simon.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtProductDao;
import com.simon.core.enums.PriceType;


/**
 * ExtProductDaoImpl extend {@link DefaultProductDao} provides additional db based implementations of operations defined
 * in {@link ExtProductDao}.
 */
public class ExtProductDaoImpl extends DefaultProductDao implements ExtProductDao
{


	/**
	 * Instantiates a new ext product dao impl.
	 *
	 * @param typecode
	 *           the typecode
	 */
	public ExtProductDaoImpl(final String typecode)
	{
		super(typecode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> findProductsByCodes(final CatalogVersionModel catalogVersion, final Set<String> codes)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		validateParameterNotNullStandardMessage("Product Codes", codes);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRODUCTS_FOR_CODES);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.addQueryParameter("codes", codes);

		query.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> findCheckedProductsHavingPriceRow(final PriceType priceType,
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("priceType", priceType);
		validateParameterNotNullStandardMessage("catalogVersion", catalogVersion);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_CHECKED_GVP_HAVING_PRICEROW);
		query.addQueryParameter("approvalStatus", ArticleApprovalStatus.CHECK);
		query.addQueryParameter("priceType", priceType);
		query.addQueryParameter("catalogVersion", catalogVersion);

		final SearchResult<GenericVariantProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> findUnApprovedProductsHavingImageAndMerchandizingCategory(
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				SimonFlexiConstants.SELECT_UNAPPROVED_GVP_HAVING_IMAGE_AND_MERCH_CATEGORY);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.setResultClassList(Collections.singletonList(GenericVariantProductModel.class));
		final SearchResult<GenericVariantProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> findProductsByCatalogVersion(final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRODUCTS_FOR_CATALOGVERSION);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> findProductsWithoutMerchandizingCategory(final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRODUCTS_MISSING_MERCH_CATEGORY);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> findProductByCodes(final Set<String> codes)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRODUCT_CODES);
		query.addQueryParameter("codes", codes);

		query.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> findProductsByCatalogVersion(final CatalogVersionModel catalogVersion,
			final ArticleApprovalStatus approvalStatus)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		validateParameterNotNullStandardMessage("Approval Status", approvalStatus);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				SimonFlexiConstants.SELECT_PRODUCTS_FOR_CATALOGVERSION_APPROVAL_STATUS);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.addQueryParameter("approvalStatus", approvalStatus);
		query.setResultClassList(Collections.singletonList(ProductModel.class));
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> findProductsWithZeroStock(final CatalogVersionModel catalogVersion,
			final Date lastSuccessJobRun)
	{
		validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRODUCTS_WITH_ZERO_STOCK);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.addQueryParameter("lastSuccessJobRun", lastSuccessJobRun);
		query.setResultClassList(Collections.singletonList(GenericVariantProductModel.class));
		final SearchResult<GenericVariantProductModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}
}
