package com.simon.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.CategoryUpdateType;
import com.simon.core.model.CategoryUpdateCronJobModel;
import com.simon.core.services.ExtProductService;
import com.simon.core.util.AdditionalAttributesUtils;


/**
 * CategoryUpdateJob updates Merchandizing Categories on Product as per the additional attributes present in their
 * Variant Products.<br>
 * Two category update types are supported one in which all base products are updated and in another one only base
 * products that have missing merchandizing categories are updated.
 */
public class CategoryUpdateJob extends AbstractJobPerformable<CategoryUpdateCronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryUpdateJob.class);

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private ExtProductService extProductService;

	/**
	 * Updates Merchandizing categories on Product's as per the additional attributes present in their Variant Products.
	 */
	@Override
	public PerformResult perform(final CategoryUpdateCronJobModel categoryUpdateCronJob)
	{
		PerformResult performResult;
		List<ProductModel> baseProducts = null;

		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);

		if (categoryUpdateCronJob.getCategoryUpdateType() == CategoryUpdateType.MISSING_CATEGORIES)
		{
			baseProducts = extProductService.getProductsWithoutMerchandizingCategory(catalogVersion);
		}
		else if (categoryUpdateCronJob.getCategoryUpdateType() == CategoryUpdateType.FULL_IMPORT)
		{
			baseProducts = extProductService.getProductsForCatalogVersion(catalogVersion);
		}
		else
		{
			LOGGER.debug("No Updated Type Selected. Finishing Job");
		}

		if (CollectionUtils.isNotEmpty(baseProducts))
		{
			baseProducts.forEach(updateCategories.andThen(saveModels));
		}

		performResult = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		return performResult;
	}

	private final Consumer<ProductModel> updateCategories = product -> {
		final Set<CategoryModel> allMerchCategories = new HashSet<>();
		final Set<CategoryModel> matchedMerchCategories = new HashSet<>();
		allMerchCategories.addAll(CollectionUtils.subtract(product.getSupercategories(), product.getAutomatedGeneratedCategories()));
		product.getVariants().forEach(p -> matchedMerchCategories
				.addAll(AdditionalAttributesUtils.getMatchingMerchandizingCategories((GenericVariantProductModel) p)));
		product.setAutomatedGeneratedCategories(matchedMerchCategories.stream().collect(Collectors.toList()));
		allMerchCategories.addAll(matchedMerchCategories);
		product.setSupercategories(allMerchCategories);
	};

	private final Consumer<ProductModel> saveModels = product -> {
		try
		{
			LOGGER.debug("Updating Product : {}", product.getCode());
			modelService.save(product);
		}
		catch (final ModelSavingException modelSavingException)
		{
			LOGGER.error("Error Occurred In Saving Model" + product.getCode(), modelSavingException);
		}
	};
}
