package com.simon.core.cms.component.caching;

import de.hybris.platform.acceleratorcms.component.cache.impl.DefaultCmsCacheService;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;



/**
 * This class is extension of {@link DefaultCmsCacheService} to cache particular component in cms cache region.
 */
public class ExtCMSCacheService extends DefaultCmsCacheService
{
	/**
	 * {@inheritDoc}}
	 */
	@Override
	public boolean useCache(final HttpServletRequest request, final AbstractCMSComponentModel component)
	{
		return isRequestAndComponentNull(request, component) && isComponentCacheRequired(component) && useCacheInternal()
				&& !isPreviewOrLiveEditEnabled(request);

	}

	/**
	 * This method is used to check which component is required cms caching as per given configuration.
	 *
	 * @param component
	 *           the component
	 * @return true, if is component cache required
	 */
	protected boolean isComponentCacheRequired(final AbstractCMSComponentModel component)
	{
		final String componentNames = getConfigurationService().getConfiguration().getString("cms.cache.enabled.components");

		if (StringUtils.isNotEmpty(componentNames))
		{
			final String[] componentArray = componentNames.split(",");
			final List<String> components = Arrays.asList(componentArray);
			if (components.contains(component.getUid()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to check if request and component objects null or not.
	 *
	 * @param request
	 * @param component
	 *
	 * @return boolean true or false.
	 */
	private boolean isRequestAndComponentNull(final HttpServletRequest request, final AbstractCMSComponentModel component)
	{
		return request != null && component != null;
	}
}
