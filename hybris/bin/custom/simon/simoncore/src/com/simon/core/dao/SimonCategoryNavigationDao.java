package com.simon.core.dao;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;


/**
 * This Data access Object Interface is used to retrieve sub categories of Level 3 categories which contains products
 * from database.
 */
public interface SimonCategoryNavigationDao
{

	/**
	 * Method is used to fetch sub categories of level 2 categories which contains product.
	 * 
	 * @param categoryId
	 *           categoryId is the Id of category for which subcategories are fetched
	 * @return List<CategoryModel> List of category model is the list which contains subcategories
	 */
	public List<CategoryModel> getSubCategoriesWithProducts(final String categoryId);
}
