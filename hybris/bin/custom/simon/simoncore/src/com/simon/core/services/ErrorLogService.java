package com.simon.core.services;

import java.util.Date;
import java.util.List;


/**
 * This interface is use to fetch information from ErrorLog
 */
public interface ErrorLogService
{
	/**
	 * @param lastEndTime
	 * @return list of products
	 */
	List<String> getProductCodes(final Date lastEndTime);
}
