package com.simon.core.mirakl.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.exceptions.ProductFileImportPostProcessingException;
import com.simon.core.file.wrapper.FileWriterWrapper;
import com.simon.core.mirakl.strategies.ProductFileImportPostProcessor;


/**
 *
 *
 */
@Order(value = 2)
public class ErrorHandlingPostProcessor implements ProductFileImportPostProcessor
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandlingPostProcessor.class);

	private static final String PATH_SEPERATOR = "file.separator";

	private static final String CSV_SEPERATOR = "simon.product.feed.error.summary.csv.seperator";

	private static final String FILENAME_SEPERATOR = "product.image.filename.seperator";

	private static final String CSV_URL = "simon.product.feed.error.log";

	private static final String PREFIX_FILENAME_SUMMARY = "simon.product.feed.error.summary.prefix";

	private static final String DEFAULT_SEPERATOR = "|";

	private static final String DEFAULT_FILENAME_SEPERATOR = "-";

	private static final String EXTENSION_FILE = ".csv";

	private static final String REGEX_DATE = "yyyyMMddHHmm";

	private static final String PRODUCT_ERROR_TEXT = "simon.product.feed.error.summary.product.text";

	private static final String MAPPING_ERROR_TEXT = "simon.product.feed.error.summary.mapping.text";

	@Resource
	private ConfigurationService configurationService;

	/** The file writer wrapper for IO operations. */
	@Autowired
	FileWriterWrapper fileWriterWrapper;


	@Override
	public void postProcess(final ProductImportFileContextData context, final String importId)
			throws ProductFileImportPostProcessingException
	{

		final String fileNameSeperator = configurationService.getConfiguration().getString(FILENAME_SEPERATOR,
				DEFAULT_FILENAME_SEPERATOR);
		final StringBuffer remainingPath = new StringBuffer(configurationService.getConfiguration().getString(CSV_URL));
		final File directory = new File(remainingPath.toString());
		if (!directory.exists())
		{
			directory.mkdirs();
		}
		remainingPath.append(System.getProperty(PATH_SEPERATOR));
		remainingPath.append(configurationService.getConfiguration().getString(PREFIX_FILENAME_SUMMARY));
		remainingPath.append(fileNameSeperator);
		remainingPath.append(context.getShopId());
		remainingPath.append(fileNameSeperator);
		remainingPath.append(importId);
		remainingPath.append(fileNameSeperator);
		remainingPath.append(new SimpleDateFormat(REGEX_DATE).format(new Date()));
		remainingPath.append(EXTENSION_FILE);
		final String SEPERATOR = configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR);

		try
		{
			final Writer fw = fileWriterWrapper.getWriter(remainingPath.toString());
			fw.write(configurationService.getConfiguration().getString(PRODUCT_ERROR_TEXT) + SEPERATOR
					+ context.getProductFailure().size());
			fw.write(System.getProperty("line.separator"));
			fw.write(configurationService.getConfiguration().getString(MAPPING_ERROR_TEXT) + SEPERATOR
					+ context.getMappingFailure().size());
			fw.write(System.getProperty("line.separator"));
			IOUtils.closeQuietly(fw);
		}
		catch (final IOException e)
		{
			LOGGER.error("Error in writing file name " + remainingPath + "with import id " + importId, e);
		}
	}
}
