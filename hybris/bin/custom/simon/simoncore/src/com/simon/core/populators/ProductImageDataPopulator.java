package com.simon.core.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.ImagesData;
import com.simon.core.dto.ProductDimensionsData;


/**
 * This class populates the {@link ImagesData} with all supported media format images for the Products from
 * {@link Document}. This will be used whenever PDP is accessed for all the SKUs.
 */
public class ProductImageDataPopulator implements Populator<Document, Map<String, ImagesData>>
{

	/**
	 * This method populates data into {@link ProductDimensionsData} from {@link Document}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 *
	 * @param images
	 *           Map<String,ImgaesData> contains {@link ImagesData} DTO which holds images related to single sku
	 *           (GenericVariantProduct). Indexed Images from solr document in populated into this DTO
	 *
	 * @throws ConversionException
	 *            if document's field value is not able to cast in given type
	 */
	@Override
	public void populate(final Document document, final Map<String, ImagesData> images)
	{
		final Map<String, Map<String, String>> mediaContainerToImagesMap = new HashMap<>();
		List<String> imagesListFromSolr = new ArrayList<>();
		if (null != document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES))
		{
			imagesListFromSolr = (List<String>) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES);
		}

		final String designerName = (String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME);
		final String title = (String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME);
		final String color = (String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME);
		final StringBuilder altText = new StringBuilder();
		if (StringUtils.isNotEmpty(designerName))
		{
			altText.append(designerName).append(SimonCoreConstants.SPACE);
		}
		if (StringUtils.isNotEmpty(color))
		{
			altText.append(color).append(SimonCoreConstants.SPACE);
		}
		if (StringUtils.isNotEmpty(title))
		{
			altText.append(title);
		}

		if (CollectionUtils.isNotEmpty(imagesListFromSolr))
		{
			for (final String imageFromSolr : imagesListFromSolr)
			{
				final Map<String, String> formatToImageMap = new HashMap<>();
				final String[] imagePerMediaFormat = StringUtils.split(imageFromSolr, SimonCoreConstants.PIPE);
				final String[] mediaFormatPerMC = StringUtils.split(imagePerMediaFormat[0], SimonCoreConstants.UNDERSCORE);
				formatToImageMap.put(mediaFormatPerMC[0], imagePerMediaFormat[1]);

				if (mediaContainerToImagesMap.containsKey(mediaFormatPerMC[1]))
				{
					mediaContainerToImagesMap.get(mediaFormatPerMC[1]).putAll(formatToImageMap);
				}
				else
				{
					mediaContainerToImagesMap.put(mediaFormatPerMC[1], formatToImageMap);
				}
			}
		}
		populateProductImagesData(mediaContainerToImagesMap, images, altText.toString());
	}

	/**
	 * Sets the product images in different formats.
	 *
	 * @param images2
	 *
	 * @param images1
	 *           the images map is used to fetch the product images fetched from solr.
	 * @param counter
	 *           is used to distinguish the different sets of images.
	 * @param emptySet
	 *           flag is set whenever image is not found.
	 * @param image
	 *           represents the {@link ImagesData} that is actually populated with the product image.
	 */
	private void populateProductImagesData(final Map<String, Map<String, String>> mediaContainerToImagesMap,
			final Map<String, ImagesData> images, final String altText)
	{

		if (!CollectionUtils.sizeIsEmpty(mediaContainerToImagesMap))
		{
			for (final Entry<String, Map<String, String>> entry : mediaContainerToImagesMap.entrySet())
			{
				final Map<String, String> mediaToImageUrl = entry.getValue();
				final ImagesData imagesData = new ImagesData();
				imagesData.setMain(mediaToImageUrl.get(SimonCoreConstants.MEDIA_FORMAT_DEFAULT));
				imagesData.setMobile(mediaToImageUrl.get(SimonCoreConstants.MEDIA_FORMAT_MOBILE));
				imagesData.setThumbnail(mediaToImageUrl.get(SimonCoreConstants.MEDIA_FORMAT_THUMBNAIL));
				imagesData.setZoom(mediaToImageUrl.get(SimonCoreConstants.MEDIA_FORMAT_ZOOM));
				imagesData.setAltText(altText);
				images.put(entry.getKey(), imagesData);
			}
		}
		else
		{
			final ImagesData imagesData = new ImagesData();
			imagesData.setAltText(altText);
			images.put(SimonCoreConstants.DEFAULT, imagesData);
		}
	}
}
