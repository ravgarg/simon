package com.simon.core.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.simon.core.model.DesignerModel;


/**
 * Dao for {@link DesignerModel} contains abstract method for operations on {@link DesignerModel}
 */
public interface DesignerDao extends Dao
{

	/**
	 * Find designer by code. Returns {@link DesignerModel} for given designer <code>code</code>.
	 *
	 * @param code
	 *           the designer <code>code</code>
	 * @return the designer model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code>
	 */
	DesignerModel findDesignerByCode(String code);

	/**
	 * Find list of designer by code string. Returns {@link DesignerModel} for given designer <code>code</code>.
	 *
	 * @param objects
	 *           the designer <code>code</code>
	 * @return the designer model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code>
	 */
	List<DesignerModel> findListOfDesignerForCodes(List<String> objects);
}
