package com.simon.core.provider.impl;


import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;


/**
 * This class helps in indexing details or VariantCategories into InputDocument
 */
public class VariantCategoryValueResolver
		extends AbstractValueResolver<GenericVariantProductModel, Collection<CategoryModel>, Object>
{

	/**
	 * ModelService to fetch attribute values
	 */
	@Resource
	private ModelService modelService;
	/**
	 * TypeService to check whether a particular attribute belong to type specified.
	 */
	@Resource
	private TypeService typeService;
	/**
	 * Attribute which maps to "attribute" ValueProviderParameter in impex. Fetches the attribute name to index.
	 */
	public static final String ATTRIBUTE_PARAM = "attribute";
	/**
	 * The default value of "attribute", in case value is not fetched from model
	 *
	 */
	public static final String ATTRIBUTE_PARAM_DEFAULT_VALUE = null;
	/**
	 * Attribute which check whether indexed property is optional or not
	 */
	public static final String OPTIONAL_PARAM = "optional";
	/**
	 * The default value of optional attribute
	 */
	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	/**
	 * This method fetches VariantValueCategories from ResolverContext, if not null, gets the required value and adds to
	 * InputDocument
	 *
	 * @throws FieldValueProviderException,
	 *            Exception is thrown if there is any problem while adding data to index or expected value is not
	 *            fetched, given the attribute has optional set to false.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Collection<CategoryModel>, Object> resolverContext) throws FieldValueProviderException
	{
		boolean hasVariantValueCategory = false;
		final Collection<CategoryModel> variantValueCategories = resolverContext.getData();
		if (CollectionUtils.isNotEmpty(variantValueCategories))
		{
			final String attributeName = getAttributeName(indexedProperty);
			final String propertyName = getPropertyName(indexedProperty);
			final Object attributeValue = getAttributeValue(variantValueCategories, attributeName, propertyName);
			hasVariantValueCategory = filterAndAddFieldValues(document, batchContext, indexedProperty, attributeValue,
					resolverContext.getFieldQualifier());

		}
		if (!hasVariantValueCategory)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, OPTIONAL_PARAM,
					OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	/**
	 *
	 * @param variantValueCategories
	 *           Collection of VariantValueCategories fetched from ResolverContext
	 * @param attributeName
	 *           The name of VariantCategory for which details needs to be fetched
	 * @param propertyName
	 *           the property of VariantValueCategory for which value is fetched
	 * @return value of the property
	 */
	protected Object getAttributeValue(final Collection<CategoryModel> variantValueCategories, final String attributeName,
			final String propertyName)
	{
		Object value = null;


		if (StringUtils.isNotEmpty(attributeName))
		{
			for (final CategoryModel variantValueVategory : variantValueCategories)
			{
				final CategoryModel variantCategory = variantValueVategory.getSupercategories().get(0);
				if (variantCategory.getCode().equals(attributeName))
				{
					value = getModelAttributeValue(variantValueVategory, propertyName);
					break;
				}
			}
		}
		return value;

	}

	/**
	 *
	 * @param indexedProperty
	 *           The IndexedProeprty for which this resolver is invoked
	 * @return name of VariantCategory
	 */
	protected String getAttributeName(final IndexedProperty indexedProperty)
	{
		String attributeName = ValueProviderParameterUtils.getString(indexedProperty, ATTRIBUTE_PARAM,
				ATTRIBUTE_PARAM_DEFAULT_VALUE);

		if (attributeName == null)
		{
			attributeName = indexedProperty.getName();
		}

		return attributeName;
	}

	/**
	 *
	 * @param indexedProperty
	 *           The IndexedProeprty for which this resolver is invoked
	 * @return The name of property fetched from ValueProviderParameter
	 */
	protected String getPropertyName(final IndexedProperty indexedProperty)
	{
		String propertyName = ValueProviderParameterUtils.getString(indexedProperty, "value", ATTRIBUTE_PARAM_DEFAULT_VALUE);

		if (propertyName == null)
		{
			propertyName = indexedProperty.getName();
		}

		return propertyName;
	}

	/**
	 *
	 * @param category
	 *           The VariantValueCategory for which value needs to be fetched
	 * @param attributeName
	 *           the attribute for which values needs to be fetched
	 * @return the value of the attribute,which is to be indexed
	 */
	protected Object getModelAttributeValue(final CategoryModel category, final String attributeName)
	{
		Object value = null;

		final ComposedTypeModel composedType = typeService.getComposedTypeForClass(category.getClass());
		if (typeService.hasAttribute(composedType, attributeName))
		{
			value = modelService.getAttributeValue(category, attributeName);
		}

		return value;
	}

	/**
	 * Loads the data from {@link GenericVariantProductModel} into ResolverContext
	 */
	@Override
	protected Collection<CategoryModel> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel model)
			throws FieldValueProviderException
	{
		return model.getSupercategories();

	}



}
