package com.simon.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import com.simon.core.services.LeftNavigationCategoryService;


/**
 * Cron Job to fill create Map of L1 category Code and corresponding hierarchy
 */
public class PopulateLeftNavigationMapJob extends AbstractJobPerformable<CronJobModel>
{
	@Resource
	private LeftNavigationCategoryService leftNavigationCategoryService;

	/**
	 * Method to perform cronjob Returns Success if map Creation is successful
	 */
	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final boolean mapCreationSuccessful = leftNavigationCategoryService.createMap();
		if (mapCreationSuccessful)
		{
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
	}

}
