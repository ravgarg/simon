package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;

import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCategoryAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * SimonCategoryAttributeHandler use to map mirakl Category to product level attribute and map to merchandizing taxonomy
 */
public class SimonCategoryAttributeHandler extends DefaultCategoryAttributeHandler<MiraklCategoryCoreAttributeModel>
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attributeValue, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		super.setValue(attributeValue, data, context);
		final String categoryCode = attributeValue.getValue();
		if (StringUtils.isBlank(categoryCode))
		{
			return;
		}
		final CatalogVersionModel productCatalogVersion = modelService.get(context.getGlobalContext().getProductCatalogVersion());
		final CategoryModel receivedCategory = categoryService.getCategoryForCode(productCatalogVersion, categoryCode);
		data.getProductToUpdate().setMiraklCategory(receivedCategory);
		data.getRootBaseProductToUpdate().setMiraklCategory(receivedCategory);
	}

}
