package com.simon.core.promotions;

import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruledefinitions.conditions.AbstractRuleConditionTranslator;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.simon.promotion.rao.RetailerRAO;


/**
 * Translators are called while publishing a promotion. These are used to generate IR(intermedial rules). This class
 * enables generation of IR code for retailers specific cart.
 */
public class ExtRuleCartTotalConditionTranslator extends AbstractRuleConditionTranslator
{

	private static final String RETAILER_PARAM = "retailer";
	private static final String RETAILER_ID_ATTRIBUTE = "retailerId";
	private static final String RETAILER_SUBTOTAL_ATTRIBUTE = "total";

	/**
	 * Translates retailer sub bag total condition in intermediate rule that can be used in rule engine.
	 *
	 * @param context
	 *           the context
	 * @param condition
	 *           the condition
	 * @param conditionDefinition
	 *           the condition definition
	 * @return the rule ir condition
	 */
	@Override
	public RuleIrCondition translate(final RuleCompilerContext context, final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition)
	{
		final Map<String, RuleParameterData> conditionParams = condition.getParameters();
		final RuleParameterData operatorParam = conditionParams.get(OPERATOR_PARAM);
		final RuleParameterData valueParam = conditionParams.get(VALUE_PARAM);
		final RuleParameterData retailerParam = conditionParams.get(RETAILER_PARAM);
		if (verifyAllPresent(operatorParam, valueParam, retailerParam))
		{
			final AmountOperator operator = operatorParam.getValue();
			final Map<String, BigDecimal> value = valueParam.getValue();
			final String shop = retailerParam.getValue();
			if (verifyAllPresent(operator, value, shop))
			{
				return getConditions(context, operator, value, shop);
			}
		}
		return IrConditions.newIrRuleFalseCondition();
	}

	/**
	 * Gets the retailer total conditions.
	 *
	 * @param context
	 *           the context
	 * @param operator
	 *           the operator
	 * @param value
	 *           the value
	 * @param shop
	 *           the shop
	 * @return the retailer cart total conditions
	 *
	 */
	private RuleIrCondition getConditions(final RuleCompilerContext context, final AmountOperator operator,
			final Map<String, BigDecimal> value, final String shop)
	{
		final RuleIrGroupCondition irCartTotalCondition = RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND)
				.build();
		addRetailerTotalConditions(context, operator, value, irCartTotalCondition, shop);
		return irCartTotalCondition;
	}


	/**
	 * Adds the retailer total conditions.
	 *
	 * @param context
	 *           the context
	 * @param operator
	 *           the operator
	 * @param value
	 *           the value
	 * @param irCartTotalCondition
	 *           the ir cart total condition
	 * @param shop
	 *           the shop
	 */
	private void addRetailerTotalConditions(final RuleCompilerContext context, final AmountOperator operator,
			final Map<String, BigDecimal> value, final RuleIrGroupCondition irCartTotalCondition, final String shop)
	{
		final String retailerRaoVariable = context.generateVariable(RetailerRAO.class);
		for (final Entry<String, BigDecimal> entry : value.entrySet())
		{
			if (this.verifyAllPresent(entry.getKey(), entry.getValue()))
			{
				final RuleIrGroupCondition irCurrencyGroupCondition = RuleIrGroupConditionBuilder
						.newGroupConditionOf(RuleIrGroupOperator.AND).build();
				final List<RuleIrCondition> ruleIrConditions = irCurrencyGroupCondition.getChildren();
				ruleIrConditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(retailerRaoVariable)
						.withAttribute(RETAILER_ID_ATTRIBUTE).withOperator(RuleIrAttributeOperator.EQUAL).withValue(shop).build());
				ruleIrConditions.add(
						RuleIrAttributeConditionBuilder.newAttributeConditionFor(retailerRaoVariable).withAttribute("currencyIsoCode")
								.withOperator(RuleIrAttributeOperator.EQUAL).withValue(entry.getKey()).build());
				ruleIrConditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(retailerRaoVariable)
						.withAttribute(RETAILER_SUBTOTAL_ATTRIBUTE).withOperator(RuleIrAttributeOperator.valueOf(operator.name()))
						.withValue(entry.getValue()).build());
				irCartTotalCondition.getChildren().add(irCurrencyGroupCondition);
			}
		}

	}
}
