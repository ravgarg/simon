package com.simon.core.provider.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Required;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


/**
 * This ValueProvider will provide a list of promotion codes associated with the product. This implementation uses only
 * the DefaultPromotionGroup.
 */
public class ExtPromotionDataValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;
	private PromotionsService promotionsService;

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			final ProductModel product = (ProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<>();

			if (indexedProperty.isMultiValue())
			{
				fieldValues.addAll(createFieldValues(product, indexConfig, indexedProperty));
			}
			else
			{
				fieldValues.addAll(createFieldValue(product, indexConfig, indexedProperty));
			}
			return fieldValues;
		}
		else
		{
			throw new FieldValueProviderException(
					String.format("Cannot get promotion %s of non-product item", indexedProperty.getName()));
		}
	}

	protected List<FieldValue> createFieldValue(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if (baseSiteModel != null && baseSiteModel.getDefaultPromotionGroup() != null)
		{
			final List<? extends AbstractPromotionModel> promotionList = getPromotionsService().getAbstractProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product, true, new Date());
			final Optional<? extends AbstractPromotionModel> promo = promotionList.stream()
					.max(Comparator.comparing(p -> ((AbstractPromotionModel) p).getPriority()));
			if (promo.isPresent())
			{
				switch (indexedProperty.getName())
				{
					case IndexedKeys.PROMOTION_CODE:
						addFieldValues(fieldValues, indexedProperty, null, promo.get().getCode());
						break;
					case IndexedKeys.PROMOTION_END_DATE:
						indexEndDate(indexedProperty, fieldValues, promo.get());
						break;
					case IndexedKeys.PROMOTION_START_DATE:
						indexStartDate(indexedProperty, fieldValues, promo.get());
						break;
					case IndexedKeys.PROMOTION_TYPE:
						addFieldValues(fieldValues, indexedProperty, null, promo.get().getPromotionType());
						break;
					case IndexedKeys.PROMOTION_DESCRIPTION:
						addFieldValues(fieldValues, indexedProperty, null, getPromotionsService().getPromotionDescription(promo.get()));
						break;
					case IndexedKeys.PROMOTION_PRIORITY:
						addFieldValues(fieldValues, indexedProperty, null, promo.get().getPriority());
						break;
					default:
						break;
				}
			}

		}
		return fieldValues;
	}

	private void indexEndDate(final IndexedProperty indexedProperty, final List<FieldValue> fieldValues,
			final AbstractPromotionModel promo)
	{
		final Date endDate = promo.getEndDate();
		if (endDate != null)
		{
			addFieldValues(fieldValues, indexedProperty, null, promo.getEndDate());
		}
	}

	private void indexStartDate(final IndexedProperty indexedProperty, final List<FieldValue> fieldValues,
			final AbstractPromotionModel promo)
	{
		final Date startDate = promo.getStartDate();
		if (startDate != null)
		{
			addFieldValues(fieldValues, indexedProperty, null, promo.getStartDate());
		}
	}

	protected List<FieldValue> createFieldValues(final ProductModel product, final IndexConfig indexConfig,
			final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<>();
		final BaseSiteModel baseSiteModel = indexConfig.getBaseSite();
		if (baseSiteModel != null && baseSiteModel.getDefaultPromotionGroup() != null)
		{
			for (final AbstractPromotionModel promotion : getPromotionsService().getAbstractProductPromotions(
					Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product, true, new Date()))
			{
				switch (indexedProperty.getName())
				{
					case IndexedKeys.PROMOTION_CODE:
						addFieldValues(fieldValues, indexedProperty, null, promotion.getCode());
						break;
					case IndexedKeys.PROMOTION_END_DATE:
						indexPromotionEndDates(indexedProperty, fieldValues, promotion);
						break;
					case IndexedKeys.PROMOTION_START_DATE:
						indexPromotionStartDates(indexedProperty, fieldValues, promotion);
						break;
					case IndexedKeys.PROMOTION_TYPE:
						addFieldValues(fieldValues, indexedProperty, null, promotion.getPromotionType());
						break;
					case IndexedKeys.PROMOTION_DESCRIPTION:
						addFieldValues(fieldValues, indexedProperty, null, getPromotionsService().getPromotionDescription(promotion));
						break;
					case IndexedKeys.PROMOTION_PRIORITY:
						addFieldValues(fieldValues, indexedProperty, null, promotion.getPriority());
						break;
					default:
						break;
				}
			}
		}
		return fieldValues;
	}

	private void indexPromotionEndDates(final IndexedProperty indexedProperty, final List<FieldValue> fieldValues,
			final AbstractPromotionModel promotion)
	{
		if (promotion.getEndDate() != null)
		{
			addFieldValues(fieldValues, indexedProperty, null, promotion.getEndDate());
		}
	}

	private void indexPromotionStartDates(final IndexedProperty indexedProperty, final List<FieldValue> fieldValues,
			final AbstractPromotionModel promotion)
	{
		if (promotion.getStartDate() != null)
		{
			addFieldValues(fieldValues, indexedProperty, null, promotion.getStartDate());
		}
	}

	protected void addFieldValues(final List<FieldValue> fieldValues, final IndexedProperty indexedProperty,
			final LanguageModel language, final Object value)
	{
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,
				language == null ? null : language.getIsocode());
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	/**
	 * Inject the Field name provider
	 *
	 * @param fieldNameProvider
	 */
	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	protected PromotionsService getPromotionsService()
	{
		return promotionsService;
	}

	/**
	 * Inject the Promotion Service
	 *
	 * @param promotionsService
	 */
	@Required
	public void setPromotionsService(final PromotionsService promotionsService)
	{
		this.promotionsService = promotionsService;
	}
}
