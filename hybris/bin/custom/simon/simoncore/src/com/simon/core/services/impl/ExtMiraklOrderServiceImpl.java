package com.simon.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.lang.String.format;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang.StringUtils.isBlank;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklCreateOrderRequest;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklValidOrderRequest;
import com.mirakl.hybris.core.order.services.impl.DefaultMiraklOrderService;
import com.simon.core.model.MarketPlaceJsonModel;
import com.simon.core.services.ExtMiraklOrderService;
import com.simon.core.services.RetailerService;


/**
 * This class is created for override createMarketplaceOrders method from DefaultMiraklOrderService because we can not
 * create offer which product quantity is zero
 */
public class ExtMiraklOrderServiceImpl extends DefaultMiraklOrderService implements ExtMiraklOrderService
{

	private static final Logger LOG = LoggerFactory.getLogger(ExtMiraklOrderServiceImpl.class);
	@Resource
	private RetailerService retailerService;

	/**
	 * This method splits the hybris order in multiple separate market place orders
	 *
	 * @param order
	 * @return MiraklCreatedOrders
	 */
	@Override
	public List<MiraklCreatedOrders> createMarketplaceOrdersForRetailers(final OrderModel order)
	{
		validateParameterNotNullStandardMessage(OrderModel._TYPECODE, order);
		final List<String> retailers = retailerService.getAllRetailersFromOrder(order);
		final List<MiraklCreatedOrders> createdOrders = new ArrayList<>();

		for (final String retailer : retailers)
		{

			final MiraklCreateOrder miraklCreateOrder = new MiraklCreateOrder();
			//commercial id is set as retailer is temporary but mandatory here,
			//it will be overriden in populator. retailer id will be used in populator
			//to populate other entries. Populator will throw error if retailer id is not populated
			miraklCreateOrder.setCommercialId(retailer);
			miraklCreateOrderConverter.convert(order, miraklCreateOrder);
			miraklCreateOrder.setOffers(miraklCreateOrder.getOffers());
			if (isNotEmpty(miraklCreateOrder.getOffers()))
			{
				final MiraklCreateOrderRequest request = new MiraklCreateOrderRequest(miraklCreateOrder);
				LOG.info("Order Export to Mirakl:: Sending order {} to Mirakl calling OR01 API", miraklCreateOrder.getCommercialId());
				final MiraklCreatedOrders createdOrder = miraklApi.createOrder(request);
				storeCreatedOrdersForRetailer(order, createdOrder, retailer);
				createdOrders.add(createdOrder);

			}
		}
		return createdOrders;
	}

	/**
	 * This validates whether all retailers have it's order on mirakl or not
	 **/
	@Override
	public void validateOrder(final OrderModel order)
	{
		validateParameterNotNullStandardMessage("order", order);
		for (final String retailerOrder : retailerService.getAllRetailersFromOrder(order))
		{
			final String marketPlaceOrderForRetailer = retailerService.getRetailerOrderNumberForMarketPlace(order, retailerOrder);
			LOG.debug(format("Sending validation for order [%s].", marketPlaceOrderForRetailer));
			miraklApi.validOrder(new MiraklValidOrderRequest(marketPlaceOrderForRetailer));
		}
	}


	/**
	 * This method stores the JSON from mirakl to respective MarketPlaceJsonModel which will be used for further creation
	 * of consignment
	 *
	 * @param order
	 * @param createdOrders
	 * @param retailer
	 * @return
	 */
	@Override
	public String storeCreatedOrdersForRetailer(final OrderModel order, final MiraklCreatedOrders createdOrders,
			final String retailer)
	{
		validateParameterNotNullStandardMessage("createdOrders", createdOrders);
		validateParameterNotNullStandardMessage("order", order);
		final MarketPlaceJsonModel marketPlaceJson = modelService.create(MarketPlaceJsonModel.class);
		marketPlaceJson.setRetailerId(retailer);
		marketPlaceJson.setOrder(order);
		final String serializedOrders = jsonMarshallingService.toJson(createdOrders, MiraklCreatedOrders.class);
		marketPlaceJson.setJson(serializedOrders);

		modelService.save(marketPlaceJson);
		modelService.save(order);
		return serializedOrders;
	}

	private MarketPlaceJsonModel getCreatedOrderRetailersJson(final Collection<MarketPlaceJsonModel> createdOrderRetailersJSON,
			final String retailer)
	{
		MarketPlaceJsonModel retailerJSON = null;
		if (createdOrderRetailersJSON != null)
		{
			final Optional<MarketPlaceJsonModel> retailerJSONOptional = createdOrderRetailersJSON.stream()
					.filter(json -> retailer.equals(json.getRetailerId())).findFirst();
			if (retailerJSONOptional.isPresent())
			{
				retailerJSON = retailerJSONOptional.get();
			}
		}
		return retailerJSON;
	}

	/**
	 * This method creates MiraklCreatedOrders stored in MarketPlaceJsonModel for respective retailer
	 *
	 * @param order
	 * @param retailer
	 * @return
	 */
	@Override
	public MiraklCreatedOrders loadCreatedOrdersForRetailer(final OrderModel order, final String retailer)
	{
		validateParameterNotNullStandardMessage("order", order);
		final MarketPlaceJsonModel createdOrderForRetailer = getCreatedOrderRetailersJson(order.getRetailersCreatedOrdersJSON(),
				retailer);
		if (createdOrderForRetailer == null || isBlank(createdOrderForRetailer.getJson()))
		{
			LOG.debug(format("No marketplace orders stored for commercial order id [%s].",
					retailerService.getRetailerOrderNumberForMarketPlace(order, retailer)));
			return null;
		}
		return jsonMarshallingService.fromJson(createdOrderForRetailer.getJson(), MiraklCreatedOrders.class);
	}

}
