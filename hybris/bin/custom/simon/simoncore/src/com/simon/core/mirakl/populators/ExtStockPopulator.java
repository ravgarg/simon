package com.simon.core.mirakl.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;


/**
 * ExtStockPopulator populates {@link StockLevelModel} used as stock from {@link MiraklExportOffer}
 */
public class ExtStockPopulator implements Populator<MiraklExportOffer, StockLevelModel>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final MiraklExportOffer miraklExportOffer, final StockLevelModel target)
	{
		target.setAvailable(miraklExportOffer.getQuantity());
		target.setProductCode(miraklExportOffer.getProductSku());
	}
}
