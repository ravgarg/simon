package com.simon.core.customer.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.commerceservices.order.CommerceCardTypeService;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.customer.dao.ExtCustomerAccountDao;
import com.simon.core.customer.service.ExtCustomerAccountService;
import com.simon.core.dao.ExtDealDao;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.strategies.impl.ExtCheckoutCustomerStrategy;
import com.simon.integration.constants.SimonIntegrationConstants;


/**
 * Implementation to save spreedly payment information and associate with customer
 */
public class ExtCustomerAccountServiceImpl extends DefaultCustomerAccountService implements ExtCustomerAccountService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtCustomerAccountServiceImpl.class);

	@Autowired
	private ExtCheckoutCustomerStrategy checkoutCustomerStrategy;
	@Autowired
	private CommerceCardTypeService commerceCardTypeService;
	@Resource
	private ExtCustomerAccountDao extCustomerAccountDao;
	@Resource
	private ExtDealDao extDealDao;

	/**
	 * use to save payment information and associate with customer
	 *
	 * @param extPaymentInfoData
	 * @return CreditCardPaymentInfoModel
	 */
	@Override
	public CreditCardPaymentInfoModel createPaymentInfoSubscription(final CCPaymentInfoData extPaymentInfoData)
			throws ModelSavingException
	{
		final CustomerModel customerModel = checkoutCustomerStrategy.getCurrentUserForCheckout();
		try
		{
			final CreditCardPaymentInfoModel cardPaymentInfoModel = getPaymentInfModel(extPaymentInfoData, customerModel);
			final AddressModel billingAddress = getBillingAddress(extPaymentInfoData, customerModel, cardPaymentInfoModel);
			billingAddress.setOwner(cardPaymentInfoModel);
			cardPaymentInfoModel.setBillingAddress(billingAddress);
			getModelService().saveAll(billingAddress, cardPaymentInfoModel);
			getModelService().refresh(customerModel);

			addPaymentInfo(customerModel, cardPaymentInfoModel);

			return cardPaymentInfoModel;
		}
		catch (final AdapterException ae) //NOSONAR
		{
			LOGGER.debug("Failed to create subscription for customer. Customer PK: {} Exception {}", customerModel.getPk(),
					ae.getClass().getName());
			return null;
		}
	}

	/**
	 * use to get addressmodel using extPaymentInfoData and customerModel
	 *
	 * @param extPaymentInfoData
	 * @return CreditCardPaymentInfoModel
	 */
	private AddressModel getBillingAddress(final CCPaymentInfoData extPaymentInfoData, final CustomerModel customerModel,
			final CreditCardPaymentInfoModel cardPaymentInfoModel)
	{
		final AddressModel billingAddress = getModelService().create(AddressModel.class);
		if (StringUtils.isNotBlank(extPaymentInfoData.getBillingAddress().getTitle()))
		{
			final TitleModel title = getModelService().create(TitleModel.class);
			title.setCode(extPaymentInfoData.getBillingAddress().getTitle());
			billingAddress.setTitle(getFlexibleSearchService().getModelByExample(title));
		}
		final String[] names = getCustomerNameStrategy().splitName(cardPaymentInfoModel.getCcOwner());
		if (names != null)
		{
			billingAddress.setFirstname(names[0]);
			billingAddress.setLastname(names[1]);
		}
		billingAddress.setLine1(extPaymentInfoData.getBillingAddress().getLine1());
		billingAddress.setLine2(extPaymentInfoData.getBillingAddress().getLine2());
		billingAddress.setTown(extPaymentInfoData.getBillingAddress().getTown());
		billingAddress.setPostalcode(extPaymentInfoData.getBillingAddress().getPostalCode());
		final CountryModel country = getCommonI18NService()
				.getCountry(extPaymentInfoData.getBillingAddress().getCountry().getIsocode());
		billingAddress.setCountry(country);
		billingAddress.setRegion(
				getCommonI18NService().getRegion(country, extPaymentInfoData.getBillingAddress().getRegion().getIsocode()));
		billingAddress.setPhone1(extPaymentInfoData.getBillingAddress().getPhone());
		final String email = getCustomerEmailResolutionService().getEmailForCustomer(customerModel);
		billingAddress.setEmail(email);
		billingAddress
				.setDuplicate(StringUtils.isNotEmpty(extPaymentInfoData.getBillingAddress().getId()) ? Boolean.TRUE : Boolean.FALSE);
		billingAddress.setDuplicateFrom(StringUtils.isNotEmpty(extPaymentInfoData.getBillingAddress().getId())
				? extPaymentInfoData.getBillingAddress().getId() : null);
		return billingAddress;
	}

	/**
	 * use to get CreditCardPaymentInfoModel using extPaymentInfoData and customerModel
	 *
	 * @param extPaymentInfoData
	 * @return CreditCardPaymentInfoModel
	 */
	private CreditCardPaymentInfoModel getPaymentInfModel(final CCPaymentInfoData extPaymentInfoData,
			final CustomerModel customerModel)
	{
		final CreditCardPaymentInfoModel cardPaymentInfoModel = getModelService().create(CreditCardPaymentInfoModel.class);

		cardPaymentInfoModel.setUpdated(new Date(System.currentTimeMillis()));
		cardPaymentInfoModel.setLastFourDigits(extPaymentInfoData.getLastFourDigits());
		cardPaymentInfoModel.setCode(customerModel.getUid() + "_" + UUID.randomUUID());
		cardPaymentInfoModel.setUser(customerModel);
		cardPaymentInfoModel.setValidToMonth(extPaymentInfoData.getExpiryMonth());
		cardPaymentInfoModel.setValidToYear(extPaymentInfoData.getExpiryYear());
		cardPaymentInfoModel.setCcOwner(extPaymentInfoData.getAccountHolderName());
		cardPaymentInfoModel.setNumber(extPaymentInfoData.getCardNumber());
		cardPaymentInfoModel.setType(setCardType(extPaymentInfoData.getCardType()));
		cardPaymentInfoModel.setSaved(extPaymentInfoData.isSaveInAccount());

		return cardPaymentInfoModel;

	}

	/*
	 * Updates the customer model from customer data
	 *
	 * @see
	 * com.simon.core.services.customer.ExtCustomerAccountService#updateProfileInfo(de.hybris.platform.core.model.user.
	 * CustomerModel)
	 */
	@Override
	public void updateProfileInfo(final CustomerModel customer) throws DuplicateUidException
	{
		validateParameterNotNullStandardMessage("customerModel", customer);
		internalSaveCustomer(customer);

	}

	@Override
	public List<DesignerModel> findListOfAllDesigners(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		return extCustomerAccountDao.findListOfAllDesigners(customerModel);

	}

	/**
	 * Update customer my favorite.
	 *
	 * @param customer
	 *           the customer
	 * @throws ModelSavingException
	 *            the model saving exception
	 */
	@Override
	public void updateCustomerMyFavorite(final CustomerModel customer) throws ModelSavingException
	{
		getModelService().save(customer);
	}

	/*
	 * Fetches the Last Added Favorite from customer data
	 *
	 * @see
	 * com.simon.core.services.customer.ExtCustomerAccountService#updateProfileInfo(de.hybris.platform.core.model.user.
	 * CustomerModel)
	 */
	@Override
	public List<List<Object>> getMyFavProductOrderByDate(final CustomerModel customer)
	{
		return extCustomerAccountDao.getMyFavProductOrderByDate(customer);
	}

	/**
	 * @param code
	 * @return
	 */
	private CreditCardType setCardType(final String code)
	{
		CreditCardType creditCardType = null;
		switch (code)
		{
			case SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_VISA:
				creditCardType = CreditCardType.VISA;
				break;
			case SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_AMERICAN_EXPRESS:
				creditCardType = CreditCardType.AMEX;
				break;
			case SimonIntegrationConstants.PAYMENT_SERVICE_CARD_TYPE_MASTER:
				creditCardType = CreditCardType.MASTER;
				break;
			default:
		}
		return creditCardType;
	}


	/**
	 * use to delete the payment info associated with the customer
	 *
	 * @param customerModel
	 * @param creditCardPaymentInfo
	 *
	 */
	@Override
	public void unlinkCCPaymentInfo(final CustomerModel customerModel, final CreditCardPaymentInfoModel creditCardPaymentInfo)
	{
		validateParameterNotNull(customerModel, "Customer model cannot be null");
		validateParameterNotNull(creditCardPaymentInfo, "CreditCardPaymentInfo model cannot be null");
		if (customerModel.getPaymentInfos().contains(creditCardPaymentInfo))
		{
			if (!creditCardPaymentInfo.getBillingAddress().getDuplicate())
			{
				final Optional<AddressModel> firstDuplicateAddress = extCustomerAccountDao
						.findAddressByDuplicateFrom(getAddressPk(creditCardPaymentInfo)).stream().findFirst();
				if (firstDuplicateAddress.isPresent())
				{
					final AddressModel duplicateAddress = firstDuplicateAddress.get();
					duplicateAddress.setDuplicate(Boolean.FALSE);
					getModelService().save(duplicateAddress);
				}
			}
			final Collection<PaymentInfoModel> paymentInfoList = new ArrayList(customerModel.getPaymentInfos());
			paymentInfoList.remove(creditCardPaymentInfo);
			customerModel.setPaymentInfos(paymentInfoList);
			getModelService().save(customerModel);
			getModelService().refresh(customerModel);
		}
		else
		{
			throw new IllegalArgumentException("Credit Card Payment Info " + creditCardPaymentInfo
					+ " does not belong to the customer " + customerModel + " and will not be removed.");
		}
	}

	/**
	 * This method convert guest user to register
	 *
	 * @param customerModel
	 * @param orderGUID
	 * @throws DuplicateUidException
	 */
	@Override
	public void convertGuestToRegisterCustomer(final CustomerModel customerModel, final String orderId)
			throws DuplicateUidException
	{
		final OrderModel orderModel = getOrderDetailsForGUID(orderId, getBaseStoreService().getCurrentBaseStore());
		if (orderModel == null)
		{
			throw new UnknownIdentifierException("Order with guid " + orderId + " not found in current BaseStore");
		}

		final CustomerModel customer = (CustomerModel) orderModel.getUser();
		if (!CustomerType.GUEST.equals(customer.getType()))
		{
			throw new IllegalArgumentException("Order with guid " + orderId + " does not belong to guest user");
		}
		addAdditionalAttribute(customer, customerModel);
		register(customer, null);
		getUserService().setCurrentUser(customer);
	}

	/**
	 * Adds the additional attribute in customer
	 *
	 * @param customer
	 *           the customer
	 * @param customerModel
	 *           the customer model
	 */
	private void addAdditionalAttribute(final CustomerModel customer, final CustomerModel customerModel)
	{
		customer.setName(customerModel.getName());
		customer.setType(CustomerType.REGISTERED);
		customer.setUid(customerModel.getUid());
		customer.setExternalId(customerModel.getExternalId());
		customer.setBirthYear(customerModel.getBirthYear());
		customer.setGender(customerModel.getGender());
		customer.setBirthMonth(customerModel.getBirthMonth());
		customer.setBirthYear(customerModel.getBirthYear());
		customer.setCountry(customerModel.getCountry());
		customer.setZipCode(customerModel.getZipCode());
		customer.setSessionLanguage(customerModel.getSessionLanguage());
		customer.setSessionCurrency(customerModel.getSessionCurrency());
		customer.setOriginalUid(customerModel.getOriginalUid());
		customer.setProfileCompleted(false);
		customer.setPrimaryMall(customerModel.getPrimaryMall());
	}

	/**
	 * Gets the address pk.
	 *
	 * @param creditCardPaymentInfo
	 *           the credit card payment info
	 * @return the address pk
	 */
	protected String getAddressPk(final CreditCardPaymentInfoModel creditCardPaymentInfo)
	{
		return creditCardPaymentInfo.getBillingAddress().getPk().toString();
	}

	/**
	 * Find list of all stores.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the list
	 */
	@Override
	public List<ShopModel> findListOfAllStores(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);
		return extCustomerAccountDao.findListOfAllStores(customerModel);

	}


	/**
	 * Find list of all designers.
	 *
	 * @return the list
	 */
	public List<DesignerModel> findListOfDesigners()
	{
		return extCustomerAccountDao.findListOfAllDesigners();
	}

	/**
	 * Find list of all favorite deals.
	 *
	 * @param customer
	 *
	 * @return the list
	 */

	@Override
	public List<DealModel> getAllFavouriteDeals(final CustomerModel customer)
	{
		validateParameterNotNullStandardMessage("customerModel", customer);
		return extCustomerAccountDao.findListOfAllDeals(customer);
	}


	/**
	 * @description Method use to register the new customer and do not trigger the customer registration email
	 * @method register
	 * @param customerModel
	 * @param password
	 */
	@Override
	public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException
	{
		registerCustomer(customerModel, password);
	}
}
