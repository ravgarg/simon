package com.simon.core.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;
import java.util.Set;

import com.simon.core.model.MallModel;


/**
 * Dao for {@link MallModel} contains abstract method for operations on {@link MallModel}
 */
public interface MallDao extends Dao
{

	/**
	 * Find Mall by code. Returns {@link MallModel} for given Mall <code>code</code>.
	 *
	 * @param code
	 *           the Mall <code>code</code>
	 * @return the Mall model
	 *
	 */
	MallModel findMallByCode(String code);

	/**
	 * Find all Malls . Returns {@link List<MallModel>}.
	 *
	 * @return a list of Mall Model
	 */
	List<MallModel> findAllMalls();

	/**
	 * Find list of Mall by code string. Returns {@link MallModel} for given Mall <code>mallCodes</code>.
	 *
	 * @param mallCodes
	 *           the Mall <code>mallCodes</code>
	 * @return the Mall model
	 *
	 */

	Set<MallModel> findMallListForCode(List<String> mallCodes);

}
