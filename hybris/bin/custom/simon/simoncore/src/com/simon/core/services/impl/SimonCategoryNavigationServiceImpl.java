package com.simon.core.services.impl;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;

import javax.annotation.Resource;

import com.simon.core.dao.SimonCategoryNavigationDao;
import com.simon.core.services.SimonCategoryNavigationService;


/**
 * Implementation class of {@link SimonCategoryNavigationService} used to retrieve sub categories of dynamic category
 * nodes which contains products.
 */
public class SimonCategoryNavigationServiceImpl implements SimonCategoryNavigationService
{

	@Resource
	private SimonCategoryNavigationDao simonCategoryNavigationDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CategoryModel> getSubCategories(final String categoryId)
	{
		final List<CategoryModel> subCategories = simonCategoryNavigationDao.getSubCategoriesWithProducts(categoryId);
		return subCategories;

	}
}
