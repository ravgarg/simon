package com.simon.core.services;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;

import com.simon.core.dto.NavigationData;


/**
 * This interface fetches all L1 categories and maintain the corresponding category hierarchy per L1 category.
 */
public interface LeftNavigationCategoryService
{
	/**
	 * This method fetches all categories who are children of Home Category
	 * @return list of L1 categories
	 */
	List<CategoryModel> getL1Categories();

	/**
	 * Creates a new map of of L1 categories and corresponding category hierarchy.
	 *
	 * @return flag whether map creation was successful or not
	 */
	boolean createMap();

	/**
	 * This method returns complete category hierarchy corresponding to particular L1 category. If no entry is found
	 * corresponding to L1 category, empty list is returned.
	 *
	 * @param l1CategoryCode
	 *           L1 category for whom category hierarchy is to be fetched
	 * @return category hierarchy DTO
	 */
	List<NavigationData> getNavigationData(String l1CategoryCode);

}
