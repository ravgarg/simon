package com.simon.core.mirakl.jobs;


import static java.lang.Double.valueOf;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import com.google.common.collect.Lists;
import com.simon.core.dto.FolderCleanUpMetaData;
import com.simon.core.model.FolderCleanupCronJobModel;


/**
 * This job delete all files of input directory which are more than X number of hour older.
 */
public class FolderCleanupJob extends AbstractJobPerformable<FolderCleanupCronJobModel>
{
	@Resource
	private ConfigurationService configurationService;
	private static final Logger LOGGER = LoggerFactory.getLogger(FolderCleanupJob.class);



	@Override
	public PerformResult perform(final FolderCleanupCronJobModel folderCleanupCronJobModel)
	{
		LOGGER.info("Started Delete Old files job ..");
		final StopWatch folderCleanupWatch = new StopWatch();
		folderCleanupWatch.start();
		final FolderCleanUpMetaData folderCleanUpMetaData = new FolderCleanUpMetaData();
		folderCleanUpMetaData.setSubBatchSize(folderCleanupCronJobModel.getSubBatchSize());
		folderCleanUpMetaData.setNoOfWorkers(folderCleanupCronJobModel.getNumberOfWorkers());
		folderCleanUpMetaData.setCleanupAgeInHours(folderCleanupCronJobModel.getCleanupAgeInHours());
		folderCleanUpMetaData.setMaxFileInOneBatch(folderCleanupCronJobModel.getMaxFileInOneBatch());
		folderCleanUpMetaData.setFilesDirectory(folderCleanupCronJobModel.getFilesDirectory());

		final File directory = new File(folderCleanUpMetaData.getFilesDirectory());
		final File[] fList = directory.listFiles();
		if (null != fList && fList.length > 0)
		{
			LOGGER.info("Total no of files avialable in folder [{}] ", fList.length);
			List<File> fileList = Arrays.asList(fList);
			final long timeToCompare = System.currentTimeMillis() - (folderCleanUpMetaData.getCleanupAgeInHours() * 60 * 60 * 1000);
			fileList = fileList.stream().filter(f -> f.lastModified() <= timeToCompare).collect(Collectors.toList());
			LOGGER.info("Total no of files eligible for delete [{}] ", fileList.size());


			deleteFilesInBatches(folderCleanUpMetaData, fileList);

		}
		else
		{
			LOGGER.info("Either directory is invalid or No files exist to delete");
		}
		folderCleanupWatch.stop();
		LOGGER.info("Finished  Delete Old Files job ... Total time = [{}] seconds.",
				valueOf(folderCleanupWatch.getTotalTimeSeconds()));

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * This method break total no of list in batches and then further divide list in small batch and then pass that list
	 * to executer to delete files from physical location.
	 *
	 * @param folderCleanUpMetaData
	 * @param serviceExecutor
	 * @param fileList
	 * @throws InterruptedException
	 */
	private void deleteFilesInBatches(final FolderCleanUpMetaData folderCleanUpMetaData, final List<File> fileList)

	{
		final List<List<File>> listOfBatches = Lists.partition(fileList, folderCleanUpMetaData.getMaxFileInOneBatch());
		for (final List<File> batch : listOfBatches)
		{
			final List<List<File>> listOfSamllBatch = Lists.partition(batch, folderCleanUpMetaData.getSubBatchSize());
			final ExecutorService serviceExecutor = Executors.newFixedThreadPool(folderCleanUpMetaData.getNoOfWorkers());
			for (final List<File> listOfFile : listOfSamllBatch)
			{
				serviceExecutor.execute(() -> deletFiles(listOfFile));
			}
			serviceExecutor.shutdown();
			try
			{
				serviceExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			}
			catch (final InterruptedException e)
			{
				Thread.currentThread().interrupt();
				LOGGER.error("Exception Occurred In deleteing files", e);

			}

		}
	}

	/**
	 * This Method delete files from directory.
	 *
	 * @param files
	 */
	private void deletFiles(final List<File> files)
	{

		for (final File file : files)
		{
			if (!file.delete())
			{
				LOGGER.info("Fail to delete this file " + file.getName());
			}

		}
	}

	@Override
	public boolean isAbortable()
	{
		return true;
	}




}
