package com.simon.core.services;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.Map;



/**
 * The Interface RetailerService. This will provide service to retailer specific operations
 */
public interface RetailerService
{

	/**
	 * Gets the retailer sub bag for cart.
	 *
	 * @param cart
	 *           the cart
	 * @return the retailer sub total for cart
	 */
	public Map<String, Double> getRetailerSubBagForCart(AbstractOrderModel cart);

	/**
	 * Gets the retailer sub bag saving.
	 *
	 * @param source
	 *           the source
	 * @param retailerId
	 *           the retailer id
	 * @return the retailer sub bag saving
	 */
	public Double getRetailerSubBagSaving(AbstractOrderModel source, String retailerId);

	/**
	 * Gets the retailer sub-total
	 *
	 * @param cart
	 * @param retailer
	 * @return double
	 */
	double getRetailerSubtotal(AbstractOrderModel cart, String retailer);


	/**
	 * method calculate the you save price for items which are not eligible for promotion
	 *
	 * @param entry
	 * @return Double
	 */
	public Double getYouSave(final AbstractOrderEntryModel entry);

	/**
	 * Method returns a net retailer total
	 *
	 * @param orderModel
	 * @param retailerId
	 * @return Method returns a net retailer total
	 */
	double getRetailerTotal(AbstractOrderModel orderModel, String retailerId);


	/**
	 * Method returns list of retailers which have their product in a given hybris order
	 *
	 * @param order
	 * @return List of retailers
	 */
	List<String> getAllRetailersFromOrder(AbstractOrderModel order);

	/**
	 * This method is used to obtain the commercial id that will be sent to mirakl for respective retailer order
	 *
	 * @param orderModel
	 * @param retailerId
	 * @return Commericial Id for the given retailer for market place order
	 */
	String getRetailerOrderNumberForMarketPlace(AbstractOrderModel orderModel, String retailerId);
}
