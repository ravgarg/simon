package com.simon.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.domain.offer.MiraklUpdateOffer;
import com.mirakl.client.mmp.domain.offer.importer.MiraklOfferImportTracking;
import com.mirakl.client.mmp.operator.core.MiraklMarketplacePlatformOperatorApi;
import com.mirakl.client.mmp.operator.request.offer.MiraklUpdateOffersRequest;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.PriceType;
import com.simon.core.services.ExtProductService;


/**
 * This job will be responsible for hitting Mirakl API OF24 that will be used to update the offer quantity at Mirakl
 *
 */
public class OfferUpdateJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(OfferUpdateJob.class);

	@Resource
	private ExtProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private MiraklMarketplacePlatformOperatorApi mmpOperatorApiClient;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		final List<CronJobHistoryModel> successCronJobHistories = cronJob.getCronJobHistoryEntries().stream()
				.filter(cronJobHistory -> cronJobHistory.getResult().equals(CronJobResult.SUCCESS)).collect(Collectors.toList());

		CronJobHistoryModel lastSuccessCronJobHistory = null;
		if (CollectionUtils.isNotEmpty(successCronJobHistories))
		{
			lastSuccessCronJobHistory = successCronJobHistories.get(successCronJobHistories.size() - 1);
		}

		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.ONLINE);
		final List<GenericVariantProductModel> products = productService.getProductsWithZeroStock(catalogVersion,
				null != lastSuccessCronJobHistory ? lastSuccessCronJobHistory.getEndTime() : getDate());

		final Map<String, List<MiraklUpdateOffer>> offersForShop = createListOfShopOffers(products);
		updateOffers(offersForShop);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private Date getDate()
	{
		final Calendar calendar = Calendar.getInstance();
		calendar.set(1990, 1, 1);
		return calendar.getTime();
	}

	/**
	 * This method will create a {@link Map} of {@link List} of {@link MiraklUpdateOffer} per retailer
	 *
	 * @param products
	 * @return offersForShop
	 */
	private Map<String, List<MiraklUpdateOffer>> createListOfShopOffers(final List<GenericVariantProductModel> products)
	{
		final Map<String, List<MiraklUpdateOffer>> offersForShop = new HashMap<>();
		for (final GenericVariantProductModel product : products)
		{
			final String retailerId = product.getShop().getId();

			if (null != offersForShop.get(retailerId))
			{
				offersForShop.get(retailerId).add(createMiraklUpdateOfferDTO(product));
			}
			else
			{
				final List<MiraklUpdateOffer> offers = new ArrayList<>();
				offers.add(createMiraklUpdateOfferDTO(product));
				offersForShop.put(retailerId, offers);
			}
		}
		return offersForShop;
	}

	/**
	 * This method will return object of {@link MiraklUpdateOffer}
	 *
	 * @param product
	 * @return offer
	 */
	private MiraklUpdateOffer createMiraklUpdateOfferDTO(final GenericVariantProductModel product)
	{
		final Optional<PriceRowModel> optionalPriceRow = product.getEurope1Prices().stream()
				.filter(priceRow -> priceRow.getPriceType().equals(PriceType.LIST)).findFirst();
		if (optionalPriceRow.isPresent())
		{
			final PriceRowModel priceRow = optionalPriceRow.get();
			final MiraklUpdateOffer offer = new MiraklUpdateOffer();

			offer.setAvailableStarted(priceRow.getStartTime());
			offer.setAvailableEnded(priceRow.getEndTime());
			offer.setDescription(priceRow.getDescription());
			offer.setInternalDescription(priceRow.getDescription());
			offer.setMinQuantityAlert(0);
			offer.setPrice(BigDecimal.valueOf(priceRow.getPrice()));
			offer.addAllPrices(Collections.emptyList());
			offer.setProductId(product.getCode());
			offer.setProductIdType("SKU");
			offer.setQuantity(0);
			offer.setSku(priceRow.getOfferSku());
			offer.setStateCode(priceRow.getOfferStateCode());
			offer.setUpdateDelete("update");
			return offer;
		}
		return null;
	}

	/**
	 * This method will call the API to update offers on Mirakl
	 *
	 * @param offersForShop
	 */
	private void updateOffers(final Map<String, List<MiraklUpdateOffer>> offersForShop)
	{
		offersForShop.entrySet().stream().forEach(entry -> {
			final MiraklUpdateOffersRequest updateOffersRequest = new MiraklUpdateOffersRequest(entry.getKey());
			final List<MiraklUpdateOffer> offers = entry.getValue();
			updateOffersRequest.setOffers(offers);
			final MiraklOfferImportTracking trackingNo = mmpOperatorApiClient.updateOffers(updateOffersRequest);
			if (null != trackingNo)
			{
				LOG.info(trackingNo.getImportId());
			}
		});
	}
}
