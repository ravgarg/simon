package com.simon.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.VariantValueCategoryDao;


/**
 * VariantValueCategoryDaoImpl db based implementation of {@link VariantValueCategoryDao}.
 */
public class VariantValueCategoryDaoImpl extends AbstractItemDao implements VariantValueCategoryDao
{
	private static final Logger LOGGER = LoggerFactory.getLogger(VariantValueCategoryDaoImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantValueCategoryModel findTopSequenceVariantValueCategory(final VariantCategoryModel variantCategory)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("Variant Category", variantCategory);

		VariantValueCategoryModel topSequenceVariantValueCategory = null;

		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.FIND_TOP_VARIANTVAUECATEGORY_SEQUENCE);
		query.addQueryParameter("variantCategory", variantCategory);
		query.setResultClassList(Arrays.asList(VariantValueCategoryModel.class));
		LOGGER.debug("Query : {}", query);

		final SearchResult<VariantValueCategoryModel> queryResult = getFlexibleSearchService().search(query);

		if (queryResult.getTotalCount() > 0)
		{
			topSequenceVariantValueCategory = queryResult.getResult().get(0);
		}
		return topSequenceVariantValueCategory;
	}
}
