package com.simon.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.simon.core.dao.ExtProductDao;
import com.simon.core.enums.PriceType;
import com.simon.core.services.ExtProductService;



/**
 * ExtProductServiceImpl, extends {@link DefaultProductService} provides additional implementations of operations to
 * search {@code ProductModel}.
 */
public class ExtProductServiceImpl extends DefaultProductService implements ExtProductService
{

	private static final long serialVersionUID = 1L;

	@Resource
	private transient ExtProductDao extProductDao;

	@Resource
	private transient ConfigurationService configurationService;
	@Resource
	private transient UserService userService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getProductsForCodes(final CatalogVersionModel catalogVersion, final Set<String> codes)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		validateParameterNotNullStandardMessage("Product Codes", codes);
		return extProductDao.findProductsByCodes(catalogVersion, codes);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> getCheckedProductsHavingPriceRow(final PriceType priceType,
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage("Product Type", priceType);
		return extProductDao.findCheckedProductsHavingPriceRow(priceType, catalogVersion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> getUnApprovedProductsHavingImageAndMerchandizingCategory(
			final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		final int mediaCountInContainer = configurationService.getConfiguration().getInt("required.media.count", 1);
		final List<GenericVariantProductModel> variants = extProductDao
				.findUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion);
		return variants.stream()
				.filter(variant -> variant.getGalleryImages().stream()
						.anyMatch(mediaContainer -> (mediaContainer.getMedias().size() > mediaCountInContainer)))
				.collect(Collectors.toList());

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isFavorite(final ProductModel productModel)
	{
		boolean isFavorite = false;
		final UserModel user = userService.getCurrentUser();

		if (productModel instanceof GenericVariantProductModel && user instanceof CustomerModel
				&& !userService.isAnonymousUser(user))
		{
			final CustomerModel customer = (CustomerModel) user;
			final Set<ProductModel> productset = customer.getMyFavProduct();
			if (productset.contains(((GenericVariantProductModel) productModel).getBaseProduct()))
			{
				isFavorite = true;
			}
		}
		return isFavorite;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getProductsForCatalogVersion(final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		return extProductDao.findProductsByCatalogVersion(catalogVersion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getProductsWithoutMerchandizingCategory(final CatalogVersionModel catalogVersion)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		return extProductDao.findProductsWithoutMerchandizingCategory(catalogVersion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void makeProductsDisabled(final Set<String> productList)
	{
		final List<ProductModel> products = extProductDao.findProductByCodes(productList);
		for (final ProductModel product : products)
		{
			product.setApprovalStatus(ArticleApprovalStatus.DISABLED);
		}
		getModelService().saveAll(products);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getProductsForCatalogVersion(final CatalogVersionModel catalogVersion,
			final ArticleApprovalStatus approvalStatus)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		validateParameterNotNullStandardMessage("approval status", approvalStatus);
		return extProductDao.findProductsByCatalogVersion(catalogVersion, approvalStatus);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<GenericVariantProductModel> getProductsWithZeroStock(final CatalogVersionModel catalogVersion,
			final Date lastSuccessJobRun)
	{
		validateParameterNotNullStandardMessage(CatalogVersionModel._TYPECODE, catalogVersion);
		validateParameterNotNullStandardMessage("date", lastSuccessJobRun);
		return extProductDao.findProductsWithZeroStock(catalogVersion, lastSuccessJobRun);
	}
}
