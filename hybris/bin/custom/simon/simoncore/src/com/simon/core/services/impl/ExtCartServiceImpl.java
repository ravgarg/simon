package com.simon.core.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.stock.StockService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.dao.ExtCartDao;
import com.simon.core.enums.CallBackOrderEntryStatus;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.services.ExtCartService;
import com.simon.core.services.ExtProductService;


/**
 * This Service class is used to retrieve cart model
 */
public class ExtCartServiceImpl extends DefaultCartService implements ExtCartService
{
	private static final long serialVersionUID = 1L;

	@Resource
	private ExtCartDao extCartDao;

	@Resource
	private ExtProductService productService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private StockService stockService;
	@Resource
	private CronJobService cronJobService;
	@Resource
	private ConfigurationService configurationService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtCartServiceImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModel getCartByCartId(final String cartId)
	{
		return extCartDao.getCartByCartId(cartId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getCartCount()
	{
		int cartTotalCount = 0;

		if (hasSessionCart())
		{
			final CartModel cart = getSessionCart();

			for (final AbstractOrderEntryModel cartEntry : cart.getEntries())
			{
				cartTotalCount += cartEntry.getQuantity().intValue();
			}
		}
		return cartTotalCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void modifyStockForFailedProducts(final AbstractOrderModel cartModel)
	{
		cartModel.getAdditionalCartInfo().getRetailersInfo().stream()
				.filter(retailersInfo -> CollectionUtils.isNotEmpty(retailersInfo.getFailedProducts()))
				.forEach(retailersInfo -> retailersInfo.getFailedProducts().stream()
						.filter(failedProduct -> "out_of_stock".equalsIgnoreCase(failedProduct.getStatusReason())).forEach(
								failedProduct -> updateStockForProductCode(cartModel, failedProduct.getProductID(), false)));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void modifyStockForRemovedProducts(final AbstractOrderModel orderModel)
	{
		orderModel.getAdditionalCartInfo().getRetailersInfo().stream()
				.filter(retailersInfo -> CollectionUtils.isNotEmpty(retailersInfo.getRemovedProducts()))
				.forEach(retailersInfo -> retailersInfo.getRemovedProducts().stream()
						.forEach(removedProduct -> updateStockForProductCode(orderModel, removedProduct.getProductID(), true)));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateStockForProductCode(final AbstractOrderModel abstractOrderModel, final String productCode,
			final boolean isRemoved)
	{
		final Optional<AbstractOrderEntryModel> orderEntry = abstractOrderModel.getEntries().stream()
				.filter(entry -> entry.getProduct().getCode().equals(productCode)).findFirst();

		if (orderEntry.isPresent())
		{
			final AbstractOrderEntryModel entry = orderEntry.get();

			if (isRemoved)
			{
				entry.setCallBackOrderEntryStatus(CallBackOrderEntryStatus.CANCELLED);
				getModelService().save(entry);
			}

			final ProductModel product = orderEntry.get().getProduct();
			if (null != product)
			{
				final Set<StockLevelModel> stockLevels = new HashSet<>();
				stockLevels.addAll(stockService.getAllStockLevels(product));
				stockLevels.stream().forEach(stockLevel -> {
					stockLevel.setAvailable(0);
					stockLevel.setReserved(0);
				});
				getModelService().saveAll(stockLevels);
			}
		}
	}

	/**
	 * Checks whether create call back has been received or not
	 *
	 * @param cart
	 * @return
	 */
	@Override
	public boolean hasReceivedUpdatedCreateCallback(final CartModel cart)
	{
		getModelService().refresh(cart);
		final AdditionalCartInfoModel additionalCart = cart.getAdditionalCartInfo();
		boolean updatedCreateCallbackReceived = false;
		if (null != additionalCart)
		{
			if (!configurationService.getConfiguration().getBoolean("esb.create.cart.api.enable.stub.flag"))
			{
				if (null != cart.getCreateCartInvocationTime())
				{
					updatedCreateCallbackReceived = cart.getCreateCartInvocationTime()
							.equalsIgnoreCase(additionalCart.getCreateCartInvocationTime());
					LOGGER.info("last create call start time {} \n last cretae call status received at {}",
							cart.getCreateCartInvocationTime(), additionalCart.getCreateCartInvocationTime());
				}
				else
				{
					LOGGER.error(
							" cart.getCreateCartInvocationTime() is not expected to be null when create cart call is not stubbed, ");
				}
			}
			else
			{
				updatedCreateCallbackReceived = true;
			}
		}
		return updatedCreateCallbackReceived;
	}
}
