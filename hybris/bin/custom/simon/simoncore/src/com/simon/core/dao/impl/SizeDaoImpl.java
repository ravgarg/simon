package com.simon.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.SizeDao;
import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.model.SizeModel;


/**
 * The Class SizeDaoImpl. DB based default implementation of {@link SizeDao}
 */
public class SizeDaoImpl extends AbstractItemDao implements SizeDao
{

	private static final Logger LOG = LoggerFactory.getLogger(SizeDaoImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SizeModel> getAllSizesByCode(final List<String> sizeCodes)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_ALL_SIZES_FOR_CODES);
		fQuery.addQueryParameter("codes", sizeCodes);

		final SearchResult<SizeModel> searchResult = getFlexibleSearchService().search(fQuery);

		return searchResult.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SizeModel getSizeForId(final String sizeCode)
	{
		ServicesUtil.validateParameterNotNull(sizeCode, "size code cannot be null");
		SizeModel sizeModel = null;

		try
		{
			final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_SIZE_BY_SIZE_CODE_QUERY);
			fQuery.addQueryParameter("code", sizeCode);
			final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
			if (CollectionUtils.isNotEmpty(results))
			{
				sizeModel = (SizeModel) results.get(0);
			}
		}
		catch (final RuntimeException exception)
		{
			LOG.error("Error occured when finding the size model in the database ", exception);
		}
		return sizeModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SizeModel> findAllSizes()
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_ALL_SIZES);

		final SearchResult<SizeModel> searchResult = getFlexibleSearchService().search(fQuery);

		return searchResult.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductSizeSequenceModel getSequenceForSizes(final String sizeCode)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_SEQUENCE_FOR_SIZE);
		fQuery.addQueryParameter("size", sizeCode);
		final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
		if (CollectionUtils.isNotEmpty(results))
		{
			return (ProductSizeSequenceModel) results.get(0);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductSizeSequenceModel> getAllSequencesForSizes()
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_ALL_SEQUENCE_FOR_SIZE);
		final SearchResult<ProductSizeSequenceModel> searchResult = getFlexibleSearchService().search(fQuery);
		if (null != searchResult && CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			return searchResult.getResult();
		}
		return Collections.EMPTY_LIST;
	}
}
