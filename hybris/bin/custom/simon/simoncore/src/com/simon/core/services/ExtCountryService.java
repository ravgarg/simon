package com.simon.core.services;

import de.hybris.platform.core.model.c2l.CountryModel;


/**
 * Service providing country .
 */
public interface ExtCountryService
{
	/**
	 * Returns the country matching the given external Id
	 *
	 * @param externalId
	 * @return CountryModel
	 */
	public CountryModel getCountryForExternalId(String externalId);
}
