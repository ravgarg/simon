package com.simon.core.services;

import java.util.List;

import com.simon.core.model.DealModel;


/**
 * DealService service for {@link DealModel}. Contains abstract methods for operations on {@link DealModel}
 */
public interface ExtDealService
{

	/**
	 * Gets the deal for code. Returns matching deal for <code>code</code>
	 *
	 * @param code
	 *           the deal <code>code</code>
	 * @return the {@link DealModel} for deal <code>code</code>
	 */
	DealModel getDealForCode(String code);

	/**
	 * Return list of all deals
	 *
	 * @return List<DealModel>
	 */
	List<DealModel> findListOfDeals();

	/**
	 * Gets the list of dealsModel. Returns matching deal for <code>code</code>
	 *
	 * @param objects
	 *           the deal <code>code</code>
	 * @return the {@link dealModel} for deal <code>code</code>
	 */
	List<DealModel> getListOfDealsForCodes(List<String> objects);
}
