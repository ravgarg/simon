package com.simon.core.services.order;

import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;


/**
*
*/
public interface ExtCommerceCheckoutService extends CommerceCheckoutService
{

	/**
	 * This method populates shipping and totalSalesTax from response DTO to {@link ShippingDetailsModel} of different
	 * {@link RetailerInfoModel} ant latter's totalSalesTax which are present in {@link AdditionalCartInfoModel}
	 *
	 * @param list
	 * @param responseDTO
	 */
	void populateShippingAndTax(final List<RetailersInfoModel> list, final CartEstimateResponseDTO responseDTO);


	/**
	 * Marks selected true for the {@link ShippingDetailsModel} for which request is sent to the integration layer and
	 * marks rest of it as false
	 *
	 * @param selectedShippingMethod
	 * @param retailerInfoList
	 */
	void selectShippingForEstimateCall(String selectedShippingMethod, List<RetailersInfoModel> retailerInfoList);

	/**
	 * @param deliveryMethodId
	 * @param retailersInfoList
	 * @param cart
	 */
	void setDefaultDeliveryModePerRetailer(String deliveryMethodId, List<RetailersInfoModel> retailersInfoList, CartModel cart);

	/**
	 * @param retailer
	 * @param deliveryMethodId
	 * @param cartModel
	 */
	void setShippingForRetailer(RetailersInfoModel retailer, String deliveryMethodId, CartModel cartModel);
}
