package com.simon.core.solrfacetsearch.config.factories.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.factories.impl.DefaultFlexibleSearchQuerySpecFactory;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import com.simon.core.solrfacetsearch.config.factories.ExtFlexibleSearchQuerySpecFactory;


/**
 * Custom Create Flexible Search query for getting the lastIndexTime as param
 */
public class ExtFlexibleSearchQuerySpecFactoryImpl extends DefaultFlexibleSearchQuerySpecFactory
		implements ExtFlexibleSearchQuerySpecFactory
{

	private static final String LAST_INDEX_TIME_QUERY_PARAM = "lastIndexTime";
	protected static final String GET_CATEGORIES_FOR_UPDATED_RULE = "SELECT {pk} FROM {CatForPromotionSourceRule As catPromoRule} WHERE  {catPromoRule:rule} IN ({{SELECT {promo:pk} FROM {PromotionSourceRule As promo} WHERE {promo:modifiedtime} >= ?lastIndexTime}})";

	/**
	 * Fetched the categories for which the promotions got updated
	 *
	 * @param indexedType
	 * @param facetSearchConfig
	 * @return FlexiQuery for getting the category codes for which the promotions are updated recently
	 * @throws SolrServiceException
	 */
	@Override
	public FlexibleSearchQuery fetchCategoriesForUpdatedPromotions(final IndexedType indexedType,
			final FacetSearchConfig facetSearchConfig) throws SolrServiceException
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(GET_CATEGORIES_FOR_UPDATED_RULE);
		query.addQueryParameter(LAST_INDEX_TIME_QUERY_PARAM, this.getLastIndexTime(facetSearchConfig, indexedType));
		return query;
	}
}