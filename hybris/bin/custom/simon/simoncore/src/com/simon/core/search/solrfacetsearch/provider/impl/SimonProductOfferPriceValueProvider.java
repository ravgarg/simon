package com.simon.core.search.solrfacetsearch.provider.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.facades.product.OfferFacade;


/**
 * Simon Product Offer Price Value Provider. This provider provides implementation to index price for products.
 */
public class SimonProductOfferPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{

	/** The LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SimonProductOfferPriceValueProvider.class);

	/** The field name provider. */
	private FieldNameProvider fieldNameProvider;

	/** The offer facade. */
	private OfferFacade offerFacade;

	/**
	 * Index Offer Price for Product. This method fetches offer price for product using {@link OfferFacade}. If no offer
	 * is present for that product a price of zero is indexed for that product, else price is extracted from the
	 * associated offer and indexed.
	 *
	 * @param indexConfig
	 *           the index config
	 * @param indexedProperty
	 *           the indexed property
	 * @param model
	 *           the model
	 * @return Collection<FieldValue>
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{

		final Collection<FieldValue> fieldValues = new ArrayList<>();
		try
		{
			validateParameterNotNull(model, "No model(product) found in request context");

			checkModel(model);

			final ProductModel productModel = (ProductModel) model;

			LOGGER.trace("product.code: '{}'", productModel.getCode());

			final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, "USD");

			final List<OfferData> offers = offerFacade.getOffersForProductCode(productModel.getCode());

			if (offers.isEmpty())
			{
				addFieldValues(fieldValues, 0d, fieldNames);

				LOGGER.debug("Offers are empty for product: '{}' / offers.size(): '{}'.", productModel.getCode(), offers.size());
			}
			else
			{
				final OfferData offer = offers.get(0);

				LOGGER.debug("Offers.size(): '{}'. This offer set is for product code: '{}'.", offers.size(), offer.getProductCode());

				final PriceData priceData = offer.getPrice();

				addFieldValues(fieldValues, priceData.getValue().doubleValue(), fieldNames);
			}
		}
		catch (final Exception e)
		{
			throw new FieldValueProviderException("Failed to generate volume price, Cannot evaluate " + indexedProperty.getName()
					+ " using " + this.getClass().getName(), e);
		}

		return fieldValues;
	}

	/**
	 * Adding Filed value in collection.
	 *
	 * @param fieldValues
	 *           the field values
	 * @param value
	 *           the value
	 * @param fieldNames
	 *           the field names
	 */
	protected void addFieldValues(final Collection<FieldValue> fieldValues, final Double value,
			final Collection<String> fieldNames)
	{
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
	}

	/**
	 * Checking the model is instance of Product model or not.
	 *
	 * @param model
	 *           the model
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	protected void checkModel(final Object model) throws FieldValueProviderException
	{
		if (!(model instanceof ProductModel))
		{
			throw new FieldValueProviderException("Cannot evaluate price of non-product item");
		}
	}

	/**
	 * Gets the field name provider.
	 *
	 * @return the fieldNameProvider
	 */
	public FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	/**
	 * Sets the field name provider.
	 *
	 * @param fieldNameProvider
	 *           the fieldNameProvider to set
	 */
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * Gets the offer facade.
	 *
	 * @return the offerFacade
	 */
	public OfferFacade getOfferFacade()
	{
		return offerFacade;
	}

	/**
	 * Sets the offer facade.
	 *
	 * @param offerFacade
	 *           the offerFacade to set
	 */
	public void setOfferFacade(final OfferFacade offerFacade)
	{
		this.offerFacade = offerFacade;
	}
}
