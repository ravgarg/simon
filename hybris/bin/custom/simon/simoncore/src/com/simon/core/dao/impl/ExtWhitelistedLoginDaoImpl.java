package com.simon.core.dao.impl;

import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtWhitelistedLoginDao;
import com.simon.core.model.WhitelistedUserModel;


/**
 * This dao class is used to retrieve betaUser model.
 */
public class ExtWhitelistedLoginDaoImpl extends AbstractItemDao implements ExtWhitelistedLoginDao
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhitelistedUserModel findUserByEmail(final String email)
	{
		final FlexibleSearchQuery fQuery = getFlexibleSearchQuery(SimonFlexiConstants.WHITELISTEDUSER_BY_EMAIL_QUERY);
		fQuery.addQueryParameter("email", email);
		final List<Object> results = getFlexibleSearchService().search(fQuery).getResult();
		if (CollectionUtils.isNotEmpty(results))
		{
			return (WhitelistedUserModel) results.get(0);
		}
		return null;
	}

	protected FlexibleSearchQuery getFlexibleSearchQuery(final String code)
	{
		return new FlexibleSearchQuery(code);
	}
}
