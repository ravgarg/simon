package com.simon.core.mirakl.populators;

import static org.apache.commons.collections.CollectionUtils.isEmpty;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.domain.additionalfield.MiraklAdditionalFieldType;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue.MiraklAbstractAdditionalFieldWithSingleValue;
import com.mirakl.client.mmp.domain.order.MiraklOrderLine;
import com.mirakl.hybris.core.enums.MiraklOrderLineStatus;
import com.mirakl.hybris.core.ordersplitting.populators.MiraklUpdateConsignmentEntryPopulator;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.ConsignmentEntryCustomFields;


/**
 * By This Populator setting new additional field value. if having additional field value then that value will be set
 * either setting from source status field
 */
public class ExtMiraklUpdateConsignmentEntryPopulator extends MiraklUpdateConsignmentEntryPopulator
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtMiraklUpdateConsignmentEntryPopulator.class);

	@Override
	public void populate(final MiraklOrderLine source, final ConsignmentEntryModel target) throws ConversionException
	{
		populateAdditionalValue(source, target);

		if (source.getId() != null)
		{
			target.setMiraklOrderLineId(source.getId());
		}
		if (source.getStatus() != null && shippingStates.contains(source.getStatus().getState()))
		{
			target.setShippedQuantity((long) source.getQuantity());
		}
		target.setCanOpenIncident(source.getCanOpenIncident());
		target.setMiraklOrderLineId(source.getId());

		populateRefunds(source, target);
	}

	private void populateAdditionalValue(final MiraklOrderLine source, final ConsignmentEntryModel target)
	{
		target.setMiraklOrderLineStatus(MiraklOrderLineStatus.PROCESSING);
		if (isEmpty(source.getAdditionalFields()))
		{
			return;
		}
		final List<MiraklAdditionalFieldValue> miraklAdditionalFieldValue = source.getAdditionalFields();

		if (CollectionUtils.isNotEmpty(miraklAdditionalFieldValue))
		{
			for (final MiraklAdditionalFieldValue additionalFieldValue : miraklAdditionalFieldValue)
			{
				if (!MiraklAdditionalFieldType.MULTIPLE_VALUES_LIST.equals(additionalFieldValue.getFieldType()))
				{
					populateAddtionalSingleValue(target, additionalFieldValue);
				}
			}
		}

	}

	private void populateAddtionalSingleValue(final ConsignmentEntryModel target,
			final MiraklAdditionalFieldValue additionalFieldValue)
	{
		final MiraklAbstractAdditionalFieldWithSingleValue singleAdditionalFieldValue = (MiraklAbstractAdditionalFieldWithSingleValue) additionalFieldValue;
		final String rawValue = isNotEmpty(singleAdditionalFieldValue.getValue());

		switch (singleAdditionalFieldValue.getCode())
		{
			case ConsignmentEntryCustomFields.ORDER_LINE_STATUS:
				setStatusInLineItems(rawValue, target);
				break;
			case ConsignmentEntryCustomFields.SHIP_DATE:
				target.setShipDate(getDateObjectFromString(rawValue));
				break;
			case ConsignmentEntryCustomFields.CARRIER_CODE:
				target.setCarrierCode(rawValue);
				break;
			case ConsignmentEntryCustomFields.CARRIER_NAME:
				target.setCarrierName(rawValue);
				break;
			case ConsignmentEntryCustomFields.CARRIER_URL:
				target.setCarrierURL(rawValue);
				break;
			case ConsignmentEntryCustomFields.TRACKING_NUMBER:
				target.setTrackingNo(rawValue);
				break;
			case ConsignmentEntryCustomFields.CANCELLATION_REASON:
				target.setCancellationReason(rawValue);
				break;
			case ConsignmentEntryCustomFields.CANCELLATION_DATE:
				target.setCancellationDate(getDateObjectFromString(rawValue));
				break;
			case ConsignmentEntryCustomFields.REFUND_REASON:
				target.setRefundReason(rawValue);
				break;
			case ConsignmentEntryCustomFields.REFUND_DATE:
				target.setRefundDate(getDateObjectFromString(rawValue));
				break;
			case ConsignmentEntryCustomFields.PRODUCT_AMOUNT:
				target.setProductAmount(Double.parseDouble(rawValue));
				break;
			case ConsignmentEntryCustomFields.SHIPPING_AMOUNT:
				target.setShippingAmount(Double.parseDouble(rawValue));
				break;
			case ConsignmentEntryCustomFields.TAX_AMOUNT:
				target.setTaxAmount(Double.parseDouble(rawValue));
				break;
			default:
				LOGGER.debug("Custom Order Line::Invalid Additional Field '{}' Recevied. Ignoring",
						singleAdditionalFieldValue.getCode());
				break;
		}
	}

	private String isNotEmpty(final String rawValue)
	{
		String result = StringUtils.EMPTY;
		if (StringUtils.isNotEmpty(rawValue))
		{
			result = rawValue;
		}
		return result;
	}

	/**
	 * By this method setting normal field value.
	 *
	 * @param miraklAdditionalFieldValue
	 *
	 * @param MiraklOrderLine
	 * @param ConsignmentEntryModel
	 */
	private void setStatusInLineItems(final String lineItemStatus, final ConsignmentEntryModel target)
	{
		if (MiraklOrderLineStatus.SHIPPED.toString().equalsIgnoreCase(lineItemStatus))
		{
			target.setMiraklOrderLineStatus(MiraklOrderLineStatus.SHIPPED);
		}
		else if (MiraklOrderLineStatus.CANCELED.toString().equalsIgnoreCase(lineItemStatus)
				|| MiraklOrderLineStatus.CANCELLED.toString().equalsIgnoreCase(lineItemStatus))
		{
			target.setMiraklOrderLineStatus(MiraklOrderLineStatus.CANCELLED);
		}
		else if (MiraklOrderLineStatus.RETURNED.toString().equalsIgnoreCase(lineItemStatus)
				|| MiraklOrderLineStatus.REFUNDED.toString().equalsIgnoreCase(lineItemStatus))
		{
			target.setMiraklOrderLineStatus(MiraklOrderLineStatus.RETURNED);
		}
		else
		{
			target.setMiraklOrderLineStatus(MiraklOrderLineStatus.PROCESSING);
		}
	}

	/**
	 * creation date of the order, value format will be yyyy-MM-dd'T'hh:mm:ss+00 e.g. 2017-06-15T10:45:53+05 (+nn is the
	 * timezone relative to GMT)
	 *
	 * @param stringDate
	 * @return Date
	 */
	private Date getDateObjectFromString(final String stringDate)
	{
		Date date = null;
		try
		{
			date = DateUtils.parseDate(getDateString(stringDate), Locale.US, "yyyy-MM-dd'T'hh:mm:ss Z");
		}
		catch (final ParseException e)
		{
			LOGGER.error("Error Occured in Parsing Date::", e);
		}
		return date;
	}

	private String getDateString(final String stringDate)
	{
		final StringBuilder sb = new StringBuilder();
		if (stringDate.contains(SimonCoreConstants.PLUS_SIGN) && stringDate.length() == SimonCoreConstants.TWENTY_TWO)
		{
			final String[] arrayStr = stringDate.split("\\" + SimonCoreConstants.PLUS_SIGN);
			sb.append(arrayStr[SimonCoreConstants.ZERO_INT]);
			sb.append(SimonCoreConstants.SPACE);
			sb.append(SimonCoreConstants.PLUS_SIGN);
			sb.append(arrayStr[SimonCoreConstants.FIRST_INT].length() == SimonCoreConstants.TWO_INT
					? arrayStr[SimonCoreConstants.FIRST_INT] + SimonCoreConstants.DOUBLE_ZERO
					: arrayStr[SimonCoreConstants.FIRST_INT]);
		}
		else
		{
			sb.append(stringDate.length() >= SimonCoreConstants.NINETEEN_INT
					? stringDate.substring(SimonCoreConstants.ZERO_INT, SimonCoreConstants.NINETEEN_INT) + SimonCoreConstants.SPACE
							+ SimonCoreConstants.PLUS_SIGN + SimonCoreConstants.DOUBLE_ZERO + SimonCoreConstants.DOUBLE_ZERO
					: StringUtils.EMPTY);
		}
		return sb.toString();
	}

}
