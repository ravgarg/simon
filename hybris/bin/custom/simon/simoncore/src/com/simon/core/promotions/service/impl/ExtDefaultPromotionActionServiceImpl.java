package com.simon.core.promotions.service.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotionengineservices.promotionengine.impl.DefaultPromotionActionService;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryConsumedRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.util.DiscountValue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.simon.core.promotions.service.ExtDefaultPromotionActionService;
import com.simon.core.util.CommonUtils;


/**
 * This class acts as a service for retailer specific discounts for retailer level promotion
 */
public class ExtDefaultPromotionActionServiceImpl extends DefaultPromotionActionService
		implements ExtDefaultPromotionActionService
{
	/**
	 * {@inheritDoc}
	 **/
	@Override
	public void createRetailerDiscountValue(final DiscountRAO discountRao, final String code, final AbstractOrderModel order)
	{
		final boolean isAbsoluteDiscount = discountRao.getCurrencyIsoCode() != null;
		final String retailer = discountRao.getRetailer().getRetailerId();
		final DiscountValue discountValue = new DiscountValue(code, discountRao.getValue().doubleValue(), isAbsoluteDiscount,
				order.getCurrency().getIsocode());
		final Map<String, List<DiscountValue>> retailersDVsMap = new HashMap<>(order.getSubbagDiscountValues());
		final List<DiscountValue> retailersDVs = CommonUtils.createArrayListFrom(retailersDVsMap.get(retailer));
		retailersDVs.add(discountValue);
		retailersDVsMap.put(retailer, retailersDVs);
		order.setSubbagDiscountValues(retailersDVsMap);
	}

	/**
	 * {@inheritDoc}
	 **/
	@Override
	public AbstractOrderEntryModel getConsumedOrderEntry(final AbstractRuleActionRAO action)
	{
		OrderEntryRAO orderEntry = null;
		final Optional<OrderEntryConsumedRAO> orderEntryOption = action.getConsumedEntries().stream()
				.filter(consumedEntry -> consumedEntry.getFiredRuleCode().equals(action.getFiredRuleCode())).findAny();
		if (orderEntryOption.isPresent())
		{
			orderEntry = orderEntryOption.get().getOrderEntry();
		}
		return invokeOrderEntry(orderEntry);
	}

	/**
	 * {@inheritDoc}
	 **/
	@Override
	public void createDiscountValueForBuyMoreSaveMore(final DiscountRAO discountRao, final String code,
			final AbstractOrderEntryModel orderEntry)
	{
		final boolean isAbsoluteDiscount = discountRao.getCurrencyIsoCode() != null;
		final DiscountValue discountValue = new DiscountValue(code,
				discountRao.getValue().doubleValue() / orderEntry.getQuantity().doubleValue(), isAbsoluteDiscount,
				orderEntry.getOrder().getCurrency().getIsocode());

		final List<DiscountValue> globalDVs = CommonUtils.createArrayListFrom(orderEntry.getDiscountValues());
		globalDVs.add(discountValue);
		orderEntry.setDiscountValues(globalDVs);
		orderEntry.setCalculated(Boolean.FALSE);
	}

	/**
	 * This method invokes getOrderEntry of parent
	 *
	 * @param orderEntry
	 * @return
	 */
	protected AbstractOrderEntryModel invokeOrderEntry(final OrderEntryRAO orderEntry)
	{
		return super.getOrderEntry(orderEntry);
	}

	/**
	 * this method set promocode in RuleBasedPromotionModel and return the value, first it check if any fired promotion
	 * is there if it exist than it get promocode from abstractRuleModel and set into promotionModel.
	 *
	 *
	 * return promotionModel
	 */
	@Override
	protected RuleBasedPromotionModel getPromotion(final AbstractRuleActionRAO abstractRao)
	{
		RuleBasedPromotionModel promotionModel = null;
		if (Objects.nonNull(abstractRao) && Objects.nonNull(abstractRao.getFiredRuleCode()))
		{
			final AbstractRuleEngineRuleModel rule = this.getRules(abstractRao);
			if (Objects.nonNull(rule))
			{
				promotionModel = rule.getPromotion();
				final AbstractRuleModel abstractRuleModel = rule.getSourceRule();
				if (Objects.nonNull(abstractRuleModel) && Objects.nonNull(promotionModel))
				{
					promotionModel.setPromoCode(abstractRuleModel.getPromoCode());
					savePromotionModel(promotionModel);
				}
			}
		}
		return promotionModel;
	}

	protected AbstractRuleEngineRuleModel getRules(final AbstractRuleActionRAO abstractRao)
	{
		return this.getRule(abstractRao);
	}

	protected void savePromotionModel(final RuleBasedPromotionModel promotionModel)
	{
		getModelService().save(promotionModel);
	}

}
