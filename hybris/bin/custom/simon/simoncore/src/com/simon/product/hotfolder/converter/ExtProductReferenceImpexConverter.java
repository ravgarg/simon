package com.simon.product.hotfolder.converter;

import de.hybris.platform.acceleratorservices.dataimport.batch.converter.impl.DefaultImpexConverter;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;





public class ExtProductReferenceImpexConverter extends DefaultImpexConverter
{

	@Override
	public String convert(final Map<Integer, String> row, final Long sequenceId)
	{
		final StringBuilder builder = new StringBuilder();
		final String[] targets = row.get(1).split("\\|");
		for (final String target : targets)
		{
			row.put(1, target);
			builder.append(super.convert(row, sequenceId)).append(StringUtils.LF);

		}
		return builder.toString();
	}

}
