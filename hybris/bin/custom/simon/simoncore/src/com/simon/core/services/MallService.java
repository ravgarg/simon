package com.simon.core.services;

import java.util.List;
import java.util.Set;

import com.simon.core.model.MallModel;


/**
 * MallService service for {@link MallModel}. Contains abstract methods for operations on {@link MallModel}
 */
public interface MallService
{
	/**
	 * Gets the Mall for code. Returns matching mall for <code>code</code>
	 *
	 * @param code
	 *           the Mall <code>code</code>
	 * @return the {@link MallModel} for mall <code>code</code>
	 */
	MallModel getMallForCode(String code);

	/**
	 * Gets the List of Malls.
	 *
	 *
	 * @return the {@link List<MallModel>}
	 */
	List<MallModel> getAllMalls();

	/**
	 * Gets the List of Malls for the code matching for <code> mallCodes </code>
	 *
	 * @param mallCodes
	 *           <code> mallCodes </code>
	 * @return the {@link List<MallModel>} for <code> mallCodes </code>
	 */
	Set<MallModel> getMallListForCode(List<String> mallCodes);
}
