package com.simon.core.dao;

import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.simon.core.model.DealModel;


/**
 * Dao for {@link DealModel} contains abstract method for operations on {@link DealModel}
 */
public interface ExtDealDao extends Dao
{

	/**
	 * Find deal by code. Returns {@link DealModel} for given deal <code>code</code>.
	 *
	 * @param code
	 *           the deal <code>code</code>
	 * @return the deal model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code>
	 */
	DealModel findDealByCode(String code);

	/**
	 * Find all Deals . Returns {@link List<DealModel>}.
	 *
	 * @return a list of Deal Model
	 */
	List<DealModel> findAllDeals();


	/**
	 * Find list of deal by code string. Returns {@link DealModel} for given deal <code>code</code>.
	 *
	 * @param objects
	 *           the deal <code>code</code>
	 * @return the deal model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code>
	 */
	List<DealModel> findListOfDealForCodes(List<String> objects);

}
