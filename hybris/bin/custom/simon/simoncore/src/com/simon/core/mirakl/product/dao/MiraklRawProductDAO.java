package com.simon.core.mirakl.product.dao;

import java.util.List;

import com.mirakl.hybris.core.model.MiraklRawProductModel;


/**
 *
 */
public interface MiraklRawProductDAO
{
	/**
	 * Fetch Mirakl products for a specific import id
	 * 
	 * @param importId
	 * @return
	 */
	public List<MiraklRawProductModel> fetchMiraklProductsForImport(final String importId);
}
