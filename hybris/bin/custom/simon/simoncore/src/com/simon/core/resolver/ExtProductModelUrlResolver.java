package com.simon.core.resolver;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultProductModelUrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.MerchandizingCategoryModel;


/**
 * This class extends DefaultProductModelUrlResolver.
 *
 * Makes URL to be set in Product. DefaultPattern has been updated to fulfill our category hierarchy structure.
 */
public class ExtProductModelUrlResolver extends DefaultProductModelUrlResolver
{
	@Resource
	private SessionService sessionService;

	@Value("${navigation.home.category.code}")
	protected String homeCategoryCode;

	/**
	 * Overridden method to support our updated pattern. If categoryCodePath is not set in session, we make the complete
	 * category path from DB
	 *
	 * In Pattern complete category hierarchy is set after /catcodes/ and before /p/
	 */
	@Override
	protected String resolveInternal(final ProductModel source)
	{
		final ProductModel baseProduct = getBaseProduct(source);

		String url = getPattern();

		final List<CategoryModel> path = getCategoryPathFromProduct(baseProduct);

		final String catCodePath = Objects.toString(sessionService.getAttribute(SimonCoreConstants.CATEGORY_CODE_PATH),
				buildCategoryCodePathString(path));
		if (url.contains("{category-codes}"))
		{
			if (StringUtils.isEmpty(catCodePath))
			{
				url = url.replace("/catcodes/{category-codes}", "");
			}
			else
			{
				url = url.replace("{category-codes}", urlEncode(catCodePath));
			}

		}
		if (url.contains("{category-path}"))
		{
			url = url.replace("{category-path}", buildPathString(path).toLowerCase());
		}

		if (url.contains("{product-name}"))
		{
			final String productName = baseProduct.getName() != null ? baseProduct.getName().toLowerCase() : baseProduct.getName();
			url = url.replace("{product-name}", urlSafe(productName));
		}
		if (url.contains("{product-code}"))
		{
			url = url.replace("{product-code}", urlEncode(source.getCode()));
		}

		return url;
	}

	/**
	 * Method to call super class category path method
	 *
	 * @param source
	 * @return
	 */
	protected List<CategoryModel> getCategoryPathFromProduct(final ProductModel source)
	{
		return getCategoryPath(source);
	}

	/**
	 * The method makes complete category code hierarchy path, is categoryCodePath value is not set in session
	 *
	 * @param path
	 *           Current category model path
	 * @return category hierarchy path
	 */
	protected String buildCategoryCodePathString(final List<CategoryModel> path)
	{
		final StringBuilder result = new StringBuilder();
		for (int i = 0; i < path.size(); i++)
		{
			if (i != 0)
			{
				result.append(SimonCoreConstants.UNDERSCORE);
			}
			result.append(path.get(i).getCode());
		}

		return StringUtils.contains(result.toString(), homeCategoryCode)
				? result.substring(result.indexOf(homeCategoryCode) + homeCategoryCode.length() + 1) : StringUtils.EMPTY;
	}

	/**
	 * Overridden method to fetch only Merchandizing category from Base Product
	 */
	@Override
	protected CategoryModel getPrimaryCategoryForProduct(final ProductModel product)
	{
		// Get the first super-category from the product that isn't a classification category
		for (final CategoryModel category : product.getSupercategories())
		{
			if ((category instanceof MerchandizingCategoryModel))
			{
				return category;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden method to achieve correct URL for product pages.This will remove /spo/home from all the urls.
	 *
	 */
	@Override
	protected String buildPathString(final List<CategoryModel> path)
	{
		final CategoryModel homeCategory = getCommerceCategoryService().getCategoryForCode(homeCategoryCode);
		final List<Object> merchandizingCategories = new ArrayList<>(Arrays.asList(homeCategory));
		merchandizingCategories.addAll(homeCategory.getSupercategories());
		path.removeAll(merchandizingCategories);
		return super.buildPathString(path);
	}

}
