package com.simon.core.mirakl.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;
import static java.util.Collections.singletonMap;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.google.common.collect.Iterables;
import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.enums.PriceType;
import com.simon.core.mirakl.dao.ExtOfferDao;


/**
 * ExtOfferDaoImpl db based implementation of {@link ExtOfferDao}. Implements the operations defined in
 * <code>ExtOfferDao</code> via {@link FlexibleSearchService}
 */
public class ExtOfferDaoImpl extends AbstractItemDao implements ExtOfferDao
{

	private static final int DEFAULT_BATCH_SIZE = 50;

	@Resource
	private ConfigurationService configurationService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PriceRowModel> findPricesModifiedBeforeDate(final Date modificationDate)
	{
		validateParameterNotNullStandardMessage("modificationDate", modificationDate);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRICES_MODIFIED_BEFORE_DATE,
				singletonMap(PriceRowModel.MODIFIEDTIME, modificationDate));
		final SearchResult<PriceRowModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<StockLevelModel> findStocksModifiedBeforeDate(final Date modificationDate)
	{
		validateParameterNotNullStandardMessage("modificationDate", modificationDate);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_STOCKS_MODIFIED_BEFORE_DATE,
				singletonMap(StockLevelModel.MODIFIEDTIME, modificationDate));
		final SearchResult<StockLevelModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PriceRowModel> findPricesForProductCodes(final Set<String> productIdsList, final PriceType priceType)
	{
		validateParameterNotNullStandardMessage("prodIds", productIdsList);
		validateParameterNotNullStandardMessage("priceType", priceType);

		final List<PriceRowModel> results = new ArrayList<>();
		final Iterable<List<String>> partitions = Iterables.partition(productIdsList,
				configurationService.getConfiguration().getInt("fetchprice.batchsize", DEFAULT_BATCH_SIZE));

		for (final List<String> productCodes : partitions)
		{
			final Map<String, Object> queryArguments = new HashMap<>();
			queryArguments.put("prodIds", productCodes);
			queryArguments.put("priceType", priceType);

			final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_PRICES_FOR_PRODUCT_CODES,
					queryArguments);
			final SearchResult<PriceRowModel> result = getFlexibleSearchService().search(query);
			results.addAll(result.getResult());
		}
		return results;
	}
}
