package com.simon.core.services;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.OrderService;

import java.util.List;
import java.util.Set;

import com.simon.core.dto.OrderExportData;
import com.simon.integration.dto.OrderConfirmCallBackDTO;


/**
 * This Service Interface is used to retrieve order model
 */
public interface ExtOrderService extends OrderService
{

	/**
	 * @description Method call to get Order by orderId. This is a callback method used to save additional cart model in
	 *              the found order model.
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return AdditionalCartInfoModel
	 */
	OrderModel getOrderByOrderId(final String orderId);

	/**
	 * @description This method will store the Order Id received per retailer into the respective Consignments.
	 * @method processPurchaseConfirmCallback
	 * @param consignments
	 * @param orderConfirmCallBackDTO
	 */

	void setOrderIdPerRetailer(OrderModel orderModel, OrderConfirmCallBackDTO orderConfirmCallBackDTO);

	/**
	 * @description Returns true/false when order should be accepted or rejected after checking the discrepancy in
	 *              configuration
	 * @method checkAcceptOrRejectTheOrder
	 * @param orderModel
	 * @return boolean
	 */
	boolean checkAcceptOrRejectTheOrder(OrderModel orderModel);


	/**
	 * This method will return the list of the retailers in an order
	 *
	 * @param orderModel
	 *           {@link OrderModel} for which the retailer list is to be returned
	 * @return retailerList list of the retailer's names
	 */
	Set<String> getRetailerList(OrderModel orderModel);

	/**
	 *
	 * @return {@link List} of {@link OrderModel} - matched orders
	 */
	List<OrderModel> findOrdersToExport(int day, String startHours, String endHours);

	/**
	 *
	 * @param orderList
	 * @return {@link List} of {@link OrderExportData} - matched orders
	 */
	boolean createOrderExportFiles(List<OrderModel> orderList);

	/**
	 * This is an utility method to create csv file. By this method create csv file for Order Status
	 *
	 * @param retailerPrefix
	 * @param orderList
	 */
	void createFileForStatus(List<OrderExportData> orderList, String retailerPrefix);

	/**
	 * This is an utility method to make data for csv file. By this method making csv data for Order Status
	 *
	 * @param orderExportDataList
	 * @return List<String[]>
	 */
	List<String[]> orderStatusFile(List<OrderExportData> orderExportDataList);

	/**
	 * This is an utility method to create csv file. By this method creating csv file for BOT Order export
	 *
	 * @param orderList
	 * @param retailerId
	 */
	void createFileForBotOrderEngine(List<OrderExportData> orderList, String retailerId);

	/**
	 * This is an utility method. By this method making csv data for BOT Order export
	 *
	 * @param orderExportDataList
	 * @return List<String[]>
	 */
	List<String[]> orderFileForBOTEngine(List<OrderExportData> orderExportDataList);

	/**
	 * This is an utility method. By this method move Order Export CSV File from NFS mount to SFTP
	 *
	 * @return boolean
	 */
	boolean moveOrderexportCsvfileTosftp();

	/**
	 * @description This method returns the versioned order
	 * @method getVersionedOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	OrderModel getVersionedOrderByOrderId(String orderId);
}
