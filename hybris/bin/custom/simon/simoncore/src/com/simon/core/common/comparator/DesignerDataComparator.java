package com.simon.core.common.comparator;

import java.io.Serializable;
import java.util.Comparator;

import com.simon.facades.account.data.DesignerData;


/**
 * THis is the comparator class to compare DesignerData on the basis of designeName
 */
public class DesignerDataComparator implements Comparator<DesignerData>, Serializable
{
	private static final long serialVersionUID = 8336243241698251806L;

	@Override
	public int compare(final DesignerData e1, final DesignerData e2)
	{
		return e1.getName().compareTo(e2.getName());
	}
}
