package com.simon.core.mirakl.services.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.services.impl.DefaultMiraklRawProductImportService;
import com.simon.core.caches.AbstractPreProcessor;
import com.simon.core.caches.ProductFileImportPreProcessor;

import shaded.org.supercsv.io.ICsvMapReader;


/**
 * ExtMiraklRawProductImportService extends {@link DefaultMiraklRawProductImportService} and adds additional feature of
 * executing pre processors for every line item.
 */
public class ExtMiraklRawProductImportService extends DefaultMiraklRawProductImportService
{
	private Map<String, List<ProductFileImportPreProcessor>> preProcessorsMap;

	/**
	 * Executes list of pre-processors <code>ProductFileImportPreProcessor</code> for every attribute defined in
	 * {@link #preProcessorsMap}
	 */
	@Override
	protected MiraklRawProductModel createRawProduct(final Map<String, String> productValues, final ICsvMapReader csvMapReader,
			final ProductImportFileContextData context, final String importUUID)
	{
		preProcessorsMap.forEach((attribute, preProcessors) -> preProcessors
				.forEach(preProcessor -> preProcessor.preProcess(attribute, productValues, context)));
		return super.createRawProduct(productValues, csvMapReader, context, importUUID);
	}

	/**
	 * Import raw products and Performs cache cleanup for each preprocesssor
	 */
	@Override
	public String importRawProducts(final File inputFile, final ProductImportFileContextData context)
	{
		final String importId = super.importRawProducts(inputFile, context);
		preProcessorsMap.forEach((attribute, preProcessors) -> preProcessors
				.forEach(preProcessor -> ((AbstractPreProcessor<?>) preProcessor).cleanup()));
		return importId;
	}

	/**
	 * Sets the import cache map.
	 *
	 * @param preProcessorsMap
	 *           the import cache map
	 */
	public void setImportCacheMap(final Map<String, List<ProductFileImportPreProcessor>> preProcessorsMap)
	{
		this.preProcessorsMap = preProcessorsMap;
	}
}
