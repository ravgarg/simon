package com.simon.core.customer.dao;

import de.hybris.platform.commerceservices.customer.dao.CustomerAccountDao;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;


/**
 * Dao for {@link CustomerAccountDao} contains abstract method for operations on {@link DesignerModel}
 */
public interface ExtCustomerAccountDao extends CustomerAccountDao
{
	/**
	 * Find list of all designers. Returns List of {@link DesignerModel} for user <code>code</code>.
	 *
	 * @param customerModel
	 *           the designer <code>customerModel</code>
	 * @return the list of designer model
	 *
	 */
	List<DesignerModel> findListOfAllDesigners(CustomerModel customerModel);

	/**
	 * Find Favorite For Customer. Returns {@link DesignerModel} for given designer <code>code</code>.
	 *
	 * @param customerModel
	 *
	 * @return List of Product model
	 *
	 */
	List<List<Object>> getMyFavProductOrderByDate(CustomerModel customerModel);

	/**
	 * @param duplicateFromAddressId
	 * @return list of AddressModel
	 */
	List<AddressModel> findAddressByDuplicateFrom(String duplicateFromAddressId);

	/**
	 * Find list of all stores. Returns List of {@link ShopModel} for user <code>code</code>.
	 *
	 * @param customerModel
	 *           the shop <code>customerModel</code>
	 * @return the list of shop model
	 *
	 */
	List<ShopModel> findListOfAllStores(CustomerModel customerModel);

	/**
	 * Find list of all designers. Returns List of {@link DesignerModel} for user <code>code</code>.
	 *
	 * @return the list of designer model
	 *
	 */
	List<DesignerModel> findListOfAllDesigners();

	/**
	 * Find list of all deals. Returns List of {@link DealModel} for user <code>code</code>.
	 *
	 * @param customerModel
	 *
	 * @return the list of deal model
	 *
	 */
	List<DealModel> findListOfAllDeals(CustomerModel customerModel);

}
