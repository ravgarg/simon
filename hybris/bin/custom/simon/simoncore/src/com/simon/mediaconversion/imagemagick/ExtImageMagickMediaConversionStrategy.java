package com.simon.mediaconversion.imagemagick;

import de.hybris.platform.mediaconversion.MediaConversionService;
import de.hybris.platform.mediaconversion.conversion.MediaConversionStrategy;
import de.hybris.platform.mediaconversion.imagemagick.ImageMagickMediaConversionStrategy;
import de.hybris.platform.mediaconversion.model.ConversionMediaFormatModel;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;


/**
 * convert image based on input image height and width which ever is greater
 */
public class ExtImageMagickMediaConversionStrategy extends ImageMagickMediaConversionStrategy implements MediaConversionStrategy
{
	private static final Pattern CONVERSION_SPLIT_PATTERN = Pattern.compile(" ");

	/**
	 * build Command based on input image height and width which ever is greater
	 */
	@Override
	protected List<String> buildCommand(final MediaConversionService mediaConversionService, final String input,
			final String output, final ConversionMediaFormatModel format) throws IOException
	{
		boolean addInput = true;
		boolean addOutput = true;
		final LinkedList ret = new LinkedList();
		if (format.getConversion() != null && !format.getConversion().isEmpty())
		{
			final String[] formatConversion = CONVERSION_SPLIT_PATTERN.split(format.getConversion(), 0);
			final String[] formatStr = formatConversion;
			final int length = formatConversion.length;
			for (int index = 0; index < length; ++index)
			{
				String part = formatStr[index];
				if (part != null && !part.isEmpty())
				{
					if (index == 1)
					{
						final String[] convertWidthNHeight = part.replace("X", "x").replace("!", "").split("x");
						part = convertDimensions(input, convertWidthNHeight);
					}
					final String partInput = part.replace("{input}", input);
					addInput &= part.equals(partInput);
					final String partOutput = partInput.replace("{output}", output);
					addOutput &= partOutput.equals(partInput);
					ret.add(this.replaceAddOns(mediaConversionService, partOutput, format));
				}
			}
		}

		if (addOutput)
		{

			ret.add(output);
		}
		if (addInput)
		{

			ret.add(0, input);
		}
		return ret;
	}

	/**
	 * fix output height or width based on input image height and width ratio
	 */
	protected String convertDimensions(final String input, final String[] convertWidthNHeight) throws IOException
	{
		final BufferedImage image = getImage(input);
		final int width = image.getWidth();
		final int height = image.getHeight();
		String part = null;

		if (convertWidthNHeight.length > 1)
		{
			final int convertWidth = Integer.parseInt(convertWidthNHeight[0]);
			final int convertHeight = Integer.parseInt(convertWidthNHeight[1]);

			if (width <= convertWidth && height <= convertHeight)
			{
				part = width + "x" + height;
			}
			else
			{
				if (width >= height)
				{
					final int adjustedHeight = (int) ((height * 1.0) / width * convertWidth);
					part = convertWidth + "x" + adjustedHeight;
				}
				else
				{
					final int adjustedWidth = (int) ((width * 1.0) / height * convertHeight);
					part = adjustedWidth + "x" + convertHeight;
				}
			}
		}
		return part;
	}


	protected BufferedImage getImage(final String input) throws IOException
	{
		return ImageIO.read(new File(input));
	}

}
