package com.simon.core.provider.impl;


import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.SwatchColorEnum;


/**
 * This method fetches the normalized Color and normalized color URL {@link SwatchColorEnum} for the
 * {@link GenericVariantProductModel} from ResolverContext, if not null, gets the required value and adds to
 * InputDocument
 */
public class ProductColorFacetResolver extends AbstractValueResolver<GenericVariantProductModel, Set<SwatchColorEnum>, Object>
{
	private TypeService typeService;

	/**
	 * Attribute which maps to "attribute" ValueProviderParameter in impex. Fetches the attribute name to index.
	 */
	public static final String ATTRIBUTE_PARAM = "attribute";
	/**
	 * The default value of "attribute", in case value is not fetched from model
	 *
	 */
	public static final String ATTRIBUTE_PARAM_DEFAULT_VALUE = null;

	/**
	 * Sets the type service.
	 *
	 * @param typeService
	 *           of type {@link TypeService} which is used to fetch EnumerationValueModel.
	 */
	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	/**
	 * adds the normalizedColorCode and normalizedColorURL for given sku into the solr indexes. throws
	 * {@link FieldValueProviderException} if it is not able to index the property.
	 *
	 * @param document
	 *           of type {@link InputDocument} denotes the solr document of gven sku in which the property will be
	 *           indexed.
	 * @param batchContext
	 *           of type {@link IndexerBatchContext}
	 * @param indexedProperty
	 *           of type {@link IndexedProperty} denotes the property that is indexed in solr for gven sku.
	 * @param model
	 *           of type {@link GenericVariantProductModel} is used to fetch the images from the database to index in
	 *           solr.
	 * @param resolverContext
	 *           of type {@link ValueResolverContext} used to fetch images from {@link GenericVariantProductModel}.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Set<SwatchColorEnum>, Object> resolverContext) throws FieldValueProviderException
	{
		final Set<SwatchColorEnum> swaches = resolverContext.getData();
		if (CollectionUtils.isNotEmpty(swaches))
		{

			final String propertyName = getPropertyName(indexedProperty);
			if (propertyName.equals(SimonCoreConstants.NORMALIZED_COLOR_CODE))
			{
				final String normalizedColor = swaches.iterator().next().getCode();
				document.addField(indexedProperty, normalizedColor, resolverContext.getFieldQualifier());
			}
			else if (propertyName.equals(SimonCoreConstants.NORMALIZED_COLOR_URL))
			{
				final EnumerationValueModel enumerationValueModel = typeService.getEnumerationValue(swaches.iterator().next());
				final String normalizedColorSwachURL = enumerationValueModel.getIcon().getURL();
				document.addField(indexedProperty, normalizedColorSwachURL, resolverContext.getFieldQualifier());
			}

		}
		else
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, SimonCoreConstants.OPTIONAL_PARAM,
					SimonCoreConstants.OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}
	}

	/**
	 * used to fetch the color swatch {@link SwatchColorEnum} from {@link GenericVariantProductModel}
	 *
	 * @param batchContext
	 *           of type {@link IndexerBatchContext}.
	 * @param indexedProperties
	 *           of type {@link Collection<IndexedProperty>}.
	 * @param product
	 *           of type {@link GenericVariantProductModel} used to fetch the medias for the product.
	 */
	@Override
	protected Set<SwatchColorEnum> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel product)
			throws FieldValueProviderException
	{
		return product.getColorFacetSwatches();
	}

	/**
	 *
	 * @param indexedProperty
	 *           The IndexedProeprty for which this resolver is invoked
	 * @return The name of property fetched from ValueProviderParameter
	 */
	protected String getPropertyName(final IndexedProperty indexedProperty)
	{
		String propertyName = ValueProviderParameterUtils.getString(indexedProperty, "value", ATTRIBUTE_PARAM_DEFAULT_VALUE);

		if (propertyName == null)
		{
			propertyName = indexedProperty.getName();
		}

		return propertyName;
	}

}
