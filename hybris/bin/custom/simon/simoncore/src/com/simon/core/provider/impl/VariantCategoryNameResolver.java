package com.simon.core.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.ControlType;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.services.ShopGatewayService;
import com.simon.core.services.VariantAttributeMappingService;


/**
 * This class helps in indexing VariantCategories Name into InputDocument
 */
public class VariantCategoryNameResolver
		extends AbstractValueResolver<GenericVariantProductModel, Collection<CategoryModel>, Object>
{

	@Resource
	private VariantAttributeMappingService variantAttributeMappingService;

	@Resource
	private ShopGatewayService shopGatewayService;

	@Value("${targetsystem.shopgateway.code}")
	protected String shopGatewayCode;

	/**
	 * This method fetches VariantValueCategories from Model, and index the variant category in a specific pipe separated
	 * format with variant category's code and variant category Display Name.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Collection<CategoryModel>, Object> resolverContext) throws FieldValueProviderException
	{
		final List<String> variantCategoryList = new ArrayList<>();

		final ShopGatewayModel shopGateway = shopGatewayService.getShopGatewayForCode(shopGatewayCode);
		final Map<String, String> sourceAttributeMap = variantAttributeMappingService.getVariantAttributeLabelMap(model.getShop(),
				shopGateway);
		final Map<String, ControlType> controlTypeMap = variantAttributeMappingService
				.getVariantAttributeControlTypeMap(model.getShop(), shopGateway);

		for (final CategoryModel variantValueVategory : (List<CategoryModel>) model.getSupercategories())
		{
			final CategoryModel variantCategory = variantValueVategory.getSupercategories().get(0);
			final ControlType controlTypeValue = controlTypeMap.get(variantCategory.getCode());
			final ControlType controlType = controlTypeValue != null ? controlTypeValue : ControlType.SWATCH;
			final String variantCategoryDisplayName = Objects.toString(sourceAttributeMap.get(variantCategory.getCode()),
					variantCategory.getName());
			variantCategoryList.add(variantCategory.getCode() + SimonCoreConstants.PIPE + variantCategoryDisplayName
					+ SimonCoreConstants.PIPE + controlType.getCode());
		}
		filterAndAddFieldValues(document, batchContext, indexedProperty, variantCategoryList, resolverContext.getFieldQualifier());
	}
}
