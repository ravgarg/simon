package com.simon.media.hotfolder.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupHelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants;


/**
 * class to move feed file ,csv and impex file to archive or error folder
 */
public class ExtCleanupHelper extends CleanupHelper
{

	private String inPutFile;
	private String outPutFile;
	private String inPutDir;
	private String outPutDir;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtCleanupHelper.class);

	/**
	 *
	 * @see de.hybris.platform.acceleratorservices.dataimport.batch.task.CleanupHelper#cleanup(de.hybris.platform.
	 *      acceleratorservices.dataimport.batch.BatchHeader, boolean)
	 *
	 *      method to move feed file and csv, impex file to archive or error folder
	 *
	 *
	 */
	@Override
	public void cleanup(final BatchHeader header, final boolean error)
	{

		if (!error && header.getFile().getName().startsWith(getInPutFile()))
		{
			try
			{
				final StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.append(getOutPutFile()).append(SimonCoreConstants.UNDERSCORE).append(header.getSequenceId())
						.append(SimonCoreConstants.DOT).append(SimonCoreConstants.CSV);
				final String fileName = stringBuilder.toString();
				final Path inputFilePath = Paths.get(getInPutDir() + fileName);
				final Path outPutFilePath = Paths.get(getOutPutDir() + fileName);
				moveFile(inputFilePath, outPutFilePath);
			}
			catch (final IOException e)
			{

				LOGGER.error("IOException", e);
			}
		}
		superCleanup(header, error);
	}


	/**
	 * call super method for cleanup
	 *
	 * @param header
	 * @param error
	 */
	protected void superCleanup(final BatchHeader header, final boolean error)
	{
		super.cleanup(header, error);
	}

	protected void moveFile(final Path inputFilePath, final Path outPutFilePath) throws IOException
	{
		Files.move(inputFilePath, outPutFilePath);
	}

	/**
	 *
	 * @return method provide inPutFile
	 */
	public String getInPutFile()
	{
		return inPutFile;
	}


	/**
	 * @param inPutFile
	 */
	public void setInPutFile(final String inPutFile)
	{
		this.inPutFile = inPutFile;
	}


	/**
	 * @return outPutFile
	 */
	public String getOutPutFile()
	{
		return outPutFile;
	}


	/**
	 * @param outPutFile
	 */
	public void setOutPutFile(final String outPutFile)
	{
		this.outPutFile = outPutFile;
	}


	/**
	 * @return inPutDir
	 */
	public String getInPutDir()
	{
		return inPutDir;
	}


	/**
	 * @param inPutDir
	 */
	public void setInPutDir(final String inPutDir)
	{
		this.inPutDir = inPutDir;
	}


	/**
	 * @return outPutDir
	 */
	public String getOutPutDir()
	{
		return outPutDir;
	}


	/**
	 * @param outPutDir
	 */
	public void setOutPutDir(final String outPutDir)
	{
		this.outPutDir = outPutDir;
	}

}
