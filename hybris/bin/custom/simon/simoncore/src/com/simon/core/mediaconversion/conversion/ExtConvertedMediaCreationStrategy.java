package com.simon.core.mediaconversion.conversion;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.mediaconversion.conversion.DefaultConvertedMediaCreationStrategy;
import de.hybris.platform.mediaconversion.model.ConversionMediaFormatModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.media.MediaIOException;

import java.io.InputStream;

import org.apache.log4j.Logger;


/**
 * This class add same version no of media container to media which will be created during conversion.
 */
public class ExtConvertedMediaCreationStrategy extends DefaultConvertedMediaCreationStrategy
{
	private static final Logger LOG = Logger.getLogger(DefaultConvertedMediaCreationStrategy.class);

	/**
	 * This method add version no in media model from media container.
	 */
	@Override
	public MediaModel createOrUpdate(final MediaModel parent, final ConversionMediaFormatModel format, final InputStream content)
			throws MediaIOException
	{
		MediaModel dmm;
		try
		{
			dmm = this.getMediaService().getMediaByFormat(parent.getMediaContainer(), format);
			LOG.debug("Updating existing media \'" + dmm + "\'.");

		}
		catch (final ModelNotFoundException arg4)
		{
			dmm = this.createModel();
			dmm.setCode(this.createCode(parent, format));
			dmm.setFolder(parent.getFolder());
			dmm.setMediaContainer(parent.getMediaContainer());
			dmm.setMediaFormat(format);


			dmm.setAltText(parent.getAltText());
			dmm.setCatalogVersion(parent.getCatalogVersion());
			dmm.setDescription(parent.getDescription());

		}
		dmm.setOriginal(parent);
		dmm.setVersion(parent.getMediaContainer().getVersion());
		dmm.setOriginalDataPK(parent.getDataPK());
		this.getModelService().save(dmm);
		this.loadContents(dmm, parent, format, content);
		this.getModelService().refresh(dmm);
		return dmm;
	}

}
