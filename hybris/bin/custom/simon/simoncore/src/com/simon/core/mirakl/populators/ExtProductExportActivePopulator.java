package com.simon.core.mirakl.populators;

import static com.mirakl.hybris.core.enums.MiraklProductExportHeader.ACTIVE;
import static java.lang.Boolean.valueOf;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Map;


/**
 * This class updates Mirakl ProductExportActivePopulator. Marks Product as Active, if it's approval status is CHECK or
 * APPROVED
 */
public class ExtProductExportActivePopulator implements Populator<ProductModel, Map<String, String>>
{

	/**
	 * This method sets target as active, if Product approval status is CHECK or APPROVED
	 *
	 * throws ConversionException
	 */
	@Override
	public void populate(final ProductModel source, final Map<String, String> target)
	{
		final Boolean active = !valueOf(ArticleApprovalStatus.UNAPPROVED.equals(source.getApprovalStatus()));
		target.put(ACTIVE.getCode(), active.toString());

	}

}
