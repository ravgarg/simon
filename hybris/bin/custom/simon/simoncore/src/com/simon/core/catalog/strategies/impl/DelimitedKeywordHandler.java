package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCoreAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.enums.KeywordType;
import com.simon.core.services.ExtKeywordService;


/**
 * DelimitedKeywordHandler attribute handler for storing delimited texts as <code>List<{@link KeywordModel}></code>
 * <p>
 * {@link MiraklCoreAttributeModel#TYPEPARAMETER} is used as an delimiter, attribute value is split on this delimiter.
 */
public class DelimitedKeywordHandler extends DefaultCoreAttributeHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(DelimitedKeywordHandler.class);

	@Autowired
	private ExtKeywordService keywordService;

	/**
	 * Splits the attribute text value {@link AttributeValueData#getValue()} by delimiter specified by
	 * {@link MiraklCoreAttributeModel#TYPEPARAMETER} and stores the list of text as <code>List<{@link KeywordModel}></code>
	 * collection on {@link ProductModel#KEYWORDS} attribute
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = attribute.getCoreAttribute();
		final String rawText = attribute.getValue();
		final String delimiter = coreAttribute.getTypeParameter();

		if (StringUtils.isNotEmpty(rawText) && StringUtils.isNotEmpty(delimiter))
		{
			final ProductModel product = determineOwner(coreAttribute, data, context);
			final String[] splitTexts = StringUtils.split(rawText, delimiter);
			final String attributeQualifier = coreAttribute.getCode();

			final CatalogVersionModel productCatalogVersion = modelService
					.get(context.getGlobalContext().getProductCatalogVersion());
			final List<KeywordModel> newKeywords = new ArrayList<>();

			final KeywordType keywordType = "seoKeywords".equalsIgnoreCase(attributeQualifier) ? KeywordType.SEO
					: KeywordType.MARKETING;

			for (final String keywordText : splitTexts)
			{
				try
				{
					final KeywordModel keyword = keywordService.getKeyword(productCatalogVersion, keywordText, keywordType);
					newKeywords.add(keyword);
				}
				catch (final Exception exception)
				{
					LOGGER.error("Exception Occurred In Getting/Creating Keyword :" + keywordText + "Type :" + keywordType
							+ " For Product :" + product.getCode(), exception);
				}
			}
			final List<KeywordModel> uniqueKeywordsToAdd = new ArrayList<>();
			Optional.ofNullable(product.getKeywords()).ifPresent(uniqueKeywordsToAdd::addAll);
			newKeywords.stream().filter(keyword -> !(uniqueKeywordsToAdd.contains(keyword))).forEach(uniqueKeywordsToAdd::add);
			product.setKeywords(uniqueKeywordsToAdd);
			LOGGER.debug("Updating Keywords : {} On Attribute : {} Product : {}", splitTexts, attributeQualifier, product.getCode());
		}
	}
}
