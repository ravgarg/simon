package com.simon.core.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.ProductUrlValueProvider;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.List;


/**
 * Extended {@link ProductUrlValueProvider} for obtaining the url of the base product
 */
public class ExtProductUrlValueProvider extends ProductUrlValueProvider
{
	private static final String BASE_PRODUCT_URL = "baseProductUrl";

	@Override
	protected List<FieldValue> createFieldValue(final ProductModel product, final LanguageModel language,
			final IndexedProperty indexedProperty)
	{
		if (product instanceof GenericVariantProductModel && BASE_PRODUCT_URL.equals(indexedProperty.getName()))
		{
			final ProductModel baseProduct = ((GenericVariantProductModel) product).getBaseProduct();
			return super.createFieldValue(baseProduct, language, indexedProperty);
		}
		return super.createFieldValue(product, language, indexedProperty);
	}

}