package com.simon.core.solrfacetsearch.converters.populator;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.converters.populator.DefaultIndexedPropertyPopulator;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;


/**
 * Extends DefaultIndexedPropertyPopulator for add attribute display facet type (Square,SquareImage,CheckBox)
 */
public class ExtDefaultIndexedPropertyPopulator extends DefaultIndexedPropertyPopulator
{

	/**
	 * Override DefaultIndexedPropertyPopulator's populate method for add attribute display facet type
	 * (Square,SquareImage,CheckBox)
	 *
	 * @param source
	 * @param target
	 *
	 */
	@Override
	public void populate(final SolrIndexedPropertyModel source, final IndexedProperty target)
	{
		superPopulate(source, target);
		if (source.getDisplayFacetType() != null)
		{
			target.setDisplayFacetType(source.getDisplayFacetType().getCode());
		}

	}

	protected void superPopulate(final SolrIndexedPropertyModel source, final IndexedProperty target)
	{
		super.populate(source, target);
	}
}
