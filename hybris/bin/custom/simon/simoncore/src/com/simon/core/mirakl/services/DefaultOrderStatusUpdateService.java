package com.simon.core.mirakl.services;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;


/**
 * This class is used to make a call to accept order on Mirakl and updating status as per consignment and make a call to
 * processPayment method to valid/invalid order Payment.
 */
public interface DefaultOrderStatusUpdateService
{
	/**
	 * This method is used to update status of line item as per Order consignment
	 *
	 * @param consignment
	 */
	void updateOrderConsignmentAndLineItemsStatus(MarketplaceConsignmentModel consignment);

}
