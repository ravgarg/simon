package com.simon.core.services;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;


/**
 * SimonCategoryNavigationService is service which in list of sub categories which contains products is returned.
 */
public interface SimonCategoryNavigationService
{

	/**
	 * Method is used to fetch sub categories of level 2 categories which contains product.
	 * 
	 * @param categoryId
	 *           categoryId is the Id of category for which subcategories are fetched
	 * @return List<CategoryModel> List of category model is the list which contains subcategories
	 */
	public List<CategoryModel> getSubCategories(String categoryId);
}
