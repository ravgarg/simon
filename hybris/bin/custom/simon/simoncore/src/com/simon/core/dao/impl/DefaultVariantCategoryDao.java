package com.simon.core.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.VariantCategoryDao;


/**
 * DefaultVariantCategoryDao db based implementation of {@link VariantCategoryDao}.
 */
public class DefaultVariantCategoryDao extends AbstractItemDao implements VariantCategoryDao
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultVariantCategoryDao.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantCategoryModel findLeafVariantCategory(final CatalogVersionModel catalogVersion)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("CatalogVersion", catalogVersion);

		VariantCategoryModel leafVariantCategory = null;

		final FlexibleSearchQuery query = new FlexibleSearchQuery(SimonFlexiConstants.FIND_LEAF_VARIANT_CATEGORY);
		query.addQueryParameter("catalogVersion", catalogVersion);
		query.setResultClassList(Arrays.asList(VariantCategoryModel.class));
		LOGGER.debug("Query : {}", query);

		final SearchResult<VariantCategoryModel> queryResult = getFlexibleSearchService().search(query);

		if (queryResult.getTotalCount() > 0)
		{
			leafVariantCategory = queryResult.getResult().get(0);
		}
		return leafVariantCategory;
	}
}
