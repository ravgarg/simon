package com.simon.core.caches;

import de.hybris.platform.core.model.ItemModel;

import java.util.Map;

import com.mirakl.hybris.beans.ProductImportFileContextData;


/**
 * The ProductFileImportPreProcessor pre processes raw values while importing Product feed. This is used to create in
 * advance master data of different types extending {@link ItemModel} that will be further used by the products during
 * ingestion so that these data's are already available in the system.
 *
 * @param <T>
 *           the generic type
 */
public interface ProductFileImportPreProcessor
{

	/**
	 * Pre process from raw values. This is used to create in advance master data for attribute determined by
	 * <code>attribute</code>
	 *
	 * @param attribute
	 *           the attribute using which raw value will be pre processed
	 * @param rawValues
	 *           the raw values containing key-value map of product values present in product feed
	 */
	public void preProcess(final String attribute, final Map<String, String> rawValues,
			final ProductImportFileContextData context);
}
