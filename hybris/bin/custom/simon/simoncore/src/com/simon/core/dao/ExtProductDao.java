package com.simon.core.dao;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.simon.core.enums.PriceType;


/**
 * The Interface ExtProductDao, provides additional dao operations on {@link ProductModel}
 */
public interface ExtProductDao extends ProductDao
{
	/**
	 * Find products by codes, returns for the given product <code>code</code>s the {@link ProductModel} collection.
	 *
	 * @param catalogVersion
	 *           the catalog version model to search products
	 * @param codes
	 *           the product <code>code</code>s
	 * @return a List of {@link ProductModel}
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code> or if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> findProductsByCodes(CatalogVersionModel catalogVersion, final Set<String> codes);

	/**
	 * Find list of products having price row of list type and product Approval status is CHECK.
	 *
	 * @param priceType
	 *           The type of price
	 * @param catalogVersion
	 *           The catalog version model to search products
	 * @return a List of {@link GenericVariantProductModel}
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>priceType</code> or <code>catalogVersion</code> is null
	 */
	List<GenericVariantProductModel> findCheckedProductsHavingPriceRow(PriceType priceType, CatalogVersionModel catalogVersion);

	/**
	 * This method returns all Generic Variant Products have alteast 1 gallery image, and whose base Product is
	 * associated with atleast 1 Merchandizing category And GTIN or MPN is not null
	 *
	 * @param catalogVersion
	 *           the catalog version for which products will be returned
	 * @return returns list of GenericVariantProductModel
	 * @throws IllegalArgumentException
	 *            if the given catalogVersion is null
	 */
	List<GenericVariantProductModel> findUnApprovedProductsHavingImageAndMerchandizingCategory(CatalogVersionModel catalogVersion);

	/**
	 * Find products by catalog version, returns for the given catalog version <code>catalogVersion</code>s the
	 * {@link ProductModel} collection.
	 *
	 * @param catalogVersion
	 *           the catalog version model to search products
	 * @return a List of {@link ProductModel}
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> findProductsByCatalogVersion(CatalogVersionModel catalogVersion);

	/**
	 * Find products by catalog version not associated to any merchandizing category, returns for the given catalog
	 * version <code>catalogVersion</code>s the {@link ProductModel} collection.
	 *
	 * @param catalogVersion
	 *           the catalog version model to search products
	 * @return a List of {@link ProductModel}
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> findProductsWithoutMerchandizingCategory(CatalogVersionModel catalogVersion);

	/**
	 * Find products by codes, returns for the given product <code>code</code>s the {@link ProductModel} collection.
	 *
	 * @param catalogVersion
	 * @param codes
	 *           the product <code>code</code>s
	 * @return a List of {@link ProductModel}
	 * @throws IllegalArgumentException
	 *            if the given <code>code</code> is <code>null</code> or if the given <code>catalogVersion</code> is null
	 */
	List<ProductModel> findProductByCodes(final Set<String> codes);

	/**
	 * Find products by catalog version and approval status, returns for the given catalog version
	 * <code>catalogVersion</code> and <code>approvalStatus</code> the {@link ProductModel} collection.
	 *
	 * @param catalogVersion
	 *           the catalog version model to search products
	 * @param approvalStatus
	 *           the approval status to search products
	 * @return a List of {@link ProductModel}
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> or <code>approvalStatus</code> is null
	 */
	List<ProductModel> findProductsByCatalogVersion(CatalogVersionModel catalogVersion, ArticleApprovalStatus approvalStatus);

	/**
	 * This method will fetch all the products for which the inventory went OOS after last successful run of
	 * OfferUpdateJob
	 *
	 * @param catalogVersion
	 * @param lastSuccessJobRun
	 * @return List<GenericVariantProductModel>
	 */
	List<GenericVariantProductModel> findProductsWithZeroStock(CatalogVersionModel catalogVersion, Date lastSuccessJobRun);

}
