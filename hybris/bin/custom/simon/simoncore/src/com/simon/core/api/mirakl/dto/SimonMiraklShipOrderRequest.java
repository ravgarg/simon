package com.simon.core.api.mirakl.dto;

import java.util.Map;

import com.mirakl.client.core.internal.MiraklApiEndpoint;
import com.mirakl.client.request.AbstractMiraklApiRequest;

/**
 * Class responsible for retrieving values for API endPoint
 * {@link SimonMiraklFrontOperatorApiEndpoint}, request template that need to be
 * passed with in request object.
 *
 */
public class SimonMiraklShipOrderRequest extends AbstractMiraklApiRequest {
		private String orderId;

		public SimonMiraklShipOrderRequest(String orderId) {
				setOrderId(orderId);
		}

		/**
		 * Method used to retrieve OR24 API EndPoint using
		 * {@link SimonMiraklFrontOperatorApiEndpoint}
		 */
		public MiraklApiEndpoint getEndpoint() {
				return SimonMiraklFrontOperatorApiEndpoint.OR24;
		}

		/**
		 * Method used to set the parameters with in requestTemplate.
		 */
		public Map<String, String> getRequestTemplates() {
				Map<String, String> templates = super.getRequestTemplates();
				templates.put("order", orderId);
				return templates;
		}

		public String getOrderId() {
				return orderId;
		}

		public void setOrderId(String orderId) {
				checkRequiredArgument(orderId, "orderId");
				this.orderId = orderId;
		}

		public boolean equals(Object o) {
				if (this == o) {
						return true;
				}
				if ((o == null) || (getClass() != o.getClass())) {
						return false;
				}

				return orderId == null ? true :  false;
		}

		public int hashCode() {
				return orderId != null ? orderId.hashCode() : 0;
		}

}
