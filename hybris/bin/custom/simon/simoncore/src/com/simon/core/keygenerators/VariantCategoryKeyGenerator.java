package com.simon.core.keygenerators;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;


/**
 * VariantCategoryKeyGenerator generates Variant Category Code for given raw name
 */
public class VariantCategoryKeyGenerator implements KeyGenerator
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object generate()
	{
		throw new UnsupportedOperationException("Not supported, please call generateFor().");
	}

	/**
	 * Converts Variant Category Code to lower case and replaces all nbsp by hyphen.
	 *
	 * @param arg0
	 *           the arg 0
	 * @return the object
	 */
	@Override
	public Object generateFor(final Object arg0)
	{
		return ((String) arg0).toLowerCase().replaceAll("\\s+", "-");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		throw new UnsupportedOperationException("A reset of VariantCategoryKeyGenerator is not supported.");
	}

}
