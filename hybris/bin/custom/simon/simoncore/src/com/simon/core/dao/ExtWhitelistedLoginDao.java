package com.simon.core.dao;

import com.simon.core.model.WhitelistedUserModel;


/**
 * Dao for {@link WhitelistedUserModel} contains abstract method for operations on {@link WhitelistedUserModel}
 */
public interface ExtWhitelistedLoginDao
{
	/**
	 * This method is used to find the beta user from the whitelistedUser Table on the basis of emailId.
	 *
	 * @param email
	 * @return WhiteListedUserModel
	 */
	WhitelistedUserModel findUserByEmail(String email);
}
