package com.simon.core.price.jalo;

import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.enums.PriceType;
import com.simon.core.price.ExtPriceRowInfoComparator;
import com.simon.core.price.ExtPriceRowMatchComparator;


/**
 * This class will be used as price factory for simon project
 */
public class ExtEurope1PriceFactory extends Europe1PriceFactory
{


	@Resource
	private ModelService modelService;
	@Resource
	private RetrieveChannelStrategy retrieveChannelStrategy;

	/**
	 * Returns PriceInformation which contains ExtPriceValue. ExtPriceValue is extended from PriceValue to accommodate
	 * extra information of listPrice and start and end of a sale price. This function is similar to
	 * {@link Europe1Tools.createPriceInformation()}
	 *
	 * @param priceRow
	 *           This is the priceRow which can be either be sale Price and list price based on which respective values
	 *           will be populated
	 * @param currency
	 *           This parameter is passed on to {@link Europe1Tools.createPriceInformation()} which converts the price
	 *           based on the currency
	 * @return PriceInformation which contains ExtPriceValue.
	 */
	public PriceInformation createPriceInformation(final PriceRow priceRow, final Currency currency)
	{
		final PriceInformation priceInfo = Europe1Tools.createPriceInformation(priceRow, currency);
		final PriceRowModel priceRowModel = modelService.get(priceRow);
		final PriceValue priceValue = priceInfo.getPriceValue();
		return new PriceInformation(priceInfo.getQualifiers(),
				new ExtPriceValue(priceValue.getCurrencyIso(), priceValue.getValue(), priceValue.isNet(), getListPrice(priceRowModel),
						priceRow.getStartTime(), priceRow.getEndTime(), priceRowModel.getOfferId(), priceRowModel.getLeadtimeToShip()));
	}


	/**
	 * This method has been overridden to incorporate list price and its population in {@link ExtPriceValue} is extended
	 * from PriceValue to accommodate extra information of listPrice and start and end of a sale price
	 *
	 * @param entry
	 *           This is the cart entry for which price i.e, evaluated
	 * @return If the cart entry is not giveAway it returns {@link ExtPriceValue} which has been extended from PriceValue
	 *         to accommodate extra information of listPrice and start and end of a sale price
	 */
	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = this.getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		final Currency currency = order.getCurrency(ctx);
		final User user = order.getUser();
		final Unit unit = entry.getUnit(ctx);
		final Product product = entry.getProduct();
		final EnumerationValue productGroup = this.getPPG(ctx, product);
		final EnumerationValue userGroup = this.getUPG(ctx, user);
		final Date date = new Date();
		final long quantity = entry.getQuantity(ctx).longValue();
		final boolean net = order.isNet().booleanValue();
		final boolean giveAwayMode = entry.isGiveAway(ctx).booleanValue();
		final boolean entryIsRejected = entry.isRejected(ctx).booleanValue();
		PriceRow priceRow;
		if (giveAwayMode && entryIsRejected)
		{
			priceRow = null;
		}
		else
		{
			priceRow = this.matchPriceRowForPrice(ctx, product, productGroup, user, userGroup, quantity, unit, currency, date, net,
					giveAwayMode);
		}

		if (priceRow != null)
		{
			final Currency priceCurrency = priceRow.getCurrency();
			double price;
			if (currency.equals(priceCurrency))
			{
				price = priceRow.getPriceAsPrimitive() / priceRow.getUnitFactorAsPrimitive();
			}
			else
			{
				price = priceCurrency.convert(currency, priceRow.getPriceAsPrimitive() / priceRow.getUnitFactorAsPrimitive());
			}

			final Unit priceUnit = priceRow.getUnit();
			final Unit entryUnit = entry.getUnit();
			final PriceRowModel priceRowModel = modelService.get(priceRow);
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);
			return new ExtPriceValue(currency.getIsoCode(), convertedPrice, priceRow.isNetAsPrimitive(), getListPrice(priceRowModel),
					priceRow.getStartTime(), priceRow.getEndTime(), priceRowModel.getOfferId(), priceRowModel.getLeadtimeToShip());
		}
		else if (giveAwayMode)
		{

			return new PriceValue(order.getCurrency(ctx).getIsoCode(), 0.0D, order.isNet().booleanValue());
		}
		else
		{
			final String msg = Localization
					.getLocalizedString("exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1", new Object[]
			{ product, productGroup, user, userGroup, Long.toString(quantity), unit, currency, date, Boolean.toString(net) });
			throw new JaloPriceFactoryException(msg, 0);
		}
	}


	/**
	 *
	 * This method has been overridden to incorporate list price and its population in {@link ExtPriceValue}
	 * ExtPriceValue is extended from PriceValue to accommodate extra information of listPrice and start and end of a
	 * sale price
	 *
	 * @param ctx
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param curr
	 * @param net
	 * @param date
	 * @param taxValues
	 * @return
	 * @throws JaloPriceFactoryException
	 */
	@Override
	protected List<PriceInformation> getPriceInformations(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency curr,
			final boolean net, final Date date, final Collection taxValues) throws JaloPriceFactoryException
	{

		final List<PriceRow> priceRows = this
				.filterPriceRows(this.matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, curr, date, net));
		final ArrayList<PriceInformation> priceInfos = new ArrayList<>(priceRows.size());
		final ArrayList<PriceInformation> defaultPriceInfos = new ArrayList<>(priceRows.size());
		final PriceRowChannel channel = retrieveChannelStrategy.getChannel(ctx);
		final Iterator<PriceRow> priceItr = priceRows.iterator();
		Collection<?> theTaxValues = taxValues;

		while (priceItr.hasNext())
		{
			final PriceRow row = priceItr.next();
			PriceInformation pInfo = createPriceInformation(row, curr);
			if (pInfo.getPriceValue().isNet() != net)
			{
				if (theTaxValues == null)
				{
					theTaxValues = Europe1Tools.getTaxValues(
							this.getTaxInformations(product, this.getPTG(ctx, product), user, this.getUTG(ctx, user), date));
				}

				pInfo = new PriceInformation(pInfo.getQualifiers(), pInfo.getPriceValue().getOtherPrice(theTaxValues));
			}

			if (row.getChannel() == null)
			{
				defaultPriceInfos.add(pInfo);
				if (channel == null)
				{
					priceInfos.add(pInfo);
				}
			}
			else if (channel != null && row.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
			{
				priceInfos.add(pInfo);
			}
		}

		if (priceInfos.isEmpty())
		{
			return defaultPriceInfos;
		}

		return priceInfos;

	}


	private double getListPrice(final PriceRowModel salePriceModel)
	{
		double listPrice = 0.0d;
		final PriceRowModel listPriceModel = salePriceModel.getListPrice();
		if (PriceType.SALE.equals(salePriceModel.getPriceType()) && null != listPriceModel
				&& PriceType.LIST.equals(listPriceModel.getPriceType()))
		{
			listPrice = listPriceModel.getPrice();
		}
		return listPrice;
	}

	/**
	 *
	 *
	 * method match price row for info and compare each price row based on unit, price date, currency , minimum quantity
	 * and net value
	 *
	 * @param ctx
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param currency
	 * @param date
	 * @param net
	 * @return List<PriceRow>
	 */
	@Override
	public List<PriceRow> matchPriceRowsForInfo(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency currency,
			final Date date, final boolean net) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without product and product group - at least one must be present", 0);
		}
		else if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without user and user group - at least one must be present",
					0);
		}
		else if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without currency", 0);
		}
		else if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without date", 0);
		}
		else
		{
			return getPriceRowsForInfo(ctx, product, productGroup, user, userGroup, currency, date, net);
		}
	}


	/**
	 *
	 * method match price row for info and compare each price row based on unit, price date, currency , minimum quantity
	 * and net value
	 *
	 * @param ctx
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param currency
	 * @param date
	 * @param net
	 * @return List<PriceRow>
	 */
	protected List<PriceRow> getPriceRowsForInfo(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency currency,
			final Date date, final boolean net)
	{

		final Collection<PriceRow> rows = this.queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
		if (!rows.isEmpty())
		{
			final PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
			final ArrayList<PriceRow> ret = new ArrayList<>(rows);
			if (ret.size() > 1)
			{
				Collections.sort(ret, new ExtPriceRowInfoComparator(currency, net));
			}

			return this.filterPriceRows4Info(ret, currency, date, channel);
		}
		else
		{
			return Collections.emptyList();
		}
	}

	/**
	 *
	 *
	 * method match price row for info and compare each price row based on unit, price date, currency , minimum quantity
	 * and net value
	 *
	 * @param ctx
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param qtd
	 * @param unit
	 * @param currency
	 * @param date
	 * @param net
	 * @param giveAwayMode
	 * @return PriceRow
	 */
	@Override
	public PriceRow matchPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price without product and product group - at least one must be present", 0);
		}
		else if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price without user and user group - at least one must be present", 0);
		}
		else if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}
		else if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}
		else if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}
		else
		{
			return getPriceRowForPrice(ctx, product, productGroup, user, userGroup, qtd, unit, currency, date, net, giveAwayMode);
		}
	}

	/**
	 * method match price row for info and compare each price row based on unit, price date, currency , minimum quantity
	 * and net value
	 *
	 * @param ctx
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param qtd
	 * @param unit
	 * @param currency
	 * @param date
	 * @param net
	 * @param giveAwayMode
	 * @return PriceRow
	 */
	public PriceRow getPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode)
	{
		final Collection<PriceRow> rows = this.queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
		if (!rows.isEmpty())
		{
			final PriceRowChannel channel = this.retrieveChannelStrategy.getChannel(ctx);
			final List<PriceRow> list = this.filterPriceRows4Price(rows, qtd, unit, currency, date, giveAwayMode, channel);
			if (list.isEmpty())
			{
				return null;
			}
			else if (list.size() == 1)
			{
				return list.get(0);
			}
			else
			{
				Collections.sort(list, new ExtPriceRowMatchComparator(currency, net, unit));
				return list.get(0);
			}
		}
		else
		{
			return null;
		}
	}
}
