package com.simon.core.syetem.initial.data.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;


/**
 * This class extends CoreDataImportService in order to import core data for mirakl and some project specific imports
 */
public class SimonCoreDataImportService extends CoreDataImportService
{


	@Override
	public void importCommonData(final String extensionName)
	{
		parentCommonData(extensionName);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/designers.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/retailers.impex", extensionName), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/mirakl-user-groups.impex", extensionName),
				false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/user-groups.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/common/mirakl-common-addon-extra.impex", extensionName), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/constraints.impex", extensionName), false);

		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/searchRestriction.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/cronJob.impex", extensionName), false);


	}

	/**
	 * This method is created to mock the call to parent importCommonData method
	 *
	 * @param extensionName
	 *           - extension name
	 */
	public void parentCommonData(final String extensionName)
	{
		super.importCommonData(extensionName);
	}

	@Override
	public void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		parentProductCatalog(extensionName, productCatalogName);

		getSetupImpexService().importImpexFile(
				String.format("/%s/import/coredata/productCatalogs/simonProductCatalog/categories.impex", extensionName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/mirakl-core-attributes.impex",
						extensionName, productCatalogName), false);

		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/mirakl-cronjobs.impex",
						extensionName, productCatalogName), false);
	}

	/**
	 * This method is created to mock the call to parent importProductCatalog method
	 *
	 * @param extensionName
	 *           - extension name
	 * @param productCatalogName
	 *           - product catalog name
	 */
	public void parentProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);
	}

	@Override
	public void importStore(final String extensionName, final String storeName, final String productCatalogName)
	{
		parentStoreData(extensionName, storeName, productCatalogName);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/stores/%s/mirakl-site.impex", extensionName, storeName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/coredata/common/mirakl-cronjobs.impex", extensionName),
				false);
	}

	/**
	 * This method is created to mock the call to parent importStore method
	 *
	 * @param extensionName
	 *           - extension name
	 * @param storeName
	 *           - store name
	 * @param productCatalogName
	 *           -product catalog name
	 */
	public void parentStoreData(final String extensionName, final String storeName, final String productCatalogName)
	{
		super.importStore(extensionName, storeName, productCatalogName);
	}
}
