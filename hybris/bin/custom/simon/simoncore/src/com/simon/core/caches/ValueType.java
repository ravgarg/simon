package com.simon.core.caches;

/**
 * The Enum ValueType. Type of Raw Value
 */
public enum ValueType
{
	SINGLE, MULTIPLE
}
