package com.simon.core.dao.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.SimonCategoryNavigationDao;


/**
 * Implementation class of {@link SimonCategoryNavigationDao} extending AbstractItemDao in which sub categories is
 * retrieving for level 2 categories ehich contains products.
 *
 *
 */
public class SimonCategoryNavigationDaoImpl extends AbstractItemDao implements SimonCategoryNavigationDao
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CategoryModel> getSubCategoriesWithProducts(final String categoryId)
	{

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(
				SimonFlexiConstants.SUBCATEGORIES_OF_CATEGORIES_BY_CATEGORY_ID_QUERY);
		fQuery.addQueryParameter("categoryId", categoryId);

		final SearchResult<CategoryModel> searchResult = getFlexibleSearchService().search(fQuery);

		return searchResult.getResult();

	}




}
