package com.simon.media.hotfolder.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.impex.ImportConfig;
import de.hybris.platform.servicelayer.impex.ImportService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.FileNotFoundException;

import org.springframework.beans.factory.annotation.Required;


/**
 * Task that imports an impex file by executing impex.
 */
public abstract class ExtAbstractImpexRunnerTask extends AbstractImpexRunnerTask
{
	private SessionService sessionService;
	private ImportService importService;
	private CronJobService cronJobService;
	private String fileName;

	/**
	 *
	 * @see de.hybris.platform.acceleratorservices.dataimport.batch.task.AbstractImpexRunnerTask#execute(de.hybris.platform.
	 *      acceleratorservices.dataimport.batch.BatchHeader)
	 *
	 *
	 *      execute Impex Runner Task and cronjob for image media conversion
	 *
	 */
	@Override
	public BatchHeader execute(final BatchHeader header) throws FileNotFoundException
	{
		if (null != header && null != header.getFile() && header.getFile().getName().startsWith(fileName))
		{
			super.execute(header);
			cronJobService.performCronJob(cronJobService.getCronJob("extImageMediaConversionCronJob"), false);
		}
		return header;
	}


	/**
	 * @return cronJobService
	 */
	public CronJobService getCronJobService()
	{
		return cronJobService;
	}


	/**
	 * @param cronJobService
	 */
	public void setCronJobService(final CronJobService cronJobService)
	{
		this.cronJobService = cronJobService;
	}


	@Override
	public SessionService getSessionService()
	{
		return sessionService;
	}

	@Override
	@Required
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Override
	public ImportService getImportService()
	{
		return importService;
	}

	@Override
	@Required
	public void setImportService(final ImportService importService)
	{
		this.importService = importService;
	}

	/**
	 * Lookup method to return the import config
	 *
	 * @return import config
	 */
	@Override
	public abstract ImportConfig getImportConfig();


	/**
	 * @return fileName
	 */
	public String getFileName()
	{
		return fileName;
	}


	/**
	 * @param fileName
	 */
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}
}
