package com.simon.core.populators;

import de.hybris.platform.acceleratorfacades.order.data.PriceRangeData;
import de.hybris.platform.commercefacades.order.converters.populator.GroupOrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import javax.annotation.Resource;

import com.simon.core.util.CommonUtils;


/**
 * Groups multiple {@link OrderEntryData} as one entry in a {@link AbstractOrderData} based on the multidimensional
 * variants that share the same base product. All non multidimensional product entries will be leaved unmodified as a
 * single entry.
 */
public class ExtGroupOrderEntryPopulator extends GroupOrderEntryPopulator
{
	@Resource
	private ProductService productService;

	@Override
	protected ProductData createBaseProduct(final ProductData variant)
	{
		final ProductData productData = new ProductData();

		productData.setUrl(variant.getUrl());
		productData.setPurchasable(variant.getPurchasable());
		productData.setMultidimensional(Boolean.TRUE);
		productData.setImages(variant.getImages());

		final ProductModel productModel = productService.getProductForCode(variant.getBaseProduct());
		productData.setCode(productModel.getCode());
		productData.setName(CommonUtils.replaceAsciiWithHtmlCode(productModel.getName()));
		productData.setDescription(CommonUtils.replaceAsciiWithHtmlCode(productModel.getDescription()));

		productData.setPriceRange(new PriceRangeData());

		return productData;
	}

	@Override
	protected boolean isGroupable(final ProductData product)
	{
		return false;
	}


}
