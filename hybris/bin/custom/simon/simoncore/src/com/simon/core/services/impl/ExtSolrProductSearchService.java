package com.simon.core.services.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.impl.DefaultSolrProductSearchService;

import java.util.Collections;

import org.apache.commons.lang.StringUtils;



/**
 * The Class ExtSolrProductSearchService.
 *
 */
public class ExtSolrProductSearchService<ITEM> extends DefaultSolrProductSearchService<ITEM> //NOSONAR
{

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> categorySearch(final String categoryCode,
			final SearchQueryContext searchQueryContext, final PageableData pageableData)
	{
		final SolrSearchQueryData searchQueryData = callSuperCreateSearchQueryData();
		searchQueryData.setCategoryCode(categoryCode);
		searchQueryData.setFilterTerms(Collections.<SolrSearchQueryTermData> emptyList());
		searchQueryData.setSearchQueryContext(SearchQueryContext.CATEGORYLINKS);
		return callSuperDoSearch(searchQueryData, pageableData);

	}


	protected ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> callSuperDoSearch(
			final SolrSearchQueryData searchQueryData, final PageableData pageableData)
	{
		return super.doSearch(searchQueryData, pageableData);
	}



	protected SolrSearchQueryData callSuperCreateSearchQueryData()
	{
		return super.createSearchQueryData();
	}

	@Override
	public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> searchAgain(
			final SolrSearchQueryData searchQueryData, final PageableData pageableData)
	{
		if (StringUtils.isNotEmpty(searchQueryData.getCategoryCode()) && StringUtils.isEmpty(searchQueryData.getFreeTextSearch()))
		{
			searchQueryData.setSearchQueryContext(SearchQueryContext.CATEGORYLINKS);
		}
		else if (null != searchQueryData.getSearchQueryContext()
				&& SearchQueryContext.DESIGNERS.equals(searchQueryData.getSearchQueryContext()))
		{
			searchQueryData.setSearchQueryContext(SearchQueryContext.DESIGNERS);
		}
		else
		{
			searchQueryData.setSearchQueryContext(SearchQueryContext.SEARCH);
		}
		return callSuperDoSearch(searchQueryData, pageableData);
	}

}
