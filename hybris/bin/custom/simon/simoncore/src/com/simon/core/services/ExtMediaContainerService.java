package com.simon.core.services;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.MediaContainerService;

import java.util.Date;
import java.util.List;


/**
 * This class is extension of MediaContainerService with some additional methods.
 */
public interface ExtMediaContainerService extends MediaContainerService
{
	/**
	 * This method get all media container whose version no is greater then zero.
	 *
	 * @return
	 */
	public List<MediaContainerModel> getMediaContainersWithVersionGreaterThenZero(Date cleanupAgeDate);
}
