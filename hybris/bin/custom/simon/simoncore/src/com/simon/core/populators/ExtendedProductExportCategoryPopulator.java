package com.simon.core.populators;

import static com.mirakl.hybris.core.enums.MiraklProductExportHeader.CATEGORY_CODE;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Map;

import com.mirakl.hybris.core.product.populators.ProductExportCategoryPopulator;


/**
 * This is the implementation class of {@link ProductExportCategoryPopulator} used to populate the category information
 * to P21 service
 *
 */
public class ExtendedProductExportCategoryPopulator extends ProductExportCategoryPopulator
{
	/**
	 * This method will be used to populate the category code that will be sent to mirakl as header for P21 service
	 *
	 * @param source
	 *           The source attribute of {@link ProductModel} from where the category information will be retrieved
	 * @param target
	 *           The target map where the category information will be saved. This map will be used to build a csv line to
	 *           sent to mirakl
	 *
	 */
	@Override
	protected void populateAttributesIfNotPresent(final ProductModel source, final Map<String, String> target)
			throws ConversionException
	{
		if (target.get(CATEGORY_CODE.getCode()) == null)
		{
			target.put(CATEGORY_CODE.getCode(),
					((GenericVariantProductModel) source).getBaseProduct().getMiraklCategory().getCode());
		}

	}
}
