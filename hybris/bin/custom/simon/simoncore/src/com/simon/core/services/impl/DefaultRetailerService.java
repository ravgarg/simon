package com.simon.core.services.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.simon.core.model.RetailersInfoModel;
import com.simon.core.services.RetailerService;
import com.simon.core.util.CommonUtils;


/**
 * This class will provide service to retailer specific operations
 */
public class DefaultRetailerService implements RetailerService
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Double> getRetailerSubBagForCart(final AbstractOrderModel cart)
	{
		final Map<String, Double> retailerData = new HashMap<>();
		final List<AbstractOrderEntryModel> entries = cart.getEntries();
		final Set<String> distinctRetailers = new HashSet<>();
		entries.stream().forEach(entry -> distinctRetailers.add(entry.getProduct().getShop().getId()));
		distinctRetailers.stream().forEach(retailer -> retailerData.put(retailer, getRetailerSubtotal(cart, retailer)));
		return retailerData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double getRetailerSubBagSaving(final AbstractOrderModel source, final String retailerCode)
	{
		final Double savingWithoutPromotion = source.getEntries().stream().filter((entry) -> {
			return retailerCode.equals(entry.getProduct().getShop().getId());
		}).mapToDouble(entry -> entryLevelSaving(entry)).sum();

		return savingWithoutPromotion + CommonUtils.getValueFromMap(source.getRetailersTotalDiscount(), retailerCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getRetailerSubtotal(final AbstractOrderModel cart, final String retailer)
	{
		return cart.getEntries().stream().filter(entry -> retailer.equals(entry.getProduct().getShop().getId()))
				.mapToDouble(entry -> entry.getTotalPrice()).sum();
	}

	private double entryLevelSaving(final AbstractOrderEntryModel entry)
	{

		final Double strikethrough = (entry.getProduct().getMsrp() != null ? entry.getProduct().getMsrp() : entry.getListValue())
				* entry.getQuantity();
		final Double plumPrice = entry.getTotalPrice();
		return (strikethrough - plumPrice);
	}


	/**
	 * method calculate the you save price for items which are not eligible for promotion
	 *
	 * @param entry
	 * @return Double
	 */
	@Override
	public Double getYouSave(final AbstractOrderEntryModel entry)
	{
		final Double strikethrough = entry.getProduct().getMsrp() != null ? entry.getProduct().getMsrp() : entry.getListValue();
		final Double plumPrice = entry.getSaleValue() != null ? entry.getSaleValue() : entry.getListValue();
		return strikethrough - plumPrice;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getRetailerTotal(final AbstractOrderModel orderModel, final String retailer)
	{
		double total = 0.0;
		final Optional<RetailersInfoModel> retailerInfo = orderModel.getAdditionalCartInfo().getRetailersInfo().stream()
				.filter(retInfo -> retInfo.getRetailerId().equalsIgnoreCase(retailer)).findFirst();
		if (retailerInfo.isPresent())
		{
			total = CommonUtils.getValueFromMap(orderModel.getRetailersSubtotal(), retailer)
					+ CommonUtils.getValueFromMap(orderModel.getRetailersDeliveryCost(), retailer)
					+ retailerInfo.get().getTotalSalesTax()
					- CommonUtils.getValueFromMap(orderModel.getRetailersTotalDiscount(), retailer);
		}
		return total;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getAllRetailersFromOrder(final AbstractOrderModel order)
	{
		return new ArrayList<>(order.getRetailersSubtotal().keySet());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getRetailerOrderNumberForMarketPlace(final AbstractOrderModel orderModel, final String retailerId)
	{
		return orderModel.getRetailersOrderNumbers().get(retailerId) + '_' + retailerId;
	}



}
