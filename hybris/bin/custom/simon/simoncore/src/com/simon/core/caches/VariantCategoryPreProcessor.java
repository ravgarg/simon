package com.simon.core.caches;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.daos.SolrIndexedTypeDao;
import de.hybris.platform.solrfacetsearch.enums.SolrPropertiesTypes;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.services.VariantCategoryService;


/**
 * VariantCategoryPreProcessor creates new {@link VariantCategoryModel} if present in product feed found during
 * pre-processing and also creates Solr Indexed Properties {@link SolrIndexedPropertyModel} to index newly created
 * Variant Categories in solr.
 */
public class VariantCategoryPreProcessor extends AbstractPreProcessor<VariantCategoryModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(VariantCategoryPreProcessor.class);

	private static final int VALID_DELIMIS = 2;

	@Autowired
	private VariantCategoryService variantCategoryService;

	@Autowired
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private UserService userService;

	@Autowired
	private SolrIndexedTypeDao solrIndexedTypeDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createKey(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		String key = null;
		final String[] delims = getRawValue(rawValue);
		if (delims.length == VALID_DELIMIS && StringUtils.isNotEmpty(delims[0]))
		{
			key = (String) variantCategoryKeyGenerator.generateFor(delims[0]);
		}
		return key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantCategoryModel createValue(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		VariantCategoryModel variantCategory;

		final String variantCategoryCode = createKey(attribute, rawValue, productValues, context);
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);

		final VariantCategoryModel existingVariantCategory = getVariantCategoryFromDatabase(catalogVersion, variantCategoryCode);
		if (existingVariantCategory != null)
		{
			variantCategory = existingVariantCategory;
		}
		else
		{
			variantCategory = createVariantCategory(variantCategoryCode, rawValue, catalogVersion);
			createIndexedProperties(variantCategoryCode);
		}
		return variantCategory;
	}

	/**
	 * Creates the variant category.
	 *
	 * @param variantCategoryCode
	 *           the variant category code
	 * @param rawValue
	 *           the raw value
	 * @param catalogVersion
	 *           the catalog version
	 * @return the variant category model
	 */
	private VariantCategoryModel createVariantCategory(final String variantCategoryCode, final String rawValue,
			final CatalogVersionModel catalogVersion)
	{
		LOGGER.info("Creating New Variant Category :{} ", variantCategoryCode);
		final VariantCategoryModel variantCategory = modelService.create(VariantCategoryModel.class);
		variantCategory.setCode(variantCategoryCode);
		variantCategory.setName(getRawValue(rawValue)[0]);
		variantCategory.setCatalogVersion(catalogVersion);
		variantCategory.setAllowedPrincipals(Arrays.asList(userService.getUserGroupForUID("customergroup")));

		final VariantCategoryModel parentVariantCategory = variantCategoryService.getLeafVariantCategory(catalogVersion);
		Optional.ofNullable(parentVariantCategory).ifPresent(parent -> {
			LOGGER.info("Associating Parent Category :{} On :{}", parentVariantCategory.getCode(), variantCategory.getCode());
			variantCategory.setSupercategories(Arrays.asList(parent));
		});
		modelService.save(variantCategory);
		return variantCategory;
	}


	/**
	 * Creates the Index Property.
	 *
	 * @param variantCategoryCode
	 *           the variant category code
	 */
	private void createIndexedProperties(final String variantCategoryCode)
	{
		final List<SolrIndexedPropertyModel> solrIndexProperties = new ArrayList<>();
		solrIndexProperties.add(
				createIndexedProperty(variantCategoryCode, StringUtils.lowerCase(SimonCoreConstants.NAME), SimonCoreConstants.NAME));
		solrIndexProperties.add(
				createIndexedProperty(variantCategoryCode, StringUtils.lowerCase(SimonCoreConstants.CODE), SimonCoreConstants.ID));
		modelService.saveAll(solrIndexProperties);
	}

	/**
	 * Creates the individual Index Property.
	 *
	 * @param variantCategoryCode
	 *           the variant category code
	 * @param value
	 *           Value of ValueProviderParameter map
	 * @param prefix
	 *           Prefix to be appended to variantCategoryCode to make a Indexed Property
	 */
	private SolrIndexedPropertyModel createIndexedProperty(final String variantCategoryCode, final String value,
			final String prefix)
	{
		final String solrIndexedPropertyName = variantCategoryCode + prefix;
		LOGGER.info("Created {} SolrIndexedProperty For {}", solrIndexedPropertyName, variantCategoryCode);
		final SolrIndexedPropertyModel solrIndexedProperty = modelService.create(SolrIndexedPropertyModel.class);
		final Map<String, String> parameters = new HashMap<>();
		parameters.put(SimonCoreConstants.ATTRIBUTE, variantCategoryCode);
		parameters.put(SimonCoreConstants.VALUE, value);
		solrIndexedProperty.setName(solrIndexedPropertyName);
		solrIndexedProperty.setSolrIndexedType(solrIndexedTypeDao.findIndexedTypeByIdentifier(SimonCoreConstants.PRODUCT_TYPE));
		solrIndexedProperty.setType(SolrPropertiesTypes.STRING);
		solrIndexedProperty.setFieldValueProvider(SimonCoreConstants.VARIANT_CATEGORY_RESOLVER);
		solrIndexedProperty.setValueProviderParameters(parameters);
		return solrIndexedProperty;

	}

	/**
	 * Gets the variant category from database.
	 *
	 * @param catalogVersion
	 *           the catalog version
	 * @param variantCategoryCode
	 *           the variant category code
	 * @return the variant category from database
	 */
	private VariantCategoryModel getVariantCategoryFromDatabase(final CatalogVersionModel catalogVersion,
			final String variantCategoryCode)
	{
		VariantCategoryModel variantCategory = null;
		try
		{
			final CategoryModel category = variantCategoryService.getCategoryForCode(catalogVersion, variantCategoryCode);
			if (category instanceof VariantCategoryModel)
			{
				variantCategory = (VariantCategoryModel) category;
				LOGGER.debug("Variant Category :{} Found", variantCategory.getCode());
			}
		}
		catch (final UnknownIdentifierException identifierException)
		{
			LOGGER.debug("Variant Category :{} Not Found:{}", variantCategoryCode, identifierException);
		}
		return variantCategory;
	}

	/**
	 * Get delimited raw values
	 *
	 * @return delimited raw values
	 */
	private String[] getRawValue(final String rawValue)
	{
		return StringUtils.split(rawValue, "=");
	}

	@Override
	public ValueType getValueType()
	{
		return ValueType.MULTIPLE;
	}
}
