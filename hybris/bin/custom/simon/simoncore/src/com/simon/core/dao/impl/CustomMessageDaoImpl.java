package com.simon.core.dao.impl;


import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.CustomMessageDao;
import com.simon.core.exceptions.SystemException;
import com.simon.core.model.CustomMessageModel;


/**
 * This class SimonMessageDaoImpl implements CustomMessageDao interface. It fetches the CustomMessageModel from DB using
 * flexible search against messageCode and catalogVersion provided.
 *
 */
public class CustomMessageDaoImpl implements CustomMessageDao
{

	/*
	 * FlexibleSearchService implementation class is injected. Helps in forming search query and returning SearchResult.
	 */
	@Autowired
	private FlexibleSearchService flexibleSearchService;



	/*
	 * Returns the message text of SimonMessageModel fetched, if not empty. Flexi Query fetches result, If unique
	 * CustomMessageModel exist in DB corresponding to 1 messagecode If Messagetext attribute of CustomMessageModel is
	 * not empty, it is returned, else empty string is returned.
	 *
	 * @param messageCode The uid/code of CustomMessage Type
	 *
	 * @param CatalogVersion The current catalogVersion
	 *
	 * @throws SystemException Encapsulates ModelNotFoundException : When No result are found for the given query, and
	 * AmbiguousIdentifierException : When more than 1 SimonMessageModel is found for single message Code.
	 *
	 * Exception is handled in upper hierarchy.
	 */

	@Override
	public String getMessageTextForCode(final String messageCode, final Collection<CatalogVersionModel> catalogVersions)
			throws SystemException
	{
		final FlexibleSearchQuery query = getFlexibleSearchQuery(SimonFlexiConstants.MESSAGE_BY_CODE);
		query.addQueryParameter("code", messageCode);
		query.addQueryParameter("catalogVersions", catalogVersions);
		try
		{

			final CustomMessageModel searchResult = flexibleSearchService.searchUnique(query);
			if (StringUtils.isNotEmpty(searchResult.getMessageText()))
			{
				return searchResult.getMessageText();
			}
			else
			{
				return StringUtils.EMPTY;
			}
		}
		catch (ModelNotFoundException | AmbiguousIdentifierException exp)
		{
			throw new SystemException(exp, messageCode);
		}

	}

	protected FlexibleSearchQuery getFlexibleSearchQuery(final String code)
	{
		return new FlexibleSearchQuery(code);
	}


}
