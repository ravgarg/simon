package com.simon.core.exceptions;

/**
 * OfferImportException used to indicate exceptions in importing offers.
 */
public class OfferImportException extends BusinessException
{

	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 4919527435648789531L;

	/**
	 * Instantiates a new offer import exception.
	 *
	 * @param message
	 *           the message
	 */
	public OfferImportException(final String message)
	{
		super(message);
	}

	/**
	 * Instantiates a new offer import exception.
	 *
	 * @param message
	 *           the message
	 * @param paramThrowable
	 *           the param throwable
	 */
	public OfferImportException(final String message, final Throwable paramThrowable)
	{
		super(paramThrowable, message);
	}

}
