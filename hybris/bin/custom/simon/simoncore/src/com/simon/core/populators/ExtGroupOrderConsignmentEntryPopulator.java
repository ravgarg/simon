package com.simon.core.populators;

import de.hybris.platform.commercefacades.order.converters.populator.GroupOrderConsignmentEntryPopulator;
import de.hybris.platform.commercefacades.order.data.ConsignmentData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * Extended GroupOrderConsignmentEntryPopulator remove the groupentry
 */
public class ExtGroupOrderConsignmentEntryPopulator extends GroupOrderConsignmentEntryPopulator
{
	@Override
	public void populate(final OrderModel source, final OrderData target)
	{

		final List<ConsignmentData> consignments = target.getConsignments();
		for (final ConsignmentData consignment : consignments)
		{
			consignment.setEntries(groupConsignmentEntries(consignment.getEntries(), target));
		}

	}
}
