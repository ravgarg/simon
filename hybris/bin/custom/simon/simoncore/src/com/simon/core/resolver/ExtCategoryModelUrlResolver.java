package com.simon.core.resolver;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.url.impl.DefaultCategoryModelUrlResolver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.simon.core.constants.SimonCoreConstants;


/**
 * This class extends DefaultCategoryModelUrlResolver. It appends complete category hierarchy in URL
 */
public class ExtCategoryModelUrlResolver extends DefaultCategoryModelUrlResolver
{
	@Value("${navigation.home.category.code}")
	protected String homeCategoryCode;

	/**
	 * This method calls OOB resolve method to fill in URL pattern. And then appends category hierarchy
	 *
	 * @param source
	 *           current Category Model
	 * @param catCodeURL
	 *           the URL to be appended
	 * @return final URL
	 */
	public String resolveCategoryPath(final CategoryModel source, final String catCodeURL)
	{
		String url = resolveURL(source);
		String catCode = StringUtils.isEmpty(catCodeURL) ? buildCategoryCodePathString(getCategoryPathFromCategory(source))
				: catCodeURL;

		if (StringUtils.isNotEmpty(url))
		{
			url = url.toLowerCase(Locale.US);
			catCode = StringUtils.isNotEmpty(catCode) ? catCode.toLowerCase(Locale.US) : catCode;
			url = url.substring(0, url.lastIndexOf('/') + 1).concat(catCode);
		}
		return url;
	}


	/**
	 * This method calls OOB resolve method to fill in URL second level hierarchy. And then appends category hierarchy
	 *
	 * @param source
	 *           current Category Model
	 * @param catCodeURL
	 *           the URL to be appended
	 * @return final URL
	 */
	public String resolveCategoryPathForChildNode(final CategoryModel source, final String catCodeURL)
	{
		return StringUtils.isEmpty(catCodeURL) ? buildCategoryCodePathString(getCategoryPathFromCategory(source)) : catCodeURL;
	}


	/**
	 * Method to call super class resolve method
	 *
	 * @param source
	 * @return
	 */
	protected String resolveURL(final CategoryModel source)
	{
		return resolve(source);
	}

	/**
	 * Method to call super class category path method
	 *
	 * @param source
	 * @return
	 */
	protected List<CategoryModel> getCategoryPathFromCategory(final CategoryModel source)
	{
		return getCategoryPath(source);
	}

	/**
	 * If the category hierarchy is empty, it creates one from DB
	 *
	 * @param path
	 *           Current category model path
	 * @return category hierarchy path
	 */
	protected String buildCategoryCodePathString(final List<CategoryModel> path)
	{
		final StringBuilder result = new StringBuilder();

		for (int i = 0; i < path.size(); i++)
		{
			if (i != 0)
			{
				result.append(SimonCoreConstants.UNDERSCORE);
			}
			result.append(path.get(i).getCode());
		}
		final String categoryCodePath = StringUtils.contains(result.toString(), homeCategoryCode)
				? result.substring(result.indexOf(homeCategoryCode) + homeCategoryCode.length() + 1) : StringUtils.EMPTY;
		return StringUtils.isNotEmpty(categoryCodePath) ? categoryCodePath.toLowerCase() : categoryCodePath;
	}

	/**
	 * {@inheritDoc}
	 *
	 * Overridden method to achieve correct URL for category pages.This will remove /spo/home from all the urls.
	 *
	 */
	@Override
	protected String buildPathString(final List<CategoryModel> path)
	{
		final CategoryModel homeCategory = getCommerceCategoryService().getCategoryForCode(homeCategoryCode);
		final List<Object> merchandizingCategories = new ArrayList<>(Arrays.asList(homeCategory));
		merchandizingCategories.addAll(homeCategory.getSupercategories());
		path.removeAll(merchandizingCategories);
		return super.buildPathString(path);
	}

}
