package com.simon.core.dao.impl;

import de.hybris.platform.promotionengineservices.model.CatForPromotionSourceRuleModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.dao.ExtPromotionSourceRuleDao;
import com.simon.core.solrfacetsearch.config.factories.ExtFlexibleSearchQuerySpecFactory;


/**
 * Custom Dao to fetch the PromotionSourceRules information from the database
 */
public class ExtPromotionSourceRuleDaoImpl extends AbstractItemDao implements ExtPromotionSourceRuleDao
{
	@Autowired
	private ExtFlexibleSearchQuerySpecFactory extFlexibleSearchQuerySpecFactory;

	/**
	 * Find the categories for which the promotions have been updated
	 */
	@Override
	public List<CatForPromotionSourceRuleModel> findUpdatedCategoriesForPromotionSourceRule(final IndexedType indexedType,
			final FacetSearchConfig facetSearchConfig) throws SolrServiceException
	{
		final SearchResult<CatForPromotionSourceRuleModel> searchResult = this.getFlexibleSearchService()
				.search(extFlexibleSearchQuerySpecFactory.fetchCategoriesForUpdatedPromotions(indexedType, facetSearchConfig));
		return searchResult.getResult();
	}

}