package com.simon.core.dynamichandlers;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.core.model.ShopModel;


/**
 * ProductUrlHandler is a dynamic handler for attribute {@link GenericVariantProductModel#PRODUCTURL}
 */
public class ProductUrlHandler extends AbstractDynamicAttributeHandler<String, GenericVariantProductModel>
{

	@Resource
	private ConfigurationService configurationService;


	private static final Logger LOGGER = LoggerFactory.getLogger(ProductUrlHandler.class);

	/**
	 * Method to get product url : 1 - check if test mode toggle is on or off 2 - If on , then return shop domain
	 * combined with test product url 3 - If off , then return raw product url from feed
	 */
	@Override
	public String get(final GenericVariantProductModel product)
	{
		StringBuilder returnUrl = null;
		final ShopModel shop = product.getShop();
		if (Objects.nonNull(shop) && StringUtils.isNotEmpty(shop.getTestDomain()) && shop.isTestMode())
		{
			returnUrl = new StringBuilder(shop.getTestDomain());
			if (StringUtils.isNotEmpty(product.getTestProductPath()))
			{
				returnUrl.append(product.getTestProductPath());
			}
		}
		else
		{
			returnUrl = new StringBuilder(product.getRawProductUrl());
		}
		LOGGER.debug("Product URL returned : " + returnUrl.toString());
		return returnUrl.toString();
	}
}
