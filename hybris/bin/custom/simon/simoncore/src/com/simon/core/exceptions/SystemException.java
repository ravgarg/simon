/**
 *
 */
package com.simon.core.exceptions;


/**
 * This <tt>class</tt> will be used for application specific {@link SystemException}. This class represents the type of
 * RuntimeException which are expected to occur and can be handled.
 *
 */
public class SystemException extends RuntimeException
{


	/**
	 * Serial Version UID.
	 */
	private static final long serialVersionUID = 99003L;

	/** Exception code. */
	private final String code;



	/**
	 * Instantiates a new system exception.
	 *
	 * @param message
	 *           the message
	 */
	public SystemException(final String message)
	{
		super(message);
		this.code = "";
	}


	/**
	 * Instantiates a new system exception.
	 *
	 * @param message
	 *           the message
	 * @param code
	 *           the code
	 */
	public SystemException(final String message, final String code)
	{
		super(message);
		this.code = code;
	}



	/**
	 * Instantiates a new system exception.
	 *
	 * @param message
	 *           the message
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public SystemException(final String message, final Throwable cause, final String code)
	{
		super(message, cause);
		this.code = code;
	}



	/**
	 * Instantiates a new system exception.
	 *
	 * @param cause
	 *           the cause
	 * @param code
	 *           the code
	 */
	public SystemException(final Throwable cause, final String code)
	{
		super(cause);
		this.code = code;
	}


	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode()
	{
		return this.code;
	}


}
