package com.simon.core.promotions;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ruleengineservices.converters.populator.AbstractOrderRaoPopulator;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;

import com.simon.promotion.rao.RetailerRAO;


/**
 * Populates {@link RetailerRAO} entries from {@link CartModel}
 */
public class RetailerRaoEntryPopulator extends AbstractOrderRaoPopulator<CartModel, RetailerRAO>
{
	@Override
	public void populate(final CartModel source, final RetailerRAO target)
	{
		ServicesUtil.validateParameterNotNull(target.getRetailerId(), "Target should be prepopulated with retailer id");

		if (target.getActions() == null)
		{
			target.setActions(new LinkedHashSet<>());
		}

		target.setCode(source.getCode());
		if (source.getCurrency() != null)
		{
			target.setCurrencyIsoCode(source.getCurrency().getIsocode());
		}
		final Double deiveryCost = source.getRetailersDeliveryCost().get(target.getRetailerId());
		final BigDecimal deliveryCost = BigDecimal.valueOf(deiveryCost != null ? deiveryCost : 0.0);
		target.setDeliveryCost(Objects.isNull(source.getDeliveryCost()) ? BigDecimal.ZERO : deliveryCost);
		if (CollectionUtils.isNotEmpty(source.getEntries()))
		{
			final List<OrderEntryRAO> list = new ArrayList<>();

			source.getEntries().stream()
					.filter(entry -> entry.getProduct().getShop().getId().equalsIgnoreCase(target.getRetailerId()))
					.forEach(retEntry -> convertEntry(list, retEntry, target));

			target.setEntries(new LinkedHashSet<>(list));
		}
		if (CollectionUtils.isNotEmpty(source.getDiscounts()))
		{
			target.setDiscountValues(Converters.convertAll(source.getDiscounts(), this.getDiscountConverter()));
		}
		this.convertAndSetUser(target, source.getUser());
		this.convertAndSetPaymentMode(target, source.getPaymentMode());

	}

	private boolean convertEntry(final List<OrderEntryRAO> list, final AbstractOrderEntryModel retEntry, final RetailerRAO target)
	{
		final OrderEntryRAO entry = getEntryConverter().convert(retEntry);
		entry.setOrder(target);
		return list.add(entry);
	}

}
