/**
 * Copyright (c) 2015 Simon Property Group - All Rights Reserved
 */

package com.simon.core.util;

import de.hybris.platform.servicelayer.exceptions.SystemException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;



/**
 * Web Utility define the All Type of format which supports by web (content-type).
 */
public class WebUtils
{

	/** The Constant ZIP_MIME_TYPE. */
	public static final String ZIP_MIME_TYPE = "application/zip";

	/** The Constant APPLICATION_POSTSCRIPT. */
	public static final String APPLICATION_POSTSCRIPT = "application/postscript";

	/** The Constant AUDIO_TYPE. */
	public static final String AUDIO_TYPE = "audio/x-mpeg";

	/** The Constant IMAGE_TYPE. */
	public static final String IMAGE_TYPE = "image/pict";

	/** The Constant BODY_TYPE. */
	public static final String BODY_TYPE = "text/html";

	/** The Constant VIDEO_TYPE. */
	public static final String VIDEO_TYPE = "video/mpeg";



	/** The Constant UTF_8. */
	public static final String UTF_8 = "UTF-8";

	/** The Constant DEFAULT_MIME_TYPE. */
	private static final String DEFAULT_MIME_TYPE = "application/octet-stream";

	/** The Constant MIME_TYPES. */
	private static final Map<String, String> MIME_TYPES = new HashMap<>();

	static
	{
		MIME_TYPES.put("abs", AUDIO_TYPE);
		MIME_TYPES.put("ai", APPLICATION_POSTSCRIPT);
		MIME_TYPES.put("aif", "audio/x-aiff");
		MIME_TYPES.put("aiff", "audio/x-aiff");
		MIME_TYPES.put("aim", "application/x-aim");
		MIME_TYPES.put("art", "image/x-jg");
		MIME_TYPES.put("asf", "video/x-ms-asf");
		MIME_TYPES.put("asx", "video/x-ms-asf");
		MIME_TYPES.put("au", "audio/basic");
		MIME_TYPES.put("avi", "video/x-msvideo");
		MIME_TYPES.put("bmp", "image/bmp");
		MIME_TYPES.put("body", BODY_TYPE);
		MIME_TYPES.put("cer", "application/x-x509-ca-cert");
		MIME_TYPES.put("css", "text/css");
		MIME_TYPES.put("doc", "application/msword");
		MIME_TYPES.put("dtd", "text/plain");
		MIME_TYPES.put("dv", "video/x-dv");
		MIME_TYPES.put("dvi", "application/x-dvi");
		MIME_TYPES.put("eps", APPLICATION_POSTSCRIPT);
		MIME_TYPES.put("gif", "image/gif");
		MIME_TYPES.put("gz", "application/x-gzip");
		MIME_TYPES.put("hqx", "application/mac-binhex40");
		MIME_TYPES.put("htm", BODY_TYPE);
		MIME_TYPES.put("html", BODY_TYPE);
		MIME_TYPES.put("jpeg", "image/jpeg");
		MIME_TYPES.put("jpg", "image/jpeg");
		MIME_TYPES.put("js", "text/javascript");
		MIME_TYPES.put("m3u", "audio/x-mpegurl");
		MIME_TYPES.put("mac", "application/x-macpaint");
		MIME_TYPES.put("mid", "audio/x-midi");
		MIME_TYPES.put("midi", "audio/x-midi");
		MIME_TYPES.put("mov", "video/quicktime");
		MIME_TYPES.put("movie", "video/x-sgi-movie");
		MIME_TYPES.put("mp1", AUDIO_TYPE);
		MIME_TYPES.put("mp2", AUDIO_TYPE);
		MIME_TYPES.put("mp3", AUDIO_TYPE);
		MIME_TYPES.put("mpa", AUDIO_TYPE);
		MIME_TYPES.put("mpe", VIDEO_TYPE);
		MIME_TYPES.put("mpeg", VIDEO_TYPE);
		MIME_TYPES.put("mpg", VIDEO_TYPE);
		MIME_TYPES.put("pct", IMAGE_TYPE);
		MIME_TYPES.put("pict", IMAGE_TYPE);
		MIME_TYPES.put("pic", IMAGE_TYPE);
		MIME_TYPES.put("pdf", "application/pdf");
		MIME_TYPES.put("pict", IMAGE_TYPE);
		MIME_TYPES.put("pic", IMAGE_TYPE);
		MIME_TYPES.put("png", "image/x-png");
		MIME_TYPES.put("ps", APPLICATION_POSTSCRIPT);
		MIME_TYPES.put("psd", "application/x-photoshop");
		MIME_TYPES.put("qt", "video/quicktime");
		MIME_TYPES.put("rtf", "application/rtf");
		MIME_TYPES.put("sqlite", "application/octet-stream");
		MIME_TYPES.put("svg", "image/svg+xml");
		MIME_TYPES.put("svgz", "image/svg+xml");
		MIME_TYPES.put("swf", "application/x-shockwave-flash");
		MIME_TYPES.put("tar", "application/x-tar");
		MIME_TYPES.put("tif", "image/tiff");
		MIME_TYPES.put("tiff", "image/tiff");
		MIME_TYPES.put("txt", "text/plain");
		MIME_TYPES.put("wav", "audio/wav");
		MIME_TYPES.put("xml", "text/xml");
		MIME_TYPES.put("xsl", "text/xml");
		MIME_TYPES.put("zip", ZIP_MIME_TYPE);
	}

	/**
	 * Instantiates a new web utils.
	 */
	private WebUtils()
	{

	}

	/**
	 * Gets the mime type.
	 *
	 * @param fileName
	 *           the file name
	 * @return the mime type
	 */
	public static String getMimeType(final String fileName)
	{

		final String extension = isBlankFilenameExten(fileName);

		final String mimeType = MIME_TYPES.get(extension);

		if (mimeType == null)
		{
			return DEFAULT_MIME_TYPE;
		}

		return mimeType;
	}

	/**
	 * Method to check for blank filename or extension.
	 *
	 * @param fileName
	 *
	 * @return extension.
	 */
	private static String isBlankFilenameExten(final String fileName)
	{
		if (StringUtils.isBlank(fileName))
		{
			return DEFAULT_MIME_TYPE;
		}

		final String extension = getExtension(fileName);

		if (StringUtils.isBlank(extension))
		{
			return DEFAULT_MIME_TYPE;
		}
		return extension;
	}

	/**
	 * Gets the extension.
	 *
	 * @param fileName
	 *           the file name
	 * @return the extension
	 */
	public static String getExtension(final String fileName)
	{

		if (StringUtils.isBlank(fileName))
		{
			return null;
		}

		final int index = fileName.lastIndexOf('.');

		if (index >= 0)
		{
			return fileName.substring(index + 1);
		}

		return null;
	}

	/**
	 * Url encode.
	 *
	 * @param str
	 *           the str
	 * @return the string
	 */
	public static String urlEncode(final String str)
	{

		if (StringUtils.isBlank(str))
		{
			return null;
		}


		return encodeURL(str);

	}



	/**
	 * Performs urlEncoding but overrides the space encoding (+) with the %20 value.
	 *
	 * @param str
	 *           string to encode
	 * @return encoded string
	 */
	public static String urlEncodeOverrideSpace(final String str)
	{

		if (StringUtils.isBlank(str))
		{
			return null;
		}


		final String encoded = encodeURL(str);

		return encoded.replace("+", "%20");

	}

	/**
	 * Url decode.
	 *
	 * @param str
	 *           the str
	 * @return the string
	 */
	public static String urlDecode(final String str)
	{

		return urlDecode(str, UTF_8);
	}

	/**
	 * Url decode.
	 *
	 * @param str
	 *           the str
	 * @param characterEncoding
	 *           the character encoding
	 * @return the string
	 */
	public static String urlDecode(final String str, final String characterEncoding)
	{

		if (StringUtils.isBlank(str))
		{
			return null;
		}

		String enc = characterEncoding;

		if (StringUtils.isBlank(characterEncoding))
		{
			enc = UTF_8;
		}

		return decodeURL(str, enc);

	}

	/**
	 * Decode URL.
	 *
	 * @param str
	 *           the str
	 * @param enc
	 *           the enc
	 * @return the string
	 */
	protected static String decodeURL(final String str, final String enc)
	{
		try
		{
			return URLDecoder.decode(str, enc);
		}
		catch (final UnsupportedEncodingException e)
		{
			throw new SystemException(e);
		}

	}

	/**
	 * Encode URL.
	 *
	 * @param str
	 *           the str
	 * @return the string
	 */
	protected static String encodeURL(final String str)
	{
		try
		{
			return URLEncoder.encode(str, UTF_8);
		}
		catch (final UnsupportedEncodingException e)
		{
			throw new SystemException(e);
		}
	}

}
