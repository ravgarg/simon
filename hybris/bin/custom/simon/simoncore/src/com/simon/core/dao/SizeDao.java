package com.simon.core.dao;

import java.util.List;

import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.model.SizeModel;


/**
 * DAO for {@link SizeModel} contains abstract method for operations on {@link SizeModel}
 */
public interface SizeDao
{

	/**
	 * Gets the List of Sizes.
	 *
	 * @param sizeCodes
	 *
	 * @return the {@link List<SizeModel>}
	 */
	List<SizeModel> getAllSizesByCode(List<String> sizeCodes);

	/**
	 * Find Mall by code. Returns {@link SizeModel} for given Mall <code>code</code>.
	 *
	 * @param sizeCode
	 *           the Size <code>code</code>
	 * @return the Size model
	 *
	 */
	SizeModel getSizeForId(String sizeCode);

	/**
	 * Find all Sizes . Returns {@link List<SizeModel>}.
	 *
	 * @return a list of Sizes Model
	 */
	List<SizeModel> findAllSizes();

	/**
	 * Gets the Sequence number for given Sizes.
	 *
	 * @return the ProductSizeSequenceModel
	 */
	ProductSizeSequenceModel getSequenceForSizes(String sizeCode);

	/**
	 * Gets the Sequence numbers for all Sizes.
	 *
	 * @return the List<ProductSizeSequenceModel>
	 */
	List<ProductSizeSequenceModel> getAllSequencesForSizes();
}
