package com.simon.core.services.impl;

import static org.apache.commons.collections.CollectionUtils.isEmpty;

import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.simon.core.model.ShopGatewayModel;
import com.simon.core.services.ShopGatewayService;


/**
 * The Class fetches configured target system
 */
public class ShopGatewayServiceImpl implements ShopGatewayService
{

	@Resource
	private GenericDao<ShopGatewayModel> shopGatewayGenericDao;

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public ShopGatewayModel getShopGatewayForCode(final String shopGatewayCode)
	{
		final Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("code", shopGatewayCode);
		final List<ShopGatewayModel> shopGateway = shopGatewayGenericDao.find(paramMap);
		return isEmpty(shopGateway) ? null : shopGateway.get(0);
	}

}
