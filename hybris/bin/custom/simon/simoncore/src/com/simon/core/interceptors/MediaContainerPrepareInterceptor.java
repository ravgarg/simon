package com.simon.core.interceptors;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class sets version to every Media associated with MediaContainer if MediaContainer's version attribute is
 * modified.
 */
public class MediaContainerPrepareInterceptor implements PrepareInterceptor<MediaContainerModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(MediaContainerPrepareInterceptor.class);

	/**
	 * Sets version to Media and marks to persist it in DB.
	 */
	@Override
	public void onPrepare(final MediaContainerModel mediaContainer, final InterceptorContext ctx) throws InterceptorException
	{
		if (ctx.isModified(mediaContainer, MediaContainerModel.VERSION))
		{
			final int mediaContainerVersion = mediaContainer.getVersion();
			mediaContainer.getMedias().stream().forEach(media -> {
				media.setVersion(mediaContainerVersion);
				LOGGER.debug("Setting Expiry version {} on Media {}", mediaContainerVersion, media.getCode());
				ctx.registerElementFor(media, PersistenceOperation.SAVE);
			});
		}

	}

}
