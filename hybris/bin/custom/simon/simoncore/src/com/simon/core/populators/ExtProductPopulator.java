package com.simon.core.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import com.simon.core.util.CommonUtils;


/**
 * Converter implementation for {@link de.hybris.platform.core.model.product.ProductModel} as source and
 * {@link de.hybris.platform.commercefacades.product.data.ProductData} as target type.
 */
public class ExtProductPopulator extends ProductPopulator
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		super.populate(source, target);
		target.setName(CommonUtils.replaceAsciiWithHtmlCode(source.getName()));
	}
}
