package com.simon.core.api.impl;

import java.util.Collections;
import java.util.List;

import com.mirakl.client.core.internal.Entity;
import com.mirakl.client.core.security.MiraklCredential;
import com.mirakl.client.mmp.core.MiraklMarketplacePlatformFrontOperatorApiClient;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.mirakl.client.mmp.front.request.order.accept.MiraklAcceptOrderRequest;
import com.mirakl.client.mmp.request.offer.MiraklOffersExportRequest;
import com.simon.core.api.SimonMiraklMarketplacePlatformFrontOperatorApi;
import com.simon.core.api.mirakl.dto.SimonMiraklShipOrderRequest;
import com.simon.core.api.mirakl.dto.SimonMiraklTrackingOrderRequest;

/**
 * This is the implementation class of
 * {@link SimonMiraklMarketplacePlatformFrontOperatorApi} used to call Mirakl
 * API for accept/reject or order tracking.
 */
public class SimonMiraklMarketplacePlatformFrontOperatorApiClient extends
		MiraklMarketplacePlatformFrontOperatorApiClient implements SimonMiraklMarketplacePlatformFrontOperatorApi {

	protected SimonMiraklMarketplacePlatformFrontOperatorApiClient(String endpoint, MiraklCredential credential) {
		super(endpoint, credential);
	}

	@Override
	public List<MiraklExportOffer> exportOffers(MiraklOffersExportRequest paramMiraklOffersExportRequest) {
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	public void acceptOrRejectOrder(MiraklAcceptOrderRequest request) {
		put(request, Entity.json(request), 204);

	}

	/**
	 * {@inheritDoc}
	 */
	public void shipOrder(SimonMiraklShipOrderRequest request) {
		put(request, Entity.json(Integer.valueOf(0)), 204);
	}

	/**
	 * {@inheritDoc}
	 */
	public void trackOrder(SimonMiraklTrackingOrderRequest request) {
		put(request, Entity.json(Integer.valueOf(0)), 204);
	}

}
