package com.simon.core.provider.impl;

import de.hybris.platform.catalog.model.classification.ClassificationClassModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.CategoryPathValueProvider;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.model.MerchandizingCategoryModel;


/**
 * This class extends CategoryPathValueProvider, to fetch category paths for only Merchandizing Category Model
 */
public class ExtMerchandizingCategoryPathValueProvider extends CategoryPathValueProvider
{

	/**
	 * Only merchandizing categories belonging to base product are indexed.
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		ProductModel baseProduct;
		if (model instanceof GenericVariantProductModel)
		{
			baseProduct = ((GenericVariantProductModel) model).getBaseProduct();
		}
		else
		{
			baseProduct = (ProductModel) model;
		}
		final Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig,
				indexedProperty, baseProduct);
		final List<CategoryModel> merchCategories = categories.stream()
				.filter(categoryModel -> categoryModel instanceof MerchandizingCategoryModel).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(merchCategories))
		{
			final Collection<FieldValue> fieldValues = new ArrayList<>();

			final Set<String> categoryPaths = getCategoryPaths(merchCategories);
			fieldValues.addAll(createFieldValue(categoryPaths, indexedProperty));

			return fieldValues;
		}
		else
		{
			return Collections.emptyList();
		}
	}

	/**
	 * This method overrides OOB method, to append category name, along with category codes.
	 */
	@Override
	protected void accumulateCategoryPaths(final List<CategoryModel> categoryPath, final Set<String> output)
	{
		final StringBuilder accumulator = new StringBuilder();
		String categoryCode;
		String categoryName;
		for (final CategoryModel category : categoryPath)
		{
			categoryCode = category.getCode();
			categoryName = category.getName();
			if (category instanceof ClassificationClassModel)
			{
				break;
			}
			accumulator.append('/').append(categoryCode).append('|')
					.append(StringUtils.isEmpty(categoryName) ? categoryCode : categoryName);
			output.add(accumulator.toString());
		}
	}

}
