package com.simon.core.dao.impl;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.impl.DefaultMediaContainerDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtMediaContainerDao;


/**
 * This class is a extension to default media container which have additional method.
 */
public class ExtMediaContainerDaoImpl extends DefaultMediaContainerDao implements ExtMediaContainerDao, Serializable
{
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method get all mediaContainer which have version value greater then zero.
	 */
	public List<MediaContainerModel> getMediaContainersWithVersionGreaterThenZero(final Date cleanupAgeDate)
	{
		final FlexibleSearchQuery query = getFlexibleSearchQuery(
				SimonFlexiConstants.FIND_ALL_MEDIA_CONTAINER_WHERE_VERSION_GT_ZERO);
		query.addQueryParameter("cleanupAgeDate", cleanupAgeDate);
		final SearchResult<MediaContainerModel> result = getFlexibleSearchService().search(query);
		return result.getResult();
	}

	protected FlexibleSearchQuery getFlexibleSearchQuery(final String code)
	{
		return new FlexibleSearchQuery(code);
	}

}
