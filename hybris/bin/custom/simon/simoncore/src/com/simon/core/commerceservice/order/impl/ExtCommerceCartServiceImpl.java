package com.simon.core.commerceservice.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import com.simon.core.commerceservice.order.ExtCommerceCartService;


/**
 * This Class is used to validate cart with recalculated method. This require in only case when the price will be
 * changes in background so the price need to be updated on cart page
 */
public class ExtCommerceCartServiceImpl extends DefaultCommerceCartService implements ExtCommerceCartService
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CommerceCartModification> getChangeEntriesInCart(final CommerceCartParameter parameter)
			throws CommerceCartModificationException
	{
		final CartModel cartModel = parameter.getCart();
		validateParameterNotNull(cartModel, "Cart model cannot be null");

		final List<CommerceCartModification> modifications = getCartValidationStrategy().validateCart(parameter);

		// We only care about modifications that weren't successful
		final List<CommerceCartModification> errorModifications = new ArrayList<>(modifications.size());
		for (final CommerceCartModification modification : modifications)
		{
			if (!CommerceCartModificationStatus.SUCCESS.equals(modification.getStatusCode()))
			{
				errorModifications.add(modification);
			}
		}
		//Change the calculate method to recalculate as per simon Requirement
		recalculateCart(parameter);
		return errorModifications;
	}

}
