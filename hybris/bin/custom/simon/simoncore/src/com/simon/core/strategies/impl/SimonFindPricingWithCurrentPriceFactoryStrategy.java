package com.simon.core.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.impl.FindPricingWithCurrentPriceFactoryStrategy;
import de.hybris.platform.util.PriceValue;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.facades.product.OfferFacade;


/**
 * Simon implementation of price, taxes and discounts resolver strategies
 * {@link SimonFindPricingWithCurrentPriceFactoryStrategy}, that resolves values for calculation from current session's
 * price factory.
 */
public class SimonFindPricingWithCurrentPriceFactoryStrategy extends FindPricingWithCurrentPriceFactoryStrategy
{
	private static final long serialVersionUID = 1;

	/** The Constant LOG used to log the intermediary outputs at various points. */
	private static final Logger LOG = LoggerFactory.getLogger(SimonFindPricingWithCurrentPriceFactoryStrategy.class);
	/** The Constant DOUBLE_ZERO stores the double with zero value. */
	private static final double DOUBLE_ZERO = 0.00d;

	/** The offer facade used to fetch the offer prices for provided product . */
	private OfferFacade offerFacade;

	/**
	 * Method is used to find the BasePrice of the product from the associated offer from {@link OfferData} or from
	 * {@link AbstractOrderEntryModel#getTwoTapProductBasePrice()} it will first look for price with respect to TwoTap
	 * otherwise find Mirakl offer Price.
	 *
	 * @param entry
	 *           of type {@link AbstractOrderEntryModel} used to extract values for Two Tap prices.
	 * @return object of type {@link PriceValue} to return base price.
	 * @throws CalculationException
	 *            the calculation exception
	 */
	@Override
	public PriceValue findBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		PriceValue basePrice = null;

		final String currencyCode = entry.getOrder().getCurrency() == null ? "USD" : entry.getOrder().getCurrency().getIsocode();

		if (entry.getOrder().getAdditionalCartInfo() == null || entry.getAdditionalProductBasePrice() == null)
		{
			final ProductModel product = entry.getProduct();

			validateParameterNotNull(product, "No product found in request context");

			final List<OfferData> offers = getOfferFacade().getOffersForProductCode(product.getCode());

			if (offers == null || offers.isEmpty())
			{
				LOG.error("empty offers.");
			}
			else
			{
				final OfferData offerData = offers.get(0);

				LOG.debug("offer located. ");
				LOG.debug("offerData.getProductCode(): {}", offerData.getProductCode());
				LOG.debug("offerData.getPrice():       {}", offerData.getPrice().getValue().doubleValue());
				LOG.debug("offerData.getCode():        {} .", offerData.getCode());
				LOG.debug("offerData.getShopName():   {} / offerData.getShopCode(): {}", offerData.getShopName(),
						offerData.getShopCode());

				final PriceData priceData = offerData.getPrice();
				basePrice = getMiraklBasePrice(priceData, currencyCode);
			}
		}
		else
		{

			basePrice = getTwoTapBasePrice(entry.getAdditionalProductBasePrice(), currencyCode);
		}

		if (basePrice != null)
		{
			return basePrice;
		}

		return getBasePrice(entry);
	}

	/**
	 * Gets the offer facade.
	 *
	 * @return the offerFacade
	 */
	public OfferFacade getOfferFacade()
	{
		return offerFacade;
	}

	/**
	 * Sets the offer facade.
	 *
	 * @param offerFacade
	 */
	public void setOfferFacade(final OfferFacade offerFacade)
	{
		this.offerFacade = offerFacade;
	}

	/**
	 * Gets the base price {@link PriceValue} calculated for the order entry {@link AbstractOrderEntryModel} with the
	 * OOTB logic provided in super class {@link FindPricingWithCurrentPriceFactoryStrategy}.
	 *
	 * @param entry
	 *           {@link AbstractOrderEntryModel} used to extract the base price.
	 * @return the base price {@link PriceValue}
	 * @throws CalculationException
	 *            {@link CalculationException}
	 */
	protected PriceValue getBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return super.findBasePrice(entry);
	}

	/**
	 * Gets the mirakl base price by creating a new {@link PriceValue} object with provided currency code and price data.
	 *
	 * @param priceData
	 *           {@link PriceData} used to create the new {@link PriceValue}
	 * @param currencyCode
	 * @return the mirakl base price {@link PriceValue}
	 */
	private PriceValue getMiraklBasePrice(final PriceData priceData, final String currencyCode)
	{
		return new PriceValue(currencyCode, priceData.getValue().doubleValue(), true);
	}

	/**
	 * Gets the two tap base price {@link PriceValue} by calculated base price from priceStr and currency code provided.
	 *
	 * @param priceStr
	 *           used to calculate base price.
	 * @param currencyCode
	 * @return the two tap base price of type {@link PriceValue}
	 */
	private PriceValue getTwoTapBasePrice(final String priceStr, final String currencyCode)
	{
		if (StringUtils.isNotEmpty(priceStr))
		{
			final String amount = priceStr.substring(1);
			final double ttBasePrice = Double.parseDouble(amount);
			if (ttBasePrice > DOUBLE_ZERO)
			{
				// Only use TwoTap shippingCost when it is larger than 0.00d
				final PriceValue bPrice = new PriceValue(currencyCode, ttBasePrice, true);
				LOG.debug("BasePrice returned from TT cart/status {}", bPrice);
				return bPrice;
			}
		}
		return null;
	}

}
