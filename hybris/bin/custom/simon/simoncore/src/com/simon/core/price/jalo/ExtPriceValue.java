package com.simon.core.price.jalo;

import de.hybris.platform.util.PriceValue;

import java.util.Date;


/**
 * This class extends PriceValue to accomodate extra information needed in this project, i.e,
 * listPrice,startDate,endDate and OfferId
 */
public class ExtPriceValue extends PriceValue
{


	/**
	 *
	 */
	private static final long serialVersionUID = 1L;


	private final double listPrice;
	private final Date salePriceStartDate;
	private final Date salePriceEndDate;
	private final String offerId;
	private final int leadtimeToShip;



	/**
	 * This constructor populates the additional properties i.e listPrice,salePriceStartDate,salePriceEndDate,offerId
	 *
	 * @param currencyIso
	 * @param price
	 * @param netto
	 * @param lp
	 * @param spStartDate
	 * @param spEndDate
	 * @param offer
	 * @param leadtimeToShip
	 */
	public ExtPriceValue(final String currencyIso, final double price, final boolean netto, final double lp,
			final Date spStartDate, final Date spEndDate, final String offer, final int leadtimeToShip)
	{
		super(currencyIso, price, netto);
		this.listPrice = lp;
		this.salePriceStartDate = spStartDate;
		this.salePriceEndDate = spEndDate;
		this.offerId = offer;
		this.leadtimeToShip = leadtimeToShip;
	}

	@Override
	public boolean equals(final Object object)
	{
		final boolean isSuperEqual = super.equals(object);
		return isSuperEqual && object instanceof ExtPriceValue
				&& this.salePriceStartDate == ((ExtPriceValue) object).salePriceStartDate
				&& this.salePriceEndDate == ((ExtPriceValue) object).salePriceEndDate
				&& this.listPrice == ((ExtPriceValue) object).listPrice;
	}

	@Override
	public int hashCode()
	{
		return super.hashCode() ^ this.salePriceStartDate.hashCode() ^ this.salePriceEndDate.hashCode() ^ (this.offerId.hashCode())
				^ (int) this.listPrice;
	}

	/**
	 * @return offerId
	 */
	public String getOfferId()
	{
		return offerId;
	}


	/**
	 * @return sale price
	 */
	public double getListPrice()
	{
		return listPrice;
	}

	/**
	 * @return start date of the sale price
	 */
	public Date getSalePriceStartDate()
	{
		return salePriceStartDate;
	}

	/**
	 * @return end date of the sale price
	 */
	public Date getSalePriceEndDate()
	{
		return salePriceEndDate;
	}

	/**
	 * @return int of the sale price
	 */
	public int getLeadtimeToShip()
	{
		return leadtimeToShip;
	}

}
