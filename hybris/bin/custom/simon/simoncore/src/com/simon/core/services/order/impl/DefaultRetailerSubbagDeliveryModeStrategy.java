/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.core.services.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.DeliveryModeService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import com.simon.core.services.order.RetailerSubbagDeliveryModeStrategy;


/**
 * This class defines how to associate a retailer sub-bag specific delivery modes to the cart
 */
public class DefaultRetailerSubbagDeliveryModeStrategy implements RetailerSubbagDeliveryModeStrategy
{
	@Resource
	private ModelService modelService;
	@Resource
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;
	@Resource
	private DeliveryModeService deliveryModeService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean setRetailerSubbagDeliveryMode(final CartModel cartModel, final String retailerId, final String deliveryMode)
	{
		validateParameterNotNull(cartModel, "Cart model cannot be null");
		validateParameterNotNull(deliveryMode, "Delivery mode model cannot be null");
		final Map<String, DeliveryModeModel> retailerDeliveryMode = new HashMap<>();
		retailerDeliveryMode.putAll(cartModel.getRetailersDeliveryModes());
		final DeliveryModeModel deliveryModeModel = deliveryModeService.getDeliveryModeForCode(deliveryMode.toLowerCase(Locale.US));
		retailerDeliveryMode.put(retailerId, deliveryModeModel);
		cartModel.setRetailersDeliveryModes(retailerDeliveryMode);
		modelService.save(cartModel);
		return true;
	}


}
