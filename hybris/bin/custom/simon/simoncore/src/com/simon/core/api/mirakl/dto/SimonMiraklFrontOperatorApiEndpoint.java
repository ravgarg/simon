package com.simon.core.api.mirakl.dto;

import com.mirakl.client.core.internal.MiraklApiEndpoint;

/**
 * Enum used to get Mirakl Front Operator API endpoint.
 */
public enum SimonMiraklFrontOperatorApiEndpoint implements MiraklApiEndpoint {

	OR23(new String[] { "orders", "{order}", "tracking" }), OR24(new String[] { "orders", "{order}", "ship" });

	private final String[] paths;

	private SimonMiraklFrontOperatorApiEndpoint(String... paths) {
		this.paths = paths;
	}

	public String[] getPaths() {
		return paths;
	}

}
