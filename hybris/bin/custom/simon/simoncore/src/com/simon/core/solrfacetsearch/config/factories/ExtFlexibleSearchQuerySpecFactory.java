package com.simon.core.solrfacetsearch.config.factories;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;


/**
 * Custom Create Flexible Search query for getting the lastIndexTime as param
 */
public interface ExtFlexibleSearchQuerySpecFactory
{
	/**
	 * Fetched the categories for which the promotions got updated
	 * 
	 * @param indexedType
	 * @param facetSearchConfig
	 * @return FlexiQuery for getting the category codes for which the promotions are updated recently
	 * @throws SolrServiceException
	 */
	FlexibleSearchQuery fetchCategoriesForUpdatedPromotions(final IndexedType indexedType,
			final FacetSearchConfig facetSearchConfig) throws SolrServiceException;
}