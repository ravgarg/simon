package com.simon.core.sitemap.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.CategoryPageSiteMapGenerator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Extension of CategoryPageSiteMapGenerator Get only merchanizingcategory
 *
 */
public class ExtCategoryPageSiteMapGenerator extends CategoryPageSiteMapGenerator
{
	/**
	 * This method used to get the category page results.
	 *
	 * @param siteModel
	 * @return List<CategoryModel>
	 */
	@Override
	protected List<CategoryModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final String query = "SELECT {c.pk} FROM { MerchandizingCategory AS c JOIN CatalogVersion AS cv ON {c.catalogVersion}={cv.pk} "
				+ " JOIN Catalog AS cat ON {cv.pk}={cat.activeCatalogVersion} "
				+ " JOIN CMSSite AS site ON {cat.pk}={site.defaultCatalog}}  WHERE {site.pk} = ?site "
				+ " AND NOT exists ({{select {cr.pk} from {CategoriesForRestriction as cr} where {cr.target} = {c.pk} }})";

		final Map<String, Object> params = new HashMap<String, Object>();
		params.put("site", siteModel);
		return doSearch(query, params, CategoryModel.class);
	}
}
