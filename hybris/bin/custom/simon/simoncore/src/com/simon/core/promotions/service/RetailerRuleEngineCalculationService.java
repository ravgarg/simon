package com.simon.core.promotions.service;

import de.hybris.platform.ruleengineservices.rao.DiscountRAO;

import java.math.BigDecimal;

import com.simon.promotion.rao.RetailerRAO;


/**
 * This interface declares methods which are needed by Rule Engine to calculations for Retailer sub-bag
 */
public interface RetailerRuleEngineCalculationService
{
	/**
	 * Updates the RetailerRAO fields after recalculations and creates DiscountRAO
	 * 
	 * @param retailer
	 * @param absolute
	 * @param amount
	 * @return DiscountRAO which is used to communicate discount information to the strategy corresponding to the
	 *         promotion
	 */
	DiscountRAO addOrderLevelDiscount(RetailerRAO retailer, boolean absolute, BigDecimal amount);
}
