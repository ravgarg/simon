package com.simon.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;

import com.simon.core.model.BackgroundImageItemModel;


/**
 * This class will perform the required tasks that need to be executed at the time of object creation.
 */
public class BackgroundImageItemPrepareInterceptor implements PrepareInterceptor<BackgroundImageItemModel>
{

	@Override
	public void onPrepare(final BackgroundImageItemModel backgroundImageItem, final InterceptorContext interceptorContext)
			throws InterceptorException
	{
		if (null != backgroundImageItem.getDesktopImage() && null == backgroundImageItem.getMobileImage())
		{
			backgroundImageItem.setMobileImage(backgroundImageItem.getDesktopImage());
		}
	}

}
