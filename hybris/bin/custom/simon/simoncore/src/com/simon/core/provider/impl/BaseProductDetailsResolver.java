package com.simon.core.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.ModelAttributesValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.apache.commons.lang.StringUtils;


/**
 * Resolver for getting base product details. It takes into consideration generic variant products.
 */
public class BaseProductDetailsResolver extends ModelAttributesValueResolver<GenericVariantProductModel>
{
	/**
	 * This method checks for the base product for a given generic variant product and fetches model attribute value for
	 * that base product.
	 *
	 * @see de.hybris.platform.solrfacetsearch.provider.impl.ModelAttributesValueResolver#getAttributeValue(de.hybris.platform.solrfacetsearch.config.IndexedProperty,
	 *      de.hybris.platform.core.model.ItemModel, java.lang.String)
	 */
	@Override
	protected Object getAttributeValue(final IndexedProperty indexedProperty, final GenericVariantProductModel product,
			final String attributeName) throws FieldValueProviderException
	{
		final ProductModel baseProduct = product.getBaseProduct();
		if (baseProduct != null && StringUtils.isNotEmpty(attributeName))
		{
			return getModelAttributeValue(baseProduct, attributeName);
		}
		return null;

	}


	/**
	 * This method fetches model attribute value for a given product and attribute. It checks whether the given composed
	 * type has that attribute first and then fetches the corresponding value.
	 *
	 * @param product
	 * @param attributeName
	 * @return value
	 */
	protected Object getModelAttributeValue(final ProductModel product, final String attributeName)
	{
		Object value = null;
		final ComposedTypeModel composedType = getTypeService().getComposedTypeForClass(product.getClass());
		if (getTypeService().hasAttribute(composedType, attributeName))
		{
			value = getModelService().getAttributeValue(product, attributeName);
		}
		return value;
	}


}
