package com.simon.core.provider.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.impl.DefaultCategorySource;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.HashSet;
import java.util.Set;


/**
 * to fetch the base product from generic variant product as categories are aligned to base product
 */
public class ExtDefaultCategorySource extends DefaultCategorySource
{
	/**
	 * to fetch base product assigned to product model - generic variant product model
	 */
	@Override
	protected Set<ProductModel> getProducts(final Object model)
	{
		final Set<ProductModel> baseProducts = new HashSet<>();
		if (model instanceof GenericVariantProductModel)
		{
			baseProducts.add(((GenericVariantProductModel) model).getBaseProduct());
		}
		return baseProducts;
	}
}
