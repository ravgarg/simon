package com.simon.core.promotions;

import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleActionContext;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.RAOAction;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.impl.RuleOrderPercentageDiscountRAOAction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import javax.annotation.Resource;

import com.simon.core.promotions.service.RetailerRuleEngineCalculationService;
import com.simon.promotion.rao.RetailerRAO;


/**
 * On successful evaluation of a condition it's respective action is called. This RAOAction is called when Retailer
 * Percentage discount is applicable. This has been extended from {@link RuleOrderPercentageDiscountRAOAction} in order
 * to add retailer specific information to rule action rao. performAction of
 * {@link RuleOrderPercentageDiscountRAOAction} is called when condition is evaluated as true. performAction calls
 * setRAOMetaData which has been overridden here in order to add retailer information
 */
public class RuleRetailerPercentageDiscountRAOAction extends RuleOrderPercentageDiscountRAOAction implements RAOAction
{
	@Resource
	private RetailerRuleEngineCalculationService retailerRuleEngineCalculationService;

	/**
	 * Fetches the retailer for which the condition has been evaluated from rule context and Sets its information in
	 * {@link DiscoutRAO}}
	 *
	 * @param context
	 *           the context
	 * @param raos
	 *           the raos
	 */
	@Override
	public void setRAOMetaData(final RuleActionContext context, final AbstractRuleActionRAO... raos)
	{
		super.setRAOMetaData(context, raos);
		if (Objects.nonNull(raos))
		{
			Stream.of(raos).filter(Objects::nonNull).forEach(rao -> {
				setRetailer(context, rao);
			});
		}
	}

	/**
	 * This method sets the retailerRao.And on this basis percentage calulation is done for particular sub bag.
	 * {@link AbstractRuleActionRAO}}
	 *
	 * @param context
	 *           the context
	 * @param ruleRao
	 *           the ruleRao
	 */
	private void setRetailer(final RuleActionContext context, final AbstractRuleActionRAO ruleRao)
	{
		RetailerRAO retailerRao = null;
		if (context instanceof DefaultDroolsRuleActionContext && ruleRao instanceof DiscountRAO)
		{
			final DefaultDroolsRuleActionContext droolContext = (DefaultDroolsRuleActionContext) context;
			final Object droolVariable = droolContext.getVariables().get(RetailerRAO.class.getName());
			if (null != droolVariable)
			{
				final List<RetailerRAO> evaluatedRetailerList = new ArrayList<>(
						Collections.unmodifiableSet((Set<RetailerRAO>) droolVariable));
				retailerRao = evaluatedRetailerList.get(0);
				((DiscountRAO) ruleRao).setRetailer(retailerRao);
			}
		}

	}

	/**
	 * This method gives the retailerRao.And on this basis percentage calulation is done for particular sub bag.
	 *
	 * @param context
	 *           the context
	 */
	protected RetailerRAO getRetailer(final RuleActionContext context)
	{
		RetailerRAO retailerRao = null;
		if (context instanceof DefaultDroolsRuleActionContext)
		{
			final DefaultDroolsRuleActionContext droolContext = (DefaultDroolsRuleActionContext) context;
			final Object droolVariable = droolContext.getVariables().get(RetailerRAO.class.getName());
			if (null != droolVariable)
			{
				final List<RetailerRAO> evaluatedRetailerList = new ArrayList<>(
						Collections.unmodifiableSet((Set<RetailerRAO>) droolVariable));
				retailerRao = evaluatedRetailerList.get(0);
			}
		}
		return retailerRao;
	}

	/**
	 * This method creates the {@link DiscountRAO} that will be used by the respective strategy to create
	 * {@link PromotionResult}, update the Rule engine context with the new calculated total of {@link RetailerRAO} and
	 * {@link CartRAO}
	 *
	 *
	 * @param context
	 *           the context
	 * @param percentageRate
	 *           the percentage rate.
	 */
	@Override
	protected void performAction(final RuleActionContext context, final BigDecimal percentageRate)
	{
		final CartRAO cartRao = context.getCartRao();
		final RetailerRAO retailerRAO = getRetailer(context);
		final BigDecimal totalBeforePromotion = retailerRAO.getTotal();
		final DiscountRAO discount = retailerRuleEngineCalculationService.addOrderLevelDiscount(retailerRAO, false, percentageRate);// 46
		final BigDecimal totalAfterPromotion = retailerRAO.getTotal();
		if (null == totalBeforePromotion || null == totalAfterPromotion)
		{
			return;
		}
		this.getRuleEngineCalculationService().addOrderLevelDiscount(cartRao, true,
				totalBeforePromotion.subtract(totalAfterPromotion));
		final RuleEngineResultRAO result = context.getRuleEngineResultRao();
		result.getActions().add(discount);
		this.setRAOMetaData(context, discount);
		context.updateFacts(new Object[]
		{ cartRao, result, retailerRAO });
		context.insertFacts(new Object[]
		{ discount });
	}


}
