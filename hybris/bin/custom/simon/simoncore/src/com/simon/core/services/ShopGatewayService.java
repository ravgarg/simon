package com.simon.core.services;

import com.simon.core.model.ShopGatewayModel;


/**
 * The Interface ShopGatewayService fetches the configured target system
 */
public interface ShopGatewayService
{

	/**
	 * Gets the Shop Gateway Using shop gateway code.
	 *
	 * @param shopGatewayCode
	 *           The Shop gateway code
	 *
	 * @return the Shop gateway Model
	 */
	ShopGatewayModel getShopGatewayForCode(String shopGatewayCode);
}
