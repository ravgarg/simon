package com.simon.core.services.price.impl;

import de.hybris.platform.product.impl.DefaultPriceService;

import java.util.EnumMap;
import java.util.Map;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.services.price.ExtPriceService;


/**
 * This class provides additional information related to price. Like which price need to be stroked off and what is the
 * percent saving.Here lp is the list price and sp is the sale price.
 */
public class ExtDefaultPriceService extends DefaultPriceService implements ExtPriceService
{
	private static final long serialVersionUID = 1;


	@Override
	public Map<DetailedPriceInfo, Object> getDetailedPriceInformation(final double msrp, final double lp, final double sp)
	{
		final Map<DetailedPriceInfo, Object> priceDetail = new EnumMap<>(DetailedPriceInfo.class);

		final boolean validMsrp = Double.compare(msrp, SimonCoreConstants.ZERO_DOUBLE) > SimonCoreConstants.ZERO_INT;
		final boolean validSp = Double.compare(sp, SimonCoreConstants.ZERO_DOUBLE) > SimonCoreConstants.ZERO_INT;
		double strikeThroughPrice = lp;
		double finalSellingPrice = lp;
		priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, false);
		priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, false);
		if (validMsrp)
		{
			priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, true);
			priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, false);
			strikeThroughPrice = msrp;
			if (validSp)
			{
				finalSellingPrice = sp;
			}

		}
		else if (validSp)
		{
			strikeThroughPrice = lp;
			finalSellingPrice = sp;
			priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, false);
			priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, true);
		}
		final int percent = (int) Math
				.round((((strikeThroughPrice - finalSellingPrice) / strikeThroughPrice) * SimonCoreConstants.HUNDRED_INT));
		if (percent > 0)
		{
			priceDetail.put(DetailedPriceInfo.PERCENTAGE_SAVING, percent);
		}
		return priceDetail;
	}

}
