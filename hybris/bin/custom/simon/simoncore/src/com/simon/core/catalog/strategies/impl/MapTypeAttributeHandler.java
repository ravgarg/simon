package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;
import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCoreAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * MapTypeAttributeHandler is a generic attribute handler for handling <code>MapType</code> attribute values imported
 * from mirakl.<br>
 * MapType attributes are sent in the form of <code>key1:value1|key2:value2</code>. This string is split and converted
 * into map and stored in the attribute specified by {@link MiraklCoreAttributeModel#CODE}
 */
public class MapTypeAttributeHandler extends DefaultCoreAttributeHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(MapTypeAttributeHandler.class);

	/**
	 * Splits the attribute text value {@link AttributeValueData#getValue()} by delimiters specified by
	 * {@link MiraklCoreAttributeModel#TYPEPARAMETER}. <br>
	 * First element of delimiter string is used as a main separator and Second element is used as key-value separator. For
	 * instance if delimiter is <code>|:</code> then <code>|</code> will be used as main separator and <code>:</code> will
	 * be used as key-value separator. Using these delimiters will perfectly convert a string instance of
	 * <code>a:b|c:d</code> into a map.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = attribute.getCoreAttribute();
		final String attributeQualifier = coreAttribute.getCode();
		final String delimiters = coreAttribute.getTypeParameter();
		final String rawText = attribute.getValue();

		if (StringUtils.isNotEmpty(rawText) && StringUtils.isNotEmpty(delimiters) && delimiters.length() == 2)
		{
			final ProductModel product = determineOwner(coreAttribute, data, context);

			final char[] delims = delimiters.toCharArray();

			final Map<String, String> additionalAttributesMap = Splitter.on(delims[0]).withKeyValueSeparator(delims[1])
					.split(rawText);
			modelService.setAttributeValue(product, attributeQualifier, additionalAttributesMap);
			LOGGER.debug("Updating Map : {} On Attribute : {} On Product : {}", rawText, attributeQualifier, product.getCode());
		}
	}

}
