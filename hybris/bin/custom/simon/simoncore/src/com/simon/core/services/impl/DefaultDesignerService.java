package com.simon.core.services.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.dao.DesignerDao;
import com.simon.core.model.DesignerModel;
import com.simon.core.services.DesignerService;


/**
 * DefaultDesignerService, default implementation of {@link DesignerService}.
 */
public class DefaultDesignerService implements DesignerService
{

	/** The designer dao. */
	@Autowired
	private DesignerDao designerDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DesignerModel getDesignerForCode(final String code)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", code);
		return designerDao.findDesignerByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DesignerModel> getListOfDesignerForCodes(final List<String> objects)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", objects);
		return designerDao.findListOfDesignerForCodes(objects);
	}
}
