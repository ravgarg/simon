package com.simon.core.api.mirakl.dto;

import java.util.Map;

import com.mirakl.client.core.internal.MiraklApiEndpoint;
import com.mirakl.client.request.AbstractMiraklApiRequest;

/**
 * Class responsible for retrieving values for API endPoint
 * {@link SimonMiraklTrackingOrderRequest}, request template that need to be
 * passed with in request object.
 *
 */
public class SimonMiraklTrackingOrderRequest extends AbstractMiraklApiRequest {

		private SimonMiraklTrackingInfoBean miraklTrackingInfoBean;

		private String orderId;

		public SimonMiraklTrackingOrderRequest(SimonMiraklTrackingInfoBean infoBean, String orderId) {
				setMiraklTrackingInfoBean(infoBean);
		}

		/**
		 * Method used to set the parameters with in requestTemplate.
		 */
		public Map<String, String> getRequestTemplates() {
				Map<String, String> templates = super.getRequestTemplates();
				templates.put("order", orderId);
				return templates;
		}

		/**
		 * @return the miraklTrakingInfoBean
		 */
		public SimonMiraklTrackingInfoBean getMiraklTrackingInfoBean() {
				return miraklTrackingInfoBean;
		}

		/**
		 * @param miraklTrakingInfoBean
		 *            the miraklTrakingInfoBean to set
		 */
		public void setMiraklTrackingInfoBean(SimonMiraklTrackingInfoBean miraklTrackingInfoBean) {
				this.miraklTrackingInfoBean = miraklTrackingInfoBean;
		}

		/**
		 * Method used to retrieve OR23 API EndPoint using
		 * {@link SimonMiraklFrontOperatorApiEndpoint}
		 */
		public MiraklApiEndpoint getEndpoint() {
				return SimonMiraklFrontOperatorApiEndpoint.OR23;
		}

		/**
		 * @return the orderId
		 */
		public String getOrderId() {
				return orderId;
		}

		/**
		 * @param orderId
		 *            the orderId to set
		 */
		public void setOrderId(String orderId) {
				checkRequiredArgument(orderId, "orderId");
				this.orderId = orderId;
		}

		public boolean equals(Object o) {
				if (this == o) {
						return true;
				}
				if ((o == null) || (getClass() != o.getClass())) {
						return false;
				}

				SimonMiraklTrackingOrderRequest that = (SimonMiraklTrackingOrderRequest) o;
				
				boolean isInstance = miraklTrackingInfoBean != null ? that.equals(miraklTrackingInfoBean) : false;
				return miraklTrackingInfoBean == null ? true : isInstance;
		}

		public int hashCode() {
				return miraklTrackingInfoBean != null ? miraklTrackingInfoBean.hashCode() : 0;
		}
}
