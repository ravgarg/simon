package com.simon.core.strategies.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.util.PriceValue;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;

import com.mirakl.hybris.core.order.strategies.impl.MiraklFindDeliveryCostStrategy;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;


/**
 * Implementation of {@link MiraklFindDeliveryCostStrategy} that resolves {@link PriceValue}s from the order's attached
 * {@link PriceValue}s. Strategy used to find out Delivery Cost from two Tap by manipulating TwoTapCar
 */
public class SimonFindDeliveryCostStrategy extends MiraklFindDeliveryCostStrategy
{
	private static final long serialVersionUID = 1;

	/** The Constant CONST_USD used to store currency String. */
	private static final String CONST_USD = "USD";

	/**
	 * Method used to get the deliverycost of the items addeded in cart. This method extract TwoTap Cart from
	 * {@link AbstractOrderModel} and then evaluate the estimatePrices to retrieve delivery cost.
	 *
	 * @param order
	 *           of type {@link AbstractOrderModel} to retrieve TwoTap cart and estimates
	 * @return {@link PriceValue} object is used to return delivery cost.
	 */
	@Override
	public PriceValue getDeliveryCost(final AbstractOrderModel order)
	{
		validateParameterNotNull(order, "abstractOrder model cannot be null");
		if (order.getSubtotal() == null || order.getSubtotal().doubleValue() <= 0.00d)
		{
			return getDeliveryCostFromSuper(order);
		}
		double totalRetailerCost = 0.0;
		if (CollectionUtils.isNotEmpty(order.getRetailersDeliveryModes().values()))
		{
			final Map<String, DeliveryModeModel> retailerDeliveryMode = order.getRetailersDeliveryModes();
			retailerDeliveryMode.keySet().stream()
					.forEach(reatiler -> setDeliveryCost(reatiler, retailerDeliveryMode.get(reatiler), order));

			final Map<String, Double> retailerDeliveryCost = order.getRetailersDeliveryCost();
			totalRetailerCost = retailerDeliveryCost.values().stream().mapToDouble(retailerCost -> retailerCost).sum();
		}
		return new PriceValue(order.getCurrency() == null ? CONST_USD : order.getCurrency().getIsocode(), totalRetailerCost, true);
	}

	private void setDeliveryCost(final String reatiler, final DeliveryModeModel deliveryModeModel, final AbstractOrderModel order)
	{
		final String selDeliveryMode = deliveryModeModel.getCode();
		final AdditionalCartInfoModel ttCart = order.getAdditionalCartInfo();
		final double shippingCost = populateShippingCost(ttCart, reatiler, selDeliveryMode);
		final Map<String, Double> retailerDeliveryCost = new HashMap<>();
		retailerDeliveryCost.putAll(order.getRetailersDeliveryCost());
		retailerDeliveryCost.put(reatiler, shippingCost);
		order.setRetailersDeliveryCost(retailerDeliveryCost);
	}

	/**
	 * Gets the delivery cost from super.To achieve this, it Calls the OOTB logic provided in superclass
	 * {@link MiraklFindDeliveryCostStrategy} to fetch the delivery cost for the given order.
	 *
	 * @param order
	 *           {@link AbstractOrderModel} used to calculate the delivery cost.
	 * @return the delivery cost {@link PriceValue} from superclass {@link MiraklFindDeliveryCostStrategy}
	 */
	protected PriceValue getDeliveryCostFromSuper(final AbstractOrderModel order)
	{
		return super.getDeliveryCost(order);
	}

	private double populateShippingCost(final AdditionalCartInfoModel ttCart, final String retailerId, final String deliveryCode)
	{
		double shippingCost = 0.0;
		if (null != ttCart && null != ttCart.getRetailersInfo())
		{
			final Optional<RetailersInfoModel> retailerOption = ttCart.getRetailersInfo().stream()
					.filter(retailerInfo -> retailerInfo.getRetailerId().equalsIgnoreCase(retailerId)).findFirst();
			if (retailerOption.isPresent())
			{
				final Optional<ShippingDetailsModel> shippingDetails = retailerOption.get().getShippingDetails().stream()
						.filter(shipping -> shipping.getCode().equalsIgnoreCase(deliveryCode)).findFirst();
				if (shippingDetails.isPresent())
				{
					shippingCost = null != shippingDetails.get().getShippingEstimate() ? shippingDetails.get().getShippingEstimate()
							: 0.0;
				}
			}
		}
		return shippingCost;
	}



}
