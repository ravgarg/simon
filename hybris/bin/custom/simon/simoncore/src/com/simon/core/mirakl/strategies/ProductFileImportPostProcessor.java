package com.simon.core.mirakl.strategies;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.exceptions.ProductFileImportPostProcessingException;
import com.simon.core.mirakl.strategies.impl.ExtPostProcessProductFileImportStrategy;


/**
 * A ProductFileImportPostProcessor adds post processing logic after Product Import from mirakl is completed.
 * {@link ExtPostProcessProductFileImportStrategy} automatically invokes all implementations of this interface
 * sequentially defined by <code>@Order</code> annotation. <br>
 * If any exception occurs during post processing {@link ProductFileImportPostProcessingException} will be thrown
 */
public interface ProductFileImportPostProcessor
{

	/**
	 * Post process operations on imported products
	 *
	 * @param context
	 *           the context
	 * @param importId
	 *           the import id
	 * @throws ProductFileImportPostProcessingException
	 *            the product file import post processing exception
	 */
	void postProcess(ProductImportFileContextData context, String importId) throws ProductFileImportPostProcessingException;
}
