package com.simon.core.promotions;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DeliveryModeRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.simon.core.services.RetailerService;
import com.simon.promotion.rao.RetailerRAO;


/**
 * This class adds list of {@link RetailerRAO} to {@link CartRAO} from {@link CartModel}
 */
public class RetailerRaoPopulator implements Populator<CartModel, CartRAO>
{

	@Resource
	private RetailerService retailerService;
	@Resource
	private Converter<AbstractOrderModel, RetailerRAO> retailerRaoEntryConverter;

	/**
	 * Populate method gets the retailer sub bag total using {@link RetailerService} and populates it in
	 * {@link RetailerRAO} and finally its list in {@link CartRAO}. It also add entries to the {@link RetailerRAO} using
	 * retailerRaoConverter.
	 *
	 * @param source
	 *           the cart model
	 * @param target
	 *           the cart rao
	 */
	@Override
	public void populate(final CartModel source, final CartRAO target)
	{
		final List<RetailerRAO> retailers = new ArrayList<>();
		final Map<String, Double> retailerSubtotal = retailerService.getRetailerSubBagForCart(source);
		retailerSubtotal.keySet().stream().forEach(retailer -> retailers.add(populateRetailer(retailer, retailerSubtotal, source)));
		target.setRetailers(retailers);
	}

	private RetailerRAO populateRetailer(final String retailer, final Map<String, Double> retailerSubtotal, final CartModel source)
	{
		final RetailerRAO retailerRao = new RetailerRAO();
		final DeliveryModeRAO deliveryModeRAO = new DeliveryModeRAO();
		retailerRao.setRetailerId(retailer);

		deliveryModeRAO.setCode(null != source.getRetailersDeliveryModes().get(retailer)
				? source.getRetailersDeliveryModes().get(retailer).getCode() : StringUtils.EMPTY);

		retailerRao.setDeliveryMode(deliveryModeRAO);
		retailerRaoEntryConverter.convert(source, retailerRao);
		retailerRao.setTotal(BigDecimal.valueOf(retailerSubtotal.get(retailer)));
		retailerRao.setSubTotal(BigDecimal.valueOf(retailerSubtotal.get(retailer)));
		return retailerRao;
	}

}
