package com.simon.core.common.comparator;

import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.services.SizeService;



/**
 * This comparator is used to sort Normalized size based on the sequence number associated with it.
 */
public class NormalizedSizeSortComparator implements Comparator<FacetValue>
{
	@Resource
	private SizeService sizeService;

	/**
	 * This method fetches the size sequences from db and sorts the sizes based on sequence number.
	 *
	 * @param value1
	 *           {@link FacetValue}
	 * @param value2
	 *           {@link FacetValue}
	 * @return int
	 */
	public int compare(final FacetValue value1, final FacetValue value2)
	{
		final List<ProductSizeSequenceModel> allSizeSequences = sizeService.getAllSequencesForSizes();
		Integer seq1 = Integer.MAX_VALUE;
		Integer seq2 = Integer.MAX_VALUE;
		for (final ProductSizeSequenceModel sizeSeq : allSizeSequences)
		{
			if (null != sizeSeq.getSize())
			{
				if (sizeSeq.getSize().equalsIgnoreCase(value1.getName()))
				{
					seq1 = sizeSeq.getSequenceNumber();
				}
				else if (sizeSeq.getSize().equalsIgnoreCase(value2.getName()))
				{
					seq2 = sizeSeq.getSequenceNumber();
				}
			}
		}
		return seq1.compareTo(seq2);
	}
}
