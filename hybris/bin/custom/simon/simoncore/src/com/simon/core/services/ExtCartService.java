package com.simon.core.services;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;


/**
 * This Service Interface is used to retrieve cart model
 */
public interface ExtCartService extends CartService
{

	/**
	 * Method call to get Cart by cart ID
	 *
	 * @param cartId
	 * @return CartModel
	 */
	CartModel getCartByCartId(String cartId);

	/**
	 * This method will check for all the failed products returned from the retailer and will zero down the stock
	 * availability for those products
	 *
	 * @param abstractOrderModel
	 */
	void modifyStockForFailedProducts(AbstractOrderModel abstractOrderModel);

	/**
	 * This method will check for all the removed products returned from the retailer and will zero down the stock
	 * availability for those products
	 *
	 * @param abstractOrderModel
	 */
	void modifyStockForRemovedProducts(AbstractOrderModel abstractOrderModel);

	/**
	 * This method will zero down the stock availability for those products
	 *
	 * @param abstractOrderModel
	 * @param productCode
	 * @throws ModelSavingException
	 */
	void updateStockForProductCode(final AbstractOrderModel abstractOrderModel, String productCode, boolean isRemoved)
			throws ModelSavingException;

	int getCartCount();

	/**
	 * @param cart
	 * @return
	 */
	boolean hasReceivedUpdatedCreateCallback(CartModel cart);
}
