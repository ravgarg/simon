package com.simon.core.sitemap.impl;

import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ContentPageModelSiteMapGenerator;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.util.FlexibleSearchUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Extension of ContentPageModelSiteMapGenerator removed cmslinkcomponent
 */
public class ExtContentPageModelSiteMapGenerator extends ContentPageModelSiteMapGenerator
{
	/**
	 * This method used to get the content page results.
	 *
	 * @param siteModel
	 * @return List<ContentPageModel>
	 */
	@Override
	protected List<ContentPageModel> getDataInternal(final CMSSiteModel siteModel)
	{
		final Map<String, Object> params = new HashMap<String, Object>();

		final StringBuilder queryBuilder = new StringBuilder(60);
		queryBuilder.append("SELECT {cp." + ContentPageModel.PK + "} FROM {" + ContentPageModel._TYPECODE + " AS cp } WHERE ");
		queryBuilder.append(buildOracleCompatibleStatement(params));

		return doSearch(queryBuilder.toString(), params, ContentPageModel.class);

	}

	protected String buildOracleCompatibleStatement(final Map<String, Object> params)
	{
		return FlexibleSearchUtils.buildOracleCompatibleCollectionStatement(
				"{cp." + ContentPageModel.CATALOGVERSION + "} in (?catalogVersions)", "catalogVersions", "OR",
				getCatalogVersionService().getSessionCatalogVersions(), params);
	}
}
