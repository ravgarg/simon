package com.simon.core.job;



import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.mirakl.jobs.FolderCleanupJob;
import com.simon.core.model.DeleteOrphanMediaContainerJobModel;
import com.simon.core.services.ExtMediaContainerService;


/**
 * This job get all mediaContainers with version greater then zero and delete all those media.
 */
public class DeleteOrphanMediaContainerJob extends AbstractJobPerformable<DeleteOrphanMediaContainerJobModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(FolderCleanupJob.class);
	@Resource
	private ExtMediaContainerService extMediaContainerService;

	@Override
	public PerformResult perform(final DeleteOrphanMediaContainerJobModel deleteOrphanMediaContainerJobModel)
	{
		LOG.info("Starting delete orphan mediaContainer job");

		final Date cleanupAgeDate = getCleanupAgeDate(deleteOrphanMediaContainerJobModel);
		final List<MediaContainerModel> listOfMediaContainers = extMediaContainerService
				.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate);
		modelService.removeAll(listOfMediaContainers);
		LOG.info("Finished delete orphan mediaContainer job total no of container deleted [{}] ", listOfMediaContainers.size());
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

	/**
	 * This method calculate date on basis of cleanupAgeIndays attribute of deleteOrphanMediaContainerJobModel, it will
	 * subtract no of x days from current day.
	 *
	 * @param deleteOrphanMediaContainerJobModel
	 * @return
	 */
	public Date getCleanupAgeDate(final DeleteOrphanMediaContainerJobModel deleteOrphanMediaContainerJobModel)
	{
		final Date currentDate = new Date();
		return new Date(currentDate.getTime() - (deleteOrphanMediaContainerJobModel.getCleanupAgeInDays() * 24 * 3600 * 1000));
	}

}
