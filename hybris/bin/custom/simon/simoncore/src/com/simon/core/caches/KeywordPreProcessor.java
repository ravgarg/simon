package com.simon.core.caches;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.KeywordType;
import com.simon.core.services.ExtKeywordService;



/**
 * KeywordPreProcessor creates new {@link KeywordModel} if present in product feed found during pre-processing.
 */
@Order(value = 1)
public class KeywordPreProcessor extends AbstractPreProcessor<KeywordModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(KeywordPreProcessor.class);

	@Autowired
	private ExtKeywordService keywordService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private CommonI18NService commonI18NService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createKey(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		final KeywordType keywordType = "seoKeywords".equalsIgnoreCase(attribute) ? KeywordType.SEO : KeywordType.MARKETING;
		return keywordType.getCode() + "-" + rawValue;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KeywordModel createValue(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		final KeywordType keywordType = "seoKeywords".equalsIgnoreCase(attribute) ? KeywordType.SEO : KeywordType.MARKETING;
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);
		KeywordModel keyword;
		final KeywordModel existingKeyword = keywordService.getKeyword(catalogVersion, rawValue, keywordType);
		if (existingKeyword != null)
		{
			LOGGER.debug("Keyword : {} Type : {} found", rawValue, keywordType);
			keyword = existingKeyword;
		}
		else
		{
			LOGGER.info("Creating New Keyword : {} Type : {}", rawValue, keywordType);
			keyword = modelService.create(KeywordModel.class);
			keyword.setCatalogVersion(catalogVersion);
			keyword.setKeywordType(keywordType);
			keyword.setKeyword(rawValue);
			keyword.setLanguage(commonI18NService.getCurrentLanguage());
			modelService.save(keyword);
		}
		return keyword;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ValueType getValueType()
	{
		return ValueType.MULTIPLE;
	}
}
