package com.simon.core.services.impl;

import de.hybris.platform.catalog.impl.DefaultKeywordService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.enums.KeywordType;
import com.simon.core.services.ExtKeywordService;


/**
 * ExtKeywordServiceImpl extends {@link DefaultKeywordService} and provides db based implementation of new operations
 * defined in {@link ExtKeywordService}
 */
public class ExtKeywordServiceImpl extends DefaultKeywordService implements ExtKeywordService
{

	@Resource
	private DefaultGenericDao<KeywordModel> keywordModelGenericDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public KeywordModel getKeyword(final CatalogVersionModel catalogVersion, final String keywordValue,
			final KeywordType keywordType)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put("catalogVersion", catalogVersion);
		params.put("keyword", keywordValue);
		params.put("keywordType", keywordType);
		final List<KeywordModel> resList = keywordModelGenericDao.find(params);
		return CollectionUtils.isEmpty(resList) ? null : resList.get(0);
	}
}
