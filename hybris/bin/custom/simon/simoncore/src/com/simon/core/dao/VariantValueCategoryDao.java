package com.simon.core.dao;

import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;


/**
 * Dao for {@link VariantValueCategoryModel} contains abstract method for operations on
 * {@link VariantValueCategoryModel}.
 */
public interface VariantValueCategoryDao
{

	/**
	 * Find top sequence variant value category. Returns {@link VariantValueCategoryModel} having top sequence for given
	 * <code>variantCategory</code>
	 *
	 * @param variantCategory
	 *           the <code>variantCategory</code>
	 * @return the variant value category model, <code>null</code> if no variant value category exists
	 * @throws IllegalArgumentException
	 *            if the given <code>variantCategory</code> is <code>null</code>
	 */
	public VariantValueCategoryModel findTopSequenceVariantValueCategory(final VariantCategoryModel variantCategory);

}
