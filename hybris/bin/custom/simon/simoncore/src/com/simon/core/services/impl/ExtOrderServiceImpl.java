package com.simon.core.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.impl.DefaultOrderService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;

import java.io.FileWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;
import com.opencsv.CSVWriter;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.dao.impl.ExtOrderDaoImpl;
import com.simon.core.dto.OrderExportData;
import com.simon.core.enums.OrderExportType;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.services.ExtOrderService;
import com.simon.core.services.RetailerService;
import com.simon.core.util.TransferFileToSftpUtils;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;


/**
 * This Service class is used to retrieve order model
 */
public class ExtOrderServiceImpl extends DefaultOrderService implements ExtOrderService
{

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(ExtOrderServiceImpl.class);

	@Resource
	private ExtOrderDaoImpl extOrderDao;
	@Resource
	private ConfigurationService configurationService;
	@Resource
	private RetailerService retailerService;
	@Resource
	private ShopDao shopDao;

	protected Converter<OrderEntryModel, OrderExportData> orderForBotAndRetailerFeedStatusConverter;

	/**
	 * @description This method returns the versioned order
	 * @method getVersionedOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	@Override
	public OrderModel getVersionedOrderByOrderId(final String orderId)
	{
		return extOrderDao.getVersionedOrderByOrderId(orderId);
	}

	/**
	 * @description Method call to get Order by orderId. This is a callback method used to save additional cart model in
	 *              the found order model.
	 * @method getOrderByOrderId
	 * @param orderId
	 * @return OrderModel
	 */
	@Override
	public OrderModel getOrderByOrderId(final String orderId)
	{
		return extOrderDao.getOrderByOrderId(orderId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getRetailerList(final OrderModel orderModel)
	{
		final Set<String> retailers = new HashSet<>();
		for (final AbstractOrderEntryModel orderEntry : orderModel.getEntries())
		{
			try
			{
				final ShopModel shop = shopDao.findShopById(orderEntry.getShopId());
				retailers.add(null != shop ? shop.getName() : "");
			}
			catch (final AmbiguousIdentifierException ambiguousIdentifierException)
			{
				LOG.error("Multiple retailers found for the Shop Id: {}. Moving ahead with rest of the retailers.",
						orderEntry.getShopId(), ambiguousIdentifierException);
			}
			catch (final IllegalArgumentException illegalArgumentException)
			{
				LOG.error("Shop Id not found for Order Entry: {} with Product Code {}. Moving ahead with rest of the entries.",
						orderEntry.getPk(), orderEntry.getProduct().getCode(), illegalArgumentException);
			}
		}
		return retailers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkAcceptOrRejectTheOrder(final OrderModel orderModel)
	{
		validateParameterNotNull(orderModel, "Order must not be null");
		validateParameterNotNull(orderModel.getAdditionalCartInfo(),
				"Additional Cart Info must not be null for order" + orderModel.getCode());

		boolean accepted = true;
		final boolean isOrderAcceptanceAbsolute = configurationService.getConfiguration()
				.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE);
		final AdditionalCartInfoModel additionalCart = orderModel.getAdditionalCartInfo();
		for (final RetailersInfoModel retailer : additionalCart.getRetailersInfo())
		{
			final double commerceTotal = retailerService.getRetailerSubtotal(orderModel, retailer.getRetailerId());
			final double retailerTotalFromBOT = retailer.getFinalPrice()
					- (retailer.getTotalShippingPrice() + retailer.getTotalSalesTax()) + retailer.getCouponValue();
			final double difference = retailerTotalFromBOT - commerceTotal;
			final double compareAgainst = getCompareAgainstValue(isOrderAcceptanceAbsolute, commerceTotal);
			if (difference >= compareAgainst)
			{
				accepted = false;
				LOG.info("order has been rejected for order id as there was a order total differ by {} for retailer {}", difference,
						retailer.getRetailerId());
				break;
			}
		}
		return accepted;
	}

	private double getCompareAgainstValue(final boolean isOrderAcceptanceAbsolute, final double commerceTotal)
	{
		double compareAgainst = configurationService.getConfiguration().getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE);
		if (!isOrderAcceptanceAbsolute)
		{
			compareAgainst = (compareAgainst / 100) * commerceTotal;
		}
		return compareAgainst;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOrderIdPerRetailer(final OrderModel orderModel, final OrderConfirmCallBackDTO orderConfirmCallBackDTO)
	{
		final Map<String, String> retailerOrderNumbers = new HashMap<>();
		for (final OrderConfirmRetailersInfoCallBackDTO retailerInfo : orderConfirmCallBackDTO.getRetailers())
		{
			if (StringUtils.isNotEmpty(retailerInfo.getOrderId()))
			{
				retailerOrderNumbers.put(retailerInfo.getRetailerId(), retailerInfo.getOrderId());
			}
		}
		orderModel.setRetailersOrderNumbers(retailerOrderNumbers);
		getModelService().save(orderModel);
	}

	/**
	 *
	 * @return {@link List} of {@link OrderModel} - matched orders
	 */
	@Override
	public List<OrderModel> findOrdersToExport(final int day, final String startHours, final String endHours)
	{
		return extOrderDao.findOrdersToExport(day, startHours, endHours);
	}

	/**
	 *
	 * @param orderList
	 * @return {@link List} of {@link OrderExportData} - matched orders
	 */
	public boolean createOrderExportFiles(final List<OrderModel> orderList)
	{
		boolean result = false;
		final Map<String, List<OrderExportData>> retailerOrderMap = new LinkedHashMap<>();
		final Map<String, List<OrderExportData>> botOrderMap = new LinkedHashMap<>();

		for (final OrderModel order : orderList)
		{
			if (CollectionUtils.isNotEmpty(order.getEntries()))
			{
				LOG.info("Order Export For Order Status:: Order {} is being processed for order status export", order.getCode());

				for (final AbstractOrderEntryModel orderEntry : order.getEntries())
				{
					final OrderExportData orderExportData = orderExportPopulator(order, orderEntry);

					if (null != orderExportData && null != orderExportData.getBotExport()
							&& orderExportData.getBotExport().equals(OrderExportType.BOT))
					{
						addBotOrderToMap(
								orderExportData.getRetailerPrefix() + SimonCoreConstants.HYPHEN + orderExportData.getSellerId(),
								orderExportData, botOrderMap);
					}
					else if (null != orderExportData && null != orderExportData.getBotExport()
							&& orderExportData.getBotExport().equals(OrderExportType.RETAILER))
					{
						addRetailerOrderToMap(
								orderExportData.getRetailerPrefix() + SimonCoreConstants.HYPHEN + orderExportData.getSellerId(),
								orderExportData, retailerOrderMap);
					}

				}

			}
		}
		if (MapUtils.isNotEmpty(retailerOrderMap))
		{
			writeOrderExportStatusFeedFile(retailerOrderMap);
			result = true;
		}
		if (MapUtils.isNotEmpty(botOrderMap))
		{
			writeOrderExportBotFile(botOrderMap);
			result = true;
		}
		return result;
	}

	private OrderExportData orderExportPopulator(final OrderModel order, final AbstractOrderEntryModel orderEntry)
	{
		OrderExportData orderExportData = null;
		try
		{

			if (CollectionUtils.isNotEmpty(orderEntry.getConsignmentEntries())
					&& StringUtils.isNotEmpty(orderEntry.getConsignmentEntries().iterator().next().getMiraklOrderLineId()))
			{
				final OrderEntryModel orderEntryModel = (OrderEntryModel) orderEntry;
				orderExportData = orderForBotAndRetailerFeedStatusConverter.convert(orderEntryModel);
				populateOrderAttribute(order, orderEntryModel, orderExportData);
				LOG.info(
						"Order Export For Order Status:: Successfully Populated order entry {}  for  Order {} is being processed for order status export",
						orderExportData.getSpoOrderLineId(), order.getCode());
			}
		}
		catch (final Exception ex)
		{
			LOG.error("Order Export For Order Status:: Order Export populator error occured at line item for Order {}",
					order.getCode(), ex);
		}
		return orderExportData;
	}


	private void addRetailerOrderToMap(final String shopId, final OrderExportData orderExportData,
			final Map<String, List<OrderExportData>> retailerOrderMap)
	{
		if (retailerOrderMap.containsKey(shopId))
		{
			retailerOrderMap.get(shopId).add(orderExportData);
		}
		else
		{
			final List<OrderExportData> orderExportList = new ArrayList<>();
			orderExportList.add(orderExportData);
			retailerOrderMap.put(shopId, orderExportList);
		}

	}

	private void addBotOrderToMap(final String shopId, final OrderExportData orderExportData,
			final Map<String, List<OrderExportData>> botOrderMap)
	{
		if (botOrderMap.containsKey(shopId))
		{
			botOrderMap.get(shopId).add(orderExportData);
		}
		else
		{
			final List<OrderExportData> orderExportList = new ArrayList<>();
			orderExportList.add(orderExportData);
			botOrderMap.put(shopId, orderExportList);
		}

	}

	private void populateOrderAttribute(final OrderModel order, final OrderEntryModel orderEntryModel,
			final OrderExportData orderExportData)
	{
		orderExportData.setOrderDate(order.getCreationtime());
		orderExportData.setRetailerOrderId(order.getRetailersOrderNumbers().get(orderEntryModel.getShopId()));
		orderExportData.setCustomerFirstName(order.getDeliveryAddress().getFirstname());
		orderExportData.setCustomerLastName(order.getDeliveryAddress().getLastname());
		orderExportData.setCustomerEmailAddress(order.getDeliveryAddress().getEmail());
		orderExportData.setCustomerBillingZipCode(order.getDeliveryAddress().getPostalcode());
		orderExportData.setCustomerShippingZipCode(order.getPaymentInfo().getBillingAddress().getPostalcode());

	}

	private boolean writeOrderExportStatusFeedFile(final Map<String, List<OrderExportData>> orderList)
	{
		boolean result = false;
		final Set<Entry<String, List<OrderExportData>>> entriesMap = orderList.entrySet();
		for (final Map.Entry<String, List<OrderExportData>> entry : entriesMap)
		{
			createFileForStatus(entry.getValue(), entry.getKey());
			result = true;
			LOG.info("Order Export For Order Status:: Order Export File created successfully for Retailer {}", entry.getKey());
		}
		return result;
	}

	private boolean writeOrderExportBotFile(final Map<String, List<OrderExportData>> orderList)
	{
		boolean result = false;
		final Set<Entry<String, List<OrderExportData>>> entriesMap = orderList.entrySet();
		for (final Map.Entry<String, List<OrderExportData>> entry : entriesMap)
		{
			createFileForBotOrderEngine(entry.getValue(), entry.getKey());
			result = true;
			LOG.info("Order Export For Order Status:: Order Export File created successfully for Bot {}", entry.getKey());
		}
		return result;
	}


	/**
	 * {@inheritDoc}
	 */
	public void createFileForStatus(final List<OrderExportData> orderList, final String retailerPrefix)
	{
		final List<String[]> data = orderStatusFile(orderList);
		if (CollectionUtils.isNotEmpty(data))
		{
			final String fileName = getFileNameForRetailer(
					retailerPrefix.split(SimonCoreConstants.HYPHEN)[SimonCoreConstants.ZERO_INT]);
			try (final FileWriter fileWriter = new FileWriter(fileName, true); // NOSONAR
					final CSVWriter csvWriter = new CSVWriter(fileWriter, ',', Character.MIN_VALUE, Character.MIN_VALUE, "\n");)
			{
				csvWriter.writeAll(data);
			}
			catch (final Exception exception)
			{
				LOG.error("Order Export For Order Status:: Failed to create csv file of orders status", exception);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String[]> orderStatusFile(final List<OrderExportData> orderExportDataList)
	{
		final List<String[]> records = new ArrayList<>();
		final String headerOfOrderStatusFeed = configurationService.getConfiguration()
				.getString(SimonCoreConstants.ORDER_EXPORT_CSV_FILE_HEADER_FOR_RETAILER);
		final String[] header = headerOfOrderStatusFeed.split(SimonCoreConstants.DELIMITER); // NOSONAR
		records.add(header);
		for (final OrderExportData orderExportData : orderExportDataList)
		{
			final String[] stringArrayForCSVValue = new String[header.length];
			stringArrayForCSVValue[SimonCoreConstants.ZERO_INT] = orderExportData.getSellerId();
			stringArrayForCSVValue[SimonCoreConstants.FIRST_INT] = convertDateToString(orderExportData.getOrderDate());
			stringArrayForCSVValue[SimonCoreConstants.TWO_INT] = orderExportData.getRetailerOrderId();
			stringArrayForCSVValue[SimonCoreConstants.THREE_INT] = orderExportData.getSpoOrderId();
			stringArrayForCSVValue[SimonCoreConstants.FOUR_INT] = orderExportData.getSpoOrderLineId();
			stringArrayForCSVValue[SimonCoreConstants.FIVE_INT] = orderExportData.getProductSku();
			records.add(stringArrayForCSVValue);
		}
		return records;
	}

	/**
	 * {@inheritDoc}
	 */
	public void createFileForBotOrderEngine(final List<OrderExportData> orderList, final String retailerPrefix)
	{
		final List<String[]> data = orderFileForBOTEngine(orderList);
		if (CollectionUtils.isNotEmpty(data))
		{
			final String fileName = getfileNameForBOT(retailerPrefix.split(SimonCoreConstants.HYPHEN)[SimonCoreConstants.ZERO_INT]);
			try (final FileWriter fileWriter = new FileWriter(fileName, true); // NOSONAR
					final CSVWriter csvWriter = new CSVWriter(fileWriter, ',', Character.MIN_VALUE, Character.MIN_VALUE, "\n");)
			{
				csvWriter.writeAll(data);
			}
			catch (final Exception exception)
			{
				LOG.error("Order Export For Order Status:: Failed to create csv file for BOT orders to export", exception);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public List<String[]> orderFileForBOTEngine(final List<OrderExportData> orderExportDataList)
	{
		final List<String[]> records = new ArrayList<>();
		final String headerOfOrderStatusFeed = configurationService.getConfiguration()
				.getString(SimonCoreConstants.ORDER_EXPORT_CSV_FILE_HEADER_FOR_BOT);
		final String[] header = headerOfOrderStatusFeed.split(SimonCoreConstants.DELIMITER); // NOSONAR
		records.add(header);
		for (final OrderExportData orderExportData : orderExportDataList)
		{
			final String[] stringArrayForCSVValue = new String[header.length];

			stringArrayForCSVValue[SimonCoreConstants.ZERO_INT] = orderExportData.getSellerId();
			stringArrayForCSVValue[SimonCoreConstants.FIRST_INT] = convertDateToString(orderExportData.getOrderDate());
			stringArrayForCSVValue[SimonCoreConstants.TWO_INT] = orderExportData.getRetailerOrderId();
			stringArrayForCSVValue[SimonCoreConstants.THREE_INT] = orderExportData.getSpoOrderId();
			stringArrayForCSVValue[SimonCoreConstants.FOUR_INT] = orderExportData.getSpoOrderLineId();
			stringArrayForCSVValue[SimonCoreConstants.FIVE_INT] = orderExportData.getProductId();
			stringArrayForCSVValue[SimonCoreConstants.SIX_INT] = orderExportData.getProductSku();
			stringArrayForCSVValue[SimonCoreConstants.SEVEN_INT] = orderExportData.getBaseProductId();
			stringArrayForCSVValue[SimonCoreConstants.EIGHT_INT] = orderExportData.getProductName();
			stringArrayForCSVValue[SimonCoreConstants.NINE_INT] = orderExportData.getProductVariantFields();
			stringArrayForCSVValue[SimonCoreConstants.TEN_INT] = getProductPriceinFormat(orderExportData.getProductPrice());
			stringArrayForCSVValue[SimonCoreConstants.ELEVEN_INT] = orderExportData.getCustomerFirstName();
			stringArrayForCSVValue[SimonCoreConstants.TWELVE_INT] = orderExportData.getCustomerLastName();
			stringArrayForCSVValue[SimonCoreConstants.THIRTEEN_INT] = orderExportData.getCustomerEmailAddress();
			stringArrayForCSVValue[SimonCoreConstants.FOURTEEN_INT] = orderExportData.getCustomerBillingZipCode();
			stringArrayForCSVValue[SimonCoreConstants.FIFTEEN_INT] = orderExportData.getCustomerShippingZipCode();
			records.add(stringArrayForCSVValue);
		}
		return records;
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	public boolean moveOrderexportCsvfileTosftp()
	{
		boolean result;
		final String privateKey = configurationService.getConfiguration()
				.getString(SimonCoreConstants.ORDER_EXPORT_SFTP_PRIVATE_KEY);
		final String host = configurationService.getConfiguration().getString(SimonCoreConstants.ORDER_EXPORT_SFTP_HOST);
		final String user = configurationService.getConfiguration().getString(SimonCoreConstants.ORDER_EXPORT_SFTP_USER);
		final String sourcePathLocalDirectory = configurationService.getConfiguration()
				.getString(SimonCoreConstants.VALUE_FOR_ORDER_EXPORT);
		final String targetPathSftpDirectory = configurationService.getConfiguration()
				.getString(SimonCoreConstants.MOVE_ORDER_EXPORT_FROM_NFS_TO_SFTP_PATH);
		final TransferFileToSftpUtils transferFileToSftpUtils = new TransferFileToSftpUtils(privateKey, host, user,
				sourcePathLocalDirectory, targetPathSftpDirectory);
		result = transferFileToSftpUtils.moveFilesToSftp();
		return result;
	}

	/**
	 * Creation date of the order, value format will be yyyy-MM-dd'T'hh:mm:ss+00 e.g. 2017-06-15T10:45:53+05 (+nn is the
	 * timezone relative to GMT)
	 *
	 * @param date
	 * @return String
	 */
	private String convertDateToString(final Date date)
	{
		final DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss ZZ", Locale.US);
		format.setTimeZone(TimeZone.getTimeZone("GMT"));
		String dateString = format.format(date);
		final String[] arrayString = dateString.split(SimonCoreConstants.SPACE);
		dateString = arrayString[SimonCoreConstants.ZERO_INT] + StringUtils.EMPTY
				+ arrayString[SimonCoreConstants.FIRST_INT].substring(SimonCoreConstants.ZERO_INT, SimonCoreConstants.THREE_INT);
		return dateString;
	}

	private String getProductPriceinFormat(final Double price)
	{
		final DecimalFormat df = new DecimalFormat("#0.00");
		return df.format(price);
	}

	/**
	 * Use to create file name
	 *
	 * @param retailerPrefix
	 * @return fileName
	 */
	private String getFileNameForRetailer(final String retailerPrefix)
	{
		String fileName;
		final DateFormat format = new SimpleDateFormat(SimonCoreConstants.TIMESTAMP_FOR_RETAILER, Locale.US);
		final String timeStamp = format.format(new Date());
		fileName = configurationService.getConfiguration().getString(SimonCoreConstants.VALUE_FOR_ORDER_EXPORT)
				+ SimonCoreConstants.FORWARD_SLASH + retailerPrefix + SimonCoreConstants.UNDERSCORE
				+ SimonCoreConstants.SPO_ORDER_FILE_NAME_FOR_RETAILER + SimonCoreConstants.UNDERSCORE + timeStamp
				+ SimonCoreConstants.DOT + SimonCoreConstants.CSV;
		return fileName;
	}

	/**
	 * Use to create file name
	 *
	 * @param retailerPrefix
	 * @return fileName
	 */
	private String getfileNameForBOT(final String retailerPrefix)
	{
		String fileName;
		final DateFormat format = new SimpleDateFormat(SimonCoreConstants.TIMESTAMP_FOR_BOT, Locale.US);
		final String timeStamp = format.format(new Date());
		fileName = configurationService.getConfiguration().getString(SimonCoreConstants.VALUE_FOR_ORDER_EXPORT)
				+ SimonCoreConstants.FORWARD_SLASH + retailerPrefix + SimonCoreConstants.UNDERSCORE
				+ SimonCoreConstants.SPO_ORDER_FILE_NAME_FOR_BOT + SimonCoreConstants.UNDERSCORE + timeStamp + SimonCoreConstants.DOT
				+ SimonCoreConstants.CSV;
		return fileName;
	}

	/**
	 * @param orderForBotAndRetailerFeedStatusConverter
	 */
	@Required
	public void setOrderForBotAndRetailerFeedStatusConverter(
			final Converter<OrderEntryModel, OrderExportData> orderForBotAndRetailerFeedStatusConverter)
	{
		this.orderForBotAndRetailerFeedStatusConverter = orderForBotAndRetailerFeedStatusConverter;
	}

}
