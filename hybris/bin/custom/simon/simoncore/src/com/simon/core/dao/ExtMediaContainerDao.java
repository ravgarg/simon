package com.simon.core.dao;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.dao.MediaContainerDao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * This is a extension to mediaContainerDao which have some additional.
 */
public interface ExtMediaContainerDao extends MediaContainerDao, Serializable
{
	/**
	 * This method get all mediaContainer which have version value greater then zero.
	 *
	 * @return
	 */
	public List<MediaContainerModel> getMediaContainersWithVersionGreaterThenZero(final Date cleanupAgeDate);
}
