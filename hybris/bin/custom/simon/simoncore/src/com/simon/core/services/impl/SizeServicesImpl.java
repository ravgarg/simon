package com.simon.core.services.impl;

import java.util.List;

import javax.annotation.Resource;

import com.simon.core.dao.SizeDao;
import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.model.SizeModel;
import com.simon.core.services.SizeService;


/**
 * SizeImpl, default implementation of {@link SizeService}.
 */
public class SizeServicesImpl implements SizeService
{
	/** The Size DAO. */
	@Resource(name = "sizeDao")
	private SizeDao sizeDao;

	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public List<SizeModel> getAllSizesByCode(final List<String> sizeCodes)
	{
		return sizeDao.getAllSizesByCode(sizeCodes);
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public SizeModel getSizeForId(final String sizeCode)
	{
		return sizeDao.getSizeForId(sizeCode);
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public List<SizeModel> getAllSizes()
	{
		return sizeDao.findAllSizes();
	}


	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public ProductSizeSequenceModel getSequenceForSizes(final String sizeCode)
	{

		return sizeDao.getSequenceForSizes(sizeCode);
	}

	/**
	 *
	 * {@inheritDoc}
	 *
	 */
	@Override
	public List<ProductSizeSequenceModel> getAllSequencesForSizes()
	{
		return sizeDao.getAllSequencesForSizes();
	}
}
