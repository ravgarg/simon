package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.jalo.numberseries.NumberSeriesManager;
import de.hybris.platform.order.strategies.impl.DefaultCreateOrderFromCartStrategy;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.math.RandomUtils;


/**
 * This class use to generate order code
 */
public class ExtDefaultCreateOrderFromCartStrategy extends DefaultCreateOrderFromCartStrategy
{

	public static final int MAX_ORDER_COUNT = 9999999;
	@Resource
	private CMSSiteService cmsSiteService;

	@Resource
	private ConfigurationService configurationService;

	/**
	 * Generate a code for created order.
	 *
	 * @param cart
	 *           You can use a cart to generate new code for order.
	 */
	@Override
	protected String generateOrderCode(final CartModel cart)
	{
		String orderNumber = super.generateOrderCode(cart);
		final int orderCode = Integer.parseInt(orderNumber);
		if (orderCode > MAX_ORDER_COUNT)
		{
			final NumberSeriesManager nsm = NumberSeriesManager.getInstance();
			final String startCode = configurationService.getConfiguration().getString("keygen.order.code.start");
			final String key = configurationService.getConfiguration().getString("keygen.order.code.ordername");
			final String template = configurationService.getConfiguration().getString("keygen.order.code.template");

			nsm.resetNumberSeries(key, startCode, 1, template);
			orderNumber = super.generateOrderCode(cart);
		}

		return new StringBuilder(getDateString()).append(getSiteId()).append(getRandomNumber()).append(orderNumber).toString();
	}

	/**
	 * Gets the site id.
	 *
	 * @return the site id
	 */
	private String getSiteId()
	{
		return cmsSiteService.getCurrentSite().getSiteId();
	}

	/**
	 * Gets the 6 digit random number.
	 *
	 * @return the random number
	 */
	private int getRandomNumber()
	{
		return RandomUtils.nextInt(100000) + 900000;

	}

	/**
	 * Gets the date yyMMdd as string
	 *
	 * @return the date string
	 */
	private String getDateString()
	{
		final DateFormat dateFormat = new SimpleDateFormat("yyMMdd", Locale.US);
		final Date date = new Date();
		return dateFormat.format(date);
	}

}
