package com.simon.core.dao;

import de.hybris.platform.category.model.CategoryModel;

import java.util.List;


/**
 * This interface fetches all L1 categories corresponding to Home category code
 */
public interface LeftNavigationCategoryDao
{
	/**
	 * This method returns all categories corresponding to Home category code
	 *
	 * @param categoryId
	 *           Home category code
	 * @return list of all L1 categories
	 */
	List<CategoryModel> getL1Categories(final String categoryId);
}
