package com.simon.core.commerceservice.order;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;

import java.util.List;


/**
 *
 */
public interface ExtCommerceCartService extends CommerceCartService
{

	/**
	 * Validate cart with only remove the OOS Product if Stock Quantity change for Order Entry then nothing will be
	 * happen
	 *
	 * @param parameter
	 * @return List<CommerceCartModification>
	 * @throws CommerceCartModificationException
	 */
	List<CommerceCartModification> getChangeEntriesInCart(CommerceCartParameter parameter)
			throws CommerceCartModificationException;

}
