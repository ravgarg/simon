package com.simon.core.syetem.initial.data.setup;

import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;

import org.apache.log4j.Logger;


/**
 * This class imports the sample data based on some properties file and Mirkal related data.
 */
public class SimonSampleDataImportService extends SampleDataImportService
{
	private static final Logger LOG = Logger.getLogger(SimonSampleDataImportService.class);
	private static final String IMPORT_SAMPLE_PRODUCT_DATA = "importSampleProduct";
	private boolean importSampleProduct;



	@Override
	public void importCommonData(final String extensionName)
	{
		parentSampleCommon(extensionName);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/designers.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/customer-address.impex", extensionName),
				false);


		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/custom-message.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/my-centers.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/my-size.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/coredata/common/essential-data_image_magick.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/pdp-swatch-enum.impex", extensionName),
				false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/common/essential-data_sync_job.impex", extensionName), false);


	}

	@Override
	public void importAllData(final AbstractSystemSetup systemSetup, final SystemSetupContext context, final ImportData importData,
			final boolean syncCatalogs)
	{
		importSampleProduct = systemSetup.getBooleanSystemSetupParameter(context, IMPORT_SAMPLE_PRODUCT_DATA);
		parentSampleImportAll(systemSetup, context, importData, syncCatalogs);
	}

	@Override
	public void importProductCatalog(final String extensionName, final String productCatalogName)
	{
		parentImportProductCatalog(extensionName, productCatalogName);
		if (importSampleProduct)
		{
			importSampleProduct(extensionName);
		}
	}


	/**
	 * @param extensionName
	 * @param productCatalogName
	 */
	public void parentImportProductCatalog(final String extensionName, final String productCatalogName)
	{
		super.importProductCatalog(extensionName, productCatalogName);
	}


	private void importSampleProduct(final String extensionName)
	{
		LOG.info("*************************Importing Sample designer and retailers Data*******************************");



		getSetupImpexService().importImpexFile(String.format("/%s/import/sampledata/common/retailers.impex", extensionName), false);
		getSetupImpexService()
				.importImpexFile(String.format("/%s/import/sampledata/common/contentPageMapping.impex", extensionName), false);
		LOG.info("*************************Importing Sample designer and retailers Data*******************************");
		LOG.info("************************Importing Sample Product Data******************************");
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-media.impex", extensionName),
				false);
		getSetupImpexService().importImpexFile(String.format(
				"/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-prices.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format(
				"/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-stocklevels.impex", extensionName), false);
		getSetupImpexService().importImpexFile(String.format(
				"/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-relations.impex", extensionName), false);

		LOG.info("*************************Sample Product Data Imported*******************************");


	}




	/**
	 * This method is created to mock the call to parent importAllData method
	 *
	 * @param systemSetup
	 * @param context
	 * @param importData
	 * @param syncCatalogs
	 *
	 *
	 */
	public void parentSampleImportAll(final AbstractSystemSetup systemSetup, final SystemSetupContext context,
			final ImportData importData, final boolean syncCatalogs)
	{
		super.importAllData(systemSetup, context, importData, syncCatalogs);
	}

	/**
	 * This method is created to mock the call to parent importCommonData method
	 *
	 * @param extensionName
	 *           - extension name
	 */
	public void parentSampleCommon(final String extensionName)
	{
		super.importCommonData(extensionName);
	}

	@Override
	public void importSolrIndex(final String extensionName, final String storeName)
	{
		parentSampleSolr(extensionName, storeName);
	}

	/**
	 * This method is created to mock the call to parent importSolrIndex method
	 *
	 * @param extensionName
	 *           - extension name
	 * @param storeName
	 *           - store name
	 */
	public void parentSampleSolr(final String extensionName, final String storeName)
	{
		super.importSolrIndex(extensionName, storeName);
	}

	/**
	 * @return importSampleProduct
	 */
	public boolean isImportSampleProduct()
	{
		return importSampleProduct;
	}
}
