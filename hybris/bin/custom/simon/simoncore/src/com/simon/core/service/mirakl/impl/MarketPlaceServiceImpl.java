package com.simon.core.service.mirakl.impl;

import static java.util.Collections.singletonList;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.payment.MiraklPaymentStatus;
import com.mirakl.client.mmp.domain.payment.debit.MiraklOrderPayment;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.front.request.order.accept.MiraklAcceptOrderRequest;
import com.mirakl.client.mmp.request.order.accept.MiraklAcceptOrderLine;
import com.mirakl.client.mmp.request.order.worflow.MiraklReceiveOrderRequest;
import com.mirakl.client.mmp.request.payment.debit.MiraklConfirmOrderDebitRequest;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.simon.core.api.SimonMiraklMarketplacePlatformFrontOperatorApi;
import com.simon.core.api.mirakl.dto.SimonMiraklShipOrderRequest;
import com.simon.core.api.mirakl.dto.SimonMiraklTrackingInfoBean;
import com.simon.core.api.mirakl.dto.SimonMiraklTrackingOrderRequest;
import com.simon.core.service.mirakl.MarketPlaceService;


/**
 * This is the implementation class of {@link MarketPlaceService} used for invoking Mirakl API's to fetch/update details
 * from/within Mirakl.
 *
 */
public class MarketPlaceServiceImpl implements MarketPlaceService
{
	private static final Logger LOG = LoggerFactory.getLogger(MarketPlaceServiceImpl.class);

	@Autowired
	SimonMiraklMarketplacePlatformFrontOperatorApi frontOperatorApi;

	@Autowired
	MarketplaceConsignmentService marketplaceConsignmentService;

	@Autowired
	MiraklMarketplacePlatformFrontApi miraklApi;
	@Autowired
	ModelService modelService;

	/**
	 * Method is used to call Mirakl API OR21, it will update the Order <code>acceptance/Rejection</code> status
	 * depending on Retailer action.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @param accepted
	 *           boolean
	 * @return void
	 *
	 */
	@Override
	public void acceptOrRejectOrder(final AbstractOrderModel orderModel, final boolean accepted)
	{
		final List<MiraklAcceptOrderLine> acceptOrderLines = new ArrayList<>();
		final Set<ConsignmentModel> consignmentModels = orderModel.getConsignments();
		for (final ConsignmentModel consignmentModel : consignmentModels)
		{
			final Set<ConsignmentEntryModel> consignmentEntries = consignmentModel.getConsignmentEntries();
			for (final ConsignmentEntryModel entryModel : consignmentEntries)
			{
				final MiraklAcceptOrderLine acceptOrderLine = new MiraklAcceptOrderLine(entryModel.getMiraklOrderLineId(), accepted);
				acceptOrderLines.add(acceptOrderLine);
			}
			final MiraklAcceptOrderRequest request = new MiraklAcceptOrderRequest(consignmentModel.getCode(), acceptOrderLines);
			try
			{
				frontOperatorApi.acceptOrRejectOrder(request);
			}
			catch (final MiraklApiException exception)
			{
				LOG.error("No authentication key given or no user was found with given API key", exception);
			}
		}
	}

	/**
	 * Method is used to call OR24 API, to update the <code>shipping state</code> within Mirakl.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @return void
	 */
	@Override
	public void shipOrder(final AbstractOrderModel orderModel)
	{

		final Set<ConsignmentModel> consignmentModels = orderModel.getConsignments();
		for (final ConsignmentModel consignmentModel : consignmentModels)
		{
			final SimonMiraklShipOrderRequest request = new SimonMiraklShipOrderRequest(consignmentModel.getCode());
			try
			{
				frontOperatorApi.shipOrder(request);
			}
			catch (final MiraklApiException exception)
			{
				LOG.error("No authentication key given or no user was found with given API key", exception);
			}
		}
	}

	/**
	 * Method is used to call OR23 API, to update the <code>carrier tracking details</code> within Mirakl.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @return void
	 */
	@Override
	public void trackingOrder(final AbstractOrderModel orderModel)
	{

		final Set<ConsignmentModel> consignmentModels = orderModel.getConsignments();

		for (final ConsignmentModel consignmentModel : consignmentModels)
		{

			final SimonMiraklTrackingInfoBean infoBean = new SimonMiraklTrackingInfoBean();
			infoBean.setCarrierCode(consignmentModel.getCarrier());
			infoBean.setCarrierName(consignmentModel.getCarrier());
			infoBean.setCarrierUrl(consignmentModel.getTrackingID());
			infoBean.setTrackingNumber(consignmentModel.getTrackingID());
			final SimonMiraklTrackingOrderRequest request = new SimonMiraklTrackingOrderRequest(infoBean,
					consignmentModel.getCode());
			try
			{
				frontOperatorApi.trackOrder(request);
			}
			catch (final MiraklApiException exception)
			{
				LOG.error("No authentication key given or no user was found with given API key", exception);
			}
		}
	}

	/**
	 * Method is used to call PA01 API, to <code>valid/invalid</code the payment of the order in Mirakl.
	 *
	 * @param consignmentModel
	 *           ConsignmentModel
	 * @return void.
	 *
	 */
	@Override
	public void processPayment(final ConsignmentModel consignmentModel)
	{

		final MarketplaceConsignmentModel consignment = (MarketplaceConsignmentModel) consignmentModel;
		final MiraklOrderPayment debitRequest = new MiraklOrderPayment();
		debitRequest.setAmount(BigDecimal.valueOf(consignment.getTotalPrice()));
		debitRequest.setCurrencyIsoCode(MiraklIsoCurrencyCode.valueOf(consignment.getOrder().getCurrency().getIsocode()));
		debitRequest.setCustomerId(((CustomerModel) consignment.getOrder().getUser()).getContactEmail());
		debitRequest.setOrderId(consignment.getCode());
		debitRequest.setPaymentStatus(MiraklPaymentStatus.OK);
		final MiraklConfirmOrderDebitRequest confirmOrderDebitRequest = new MiraklConfirmOrderDebitRequest(
				singletonList(debitRequest));
		try
		{
			miraklApi.confirmOrderDebit(confirmOrderDebitRequest);
		}
		catch (final MiraklApiException exception)
		{
			LOG.error("No authentication key given or no user was found with given API key", exception);
		}
	}

	/**
	 * Method is used to call Mirakl API OR21, update the Order status from Pending Acceptance to Shipping in progress
	 * status
	 *
	 * @param consignment
	 * @param accepted
	 */
	@Override
	public void acceptOrRejectOrder(final MarketplaceConsignmentModel consignment, final boolean accepted)
	{
		final String orderId = consignment.getCode();
		final List<MiraklAcceptOrderLine> acceptOrderLines = new ArrayList<>();

		final Set<ConsignmentEntryModel> consignmentEntries = consignment.getConsignmentEntries();
		for (final ConsignmentEntryModel entryModel : consignmentEntries)
		{
			final MiraklAcceptOrderLine acceptOrderLine = new MiraklAcceptOrderLine(entryModel.getMiraklOrderLineId(), accepted);
			acceptOrderLines.add(acceptOrderLine);
		}
		final MiraklAcceptOrderRequest request = new MiraklAcceptOrderRequest(orderId, acceptOrderLines);
		try
		{
			frontOperatorApi.acceptOrRejectOrder(request);
			LOG.debug("Successfully call Mirakl OR21 API for Mirakl Order {}", orderId);
		}
		catch (final MiraklApiException exception)
		{
			LOG.error("Error Occured:: To call Mirakl OR21 API", exception);
		}
	}

	/**
	 * Method is used to call Mirakl API OR25, update the Mirakl Order status from Shipping in progress to Received
	 * status
	 *
	 * @param miraklOrderId
	 */
	@Override
	public void receivedMarketplaceOrder(final String miraklOrderId)
	{
		final MiraklReceiveOrderRequest request = new MiraklReceiveOrderRequest(miraklOrderId);
		try
		{
			frontOperatorApi.receiveOrder(request);
			LOG.debug("Successfully called Mirakl OR25 API for Mirakl Order {}", miraklOrderId);
		}
		catch (final MiraklApiException exception)
		{
			LOG.error("Error Occured :: To called Mirakl OR25 API", exception);
		}
	}

}
