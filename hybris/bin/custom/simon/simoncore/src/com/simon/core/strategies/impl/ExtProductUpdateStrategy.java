package com.simon.core.strategies.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductUpdateStrategy;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.model.MiraklCategoryModel;
import com.simon.core.util.AdditionalAttributesUtils;
import com.simon.core.util.CommonUtils;


/**
 * ExtProductUpdateStrategy extends {@link DefaultProductUpdateStrategy} and updates additional attributes such as
 * merchandizing category.
 */
public class ExtProductUpdateStrategy extends DefaultProductUpdateStrategy
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtProductUpdateStrategy.class);

	/**
	 * Updates additional attributes on products <br>
	 * {@inheritDoc}
	 */
	@Override
	public void applyValues(final ProductImportData data, final ProductImportFileContextData context) throws ProductImportException
	{
		LOGGER.info("Creating/Updating Product : {}", data.getProductToUpdate().getCode());
		data.getProductToUpdate().setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		applyDefaultValues(data, context);
		updateMerchandizingCategory(data, context);
	}

	/**
	 * Updates merchandizing category on product by matching additional attributes in Product defined by
	 * {@link GenericVariantProductModel#getAdditionalAttributes()} with Additional Attributes in Merchandizing Category
	 * Defined by {@link MerchandizingCategoryModel#getAdditionalAttributes()}. <br>
	 * Only those merchandizing categories are search for additional attributes that are associated with
	 * {@link MiraklCategoryModel} associated with product {@link ProductModel#getMiraklCategory()} <br>
	 * There may be cases when more than one category is matched for a given product in that case all matched categories
	 * will be associated with that product.
	 *
	 * @param data
	 *           product import data
	 */
	private void updateMerchandizingCategory(final ProductImportData data, final ProductImportFileContextData context)
	{
		final GenericVariantProductModel product = (GenericVariantProductModel) data.getProductToUpdate();

		final MiraklCategoryModel miraklCategoryModel = (MiraklCategoryModel) product.getMiraklCategory();

		final Collection<MerchandizingCategoryModel> merchandizingCategories = miraklCategoryModel.getMerchandizingCategories();

		final Set<MerchandizingCategoryModel> matchedMerchandizingCategories = AdditionalAttributesUtils
				.getMatchingMerchandizingCategories(product);
		updateMatchedMerchandizingCategories(data, product, matchedMerchandizingCategories);
		if (CollectionUtils.isEmpty(matchedMerchandizingCategories))
		{
			final String errorMessage = CommonUtils.format(
					"Import Id : [%s] Shop Id : [%s] Product : [%s] Base Product : [%s] Having Mirakl Category : [%s] Not Mapped To Any : [%s] Merchandizing Category",
					context.getMiraklImportId(), context.getShopId(), product.getCode(), product.getBaseProduct().getCode(),
					miraklCategoryModel.getCode(), merchandizingCategories.size());
			context.getMappingFailure().add(errorMessage);
			LOGGER.info(errorMessage);
		}
	}

	/**
	 * This method get all merchandising category from base product and remove auto generated merchandising category this
	 * list and add new matched merchandising to this list then save it to product model.
	 *
	 * @param data
	 * @param product
	 * @param matchedMerchandizingCategories
	 */
	private void updateMatchedMerchandizingCategories(final ProductImportData data, final GenericVariantProductModel product,
			final Set<MerchandizingCategoryModel> matchedMerchandizingCategories)
	{
		if (null != product && null != matchedMerchandizingCategories)
		{
			final Set<CategoryModel> listOfSuperCategories = new HashSet<>();
			listOfSuperCategories.addAll(CollectionUtils.subtract(product.getBaseProduct().getSupercategories(),
					product.getBaseProduct().getAutomatedGeneratedCategories()));
			listOfSuperCategories.addAll(matchedMerchandizingCategories);
			product.getBaseProduct()
					.setAutomatedGeneratedCategories(matchedMerchandizingCategories.stream().collect(Collectors.toList()));
			product.getBaseProduct().setSupercategories(listOfSuperCategories);
			data.getModelsToSave().add(product.getBaseProduct());
		}
	}

	/**
	 * Apply default values.
	 *
	 * @param data
	 *           the data
	 * @param context
	 *           the context
	 * @throws ProductImportException
	 *            the product import exception
	 */
	protected void applyDefaultValues(final ProductImportData data, final ProductImportFileContextData context)
			throws ProductImportException
	{
		super.applyValues(data, context);
	}

}
