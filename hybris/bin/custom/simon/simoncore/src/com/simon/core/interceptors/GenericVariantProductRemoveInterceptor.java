package com.simon.core.interceptors;

import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Remove interceptor to remove media Containers, price rows, stock levels, thumbnail and raw image of the variant being
 * removed.
 */
public class GenericVariantProductRemoveInterceptor implements RemoveInterceptor<GenericVariantProductModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(GenericVariantProductRemoveInterceptor.class);

	/**
	 * On remove method to register elements to remove
	 */
	@Override
	public void onRemove(final GenericVariantProductModel genericVariantProduct, final InterceptorContext ctx)
			throws InterceptorException
	{
		genericVariantProduct.getGalleryImages().forEach(mediaContainer -> {
			LOGGER.debug("Registering Removal for MediaContainer {}:{} for Product {}", mediaContainer.getQualifier(),
					mediaContainer.getPk(), genericVariantProduct.getCode());
			ctx.registerElementFor(mediaContainer, PersistenceOperation.DELETE);
		});

		genericVariantProduct.getEurope1Prices().forEach(priceRow -> {
			LOGGER.debug("Registering Removal for PriceRow {} for Product {}", priceRow.getPk(), genericVariantProduct.getCode());
			ctx.registerElementFor(priceRow, PersistenceOperation.DELETE);
		});

		genericVariantProduct.getStockLevels().forEach(stockLevel -> {
			LOGGER.debug("Registering Removal for StockLevel {} for Product {}", stockLevel.getPk(),
					genericVariantProduct.getCode());
			ctx.registerElementFor(stockLevel, PersistenceOperation.DELETE);
		});

		if (null != genericVariantProduct.getRawPicture())
		{
			LOGGER.debug("Registering Removal for RawPicture {}:{} for Product {}", genericVariantProduct.getRawPicture().getCode(),
					genericVariantProduct.getRawPicture().getPk(), genericVariantProduct.getCode());
			ctx.registerElementFor(genericVariantProduct.getRawPicture(), PersistenceOperation.DELETE);
		}
		if (null != genericVariantProduct.getThumbnail())
		{
			LOGGER.debug("Registering Removal for Thumbnail {}:{} for Product {}", genericVariantProduct.getThumbnail().getCode(),
					genericVariantProduct.getThumbnail().getPk(), genericVariantProduct.getCode());
			ctx.registerElementFor(genericVariantProduct.getThumbnail(), PersistenceOperation.DELETE);
		}

	}

}
