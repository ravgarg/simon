package com.simon.core.dynamichandlers;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import com.mirakl.hybris.core.enums.ShopState;


/**
 * ProductRetailerStatusHandler is a dynamic handler for attribute {@link ProductModel#RETAILERACTIVE}
 */
public class ProductRetailerStatusHandler extends AbstractDynamicAttributeHandler<Boolean, ProductModel>
{

	/**
	 * Returns true if associated Retailer's Shop State is in OPEN state else returns false.
	 */
	@Override
	public Boolean get(final ProductModel product)
	{
		return product.getShop() != null && ShopState.OPEN.equals(product.getShop().getState());
	}
}
