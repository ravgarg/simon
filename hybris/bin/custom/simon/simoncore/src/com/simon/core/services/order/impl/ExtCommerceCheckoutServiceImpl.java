package com.simon.core.services.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.core.services.order.ExtCommerceCheckoutService;
import com.simon.integration.dto.CreateCartConfirmationDTO;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.cartestimate.EstimatedPriceDTO;
import com.simon.integration.dto.cart.cartestimate.RetailerResponseDTO;
import com.simon.integration.services.CartCheckOutIntegrationService;


/**
 * This class extends {@link DefaultCommerceCheckoutService} and implements new methods of
 * {@link ExtCommerceCheckoutService} in order to accommodate synchronization between hybris cart and a third party cart
 */
public class ExtCommerceCheckoutServiceImpl extends DefaultCommerceCheckoutService implements ExtCommerceCheckoutService
{

	@Resource
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;

	@Resource
	private ModelService modelService;


	@Resource
	private Converter<CreateCartConfirmationDTO, AdditionalCartInfoModel> additionalCartInfoReverseConverter;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource
	private DefaultRetailerSubbagDeliveryModeStrategy retailerSubbagDeliveryModeStrategy;


	/**
	 * @param responseDTO
	 */
	@Override
	public void populateShippingAndTax(final List<RetailersInfoModel> retailerInfoList, final CartEstimateResponseDTO responseDTO)
	{
		for (final RetailerResponseDTO retailerDTO : responseDTO.getRetailers())
		{
			retailerInfoList.stream().filter(retailerInfo -> retailerInfo.getRetailerId().equals(retailerDTO.getRetailerID()))
					.findFirst().ifPresent(retailerInfo -> populateRetailerShippingAndTax(retailerDTO, retailerInfo));
		}
	}

	@Override
	public void setDefaultDeliveryModePerRetailer(final String deliveryMethodId, final List<RetailersInfoModel> retailersInfoList,
			final CartModel cart)
	{
		retailersInfoList.stream().forEach(retailer -> setShippingForRetailer(retailer, deliveryMethodId, cart));
		final CommerceCartParameter commerceCartParameter = new CommerceCartParameter();
		commerceCartParameter.setEnableHooks(true);
		commerceCartParameter.setCart(cart);
		cart.setCalculated(false);
		getCommerceCartCalculationStrategy().calculateCart(commerceCartParameter);
	}

	@Override
	public void selectShippingForEstimateCall(final String selectedShippingMethod, final List<RetailersInfoModel> retailerInfoList)
	{
		retailerInfoList.stream().forEach(retailer -> retailer.getShippingDetails().stream()
				.forEach(shipping -> selectDeselectForEstimateCall(shipping, selectedShippingMethod)));
	}

	/**
	 * @param retailer
	 * @param deliveryMethodId
	 * @param cartModel
	 */
	@Override
	public void setShippingForRetailer(final RetailersInfoModel retailer, final String deliveryMethodId, final CartModel cartModel)
	{
		retailer.getShippingDetails().stream()
				.forEach(shippingDetail -> selectDeselectForCheckout(shippingDetail, deliveryMethodId));
		retailerSubbagDeliveryModeStrategy.setRetailerSubbagDeliveryMode(cartModel, retailer.getRetailerId(), deliveryMethodId);
	}

	private void selectDeselectForEstimateCall(final ShippingDetailsModel shipping, final String selectedShipping)
	{
		if (shipping.getCode().equalsIgnoreCase(selectedShipping))
		{
			shipping.setSelected(true);
		}
		else
		{
			shipping.setSelected(false);
		}
	}

	private void selectDeselectForCheckout(final ShippingDetailsModel shippingDetail, final String selectedShipping)
	{
		if (shippingDetail.getCode().equalsIgnoreCase(selectedShipping))
		{
			shippingDetail.setSelectedForCart(true);
		}
		else
		{
			shippingDetail.setSelectedForCart(false);
		}
	}

	private void populateRetailerShippingAndTax(final RetailerResponseDTO retailerDTO, final RetailersInfoModel retailersInfoModel)
	{
		final EstimatedPriceDTO estimatedPrice = retailerDTO.getRetailerPriceDTO();

		retailersInfoModel.setAppliedSalesTax(Double.parseDouble(estimatedPrice.getTotalSalesTax().substring(1)));
		double taxRate = 0d;
		final double receivedSubtotal = Double.parseDouble(estimatedPrice.getSubtotal().substring(1));
		final double appliedTax = Double.parseDouble(estimatedPrice.getTotalSalesTax().substring(1));
		if (appliedTax > 0)
		{
			taxRate = appliedTax / (receivedSubtotal / 100);
		}


		final double appliedTaxRate = BigDecimal.valueOf(taxRate).setScale(2, RoundingMode.HALF_UP).doubleValue();

		retailersInfoModel.setAppliedTaxRate(appliedTaxRate);

		retailersInfoModel.getShippingDetails().stream().filter(shippingDetailsModel -> shippingDetailsModel.isSelected())
				.findFirst().ifPresent(shippingDetailsModel -> setAndSaveShipping(estimatedPrice, shippingDetailsModel));
	}

	private void setAndSaveShipping(final EstimatedPriceDTO price, final ShippingDetailsModel shipping)
	{
		shipping.setShippingEstimate(Double.parseDouble(price.getTotalShippingPrice().substring(1)));
	}

}
