package com.simon.core.customer.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.dao.CustomerAccountDao;
import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.customer.dao.ExtCustomerAccountDao;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;


/**
 * The Class ExtCustomerAccountDaoImpl. DB based default implementation of {@link CustomerAccountDao}
 */
public class ExtCustomerAccountDaoImpl extends DefaultCustomerAccountDao implements ExtCustomerAccountDao
{
	private static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY = "SELECT {" + OrderModel.PK + "}, {" + OrderModel.CREATIONTIME
			+ "}, {" + OrderModel.CODE + "} FROM {" + OrderModel._TYPECODE + "} WHERE {" + OrderModel.USER + "} = ?customer AND {"
			+ OrderModel.CREATIONTIME + "} >= ?dateRange AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.STORE
			+ "} = ?store";

	private static final String FILTER_ORDER_STATUS = " AND {" + OrderModel.STATUS + "} NOT IN (?filterStatusList)";

	private static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS = FIND_ORDERS_BY_CUSTOMER_STORE_QUERY + " AND {"
			+ OrderModel.STATUS + "} IN (?statusList)";

	private static final String SORT_ORDERS_BY_DATE = " ORDER BY {" + OrderModel.CREATIONTIME + "} DESC, {" + OrderModel.PK + "}";

	private static final String SORT_ORDERS_BY_CODE = " ORDER BY {" + OrderModel.CODE + "},{" + OrderModel.CREATIONTIME
			+ "} DESC, {" + OrderModel.PK + "}";

	private static final String CUSTOMER_MUST_NOT_BE_NULL = "Customer must not be null";
	private static final String CUSTOMER = "customer";
	private static final String BY_DATE = "byDate";
	private static final String BY_ORDER_NUMBER = "byOrderNumber";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DesignerModel> findListOfAllDesigners(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		final SearchResult<DesignerModel> result = getFlexibleSearchService()
				.search(SimonFlexiConstants.FIND_ALL_DESIGNERS_EXCEPT_SAVED, queryParams);
		return result.getResult();

	}

	@Override
	public List<List<Object>> getMyFavProductOrderByDate(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_FAVORITES_BY_CUSTOMER);
		fQuery.addQueryParameter(CUSTOMER, customerModel.getPk());
		fQuery.setResultClassList(Arrays.asList(String.class, ProductModel.class));
		final SearchResult<List<Object>> searchResult = getFlexibleSearchService().search(fQuery);
		return searchResult.getResult();
	}

	/**
	 * @param duplicateFromAddressId
	 * @return list of AddressModel
	 */
	@Override
	public List<AddressModel> findAddressByDuplicateFrom(final String duplicateFromAddressId)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("duplicateFromAddressId", duplicateFromAddressId);
		final SearchResult<AddressModel> result = getFlexibleSearchService()
				.search(SimonFlexiConstants.FIND_ADDRESS_BY_DUPLICATE_FROM, queryParams);
		return result.getResult();

	}

	@Override
	public List<ShopModel> findListOfAllStores(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		final SearchResult<ShopModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_ALL_STORES_EXCEPT_SAVED,
				queryParams);
		return result.getResult();
	}

	/**
	 * Find list of all designers. Returns List of {@link DesignerModel} for user <code>code</code>.
	 *
	 * @return the list of designer model
	 *
	 */
	public List<DesignerModel> findListOfAllDesigners()
	{
		final SearchResult<DesignerModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_ALL_DESIGNERS);
		return result.getResult();
	}

	/**
	 * Find list of all deals for customer. Returns List of {@link DealModel} for user <code>code</code>.
	 *
	 * @return the list of deal model
	 *
	 */
	@Override
	public List<DealModel> findListOfAllDeals(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		final SearchResult<DealModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_DEALS_BY_CUSTOMER,
				queryParams);
		return result.getResult();
	}

	@Override
	public SearchPageData<OrderModel> findOrdersByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store,
			final OrderStatus[] status, final PageableData pageableData)
	{
		validateParameterNotNull(customerModel, CUSTOMER_MUST_NOT_BE_NULL);
		validateParameterNotNull(store, "Store must not be null");

		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CUSTOMER, customerModel);
		queryParams.put("store", store);

		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -2);
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		queryParams.put("dateRange", dateFormat.format(cal.getTime()));

		String filterClause = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(getFilterOrderStatusList()))
		{
			queryParams.put("filterStatusList", getFilterOrderStatusList());
			filterClause = FILTER_ORDER_STATUS;
		}

		List<SortQueryData> sortQueries;

		if (ArrayUtils.isNotEmpty(status))
		{
			queryParams.put("statusList", Arrays.asList(status));
			sortQueries = Arrays.asList(
					createSortQueryData(BY_DATE,
							createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS, filterClause, SORT_ORDERS_BY_DATE)),
					createSortQueryData(BY_ORDER_NUMBER,
							createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY_AND_STATUS, filterClause, SORT_ORDERS_BY_CODE)));
		}
		else
		{
			sortQueries = Arrays.asList(
					createSortQueryData(BY_DATE, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, filterClause, SORT_ORDERS_BY_DATE)),
					createSortQueryData(BY_ORDER_NUMBER,
							createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, filterClause, SORT_ORDERS_BY_CODE)));
		}

		return getPagedFlexibleSearchService().search(sortQueries, BY_DATE, queryParams, pageableData);
	}
}
