package com.simon.core.order.export.cronjob;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.model.OrderExportCronJobModel;
import com.simon.core.services.ExtOrderService;


/**
 * Cronjob for creating csv file find the order to export and create csv file
 */
public class OrderExportJob extends AbstractJobPerformable<OrderExportCronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderExportJob.class);
	@Resource
	private ExtOrderService orderService;

	@Override
	public PerformResult perform(final OrderExportCronJobModel orderExportJobModel)
	{
		PerformResult performResult = null;
		try
		{
			final List<OrderModel> orders = orderService.findOrdersToExport(orderExportJobModel.getDay(),
					orderExportJobModel.getStartHours(), orderExportJobModel.getEndHours());

			LOGGER.info("Order Export For Order Status:: Total orders {}  found for export", orders.size());
			if ((CollectionUtils.isNotEmpty(orders) && orderService.createOrderExportFiles(orders))
					&& orderService.moveOrderexportCsvfileTosftp())
			{
				LOGGER.info(
						"Order Export For Order Status:: Cron Job Finished Successfully for Generating Order Export CSV File. Or Transferred files to SFTP");
				performResult = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}
			else
			{
				LOGGER.info(
						"Order Export For Order Status:: Cron Job Finished Successfully but something went wrong in Generating  File Or Transferred files to SFTP");
				performResult = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
			}
		}
		catch (final Exception ex)
		{
			LOGGER.error(
					"Order Export For Order Status:: Error Occured Cron Job fail, Or something went wrong in generating  File Or Transferred files to SFTP",
					ex);
			performResult = new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		return performResult;
	}


}
