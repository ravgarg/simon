package com.simon.core.services;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;

import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.hybris.core.order.services.MiraklOrderService;


/**
*
*/
public interface ExtMiraklOrderService extends MiraklOrderService
{
	/**
	 * @param order
	 * @return
	 */
	List<MiraklCreatedOrders> createMarketplaceOrdersForRetailers(final OrderModel order);

	void validateOrder(final OrderModel order);

	/**
	 * @param order
	 * @param createdOrders
	 * @param retailer
	 * @return
	 */
	String storeCreatedOrdersForRetailer(final OrderModel order, final MiraklCreatedOrders createdOrders, final String retailer);

	/**
	 * @param order
	 * @param retailer
	 * @return
	 */
	MiraklCreatedOrders loadCreatedOrdersForRetailer(final OrderModel order, final String retailer);
}
