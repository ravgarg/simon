package com.simon.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.services.ExtProductService;


/**
 * This job updates the GenericVariantProduct/Base Product status to CHECK from UNAPPROVED, based on certain conditions.
 *
 */
public class ProductUnapproveToCheckJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductUnapproveToCheckJob.class);
	@Resource
	private ExtProductService extProductService;

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * This method updates product status of Variants,if variant has atleast one gallery image and its base product has
	 * atleast one MerchandizingCategory.
	 *
	 * Base Products of all such variants are identified, and there status is also updated to CHECK only if there status
	 * was not APPROVED earlier
	 */
	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);
		final Consumer<ProductModel> consumer = product -> {
			product.setApprovalStatus(ArticleApprovalStatus.CHECK);
			LOGGER.debug("Product/VariantProduct {} Status Updated to CHECK", product.getCode());
		};
		final List<GenericVariantProductModel> variants = extProductService
				.getUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion);
		final Set<ProductModel> baseProducts = variants.stream()
				.filter(product -> !product.getBaseProduct().getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))
				.map(GenericVariantProductModel::getBaseProduct).collect(Collectors.toSet());
		LOGGER.info("{} Variant Products' Status updated to check", variants.size());
		LOGGER.info("{} Base Products' status updated to check", baseProducts.size());
		variants.forEach(consumer);
		baseProducts.forEach(consumer);
		try
		{
			modelService.saveAll(baseProducts);
			modelService.saveAll(variants);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final ModelSavingException exception)
		{
			LOGGER.error("Exception while updating product status to CHECK", exception);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}
}
