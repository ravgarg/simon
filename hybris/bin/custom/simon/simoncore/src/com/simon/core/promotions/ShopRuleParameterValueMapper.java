package com.simon.core.promotions;

import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapper;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapperException;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import javax.annotation.Resource;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;



/**
 * This class helps to convert ShopModel to and from string. This class is required for display of ItemType(Shop) in
 * back office and shop model association with actions and conditions in
 */
public class ShopRuleParameterValueMapper implements RuleParameterValueMapper<ShopModel>
{

	/** The shop service. */
	@Resource
	private ShopService shopService;


	/**
	 * maps items string identifier to its model.
	 *
	 * @param shopId
	 *           the shop id
	 * @return the shop model
	 */
	@Override
	public ShopModel fromString(final String shopId)
	{
		ServicesUtil.validateParameterNotNull(shopId, "String value cannot be null");
		final ShopModel shop = shopService.getShopForId(shopId);
		if (shop == null)
		{
			throw new RuleParameterValueMapperException("Cannot find shop with the code: " + shopId);
		}
		return shop;

	}


	/**
	 * maps items to it the string identifier
	 *
	 * @param shop
	 *           the shop
	 * @return the string
	 */
	@Override
	public String toString(final ShopModel shop)
	{
		ServicesUtil.validateParameterNotNull(shop, "Object cannot be null");
		return shop.getId();
	}
}
