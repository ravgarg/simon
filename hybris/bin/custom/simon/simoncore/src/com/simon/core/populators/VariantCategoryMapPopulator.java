package com.simon.core.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.VariantCategoryData;
import com.simon.core.dto.VariantValueCategoryData;
import com.simon.core.enums.ControlType;


/**
 * This class populates data into Map of string and {@link VariantCategoryData}, where string is corresponding
 * VariantCategory ID. Indexed Data from solr document in populated into this Map.
 *
 */
public class VariantCategoryMapPopulator implements Populator<Document, Map<String, VariantCategoryData>>
{
	private static final Logger LOG = LoggerFactory.getLogger(VariantCategoryMapPopulator.class);
	private static final String NAME_TEXT = "Name";

	/**
	 * This method populates data into Map of string and {@link VariantCategoryData}, where string is corresponding
	 * VariantCategory ID from {@link Document}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 *
	 * @param variantCategoryMap
	 *           {String, VariantCategoryData} Key is VariantCategory ID and value is DTO which holds TITLE of
	 *           VariantCategory ID and list of {@link VariantValueCategoryData}. Title is fetched from locales
	 *           properties, based on VariantCategory name
	 *
	 *           List of {@link VariantValueCategoryData} contains VariantValueCategory name and ID, which belong to
	 *           VariantCategory
	 *
	 *           Data is inserted into LinkedHashMap @param variantCategoryMap order of VariantCategory hierarchy
	 *
	 * @throws ConversionException
	 *            if document's field value is not able to cast in given type
	 */
	@Override
	public void populate(final Document document, final Map<String, VariantCategoryData> variantCategoryMap)
	{

		final List<String> variantCategories = (List<String>) document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL);
		final Map<String, String> variantCategoryIdAndNameMap = new HashMap<>();
		final Map<String, String> variantCategoryIdAndTypeMap = new HashMap<>();
		String controlType = ControlType.SWATCH.getCode();
		if (CollectionUtils.isNotEmpty(variantCategories))
		{
			for (final String variantCategoryValue : variantCategories)
			{
				final String[] variantCategory = variantCategoryValue.split(Pattern.quote(SimonCoreConstants.PIPE));
				variantCategoryIdAndNameMap.put(variantCategory[0], variantCategory[1]);
				if (variantCategory.length > 2)
				{
					controlType = variantCategory[2];
				}
				variantCategoryIdAndTypeMap.put(variantCategory[0], controlType);
			}
		}
		for (final String variantCategoryId : getOrder(document))
		{
			final String variantValueCategoryId = (String) document.getFieldValue(variantCategoryId + "Id");
			if (StringUtils.isNotEmpty(variantValueCategoryId))
			{
				final VariantValueCategoryData variantValueCategory = new VariantValueCategoryData();
				variantValueCategory.setValueCategoryId(variantValueCategoryId);
				final String valueCategoryName = ((String) document.getFieldValue(variantCategoryId + NAME_TEXT)) != null
						? ((String) document.getFieldValue(variantCategoryId + NAME_TEXT)).toLowerCase()
						: ((String) document.getFieldValue(variantCategoryId + NAME_TEXT));
				if (StringUtils.isNotEmpty(valueCategoryName))
				{
					final String[] variantValueCategorySeq = valueCategoryName.split(Pattern.quote(SimonCoreConstants.PIPE));
					if (null != variantValueCategorySeq && variantValueCategorySeq.length > 0)
					{
						variantValueCategory.setValueCategoryName(variantValueCategorySeq[0]);
						if (variantValueCategorySeq.length > 1)
						{
							try
							{
								variantValueCategory.setValueCategorySequence(Integer.parseInt(variantValueCategorySeq[1]));
							}
							catch (final NumberFormatException e)
							{
								LOG.error("Exception during parsing variantValueCategorySeq : ", e);
							}
						}
					}
				}
				final List<VariantValueCategoryData> variantValueCategoryList = new ArrayList<>();
				variantValueCategoryList.add(variantValueCategory);
				final VariantCategoryData variantCategoryData = new VariantCategoryData();
				variantCategoryData.setTitle(getLocalizedStringForLabel(variantCategoryIdAndNameMap.get(variantCategoryId)));
				variantCategoryData.setSwatches(variantValueCategoryList);
				variantCategoryData.setControlType(variantCategoryIdAndTypeMap.get(variantCategoryId));
				variantCategoryMap.put(variantCategoryId, variantCategoryData);
			}
		}
	}

	/**
	 * Returns localized value for VariantCategoryId.label
	 *
	 * @param variantCategoryId
	 * @return localized Value
	 */
	protected String getLocalizedStringForLabel(final String variantCategoryId)
	{
		return Localization.getLocalizedString(SimonCoreConstants.CATEGORY_LABEL) + variantCategoryId;
	}

	/**
	 * This method fetches indexed field "categoryPath" from solr {@link Document} and returns the max length
	 * {@link List}&lt;{@link String}&gt; which represents the complete hierarchy of VariantCategories
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 * @return ordered list of VariantCategories Name in order of there hierarchy
	 */
	private List<String> getOrder(final Document document)
	{
		List<String> orderedList;
		final List<String> categoryPath = (List<String>) document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH);
		final Optional<String> maxLength = categoryPath.stream().sorted((a, b) -> a.length() > b.length() ? -1 : 1).findFirst();
		if (maxLength.isPresent())
		{
			final String order = maxLength.get();
			final String[] hierarchyList = order.split("/");
			final int orderLength = hierarchyList.length;
			orderedList = Arrays.asList(hierarchyList).subList(SimonCoreConstants.TWO_INT, orderLength);
		}
		else
		{
			orderedList = Collections.emptyList();
		}
		return orderedList;
	}

}
