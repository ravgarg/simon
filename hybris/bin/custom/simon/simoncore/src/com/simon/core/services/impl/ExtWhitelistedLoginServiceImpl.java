package com.simon.core.services.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.annotation.Resource;

import com.simon.core.dao.ExtWhitelistedLoginDao;
import com.simon.core.model.WhitelistedUserModel;
import com.simon.core.services.ExtWhitelistedLoginService;


/**
 * This Service class is used to retrieve betaUser model.
 */
public class ExtWhitelistedLoginServiceImpl implements ExtWhitelistedLoginService
{
	@Resource(name = "extWhitelistedLoginDao")
	private ExtWhitelistedLoginDao extWhitelistedLoginDao;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WhitelistedUserModel findUserByEmailAndPassword(final String email, final String password)
	{
		final WhitelistedUserModel model = extWhitelistedLoginDao.findUserByEmail(email);
		final boolean passwordDisabled = configurationService.getConfiguration().getBoolean("simon.whitelisting.password.disable");
		boolean validPwd = false;
		if (null != model)
		{
			if (!passwordDisabled)
			{
				validPwd = getCurrentTenant().getJaloConnection().getPasswordEncoder(model.getPasswordEncoding())
						.check(model.getEmailId(), model.getPassword(), password);
			}

			if (validPwd || passwordDisabled)
			{
				return model;
			}
		}
		return null;
	}

	/**
	 * @return
	 */
	protected Tenant getCurrentTenant()
	{
		return Registry.getCurrentTenant();
	}

}
