package com.simon.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.daos.impl.DefaultShopDao;
import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.ExtShopDao;


public class ExtShopDaoImpl extends DefaultShopDao implements ExtShopDao
{
	/**
	 * Find list of shop by code string. Returns {@link ShopModel} for given shop <code>id</code>.
	 *
	 * @param shopIds
	 *
	 * @param objects
	 *           the store <code>id</code>
	 * @return the shop model
	 *
	 * @throws IllegalArgumentException
	 *            if the given <code>id</code> is <code>null</code>
	 */
	@Override
	public List<ShopModel> findListOfShopsForCodes(final String[] shopIds)
	{
		validateParameterNotNull(shopIds, "codes must not be null");
		final Map<String, List<Object>> queryParams = new HashMap<>();
		queryParams.put("ids", Arrays.asList(shopIds));
		final SearchResult<ShopModel> result = getFlexibleSearchService().search(SimonFlexiConstants.FIND_ALL_STORES_FOR_CODES,
				queryParams);
		return result.getResult();
	}

	/**
	 * This method return retailers list
	 *
	 * @return
	 */
	@Override
	public List<ShopModel> getRetailerList()
	{
		return find();
	}

}
