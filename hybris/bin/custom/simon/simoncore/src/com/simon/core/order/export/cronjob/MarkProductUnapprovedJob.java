package com.simon.core.order.export.cronjob;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobHistoryModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.services.ExtProductService;
import com.simon.core.services.impl.ErrorLogServiceImpl;


/**
 * Job is use to make the product disabled in staged and online version
 *
 */
public class MarkProductUnapprovedJob extends AbstractJobPerformable<CronJobModel>
{
	@Resource
	private ErrorLogServiceImpl errorLogService;
	@Resource
	private ExtProductService extProductService;

	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final List<CronJobHistoryModel> cronJobHistory = arg0.getCronJobHistoryEntries();
		Date lastEndTime;

		if (cronJobHistory.size() > 1)
		{
			lastEndTime = cronJobHistory.get(cronJobHistory.size() - SimonCoreConstants.TWO_INT).getModifiedtime();
		}
		else
		{
			lastEndTime = cronJobHistory.get(0).getStartTime();
		}
		final List<String> productList = errorLogService.getProductCodes(lastEndTime);
		if (CollectionUtils.isNotEmpty(productList))
		{
			extProductService.makeProductsDisabled(new HashSet<String>(productList));
		}

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
