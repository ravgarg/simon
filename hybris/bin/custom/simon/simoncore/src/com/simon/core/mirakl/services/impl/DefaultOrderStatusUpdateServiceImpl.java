package com.simon.core.mirakl.services.impl;

import static com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State.SHIPPING;
import static com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State.WAITING_ACCEPTANCE;
import static com.mirakl.hybris.core.enums.MarketplaceConsignmentPaymentStatus.SUCCESS;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.returns.model.ReturnRequestModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.order.state.MiraklOrderStatus;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.order.services.TakePaymentService;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.simon.core.mirakl.services.DefaultOrderStatusUpdateService;
import com.simon.core.service.mirakl.MarketPlaceService;


/**
 * This class is used to make a call to accept order on Mirakl and updating status as per consignment and make a call to
 * processPayment method to valid/invalid order Payment. {@link DefaultOrderStatusUpdateService}
 */
public class DefaultOrderStatusUpdateServiceImpl implements DefaultOrderStatusUpdateService
{
	private static final Logger LOG = LoggerFactory.getLogger(DefaultOrderStatusUpdateServiceImpl.class);

	private static final String PENDING_ACCEPTANCE_TO_SHIPPING = "acceptance.order.in.order.process";

	@Resource
	protected ModelService modelService;

	@Resource
	protected MarketplaceConsignmentService marketplaceConsignmentService;

	@Resource
	protected TakePaymentService takePaymentService;

	@Resource
	private MarketPlaceService marketPlaceService;

	@Resource
	private ConfigurationService configurationService;

	protected Populator<MiraklOrder, MarketplaceConsignmentModel> updateConsignmentPopulator;

	@Override
	public void updateOrderConsignmentAndLineItemsStatus(final MarketplaceConsignmentModel consignment)
	{
		final MiraklOrder miraklOrder = marketplaceConsignmentService.loadConsignmentUpdate(consignment);

		if (miraklOrder == null)
		{
			LOG.warn("No stored update was found for consignment {}", consignment.getCode());
			return;
		}

		if (!consignment.isLastUpdateProcessed())
		{
			LOG.info("Processing a received update for consignment {}", consignment.getCode());
			processConsignmentUpdate(consignment, miraklOrder);
			saveConsignment(consignment);
		}

		final MiraklOrderStatus miraklStatus = miraklOrder.getStatus();
		//		Here calling Mirakl API OR21, it will update the Order status from Pending Acceptance to Shipping in progress status
		final boolean pendingToShipping = configurationService.getConfiguration().getBoolean(PENDING_ACCEPTANCE_TO_SHIPPING);
		if (pendingToShipping && WAITING_ACCEPTANCE.equals(miraklStatus.getState()))
		{
			marketPlaceService.acceptOrRejectOrder(consignment, true);
		}

		if (SHIPPING.equals(miraklStatus.getState()) && !(SUCCESS.equals(consignment.getPaymentStatus())))
		{
			marketPlaceService.processPayment(consignment);
			consignment.setPaymentStatus(SUCCESS);
			modelService.save(consignment);
		}
		//Prerequisite whenever an Order has not been debited yet and cannot be marked as received
		if (SHIPPING.equals(miraklStatus.getState()) && SUCCESS.equals(consignment.getPaymentStatus()))
		{
			marketPlaceService.receivedMarketplaceOrder(consignment.getCode());
		}

	}

	protected void processConsignmentUpdate(final MarketplaceConsignmentModel consignment, final MiraklOrder miraklOrder)
	{
		updateConsignmentPopulator.populate(miraklOrder, consignment);
		consignment.setLastUpdateProcessed(true);
	}

	protected void saveConsignment(final MarketplaceConsignmentModel consignment)
	{
		modelService.saveAll(consignment.getConsignmentEntries());
		modelService.save(consignment);
		final List<ReturnRequestModel> returnRequests = ((OrderModel) consignment.getOrder()).getReturnRequests();
		if (CollectionUtils.isNotEmpty(returnRequests))
		{
			modelService.saveAll(returnRequests);
			for (final ReturnRequestModel returnRequest : returnRequests)
			{
				modelService.saveAll(returnRequest.getReturnEntries());
			}
		}
	}

	@Required
	public void setUpdateConsignmentPopulator(final Populator<MiraklOrder, MarketplaceConsignmentModel> updateConsignmentPopulator)
	{
		this.updateConsignmentPopulator = updateConsignmentPopulator;
	}

}
