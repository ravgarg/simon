package com.simon.core.mediaconversion.dao.impl;

import de.hybris.platform.core.PK;
import de.hybris.platform.mediaconversion.constants.GeneratedMediaConversionConstants.Relations;
import de.hybris.platform.mediaconversion.job.DefaultMediaConversionJobDao;
import de.hybris.platform.mediaconversion.model.job.MediaConversionCronJobModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


/**
 * In this class we have changed query we have added version in where clause, so only those media container will be
 * picked for conversion which have version value 0.
 */
public class ExtMediaConversionJobDao extends DefaultMediaConversionJobDao
{
	/**
	 * In this method we have query to add verison clause for media container.
	 */
	@Override
	public Collection<List<PK>> queryFormatsPerContainerToConvert(final MediaConversionCronJobModel cronJob)
	{
		final Map params = queryParams(cronJob);

		final FlexibleSearchQuery query = new FlexibleSearchQuery(
				"SELECT un.container, un.format FROM ( {{SELECT sub.container, sub.format FROM ({{SELECT {a.pk} AS container, {f.pk} AS format, {m.pk} AS media FROM {MediaContainer AS a LEFT JOIN "
						+ Relations.CONVERSIONGROUPTOFORMATREL + " AS tf " + "ON {a." + "conversionGroup" + "} = {tf." + "source" + "} "
						+ "JOIN " + "ConversionMediaFormat" + " AS f " + "ON ({a." + "conversionGroup" + "} IS NULL " + "OR {tf."
						+ "target" + "} = {f." + "pk" + "}) " + "LEFT JOIN " + "Media" + " AS m " + "ON {m." + "mediaContainer"
						+ "} = {a." + "pk" + "} " + "AND {m." + "mediaFormat" + "} = {f." + "pk" + "} " + "} " + "WHERE 1 = 1 "
						+ (params.containsKey("formats") ? "AND {f.pk} IN (?formats) " : "")
						+ (params.containsKey("catVersion") ? "AND {a.catalogVersion} = ?catVersion" : "") + " AND {a.version}=0 "
						+ "}}) sub " + "WHERE sub.media IS NULL " + "}} " + "UNION ALL " + "{{" + outdatedMediaQuery(params) + "}} "
						+ ") un " + "ORDER BY un.container",
				params);
		query.setResultClassList(Arrays.asList(new Class[]
		{ PK.class, PK.class }));
		final SearchResult<List<PK>> searchResult = getFlexibleSearchService().search(query);
		return searchResult.getResult();


	}

	Map<String, Object> queryParams(final MediaConversionCronJobModel cronJob)
	{
		final TreeMap params = new TreeMap();
		if (cronJob.getIncludedFormats() != null && !cronJob.getIncludedFormats().isEmpty())
		{
			params.put("formats", cronJob.getIncludedFormats());
		}
		if (cronJob.getCatalogVersion() != null)
		{

			params.put("catVersion", cronJob.getCatalogVersion());
		}
		return params;
	}

	private String outdatedMediaQuery(final Map<String, Object> params)
	{
		return "SELECT {ua.pk} AS container, {cm.mediaFormat} AS format FROM {MediaContainer AS ua JOIN Media as cm ON {cm.mediaContainer} = {ua.pk} JOIN ConversionMediaFormat as cmf ON {cm.mediaFormat} = {cmf.pk} LEFT JOIN Media as om ON {om.mediaContainer} = {ua.pk} AND {om.pk} = {cm.original} } WHERE {cm.originalDataPK} IS NOT NULL AND ({om.dataPK} IS NULL OR {cm.originalDataPK} <> {om.dataPK}) "
				+ (params.containsKey("formats") ? "AND {cm.mediaFormat} IN (?formats) " : "")
				+ (params.containsKey("catVersion") ? "AND {ua.catalogVersion} = ?catVersion" : "") + " AND {ua.version}=0 ";
	}

}
