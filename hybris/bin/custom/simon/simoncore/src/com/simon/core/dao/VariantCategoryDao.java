package com.simon.core.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.variants.model.VariantCategoryModel;


/**
 * Dao for {@link VariantCategoryModel} contains abstract method for operations on {@link VariantCategoryModel}.
 */
public interface VariantCategoryDao
{

	/**
	 * Find leaf variant category for catalog version. Returns leaf {@link VariantCategoryModel} for given
	 * <code>catalogVersion</code>
	 *
	 * @param catalogVersion
	 *           the catalog version
	 * @return the variant category model, <code>null</code> if there is no leaf variant category
	 * @throws IllegalArgumentException
	 *            if the given <code>catalogVersion</code> is <code>null</code>
	 */
	public VariantCategoryModel findLeafVariantCategory(final CatalogVersionModel catalogVersion);

}
