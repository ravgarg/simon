package com.simon.core.mirakl.services.impl;

import static java.lang.String.format;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Splitter;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.services.impl.DefaultProductImportService;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;


/**
 * ExtProductImportServiceImpl extends {@link DefaultProductImportService} and overrides parent implementations to add
 * more operations.
 */
public class ExtProductImportServiceImpl extends DefaultProductImportService
{

	private static final String VARIANT_ATTRIBUTES = "variantAttributes";
	@Autowired
	SessionService sessionService;
	@Resource
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;
	@Resource
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;
	private static final int VALID_DELIMIS = 2;

	private static final Logger LOGGER = Logger.getLogger(ExtProductImportServiceImpl.class);

	@Override
	protected void writeErrorToResultQueue(final ProductImportException exception, final ProductImportFileContextData context)
	{
		final String errorString = format("Import Id : [%s],  Shop Id : [%s] , Line value: [%s], Line number: [%s]",
				context.getMiraklImportId(), context.getShopId(), exception.getRawProduct().getValues(),
				exception.getRawProduct().getRowNumber());
		context.getProductFailure().add(errorString);
		LOGGER.error(errorString);
		super.writeErrorToResultQueue(exception, context);
	}

	@Override
	public void importProducts(final Collection<MiraklRawProductModel> variants, final ProductImportFileContextData context)
	{
		validate(variants, context);
		final Map<String, PK> coreAttributes = context.getGlobalContext().getCoreAttributes();
		final String delimiters = ((MiraklCoreAttributeModel) modelService.get(coreAttributes.get(VARIANT_ATTRIBUTES)))
				.getTypeParameter();

		for (final MiraklRawProductModel variant : variants)
		{
			try
			{
				if (alreadyReceived(variant, context))
				{
					writeSuccessToResultQueue(variant, getAlreadyReceivedMessage(context), context);
					continue;
				}
				validateProduct(variant, context);
				final ProductImportData data = productImportDataConverter.convert(Pair.of(variant, context));
				identifyProduct(data);


				ProductModel productToUpdate = data.getIdentifiedProduct();

				//If new sku in feed and variant category in sku are diffrent then raw product
				if (productToUpdate == null && checkRawProductHaveDiffrentVariantCategory(data, variant, delimiters))
				{
					logAndRejectCurrentSku(context, variant);
				}
				//If new sku in feed and have same variant category as it's base product have.
				else if (productToUpdate == null)
				{
					productToUpdate = createProduct(data, context);
					populateProductImportData(context, variant, data, productToUpdate);
				}
				//If a existing sku come in feed for update and have different variant category or variant value category as existing sku.
				else if (productToUpdate != null
						&& checkRawProductHaveDiffrentVariantCategoryAndValue(productToUpdate, variant, delimiters))
				{
					logAndRejectCurrentSku(context, variant);
				}
				//if a existing sku come in feed for update and have same variant category and variant value category as existing sku.
				else
				{
					populateProductImportData(context, variant, data, productToUpdate);

				}
			}
			catch (final ProductImportException e)
			{
				LOGGER.error(format("An error occurred during product import. Raw line: [%s]", variant.getValues()), e);
				writeErrorToResultQueue(e, context);
			}
			catch (final Exception e)
			{
				LOGGER.error(format("Unable to import product. Raw line: [%s]", variant.getValues()), e);
				writeErrorToResultQueue(new ProductImportException(variant, e), context);
			}
		}

	}

	/**
	 * This method populate few values in product import data.
	 *
	 * @param context
	 * @param variant
	 * @param data
	 * @param productToUpdate
	 * @throws ProductImportException
	 */
	private void populateProductImportData(final ProductImportFileContextData context, final MiraklRawProductModel variant,
			final ProductImportData data, final ProductModel productToUpdate) throws ProductImportException
	{
		data.setProductToUpdate(productToUpdate);
		data.setRootBaseProductToUpdate(getRootBaseProduct(productToUpdate));
		applyReceivedValues(data, context);
		postProcessProductLineImport(data, variant, context);
		modelService.saveAll(data.getModelsToSave());
		writeSuccessToResultQueue(variant, context);
	}

	/**
	 * This method log and insert corrupted sku in rejected queue.
	 *
	 * @param context
	 * @param variant
	 */
	private void logAndRejectCurrentSku(final ProductImportFileContextData context, final MiraklRawProductModel variant)
	{
		LOGGER.error(format("Unable to import product:  [%s], due to of  variant category mismatch. Raw line: [%s]",
				variant.getSku(), variant.getValues()));
		writeErrorToResultQueue(new ProductImportException(variant,
				format(
						"Unable to import product : [%s].,Change in variant category and variant value category not allowed.  Raw line: [%s]",
						variant.getSku(), variant.getValues())),
				context);
		modelService.remove(variant);
	}

	/**
	 * This method check raw product have same variant category and variant value category as existing productModel.
	 *
	 * @param productModel
	 * @param miraklRawProductModel
	 * @return
	 */
	private boolean checkRawProductHaveDiffrentVariantCategoryAndValue(final ProductModel productModel,
			final MiraklRawProductModel miraklRawProductModel, final String delimiters)
	{
		final List<String> variantCategoryCodeListOfProduct = new ArrayList<>();
		final List<String> variantCategoryCodeListOfRawProduct = new ArrayList<>();

		final String variantAttributeValue = miraklRawProductModel.getValues().get(VARIANT_ATTRIBUTES);
		if (StringUtils.isNotEmpty(variantAttributeValue))
		{
			final Map<String, String> variantAttributeRawMap = getVariantAttributesRawMap(variantAttributeValue, delimiters);
			if (MapUtils.isNotEmpty(variantAttributeRawMap))
			{
				for (final Entry<String, String> variantAttributeRawEntry : variantAttributeRawMap.entrySet())
				{
					final String variantCategoryCode = (String) variantCategoryKeyGenerator
							.generateFor(variantAttributeRawEntry.getKey());
					final String variantValueCategoryCode = (String) variantValueCategoryKeyGenerator
							.generateFor(variantAttributeRawEntry.getValue()) + "-" + variantCategoryCode;
					variantCategoryCodeListOfRawProduct.add(variantCategoryCode);
					variantCategoryCodeListOfRawProduct.add(variantValueCategoryCode);
				}
			}
		}
		if (productModel instanceof GenericVariantProductModel)
		{
			final GenericVariantProductModel genericVariantProductModel = (GenericVariantProductModel) productModel;
			variantCategoryCodeListOfProduct.addAll(genericVariantProductModel.getSupercategories().stream()
					.filter(category -> category instanceof VariantValueCategoryModel).map(category -> category.getCode())
					.collect(Collectors.toList()));
			variantCategoryCodeListOfProduct.addAll(genericVariantProductModel.getBaseProduct().getSupercategories().stream()
					.filter(category -> category instanceof VariantCategoryModel).map(category -> category.getCode())
					.collect(Collectors.toList()));
			return (!CollectionUtils.isEqualCollection(variantCategoryCodeListOfProduct, variantCategoryCodeListOfRawProduct));
		}
		return false;

	}

	/**
	 * This method check raw product have same variant category as base product have.
	 *
	 * @param productModel
	 * @param miraklRawProductModel
	 * @return
	 */
	private boolean checkRawProductHaveDiffrentVariantCategory(final ProductImportData data,
			final MiraklRawProductModel miraklRawProductModel, final String delimiters)
	{
		final List<String> variantCategoryCodeListOfProduct = new ArrayList<>();
		final List<String> variantCategoryCodeListOfRawProduct = new ArrayList<>();

		final String variantAttributeValue = miraklRawProductModel.getValues().get(VARIANT_ATTRIBUTES);
		if (StringUtils.isNotEmpty(variantAttributeValue))
		{
			final Map<String, String> variantAttributeRawMap = getVariantAttributesRawMap(variantAttributeValue, delimiters);
			if (MapUtils.isNotEmpty(variantAttributeRawMap))
			{
				for (final Entry<String, String> variantAttributeRawEntry : variantAttributeRawMap.entrySet())
				{
					final String variantCategoryCode = (String) variantCategoryKeyGenerator
							.generateFor(variantAttributeRawEntry.getKey());
					variantCategoryCodeListOfRawProduct.add(variantCategoryCode);
				}
			}
		}
		final ProductModel baseProductModel = data.getProductResolvedByShopVariantGroup();
		if (baseProductModel != null)
		{
			variantCategoryCodeListOfProduct
					.addAll(baseProductModel.getSupercategories().stream().filter(category -> category instanceof VariantCategoryModel)
							.map(category -> category.getCode()).collect(Collectors.toList()));
			return (!CollectionUtils.isEqualCollection(variantCategoryCodeListOfProduct, variantCategoryCodeListOfRawProduct));
		}
		return false;
	}

	private Map<String, String> getVariantAttributesRawMap(final String variantRawText, final String delimiters)
	{
		Map<String, String> variantAttributesRawMap = null;
		if (StringUtils.isNotEmpty(variantRawText) && StringUtils.isNotEmpty(delimiters) && delimiters.length() == VALID_DELIMIS)
		{
			final char[] delims = delimiters.toCharArray();
			variantAttributesRawMap = Splitter.on(delims[0]).withKeyValueSeparator(delims[1]).split(variantRawText);
		}
		return variantAttributesRawMap;
	}


}
