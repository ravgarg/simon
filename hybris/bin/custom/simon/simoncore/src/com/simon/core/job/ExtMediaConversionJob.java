package com.simon.core.job;

import de.hybris.platform.mediaconversion.job.MediaConversionJob;


/**
 * ExtMediaConversionJob extends {@link MediaConversionJob} and makes it abortable by overriding {@link #isAbortable()}
 */
public class ExtMediaConversionJob extends MediaConversionJob
{
	@Override
	public boolean isAbortable()
	{
		return true;
	}
}
