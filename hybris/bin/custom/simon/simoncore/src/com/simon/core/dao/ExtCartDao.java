package com.simon.core.dao;

import de.hybris.platform.core.model.order.CartModel;


/**
 * This DAO interface is used to retrieve cart model using cart ID
 */
public interface ExtCartDao
{
	/**
	 * This method is used to retrieve cart model using cart ID
	 *
	 * @param cartId
	 * @return CartModel
	 */
	CartModel getCartByCartId(String cartId);
}
