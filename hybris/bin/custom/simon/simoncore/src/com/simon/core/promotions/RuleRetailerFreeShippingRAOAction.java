package com.simon.core.promotions;

import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleActionContext;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rule.evaluation.RuleActionContext;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.RAOAction;
import de.hybris.platform.ruleengineservices.rule.evaluation.actions.impl.RuleChangeDeliveryModeRAOAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import com.simon.promotion.rao.RetailerRAO;


/**
 * On successful evaluation of a condition it's respective action is called. This RAOAction is called when Retailer free
 * shipping is applicable. This has been extended from {@link RuleChangeDeliveryModeRAOAction} in order to add retailer
 * specific information to rule action rao. performAction of {@link RuleRetailerFreeShippingRAOAction} is called when
 * condition is evaluated as true. performAction calls setRAOMetaData which has been overridden here in order to add
 * retailer information
 */
public class RuleRetailerFreeShippingRAOAction extends RuleChangeDeliveryModeRAOAction implements RAOAction
{
	/**
	 * Fetches the retailer for which the condition has been evaluated from rule context and Sets its information in
	 * {@link DiscoutRAO}}
	 *
	 * @param context
	 *           the context
	 * @param raos
	 *           the raos
	 */
	@Override
	public void setRAOMetaData(final RuleActionContext context, final AbstractRuleActionRAO... raos)
	{
		super.setRAOMetaData(context, raos);
		if (Objects.nonNull(raos))
		{
			Stream.of(raos).filter(Objects::nonNull).forEach(rao -> {
				setRetailer(context, rao);
			});
		}
	}

	private void setRetailer(final RuleActionContext context, final AbstractRuleActionRAO ruleRao)
	{
		RetailerRAO retailerRao = null;
		if (context instanceof DefaultDroolsRuleActionContext)
		{
			final DefaultDroolsRuleActionContext droolContext = (DefaultDroolsRuleActionContext) context;
			final Object droolVariable = droolContext.getVariables().get(RetailerRAO.class.getName());
			if (null != droolVariable)
			{
				final Set<RetailerRAO> evaluatedRetailer = Collections.unmodifiableSet((Set<RetailerRAO>) droolVariable);
				final List<RetailerRAO> evaluatedRetailerList = new ArrayList<>(evaluatedRetailer);
				retailerRao = evaluatedRetailerList.get(0);
				ruleRao.setRetailer(retailerRao);
			}
		}
	}
}
