package com.simon.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.PriceType;
import com.simon.core.services.ExtProductService;


/**
 * Cronjob to Move product from Check to approve if Product is having price with PriceType list .
 */
public class ProductCheckToApprovedCronJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductCheckToApprovedCronJob.class);

	@Autowired
	private ExtProductService extProductService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	/**
	 * In this method we are getting all GenericVariantProductModel having a price Row with price type list and Product
	 * approval status is check and changing all product status to Approved as All product is having all mandatory
	 * Attribute.
	 */
	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);
		final List<GenericVariantProductModel> variants = extProductService.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogVersion);

		final Consumer<ProductModel> consumer = product -> {
			product.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			LOGGER.debug("Product/VariantProduct {} Status updated to Approved", product.getCode());
		};

		final Set<ProductModel> baseProducts = variants.stream()
				.filter(product -> !product.getBaseProduct().getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))
				.map(GenericVariantProductModel::getBaseProduct).collect(Collectors.toSet());

		LOGGER.info("{} Variant Products' Status updated to APPROVED", variants.size());
		LOGGER.info("{} Base Products' Status updated to APPROVED", baseProducts.size());
		variants.forEach(consumer);
		baseProducts.forEach(consumer);
		try
		{
			modelService.saveAll(baseProducts);
			modelService.saveAll(variants);
			return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
		}
		catch (final ModelSavingException ex)
		{
			LOGGER.error("Exception while upadting product status to APPROVED", ex);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
	}
}
