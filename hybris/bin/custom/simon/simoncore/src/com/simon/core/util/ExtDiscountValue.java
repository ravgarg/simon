package com.simon.core.util;

import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;


/**
 * This class has been extended in order to a
 */
public class ExtDiscountValue extends DiscountValue
{
	private static final long serialVersionUID = 1;

	private static final String COLLECTION_END = "]";
	private static final String COLLECTION_OBJECT_SEPRATOR = "|";
	private static final String COLLECTION_START = "[";
	private static final String EMPTY_COLLECTION_STR = "[]";
	private static final String VD_END = ">VD>";
	private static final String NULL_STR = "NULL";
	private static final String DV_START = "<DV<";
	private static final String SEPERATOR = "#";
	private String retailer;

	/**
	 * @param code
	 * @param value
	 * @param absolute
	 * @param currencyIsoCode
	 * @param retailer
	 */
	public ExtDiscountValue(final String code, final double value, final boolean absolute, final String currencyIsoCode,
			final String retailer)
	{
		super(code, value, absolute, 0.0D, currencyIsoCode);
		this.retailer = retailer;
	}

	/**
	 * @param code
	 * @param value
	 * @param absolute
	 * @param appliedValue
	 * @param isoCode
	 * @param asTargetPrice
	 * @param retailer
	 */
	public ExtDiscountValue(final String code, final double value, final boolean absolute, final double appliedValue,
			final String isoCode, final boolean asTargetPrice, final String retailer)
	{
		super(code, value, absolute, appliedValue, isoCode, asTargetPrice);
		this.retailer = retailer;
	}



	/**
	 * @param discountValue
	 * @param retailer
	 */
	public ExtDiscountValue(final DiscountValue discountValue, final String retailer)
	{
		super(discountValue.getCode(), discountValue.getValue(), discountValue.isAbsolute(), discountValue.getAppliedValue(),
				discountValue.getCurrencyIsoCode(), discountValue.isAsTargetPrice());
		this.retailer = retailer;
	}

	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder(DV_START);
		sb.append(this.getCode());
		sb.append(SEPERATOR).append(this.getValue());
		sb.append(SEPERATOR).append(this.isAbsolute());
		sb.append(SEPERATOR).append(this.getAppliedValue());
		sb.append(SEPERATOR).append(this.getCurrencyIsoCode() != null ? this.getCurrencyIsoCode() : NULL_STR);
		sb.append(SEPERATOR).append(this.isAsTargetPrice());
		sb.append(SEPERATOR).append(this.getRetailer());
		sb.append(VD_END);
		return sb.toString();
	}

	/**
	 * @param quantity
	 * @param startPrice
	 * @param digits
	 * @param discounts
	 * @param currencyIsoCode
	 * @return list of retailer discount
	 */
	public static List<DiscountValue> applyRetailer(final double quantity, final double startPrice, final int digits,
			final List<DiscountValue> discounts, final String currencyIsoCode)
	{
		final ArrayList<DiscountValue> ret = new ArrayList<>(discounts.size());
		double tmp = startPrice;
		for (final DiscountValue discount : discounts)
		{
			if (discount instanceof ExtDiscountValue)
			{
				final ExtDiscountValue retailerDiscount = (ExtDiscountValue) discount;
				final ExtDiscountValue discountValue = retailerDiscount.apply(quantity, tmp, digits, currencyIsoCode,
						retailerDiscount.getRetailer());
				tmp -= discountValue.getAppliedValue();
				ret.add(discountValue);
			}
		}
		return ret;
	}

	/**
	 * @param quantity
	 * @param price
	 * @param digits
	 * @param currencyIsoCode
	 * @param ret
	 * @return retailer discount
	 */
	public ExtDiscountValue apply(final double quantity, final double price, final int digits, final String currencyIsoCode,
			final String ret)
	{
		final DiscountValue discountValue = super.apply(quantity, price, digits, currencyIsoCode);
		return new ExtDiscountValue(discountValue, ret);
	}

	/**
	 * @return retailer
	 */
	public String getRetailer()
	{
		return retailer;
	}

	/**
	 * @param retailer
	 */
	public void setRetailer(final String retailer)
	{
		this.retailer = retailer;
	}


	public static String toString(final Collection discountValueCollection)
	{
		if (discountValueCollection == null || discountValueCollection.isEmpty())
		{
			return EMPTY_COLLECTION_STR;
		}
		else
		{
			final StringBuilder stringBuilder = new StringBuilder(COLLECTION_START);
			final Iterator<ExtDiscountValue> iterator = discountValueCollection.iterator();

			while (iterator.hasNext())
			{
				stringBuilder.append(iterator.next().toString());
				if (iterator.hasNext())
				{
					stringBuilder.append(COLLECTION_OBJECT_SEPRATOR);
				}
			}

			stringBuilder.append(COLLECTION_END);
			return stringBuilder.toString();
		}
	}

	public static Collection<ExtDiscountValue> parseDiscountValueCollection(final String str)
	{
		if (str == null || str.equals(EMPTY_COLLECTION_STR))
		{
			return Collections.emptyList();
		}
		else
		{
			final LinkedList<ExtDiscountValue> ret = new LinkedList<>();
			final StringTokenizer st = new StringTokenizer(str.substring(1, str.length() - 1), COLLECTION_OBJECT_SEPRATOR);

			while (st.hasMoreTokens())
			{
				ret.add(parseDiscountValue(st.nextToken()));
			}

			return ret;
		}
	}

	public static ExtDiscountValue parseDiscountValue(final String str)
	{
		try
		{
			final int e = str.indexOf(DV_START);
			final int end = str.indexOf(VD_END, e);

			if (e < 0 || end < 0)
			{
				throw new IllegalArgumentException("could not find <DV< or >VD> in discount value string \'" + str + "\'");
			}
			int pos = 0;
			String code = null;
			double value = 0.0D;
			boolean absolute = false;
			boolean asTargetPrice = false;
			double appliedValue = 0.0D;
			String retailer = null;
			String iso = null;
			final String substr = str.substring(e + DV_START.length(), end);
			if (substr.startsWith(SEPERATOR))
			{
				pos = 1;
				code = "";
			}

			for (final StringTokenizer st = new StringTokenizer(substr, SEPERATOR); st.hasMoreTokens(); ++pos)
			{
				final String token = st.nextToken();
				switch (pos)
				{
					case 0:
						code = token;
						break;
					case 1:
						value = Double.parseDouble(token);
						break;
					case 2:
						absolute = Boolean.parseBoolean(token);
						break;
					case 3:
						appliedValue = Double.parseDouble(token);
						break;
					case 4:
						iso = token;
						break;
					case 5:
						asTargetPrice = Boolean.parseBoolean(token);
						break;
					case 6:
						retailer = token;
						break;
					default:
						throw new IllegalArgumentException("illegal discount value string \'" + str + "\' (pos=" + pos + ", moreTokens="
								+ st.hasMoreTokens() + ", nextToken=\'" + token + "\')");
				}
			}

			if (pos < 3)
			{
				throw new IllegalArgumentException("illegal discount value string \'" + str + "\' (pos=" + pos + ")");
			}
			else
			{
				return new ExtDiscountValue(code, value, absolute, appliedValue, NULL_STR.equals(iso) ? null : iso, asTargetPrice,
						retailer);
			}
		}
		catch (final Exception exception)
		{
			throw new IllegalArgumentException("error parsing discount value string \'" + str + "\' : " + exception);
		}
	}
}
