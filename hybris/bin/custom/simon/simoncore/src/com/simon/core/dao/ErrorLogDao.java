package com.simon.core.dao;

import java.util.Date;
import java.util.List;


/**
 * Use to fetch information from ErrorLog
 */
public interface ErrorLogDao
{

	/**
	 * Returns product codes
	 *
	 * @param lastEndTime
	 *
	 * @return {@link List} of {@link String} - matched products
	 */
	List<String> getProductCodes(final Date lastEndTime);
}
