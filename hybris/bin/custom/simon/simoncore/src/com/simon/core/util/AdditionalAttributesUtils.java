package com.simon.core.util;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.jalo.MerchandizingCategory;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.model.MiraklCategoryModel;


/**
 * AdditionalAttributesUtils returns Set of matching Merchandizing Categories as per the additional attributes
 * {@link GenericVariantProductModel#getAdditionalAttributes()} present on <code>GenericVariantProduct</code>.
 */
public class AdditionalAttributesUtils
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AdditionalAttributesUtils.class);

	/**
	 * Private Constructor To Enforce Static Invocation
	 */
	private AdditionalAttributesUtils()
	{
		//Private Constructor To Enforce Static Invocation
	}

	/**
	 * Gets the matching merchandizing categories. Returns Set of matching Merchandizing Categories as per the additional
	 * attributes {@link GenericVariantProductModel#getAdditionalAttributes()} present on
	 * <code>GenericVariantProduct</code>.<br>
	 * <p>
	 * To find matching categories first Mirakl Category associated {@link ProductModel#getMiraklCategory()}with given
	 * Product is fetched. <br>
	 * Each Mirakl Category can be associated with multiple Merchandizing Categories that have additional attributes
	 * {@link MerchandizingCategory#getAllAdditionalAttributes()}. <br>
	 * Product's additional attribute {@link GenericVariantProductModel#getAdditionalAttributes()} is matched with
	 * additionalAttributes of all the associated merchandizing categories and match is determined if Product's
	 * additionalAttribute is a submap of category's additonalAttribute
	 *
	 * @param product
	 *           the product
	 * @return the matching merchandizing categories
	 */
	@SuppressWarnings("unchecked")
	public static Set<MerchandizingCategoryModel> getMatchingMerchandizingCategories(final GenericVariantProductModel product)
	{

		final Map<String, String> productAdditionalAttributes = product.getAdditionalAttributes();

		final MiraklCategoryModel miraklCategoryModel = (MiraklCategoryModel) product.getMiraklCategory();

		final Collection<MerchandizingCategoryModel> merchandizingCategories = miraklCategoryModel.getMerchandizingCategories();

		LOGGER.debug("Product : {} With Attributes : {} Fetching Matching Categories", product.getCode(),
				product.getAdditionalAttributes());

		LOGGER.debug("Product : {} Merchandizing Categories : {} Found For Mirakl Category : {}", product.getCode(),
				merchandizingCategories.size(), miraklCategoryModel.getCode());

		final Map<MerchandizingCategoryModel, Map<String, List<String>>> catAttributesMap = new HashMap<>();

		for (final MerchandizingCategoryModel merchandizingCategory : merchandizingCategories)
		{
			LOGGER.debug("Product : {} Comparing Against Category : {} Having Additional Attributes : {}", product.getCode(),
					merchandizingCategory.getCode(), merchandizingCategory.getAdditionalAttributes());
			catAttributesMap.put(merchandizingCategory, merchandizingCategory.getAdditionalAttributes());
		}

		final Map<Object, Set<Object>> invertedCatAttributesMap = catAttributesMap.entrySet().stream()
				.collect(Collectors.groupingBy(Entry::getValue, Collectors.mapping(Entry::getKey, Collectors.toSet())));

		final Set<MerchandizingCategoryModel> matchedMerchandizingCategories = new HashSet<>();

		for (final Entry<Object, Set<Object>> invertedCatAttributesMapEntry : invertedCatAttributesMap.entrySet())
		{
			if (containsSubMap((Map<String, List<String>>) invertedCatAttributesMapEntry.getKey(), productAdditionalAttributes))
			{
				invertedCatAttributesMapEntry.getValue()
						.forEach(p -> matchedMerchandizingCategories.add((MerchandizingCategoryModel) p));
			}
		}

		if (CollectionUtils.isNotEmpty(matchedMerchandizingCategories))
		{
			matchedMerchandizingCategories
					.forEach(cat -> LOGGER.info("Mapped Merchandizing Category : {} To Variant Product's : {} Base Product : {}",
							cat.getCode(), product.getCode(), product.getBaseProduct().getCode()));
		}
		return matchedMerchandizingCategories;
	}

	/**
	 * Contains sub map. Returns true if <code>map</code> contains key-value pairs present in <code>subMap</code>
	 *
	 * @param map
	 *           the map
	 * @param subMap
	 *           the sub map
	 * @return true, if successful
	 */
	private static boolean containsSubMap(final Map<String, List<String>> map, final Map<String, String> subMap)
	{
		if (CollectionUtils.isEqualCollection(map.keySet(), subMap.keySet()))
		{
			for (final Entry<String, String> subMapEntry : subMap.entrySet())
			{
				if (!map.get(subMapEntry.getKey()).contains(subMapEntry.getValue()))
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
}
