package com.simon.core.promotions;

import de.hybris.platform.ruledefinitions.conditions.AbstractRuleConditionTranslator;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrAttributeRelConditionBuilder;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rao.DeliveryModeRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.util.ArrayList;
import java.util.Map;

import com.google.common.collect.Lists;
import com.simon.promotion.rao.RetailerRAO;


/**
 * Translators are called while publishing a promotion. These are used to generate IR(intermedial rules). This class
 * enables generation of IR code for retailers specific cart.
 */
public class ExtRuleDeliveryModeConditionTranslator extends AbstractRuleConditionTranslator
{

	private static final String RETAILER_DELIVERY_MODE_ATTRIBUTE = "ret_delivery_mode";

	/**
	 * Translates retailer sub bag total condition in intermediate rule that can be used in rule engine.
	 *
	 * @param context
	 *           the context
	 * @param condition
	 *           the condition
	 * @param conditionDefinition
	 *           the condition definition
	 * @return the rule ir condition
	 */
	@Override
	public RuleIrCondition translate(final RuleCompilerContext context, final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition)
	{
		final Map<String, RuleParameterData> conditionParams = condition.getParameters();
		final RuleParameterData retailerParam = conditionParams.get(RETAILER_DELIVERY_MODE_ATTRIBUTE);
		if (verifyAllPresent(retailerParam))
		{
			final String deliveryMode = retailerParam.getValue();
			if (verifyAllPresent(deliveryMode))
			{
				return getConditions(context, deliveryMode);
			}
		}
		return IrConditions.newIrRuleFalseCondition();
	}

	/**
	 * Gets the delivery mode conditions.
	 *
	 * @param context
	 *           the context
	 * @return the delivery mode conditions
	 *
	 */
	private RuleIrCondition getConditions(final RuleCompilerContext context, final String deliveryMode)
	{
		final RuleIrGroupCondition irCartTotalCondition = RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND)
				.build();
		addRetailerTotalConditions(context, irCartTotalCondition, deliveryMode);
		return irCartTotalCondition;
	}


	/**
	 * Adds the delivery mode conditions.
	 *
	 * @param context
	 *           the context
	 * @param irDeliveryModeCondition
	 *           the ir delivery mode condition
	 * @param string
	 *           the deliveryMode
	 */
	private void addRetailerTotalConditions(final RuleCompilerContext context, final RuleIrGroupCondition irDeliveryModeCondition,
			final String deliveryMode)
	{
		final String deliveryRaoVariable = context.generateVariable(DeliveryModeRAO.class);
		final String retailerRaoVariable = context.generateVariable(RetailerRAO.class);

		final ArrayList irConditions = Lists.newArrayList();

		irConditions.add(RuleIrAttributeConditionBuilder.newAttributeConditionFor(deliveryRaoVariable).withAttribute("code")
				.withOperator(RuleIrAttributeOperator.EQUAL).withValue(deliveryMode).build());

		irConditions.add(
				RuleIrAttributeRelConditionBuilder.newAttributeRelationConditionFor(retailerRaoVariable).withAttribute("deliveryMode")
						.withOperator(RuleIrAttributeOperator.EQUAL).withTargetVariable(deliveryRaoVariable).build());

		irDeliveryModeCondition.getChildren().addAll(irConditions);

	}
}
