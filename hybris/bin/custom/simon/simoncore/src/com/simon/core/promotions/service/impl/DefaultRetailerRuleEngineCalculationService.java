package com.simon.core.promotions.service.impl;

import de.hybris.order.calculation.domain.Order;
import de.hybris.order.calculation.domain.OrderDiscount;
import de.hybris.platform.ruleengineservices.calculation.impl.DefaultRuleEngineCalculationService;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.math.BigDecimal;

import com.simon.core.promotions.service.RetailerRuleEngineCalculationService;
import com.simon.promotion.rao.RetailerRAO;



/**
 * This class defines methods which are needed by Rule Engine for recalculations for Retailer sub-bag
 */
public class DefaultRetailerRuleEngineCalculationService extends DefaultRuleEngineCalculationService
		implements RetailerRuleEngineCalculationService
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public DiscountRAO addOrderLevelDiscount(final RetailerRAO retailerRao, final boolean absolute, final BigDecimal amount)
	{
		ServicesUtil.validateParameterNotNull(retailerRao, "retailer rao must not be null");
		ServicesUtil.validateParameterNotNull(amount, "amount must not be null");
		final Order order = getAbstractOrderRaoToOrderConverter().convert(retailerRao);
		final OrderDiscount discount = createOrderDiscount(order, absolute, amount);
		final DiscountRAO discountRAO = createDiscountRAO(discount);
		getRaoUtils().addAction(retailerRao, discountRAO);
		this.recalculateTotals(retailerRao, order);
		return discountRAO;
	}
}
