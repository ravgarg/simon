package com.simon.core.services;

import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;


/**
 * VariantValueCategoryService service for {@link VariantValueCategoryModel}. Contains abstract methods for operations
 * on {@link VariantValueCategoryService}
 */
public interface VariantValueCategoryService
{

	/**
	 * Gets the top sequence variant value category. Returns variant value category having top sequence under given
	 * <code>variantCategory</code>
	 *
	 * @param variantCategory
	 *           the <code>variantCategory</code>
	 * @return the top sequence {@link VariantValueCategoryModel}
	 */
	VariantValueCategoryModel getTopSequenceVariantValueCategory(VariantCategoryModel variantCategory);
}
