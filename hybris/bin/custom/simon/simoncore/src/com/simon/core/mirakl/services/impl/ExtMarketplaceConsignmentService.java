package com.simon.core.mirakl.services.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.stock.StockService;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.impl.DefaultMarketplaceConsignmentService;
import com.simon.core.mirakl.services.DefaultOrderStatusUpdateService;


/**
 * ExtMarketplaceConsignmentService extends {@link DefaultMarketplaceConsignmentService} and adds additional feature of
 * received update and stop triggers for the consignment process (if any) instead of calling service Method.
 */
public class ExtMarketplaceConsignmentService extends DefaultMarketplaceConsignmentService
{
	private static final Logger LOG = LoggerFactory.getLogger(ExtMarketplaceConsignmentService.class);

	@Resource
	private DefaultOrderStatusUpdateService orderStatusUpdateService;
	@Resource
	private StockService stockService;

	@Override
	public MarketplaceConsignmentModel receiveConsignmentUpdate(final MiraklOrder miraklOrder)
	{
		final MarketplaceConsignmentModel updatedConsignment = storeConsignmentUpdate(miraklOrder);
		orderStatusUpdateService.updateOrderConsignmentAndLineItemsStatus(updatedConsignment);
		return updatedConsignment;
	}

	/**
	 * Returns a set of created {@link MarketplaceConsignmentModel}s from Mirakl's response. This also handles the stock
	 * level of the product and reduces the reserved amount by the entry quantity.
	 *
	 * @param order
	 *           order with marketplace entries
	 * @param miraklOrders
	 *           {@link MiraklCreatedOrders}
	 * @return set of {@link MarketplaceConsignmentModel}
	 */
	@Override
	public Set<MarketplaceConsignmentModel> createMarketplaceConsignments(final OrderModel order,
			final MiraklCreatedOrders miraklOrders)
	{
		validateParameterNotNullStandardMessage("order", order);
		validateParameterNotNullStandardMessage("miraklOrders", miraklOrders);
		final Set<MarketplaceConsignmentModel> consignments = new HashSet<>();
		for (final MiraklOrder miraklOrder : miraklOrders.getOrders())
		{
			LOG.debug("Creating {} marketplace consignments for order {}", miraklOrders.getOrders().size(), order.getCode());
			consignments.add(miraklCreateConsignmentConverter.convert(Pair.of(order, miraklOrder)));
		}
		order.setConsignments(new HashSet<ConsignmentModel>(consignments));
		modelService.saveAll(consignments);

		consignments.stream()
				.forEach(marketplaceConsignment -> marketplaceConsignment.getConsignmentEntries().stream().forEach(consignmentEntry ->
				{
					final Set<StockLevelModel> stockLevels = new HashSet<>();
					stockLevels.addAll(stockService.getAllStockLevels(consignmentEntry.getOrderEntry().getProduct()));
					stockLevels.stream().forEach(
							stockLevel -> stockLevel.setReserved(stockLevel.getReserved() - consignmentEntry.getQuantity().intValue()));
					modelService.saveAll(stockLevels);
				}));

		return consignments;
	}
}
