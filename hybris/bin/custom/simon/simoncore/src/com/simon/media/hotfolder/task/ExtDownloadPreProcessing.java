package com.simon.media.hotfolder.task;


import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.HeaderInitTask;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.opencsv.CSVReader;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.file.wrapper.FileWriterWrapper;



/**
 * ExtDownloadPreProcessing adds additional logic of downloading images present as urls in Header file if header files
 * is of image media file. After downloading images this creates an output file that is fed to hot folder for importing
 * downloaded images into hybris
 */
public class ExtDownloadPreProcessing extends HeaderInitTask
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtDownloadPreProcessing.class);

	@Autowired
	private FileWriterWrapper fileWriterWrapper;

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private I18NService i18nService;

	private static final String MEDIA_ERROR_TEXT = "simon.media.feed.error.summary.mapping.text";

	private static final String CSV_SEPERATOR = "simon.media.feed.error.summary.csv.seperator";

	private static final String DEFAULT_SEPERATOR = "|";

	private static final String FILENAME_SEPERATOR = "product.image.filename.seperator";

	private static final String ERROR_FOLDER = "simon.media.feed.error.log";

	private static final String PREFIX_FILENAME_SUMMARY = "simon.media.feed.error.summary.prefix";

	private static final String DEFAULT_FILENAME_SEPERATOR = "-";

	private static final String EXTENSION_FILE = ".csv";

	private static final String EXTENSION_TEMP_FILE = ".temp";

	private static final String REGEX_DATE = "yyyyMMddHHmm";

	private String fileName;

	private String outputCsvPath;

	private String imageDir;

	private String fileExt;

	private String filePrefix;

	private String primaryImageGroup;


	/**
	 * Executes the super method for generic batch header creation. Additionally for files(identified by file name) that
	 * contains image urls are read and each image is downloaded and stored on disk. An output file containing products and
	 * their downloaded images. If any image is failed in downloading it is logged in media error file. <br>
	 *
	 * Images are downloaded in multiple threads using {@link ExecutorService}
	 *
	 * {@inheritDoc}
	 */
	@Override
	public BatchHeader execute(final BatchHeader header)
	{
		super.execute(header);
		if (!header.getFile().getName().startsWith(getFileName()))
		{
			LOGGER.info("Incorrect File Name Received : {} Ignoring. Should Start With : {}", header.getFile().getName(),
					getFileName());
			return header;
		}
		final Instant start = Instant.now();
		try (final CSVReader reader = getCSVReader(header.getFile(), SimonCoreConstants.UTF8))
		{
			LOGGER.info("Starting download process from File : {}", header.getFile().getName());
			final Pair<List<String>, List<String[]>> pairOfReport = downloadAndRetryFailedImageUrls(reader);
			final List<String[]> mediaErrors = pairOfReport.getRight();
			final List<String> mediaSuccess = pairOfReport.getLeft();
			createSuccessFile(mediaSuccess, header);
			createErrorFile(mediaErrors);
		}
		catch (final InterruptedException e)
		{
			Thread.currentThread().interrupt();
			LOGGER.error("Exception Occurred In Completing Media Download Threads", e);
		}
		catch (final IOException e)
		{
			LOGGER.error("Exception Occurred While Creating Media File", e);
		}
		final Instant end = Instant.now();
		LOGGER.info("Time Taken In Downloading Images In File : {} is : {}", header.getFile().getName(),
				Duration.between(start, end));
		return header;
	}

	/**
	 * Retries failed image URLs, configurable number of times. Returns a Pair of mediaErrors and mediaSuccess, consisting
	 * of failed image urls and successfully downloaded images respectively.
	 */

	private Pair<List<String>, List<String[]>> downloadAndRetryFailedImageUrls(final CSVReader reader)
			throws IOException, InterruptedException
	{
		List<String[]> productList = reader.readAll();
		ExecutorService executorService;
		List<String[]> mediaErrors = null;
		final List<String> mediaSuccess = new CopyOnWriteArrayList<>();
		final int downloadThreads = configurationService.getConfiguration().getInt("simon.media.feed.download.threads", 1);
		final int readTimeOut = configurationService.getConfiguration().getInt("simon.media.feed.read.timeout", 30000);
		int retryCounter = configurationService.getConfiguration().getInt("simon.media.retry.counter", 3);
		final Map<String, Integer> productSuccessCounter = new ConcurrentHashMap<>();
		while (CollectionUtils.isNotEmpty(productList) && (retryCounter > 0))
		{
			executorService = Executors.newFixedThreadPool(downloadThreads);
			LOGGER.info("Downloading Images Pass {}", retryCounter);
			mediaErrors = new CopyOnWriteArrayList<>();
			downloadMediasFromProductList(readTimeOut, executorService, mediaSuccess, mediaErrors, productList,
					productSuccessCounter);
			retryCounter--;
			executorService.shutdown();
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			productList = mediaErrors;
		}
		return new ImmutablePair<>(mediaSuccess, mediaErrors);
	}

	/**
	 * Executes each line from productList onto multiple threads using {@link ExecutorService}
	 */
	private void downloadMediasFromProductList(final int readTimeOut, final ExecutorService executorService,
			final List<String> mediaSuccess, final List<String[]> mediaErrors, final List<String[]> productList,
			final Map<String, Integer> productSuccessCounter)
	{
		productList.forEach(line -> executorService
				.execute(() -> downloadMedias(line, mediaSuccess, mediaErrors, readTimeOut, productSuccessCounter)));
	}

	/**
	 * Download images and stored them into disk. This also logs failed images into media error.
	 *
	 * @param csvLine
	 *           the csv line
	 * @param outputLines
	 *           the output lines
	 * @param mediaError
	 *           the media error
	 * @param productSuccessCounter
	 */
	private void downloadMedias(final String[] csvLine, final List<String> outputLines, final List<String[]> mediaError,
			final int readTimeOut, final Map<String, Integer> productSuccessCounter)
	{
		if (ArrayUtils.isNotEmpty(csvLine))
		{
			final String productCode = csvLine[0];
			LOGGER.info("Product : {} Medias Downloading", productCode);
			int counter = productSuccessCounter.get(productCode) != null ? productSuccessCounter.get(productCode) : 0;
			final List<String> failedimageUrls = new ArrayList<>();
			final String simonConversionGroup = getPrimaryImageGroup();
			final StringTokenizer imageTokens = new StringTokenizer(csvLine[1], SimonCoreConstants.PIPE);
			while (imageTokens.hasMoreTokens())
			{
				final String imageUrl = imageTokens.nextToken();
				final String imageFile = new StringBuilder().append(getImageDir()).append(productCode)
						.append(SimonCoreConstants.HYPHEN).append(counter + 1).append(SimonCoreConstants.DOT).append(getFileExt())
						.toString();
				if (downloadImage(imageFile, imageUrl, readTimeOut))
				{
					final StringBuilder stringBuilder = new StringBuilder().append(productCode).append(",").append(imageFile)
							.append(",").append(counter + 1).append(",");

					if (counter == 0)
					{
						stringBuilder.append(simonConversionGroup).append(",");
					}
					outputLines.add(stringBuilder.toString());
					counter++;
				}
				else
				{
					failedimageUrls.add(imageUrl);
				}
			}
			if (CollectionUtils.isNotEmpty(failedimageUrls))
			{
				mediaError.add(new String[]
				{ productCode, StringUtils.join(failedimageUrls, "|") });
			}
			productSuccessCounter.put(productCode, counter);
			LOGGER.info("Product : {} Medias Downloaded Total Sucess : {} Failures : {}", productCode, counter,
					failedimageUrls.size());
		}
	}

	/**
	 * Creates the error file.
	 *
	 * @param mediaErrorList
	 *           the error list
	 */
	private void createErrorFile(final List<String[]> mediaErrorList)
	{
		if (CollectionUtils.isNotEmpty(mediaErrorList))
		{
			final String fileNameSeperator = configurationService.getConfiguration().getString(FILENAME_SEPERATOR,
					DEFAULT_FILENAME_SEPERATOR);
			final StringBuilder path = new StringBuilder(configurationService.getConfiguration().getString(ERROR_FOLDER));
			final File directory = new File(path.toString());
			boolean directoryExist = true;
			if (!directory.exists())
			{
				directoryExist = directory.mkdirs();
			}
			if (directoryExist)
			{
				path.append(System.getProperty("file.separator"));
				path.append(configurationService.getConfiguration().getString(PREFIX_FILENAME_SUMMARY));
				path.append(fileNameSeperator);
				path.append(new SimpleDateFormat(REGEX_DATE, i18nService.getCurrentLocale()).format(new Date()));
				path.append(EXTENSION_FILE);
				final String defaultSeperator = configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR);
				try (final Writer fw = fileWriterWrapper.getWriter(path.toString()))
				{
					fw.write(configurationService.getConfiguration().getString(MEDIA_ERROR_TEXT) + defaultSeperator
							+ mediaErrorList.size());
					fw.write(System.getProperty("line.separator"));
					for (final String[] line : mediaErrorList)
					{
						fw.write(line[0] + "," + line[1]);
						fw.write(System.lineSeparator());
					}
				}
				catch (final IOException e)
				{
					LOGGER.error("Error in writing file name " + path, e);
				}
			}
		}
	}

	/**
	 * Creates the success file.
	 *
	 * @param mediaSuccessList
	 *           the media success
	 * @param header
	 *           the header
	 * @throws IOException
	 *            if any exception occurs in creating file
	 */
	private void createSuccessFile(final List<String> mediaSuccessList, final BatchHeader header) throws IOException
	{
		if (CollectionUtils.isNotEmpty(mediaSuccessList))
		{
			final StringBuilder outputFileBuilder = new StringBuilder();
			outputFileBuilder.append(getOutputCsvPath()).append(getFilePrefix()).append(SimonCoreConstants.UNDERSCORE)
					.append(header.getSequenceId()).append(EXTENSION_TEMP_FILE);
			final String tempOutputFilePath = outputFileBuilder.toString();
			final File directory = new File(getOutputCsvPath());
			if (!directory.exists())
			{
				directory.mkdir();
			}
			Files.write(Paths.get(tempOutputFilePath), mediaSuccessList, Charset.defaultCharset());
			LOGGER.info("Created Temp Media File : {}", tempOutputFilePath);
			renameFile(tempOutputFilePath);
		}
	}

	/**
	 * Save file from url.
	 *
	 * @param fileName
	 *           the file name
	 * @param fileUrl
	 *           the file url
	 * @param mediaError
	 * @param productCode
	 */
	private boolean downloadImage(final String fileName, final String fileUrl, final int readTimeOut)
	{
		return deleteAndSaveFile(fileName, fileUrl, readTimeOut);
	}

	/**
	 * Delete old file if already exist and download new one.
	 *
	 * @param fileName
	 *           the file name
	 * @param fileUrl
	 *           the file url
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	protected boolean deleteAndSaveFile(final String fileName, final String fileUrl, final int readTimeOut)
	{
		boolean downloadSuccess;
		final File file = new File(fileName);

		final RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(readTimeOut).build();

		try (OutputStream outputStream = new FileOutputStream(file);
				final CloseableHttpClient client = HttpClientBuilder.create().setSSLHostnameVerifier(new NoopHostnameVerifier())
						.setDefaultRequestConfig(requestConfig).build();)
		{
			FileUtils.forceDeleteOnExit(file);

			final HttpGet request = new HttpGet(fileUrl);
			final HttpResponse response = client.execute(request);
			final HttpEntity entity = response.getEntity();
			final InputStream inputStream = entity.getContent();

			final byte[] buffer = new byte[2048];
			int length;
			while ((length = inputStream.read(buffer)) != -1)
			{
				outputStream.write(buffer, 0, length);
			}
			outputStream.flush();
			downloadSuccess = true;
		}
		catch (final SocketTimeoutException exception)
		{
			LOGGER.error("Request Timeout for {} ", fileUrl, exception);
			downloadSuccess = false;
		}
		catch (final Exception e)
		{
			LOGGER.error("Error Occurred In Downloading And Saving File for {} ", fileUrl, e);
			downloadSuccess = false;
		}
		return downloadSuccess;
	}



	/**
	 * Rename file.
	 *
	 * @param tempFilePath
	 *           the temp file path
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	private void renameFile(final String tempFilePath) throws IOException
	{
		final File tempFile = new File(tempFilePath);
		final String actualFilePath = tempFilePath.replaceAll(EXTENSION_TEMP_FILE, EXTENSION_FILE);
		final File actualFile = new File(actualFilePath);
		if (tempFile.renameTo(actualFile))
		{
			LOGGER.info("Temp File : {} Successfully Renamed To : {}", tempFilePath, actualFilePath);
		}
		else
		{
			throw new IOException(
					"Exception Occurred In Renaming Temp File : " + tempFilePath + " To Actual File : " + actualFilePath);
		}
	}

	/**
	 * Gets the CSV reader.
	 *
	 * @param file
	 *           the file
	 * @param charset
	 *           the charset
	 * @return the CSV reader
	 * @throws UnsupportedEncodingException
	 *            the unsupported encoding exception
	 * @throws FileNotFoundException
	 *            the file not found exception
	 */
	protected CSVReader getCSVReader(final File file, final String charset)
			throws UnsupportedEncodingException, FileNotFoundException
	{
		return new CSVReader(new InputStreamReader(new FileInputStream(file), charset));
	}

	/**
	 * Method return the feed file name with out extension.
	 *
	 * @return String
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * method return the output csv file path of feed file.
	 *
	 * @return String
	 */
	public String getOutputCsvPath()
	{
		return outputCsvPath;
	}

	/**
	 * method return the image folder path where image was download.
	 *
	 * @return String
	 */
	public String getImageDir()
	{
		return imageDir;
	}

	/**
	 * Method set the feed file name with out extension.
	 *
	 * @param fileName
	 *           the new file name
	 */
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}


	/**
	 * method set the image folder path where image was download.
	 *
	 * @param imageDir
	 *           the new image dir
	 */
	public void setImageDir(final String imageDir)
	{
		this.imageDir = imageDir;
	}

	/**
	 * method get the feed file extension.
	 *
	 * @return String
	 */
	public String getFileExt()
	{
		return fileExt;
	}

	/**
	 * method set the feed file extension.
	 *
	 * @param fileExt
	 *           the new file ext
	 */
	public void setFileExt(final String fileExt)
	{
		this.fileExt = fileExt;
	}


	/**
	 * method get the create file prefix.
	 *
	 * @return String
	 */
	public String getFilePrefix()
	{
		return filePrefix;
	}

	/**
	 * method set the create file prefix.
	 *
	 * @param filePrefix
	 *           the new file prefix
	 */
	public void setFilePrefix(final String filePrefix)
	{
		this.filePrefix = filePrefix;
	}

	/**
	 * method to set the output csv file path of feed file.
	 *
	 * @param outputCsvPath
	 *           the new output csv path
	 */
	public void setOutputCsvPath(final String outputCsvPath)
	{
		this.outputCsvPath = outputCsvPath;
	}

	/**
	 *
	 * @return primary image group
	 */
	public String getPrimaryImageGroup()
	{
		return primaryImageGroup;
	}

	/**
	 * set primary image group
	 *
	 * @param primaryImageGroup
	 */
	public void setPrimaryImageGroup(final String primaryImageGroup)
	{
		this.primaryImageGroup = primaryImageGroup;
	}

}
