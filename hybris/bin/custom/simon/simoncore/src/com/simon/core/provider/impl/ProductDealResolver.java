package com.simon.core.provider.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.promotionengineservices.model.PromotionSourceRuleModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import com.simon.core.model.DealModel;


/**
 * Resolver to index Multi valued deal code
 */
public class ProductDealResolver
		extends AbstractValueResolver<GenericVariantProductModel, List<DealModel>, Object>
{
	@Resource
	private PromotionsService promotionsService;



	/**
	 * Load data into context. Deals are fetched from PromotionModel of GenericVariantProduct.
	 */
	@Override
	protected List<DealModel> loadData(final IndexerBatchContext batchContext, final Collection<IndexedProperty> indexedProperties,
			final GenericVariantProductModel product) throws FieldValueProviderException
	{
		final BaseSiteModel baseSiteModel = batchContext.getFacetSearchConfig().getIndexConfig().getBaseSite();
		final List<? extends AbstractPromotionModel> promotionList = promotionsService.getAbstractProductPromotions(
				Collections.singletonList(baseSiteModel.getDefaultPromotionGroup()), product, true, new Date());
		return promotionList.stream()
		.filter(promotionModel -> promotionModel instanceof RuleBasedPromotionModel).map(promotion -> ((PromotionSourceRuleModel) ((RuleBasedPromotionModel) promotion).getRule()
						.getSourceRule()).getDeal())
				.filter(Objects::nonNull).collect(Collectors.toList());

	}



	/**
	 * Fetches deals from Context and indexed the deal Codes
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<List<DealModel>, Object> resolverContext) throws FieldValueProviderException
	{
		final List<DealModel> deals = resolverContext.getData();
		final List<String> dealCodes = deals.stream().map(DealModel::getCode).collect(Collectors.toList());
		filterAndAddFieldValues(document, batchContext, indexedProperty, dealCodes, resolverContext.getFieldQualifier());

	}


}
