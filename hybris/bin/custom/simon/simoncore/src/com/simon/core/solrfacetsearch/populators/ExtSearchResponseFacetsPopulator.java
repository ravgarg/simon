package com.simon.core.solrfacetsearch.populators;

import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.populators.SearchResponseFacetsPopulator;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 * Extends SearchResponseFacetsPopulator class for set DisplayFacetType value in facetData
 */
public class ExtSearchResponseFacetsPopulator extends SearchResponseFacetsPopulator
{
	/**
	 * Override Populator of SearchResponseFacetsPopulator for set DisplayFacetType value in facetData
	 */
	@Override
	protected List<FacetData<SolrSearchQueryData>> buildFacets(final SearchResult solrSearchResult,
			final SolrSearchQueryData searchQueryData, final IndexedType indexedType)
	{
		final List<Facet> solrSearchResultFacets = solrSearchResult.getFacets();
		final List<FacetData<SolrSearchQueryData>> result = new ArrayList<>(solrSearchResultFacets.size());

		for (final Facet facet : solrSearchResultFacets)
		{
			final IndexedProperty indexedProperty = indexedType.getIndexedProperties().get(facet.getName());

			// Ignore any facets with a priority less than or equal to 0 as they are for internal use only
			final FacetData<SolrSearchQueryData> facetData = createFacetData();
			facetData.setCode(facet.getName());
			facetData.setCategory(indexedProperty.isCategoryField());
			final String displayName = indexedProperty.getDisplayName();
			facetData.setName(displayName == null ? facet.getName() : displayName);
			facetData.setMultiSelect(facet.isMultiselect());
			facetData.setPriority(facet.getPriority());
			facetData.setVisible(indexedProperty.isVisible());
			facetData.setDisplayFacetType(indexedProperty.getDisplayFacetType());
			buildFacetValue(facetData, facet, indexedProperty, solrSearchResult, searchQueryData);
			// Only add the facet if there are values
			if (CollectionUtils.isNotEmpty(facetData.getValues()))
			{
				result.add(facetData);
			}
		}

		return result;
	}

	protected void buildFacetValue(final FacetData<SolrSearchQueryData> facetData, final Facet facet,
			final IndexedProperty indexedProperty, final SearchResult solrSearchResult, final SolrSearchQueryData searchQueryData)
	{
		buildFacetValues(facetData, facet, indexedProperty, solrSearchResult, searchQueryData);
	}
}
