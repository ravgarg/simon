package com.simon.core.strategies.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.strategies.impl.DefaultCartValidationStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.AdditionalProductDetailsModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.services.ExtCartService;


/**
 * ExtDefaultCartValidationStrategy extends DefaultCartValidationStrategy and method validateCartEntry remove entry if
 * retailer is in active
 */
public class ExtDefaultCartValidationStrategy extends DefaultCartValidationStrategy
{

	@Resource
	private ModelService modelService;
	@Resource
	private SessionService sessionService;
	@Resource
	private ExtCartService cartService;

	/**
	 * method validateCartEntry remove entry if retailer is in active
	 */
	@Override
	protected CommerceCartModification validateCartEntry(final CartModel cart, final CartEntryModel entry)
	{
		CommerceCartModification modification = new CommerceCartModification();
		if (!entry.getProduct().getRetailerActive()
				|| (null != entry.getProduct().getShop() && entry.getProduct().getShop().isPunchOutFlag()))
		{
			modification.setQuantity(entry.getQuantity());
			modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
			modification.setQuantityAdded(0);
			modification.setEntry(getUnavailableCartEntry(entry));
			getModelService().remove(entry);
			getModelService().refresh(cart);
			return modification;
		}
		else if (getCartService().hasReceivedUpdatedCreateCallback(cart))
		{
			modification = validateFailedProduct(cart, entry);
		}

		if (null == modification.getStatusCode() || modification.getStatusCode().equals(CommerceCartModificationStatus.SUCCESS))
		{
			return super.validateCartEntry(cart, entry);
		}
		return modification;
	}

	private CommerceCartModification validateFailedProduct(final CartModel cart, final CartEntryModel entry)
	{
		boolean entryRemoved = false;
		CommerceCartModification modification = new CommerceCartModification();
		modification.setStatusCode(CommerceCartModificationStatus.SUCCESS);

		for (final RetailersInfoModel infoModel : cart.getAdditionalCartInfo().getRetailersInfo())
		{
			if (CollectionUtils.isNotEmpty(infoModel.getFailedProducts()))
			{
				for (final AdditionalProductDetailsModel additionalProductDetailsModel : infoModel.getFailedProducts())
				{
					if (entry.getProduct().getCode().equalsIgnoreCase(additionalProductDetailsModel.getProductID()))
					{
						modification = removeEntry(entry);
						entryRemoved = true;
						getModelService().refresh(cart);
					}
				}
			}

		}

		final List<String> fieldMissingName = sessionService.getAttribute(SimonCoreConstants.UNAPPROVED_PRODUCTIDS);
		if (!entryRemoved && CollectionUtils.isNotEmpty(fieldMissingName)
				&& fieldMissingName.contains(entry.getProduct().getCode()))
		{
			modification = unavailableProduct(entry);
			entryRemoved = true;
			getModelService().refresh(cart);
		}

		final List<String> fieldMissingNameValues = sessionService.getAttribute(SimonCoreConstants.OOS_PRODUCTIDS);
		if (!entryRemoved && CollectionUtils.isNotEmpty(fieldMissingNameValues)
				&& fieldMissingNameValues.contains(entry.getProduct().getCode()))
		{
			modification = removeEntry(entry);
			getModelService().refresh(cart);
		}


		return modification;
	}

	/**
	 * method removeProduct remove entry product out of stock
	 */
	private CommerceCartModification removeEntry(final CartEntryModel entry)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setEntry(getUnavailableCartEntry(entry));
		modification.setStatusCode(CommerceCartModificationStatus.NO_STOCK);
		getModelService().remove(entry);
		return modification;
	}

	/**
	 * method removeProduct remove entry product unavailable
	 */
	private CommerceCartModification unavailableProduct(final CartEntryModel entry)
	{
		final CommerceCartModification modification = new CommerceCartModification();
		modification.setEntry(getUnavailableCartEntry(entry));
		modification.setStatusCode(CommerceCartModificationStatus.UNAVAILABLE);
		getModelService().remove(entry);
		return modification;
	}


	/**
	 * create a CartEntryModel
	 *
	 * @param entry
	 * @return
	 */
	protected CartEntryModel getUnavailableCartEntry(final AbstractOrderEntryModel entry)
	{
		final CartEntryModel cartEntry = new CartEntryModel();
		cartEntry.setProduct(entry.getProduct());
		return cartEntry;
	}

	/**
	 * @return cartService
	 */
	@Override
	public ExtCartService getCartService()
	{
		return this.cartService;
	}
}
