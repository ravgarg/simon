package com.simon.core.caches;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.keygenerators.DesignerKeyGenerator;
import com.simon.core.model.DesignerModel;
import com.simon.core.services.DesignerService;


/**
 * DesignerPreProcessor creates new {@link DesignerModel} if present in product feed found during pre-processing.
 */
@Order(value = 2)
public class DesignerPreProcessor extends AbstractPreProcessor<DesignerModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(DesignerPreProcessor.class);

	@Autowired
	private DesignerService designerService;

	@Autowired
	private DesignerKeyGenerator designerKeyGenerator;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createKey(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		return (String) designerKeyGenerator.generateFor(rawValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DesignerModel createValue(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		final String code = createKey(attribute, rawValue, productValues, context);
		DesignerModel designer;
		final DesignerModel existingDesigner = designerService.getDesignerForCode(code);
		if (null != existingDesigner)
		{
			LOGGER.debug("Designer with code {} found", code);
			designer = existingDesigner;
		}
		else
		{
			LOGGER.info("Designer {} Not Found. Creating new Designer", code);
			designer = modelService.create(DesignerModel.class);
			designer.setCode(code);
			designer.setName(rawValue);
			modelService.save(designer);
		}
		return designer;
	}
}
