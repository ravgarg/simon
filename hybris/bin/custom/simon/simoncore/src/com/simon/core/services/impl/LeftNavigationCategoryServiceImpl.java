package com.simon.core.services.impl;

import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import com.simon.core.dao.LeftNavigationCategoryDao;
import com.simon.core.dto.NavigationData;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;
import com.simon.core.services.LeftNavigationCategoryService;


/**
 * This class fetches all L1 categories and maintain the corresponding category hierarchy per L1 category.
 */
public class LeftNavigationCategoryServiceImpl implements LeftNavigationCategoryService
{
	@Resource
	private LeftNavigationCategoryDao leftNavigationCategoryDao;

	@Resource
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Resource
	private CategoryService categoryService;

	@Value("${navigation.home.category.code}")
	private String homeCategoryCode;

	private Map<String, NavigationData> leftNavigationMap;

	/**
	 * This method fetches all categories who are children of Home Category
	 */
	@Override
	public List<CategoryModel> getL1Categories()
	{
		return leftNavigationCategoryDao.getL1Categories(homeCategoryCode);
	}

	/**
	 * This method returns complete category hierarchy corresponding to particular L1 category. If no entry is found
	 * corresponding to L1 category, empty list is returned.
	 */
	@Override
	public List<NavigationData> getNavigationData(final String l1CategoryCode)
	{
		List<NavigationData> navData = Collections.emptyList();

		if (MapUtils.isEmpty(leftNavigationMap))
		{
			createMap();
		}
		if (null != leftNavigationMap.get(l1CategoryCode))
		{
			navData = leftNavigationMap.get(l1CategoryCode).getChildNode();
		}
		return navData;
	}

	/**
	 * This recursive method makes category hierarchy corresponding to each L1 category
	 *
	 * @param navigationData
	 *           Current Navigation DTO
	 * @param parentCategory
	 *           Current category model to be set in DTO
	 * @param beanComparator
	 *           comparator to sort categories alphabetically
	 * @param catCodeURL
	 *           the complete category hierarchy to be set in URL
	 */
	private void getchildNodes(final NavigationData navigationData, final CategoryModel parentCategory,
			final BeanComparator<NavigationData> beanComparator, final String catCodeURL)
	{
		navigationData.setId(parentCategory.getCode());
		navigationData.setName(StringUtils.isEmpty(parentCategory.getName()) ? parentCategory.getCode() : parentCategory.getName());
		navigationData.setUrl(extCategoryModelUrlResolver.resolveCategoryPath(parentCategory, catCodeURL));
		if (!parentCategory.getProducts().isEmpty())
		{
			navigationData.setShow(true);
		}
		else
		{
			navigationData.setShow(false);
		}
		final List<CategoryModel> subCategories = parentCategory.getCategories();
		if (CollectionUtils.isNotEmpty(subCategories))
		{
			final long subCategoriesCount = subCategories.stream()
					.filter(t -> CollectionUtils.isNotEmpty(t.getProducts()) || CollectionUtils.isNotEmpty(t.getCategories())).count();
			if (subCategoriesCount > 0)
			{
				final List<NavigationData> childNavList = new ArrayList<>();
				subCategories.forEach(subCategory -> {
					final NavigationData childNavigationData = new NavigationData();
					if (CollectionUtils.isEmpty(navigationData.getChildNode()))
					{
						childNavList.add(childNavigationData);
						navigationData.setChildNode(childNavList);
					}
					else
					{
						navigationData.getChildNode().add(childNavigationData);
					}
					getchildNodes(childNavigationData, subCategory, beanComparator, catCodeURL + "_" + subCategory.getCode());
				});
				Collections.sort(navigationData.getChildNode(), beanComparator);
				navigationData.getChildNode().forEach(childNode -> {
					if (childNode.isShow())
					{
						navigationData.setShow(true);
					}
				});
			}
		}
	}

	/**
	 * This method initiated map creation of L1 categories and corresponding category hierarchy.
	 */
	@Override
	public boolean createMap()
	{
		final List<CategoryModel> listOfL1Categories = getL1Categories();
		leftNavigationMap = new HashMap<>();
		final BeanComparator<NavigationData> beanComparator = new BeanComparator<>("name");
		for (final CategoryModel l1Category : listOfL1Categories)
		{
			final String l1CategoryCode = l1Category.getCode();
			final NavigationData navigationData = new NavigationData();
			getchildNodes(navigationData, l1Category, beanComparator, l1CategoryCode);
			leftNavigationMap.put(l1CategoryCode, navigationData);
		}
		return true;
	}
}
