package com.simon.core.services.impl;



import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.model.RetailersInfoModel;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.RetailerService;
import com.simon.core.util.CommonUtils;


/**
 * Service allows calculation or recalculation of the order. This includes calculation of all the entries, taxes and
 * discount values. Information about price, taxes and discounts are fetched using dedicated strategies :
 * {@link FindDiscountValuesStrategy}, {@link FindTaxValuesStrategy}, {@link FindPriceStrategy}. Also payment and
 * delivery costs are resolved using strategies {@link FindDeliveryCostStrategy} and {@link FindPaymentCostStrategy}.
 *
 * Whether order needs to be calculated or not is up to the implementation of {@link OrderRequiresCalculationStrategy}.
 *
 * it also calculate price value
 */
public class ExtCalculationService extends DefaultCalculationService
{
	private static final long serialVersionUID = -4980981544384899533L;
	private transient CommonI18NService extCommonI18NService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ExtCalculationService.class);

	@Resource(name = "orderRequiresCalculationStrategy")
	private OrderRequiresCalculationStrategy requireCalculationStrategy;
	@Resource
	private RetailerService retailerService;

	/**
	 * calculates all totals. this does not trigger price, tax and discount calculation but takes all currently set
	 * price, tax and discount values as base. this method requires the correct subtotal to be set before and the correct
	 * tax value map.
	 *
	 * @param recalculate
	 *           if false calculation is done only if the calculated flag is not set
	 * @param taxValueMap
	 *           the map { tax value -> Double( sum of all entry totals for this tax ) } obtainable via
	 *           {@link #calculateSubtotal(AbstractOrderModel, boolean)}
	 * @throws CalculationException
	 */
	@Override
	protected void calculateTotals(final AbstractOrderModel order, final boolean recalculate,
			final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap) throws CalculationException
	{
		super.calculateTotals(order, recalculate, taxValueMap);
		if (recalculate || requireCalculationStrategy.requiresCalculation(order))
		{
			order.setRetailersSubtotal(retailerService.getRetailerSubBagForCart(order));

		}
	}

	/**
	 * resetAllValues method reset All values of entry and take list price and base price after getting price value from
	 * entry and set these value to list.
	 *
	 */

	@Override
	protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		// taxes
		final Collection<TaxValue> entryTaxes = extFindTaxValues(entry);
		entry.setTaxValues(entryTaxes);
		final PriceValue pv = extFindBasePrice(entry);
		final ExtPriceValue extPriceValue = (ExtPriceValue) pv;
		final AbstractOrderModel order = entry.getOrder();
		final ExtPriceValue basePrice = convertPriceIfNecessary(extPriceValue, order.getNet().booleanValue(), order.getCurrency(),
				entryTaxes);
		setPrice(entry, basePrice);
	}

	@Override
	protected Map resetAllValues(final AbstractOrderModel order) throws CalculationException
	{
		order.setSubbagDiscountValues(MapUtils.EMPTY_MAP);
		return super.resetAllValues(order);
	}

	/**
	 * set sales price , list price and base price after getting pricevalue from entry and set these value to list price
	 * and sales price as per FSD if list price is not zero sale price is base price and list price is list price. while
	 * if list price is zero sales price is zero and list price is base price.
	 *
	 * @param entry
	 * @param basePrice
	 * @throws CalculationException
	 */
	private void setPrice(final AbstractOrderEntryModel entry, final ExtPriceValue extPriceValue) throws CalculationException
	{
		entry.setBasePrice(Double.valueOf(extPriceValue.getValue()));
		entry.setOfferId(extPriceValue.getOfferId());
		if (extPriceValue.getListPrice() > 0)
		{
			entry.setSaleValue(Double.valueOf(extPriceValue.getValue()));
			entry.setListValue(Double.valueOf(extPriceValue.getListPrice()));
		}
		else
		{
			entry.setListValue(Double.valueOf(extPriceValue.getValue()));
		}
		final List<DiscountValue> entryDiscounts = extFindDiscountValues(entry);
		entry.setDiscountValues(entryDiscounts);
		entry.setSpStartDate(extPriceValue.getSalePriceStartDate());
		entry.setSpEndDate(extPriceValue.getSalePriceEndDate());
		entry.setLeadTimeToShip(extPriceValue.getLeadtimeToShip());
	}


	/**
	 * Returns the combined discount for all retailers in this abstract order.
	 *
	 * @param recalculate
	 *           <code>true</code> forces a recalculation
	 * @return totalDiscounts
	 */
	protected double calculateReatailerDiscountValues(final AbstractOrderModel order, final boolean recalculate)
	{
		double totalRetailerDiscount = 0.0;
		final Map<String, List<DiscountValue>> retailersDiscountValuesMap = order.getSubbagDiscountValues();
		if (recalculate || requireCalculationStrategy.requiresCalculation(order))
		{
			if (MapUtils.isNotEmpty(retailersDiscountValuesMap))
			{
				final CurrencyModel curr = order.getCurrency();
				final String iso = curr.getIsocode();
				final int digits = curr.getDigits().intValue();
				order.setRetailersSubtotal(retailerService.getRetailerSubBagForCart(order));
				for (final String retailer : retailersDiscountValuesMap.keySet())
				{
					final double discountablePrice = retailerSubtotal(order, retailer)
							+ (order.isDiscountsIncludeDeliveryCost() ? order.getRetailersDeliveryCost().get(retailer) : 0.0)
							+ (order.isDiscountsIncludePaymentCost() ? order.getPaymentCost().doubleValue() : 0.0);

					final List<DiscountValue> retailersDiscountValues = CommonUtils
							.createArrayListFrom(retailersDiscountValuesMap.get(retailer));
					final List<DiscountValue> appliedRetailersDiscounts = DiscountValue.apply(1.0, discountablePrice, digits,
							convertRetailerDiscountValues(order, retailersDiscountValues), iso);
					final Map<String, List<DiscountValue>> retailerDiscounts = new HashMap<>(order.getSubbagDiscountValues());
					retailerDiscounts.put(retailer, appliedRetailersDiscounts);
					final double totalDiscountForCurrentRet = DiscountValue.sumAppliedValues(appliedRetailersDiscounts);
					totalRetailerDiscount += totalDiscountForCurrentRet;
					setRetailerDiscount(order, retailer, totalDiscountForCurrentRet);
				}
			}
		}
		else
		{
			for (final String retailer : retailersDiscountValuesMap.keySet())
			{
				final double totalDiscountForCurrentRet = DiscountValue
						.sumAppliedValues(order.getSubbagDiscountValues().get(retailer));
				totalRetailerDiscount = totalRetailerDiscount + totalDiscountForCurrentRet;
				setRetailerDiscount(order, retailer, totalDiscountForCurrentRet);
			}
		}
		return totalRetailerDiscount;
	}

	private void setRetailerDiscount(final AbstractOrderModel order, final String retailer,
			final double totalDiscountForCurrentRet)
	{
		final CurrencyModel curr = order.getCurrency();
		final int digits = curr.getDigits().intValue();
		final double roundedTotalDiscounts = getExtCommonI18NService().roundCurrency(totalDiscountForCurrentRet, digits);
		final Map<String, Double> retailerDiscounts = new HashMap<>(order.getRetailersTotalDiscount());
		retailerDiscounts.put(retailer, roundedTotalDiscounts);
		order.setRetailersTotalDiscount(retailerDiscounts);
	}

	private double retailerSubtotal(final AbstractOrderModel order, final String retailer)
	{
		Double subtotal = order.getRetailersSubtotal().get(retailer);
		if (null == subtotal)
		{
			subtotal = Double.valueOf(0);
		}
		return subtotal.doubleValue();
	}

	protected List<DiscountValue> convertRetailerDiscountValues(final AbstractOrderModel order,
			final List<DiscountValue> retailersDiscountValues)
	{
		final CurrencyModel curr = order.getCurrency();
		final String iso = curr.getIsocode();
		final List<DiscountValue> tmp = new ArrayList<>(retailersDiscountValues);
		final Map<String, CurrencyModel> currencyMap = new HashMap<>();
		for (int loopVariable = 0; loopVariable < tmp.size(); loopVariable++)
		{
			final DiscountValue discountValue = tmp.get(loopVariable);
			if (discountValue.isAbsolute() && !iso.equals(discountValue.getCurrencyIsoCode()))
			{
				CurrencyModel dCurr = currencyMap.get(discountValue.getCurrencyIsoCode());
				if (dCurr == null)
				{
					dCurr = extCommonI18NService.getCurrency(discountValue.getCurrencyIsoCode());
					currencyMap.put(discountValue.getCurrencyIsoCode(), dCurr);
				}
				tmp.set(loopVariable,
						new DiscountValue(discountValue.getCode(),
								extCommonI18NService.convertAndRoundCurrency(dCurr.getConversion().doubleValue(),
										curr.getConversion().doubleValue(), curr.getDigits().intValue(), discountValue.getValue()),
								true, iso));
			}
		}
		return tmp;
	}

	/**
	 * Returns the summation of all the discounts through promotion, whether it is order level or retailer level. And
	 * also resets retailers discount when needed.
	 *
	 * @param order
	 * @param recalculate
	 * @return double total discounts through promotions (retailer level as well as order level)
	 */
	@Override
	protected double calculateDiscountValues(final AbstractOrderModel order, final boolean recalculate)
	{
		final double retailersDiscount = calculateReatailerDiscountValues(order, recalculate);
		if (Double.compare(retailersDiscount, 0.0) <= 0)
		{
			order.setRetailersTotalDiscount(new HashMap<>());
		}
		return super.calculateDiscountValues(order, recalculate) + retailersDiscount;
	}

	/**
	 * This method will calculate the taxes per retailer and will create a Map to be stored in the cart. This will also
	 * add the taxes applicable per retailer and will store the summation in the Cart's TotalTax as well.
	 *
	 * @param order
	 * @param recalculate
	 * @param digits
	 * @param taxAdjustmentFactor
	 * @param taxValueMap
	 * @return total taxes
	 */
	@Override
	public double calculateTotalTaxValues(final AbstractOrderModel order, final boolean recalculate, final int digits,
			final double taxAdjustmentFactor, final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap)
	{
		double totalSalesTax = 0;
		if (recalculate || requireCalculationStrategy.requiresCalculation(order))
		{
			final Map<String, Double> retailersSubtotal = order.getRetailersSubtotal();
			final Map<String, Double> retailersSalesTax = new HashMap<>();
			if (null != order.getAdditionalCartInfo()
					&& CollectionUtils.isNotEmpty(order.getAdditionalCartInfo().getRetailersInfo()))
			{
				for (final RetailersInfoModel retailer : order.getAdditionalCartInfo().getRetailersInfo())
				{
					if (null != retailersSubtotal.get(retailer.getRetailerId()) && null != retailer.getAppliedTaxRate())
					{
						final double retailerSubtotal = retailersSubtotal.get(retailer.getRetailerId()).doubleValue();
						final double retailerSalesTax = (retailerSubtotal * retailer.getAppliedTaxRate()) / 100;
						totalSalesTax += retailerSalesTax;
						retailersSalesTax.put(retailer.getRetailerId(), Double.valueOf(retailerSalesTax));
						retailer.setTotalSalesTax(retailerSalesTax);
					}
					getModelService().save(retailer);
				}
				order.setRetailersTotalTax(retailersSalesTax);
				order.setTotalTax(Double.valueOf(totalSalesTax));
				getModelService().save(order);
			}
		}
		else
		{
			totalSalesTax = order.getTotalTax().doubleValue();
		}
		return totalSalesTax;
	}

	/**
	 * this method is used for convert price to ExtPriceValue while creating the a new object of ExtPriceValue and
	 * passing the values of currency iso code convertedPrice, net price, sales price, sales price start date, sales
	 * price end date and offerId.
	 *
	 * @param pv
	 * @param toNet
	 * @param toCurrency
	 * @param taxValues
	 * @return ExtPriceValue
	 */
	public ExtPriceValue convertPriceIfNecessary(final ExtPriceValue pv, final boolean toNet, final CurrencyModel toCurrency,
			final Collection taxValues)
	{
		// net - gross - conversion
		double convertedPrice = pv.getValue();
		if (pv.isNet() != toNet)
		{
			final PriceValue otherPrice = pv.getOtherPrice(taxValues);
			convertedPrice = null != otherPrice ? otherPrice.getValue() : 0.0;
			convertedPrice = getExtCommonI18NService().roundCurrency(convertedPrice, toCurrency.getDigits().intValue());
		}
		// currency conversion
		final String iso = pv.getCurrencyIso();
		if (iso != null && !iso.equals(toCurrency.getIsocode()))
		{
			try
			{
				final CurrencyModel basePriceCurrency = getExtCommonI18NService().getCurrency(iso);
				convertedPrice = getExtCommonI18NService().convertAndRoundCurrency(basePriceCurrency.getConversion().doubleValue(),
						toCurrency.getConversion().doubleValue(), toCurrency.getDigits().intValue(), convertedPrice);
			}
			catch (final UnknownIdentifierException e)
			{
				LOGGER.error("Cannot convert from currency '" + iso + "' to currency '" + toCurrency.getIsocode() + "' since '" + iso
						+ "' doesn't exist any more - ignored", e);
			}
		}
		return new ExtPriceValue(toCurrency.getIsocode(), convertedPrice, toNet, pv.getListPrice(), pv.getSalePriceStartDate(),
				pv.getSalePriceEndDate(), pv.getOfferId(), pv.getLeadtimeToShip());
	}

	protected Collection<TaxValue> extFindTaxValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return super.findTaxValues(entry);
	}

	protected PriceValue extFindBasePrice(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return super.findBasePrice(entry);
	}

	protected List<DiscountValue> extFindDiscountValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		return super.findDiscountValues(entry);
	}


	/**
	 * @return extCommonI18NService
	 */
	public CommonI18NService getExtCommonI18NService()
	{
		return extCommonI18NService;
	}


	/**
	 * @param extCommonI18NService
	 */
	public void setExtCommonI18NService(final CommonI18NService extCommonI18NService)
	{
		this.extCommonI18NService = extCommonI18NService;
	}



}
