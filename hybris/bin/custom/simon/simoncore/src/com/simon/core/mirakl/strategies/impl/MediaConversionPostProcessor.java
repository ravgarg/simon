package com.simon.core.mirakl.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.mirakl.product.dao.MiraklRawProductDAO;
import com.simon.core.mirakl.strategies.ProductFileImportPostProcessor;


/**
 * Extended post process strategy to be executed as a post process to create csv for image generation.
 */
@Order(value = 1)
public class MediaConversionPostProcessor implements ProductFileImportPostProcessor
{
	private static final Logger LOGGER = LoggerFactory.getLogger(MediaConversionPostProcessor.class);

	private static final String PRODUCT_SKU = "sku";

	private static final String LINE_SEPERATOR = "line.separator";

	private static final String PATH_SEPERATOR = "file.separator";

	private static final String CSV_SEPERATOR = "product.image.csv.seperator";

	private static final String FILENAME_SEPERATOR = "product.image.filename.seperator";

	private static final String CSV_URL = "simon.hotfolder.importdata.image";

	private static final String PREFIX_FILENAME_CSV = "image.product.import.prefix.filename";

	private static final String DEFAULT_SEPERATOR = ",";

	private static final String DEFAULT_FILENAME_SEPERATOR = "-";

	private static final String EXTENSION_FILE = ".csv";

	private static final String EXTENSION_TEMP_FILE = ".temp";

	private static final String REGEX_DATE = "yyyyMMddHHmm";

	private static final String PREFIX_SKU = "Shop";

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private MiraklRawProductDAO miraklRawProductDAOImpl;

	@Resource
	private ShopService shopService;


	/**
	 * Called after a product is imported.
	 *
	 * @param context
	 *           the file import context
	 * @param importId
	 *           ther importId
	 */
	@Override
	public void postProcess(final ProductImportFileContextData context, final String importId)
	{
		final List<MiraklRawProductModel> miraklProductList = miraklRawProductDAOImpl.fetchMiraklProductsForImport(importId);
		if (CollectionUtils.isNotEmpty(miraklProductList))
		{
			final String fileNameSeperator = configurationService.getConfiguration().getString(FILENAME_SEPERATOR,
					DEFAULT_FILENAME_SEPERATOR);
			final StringBuilder tempfilepath = new StringBuilder(configurationService.getConfiguration().getString(CSV_URL));
			tempfilepath.append(System.getProperty(PATH_SEPERATOR));
			tempfilepath.append(configurationService.getConfiguration().getString(PREFIX_FILENAME_CSV));
			tempfilepath.append(fileNameSeperator);
			tempfilepath.append(importId);
			tempfilepath.append(fileNameSeperator);
			tempfilepath.append(new SimpleDateFormat(REGEX_DATE).format(new Date()));
			tempfilepath.append(EXTENSION_TEMP_FILE);
			final String seperator = configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR);
			final ShopModel shopModel = shopService.getShopForId(context.getShopId());
			final String shopPrefix = StringUtils.isNotEmpty(shopModel.getPreFix()) ? shopModel.getPreFix() : PREFIX_SKU;
			try (final FileWriter fw = new FileWriter(tempfilepath.toString()))
			{
				writeProductImageUrls(miraklProductList, seperator, shopPrefix, fw);
				IOUtils.closeQuietly(fw);
				renameFile(tempfilepath.toString());
			}
			catch (final IOException e)
			{
				LOGGER.error("Error in creating image feed file name " + tempfilepath + "with import id " + importId, e);
			}
		}
	}

	/**
	 * Write product image urls.
	 *
	 * @param miraklProductList
	 *           the mirakl product list
	 * @param seperator
	 *           the seperator
	 * @param shopPrefix
	 *           the shop prefix
	 * @param fw
	 *           the fw
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	private void writeProductImageUrls(final List<MiraklRawProductModel> miraklProductList, final String seperator,
			final String shopPrefix, final FileWriter fw) throws IOException
	{
		for (final MiraklRawProductModel product : miraklProductList)
		{
			final Map<String, String> values = product.getValues();
			final List<String> urlList = new ArrayList<>();
			final List<Object> attributeList = Lists.newArrayList(
					Splitter.on(",").split(configurationService.getConfiguration().getString("simon.product.import.image.fields")));
			if (MapUtils.isNotEmpty(values))
			{
				final String productSku = values.get(PRODUCT_SKU);
				attributeList.stream().forEach(attribute -> {
					final String url = values.get(attribute);
					if (StringUtils.isNotEmpty(url))
					{
						urlList.add(url);
					}
				});
				if (CollectionUtils.isNotEmpty(urlList))
				{
					fw.write(shopPrefix + SimonCoreConstants.HYPHEN + productSku);
					fw.write(seperator);
					fw.write(urlList.stream().collect(Collectors.joining(SimonCoreConstants.PIPE)));
					fw.write(System.getProperty(LINE_SEPERATOR));
				}
			}
		}
	}


	/**
	 * Rename file.
	 *
	 * @param tempFilePath
	 *           the temp file path
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	private void renameFile(final String tempFilePath) throws IOException
	{
		final File tempFile = new File(tempFilePath);
		final String actualFilePath = tempFilePath.replaceAll(EXTENSION_TEMP_FILE, EXTENSION_FILE);
		final File actualFile = new File(actualFilePath);
		if (tempFile.renameTo(actualFile))
		{
			LOGGER.info("Temp File : {} Successfully Renamed To : {}", tempFilePath, actualFilePath);
		}
		else
		{
			throw new IOException(
					"Exception Occurred In Renaming Temp File : " + tempFilePath + " To Actual File : " + actualFilePath);
		}
	}
}
