package com.simon.core.util;

import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.exceptions.SystemException;
import com.simon.restfulws.client.RestfulWsClient;
import com.simon.restfulws.client.RestfulWsException;
import com.simon.restfulws.client.RestfulWsRequestMeta;


/**
 * Utility class that uses ngrok to create public urls that can be used by Two Tap for call backs. This will not be used
 * on environments where the app is on DMZ and Two Tap can access it for callbacks.
 *
 */
public class DmzUntils
{

	private static final String API_TUNNELS = "http://localhost:4040/api/tunnels";

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DmzUntils.class);

	/** The json mapper. */
	private static ObjectMapper jsonMapper = new ObjectMapper();

	/** The purchase callback url updated. */
	private static String purchaseCallbackUrlUpdated;

	/** The purchase callback url confirmed. */
	private static String purchaseCallbackUrlConfirmed;

	/** The http tunnel to port. */
	@Value("${twotap.http.tunnel.to.port}")
	private String httpTunnelToPort;

	/**
	 * Setup callback urls. This method fetches public host with the help of ngrok. If ngrok is running it sets the host,
	 * creates urls and sets them in {@link #purchaseCallbackUrlConfirmed} and {@link #purchaseCallbackUrlUpdated} else
	 * it sets the default url {@link SimonCoreConstants#TWOTAP_DEFAULT_CALLBACK_HOST}
	 *
	 * @throws SystemException
	 *            the system exception
	 */
	public static void setupCallbackUrls() throws SystemException
	{
		//check if ngrok running on the same network
		boolean isNgrokRunning = false;
		final HttpGet get = new HttpGet(API_TUNNELS);
		final Map<String, String> hdrs = JsonUtils.acceptJsonHdr();

		String response = null;
		try
		{
			response = RestfulWsClient.callWebService(get, new RestfulWsRequestMeta(null, hdrs, true), String.class)
					.getServiceResponse();
			isNgrokRunning = true;
		}
		catch (final RestfulWsException ex)
		{
			LOGGER.error(String.format("Exception %s calling ngrok tunnel web service", ex), ex);
		}

		String publicUrl = Config.getParameter(SimonCoreConstants.TWOTAP_DEFAULT_CALLBACK_HOST);
		if (isNgrokRunning)
		{
			//ok, set up callback for TwoTap service
			LOGGER.info("got ngrok json response {}", response);
			JsonNode rootNode = null;
			try
			{
				rootNode = jsonMapper.readTree(response);
			}
			catch (final IOException t)
			{
				throw new SystemException(String.format("Exception %s trying to parse json %s", t, response), t, "");
			}
			final JsonNode tunnelsNode = rootNode.get("tunnels");

			final Iterator<JsonNode> elements = tunnelsNode.iterator();
			JsonNode tunnelNode = null;
			while (elements.hasNext())
			{
				tunnelNode = elements.next();

				if ("http".equalsIgnoreCase(JsonUtils.getJsonAttributeValue(tunnelNode, "proto")))
				{
					publicUrl = JsonUtils.getJsonAttributeValue(tunnelNode, "public_url");
					LOGGER.info("Got ngrok public URL {}", publicUrl);
					break;
				}
			}
		}
		purchaseCallbackUrlUpdated = publicUrl + SimonCoreConstants.TWOTAP_PURCHASE_CALLBACK_URL_UPDATED;
		purchaseCallbackUrlConfirmed = publicUrl + SimonCoreConstants.TWOTAP_PURCHASE_CALLBACK_URL_CONFIRMED;
	}

	/**
	 * Sets the purchase callback url updated.
	 *
	 * @param purchaseCallbackUrlUpdated
	 *           the new purchase callback url updated
	 */
	public static void setPurchaseCallbackUrlUpdated(final String purchaseCallbackUrlUpdated)
	{
		DmzUntils.purchaseCallbackUrlUpdated = purchaseCallbackUrlUpdated;
	}

	/**
	 * Gets the purchase callback url updated.
	 *
	 * @return the purchase callback url updated
	 */
	public static String getPurchaseCallbackUrlUpdated()
	{
		return purchaseCallbackUrlUpdated;
	}

	/**
	 * Sets the purchase callback url confirmed.
	 *
	 * @param purchaseCallbackUrlConfirmed
	 *           the new purchase callback url confirmed
	 */
	public static void setPurchaseCallbackUrlConfirmed(final String purchaseCallbackUrlConfirmed)
	{
		DmzUntils.purchaseCallbackUrlConfirmed = purchaseCallbackUrlConfirmed;
	}

	/**
	 * Gets the purchase callback url confirmed.
	 *
	 * @return the purchase callback url confirmed
	 */
	public static String getPurchaseCallbackUrlConfirmed()
	{
		return purchaseCallbackUrlConfirmed;
	}
}
