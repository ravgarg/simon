package com.simon.core.keygenerators;

import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Splitter;
import com.simon.core.constants.SimonCoreConstants;


/**
 * MediaKeyGenerator generates Swatch media Code for given raw name
 */
public class MediaKeyGenerator implements KeyGenerator
{
	private static final String COLOR = "color";

	@Autowired
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Autowired
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;
	private static final int VALID_DELIMS = 2;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object generate()
	{
		throw new UnsupportedOperationException("Not supported, please call generateFor().");
	}

	/**
	 * Converts color variant value category value into code by removing all space from value and add constant thumbnail.
	 *
	 * @param arg0
	 *           the Raw Name
	 * @return the object
	 */
	@Override
	public Object generateFor(final Object arg0)
	{

		String key = null;
		final String variantAttributeValue = (String) arg0;
		final Map<String, String> variantAttributeRawMap = getVariantAttributesRawMap(variantAttributeValue, "|=");
		final String colorVariantValue = variantAttributeRawMap.get(COLOR);
		if (StringUtils.isNotEmpty(colorVariantValue))
		{
			final String variantValueCategoryCode = createKeyOfColorVariantValueCategoryCode(COLOR, colorVariantValue);
			if (StringUtils.isNotEmpty(variantValueCategoryCode))
			{
				key = variantValueCategoryCode + SimonCoreConstants.UNDERSCORE + SimonCoreConstants.THUMBNAIL;
			}
		}
		return key;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset()
	{
		throw new UnsupportedOperationException("A reset of DesignerKeyGenerator is not supported.");

	}

	private Map<String, String> getVariantAttributesRawMap(final String variantRawText, final String delimiters)
	{
		Map<String, String> variantAttributesRawMap = new HashMap<>();
		if (StringUtils.isNotEmpty(variantRawText) && StringUtils.isNotEmpty(delimiters) && delimiters.length() == VALID_DELIMS)
		{
			final char[] delims = delimiters.toCharArray();
			variantAttributesRawMap = Splitter.on(delims[0]).withKeyValueSeparator(delims[1]).split(variantRawText);
		}
		return variantAttributesRawMap;
	}

	private String createKeyOfColorVariantValueCategoryCode(final String rawKey, final String rawValue)
	{
		String key = null;
		if (StringUtils.isNotEmpty(rawKey) && StringUtils.isNotEmpty(rawValue))
		{
			final String variantCategoryCode = (String) variantCategoryKeyGenerator.generateFor(rawKey);
			final String variantValueCategoryCode = (String) variantValueCategoryKeyGenerator.generateFor(rawValue)
					+ SimonCoreConstants.HYPHEN + variantCategoryCode;
			key = variantValueCategoryCode;
		}
		return key;
	}

}
