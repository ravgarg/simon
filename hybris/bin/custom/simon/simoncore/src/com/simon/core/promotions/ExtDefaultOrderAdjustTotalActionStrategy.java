package com.simon.core.promotions;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotionengineservices.action.impl.DefaultOrderAdjustTotalActionStrategy;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderAdjustTotalActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.promotions.service.ExtDefaultPromotionActionService;



/**
 * It enables the application of retailer level discount i.e, creation of {@link PromotionResultModel}and
 * {@link RuleBasedOrderAdjustTotalActionModel} and setting the retailer discount in {@link CartModel}
 */
public class ExtDefaultOrderAdjustTotalActionStrategy extends DefaultOrderAdjustTotalActionStrategy
{

	@Resource
	private ExtDefaultPromotionActionService promotionActionService;

	/**
	 * This method has been overridden in order to add retailerDiscountValues to CartModel. retailerDiscountValues are
	 * new attributes of CartModel which holds retailer sub cart level discounts.
	 *
	 * @param action
	 *           the action
	 * @return the list
	 *
	 */
	@Override
	public List<PromotionResultModel> apply(final AbstractRuleActionRAO action)
	{
		if (!(action instanceof DiscountRAO))
		{
			return Collections.emptyList();
		}
		else
		{
			final PromotionResultModel promoResult = this.getPromotionActionService().createPromotionResult(action);
			if (promoResult == null)
			{
				return Collections.emptyList();
			}
			else
			{
				return applyPromotion(action, promoResult);
			}
		}
	}

	/**
	 * Method to apply promotion and save it in DB using modelService.
	 *
	 * @param action
	 * @param promoResult
	 *
	 * @return listOfPromotionResultModel
	 */
	private List<PromotionResultModel> applyPromotion(final AbstractRuleActionRAO action, final PromotionResultModel promoResult)
	{
		final AbstractOrderModel order = promoResult.getOrder();
		if (order == null)
		{
			if (this.getModelService().isNew(promoResult))
			{
				this.getModelService().detach(promoResult);
			}
			return Collections.emptyList();
		}
		else
		{
			final DiscountRAO discountRao = (DiscountRAO) action;
			final RuleBasedOrderAdjustTotalActionModel actionModel = this.createOrderAdjustTotalAction(promoResult, discountRao);
			this.handleActionMetadata(action, actionModel);
			promoResult.setRetailerID(discountRao.getRetailer().getRetailerId());
			promotionActionService.createRetailerDiscountValue(discountRao, actionModel.getGuid(), order);
			this.getModelService().saveAll(promoResult, actionModel, order);
			this.recalculateIfNeeded(order);
			return Collections.singletonList(promoResult);
		}
	}
}
