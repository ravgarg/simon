package com.simon.core.product.strategies.impl;

import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.apache.commons.collections.CollectionUtils;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductCreationStrategy;
import com.mirakl.hybris.core.shop.services.ShopService;


/**
 * ExtProductCreationStrategy, this strategy adds additional business logic for creating product.
 */
public class ExtProductCreationStrategy extends DefaultProductCreationStrategy
{
	/**
	 * Extends default created product by adding {@link ShopModel} to variant and its base product. <code>shopId</code>
	 * is fetched from context and shop is fetched via {@link ShopService}
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public ProductModel createProduct(final ProductImportData data, final ProductImportFileContextData context)
			throws ProductImportException
	{
		final ProductModel product = createDefaultProduct(data, context);
		setShop(data.getShop(), product);
		setApprovalStatus(product);
		setRetailerSkuId(data.getRawProduct(), product);
		return product;
	}

	/**
	 * Sets Approval status as "UNAPPROVED". Default Approval status is "CHECK".
	 *
	 * @param product
	 *           the product
	 */
	private void setApprovalStatus(final ProductModel product)
	{
		if (CollectionUtils.isEmpty(((VariantProductModel) product).getBaseProduct().getVariants()))
		{
			((VariantProductModel) product).getBaseProduct().setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		}
		product.setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
	}

	/**
	 * Sets the retailer sku id.
	 *
	 * @param rawProduct
	 *           the raw product
	 * @param product
	 *           the product
	 */
	private void setRetailerSkuId(final MiraklRawProductModel rawProduct, final ProductModel product)
	{
		product.setRetailerSkuID(rawProduct.getSku());
		((VariantProductModel) product).getBaseProduct().setRetailerSkuID(rawProduct.getVariantGroupCode());
	}

	/**
	 * Sets the shop.
	 *
	 * @param shopModel
	 *           the context
	 * @param product
	 *           the product
	 */
	private void setShop(final ShopModel shop, final ProductModel product)
	{
		product.setShop(shop);
		((VariantProductModel) product).getBaseProduct().setShop(shop);
	}

	/**
	 * Creates the default product.
	 *
	 * @param data
	 *           the data
	 * @param context
	 *           the context
	 * @return the product model
	 * @throws ProductImportException
	 *            the product import exception
	 */
	protected ProductModel createDefaultProduct(final ProductImportData data, final ProductImportFileContextData context)
			throws ProductImportException
	{
		return super.createProduct(data, context);
	}
}
