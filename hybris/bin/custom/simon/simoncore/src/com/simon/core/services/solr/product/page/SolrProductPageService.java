package com.simon.core.services.solr.product.page;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;


public interface SolrProductPageService<STATE, ITEM, ProductDetailsData> //NOSONAR
{
	public ProductDetailsData getProductDetails(final SolrSearchQueryData searchQueryData, final PageableData pageableData);
}
