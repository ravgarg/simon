package com.simon.core.provider.impl;

import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.impl.DefaultPriceService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.price.ExtPriceService;


/**
 * Resolver to fetch Plum Price and List of prices of {@link GenericVariantProductModel}
 */
public class VariantPriceValueResolver extends AbstractValueResolver<GenericVariantProductModel, Map<String, Double>, Object>
{
	private static final String SALE_PRICE = "SALE";

	private static final String LIST_PRICE = "LIST";

	@Resource
	private DefaultPriceService defaultPriceService;

	@Resource
	private ExtPriceService priceService;

	/**
	 * Attribute mentioned in ValueProviderParameter in impex
	 */
	public static final String FETCH_STRIKEOFF_PRICE_PARAM = "fetchPriceWithStrikeOff";

	/**
	 * The default value of attribute if not found in impex
	 */
	public static final boolean FETCH_STRIKEOFF_PRICE_PARAM_DEFAULT_VALUE = false;

	/**
	 * Method called every time for each indexed property using this resolver Map containing prices is fetched from resolver
	 * context. Either Collection of list or plum price is to be indexed.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Map<String, Double>, Object> resolverContext) throws FieldValueProviderException
	{
		final Map<String, Double> priceMap = resolverContext.getData();
		if (MapUtils.isNotEmpty(priceMap))
		{
			final double listPrice = priceMap.get(SimonCoreConstants.PRODUCT_LIST_PRICE);
			final double salePrice = priceMap.get(SimonCoreConstants.PRODUCT_SALE_PRICE);
			final boolean fetchStrikeOffPrice = ValueProviderParameterUtils.getBoolean(indexedProperty, FETCH_STRIKEOFF_PRICE_PARAM,
					FETCH_STRIKEOFF_PRICE_PARAM_DEFAULT_VALUE);
			if (fetchStrikeOffPrice)
			{
				final List<String> priceValues = fetchVariantPrice(model, listPrice, salePrice);
				filterAndAddFieldValues(document, batchContext, indexedProperty, priceValues, resolverContext.getFieldQualifier());
			}
			else
			{
				final double priceToIndex = salePrice > SimonCoreConstants.ZERO_DOUBLE ? salePrice : listPrice;
				filterAndAddFieldValues(document, batchContext, indexedProperty, priceToIndex, resolverContext.getFieldQualifier());
			}
		}
	}

	/**
	 * This method fetches list of all available prices in particular product. Format them in needed format
	 *
	 * @param model
	 *           The {@link GenericVariantProductModel} for whose prices are to be indexed
	 * @param listPrice
	 *           The mandatory ListPrice of the product
	 * @param salePrice
	 *           The optional sale Price of Product
	 * @return List of Valid price values
	 */
	private List<String> fetchVariantPrice(final GenericVariantProductModel model, final double listPrice, final double salePrice)
	{
		final double msrp = model.getMsrp() != null ? model.getMsrp() : SimonCoreConstants.ZERO_DOUBLE;
		final List<String> priceValues = new ArrayList<>();

		final Map<DetailedPriceInfo, Object> priceDetail = priceService.getDetailedPriceInformation(msrp, listPrice, salePrice);
		final String salePriceValue = salePrice > SimonCoreConstants.ZERO_DOUBLE
				? new StringBuilder(SimonCoreConstants.PRODUCT_SALE_PRICE).append(SimonCoreConstants.PIPE).append(salePrice)
						.append(SimonCoreConstants.PIPE).append(Boolean.FALSE).toString()
				: StringUtils.EMPTY;
		final String listPriceValue = new StringBuilder(SimonCoreConstants.PRODUCT_LIST_PRICE).append(SimonCoreConstants.PIPE)
				.append(listPrice).append(SimonCoreConstants.PIPE).append(priceDetail.get(DetailedPriceInfo.IS_LP_STRIKEOFF))
				.toString();
		final String msrpValue = msrp > SimonCoreConstants.ZERO_DOUBLE
				? new StringBuilder(SimonCoreConstants.PRODUCT_MSRP).append(SimonCoreConstants.PIPE).append(msrp)
						.append(SimonCoreConstants.PIPE).append(priceDetail.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF)).toString()
				: StringUtils.EMPTY;

		if (priceDetail.get(DetailedPriceInfo.PERCENTAGE_SAVING) != null)
		{
			final int percentSaving = (int) priceDetail.get(DetailedPriceInfo.PERCENTAGE_SAVING);
			final String percentOff = percentSaving > SimonCoreConstants.ZERO_INT
					? SimonCoreConstants.PERCENTAGE_SAVING + SimonCoreConstants.PIPE + percentSaving
					: StringUtils.EMPTY;
			priceValues.add(percentOff);
		}
		priceValues.add(listPriceValue);
		priceValues.add(salePriceValue);
		priceValues.add(msrpValue);
		priceValues.removeIf(String::isEmpty);
		return priceValues;
	}

	/**
	 * Loads the data for all indexed properties calling this resolver A Map of String constant and corresponding prices is
	 * loaded into context
	 */
	@Override
	protected Map<String, Double> loadData(final IndexerBatchContext batchContext,
			final Collection<IndexedProperty> indexedProperties, final GenericVariantProductModel model)
			throws FieldValueProviderException
	{
		Map<String, Double> priceMap = new HashMap<>();
		final List<PriceInformation> productPrice = defaultPriceService.getPriceInformationsForProduct(model);

		if (CollectionUtils.isNotEmpty(productPrice))
		{
			double listPrice;
			double salePrice;
			priceMap = new HashMap<>();
			final ExtPriceValue extPriceValue = (ExtPriceValue) productPrice.get(0).getPriceValue();
			if (extPriceValue.getListPrice() > SimonCoreConstants.ZERO_DOUBLE)
			{
				salePrice = extPriceValue.getValue();
				listPrice = extPriceValue.getListPrice();
			}
			else
			{
				salePrice = SimonCoreConstants.ZERO_DOUBLE;
				listPrice = extPriceValue.getValue();
			}
			priceMap.put(LIST_PRICE, listPrice);
			priceMap.put(SALE_PRICE, salePrice);
		}

		return priceMap;

	}

}
