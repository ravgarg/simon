package com.simon.core.promotions;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.promotionengineservices.action.impl.AbstractRuleActionStrategy;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simon.core.promotions.service.ExtDefaultPromotionActionService;
import com.simon.core.promotions.service.impl.ExtDefaultPromotionActionServiceImpl;


/**
 * This strategy can be used to create item level discounts in case the in promotion engine applies the discount on
 * CartRao but created OrderConsumedEntry.
 */
public class BuyMoreSaveMoreStrategy extends AbstractRuleActionStrategy<RuleBasedOrderEntryAdjustActionModel>
{
	private static final Logger LOG = LoggerFactory.getLogger(BuyMoreSaveMoreStrategy.class);

	/**
	 *
	 * apply method is used to create promotionPromotionResult,RuleBasedOrderEntryAdjustAction and DiscountValues This
	 * method applied discountValues on OrderEntry
	 *
	 * @param action
	 *           the action
	 * @return the list
	 *
	 */
	public List<PromotionResultModel> apply(final AbstractRuleActionRAO action)
	{
		List<PromotionResultModel> promotionResults = Collections.emptyList();
		if (!(action instanceof DiscountRAO))
		{
			LOG.error("cannot apply {}, action is not of type DiscountRAO", this.getClass().getSimpleName());
		}
		else
		{
			final AbstractOrderEntryModel entry = ((ExtDefaultPromotionActionServiceImpl) getPromotionActionService())
					.getConsumedOrderEntry(action);
			if (entry == null)
			{
				LOG.error("cannot apply {}, orderEntry could not be found.", this.getClass().getSimpleName());
			}
			else
			{
				promotionResults = createAndApplyPromotionResult(action, entry);
			}
		}

		return promotionResults;
	}

	/**
	 * This method is called while removing promotions before calculation
	 */
	public void undo(final ItemModel action)
	{
		if (action instanceof RuleBasedOrderEntryAdjustActionModel)
		{
			this.handleUndoActionMetadata((RuleBasedOrderEntryAdjustActionModel) action);
			final AbstractOrderModel order = this.undoInternal((RuleBasedOrderEntryAdjustActionModel) action);
			this.recalculateIfNeeded(order);
		}

	}

	protected void adjustDiscountRaoValue(final AbstractOrderEntryModel entry, final DiscountRAO discountRao,
			final BigDecimal discountAmount)
	{
		if (!StringUtils.isEmpty(discountRao.getCurrencyIsoCode()) && discountRao.getAppliedToQuantity() > 0L
				|| discountRao.isPerUnit())
		{
			final BigDecimal amount = discountAmount.multiply(BigDecimal.valueOf(discountRao.getAppliedToQuantity()))
					.divide(BigDecimal.valueOf(entry.getQuantity().longValue()), 5, 4);
			discountRao.setValue(amount);
		}

	}

	protected RuleBasedOrderEntryAdjustActionModel createOrderEntryAdjustAction(final PromotionResultModel promoResult,
			final AbstractRuleActionRAO action, final AbstractOrderEntryModel entry, final BigDecimal discountAmount)
	{
		final RuleBasedOrderEntryAdjustActionModel actionModel = this.createPromotionAction(promoResult, action);
		actionModel.setAmount(discountAmount);
		actionModel.setOrderEntryNumber(entry.getEntryNumber());
		actionModel.setOrderEntryProduct(entry.getProduct());
		actionModel.setOrderEntryQuantity(Long.valueOf(this.getConsumedQuantity(promoResult)));
		return actionModel;
	}


	protected long getConsumedQuantity(final PromotionResultModel promoResult)
	{
		long consumedQuantity = 0L;
		if (CollectionUtils.isNotEmpty(promoResult.getConsumedEntries()))
		{
			consumedQuantity = promoResult.getConsumedEntries().stream()
					.mapToLong(consumedEntry -> consumedEntry.getQuantity().longValue()).sum();
		}

		return consumedQuantity;
	}

	private List<PromotionResultModel> createAndApplyPromotionResult(final AbstractRuleActionRAO action,
			final AbstractOrderEntryModel entry)
	{
		final PromotionResultModel promoResult = this.getPromotionActionService().createPromotionResult(action);
		List<PromotionResultModel> promotionResults = Collections.emptyList();
		if (promoResult == null)
		{
			LOG.error("cannot apply {}, promotionResult could not be created.", this.getClass().getSimpleName());
		}
		else
		{
			promotionResults = applyDiscountOnOrder(action, entry, promoResult);
		}
		return promotionResults;
	}

	private List<PromotionResultModel> applyDiscountOnOrder(final AbstractRuleActionRAO action,
			final AbstractOrderEntryModel entry, final PromotionResultModel promoResult)
	{
		final AbstractOrderModel order = entry.getOrder();
		List<PromotionResultModel> promotionResults = Collections.emptyList();
		if (order == null)
		{
			LOG.error("cannot apply {}, order does not exist for order entry", this.getClass().getSimpleName());
			if (this.getModelService().isNew(promoResult))
			{
				this.getModelService().detach(promoResult);
			}

		}
		else
		{
			final DiscountRAO discountRao = (DiscountRAO) action;
			final BigDecimal discountAmount = discountRao.getValue();
			this.adjustDiscountRaoValue(entry, discountRao, discountAmount);
			final RuleBasedOrderEntryAdjustActionModel actionModel = this.createOrderEntryAdjustAction(promoResult, action, entry,
					discountAmount);
			this.handleActionMetadata(action, actionModel);
			((ExtDefaultPromotionActionService) getPromotionActionService()).createDiscountValueForBuyMoreSaveMore(discountRao,
					actionModel.getGuid(), entry);
			this.getModelService().saveAll(promoResult, actionModel, order, entry);
			this.recalculateIfNeeded(order);
			promotionResults = Collections.singletonList(promoResult);
		}
		return promotionResults;
	}



}
