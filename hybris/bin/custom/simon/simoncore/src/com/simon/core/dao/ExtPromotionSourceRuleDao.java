package com.simon.core.dao;

import de.hybris.platform.promotionengineservices.model.CatForPromotionSourceRuleModel;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;

import java.util.List;


/**
 * Custom Dao to fetch the PromotionSourceRules information from the database
 */
public interface ExtPromotionSourceRuleDao
{

	/**
	 *
	 * Find the categories for which the promotions have been updated
	 *
	 * @param indexedType
	 * @param facetSearchConfig
	 * @return List of the category to promotion relations
	 * @throws SolrServiceException
	 */
	List<CatForPromotionSourceRuleModel> findUpdatedCategoriesForPromotionSourceRule(IndexedType indexedType,
			FacetSearchConfig facetSearchConfig) throws SolrServiceException;
}