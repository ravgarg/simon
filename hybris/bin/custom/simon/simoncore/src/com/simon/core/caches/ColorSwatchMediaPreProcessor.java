package com.simon.core.caches;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;

import java.net.URL;
import java.util.Map;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.MediaKeyGenerator;



/**
 * ColorSwatchMediaPreProcessor creates new {@link mediaModel} and associated Media for Color type with product if
 * present in product feed found during pre-processin.
 */
public class ColorSwatchMediaPreProcessor extends AbstractPreProcessor<MediaModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ColorSwatchMediaPreProcessor.class);

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private MediaService mediaService;
	@Autowired
	private MediaKeyGenerator mediaKeyGenerator;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String createKey(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)

	{
		final String shopId = context.getShopId();
		String key = null;
		final String variantAttributeValue = extractRawValue("variantAttributes", productValues);
		final String mediaKey = (String) mediaKeyGenerator.generateFor(variantAttributeValue);
		if (StringUtils.isNotEmpty(rawValue) && StringUtils.isNotEmpty(mediaKey))
		{
			key = shopId + SimonCoreConstants.UNDERSCORE + mediaKeyGenerator.generateFor(variantAttributeValue);
		}
		return key;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MediaModel createValue(final String attribute, final String rawValue, final Map<String, String> productValues,
			final ProductImportFileContextData context)
	{
		MediaModel colorSwatchMedia = null;
		final String colorSwatchMediaCode = createKey(attribute, rawValue, productValues, context);
		if (StringUtils.isNotEmpty(colorSwatchMediaCode))
		{
			final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE,
					Catalog.DEFAULT);
			final MediaModel existingMediaModel = getMediaCategoryFromDatabase(catalogVersion, colorSwatchMediaCode);
			colorSwatchMedia = createOrUpdateSwatchMedia(colorSwatchMediaCode, rawValue, catalogVersion, existingMediaModel);
		}

		return colorSwatchMedia;
	}


	/**
	 * Update swatch media.
	 *
	 * @param colorSwatchMediaCode
	 * @param colorSwatchUrl
	 * @param catalogVersion
	 * @param existingMediaModel
	 * @return
	 */
	private MediaModel createOrUpdateSwatchMedia(final String colorSwatchMediaCode, final String colorSwatchUrl,
			final CatalogVersionModel catalogVersion, final MediaModel existingMediaModel)
	{
		MediaModel thumbnail = null;
		if (existingMediaModel == null)
		{
			LOGGER.info("Creating Media  Swatch :{} From  color Swatch Url :{}", colorSwatchMediaCode, colorSwatchUrl);
			thumbnail = modelService.create(MediaModel.class);
			thumbnail.setCode(colorSwatchMediaCode);
			thumbnail.setCatalogVersion(catalogVersion);
			modelService.save(thumbnail);
		}
		else
		{
			thumbnail = existingMediaModel;
		}
		if (!colorSwatchUrl.equalsIgnoreCase(thumbnail.getRealFileName()))
		{
			thumbnail.setURL(colorSwatchUrl);
			thumbnail.setRealFileName(colorSwatchUrl);
			modelService.save(thumbnail);
			downloadMedia(colorSwatchUrl, thumbnail);
		}
		return thumbnail;
	}


	/**
	 * Download physical media
	 *
	 * @param url
	 *           the media url
	 * @param media
	 *           the media
	 */
	private void downloadMedia(final String url, final MediaModel media)
	{
		try
		{
			LOGGER.debug("Downloading Url :{} For Media :{}", url, media.getCode());
			mediaService.setStreamForMedia(media, new URL(url).openStream());
		}
		catch (final Exception e)
		{
			LOGGER.error("Unable to set stream for media [%s] from url [%s]" + media.getCode() + url, e);
		}
	}

	/**
	 * Gets the media from database.
	 *
	 * @param catalogVersion
	 * @param colorSwatchMediaCode
	 * @return
	 */
	private MediaModel getMediaCategoryFromDatabase(final CatalogVersionModel catalogVersion, final String colorSwatchMediaCode)
	{
		MediaModel mediaModel = null;
		try
		{
			final MediaModel media = mediaService.getMedia(catalogVersion, colorSwatchMediaCode);
			if (media != null)
			{
				mediaModel = media;
				LOGGER.debug("color swatch Media :{} Found", media.getCode());
			}
		}
		catch (final NoSuchElementException | UnknownIdentifierException exception)
		{
			LOGGER.error("color swatch Media :{} Not Found:{}", colorSwatchMediaCode, exception);
		}
		return mediaModel;
	}
}
