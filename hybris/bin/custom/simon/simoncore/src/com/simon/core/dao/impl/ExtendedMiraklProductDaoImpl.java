package com.simon.core.dao.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mirakl.hybris.core.product.daos.impl.DefaultMiraklProductDao;


/**
 * This is the implementation class of {@link DefaultMiraklProductDao} used for fetching the products for P21 service
 *
 */
public class ExtendedMiraklProductDaoImpl extends DefaultMiraklProductDao
{
	private static final String FIND_GEN_VAR_PROD_OF_SPO_CAT = "SELECT DISTINCT {gvp.pk} FROM {GenericVariantProduct AS gvp} WHERE {gvp.approvalStatus} IN ({{SELECT {pk} FROM {ArticleApprovalStatus AS aas} WHERE {aas:code} IN ('check')}}) and {gvp.CatalogVersion}=?catalogVersion";


	/**
	 * This method will be used to find all the {@link GenericVariantProductModel} from the system that have no variant
	 * type associated with it.
	 *
	 * @param modifiedAfter
	 *           The date attribute that will be used to find all the products that were modified after this date
	 * @param catalogVersion
	 *           The catalog version from which the products will be fetched
	 * @return list of {@link GenericVariantProductModel}
	 *
	 */
	@Override
	public List<ProductModel> findModifiedProductsWithNoVariantType(final Date modifiedAfter,
			final CatalogVersionModel catalogVersion)
	{
		final Map<String, Object> params = new HashMap<>();
		params.put(ProductModel.CATALOGVERSION, catalogVersion);
		params.put(ProductModel.MODIFIEDTIME, modifiedAfter);

		final StringBuilder queryString = new StringBuilder(FIND_GEN_VAR_PROD_OF_SPO_CAT);

		if (modifiedAfter != null)
		{
			queryString.append(" AND {").append(GenericVariantProductModel.MODIFIEDTIME).append("} > ?")
					.append(GenericVariantProductModel.MODIFIEDTIME);
		}

		final SearchResult<ProductModel> searchResult = getFlexibleSearchService()
				.search(new FlexibleSearchQuery(queryString.toString(), params));
		return searchResult.getResult();
	}
}
