package com.simon.core.customer.service;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.List;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;


// TODO: Auto-generated Javadoc
/**
 * Defined to save spreedly payment information and associate with customer.
 */

public interface ExtCustomerAccountService extends CustomerAccountService
{

	/**
	 * Creates the payment info subscription.
	 *
	 * @param extPaymentInfoData
	 *           the ext payment info data
	 * @return CreditCardPaymentInfoModel
	 * @throws ModelSavingException
	 *            the model saving exception
	 */
	CreditCardPaymentInfoModel createPaymentInfoSubscription(CCPaymentInfoData extPaymentInfoData) throws ModelSavingException;

	/**
	 * use to update profile info in customer model.
	 *
	 * @param customer
	 *           the customer
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	void updateProfileInfo(CustomerModel customer) throws DuplicateUidException;



	/**
	 * Update Customer my favorite.
	 *
	 * @param customer
	 *           the customer
	 * @return true, if successful
	 * @throws ModelSavingException
	 *            the model saving exception
	 */
	void updateCustomerMyFavorite(CustomerModel customer) throws ModelSavingException;


	/**
	 * Find list of all designers.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the list
	 */
	List<DesignerModel> findListOfAllDesigners(final CustomerModel customerModel);

	/**
	 * used to get last added favorite.
	 *
	 * @param customer
	 *           the customer
	 * @return ProductData
	 */
	List<List<Object>> getMyFavProductOrderByDate(CustomerModel customer);

	/**
	 * Find list of all stores.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the list
	 */
	List<ShopModel> findListOfAllStores(final CustomerModel customerModel);

	/**
	 * @param customerModel
	 * @param orderGUID
	 * @param pwd
	 * @throws DuplicateUidException
	 */
	public void convertGuestToRegisterCustomer(final CustomerModel customerModel, final String orderid)
			throws DuplicateUidException;


	/**
	 * Find list of all designers.
	 *
	 * @return the list
	 */
	List<DesignerModel> findListOfDesigners();

	/**
	 * Find List Of all Fav Deals
	 *
	 * @param customer
	 * @return {@link List of String}
	 */
	List<DealModel> getAllFavouriteDeals(final CustomerModel customer);


	/**
	 * @description Method use to register the new customer and do not trigger the customer registration email
	 * @method register
	 * @param customerModel
	 * @param password
	 */
	public void register(final CustomerModel customerModel, final String password) throws DuplicateUidException;
}
