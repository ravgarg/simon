package com.simon.core.mirakl.services.impl;

import static java.util.concurrent.TimeUnit.MINUTES;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.TenantAwareThreadFactory;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.offer.MiraklOffersExportRequest;
import com.mirakl.hybris.core.i18n.services.CurrencyService;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.dto.OfferImportMetaData;
import com.simon.core.enums.PriceType;
import com.simon.core.exceptions.OfferImportException;
import com.simon.core.mirakl.dao.ExtOfferDao;
import com.simon.core.mirakl.populators.ExtStockPopulator;
import com.simon.core.mirakl.services.ExtOfferImportService;
import com.simon.core.services.ExtProductService;


/**
 * ExtOfferImportServiceImpl, Mirakl API based implementation of {@link ExtOfferImportService}. This fetches list of
 * {@link MiraklExportOffer} from Mirakl and converts them to {@link PriceRowModel} to store list price and selling
 * price and {@link StockLevelModel} to store Stocks.
 */
public class ExtOfferImportServiceImpl implements ExtOfferImportService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtOfferImportServiceImpl.class);

	@Autowired
	private MiraklMarketplacePlatformFrontApi miraklOperatorApi;

	@Autowired
	private ExtOfferDao extOfferDao;

	@Autowired
	private ExtProductService extProductService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private UnitService unitService;

	@Autowired
	private WarehouseService warehouseService;

	@Autowired
	private UserService userService;

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private CurrencyService currencyService;

	@Resource
	private Converter<MiraklExportOffer, PriceRowModel> listPriceConverter;

	@Resource
	private Converter<MiraklExportOffer, PriceRowModel> sellingPriceConverter;

	@Resource
	private Converter<MiraklExportOffer, StockLevelModel> extStockConverter;

	private static final long TIMEOUT = 1440L;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void importAllOffers(final Date missingOffersDeletionDate, final OfferImportMetaData offerImportMetaData)
			throws OfferImportException
	{
		LOGGER.info("Full Offer Import Started");
		final List<MiraklExportOffer> miraklExportOffers = getMiraklOffers(null);
		importOffers(miraklExportOffers, offerImportMetaData);
		if (offerImportMetaData.isDeleteMissingOffers())
		{
			LOGGER.info("Full Offer Import Completed - Deleting Missing Offers");
			removeMissingOffersAndStocks(miraklExportOffers, missingOffersDeletionDate);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void importOffersUpdatedSince(final Date lastImportDate, final OfferImportMetaData offerImportMetaData)
			throws OfferImportException
	{
		LOGGER.info("Partial Offer Import Started");
		importOffers(getMiraklOffers(lastImportDate), offerImportMetaData);
		LOGGER.info("Partial Offer Import Completed");
	}

	/**
	 * Gets raw mirakl offers list of {@link MiraklExportOffer} via {@link MiraklMarketplacePlatformFrontApi}.
	 *
	 * @param lastImportDate
	 *           the last import date
	 * @return the mirakl offers
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	private List<MiraklExportOffer> getMiraklOffers(final Date lastImportDate) throws OfferImportException
	{
		final MiraklOffersExportRequest miraklOffersExportRequest = new MiraklOffersExportRequest();
		miraklOffersExportRequest.setLastRequestDate(lastImportDate);

		final List<MiraklExportOffer> miraklExportOffers;
		try
		{
			miraklExportOffers = miraklOperatorApi.exportOffers(miraklOffersExportRequest);
		}
		catch (final MiraklApiException apiException)
		{
			throw new OfferImportException("Exception Occured In Importing Offers From Mirakl", apiException);
		}
		return miraklExportOffers;
	}

	/**
	 * Import offers, this method import offers in batches and does it in parallel using parallel worker threads.
	 * <p>
	 * Batch size and no of worker threads are read from {@link OfferImportMetaData#getBatchSize()} and
	 * {@link OfferImportMetaData#getNoOfWorkers()} simultaneously.
	 *
	 * @param miraklExportOffers
	 *           the mirakl export offers to import
	 * @param offerImportMetaData
	 */
	private void importOffers(final List<MiraklExportOffer> miraklExportOffers, final OfferImportMetaData offerImportMetaData)
	{
		final ExecutorService serviceExecutor = getExecutorService(offerImportMetaData);
		final int batchSize = offerImportMetaData.getBatchSize();

		final List<List<MiraklExportOffer>> partitions = Lists.partition(miraklExportOffers, batchSize);
		try
		{
			for (final List<MiraklExportOffer> partition : partitions)
			{
				serviceExecutor.execute(() -> importOffersIntoPriceRowAndStockLevel(partition));
			}
		}
		finally
		{
			shutdownWorkers(serviceExecutor);
		}
	}


	/**
	 * Import offers into price row and stock level. Converts each individual <code>MiraklExportOffer</code> into two
	 * price rows {@link PriceRowModel} and one stock level {@link StockLevelModel}.
	 * <p>
	 * Origin price in <code>MiraklExportOffer</code> is converted to <code>PriceRow</code> of type
	 * {@link PriceType.LIST} and discounted price is converted into to a price row of type {@link PriceType.SALE}
	 * <p>
	 * Quantity in <code>MiraklExportOffer</code> is converted to <code>StockLevel</code>
	 * <p>
	 * For updates on prices and stocks same entities are updated
	 *
	 * @param miraklExportOffer
	 *           the mirakl export offer used to create price and stocks
	 */
	private void importOffersIntoPriceRowAndStockLevel(final List<MiraklExportOffer> miraklExportOffer)
	{
		CurrencyModel currency = currencyService.getCurrencyForCode(miraklExportOffer.get(0).getCurrencyIsoCode().name());

		final UnitModel unit = unitService.getUnitForCode("pieces");

		final Set<String> productIdList = miraklExportOffer.stream().map(MiraklExportOffer::getProductSku)
				.collect(Collectors.toSet());

		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);

		final List<ProductModel> products = extProductService.getProductsForCodes(catalogVersion, productIdList);
		final Map<String, ProductModel> productCodesMap = products.stream()
				.collect(Collectors.toMap(ProductModel::getCode, Function.identity()));

		// fetches all the list price rows for products
		final List<PriceRowModel> listPrices = extOfferDao.findPricesForProductCodes(productIdList, PriceType.LIST);
		final Map<String, PriceRowModel> listPriceMap = listPrices.stream()
				.collect(Collectors.toMap(PriceRowModel::getProductId, Function.identity()));

		final List<PriceRowModel> sellPrices = extOfferDao.findPricesForProductCodes(productIdList, PriceType.SALE);
		final Map<String, PriceRowModel> sellPriceMap = sellPrices.stream()
				.collect(Collectors.toMap(PriceRowModel::getProductId, Function.identity()));

		final List<PriceRowModel> priceRows = new ArrayList<>();
		final List<StockLevelModel> stockLevels = new ArrayList<>();

		for (final MiraklExportOffer offer : miraklExportOffer)
		{
			final String productSku = offer.getProductSku();

			if (productCodesMap.containsKey(productSku))
			{
				LOGGER.info("Importing Offer For Product : {}", productSku);
				currency = getUpdatedCurrency(currency, offer);

				// Update List Price
				PriceRowModel updatedListPrice = listPriceMap.get(productSku);
				updatedListPrice = getUpdatedPrice(listPriceConverter, updatedListPrice, offer, currency, unit);
				priceRows.add(updatedListPrice);
				listPriceMap.put(productSku, updatedListPrice);
				LOGGER.debug("Updating List Price {} For Product : {}", updatedListPrice.getPrice(), productSku);

				// Update Selling Price
				if (offer.getDiscount() != null)
				{
					PriceRowModel updatedSellingPrice = sellPriceMap.get(productSku);
					updatedSellingPrice = getUpdatedPrice(sellingPriceConverter, updatedSellingPrice, offer, currency, unit);
					updatedSellingPrice.setListPrice(updatedListPrice);
					priceRows.add(updatedSellingPrice);
					sellPriceMap.put(productSku, updatedSellingPrice);
					LOGGER.debug("Updating Selling Price {} For Product : {}", updatedSellingPrice.getPrice(), productSku);
				}
				// Update Stock Level
				final StockLevelModel updatedStock = getUpdatedStock(offer, productCodesMap.get(productSku));
				if (updatedStock != null)
				{
					stockLevels.add(updatedStock);
				}
			}
			else
			{
				LOGGER.warn("Invalid Offer Recevied. Product : {} does not exist in system", productSku);
			}
		}
		modelService.saveAll(priceRows);
		modelService.saveAll(stockLevels);
		modelService.saveAll(products);
	}

	/**
	 * Gets the updated price. If existing price is null this creates new <code>PriceRow</code> from
	 * <code>modelService</code>.
	 * <p>
	 * For both scenarios whether existing price is present or not present it is updated from
	 * <code>MiraklExportOffer</code> via <code>converter</code>
	 *
	 * @param converter
	 *           the converter to update price row from offer
	 * @param price
	 *           the price existing price
	 * @param offer
	 *           the offer to convert into price row
	 * @param currency
	 *           the currency of price
	 * @param unit
	 *           the unit of price
	 * @return the updated price
	 */
	private PriceRowModel getUpdatedPrice(final Converter<MiraklExportOffer, PriceRowModel> converter, final PriceRowModel price,
			final MiraklExportOffer offer, final CurrencyModel currency, final UnitModel unit)
	{
		PriceRowModel existingPrice = price;
		PriceRowModel updatedPrice;
		if (null == existingPrice)
		{
			existingPrice = modelService.create(PriceRowModel.class);
			existingPrice.setProductId(offer.getProductSku());
		}
		updatedPrice = converter.convert(offer, existingPrice);
		updatedPrice.setCurrency(currency);
		updatedPrice.setUnit(unit);
		return updatedPrice;
	}

	/**
	 * Gets the updated stock. If no existing stock is present for given <code>Product</code> it is create via
	 * <code>ModelService</code> and default warehouse is associated with it.
	 * <p>
	 * For both existing/non-existing stock, it is updated from <code>MiraklExportOffer</code> via
	 * {@link ExtStockPopulator}
	 *
	 * @param offer
	 *           the offer to update stock from
	 * @param product
	 *           the product to update stock for
	 * @return the updated stock
	 */
	private StockLevelModel getUpdatedStock(final MiraklExportOffer offer, final ProductModel product)
	{
		StockLevelModel stock = null;
		LOGGER.debug("Updating Stock {} For Product : {}", offer.getQuantity(), offer.getProductSku());
		final Set<StockLevelModel> stocks = product.getStockLevels();
		if (CollectionUtils.isEmpty(stocks))
		{
			LOGGER.debug("No Stock Found For Product : {}. Creating New Stock");
			stock = modelService.create(StockLevelModel.class);
			stock.setWarehouse(warehouseService.getWarehouseForCode("simon_warehouse"));
			final Set<StockLevelModel> stockLevel = new HashSet<>();
			stockLevel.add(stock);
			product.setStockLevels(stockLevel);
		}
		else
		{
			stock = stocks.iterator().next();
		}
		stock = extStockConverter.convert(offer, stock);
		return stock;
	}

	/**
	 * Removes the missing offers and stocks.
	 *
	 * @param miraklExportOffers
	 *           the mirakl export offers
	 * @param missingOffersDeletionDate
	 *           the missing offers deletion date
	 */
	private void removeMissingOffersAndStocks(final List<MiraklExportOffer> miraklExportOffers,
			final Date missingOffersDeletionDate)
	{
		final List<String> productCodes = miraklExportOffers.stream().map(MiraklExportOffer::getProductSku)
				.collect(Collectors.toList());

		final List<PriceRowModel> missingPriceRows = extOfferDao.findPricesModifiedBeforeDate(missingOffersDeletionDate);
		final List<StockLevelModel> missingStocks = extOfferDao.findStocksModifiedBeforeDate(missingOffersDeletionDate);

		List<PriceRowModel> priceRowsToRemove = new ArrayList<>(missingPriceRows);
		List<StockLevelModel> stockLevelsToRemove = new ArrayList<>(missingStocks);

		priceRowsToRemove = priceRowsToRemove.stream().filter(price -> !productCodes.contains(price.getProductId()))
				.collect(Collectors.toList());
		stockLevelsToRemove = stockLevelsToRemove.stream().filter(stock -> !productCodes.contains(stock.getProductCode()))
				.collect(Collectors.toList());

		modelService.removeAll(priceRowsToRemove);
		modelService.removeAll(stockLevelsToRemove);
	}

	/**
	 * Gets the updated currency.
	 *
	 * @param currency
	 *           the currency
	 * @param offer
	 *           the offer
	 * @return the updated currency
	 */
	private CurrencyModel getUpdatedCurrency(final CurrencyModel currency, final MiraklExportOffer offer)
	{
		CurrencyModel updatedCurrency = currency;
		// If existing fetched currency is not equal to existing currency
		if (!offer.getCurrencyIsoCode().toString().equals(currency.getIsocode()))
		{
			updatedCurrency = currencyService.getCurrencyForCode(offer.getCurrencyIsoCode().name());
		}
		return updatedCurrency;
	}

	/**
	 * Gets the executor service.
	 *
	 * @param offerImportMetaData
	 *
	 * @return the executor service
	 */
	protected ExecutorService getExecutorService(final OfferImportMetaData offerImportMetaData)
	{
		return Executors.newFixedThreadPool(offerImportMetaData.getNoOfWorkers(), getThreadFactory(offerImportMetaData));
	}

	/**
	 * Gets the thread factory.
	 *
	 * @param offerImportMetaData
	 *
	 * @return the thread factory
	 */
	protected ThreadFactory getThreadFactory(final OfferImportMetaData offerImportMetaData)
	{
		final TenantAwareThreadFactory threadFactory = new TenantAwareThreadFactory(Registry.getCurrentTenant())
		{
			@Override
			protected void afterPrepareThread()
			{
				userService.setCurrentUser(offerImportMetaData.getSessionUser());
				commonI18NService.setCurrentLanguage(offerImportMetaData.getSessionLanguage());
				commonI18NService.setCurrentCurrency(offerImportMetaData.getSessionCurrency());
			}
		};
		return new ThreadFactoryBuilder().setThreadFactory(threadFactory).setNameFormat("Simon Offer Import - %d").build();
	}

	/**
	 * Shutdown workers.
	 *
	 * @param serviceExecutor
	 *           the service executor
	 */
	private void shutdownWorkers(final ExecutorService serviceExecutor)
	{
		serviceExecutor.shutdown();
		try
		{
			serviceExecutor.awaitTermination(TIMEOUT, MINUTES);
		}
		catch (final InterruptedException e)
		{
			LOGGER.warn("Interruption occured while waiting for the workers to finish", e);
			serviceExecutor.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}
