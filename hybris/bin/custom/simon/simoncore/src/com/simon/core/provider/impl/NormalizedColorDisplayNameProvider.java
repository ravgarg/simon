package com.simon.core.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider;
import de.hybris.platform.solrfacetsearch.search.SearchQuery;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.SwatchColorEnum;


/**
 * This class is a name provider class
 */
public class NormalizedColorDisplayNameProvider extends AbstractFacetValueDisplayNameProvider
{
	private static final Logger LOGGER = Logger.getLogger(NormalizedColorDisplayNameProvider.class);
	@Resource
	private TypeService typeService;
	@Resource
	private EnumerationService enumerationService;
	@Resource
	private MediaService mediaService;

	/*
	 * This method set color swatch image in name filed of facet value data (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.solrfacetsearch.provider.impl.AbstractFacetValueDisplayNameProvider#getDisplayName(de.hybris.
	 * platform.solrfacetsearch.search.SearchQuery, de.hybris.platform.solrfacetsearch.config.IndexedProperty,
	 * java.lang.String)
	 */
	@Override
	public String getDisplayName(final SearchQuery query, final IndexedProperty property, final String facetValue)
	{
		String normalizedColorSwatchURL = StringUtils.EMPTY;
		final HybrisEnumValue enumObj = enumerationService.getEnumerationValue(SwatchColorEnum.class, facetValue);

		if (enumObj != null)
		{
			final EnumerationValueModel enumerationValueModel = typeService.getEnumerationValue(enumObj);
			if (enumerationValueModel.getIcon() != null)
			{
				normalizedColorSwatchURL = enumerationValueModel.getIcon().getURL();
			}
			else
			{
				MediaModel swatchMedia = null;
				try
				{
					swatchMedia = mediaService.getMedia(facetValue.toLowerCase() + SimonCoreConstants.MEDIA_CODE_SWATCH_SUFFIX);
				}
				catch (final UnknownIdentifierException e)
				{
					LOGGER.error("Media not found ", e);
				}
				if (null != swatchMedia)
				{
					normalizedColorSwatchURL = swatchMedia.getURL();
				}
			}
		}
		return normalizedColorSwatchURL;
	}

}
