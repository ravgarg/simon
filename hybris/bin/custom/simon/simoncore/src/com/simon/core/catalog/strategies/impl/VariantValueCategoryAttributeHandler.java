package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.base.Splitter;
import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;


/**
 * VariantValueCategoryAttributeHandler associates Variant Value Categories present in feed as
 * <code>superCategories</code> on imported products and corresponding Variant Category as <code>superCategories</code>
 * on base product.
 */
public class VariantValueCategoryAttributeHandler extends MapTypeAttributeHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(VariantValueCategoryAttributeHandler.class);

	private static final int VALID_DELIMIS = 2;

	@Autowired
	private CategoryService categoryService;

	@Resource
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	@Resource
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	/**
	 * Fetches {@link VariantValueCategoryModel} for variant category code and associates it with product.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		if (StringUtils.isNotEmpty(attribute.getValue()))
		{
			super.setValue(attribute, data, context);

			final MiraklCoreAttributeModel miraklCoreAttribute = attribute.getCoreAttribute();

			final GenericVariantProductModel variantProduct = (GenericVariantProductModel) determineOwner(miraklCoreAttribute, data,
					context);
			final Map<String, String> variantAttributeRawMap = getVariantAttributesRawMap(attribute.getValue(),
					miraklCoreAttribute.getTypeParameter());

			if (variantAttributeRawMap != null)
			{
				final CatalogVersionModel productCatalogVersion = modelService
						.get(context.getGlobalContext().getProductCatalogVersion());

				for (final Entry<String, String> variantAttributeRawEntry : variantAttributeRawMap.entrySet())
				{
					final String variantCategoryName = variantAttributeRawEntry.getKey();
					final String variantValueCategoryName = variantAttributeRawEntry.getValue();
					updateOrCreateCategories(variantValueCategoryName, variantCategoryName, variantProduct, productCatalogVersion);
				}
			}
		}
	}

	/**
	 * Update or create categories. This checks if {@link VariantValueCategoryModel} for given
	 * <code>variantCategoryCode</code> is present in database. If it's present it is associated with product else
	 * {@link ProductVariantValueCategoryCache} is populated for given category
	 *
	 * @param variantValueCategoryName
	 *           the variant value category raw
	 * @param product
	 *           the product
	 * @param productCatalogVersion
	 *           the product catalog version
	 * @param parentVariantCategory
	 * @param variantCategoryCode
	 *           the variant category code
	 */
	private void updateOrCreateCategories(final String variantValueCategoryName, final String variantCategoryName,
			final GenericVariantProductModel product, final CatalogVersionModel productCatalogVersion)
	{
		final String variantCategoryCode = (String) variantCategoryKeyGenerator.generateFor(variantCategoryName);
		final String variantValueCategoryCode = (String) variantValueCategoryKeyGenerator.generateFor(variantValueCategoryName)
				+ "-" + variantCategoryCode;

		final VariantCategoryModel variantCategory = getVariantCategoryFromDatabase(productCatalogVersion, variantCategoryCode);
		final VariantValueCategoryModel variantValueCategory = getVariantValueCategoryFromDatabase(productCatalogVersion,
				variantValueCategoryCode);
		updateCategories(product.getBaseProduct(), variantCategory);
		updateCategories(product, variantValueCategory);
	}

	/**
	 * Updated categories. Updates {@link VariantValueCategoryModel} in product {@link VariantCategoryModel} in base product
	 * as super-categories.
	 *
	 * @param product
	 *           the product
	 * @param persistedVariantValueCategory
	 *           the persisted variant value category
	 */
	private void updateCategories(final ProductModel product, final CategoryModel persistedVariantValueCategory)
	{
		final Set<CategoryModel> updatedSuperCategories = new HashSet<>();
		Optional.ofNullable(product.getSupercategories()).ifPresent(updatedSuperCategories::addAll);
		updatedSuperCategories.add(persistedVariantValueCategory);
		product.setSupercategories(updatedSuperCategories);

	}

	private VariantCategoryModel getVariantCategoryFromDatabase(final CatalogVersionModel productCatalogVersion,
			final String variantCategoryCode)
	{
		VariantCategoryModel variantCategory = null;
		try
		{
			final CategoryModel category = categoryService.getCategoryForCode(productCatalogVersion, variantCategoryCode);
			if (category instanceof VariantCategoryModel)
			{
				variantCategory = (VariantCategoryModel) category;
			}
		}
		catch (final UnknownIdentifierException identifierException)
		{
			LOGGER.debug("Variant Category With Code :{} Not Found :{}", variantCategoryCode, identifierException);
		}
		return variantCategory;
	}

	/**
	 * Gets the variant value category from database.
	 *
	 * @param product
	 *           the product
	 * @param productCatalogVersion
	 *           the product catalog version
	 * @param variantValueCategoryCode
	 *           the variant value category code
	 * @return the variant value category from database
	 */
	private VariantValueCategoryModel getVariantValueCategoryFromDatabase(final CatalogVersionModel productCatalogVersion,
			final String variantValueCategoryCode)
	{
		VariantValueCategoryModel variantValueCategory = null;
		try
		{
			final CategoryModel category = categoryService.getCategoryForCode(productCatalogVersion, variantValueCategoryCode);
			if (category instanceof VariantValueCategoryModel)
			{
				variantValueCategory = (VariantValueCategoryModel) category;
			}
		}
		catch (final UnknownIdentifierException identifierException)
		{
			LOGGER.debug("Variant Value Category With Code :{} Not Found:{}", variantValueCategoryCode, identifierException);
		}
		return variantValueCategory;
	}

	private Map<String, String> getVariantAttributesRawMap(final String variantRawText, final String delimiters)
	{
		Map<String, String> variantAttributesRawMap = null;
		if (StringUtils.isNotEmpty(variantRawText) && StringUtils.isNotEmpty(delimiters) && delimiters.length() == VALID_DELIMIS)
		{
			final char[] delims = delimiters.toCharArray();
			variantAttributesRawMap = Splitter.on(delims[0]).withKeyValueSeparator(delims[1]).split(variantRawText);
		}
		return variantAttributesRawMap;
	}
}
