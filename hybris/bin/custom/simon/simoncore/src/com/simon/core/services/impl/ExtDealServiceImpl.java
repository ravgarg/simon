package com.simon.core.services.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.simon.core.dao.ExtDealDao;
import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;


/**
 * DefaultDealService, default implementation of {@link ExtDealService}.
 */
public class ExtDealServiceImpl implements ExtDealService
{

	/** The deal dao. */
	@Resource
	private ExtDealDao extDealDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DealModel getDealForCode(final String code)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", code);
		return extDealDao.findDealByCode(code);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DealModel> findListOfDeals()
	{
		return extDealDao.findAllDeals();
	}

	/**
	 * Gets the list of dealsModel. Returns matching deal for <code>code</code>
	 *
	 * @param objects
	 *           the deal <code>code</code>
	 * @return the {@link dealModel} for deal <code>code</code>
	 */
	@Override
	public List<DealModel> getListOfDealsForCodes(final List<String> objects)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("code", objects);
		return extDealDao.findListOfDealForCodes(objects);
	}

}
