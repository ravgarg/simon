package com.simon.core.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.dto.OrderExportData;
import com.simon.core.enums.OrderExportType;


/**
 * This class populates {@link OrderExportData} DTO from {@link OrderEntryModel}
 */
public class OrderExportPopulator implements Populator<OrderEntryModel, OrderExportData>
{
	private static final Logger LOG = LoggerFactory.getLogger(OrderExportPopulator.class);

	@Override
	public void populate(final OrderEntryModel orderEntryModel, final OrderExportData orderExportData)
	{
		try
		{
			orderExportData.setSellerId(orderEntryModel.getShopId());
			orderExportData.setProductId(orderEntryModel.getProduct().getCode());
			orderExportData.setProductSku(orderEntryModel.getProduct().getRetailerSkuID());
			orderExportData.setBaseProductId(getBaseProductId(orderEntryModel.getProduct()));
			orderExportData.setProductName(orderEntryModel.getProduct().getName());
			orderExportData.setProductVariantFields(getProductVarient(orderEntryModel.getProduct()).toString());
			orderExportData.setProductPrice(orderEntryModel.getBasePrice());
			final ShopModel shopModel = orderEntryModel.getProduct().getShop();
			orderExportData.setRetailerPrefix(
					StringUtils.isNotEmpty(shopModel.getPreFix()) ? shopModel.getPreFix() : shopModel.getName().substring(0, 4));
			final OrderExportType orderExportType = orderEntryModel.getProduct().getShop().getOrderExportType();
			if (null != orderExportType)
			{
				orderExportData.setBotExport(orderExportType);
			}
			final ConsignmentEntryModel consignment = orderEntryModel.getConsignmentEntries().iterator().next();
			orderExportData.setSpoOrderId(consignment.getConsignment().getCode());
			orderExportData.setSpoOrderLineId(consignment.getMiraklOrderLineId());
		}
		catch (final Exception ex)
		{
			LOG.error("Order Export For Order Status:: Order Export populator error occured at line item for Retailer's {}",
					orderEntryModel.getShopName(), ex);
		}
	}

	protected StringBuilder getProductVarient(final ProductModel productModel)
	{
		final StringBuilder productVarientField = new StringBuilder();

		if (productModel instanceof GenericVariantProductModel)
		{
			final GenericVariantProductModel genericVariantProductModel = (GenericVariantProductModel) productModel;
			for (final CategoryModel category : genericVariantProductModel.getSupercategories())
			{
				if (category instanceof VariantValueCategoryModel)
				{
					productVarientField.append(category.getName());
					productVarientField.append(SimonCoreConstants.PIPE);
				}

			}
			productVarientField.deleteCharAt(productVarientField.lastIndexOf(SimonCoreConstants.PIPE));
		}

		return productVarientField;
	}

	/**
	 * By this method want to get base Product id (Retailer Id) without Store Pre Fix AEROFCT-40015339 e.g. 40015339
	 * COLE-03504 e.g. 03504
	 *
	 * @param productModel
	 * @return String, the value of base-product-id should be without prefix
	 */
	private String getBaseProductId(final ProductModel productModel)
	{
		String baseProductId = StringUtils.EMPTY;
		if (productModel instanceof GenericVariantProductModel)
		{
			final ProductModel baseProdModel = ((GenericVariantProductModel) productModel).getBaseProduct();

			if (StringUtils.isNotBlank(baseProdModel.getRetailerSkuID()))
			{
				baseProductId = baseProdModel.getRetailerSkuID();
			}
			else
			{
				baseProductId = baseProdModel.getCode();
				baseProductId = baseProductId.substring(
						baseProductId.indexOf(SimonCoreConstants.HYPHEN) + SimonCoreConstants.FIRST_INT, baseProductId.length());
			}
		}
		return baseProductId;
	}

}
