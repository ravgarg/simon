package com.simon.core.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


/**
 * Utility class that uses for Move Order Export from NFS mount to SFTP
 */
public class TransferFileToSftpUtils
{
	private final String privateKey;
	private final String host;
	private final String user;
	private final String sourcePathLocalDirectory;
	private final String targetPathSftpDirectory;
	private static final Logger LOGGER = LoggerFactory.getLogger(TransferFileToSftpUtils.class);

	/**
	 * TransferFileToSftpUtils class constructor
	 *
	 * @param privateKey
	 * @param host
	 * @param user
	 * @param sourcePathLocalDirectory
	 * @param targetPathSftpDirectory
	 */
	public TransferFileToSftpUtils(final String privateKey, final String host, final String user,
			final String sourcePathLocalDirectory, final String targetPathSftpDirectory)
	{
		this.privateKey = privateKey;
		this.host = host;
		this.user = user;
		this.sourcePathLocalDirectory = sourcePathLocalDirectory;
		this.targetPathSftpDirectory = targetPathSftpDirectory;
	}

	/**
	 * This is an utility method. By this method move Order Export CSV File from NFS mount to SFTP
	 *
	 * @return boolean
	 * @throws IOException
	 */
	public boolean moveFilesToSftp()
	{
		boolean result = false;
		final List<String> fileNamesList = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(sourcePathLocalDirectory),
				path -> path.toString().endsWith(".csv")))
		{
			directoryStream.forEach(filePath -> fileNamesList.add(filePath.toString()));
			if (CollectionUtils.isNotEmpty(fileNamesList))
			{
				final JSch jsch = new JSch();
				jsch.addIdentity(privateKey);
				LOGGER.info("Order Export For Order Status:: identity added ");
				final Session session = jsch.getSession(user, host, 22);
				session.setConfig("StrictHostKeyChecking", "no");
				session.connect();
				LOGGER.info("Order Export For Order Status:: session connected.....");
				final Channel channel = session.openChannel("sftp");
				channel.setInputStream(System.in);
				channel.setOutputStream(System.out); //NOSONAR
				channel.connect();
				LOGGER.info("Order Export For Order Status:: shell channel connected....");
				final ChannelSftp c = (ChannelSftp) channel;

				for (final String fileName : fileNamesList)
				{
					c.put(fileName, targetPathSftpDirectory);
					LOGGER.info("Order Export For Order Status:: Moved Order Export from NFS mount to SFTP Successfully! {}",
							fileName);
					final File file = new File(fileName);
					result = deleteFile(file);
				}
				c.exit();
				session.disconnect();
				LOGGER.info("Order Export For Order Status:: All Order Export CSV files moved from NFS mount to SFTP Successfully!");
			}
		}
		catch (IOException | JSchException | SftpException e)
		{
			LOGGER.error("Order Export For Order Status:: Error Occured :: Try to move Order Export from NFS mount to SFTP Failed!",
					e);
		}
		return result;
	}

	private boolean deleteFile(final File file)
	{
		boolean result = false;
		if (file.delete())
		{
			result = true;
		}

		return result;
	}

}
