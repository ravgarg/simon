package com.simon.core.services;

import java.util.List;

import com.simon.core.model.DesignerModel;


/**
 * DesignerService service for {@link DesignerModel}. Contains abstract methods for operations on {@link DesignerModel}
 */
public interface DesignerService
{

	/**
	 * Gets the designer for code. Returns matching designer for <code>code</code>
	 *
	 * @param code
	 *           the designer <code>code</code>
	 * @return the {@link DesignerModel} for designer <code>code</code>
	 */
	DesignerModel getDesignerForCode(String code);

	/**
	 * Gets the list of designersModel. Returns matching designer for <code>code</code>
	 *
	 * @param objects
	 *           the designer <code>code</code>
	 * @return the {@link DesignerModel} for designer <code>code</code>
	 */
	List<DesignerModel> getListOfDesignerForCodes(List<String> objects);
}
