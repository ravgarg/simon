package com.simon.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.facades.account.data.DesignerData;


/**
 * This class acts as utility class. It contains common methods that can be use across the project.
 */
public class CommonUtils
{
	private static final Logger LOG = Logger.getLogger(CommonUtils.class);

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

	private CommonUtils()
	{
		// empty
	}

	/**
	 * Creates a new list from an existing non-modifiable list.
	 *
	 * @param existingList
	 * @return newList which contains existing element
	 */
	public static <T> List<T> createArrayListFrom(final List<T> existingList)
	{
		final List<T> newList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(existingList))
		{
			newList.addAll(existingList);
		}
		return newList;
	}

	/**
	 * Creates a new map from an existing non-modifiable map.
	 *
	 * @param <S>
	 *
	 * @param existingMap
	 * @return newMap which contains existing element
	 */
	public static <T> Map<String, T> createMapFrom(final Map<String, T> existingMap)
	{
		final Map<String, T> newMap = new HashMap<>();
		if (MapUtils.isNotEmpty(existingMap))
		{
			newMap.putAll(existingMap);
		}

		return newMap;
	}

	/**
	 * Returns the double value for the given key, if key is present else returns 0.
	 *
	 * @param map
	 * @param key
	 * @return Returns the double value for the given key, if key is present else returns 0.
	 */
	public static double getValueFromMap(final Map<String, Double> map, final String key)
	{

		double returnValue = 0.0;
		if (MapUtils.isNotEmpty(map) && map.containsKey(key))
		{
			returnValue = map.get(key).doubleValue();
		}
		return returnValue;

	}


	/**
	 * Sort DesignerData by Name.
	 *
	 * @param designer
	 *           the products
	 * @return the date
	 */
	public static List<DesignerData> getDesignerOrderByName(final List<DesignerData> designer)
	{

		Collections.sort(designer, (o1, o2) -> o1.getName().compareTo(o2.getName()));

		return designer;
	}

	public static String format(final String arg, final Object... arg0)
	{
		return (new Formatter()).format(arg, arg0).toString();
	}

	/**
	 * Convert string to date.
	 *
	 * @param date
	 *           the date
	 * @return the date
	 */
	public static Date convertStringToDate(final String date)
	{
		final DateFormat df = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
		try
		{
			return df.parse(date);
		}
		catch (final ParseException e)
		{
			LOG.error("Error in string to date converter :" + e);
		}
		return new Date();
	}

	/**
	 * Replace ascii with html code.
	 *
	 * @param code
	 *           the code
	 * @return the string
	 */
	public static String replaceAsciiWithHtmlCode(final String code)
	{
		return code.replaceAll("'", "&apos;").replaceAll(String.valueOf('"'), "\"").replaceAll("&quot;", "\"");
	}

	/**
	 * Creating String date for OrderModel.MODIFIEDTIME and format is YYYY-MM-DD HH24:MM:SS. This would be start Date
	 * Time for any specific Date
	 *
	 * @param startDay
	 *           int
	 * @param startHours
	 *           String
	 * @return the string
	 */
	public static String getStartDate(final int startDay, final String startHours)
	{
		final StringBuilder sb = new StringBuilder();
		final Calendar endCal = Calendar.getInstance();
		endCal.add(Calendar.DATE, -startDay);
		sb.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(endCal.getTime()));
		sb.append(SimonCoreConstants.SPACE);
		sb.append(startHours);
		sb.append(SimonCoreConstants.COLON);
		sb.append(SimonCoreConstants.DOUBLE_ZERO);
		sb.append(SimonCoreConstants.COLON);
		sb.append(SimonCoreConstants.DOUBLE_ZERO);
		return sb.toString();
	}

	/**
	 * Creating String date for OrderModel.MODIFIEDTIME and format is YYYY-MM-DD HH24:MM:SS. This would be end Date Time
	 * for any specific Date
	 *
	 * @param endtDay
	 *           int
	 * @param endHours
	 *           String
	 * @return the string
	 */
	public static String getEndDate(final int endtDay, final String endHours)
	{
		final StringBuilder sb = new StringBuilder();
		final Calendar endCal = Calendar.getInstance();
		endCal.add(Calendar.DATE, -endtDay);
		sb.append(new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(endCal.getTime()));
		sb.append(SimonCoreConstants.SPACE);
		sb.append(endHours);
		sb.append(SimonCoreConstants.COLON);
		sb.append("59");
		sb.append(SimonCoreConstants.COLON);
		sb.append("59");
		return sb.toString();
	}

	/**
	 * This method to populate valid of format Valid like Oct. 7-10
	 *
	 * @param startDate
	 *           The Start date
	 * @param endDate
	 *           The end Date
	 * @return String Return valid date of format like Oct. 7-10
	 */
	public static String formateOfferValid(final Date startDate, final Date endDate)
	{

		StringBuilder validity = new StringBuilder();
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM", Locale.US);
		if (null != startDate && null != endDate)
		{
			final Calendar cal = Calendar.getInstance();
			cal.setTime(startDate);
			final String startDateMonth = simpleDateFormat.format(startDate);
			final int startDateDay = cal.get(Calendar.DAY_OF_MONTH);
			cal.setTime(endDate);
			final String endDateMonth = simpleDateFormat.format(endDate);
			final int endDateDay = cal.get(Calendar.DAY_OF_MONTH);
			if (startDateMonth.equalsIgnoreCase(endDateMonth))
			{
				validity = validity.append("Valid ").append(startDateMonth).append(". ").append(startDateDay).append("-")
						.append(endDateDay);
			}
			else
			{
				validity = validity.append("Valid ").append(startDateMonth).append(". ").append(startDateDay).append(" - ")
						.append(endDateMonth).append(". ").append(endDateDay);
			}
		}

		return validity.toString();
	}

	/**
	 * convert values of map in lower case
	 *
	 * @param values
	 * @return Map<String, String>
	 */
	public static Map<String, String> convertMapIntoLowerCase(final Map<String, String> values)
	{
		final Map<String, String> keyValueLowerCase = new HashMap<>();
		final Iterator<Map.Entry<String, String>> itr = values.entrySet().iterator();

		while (itr.hasNext())
		{
			final Map.Entry<String, String> entry = itr.next();
			keyValueLowerCase.put(entry.getKey().toLowerCase(Locale.US), entry.getValue().toLowerCase(Locale.US));
		}

		return keyValueLowerCase;
	}


}
