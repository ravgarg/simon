package com.simon.core.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.exceptions.SystemException;


/**
 * JSON-Utils for Changing DTO to JSON or JSON to getting value in Object.
 */
public class JsonUtils
{

	/**
	 * Private constructor created to stop static class from instantiation and enforce this class is used statically.
	 */
	private JsonUtils()
	{
		//Empty Body
	}

	/** The json mapper. */
	private static ObjectMapper jsonMapper = new ObjectMapper();
	private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

	/**
	 * Creates single json attribute.
	 *
	 * @param attr
	 *           the attr
	 * @param label
	 *           the label
	 * @return the string
	 */
	public static String createJsonAttr(final String attr, final boolean label)
	{
		final StringBuilder returnAttr = new StringBuilder("\"").append(attr).append("\"");

		if (label)
		{
			returnAttr.append(": ");
		}

		return returnAttr.toString();
	}

	/**
	 * Getting attribute value from json string.
	 *
	 * @param jsonResponse
	 *           the json response
	 * @param attributeName
	 *           the attribute name
	 * @return the json attribute value
	 */
	public static String getJsonAttributeValue(final String jsonResponse, final String attributeName)
	{

		try
		{
			final JsonNode rootNode = jsonMapper.readTree(jsonResponse);

			final JsonNode foundNode = rootNode.findValue(attributeName);

			if (foundNode == null)
			{
				return null;
			}

			return foundNode.textValue();
		}
		catch (final IOException t)
		{
			throw new SystemException(
					String.format("Exception %s trying to get json attribute %s value from json %s", t, attributeName, jsonResponse),
					t, "");
		}
	}

	/**
	 * Getting attribute value from json node.
	 *
	 * @param rootNode
	 *           the root node
	 * @param attributeName
	 *           the attribute name
	 * @return the json attribute value
	 */
	public static String getJsonAttributeValue(final JsonNode rootNode, final String attributeName)
	{

		final JsonNode foundNode = rootNode.get(attributeName);

		if (foundNode == null)
		{
			return null;
		}

		return foundNode.textValue();
	}

	/**
	 * Getting boolean attribute value from json node.
	 *
	 * @param rootNode
	 *           the root node
	 * @param attributeName
	 *           the attribute name
	 * @return the json attribute boolean value
	 */
	public static boolean getJsonAttributeBooleanValue(final JsonNode rootNode, final String attributeName)
	{

		final JsonNode foundNode = rootNode.get(attributeName);

		if (foundNode == null)
		{
			return false;
		}

		return foundNode.booleanValue();
	}

	/**
	 * Get values from list type json nodes.
	 *
	 * @param rootNode
	 *           the root node
	 * @param attributeName
	 *           the attribute name
	 * @return the json attribute array values
	 */
	public static List<String> getJsonAttributeArrayValues(final JsonNode rootNode, final String attributeName)
	{

		final JsonNode foundNode = rootNode.get(attributeName);

		if (foundNode == null)
		{
			return Collections.emptyList();
		}

		final List<String> values = new ArrayList<>();

		final Iterator<JsonNode> elements = foundNode.elements();

		while (elements.hasNext())
		{
			values.add(elements.next().textValue());
		}

		return values;
	}

	/**
	 * Returns a map "Content-Type:application/json" which can be added in header request to notify our receiver the json
	 * type request is supported
	 *
	 * @return the map
	 */
	public static Map<String, String> jsonContentTypeHdr()
	{

		final Map<String, String> hdr = new HashMap<>();
		hdr.put("Content-Type", "application/json");

		return hdr;
	}

	/**
	 * Accept json hdr. Returns a map with value "Accept:application/json" which can be used receive input in json.
	 *
	 * @return the map
	 */
	public static Map<String, String> acceptJsonHdr()
	{

		final Map<String, String> hdr = new HashMap<>();
		hdr.put("Accept", "application/json");

		return hdr;
	}

	/**
	 * This function is used to convert java object into json string
	 *
	 * @param jsonObject
	 *           the json object
	 * @return the JSON string for corresponding object
	 */
	public static String getJSONStringFromObject(final Object jsonObject)
	{
		String jsonString = null;
		if (jsonObject != null)
		{
			try
			{
				jsonString = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
			}
			catch (final JsonProcessingException e)
			{
				LOGGER.error("Exception occured while parsing Object to Json String.", e);
			}
		}
		return jsonString;
	}

}
