package com.simon.core.services;

import de.hybris.platform.catalog.KeywordService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;

import com.simon.core.enums.KeywordType;


/**
 * ExtKeywordService extends {@link KeywordService} provides additional operations to search {@code KeywordModel}
 */
public interface ExtKeywordService extends KeywordService
{

	/**
	 * Returns the Keyword for the specified keyword value, <code>CatalogVersion</code> and <code>KeywordType</code>
	 *
	 * @param catalogVersion
	 *           The <code>CatalogVersion</code> the <code>Keyword</code> belongs to.
	 * @param keywordValue
	 *           The value of the searched <code>Keyword</code>.
	 * @param keywordType
	 *           The Keyword Type
	 * @return The matching <code>Keyword</code>.
	 */
	KeywordModel getKeyword(final CatalogVersionModel catalogVersion, final String keywordValue, final KeywordType keywordType);
}
