package com.simon.core.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import javax.annotation.Resource;

import com.simon.core.services.ExtProductService;
import com.simon.core.util.CommonUtils;


public class ExtProductBasicPopulator<S extends ProductModel, T extends ProductData> extends ProductBasicPopulator<S, T>
{
	@Resource
	private ExtProductService extProductService;

	@Override
	public void populate(final S productModel, final T productData)
	{
		super.populate(productModel, productData);
		productData.setName(CommonUtils.replaceAsciiWithHtmlCode(getProductAttributes(productModel, ProductModel.NAME)));
		productData.setPurchasable(
				Boolean.valueOf(productModel.getVariantType() == null && isApproved(productModel) && isRetailerActive(productModel)));
		productData.setFavorite(extProductService.isFavorite(productModel));
		if (productModel instanceof GenericVariantProductModel)
		{
			final ProductModel prodModel = ((GenericVariantProductModel) productModel).getBaseProduct();
			productData.setBaseProductName(CommonUtils.replaceAsciiWithHtmlCode(prodModel.getName()));
			productData.setDescription(CommonUtils.replaceAsciiWithHtmlCode(prodModel.getDescription()));
			if (null != prodModel.getProductDesigner())
			{
				productData.setBaseDesignerName(prodModel.getProductDesigner().getName());
			}
		}
	}

	private boolean isRetailerActive(final S productModel)
	{
		return productModel.getRetailerActive();
	}

	/**
	 * Get an attribute value from a product. If the attribute value is null and the product is a variant then the same
	 * attribute will be requested from the base product.
	 *
	 * @param productModel
	 *           the product
	 * @param attribute
	 *           the name of the attribute to lookup
	 * @return the value of the attribute
	 */
	protected String getProductAttributes(final ProductModel productModel, final String attribute)
	{
		return (String) getProductAttribute(productModel, attribute);
	}

}
