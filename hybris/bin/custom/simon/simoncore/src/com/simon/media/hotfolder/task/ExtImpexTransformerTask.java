package com.simon.media.hotfolder.task;

import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.task.ImpexTransformerTask;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;


/**
 * Transformer that retrieves a CSV file and transforms it to an impex file.
 */
public class ExtImpexTransformerTask extends ImpexTransformerTask
{
	private String fileName;

	/**
	 * Executes a task with a predefined {@link BatchHeader} identifying all relevant process information.
	 *
	 * @param header
	 * @return the header
	 */
	@Override
	public BatchHeader execute(final BatchHeader header) throws UnsupportedEncodingException, FileNotFoundException
	{
		if (header.getFile().getName().startsWith(getFileName()))
		{
			return super.execute(header);

		}
		else
		{
			return header;
		}
	}

	/**
	 * @return fileName
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * @param fileName
	 */
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}

}
