package com.simon.core.product.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.strategies.impl.DefaultProductCodeGenerationStrategy;
import com.mirakl.hybris.core.shop.services.ShopService;
import com.simon.core.constants.SimonCoreConstants;


/**
 * Code Generation Strategy for New Generic Variant product and their Base Product.
 */
public class ExtProductCodeGenerationStrategy extends DefaultProductCodeGenerationStrategy
{

	@Autowired
	private ShopService shopService;

	/**
	 * {@inheritDoc}
	 *
	 * This method adds additional logic of appending Shop Id fetched from {@link ShopModel#PREFIX} (default = "Shop") to
	 * product code.
	 */
	@Override
	public String generateCode(final ComposedTypeModel composedType, final MiraklRawProductModel rawProduct,
			final ProductModel baseProduct, final ProductImportFileContextData context)
	{
		final ShopModel shopModel = shopService.getShopForId(context.getShopId());
		final String prefix = StringUtils.isNotEmpty(shopModel.getPreFix()) ? shopModel.getPreFix() : SimonCoreConstants.SHOP;

		String generatedCode;
		if (baseProduct == null)
		{
			generatedCode = createGeneratedCode(prefix, rawProduct.getVariantGroupCode());
		}
		else
		{
			generatedCode = createGeneratedCode(prefix, rawProduct.getSku());
		}
		return generatedCode;
	}

	/**
	 * Creates the generated code.
	 *
	 * @param prefix
	 *           the prefix
	 * @param code
	 *           the code
	 * @return the string
	 */
	private String createGeneratedCode(final String prefix, final String code)
	{
		return prefix + "-" + code;
	}
}
