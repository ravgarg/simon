package com.simon.core.catalog.strategies.impl;

import de.hybris.platform.core.model.product.ProductModel;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.impl.DefaultCoreAttributeHandler;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.keygenerators.DesignerKeyGenerator;
import com.simon.core.model.DesignerModel;
import com.simon.core.services.DesignerService;


/**
 * DesignerAttributeHandler, attribute handler for {@link ProductModel#PRODUCTDESIGNER}.
 */
public class DesignerAttributeHandler extends DefaultCoreAttributeHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignerAttributeHandler.class);

	@Autowired
	private DesignerService designerService;

	@Autowired
	private DesignerKeyGenerator designerKeyGenerator;

	/**
	 * Fetches {@link DesignerModel} for designer code and associates it with product. In this first we create Code from Raw
	 * designer Name by using DesignerKeyGenerator and then fetch Designer from Service.If Designer not exists we create and
	 * return designer and associate it to product. New Designer is set to save further when main model is saved.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public void setValue(final AttributeValueData attribute, final ProductImportData data,
			final ProductImportFileContextData context) throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = attribute.getCoreAttribute();
		final ProductModel product = determineOwner(coreAttribute, data, context);
		if (StringUtils.isNotEmpty(attribute.getValue()))
		{
			try
			{
				final String designerCode = (String) designerKeyGenerator.generateFor(attribute.getValue());
				final DesignerModel designer = designerService.getDesignerForCode(designerCode);
				product.setProductDesigner(designer);
			}
			catch (final Exception exception)
			{
				LOGGER.error("Exception Occurred In Getting/Creating Designer : " + attribute.getValue() + " For Product : "
						+ product.getCode(), exception);
			}
		}
	}

}
