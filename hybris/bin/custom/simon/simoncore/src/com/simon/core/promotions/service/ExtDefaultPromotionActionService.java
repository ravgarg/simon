package com.simon.core.promotions.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;


/**
 * This interface will serve as an contract for retailer specific changes in action service
 */
public interface ExtDefaultPromotionActionService
{
	/**
	 * This method adds retailerDiscountValues to CartModel. retailerDiscountValues are new attributes of CartModel which
	 * holds retailer sub cart level discounts.
	 *
	 * @param discountRao
	 *           the discount rao
	 * @param code
	 *           the code
	 * @param order
	 *           the order
	 */
	void createRetailerDiscountValue(DiscountRAO discountRao, String code, AbstractOrderModel order);

	/**
	 * This method fetches the order entry from the OrderConsumedEntry
	 *
	 * @param action
	 * @return
	 */
	AbstractOrderEntryModel getConsumedOrderEntry(AbstractRuleActionRAO action);

	/**
	 * OOTB Multi Buy product (Buy More Save More) is creates global discounts which has been modified to create entry
	 * level discounts.This method divides the global discounts by the order quantity to give you the discount values for
	 * order entry level
	 *
	 * @param discountRao
	 * @param code
	 * @param orderEntry
	 */
	void createDiscountValueForBuyMoreSaveMore(DiscountRAO discountRao, String code, AbstractOrderEntryModel orderEntry);

}
