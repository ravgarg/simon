package com.simon.core.mirakl.dao;

import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.simon.core.enums.PriceType;


/**
 * ExtOfferDao provides operations to fetch {@link PriceRowModel} and {@link StockLevelModel}
 */
public interface ExtOfferDao
{

	/**
	 * Find prices modified before date. Returns all price rows {@link PriceRowModel} modified before given date
	 * <code>modificationDate</code>
	 *
	 * @param modificationDate
	 *           the modification date
	 * @return the list of <code>PriceRowModel</code>
	 *
	 * @throws IllegalArgumentException
	 *            if <code>modificationDate</code> is null
	 */
	List<PriceRowModel> findPricesModifiedBeforeDate(Date modificationDate);

	/**
	 * Find stocks modified before date. Returns all stock levels {@link StockLevelModel} modified before given date
	 * <code>modificationDate</code>
	 *
	 * @param modificationDate
	 *           the modification date
	 * @return the list of <code>StockLevelModel</code>
	 *
	 * @throws IllegalArgumentException
	 *            if <code>modificationDate</code> is null
	 */
	List<StockLevelModel> findStocksModifiedBeforeDate(Date modificationDate);

	/**
	 * Find prices for given product codes. Returns Prices for given Product Codes <code>productList</code> and given Price
	 * Type <code>priceType</code>
	 *
	 * @param productIdList
	 *           the product list
	 * @param priceType
	 *           Price Type to fetch
	 * @return the list of Price Rows <code>PriceRowModel</code>
	 *
	 * @throws IllegalArgumentException
	 *            if <code>productList</code> or <code>priceType</code> is null
	 */
	List<PriceRowModel> findPricesForProductCodes(Set<String> productIdList, PriceType priceType);
}
