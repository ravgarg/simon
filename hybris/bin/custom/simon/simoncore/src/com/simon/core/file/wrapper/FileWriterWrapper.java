package com.simon.core.file.wrapper;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;

import shaded.org.apache.commons.io.output.FileWriterWithEncoding;


/**
 * Wrapper class written to encapsulate file writer and use this as a spring bean where ever file writers are required
 */
public class FileWriterWrapper
{

	/**
	 * Gets the file writer.
	 *
	 * @param filename
	 *           the filename
	 * @return the file writer
	 * @throws IOException
	 */
	public Writer getWriter(final String filename) throws IOException
	{
		final File file = new File(filename);
		if (!file.getParentFile().exists() && !file.getParentFile().mkdirs())
		{
			throw new IOException("Exception Occurred While Creating Parent Folder");
		}
		try
		{
			return new FileWriterWithEncoding(file, Charset.forName("UTF-8"));
		}
		catch (final IOException e)
		{
			throw new IOException("Exception Occurred While Creating File Writer", e);
		}
	}

}
