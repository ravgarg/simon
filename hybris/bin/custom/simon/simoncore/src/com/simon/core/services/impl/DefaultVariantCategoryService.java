package com.simon.core.services.impl;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.variants.model.VariantCategoryModel;

import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.dao.VariantCategoryDao;
import com.simon.core.services.VariantCategoryService;


/**
 * DefaultVariantCategoryService provides implementations of operations to search {@code VariantCategoryService}.
 */
public class DefaultVariantCategoryService extends DefaultCategoryService implements VariantCategoryService
{
	public static final long serialVersionUID = 1L;

	@Autowired
	private transient VariantCategoryDao variantCategoryDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VariantCategoryModel getLeafVariantCategory(final CatalogVersionModel catalogVersion)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("Catalog Version", catalogVersion);
		return variantCategoryDao.findLeafVariantCategory(catalogVersion);
	}
}
