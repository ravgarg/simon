package com.simon.core.services;

import com.simon.core.model.WhitelistedUserModel;


/**
 * This Service class is used to retrieve betaUser model.
 */
public interface ExtWhitelistedLoginService
{
	/**
	 * This is to find the whitelistedUser model from the database.
	 *
	 * @param email
	 * @param password
	 * @return valid beta user model otherwise null.
	 */
	WhitelistedUserModel findUserByEmailAndPassword(final String email, final String password);
}
