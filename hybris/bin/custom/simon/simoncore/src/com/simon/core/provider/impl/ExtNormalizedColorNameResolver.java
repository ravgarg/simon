package com.simon.core.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;


/**
 * This class helps in indexing Normalized Color Name into InputDocument
 */
public class ExtNormalizedColorNameResolver extends AbstractValueResolver<GenericVariantProductModel, Object, Object>
{
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<Object, Object> resolverContext) throws FieldValueProviderException
	{
		final String indexedPropertyName = indexedProperty.getName();
		final Object modelProperty = model.getProperty(indexedPropertyName);
		if (this.isHybrisEnum(modelProperty))
		{
			final HybrisEnumValue hybrisEnum = (HybrisEnumValue) modelProperty;
			document.addField(indexedProperty, this.getEnumValue(hybrisEnum));
		}
	}

	protected boolean isHybrisEnum(final Object modelProperty)
	{
		return modelProperty instanceof HybrisEnumValue;
	}

	protected String getEnumValue(final HybrisEnumValue hybrisEnumValue)
	{
		return hybrisEnumValue.getCode();
	}
}
