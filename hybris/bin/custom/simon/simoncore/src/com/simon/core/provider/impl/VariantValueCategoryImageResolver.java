package com.simon.core.provider.impl;

import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Locale;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.SwatchColorEnum;


/**
 * This class adds the "COLOR" VaraintValueCategory Thumbnail image to the InputDocument
 */
public class VariantValueCategoryImageResolver extends AbstractValueResolver<GenericVariantProductModel, MediaModel, Object>
{
	private static final Logger LOGGER = Logger.getLogger(VariantValueCategoryImageResolver.class);

	/** The type service. */
	@Resource
	private TypeService typeService;

	/** The enumeration service. */
	@Resource
	private EnumerationService enumerationService;

	@Resource
	private MediaService mediaService;

	/**
	 * Attribute which check whether indexed property is optional or not
	 */
	public static final String OPTIONAL_PARAM = "optional";
	/**
	 * The default value of optional attribute
	 */
	public static final boolean OPTIONAL_PARAM_DEFAULT_VALUE = true;

	/**
	 * This method fetches the supercategories from {@link GenericVariantProductModel}. It fetches out the "color"
	 * VariantCategory and checks whether is has thumbnail image. If thumbnail image exist, it's URL is returned and
	 * passed to add it to {@link InputDocument}
	 *
	 * @throws FieldValueProviderException,
	 *            Exception is thrown if there is any problem while adding data to index or expected value is not
	 *            fetched, given the attribute has optional set to false.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<MediaModel, Object> resolverContext) throws FieldValueProviderException
	{
		if (model != null && model.getSwatchColorMedia() != null && model.getSwatchColorMedia().getURL() != null)
		{
			filterAndAddFieldValues(document, batchContext, indexedProperty, model.getSwatchColorMedia().getURL(),
					resolverContext.getFieldQualifier());
		}
		else
		{
			if (null != model && null != model.getNormalizedColorCode())
			{
				filterAndAddFieldValues(document, batchContext, indexedProperty,
						extractColorURLFromNormalizedColorCode(model.getNormalizedColorCode().getCode()),
						resolverContext.getFieldQualifier());
			}
		}
	}

	/**
	 * This method extracts the image url of normalized color based on the normalizedColorCode
	 *
	 * @param normalizedColorCode
	 * @return String that is the URL of normalized color
	 *
	 */
	private String extractColorURLFromNormalizedColorCode(final String normalizedColorCode)
	{
		String normalizedColorSwatchURL = StringUtils.EMPTY;
		if (StringUtils.isNotEmpty(normalizedColorCode))
		{
			final HybrisEnumValue enumObj = enumerationService.getEnumerationValue(SwatchColorEnum.class, normalizedColorCode);

			if (enumObj != null)
			{
				final EnumerationValueModel enumerationValueModel = typeService.getEnumerationValue(enumObj);
				if (enumerationValueModel.getIcon() != null)
				{
					normalizedColorSwatchURL = enumerationValueModel.getIcon().getURL();
				}
				else
				{
					MediaModel swatchMedia = null;
					try
					{
						swatchMedia = mediaService
								.getMedia(normalizedColorCode.toLowerCase(Locale.US) + SimonCoreConstants.MEDIA_CODE_SWATCH_SUFFIX);
					}
					catch (final UnknownIdentifierException e)
					{
						LOGGER.error("Media not found ", e);
					}
					if (null != swatchMedia)
					{
						normalizedColorSwatchURL = swatchMedia.getURL();
					}
				}
			}
		}

		return normalizedColorSwatchURL;
	}

}
