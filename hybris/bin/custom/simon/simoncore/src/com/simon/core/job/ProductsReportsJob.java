package com.simon.core.job;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.services.ExtProductService;


/**
 * ProductsReportsJob analyzes unapproved and checked products in the system and logs their count, types and causes.
 */
public class ProductsReportsJob extends AbstractJobPerformable<CronJobModel>
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductsReportsJob.class);

	@Autowired
	private ExtProductService extProductService;

	@Autowired
	private CatalogVersionService catalogVersionService;

	@Autowired
	private ConfigurationService configurationService;

	/**
	 * Logs detailed information about unapproved and checked products in the system. Logs unapproved products counts and
	 * causes retailer-wise and logs checked products count retailer-wise.
	 *
	 * @param cronJobModel
	 *           the cron job model.
	 * @return the perform result
	 */
	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE,
				Catalog.STAGED);
		logsUnapprovedProducts(catalogVersion);
		logsCheckedProducts(catalogVersion);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	/**
	 * Logs checked products details.
	 *
	 * @param catalogVersion
	 *           the catalog version
	 */
	private void logsCheckedProducts(final CatalogVersionModel catalogVersion)
	{
		final List<ProductModel> checkedProducts = extProductService.getProductsForCatalogVersion(catalogVersion,
				ArticleApprovalStatus.CHECK);

		LOGGER.info("Total Checked Products={}", checkedProducts.size());

		final Map<ShopModel, Set<ProductModel>> retailerCheckedProductsMap = checkedProducts.stream()
				.collect(groupByShopCollector());

		LOGGER.info("Retailer-wise Checked Products Analysis", checkedProducts.size());
		retailerCheckedProductsMap.forEach((shop, products) -> logCount("Retailer={}:Id={} Checked Products={}", shop, products));
	}

	/**
	 * Logs unapproved products details.
	 *
	 * @param catalogVersion
	 *           the catalog version
	 */
	private void logsUnapprovedProducts(final CatalogVersionModel catalogVersion)
	{
		final List<ProductModel> unapprovedProducts = extProductService.getProductsForCatalogVersion(catalogVersion,
				ArticleApprovalStatus.UNAPPROVED);

		final int mediaCountInContainer = configurationService.getConfiguration().getInt("required.media.count", 1);

		LOGGER.info("Total Unapproved Products={}", unapprovedProducts.size());

		final Map<ShopModel, Set<ProductModel>> retailerUnapprovedProductsMap = unapprovedProducts.stream()
				.collect(groupByShopCollector());

		LOGGER.info("Retailer-wise Unapproved Products Analysis", unapprovedProducts.size());

		for (final Entry<ShopModel, Set<ProductModel>> retailerUnapprovedProductsMapEntry : retailerUnapprovedProductsMap
				.entrySet())
		{
			final ShopModel retailer = retailerUnapprovedProductsMapEntry.getKey();
			final String retailerName = retailer.getName();
			final String retailerId = retailer.getId();

			logCount("Retailer={}:Id={} Unapproved Products={}", retailer, retailerUnapprovedProductsMapEntry.getValue());

			final Set<ProductModel> missingImagesProducts = new HashSet<>();
			final Map<ProductModel, Map<String, String>> missingCategoriesProductsAttributesMap = new HashMap<>();

			for (final ProductModel product : retailerUnapprovedProductsMapEntry.getValue())
			{
				if (!sufficientGalleryImages(product, mediaCountInContainer))
				{
					missingImagesProducts.add(product);
				}
				if (!((GenericVariantProductModel) product).getBaseProduct().getSupercategories().stream()
						.filter(MerchandizingCategoryModel.class::isInstance).findAny().isPresent())
				{
					final Map<String, String> additionalAttributesMap = new LinkedHashMap<>();
					additionalAttributesMap.put("mirakl-category", product.getMiraklCategory().getCode());
					additionalAttributesMap.putAll(((GenericVariantProductModel) product).getAdditionalAttributes());
					missingCategoriesProductsAttributesMap.put(product, additionalAttributesMap);
				}
			}

			logCount("Retailer={}:Id={} Missing Gallery Images Unapproved Products={}", retailer, missingImagesProducts);

			logCount("Retailer={}:Id={} Missing Merchandizing Categories Unapproved Products={}", retailer,
					missingCategoriesProductsAttributesMap.keySet());

			logCount("Retailer={}:Id={} Missing Gallery and Merchandizing Categories Unapproved Products={}", retailer,
					CollectionUtils.intersection(missingImagesProducts, missingCategoriesProductsAttributesMap.keySet()));

			final Map<Map<String, String>, Set<ProductModel>> invertedMap = missingCategoriesProductsAttributesMap.entrySet()
					.stream()
					.collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.mapping(Map.Entry::getKey, Collectors.toSet())));

			LOGGER.info("Retailer={}:Id={} Additional Attributes-wise Unapproved Products Analysis", retailerName, retailerId);
			invertedMap.entrySet().forEach(entry -> LOGGER.info("Retailer={}:Id={} Count={} Attributes={}", retailerName, retailerId,
					entry.getValue().size(), entry.getKey().toString()));
		}
	}

	private boolean sufficientGalleryImages(final ProductModel product, final int mediaCountInContainer)
	{
		return !product.getGalleryImages().isEmpty()
				&& product.getGalleryImages().get(0).getMedias().size() > mediaCountInContainer;
	}

	protected void logCount(final String logMessage, final ShopModel retailer, final Collection<?> products)
	{
		LOGGER.info(logMessage, retailer.getName(), retailer.getId(), products.size());
	}

	/**
	 * Group by shop collector.
	 *
	 * @return the collector
	 */
	private Collector<ProductModel, ?, Map<ShopModel, Set<ProductModel>>> groupByShopCollector()
	{
		return Collectors.groupingBy(ProductModel::getShop, Collectors.mapping(Function.identity(), Collectors.toSet()));
	}
}
