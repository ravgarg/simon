package com.simon.core.services;

import java.util.List;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;


public interface ExtShopService extends ShopService
{
	/**
	 * Gets the list of ShopModel. Returns matching store for <code>id</code>
	 *
	 * @param storeIds
	 *
	 * @param objects
	 *           the store <code>code</code>
	 * @return the {@link ShopModel} for store <code>code</code>
	 */
	List<ShopModel> getListOfShopsForCodes(String[] storeIds);

	/**
	 * This method return retailer list
	 *
	 * @return
	 */
	public List<ShopModel> getRetailerlist();


}
