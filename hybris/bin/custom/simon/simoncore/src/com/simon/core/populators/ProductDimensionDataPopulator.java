package com.simon.core.populators;

import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.PromotionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.util.localization.Localization;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.Lists;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.ImagesData;
import com.simon.core.dto.ProductDimensionsData;
import com.simon.core.dto.VariantCategoryData;
import com.simon.core.dto.VariantProductPriceData;
import com.simon.core.exceptions.SystemException;


/**
 * This class populates data into {@link ProductDimensionsData} from {@link Document}
 *
 */
public class ProductDimensionDataPopulator implements Populator<Document, ProductDimensionsData>
{

	/**
	 * This populator {@link VariantCategoryMapPopulator} populates {@link VariantCategoryData} from {@link Document}
	 */
	@Resource
	private VariantCategoryMapPopulator variantCategoryMapPopulator;

	/**
	 * This populator {@link ProductImageDataPopulator} populates {@link ImagesData} from {@link Document}
	 */
	@Resource
	private ProductImageDataPopulator productImageDataPopulator;

	/**
	 * The PriceDatafactory to fetch the formatted value
	 */
	@Resource
	private PriceDataFactory priceDataFactory;

	/**
	 * Commoni18N service to fetch current currecy ISO code, to fetch price data from price factory
	 */
	@Resource
	private CommonI18NService commonI18NService;

	/**
	 * This method populates data into {@link ProductDimensionsData} from {@link Document}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 *
	 * @param product
	 *           {@link ProductDimensionsData} DTO which holds data related to single sku (GenericVariantProduct).
	 *           Indexed Data from solr document in populated into this DTO
	 *
	 * @throws ConversionException
	 *            if document's field value is not able to cast in given type
	 */
	@Override
	public void populate(final Document document, final ProductDimensionsData product)
	{
		populateVariantData(document, product);
		populateImageData(document, product);
		populateCategoryData(document, product);
		populatePriceData(document, product);
		populatePotentialPromotionData(document, product);
	}

	/**
	 * This method sets basic attributes of {@link ProductDimensionsData}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 * @param product
	 *           {@link ProductDimensionsData} DTO which holds data related to single sku (GenericVariantProduct).
	 *           Indexed Data from solr document in populated into this DTO
	 */
	private void populateVariantData(final Document document, final ProductDimensionsData product)
	{
		product.setVariantInStore((Boolean) document.getFieldValue(IndexedKeys.INSTOCKFLAG));
		product.setBaseProductCode((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_CODE));
		product.setBaseProductName((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME));
		product.setBaseProductdescription((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESCRIPTION));
		product.setName((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_NAME));
		product.setDescription((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_DESCRIPTION));
		product.setDesignerName((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME));
		product.setRetailerName((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_NAME));
		product.setRetailerLogo((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_LOGO));
		product.setColorId((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID));
		product.setColorName((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME));
		product.setSku((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_CODE));
	}

	/**
	 * This method sets the Price related attributes into {@link ProductDimensionsData}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 * @param product
	 *           {@link ProductDimensionsData} DTO which holds data related to single sku (GenericVariantProduct).
	 *           Indexed Data from solr document in populated into this DTO
	 */
	private void populatePriceData(final Document document, final ProductDimensionsData product)
	{
		final List<String> vairantPrice = (ArrayList<String>) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PRICE);
		if (CollectionUtils.isNotEmpty(vairantPrice))
		{
			VariantProductPriceData priceData;
			for (final String priceValue : vairantPrice)
			{
				final String[] price = priceValue.split(Pattern.quote(SimonCoreConstants.PIPE));

				if (SimonCoreConstants.PERCENTAGE_SAVING.equals(price[0]))
				{
					final String localizedString = getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
					product.setPercentOff(price[1] + localizedString);
					continue;
				}

				priceData = fetchPrice(price);
				if (SimonCoreConstants.PRODUCT_LIST_PRICE.equals(price[0]))
				{
					product.setListValue(priceData);
				}
				else if (SimonCoreConstants.PRODUCT_SALE_PRICE.equals(price[0]))
				{
					product.setSaleValue(priceData);
				}
				else
				{
					product.setMsrpValue(priceData);
				}
				if (priceData.isStrikeOff())
				{
					product.setStrikeOffPrice(priceData.getValue());
				}
			}
		}
	}

	/**
	 * This method sets variant category data into {@link ProductDimensionsData}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 * @param product
	 *           {@link ProductDimensionsData} DTO which holds data related to single sku (GenericVariantProduct).
	 *           Indexed Data from solr document in populated into this DTO
	 */
	private void populateCategoryData(final Document document, final ProductDimensionsData product)
	{
		final Map<String, VariantCategoryData> variantCategoryMap = new LinkedHashMap<>();
		variantCategoryMapPopulator.populate(document, variantCategoryMap);
		product.setDimensions(variantCategoryMap);
	}

	/**
	 * This method sets Image data into {@link ProductDimensionsData}
	 *
	 * @param document
	 *           {@link Document} A single solr document, which represents a GenericVariantProduct
	 * @param product
	 *           {@link ProductDimensionsData} DTO which holds data related to single sku (GenericVariantProduct).
	 *           Indexed Data from solr document in populated into this DTO
	 */
	private void populateImageData(final Document document, final ProductDimensionsData product)
	{
		final Map<String, ImagesData> images = new HashMap<>();
		productImageDataPopulator.populate(document, images);
		product.setImages(images);
	}

	/**
	 * This method sets price data into {@link VariantProductPriceData}
	 *
	 * @param price
	 *           A array representing a single price value from Indexed VariantPrice
	 * @return {@link VariantProductPriceData} fetching only those attributes required in json
	 */
	private VariantProductPriceData fetchPrice(final String[] price)
	{
		final DecimalFormat format = new DecimalFormat();
		format.setParseBigDecimal(true);
		PriceData priceFactoryData;
		final VariantProductPriceData priceData = new VariantProductPriceData();
		try
		{
			priceFactoryData = priceDataFactory.create(PriceDataType.BUY, (BigDecimal) format.parse((price[1])),
					commonI18NService.getCurrentCurrency().getIsocode());
			priceData.setFormattedValue(priceFactoryData.getFormattedValue());
			priceData.setValue(priceFactoryData.getValue());
			priceData.setStrikeOff(Boolean.parseBoolean(price[2]));
		}
		catch (final ParseException parseException)
		{
			throw new SystemException(parseException, "Exception occured while parsing Price data from String to BigDecimal");
		}
		return priceData;
	}

	protected void populatePotentialPromotionData(final Document document, final ProductDimensionsData target)
	{
		final PromotionData potentialPromoData = new PromotionData();
		potentialPromoData.setCode((String) document.getFieldValue(IndexedKeys.PROMOTION_CODE));
		potentialPromoData.setDescription((String) document.getFieldValue(IndexedKeys.PROMOTION_DESCRIPTION));
		potentialPromoData.setPromotionType((String) document.getFieldValue(IndexedKeys.PROMOTION_TYPE));
		potentialPromoData.setEndDate((Date) document.getFieldValue(IndexedKeys.PROMOTION_END_DATE));
		target.setPotentialPromotions(Lists.newArrayList(potentialPromoData));
	}

	/**
	 * Method to get localized label from locale properties
	 *
	 * @return String
	 */
	protected String getLocalizedString(final String percentSavingMessage)
	{
		return Localization.getLocalizedString(percentSavingMessage);
	}
}
