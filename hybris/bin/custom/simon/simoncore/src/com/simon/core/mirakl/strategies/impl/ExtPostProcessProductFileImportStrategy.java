package com.simon.core.mirakl.strategies.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.product.strategies.impl.DefaultPostProcessProductFileImportStrategy;
import com.simon.core.exceptions.ProductFileImportPostProcessingException;
import com.simon.core.mirakl.strategies.ProductFileImportPostProcessor;


/**
 * Extended post process strategy to be executed as a post process to create csv for image generation.
 */
public class ExtPostProcessProductFileImportStrategy extends DefaultPostProcessProductFileImportStrategy
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ExtPostProcessProductFileImportStrategy.class);

	@Autowired
	private List<ProductFileImportPostProcessor> postprocessors;

	@Autowired
	private ConfigurationService configurationService;

	/**
	 * Executes all post processors in sequence if they are not null and are enabled. A specific post processor can be
	 * disabled by setting <code>postProcessor.PostProcessorClassName.enabled=false</code> in configuration. <br>
	 * {@inheritDoc}
	 */
	@Override
	public void postProcess(final ProductImportFileContextData context, final String importId)
	{
		if (postprocessors == null)
		{
			return;
		}

		for (final ProductFileImportPostProcessor postprocessor : postprocessors)
		{
			if (postprocessor != null && isEnabled(postprocessor))
			{
				try
				{
					postprocessor.postProcess(context, importId);
				}
				catch (final ProductFileImportPostProcessingException postprocessingException)
				{
					LOGGER.error("Error Occurred In Post Processor : {}", postprocessor.getClass().getSimpleName(),
							postprocessingException);
				}
			}
		}
	}

	/**
	 * Checks if postprocessor is enabled.
	 *
	 * @param productFileImportPostProcessor
	 *           the product file import post processor
	 * @return true, if is enabled
	 */
	protected boolean isEnabled(final ProductFileImportPostProcessor productFileImportPostProcessor)
	{
		return configurationService.getConfiguration().getBoolean(
				String.format("postProcessor.%s.enabled", productFileImportPostProcessor.getClass().getSimpleName()), true);
	}

	/**
	 * Sets the postprocessors.
	 *
	 * @param postprocessors
	 *           the new postprocessors
	 */
	public void setPostprocessors(final List<ProductFileImportPostProcessor> postprocessors)
	{
		this.postprocessors = postprocessors;
	}
}
