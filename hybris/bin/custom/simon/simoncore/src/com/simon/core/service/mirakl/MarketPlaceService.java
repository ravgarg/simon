package com.simon.core.service.mirakl;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;


/**
 * This is the interface used for invoking Mirakl API's to fetch/update details from/within Mirakl.
 *
 */
public interface MarketPlaceService
{

	/**
	 * Method is used to call Mirakl API OR21, it will update the Order <code>acceptance/Rejection</code> status
	 * depending on Retailer action.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @param accepted
	 *           boolean
	 * @return void
	 *
	 */
	void acceptOrRejectOrder(AbstractOrderModel orderModel, boolean accepted);

	/**
	 * Method is used to call OR24 API, to update the <code>shipping state</code> within Mirakl.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @return void
	 */
	void shipOrder(AbstractOrderModel orderModel);

	/**
	 * Method is used to call OR23 API, to update the <code>carrier tracking details</code> within Mirakl.
	 *
	 * @param orderModel
	 *           AbstractOrderModel
	 * @return void
	 */
	void trackingOrder(AbstractOrderModel orderModel);

	/**
	 * Method is used to call PA01 API, to <code>valid/invalid</code the payment of the order in Mirakl.
	 *
	 * @param consignmentModel
	 *           ConsignmentModel
	 * @return void.
	 *
	 */
	void processPayment(ConsignmentModel consignmentModel);

	/**
	 * Method is used to call Mirakl API OR21, update the Order status from Pending Acceptance to Shipping in progress
	 * status
	 *
	 * @param consignment
	 * @param accepted
	 */
	void acceptOrRejectOrder(MarketplaceConsignmentModel consignment, boolean accepted);

	/**
	 * Method is used to call Mirakl API OR25, update the Mirakl Order status from Shipping in progress to Received
	 * status
	 *
	 * @param miraklOrderId
	 */
	void receivedMarketplaceOrder(final String miraklOrderId);
}
