package com.simon.core.services.impl;

import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.media.impl.DefaultMediaContainerService;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.simon.core.dao.ExtMediaContainerDao;
import com.simon.core.services.ExtMediaContainerService;


/**
 * This class is extension of defaultMediaContainerService with some additional methods.
 */
public class ExtMediaContainerServiceImpl extends DefaultMediaContainerService implements ExtMediaContainerService
{
	private static final long serialVersionUID = 1L;
	@Resource
	private transient ExtMediaContainerDao extMediaContainerDao;

	/**
	 * This method get all media container whose version no is greater then zero.
	 *
	 * @return
	 */
	public List<MediaContainerModel> getMediaContainersWithVersionGreaterThenZero(final Date cleanupAgeDate)
	{
		return extMediaContainerDao.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate);
	}

}
