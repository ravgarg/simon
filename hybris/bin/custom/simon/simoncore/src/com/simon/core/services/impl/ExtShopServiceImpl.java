package com.simon.core.services.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.List;

import javax.annotation.Resource;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.impl.DefaultShopService;
import com.simon.core.dao.ExtShopDao;
import com.simon.core.services.ExtShopService;


public class ExtShopServiceImpl extends DefaultShopService implements ExtShopService
{
	@Resource(name = "shopDao")
	private ExtShopDao extShopDao;


	/**
	 * This method return retailer list
	 *
	 * @return
	 */
	@Override
	public List<ShopModel> getRetailerlist()
	{
		return extShopDao.getRetailerList();
	}

	@Override
	public List<ShopModel> getListOfShopsForCodes(final String[] shopIds)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("id", shopIds);
		return extShopDao.findListOfShopsForCodes(shopIds);
	}
}
