package com.simon.core.provider.impl;

import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.ValueProviderParameterUtils;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.constants.SimonCoreConstants;


/**
 * This class adds the Shop/Retailer logo image to the InputDocument
 */
public class ShopLogoValueResolver extends AbstractValueResolver<GenericVariantProductModel, MediaModel, Object>
{
	/**
	 * This method fetches the base product's Logo media from {@link GenericVariantProductModel}. If logo image exist,
	 * it's URL is returned and passed to add it to {@link InputDocument}
	 *
	 * @throws FieldValueProviderException,
	 *            Exception is thrown if there is any problem while adding data to index or expected value is not
	 *            fetched, given the attribute has optional set to false.
	 */
	@Override
	protected void addFieldValues(final InputDocument document, final IndexerBatchContext batchContext,
			final IndexedProperty indexedProperty, final GenericVariantProductModel model,
			final ValueResolverContext<MediaModel, Object> resolverContext) throws FieldValueProviderException
	{
		boolean hasLogo = false;
		final ShopModel shop = model.getBaseProduct().getShop();
		if (null != shop && null != shop.getLogo())
		{
			final Object logoUrl = shop.getLogo().getURL();
			hasLogo = filterAndAddFieldValues(document, batchContext, indexedProperty, logoUrl, resolverContext.getFieldQualifier());
		}
		if (!hasLogo)
		{
			final boolean isOptional = ValueProviderParameterUtils.getBoolean(indexedProperty, SimonCoreConstants.OPTIONAL_PARAM,
					SimonCoreConstants.OPTIONAL_PARAM_DEFAULT_VALUE);
			if (!isOptional)
			{
				throw new FieldValueProviderException("No value resolved for indexed property " + indexedProperty.getName());
			}
		}

	}

}
