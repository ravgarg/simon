package com.simon.core.services.impl;

import de.hybris.platform.catalog.CatalogVersionService;

import org.springframework.beans.factory.annotation.Autowired;

import com.simon.core.dao.CustomMessageDao;
import com.simon.core.exceptions.SystemException;
import com.simon.core.services.CustomMessageService;


/**
 * Service implementation class for CustomMessageService interface Fetches the message text for given message code and
 * catalog version
 */
public class CustomMessageServiceImpl implements CustomMessageService
{
	/*
	 * CustomMessageDao implementation class is injected. Fetches values from DB using flexiquery.
	 */
	@Autowired
	private CustomMessageDao messageDao;

	/*
	 * CatalogVersionService implementation class is injected. Fetches current session catalog version.
	 */
	@Autowired
	private CatalogVersionService catalogVersionService;

	/**
	 * Returns message Text corresponding to message code from CustomMessageDao. Current catalog version is fetched from
	 * session using CatalogVersionService
	 *
	 * @param code
	 *           The uid/code of SimonMessage Type
	 * @throws SystemException.
	 *            Forwards the wrapper SystemException, to be handled at upper class.
	 */
	@Override
	public String getMessageForCode(final String messageCode) throws SystemException
	{
		return messageDao.getMessageTextForCode(messageCode, catalogVersionService.getSessionCatalogVersions());
	}

}
