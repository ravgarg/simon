package com.simon.core.dao.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.dao.LeftNavigationCategoryDao;


/**
 * This class fetches all L1 categories corresponding to Home category code
 */
public class LeftNavigationCategoryDaoImpl extends AbstractItemDao implements LeftNavigationCategoryDao
{
	/**
	 * This method returns all categories corresponding to Home category code
	 */
	@Override
	public List<CategoryModel> getL1Categories(final String categoryId)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.SELECT_L1_CATEGORIES);
		fQuery.addQueryParameter("categoryId", categoryId);
		final SearchResult<CategoryModel> searchResult = getFlexibleSearchService().search(fQuery);
		return searchResult.getResult();
	}
}
