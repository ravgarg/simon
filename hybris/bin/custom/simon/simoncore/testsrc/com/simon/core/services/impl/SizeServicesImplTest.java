package com.simon.core.services.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.dao.SizeDao;
import com.simon.core.model.ProductSizeSequenceModel;



/**
 * The Class SizeServicesImplTest. Unit test for {@link SizeServicesImpl}}
 */
@UnitTest
public class SizeServicesImplTest
{
	/** The Size Service . */
	@InjectMocks
	private SizeServicesImpl sizeServiceImpl;

	/** The Size Dao. */
	@Mock
	private SizeDao sizeDao;


	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	@Test
	public void testGetAllSequencesForSizes()
	{
		final List<ProductSizeSequenceModel> list = new ArrayList<>();
		final ProductSizeSequenceModel sizeSeq = new ProductSizeSequenceModel();
		list.add(sizeSeq);
		when(sizeDao.getAllSequencesForSizes()).thenReturn(list);
		sizeServiceImpl.getAllSequencesForSizes();
		verify(sizeDao, Mockito.times(1)).getAllSequencesForSizes();
	}
}
