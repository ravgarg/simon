package com.simon.core.strategies.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.model.MiraklCategoryModel;


/**
 * ExtProductUpdateStrategyTest unit test for {@link ExtProductUpdateStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtProductUpdateStrategyTest
{

	@InjectMocks
	@Spy
	private ExtProductUpdateStrategy extProductUpdateStrategy;

	@Mock
	private ArrayList<CategoryModel> existingCategories;

	@Captor
	private ArgumentCaptor<Collection<CategoryModel>> categoriesCaptor;

	/**
	 * Verify no category is associated with base product if not matched correctly.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoCategoryIsAssociatedWithBaseProductIfNotMatchedCorrectly() throws ProductImportException
	{
		Mockito.doNothing().when(extProductUpdateStrategy).applyDefaultValues(any(ProductImportData.class),
				any(ProductImportFileContextData.class));

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final ProductModel baseProduct = mock(ProductModel.class);
		final ArrayList<CategoryModel> superCategories = new ArrayList<>();
		final MerchandizingCategoryModel merchandizingCategory3 = mock(MerchandizingCategoryModel.class);
		superCategories.add(merchandizingCategory3);
		final ArrayList<CategoryModel> autogenratedCategories = new ArrayList<>();
		final MerchandizingCategoryModel merchandizingCategory4 = mock(MerchandizingCategoryModel.class);
		autogenratedCategories.add(merchandizingCategory4);
		when(baseProduct.getSupercategories()).thenReturn(superCategories);
		when(baseProduct.getAutomatedGeneratedCategories()).thenReturn(autogenratedCategories);

		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(data.getProductToUpdate()).thenReturn(genericVariantProduct);
		when(genericVariantProduct.getBaseProduct()).thenReturn(baseProduct);

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("GenderType", "Men");
		productAdditionalAttributes.put("SizeType", "Plus");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("GenderType", Arrays.asList("Unisex"));
		category1AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("GenderType", Arrays.asList("Women"));
		category2AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));
		when(merchandizingCategory2.getAdditionalAttributes()).thenReturn(category2AdditionalAttributes);
		when(miraklCategory.getMerchandizingCategories()).thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2));
		when(context.getMappingFailure()).thenReturn(new ConcurrentLinkedQueue<String>());
		when(context.getMiraklImportId()).thenReturn("importid");
		when(context.getShopId()).thenReturn("shopid");
		extProductUpdateStrategy.applyValues(data, context);

		verify(existingCategories, times(0)).add(any(MerchandizingCategoryModel.class));
		assertTrue(baseProduct.getSupercategories().contains(merchandizingCategory3));

	}

	/**
	 * Verify matched category is correctly associated with base product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifySingleMatchedCategoryIsCorrectlyAssociatedWithBaseProduct() throws ProductImportException
	{
		Mockito.doNothing().when(extProductUpdateStrategy).applyDefaultValues(any(ProductImportData.class),
				any(ProductImportFileContextData.class));

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final ProductModel baseProduct = mock(ProductModel.class);

		final List<CategoryModel> extCats = new ArrayList<>();

		when(baseProduct.getSupercategories()).thenReturn(extCats);

		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(data.getProductToUpdate()).thenReturn(genericVariantProduct);
		when(genericVariantProduct.getBaseProduct()).thenReturn(baseProduct);

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("GenderType", "Men");
		productAdditionalAttributes.put("SizeType", "Plus");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("GenderType", Arrays.asList("Men"));
		category1AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("GenderType", Arrays.asList("Women"));
		category2AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));
		when(merchandizingCategory2.getAdditionalAttributes()).thenReturn(category2AdditionalAttributes);

		when(miraklCategory.getMerchandizingCategories()).thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2));

		extProductUpdateStrategy.applyValues(data, context);

		verify(baseProduct).setSupercategories(categoriesCaptor.capture());
		assertTrue(categoriesCaptor.getValue().contains(merchandizingCategory1));
	}

	/**
	 * Verify more than one matched category is correctly associated with base product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyMoreThanOneMatchedCategoryIsCorrectlyAssociatedWithBaseProduct() throws ProductImportException
	{
		Mockito.doNothing().when(extProductUpdateStrategy).applyDefaultValues(any(ProductImportData.class),
				any(ProductImportFileContextData.class));

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final ProductModel baseProduct = mock(ProductModel.class);

		final List<CategoryModel> extCats = new ArrayList<>();

		when(baseProduct.getSupercategories()).thenReturn(extCats);

		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(data.getProductToUpdate()).thenReturn(genericVariantProduct);
		when(genericVariantProduct.getBaseProduct()).thenReturn(baseProduct);

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("GenderType", "Men");
		productAdditionalAttributes.put("SizeType", "Plus");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("GenderType", Arrays.asList("Men"));
		category1AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("GenderType", Arrays.asList("Women"));
		category2AdditionalAttributes.put("SizeType", Arrays.asList("Plus"));

		final MerchandizingCategoryModel merchandizingCategory3 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category3AdditionalAttributes = new HashMap<>();
		category3AdditionalAttributes.put("GenderType", Arrays.asList("Men"));
		category3AdditionalAttributes.put("SizeType", Arrays.asList("Plus", "Regular"));
		when(merchandizingCategory3.getAdditionalAttributes()).thenReturn(category3AdditionalAttributes);

		when(miraklCategory.getMerchandizingCategories())
				.thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2, merchandizingCategory3));

		extProductUpdateStrategy.applyValues(data, context);

		verify(baseProduct).setSupercategories(categoriesCaptor.capture());
		assertTrue(categoriesCaptor.getValue().contains(merchandizingCategory1));
		assertTrue(categoriesCaptor.getValue().contains(merchandizingCategory3));
	}
}
