package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class BaseProductDetailsResolverTest
{
	@InjectMocks
	private BaseProductDetailsResolver baseProductDetailsResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	@Mock
	private IndexedProperty indexedProperty;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAttributeValueWithValidAttributeNameAndGenericVariantProduct() throws FieldValueProviderException
	{
		final GenericVariantProductModel product = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		product.setBaseProduct(baseProduct);
		final ComposedTypeModel composedType = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(baseProduct.getClass())).thenReturn(composedType);
		final String attributeName = "Name";
		when(typeService.hasAttribute(composedType, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(baseProduct, attributeName)).thenReturn(value);
		assertEquals(value, baseProductDetailsResolver.getAttributeValue(indexedProperty, product, attributeName));
	}

	@Test
	public void testGetAttributeValueWithValidAttributeNameAndGenericVariantProductAndNullBaseProduct()
			throws FieldValueProviderException
	{
		final GenericVariantProductModel product = new GenericVariantProductModel();
		final ProductModel baseProduct = null;
		product.setBaseProduct(baseProduct);
		final ComposedTypeModel composedType = new ComposedTypeModel();
		final String attributeName = "Name";
		assertNull(baseProductDetailsResolver.getAttributeValue(indexedProperty, product, attributeName));
	}

	@Test
	public void testGetAttributeValueWithEmptyAttributeName() throws FieldValueProviderException
	{
		final GenericVariantProductModel product = new GenericVariantProductModel();
		final String attributeName = "";
		assertNull(baseProductDetailsResolver.getAttributeValue(indexedProperty, product, attributeName));
	}

	@Test
	public void testGetModelAttributeValueWithTypeServiceWithoutAttribute()
	{
		final ComposedTypeModel composedType = new ComposedTypeModel();
		final GenericVariantProductModel product = new GenericVariantProductModel();
		when(typeService.getComposedTypeForClass(product.getClass())).thenReturn(composedType);
		final String attributeName = "Name";
		when(typeService.hasAttribute(composedType, attributeName)).thenReturn(Boolean.FALSE);
		final Object value = new Object();
		when(modelService.getAttributeValue(product, attributeName)).thenReturn(value);
		assertNull(baseProductDetailsResolver.getModelAttributeValue(product, attributeName));
	}

}
