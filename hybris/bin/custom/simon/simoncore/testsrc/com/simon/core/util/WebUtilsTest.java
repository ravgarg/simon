/**
 *
 */
package com.simon.core.util;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;


/**
 *
 */
@UnitTest
public class WebUtilsTest
{
	public static final String IMAGE = "test.jpg";


	@Test
	public void verifyGetMimeTypeForGivenFile_whenFileIsAssociated()
	{
		assertThat(WebUtils.getMimeType(IMAGE), is("image/jpeg"));
	}

	@Test
	public void verifyGetMimeTypeForGivenFile_whenFileIsNotAssociated()
	{
		assertThat(WebUtils.getMimeType(""), is("application/octet-stream"));
	}

	@Test
	public void verifyGetMimeTypeForGivenFile_whenNullValuePassed()
	{
		assertThat(WebUtils.getMimeType("xyz.xyz"), is("application/octet-stream"));
	}

	@Test
	public void verifyGetMimeTypeForGivenFile_whenFileExtensionIsMissing()
	{
		assertThat(WebUtils.getMimeType("test"), is("application/octet-stream"));
	}

	@Test
	public void verifyGetExtensionForGivenFile_whenFileExtensionIsMissing()
	{
		assertThat(WebUtils.getExtension(""), isEmptyOrNullString());
	}

	@Test
	public void verifyUrlEncode_whenURLIsMissing()
	{
		assertThat(WebUtils.urlEncode(""), isEmptyOrNullString());
	}

	@Test
	public void verifyUrlEncode_whenURLIsGiven()
	{
		assertThat(WebUtils.urlEncode("/test/product"), is("%2Ftest%2Fproduct"));
	}

	@Test
	public void verifyUrlEncodeOverrideSpace_whenURLIsMissing()
	{
		assertThat(WebUtils.urlEncodeOverrideSpace(""), isEmptyOrNullString());
	}

	@Test
	public void verifyUrlEncodeOverrideSpace_whenURLIsGiven()
	{
		assertThat(WebUtils.urlEncodeOverrideSpace("/test/ /product"), is("%2Ftest%2F%20%2Fproduct"));
	}


	@Test
	public void verifyUrlDecode_whenURLIsMissing()
	{
		assertThat(WebUtils.urlDecode(""), isEmptyOrNullString());
	}

	@Test
	public void verifyUrlDecode_whenURLIsGiven()
	{
		assertThat(WebUtils.urlDecode("%2Ftest%2Fproduct"), is("/test/product"));
	}

	@Test
	public void verifyUrlDecode_whenURLIsGivenWithOutEncoding()
	{
		assertThat(WebUtils.urlDecode("%2Ftest%2Fproduct", null), is("/test/product"));
	}
}
