package com.simon.core.mirakl.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.math.BigDecimal;
import java.sql.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.common.MiraklDiscount;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.simon.core.enums.PriceType;


/**
 * ListPriceRowPopulatorTest, unit test for {@link ListPriceRowPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ListPriceRowPopulatorTest
{
	@InjectMocks
	private ListPriceRowPopulator listPriceRowPopulator;

	private static final String OFFER_ID = "12345";

	private static final String STATE_CODE = "state";

	private static final String PRODUCT_SKU = "sku";

	/**
	 * Verify list price populated correctly.
	 */
	@Test
	public void verifyListPricePopulatedCorrectly()
	{
		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);

		final MiraklDiscount miraklDiscount = mock(MiraklDiscount.class);
		when(miraklDiscount.getOriginPrice()).thenReturn(BigDecimal.valueOf(20.0));

		when(exportOffer.getDiscount()).thenReturn(miraklDiscount);
		when(exportOffer.isActive()).thenReturn(true);
		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		when(exportOffer.getAvailableStartDate()).thenReturn(startDate);
		when(exportOffer.getAvailableEndDate()).thenReturn(endDate);

		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		when(exportOffer.getLeadtimeToShip()).thenReturn(122);
		when(exportOffer.getShopId()).thenReturn("2023");

		final PriceRowModel target = mock(PriceRowModel.class);
		listPriceRowPopulator.populate(exportOffer, target);

		verify(target).setPrice(20.0);
		verify(target).setStartTime(startDate);
		verify(target).setEndTime(endDate);
		verify(target).setPriceType(PriceType.LIST);
		verify(target).setOfferId((OFFER_ID));

	}

	/**
	 * Verify list price populated from price if discount is null.
	 */
	@Test
	public void verifyListPricePopulatedFromPriceIfDiscountIsNull()
	{
		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);
		when(exportOffer.isActive()).thenReturn(true);
		when(exportOffer.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		when(exportOffer.getAvailableStartDate()).thenReturn(startDate);
		when(exportOffer.getAvailableEndDate()).thenReturn(endDate);
		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		when(exportOffer.getLeadtimeToShip()).thenReturn(122);
		when(exportOffer.getShopId()).thenReturn("2023");

		final PriceRowModel target = mock(PriceRowModel.class);
		listPriceRowPopulator.populate(exportOffer, target);
		verify(target).setPrice(10.0);
		verify(target).setStartTime(startDate);
		verify(target).setEndTime(endDate);
		verify(target).setPriceType(PriceType.LIST);
		verify(target).setOfferId((OFFER_ID));
	}

	/**
	 * Verify list price populated from price if discount present but discount price is null.
	 */
	@Test
	public void verifyListPricePopulatedFromPriceIfDiscountPresentButDiscountPriceIsNull()
	{
		final MiraklDiscount miraklDiscount = mock(MiraklDiscount.class);

		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);
		when(exportOffer.getDiscount()).thenReturn(miraklDiscount);
		when(exportOffer.isActive()).thenReturn(true);
		when(exportOffer.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		when(exportOffer.getLeadtimeToShip()).thenReturn(122);
		when(exportOffer.getShopId()).thenReturn("2023");

		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		when(exportOffer.getAvailableStartDate()).thenReturn(startDate);
		when(exportOffer.getAvailableEndDate()).thenReturn(endDate);

		final PriceRowModel target = mock(PriceRowModel.class);
		listPriceRowPopulator.populate(exportOffer, target);

		verify(target).setPrice(10.0);
		verify(target).setStartTime(startDate);
		verify(target).setEndTime(endDate);
		verify(target).setPriceType(PriceType.LIST);
		verify(target).setOfferId((OFFER_ID));
	}

	/**
	 * Verify not null lead time to ship populated correctly.
	 */
	@Test
	public void verifyNotNullLeadTimeToShipPopulatedCorrectly()
	{
		final MiraklDiscount miraklDiscount = mock(MiraklDiscount.class);

		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);
		when(exportOffer.getDiscount()).thenReturn(miraklDiscount);
		when(exportOffer.isActive()).thenReturn(true);
		when(exportOffer.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		when(exportOffer.getLeadtimeToShip()).thenReturn(122);
		when(exportOffer.getShopId()).thenReturn("2023");

		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		when(exportOffer.getAvailableStartDate()).thenReturn(startDate);
		when(exportOffer.getAvailableEndDate()).thenReturn(endDate);

		final PriceRowModel target = mock(PriceRowModel.class);
		listPriceRowPopulator.populate(exportOffer, target);

		verify(target).setLeadtimeToShip(122);
	}

	/**
	 * Verify null lead time to ship populated as zero correctly.
	 */
	@Test
	public void verifyNullLeadTimeToShipPopulatedAsZeroCorrectly()
	{
		final MiraklDiscount miraklDiscount = mock(MiraklDiscount.class);

		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);
		when(exportOffer.getDiscount()).thenReturn(miraklDiscount);
		when(exportOffer.isActive()).thenReturn(true);
		when(exportOffer.getPrice()).thenReturn(BigDecimal.valueOf(10.0));
		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		when(exportOffer.getShopId()).thenReturn("2023");
		when(exportOffer.getLeadtimeToShip()).thenReturn(null);

		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);
		when(exportOffer.getAvailableStartDate()).thenReturn(startDate);
		when(exportOffer.getAvailableEndDate()).thenReturn(endDate);

		final PriceRowModel target = mock(PriceRowModel.class);
		listPriceRowPopulator.populate(exportOffer, target);
		verify(target).setLeadtimeToShip(0);
	}
}
