package com.simon.core.catalog.strategies.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;



/**
 * VariantValueCategoryAttributeHandlerTest unit test for {@link VariantValueCategoryAttributeHandler}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantValueCategoryAttributeHandlerTest
{

	@InjectMocks
	private VariantValueCategoryAttributeHandler variantValueCategoryAttributeHandler;

	@Mock
	private CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	@Mock
	private ModelService modelService;

	@Mock
	private CategoryService categoryService;

	@Mock
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	@Mock
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Captor
	private ArgumentCaptor<Collection<CategoryModel>> variantCategoriesCaptor;

	@Captor
	private ArgumentCaptor<Collection<CategoryModel>> baseCategoriesCaptor;

	/**
	 * Verify no interaction for null attribute value.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoInteractionForNullAttributeValue() throws ProductImportException
	{
		final AttributeValueData attributeValue = mock(AttributeValueData.class);
		when(attributeValue.getCoreAttribute()).thenReturn(mock(MiraklCategoryCoreAttributeModel.class));

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final VariantProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		mockCommonData(data, context, productCatalogVersion, variantProduct, baseProduct);

		variantValueCategoryAttributeHandler.setValue(attributeValue, data, context);

		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify VC and VVC present associated correctly.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyVCAndVVCPresentAssociatedCorrectly() throws ProductImportException
	{
		final AttributeValueData attributeValue = mock(AttributeValueData.class);
		when(attributeValue.getValue()).thenReturn("size=12");

		final MiraklCoreAttributeModel miraklCoreAttribute = mock(MiraklCoreAttributeModel.class);
		when(miraklCoreAttribute.getTypeParameter()).thenReturn("|=");

		when(attributeValue.getCoreAttribute()).thenReturn(miraklCoreAttribute);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final GenericVariantProductModel variantProduct = mock(GenericVariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		mockCommonData(data, context, productCatalogVersion, variantProduct, baseProduct);

		when(variantValueCategoryKeyGenerator.generateFor("12")).thenReturn("VVC-12");
		final VariantValueCategoryModel variantValueCategory = mock(VariantValueCategoryModel.class);
		when(categoryService.getCategoryForCode(productCatalogVersion, "VVC-12-size")).thenReturn(variantValueCategory);

		when(variantCategoryKeyGenerator.generateFor("size")).thenReturn("size");
		final VariantCategoryModel variantCategory = mock(VariantCategoryModel.class);
		when(categoryService.getCategoryForCode(productCatalogVersion, "size")).thenReturn(variantCategory);

		when(coreAttributeOwnerStrategy.determineOwner(any(MiraklCoreAttributeModel.class), any(ProductImportData.class),
				any(ProductImportFileContextData.class))).thenReturn(variantProduct);

		variantValueCategoryAttributeHandler.setValue(attributeValue, data, context);

		verify(variantProduct).setSupercategories(variantCategoriesCaptor.capture());
		verify(baseProduct).setSupercategories(baseCategoriesCaptor.capture());
		assertTrue(baseCategoriesCaptor.getValue().contains(variantCategory));
		assertTrue(variantCategoriesCaptor.getValue().contains(variantValueCategory));
	}

	/**
	 * Verify VC present VVC not present is populated in cache correctly.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyVCPresentVVCNotPresentIsPopulatedInCacheCorrectly() throws ProductImportException
	{
		final AttributeValueData attributeValue = mock(AttributeValueData.class);
		when(attributeValue.getValue()).thenReturn("size=12");

		final MiraklCoreAttributeModel miraklCoreAttribute = mock(MiraklCoreAttributeModel.class);
		when(miraklCoreAttribute.getTypeParameter()).thenReturn("|=");

		when(attributeValue.getCoreAttribute()).thenReturn(miraklCoreAttribute);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final GenericVariantProductModel variantProduct = mock(GenericVariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		mockCommonData(data, context, productCatalogVersion, variantProduct, baseProduct);

		when(variantValueCategoryKeyGenerator.generateFor("12")).thenReturn("VVC-12");

		when(variantCategoryKeyGenerator.generateFor("size")).thenReturn("size");
		final VariantCategoryModel variantCategory = mock(VariantCategoryModel.class);
		when(categoryService.getCategoryForCode(productCatalogVersion, "size")).thenReturn(variantCategory);

		when(coreAttributeOwnerStrategy.determineOwner(any(MiraklCoreAttributeModel.class), any(ProductImportData.class),
				any(ProductImportFileContextData.class))).thenReturn(variantProduct);

		variantValueCategoryAttributeHandler.setValue(attributeValue, data, context);
	}

	/**
	 * Mock common data.
	 *
	 * @param data
	 *           the data
	 * @param context
	 *           the context
	 * @param productCatalogVersion
	 *           the product catalog version
	 * @param variantProduct
	 *           the variant product
	 * @param baseProduct
	 *           the base product
	 */
	private void mockCommonData(final ProductImportData data, final ProductImportFileContextData context,
			final CatalogVersionModel productCatalogVersion, final VariantProductModel variantProduct,
			final ProductModel baseProduct)
	{
		final ProductImportGlobalContextData globalContext = mock(ProductImportGlobalContextData.class);
		final PK catalogVersionPK = PK.fromLong(1L);

		when(context.getGlobalContext()).thenReturn(globalContext);
		when(globalContext.getProductCatalogVersion()).thenReturn(catalogVersionPK);
		when(modelService.get(catalogVersionPK)).thenReturn(productCatalogVersion);

		when(variantProduct.getItemtype()).thenReturn("VariantProduct");
		when(data.getProductToUpdate()).thenReturn(variantProduct);

		when(data.getRootBaseProductToUpdate()).thenReturn(baseProduct);
		when(baseProduct.getItemtype()).thenReturn("Product");
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);
		when(data.getRawProduct()).thenReturn(rawProduct);
		final Map<String, String> values = new HashMap<>();
		values.put(SimonCoreConstants.COLOR_SWATCH_URL, null);
		when(rawProduct.getValues()).thenReturn(values);

		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
	}
}
