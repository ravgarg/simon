package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.CategorySource;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.MerchandizingCategoryModel;


@UnitTest
public class ExtMerchandizingCategoryPathValueProviderTest
{
	@InjectMocks
	ExtMerchandizingCategoryPathValueProvider extMerchandizingCategoryPathValueProvider;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private CategorySource categorySource;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private CategoryService categoryService;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetFieldValuesWithGenericVariantProduct() throws FieldValueProviderException
	{
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		final List<CategoryModel> categories = new ArrayList<>();
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final MerchandizingCategoryModel cat = new MerchandizingCategoryModel();
		final ItemModelContextImpl itemModelContext1 = (ItemModelContextImpl) cat.getItemModelContext();
		itemModelContext1.setLocaleProvider(localeProvider);
		cat.setName("SPO");
		cat.setCode("Code");
		categories.add(cat);
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, baseProduct)).thenReturn(categories);
		final List<List<CategoryModel>> pathsForCategory = new ArrayList<>();
		pathsForCategory.add(categories);
		when(categoryService.getPathsForCategory(cat)).thenReturn(pathsForCategory);
		final List<String> fieldNames = new ArrayList<>();
		fieldNames.add("FieldName");
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
		assertEquals("FieldName", extMerchandizingCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model)
				.iterator().next().getFieldName());
	}

	@Test
	public void testGetFieldValuesWithProductNotGenericVariant() throws FieldValueProviderException
	{
		final ProductModel model = new ProductModel();
		final List<CategoryModel> categories = new ArrayList<>();
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final MerchandizingCategoryModel cat = new MerchandizingCategoryModel();
		final ItemModelContextImpl itemModelContext1 = (ItemModelContextImpl) cat.getItemModelContext();
		itemModelContext1.setLocaleProvider(localeProvider);
		cat.setName("SPO");
		cat.setCode("Code");
		categories.add(cat);
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model)).thenReturn(categories);
		final List<List<CategoryModel>> pathsForCategory = new ArrayList<>();
		pathsForCategory.add(categories);
		when(categoryService.getPathsForCategory(cat)).thenReturn(pathsForCategory);
		final List<String> fieldNames = new ArrayList<>();
		fieldNames.add("FieldName");
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
		assertEquals("FieldName",
				extMerchandizingCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model).iterator().next()
						.getFieldName());
	}

	@Test
	public void testGetFieldValuesWithProductNotGenericVariantAndMerchandizingCategoriesNull() throws FieldValueProviderException
	{
		final ProductModel model = new ProductModel();
		final List<CategoryModel> categories = new ArrayList<>();
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model)).thenReturn(categories);
		assertEquals(Collections.emptyList(),
				extMerchandizingCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model));
	}

}
