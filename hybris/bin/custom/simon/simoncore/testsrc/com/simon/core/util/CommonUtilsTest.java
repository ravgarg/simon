package com.simon.core.util;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.account.data.DesignerData;


/**
 * Common Utils test class.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CommonUtilsTest
{
	@InjectMocks
	CommonUtils commonUtils;
	@Mock
	DesignerData designerData1, designerData2;

	@Before
	public void prepare()
	{
		initMocks(this);
	}

	@Test
	public void testGetDesignerOrderByName()
	{
		designerData1 = mock(DesignerData.class);
		designerData2 = mock(DesignerData.class);
		final List<DesignerData> designer = new ArrayList<DesignerData>();
		designer.add(designerData1);
		designer.add(designerData2);
		when(designerData1.getName()).thenReturn("ABC");
		when(designerData2.getName()).thenReturn("US");
		Collections.sort(designer, (o1, o2) -> o1.getName().compareTo(o2.getName()));
		assertThat(commonUtils.getDesignerOrderByName(designer)).isNotEmpty();
	}

	@Test
	public void testCreateArrayListFrom()
	{
		final DiscountValue discountValue = mock(DiscountValue.class);
		final List<DiscountValue> existingList = new ArrayList<>();
		existingList.add(discountValue);
		final List<DiscountValue> newList = new ArrayList<>(existingList);
		assertEquals(newList, CommonUtils.createArrayListFrom(existingList));
	}

	@Test
	public void testCreateEmptyArrayListFrom()
	{
		final List<DiscountValue> existingList = new ArrayList<>();
		final List<DiscountValue> newList = new ArrayList<>();
		assertEquals(newList, CommonUtils.createArrayListFrom(existingList));
	}

	@Test
	public void testconvertStringToDate_InvalidDate()
	{
		CommonUtils.convertStringToDate("InvalidDate");

	}

	@Test
	public void testconvertStringToDate_validDate()
	{
		CommonUtils.convertStringToDate("2017-01-01 12:00:00.0");

	}


	@Test
	public void testGetValueFromMap()
	{
		final Map<String, Double> map = new HashMap<>();
		final String key = "SampleKey";
		final Double value = 69d;
		map.put(key, value);
		assertEquals("The value should be:" + value.doubleValue(), value.doubleValue(), CommonUtils.getValueFromMap(map, key), 0d);

	}

	@Test
	public void testGetValueFromMap_emptyMapCase()
	{
		final Map<String, Double> map = new HashMap<>();
		final String key = "SampleKey";
		final double expectedValue = 0.0;
		assertEquals("The value should be:" + expectedValue, expectedValue, CommonUtils.getValueFromMap(map, key), 0d);

	}

	@Test
	public void testcreateMapFrom_emptyMapCase()
	{
		final Map<String, String> map = new HashMap<>();
		final Map<String, String> returnedMap = CommonUtils.createMapFrom(map);
		assertThat(returnedMap).isEmpty();

	}

	@Test
	public void testcreateMapFrom_NonEmptyMapCase()
	{
		final Map<String, String> map = new HashMap<>();
		final String key = "SampleKey";
		final String value = "SampleValue";
		map.put(key, value);
		final Map<String, String> returnedMap = CommonUtils.createMapFrom(map);
		assertThat(returnedMap).isNotNull();
		assertThat(returnedMap).isNotEmpty();
		assertThat(returnedMap.size()).isEqualTo(1);
		assertThat(returnedMap.containsKey(key));
		assertThat(returnedMap.get(key)).isEqualTo(value);
	}


	@Test
	public void testFormat()
	{
		final String message = "Product Id : %s & Product Name : %s";
		final String productIdValue = "123";
		final String productNameValue = "Jeans";
		final String expectedMessage = "Product Id : " + productIdValue + " & Product Name : " + productNameValue;


		final String actualMessage = CommonUtils.format(message, productIdValue, productNameValue);
		assertEquals("The return value should be: " + expectedMessage, expectedMessage, actualMessage);

	}


}
