package com.simon.core.mirakl.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportErrorData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportResultData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * ExtProductImportServiceImplTest unit test for {@link ExtProductImportServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtProductImportServiceImplTest
{

	@InjectMocks
	private ExtProductImportServiceImpl extProductImportServiceImpl;

	@Mock
	private SessionService sessionService;

	@Mock
	private ConcurrentLinkedQueue<String> concurrentLinkedQueue;

	@Mock
	private BlockingQueue<ProductImportResultData> blockingQueue;

	@Mock
	private Converter<ProductImportException, ProductImportErrorData> errorDataConverter;

	/**
	 * Verify error message is formatted with import and shop id.
	 */
	@Test
	public void verifyErrorMessageIsFormattedWithImportAndShopId()
	{
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(context.getProductFailure()).thenReturn(concurrentLinkedQueue);
		when(context.getImportResultQueue()).thenReturn(blockingQueue);
		when(context.getMiraklImportId()).thenReturn("12345");
		when(context.getShopId()).thenReturn("2025");
		final ProductImportException exception = mock(ProductImportException.class);

		final MiraklRawProductModel miraklRawProduct = mock(MiraklRawProductModel.class);
		when(miraklRawProduct.getRowNumber()).thenReturn(100);
		when(exception.getRawProduct()).thenReturn(miraklRawProduct);
		extProductImportServiceImpl.writeErrorToResultQueue(exception, context);

		verify(concurrentLinkedQueue).add(Matchers.contains("2025"));
		verify(concurrentLinkedQueue).add(Matchers.contains("12345"));
	}
}

