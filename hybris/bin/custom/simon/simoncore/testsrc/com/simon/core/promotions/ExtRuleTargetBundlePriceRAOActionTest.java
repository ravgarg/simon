package com.simon.core.promotions;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleActionContext;
import de.hybris.platform.ruleengineservices.calculation.RuleEngineCalculationService;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.EntriesSelectionStrategyRPD;
import de.hybris.platform.ruleengineservices.rao.OrderEntryConsumedRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import de.hybris.platform.ruleengineservices.util.OrderUtils;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.simon.core.promotions.service.RetailerRuleEngineCalculationService;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class ExtRuleTargetBundlePriceRAOActionTest extends ExtRuleTargetBundlePriceRAOAction
{
	@InjectMocks
	@Spy
	private ExtRuleTargetBundlePriceRAOActionTest raoAction;
	@Mock
	private RuleEngineCalculationService ruleEngineCalculationService;
	@Mock
	private RetailerRuleEngineCalculationService retailerRuleEngineCalculationService;
	@Mock
	private DefaultDroolsRuleActionContext context;
	@Mock
	private DiscountRAO discount;
	@Mock
	private RetailerRAO retailer;
	@Mock
	private Map<String, Object> map;
	@Mock
	private CartRAO cartRao;
	@Mock
	private RuleEngineResultRAO result;
	@Mock
	private AbstractRuleActionRAO abstractRuleActionRAO;
	@Mock
	private RetailerRAO retailerRAO;
	@Mock
	private OrderUtils orderUtils;

	@Before
	public void setup()
	{
		initMocks(this);
		when(context.getVariables()).thenReturn(map);
		final Set<RetailerRAO> retailers = new HashSet();
		retailers.add(retailer);
		when(map.get(RetailerRAO.class.getName())).thenReturn(retailers);
		when(retailer.getTotal()).thenReturn(BigDecimal.valueOf(10.0));

	}

	@Test
	public void performActionTestWhenThereIsNotEnoughQty()
	{
		final BigDecimal amount = BigDecimal.ZERO;
		raoAction.setRuleEngineCalculationService(ruleEngineCalculationService);
		doReturn(discount).when(ruleEngineCalculationService).addOrderEntryLevelDiscount(null, true, BigDecimal.ZERO);
		doReturn(BigDecimal.ONE).when(retailerRAO).getTotal();
		doReturn(result).when(context).getRuleEngineResultRao();
		final List<EntriesSelectionStrategyRPD> selectionContainers = null;
		final BigDecimal discountValueForCartCurrency = null;
		doNothing().when(raoAction).validateSelectionStrategy(selectionContainers, context);
		doReturn(false).when(raoAction).hasEnoughQuantity(selectionContainers);
		raoAction.performAction(context, selectionContainers, discountValueForCartCurrency);
		verify(raoAction, times(1)).validateSelectionStrategy(selectionContainers, context);
	}

	@Test
	public void performActionTestWhenDiscountedPriceIsMore()
	{
		final BigDecimal amount = BigDecimal.ZERO;
		raoAction.setRuleEngineCalculationService(ruleEngineCalculationService);
		doReturn(discount).when(ruleEngineCalculationService).addOrderEntryLevelDiscount(null, true, BigDecimal.ZERO);
		doReturn(BigDecimal.ONE).when(retailerRAO).getTotal();
		doReturn(result).when(context).getRuleEngineResultRao();
		final List<EntriesSelectionStrategyRPD> selectionContainers = null;
		final BigDecimal discountValueForCartCurrency = new BigDecimal(11L);
		doNothing().when(raoAction).validateSelectionStrategy(selectionContainers, context);
		doReturn(true).when(raoAction).hasEnoughQuantity(selectionContainers);
		doReturn(1).when(raoAction).adjustStrategyQuantity(selectionContainers, -1);
		final Map<Integer, Integer> selectedQuantityMap = null;
		doReturn(selectedQuantityMap).when(raoAction).getSelectedOrderEntryQuantities(selectionContainers);
		final Set<OrderEntryRAO> orderEntrySet = null;
		doReturn(orderEntrySet).when(raoAction).getSelectedOrderEntryRaos(selectionContainers, selectedQuantityMap);
		final BigDecimal price = new BigDecimal(10L);
		when(ruleEngineCalculationService.getCurrentPrice(orderEntrySet, selectedQuantityMap)).thenReturn(price);
		raoAction.performAction(context, selectionContainers, discountValueForCartCurrency);
		verify(raoAction, times(1)).validateSelectionStrategy(selectionContainers, context);
	}

	@Test
	public void performActionTestWhenDiscountedPriceIsLess()
	{
		final BigDecimal amount = BigDecimal.ZERO;
		raoAction.setRuleEngineCalculationService(ruleEngineCalculationService);
		doReturn(discount).when(ruleEngineCalculationService).addOrderEntryLevelDiscount(null, true, BigDecimal.ZERO);
		doReturn(BigDecimal.ONE).when(retailerRAO).getTotal();
		doReturn(result).when(context).getRuleEngineResultRao();
		final List<EntriesSelectionStrategyRPD> selectionContainers = null;
		final BigDecimal discountValueForCartCurrency = BigDecimal.valueOf(10L);
		doNothing().when(raoAction).validateSelectionStrategy(selectionContainers, context);
		doReturn(true).when(raoAction).hasEnoughQuantity(selectionContainers);
		doReturn(1).when(raoAction).adjustStrategyQuantity(selectionContainers, -1);
		final Map<Integer, Integer> selectedQuantityMap = null;
		doReturn(selectedQuantityMap).when(raoAction).getSelectedOrderEntryQuantities(selectionContainers);
		final Set<OrderEntryRAO> orderEntrySet = new HashSet<>();
		doReturn(orderEntrySet).when(raoAction).getSelectedOrderEntryRaos(selectionContainers, selectedQuantityMap);
		final BigDecimal price = BigDecimal.valueOf(11L);
		when(ruleEngineCalculationService.getCurrentPrice(orderEntrySet, selectedQuantityMap)).thenReturn(price);
		when(ruleEngineCalculationService.addOrderLevelDiscount(cartRao, true, BigDecimal.valueOf(1))).thenReturn(discount);
		final RuleEngineResultRAO ruleEngineResult = mock(RuleEngineResultRAO.class);
		when(context.getRuleEngineResultRao()).thenReturn(ruleEngineResult);
		final LinkedHashSet<AbstractRuleActionRAO> actions = new LinkedHashSet<>();
		when(ruleEngineResult.getActions()).thenReturn(actions);
		when(context.getCartRao()).thenReturn(cartRao);
		when(cartRao.getCurrencyIsoCode()).thenReturn("USD");
		final BigDecimal currentPriceOfToBeDiscountedOrderEntries;
		final BigDecimal fixedPrice;
		when(orderUtils.applyRounding(BigDecimal.valueOf(1), "USD")).thenReturn(BigDecimal.valueOf(1));
		when(orderUtils.applyRounding(new BigDecimal(1L), "USD")).thenReturn(new BigDecimal(1L));
		doReturn(orderEntrySet).when(raoAction).getSelectedOrderEntryRaos(selectionContainers, selectedQuantityMap);
		doReturn(orderEntrySet).when(raoAction).getSelectedOrderEntryRaos(selectionContainers, selectedQuantityMap);
		final Set<OrderEntryConsumedRAO> orderConsumedEntrySet = null;
		doReturn(orderConsumedEntrySet).when(raoAction).consumeOrderEntries(orderEntrySet, selectedQuantityMap, discount);
		doNothing().when(context).insertFacts(Mockito.anyCollection());
		doNothing().when(context).updateFacts(Mockito.anyCollection());
		doNothing().when(raoAction).trackRuleExecution(context);
		doNothing().when(raoAction).trackRuleGroupExecutions(context);

		raoAction.performAction(context, selectionContainers, discountValueForCartCurrency);

		verify(raoAction, times(1)).validateSelectionStrategy(selectionContainers, context);
	}

}
