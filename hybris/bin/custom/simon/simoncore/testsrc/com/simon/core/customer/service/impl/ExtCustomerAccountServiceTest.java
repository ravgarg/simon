package com.simon.core.customer.service.impl;


import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.customer.dao.ExtCustomerAccountDao;
import com.simon.core.model.DealModel;
import com.simon.core.model.DesignerModel;
import com.simon.core.strategies.impl.ExtCheckoutCustomerStrategy;


@UnitTest
public class ExtCustomerAccountServiceTest
{
	@Spy
	@InjectMocks
	private ExtCustomerAccountServiceImpl extCustomerAccountServicesImpl;
	@Mock
	private ExtCustomerAccountDao extCustomerAccountDao;

	@Mock
	private ExtCheckoutCustomerStrategy checkoutCustomerStrategy;
	@Mock
	private CustomerEmailResolutionService customerEmailResolutionService;
	@Mock
	private ModelService modelService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private CustomerNameStrategy customerNameStrategy;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testUpdateProfileInfo() throws DuplicateUidException
	{
		final ModelService modelService = Mockito.mock(ModelService.class);
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		extCustomerAccountServicesImpl.setModelService(modelService);
		extCustomerAccountServicesImpl.updateProfileInfo(customer);
		Mockito.verify(modelService, Mockito.times(1)).save(customer);
	}

	@Test
	public void testFindListOfAllDesigners() throws DuplicateUidException
	{
		final List<DesignerModel> designerList = new ArrayList<DesignerModel>();
		final DesignerModel designerModel = mock(DesignerModel.class);
		designerList.add(designerModel);
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		when(extCustomerAccountDao.findListOfAllDesigners(customer)).thenReturn(designerList);
		extCustomerAccountServicesImpl.findListOfAllDesigners(customer);
	}


	@Test
	public void testcreatePaymentInfoSubscriptionVisa()
	{
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		extCustomerAccountServicesImpl.setCustomerEmailResolutionService(customerEmailResolutionService);
		extCustomerAccountServicesImpl.setCommonI18NService(commonI18NService);
		final CCPaymentInfoData extPaymentInfoData = new CCPaymentInfoData();
		extCustomerAccountServicesImpl.setModelService(modelService);
		extPaymentInfoData.setPaymentToken("123");
		extPaymentInfoData.setLastFourDigits("1234");
		extPaymentInfoData.setExpiryMonth("12");
		extPaymentInfoData.setExpiryYear("2018");
		extPaymentInfoData.setAccountHolderName("test");
		extPaymentInfoData.setCardNumber("1234");
		extPaymentInfoData.setCardType("visa");
		extPaymentInfoData.setSaveInAccount(true);
		extPaymentInfoData.setPaymentToken("121212");
		final AddressData billingAddress = new AddressData();
		billingAddress.setFirstName("First");
		billingAddress.setLastName("Last");
		billingAddress.setLine1("Line1");
		billingAddress.setLine2("Line2");
		billingAddress.setTown("Town");
		billingAddress.setPostalCode("192");
		billingAddress.setPhone("123");
		final RegionData region = new RegionData();
		region.setIsocode("AL");
		billingAddress.setRegion(region);
		extPaymentInfoData.setBillingAddress(billingAddress);
		final CountryData country = new CountryData();
		country.setIsocode("US");
		billingAddress.setCountry(country);
		final CountryModel countryModel = Mockito.mock(CountryModel.class);
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customer);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(modelService.create(CreditCardPaymentInfoModel.class)).thenReturn(creditCardPaymentInfoModel);
		when(creditCardPaymentInfoModel.getCcOwner()).thenReturn("Test Test");
		extCustomerAccountServicesImpl.setCustomerNameStrategy(customerNameStrategy);
		final String[] names =
		{ "test", "test" };
		when(customerNameStrategy.splitName("Test Test")).thenReturn(names);
		when(commonI18NService.getCountry("US")).thenReturn(countryModel);
		when(customerEmailResolutionService.getEmailForCustomer(customer)).thenReturn("test@test.com");
		assertNotNull(extCustomerAccountServicesImpl.createPaymentInfoSubscription(extPaymentInfoData));
	}

	@Test
	public void testcreatePaymentInfoSubscriptionAmericanExpress()
	{
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		extCustomerAccountServicesImpl.setCustomerEmailResolutionService(customerEmailResolutionService);
		extCustomerAccountServicesImpl.setCommonI18NService(commonI18NService);
		final CCPaymentInfoData extPaymentInfoData = new CCPaymentInfoData();
		extCustomerAccountServicesImpl.setModelService(modelService);
		extPaymentInfoData.setPaymentToken("123");
		extPaymentInfoData.setLastFourDigits("1234");
		extPaymentInfoData.setExpiryMonth("12");
		extPaymentInfoData.setExpiryYear("2018");
		extPaymentInfoData.setAccountHolderName("test");
		extPaymentInfoData.setCardNumber("1234");
		extPaymentInfoData.setCardType("american_express");
		extPaymentInfoData.setSaveInAccount(true);
		extPaymentInfoData.setPaymentToken("121212");
		final AddressData billingAddress = new AddressData();
		billingAddress.setFirstName("First");
		billingAddress.setLastName("Last");
		billingAddress.setLine1("Line1");
		billingAddress.setLine2("Line2");
		billingAddress.setTown("Town");
		billingAddress.setPostalCode("192");
		billingAddress.setPhone("123");
		final RegionData region = new RegionData();
		region.setIsocode("AL");
		billingAddress.setRegion(region);
		extPaymentInfoData.setBillingAddress(billingAddress);
		final CountryData country = new CountryData();
		country.setIsocode("US");
		billingAddress.setCountry(country);
		final CountryModel countryModel = Mockito.mock(CountryModel.class);
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customer);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(creditCardPaymentInfoModel.getCcOwner()).thenReturn("Test Test");
		extCustomerAccountServicesImpl.setCustomerNameStrategy(customerNameStrategy);
		final String[] names =
		{ "test", "test" };
		when(customerNameStrategy.splitName("Test Test")).thenReturn(names);
		when(modelService.create(CreditCardPaymentInfoModel.class)).thenReturn(creditCardPaymentInfoModel);
		when(commonI18NService.getCountry("US")).thenReturn(countryModel);
		when(customerEmailResolutionService.getEmailForCustomer(customer)).thenReturn("test@test.com");
		assertNotNull(extCustomerAccountServicesImpl.createPaymentInfoSubscription(extPaymentInfoData));
	}

	@Test
	public void testcreatePaymentInfoSubscriptionMaster()
	{
		final AddressModel addressModel = Mockito.mock(AddressModel.class);
		extCustomerAccountServicesImpl.setCustomerEmailResolutionService(customerEmailResolutionService);
		extCustomerAccountServicesImpl.setCommonI18NService(commonI18NService);
		extCustomerAccountServicesImpl.setFlexibleSearchService(flexibleSearchService);
		final CCPaymentInfoData extPaymentInfoData = new CCPaymentInfoData();
		extCustomerAccountServicesImpl.setModelService(modelService);
		extPaymentInfoData.setPaymentToken("123");
		extPaymentInfoData.setLastFourDigits("1234");
		extPaymentInfoData.setExpiryMonth("12");
		extPaymentInfoData.setExpiryYear("2018");
		extPaymentInfoData.setAccountHolderName("test");
		extPaymentInfoData.setCardNumber("1234");
		extPaymentInfoData.setCardType("master");
		extPaymentInfoData.setSaveInAccount(true);
		extPaymentInfoData.setPaymentToken("121212");
		final AddressData billingAddress = new AddressData();
		billingAddress.setTitle("Mr");
		final TitleModel titleModel = Mockito.mock(TitleModel.class);
		when(modelService.create(TitleModel.class)).thenReturn(titleModel);
		when(flexibleSearchService.getModelByExample(titleModel)).thenReturn(titleModel);
		billingAddress.setFirstName("First");
		billingAddress.setLastName("Last");
		billingAddress.setLine1("Line1");
		billingAddress.setLine2("Line2");
		billingAddress.setTown("Town");
		billingAddress.setPostalCode("192");
		billingAddress.setPhone("123");
		billingAddress.setId("12345");
		final RegionData region = new RegionData();
		region.setIsocode("AL");
		billingAddress.setRegion(region);
		extPaymentInfoData.setBillingAddress(billingAddress);
		final CountryData country = new CountryData();
		country.setIsocode("US");
		billingAddress.setCountry(country);
		final CountryModel countryModel = Mockito.mock(CountryModel.class);
		final CustomerModel customer = Mockito.mock(CustomerModel.class);
		when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customer);
		when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		final CreditCardPaymentInfoModel creditCardPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);
		when(modelService.create(CreditCardPaymentInfoModel.class)).thenReturn(creditCardPaymentInfoModel);
		when(creditCardPaymentInfoModel.getCcOwner()).thenReturn("Test Test");
		extCustomerAccountServicesImpl.setCustomerNameStrategy(customerNameStrategy);
		final String[] names =
		{ "test", "test" };
		when(customerNameStrategy.splitName("Test Test")).thenReturn(names);
		when(commonI18NService.getCountry("US")).thenReturn(countryModel);
		when(customerEmailResolutionService.getEmailForCustomer(customer)).thenReturn("test@test.com");
		assertNotNull(extCustomerAccountServicesImpl.createPaymentInfoSubscription(extPaymentInfoData));
	}

	@Test
	public void testUnlinkCCPaymentInfo()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CreditCardPaymentInfoModel creditCardPaymentInfo = Mockito.mock(CreditCardPaymentInfoModel.class);
		final Collection<PaymentInfoModel> paymentInfos = new ArrayList();
		paymentInfos.add(creditCardPaymentInfo);
		when(customerModel.getPaymentInfos()).thenReturn(paymentInfos);
		final AddressModel billingAddress = Mockito.mock(AddressModel.class);
		when(billingAddress.getDuplicate()).thenReturn(Boolean.FALSE);
		when(creditCardPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
		final List<AddressModel> addressList = new ArrayList();
		final AddressModel addresses = Mockito.mock(AddressModel.class);
		addressList.add(addresses);
		when(extCustomerAccountDao.findAddressByDuplicateFrom(Mockito.anyString())).thenReturn(addressList);
		Mockito.doReturn("21212121121").when(extCustomerAccountServicesImpl).getAddressPk(creditCardPaymentInfo);
		extCustomerAccountServicesImpl.unlinkCCPaymentInfo(customerModel, creditCardPaymentInfo);
		Mockito.verify(modelService, Mockito.times(1)).save(customerModel);

	}

	@Test
	public void testUnlinkCCPaymentInfoDuplicateAddress()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CreditCardPaymentInfoModel creditCardPaymentInfo = Mockito.mock(CreditCardPaymentInfoModel.class);
		final Collection<PaymentInfoModel> paymentInfos = new ArrayList();
		paymentInfos.add(creditCardPaymentInfo);
		when(customerModel.getPaymentInfos()).thenReturn(paymentInfos);
		final AddressModel billingAddress = Mockito.mock(AddressModel.class);
		when(billingAddress.getDuplicate()).thenReturn(Boolean.TRUE);
		when(creditCardPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
		final List<AddressModel> addressList = new ArrayList();
		final AddressModel addresses = Mockito.mock(AddressModel.class);
		addressList.add(addresses);
		extCustomerAccountServicesImpl.unlinkCCPaymentInfo(customerModel, creditCardPaymentInfo);
		Mockito.verify(modelService, Mockito.times(1)).save(customerModel);

	}

	@Test(expected = IllegalArgumentException.class)
	public void testUnlinkCCPaymentInfoException()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final CreditCardPaymentInfoModel creditCardPaymentInfo = Mockito.mock(CreditCardPaymentInfoModel.class);
		final CreditCardPaymentInfoModel creditCardPaymentInfo1 = Mockito.mock(CreditCardPaymentInfoModel.class);
		final Collection<PaymentInfoModel> paymentInfos = new ArrayList();
		paymentInfos.add(creditCardPaymentInfo);
		when(customerModel.getPaymentInfos()).thenReturn(paymentInfos);
		final AddressModel billingAddress = Mockito.mock(AddressModel.class);
		when(billingAddress.getDuplicate()).thenReturn(Boolean.TRUE);
		when(creditCardPaymentInfo.getBillingAddress()).thenReturn(billingAddress);
		final List<AddressModel> addressList = new ArrayList();
		final AddressModel addresses = Mockito.mock(AddressModel.class);
		addressList.add(addresses);
		extCustomerAccountServicesImpl.unlinkCCPaymentInfo(customerModel, creditCardPaymentInfo1);
		Mockito.verify(modelService, Mockito.times(1)).save(customerModel);

	}

	@Test
	public void testFindListOfAllStores()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		final List<ShopModel> shops = new ArrayList();
		final ShopModel shopModel = Mockito.mock(ShopModel.class);
		shops.add(shopModel);
		when(extCustomerAccountDao.findListOfAllStores(customerModel)).thenReturn(shops);
		assertNotNull(extCustomerAccountServicesImpl.findListOfAllStores(customerModel));
	}


	/**
	 * Test to find list of all designers.
	 */
	@Test
	public void testFindListOfDesigners()
	{
		final List<DesignerModel> designer = new ArrayList<>();
		final DesignerModel designerModel = Mockito.mock(DesignerModel.class);
		designer.add(designerModel);
		Mockito.when(extCustomerAccountDao.findListOfAllDesigners()).thenReturn(designer);
		extCustomerAccountServicesImpl.findListOfDesigners();
		Mockito.verify(extCustomerAccountDao).findListOfAllDesigners();
	}

	/**
	 * Test to find list of all deals.
	 */
	@Test
	public void testAllFavouriteDeals()
	{
		final CustomerModel customerModel = new CustomerModel();
		final List<DealModel> deal = new ArrayList<>();
		final Set<DealModel> deals = new HashSet<>();
		final DealModel dealModel = Mockito.mock(DealModel.class);
		deal.add(dealModel);
		deals.addAll(deal);
		customerModel.setMyOffers(deals);
		Mockito.when(extCustomerAccountDao.findListOfAllDeals(customerModel)).thenReturn(deal);
		extCustomerAccountServicesImpl.getAllFavouriteDeals(customerModel);
		Mockito.verify(extCustomerAccountDao).findListOfAllDeals(customerModel);
	}

}
