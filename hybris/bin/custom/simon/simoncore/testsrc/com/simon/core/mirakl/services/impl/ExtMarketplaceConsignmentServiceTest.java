package com.simon.core.mirakl.services.impl;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.daos.MarketplaceConsignmentDao;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;
import com.simon.core.mirakl.services.DefaultOrderStatusUpdateService;


/**
 * ExtMarketplaceConsignmentServiceTest, unit test for {@link ExtMarketplaceConsignmentService}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMarketplaceConsignmentServiceTest
{
	@InjectMocks
	@Spy
	private ExtMarketplaceConsignmentService marketplaceConsignmentService;

	@Mock
	private Converter<Pair<OrderModel, MiraklOrder>, MarketplaceConsignmentModel> miraklCreateConsignmentConverter;

	@Mock
	private DefaultOrderStatusUpdateService orderStatusUpdateService;
	@Mock
	private JsonMarshallingService jsonMarshallingService;
	@Mock
	private ModelService modelService;

	@Mock
	private MarketplaceConsignmentModel marketplaceConsignment;
	@Mock
	private ConsignmentEntryModel consignmentEntry;
	@Mock
	private AbstractOrderEntryModel orderEntry;
	@Mock
	private ProductModel product;
	@Mock
	private StockLevelModel stockLevel;
	@Mock
	private OrderModel order;

	@Mock
	private MarketplaceConsignmentDao marketplaceConsignmentDao;

	@Mock
	private MiraklOrder miraklOrder;
	@Mock
	private MiraklCreatedOrders miraklCreatedOrders;

	@Mock
	private StockService stockService;

	private final Set<ConsignmentEntryModel> consignmentEntries = new HashSet<>();


	private final Set<StockLevelModel> stockLevels = new HashSet<>();

	private final Collection<StockLevelModel> stockLevels1 = new HashSet<>();

	/**
	 * Setup method.
	 */
	@Before
	public void setUp()
	{
		when(miraklOrder.getId()).thenReturn("Test");
		when(jsonMarshallingService.toJson(miraklOrder, MiraklOrder.class)).thenReturn(Mockito.anyString());
		when(marketplaceConsignmentDao.findMarketplaceConsignmentByCode("Test")).thenReturn(marketplaceConsignment);
		when(marketplaceConsignmentService.getMarketplaceConsignmentForCode("Test")).thenReturn(marketplaceConsignment);

	}

	/**
	 *
	 */
	@Test
	public void receiveConsignmentUpdate()
	{
		marketplaceConsignmentService.receiveConsignmentUpdate(miraklOrder);
		verify(orderStatusUpdateService).updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
		verify(modelService).save(marketplaceConsignment);
	}

	/**
	 *
	 */
	@Test
	public void createMarketplaceConsignments()
	{
		final List<MiraklOrder> miraklOrders = new ArrayList<>();
		miraklOrders.add(miraklOrder);
		when(miraklCreateConsignmentConverter.convert(Pair.of(order, miraklOrder))).thenReturn(marketplaceConsignment);
		when(miraklOrder.getCommercialId()).thenReturn("retailerACommercialId");
		when(miraklCreatedOrders.getOrders()).thenReturn(miraklOrders);

		consignmentEntries.add(consignmentEntry);
		when(marketplaceConsignment.getConsignmentEntries()).thenReturn(consignmentEntries);
		when(consignmentEntry.getOrderEntry()).thenReturn(orderEntry);
		when(consignmentEntry.getQuantity()).thenReturn(2l);
		when(orderEntry.getProduct()).thenReturn(product);
		stockLevels.add(stockLevel);
		stockLevel.setReserved(10);
		when(product.getStockLevels()).thenReturn(stockLevels);
		when(stockService.getAllStockLevels(product)).thenReturn(stockLevels1);
		marketplaceConsignmentService.createMarketplaceConsignments(order, miraklCreatedOrders);

		verify(miraklCreateConsignmentConverter).convert(Pair.of(order, miraklOrder));
	}
}
