package com.simon.core.services.impl;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * This is the class to test ExtCountryService
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCountryServiceImplTest
{
	@InjectMocks
	private final ExtCountryServiceImpl extCountryService = new ExtCountryServiceImpl();
	@Mock
	private GenericDao<CountryModel> extGenericCountryDao;

	/**
	 * Test for the case when service is returning Country Model
	 */
	@Test
	public void getCountryForExteralIdShouldReturnAResult()
	{
		when(extGenericCountryDao.find(anyMapOf(String.class, Object.class))).thenReturn(singletonList(new CountryModel()));

		assertNotNull(extCountryService.getCountryForExternalId("testId"));
	}

	/**
	 * Test for the case when service is not returning Country
	 */
	@Test
	public void getCountryForExteralIdShouldReturnNullWhenNoResult()
	{
		when(extGenericCountryDao.find(anyMapOf(String.class, Object.class))).thenReturn(new ArrayList<CountryModel>());

		assertNull(extCountryService.getCountryForExternalId("testId"));
	}

	/**
	 * Test for the case when service is throwing Exception
	 */
	@Test(expected = AmbiguousIdentifierException.class)
	public void getCountryForExteralIdShouldThrowExceptionWhenMultipleResults()
	{
		when(extGenericCountryDao.find(anyMapOf(String.class, Object.class)))
				.thenReturn(asList(new CountryModel(), new CountryModel()));
		extCountryService.getCountryForExternalId("testId");
	}
}
