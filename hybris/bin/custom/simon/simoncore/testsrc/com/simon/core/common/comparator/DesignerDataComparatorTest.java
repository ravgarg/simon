package com.simon.core.common.comparator;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.facades.account.data.DesignerData;


/**
 * compare designerData based on name.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DesignerDataComparatorTest
{
	@InjectMocks
	DesignerDataComparator designerDataComparator;
	@Mock
	DesignerData designerData1, designerData2;

	@Test
	public void testCompare()
	{
		designerData1 = mock(DesignerData.class);
		designerData2 = mock(DesignerData.class);
		when(designerData1.getName()).thenReturn("ABC");
		when(designerData2.getName()).thenReturn("US");
		final int compare = designerDataComparator.compare(designerData1, designerData2);

		assertThat(compare).isNegative();
	}
}
