package com.simon.mediaconversion.imagemagick;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.mediaconversion.MediaConversionService;
import de.hybris.platform.mediaconversion.model.ConversionMediaFormatModel;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Junit Test case to test ExtImageMagickMediaConversionStrategy
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtImageMagickMediaConversionStrategyTest
{
	@InjectMocks
	@Spy
	private ExtImageMagickMediaConversionStrategy extImageMagickMediaConversionStrategy;
	@Mock
	private MediaConversionService mediaConversionService;
	@Mock
	private ConversionMediaFormatModel format;
	@Mock
	private BufferedImage bufferedImage;

	/**
	 * @throws IOException
	 */
	@Test
	public void buildCommand() throws IOException
	{

		when(format.getConversion()).thenReturn("-resize 84x128");
		final String[] convertWidthNHeight =
		{ "84", "128" };
		doReturn("113x128").when(extImageMagickMediaConversionStrategy).convertDimensions("input", convertWidthNHeight);
		assertEquals(4,
				extImageMagickMediaConversionStrategy.buildCommand(mediaConversionService, "input", "output", format).size());
	}

	@Test
	public void buildCommandFormatisNull() throws IOException
	{

		when(format.getConversion()).thenReturn(null);
		final String[] convertWidthNHeight =
		{ "84", "128" };
		doReturn("113x128").when(extImageMagickMediaConversionStrategy).convertDimensions("input", convertWidthNHeight);
		assertEquals(2,
				extImageMagickMediaConversionStrategy.buildCommand(mediaConversionService, "input", "output", format).size());
	}

	@Test
	public void buildCommandFormatisEmpty() throws IOException
	{

		when(format.getConversion()).thenReturn("");
		final String[] convertWidthNHeight =
		{ "84", "128" };
		doReturn("113x128").when(extImageMagickMediaConversionStrategy).convertDimensions("input", convertWidthNHeight);
		assertEquals(2,
				extImageMagickMediaConversionStrategy.buildCommand(mediaConversionService, "input", "output", format).size());
	}

	@Test
	public void convertDimensionsWgtH() throws IOException
	{
		final String[] convertWidthNHeight =
		{ "84", "128" };
		doReturn(bufferedImage).when(extImageMagickMediaConversionStrategy).getImage("input");
		when(bufferedImage.getWidth()).thenReturn(84);
		when(bufferedImage.getHeight()).thenReturn(128);
		;
		assertEquals("84x128", extImageMagickMediaConversionStrategy.convertDimensions("input", convertWidthNHeight));
	}

	@Test
	public void convertDimensionsWltH() throws IOException
	{
		final String[] convertWidthNHeight =
		{ "84", "128" };
		doReturn(bufferedImage).when(extImageMagickMediaConversionStrategy).getImage("input");
		when(bufferedImage.getWidth()).thenReturn(184);
		when(bufferedImage.getHeight()).thenReturn(128);
		assertEquals("84x58", extImageMagickMediaConversionStrategy.convertDimensions("input", convertWidthNHeight));
	}

	@Test
	public void convertDimensionsWlt() throws IOException
	{
		final String[] convertWidthNHeight =
		{ "128" };
		doReturn(bufferedImage).when(extImageMagickMediaConversionStrategy).getImage("input");
		when(bufferedImage.getWidth()).thenReturn(184);
		when(bufferedImage.getHeight()).thenReturn(128);
		assertNull(extImageMagickMediaConversionStrategy.convertDimensions("input", convertWidthNHeight));
	}

}
