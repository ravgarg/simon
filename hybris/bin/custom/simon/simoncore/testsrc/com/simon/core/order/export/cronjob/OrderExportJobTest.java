package com.simon.core.order.export.cronjob;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobResult;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dto.OrderExportData;
import com.simon.core.model.OrderExportCronJobModel;
import com.simon.core.services.impl.ExtOrderServiceImpl;


@UnitTest
public class OrderExportJobTest
{
	@InjectMocks
	private OrderExportJob orderExportFromSPOJob;


	@Mock
	private ExtOrderServiceImpl extOrderService;

	@Mock
	private List<OrderModel> orderModelList;

	@Mock
	private Map<String, List<OrderExportData>> orderList;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void findOrdersToExportTest()
	{
		final int day = 1;
		final String startHours = "00";
		final String endHours = "23";
		when(extOrderService.findOrdersToExport(day, startHours, endHours)).thenReturn(orderModelList);
		final OrderExportCronJobModel cronJobModel = mock(OrderExportCronJobModel.class);
		final PerformResult objPerformResult = orderExportFromSPOJob.perform(cronJobModel);
		Assert.assertNotNull(objPerformResult);
		assertThat(objPerformResult.getResult().getCode(), is(CronJobResult.SUCCESS));
	}

	@Test
	public void findOrdersToExportFalseTest()
	{
		final List<OrderModel> oModelList = new ArrayList<>();
		final OrderModel oModel = new OrderModel();
		oModel.setCode("Test");
		oModelList.add(oModel);
		final int day = 1;
		final String startHours = "00";
		final String endHours = "23";
		when(extOrderService.findOrdersToExport(day, startHours, endHours)).thenReturn(oModelList);
		when(extOrderService.createOrderExportFiles(oModelList)).thenReturn(Boolean.TRUE);
		when(extOrderService.moveOrderexportCsvfileTosftp()).thenReturn(Boolean.FALSE);
		final OrderExportCronJobModel cronJobModel = mock(OrderExportCronJobModel.class);
		final PerformResult objPerformResult = orderExportFromSPOJob.perform(cronJobModel);
		Assert.assertNotNull(objPerformResult);
		assertThat(objPerformResult.getResult().getCode(), is(CronJobResult.SUCCESS));
	}

}
