package com.simon.core.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.dao.SimonCategoryNavigationDao;
import com.simon.core.services.SimonCategoryNavigationService;


/**
 * Test Implementation class of {@link SimonCategoryNavigationService} used to retrieve L3 sub categories of L2 nodes.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SimonCategoryNavigationServiceTest
{

	@InjectMocks
	private final SimonCategoryNavigationService simonCategoryNavigationService = Mockito
			.spy(new SimonCategoryNavigationServiceImpl());

	@Mock
	private SimonCategoryNavigationDao simonCategoryNavigationDao;

	@Mock
	CategoryModel categoryModel;

	private final String categoryId = "110011";




	/**
	 * Test getSubCategoriesTest is used to test getSubCategories which fetch sub categories of level 2 categories which
	 * contains product.
	 */
	@Test
	public void getSubCategoriesTest()
	{

		final List<CategoryModel> categoryModelList = new ArrayList<>();
		categoryModelList.add(categoryModel);
		Mockito.when(simonCategoryNavigationDao.getSubCategoriesWithProducts(categoryId)).thenReturn(categoryModelList);
		Assert.assertTrue(simonCategoryNavigationService.getSubCategories(categoryId).size() > 0);
	}


}
