package com.simon.core.services.impl;

import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.dao.impl.ErrorLogDaoImpl;
import com.simon.core.dao.impl.ExtProductDaoImpl;
import com.simon.core.services.ErrorLogService;


/**
 * The Class ErrorLogServiceImplTest. Unit test for {@link ErrorLogService}}
 */
@UnitTest
public class ErrorLogServiceImplTest
{

	/** The default store service. */
	@InjectMocks
	private ErrorLogServiceImpl errorLogService;

	@Mock
	private CatalogVersionService catalogVersionService;
	@Mock
	private ExtProductDaoImpl productDao;
	@Mock
	private ModelService modelService;
	@Mock
	private ErrorLogDaoImpl errorLogDao;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test
	public void verifyMakeProductsUnapproved()
	{
		final List<String> productList = new ArrayList<String>();
		productList.add("TestProduct");

		final Date lastEndTime = new Date();

		Mockito.when(errorLogDao.getProductCodes(lastEndTime)).thenReturn(productList);
		errorLogDao.getProductCodes(lastEndTime);
	}
}
