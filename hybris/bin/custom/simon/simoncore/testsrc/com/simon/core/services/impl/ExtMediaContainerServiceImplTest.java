package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dao.ExtMediaContainerDao;


/**
 * Test class for ExtMediaContainerServiceImpl.
 */
@UnitTest
public class ExtMediaContainerServiceImplTest
{
	@InjectMocks
	ExtMediaContainerServiceImpl ExtMediaContainerServiceImpl;

	@Mock
	private transient ExtMediaContainerDao extMediaContainerDao;

	private Date cleanupAgeDate;

	private List<MediaContainerModel> listOfMediaContainer;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		cleanupAgeDate = new Date();
		final MediaContainerModel mediaContainerModel = new MediaContainerModel();
		listOfMediaContainer = new ArrayList<>();
		listOfMediaContainer.add(mediaContainerModel);
		when(extMediaContainerDao.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate)).thenReturn(listOfMediaContainer);
	}

	/**
	 * This method test MediaContainersWithVersionGreaterThenZero will return list of mediaContainer
	 */
	@Test
	public void testMediaContainersWithVersionGreaterThenZero()
	{
		assertEquals(listOfMediaContainer,
				ExtMediaContainerServiceImpl.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate));
	}

}
