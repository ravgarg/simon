/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.simon.core.provider.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.constants.SimonCoreConstants;


@UnitTest
public class ProductImageResolverTest extends AbstractValueResolverTest
{
	private ProductImageResolver valueResolver;

	@Rule
	public ExpectedException expectedException = ExpectedException.none(); //NOPMD
	@Mock
	AbstractValueResolver<GenericVariantProductModel, List<MediaContainerModel>, Object> abstractValueResolver;
	@Mock
	private InputDocument document;
	@Mock
	private IndexerBatchContext batchContext;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private GenericVariantProductModel model;

	@Mock
	private Collection<IndexedProperty> indexedProperties;
	@Mock
	private GenericVariantProductModel product;
	private static final String mediaModelUrl1 = "url1";
	private static final String mediaModelUrl2 = "url2";
	private IndexedProperty indexedProperty1;

	@Mock
	private VariantProductModel variantProductModel;

	@Mock
	private MediaContainerModel mediaContainerModel;

	@Mock
	private MediaModel mediaModel;

	@Mock
	private MediaModel mediaModel2;

	@Mock
	private MediaFormatModel mediaFormatModel;

	@Mock
	private MediaFormatModel mediaFormatModel2;

	@Before
	public void setUp()
	{
		indexedProperty1 = new IndexedProperty();
		indexedProperty1.setName("images");
		indexedProperty1.setValueProviderParameters(new HashMap<String, String>());

		when(Boolean.valueOf(getQualifierProvider().canApply(any(IndexedProperty.class)))).thenReturn(Boolean.FALSE);
		when(mediaModel.getURL()).thenReturn(mediaModelUrl1);
		when(mediaModel2.getURL()).thenReturn(mediaModelUrl2);
		when(variantProductModel.getBaseProduct()).thenReturn(product);


		valueResolver = new ProductImageResolver();
		valueResolver.setSessionService(getSessionService());
		valueResolver.setQualifierProvider(getQualifierProvider());
	}

	@Test
	public void resolveProductWithNoMediaGallery() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final List<MediaContainerModel> galleryList = Collections.emptyList();

		when(product.getGalleryImages()).thenReturn(galleryList);

		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);

		// then
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void resolveProductWithNoMediaInContainer() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final List<MediaContainerModel> galleryList = Collections.singletonList(mediaContainerModel);
		final List<MediaModel> medias = Collections.emptyList();

		when(product.getGalleryImages()).thenReturn(galleryList);
		when(mediaContainerModel.getMedias()).thenReturn(medias);

		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);

		// then
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test(expected = FieldValueProviderException.class)
	public void resolveProductWithNoMediaInContainerExceptionScenario() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final List<MediaContainerModel> galleryList = Collections.singletonList(mediaContainerModel);
		final List<MediaModel> medias = Collections.emptyList();
		final Map<String, String> params = new HashMap<String, String>();
		params.put(SimonCoreConstants.OPTIONAL_PARAM, "false");
		indexedProperty1.setValueProviderParameters(params);

		when(product.getGalleryImages()).thenReturn(galleryList);
		when(mediaContainerModel.getMedias()).thenReturn(medias);

		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);

	}

	@Test
	public void resolveProductWithMedias() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = new ArrayList<>();
		indexedProperties.add(indexedProperty1);
		final List<MediaContainerModel> galleryList = new ArrayList<>();
		galleryList.add(mediaContainerModel);

		final List<MediaModel> medias = new ArrayList<>();
		medias.add(mediaModel);
		medias.add(mediaModel2);


		when(product.getGalleryImages()).thenReturn(galleryList);
		when(mediaContainerModel.getMedias()).thenReturn(medias);
		when(mediaFormatModel.getQualifier()).thenReturn("580Wx884H");
		when(mediaFormatModel2.getQualifier()).thenReturn("345Wx495H");
		when(mediaModel.getMediaFormat()).thenReturn(mediaFormatModel);
		when(mediaModel2.getMediaFormat()).thenReturn(mediaFormatModel2);

		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);
		// then
		final List<String> image = new ArrayList<String>();
		image.add("580Wx884H_default|url1");
		image.add("345Wx495H_default|url2");
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty1, image, null);
	}
}
