package com.simon.core.strategies.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@UnitTest
public class ExtCheckoutCustomerStrategyTest
{
	@InjectMocks
	private ExtCheckoutCustomerStrategy extCheckoutCustomerStrategy;
	@Mock
	private UserService userService;
	@Mock
	private CustomerModel checkoutCustomer;
	@Mock
	private CartService cartService;

	@Before
	public void prepare()
	{
		initMocks(this);
	}

	@Test
	public void getCurrentUserForCheckoutAnnonymousTest()
	{
		final CartModel cartModel = mock(CartModel.class);
		final UserModel userModel = mock(UserModel.class);
		final String custID = "12345";
		doReturn(userModel).when(userService).getCurrentUser();
		doReturn(true).when(userService).isAnonymousUser(userModel);
		doReturn(cartModel).when(cartService).getSessionCart();
		doReturn(userModel).when(cartModel).getUser();
		doReturn(custID).when(userModel).getUid();
		doReturn(checkoutCustomer).when(userService).getUserForUID(custID);
		Assert.assertEquals(checkoutCustomer, extCheckoutCustomerStrategy.getCurrentUserForCheckout());
	}

	@Test
	public void getCurrentUserForCheckoutRegisterTest()
	{
		final UserModel userModel = mock(UserModel.class);
		doReturn(userModel).when(userService).getCurrentUser();
		doReturn(false).when(userService).isAnonymousUser(userModel);
		doReturn(checkoutCustomer).when(userService).getCurrentUser();
		Assert.assertEquals(checkoutCustomer, extCheckoutCustomerStrategy.getCurrentUserForCheckout());
	}

}
