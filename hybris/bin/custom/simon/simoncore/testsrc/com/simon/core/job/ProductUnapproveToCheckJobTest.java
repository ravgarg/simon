package com.simon.core.job;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableSet;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.services.ExtProductService;

@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductUnapproveToCheckJobTest
{
	@InjectMocks
	private ProductUnapproveToCheckJob productUnapproveToCheckJob;

	@Mock
	private CronJobModel cronjob;

	@Mock
	private ModelService modelService;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	CatalogVersionModel catalogVersion;

	@Mock
	GenericVariantProductModel variant;

	@Mock
	ProductModel baseProduct;


	@Test
	public void testUnApprovedVariantAndBaseProductStatusChange()
	{
		when(variant.getBaseProduct()).thenReturn(baseProduct);
		when(variant.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(baseProduct.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);
		when(extProductService.getUnApprovedProductsHavingImageAndMerchandizingCategory(Matchers.anyObject()))
				.thenReturn(Arrays.asList(variant));
		productUnapproveToCheckJob.perform(cronjob);
		verify(variant, Mockito.times(1)).setApprovalStatus(ArticleApprovalStatus.CHECK);
		verify(baseProduct, Mockito.times(1)).setApprovalStatus(ArticleApprovalStatus.CHECK);
		verify(modelService, Mockito.times(1)).saveAll(Arrays.asList(variant));
		verify(modelService, Mockito.times(1)).saveAll(ImmutableSet.of(baseProduct));


	}

	@Test
	public void testBaseProductsStatusNotChangeWhenApproved()
	{
		when(variant.getBaseProduct()).thenReturn(baseProduct);
		when(variant.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(baseProduct.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);
		when(extProductService.getUnApprovedProductsHavingImageAndMerchandizingCategory(Matchers.anyObject()))
				.thenReturn(Arrays.asList(variant));
		productUnapproveToCheckJob.perform(cronjob);
		verify(modelService, Mockito.times(1)).saveAll(Arrays.asList(variant));
		verify(baseProduct, Mockito.times(0)).setApprovalStatus(ArticleApprovalStatus.CHECK);

	}
}
