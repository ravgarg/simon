package com.simon.core.price.jalo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.product.daos.ProductDao;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.i18n.daos.CurrencyDao;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.PriceValue;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import com.simon.core.enums.PriceType;


/**
 *
 */
@IntegrationTest
public class ExtEurope1PriceFactoryTest extends ServicelayerTransactionalTest
{


	ExtEurope1PriceFactory priceFactory;
	@Resource
	private ModelService modelService;
	@Resource
	private ProductDao productDao;
	@Resource
	private UserService userService;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	UnitService unitService;
	@Resource
	CurrencyDao currencyDao;
	private CurrencyModel currencyModel;
	private EnumerationValue PPG, UPG;
	private UnitModel unit;
	private CatalogVersionModel catalogVersion;
	private Currency currency;

	@Before
	public void setup() throws Exception
	{

		final ApplicationContext appCtx = Registry.getApplicationContext();
		priceFactory = (ExtEurope1PriceFactory) appCtx.getBean("simon.manager");
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testCategories.impex", "utf-8");
		importCsv("/simoncore/test/testProducts.impex", "utf-8");
		importCsv("/simoncore/test/testPrices.impex", "utf-8");
		unit = unitService.getUnitForCode("pieces");
		currencyModel = currencyDao.findCurrenciesByCode("USD").get(0);
		currency = modelService.getSource(currencyModel);
	}

	@Test
	public void testCreatePriceInformationWhenRowIsSale() throws JaloPriceFactoryException, ConsistencyCheckException
	{
		final PriceRowModel price = modelService.create(PriceRowModel.class);
		price.setProductId("XYZ");
		price.setUnit(unit);
		price.setCurrency(currencyModel);
		price.setPrice(15.0);
		price.setPriceType(PriceType.SALE);
		final Date startDate = DateUtils.addDays(new Date(), -5);
		final Date endDate = DateUtils.addDays(new Date(), 5);
		price.setStartTime(startDate);
		price.setEndTime(endDate);
		final PriceRowModel lp = modelService.create(PriceRowModel.class);
		lp.setProductId("XYZ");
		lp.setUnit(unit);
		lp.setCurrency(currencyModel);
		lp.setPrice(20.0);
		lp.setPriceType(PriceType.LIST);
		price.setListPrice(lp);
		modelService.saveAll();
		final PriceRow row = modelService.getSource(price);
		final Currency currency = modelService.getSource(currencyModel);
		final PriceInformation priceInfo = priceFactory.createPriceInformation(row, currency);
		final ExtPriceValue priceValue = (ExtPriceValue) priceInfo.getPriceValue();
		assertNotNull(priceValue.getListPrice());
		assertEquals(priceValue.getListPrice(), 20.0, 0.0);
		assertEquals(priceValue.getSalePriceStartDate(), startDate);
		assertEquals(priceValue.getSalePriceEndDate(), endDate);
	}

	@Test
	public void testCreatePriceInformationWhenRowIsList() throws JaloPriceFactoryException, ConsistencyCheckException
	{
		final PriceRowModel price = modelService.create(PriceRowModel.class);
		price.setProductId("XYZ");
		price.setUnit(unit);
		price.setCurrency(currencyModel);
		price.setPrice(15.0);
		price.setPriceType(PriceType.LIST);
		final Date startDate = DateUtils.addDays(new Date(), -5);
		final Date endDate = DateUtils.addDays(new Date(), 5);
		price.setStartTime(startDate);
		price.setEndTime(endDate);
		final PriceRowModel lp = modelService.create(PriceRowModel.class);
		lp.setProductId("XYZ");
		lp.setUnit(unit);
		lp.setCurrency(currencyModel);
		lp.setPrice(20.0);
		lp.setPriceType(PriceType.SALE);
		price.setListPrice(lp);
		modelService.saveAll();
		final PriceRow row = modelService.getSource(price);
		final Currency currency = modelService.getSource(currencyModel);
		final PriceInformation priceInfo = priceFactory.createPriceInformation(row, currency);
		final ExtPriceValue priceValue = (ExtPriceValue) priceInfo.getPriceValue();
		assertNotNull(priceValue.getListPrice());
		assertEquals(priceValue.getListPrice(), 0.0, 0.0);
		assertEquals(priceValue.getSalePriceStartDate(), startDate);
		assertEquals(priceValue.getSalePriceEndDate(), endDate);
	}

	@Test
	public void testCreatePriceInformationWhenListPriceIsNull() throws JaloPriceFactoryException, ConsistencyCheckException
	{
		final PriceRowModel price = modelService.create(PriceRowModel.class);
		price.setProductId("XYZ");
		price.setUnit(unit);
		price.setCurrency(currencyModel);
		price.setPrice(15.0);
		price.setPriceType(PriceType.LIST);
		final Date startDate = DateUtils.addDays(new Date(), -5);
		final Date endDate = DateUtils.addDays(new Date(), 5);
		price.setStartTime(startDate);
		price.setEndTime(endDate);
		modelService.saveAll();
		final PriceRow row = modelService.getSource(price);
		final PriceInformation priceInfo = priceFactory.createPriceInformation(row, currency);
		final ExtPriceValue priceValue = (ExtPriceValue) priceInfo.getPriceValue();
		assertNotNull(priceValue.getListPrice());
		assertEquals(priceValue.getListPrice(), 0.0, 0.0);
		assertEquals(priceValue.getSalePriceStartDate(), startDate);
		assertEquals(priceValue.getSalePriceEndDate(), endDate);
	}

	@Test
	public void testGetPriceInformations() throws JaloPriceFactoryException, ConsistencyCheckException, ImpExException
	{
		catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
		final ProductModel productModel = productDao.findProductsByCode(catalogVersion, "testSkuMVId1111").get(0);
		final Product product = modelService.getSource(productModel);
		final SessionContext ctx = jaloSession.getSessionContext();
		ctx.setCurrency(currency);
		final List<PriceInformation> priceInfo = priceFactory.getProductPriceInformations(ctx, product, new Date(), false);
		final ExtPriceValue priceValue = (ExtPriceValue) priceInfo.get(0).getPriceValue();
		assertNotNull(priceValue.getListPrice());

	}

	@Test
	public void testGetBasePrice() throws JaloPriceFactoryException, ConsistencyCheckException, ImpExException
	{
		catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
		final ProductModel productModel = productDao.findProductsByCode(catalogVersion, "testSkuMVId1111").get(0);
		final OrderModel orderModel = modelService.create(OrderModel.class);
		orderModel.setCode("0002");
		orderModel.setCurrency(currencyModel);
		orderModel.setTotalPrice(Double.valueOf(1.0));
		orderModel.setSubtotal(Double.valueOf(2.0));
		orderModel.setDeliveryCost(Double.valueOf(3.0));
		orderModel.setDate(new Date());
		orderModel.setNet(Boolean.FALSE);
		orderModel.setUser(userService.getAnonymousUser());
		final OrderEntryModel entry = modelService.create(OrderEntryModel.class);
		entry.setProduct(productModel);
		entry.setUnit(unit);
		entry.setQuantity(1L);
		entry.setOrder(orderModel);
		modelService.saveAll();
		final ExtPriceValue priceValue = (ExtPriceValue) priceFactory.getBasePrice(modelService.getSource(entry));
		assertNotNull(priceValue.getListPrice());
	}


	@Test
	public void testGetBasePriceWhenThereIsNoPrice() throws JaloPriceFactoryException, ConsistencyCheckException, ImpExException
	{
		catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
		final ProductModel productModel = productDao.findProductsByCode(catalogVersion, "testSkuMVId1112").get(0);
		final OrderModel orderModel = modelService.create(OrderModel.class);
		orderModel.setCode("0001");
		orderModel.setCurrency(currencyModel);
		orderModel.setTotalPrice(Double.valueOf(1.0));
		orderModel.setSubtotal(Double.valueOf(2.0));
		orderModel.setDeliveryCost(Double.valueOf(3.0));
		orderModel.setDate(new Date());
		orderModel.setNet(Boolean.FALSE);
		orderModel.setUser(userService.getAnonymousUser());
		final OrderEntryModel entry = modelService.create(OrderEntryModel.class);
		entry.setProduct(productModel);
		entry.setUnit(unit);
		entry.setQuantity(1L);
		entry.setOrder(orderModel);
		modelService.saveAll();
		final AbstractOrderEntry entrySource = modelService.getSource(entry);
		entrySource.setRejected(true);
		entrySource.setGiveAway(true);
		final PriceValue priceValue = priceFactory.getBasePrice(entrySource);
		assertEquals(0.0d, priceValue.getValue(), 0.0d);
	}


}
