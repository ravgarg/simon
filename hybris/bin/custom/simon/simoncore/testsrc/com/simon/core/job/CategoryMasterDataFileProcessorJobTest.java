package com.simon.core.job;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test class for CategoryMasterDataFileProcessorCronJob
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryMasterDataFileProcessorJobTest
{

	private static final String INPUT_PATH = "category.master.feed.hotfolder.directory";
	private static final String PROCESSED_PATH = "category.master.feed.processed.directory";
	private static final String INPUT_FILE_NAME = "category.master.feed.input.filename";
	private static final String OUTPUT_PATH = "category.master.feed.hotfolder.inbound";
	private static final String MIRAKL_FILE_PREFIX = "category.master.feed.mirakl.category.file";
	private static final String MERCH_FILE_PREFIX = "category.master.feed.merchandizing.category.file";


	@InjectMocks
	@Spy
	private CategoryMasterDataFileProcessorJob categoryMasterDataFileProcessorJob;
	@Mock
	private CronJobModel cronjob;
	@Mock
	private File file;

	@Mock
	private Reader reader;

	@Mock
	private ConfigurationService configurationService;

	/**
	 * Test perform Method With input file
	 *
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	@Test
	public void testPerformWithInputFile() throws IOException, URISyntaxException
	{
		final Configuration configuration = Mockito.mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(INPUT_PATH)).thenReturn("inputPath");
		when(configuration.getString(INPUT_FILE_NAME)).thenReturn("inputFileName");
		when(configuration.getString(OUTPUT_PATH)).thenReturn("outputPath");
		when(configuration.getString(PROCESSED_PATH)).thenReturn("processedPath");
		when(configuration.getString(MIRAKL_FILE_PREFIX)).thenReturn("test_mirakl_file");
		when(configuration.getString(MERCH_FILE_PREFIX)).thenReturn("test_merch_file");
		final InputStream inputStream = CategoryMasterDataFileProcessorJobTest.class
				.getResourceAsStream("/simoncore/test/testMasterCatagory.csv");

		reader = new InputStreamReader(inputStream);
		when(file.getName()).thenReturn("file_101011");
		doReturn(reader).when(categoryMasterDataFileProcessorJob).getFileReader(Mockito.anyString());
		final PerformResult result = categoryMasterDataFileProcessorJob.perform(cronjob);
		assertEquals(CronJobResult.SUCCESS, result.getResult());
		assertEquals(CronJobStatus.FINISHED, result.getStatus());
	}

	/**
	 * Test perform Method WithOut input file
	 *
	 * @throws IOException
	 */
	@Test
	public void testPerformWithOutInputFile() throws IOException
	{
		final Configuration configuration = Mockito.mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(INPUT_PATH)).thenReturn("inputPath");
		when(configuration.getString(INPUT_FILE_NAME)).thenReturn("inputFileName");
		when(configuration.getString(OUTPUT_PATH)).thenReturn("outputPath");
		when(configuration.getString(PROCESSED_PATH)).thenReturn("processedPath");
		when(configuration.getString(MIRAKL_FILE_PREFIX)).thenReturn("test_mirakl_file");
		when(configuration.getString(MERCH_FILE_PREFIX)).thenReturn("test_merch_file");
		final PerformResult result = categoryMasterDataFileProcessorJob.perform(cronjob);

		assertEquals(CronJobResult.ERROR, result.getResult());
		assertEquals(CronJobStatus.ABORTED, result.getStatus());
	}
}
