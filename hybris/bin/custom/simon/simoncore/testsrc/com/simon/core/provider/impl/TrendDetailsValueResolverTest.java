package com.simon.core.provider.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.model.TrendModel;


/**
 * The Class TrendDetailsValueResolverTest.
 */
public class TrendDetailsValueResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	TrendDetailsValueResolver trendDetailsValueResolver;

	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	public static final String OPTIONAL_PARAM = "optional";
	public static final String ATTRIBUTE_PARAM = "attribute";

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final String attributeName = "attributeName";
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, attributeName);
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		final TrendModel trend = new TrendModel();
		trend.setCode("12345");
		final List<TrendModel> trendList = new ArrayList<>();
		trendList.add(trend);
		baseProduct.setTrends(trendList);
		model.setBaseProduct(baseProduct);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(trend.getClass())).thenReturn(composedValue);
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(trendList, attributeName)).thenReturn(value);
		trendDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}




	@Test(expected = FieldValueProviderException.class)
	public void testResolveNullTrendModelAndIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		trendDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
	}


	@Test
	public void testResolveNullTrendModel() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		trendDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

}
