package com.simon.core.mirakl.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.europe1.model.PriceRowModel;

import java.math.BigDecimal;
import java.sql.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.common.MiraklDiscount;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.simon.core.enums.PriceType;


/**
 * SellingPriceRowPopulatorTest, unit test for {@link SellingPriceRowPopulator}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SellingPriceRowPopulatorTest
{

	/** The selling price row populator. */
	@InjectMocks
	private SellingPriceRowPopulator sellingPriceRowPopulator;

	private static final String OFFER_ID = "12345";

	private static final String STATE_CODE = "state";

	private static final String PRODUCT_SKU = "sku";

	@Mock
	private MiraklExportOffer exportOffer;

	/**
	 * Verify selling price populated correctly.
	 */
	@Test
	public void verifySellingPricePopulatedCorrectly()
	{
		final Date startDate = mock(Date.class);
		final Date endDate = mock(Date.class);

		final MiraklDiscount miraklDiscount = mock(MiraklDiscount.class);
		when(miraklDiscount.getDiscountPrice()).thenReturn(BigDecimal.valueOf(20.0));
		when(miraklDiscount.getStartDate()).thenReturn(startDate);
		when(miraklDiscount.getEndDate()).thenReturn(endDate);

		final MiraklExportOffer exportOffer = mock(MiraklExportOffer.class);
		when(exportOffer.getDiscount()).thenReturn(miraklDiscount);
		when(exportOffer.isActive()).thenReturn(true);
		when(exportOffer.getId()).thenReturn(OFFER_ID);
		when(exportOffer.getStateCode()).thenReturn(STATE_CODE);
		when(exportOffer.getProductSku()).thenReturn(PRODUCT_SKU);
		final PriceRowModel target = mock(PriceRowModel.class);
		sellingPriceRowPopulator.populate(exportOffer, target);

		verify(target).setPrice(20.0);
		verify(target).setStartTime(startDate);
		verify(target).setEndTime(endDate);
		verify(target).setPriceType(PriceType.SALE);
		verify(target).setOfferId((OFFER_ID));
		verify(target).setOfferSku((PRODUCT_SKU));
		verify(target).setOfferStateCode((STATE_CODE));
	}
}
