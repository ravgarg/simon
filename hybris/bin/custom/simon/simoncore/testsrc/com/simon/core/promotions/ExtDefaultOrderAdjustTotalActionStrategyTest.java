package com.simon.core.promotions;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderAdjustTotalActionModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.ShipmentRAO;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.promotions.service.impl.ExtDefaultPromotionActionServiceImpl;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class ExtDefaultOrderAdjustTotalActionStrategyTest
{
	@InjectMocks
	private ExtDefaultOrderAdjustTotalActionStrategy actionStrategy;
	@Mock
	private ExtDefaultPromotionActionServiceImpl promotionActionService;
	@Mock
	private DiscountRAO action;
	@Mock
	CartModel cart;
	@Mock
	private PromotionResultModel promotionResult;
	@Mock
	ModelService modelService;
	@Mock
	private RuleBasedOrderAdjustTotalActionModel actionModel;
	@Mock
	private RetailerRAO retailerRao;

	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test
	public void applyTest_whenActionRaoIsNotDiscount()
	{
		final ShipmentRAO action = new ShipmentRAO();
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTest_whenPRIsNull()
	{
		when(promotionActionService.createPromotionResult(action)).thenReturn(null);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTest_whenOrderOfPRIsNull()
	{
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionResult.getOrder()).thenReturn(null);
		when(modelService.isNew(promotionResult)).thenReturn(true);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTest_whenPRIsnotNew()
	{
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionResult.getOrder()).thenReturn(null);
		when(modelService.isNew(promotionResult)).thenReturn(false);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTest()
	{
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionResult.getOrder()).thenReturn(cart);
		when(action.getRetailer()).thenReturn(retailerRao);
		when(retailerRao.getRetailerId()).thenReturn("retailerId");

		when(modelService.create(RuleBasedOrderAdjustTotalActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList();
		metaDataList.add("");
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		actionStrategy.setPromotionAction(RuleBasedOrderAdjustTotalActionModel.class);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 1);
	}

}
