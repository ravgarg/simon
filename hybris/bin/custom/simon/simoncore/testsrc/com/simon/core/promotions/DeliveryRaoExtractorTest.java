package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DeliveryModeRAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class DeliveryRaoExtractorTest
{
	@InjectMocks
	DeliveryRaoExtractor deliveryRaoExtractor;
	@Mock
	CartRAO cart;

	@Before
	public void setup()
	{
		initMocks(this);

	}



	@Test
	public void expandFact()
	{
		final DeliveryModeRAO deliveryMode1 = new DeliveryModeRAO();
		final DeliveryModeRAO deliveryMode2 = new DeliveryModeRAO();

		deliveryMode1.setCode("cheapest");
		deliveryMode2.setCode("fastest");

		final RetailerRAO retailer1 = new RetailerRAO();
		final RetailerRAO retailer2 = new RetailerRAO();

		retailer1.setDeliveryMode(deliveryMode1);
		retailer2.setDeliveryMode(deliveryMode2);

		final List<RetailerRAO> retailers = new ArrayList();
		retailers.add(retailer1);
		retailers.add(retailer2);
		when(cart.getRetailers()).thenReturn(retailers);

		final Set<?> facts = deliveryRaoExtractor.expandFact(cart);
		assertEquals(2, facts.size());
	}

	@Test
	public void getTriggeringOption()
	{
		assertEquals("EXPAND_DELIVERY_RAO", deliveryRaoExtractor.getTriggeringOption());
	}

	@Test
	public void isDefault()
	{
		assertTrue(deliveryRaoExtractor.isDefault());
	}

	@Test
	public void isMinOption()
	{
		assertFalse(deliveryRaoExtractor.isMinOption());
	}

}
