package com.simon.core.resolver;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.model.MerchandizingCategoryModel;


@UnitTest
public class ExtProductModelUrlResolverTest
{
	@InjectMocks
	@Spy
	private ExtProductModelUrlResolver extProductModelUrlResolver;
	@Mock
	ProductModel source;
	@Mock
	private BaseSiteService baseSiteService;
	@Mock
	private BaseSiteModel currentBaseSite;
	@Mock
	private SessionService sessionService;
	@Mock
	private MerchandizingCategoryModel lastCategoryModel;
	@Mock
	private MerchandizingCategoryModel level2CategoryModel;
	@Mock
	private MerchandizingCategoryModel level1CategoryModel;
	@Mock
	private MerchandizingCategoryModel level0CategoryModel;
	@Mock
	private CommerceCategoryService commerceCategoryService;
	final List<MerchandizingCategoryModel> path = new ArrayList<>();

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		extProductModelUrlResolver.homeCategoryCode = "Gender";
		path.add(level0CategoryModel);
		path.add(level1CategoryModel);
		path.add(level2CategoryModel);
		path.add(lastCategoryModel);
		when(getCommerceCategoryService().getCategoryForCode("Gender")).thenReturn(level0CategoryModel);
		when(currentBaseSite.getUid()).thenReturn("simon");
		when(source.getCode()).thenReturn("product");
		when(lastCategoryModel.getCode()).thenReturn("Pants");
		when(lastCategoryModel.getName()).thenReturn("Pants");
		when(level2CategoryModel.getCode()).thenReturn("Clothing");
		when(level2CategoryModel.getName()).thenReturn("Clothing");
		when(level1CategoryModel.getCode()).thenReturn("Men");
		when(level1CategoryModel.getName()).thenReturn("Men");
		when(level0CategoryModel.getCode()).thenReturn("Gender");
		when(level0CategoryModel.getName()).thenReturn("Gender");
		Mockito.doReturn(path).when(extProductModelUrlResolver).getCategoryPathFromProduct(source);
		extProductModelUrlResolver.setDefaultPattern("/{category-path}/{product-name}/catcodes/{category-codes}/p/{product-code}");
		when(baseSiteService.getCurrentBaseSite()).thenReturn(currentBaseSite);
	}



	@Test
	public void testWhenCategoryHierarchyIsNotSetInSession()
	{
		assertEquals(extProductModelUrlResolver.resolveInternal(source),
				"/men/clothing/pants//catcodes/Men_Clothing_Pants/p/product");
	}

	@Test
	public void testWhenCategoryHierarchyIsSetInSession()
	{
		when(sessionService.getAttribute(SimonCoreConstants.CATEGORY_CODE_PATH)).thenReturn("Gender_Men_Clothing_Pants");
		assertEquals(extProductModelUrlResolver.resolveInternal(source),
				"/men/clothing/pants//catcodes/Gender_Men_Clothing_Pants/p/product");
	}

	@Test
	public void testWhenPatternDoNotContainElementToResolve()
	{
		extProductModelUrlResolver.setDefaultPattern("/{catcode}");
		assertEquals(extProductModelUrlResolver.resolveInternal(source), "/{catcode}");
	}

	protected CommerceCategoryService getCommerceCategoryService()
	{
		return commerceCategoryService;
	}
}
