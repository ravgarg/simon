package com.simon.core.services.impl;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;



@UnitTest
public class ExtSolrProductSearchServiceTest
{
	@Spy
	@InjectMocks
	private ExtSolrProductSearchService extSolrProductSearchService;

	private String categoryCode;

	private SearchQueryContext searchQueryContext;
	@Mock
	private PageableData pageableData;

	private SolrSearchQueryData searchQueryData;
	@Mock
	private ProductCategorySearchPageData productCategorySearchPageData;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		searchQueryData = new SolrSearchQueryData();
	}

	@Test
	public void testProductWithVariant()
	{
		doReturn(searchQueryData).when(extSolrProductSearchService).callSuperCreateSearchQueryData();
		doReturn(productCategorySearchPageData).when(extSolrProductSearchService).callSuperDoSearch(searchQueryData, pageableData);
		extSolrProductSearchService.categorySearch(categoryCode, searchQueryContext.CATEGORYLINKS, pageableData);
		Assert.assertEquals(
				extSolrProductSearchService.categorySearch(categoryCode, searchQueryContext.CATEGORYLINKS, pageableData),
				productCategorySearchPageData);
	}

	@Test
	public void testcreateSearchQueryData()
	{
		extSolrProductSearchService.callSuperCreateSearchQueryData();
	}

	@Test
	public void testSearchAgainForCategoryContext()
	{
		searchQueryData.setFreeTextSearch("");
		searchQueryData.setCategoryCode("m001");
		doReturn(productCategorySearchPageData).when(extSolrProductSearchService).callSuperDoSearch(searchQueryData, pageableData);
		extSolrProductSearchService.searchAgain(searchQueryData, pageableData);
		Assert.assertEquals(extSolrProductSearchService.searchAgain(searchQueryData, pageableData), productCategorySearchPageData);
	}

	@Test
	public void testSearchAgainForSearchContext()
	{
		searchQueryData.setFreeTextSearch("jeans");
		searchQueryData.setCategoryCode("");
		searchQueryData.setSearchQueryContext(SearchQueryContext.SEARCH);
		doReturn(productCategorySearchPageData).when(extSolrProductSearchService).callSuperDoSearch(searchQueryData, pageableData);
		extSolrProductSearchService.searchAgain(searchQueryData, pageableData);
		Assert.assertEquals(extSolrProductSearchService.searchAgain(searchQueryData, pageableData), productCategorySearchPageData);
	}

	@Test
	public void testSearchAgainWhenFreeTextEmptyForSearchContext()
	{
		searchQueryData.setFreeTextSearch("");
		searchQueryData.setCategoryCode("");
		searchQueryData.setSearchQueryContext(SearchQueryContext.CATEGORYLINKS);
		doReturn(productCategorySearchPageData).when(extSolrProductSearchService).callSuperDoSearch(searchQueryData, pageableData);
		extSolrProductSearchService.searchAgain(searchQueryData, pageableData);
		Assert.assertEquals(extSolrProductSearchService.searchAgain(searchQueryData, pageableData), productCategorySearchPageData);
	}

	@Test
	public void testSearchAgainWhenDesignerForSearchContext()
	{
		searchQueryData.setFreeTextSearch("");
		searchQueryData.setCategoryCode("");
		searchQueryData.setSearchQueryContext(SearchQueryContext.DESIGNERS);
		doReturn(productCategorySearchPageData).when(extSolrProductSearchService).callSuperDoSearch(searchQueryData, pageableData);
		extSolrProductSearchService.searchAgain(searchQueryData, pageableData);
		Assert.assertEquals(extSolrProductSearchService.searchAgain(searchQueryData, pageableData), productCategorySearchPageData);
	}
}
