package com.simon.core.dao.impl;

import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.simon.core.model.DealModel;


/**
 * The Class ExtDealDaoTest. Integration test for {@link ExtDealDaoImpl}
 */
@IntegrationTest
public class ExtDealDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource
	private ExtDealDaoImpl extDealDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testDeals.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify null Deal is returned for invalid code.
	 */
	@Test
	public void verifyNullDealIsReturnedForInvalidCode()
	{
		final DealModel actual = extDealDao.findDealByCode("InvalidDealCode");
		assertNull(actual);
	}


	/**
	 * Method to test findAllDeals() in empty list case.
	 */
	@Test
	public void testFindAllDeals()
	{
		final List<DealModel> results = extDealDao.findAllDeals();
		Assert.assertTrue(results.isEmpty());
	}

	/**
	 * Method to test findAllDeals() in valid list case.
	 */
	@Test
	public void testFindDealByCode()
	{
		final DealModel result = extDealDao.findDealByCode("testDeal1");
		Assert.assertEquals(result.getName(), "Test Deal 1");
	}

	/**
	 * Method to test findListOfDealForCodes() in empty list case.
	 */
	@Test
	public void testFindListOfDealForCodes()
	{
		final List<String> codes = new ArrayList<>();
		codes.add("testDeal1");
		codes.add("testDeal2");
		final List<DealModel> result = extDealDao.findListOfDealForCodes(codes);
		Assert.assertTrue(result.isEmpty());
	}



}
