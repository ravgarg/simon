package com.simon.core.job;

import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * ExtMediaConversionJobTest unit test for {@link ExtMediaConversionJob}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMediaConversionJobTest
{
	@InjectMocks
	private ExtMediaConversionJob extMediaConversionJob;

	/**
	 * Verify is abortable returns true.
	 */
	@Test
	public void verifyIsAbortableReturnsTrue()
	{
		assertTrue(extMediaConversionJob.isAbortable());
	}
}
