package com.simon.core.keygenerators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * VariantCategoryKeyGenerator unit test for {@link VariantCategoryKeyGenerator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantCategoryKeyGeneratorTest
{

	@InjectMocks
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	/**
	 * Verify exception thrown for generate without argument.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForGenerateWithoutArgument()
	{
		variantCategoryKeyGenerator.generate();
	}

	/**
	 * Verify exception thrown for reset.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForReset()
	{
		variantCategoryKeyGenerator.reset();
	}

	/**
	 * Verify code generated correctly.
	 */
	@Test
	public void verifyCodeGeneratedCorrectly()
	{
		assertThat(variantCategoryKeyGenerator.generateFor("pant length"), is("pant-length"));
	}
}
