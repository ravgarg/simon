package com.simon.core.caches;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;
import com.simon.core.services.VariantCategoryService;
import com.simon.core.services.VariantValueCategoryService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantValueCategoryPreProcessorTest
{
	@InjectMocks
	private VariantValueCategoryPreProcessor variantValueCategoryPreProcessor;

	@Mock
	private VariantCategoryService variantCategoryService;

	@Mock
	private VariantValueCategoryService variantValueCategoryService;

	@Mock
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Mock
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private UserService userService;

	@Mock
	private MediaService mediaService;

	@Mock
	private ModelService modelService;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private VariantValueCategoryModel variantValueCategory;

	@Mock
	private VariantValueCategoryModel topSequenceVariantValueCategory;

	@Mock
	private VariantCategoryModel variantCategory;

	@Mock
	private MediaModel thumbnail;

	@Mock
	private UserGroupModel userGroup;

	@Mock
	private ProductImportFileContextData context;

	@Test
	public void testCreateKey()
	{
		final String attribute = "variantAttributes";
		final String rawValue = "COLOR=Blue White";
		final Map<String, String> productValues = ImmutableMap.of("colorSwatchUrl", "https://dummyimage.com/600x400/f211f2/fff");
		when(variantCategoryKeyGenerator.generateFor("COLOR")).thenReturn("color");
		when(variantValueCategoryKeyGenerator.generateFor("Blue White")).thenReturn("VVC-blue-white");
		final String key = variantValueCategoryPreProcessor.createKey(attribute, rawValue, productValues, context);
		assertThat(key, is("VVC-blue-white-color"));
	}

	@Test
	public void verifyThumbnailNotUpdatedForExistingVVCWithThumbnailAndSameRealFileName()
	{
		final String attribute = "variantAttributes";
		final String rawValue = "COLOR=Blue White";
		final Map<String, String> productValues = ImmutableMap.of("colorSwatchUrl", "https://dummyimage.com/600x400/f211f2/fff");
		when(variantCategoryKeyGenerator.generateFor("COLOR")).thenReturn("color");
		when(variantValueCategoryKeyGenerator.generateFor("Blue White")).thenReturn("VVC-blue-white");
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);
		when(variantCategoryService.getCategoryForCode(catalogVersion, "VVC-blue-white-color")).thenReturn(variantValueCategory);
		when(variantValueCategory.getSupercategories()).thenReturn(Arrays.asList(variantCategory));
		when(variantValueCategory.getCode()).thenReturn("VVC-blue-white-color");
		when(variantValueCategory.getThumbnail()).thenReturn(thumbnail);
		when(thumbnail.getRealFileName()).thenReturn("https://dummyimage.com/600x400/f211f2/fff");
		when(variantValueCategory.getCatalogVersion()).thenReturn(catalogVersion);
		when(variantCategory.getCode()).thenReturn("color");
		variantValueCategoryPreProcessor.createValue(attribute, rawValue, productValues, context);
		Mockito.verify(modelService, Mockito.times(0)).save(thumbnail);
	}

	@Test
	public void testCorrectVVCReturnedWhenAlreadyExisted()
	{
		final String attribute = "variantAttributes";
		final String rawValue = "COLOR=Blue White";
		final Map<String, String> productValues = new HashMap<>();
		when(variantCategoryKeyGenerator.generateFor("COLOR")).thenReturn("color");
		when(variantValueCategoryKeyGenerator.generateFor("Blue White")).thenReturn("VVC-blue-white");
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);
		when(variantCategoryService.getCategoryForCode(catalogVersion, "VVC-blue-white-color")).thenReturn(variantValueCategory);
		final VariantValueCategoryModel returnedVariantValueCategory = variantValueCategoryPreProcessor.createValue(attribute,
				rawValue, productValues, context);
		assertThat(returnedVariantValueCategory, is(variantValueCategory));
	}

	@Test
	public void testNewVVCCreatedWhenVVCNotExisted()
	{
		final String attribute = "variantAttributes";
		final String rawValue = "COLOR=Blue White";
		final Map<String, String> productValues = new HashMap<>();
		when(variantCategoryKeyGenerator.generateFor("COLOR")).thenReturn("color");
		when(variantValueCategoryKeyGenerator.generateFor("Blue White")).thenReturn("VVC-blue-white");
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);
		when(modelService.create(VariantValueCategoryModel.class)).thenReturn(variantValueCategory);
		when(userService.getUserGroupForUID("customergroup")).thenReturn(userGroup);
		when(variantCategoryService.getCategoryForCode(catalogVersion, "color")).thenReturn(variantCategory);
		when(variantValueCategoryService.getTopSequenceVariantValueCategory(variantCategory))
				.thenReturn(topSequenceVariantValueCategory);
		when(topSequenceVariantValueCategory.getSequence()).thenReturn(100);
		final VariantValueCategoryModel returnedVariantValueCategory = variantValueCategoryPreProcessor.createValue(attribute,
				rawValue, productValues, context);
		Mockito.verify(modelService, Mockito.times(1)).save(variantValueCategory);
		assertThat(returnedVariantValueCategory, is(variantValueCategory));
	}




}
