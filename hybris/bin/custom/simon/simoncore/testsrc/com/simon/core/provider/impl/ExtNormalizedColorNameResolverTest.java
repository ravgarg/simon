package com.simon.core.provider.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.HybrisEnumValue;
import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.enums.SwatchColorEnum;


/**
 * Test class for {@link ExtNormalizedColorNameResolver}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtNormalizedColorNameResolverTest extends AbstractValueResolverTest
{
	private static final String ATTRIBUTE_NAME = "attributeName";
	public static final String ATTRIBUTE_PARAM = "attribute";
	protected static final String RED = "RED";

	@InjectMocks
	ExtNormalizedColorNameResolver normalizedColorNameResolver;

	@Mock
	private ModelService modelService;

	@Mock
	private TypeService typeService;

	@Mock
	private GenericVariantProductModel product;

	@Mock
	private HybrisEnumValue hybrisEnum;

	private final SwatchColorEnum colorEnum = SwatchColorEnum.RED;

	/**
	 *
	 * @throws Exception
	 */
	@Test
	public void shouldWorkOnlyForHybrisEnumValues() throws Exception
	{
		final boolean resultForHybrisEnum = this.normalizedColorNameResolver.isHybrisEnum(this.hybrisEnum);
		final boolean resultForProductModel = this.normalizedColorNameResolver.isHybrisEnum(this.product);
		assertThat(resultForHybrisEnum).isTrue();
		assertThat(resultForProductModel).isFalse();
	}

	/**
	 *
	 */
	@Test
	public void shouldReturnCodeValueForEnum()
	{
		when(this.hybrisEnum.getCode()).thenReturn(RED);
		final String result = this.normalizedColorNameResolver.getEnumValue(this.hybrisEnum);
		assertThat(result).isEqualTo(RED);
	}

	/**
	 * @throws FieldValueProviderException
	 *
	 */
	@Test
	public void shouldAddFieldValuesForEnum() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		when(product.getProperty(INDEXED_PROPERTY_NAME)).thenReturn(colorEnum);
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, ATTRIBUTE_NAME);
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		final String attributeName = ATTRIBUTE_NAME;
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(colorEnum, attributeName)).thenReturn(value);
		normalizedColorNameResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(String.class));
	}

	/**
	 *
	 * @throws FieldValueProviderException
	 */
	@Test
	public void shouldNotAddFieldValuesForNonEnum() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		when(product.getProperty(INDEXED_PROPERTY_NAME)).thenReturn(new ItemModel());
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, ATTRIBUTE_NAME);
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		final String attributeName = ATTRIBUTE_NAME;
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(colorEnum, attributeName)).thenReturn(value);
		normalizedColorNameResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);
		verify(getInputDocument(), Mockito.times(0)).addField(any(IndexedProperty.class), any(String.class));
	}
}
