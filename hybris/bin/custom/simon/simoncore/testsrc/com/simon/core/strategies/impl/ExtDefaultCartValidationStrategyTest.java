package com.simon.core.strategies.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.services.ExtCartService;


@UnitTest
public class ExtDefaultCartValidationStrategyTest
{

	@InjectMocks
	@Spy
	ExtDefaultCartValidationStrategy cartValidationStrategy;
	@Mock
	private CartEntryModel entry;
	@Mock
	private CartModel cart;
	@Mock
	private ProductModel product;
	@Mock
	private ModelService modelService;
	@Mock
	private ProductService productService;
	@Mock
	BaseStoreService baseStoreService;
	@Mock
	private BaseStoreModel baseStore;
	@Mock
	CommerceStockService commerceStockService;
	@Mock
	ExtCartService cartService;



	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		doNothing().when(modelService).remove(entry);
		doNothing().when(modelService).refresh(any());
		when(entry.getQuantity()).thenReturn((long) 2);
		when(entry.getProduct()).thenReturn(product);
		when(product.getCode()).thenReturn("product");
		when(productService.getProductForCode("product")).thenReturn(product);
		when(baseStoreService.getCurrentBaseStore()).thenReturn(baseStore);
		when(commerceStockService.getStockLevelForProductAndBaseStore(product, baseStore)).thenReturn(10l);
		final List<CartEntryModel> entrylist = new ArrayList();
		entrylist.add(entry);
		doReturn(entry).when(cartValidationStrategy).getUnavailableCartEntry(entry);
		doReturn(cartService).when(cartValidationStrategy).getCartService();
		when(cartService.getEntriesForProduct(cart, product)).thenReturn(entrylist);
	}

	@Test
	public void validateCartWhenRetailerIsInactive()
	{
		when(product.getRetailerActive()).thenReturn(false);
		when(modelService.create(CartEntryModel.class)).thenReturn(entry);
		final CommerceCartModification modification = cartValidationStrategy.validateCartEntry(cart, entry);
		verify(modelService).remove(entry);
		verify(modelService).refresh(cart);
	}

	@Test
	public void validateCartWhenRetailerIsActive()
	{
		when(product.getRetailerActive()).thenReturn(true);
		final CommerceCartModification modification = cartValidationStrategy.validateCartEntry(cart, entry);
		assertNotNull(modification.getEntry().getBasePrice());
	}

	@Test
	public void validateCartWhenRetailerIsPunchedOut()
	{
		final ShopModel shop = new ShopModel();
		shop.setPunchOutFlag(true);
		when(product.getShop()).thenReturn(shop);
		final CommerceCartModification modification = cartValidationStrategy.validateCartEntry(cart, entry);
		assertNotNull(modification.getStatusCode());
	}

	@Test
	public void validateCartWhenRetailerIsNotPunchedOut()
	{
		final ShopModel shop = new ShopModel();
		shop.setPunchOutFlag(false);
		when(product.getShop()).thenReturn(shop);
		final CommerceCartModification modification = cartValidationStrategy.validateCartEntry(cart, entry);
		assertEquals(CommerceCartModificationStatus.UNAVAILABLE, modification.getStatusCode());
	}
}
