package com.simon.core.common.comparator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.FacetValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.ProductSizeSequenceModel;
import com.simon.core.services.SizeService;




/**
 * This Class tests the compare method of {@link NormalizedSizeSortComparatorTest}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class NormalizedSizeSortComparatorTest
{
	@InjectMocks
	NormalizedSizeSortComparator normalizedSizeSortComparator;

	@Mock
	private SizeService sizeService;

	@Mock
	FacetValue facetValue1, facetValue2;

	private final List<ProductSizeSequenceModel> seqList = new ArrayList<>();

	private final ProductSizeSequenceModel sizeSeq2 = new ProductSizeSequenceModel();
	private final ProductSizeSequenceModel sizeSeq1 = new ProductSizeSequenceModel();
	private final ProductSizeSequenceModel sizeSeq3 = new ProductSizeSequenceModel();

	@Test
	public void testCompareForSameSequences()
	{
		facetValue1 = mock(FacetValue.class);
		facetValue1 = mock(FacetValue.class);
		when(facetValue1.getName()).thenReturn("medium");
		when(facetValue2.getName()).thenReturn("small");
		when(sizeService.getAllSequencesForSizes()).thenReturn(Collections.EMPTY_LIST);
		final int compare = normalizedSizeSortComparator.compare(facetValue1, facetValue2);
		Assert.assertEquals(0, compare);
	}

	@Test
	public void testCompareForDifferentSequences()
	{
		facetValue1 = mock(FacetValue.class);
		facetValue1 = mock(FacetValue.class);
		sizeSeq1.setSequenceNumber(2);
		sizeSeq1.setSize("medium");
		sizeSeq2.setSequenceNumber(1);
		sizeSeq2.setSize("small");
		seqList.add(sizeSeq1);
		seqList.add(sizeSeq2);
		when(facetValue1.getName()).thenReturn("medium");
		when(facetValue2.getName()).thenReturn("small");
		when(sizeService.getAllSequencesForSizes()).thenReturn(seqList);
		final int compare = normalizedSizeSortComparator.compare(facetValue1, facetValue2);
		Assert.assertEquals(1, compare);
	}

	@Test
	public void testCompareForDifferentSequencesWithOneInvalidEntry()
	{
		facetValue1 = mock(FacetValue.class);
		facetValue1 = mock(FacetValue.class);
		sizeSeq1.setSequenceNumber(2);
		sizeSeq1.setSize("medium");
		sizeSeq2.setSequenceNumber(1);
		sizeSeq2.setSize("small");
		seqList.add(sizeSeq1);
		seqList.add(sizeSeq2);
		seqList.add(sizeSeq3);
		when(facetValue1.getName()).thenReturn("medium");
		when(facetValue2.getName()).thenReturn("small");
		when(sizeService.getAllSequencesForSizes()).thenReturn(seqList);
		final int compare = normalizedSizeSortComparator.compare(facetValue1, facetValue2);
		Assert.assertEquals(1, compare);
	}
}
