package com.simon.core.product.strategies.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.enums.MiraklAttributeRole;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;


/**
 * The Class SimonProductCodeGenerationStrategyTest. Unit test for {@link ExtProductCodeGenerationStrategy}
 */
@UnitTest
public class ExtProductCodeGenerationStrategyTest
{

	/** The simon product code generation strategy. */
	@InjectMocks
	private ExtProductCodeGenerationStrategy simonProductCodeGenerationStrategy;

	/** The shop service. */
	@Mock
	private ShopService shopService;

	/** The key generator. */
	@Mock
	KeyGenerator keyGenerator;


	/**
	 * Setup method.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify shop prefix present is correctly appended to product code.
	 */
	@Test
	public void verifyShopPrefixPresentIsCorrectlyAppendedToVariantProductCode()
	{

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);
		mockCommonData(context, rawProduct);

		final ShopModel shopModel = mock(ShopModel.class);
		when(shopModel.getPreFix()).thenReturn("USPOLO");
		when(shopService.getShopForId("18")).thenReturn(shopModel);

		final ComposedTypeModel composedType = mock(ComposedTypeModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);

		final String actual = simonProductCodeGenerationStrategy.generateCode(composedType, rawProduct, baseProduct, context);

		assertThat(actual, is("USPOLO-786664457021"));
	}

	/**
	 * Verify shop prefix not present default is appended to product code.
	 */
	@Test
	public void verifyShopPrefixNotPresentDefaultIsAppendedToVariantProductCode()
	{
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);
		mockCommonData(context, rawProduct);

		final ShopModel shopModel = mock(ShopModel.class);
		when(shopService.getShopForId("18")).thenReturn(shopModel);

		final ComposedTypeModel composedType = mock(ComposedTypeModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);

		final String actual = simonProductCodeGenerationStrategy.generateCode(composedType, rawProduct, baseProduct, context);

		assertThat(actual, is("Shop-786664457021"));
	}

	/**
	 * Verify shop prefix present is correctly appended to base product code.
	 */
	@Test
	public void verifyShopPrefixPresentIsCorrectlyAppendedToBaseProductCode()
	{

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);
		mockCommonData(context, rawProduct);

		final ShopModel shopModel = mock(ShopModel.class);
		when(shopModel.getPreFix()).thenReturn("USPOLO");
		when(shopService.getShopForId("18")).thenReturn(shopModel);

		final ComposedTypeModel composedType = mock(ComposedTypeModel.class);

		final String actual = simonProductCodeGenerationStrategy.generateCode(composedType, rawProduct, null, context);

		assertThat(actual, is("USPOLO-113903-0400A"));
	}

	/**
	 * Verify shop prefix not present default is appended to base product code.
	 */
	@Test
	public void verifyShopPrefixNotPresentDefaultIsAppendedToBaseProductCode()
	{
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);
		mockCommonData(context, rawProduct);

		final ShopModel shopModel = mock(ShopModel.class);
		when(shopService.getShopForId("18")).thenReturn(shopModel);

		final ComposedTypeModel composedType = mock(ComposedTypeModel.class);

		final String actual = simonProductCodeGenerationStrategy.generateCode(composedType, rawProduct, null, context);

		assertThat(actual, is("Shop-113903-0400A"));
	}

	/**
	 * Mock common data.
	 *
	 * @param context
	 *           the context
	 * @param rawProduct
	 *           the raw product
	 */
	public void mockCommonData(final ProductImportFileContextData context, final MiraklRawProductModel rawProduct)
	{
		final Map<MiraklAttributeRole, String> coreAttributeRoleMap = new HashMap<>();
		coreAttributeRoleMap.put(MiraklAttributeRole.SHOP_SKU_ATTRIBUTE, "SKU");

		final ProductImportGlobalContextData globalContext = mock(ProductImportGlobalContextData.class);
		when(globalContext.getCoreAttributePerRole()).thenReturn(coreAttributeRoleMap);
		when(context.getGlobalContext()).thenReturn(globalContext);
		when(context.getShopId()).thenReturn("18");

		when(rawProduct.getSku()).thenReturn("786664457021");
		when(rawProduct.getVariantGroupCode()).thenReturn("113903-0400A");
	}
}
