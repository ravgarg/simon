package com.simon.media.hotfolder.task;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.exceptions.ProductFileImportPostProcessingException;
import com.simon.core.file.wrapper.FileWriterWrapper;
import com.simon.core.mirakl.strategies.impl.ErrorHandlingPostProcessor;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ErrorHandlingPostProcessorTest
{

	@InjectMocks
	@Mock
	ErrorHandlingPostProcessor errorHandlingPostProcessor;

	@Mock
	private ConfigurationService configurationService;

	/** The file writer wrapper for IO operations. */
	@Mock
	FileWriterWrapper fileWriterWrapper;

	@Mock
	ProductImportFileContextData context;

	private static final String PATH_SEPERATOR = "file.separator";

	private static final String CSV_SEPERATOR = "simon.product.feed.error.summary.csv.seperator";

	private static final String FILENAME_SEPERATOR = "product.image.filename.seperator";

	private static final String CSV_URL = "simon.product.feed.error.log";

	private static final String PREFIX_FILENAME_SUMMARY = "simon.product.feed.error.summary.prefix";

	private static final String DEFAULT_SEPERATOR = "|";

	private static final String DEFAULT_FILENAME_SEPERATOR = "-";

	private static final String EXTENSION_FILE = ".csv";

	private static final String REGEX_DATE = "yyyyMMddHHmm";

	private static final String IMPORT_ID = "importId";

	private static final String URL = "url";

	private static final String PREFIX = "prefix";

	private static final String SHOP_ID = "shopID";

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	private void testPostProcess() throws IOException, ProductFileImportPostProcessingException
	{
		when(configurationService.getConfiguration().getString(FILENAME_SEPERATOR, DEFAULT_FILENAME_SEPERATOR))
				.thenReturn(DEFAULT_FILENAME_SEPERATOR);
		when(configurationService.getConfiguration().getString(CSV_URL)).thenReturn(URL);
		when(configurationService.getConfiguration().getString(PREFIX_FILENAME_SUMMARY)).thenReturn(PREFIX);
		when(configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR)).thenReturn(DEFAULT_SEPERATOR);

		when(context.getShopId()).thenReturn(SHOP_ID);
		errorHandlingPostProcessor.postProcess(context, IMPORT_ID);
		verify(fileWriterWrapper).getWriter(eq(CSV_URL + System.getProperty(PATH_SEPERATOR) + PREFIX + DEFAULT_FILENAME_SEPERATOR
				+ SHOP_ID + DEFAULT_FILENAME_SEPERATOR + IMPORT_ID + new SimpleDateFormat(REGEX_DATE).format(new Date())
				+ EXTENSION_FILE));
	}

	@Test(expected = IOException.class)
	private void testPostProcessThrowIOException() throws IOException, ProductFileImportPostProcessingException
	{
		when(configurationService.getConfiguration().getString(FILENAME_SEPERATOR, DEFAULT_FILENAME_SEPERATOR))
				.thenReturn(DEFAULT_FILENAME_SEPERATOR);
		when(configurationService.getConfiguration().getString(CSV_URL)).thenReturn(URL);
		when(configurationService.getConfiguration().getString(PREFIX_FILENAME_SUMMARY)).thenReturn(PREFIX);
		when(configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR)).thenReturn(DEFAULT_SEPERATOR);
		when(context.getShopId()).thenReturn(SHOP_ID);
		when(fileWriterWrapper.getWriter(CSV_URL + System.getProperty(PATH_SEPERATOR) + PREFIX + DEFAULT_FILENAME_SEPERATOR
				+ SHOP_ID + DEFAULT_FILENAME_SEPERATOR + IMPORT_ID + new SimpleDateFormat(REGEX_DATE).format(new Date())
				+ EXTENSION_FILE)).thenThrow(new IOException());
		errorHandlingPostProcessor.postProcess(context, IMPORT_ID);
	}



}
