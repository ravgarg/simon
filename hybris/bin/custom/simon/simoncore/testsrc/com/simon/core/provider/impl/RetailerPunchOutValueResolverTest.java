package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.mirakl.hybris.core.model.ShopModel;


@UnitTest
public class RetailerPunchOutValueResolverTest extends AbstractValueResolverTest
{
	@Spy
	@InjectMocks
	RetailerPunchOutValueResolver retailerPunchOutValueResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	public static final String OPTIONAL_PARAM = "optional";
	public static final String ATTRIBUTE_PARAM = "attribute";

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final String attributeName = "attributeName";
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, attributeName);
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		final ShopModel shop = new ShopModel();
		baseProduct.setShop(shop);
		model.setBaseProduct(baseProduct);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(shop.getClass())).thenReturn(composedValue);
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		when(modelService.getAttributeValue(shop, attributeName)).thenReturn(Boolean.FALSE);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		retailerPunchOutValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}




	@Test(expected = FieldValueProviderException.class)
	public void testResolveNullShopModelAndIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		retailerPunchOutValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
	}


	@Test
	public void testResolveNullShopModel() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		retailerPunchOutValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}


	@Test
	public void testGetAttributeNameWithValidAttributeName()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, "attributeName");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		assertEquals("attributeName", retailerPunchOutValueResolver.getAttributeName(indexedProperty));
	}

	@Test
	public void testGetAttributeValueWithNullAttributeName()
	{
		final ShopModel shop = new ShopModel();
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		assertNull(retailerPunchOutValueResolver.getAttributeValue(shop, null));

	}

	@Test
	public void testGetModelAttributeValueWithValidData()
	{
		final ShopModel shop = new ShopModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(shop.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(shop, attributeName)).thenReturn(value);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		assertEquals(value, retailerPunchOutValueResolver.getModelAttributeValue(shop, attributeName));
	}

	@Test
	public void testGetAttributeNameWithNullAttributeName()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		indexedProperty.setName("attributeName");
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		assertEquals("attributeName", retailerPunchOutValueResolver.getAttributeName(indexedProperty));
	}

	@Test
	public void testGetModelAttributeValueWithTypeServiceWithoutAttribute()
	{
		final ShopModel shop = new ShopModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(shop.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.FALSE);
		doReturn("Buy on SPO.com").when(retailerPunchOutValueResolver).getLocalizedFacetName();
		assertNull(retailerPunchOutValueResolver.getModelAttributeValue(shop, attributeName));
	}
}
