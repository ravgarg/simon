package com.simon.core.solrfacetsearch.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.search.Facet;
import de.hybris.platform.solrfacetsearch.search.SearchResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Junit Test case to test ExtSearchResponseFacetsPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtSearchResponseFacetsPopulatorTest
{
	@InjectMocks
	@Spy
	private ExtSearchResponseFacetsPopulator extSearchResponseFacetsPopulator;
	@Mock
	private SearchResult solrSearchResult;
	@Mock
	private SolrSearchQueryData searchQueryData;
	@Mock
	private IndexedType indexedType;

	@Mock
	private Facet facet;
	@Mock
	private IndexedProperty indexedProperty;

	/**
	 * Junit Test case to test ExtSearchResponseFacetsPopulator
	 */
	@Test
	public void buildFacets()
	{

		final List<Facet> solrSearchResultFacets = new ArrayList<>();
		solrSearchResultFacets.add(facet);
		when(solrSearchResult.getFacets()).thenReturn(solrSearchResultFacets);
		when(facet.getName()).thenReturn("name");
		final Map<String, IndexedProperty> indexedProperties = new HashMap<>();
		indexedProperties.put("name", indexedProperty);
		when(indexedType.getIndexedProperties()).thenReturn(indexedProperties);
		assertEquals(0, extSearchResponseFacetsPopulator.buildFacets(solrSearchResult, searchQueryData, indexedType).size());
	}
}
