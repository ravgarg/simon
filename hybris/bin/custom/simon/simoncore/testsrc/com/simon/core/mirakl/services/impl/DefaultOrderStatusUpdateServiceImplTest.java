package com.simon.core.mirakl.services.impl;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.order.MiraklOrder;
import com.mirakl.client.mmp.domain.order.state.AbstractMiraklOrderStatus.State;
import com.mirakl.client.mmp.domain.order.state.MiraklOrderStatus;
import com.mirakl.client.mmp.domain.payment.debit.MiraklOrderPayment;
import com.mirakl.hybris.core.fulfilment.strategies.ProcessMarketplacePaymentStrategy;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.ordersplitting.services.MarketplaceConsignmentService;
import com.simon.core.service.mirakl.MarketPlaceService;


/**
 * DefaultOrderStatusUpdateServiceImplTest, unit Test for {@link DefaultOrderStatusUpdateServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultOrderStatusUpdateServiceImplTest
{
	@InjectMocks
	@Spy
	private DefaultOrderStatusUpdateServiceImpl defaultOrderStatusUpdateServiceImpl;

	@Mock
	private MarketplaceConsignmentService marketplaceConsignmentService;

	@Mock
	private ModelService modelService;

	@Mock
	private Populator<MiraklOrder, MarketplaceConsignmentModel> updateConsignmentPopulator;

	@Mock
	private OrderModel order;

	@Mock
	private MiraklOrder miraklOrder;

	@Mock
	private MiraklOrderPayment miraklOrderPayment;

	@Mock
	private MarketPlaceService marketPlaceService;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private MiraklOrderStatus miraklOrderStatus;

	@Mock
	private ProcessMarketplacePaymentStrategy processMarketplacePaymentStrategy;

	private MarketplaceConsignmentModel marketplaceConsignment;


	@Before
	public void setUp()
	{
		marketplaceConsignment = new MarketplaceConsignmentModel();
		marketplaceConsignment.setOrder(order);
		marketplaceConsignment.setCode("Test");
		when(marketplaceConsignmentService.loadConsignmentUpdate(marketplaceConsignment)).thenReturn(miraklOrder);
		when(marketplaceConsignmentService.loadDebitRequest(marketplaceConsignment)).thenReturn(miraklOrderPayment);
	}

	@Test
	public void updateOrderConsignmentAndLineItemsStatusIfNoUpdate()
	{
		marketplaceConsignment.setConsignmentUpdatePayload(null);
		when(marketplaceConsignmentService.loadConsignmentUpdate(marketplaceConsignment)).thenReturn(null);
		defaultOrderStatusUpdateServiceImpl.updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
		verify(defaultOrderStatusUpdateServiceImpl).updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);

	}

	@Test
	public void UpdateOrderConsignmentAndLineItemsStatusAcceptOrder()
	{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean("acceptance.order.in.order.process")).thenReturn(Boolean.TRUE);
		when(miraklOrder.getStatus()).thenReturn(miraklOrderStatus);
		when(miraklOrderStatus.getState()).thenReturn(State.WAITING_ACCEPTANCE);
		defaultOrderStatusUpdateServiceImpl.updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
		verify(updateConsignmentPopulator).populate(eq(miraklOrder), eq(marketplaceConsignment));
		verify(marketPlaceService).acceptOrRejectOrder(marketplaceConsignment, Boolean.TRUE);
		verify(defaultOrderStatusUpdateServiceImpl).updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
	}

	@Test
	public void UpdateOrderConsignmentAndLineItemsStatusUpdatePayment()
	{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean("acceptance.order.in.order.process")).thenReturn(Boolean.TRUE);
		when(miraklOrder.getStatus()).thenReturn(miraklOrderStatus);
		when(miraklOrderStatus.getState()).thenReturn(State.SHIPPING);
		defaultOrderStatusUpdateServiceImpl.updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
		verify(updateConsignmentPopulator).populate(eq(miraklOrder), eq(marketplaceConsignment));
		verify(marketPlaceService).processPayment(marketplaceConsignment);
		verify(defaultOrderStatusUpdateServiceImpl).updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
	}

	@Test
	public void UpdateOrderConsignmentAndReceivedOrder()
	{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getBoolean("acceptance.order.in.order.process")).thenReturn(Boolean.TRUE);
		when(miraklOrder.getStatus()).thenReturn(miraklOrderStatus);
		when(miraklOrderStatus.getState()).thenReturn(State.SHIPPING);
		defaultOrderStatusUpdateServiceImpl.updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
		verify(updateConsignmentPopulator).populate(eq(miraklOrder), eq(marketplaceConsignment));
		verify(marketPlaceService).receivedMarketplaceOrder(marketplaceConsignment.getCode());
		verify(defaultOrderStatusUpdateServiceImpl).updateOrderConsignmentAndLineItemsStatus(marketplaceConsignment);
	}



}
