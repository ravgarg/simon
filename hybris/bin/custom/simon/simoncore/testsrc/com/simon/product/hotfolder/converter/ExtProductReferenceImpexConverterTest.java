package com.simon.product.hotfolder.converter;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;


/**
 * Contains tests for ExtProductReferenceImpexConverter.
 */
@IntegrationTest
public class ExtProductReferenceImpexConverterTest extends ServicelayerTransactionalTest
{
	@Resource
	private ExtProductReferenceImpexConverter batchProductReferenceConverter;

	/**
	 * Test when comma seperated targets are in the CSV
	 */
	@Test
	public void testConvert()
	{
		final Map<Integer, String> row = new HashMap<>();
		row.put(0, "CODE1");
		row.put(1, "CODE2\\|CODE3\\|CODE4");

		final Long sequenceId = Long.valueOf(1l);

		final String result = batchProductReferenceConverter.convert(row, sequenceId);

		Assert.assertTrue(!result.isEmpty());
	}
}
