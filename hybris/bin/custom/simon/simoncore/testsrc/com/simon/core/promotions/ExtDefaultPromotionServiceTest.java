package com.simon.core.promotions;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.promotions.service.impl.ExtDefaultPromotionActionServiceImpl;
import com.simon.promotion.rao.RetailerRAO;


/**
 *
 */
@UnitTest
public class ExtDefaultPromotionServiceTest
{
	@InjectMocks
	ExtDefaultPromotionActionServiceImpl promotionService;
	@Mock
	DiscountRAO discount;
	@Mock
	CartModel cart;
	@Mock
	RetailerRAO retailer;
	@Mock
	private CurrencyModel currency;
	private Map<String, List<DiscountValue>> map;
	@Mock
	private RetailerRAO retailer1;

	@Before
	public void setUp()
	{
		initMocks(this);
		when(discount.getRetailer()).thenReturn(retailer);
		when(discount.getValue()).thenReturn(new BigDecimal(1));
		when(retailer.getRetailerId()).thenReturn("retailer");
		when(cart.getCurrency()).thenReturn(currency);
		map = new HashMap<>();
		when(cart.getSubbagDiscountValues()).thenReturn(map);
		doNothing().when(cart).setSubbagDiscountValues(map);
	}

	@Test
	public void createRetailerDiscountValue()
	{
		when(discount.getCurrencyIsoCode()).thenReturn("USD");
		promotionService.createRetailerDiscountValue(discount, "abc", cart);
		verify(cart).getSubbagDiscountValues();
	}

	@Test
	public void createRetailerDiscountValue_whenCurrencyIsNull()
	{
		when(discount.getCurrencyIsoCode()).thenReturn(null);
		promotionService.createRetailerDiscountValue(discount, "abc", cart);
		verify(cart).getSubbagDiscountValues();
	}

	@Test
	public void createRetailerDiscountValue_whenThereIsExistingDiscount()
	{
		when(discount.getCurrencyIsoCode()).thenReturn(null);
		when(discount.getRetailer()).thenReturn(retailer1);
		when(retailer1.getRetailerId()).thenReturn("retailer1");
		final Map<String, List<DiscountValue>> retailerDiscountValue = new HashMap<>();
		final List<DiscountValue> discValue = new ArrayList<>();
		final DiscountValue discountValue = new DiscountValue("code", 0, false, 0, "USD");
		discValue.add(discountValue);
		retailerDiscountValue.put("retailer1", discValue);
		when(cart.getSubbagDiscountValues()).thenReturn(retailerDiscountValue);
		promotionService.createRetailerDiscountValue(discount, "abc", cart);
		verify(cart).getSubbagDiscountValues();
	}
}
