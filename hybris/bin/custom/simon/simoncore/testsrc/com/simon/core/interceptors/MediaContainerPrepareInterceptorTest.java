package com.simon.core.interceptors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaContainerPrepareInterceptorTest
{
	@InjectMocks
	private MediaContainerPrepareInterceptor mediaContainerPrepareInterceptor;

	@Mock
	private InterceptorContext ctx;

	@Mock
	private MediaContainerModel mediaContainer;

	@Mock
	private MediaModel media;

	@Test
	public void testMediaGetsRegisteredWhenMediaContainerVersionIsUpdated() throws InterceptorException
	{
		when(ctx.isModified(mediaContainer, MediaContainerModel.VERSION)).thenReturn(true);
		when(mediaContainer.getVersion()).thenReturn(3);
		when(mediaContainer.getMedias()).thenReturn(Arrays.asList(media));
		mediaContainerPrepareInterceptor.onPrepare(mediaContainer, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(media, PersistenceOperation.SAVE);
	}

	@Test
	public void testMediaIsNotRegisteredWhenMediaContainerVersionIsNotUpdated() throws InterceptorException
	{
		when(ctx.isModified(mediaContainer, MediaContainerModel.VERSION)).thenReturn(false);
		mediaContainerPrepareInterceptor.onPrepare(mediaContainer, ctx);
		verify(ctx, Mockito.times(0)).registerElementFor(media, PersistenceOperation.SAVE);
	}

}
