package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentModeModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.services.RetailerService;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class RetailerEntryRaoPopulatorTest
{
	@InjectMocks
	RetailerRaoEntryPopulator populator;
	@Mock
	RetailerService retailerService;
	CartModel cart;
	CartRAO cartRAO;
	RetailerRAO retailerRAO;
	OrderEntryRAO orderEntryRAO;
	@Mock
	CartEntryModel entry1;
	@Mock
	private Converter<AbstractOrderEntryModel, OrderEntryRAO> entryConverter;
	@Mock
	private Converter<AbstractOrderModel, RetailerRAO> retailerRaoConverter;

	@Before
	public void setup()
	{
		initMocks(this);
		entry1 = new CartEntryModel();
		final ProductModel p1 = new ProductModel();
		final PaymentModeModel paymentMode = new PaymentModeModel();
		cart = new CartModel();
		final CurrencyModel currency = new CurrencyModel();
		currency.setIsocode("USD");

		cartRAO = new CartRAO();
		retailerRAO = new RetailerRAO();
		orderEntryRAO = new OrderEntryRAO();

		retailerRAO.setRetailerId("retailer1");
		final Map<String, Double> retailerSubTotalMap = new HashMap();

		retailerSubTotalMap.put("retailer1", 10.0);

		final List<AbstractOrderEntryModel> entries = new ArrayList();

		p1.setCode("product1");
		final ShopModel s1 = new ShopModel();
		s1.setId("retailer1");
		p1.setShop(s1);
		entry1.setProduct(p1);
		entries.add(entry1);

		cart.setPaymentMode(paymentMode);
		cart.setEntries(entries);
		cart.setCode("1234");
		cart.setCurrency(currency);

		final Map<String, Double> retailerDeliveryCost = new HashMap<String, Double>();
		retailerDeliveryCost.put("retailer1", 20.0);

		cart.setRetailersDeliveryCost(retailerDeliveryCost);
		cart.setDeliveryCost(Double.valueOf(20.0));

		when(retailerService.getRetailerSubBagForCart(cart)).thenReturn(retailerSubTotalMap);
		when(entryConverter.convert(entry1)).thenReturn(orderEntryRAO);
	}

	@Test
	public void populate()
	{
		populator.populate(cart, retailerRAO);
		assertEquals("retailer1", retailerRAO.getRetailerId());
		assertEquals("USD", retailerRAO.getCurrencyIsoCode());
		assertEquals("1234", retailerRAO.getCode());
		final BigDecimal bd1 = new BigDecimal("20.0");
		assertEquals(bd1, retailerRAO.getDeliveryCost());
	}

}
