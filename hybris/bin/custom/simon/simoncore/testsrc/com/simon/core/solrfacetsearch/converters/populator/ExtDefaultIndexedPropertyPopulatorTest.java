package com.simon.core.solrfacetsearch.converters.populator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedPropertyModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.enums.SolrDisplayFacetType;


/**
 * Junit Test case to test ExtDefaultIndexedPropertyPopulator
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtDefaultIndexedPropertyPopulatorTest
{
	@Mock
	private SolrIndexedPropertyModel source;
	@InjectMocks
	@Spy
	private ExtDefaultIndexedPropertyPopulator extDefaultIndexedPropertyPopulator;

	/**
	 * Junit Test case to test ExtDefaultIndexedPropertyPopulator
	 */
	@Test
	public void populate()
	{
		MockitoAnnotations.initMocks(this);
		final IndexedProperty target = new IndexedProperty();
		when(source.getDisplayFacetType()).thenReturn(SolrDisplayFacetType.SQUARE);
		doNothing().when(extDefaultIndexedPropertyPopulator).superPopulate(source, target);
		extDefaultIndexedPropertyPopulator.populate(source, target);
		assertEquals("SQUARE", target.getDisplayFacetType());
	}
}
