package com.simon.core.product.strategies.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * The Class SimonProductIdentificationStrategyTest. Unit test for {@link SimonProductIdentificationStrategy}
 */
@UnitTest
public class SimonProductIdentificationStrategyTest
{

	/** The simon product identification strategy. */
	@InjectMocks
	private SimonProductIdentificationStrategy simonProductIdentificationStrategy;

	/**
	 * Setup method.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify if product resolved by shop sku present used as identified product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyIfProductResolvedByShopSkuPresentUsedAsIdentifiedProduct() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		final ProductModel productResolvedByShopSku = mock(ProductModel.class);
		when(data.getProductResolvedByShopSku()).thenReturn(productResolvedByShopSku);

		simonProductIdentificationStrategy.identifyProduct(data);
		verify(data).setIdentifiedProduct(productResolvedByShopSku);
	}

	/**
	 * Verify if not present product resolved by shop sku identified product is not set.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyIfNotPresentProductResolvedByShopSkuIdentifiedProductIsNotSet() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		simonProductIdentificationStrategy.identifyProduct(data);
		verify(data, times(0)).setIdentifiedProduct(any(ProductModel.class));
	}

}
