package com.simon.core.mirakl.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.StockLevelModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;


/**
 * ExtStockPopulatorTest, unit test for {@link ExtStockPopulator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtStockPopulatorTest
{
	@InjectMocks
	private ExtStockPopulator extStockPopulator;

	/**
	 * Verify stock level populated correctly from mirakl offer.
	 */
	@Test
	public void verifyStockLevelPopulatedCorrectlyFromMiraklOffer()
	{
		final MiraklExportOffer miraklExportOffer = mock(MiraklExportOffer.class);
		when(miraklExportOffer.getQuantity()).thenReturn(10);
		when(miraklExportOffer.getId()).thenReturn("OFID-1");
		when(miraklExportOffer.getProductSku()).thenReturn("testSkuId1");

		final StockLevelModel target = mock(StockLevelModel.class);
		extStockPopulator.populate(miraklExportOffer, target);

		verify(target).setAvailable(10);
		verify(target).setProductCode("testSkuId1");
	}
}
