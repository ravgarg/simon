package com.simon.core.populators;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.VariantCategoryData;


@UnitTest
public class VariantCategoryMapPopulatorTest
{
	@Spy
	@InjectMocks
	private VariantCategoryMapPopulator variantCategoryMapPopulator;
	@Mock
	private Document document;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testPopulateWithValidData()
	{
		final List<String> categoryPath = new ArrayList<>();
		categoryPath.add("/color/size/fit");
		categoryPath.add("/color/size/fit/style");
		final List<String> categoryDetail = new ArrayList<>();
		categoryDetail.add("size|Size|swatch");
		categoryDetail.add("color|Color|swatch");
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(categoryDetail);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(categoryPath);
		when(document.getFieldValue("sizeId")).thenReturn("variantValueCategoryId");
		when(document.getFieldValue("sizeName")).thenReturn("CatName");
		doReturn("Title").when(variantCategoryMapPopulator).getLocalizedStringForLabel("Size");
		final Map<String, VariantCategoryData> variantCategoryMap = new HashMap<>();
		variantCategoryMapPopulator.populate(document, variantCategoryMap);
		assertTrue(variantCategoryMap.containsKey("size"));
	}

	@Test
	public void testPopulateWithEmptyCategoryPath()
	{
		final List<String> categoryPath = new ArrayList<>();
		final List<String> categoryDetail = new ArrayList<>();
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(categoryDetail);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(categoryPath);
		final Map<String, VariantCategoryData> variantCategoryMap = new HashMap<>();
		variantCategoryMapPopulator.populate(document, variantCategoryMap);
		assertTrue(variantCategoryMap.isEmpty());
	}

	@Test
	public void testPopulateWithEmptyVariantValueCategoryId()
	{
		final List<String> categoryPath = new ArrayList<>();
		categoryPath.add("/color/size/fit");
		categoryPath.add("/color/size/fit/style");
		final List<String> categoryDetail = new ArrayList<>();
		categoryDetail.add("size|Size|swatch");
		categoryDetail.add("color|Color|swatch");
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(categoryDetail);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(categoryPath);
		when(document.getFieldValue("fit")).thenReturn(null);
		final Map<String, VariantCategoryData> variantCategoryMap = new HashMap<>();
		variantCategoryMapPopulator.populate(document, variantCategoryMap);
		assertTrue(variantCategoryMap.isEmpty());
	}

}
