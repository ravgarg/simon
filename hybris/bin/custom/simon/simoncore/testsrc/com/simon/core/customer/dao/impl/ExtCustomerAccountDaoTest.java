package com.simon.core.customer.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.core.customer.dao.ExtCustomerAccountDao;
import com.simon.core.model.DesignerModel;


/**
 * The Class ExtCustomerAccountDaoTest. Integration test for {@link ExtCustomerAccountDaoImpl}
 */
@IntegrationTest
public class ExtCustomerAccountDaoTest extends ServicelayerTransactionalTest
{

	@Resource
	private ExtCustomerAccountDao extCustomerAccountDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private UserService userService;

	private CustomerModel customerModel;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testDesigners.impex", "utf-8");
		importCsv("/simoncore/test/testDeals.impex", "utf-8");
		customerModel = (CustomerModel) userService.getUserForUID("ava@milanie.com");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify correct designer is returned for valid customer.
	 */
	@Test
	public void verifyFindListOfAllDesigners()
	{
		final List<DesignerModel> designers = extCustomerAccountDao.findListOfAllDesigners(customerModel);
		assertNotNull(designers);
		assertEquals(2, designers.size());
	}

	/**
	 * Verify the address is duplicated by some other billing address
	 */
	@Test
	public void veriFindAddressByDuplicateFrom()
	{
		final List<AddressModel> duplicateAddresses = extCustomerAccountDao.findAddressByDuplicateFrom("8024580147");
		assertNotNull(duplicateAddresses);
		assertEquals(1, duplicateAddresses.size());
	}

	/**
	 * Verify correct designer is returned for valid customer.
	 */
	@Test
	public void verifyFindListOfDesigners()
	{
		final List<DesignerModel> designers = extCustomerAccountDao.findListOfAllDesigners();
		assertNotNull(designers);
		assertEquals(3, designers.size());
	}

}
