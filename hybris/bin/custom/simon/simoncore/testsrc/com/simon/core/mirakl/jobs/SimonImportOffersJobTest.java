package com.simon.core.mirakl.jobs;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobResult;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.dto.OfferImportMetaData;
import com.simon.core.exceptions.OfferImportException;
import com.simon.core.mirakl.services.ExtOfferImportService;
import com.simon.hybris.core.model.SimonImportOffersCronJobModel;


/**
 * SimonImportOffersJobTest unit test for {@link SimonImportOffersJob}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SimonImportOffersJobTest
{

	@InjectMocks
	private SimonImportOffersJob simonImportOffersJob;

	@Mock
	private ExtOfferImportService extOfferImportService;

	@Mock
	private ModelService modelService;


	/**
	 * Verify import all offers invoked with correct parameters when last import time is null.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyImportAllOffersInvokedWithCorrectParametersWhenLastImportTimeIsNull() throws OfferImportException
	{
		final SimonImportOffersCronJobModel simonImportOffersCronJob = mock(SimonImportOffersCronJobModel.class);
		final Date startTime = mock(Date.class);
		when(simonImportOffersCronJob.getStartTime()).thenReturn(startTime);

		simonImportOffersJob.perform(simonImportOffersCronJob);
		verify(extOfferImportService).importAllOffers(eq(startTime), any(OfferImportMetaData.class));
	}

	/**
	 * Verify import all offers invoked with correct parameters.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyImportAllOffersInvokedWithCorrectParametersWhenFullImport() throws OfferImportException
	{
		final SimonImportOffersCronJobModel simonImportOffersCronJob = mock(SimonImportOffersCronJobModel.class);
		final Date startTime = mock(Date.class);
		when(simonImportOffersCronJob.getStartTime()).thenReturn(startTime);

		when(simonImportOffersCronJob.isFullImport()).thenReturn(true);
		simonImportOffersJob.perform(simonImportOffersCronJob);
		verify(extOfferImportService).importAllOffers(eq(startTime), any(OfferImportMetaData.class));
	}

	/**
	 * Verify import offers updated since invoked with correct parameters.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyImportOffersUpdatedSinceInvokedWithCorrectParameters() throws OfferImportException
	{
		final SimonImportOffersCronJobModel simonImportOffersCronJob = mock(SimonImportOffersCronJobModel.class);
		final Date lastImportTime = mock(Date.class);
		when(simonImportOffersCronJob.getLastImportTime()).thenReturn(lastImportTime);

		simonImportOffersJob.perform(simonImportOffersCronJob);
		verify(extOfferImportService).importOffersUpdatedSince(eq(lastImportTime), any(OfferImportMetaData.class));
	}

	/**
	 * Verify exception in full offer import results in cron job failure.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyExceptionInFullOfferImportResultsInCronJobFailure() throws OfferImportException
	{
		doThrow(new OfferImportException("")).when(extOfferImportService).importAllOffers(any(Date.class),
				any(OfferImportMetaData.class));
		final SimonImportOffersCronJobModel simonImportOffersCronJob = mock(SimonImportOffersCronJobModel.class);
		final PerformResult actual = simonImportOffersJob.perform(simonImportOffersCronJob);
		assertThat(actual.getResult().getCode(), is(CronJobResult.ERROR));
		assertThat(actual.getStatus().getCode(), is(CronJobStatus.ABORTED));
	}

	/**
	 * Verify exception in partial offer import results in cron job failure.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyExceptionInPartialOfferImportResultsInCronJobFailure() throws OfferImportException
	{
		doThrow(new OfferImportException("")).when(extOfferImportService).importOffersUpdatedSince(any(Date.class),
				any(OfferImportMetaData.class));
		final SimonImportOffersCronJobModel simonImportOffersCronJob = mock(SimonImportOffersCronJobModel.class);
		final Date lastImportTime = mock(Date.class);
		when(simonImportOffersCronJob.getLastImportTime()).thenReturn(lastImportTime);

		final PerformResult actual = simonImportOffersJob.perform(simonImportOffersCronJob);
		assertThat(actual.getResult().getCode(), is(CronJobResult.ERROR));
		assertThat(actual.getStatus().getCode(), is(CronJobStatus.ABORTED));
	}
}
