package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dao.LeftNavigationCategoryDao;
import com.simon.core.dto.NavigationData;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.resolver.ExtCategoryModelUrlResolver;


@UnitTest
public class LeftNavigationCategoryServiceImplTest
{
	@InjectMocks
	private LeftNavigationCategoryServiceImpl leftNavigationCategoryService;

	@Mock
	private LeftNavigationCategoryDao leftNavigationCategoryDao;

	@Mock
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;

	@Mock
	private CategoryService categoryService;

	@Mock
	MerchandizingCategoryModel lastSiblingCategoryModel;

	@Mock
	MerchandizingCategoryModel lastCategoryModel;
	@Mock
	MerchandizingCategoryModel level2CategoryModel;
	@Mock
	MerchandizingCategoryModel level1CategoryModel;
	@Mock
	MerchandizingCategoryModel level0CategoryModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(lastCategoryModel.getCategories()).thenReturn(null);
		when(level2CategoryModel.getCategories())
				.thenReturn(new ArrayList<CategoryModel>()
				{
					{
						add(lastSiblingCategoryModel);
						add(lastCategoryModel);
					}
				});
		when(level1CategoryModel.getCategories()).thenReturn(Collections.singletonList(level2CategoryModel));
		when(level0CategoryModel.getCategories()).thenReturn(Collections.singletonList(level1CategoryModel));
		when(lastSiblingCategoryModel.getCode()).thenReturn("Jeans");
		when(lastSiblingCategoryModel.getName()).thenReturn("Jeans");
		when(lastCategoryModel.getCode()).thenReturn("Pants");
		when(lastCategoryModel.getName()).thenReturn("Pants");
		when(level2CategoryModel.getCode()).thenReturn("Clothing");
		when(level2CategoryModel.getName()).thenReturn("Clothing");
		when(level1CategoryModel.getCode()).thenReturn("Men");
		when(level1CategoryModel.getName()).thenReturn("Men");
		when(level0CategoryModel.getCode()).thenReturn("Gender");
		when(level0CategoryModel.getName()).thenReturn("Gender");
		when(leftNavigationCategoryDao.getL1Categories(Matchers.anyString()))
				.thenReturn(Collections.singletonList(level0CategoryModel));
	}

	@Test
	public void testFetchNavigationDataWithValidL1Category()
	{
		final List<NavigationData> navData = leftNavigationCategoryService.getNavigationData("Gender");
		assertEquals(navData.get(0).getId(), "Men");
	}

	@Test
	public void testMapCreation()
	{
		assertEquals(leftNavigationCategoryService.createMap(), true);
	}


	@Test
	public void testFetchNavigationDataWithoutL1Category()
	{
		final List<NavigationData> navData = leftNavigationCategoryService.getNavigationData("Clothing");
		assertEquals(navData, Collections.EMPTY_LIST);
	}
}
