package com.simon.core.catalog.strategies.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.MediaKeyGenerator;


/**
 * The Class DesignerAttributeHandlerTest. Unit test for {@link DesignerAttributeHandler}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ColorSwatchUrlAttributeHandlerTest
{

	/** The designer attribute handler. */
	@InjectMocks
	private ColorSwatchUrlAttributeHandler colorSwatchUrlAttributeHandler;

	/** The core attribute owner strategy. */
	@Mock
	CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	/** The designer service. */
	@Mock
	MediaService mediaService;

	@Mock
	private MediaKeyGenerator mediaKeyGenerator;

	@Mock
	private ModelService modelService;
	@Mock
	private CatalogVersionService catalogVersionService;

	/**
	 * Verify valid media fetched are set correctly on product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyValidMediaFetchedAreSetCorrectlyOnProduct() throws ProductImportException
	{
		final GenericVariantProductModel product = mock(GenericVariantProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final MiraklRawProductModel miraklRawProductModel = mock(MiraklRawProductModel.class);
		final Map<String, String> values = mock(Map.class);
		final MediaModel media = mock(MediaModel.class);
		final AttributeValueData attribute = mockCommonData(coreAttribute);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT))
				.thenReturn(catalogVersion);
		when(data.getRawProduct()).thenReturn(miraklRawProductModel);
		when(miraklRawProductModel.getValues()).thenReturn(values);
		when(values.get("variantAttributes")).thenReturn("color=British Tan");
		when(mediaKeyGenerator.generateFor("color=British Tan")).thenReturn("VVC-british-tan-color_thumbnail");
		when(context.getShopId()).thenReturn("2027");
		when(mediaService.getMedia(catalogVersion, "2027_VVC-british-tan-color_thumbnail")).thenReturn(media);
		when(mediaKeyGenerator.generateFor("British Tan Color")).thenReturn("VVC-british-tan-color_thumbnail");
		when(mediaService.getMedia("2027_VVC-british-tan-color_thumbnail")).thenReturn(media);
		colorSwatchUrlAttributeHandler.setValue(attribute, data, context);
		verify(product).setSwatchColorMedia(media);
	}

	/**
	 * Verify if AttributeValue is null then product has no interaction with setSwatchColorMedia
	 *
	 * @throws ProductImportException
	 */
	@Test
	public void verifyMediaIsnotAddedIfAttributeValueIsNull() throws ProductImportException
	{
		final GenericVariantProductModel product = mock(GenericVariantProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final MiraklRawProductModel miraklRawProductModel = mock(MiraklRawProductModel.class);
		final Map<String, String> values = mock(Map.class);
		final MediaModel media = mock(MediaModel.class);
		final AttributeValueData attribute = mockCommonData(coreAttribute);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT))
				.thenReturn(catalogVersion);
		when(data.getRawProduct()).thenReturn(miraklRawProductModel);
		when(miraklRawProductModel.getValues()).thenReturn(values);
		when(values.get("variantAttributes")).thenReturn("color=British Tan");
		when(mediaKeyGenerator.generateFor("color=British Tan")).thenReturn("");
		when(context.getShopId()).thenReturn("2027");
		when(mediaService.getMedia(catalogVersion, "2027_VVC-british-tan-color_thumbnail")).thenReturn(media);
		when(mediaKeyGenerator.generateFor("British Tan Color")).thenReturn("VVC-british-tan-color_thumbnail");
		when(mediaService.getMedia("2027_VVC-british-tan-color_thumbnail")).thenReturn(media);
		colorSwatchUrlAttributeHandler.setValue(attribute, data, context);
		verify(product, times(0)).setSwatchColorMedia(any(MediaModel.class));
	}


	/**
	 * Mock common data.
	 *
	 * @param coreAttribute
	 *           the core attribute
	 * @return the attribute value data
	 */
	private AttributeValueData mockCommonData(final MiraklCoreAttributeModel coreAttribute)
	{
		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("British Tan Color");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);
		return attribute;
	}
}
