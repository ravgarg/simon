package com.simon.core.services.impl;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.simon.core.model.ShopGatewayModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ShopGatewayServiceTest
{
	@InjectMocks
	private ShopGatewayServiceImpl shopGatewayService;

	@Mock
	private GenericDao<ShopGatewayModel> shopGatewayGenericDao;

	@Test
	public void testShopGatewayExist()
	{

		final Map<String, Object> paramMap = ImmutableMap.of("code", "TWOTAP");

		when(shopGatewayGenericDao.find(paramMap)).thenReturn(singletonList(new ShopGatewayModel()));
		assertNotNull(shopGatewayService.getShopGatewayForCode("TWOTAP"));
	}
}
