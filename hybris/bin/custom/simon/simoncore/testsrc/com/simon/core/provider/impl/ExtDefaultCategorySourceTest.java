package com.simon.core.provider.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import reactor.util.Assert;


@UnitTest
public class ExtDefaultCategorySourceTest
{

	@InjectMocks
	ExtDefaultCategorySource extDefaultCategorySource;

	@Mock
	GenericVariantProductModel genericVariantProduct;

	@Mock
	ProductModel productModel;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getProductModelType_VariantModel()
	{
		when(genericVariantProduct.getBaseProduct()).thenReturn(new ProductModel());
		final Set<ProductModel> baseProductModel = extDefaultCategorySource.getProducts(genericVariantProduct);
		Assert.notEmpty(baseProductModel);
	}

	@Test
	public void getProductModelType_ProductModel()
	{
		final Set<ProductModel> baseProductModel = extDefaultCategorySource.getProducts(productModel);
		Assert.notNull(baseProductModel);
	}

}
