package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.FindDeliveryCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindDiscountValuesStrategy;
import de.hybris.platform.order.strategies.calculation.FindPaymentCostStrategy;
import de.hybris.platform.order.strategies.calculation.FindPriceStrategy;
import de.hybris.platform.order.strategies.calculation.FindTaxValuesStrategy;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.RetailerService;
import com.simon.core.util.ExtDiscountValue;


/**
 * test class for ExtCalculationServiceTest which allows calculation or recalculation of the order. This includes
 * calculation of all the entries, taxes and discount values. Information about price, taxes and discounts are fetched
 * using dedicated strategies : {@link FindDiscountValuesStrategy}, {@link FindTaxValuesStrategy},
 * {@link FindPriceStrategy}. Also payment and delivery costs are resolved using strategies
 * {@link FindDeliveryCostStrategy} and {@link FindPaymentCostStrategy}.
 *
 * Whether order needs to be calculated or not is up to the implementation of {@link OrderRequiresCalculationStrategy}.
 *
 * it also calculate price value
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCalculationServiceTest
{
	@InjectMocks
	private final ExtCalculationService extCalculationService = Mockito.spy(new ExtCalculationService());

	@Mock
	private CommonI18NService extCommonI18NService;
	@Mock
	private RetailerService retailerService;
	@Mock
	private ModelService modelService;

	@Mock
	private OrderRequiresCalculationStrategy requireCalculationStrategy;

	@Mock
	private AbstractOrderEntryModel orderEntryModel;
	@Mock
	private AbstractOrderModel orderModel;
	@Mock
	private CurrencyModel currencyModel;
	@Mock
	private CurrencyModel discountCurrencyModel;
	@Mock
	private CurrencyModel basePriceCurrencyModel;
	@Mock
	private AdditionalCartInfoModel additionalCartInfoModel;
	@Mock
	private RetailersInfoModel retailersInfoModel;

	@Mock
	private TaxValue entryTax;
	@Mock
	private ExtPriceValue extPriceValue;
	@Mock
	private PriceValue priceValue;
	@Mock
	private DiscountValue discountValue;

	@Mock
	private Collection<TaxValue> tax;
	@Mock
	private Map<String, Double> retailersSubtotal;

	/**
	 *
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * test resetAllValues method of ExtCalculationService which extends DefaultCalculationService this method
	 * resetAllValuse when add to cart method is called it set all previous values of entry while calculating the cart
	 *
	 * @throws CalculationException
	 */
	@Test
	public void resetAllValues() throws CalculationException
	{

		final Collection<TaxValue> entryTaxes = new ArrayList<>();
		entryTaxes.add(entryTax);


		Mockito.doReturn(entryTaxes).when(extCalculationService).extFindTaxValues(orderEntryModel);
		Mockito.doReturn(extPriceValue).when(extCalculationService).extFindBasePrice(orderEntryModel);
		Mockito.doReturn(orderModel).when(orderEntryModel).getOrder();
		Mockito.doReturn(Boolean.TRUE).when(orderModel).getNet();
		Mockito.doReturn(currencyModel).when(orderModel).getCurrency();
		Mockito.doReturn(priceValue).when(priceValue).getOtherPrice(entryTaxes);
		Mockito.doReturn(Double.valueOf(21.00)).when(priceValue).getValue();
		Mockito.doReturn("USD").when(currencyModel).getIsocode();
		Mockito.doReturn("USD").when(extPriceValue).getCurrencyIso();
		Mockito.doReturn(true).when(extPriceValue).isNet();
		extCalculationService.setExtCommonI18NService(extCommonI18NService);
		Mockito.doReturn(basePriceCurrencyModel).when(extCommonI18NService).getCurrency("USD");
		final List<DiscountValue> entryDiscounts = new ArrayList<>();
		entryDiscounts.add(discountValue);
		Mockito.doReturn(entryDiscounts).when(extCalculationService).extFindDiscountValues(orderEntryModel);
		extCalculationService.resetAllValues(orderEntryModel);
		Assert.notNull(orderEntryModel);
	}

	/**
	 * test resetAllValues method of ExtCalculationService which extends DefaultCalculationService this method
	 * resetAllValuse when add to cart method is called it set all previous values of entry while calculating the cart
	 *
	 * @throws CalculationException
	 */
	@Test
	public void testResetAllValuesIsoCodeNotEqualCurrencyISO() throws CalculationException
	{

		final Collection<TaxValue> entryTaxes = new ArrayList<>();
		entryTaxes.add(entryTax);
		final double lp = 2L;


		Mockito.doReturn(entryTaxes).when(extCalculationService).extFindTaxValues(orderEntryModel);
		Mockito.doReturn(extPriceValue).when(extCalculationService).extFindBasePrice(orderEntryModel);
		Mockito.doReturn(orderModel).when(orderEntryModel).getOrder();
		Mockito.doReturn(Boolean.TRUE).when(orderModel).getNet();
		Mockito.doReturn(currencyModel).when(orderModel).getCurrency();
		Mockito.doReturn(priceValue).when(priceValue).getOtherPrice(entryTaxes);
		Mockito.doReturn(Double.valueOf(21.00)).when(priceValue).getValue();
		Mockito.doReturn("EUR").when(currencyModel).getIsocode();
		Mockito.doReturn("USD").when(extPriceValue).getCurrencyIso();
		Mockito.doReturn(lp).when(extPriceValue).getListPrice();
		Mockito.doReturn(true).when(extPriceValue).isNet();

		extCalculationService.setExtCommonI18NService(extCommonI18NService);
		Mockito.doReturn(basePriceCurrencyModel).when(extCommonI18NService).getCurrency("USD");
		final List<DiscountValue> entryDiscounts = new ArrayList<>();
		entryDiscounts.add(discountValue);
		Mockito.doReturn(entryDiscounts).when(extCalculationService).extFindDiscountValues(orderEntryModel);
		extCalculationService.resetAllValues(orderEntryModel);
		Assert.notNull(orderEntryModel);
	}

	/**
	 * Method to resetAll Values when currency is null.
	 *
	 * @throws CalculationException
	 */
	@Test
	public void testResetAllValuesCurrencyNull() throws CalculationException
	{

		final Collection<TaxValue> entryTaxes = new ArrayList<>();
		entryTaxes.add(entryTax);
		final double lp = 2L;

		Mockito.doReturn(entryTaxes).when(extCalculationService).extFindTaxValues(orderEntryModel);
		Mockito.doReturn(extPriceValue).when(extCalculationService).extFindBasePrice(orderEntryModel);
		Mockito.doReturn(orderModel).when(orderEntryModel).getOrder();
		Mockito.doReturn(Boolean.TRUE).when(orderModel).getNet();
		Mockito.doReturn(currencyModel).when(orderModel).getCurrency();
		Mockito.doReturn(priceValue).when(priceValue).getOtherPrice(entryTaxes);
		Mockito.doReturn(Double.valueOf(21.00)).when(priceValue).getValue();
		Mockito.doReturn("USD").when(currencyModel).getIsocode();
		Mockito.doReturn(null).when(extPriceValue).getCurrencyIso();
		Mockito.doReturn(lp).when(extPriceValue).getListPrice();
		Mockito.doReturn(true).when(extPriceValue).isNet();
		extCalculationService.setExtCommonI18NService(extCommonI18NService);
		Mockito.doReturn(basePriceCurrencyModel).when(extCommonI18NService).getCurrency("USD");
		final List<DiscountValue> entryDiscounts = new ArrayList<>();
		entryDiscounts.add(discountValue);
		Mockito.doReturn(entryDiscounts).when(extCalculationService).extFindDiscountValues(orderEntryModel);
		extCalculationService.resetAllValues(orderEntryModel);
		Assert.notNull(orderEntryModel);
	}

	/**
	 * Method top test reset all when list price is lesss than one.
	 *
	 * @throws CalculationException
	 */
	@Test
	public void resetAllValuesListPriceLessthanZero() throws CalculationException
	{

		final Collection<TaxValue> entryTaxes = new ArrayList<>();
		entryTaxes.add(entryTax);
		Mockito.doReturn(entryTaxes).when(extCalculationService).extFindTaxValues(orderEntryModel);
		Mockito.doReturn(extPriceValue).when(extCalculationService).extFindBasePrice(orderEntryModel);
		Mockito.doReturn(orderModel).when(orderEntryModel).getOrder();
		Mockito.doReturn(Boolean.TRUE).when(orderModel).getNet();
		Mockito.doReturn(currencyModel).when(orderModel).getCurrency();
		Mockito.doReturn(priceValue).when(priceValue).getOtherPrice(entryTaxes);
		Mockito.doReturn(Double.valueOf(21.00)).when(priceValue).getValue();
		Mockito.doReturn("En").when(currencyModel).getIsocode();
		Mockito.doReturn("USD").when(extPriceValue).getCurrencyIso();

		extCalculationService.setExtCommonI18NService(extCommonI18NService);
		Mockito.doReturn(basePriceCurrencyModel).when(extCommonI18NService).getCurrency("USD");
		final List<DiscountValue> entryDiscounts = new ArrayList<>();
		entryDiscounts.add(discountValue);

		Mockito.doReturn(entryDiscounts).when(extCalculationService).extFindDiscountValues(orderEntryModel);
		extCalculationService.resetAllValues(orderEntryModel);
		Assert.notNull(orderEntryModel);
	}

	@Test
	public void calculateReatailerDiscountValues_retailerDiscountIsNull()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getSubbagDiscountValues()).thenReturn(null);
		assertEquals(0.0, extCalculationService.calculateReatailerDiscountValues(orderModel, false), 0.0);
	}

	@Test
	public void calculateReatailerDiscountValues_retailerDiscountIsEmpty()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getSubbagDiscountValues()).thenReturn(MapUtils.EMPTY_MAP);
		assertEquals(0.0, extCalculationService.calculateReatailerDiscountValues(orderModel, false), 0.0);
	}

	@Test
	public void calculateReatailerDiscountValues_whenCalculationIsNotRequired()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(false);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(currencyModel.getDigits()).thenReturn(2);

		final List<DiscountValue> retailersDiscounts = new ArrayList<>();
		final DiscountValue retaileADiscount = new DiscountValue("retailerADisc", 1.0, true, 1.0, "USD", false);
		final DiscountValue retaileBDiscount = new DiscountValue("retailerADisc", 2.0, true, 2.0, "USD", false);
		retailersDiscounts.add(retaileBDiscount);
		retailersDiscounts.add(retaileADiscount);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", retailersDiscounts);
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 0.0);
		when(orderModel.getRetailersSubtotal()).thenReturn(mapOfSubtotal);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		assertEquals(3.0, extCalculationService.calculateReatailerDiscountValues(orderModel, false), 0.0);
	}

	@Test
	public void calculateDiscountValues_whenCurrencyAreNotSame()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(discountCurrencyModel.getIsocode()).thenReturn("EUR");
		when(currencyModel.getConversion()).thenReturn(2d);
		when(discountCurrencyModel.getConversion()).thenReturn(1d);
		when(currencyModel.getDigits()).thenReturn(2);
		when(extCommonI18NService.getCurrency("EUR")).thenReturn(discountCurrencyModel);
		when(extCommonI18NService.convertAndRoundCurrency(1d, 2d, 2, 2)).thenReturn(30d);
		final List<DiscountValue> retailersDiscounts = new ArrayList<>();
		final DiscountValue retaileADiscount = new DiscountValue("retailerADisc", 1.0, true, 1.0, "EUR", false);
		final DiscountValue retaileBDiscount = new DiscountValue("retailerADisc", 2.0, true, 2.0, "EUR", false);
		retailersDiscounts.add(retaileBDiscount);
		retailersDiscounts.add(retaileADiscount);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", retailersDiscounts);
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 0.0);
		when(orderModel.getRetailersSubtotal()).thenReturn(mapOfSubtotal);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		assertEquals(30.0, extCalculationService.calculateDiscountValues(orderModel, false), 0.0);
	}

	@Test
	public void calculateDiscountValues_whenDiscountIsZero()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(discountCurrencyModel.getIsocode()).thenReturn("EUR");
		when(currencyModel.getConversion()).thenReturn(2d);
		when(discountCurrencyModel.getConversion()).thenReturn(1d);
		when(currencyModel.getDigits()).thenReturn(2);
		when(extCommonI18NService.getCurrency("EUR")).thenReturn(discountCurrencyModel);
		when(extCommonI18NService.convertAndRoundCurrency(1d, 2d, 2, 2)).thenReturn(30d);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 0.0);
		when(orderModel.getRetailersSubtotal()).thenReturn(mapOfSubtotal);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		assertEquals(0.0, extCalculationService.calculateDiscountValues(orderModel, false), 0.0);
		assertEquals(orderModel.getRetailersTotalDiscount().keySet().size(), 0);
	}

	@Test
	public void calculateReatailerDiscountValues_whenCalculationIsRequired()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(extCommonI18NService.roundCurrency(3.0, 2)).thenReturn(3.0);
		when(currencyModel.getIsocode()).thenReturn("USD");

		final List<DiscountValue> retailersDiscounts = new ArrayList<>();
		final DiscountValue retaileADiscount = new DiscountValue("retailerADisc", 1.0, false, 1.0, "USD", true);
		final DiscountValue retaileA2Discount = new DiscountValue("retailerADisc", 2.0, false, 2.0, "USD", true);
		retailersDiscounts.add(retaileA2Discount);
		retailersDiscounts.add(retaileADiscount);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", retailersDiscounts);
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 0.0);
		when(orderModel.getRetailersSubtotal()).thenReturn(mapOfSubtotal);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(currencyModel.getDigits()).thenReturn(2);

		assertEquals(0.3, extCalculationService.calculateReatailerDiscountValues(orderModel, false), 0.01);
	}

	@Test
	public void calculateReatailerDiscountValues_whenRecalculateIsTrue()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(extCommonI18NService.roundCurrency(3.0, 2)).thenReturn(3.0);

		final List<DiscountValue> retailersDiscounts = new ArrayList<>();
		final ExtDiscountValue retaileADiscount = new ExtDiscountValue("retailerADisc", 1.0, true, 1.0, "USD", false, "retailerA");
		final ExtDiscountValue retaileBDiscount = new ExtDiscountValue("retailerADisc", 2.0, true, 2.0, "USD", false, "retailerA");
		retailersDiscounts.add(retaileBDiscount);
		retailersDiscounts.add(retaileADiscount);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", retailersDiscounts);
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 1.0);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(currencyModel.getDigits()).thenReturn(2);

		assertEquals(3.0, extCalculationService.calculateReatailerDiscountValues(orderModel, true), 0.0);
	}

	@Test
	public void calculateReatailerDiscountValues_whenDeliveryANdPaymentIsDiscountable()
	{
		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(extCommonI18NService.roundCurrency(3.0, 2)).thenReturn(3.0);
		final List<DiscountValue> retailersDiscounts = new ArrayList<>();
		final DiscountValue retaileADiscount = new DiscountValue("retailerADisc", 1.0, true, 1.0, "USD", false);
		final DiscountValue retaileA2Discount = new DiscountValue("retailerADisc", 2.0, true, 2.0, "USD", false);
		retailersDiscounts.add(retaileA2Discount);
		retailersDiscounts.add(retaileADiscount);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", retailersDiscounts);
		final Map<String, Double> mapOfSubtotal = new HashMap<>();
		mapOfSubtotal.put("retailerA", 10.0);
		final Map<String, Double> mapOfDeliveryCost = new HashMap<>();
		mapOfDeliveryCost.put("retailerA", 1.0);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(mapOfDeliveryCost);
		when(orderModel.getSubbagDiscountValues()).thenReturn(retailerDiscountMap);
		when(orderModel.isDiscountsIncludeDeliveryCost()).thenReturn(true);
		when(orderModel.getDeliveryCost()).thenReturn(5.0);
		when(orderModel.isDiscountsIncludePaymentCost()).thenReturn(true);
		when(orderModel.getPaymentCost()).thenReturn(5.0);
		when(currencyModel.getDigits()).thenReturn(2);
		when(retailerService.getRetailerSubBagForCart(orderModel)).thenReturn(mapOfSubtotal);
		assertEquals(3.0, extCalculationService.calculateReatailerDiscountValues(orderModel, true), 0.0);
	}

	@Test
	public void testCalculateTotalTaxValues()
	{
		final List<RetailersInfoModel> retailersInfoModels = new ArrayList<>();
		final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<>();
		retailersInfoModels.add(retailersInfoModel);

		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getRetailersSubtotal()).thenReturn(retailersSubtotal);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(additionalCartInfoModel.getRetailersInfo()).thenReturn(retailersInfoModels);
		when(retailersInfoModel.getRetailerId()).thenReturn("retailerId");
		when(retailersSubtotal.get("retailerId")).thenReturn(10.00);
		when(retailersInfoModel.getAppliedTaxRate()).thenReturn(1.00);
		doNothing().when(modelService).saveAll();

		assertEquals(0.1, extCalculationService.calculateTotalTaxValues(orderModel, true, 1, 1, taxValueMap), 0.0);
	}

	@Test
	public void testCalculateTotalTaxValuesWithRecalculatTrueRequiresCalculationFalse()
	{

		final List<RetailersInfoModel> retailersInfoModels = new ArrayList<>();
		final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<>();
		retailersInfoModels.add(retailersInfoModel);

		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getRetailersSubtotal()).thenReturn(retailersSubtotal);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(additionalCartInfoModel.getRetailersInfo()).thenReturn(retailersInfoModels);
		when(retailersInfoModel.getRetailerId()).thenReturn("retailerId");
		when(retailersSubtotal.get("retailerId")).thenReturn(10.00);
		when(retailersInfoModel.getAppliedTaxRate()).thenReturn(1.00);
		doNothing().when(modelService).saveAll();

		assertEquals(0.1, extCalculationService.calculateTotalTaxValues(orderModel, true, 1, 1, taxValueMap), 0.0);
	}

	@Test
	public void testCalculateTotalTaxValuesWithRecalculatFalseRequiresCalculationTrue()
	{

		final List<RetailersInfoModel> retailersInfoModels = new ArrayList<>();
		final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<>();
		retailersInfoModels.add(retailersInfoModel);

		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(true);
		when(orderModel.getRetailersSubtotal()).thenReturn(retailersSubtotal);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfoModel);
		when(additionalCartInfoModel.getRetailersInfo()).thenReturn(retailersInfoModels);
		when(retailersInfoModel.getRetailerId()).thenReturn("retailerId");
		when(retailersSubtotal.get("retailerId")).thenReturn(10.00);
		when(retailersInfoModel.getAppliedTaxRate()).thenReturn(1.00);
		doNothing().when(modelService).saveAll();

		assertEquals(0.1, extCalculationService.calculateTotalTaxValues(orderModel, false, 1, 1, taxValueMap), 0.0);
	}

	@Test
	public void testCalculateTotalTaxValuesWithRecalculatFalseRequiresCalculationFalse()
	{
		final Map<TaxValue, Map<Set<TaxValue>, Double>> taxValueMap = new HashMap<>();
		final Map<String, Double> retailersSalesTax = new HashMap<>();
		retailersSalesTax.put("retailerId", 1.00);

		when(requireCalculationStrategy.requiresCalculation(orderModel)).thenReturn(false);
		when(orderModel.getRetailersTotalTax()).thenReturn(retailersSalesTax);

		assertEquals(0.0, extCalculationService.calculateTotalTaxValues(orderModel, false, 1, 1, taxValueMap), 0.0);
	}
}
