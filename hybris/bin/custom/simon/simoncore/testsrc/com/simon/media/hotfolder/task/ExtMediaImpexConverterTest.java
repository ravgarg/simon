package com.simon.media.hotfolder.task;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.EmployeeModel;
import de.hybris.platform.mediaconversion.conversion.DefaultMediaConversionService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.media.MediaContainerService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants.Catalog;



/**
 * Class for converting a CSV file into impex and remove old media from mediaContainer
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMediaImpexConverterTest
{
	@InjectMocks
	@Spy
	ExtMediaImpexConverter simonMediaImpexConverter = new ExtMediaImpexConverter();
	@Mock
	private DefaultMediaConversionService defaultMediaConversionService;
	@Mock
	private MediaContainerService mediaContainerService;
	@Mock
	private ModelService modelService;
	@Mock
	private MediaContainerModel mediaContainerModel;
	@Mock
	private MediaModel mediaModel;
	@Mock
	private CatalogVersionModel catalogVersion;
	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private ProductService productService;

	@Mock
	private UserService userService;

	@Mock
	private SessionService sessionService;

	final List<MediaContainerModel> mediaContainers = new ArrayList<>();

	@Mock
	private EmployeeModel userModel;

	@Mock
	private ProductModel product;

	Collection<MediaModel> mediaList;

	private static final String EMPTY_STRING = "";
	private static final String HASH_STRING = "#";
	private static final String CODE = "101101";

	/**
	 *
	 */
	@Before
	public void setUp()
	{
		mediaList = new ArrayList<>();
		mediaList.add(mediaModel);
		mediaContainers.add(mediaContainerModel);
		doReturn(catalogVersion).when(mediaContainerModel).getCatalogVersion();
		doReturn(catalogVersion).when(catalogVersionService).getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED);
		when(productService.getProductForCode(catalogVersion, CODE)).thenReturn(product);
		when(userService.getAdminUser()).thenReturn(userModel);

	}

	/**
	 * Test convert method when row is empty in CSV file
	 */
	@Test
	public void convertWhenEmptyRow()
	{
		final Map<Integer, String> row = null;
		Assert.assertTrue(EMPTY_STRING.equals(simonMediaImpexConverter.convert(row, 111L)));
	}


	/**
	 * Test when mediaContainerService throw UnknownIdentifierException
	 */
	@Test
	public void convertThrowUnknownIdentifierException()
	{
		final Map<Integer, String> row = new HashMap<>();
		row.put(0, CODE);
		when(mediaContainerService.getMediaContainerForQualifier(CODE))
				.thenThrow(new UnknownIdentifierException("throw a new UnknownIdentifierException"));
		simonMediaImpexConverter.setImpexRow(HASH_STRING);
		Assert.assertTrue(HASH_STRING.equals(simonMediaImpexConverter.convert(row, 111L)));
	}

	/**
	 * Test when media Container is null
	 */
	@Test
	public void convertWhenMediaContainerNull()
	{
		final Map<Integer, String> row = new HashMap<>();
		row.put(0, CODE);
		simonMediaImpexConverter.setImpexRow(HASH_STRING);
		when(product.getGalleryImages()).thenReturn(null);
		Assert.assertTrue(HASH_STRING.equals(simonMediaImpexConverter.convert(row, 111L)));

	}


	/**
	 * Test when defaultMediaConversionService ConvertedMedias is empty collection
	 */
	@Test
	public void convertWhenMediaModelCollectionNull()
	{
		final Map<Integer, String> row = new HashMap<>();
		row.put(0, CODE);
		simonMediaImpexConverter.setImpexRow(HASH_STRING);
		when(product.getGalleryImages()).thenReturn(mediaContainers);
		when(mediaContainerModel.getMedias()).thenReturn(null);
		Assert.assertTrue(HASH_STRING.equals(simonMediaImpexConverter.convert(row, 111L)));
	}

	/**
	 * Method for converting a CSV file into impex and remove old media from mediaContainer
	 */
	@Test
	public void convert()
	{
		final Map<Integer, String> row = new HashMap<>();
		row.put(0, CODE);
		simonMediaImpexConverter.setImpexRow(HASH_STRING);
		when(mediaContainerService.getMediaContainerForQualifier(CODE)).thenReturn(mediaContainerModel);
		when(defaultMediaConversionService.getConvertedMedias(mediaContainerModel)).thenReturn(Collections.singleton(mediaModel));
		when(product.getGalleryImages()).thenReturn(mediaContainers);
		when(mediaContainerModel.getMedias()).thenReturn(mediaList);
		Assert.assertTrue(HASH_STRING.equals(simonMediaImpexConverter.convert(row, 111L)));
	}

}
