package com.simon.core.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableList;


/**
 * Unit test case for SimonCategoryNavigationDaoImplTest
 */
@UnitTest
public class SimonCategoryNavigationDaoImplTest
{


	@InjectMocks
	SimonCategoryNavigationDaoImpl simonCategoryNavigationDaoImpl;

	@Mock
	FlexibleSearchService flexibleSearchService;

	@Mock
	private CategoryModel categoryModel;





	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}

	/**
	 * This is used to test sub categories of level 2 categories which contains product.
	 */
	@Test
	public void testGetSubCategoriesWithProducts()
	{

		final String categoryId = "m100101";

		when(flexibleSearchService.search(any(FlexibleSearchQuery.class)))
				.thenReturn(new SearchResultImpl<>(ImmutableList.<Object> of(categoryModel), 1, 0, 0));

		final List<CategoryModel> result = simonCategoryNavigationDaoImpl.getSubCategoriesWithProducts(categoryId);

		assertNotNull(result);
	}
}
