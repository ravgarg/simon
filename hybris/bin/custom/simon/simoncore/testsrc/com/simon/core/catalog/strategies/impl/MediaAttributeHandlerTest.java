package com.simon.core.catalog.strategies.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.constants.SimonCoreConstants.Catalog;


/**
 * The Class MediaAttributeHandlerTest. Unit test for {@link MediaAttributeHandler}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaAttributeHandlerTest
{

	@InjectMocks
	private MediaAttributeHandler mediaAttributeHandler;

	@Mock
	private CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	@Mock
	private ModelService modelService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private CatalogVersionModel catalogVersion;


	/**
	 * Verify no interaction for null media url.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoInteractionForNullMediaUrl() throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);
		when(coreAttribute.getTypeParameter()).thenReturn("rawPicture");

		mediaAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify no interaction for null type parameter.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoInteractionForNullTypeParameter() throws ProductImportException
	{
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("https://www.spo.com/images/logo.jpg");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		mediaAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify existing media if present is updated with new url.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyExistingMediaIfPresentIsUpdatedWithNewUrl() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final String attributeQualifier = "rawPicture";
		when(coreAttribute.getTypeParameter()).thenReturn(attributeQualifier);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		final String mediaUrl = "https://www.spo.com/images/logo.jpg";
		when(attribute.getValue()).thenReturn(mediaUrl);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		final MediaModel existingMedia = mock(MediaModel.class);
		when(modelService.getAttributeValue(product, attributeQualifier)).thenReturn(existingMedia);

		mediaAttributeHandler.setValue(attribute, data, context);

		verify(existingMedia).setURL(mediaUrl);
	}


	/**
	 * Verify if existing media not present new media is created with new url.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyIfExistingMediaNotPresentNewMediaIsCreatedWithNewUrl() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		when(product.getCode()).thenReturn("786664457021");

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final String attributeQualifier = "rawPicture";
		when(coreAttribute.getTypeParameter()).thenReturn(attributeQualifier);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final ProductImportGlobalContextData globalContext = mock(ProductImportGlobalContextData.class);
		when(globalContext.getProductCatalogVersion()).thenReturn(PK.BIG_PK);
		when(context.getGlobalContext()).thenReturn(globalContext);

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);
		when(modelService.get(any(PK.class))).thenReturn(productCatalogVersion);

		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		final String mediaUrl = "https://www.spo.com/images/logo.jpg";
		when(attribute.getValue()).thenReturn(mediaUrl);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		final MediaModel newMedia = mock(MediaModel.class);
		when(modelService.create(MediaModel.class)).thenReturn(newMedia);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT))
				.thenReturn(catalogVersion);
		mediaAttributeHandler.setValue(attribute, data, context);

		newMedia.setCatalogVersion(productCatalogVersion);
		newMedia.setURL(mediaUrl);
		verify(modelService).setAttributeValue(product, attributeQualifier, newMedia);
	}
}
