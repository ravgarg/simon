package com.simon.core.product.strategies.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * ExtProductCreationStrategyTest unit test for {@link ExtProductCreationStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtProductCreationStrategyTest
{

	@InjectMocks
	@Spy
	private ExtProductCreationStrategy extProductCreationStrategy;

	/**
	 * Verify shop model fetched is set correctly.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyShopModelFetchedIsSetCorrectlyOnVariantAndBase() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final ShopModel shopModel = mock(ShopModel.class);
		when(data.getShop()).thenReturn(shopModel);

		final MiraklRawProductModel miraklRawProduct = mock(MiraklRawProductModel.class);
		when(data.getRawProduct()).thenReturn(miraklRawProduct);

		final ProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		when(((VariantProductModel) variantProduct).getBaseProduct()).thenReturn(baseProduct);

		doReturn(variantProduct).when(extProductCreationStrategy).createDefaultProduct(data, context);

		extProductCreationStrategy.createProduct(data, context);
		verify(variantProduct).setShop(shopModel);
		verify(baseProduct).setShop(shopModel);
	}

	/**
	 * Verify shop model fetched is set correctly.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyApprovalStatusIsSetCorrectlyOnVariantAndBase() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final ProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		final MiraklRawProductModel miraklRawProduct = mock(MiraklRawProductModel.class);
		when(data.getRawProduct()).thenReturn(miraklRawProduct);
		when(((VariantProductModel) variantProduct).getBaseProduct()).thenReturn(baseProduct);
		doReturn(variantProduct).when(extProductCreationStrategy).createDefaultProduct(data, context);
		extProductCreationStrategy.createProduct(data, context);
		verify(variantProduct).setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
		verify(baseProduct).setApprovalStatus(ArticleApprovalStatus.UNAPPROVED);
	}

	/**
	 * Verify retailer sku id set correctly on variant and base.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyRetailerSkuIdSetCorrectlyOnVariantAndBase() throws ProductImportException
	{
		final MiraklRawProductModel rawProduct = mock(MiraklRawProductModel.class);

		final String sku = "786664457021";
		final String variantGroupCode = "113903-0400A";
		when(rawProduct.getSku()).thenReturn(sku);
		when(rawProduct.getVariantGroupCode()).thenReturn(variantGroupCode);

		final ProductImportData data = mock(ProductImportData.class);
		when(data.getRawProduct()).thenReturn(rawProduct);

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(context.getShopId()).thenReturn("2018");

		final ProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		when(((VariantProductModel) variantProduct).getBaseProduct()).thenReturn(baseProduct);

		doReturn(variantProduct).when(extProductCreationStrategy).createDefaultProduct(data, context);

		extProductCreationStrategy.createProduct(data, context);

		verify(variantProduct).setRetailerSkuID(sku);
		verify(baseProduct).setRetailerSkuID(variantGroupCode);
	}
}
