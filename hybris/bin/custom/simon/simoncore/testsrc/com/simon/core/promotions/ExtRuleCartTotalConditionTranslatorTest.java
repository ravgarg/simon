package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruledefinitions.AmountOperator;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@UnitTest
public class ExtRuleCartTotalConditionTranslatorTest
{
	@InjectMocks
	ExtRuleCartTotalConditionTranslator translator;
	@Mock
	private RuleCompilerContext context;
	@Mock
	private RuleConditionData condition;
	@Mock
	private RuleConditionDefinitionData conditionDefinition;
	protected static final String OPERATOR_PARAM = "operator";
	protected static final String VALUE_PARAM = "value";
	private static final String RETAILER_PARAM = "retailer";

	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test
	public void testTranslate_whenConditionParameterAreEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(OPERATOR_PARAM, operatorParam);
		final RuleParameterData valueParam = new RuleParameterData();
		conditionParams.put(VALUE_PARAM, valueParam);
		final RuleParameterData retailerParam = new RuleParameterData();
		conditionParams.put(RETAILER_PARAM, retailerParam);
		assertEquals(IrConditions.newIrRuleFalseCondition().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenConditionParameterHaveValues_ButValuesAreEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(OPERATOR_PARAM, operatorParam);
		final RuleParameterData valueParam = new RuleParameterData();
		conditionParams.put(VALUE_PARAM, valueParam);
		final RuleParameterData retailerParam = new RuleParameterData();
		conditionParams.put(RETAILER_PARAM, retailerParam);
		final AmountOperator operator = AmountOperator.GREATER_THAN;
		operatorParam.setValue(operator);
		final Map<String, BigDecimal> value = new HashMap<>();
		valueParam.setValue(value);
		final String shop = "retailer1";
		retailerParam.setValue(shop);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(IrConditions.newIrRuleFalseCondition().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenVerifyAllPresentValueTrue()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(OPERATOR_PARAM, operatorParam);
		final RuleParameterData valueParam = new RuleParameterData();
		conditionParams.put(VALUE_PARAM, valueParam);
		final RuleParameterData retailerParam = new RuleParameterData();
		conditionParams.put(RETAILER_PARAM, retailerParam);
		final AmountOperator operator = AmountOperator.GREATER_THAN;
		operatorParam.setValue(operator);
		final Map<String, BigDecimal> value = new HashMap<>();
		value.put("USD", BigDecimal.valueOf(10d));
		valueParam.setValue(value);
		final String shop = "retailer1";
		retailerParam.setValue(shop);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND).build().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenConditionParameterHaveValues_andValuesAreNotEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(OPERATOR_PARAM, operatorParam);
		final RuleParameterData valueParam = new RuleParameterData();
		conditionParams.put(VALUE_PARAM, valueParam);
		final RuleParameterData retailerParam = new RuleParameterData();
		conditionParams.put(RETAILER_PARAM, retailerParam);
		final AmountOperator operator = AmountOperator.GREATER_THAN;
		operatorParam.setValue(operator);
		final Map<String, BigDecimal> value = new HashMap<>();
		value.put("USD", null);
		valueParam.setValue(value);
		final String shop = "retailer1";
		retailerParam.setValue(shop);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND).build().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}
}
