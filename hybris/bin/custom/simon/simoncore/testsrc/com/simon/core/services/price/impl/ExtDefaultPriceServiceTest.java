package com.simon.core.services.price.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Map;

import org.junit.Test;

import com.simon.core.price.DetailedPriceInfo;


/**
 * This class provides extra information related to price like where the price need to be stroked off and is what is the
 * percent saving
 */
@UnitTest
public class ExtDefaultPriceServiceTest
{

	ExtDefaultPriceService extDefaultPriceService = new ExtDefaultPriceService();

	@Test
	public void getDetailedPriceInformationOnlyLp()
	{
		final Map<DetailedPriceInfo, Object> map = extDefaultPriceService.getDetailedPriceInformation(0.0, 20.0, 0.0);
		final boolean lpStrikeOff = (boolean) map.get(DetailedPriceInfo.IS_LP_STRIKEOFF);
		final boolean msrpSrikeOff = (boolean) map.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF);

		assertFalse(lpStrikeOff);
		assertFalse(msrpSrikeOff);
		assertNull(map.get(DetailedPriceInfo.PERCENTAGE_SAVING));
	}

	@Test
	public void getDetailedPriceInformationLpMsrp()
	{
		final Map<DetailedPriceInfo, Object> map = extDefaultPriceService.getDetailedPriceInformation(100.0, 80.0, 0.0);
		final boolean lpStrikeOff = (boolean) map.get(DetailedPriceInfo.IS_LP_STRIKEOFF);
		final boolean msrpSrikeOff = (boolean) map.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF);

		assertFalse(lpStrikeOff);
		assertTrue(msrpSrikeOff);
		assertNotNull(map.get(DetailedPriceInfo.PERCENTAGE_SAVING));
		final int percent = (int) map.get(DetailedPriceInfo.PERCENTAGE_SAVING);
		assertEquals(percent, 20);
	}

	@Test
	public void getDetailedPriceInformationLpMsrpSp()
	{
		final Map<DetailedPriceInfo, Object> map = extDefaultPriceService.getDetailedPriceInformation(100.0, 80.0, 70.0);
		final boolean lpStrikeOff = (boolean) map.get(DetailedPriceInfo.IS_LP_STRIKEOFF);
		final boolean msrpSrikeOff = (boolean) map.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF);

		assertFalse(lpStrikeOff);
		assertTrue(msrpSrikeOff);
		assertNotNull(map.get(DetailedPriceInfo.PERCENTAGE_SAVING));
		final int percent = (int) map.get(DetailedPriceInfo.PERCENTAGE_SAVING);
		assertEquals(percent, 30);
	}

	@Test
	public void getDetailedPriceInformationLpSp()
	{
		final Map<DetailedPriceInfo, Object> map = extDefaultPriceService.getDetailedPriceInformation(0.0, 100.0, 80.0);
		final boolean lpStrikeOff = (boolean) map.get(DetailedPriceInfo.IS_LP_STRIKEOFF);
		final boolean msrpSrikeOff = (boolean) map.get(DetailedPriceInfo.IS_MSRP_STRIKEOFF);

		assertTrue(lpStrikeOff);
		assertFalse(msrpSrikeOff);
		assertNotNull(map.get(DetailedPriceInfo.PERCENTAGE_SAVING));
		final int percent = (int) map.get(DetailedPriceInfo.PERCENTAGE_SAVING);
		assertEquals(percent, 20);
	}


}
