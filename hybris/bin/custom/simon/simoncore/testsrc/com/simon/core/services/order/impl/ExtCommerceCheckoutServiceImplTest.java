package com.simon.core.services.order.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;
import com.simon.integration.dto.cart.cartestimate.CartEstimateResponseDTO;
import com.simon.integration.dto.cart.cartestimate.EstimatedPriceDTO;
import com.simon.integration.dto.cart.cartestimate.RetailerResponseDTO;
import com.simon.integration.services.CartCheckOutIntegrationService;


/**
 * Unit test class for {@link ExtCommerceCheckoutServiceImpl}
 */
@UnitTest
public class ExtCommerceCheckoutServiceImplTest
{
	@InjectMocks
	private ExtCommerceCheckoutServiceImpl extCommerceCheckoutServiceImpl;

	@Mock
	private CartCheckOutIntegrationService cartCheckOutIntegrationService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private ModelService modelService;

	@Mock
	private DefaultRetailerSubbagDeliveryModeStrategy retailerSubbagDeliveryModeStrategy;
	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Mock
	private CartModel cartModel;

	private RetailersInfoModel retailersInfoModel;
	private List<RetailersInfoModel> retailersInfoModelList;
	private ShippingDetailsModel shippingDetailsModel;
	private List<ShippingDetailsModel> shippingDetailsModelList;

	private CartEstimateResponseDTO cartEstimateResponseDTO;
	private RetailerResponseDTO retailerResponseDTO;
	private EstimatedPriceDTO estimatedPriceDTO;
	private List<RetailerResponseDTO> retailerResponseDtoList;

	/**
	 * This method will prepare for the pre-requisites required for the tests
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Unit test for invokeCartEstimateMockRequest method
	 */
	@Test
	public void testInvokeCartEstimateMockRequest()
	{
		setupShippingDetailsList();
		setupRetailersInfoList();

		estimatedPriceDTO = new EstimatedPriceDTO();
		estimatedPriceDTO.setTotalSalesTax("$5.00");
		estimatedPriceDTO.setTotalShippingPrice("$10.00");
		estimatedPriceDTO.setSubtotal("$20.00");

		retailerResponseDTO = new RetailerResponseDTO();
		retailerResponseDTO.setRetailerID("Retailer1");
		retailerResponseDTO.setRetailerPriceDTO(estimatedPriceDTO);

		retailerResponseDtoList = new ArrayList<>();
		retailerResponseDtoList.add(retailerResponseDTO);

		cartEstimateResponseDTO = new CartEstimateResponseDTO();
		cartEstimateResponseDTO.setRetailers(retailerResponseDtoList);

		doNothing().when(getModelService()).saveAll();

		extCommerceCheckoutServiceImpl.populateShippingAndTax(retailersInfoModelList, cartEstimateResponseDTO);

		assertEquals(shippingDetailsModel.getShippingEstimate(), Double.valueOf("10.00"));
		assertEquals(retailersInfoModel.getAppliedSalesTax(), Double.valueOf("5.00"));
	}

	/**
	 *
	 */
	@Test
	public void testSetDefaultDeliveryModePerRetailerForCheapest()
	{
		setupShippingDetailsList();
		setupRetailersInfoList();

		extCommerceCheckoutServiceImpl.setDefaultDeliveryModePerRetailer("Cheapest", retailersInfoModelList, cartModel);
		assertEquals(true, shippingDetailsModel.isSelectedForCart());
	}

	/**
	 *
	 */
	@Test
	public void testSetDefaultDeliveryModePerRetailerForFastest()
	{
		setupShippingDetailsList();
		setupRetailersInfoList();

		extCommerceCheckoutServiceImpl.setDefaultDeliveryModePerRetailer("Fastest", retailersInfoModelList, cartModel);
		assertEquals(false, shippingDetailsModel.isSelectedForCart());
	}

	/**
	 *
	 */
	@Test
	public void testSelectShippingForEstimateCall()
	{
		setupShippingDetailsList();
		setupRetailersInfoList();

		extCommerceCheckoutServiceImpl.selectShippingForEstimateCall("Cheapest", retailersInfoModelList);
		assertEquals(true, shippingDetailsModel.isSelected());
	}

	/**
	 *
	 */
	@Test
	public void testSelectShippingForEstimateCallForFastest()
	{
		setupShippingDetailsList();
		setupRetailersInfoList();

		extCommerceCheckoutServiceImpl.selectShippingForEstimateCall("Fastest", retailersInfoModelList);
		assertEquals(false, shippingDetailsModel.isSelected());
	}

	private void setupShippingDetailsList()
	{
		shippingDetailsModel = new ShippingDetailsModel();
		shippingDetailsModel.setSelected(Boolean.TRUE);
		shippingDetailsModel.setCode("Cheapest");

		shippingDetailsModelList = new ArrayList<>();
		shippingDetailsModelList.add(shippingDetailsModel);
	}

	private void setupRetailersInfoList()
	{
		retailersInfoModel = new RetailersInfoModel();
		retailersInfoModel.setRetailerId("Retailer1");
		retailersInfoModel.setShippingDetails(shippingDetailsModelList);

		retailersInfoModelList = new ArrayList<>();
		retailersInfoModelList.add(retailersInfoModel);
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	public CommerceCartCalculationStrategy getCommerceCartCalculationStrategy()
	{
		return commerceCartCalculationStrategy;
	}

}
