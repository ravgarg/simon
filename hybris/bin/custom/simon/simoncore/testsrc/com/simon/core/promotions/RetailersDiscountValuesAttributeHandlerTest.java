package com.simon.core.promotions;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@UnitTest
public class RetailersDiscountValuesAttributeHandlerTest
{
	@InjectMocks
	RetailersDiscountValuesAttributeHandler handler;
	@Mock
	CartModel cart;

	private final String INTERNAL_DISCOUNT = "[<DV<promoCode#10.0#false#10.0#USD#false>VD>]";
	private final String INTERNAL_DISCOUNT_AS_TARGET_TRUE = "[<DV<promoCode#10.0#false#10.0#USD#true>VD>]";



	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test
	public void getTest()
	{
		final Map<String, String> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailer", INTERNAL_DISCOUNT);
		when(cart.getSubbagDiscountValuesInternal()).thenReturn(retailerDiscountMap);
		handler.get(cart);
		verify(cart).getSubbagDiscountValuesInternal();
	}

	@Test
	public void getTest_whenListIsNull()
	{
		when(cart.getSubbagDiscountValuesInternal()).thenReturn(null);
		handler.get(cart);
		verify(cart).getSubbagDiscountValuesInternal();
	}

	@Test
	public void setTest_whenListIsEmpty()
	{
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		final List<DiscountValue> discountValues = new ArrayList<>();
		retailerDiscountMap.put("retailer", discountValues);
		final Map<String, String> retailerDiscountMapInternal = new HashMap<>();
		retailerDiscountMapInternal.put("retailer", "[]");
		handler.set(cart, retailerDiscountMap);
		verify(cart).setSubbagDiscountValuesInternal(retailerDiscountMapInternal);
	}

	@Test
	public void setTest_whenListIsNull()
	{
		handler.set(cart, null);
		verify(cart).setSubbagDiscountValuesInternal(new HashMap<>());
	}

	@Test
	public void setTest_whenListHasValue_andAsTargetPriceFalse()
	{
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		final List<DiscountValue> discountValues = new ArrayList();
		final DiscountValue discountValue = new DiscountValue("promoCode", 10.0, false, 10.0, "USD", false);
		discountValues.add(discountValue);
		retailerDiscountMap.put("retailer", discountValues);
		final Map<String, String> retailerDiscountMapInternal = new HashMap<>();
		retailerDiscountMapInternal.put("retailer", INTERNAL_DISCOUNT);
		handler.set(cart, retailerDiscountMap);
		verify(cart).setSubbagDiscountValuesInternal(retailerDiscountMapInternal);
	}

	@Test
	public void setTest_whenListHas_andAsTargetPriceTrue()
	{
		final List<DiscountValue> discountValues = new ArrayList();
		final DiscountValue discountValue = new DiscountValue("promoCode", 10.0, false, 10.0, "USD", true);
		discountValues.add(discountValue);
		final Map<String, List<DiscountValue>> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailer", discountValues);
		final Map<String, String> retailerDiscountMapInternal = new HashMap<>();
		retailerDiscountMapInternal.put("retailer", INTERNAL_DISCOUNT_AS_TARGET_TRUE);
		handler.set(cart, retailerDiscountMap);
		verify(cart).setSubbagDiscountValuesInternal(retailerDiscountMapInternal);
	}



}
