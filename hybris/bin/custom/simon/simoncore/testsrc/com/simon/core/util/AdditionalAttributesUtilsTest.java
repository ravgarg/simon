package com.simon.core.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.model.MiraklCategoryModel;


/**
 * The Class AdditionalAttributesUtilsTest.
 */
@UnitTest
public class AdditionalAttributesUtilsTest
{

	/**
	 * Verify empty set is returned if no categories matched.
	 */
	@Test
	public void verifyEmptySetIsReturnedIfNoCategoriesMatched()
	{
		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(genericVariantProduct.getBaseProduct()).thenReturn(mock(ProductModel.class));

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("gender", "mens");
		productAdditionalAttributes.put("age-group", "adult");
		productAdditionalAttributes.put("size-type", "Regular");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category1AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("gender", Arrays.asList("mens", "unisex"));
		category2AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category2AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		category2AdditionalAttributes.put("shoe-type", Arrays.asList("Chukkas", "Boots"));
		when(merchandizingCategory2.getAdditionalAttributes()).thenReturn(category2AdditionalAttributes);

		when(miraklCategory.getMerchandizingCategories()).thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2));

		final Set<MerchandizingCategoryModel> actual = AdditionalAttributesUtils
				.getMatchingMerchandizingCategories(genericVariantProduct);
		assertTrue(actual.isEmpty());
	}

	/**
	 * Verify single matched merchandizing category is returned correctly.
	 */
	@Test
	public void verifySingleMatchedMerchandizingCategoryIsReturnedCorrectly()
	{
		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(genericVariantProduct.getBaseProduct()).thenReturn(mock(ProductModel.class));

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("gender", "mens");
		productAdditionalAttributes.put("age-group", "adult");
		productAdditionalAttributes.put("size-type", "Regular");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category1AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category1AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("gender", Arrays.asList("mens", "unisex"));
		category2AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category2AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		category2AdditionalAttributes.put("shoe-type", Arrays.asList("Chukkas", "Boots"));
		when(merchandizingCategory2.getAdditionalAttributes()).thenReturn(category2AdditionalAttributes);

		when(miraklCategory.getMerchandizingCategories()).thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2));

		final Set<MerchandizingCategoryModel> actual = AdditionalAttributesUtils
				.getMatchingMerchandizingCategories(genericVariantProduct);

		assertThat(actual.size(), is(1));
		assertTrue(actual.contains(merchandizingCategory1));
	}

	/**
	 * Verify more than one matched category is correctly associated with base product.
	 */
	@Test
	public void verifyMoreThanOneMatchedCategoryIsCorrectlyAssociatedWithBaseProduct()
	{
		final GenericVariantProductModel genericVariantProduct = mock(GenericVariantProductModel.class);
		when(genericVariantProduct.getBaseProduct()).thenReturn(mock(ProductModel.class));

		final MiraklCategoryModel miraklCategory = mock(MiraklCategoryModel.class);
		when(genericVariantProduct.getMiraklCategory()).thenReturn(miraklCategory);

		final Map<String, String> productAdditionalAttributes = new HashMap<>();
		productAdditionalAttributes.put("gender", "mens");
		productAdditionalAttributes.put("age-group", "adult");
		productAdditionalAttributes.put("size-type", "Regular");
		when(genericVariantProduct.getAdditionalAttributes()).thenReturn(productAdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory1 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category1AdditionalAttributes = new HashMap<>();
		category1AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category1AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category1AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		when(merchandizingCategory1.getAdditionalAttributes()).thenReturn(category1AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory2 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category2AdditionalAttributes = new HashMap<>();
		category2AdditionalAttributes.put("gender", Arrays.asList("mens", "unisex"));
		category2AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category2AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		category2AdditionalAttributes.put("shoe-type", Arrays.asList("Chukkas", "Boots"));
		when(merchandizingCategory2.getAdditionalAttributes()).thenReturn(category2AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory3 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category3AdditionalAttributes = new HashMap<>();
		category3AdditionalAttributes.put("gender", Arrays.asList("mens", "unisex"));
		category3AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category3AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		when(merchandizingCategory3.getAdditionalAttributes()).thenReturn(category3AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory4 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category4AdditionalAttributes = new HashMap<>();
		category4AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category4AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		when(merchandizingCategory4.getAdditionalAttributes()).thenReturn(category4AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory5 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category5AdditionalAttributes = new HashMap<>();
		category5AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category5AdditionalAttributes.put("age-group", Arrays.asList("adult"));
		category5AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		category5AdditionalAttributes.put("shoe-type", Arrays.asList("Sandals"));
		when(merchandizingCategory5.getAdditionalAttributes()).thenReturn(category5AdditionalAttributes);

		final MerchandizingCategoryModel merchandizingCategory6 = mock(MerchandizingCategoryModel.class);
		final Map<String, List<String>> category6AdditionalAttributes = new HashMap<>();
		category6AdditionalAttributes.put("gender", Arrays.asList("mens"));
		category6AdditionalAttributes.put("age-group", Arrays.asList("kids"));
		category6AdditionalAttributes.put("size-type", Arrays.asList("Regular"));
		when(merchandizingCategory6.getAdditionalAttributes()).thenReturn(category6AdditionalAttributes);


		when(miraklCategory.getMerchandizingCategories()).thenReturn(Arrays.asList(merchandizingCategory1, merchandizingCategory2,
				merchandizingCategory3, merchandizingCategory4, merchandizingCategory5, merchandizingCategory6));

		final Set<MerchandizingCategoryModel> actual = AdditionalAttributesUtils
				.getMatchingMerchandizingCategories(genericVariantProduct);

		assertThat(actual.size(), is(2));

		assertTrue(actual.contains(merchandizingCategory1));
		assertTrue(actual.contains(merchandizingCategory3));
	}
}
