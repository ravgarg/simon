package com.simon.core.resolver;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.model.MerchandizingCategoryModel;


@UnitTest
public class ExtCategoryModelUrlResolverTest
{
	@InjectMocks
	@Spy
	private ExtCategoryModelUrlResolver extCategoryModelUrlResolver;
	@Mock
	private MerchandizingCategoryModel lastCategoryModel;
	@Mock
	private MerchandizingCategoryModel level2CategoryModel;
	@Mock
	private MerchandizingCategoryModel level1CategoryModel;
	@Mock
	private MerchandizingCategoryModel level0CategoryModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		extCategoryModelUrlResolver.homeCategoryCode = "Gender";
		when(lastCategoryModel.getCode()).thenReturn("Pants");
		when(lastCategoryModel.getName()).thenReturn("Pants");
		when(level2CategoryModel.getCode()).thenReturn("Clothing");
		when(level2CategoryModel.getName()).thenReturn("Clothing");
		when(level1CategoryModel.getCode()).thenReturn("Men");
		when(level1CategoryModel.getName()).thenReturn("Men");
		when(level0CategoryModel.getCode()).thenReturn("Gender");
		when(level0CategoryModel.getName()).thenReturn("Gender");
		Mockito.doReturn("/Pants").when(extCategoryModelUrlResolver).resolveURL(level0CategoryModel);
		Mockito.doReturn(new ArrayList<CategoryModel>()
		{
			{
				add(level0CategoryModel);
				add(level1CategoryModel);
				add(level2CategoryModel);
				add(lastCategoryModel);
			}
		}).when(extCategoryModelUrlResolver).getCategoryPathFromCategory(level0CategoryModel);
	}

	@Test
	public void testWhenCategoryHierarchyIsPresent()
	{
		final String finalURL = "/gender_men_clothing_pants";
		final String catCodeHierarchy = "gender_men_clothing_pants";
		assertEquals(extCategoryModelUrlResolver.resolveCategoryPath(level0CategoryModel, catCodeHierarchy), finalURL);
	}

	@Test
	public void testWhenCategoryHierarchyIsNotPresent()
	{
		final String finalURL = "/men_clothing_pants";
		assertEquals(extCategoryModelUrlResolver.resolveCategoryPath(level0CategoryModel, ""), finalURL);
	}
}
