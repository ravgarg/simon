package com.simon.core.price;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.security.JaloSecurityException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * compare price row based on PK, Minqtd, MatchValue, currency,net, start date and pricetype.
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtPriceRowInfoComparatorTest
{

	@Mock
	private Currency curr;

	private boolean net;
	@Mock
	private PriceRow row1;
	@Mock
	private PriceRow row2;
	@Mock
	private EnumerationValue row1price;

	@Mock
	private Unit unit;

	@InjectMocks
	@Spy
	ExtPriceRowInfoComparator extPriceRowInfoComparator = new ExtPriceRowInfoComparator(curr, net);


	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * Test when row1.getAllAttributes() throw JaloInvalidParameterException.
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 */
	@Test
	public void evaluatePriceRowByPriceTypeJaloInvalidParameterException()
			throws JaloInvalidParameterException, JaloSecurityException
	{


		when(row1.getAllAttributes()).thenThrow(new JaloInvalidParameterException("Jalo Invalid Parameter Exception ", 0));
		doReturn(1).when(extPriceRowInfoComparator).comparePK(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByPriceType(row1, row2);
		Assert.assertEquals(1, value);

	}

	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * Test when row1.getAllAttributes() throw JaloSecurityException
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 */
	@Test
	public void evaluatePriceRowByPriceTypeJaloSecurityException() throws JaloInvalidParameterException, JaloSecurityException
	{
		when(row1.getAllAttributes()).thenThrow(new JaloSecurityException("Jalo Security Exception ", 0));
		doReturn(1).when(extPriceRowInfoComparator).comparePK(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByPriceType(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * Test when row1 price is null
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void evaluatePriceRowByPriceTypeTestRowPriceNull() throws JaloInvalidParameterException, JaloSecurityException
	{
		@SuppressWarnings("rawtypes")
		final Map rowMap = new HashMap();
		rowMap.put(PriceRowModel.PRICETYPE, null);
		when(row1.getAllAttributes()).thenReturn(rowMap);
		doReturn(1).when(extPriceRowInfoComparator).comparePK(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByPriceType(row1, row2);
		Assert.assertEquals(1, value);
	}


	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * Test when row1 pricetype is sale
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 * @throws ConsistencyCheckException
	 */
	@SuppressWarnings(
	{ "unchecked" })
	@Test
	public void evaluatePriceRowByPriceTypeIsSale()
			throws JaloInvalidParameterException, JaloSecurityException, ConsistencyCheckException
	{

		@SuppressWarnings("rawtypes")
		final Map rowMap = new HashMap();
		rowMap.put(PriceRowModel.PRICETYPE, row1price);
		when(row1price.getCode()).thenReturn("SALE");
		when(row1.getAllAttributes()).thenReturn(rowMap);
		doReturn(1).when(extPriceRowInfoComparator).comparePK(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByPriceType(row1, row2);
		Assert.assertEquals(-1, value);
	}

	/**
	 * select price row based on price type. price type "SALE" would be preferred over list price.
	 *
	 * Test when row1 pricetype is list
	 *
	 * @throws JaloInvalidParameterException
	 * @throws JaloSecurityException
	 * @throws ConsistencyCheckException
	 */
	@Test
	public void evaluatePriceRowByPriceTypeIsList()
			throws JaloInvalidParameterException, JaloSecurityException, ConsistencyCheckException
	{

		@SuppressWarnings("rawtypes")
		final Map rowMap = new HashMap();
		rowMap.put(PriceRowModel.PRICETYPE, row1price);
		when(row1price.getCode()).thenReturn("LIST");
		when(row1.getAllAttributes()).thenReturn(rowMap);
		doReturn(1).when(extPriceRowInfoComparator).comparePK(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByPriceType(row1, row2);
		Assert.assertEquals(1, value);
	}


	/**
	 * select price row based on start date. if both row have start date than selection of price based on price type.
	 *
	 * Test when row1's, row2's StartTime value both are null.
	 */
	@Test
	public void evaluatePriceRowByStartTimeRowisNull()
	{
		when(row1.getStartTime()).thenReturn(null);
		when(row2.getStartTime()).thenReturn(null);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByPriceType(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByStartTime(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on start date. if both row have start date than selection of price based on price type.
	 *
	 * Test when row1's, row2's StartTime value are not null.
	 */
	@Test
	public void evaluatePriceRowByStartTimeRowisNotNull()
	{
		when(row1.getStartTime()).thenReturn(new Date(1508275419350L));
		when(row2.getStartTime()).thenReturn(new Date(1508275419350L));
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByPriceType(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByStartTime(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on start date. if both row have start date than selection of price based on price type.
	 *
	 * Test when row1's StartTime value is not null.
	 *
	 */
	@Test
	public void evaluatePriceRowByStartTimeRow1isNotNull()
	{
		when(row1.getStartTime()).thenReturn(new Date(1508275419350L));
		when(row2.getStartTime()).thenReturn(null);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByPriceType(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByStartTime(row1, row2);
		Assert.assertEquals(-1, value);
	}

	/**
	 * select price row based on start date. if both row have start date than selection of price based on price type.
	 *
	 * Test when row2's StartTime value is not null.
	 */
	@Test
	public void evaluatePriceRowByStartTimeRow2isNotNull()
	{
		when(row2.getStartTime()).thenReturn(new Date(1508275419350L));
		when(row1.getStartTime()).thenReturn(null);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByPriceType(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByStartTime(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on net value. which price row have net value that should return if both are same than
	 * selection of price on start time.
	 *
	 * Test when row1's net value is equal to row2's net value.
	 *
	 */
	@Test
	public void evaluatePriceRowByNetRow1TrueRow2True()
	{
		when(row1.isNetAsPrimitive()).thenReturn(true);
		when(row2.isNetAsPrimitive()).thenReturn(true);
		extPriceRowInfoComparator.setNet(true);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByStartTime(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByNet(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on net value. which price row have net value that should return if both are same than
	 * selection of price on start time.
	 *
	 * Test when row1's net value is true.
	 */
	@Test
	public void evaluatePriceRowByNetRow1True()
	{
		when(row1.isNetAsPrimitive()).thenReturn(true);
		when(row2.isNetAsPrimitive()).thenReturn(false);
		extPriceRowInfoComparator.setNet(true);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByStartTime(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByNet(row1, row2);
		Assert.assertEquals(-1, value);
	}

	/**
	 * select price row based on net value. which price row have net value that should return if both are same than
	 * selection of price on start time.
	 *
	 * Test when row2's net value is true.
	 */
	@Test
	public void evaluatePriceRowByNetRow2True()
	{
		when(row1.isNetAsPrimitive()).thenReturn(false);
		when(row2.isNetAsPrimitive()).thenReturn(true);
		extPriceRowInfoComparator.setNet(true);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByStartTime(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByNet(row1, row2);
		Assert.assertEquals(1, value);
	}



	/**
	 * select price row based on currency. if price row have currency then it will return and if both have currency or
	 * both don't have currency than price row selection based on net value.
	 *
	 * Test when row1's currency value is equal to row2's currency value.
	 *
	 */
	@Test
	public void evaluatePriceRowByCurrencyRow1EqualsRow2()
	{
		when(row1.getCurrency()).thenReturn(curr);
		when(row2.getCurrency()).thenReturn(curr);
		extPriceRowInfoComparator.setCurr(curr);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByNet(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByCurrency(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on currency. if price row have currency then it will return and if both have currency or
	 * both don't have currency than price row selection based on net value.
	 *
	 * Test when row1's currency value is null.
	 */
	@Test
	public void evaluatePriceRowByCurrencyRow1Null()
	{
		when(row1.getCurrency()).thenReturn(curr);
		when(row2.getCurrency()).thenReturn(null);
		extPriceRowInfoComparator.setCurr(curr);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByCurrency(row1, row2);
		Assert.assertEquals(-1, value);
	}


	/**
	 * select price row based on currency. if price row have currency then it will return and if both have currency or
	 * both don't have currency than price row selection based on net value.
	 *
	 * Test when row2's currency value is null.
	 *
	 */
	@Test
	public void evaluatePriceRowByCurrencyRow2Null()
	{
		when(row1.getCurrency()).thenReturn(null);
		when(row2.getCurrency()).thenReturn(curr);
		extPriceRowInfoComparator.setCurr(curr);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByCurrency(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on MatchValue. which price row have MatchValue that row will preferred if both price row
	 * have and don't have MatchValue than price row selection based on currency.
	 *
	 * Test when row1's PriceRowByMatchValue value is equal to row2's PriceRowByMatchValue value.
	 *
	 */
	@Test
	public void evaluatePriceRowByMatchValueRow1EqualsRow2()
	{
		when(row1.getMatchValueAsPrimitive()).thenReturn(1);
		when(row2.getMatchValueAsPrimitive()).thenReturn(1);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByCurrency(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMatchValue(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on MatchValue. which price row have MatchValue that row will preferred if both price row
	 * have and don't have MatchValue than price row selection based on currency.
	 *
	 * Test when row1's PriceRowByMatchValue value is greater than row2's PriceRowByMatchValue value.
	 *
	 */
	@Test
	public void evaluatePriceRowByMatchValueRow1GreaterRow2()
	{
		when(row1.getMatchValueAsPrimitive()).thenReturn(2);
		when(row2.getMatchValueAsPrimitive()).thenReturn(1);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMatchValue(row1, row2);
		Assert.assertEquals(-1, value);
	}

	/**
	 * select price row based on MatchValue. which price row have MatchValue that row will preferred if both price row
	 * have and don't have MatchValue than price row selection based on currency.
	 *
	 * Test when row1's PriceRowByMatchValue value is less than row2's PriceRowByMatchValue value.
	 *
	 */
	@Test
	public void evaluatePriceRowByMatchValueRow1LessRow2()
	{
		when(row1.getMatchValueAsPrimitive()).thenReturn(1);
		when(row2.getMatchValueAsPrimitive()).thenReturn(2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMatchValue(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on Minqtd (Scale price definition. A customer needs to order at least this number of units
	 * for this price to be effective. Default value is 1.) which price row have greater value of Minqtd would be
	 * preferred.
	 *
	 * Test when row1 minqtd value is equal to row2 minqtd value.
	 */
	@Test
	public void evaluatePriceRowByMinqtdRow1EqualsRow2()
	{
		when(row1.getMinqtdAsPrimitive()).thenReturn(1508275419350L);
		when(row2.getMinqtdAsPrimitive()).thenReturn(1508275419350L);
		doReturn(1).when(extPriceRowInfoComparator).evaluatePriceRowByMatchValue(row1, row2);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMinqtd(row1, row2);
		Assert.assertEquals(1, value);
	}

	/**
	 * select price row based on Minqtd (Scale price definition. A customer needs to order at least this number of units
	 * for this price to be effective. Default value is 1.) which price row have greater value of Minqtd would be
	 * preferred.
	 *
	 * Test when row1 minqtd value is greater than row2 minqtd value.
	 */
	@Test
	public void evaluatePriceRowByMinqtdRow1GreaterRow2()
	{
		when(row1.getMinqtdAsPrimitive()).thenReturn(1508275419351L);
		when(row2.getMinqtdAsPrimitive()).thenReturn(1508275419350L);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMinqtd(row1, row2);
		Assert.assertEquals(-1, value);
	}

	/**
	 * select price row based on Minqtd (Scale price definition. A customer needs to order at least this number of units
	 * for this price to be effective. Default value is 1.) which price row have greater value of Minqtd would be
	 * preferred.
	 *
	 * Test when row1 minqtd value is less than row2 minqtd value.
	 */
	@Test
	public void evaluatePriceRowByMinqtdRow1LessRow2()
	{
		when(row1.getMinqtdAsPrimitive()).thenReturn(1508275419350L);
		when(row2.getMinqtdAsPrimitive()).thenReturn(1508275419351L);
		final int value = extPriceRowInfoComparator.evaluatePriceRowByMinqtd(row1, row2);
		Assert.assertEquals(1, value);
	}

}
