package com.simon.core.services.impl;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApiClient;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreateOrder;
import com.mirakl.client.mmp.front.domain.order.create.MiraklCreatedOrders;
import com.mirakl.client.mmp.front.domain.order.create.MiraklOfferNotShippable;
import com.mirakl.client.mmp.front.request.order.worflow.MiraklCreateOrderRequest;
import com.mirakl.hybris.core.util.services.JsonMarshallingService;
import com.simon.core.services.RetailerService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMiraklOrderServiceImplTest
{
	private static final String JSON_STRING = "json-string";
	private static final String OFFER_ID = "offerId";
	private static final String ORDER_CODE = "orderCode";

	@InjectMocks
	private ExtMiraklOrderServiceImpl extDefaultMiraklOrderService;

	@Mock
	private MiraklMarketplacePlatformFrontApiClient miraklApiMock;
	@Mock
	private Converter<OrderModel, MiraklCreateOrder> miraklCreateOrderConverterMock;
	@Mock
	private ModelService modelServiceMock;
	@Mock
	private JsonMarshallingService jsonMarshallingService;
	@Mock
	private OrderModel orderMock;
	@Mock
	private MiraklCreateOrder miraklCreateOrderMock;
	@Mock
	private MiraklCreatedOrders miraklCreateOrdersMock;
	@Mock
	private MiraklOfferNotShippable offerNotShippableMock;
	@Mock
	private AbstractOrderEntryModel shippableOfferEntryMock, notShippableOfferEntryMock;
	@Mock
	RetailerService retailerService;

	@Mock
	private MiraklCreateOrderRequest miraklCreateOrderRequest;
	@Captor
	private ArgumentCaptor<MiraklCreateOrderRequest> createOrderRequestCaptor;

	@Before
	public void setUp()
	{
		when(miraklApiMock.createOrder(createOrderRequestCaptor.capture())).thenReturn(miraklCreateOrdersMock);
		when(orderMock.getMarketplaceEntries()).thenReturn(asList(shippableOfferEntryMock, notShippableOfferEntryMock));
		when(orderMock.getCode()).thenReturn(ORDER_CODE);
		when(offerNotShippableMock.getId()).thenReturn(OFFER_ID);
		when(notShippableOfferEntryMock.getOfferId()).thenReturn(OFFER_ID);
		when(jsonMarshallingService.toJson(miraklCreateOrdersMock, MiraklCreatedOrders.class)).thenReturn(JSON_STRING);
		when(jsonMarshallingService.fromJson(JSON_STRING, MiraklCreatedOrders.class)).thenReturn(miraklCreateOrdersMock);
		when(retailerService.getRetailerOrderNumberForMarketPlace(orderMock, "RETA")).thenReturn("001_RETA");
		final List<String> retList = new ArrayList<>();
		retList.add("RETA");
		when(retailerService.getAllRetailersFromOrder(orderMock)).thenReturn(retList);
	}

	@Test
	public void createsMarketplaceOrder()
	{
		final ArgumentCaptor<MiraklCreateOrder> miraklCreateOrderCaptor = ArgumentCaptor.forClass(MiraklCreateOrder.class);
		when(miraklCreateOrderConverterMock.convert(orderMock, miraklCreateOrderMock)).thenReturn(miraklCreateOrderMock);
		final List<MiraklCreatedOrders> result = extDefaultMiraklOrderService.createMarketplaceOrdersForRetailers(orderMock);
		assertNotNull(result);
	}

}
