package com.simon.core.populators;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.search.Document;
import de.hybris.platform.solrfacetsearch.search.SearchResult;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroup;
import de.hybris.platform.solrfacetsearch.search.SearchResultGroupCommand;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.hybris.beans.ShopData;
import com.mirakl.hybris.facades.shop.ShopFacade;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.ImagesData;
import com.simon.core.dto.ProductDetailsData;
import com.simon.core.dto.ProductDimensionsData;
import com.simon.core.dto.VariantProductPriceData;
import com.simon.core.enums.SwatchColorEnum;


/**
 * This class tests the methods of class {@link ProductDetailResponsePopulator}.
 */
@UnitTest
public class ProductDetailResponsePopulatorTest
{
	private static final String SAMPLE_PROMOTION_TYPE = "Rule Based Promotion";

	private static final String SAMPLE_PROMOTION_PRIORITY = "100";

	private static final String SAMPLE_PROMO_DESCRIPTION = "promoDescription";

	private static final String SAMPLE_PROMO_CODE = "promoCode";

	@InjectMocks
	@Spy
	private ProductDetailResponsePopulator productDetailResponsePopulator;
	@Mock
	private ProductDimensionDataPopulator productDimensionDataPopulator;
	@Mock
	private VariantCategoryMapPopulator variantCategoryMapPopulator;
	@Mock
	private ProductImageDataPopulator productImageDataPopulator;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private PriceDataFactory priceDataFactory;
	@Mock
	private SolrSearchResponse source;
	private final ProductDetailsData target = new ProductDetailsData();
	@Mock
	private SearchResult searchResult;
	private final List<SearchResultGroupCommand> groupCommands = new ArrayList<>();
	private final Integer pageSize = 1;
	@Mock
	private SearchResultGroupCommand groupCommand;
	private final List<SearchResultGroup> groups = new ArrayList<>();
	@Mock
	private SearchResultGroup group;
	private final List<Document> documents = new ArrayList<>();
	@Mock
	private Document document;
	@Mock
	private Document document1;

	private final List<String> catPath = new ArrayList<>();
	@Mock
	private CurrencyModel currency;
	final List<String> imagesFromSolr = new ArrayList<String>();
	@Mock
	private SearchResultGroup group2;
	@Mock
	private ShopFacade shopFacade;
	@Mock
	private ShopData shop;

	@Mock
	private EnumerationService enumerationService;

	@Mock
	private TypeService typeService;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(currency.getSymbol()).thenReturn("$");
		Mockito.doReturn("").when(productDetailResponsePopulator).getLocalizedString(Matchers.anyString());
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");

		imagesFromSolr.add("84Wx128H_alternate1|/images/84Wx128H/2-thumb.jpg");
		imagesFromSolr.add("580Wx884H_alternate1|/images/default/2-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_alternate1|/images/zoom/2-zoom.jpg");
		imagesFromSolr.add("325Wx495H_alternate1|/images/mobile/2-mob-thumb.jpg");
		Mockito.doCallRealMethod().when(variantCategoryMapPopulator).populate(Matchers.any(), Matchers.anyMap());
		Mockito.doCallRealMethod().when(productDimensionDataPopulator).populate(Matchers.any(), Matchers.any());
		Mockito.doCallRealMethod().when(productImageDataPopulator).populate(Matchers.any(Document.class),
				Matchers.anyMapOf(String.class, ImagesData.class));
		final ProductDimensionsData prodDimensionsData = new ProductDimensionsData();
		prepareProdDimensionData(prodDimensionsData);
		prepareProdDimensionData(prodDimensionsData);
		Mockito.doReturn(prodDimensionsData).when(productDetailResponsePopulator).populateProductDimensions(document);
		Mockito.doReturn(prodDimensionsData).when(productDetailResponsePopulator).populateProductDimensions(document1);
		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(new ArrayList<String>());
		when(document1.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(new ArrayList<String>());
	}

	@Test
	public void testPopulateWhenNoSearchResultFound()
	{
		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		productDetailResponsePopulator.populate(source, target);
		Assert.assertNull(target.getBaseProductCode());
	}

	@Test
	public void testPopulateWhenNullSearchResultFound()
	{
		//when
		when(source.getSearchResult()).thenReturn(null);
		productDetailResponsePopulator.populate(source, target);
		Assert.assertNull(target.getBaseProductCode());
	}

	@Test
	public void testPopulateWhenNoNullGroupCommandFoundFound()
	{
		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(null);
		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertNull(target.getBaseProductCode());
	}

	@Test
	public void testPopulateWhenNoGroupCommandsFound()
	{
		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertNull(target.getBaseProductCode());
	}

	@Test
	public void testPopulateWithPromotionData()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		groups.add(group2);
		final String path1 = "test/test/cat01/cat01";
		final String path2 = "test/test/cat2";
		final String path3 = "test/test/cat02/cat02";
		catPath.add(path1);
		catPath.add(path2);
		catPath.add(path3);
		documents.add(document);
		documents.add(document1);
		final List<Document> documentsWithMultipleGroup = new ArrayList<>();
		documentsWithMultipleGroup.add(document);
		documentsWithMultipleGroup.add(document1);

		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		prepareDocumentDetail(document);
		prepareDocumentDetail(document1);
		when(group.getDocuments()).thenReturn(documents);
		when(group2.getDocuments()).thenReturn(documentsWithMultipleGroup);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(imagesFromSolr);
		when(document1.getFieldValue(IndexedKeys.PRODUCT_PLUM_PRICE)).thenReturn(4.2);
		when(document.getFieldValue("images")).thenReturn(null);
		when(document1.getFieldValue("images")).thenReturn(null);
		when(document1.getFieldValue(IndexedKeys.PROMOTION_CODE)).thenReturn(SAMPLE_PROMO_CODE);
		when(document1.getFieldValue(IndexedKeys.PROMOTION_DESCRIPTION)).thenReturn(SAMPLE_PROMO_DESCRIPTION);
		when(document1.getFieldValue(IndexedKeys.PROMOTION_PRIORITY)).thenReturn(SAMPLE_PROMOTION_PRIORITY);
		when(document1.getFieldValue(IndexedKeys.PROMOTION_TYPE)).thenReturn(SAMPLE_PROMOTION_TYPE);

		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals(target.getPotentialPromotionData().getCode(), SAMPLE_PROMO_CODE);
	}

	@Test
	public void testPopulateWithNonEmptyGroupCommands()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		final String path1 = "test/test/cat01";
		final String path2 = "test/test/cat2";
		catPath.add(path1);
		catPath.add(path2);
		documents.add(document);

		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		prepareDocumentDetail(document);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(null);

		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals("testProductMVId1", target.getBaseProductCode());
		Assert.assertEquals("111", target.getBaseColors().get(0).getColorId());
	}

	@Test
	public void testPopulateWithNoSkuInStock()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		final String path1 = "test/test/cat01";
		final String path2 = "test/test/cat2";
		catPath.add(path1);
		catPath.add(path2);
		documents.add(document);

		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(false);
		prepareDocumentDetail(document);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(null);
		final List<String> priceList = new ArrayList<>();
		priceList.add("LIST|19.95|false");
		priceList.add("SALE|1.95|false");
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PRICE)).thenReturn(priceList);
		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals("testProductMVId1", target.getBaseProductCode());
		Assert.assertEquals("111", target.getSelectedColorName());
	}

	@Test
	public void testPopulateWithMultipleVariantCategory()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		final String path1 = "test/test/cat01/cat01";
		final String path2 = "test/test/cat2";
		final String path3 = "test/test/cat02/cat02";
		catPath.add(path1);
		catPath.add(path2);
		catPath.add(path3);
		documents.add(document);
		documents.add(document);

		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		prepareDocumentDetail(document);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(imagesFromSolr);

		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals("testProductMVId1", target.getBaseProductCode());
	}

	@Test
	public void testPopulateWithMultipleGroupCommand()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		groups.add(group2);
		final String path1 = "test/test/cat01/cat01";
		final String path2 = "test/test/cat2";
		final String path3 = "test/test/cat02/cat02";
		catPath.add(path1);
		catPath.add(path2);
		catPath.add(path3);
		documents.add(document);
		documents.add(document);
		final List<Document> documentsWithMultipleGroup = new ArrayList<>();
		documentsWithMultipleGroup.add(document);
		documentsWithMultipleGroup.add(document);
		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		prepareDocumentDetail(document);
		when(group.getDocuments()).thenReturn(documents);
		when(group2.getDocuments()).thenReturn(documentsWithMultipleGroup);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(imagesFromSolr);

		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals("testProductMVId1", target.getBaseProductCode());
	}

	@Test
	public void testPopulateWithMultipleGroupCommandAndPriceRange()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		groups.add(group2);
		final String path1 = "test/test/cat01/cat01";
		final String path2 = "test/test/cat2";
		final String path3 = "test/test/cat02/cat02";
		catPath.add(path1);
		catPath.add(path2);
		catPath.add(path3);
		documents.add(document);
		documents.add(document1);
		final List<Document> documentsWithMultipleGroup = new ArrayList<>();
		documentsWithMultipleGroup.add(document);
		documentsWithMultipleGroup.add(document1);

		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(searchResult.getPageSize()).thenReturn(pageSize);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		prepareDocumentDetail(document);
		prepareDocumentDetail(document1);
		when(group.getDocuments()).thenReturn(documents);
		when(group2.getDocuments()).thenReturn(documentsWithMultipleGroup);
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(imagesFromSolr);
		when(document1.getFieldValue(IndexedKeys.PRODUCT_PLUM_PRICE)).thenReturn(4.2);
		when(document.getFieldValue("images")).thenReturn(null);
		when(document1.getFieldValue("images")).thenReturn(null);

		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals("testProductMVId1", target.getBaseProductCode());
	}

	private void prepareProdDimensionData(final ProductDimensionsData product)
	{
		product.setVariantInStore(true);
		product.setBaseProductCode("testProductMVId1");
		product.setBaseProductName("Distressed Skinny Jeans");
		product.setBaseProductdescription("1 Like No Other");
		product.setName("red jeans");
		product.setDescription("1 Like No Other");
		product.setDesignerName("Test Shop1");
		product.setRetailerName("SS");
		product.setRetailerLogo("");
		product.setColorId("111");
		product.setColorName("Red");
		product.setSku("1111");
		final VariantProductPriceData listPriceData = new VariantProductPriceData();
		listPriceData.setFormattedValue("$19.95");
		listPriceData.setValue(BigDecimal.valueOf(19.95));
		listPriceData.setStrikeOff(true);
		final VariantProductPriceData salePriceData = new VariantProductPriceData();
		salePriceData.setFormattedValue("$1.95");
		salePriceData.setValue(BigDecimal.valueOf(1.95));
		salePriceData.setStrikeOff(false);
		product.setPercentOff("90%");
	}

	private void prepareDocumentDetail(final Document document)
	{
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME)).thenReturn("Red");
		when(document.getFieldValue(IndexedKeys.COLOR_SWATCH_IMAGE_URL)).thenReturn("/home");
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME)).thenReturn("IndianRed");
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID)).thenReturn("111");
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(catPath);
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_CODE)).thenReturn("testProductMVId1");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESCRIPTION)).thenReturn("1 Like No Other");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("Test Shop1");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_NAME)).thenReturn("SS");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_LOGO)).thenReturn("");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_ID)).thenReturn("ID");
		when(shopFacade.getShopForId("ID")).thenReturn(shop);
		when(shop.getReturnPolicy()).thenReturn("");
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("Distressed Skinny Jeans");
		when(document.getFieldValue(IndexedKeys.PRODUCT_PLUM_PRICE)).thenReturn(2.2);
		when(document.getFieldValue("cat01Id")).thenReturn("variantValueCategoryId");
		when(document.getFieldValue("cat01Name")).thenReturn("CatName");
		final List<String> priceList = new ArrayList<>();
		priceList.add("LIST|19.95|false");
		priceList.add("SALE|1.95|false");
		priceList.add("PERCENT_SAVING|2");
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PRICE)).thenReturn(priceList);
	}


	@Test
	public void testPopulateWithNullSwatchURL()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		documents.add(document);


		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(catPath);
		when(document.getFieldValue(IndexedKeys.NORMALIZED_COLOR_CODE)).thenReturn("Black");
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		when(shopFacade.getShopForId(Mockito.anyString())).thenReturn(shop);
		when(shop.getReturnPolicy()).thenReturn("Test");
		final SwatchColorEnum enumObj = Mockito.mock(SwatchColorEnum.class);
		when(enumerationService.getEnumerationValue(SwatchColorEnum.class, "Black")).thenReturn(enumObj);
		final EnumerationValueModel enumModel = Mockito.mock(EnumerationValueModel.class);
		when(typeService.getEnumerationValue(enumObj)).thenReturn(enumModel);
		final MediaModel media = Mockito.mock(MediaModel.class);
		when(enumModel.getIcon()).thenReturn(media);
		final String url = "/test";
		when(media.getURL()).thenReturn(url);


		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals(null, target.getBaseColors().get(0).getSwatchImageUrl());
	}

	@Test
	public void testPopulateWithNotNullProductURL()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		documents.add(document);


		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(catPath);
		when(document.getFieldValue(IndexedKeys.NORMALIZED_COLOR_CODE)).thenReturn("Black");
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		final String productUrl = "http://www.google.com";
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PUNCH_OUT_URL)).thenReturn(productUrl);
		when(shopFacade.getShopForId(Mockito.anyString())).thenReturn(shop);
		when(shop.getReturnPolicy()).thenReturn("Test");
		final SwatchColorEnum enumObj = Mockito.mock(SwatchColorEnum.class);
		when(enumerationService.getEnumerationValue(SwatchColorEnum.class, "Black")).thenReturn(enumObj);
		final EnumerationValueModel enumModel = Mockito.mock(EnumerationValueModel.class);
		when(typeService.getEnumerationValue(enumObj)).thenReturn(enumModel);
		final MediaModel media = Mockito.mock(MediaModel.class);
		when(enumModel.getIcon()).thenReturn(media);
		final String url = "/test";
		when(media.getURL()).thenReturn(url);


		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals(productUrl, target.getProductUrl());
		Assert.assertEquals("www.google.com", target.getDomain());
	}

	@Test
	public void testPopulateWithNotNullInvalidProductURL()
	{
		//given
		groupCommands.add(groupCommand);
		groups.add(group);
		documents.add(document);


		//when
		when(source.getSearchResult()).thenReturn(searchResult);
		when(searchResult.getGroupCommands()).thenReturn(groupCommands);
		when(groupCommand.getGroups()).thenReturn(groups);
		when(group.getDocuments()).thenReturn(documents);
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(catPath);
		when(document.getFieldValue(IndexedKeys.NORMALIZED_COLOR_CODE)).thenReturn("Black");
		when(document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		final String productUrl = "abc";
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PUNCH_OUT_URL)).thenReturn(productUrl);
		when(document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_PUNCH_OUT)).thenReturn("Buy On Lyst");
		when(shopFacade.getShopForId(Mockito.anyString())).thenReturn(shop);
		when(shop.getReturnPolicy()).thenReturn("Test");
		final SwatchColorEnum enumObj = Mockito.mock(SwatchColorEnum.class);
		when(enumerationService.getEnumerationValue(SwatchColorEnum.class, "Black")).thenReturn(enumObj);
		final EnumerationValueModel enumModel = Mockito.mock(EnumerationValueModel.class);
		when(typeService.getEnumerationValue(enumObj)).thenReturn(enumModel);
		final MediaModel media = Mockito.mock(MediaModel.class);
		when(enumModel.getIcon()).thenReturn(media);
		final String url = "/test";
		when(media.getURL()).thenReturn(url);


		//then
		productDetailResponsePopulator.populate(source, target);
		Assert.assertEquals(productUrl, target.getProductUrl());
		Assert.assertNull(target.getDomain());
	}

}
