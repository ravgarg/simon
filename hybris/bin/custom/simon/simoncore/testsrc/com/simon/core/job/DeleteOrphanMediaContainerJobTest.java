package com.simon.core.job;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobResult;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.dao.ExtMediaContainerDao;
import com.simon.core.model.DeleteOrphanMediaContainerJobModel;
import com.simon.core.services.ExtMediaContainerService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DeleteOrphanMediaContainerJobTest
{
	@Spy
	@InjectMocks
	private DeleteOrphanMediaContainerJob deleteOrphanMediaContainerJob;

	@Mock
	private ExtMediaContainerService extMediaContainerService;

	@Mock
	private ExtMediaContainerDao extMediaContainerDao;
	@Mock
	private ModelService modelService;
	@Mock
	private CronJobModel cronJobModel;
	@Mock
	private DeleteOrphanMediaContainerJobModel deleteOrphanMediaContainerJobModel;
	@Mock
	Date date;
	private final List<MediaContainerModel> listOfMediacontainer = new ArrayList<>();

	/**
	 * this method is called to set up basic things.
	 *
	 *
	 * @throws IOException
	 */
	@Before
	public void setup() throws IOException
	{
		final MediaContainerModel mediaContainerModel = new MediaContainerModel();
		listOfMediacontainer.add(mediaContainerModel);
	}

	/**
	 * This method test when we have files available to delete.
	 */
	@Test
	public void verifyDeleteOfAllFile()
	{
		final Date currentDate = new Date();
		final Date cleanupAgeDate = new Date(currentDate.getTime() - (TimeUnit.DAYS.toMillis(365) * 7));
		doReturn(date).when(deleteOrphanMediaContainerJob).getCleanupAgeDate(deleteOrphanMediaContainerJobModel);
		when(extMediaContainerDao.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate)).thenReturn(listOfMediacontainer);
		Mockito.doNothing().when(modelService).removeAll(listOfMediacontainer);
		final PerformResult actual = deleteOrphanMediaContainerJob.perform(deleteOrphanMediaContainerJobModel);
		assertThat(actual.getResult().getCode(), is(CronJobResult.SUCCESS));


	}


}
