package com.simon.core.populators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.product.ProductConfigurableChecker;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantTypeModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.DesignerModel;
import com.simon.core.services.ExtProductService;



@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtProductBasicPopulatorTest
{
	@InjectMocks
	@Spy
	ExtProductBasicPopulator populator;
	@Mock
	private ModelService modelService;
	@Mock
	private ProductConfigurableChecker productConfigurableChecker;
	@Mock
	private ProductModel productModel;
	@Mock
	private VariantTypeModel variantType;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private GenericVariantProductModel productModel1;

	@Mock
	private GenericVariantProductModel productModel2;

	@Mock
	private DesignerModel designerModel;

	@Test
	public void populateTestWhenVariantTypeNullApprovalStatusApprovedRetailerActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(productModel.getVariantType()).thenReturn(null);
		when(productModel.getRetailerActive()).thenReturn(true);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertTrue(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNullApprovalStatusApprovedRetailerInActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(productModel.getVariantType()).thenReturn(null);
		when(productModel.getRetailerActive()).thenReturn(false);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());

	}

	@Test
	public void populateTestWhenVariantTypeNullApprovalStatusUnapprovedRetailerActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(productModel.getVariantType()).thenReturn(null);
		when(productModel.getRetailerActive()).thenReturn(true);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNullApprovalStatusUnapprovedRetailerInActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(productModel.getVariantType()).thenReturn(null);
		when(productModel.getRetailerActive()).thenReturn(false);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNotNullApprovalStatusApprovedRetailerActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(productModel.getVariantType()).thenReturn(variantType);
		when(productModel.getRetailerActive()).thenReturn(true);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNotNullApprovalStatusApprovedRetailerInActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(productModel.getVariantType()).thenReturn(variantType);
		when(productModel.getRetailerActive()).thenReturn(false);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNotNullApprovalStatusUnapprovedRetailerActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(productModel.getVariantType()).thenReturn(variantType);
		when(productModel.getRetailerActive()).thenReturn(true);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantTypeNotNullApprovalStatusUnapprovedRetailerInActive()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel)).thenReturn(true);
		when(productModel.getApprovalStatus()).thenReturn(ArticleApprovalStatus.UNAPPROVED);
		when(productModel.getVariantType()).thenReturn(variantType);
		when(productModel.getRetailerActive()).thenReturn(false);
		when(extProductService.isFavorite(productModel)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel, ProductModel.NAME);
		populator.populate(productModel, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}

	@Test
	public void populateTestWhenVariantProductInstanceOfGenericVariant()
	{
		final ProductData productData = new ProductData();
		when(productConfigurableChecker.isProductConfigurable(productModel1)).thenReturn(true);
		when(productModel1.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(productModel1.getBaseProduct()).thenReturn(productModel2);
		when(productModel2.getName()).thenReturn("name");
		when(productModel2.getDescription()).thenReturn("description");
		when(productModel2.getProductDesigner()).thenReturn(designerModel);
		when(designerModel.getName()).thenReturn("dname");
		when(productModel1.getVariantType()).thenReturn(variantType);
		when(productModel1.getRetailerActive()).thenReturn(true);
		when(extProductService.isFavorite(productModel1)).thenReturn(true);
		doReturn("").when(populator).getProductAttributes(productModel1, ProductModel.NAME);
		populator.populate(productModel1, productData);
		assertFalse(productData.getPurchasable().booleanValue());
	}
}
