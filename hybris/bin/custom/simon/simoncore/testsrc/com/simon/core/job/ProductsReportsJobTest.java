package com.simon.core.job;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.model.MerchandizingCategoryModel;
import com.simon.core.model.MiraklCategoryModel;
import com.simon.core.services.ExtProductService;


/**
 * ProductsReportsJobTest unit test for {@link ProductsReportsJob}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductsReportsJobTest
{

	@InjectMocks
	@Spy
	private ProductsReportsJob productsReportsJob;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private ConfigurationService configurationService;

	@Captor
	private ArgumentCaptor<Collection<ProductModel>> productsCaptor;

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		when(catalogVersionService.getCatalogVersion(any(), any())).thenReturn(mock(CatalogVersionModel.class));
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getInt(eq("required.media.count"), eq(1))).thenReturn(1);
	}

	/**
	 * Verify unapproved products logs correctly.
	 */
	@Test
	public void verifyUnapprovedProductsLogsCorrectly()
	{
		final List<ProductModel> unapprovedProducts = new ArrayList<>();

		final ShopModel retailer1 = mock(ShopModel.class);
		final ShopModel retailer2 = mock(ShopModel.class);

		final ProductModel baseProduct1 = mock(ProductModel.class);
		when(baseProduct1.getSupercategories()).thenReturn(Arrays.asList(mock(MerchandizingCategoryModel.class)));
		final ProductModel baseProduct2 = mock(ProductModel.class);

		final ProductModel baseProduct3 = mock(ProductModel.class);
		when(baseProduct1.getSupercategories()).thenReturn(Arrays.asList(mock(MerchandizingCategoryModel.class)));

		final ProductModel baseProduct4 = mock(ProductModel.class);
		final ProductModel baseProduct5 = mock(ProductModel.class);

		//Retailer 1 - Product With Missing Images
		final GenericVariantProductModel variantProduct1 = mock(GenericVariantProductModel.class);
		when(variantProduct1.getMiraklCategory()).thenReturn(mock(MiraklCategoryModel.class));
		when(variantProduct1.getShop()).thenReturn(retailer1);
		when(variantProduct1.getBaseProduct()).thenReturn(baseProduct1);

		//Retailer 1 - Product With Missing Category
		final GenericVariantProductModel variantProduct2 = mock(GenericVariantProductModel.class);
		final MediaContainerModel container = mock(MediaContainerModel.class);
		when(container.getMedias()).thenReturn(Arrays.asList(mock(MediaModel.class), mock(MediaModel.class)));
		when(variantProduct2.getGalleryImages()).thenReturn(Arrays.asList(container));
		when(variantProduct2.getMiraklCategory()).thenReturn(mock(MiraklCategoryModel.class));
		when(variantProduct2.getShop()).thenReturn(retailer1);
		when(variantProduct2.getBaseProduct()).thenReturn(baseProduct2);

		//Retailer 2 - Product With Missing Images
		final GenericVariantProductModel variantProduct3 = mock(GenericVariantProductModel.class);
		when(variantProduct3.getMiraklCategory()).thenReturn(mock(MiraklCategoryModel.class));
		when(variantProduct3.getShop()).thenReturn(retailer2);
		when(variantProduct3.getBaseProduct()).thenReturn(baseProduct3);

		//Retailer 2 - Product With Missing Category
		final GenericVariantProductModel variantProduct4 = mock(GenericVariantProductModel.class);
		final MediaContainerModel container4 = mock(MediaContainerModel.class);
		when(container4.getMedias()).thenReturn(Arrays.asList(mock(MediaModel.class), mock(MediaModel.class)));
		when(variantProduct4.getGalleryImages()).thenReturn(Arrays.asList(container4));
		when(variantProduct4.getMiraklCategory()).thenReturn(mock(MiraklCategoryModel.class));
		when(variantProduct4.getShop()).thenReturn(retailer2);
		when(variantProduct4.getBaseProduct()).thenReturn(baseProduct4);

		//Retailer 2 - Product With Insufficient Images and Category
		final GenericVariantProductModel variantProduct5 = mock(GenericVariantProductModel.class);
		final MediaContainerModel container5 = mock(MediaContainerModel.class);
		when(container5.getMedias()).thenReturn(Arrays.asList(mock(MediaModel.class)));
		when(variantProduct5.getGalleryImages()).thenReturn(Arrays.asList(container5));
		when(variantProduct5.getMiraklCategory()).thenReturn(mock(MiraklCategoryModel.class));
		when(variantProduct5.getShop()).thenReturn(retailer2);
		when(variantProduct5.getBaseProduct()).thenReturn(baseProduct5);

		unapprovedProducts.add(variantProduct1);
		unapprovedProducts.add(variantProduct2);
		unapprovedProducts.add(variantProduct3);
		unapprovedProducts.add(variantProduct4);
		unapprovedProducts.add(variantProduct5);

		when(extProductService.getProductsForCatalogVersion(any(CatalogVersionModel.class), eq(ArticleApprovalStatus.UNAPPROVED)))
				.thenReturn(unapprovedProducts);

		productsReportsJob.perform(null);

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Unapproved Products={}"), eq(retailer1),
				productsCaptor.capture());

		assertTrue(productsCaptor.getValue().contains(variantProduct1));
		assertTrue(productsCaptor.getValue().contains(variantProduct2));

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Unapproved Products={}"), eq(retailer2),
				productsCaptor.capture());

		assertTrue(productsCaptor.getValue().contains(variantProduct3));
		assertTrue(productsCaptor.getValue().contains(variantProduct4));
		assertTrue(productsCaptor.getValue().contains(variantProduct5));

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Missing Gallery Images Unapproved Products={}"), eq(retailer1),
				productsCaptor.capture());

		assertTrue(productsCaptor.getValue().contains(variantProduct1));

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Missing Gallery Images Unapproved Products={}"), eq(retailer2),
				productsCaptor.capture());

		assertTrue(productsCaptor.getValue().contains(variantProduct3));
		assertTrue(productsCaptor.getValue().contains(variantProduct5));

		verify(productsReportsJob).logCount(
				eq("Retailer={}:Id={} Missing Gallery and Merchandizing Categories Unapproved Products={}"), eq(retailer2),
				productsCaptor.capture());

		assertTrue(productsCaptor.getValue().contains(variantProduct5));
	}

	/**
	 * Verify checked products logs correctly.
	 */
	@Test
	public void verifyCheckedProductsLogsCorrectly()
	{
		final List<ProductModel> checkedProducts = new ArrayList<>();

		final ShopModel retailer1 = mock(ShopModel.class);
		final ShopModel retailer2 = mock(ShopModel.class);

		final GenericVariantProductModel variantProduct1 = mock(GenericVariantProductModel.class);
		when(variantProduct1.getShop()).thenReturn(retailer1);
		final GenericVariantProductModel variantProduct2 = mock(GenericVariantProductModel.class);
		when(variantProduct2.getShop()).thenReturn(retailer1);
		final GenericVariantProductModel variantProduct3 = mock(GenericVariantProductModel.class);
		when(variantProduct3.getShop()).thenReturn(retailer2);
		final GenericVariantProductModel variantProduct4 = mock(GenericVariantProductModel.class);
		when(variantProduct4.getShop()).thenReturn(retailer2);
		final GenericVariantProductModel variantProduct5 = mock(GenericVariantProductModel.class);
		when(variantProduct5.getShop()).thenReturn(retailer2);

		checkedProducts.add(variantProduct1);
		checkedProducts.add(variantProduct2);
		checkedProducts.add(variantProduct3);
		checkedProducts.add(variantProduct4);
		checkedProducts.add(variantProduct5);

		when(extProductService.getProductsForCatalogVersion(any(CatalogVersionModel.class), eq(ArticleApprovalStatus.CHECK)))
				.thenReturn(checkedProducts);

		productsReportsJob.perform(null);

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Checked Products={}"), eq(retailer1), productsCaptor.capture());
		assertTrue(productsCaptor.getValue().contains(variantProduct1));
		assertTrue(productsCaptor.getValue().contains(variantProduct2));

		verify(productsReportsJob).logCount(eq("Retailer={}:Id={} Checked Products={}"), eq(retailer2), productsCaptor.capture());
		assertTrue(productsCaptor.getValue().contains(variantProduct3));
		assertTrue(productsCaptor.getValue().contains(variantProduct4));
		assertTrue(productsCaptor.getValue().contains(variantProduct5));
	}

}
