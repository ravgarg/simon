package com.simon.core.services.solr.product.page.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.convert.converter.Converter;

import com.simon.core.dto.ProductDetailsData;


@UnitTest
public class DefaultSolrProductPageServiceTest
{
	@InjectMocks
	DefaultSolrProductPageService defaultSolrProductPageService;
	@Mock
	private Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> searchQueryPageableConverter;
	@Mock
	private Converter<SolrSearchRequest, SolrSearchResponse> searchRequestConverter;
	@Mock
	private Converter<SolrSearchResponse, ProductDetailsData> searchResponseConverter;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetProductDetails()
	{
		final SolrSearchQueryData searchQueryData = new SolrSearchQueryData();
		final PageableData pageableData = new PageableData();
		final SolrSearchRequest solrSearchRequest = new SolrSearchRequest();
		when(searchQueryPageableConverter.convert(Matchers.any(SearchQueryPageableData.class))).thenReturn(solrSearchRequest);
		final SolrSearchResponse solrSearchResponse = new SolrSearchResponse();
		when(searchRequestConverter.convert(solrSearchRequest)).thenReturn(solrSearchResponse);
		final ProductDetailsData productDetailsData = new ProductDetailsData();
		when(searchResponseConverter.convert(solrSearchResponse)).thenReturn(productDetailsData);
		assertEquals(productDetailsData, defaultSolrProductPageService.getProductDetails(searchQueryData, pageableData));
	}

}
