package com.simon.core.dynamichandlers;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.enums.ShopState;
import com.mirakl.hybris.core.model.ShopModel;


/**
 * ProductRetailerStatusHandlerTest unit test for {@link ProductRetailerStatusHandler}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductRetailerStatusHandlerTest
{

	@InjectMocks
	private ProductRetailerStatusHandler productRetailerStatusHandler;

	/**
	 * Verify status returned as true when shop state is open.
	 */
	@Test
	public void verifyStatusReturnedAsTrueWhenShopStateIsOpen()
	{
		final ShopModel shop = mock(ShopModel.class);
		when(shop.getState()).thenReturn(ShopState.OPEN);

		final ProductModel product = mock(ProductModel.class);
		when(product.getShop()).thenReturn(shop);

		assertTrue(productRetailerStatusHandler.get(product));
	}

	/**
	 * Verify status returned as false when shop not present.
	 */
	@Test
	public void verifyStatusReturnedAsFalseWhenShopNotPresent()
	{
		final ProductModel product = mock(ProductModel.class);
		assertFalse(productRetailerStatusHandler.get(product));
	}

	/**
	 * Verify status returned as false when shop is present and state is not open.
	 */
	@Test
	public void verifyStatusReturnedAsFalseWhenShopPresentAndStateIsNotOpen()
	{
		final ShopModel shop = mock(ShopModel.class);
		when(shop.getState()).thenReturn(ShopState.CLOSE);

		final ProductModel product = mock(ProductModel.class);
		when(product.getShop()).thenReturn(shop);

		assertFalse(productRetailerStatusHandler.get(product));
	}

}
