package com.simon.core.services.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.Sets;
import com.simon.core.dao.ExtProductDao;
import com.simon.core.enums.PriceType;


/**
 * ExtProductServiceImplTest, unit test for {@link ExtProductServiceImpl}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtProductServiceImplTest
{
	@InjectMocks
	private ExtProductServiceImpl extProductServiceImpl;

	@Mock
	private ModelService modelService;

	@Mock
	private ExtProductDao extProductDao;

	@Mock
	private GenericVariantProductModel productModel;

	@Mock
	private UserService userService;

	@Mock
	private UserModel userModel;

	@Mock
	private CustomerModel user;
	@Mock
	private ConfigurationService configurationService;

	/**
	 * Verify products returned from dao are returned correctly.
	 */
	@Test
	public void verifyProductsReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final HashSet<String> codes = Sets.newHashSet("testSkudId1", "testSkudId2");

		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsByCodes(catalogVersion, codes)).thenReturn(products);
		extProductServiceImpl.getProductsForCodes(catalogVersion, codes);
		assertThat(extProductServiceImpl.getProductsForCodes(catalogVersion, codes), is(products));
	}

	/**
	 * Verify exception thrown for null catalog version.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownForNullCatalogVersion()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final HashSet<String> codes = Sets.newHashSet("testSkudId1", "testSkudId2");

		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsByCodes(catalogVersion, codes)).thenReturn(products);
		extProductServiceImpl.getProductsForCodes(null, codes);
	}

	/**
	 * Verify exception thrown for null product codes.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownForNullProductCodes()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final HashSet<String> codes = Sets.newHashSet("testSkudId1", "testSkudId2");

		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsByCodes(catalogVersion, codes)).thenReturn(products);
		extProductServiceImpl.getProductsForCodes(catalogVersion, null);
	}

	/**
	 * Verify products returned from dao are returned correctly.
	 */
	@Test
	public void verifyProductsWithPriceRowReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final List<GenericVariantProductModel> products = Arrays.asList(mock(GenericVariantProductModel.class),
				mock(GenericVariantProductModel.class));
		when(extProductDao.findCheckedProductsHavingPriceRow(PriceType.LIST, catalogVersion)).thenReturn(products);
		extProductServiceImpl.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogVersion);
		assertThat(extProductServiceImpl.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogVersion), is(products));

	}

	/**
	 * Verify generic variant products returned from dao are returned correctly.
	 */
	@Test
	public void verifyGenericVariantProductsReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final GenericVariantProductModel genericVariantProductModel = new GenericVariantProductModel();
		final MediaModel mediaModel = new MediaModel();
		final MediaContainerModel mediaContainerModel = new MediaContainerModel();
		final Collection<MediaModel> medias = new ArrayList<>();
		medias.add(mediaModel);
		mediaContainerModel.setMedias(medias);
		final List<MediaContainerModel> value = new ArrayList<MediaContainerModel>();
		value.add(mediaContainerModel);
		genericVariantProductModel.setGalleryImages(value);
		final List<GenericVariantProductModel> products = Arrays.asList(genericVariantProductModel);
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getInt("required.media.count")).thenReturn(0);
		when(extProductDao.findUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion)).thenReturn(products);
		extProductServiceImpl.getUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion);
		assertThat(extProductServiceImpl.getUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion), is(products));
	}


	/**
	 * Test is favorite.
	 */
	@Test
	public void testIsFavorite()
	{
		when(userService.getCurrentUser()).thenReturn(user);
		when(userService.isAnonymousUser(userModel)).thenReturn(Boolean.TRUE);
		final ProductModel productModel = mock(ProductModel.class);
		final Set<ProductModel> productset = new HashSet<>();
		when(user.getMyFavProduct()).thenReturn(productset);
		assertFalse(extProductServiceImpl.isFavorite(productModel));
	}

	/**
	 * Verify products for catalog version returned from dao are returned correctly.
	 */
	@Test
	public void verifyProductsForCatalogVersionReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsByCatalogVersion(catalogVersion)).thenReturn(products);
		assertThat(extProductServiceImpl.getProductsForCatalogVersion(catalogVersion), is(products));
	}

	/**
	 * Verify products without merchandizing category returned from dao are returned correctly.
	 */
	@Test
	public void verifyProductsWithoutMerchandizingCategoryReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsWithoutMerchandizingCategory(catalogVersion)).thenReturn(products);
		assertThat(extProductServiceImpl.getProductsWithoutMerchandizingCategory(catalogVersion), is(products));
	}

	/**
	 * Verify products for catalog version and approval status returned from dao are returned correctly.
	 */
	@Test
	public void verifyProductsForCatalogVersionAndApprovalStatusReturnedFromDaoAreReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		final List<ProductModel> products = Arrays.asList(mock(ProductModel.class), mock(ProductModel.class));
		when(extProductDao.findProductsByCatalogVersion(catalogVersion, ArticleApprovalStatus.UNAPPROVED)).thenReturn(products);
		assertThat(extProductServiceImpl.getProductsForCatalogVersion(catalogVersion, ArticleApprovalStatus.UNAPPROVED),
				is(products));
	}

}
