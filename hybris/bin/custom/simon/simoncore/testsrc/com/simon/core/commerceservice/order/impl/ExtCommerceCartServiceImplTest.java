package com.simon.core.commerceservice.order.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.order.CommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.commerceservices.strategies.CartValidationStrategy;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * This class tests the methods of class {@link ExtCommerceCartServiceImpl}.
 */
@UnitTest
public class ExtCommerceCartServiceImplTest
{
	@InjectMocks
	@Spy
	private ExtCommerceCartServiceImpl commerceCartService;

	@Mock
	private CartValidationStrategy cartValidationStrategy;
	@Mock
	private CommerceCartCalculationStrategy commerceCartCalculationStrategy;

	@Mock
	private CartModel cartModel;

	@Mock
	private CommerceCartParameter commerceCartParameter;
	@Mock
	private CommerceCartModification commerceCartModification;

	private final List<CommerceCartModification> commerceCartModificationList = new ArrayList<>();

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		commerceCartModificationList.add(commerceCartModification);

		Mockito.when(commerceCartParameter.getCart()).thenReturn(cartModel);
		Mockito.when(getCartValidationStrategy().validateCart(commerceCartParameter)).thenReturn(commerceCartModificationList);
		Mockito.when(getCommerceCartCalculationStrategy().recalculateCart(commerceCartParameter)).thenReturn(true);
	}

	/**
	 * @throws CommerceCartModificationException
	 *
	 */
	@Test
	public void testGetChangeEntriesInCart() throws CommerceCartModificationException
	{
		Mockito.when(commerceCartModification.getStatusCode()).thenReturn(CommerceCartModificationStatus.SUCCESS);

		assertEquals(0, commerceCartService.getChangeEntriesInCart(commerceCartParameter).size());
	}

	/**
	 * @throws CommerceCartModificationException
	 *
	 */
	@Test
	public void testGetChangeEntriesInCartForError() throws CommerceCartModificationException
	{
		Mockito.when(commerceCartModification.getStatusCode()).thenReturn(CommerceCartModificationStatus.LOW_STOCK);

		assertEquals(1, commerceCartService.getChangeEntriesInCart(commerceCartParameter).size());
	}

	private CartValidationStrategy getCartValidationStrategy()
	{
		return cartValidationStrategy;
	}

	private CommerceCartCalculationStrategy getCommerceCartCalculationStrategy()
	{
		return commerceCartCalculationStrategy;
	}

}
