package com.simon.core.provider.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.AgeGroup;


@UnitTest
public class BaseProductEnumResolverTest extends AbstractValueResolverTest
{
	private BaseProductEnumResolver valueResolver;

	@Rule
	public ExpectedException expectedException = ExpectedException.none(); //NOPMD
	@Mock
	AbstractValueResolver<GenericVariantProductModel, MediaModel, Object> abstractValueResolver;
	@Mock
	private TypeService typeService;
	@Mock
	private InputDocument document;
	@Mock
	private IndexerBatchContext batchContext;
	@Mock
	private IndexedProperty indexedProperty;

	@Mock
	private Collection<IndexedProperty> indexedProperties;
	private static final String mediaModelUrl = "url1";
	private IndexedProperty indexedProperty1;
	private IndexedProperty indexedProperty2;

	private GenericVariantProductModel variantProductModel;

	private ProductModel baseProduct;

	@Before
	public void setUp()
	{
		indexedProperty1 = new IndexedProperty();
		indexedProperty1.setName(SimonCoreConstants.AGE_GROUP);
		indexedProperty1.setValueProviderParameters(new HashMap<String, String>());

		indexedProperty2 = new IndexedProperty();
		indexedProperty2.setName(SimonCoreConstants.GENDER);
		indexedProperty2.setValueProviderParameters(new HashMap<String, String>());

		variantProductModel = new GenericVariantProductModel();
		baseProduct = new ProductModel();
		when(Boolean.valueOf(getQualifierProvider().canApply(any(IndexedProperty.class)))).thenReturn(Boolean.FALSE);

		valueResolver = new BaseProductEnumResolver();
		valueResolver.setSessionService(getSessionService());
		valueResolver.setQualifierProvider(getQualifierProvider());
	}

	@Test
	public void resolveProductWithNoBaseProduct() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);


		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test(expected = FieldValueProviderException.class)
	public void resolveVariantProductExceptionScenario() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final Map<String, String> params = new HashMap<String, String>();
		params.put(SimonCoreConstants.OPTIONAL_PARAM, "false");
		indexedProperty1.setValueProviderParameters(params);


		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

	}

	@Test
	public void resolveProductWithNoPropertyToResolve() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		variantProductModel.setBaseProduct(baseProduct);
		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		//verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty1, swatch1.getCode(), null);
	}

	@Test
	public void resolveProductWithAgeGroupPropertyToResolve() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		variantProductModel.setBaseProduct(baseProduct);
		baseProduct.setAgeGroup(AgeGroup.TEEN);
		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty1, AgeGroup.TEEN.getCode(), null);
	}

	@Test
	public void resolveProductWithGenderPropertyToResolve() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty2);
		variantProductModel.setBaseProduct(baseProduct);
		baseProduct.setGender(Gender.GIRLS);
		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty2, Gender.GIRLS.getCode(), null);
	}


}
