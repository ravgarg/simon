package com.simon.core.services.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.enums.KeywordType;


/**
 * ExtKeywordServiceImplTest unit test for {@link ExtKeywordServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtKeywordServiceImplTest
{
	@InjectMocks
	private ExtKeywordServiceImpl extKeywordServiceImpl;

	@Mock
	private DefaultGenericDao<KeywordModel> genericDao;

	/**
	 * Verify null keyword returned if empty list is returned from dao.
	 */
	@Test
	public void verifyNullKeywordReturnedIfEmptyListIsReturnedFromDao()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

		final Map<String, Object> params = new HashMap<>();
		params.put("catalogVersion", catalogVersion);
		params.put("keywordType", KeywordType.SEO);
		params.put("keyword", "keyword1");

		when(genericDao.find(params)).thenReturn(Collections.emptyList());
		final KeywordModel actualKeyword = extKeywordServiceImpl.getKeyword(catalogVersion, "keyword1", KeywordType.SEO);
		assertNull(actualKeyword);
	}

	/**
	 * Verify keyword returned from dao is returned correctly.
	 */
	@Test
	public void verifyKeywordReturnedFromDaoIsReturnedCorrectly()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);

		final Map<String, Object> params = new HashMap<>();
		params.put("catalogVersion", catalogVersion);
		params.put("keywordType", KeywordType.SEO);
		params.put("keyword", "keyword1");

		final KeywordModel expected = mock(KeywordModel.class);

		when(genericDao.find(params)).thenReturn(Arrays.asList(expected));
		final KeywordModel actualKeyword = extKeywordServiceImpl.getKeyword(catalogVersion, "keyword1", KeywordType.SEO);
		assertThat(actualKeyword, is(expected));
	}

}
