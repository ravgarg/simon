package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.rule.strategies.RuleParameterValueMapperException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;


/**
 *
 */
@UnitTest
public class ShopRuleParameterValueMapperTest
{
	@InjectMocks
	ShopRuleParameterValueMapper mapper;
	@Mock
	ShopService shopService;
	@Mock
	ShopModel shop;
	String shopId = "shop";

	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test(expected = RuleParameterValueMapperException.class)
	public void fromString()
	{
		when(shopService.getShopForId(shopId)).thenReturn(null);
		mapper.fromString(shopId);

	}

	@Test
	public void fromString_whenShopExists()
	{
		when(shopService.getShopForId(shopId)).thenReturn(shop);
		assertEquals(shop, mapper.fromString(shopId));

	}



	@Test
	public void toStringTest()
	{
		when(shop.getId()).thenReturn(shopId);
		assertEquals(shopId, mapper.toString(shop));
	}
}
