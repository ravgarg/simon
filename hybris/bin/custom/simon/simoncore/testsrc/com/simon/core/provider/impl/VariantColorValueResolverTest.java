package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@UnitTest
public class VariantColorValueResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	VariantColorValueResolver variantColorValueResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	public static final String OPTIONAL_PARAM = "optional";
	public static final String ATTRIBUTE_PARAM = "attribute";


	@Test(expected = FieldValueProviderException.class)
	public void testResolveNullVariantValueCategoriesAndIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		variantColorValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
	}



	@Test
	public void testGetAttributeNameWithValidAttributeName()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, "attributeName");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		assertEquals("attributeName", variantColorValueResolver.getAttributeName(indexedProperty));
	}

	@Test
	public void testGetPropertyNameWithValidCode()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put("value", "attributeName");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		assertEquals("attributeName", variantColorValueResolver.getPropertyName(indexedProperty));
	}

	@Test
	public void testGetAttributeValueWithNullAttributeName()
	{
		final List<CategoryModel> variantValueCategories = new ArrayList<>();
		final CategoryModel cat = new CategoryModel();
		variantValueCategories.add(cat);
		assertNull(variantColorValueResolver.getAttributeValue(variantValueCategories, null, "attribute"));
	}

	@Test
	public void testGetModelAttributeValueWithValidData()
	{
		final CategoryModel cat = new CategoryModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(cat.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(cat, attributeName)).thenReturn(value);
		assertEquals(value, variantColorValueResolver.getModelAttributeValue(cat, attributeName));
	}

	@Test
	public void testGetModelAttributeValueWithTypeServiceWithoutAttribute()
	{
		final CategoryModel cat = new CategoryModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(cat.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.FALSE);
		assertNull(variantColorValueResolver.getModelAttributeValue(cat, attributeName));
	}


	private GenericVariantProductModel setGenericVariantProductModel()
	{
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final List<CategoryModel> variantValueCategories = new ArrayList<>();
		final CategoryModel cat = new CategoryModel();
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final ItemModelContextImpl itemModelContext = (ItemModelContextImpl) cat.getItemModelContext();
		itemModelContext.setLocaleProvider(localeProvider);
		cat.setName("CAT_NAME");
		final CategoryModel superCat = new CategoryModel();
		final ItemModelContextImpl itemModelContext2 = (ItemModelContextImpl) superCat.getItemModelContext();
		itemModelContext2.setLocaleProvider(localeProvider);
		superCat.setCode("attributeName");
		final List<CategoryModel> supercats = new ArrayList<>();
		supercats.add(superCat);
		cat.setSupercategories(supercats);
		variantValueCategories.add(cat);
		model.setSupercategories(variantValueCategories);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(cat.getClass())).thenReturn(composedValue);
		when(typeService.hasAttribute(composedValue, "propertyName")).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(cat, "propertyName")).thenReturn(value);
		return model;
	}

}
