package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.mirakl.hybris.core.shop.daos.ShopDao;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.dao.impl.ExtOrderDaoImpl;
import com.simon.core.dto.OrderExportData;
import com.simon.core.enums.OrderExportType;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.services.RetailerService;
import com.simon.integration.dto.OrderConfirmCallBackDTO;
import com.simon.integration.dto.OrderConfirmRetailersInfoCallBackDTO;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtOrderServiceImplTest
{
	private static final String ORDER_ID = "123456789";
	private static final String RETA = "retailerA";

	@InjectMocks
	@Spy
	private ExtOrderServiceImpl extOrderService;

	@Mock
	private ExtOrderDaoImpl extOrderDao;
	@Mock
	private OrderModel orderModel;
	@Mock
	private ModelService modelService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private AdditionalCartInfoModel additionalCartInfo;
	@Mock
	private RetailerService retailerService;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private AbstractOrderEntryModel orderEntry;

	@Mock
	private OrderEntryModel orderEntryModel;

	@Mock
	private Map<String, List<OrderExportData>> retailerOrderMap;

	@Mock
	private OrderExportData orderExportData;

	@Mock
	protected Converter<OrderEntryModel, OrderExportData> orderForBotAndRetailerFeedStatusConverter;

	@Mock
	private ShopDao shopDao;

	@Mock
	private RetailersInfoModel retA;
	private List<RetailersInfoModel> retailerList;
	private Map<String, Double> retailerSubtotal;
	private Map<String, Double> retailerDeliveryCost;

	@Before
	public void setup()
	{
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(orderModel.getAdditionalCartInfo()).thenReturn(additionalCartInfo);
		when(orderModel.getRetailersSubtotal()).thenReturn(retailerSubtotal);
		when(orderModel.getRetailersDeliveryCost()).thenReturn(retailerDeliveryCost);
		when(retA.getRetailerId()).thenReturn(RETA);
		doNothing().when(getModelService()).save(any());

		retailerList = new ArrayList<>();
		retailerList.add(retA);
		retailerSubtotal = new HashMap<>();
		retailerDeliveryCost = new HashMap<>();
	}

	@Test
	public void getOrderByOrderIdTest()
	{
		when(extOrderDao.getOrderByOrderId(ORDER_ID)).thenReturn(orderModel);
		extOrderService.getOrderByOrderId(ORDER_ID);
		verify(extOrderDao).getOrderByOrderId(ORDER_ID);
	}

	@Test
	public void testSetOrderIdPerRetailer()
	{
		doNothing().when(getModelService()).save(any());

		final UserModel user = new UserModel();
		user.setUid("user@user.com");
		final OrderModel order = new OrderModel();
		order.setUser(user);
		order.setCode("order-1001");

		final OrderConfirmCallBackDTO orderConfirmCallBackDTO = new OrderConfirmCallBackDTO();

		final OrderConfirmRetailersInfoCallBackDTO retailerInfo = new OrderConfirmRetailersInfoCallBackDTO();
		retailerInfo.setRetailerId("retailerId");
		retailerInfo.setOrderId("1001");

		final List<OrderConfirmRetailersInfoCallBackDTO> retailerInfoList = new ArrayList<>();
		retailerInfoList.add(retailerInfo);

		orderConfirmCallBackDTO.setRetailers(retailerInfoList);
		extOrderService.setOrderIdPerRetailer(order, orderConfirmCallBackDTO);

		assertEquals("1001", order.getRetailersOrderNumbers().get("retailerId"));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnDollarValue_whenCommerceAndBotPriceIsSame()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(true);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(100d);
		when(retA.getFinalPrice()).thenReturn(100d);
		assertTrue(extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnDollarValue_whenCommercePriceIsGreaterButShouldBeAccepted()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(true);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(109d);
		when(retA.getFinalPrice()).thenReturn(100d);
		assertTrue(extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnDollarValue_whenCommercePriceIsLesserButShouldBeRejected()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(true);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(100d);
		when(retA.getFinalPrice()).thenReturn(111d);
		assertTrue(!extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnPercentValue_whenCommerceAndBotPriceIsSame()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(false);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(100d);
		when(retA.getFinalPrice()).thenReturn(100d);
		assertTrue(extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnPercentValue__whenCommercePriceIsGreaterButShouldBeAccepted()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(false);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(100d);
		when(retA.getFinalPrice()).thenReturn(91d);
		assertTrue(extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testCheckAcceptOrRejectTheOrder_basedOnPercentValue__whenCommercePriceIsLesserButShouldBeRejected()
	{
		when(configuration.getBoolean(SimonCoreConstants.ACCEPT_REJECT_BASED_ON_DOLLAR_VALUE)).thenReturn(false);
		when(configuration.getDouble(SimonCoreConstants.VALUE_FOR_ORDER_ACCEPTANCE)).thenReturn(10d);
		when(additionalCartInfo.getRetailersInfo()).thenReturn(retailerList);
		when(retailerService.getRetailerTotal(orderModel, RETA)).thenReturn(89d);
		when(retA.getFinalPrice()).thenReturn(100d);
		assertTrue(extOrderService.checkAcceptOrRejectTheOrder(orderModel));
	}

	@Test
	public void testGetRetailerList()
	{
		final Set<ConsignmentModel> consignmentList = new HashSet<>();
		consignmentList.add(consignmentModel);
		when(orderModel.getConsignments()).thenReturn(consignmentList);
		when(consignmentModel.getShopDisplayName()).thenReturn("testRetailer");
		assertNotNull(extOrderService.getRetailerList(orderModel));
	}

	@Test
	public void testOrderFileForBOTEngineifBOTExportisTrue()
	{
		final List<OrderExportData> orderList = new ArrayList<>();
		final OrderExportData orderData = new OrderExportData();
		orderData.setRetailerPrefix("cole");
		orderExportData.setSellerId("2025");
		orderData.setBotExport(OrderExportType.BOT);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonCoreConstants.ORDER_EXPORT_CSV_FILE_HEADER_FOR_BOT)).thenReturn("Test");
		final List<String[]> result = extOrderService.orderFileForBOTEngine(orderList);
		assertNotNull(result);
	}

	@Test
	public void testOrderFileForRetailerifExportIsRetailer()
	{
		final List<OrderExportData> orderList = new ArrayList<>();
		final OrderExportData orderData = new OrderExportData();
		orderData.setRetailerPrefix("cole");
		orderExportData.setSellerId("2025");
		orderData.setBotExport(OrderExportType.RETAILER);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString(SimonCoreConstants.ORDER_EXPORT_CSV_FILE_HEADER_FOR_RETAILER)).thenReturn("Test");
		final List<String[]> result = extOrderService.orderStatusFile(orderList);
		assertNotNull(result);
	}

	@Test
	public void testOrderExportDataList()
	{
		final List<OrderModel> orderList = new ArrayList<>();
		orderList.add(orderModel);
		final OrderEntryModel orderEntryModel = new OrderEntryModel();
		orderEntryModel.setShopId("shop");
		orderEntryModel.setEntryNumber(1);
		final List<AbstractOrderEntryModel> abstractOrderEntryModel = new ArrayList<>();
		abstractOrderEntryModel.add(orderEntryModel);
		when(orderModel.getEntries()).thenReturn(abstractOrderEntryModel);
		when(orderForBotAndRetailerFeedStatusConverter.convert(orderEntryModel)).thenReturn(orderExportData);
		final Map<String, String> retailerMap = new HashMap<>();
		retailerMap.put("shop", "shop");
		when(orderModel.getRetailersOrderNumbers()).thenReturn(retailerMap);
		final MarketplaceConsignmentModel MarketplaceConsignmentModel = new MarketplaceConsignmentModel();
		MarketplaceConsignmentModel.setCode("tset");
		MarketplaceConsignmentModel.setShopId("shop");
		final Set<ConsignmentEntryModel> consignEntryList = new HashSet<>();
		final ConsignmentEntryModel ceModel = new ConsignmentEntryModel();
		ceModel.setMiraklOrderLineId("test");
		ceModel.setOrderEntry(orderEntryModel);
		MarketplaceConsignmentModel.setConsignmentEntries(consignEntryList);
		consignEntryList.add(ceModel);
		final Set<ConsignmentModel> consignmentSet = new HashSet<>();
		consignmentSet.add(MarketplaceConsignmentModel);
		when(orderModel.getConsignments()).thenReturn(consignmentSet);
		final AddressModel addressModel = new AddressModel();
		addressModel.setEmail("email");
		addressModel.setFirstname("Test");
		addressModel.setLastname("Test");
		addressModel.setPostalcode("test");
		when(orderModel.getDeliveryAddress()).thenReturn(addressModel);
		final PaymentInfoModel paymentInfoModel = new PaymentInfoModel();
		paymentInfoModel.setBillingAddress(addressModel);
		when(orderModel.getPaymentInfo()).thenReturn(paymentInfoModel);
		final boolean result = extOrderService.createOrderExportFiles(orderList);
		assertNotNull(result);
	}

	private ModelService getModelService()
	{
		return modelService;
	}

}
