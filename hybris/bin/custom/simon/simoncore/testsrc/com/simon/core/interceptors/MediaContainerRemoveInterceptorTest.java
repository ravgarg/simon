package com.simon.core.interceptors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaContainerRemoveInterceptorTest
{
	@InjectMocks
	private MediaContainerRemoveInterceptor mediaContainerRemoveInterceptor;

	@Mock
	private MediaContainerModel mediaContainer;

	@Mock
	private MediaModel media1;

	@Mock
	private MediaModel media2;

	@Mock
	private InterceptorContext ctx;

	@Test
	public void testMediaGetsRegistered() throws InterceptorException
	{
		when(mediaContainer.getMedias()).thenReturn(Arrays.asList(media1, media2));
		mediaContainerRemoveInterceptor.onRemove(mediaContainer, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(media1, PersistenceOperation.DELETE);
		verify(ctx, Mockito.times(1)).registerElementFor(media2, PersistenceOperation.DELETE);
	}

}
