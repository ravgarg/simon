package com.simon.core.catalog.strategies.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.keygenerators.DesignerKeyGenerator;
import com.simon.core.model.DesignerModel;
import com.simon.core.services.DesignerService;


/**
 * The Class DesignerAttributeHandlerTest. Unit test for {@link DesignerAttributeHandler}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DesignerAttributeHandlerTest
{

	/** The designer attribute handler. */
	@InjectMocks
	private DesignerAttributeHandler designerAttributeHandler;

	/** The core attribute owner strategy. */
	@Mock
	CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	/** The designer service. */
	@Mock
	DesignerService designerService;

	@Mock
	private DesignerKeyGenerator designerKeyGenerator;

	@Mock
	private ModelService modelService;

	/**
	 * Verify valid designer fetched are set correctly on product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyValidDesignerFetchedAreSetCorrectlyOnProduct() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mockCommonData(coreAttribute);

		final DesignerModel designer = mock(DesignerModel.class);
		when(designerKeyGenerator.generateFor("US POLO")).thenReturn("PD-us-polo");
		when(designerService.getDesignerForCode("PD-us-polo")).thenReturn(designer);

		designerAttributeHandler.setValue(attribute, data, context);
		verify(product).setProductDesigner(designer);
	}

	/**
	 * Verify if AttributeValue is null then product has no interaction with setDesigner
	 *
	 * @throws ProductImportException
	 */
	@Test
	public void verifyDesignerIsnotAddedIfAttributeValueIsNull() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mockCommonData(coreAttribute);
		when(attribute.getValue()).thenReturn(null);

		designerAttributeHandler.setValue(attribute, data, context);
		verify(product, times(0)).setProductDesigner(any(DesignerModel.class));
	}


	/**
	 * Mock common data.
	 *
	 * @param coreAttribute
	 *           the core attribute
	 * @return the attribute value data
	 */
	private AttributeValueData mockCommonData(final MiraklCoreAttributeModel coreAttribute)
	{
		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("US POLO");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);
		return attribute;
	}
}
