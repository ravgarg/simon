package com.simon.core.populators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.dto.OrderExportData;
import com.simon.core.enums.OrderExportType;


@UnitTest
public class OrderExportPopulatorTest
{
	@InjectMocks
	private OrderExportPopulator orderExportPopulator;

	@Mock
	private OrderEntryModel orderEntryModel;

	@Mock
	private ProductModel productModel;

	@Mock
	private ShopModel shopModel;

	@Mock
	private GenericVariantProductModel genericVariantProductModel;

	@Mock
	private CategoryModel categoryModel;

	@Mock
	private Collection<CategoryModel> category;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void populaterTestIsBOTExportTrue()
	{
		final OrderExportData orderStatusForOrderBotAndRetailerData = new OrderExportData();
		when(orderEntryModel.getShopId()).thenReturn("test");
		when(orderEntryModel.getCreationtime()).thenReturn(new Date());
		when(orderEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getCode()).thenReturn("product");
		when(productModel.getRetailerSkuID()).thenReturn("testsku");
		when(productModel.getName()).thenReturn("prodName");
		final double test = 123.00;
		when(orderEntryModel.getBasePrice()).thenReturn(test);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getPreFix()).thenReturn("Col");
		when(shopModel.getOrderExportType()).thenReturn(OrderExportType.BOT);
		when(genericVariantProductModel.getSupercategories()).thenReturn(category);
		when(categoryModel.getName()).thenReturn("Color");
		final Set<ConsignmentEntryModel> setConEntry = new HashSet<>();
		final ConsignmentEntryModel consignmentEntryModel = new ConsignmentEntryModel();
		final ConsignmentModel consignmentModel = new ConsignmentModel();
		consignmentModel.setCode("Test-A");
		consignmentEntryModel.setConsignment(consignmentModel);
		consignmentEntryModel.setMiraklOrderLineId("Test-A-1");
		setConEntry.add(consignmentEntryModel);
		when(orderEntryModel.getConsignmentEntries()).thenReturn(setConEntry);
		orderExportPopulator.populate(orderEntryModel, orderStatusForOrderBotAndRetailerData);
		assertEquals(orderStatusForOrderBotAndRetailerData.getBotExport(), OrderExportType.BOT);
	}

	@Test
	public void populaterTestIsBOTExportFalse()
	{
		final OrderExportData orderStatusForOrderBotAndRetailerData = new OrderExportData();
		when(orderEntryModel.getShopId()).thenReturn("test");
		when(orderEntryModel.getCreationtime()).thenReturn(new Date());
		when(orderEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getCode()).thenReturn("product");
		when(productModel.getRetailerSkuID()).thenReturn("testsku");
		when(productModel.getName()).thenReturn("prodName");
		final double test = 123.00;
		when(orderEntryModel.getBasePrice()).thenReturn(test);
		when(productModel.getShop()).thenReturn(shopModel);
		when(shopModel.getPreFix()).thenReturn("Col");
		when(shopModel.getOrderExportType()).thenReturn(OrderExportType.RETAILER);
		when(genericVariantProductModel.getSupercategories()).thenReturn(category);
		when(categoryModel.getName()).thenReturn("Color");
		final Set<ConsignmentEntryModel> setConEntry = new HashSet<>();
		final ConsignmentEntryModel consignmentEntryModel = new ConsignmentEntryModel();
		final ConsignmentModel consignmentModel = new ConsignmentModel();
		consignmentModel.setCode("Test-A");
		consignmentEntryModel.setConsignment(consignmentModel);
		consignmentEntryModel.setMiraklOrderLineId("Test-A-1");
		setConEntry.add(consignmentEntryModel);
		when(orderEntryModel.getConsignmentEntries()).thenReturn(setConEntry);
		orderExportPopulator.populate(orderEntryModel, orderStatusForOrderBotAndRetailerData);
		assertEquals(orderStatusForOrderBotAndRetailerData.getBotExport(), OrderExportType.RETAILER);
	}


}
