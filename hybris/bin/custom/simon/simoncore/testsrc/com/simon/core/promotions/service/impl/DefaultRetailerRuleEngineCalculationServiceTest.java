package com.simon.core.promotions.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.order.calculation.domain.AbstractCharge.ChargeType;
import de.hybris.order.calculation.domain.LineItem;
import de.hybris.order.calculation.domain.Order;
import de.hybris.order.calculation.domain.OrderCharge;
import de.hybris.order.calculation.money.Currency;
import de.hybris.order.calculation.money.Money;
import de.hybris.platform.ruleengineservices.calculation.MinimumAmountValidationStrategy;
import de.hybris.platform.ruleengineservices.calculation.NumberedLineItem;
import de.hybris.platform.ruleengineservices.rao.AbstractOrderRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.util.OrderUtils;
import de.hybris.platform.ruleengineservices.util.RaoUtils;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.promotion.rao.RetailerRAO;


public class DefaultRetailerRuleEngineCalculationServiceTest
{
	@InjectMocks
	DefaultRetailerRuleEngineCalculationService retailerRuleEngineCalculationService;
	@Mock
	private OrderUtils orderUtils;
	@Mock
	private Converter<AbstractOrderRAO, Order> abstractOrderRaoToOrderConverter;
	@Mock
	private MinimumAmountValidationStrategy minimumAmountValidationStrategy;
	@Mock
	private RaoUtils raoUtils;
	@Mock
	RetailerRAO retailerRao;
	@Mock
	private Order order;
	@Mock
	private Currency currency;
	@Mock
	private Money subtotal;
	@Mock
	private Money total;
	@Mock
	private Money zeroCost;

	private Set<OrderEntryRAO> entries;
	@Mock
	private OrderEntryRAO entry;
	@Mock
	private OrderCharge shippingCharge;
	private List<LineItem> lineItems;
	@Mock
	private NumberedLineItem lineItem;

	@Before
	public void setup()
	{
		initMocks(this);
		entries = new HashSet<>();
		lineItems = new ArrayList<>();
		entries.add(entry);
		lineItems.add(lineItem);
		when(abstractOrderRaoToOrderConverter.convert(retailerRao)).thenReturn(order);
		when(entry.getEntryNumber()).thenReturn(1);
		when(order.getCurrency()).thenReturn(currency);
		when(order.getLineItems()).thenReturn(lineItems);
		when(lineItem.getEntryNumber()).thenReturn(1);
		when(lineItem.getTotal(order)).thenReturn(total);
		when(order.getSubTotal()).thenReturn(subtotal);
		when(order.getTotal()).thenReturn(total);
		when(total.getAmount()).thenReturn(BigDecimal.valueOf(100));
		when(subtotal.getAmount()).thenReturn(BigDecimal.valueOf(100));
		when(zeroCost.getAmount()).thenReturn(BigDecimal.valueOf(0));
		when(order.getTotalChargeOfType(ChargeType.SHIPPING)).thenReturn(zeroCost);
		when(order.getTotalChargeOfType(ChargeType.PAYMENT)).thenReturn(zeroCost);
		when(currency.getDigits()).thenReturn(2);
		when(retailerRao.getEntries()).thenReturn(entries);
		when(orderUtils.createShippingCharge(currency, true, BigDecimal.valueOf(0))).thenReturn(shippingCharge);
		doNothing().when(raoUtils).addAction(Mockito.any(), Mockito.any());
	}

	@Test
	public void whenLowerLimitIsValid()
	{
		final DiscountRAO discount = retailerRuleEngineCalculationService.addOrderLevelDiscount(retailerRao, true,
				BigDecimal.valueOf(10));
		assertEquals(0.00, discount.getValue().doubleValue(), 0.0);
	}



}
