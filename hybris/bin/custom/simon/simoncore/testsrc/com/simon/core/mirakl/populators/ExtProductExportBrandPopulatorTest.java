package com.simon.core.mirakl.populators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.DesignerModel;


/**
 * ExtProductExportBrandPopulatorTest unit test for {@link ExtProductExportBrandPopulator}.
 */
@RunWith(MockitoJUnitRunner.class)
@UnitTest
public class ExtProductExportBrandPopulatorTest
{
	@InjectMocks
	private ExtProductExportBrandPopulator populator;

	@Mock
	private GenericVariantProductModel variantProduct;

	@Mock
	private ProductModel baseProduct;

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
	}

	/**
	 * Verify empty string is returned as brand label if designer is null.
	 */
	@Test
	public void verifyEmptyStringIsReturnedAsBrandLabelIfDesignerIsNull()
	{
		assertThat(populator.getBrandLabel(variantProduct), is(""));
	}

	/**
	 * Verify empty string is returned as brand label if designer name is null.
	 */
	@Test
	public void verifyEmptyStringIsReturnedAsBrandLabelIfDesignerNameIsNull()
	{
		when(variantProduct.getProductDesigner()).thenReturn(mock(DesignerModel.class));
		assertThat(populator.getBrandLabel(variantProduct), is(""));
	}

	/**
	 * Verify designer name present is returned as brand label.
	 */
	@Test
	public void verifyDesignerNamePresentIsReturnedAsBrandLabel()
	{
		final DesignerModel designerModel = mock(DesignerModel.class);
		when(designerModel.getName()).thenReturn("Cole Haan");
		when(baseProduct.getProductDesigner()).thenReturn(designerModel);
		assertThat(populator.getBrandLabel(variantProduct), is("Cole Haan"));
	}
}
