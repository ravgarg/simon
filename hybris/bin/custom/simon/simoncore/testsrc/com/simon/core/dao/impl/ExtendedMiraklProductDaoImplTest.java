package com.simon.core.dao.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.validation.services.ValidationService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;


/**
 * ExtendedMiraklProductDaoImplTest integration test for {@link ExtendedMiraklProductDaoImpl}
 */
@IntegrationTest
public class ExtendedMiraklProductDaoImplTest extends ServicelayerTransactionalTest
{
	@Resource
	private ExtendedMiraklProductDaoImpl customProductDao;
	@Resource
	private CatalogVersionService catalogVersionService;
	@Resource
	private UserService userService;
	@Resource
	private static ValidationService validationService = (ValidationService) Registry.getApplicationContext()
			.getBean("validationService");


	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *            the exception
	 */
	@Before
	public void setUp() throws Exception
	{
		validationService.unloadValidationEngine();
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testCategories.impex", "utf-8");
		importCsv("/simoncore/test/testProducts.impex", "utf-8");
	}

	/**
	 * Test find modified products with no variant type.
	 */
	@Test
	public void testFindModifiedProductsWithNoVariantType()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
		final List<ProductModel> products = customProductDao.findModifiedProductsWithNoVariantType(null, catalogVersion);

		assertEquals(2, products.size());

		final ProductModel product = products.get(0);
		assertEquals("testProdSku51", product.getCode());
		assertEquals(true, product instanceof GenericVariantProductModel);

		final GenericVariantProductModel genericVariantProduct = (GenericVariantProductModel) product;
		assertEquals("testProduct5", genericVariantProduct.getBaseProduct().getCode());

		final List<String> categoryCodes = new ArrayList<>();
		for (final CategoryModel category : genericVariantProduct.getBaseProduct().getSupercategories())
		{
			categoryCodes.add(category.getCode());
		}
		assertEquals(true, categoryCodes.contains("testmcat01"));
	}

	/**
	 * Test find modified products with no variant type with modified after.
	 */
	@Test
	public void testFindModifiedProductsWithNoVariantTypeWithModifiedAfter()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
		final List<ProductModel> products = customProductDao.findModifiedProductsWithNoVariantType(new Date(1206063070370L),
				catalogVersion);

		assertEquals(2, products.size());

		final ProductModel product = products.get(0);
		assertEquals("testProdSku51", product.getCode());
		assertEquals(true, product instanceof GenericVariantProductModel);

		final GenericVariantProductModel genericVariantProduct = (GenericVariantProductModel) product;
		assertEquals("testProduct5", genericVariantProduct.getBaseProduct().getCode());

		final List<String> categoryCodes = new ArrayList<>();
		for (final CategoryModel category : genericVariantProduct.getBaseProduct().getSupercategories())
		{
			categoryCodes.add(category.getCode());
		}
		assertEquals(true, categoryCodes.contains("testmcat01"));
	}

}
