package com.simon.core.promotions;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.droolsruleengineservices.compiler.impl.DefaultDroolsRuleActionContext;
import de.hybris.platform.ruleengineservices.calculation.RuleEngineCalculationService;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.promotions.service.RetailerRuleEngineCalculationService;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class RuleRetailerFixedDiscountRAOActionTest
{
	@InjectMocks
	private RuleRetailerFixedDiscountRAOAction raoAction;
	@Mock
	DefaultDroolsRuleActionContext context;
	@Mock
	private RetailerRuleEngineCalculationService retailerRuleEngineCalculationService;
	@Mock
	private RuleEngineCalculationService ruleEngineCalculationService;
	@Mock
	private DiscountRAO discount;
	@Mock
	private RetailerRAO retailer;
	@Mock
	private Map<String, Object> map;
	@Mock
	private RuleEngineResultRAO result;
	@Mock
	private CartRAO cartRao;


	@Before
	public void setup()
	{
		initMocks(this);
		when(context.getVariables()).thenReturn(map);
		final Set<RetailerRAO> retailers = new HashSet();
		retailers.add(retailer);
		when(map.get(RetailerRAO.class.getName())).thenReturn(retailers);
		when(retailer.getTotal()).thenReturn(BigDecimal.valueOf(10.0));
	}

	@Test
	public void setRAOMetaData()
	{
		final AbstractRuleActionRAO[] raos = new AbstractRuleActionRAO[]
		{ discount };
		raoAction.setRAOMetaData(context, raos);
		verify(discount).setRetailer(retailer);
	}

	@Test
	public void performActionTest()
	{
		final BigDecimal amount = BigDecimal.ZERO;
		raoAction.setRuleEngineCalculationService(ruleEngineCalculationService);
		when(retailerRuleEngineCalculationService.addOrderLevelDiscount(retailer, true, amount)).thenReturn(discount);
		when(context.getRuleEngineResultRao()).thenReturn(result);
		when(context.getCartRao()).thenReturn(cartRao);
		raoAction.setRuleEngineCalculationService(ruleEngineCalculationService);
		raoAction.performAction(context, amount);
		verify(retailerRuleEngineCalculationService, times(1)).addOrderLevelDiscount(retailer, true, amount);
		verify(context, times(1)).getCartRao();
	}


}
