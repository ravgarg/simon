package com.simon.core.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simon.core.exceptions.SystemException;


/**
 * The Class JsonUtilsTest. Unit test for {@link JsonUtils}
 */
@UnitTest
public class JsonUtilsTest
{

	/** The json mapper. */
	private final ObjectMapper jsonMapper = new ObjectMapper();

	/**
	 * Verify create json attr creates label attribute correctly.
	 */
	@Test
	public void verifyCreateJsonAttrCreatesLabelAttributeCorrectly()
	{
		assertThat(JsonUtils.createJsonAttr("id", true), is("\"id\": "));
	}

	/**
	 * Verify create json attr creates value attribute correctly.
	 */
	@Test
	public void verifyCreateJsonAttrCreatesValueAttributeCorrectly()
	{
		assertThat(JsonUtils.createJsonAttr("12345", false), is("\"12345\""));
	}

	/**
	 * Verify get json attribute value from json string returns correct value for key.
	 */
	@Test
	public void verifyGetJsonAttributeValueFromJsonStringReturnsCorrectValueForKey()
	{
		assertThat(JsonUtils.getJsonAttributeValue("{\"key\":\"value\"}", "key"), is("value"));
	}

	/**
	 * Verify get json attribute value from json string returns null value for key not present.
	 */
	@Test
	public void verifyGetJsonAttributeValueFromJsonStringReturnsNullValueForKeyNotPresent()
	{
		assertNull(JsonUtils.getJsonAttributeValue("{\"key\":\"value\"}", "key-not-present"));
	}

	/**
	 * Verify exception is thrown when get json attribute value from json string is invalid json.
	 */
	@Test(expected = SystemException.class)
	public void verifyExceptionIsThrownWhenGetJsonAttributeValueFromJsonStringIsInvalidJson()
	{
		assertNull(JsonUtils.getJsonAttributeValue("{\"key\":}", "key"));
	}

	/**
	 * Verify get json attribute value from json object returns correct value for key.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeValueFromJsonObjectReturnsCorrectValueForKey() throws JsonProcessingException, IOException
	{
		assertThat(JsonUtils.getJsonAttributeValue(jsonMapper.readTree("{\"key\":\"value\"}"), "key"), is("value"));
	}

	/**
	 * Verify get json attribute value from json string object null value for key not present.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeValueFromJsonObjectNullValueForKeyNotPresent() throws JsonProcessingException, IOException
	{
		assertNull(JsonUtils.getJsonAttributeValue(jsonMapper.readTree("{\"key\":\"value\"}"), "key-not-present"));
	}

	/**
	 * Verify get json attribute boolean value from json string returns correct boolean value for key.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeBooleanValueFromJsonStringReturnsCorrectBooleanValueForKey()
			throws JsonProcessingException, IOException
	{
		assertTrue(JsonUtils.getJsonAttributeBooleanValue(jsonMapper.readTree("{\"key\":true}"), "key"));
	}


	/**
	 * Verify get json attribute boolean value from json object returns false for key not present.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeBooleanValueFromJsonObjectFalseForKeyNotPresent() throws JsonProcessingException, IOException
	{
		assertFalse(JsonUtils.getJsonAttributeBooleanValue(jsonMapper.readTree("{\"key\":true}"), "key-not-present"));
	}

	/**
	 * Verify get json attribute array values from json object returns correct values for key.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeArrayValuesFromJsonObjectReturnsCorrectValuesForKey()
			throws JsonProcessingException, IOException
	{
		assertThat(JsonUtils.getJsonAttributeArrayValues(jsonMapper.readTree("{\"key\":[\"1\",\"2\",\"3\"]}"), "key"),
				is(Arrays.asList("1", "2", "3")));
	}

	/**
	 * Verify get json attribute array values from json object returns empty array list for no elements present.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeArrayValuesFromJsonObjectReturnsEmptyArrayListForNoElementsPresent()
			throws JsonProcessingException, IOException
	{
		assertThat(JsonUtils.getJsonAttributeArrayValues(jsonMapper.readTree("{\"key\":[]}"), "key"), is(Collections.emptyList()));
	}

	/**
	 * Verify get json attribute array values from json object returns empty array list for key not present.
	 *
	 * @throws JsonProcessingException
	 *            the json processing exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyGetJsonAttributeArrayValuesFromJsonObjectReturnsEmptyArrayListForKeyNotPresent()
			throws JsonProcessingException, IOException
	{
		assertThat(JsonUtils.getJsonAttributeArrayValues(jsonMapper.readTree("{\"key\":[\"1\",\"2\",\"3\"]}"), "key-not-present"),
				is(Collections.emptyList()));
	}

	/**
	 * Verify json content type hdr creates correct map.
	 */
	@Test
	public void verifyJsonContentTypeHdrCreatesCorrectMap()
	{
		final Map<String, String> expectedMap = new HashMap<>();
		expectedMap.put("Content-Type", "application/json");
		assertThat(JsonUtils.jsonContentTypeHdr(), is(expectedMap));
	}

	/**
	 * Verify accept json hdr creates correct map.
	 */
	@Test
	public void verifyAcceptJsonHdrCreatesCorrectMap()
	{
		final Map<String, String> expectedMap = new HashMap<>();
		expectedMap.put("Accept", "application/json");
		assertThat(JsonUtils.acceptJsonHdr(), is(expectedMap));
	}
}
