package com.simon.core.services.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.dao.VariantValueCategoryDao;


/**
 * DefaultVariantValueCategoryServiceTest unit test for {@link DefaultVariantValueCategoryService}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultVariantValueCategoryServiceTest
{

	@InjectMocks
	private DefaultVariantValueCategoryService defaultVariantValueCategoryService;

	@Mock
	private VariantValueCategoryDao variantValueCategoryDao;

	/**
	 * Verify exception thrown for null variant category.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownForNullVariantCategory()
	{
		defaultVariantValueCategoryService.getTopSequenceVariantValueCategory(null);
	}

	/**
	 * Verify variant value category returned from dao is returned correctly.
	 */
	@Test
	public void verifyVariantValueCategoryReturnedFromDaoIsReturnedCorrectly()
	{
		final VariantCategoryModel variantCategory = mock(VariantCategoryModel.class);
		final VariantValueCategoryModel variantValueCategory = mock(VariantValueCategoryModel.class);
		when(variantValueCategoryDao.findTopSequenceVariantValueCategory(variantCategory)).thenReturn(variantValueCategory);
		assertThat(defaultVariantValueCategoryService.getTopSequenceVariantValueCategory(variantCategory),
				is(variantValueCategory));
	}

}
