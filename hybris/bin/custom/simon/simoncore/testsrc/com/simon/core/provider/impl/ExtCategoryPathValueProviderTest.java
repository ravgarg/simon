package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.CategorySource;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class ExtCategoryPathValueProviderTest
{
	@InjectMocks
	ExtCategoryPathValueProvider extCategoryPathValueProvider;
	@Mock
	private IndexConfig indexConfig;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private CategorySource categorySource;
	@Mock
	private FieldNameProvider fieldNameProvider;
	@Mock
	private CategoryService categoryService;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetFieldValuesWithGenericVariantProduct() throws FieldValueProviderException
	{
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		final List<CategoryModel> categories = new ArrayList<>();
		final VariantCategoryModel cat = new VariantCategoryModel();
		cat.setCode("Code");
		categories.add(cat);
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, baseProduct)).thenReturn(categories);
		final List<List<CategoryModel>> pathsForCategory = new ArrayList<>();
		pathsForCategory.add(categories);
		when(categoryService.getPathsForCategory(cat)).thenReturn(pathsForCategory);
		final List<String> fieldNames = new ArrayList<>();
		fieldNames.add("FieldName");
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
		assertEquals("FieldName",
				extCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model).iterator().next().getFieldName());
	}


	@Test
	public void testGetFieldValuesWithProductNotGenericVariant() throws FieldValueProviderException
	{
		final ProductModel model = new ProductModel();
		final List<CategoryModel> categories = new ArrayList<>();
		final VariantCategoryModel cat = new VariantCategoryModel();
		cat.setCode("Code");
		categories.add(cat);
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model)).thenReturn(categories);
		final List<List<CategoryModel>> pathsForCategory = new ArrayList<>();
		pathsForCategory.add(categories);
		when(categoryService.getPathsForCategory(cat)).thenReturn(pathsForCategory);
		final List<String> fieldNames = new ArrayList<>();
		fieldNames.add("FieldName");
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(fieldNames);
		assertEquals("FieldName",
				extCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model).iterator().next().getFieldName());
	}


	@Test
	public void testGetFieldValuesWithProductNotGenericVariantAndVariantCategoriesNull() throws FieldValueProviderException
	{
		final ProductModel model = new ProductModel();
		final List<CategoryModel> categories = new ArrayList<>();
		when(categorySource.getCategoriesForConfigAndProperty(indexConfig, indexedProperty, model)).thenReturn(categories);
		assertEquals(Collections.emptyList(), extCategoryPathValueProvider.getFieldValues(indexConfig, indexedProperty, model));
	}

}
