package com.simon.core.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.core.model.WhitelistedUserModel;


/**
 * The Class ExtWhitelistedLoginDaoImplTest. Integration test for {@link ExtWhitelistedLoginDaoImpl}
 */
@IntegrationTest
public class ExtWhitelistedLoginDaoImplTest extends ServicelayerTransactionalTest
{
	@Resource
	private ExtWhitelistedLoginDaoImpl extWhitelistedLoginDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testWhitelistedUser.impex", "utf-8");
		importCsv("/simoncore/test/testCatalog.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify null designer is returned for invalid code.
	 */
	@Test
	public void verifyNullUserReturnedForInvalidCode()
	{
		final WhitelistedUserModel actual = extWhitelistedLoginDao.findUserByEmail("google1@gmail.com");
		assertNull(actual);
	}

	/**
	 * Verify correct designer is returned for valid code.
	 */
	@Test
	public void verifyCorrectUserIsReturnedForValidDetails()
	{
		final WhitelistedUserModel actual = extWhitelistedLoginDao.findUserByEmail("google@gmail.com");
		assertNotNull(actual);
		assertThat(actual.getEmailId(), is("google@gmail.com"));
	}


}
