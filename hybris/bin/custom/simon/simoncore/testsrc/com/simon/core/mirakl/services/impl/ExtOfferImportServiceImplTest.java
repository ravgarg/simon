package com.simon.core.mirakl.services.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.WarehouseService;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.UnitService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import com.google.common.collect.Sets;
import com.mirakl.client.core.error.MiraklErrorResponseBean;
import com.mirakl.client.core.exception.MiraklApiException;
import com.mirakl.client.mmp.domain.common.MiraklDiscount;
import com.mirakl.client.mmp.domain.common.currency.MiraklIsoCurrencyCode;
import com.mirakl.client.mmp.domain.offer.MiraklExportOffer;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.request.offer.MiraklOffersExportRequest;
import com.mirakl.hybris.core.i18n.services.CurrencyService;
import com.simon.core.dto.OfferImportMetaData;
import com.simon.core.enums.PriceType;
import com.simon.core.exceptions.OfferImportException;
import com.simon.core.mirakl.dao.ExtOfferDao;
import com.simon.core.services.ExtProductService;


/**
 * ExtOfferImportServiceImplTest, unit test for {@link ExtOfferImportServiceImpl}.
 */
@UnitTest
public class ExtOfferImportServiceImplTest
{

	@InjectMocks
	@Spy
	private ExtOfferImportServiceImpl extOfferImportServiceImpl;

	@Mock
	private MiraklMarketplacePlatformFrontApi miraklOperatorApi;

	@Mock
	private ExtOfferDao extOfferDao;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private ModelService modelService;

	@Mock
	private UnitService unitService;

	@Mock
	private WarehouseService warehouseService;

	@Mock
	private UserService userService;

	@Mock
	private CommonI18NService commonI18NService;

	@Mock
	private CurrencyService currencyService;

	@Mock
	private Converter<MiraklExportOffer, PriceRowModel> listPriceConverter;

	@Mock
	private Converter<MiraklExportOffer, PriceRowModel> sellingPriceConverter;

	@Mock
	private Converter<MiraklExportOffer, StockLevelModel> extStockConverter;

	@Mock
	private OfferImportMetaData offerImportMetaData;


	/**
	 * Setup method.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
		when(offerImportMetaData.getBatchSize()).thenReturn(10);
		when(offerImportMetaData.getNoOfWorkers()).thenReturn(1);

		final CurrencyModel currencyModel = mock(CurrencyModel.class);
		when(currencyModel.getIsocode()).thenReturn("USD");
		when(currencyService.getCurrencyForCode("USD")).thenReturn(currencyModel);
		when(unitService.getUnitForCode("pieces")).thenReturn(mock(UnitModel.class));

		final ExecutorService executorService = Executors.newFixedThreadPool(1);
		doReturn(executorService).when(extOfferImportServiceImpl).getExecutorService(offerImportMetaData);
	}

	/**
	 * Verify exception from mirakl api correctly wrapped.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test(expected = OfferImportException.class)
	public void verifyExceptionFromMiraklApiCorrectlyWrapped() throws OfferImportException
	{
		doThrow(new MiraklApiException(new MiraklErrorResponseBean())).when(miraklOperatorApi)
				.exportOffers(any(MiraklOffersExportRequest.class));
		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);
	}

	/**
	 * Verify partial import list price sale price stock updated from offer.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyPartialImportListPriceSalePriceStockUpdatedFromOffer() throws OfferImportException
	{
		final List<MiraklExportOffer> mockOffers = mockOffers();
		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(mockOffers);

		final List<ProductModel> mockProducts = mockProducts(mockOffers);
		final Map<String, PriceRowModel> mockListPriceRowMap = mockListPriceRowMap(mockOffers);

		final Map<String, PriceRowModel> mockSellPriceRowMap = mockSellPrice(mockOffers);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(mockProducts);

		extOfferImportServiceImpl.importOffersUpdatedSince(new Date(), offerImportMetaData);

		//Verify List Price Is Updated From Offer
		verify(listPriceConverter).convert(mockOffers.get(0), mockListPriceRowMap.get("testSkuId1"));
		verify(listPriceConverter).convert(mockOffers.get(1), mockListPriceRowMap.get("testSkuId2"));
		verify(listPriceConverter).convert(mockOffers.get(2), mockListPriceRowMap.get("testSkuId3"));

		//Verify Selling Price Is Updated From Offer
		verify(sellingPriceConverter).convert(mockOffers.get(0), mockSellPriceRowMap.get("testSkuId1"));
		verify(sellingPriceConverter).convert(mockOffers.get(1), mockSellPriceRowMap.get("testSkuId2"));
		verify(sellingPriceConverter).convert(mockOffers.get(2), mockSellPriceRowMap.get("testSkuId3"));

		//Verify Stock Is Updated From Offer
		verify(extStockConverter).convert(mockOffers.get(0), mockProducts.get(0).getStockLevels().iterator().next());
		verify(extStockConverter).convert(mockOffers.get(1), mockProducts.get(1).getStockLevels().iterator().next());
		verify(extStockConverter).convert(mockOffers.get(2), mockProducts.get(2).getStockLevels().iterator().next());
	}


	/**
	 * Verify full import list price sale price stock updated from offer.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportListPriceSalePriceStockUpdatedFromOffer() throws OfferImportException
	{
		final List<MiraklExportOffer> mockOffers = mockOffers();
		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(mockOffers);

		final List<ProductModel> mockProducts = mockProducts(mockOffers);
		final Map<String, PriceRowModel> mockListPriceRowMap = mockListPriceRowMap(mockOffers);

		final Map<String, PriceRowModel> mockSellPriceRowMap = mockSellPrice(mockOffers);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(mockProducts);

		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		//Verify List Price Is Updated From Offer
		verify(listPriceConverter).convert(mockOffers.get(0), mockListPriceRowMap.get("testSkuId1"));
		verify(listPriceConverter).convert(mockOffers.get(1), mockListPriceRowMap.get("testSkuId2"));
		verify(listPriceConverter).convert(mockOffers.get(2), mockListPriceRowMap.get("testSkuId3"));

		//Verify Selling Price Is Updated From Offer
		verify(sellingPriceConverter).convert(mockOffers.get(0), mockSellPriceRowMap.get("testSkuId1"));
		verify(sellingPriceConverter).convert(mockOffers.get(1), mockSellPriceRowMap.get("testSkuId2"));
		verify(sellingPriceConverter).convert(mockOffers.get(2), mockSellPriceRowMap.get("testSkuId3"));

		//Verify Stock Is Updated From Offer
		verify(extStockConverter).convert(mockOffers.get(0), mockProducts.get(0).getStockLevels().iterator().next());
		verify(extStockConverter).convert(mockOffers.get(1), mockProducts.get(1).getStockLevels().iterator().next());
		verify(extStockConverter).convert(mockOffers.get(2), mockProducts.get(2).getStockLevels().iterator().next());
	}

	/**
	 * Verify full import missing offers and stock deleted.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportMissingOffersAndStockDeleted() throws OfferImportException
	{
		final List<MiraklExportOffer> mockOffers = mockOffers();
		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(mockOffers);

		final List<ProductModel> mockProducts = mockProducts(mockOffers);
		mockSellPrice(mockOffers);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(mockProducts);

		final PriceRowModel missingPriceRow = mock(PriceRowModel.class);
		final List<PriceRowModel> missingPrices = new ArrayList<>();
		missingPrices.add(missingPriceRow);
		when(extOfferDao.findPricesModifiedBeforeDate(any(Date.class))).thenReturn(missingPrices);

		final List<StockLevelModel> missingStockLevels = new ArrayList<>();
		final StockLevelModel missingStockLevel = mock(StockLevelModel.class);
		missingStockLevels.add(missingStockLevel);
		when(extOfferDao.findStocksModifiedBeforeDate(any(Date.class))).thenReturn(missingStockLevels);

		when(offerImportMetaData.isDeleteMissingOffers()).thenReturn(true);
		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		verify(modelService).removeAll(missingPrices);
		verify(modelService).removeAll(missingStockLevels);
	}

	/**
	 * Verify full import missing offers and stock deleted not deleted if updated.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportMissingOffersAndStockDeletedNotDeletedIfUpdated() throws OfferImportException
	{
		final List<MiraklExportOffer> mockOffers = mockOffers();
		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(mockOffers);

		final List<ProductModel> mockProducts = mockProducts(mockOffers);
		mockSellPrice(mockOffers);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(mockProducts);

		final List<PriceRowModel> missingPrices = new ArrayList<>();
		final PriceRowModel missingPriceRow1 = mock(PriceRowModel.class);
		when(missingPriceRow1.getProductId()).thenReturn("testSkuId2");
		final PriceRowModel missingPriceRow2 = mock(PriceRowModel.class);
		when(missingPriceRow2.getProductId()).thenReturn("testSkuId4");
		missingPrices.add(missingPriceRow1);
		missingPrices.add(missingPriceRow2);
		when(extOfferDao.findPricesModifiedBeforeDate(any(Date.class))).thenReturn(missingPrices);

		final List<StockLevelModel> missingStockLevels = new ArrayList<>();
		final StockLevelModel missingStockLevel1 = mock(StockLevelModel.class);
		when(missingStockLevel1.getProductCode()).thenReturn("testSkuId1");
		final StockLevelModel missingStockLevel2 = mock(StockLevelModel.class);
		when(missingStockLevel2.getProductCode()).thenReturn("testSkuId4");
		missingStockLevels.add(missingStockLevel1);
		missingStockLevels.add(missingStockLevel2);
		when(extOfferDao.findStocksModifiedBeforeDate(any(Date.class))).thenReturn(missingStockLevels);
		when(offerImportMetaData.isDeleteMissingOffers()).thenReturn(true);
		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		final List<PriceRowModel> pricesToDelete = new ArrayList<>();
		pricesToDelete.add(missingPriceRow2);

		final List<StockLevelModel> stocksToDelete = new ArrayList<>();
		stocksToDelete.add(missingStockLevel2);

		verify(modelService).removeAll(pricesToDelete);
		verify(modelService).removeAll(stocksToDelete);
	}

	/**
	 * Verify full import junk offer ignored.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportJunkOfferIgnored() throws OfferImportException
	{
		final List<MiraklExportOffer> mockOffers = mockOffers();

		// Adding Junk Offer
		final MiraklExportOffer junkOffer = mock(MiraklExportOffer.class);
		when(junkOffer.getProductSku()).thenReturn("testSkuId4");
		when(junkOffer.getQuantity()).thenReturn(90);
		when(junkOffer.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);

		final MiraklDiscount junkOfferDiscount = mock(MiraklDiscount.class);
		final Date junkOfferDiscountStartDate = mock(Date.class);
		final Date junkOfferDiscountEndDate = mock(Date.class);
		when(junkOfferDiscount.getOriginPrice()).thenReturn(BigDecimal.valueOf(60));
		when(junkOfferDiscount.getDiscountPrice()).thenReturn(BigDecimal.valueOf(55));
		when(junkOfferDiscount.getStartDate()).thenReturn(junkOfferDiscountStartDate);
		when(junkOfferDiscount.getEndDate()).thenReturn(junkOfferDiscountEndDate);
		when(junkOffer.getDiscount()).thenReturn(junkOfferDiscount);

		mockOffers.add(junkOffer);

		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(mockOffers);

		final List<ProductModel> mockProducts = mockProducts(mockOffers);
		mockSellPrice(mockOffers);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(mockProducts);

		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		//Verify List Price Is Not Updated From Offer
		verify(listPriceConverter, times(0)).convert(eq(junkOffer), any(PriceRowModel.class));

		//Verify Selling Price Is Not Updated From Offer
		verify(sellingPriceConverter, times(0)).convert(eq(junkOffer), any(PriceRowModel.class));

		//Verify Stock Is Not Updated From Offer
		verify(extStockConverter, times(0)).convert(eq(junkOffer), any(StockLevelModel.class));
	}


	/**
	 * Verify full import new list price created if not exists.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportNewListPriceCreatedIfNotExists() throws OfferImportException
	{
		final MiraklExportOffer offer1 = mock(MiraklExportOffer.class);
		when(offer1.getProductSku()).thenReturn("testSkuId1");
		when(offer1.getQuantity()).thenReturn(10);
		when(offer1.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);

		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(Arrays.asList(offer1));

		final ProductModel product1 = mock(ProductModel.class);
		when(product1.getCode()).thenReturn("testSkuId1");
		final StockLevelModel product1Stock = mock(StockLevelModel.class);
		final Set<StockLevelModel> stockLevelsProduct1 = new HashSet<>();
		stockLevelsProduct1.add(product1Stock);
		when(product1.getStockLevels()).thenReturn(stockLevelsProduct1);
		when(extStockConverter.convert(offer1, product1Stock)).thenReturn(product1Stock);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(Arrays.asList(product1));

		final PriceRowModel listPriceCreated = mock(PriceRowModel.class);
		when(modelService.create(PriceRowModel.class)).thenReturn(listPriceCreated);
		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		//Verify New List Price Is Created And Updated From Offer
		verify(listPriceConverter).convert(offer1, listPriceCreated);
	}


	/**
	 * Verify full import new stock created if not exists.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifyFullImportNewStockCreatedIfNotExists() throws OfferImportException
	{
		final MiraklExportOffer offer1 = mock(MiraklExportOffer.class);
		when(offer1.getProductSku()).thenReturn("testSkuId1");
		when(offer1.getQuantity()).thenReturn(10);
		when(offer1.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);

		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(Arrays.asList(offer1));

		final ProductModel product1 = mock(ProductModel.class);
		when(product1.getCode()).thenReturn("testSkuId1");

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(Arrays.asList(product1));

		final PriceRowModel listPrice1 = mock(PriceRowModel.class);
		when(listPrice1.getProductId()).thenReturn("testSkuId1");
		when(listPriceConverter.convert(offer1, listPrice1)).thenReturn(listPrice1);
		when(extOfferDao.findPricesForProductCodes(productIdList, PriceType.LIST)).thenReturn(Arrays.asList(listPrice1));

		final PriceRowModel sellPrice1 = mock(PriceRowModel.class);
		when(sellPrice1.getProductId()).thenReturn("testSkuId1");
		when(sellingPriceConverter.convert(offer1, sellPrice1)).thenReturn(sellPrice1);
		when(extOfferDao.findPricesForProductCodes(productIdList, PriceType.SALE)).thenReturn(Arrays.asList((sellPrice1)));

		when(warehouseService.getDefWarehouse()).thenReturn(Arrays.asList(mock(WarehouseModel.class)));

		final StockLevelModel stockLevelCreated = mock(StockLevelModel.class);
		when(modelService.create(StockLevelModel.class)).thenReturn(stockLevelCreated);

		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		//Verify New Stock Level Is Created And Updated From Offer
		verify(extStockConverter).convert(offer1, stockLevelCreated);
	}


	/**
	 * Verify selling price updated for available discount.
	 *
	 * @throws OfferImportException
	 *            the offer import exception
	 */
	@Test
	public void verifySellingPriceUpdatedForAvailableDiscount() throws OfferImportException
	{
		final MiraklDiscount discount1 = mock(MiraklDiscount.class);
		when(discount1.getEndDate()).thenReturn(mock(Date.class));

		final MiraklExportOffer offer1 = mock(MiraklExportOffer.class);
		when(offer1.getProductSku()).thenReturn("testSkuId1");
		when(offer1.getQuantity()).thenReturn(10);
		when(offer1.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);
		when(offer1.getDiscount()).thenReturn(discount1);

		when(miraklOperatorApi.exportOffers(any(MiraklOffersExportRequest.class))).thenReturn(Arrays.asList(offer1));

		final ProductModel product1 = mock(ProductModel.class);
		when(product1.getCode()).thenReturn("testSkuId1");
		final StockLevelModel product1Stock = mock(StockLevelModel.class);
		final Set<StockLevelModel> stockLevelsProduct1 = new HashSet<>();
		stockLevelsProduct1.add(product1Stock);
		when(product1.getStockLevels()).thenReturn(stockLevelsProduct1);
		when(extStockConverter.convert(offer1, product1Stock)).thenReturn(product1Stock);

		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(anyString(), anyString())).thenReturn(catalogVersion);

		final Set<String> productIdList = Sets.newHashSet("testSkuId1");
		when(extProductService.getProductsForCodes(catalogVersion, productIdList)).thenReturn(Arrays.asList(product1));

		final PriceRowModel listPrice1 = mock(PriceRowModel.class);
		when(listPrice1.getProductId()).thenReturn("testSkuId1");
		when(listPriceConverter.convert(offer1, listPrice1)).thenReturn(listPrice1);
		when(extOfferDao.findPricesForProductCodes(productIdList, PriceType.LIST)).thenReturn(Arrays.asList(listPrice1));

		final PriceRowModel sellPrice1 = mock(PriceRowModel.class);
		when(sellPrice1.getProductId()).thenReturn("testSkuId1");
		when(extOfferDao.findPricesForProductCodes(productIdList, PriceType.SALE)).thenReturn(Arrays.asList(sellPrice1));

		extOfferImportServiceImpl.importAllOffers(null, offerImportMetaData);

		//Verify Selling Price Not Updated If Null Start Date
		verify(sellingPriceConverter).convert(offer1, sellPrice1);
	}

	/**
	 * Mock sell price.
	 *
	 * @param mockOffers
	 *           the mock offers
	 * @return the map
	 */
	private Map<String, PriceRowModel> mockSellPrice(final List<MiraklExportOffer> mockOffers)
	{
		final List<PriceRowModel> sellPrices = new ArrayList<>();
		final Map<String, PriceRowModel> sellPricesMap = new HashMap<>();
		final PriceRowModel sellPrice1 = mock(PriceRowModel.class);
		final PriceRowModel sellPrice2 = mock(PriceRowModel.class);
		final PriceRowModel sellPrice3 = mock(PriceRowModel.class);

		when(sellPrice1.getProductId()).thenReturn("testSkuId1");
		when(sellPrice2.getProductId()).thenReturn("testSkuId2");
		when(sellPrice3.getProductId()).thenReturn("testSkuId3");

		when(sellingPriceConverter.convert(mockOffers.get(0), sellPrice1)).thenReturn(sellPrice1);
		when(sellingPriceConverter.convert(mockOffers.get(1), sellPrice2)).thenReturn(sellPrice2);
		when(sellingPriceConverter.convert(mockOffers.get(2), sellPrice3)).thenReturn(sellPrice3);

		sellPrices.add(sellPrice1);
		sellPrices.add(sellPrice2);
		sellPrices.add(sellPrice3);

		sellPricesMap.put("testSkuId1", sellPrice1);
		sellPricesMap.put("testSkuId2", sellPrice2);
		sellPricesMap.put("testSkuId3", sellPrice3);

		when(extOfferDao.findPricesForProductCodes(eq(Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3")),
				eq(PriceType.SALE))).thenReturn(sellPrices);

		return sellPricesMap;
	}

	/**
	 * Mock list price row map.
	 *
	 * @param mockOffers
	 *           the mock offers
	 * @return the map
	 */
	private Map<String, PriceRowModel> mockListPriceRowMap(final List<MiraklExportOffer> mockOffers)
	{
		final List<PriceRowModel> listPrices = new ArrayList<>();
		final Map<String, PriceRowModel> listPricesMap = new HashMap<>();
		final PriceRowModel listPrice1 = mock(PriceRowModel.class);
		final PriceRowModel listPrice2 = mock(PriceRowModel.class);
		final PriceRowModel listPrice3 = mock(PriceRowModel.class);

		when(listPrice1.getProductId()).thenReturn("testSkuId1");
		when(listPrice2.getProductId()).thenReturn("testSkuId2");
		when(listPrice3.getProductId()).thenReturn("testSkuId3");

		when(listPriceConverter.convert(mockOffers.get(0), listPrice1)).thenReturn(listPrice1);
		when(listPriceConverter.convert(mockOffers.get(1), listPrice2)).thenReturn(listPrice2);
		when(listPriceConverter.convert(mockOffers.get(2), listPrice3)).thenReturn(listPrice3);

		listPrices.add(listPrice1);
		listPrices.add(listPrice2);
		listPrices.add(listPrice3);

		listPricesMap.put("testSkuId1", listPrice1);
		listPricesMap.put("testSkuId2", listPrice2);
		listPricesMap.put("testSkuId3", listPrice3);

		when(extOfferDao.findPricesForProductCodes(eq(Sets.newHashSet("testSkuId1", "testSkuId2", "testSkuId3")),
				eq(PriceType.LIST))).thenReturn(listPrices);
		return listPricesMap;
	}

	/**
	 * Mock products.
	 *
	 * @param mockOffers
	 *           the mock offers
	 * @return the list
	 */
	private List<ProductModel> mockProducts(final List<MiraklExportOffer> mockOffers)
	{
		final ProductModel product1 = mock(ProductModel.class);
		when(product1.getCode()).thenReturn("testSkuId1");
		final StockLevelModel product1Stock = mock(StockLevelModel.class);
		final Set<StockLevelModel> stockLevelsProduct1 = new HashSet<>();
		stockLevelsProduct1.add(product1Stock);
		when(product1.getStockLevels()).thenReturn(stockLevelsProduct1);

		final ProductModel product2 = mock(ProductModel.class);
		when(product2.getCode()).thenReturn("testSkuId2");
		final StockLevelModel product2Stock = mock(StockLevelModel.class);
		final Set<StockLevelModel> stockLevelsProduct2 = new HashSet<>();
		stockLevelsProduct2.add(product2Stock);
		when(product2.getStockLevels()).thenReturn(stockLevelsProduct2);

		final ProductModel product3 = mock(ProductModel.class);
		when(product3.getCode()).thenReturn("testSkuId3");
		final StockLevelModel product3Stock = mock(StockLevelModel.class);
		final Set<StockLevelModel> stockLevelsProduct3 = new HashSet<>();
		stockLevelsProduct3.add(product3Stock);
		when(product3.getStockLevels()).thenReturn(stockLevelsProduct3);

		when(extStockConverter.convert(mockOffers.get(0), product1Stock)).thenReturn(product1Stock);
		when(extStockConverter.convert(mockOffers.get(1), product2Stock)).thenReturn(product2Stock);
		when(extStockConverter.convert(mockOffers.get(2), product3Stock)).thenReturn(product3Stock);
		return Arrays.asList(product1, product2, product3);
	}

	/**
	 * Mock offers.
	 *
	 * @return the list
	 */
	private List<MiraklExportOffer> mockOffers()
	{

		final List<MiraklExportOffer> offers = new ArrayList<>();
		final MiraklExportOffer offer1 = mock(MiraklExportOffer.class);
		when(offer1.getProductSku()).thenReturn("testSkuId1");
		when(offer1.getQuantity()).thenReturn(10);
		when(offer1.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);

		final MiraklDiscount discount1 = mock(MiraklDiscount.class);
		final Date discount1StartDate = mock(Date.class);
		final Date discount1EndDate = mock(Date.class);
		when(discount1.getOriginPrice()).thenReturn(BigDecimal.valueOf(20));
		when(discount1.getDiscountPrice()).thenReturn(BigDecimal.valueOf(15));
		when(discount1.getStartDate()).thenReturn(discount1StartDate);
		when(discount1.getEndDate()).thenReturn(discount1EndDate);
		when(offer1.getDiscount()).thenReturn(discount1);

		final MiraklExportOffer offer2 = mock(MiraklExportOffer.class);
		when(offer2.getProductSku()).thenReturn("testSkuId2");
		when(offer2.getQuantity()).thenReturn(20);
		when(offer2.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.USD);

		final MiraklDiscount discount2 = mock(MiraklDiscount.class);
		final Date discount2StartDate = mock(Date.class);
		final Date discount2EndDate = mock(Date.class);
		when(discount2.getOriginPrice()).thenReturn(BigDecimal.valueOf(40));
		when(discount2.getDiscountPrice()).thenReturn(BigDecimal.valueOf(35));
		when(discount2.getStartDate()).thenReturn(discount2StartDate);
		when(discount2.getEndDate()).thenReturn(discount2EndDate);
		when(offer2.getDiscount()).thenReturn(discount2);

		final MiraklExportOffer offer3 = mock(MiraklExportOffer.class);
		when(offer3.getProductSku()).thenReturn("testSkuId3");
		when(offer3.getQuantity()).thenReturn(30);
		when(offer3.getCurrencyIsoCode()).thenReturn(MiraklIsoCurrencyCode.EUR);

		final MiraklDiscount discount3 = mock(MiraklDiscount.class);
		final Date discount3StartDate = mock(Date.class);
		final Date discount3EndDate = mock(Date.class);
		when(discount3.getOriginPrice()).thenReturn(BigDecimal.valueOf(60));
		when(discount3.getDiscountPrice()).thenReturn(BigDecimal.valueOf(55));
		when(discount3.getStartDate()).thenReturn(discount3StartDate);
		when(discount3.getEndDate()).thenReturn(discount3EndDate);
		when(offer3.getDiscount()).thenReturn(discount3);

		offers.add(offer1);
		offers.add(offer2);
		offers.add(offer3);

		return offers;
	}
}
