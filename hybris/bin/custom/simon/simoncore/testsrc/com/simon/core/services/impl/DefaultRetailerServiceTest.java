package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.services.DesignerService;


/**
 * DefaultDesignerService, default implementation of {@link DesignerService}.
 */
@UnitTest
public class DefaultRetailerServiceTest
{
	private static final int PROMOTION = 3;
	private static final double LISTPRICE = 11.0;
	private static final double SALEPRICE = 10.0;
	private static final double MSRP = 12.0;
	@InjectMocks
	DefaultRetailerService retailerService;
	@Mock
	CartModel cart;
	@Mock
	AbstractOrderEntryModel all3PriceQty1RetA;
	@Mock
	AbstractOrderEntryModel lpNspQty2RetB;
	@Mock
	AbstractOrderEntryModel msrpNLpQty2RetA;
	@Mock
	AbstractOrderEntryModel lpQty1RetB;
	@Mock
	ProductModel product1;
	@Mock
	ProductModel product2;
	@Mock
	ProductModel product3;
	@Mock
	ProductModel product4;
	@Mock
	ShopModel retailerA;
	@Mock
	ShopModel retailerB;
	List<AbstractOrderEntryModel> entries;
	List<DiscountValue> retailerDiscounts;
	Double RETAILER_A_ITEM_SAVING = (MSRP - SALEPRICE) + (MSRP - LISTPRICE) * 2;
	Double RETAILER_B_ITEM_SAVING = (LISTPRICE - SALEPRICE) * 2 + (LISTPRICE - LISTPRICE);
	Double RETAILE_A_SUBBAG_TOTAL = SALEPRICE + LISTPRICE * 2;
	Double RETAILE_B_SUBBAG_TOTAL = SALEPRICE * 2 + LISTPRICE;


	@Mock
	AbstractOrderEntryModel orderEntryModel;
	@Mock
	ProductModel productModel;


	@Before
	public void setUp()
	{
		initMocks(this);

		when(all3PriceQty1RetA.getProduct()).thenReturn(product1);
		when(lpNspQty2RetB.getProduct()).thenReturn(product2);
		when(msrpNLpQty2RetA.getProduct()).thenReturn(product3);
		when(lpQty1RetB.getProduct()).thenReturn(product4);
		when(all3PriceQty1RetA.getQuantity()).thenReturn(1L);
		when(lpNspQty2RetB.getQuantity()).thenReturn(2L);
		when(msrpNLpQty2RetA.getQuantity()).thenReturn(2L);
		when(lpQty1RetB.getQuantity()).thenReturn(1L);
		when(all3PriceQty1RetA.getTotalPrice()).thenReturn(SALEPRICE);
		when(lpNspQty2RetB.getTotalPrice()).thenReturn(SALEPRICE * 2);
		when(msrpNLpQty2RetA.getTotalPrice()).thenReturn(LISTPRICE * 2);
		when(lpQty1RetB.getTotalPrice()).thenReturn(LISTPRICE);
		when(all3PriceQty1RetA.getListValue()).thenReturn(LISTPRICE);
		when(lpNspQty2RetB.getListValue()).thenReturn(LISTPRICE);
		when(msrpNLpQty2RetA.getListValue()).thenReturn(LISTPRICE);
		when(lpQty1RetB.getListValue()).thenReturn(LISTPRICE);
		when(all3PriceQty1RetA.getSaleValue()).thenReturn(SALEPRICE);
		when(lpNspQty2RetB.getSaleValue()).thenReturn(SALEPRICE);
		when(msrpNLpQty2RetA.getSaleValue()).thenReturn(null);
		when(lpQty1RetB.getSaleValue()).thenReturn(null);
		when(product1.getMsrp()).thenReturn(MSRP);
		when(product2.getMsrp()).thenReturn(null);
		when(product3.getMsrp()).thenReturn(MSRP);
		when(product4.getMsrp()).thenReturn(null);
		when(product1.getShop()).thenReturn(retailerA);
		when(product2.getShop()).thenReturn(retailerB);
		when(product3.getShop()).thenReturn(retailerA);
		when(product4.getShop()).thenReturn(retailerB);
		when(retailerA.getId()).thenReturn("retailerA");
		when(retailerB.getId()).thenReturn("retailerB");
		entries = new ArrayList();
		entries.add(all3PriceQty1RetA);
		entries.add(lpNspQty2RetB);
		entries.add(msrpNLpQty2RetA);
		entries.add(lpQty1RetB);
		when(cart.getEntries()).thenReturn(entries);
	}


	/**
	 *
	 */
	@Test
	public void testYouSave()
	{

		when(orderEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getMsrp()).thenReturn(2.0d);
		when(orderEntryModel.getSaleValue()).thenReturn(1.0d);
		assertEquals(1, retailerService.getYouSave(orderEntryModel).intValue());


	}

	/**
	 *
	 */
	@Test
	public void testYouSaveMsrpNull()
	{

		when(orderEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getMsrp()).thenReturn(null);
		when(orderEntryModel.getListValue()).thenReturn(2.0d);
		when(orderEntryModel.getSaleValue()).thenReturn(null);
		assertEquals(0, retailerService.getYouSave(orderEntryModel).intValue());
	}

	/**
	 *
	 */
	@Test
	public void testYouSaveSaleValueNull()
	{

		when(orderEntryModel.getProduct()).thenReturn(productModel);
		when(productModel.getMsrp()).thenReturn(2.0d);
		when(orderEntryModel.getListValue()).thenReturn(1.0d);
		when(orderEntryModel.getSaleValue()).thenReturn(null);
		assertEquals(1, retailerService.getYouSave(orderEntryModel).intValue());

	}

	@Test
	public void getRetailerSubTotalForCart()
	{
		final Map<String, Double> retailersPrice = retailerService.getRetailerSubBagForCart(cart);
		assertEquals(retailersPrice.size(), 2);
		assertEquals(RETAILE_B_SUBBAG_TOTAL, retailersPrice.get("retailerB"), 0.0);
		assertEquals(RETAILE_A_SUBBAG_TOTAL, retailersPrice.get("retailerA"), 0.0);

	}

	@Test
	public void getRetailerSubBagSaving_WithoutPromotion_retailerA()
	{
		final Double retailerSaving = retailerService.getRetailerSubBagSaving(cart, "retailerA");
		assertEquals(RETAILER_A_ITEM_SAVING, retailerSaving, 0.0);
	}

	@Test
	public void getRetailerSubBagSaving_WithPromotion_retailerA()
	{
		final Map<String, Double> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerA", 3d);
		when(cart.getRetailersTotalDiscount()).thenReturn(retailerDiscountMap);
		final Double retailerSaving = retailerService.getRetailerSubBagSaving(cart, "retailerA");
		assertEquals(RETAILER_A_ITEM_SAVING + PROMOTION, retailerSaving, 0.0);
	}

	@Test
	public void getRetailerSubBagSaving_WithoutPromotion_retailerB()
	{
		final Double retailerSaving = retailerService.getRetailerSubBagSaving(cart, "retailerB");
		assertEquals(RETAILER_B_ITEM_SAVING, retailerSaving, 0.0);
	}

	@Test
	public void getRetailerSubBagSaving_WithPromotion_retailerB()
	{

		final Map<String, Double> retailerDiscountMap = new HashMap<>();
		retailerDiscountMap.put("retailerB", 3d);
		when(cart.getRetailersTotalDiscount()).thenReturn(retailerDiscountMap);
		final Double retailerSaving = retailerService.getRetailerSubBagSaving(cart, "retailerB");
		assertEquals(RETAILER_B_ITEM_SAVING + PROMOTION, retailerSaving, 0.0);
	}


}
