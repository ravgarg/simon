package com.simon.core.populators;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.solrfacetsearch.search.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.ImagesData;




/**
 * The Class ProductImageDataPopulatorTest tests the methods of ProductImageDataPopulator.
 */
@UnitTest
public class ProductImageDataPopulatorTest
{
	@InjectMocks
	private ProductImageDataPopulator productImageDataPopulator;
	@Mock
	private Document document;
	private final Map<String, ImagesData> images = new HashMap<String, ImagesData>();

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPopulateWhenDocumentHasImages()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");

		imagesFromSolr.add("84Wx128H_alternate1|/images/84Wx128H/2-thumb.jpg");
		imagesFromSolr.add("580Wx884H_alternate1|/images/default/2-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_alternate1|/images/zoom/2-zoom.jpg");
		imagesFromSolr.add("325Wx495H_alternate1|/images/mobile/2-mob-thumb.jpg");

		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("designer");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("name");
		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getMain());
		Assert.assertNull(images.get("default").getThumbnail());
		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getZoom());
		Assert.assertEquals("/images/mobile/1-mob-thumb.jpg", images.get("default").getMobile());

		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getMain());
		Assert.assertNull(images.get("alternate1").getThumbnail());
		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getZoom());
		Assert.assertEquals("/images/mobile/2-mob-thumb.jpg", images.get("alternate1").getMobile());
	}

	@Test
	public void testPopulateWhenDocumentHasNullThumbnailImage()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");

		imagesFromSolr.add("580Wx884H_alternate1|/images/default/2-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_alternate1|/images/zoom/2-zoom.jpg");
		imagesFromSolr.add("325Wx495H_alternate1|/images/mobile/2-mob-thumb.jpg");

		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("designer");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("name");
		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getMain());
		Assert.assertNull(images.get("default").getThumbnail());
		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getZoom());
		Assert.assertEquals("/images/mobile/1-mob-thumb.jpg", images.get("default").getMobile());

		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getMain());
		Assert.assertNull(images.get("alternate1").getThumbnail());
		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getZoom());
		Assert.assertEquals("/images/mobile/2-mob-thumb.jpg", images.get("alternate1").getMobile());
	}

	@Test
	public void testPopulateWhenDocumentHasNullZoomImage()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");

		imagesFromSolr.add("84Wx128H_alternate1|/images/84Wx128H/2-thumb.jpg");
		imagesFromSolr.add("580Wx884H_alternate1|/images/default/2-1x.jpg");
		imagesFromSolr.add("325Wx495H_alternate1|/images/mobile/2-mob-thumb.jpg");

		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("designer");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("name");
		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertNull(images.get("default").getMain());
		Assert.assertNull(images.get("default").getThumbnail());
		Assert.assertNull(images.get("default").getZoom());
		Assert.assertEquals("/images/mobile/1-mob-thumb.jpg", images.get("default").getMobile());

		Assert.assertNull(images.get("alternate1").getMain());
		Assert.assertNull(images.get("alternate1").getThumbnail());
		Assert.assertNull(images.get("alternate1").getZoom());
		Assert.assertEquals("/images/mobile/2-mob-thumb.jpg", images.get("alternate1").getMobile());
	}

	@Test
	public void testPopulateWhenDocumentHasNullMainImage()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");

		imagesFromSolr.add("84Wx128H_alternate1|/images/84Wx128H/2-thumb.jpg");
		imagesFromSolr.add("580Wx884H*2_alternate1|/images/zoom/2-zoom.jpg");
		imagesFromSolr.add("325Wx495H_alternate1|/images/mobile/2-mob-thumb.jpg");

		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("designer");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("name");
		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getMain());
		Assert.assertNull(images.get("default").getThumbnail());
		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getZoom());
		Assert.assertEquals("/images/mobile/1-mob-thumb.jpg", images.get("default").getMobile());

		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getMain());
		Assert.assertNull(images.get("alternate1").getThumbnail());
		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getZoom());
		Assert.assertEquals("/images/mobile/2-mob-thumb.jpg", images.get("alternate1").getMobile());
	}

	@Test
	public void testPopulateWhenDocumentHasNullMobileImage()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");

		imagesFromSolr.add("84Wx128H_alternate1|/images/84Wx128H/2-thumb.jpg");
		imagesFromSolr.add("580Wx884H_alternate1|/images/default/2-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_alternate1|/images/zoom/2-zoom.jpg");

		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);

		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getMain());
		Assert.assertNull(images.get("default").getThumbnail());
		Assert.assertEquals("/images/zoom/1-zoom.jpg", images.get("default").getZoom());
		Assert.assertNull(images.get("default").getMobile());

		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getMain());
		Assert.assertNull(images.get("alternate1").getThumbnail());
		Assert.assertEquals("/images/zoom/2-zoom.jpg", images.get("alternate1").getZoom());
		Assert.assertNull(images.get("alternate1").getMobile());
	}

	@Test
	public void testPopulateWhenDocumentHasNoImages()
	{
		final List<String> imagesFromSolr = new ArrayList<String>();
		when(document.getFieldValue("images")).thenReturn(imagesFromSolr);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESIGNER_NAME)).thenReturn("designer");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("name");
		when((String) document.getFieldValue(SimonCoreConstants.PRODUCT_COLOR_TITLE)).thenReturn("color");
		productImageDataPopulator.populate(document, images);

		Assert.assertEquals(images.size(), 1);
	}
}
