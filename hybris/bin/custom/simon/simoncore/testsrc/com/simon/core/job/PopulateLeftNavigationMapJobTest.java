package com.simon.core.job;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.services.LeftNavigationCategoryService;


@UnitTest
public class PopulateLeftNavigationMapJobTest
{
	@InjectMocks
	private PopulateLeftNavigationMapJob populateLeftNavigationMapJob;

	@Mock
	private CronJobModel cronjob;

	@Mock
	private LeftNavigationCategoryService leftNavigationCategoryService;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testMapCreationSuccessful()
	{
		when(leftNavigationCategoryService.createMap()).thenReturn(true);
		final PerformResult result = populateLeftNavigationMapJob.perform(cronjob);
		Assert.assertEquals(CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals(CronJobStatus.FINISHED, result.getStatus());
	}

	@Test
	public void testMapCreationNotSuccessful()
	{
		when(leftNavigationCategoryService.createMap()).thenReturn(false);
		final PerformResult result = populateLeftNavigationMapJob.perform(cronjob);
		Assert.assertEquals(CronJobResult.ERROR, result.getResult());
		Assert.assertEquals(CronJobStatus.ABORTED, result.getStatus());
	}
}
