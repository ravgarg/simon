package com.simon.core.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.constants.SimonFlexiConstants;


/**
 * Test class for ExtMediaContainerDaoImpl.
 */
@UnitTest
public class ExtMediaContainerDaoImplTest
{
	@InjectMocks
	@Spy
	ExtMediaContainerDaoImpl extMediaContainerDaoImpl;
	@Mock
	private FlexibleSearchQuery query;
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private List<Object> results;
	@Mock
	private SearchResult searchResult;

	private Date cleanupAgeDate;
	@Mock
	private List<MediaContainerModel> listOfMediaContainer;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		doReturn(query).when(extMediaContainerDaoImpl)
				.getFlexibleSearchQuery(SimonFlexiConstants.FIND_ALL_MEDIA_CONTAINER_WHERE_VERSION_GT_ZERO);
		cleanupAgeDate = new Date();
		final MediaContainerModel mediaContainerModel = new MediaContainerModel();
		listOfMediaContainer = new ArrayList<>();
		listOfMediaContainer.add(mediaContainerModel);
	}

	/**
	 * test for getMediaContainersWithVersionGreaterThenZero method
	 */
	@Test
	public void getMediaContainersWithVersionGreaterThenZeroTest()
	{
		extMediaContainerDaoImpl.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		when(searchResult.getResult()).thenReturn(listOfMediaContainer);
		extMediaContainerDaoImpl.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate);
		assertEquals(listOfMediaContainer, extMediaContainerDaoImpl.getMediaContainersWithVersionGreaterThenZero(cleanupAgeDate));
	}


}
