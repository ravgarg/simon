package com.simon.core.provider.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.impl.DefaultPriceService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.price.DetailedPriceInfo;
import com.simon.core.price.jalo.ExtPriceValue;
import com.simon.core.services.price.ExtPriceService;


@UnitTest
public class VariantPriceValueResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	private VariantPriceValueResolver variantPriceValueResolver;
	@Mock
	private DefaultPriceService defaultPriceService;

	@Mock
	private ExtPriceService priceService;

	@Mock
	private PriceInformation priceinfo;

	@Mock
	private ExtPriceValue extPriceValue;
	private Collection<IndexedProperty> indexedProperties;
	private GenericVariantProductModel model;
	private Map<DetailedPriceInfo, Object> priceDetail;
	public static final String FETCH_STRIKEOFF_PRICE_PARAM = "fetchPriceWithStrikeOff";

	@Before
	public void setup()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(FETCH_STRIKEOFF_PRICE_PARAM, "true");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		indexedProperties = Collections.singletonList(indexedProperty);
		model = new GenericVariantProductModel();
		model.setMsrp(50.0);
		priceDetail = new HashMap<>();
		priceDetail.put(DetailedPriceInfo.IS_LP_STRIKEOFF, false);
		priceDetail.put(DetailedPriceInfo.IS_MSRP_STRIKEOFF, true);
		priceDetail.put(DetailedPriceInfo.PERCENTAGE_SAVING, 96);
		when(priceService.getDetailedPriceInformation(anyDouble(), anyDouble(), anyDouble())).thenReturn(priceDetail);
		final List<PriceInformation> productPrice = new ArrayList<>();
		when(extPriceValue.getListPrice()).thenReturn(48.0);
		when(extPriceValue.getValue()).thenReturn(40.0);
		when(priceinfo.getPriceValue()).thenReturn(extPriceValue);
		productPrice.add(priceinfo);
		when(defaultPriceService.getPriceInformationsForProduct(model)).thenReturn(productPrice);

	}

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(4)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithListPriceNotGreaterThanZero() throws FieldValueProviderException
	{
		when(extPriceValue.getListPrice()).thenReturn(0.0);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(3)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithMSRPZero() throws FieldValueProviderException
	{
		model.setMsrp(0.0);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(3)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithNullMSRP() throws FieldValueProviderException
	{
		model.setMsrp(null);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(3)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithEmptyProductPrice() throws FieldValueProviderException
	{
		when(defaultPriceService.getPriceInformationsForProduct(model)).thenReturn(new ArrayList<>());
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(0)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithFetchStrikeOffPriceFalseAndNonZeroSalePrice() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(FETCH_STRIKEOFF_PRICE_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithFetchStrikeOffPriceFalseAndZeroSalePrice() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(FETCH_STRIKEOFF_PRICE_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		when(extPriceValue.getValue()).thenReturn(0.0);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test
	public void testResolveWithZeroPercentSavings() throws FieldValueProviderException
	{
		priceDetail.put(DetailedPriceInfo.PERCENTAGE_SAVING, 0);
		variantPriceValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(3)).addField(any(IndexedProperty.class), any(), any(String.class));
	}
}
