package com.simon.core.services.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.Tenant;
import de.hybris.platform.jalo.JaloConnection;
import de.hybris.platform.persistence.security.PasswordEncoder;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.configuration.Configuration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.simon.core.dao.impl.ExtWhitelistedLoginDaoImpl;
import com.simon.core.model.WhitelistedUserModel;


/**
 * The Class ExtWhitelistedLoginServiceTest. Unit test for {@link ExtWhitelistedLoginServiceImpl}}
 */
@UnitTest
public class ExtWhitelistedLoginServiceTest
{

	/** The white listed login service. */
	@InjectMocks
	@Spy
	private ExtWhitelistedLoginServiceImpl extWhitelistedLoginService;
	@Mock
	private ExtWhitelistedLoginDaoImpl extWhitelistedLogindao;
	@Mock
	private Tenant currentTenant;
	@Mock
	private JaloConnection jaloConnection;
	@Mock
	private PasswordEncoder passwordEncoder;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
		when(configurationService.getConfiguration()).thenReturn(configuration);

	}


	/**
	 * Verify email/password is passed to dao correctly for search.
	 */
	@Test
	public void verifyUserReturnedFromDaoIsReturnedCorrectly()
	{
		final WhitelistedUserModel model = new WhitelistedUserModel();
		model.setPasswordEncoding("md5");
		model.setEmailId("testcv@gmail.com");
		model.setPassword("123456@P2");
		when(configuration.getBoolean("simon.whitelisting.password.disable")).thenReturn(false);
		when(extWhitelistedLogindao.findUserByEmail("testcv@gmail.com")).thenReturn(model);
		Mockito.doReturn(currentTenant).when(extWhitelistedLoginService).getCurrentTenant();
		when(currentTenant.getJaloConnection()).thenReturn(jaloConnection);
		when(jaloConnection.getPasswordEncoder("md5")).thenReturn(passwordEncoder);
		when(passwordEncoder.check(model.getEmailId(), model.getPassword(), "123456@P2")).thenReturn(true);
		assertThat(extWhitelistedLoginService.findUserByEmailAndPassword("testcv@gmail.com", "123456@P2"), is(model));
	}

	/**
	 * Verify email/password is passed to dao correctly for search.
	 */
	@Test
	public void verifyUserReturnedWhenPasswordDisabled()
	{
		final WhitelistedUserModel model = new WhitelistedUserModel();
		model.setPasswordEncoding("md5");
		model.setEmailId("testcv@gmail.com");
		model.setPassword("123456@P2");
		when(extWhitelistedLogindao.findUserByEmail("testcv@gmail.com")).thenReturn(model);
		when(configuration.getBoolean("simon.whitelisting.password.disable")).thenReturn(true);
		assertThat(extWhitelistedLoginService.findUserByEmailAndPassword("testcv@gmail.com", "123456@P2"), is(model));
	}

	@Test
	public void verifyUserReturnedFromDaoIncorrectPassword()
	{
		final WhitelistedUserModel model = new WhitelistedUserModel();
		model.setPasswordEncoding("md5");
		model.setEmailId("testcv@gmail.com");
		model.setPassword("123456@P2");
		when(configuration.getBoolean("simon.whitelisting.password.disable")).thenReturn(false);
		when(extWhitelistedLogindao.findUserByEmail("testcv@gmail.com")).thenReturn(model);
		Mockito.doReturn(currentTenant).when(extWhitelistedLoginService).getCurrentTenant();
		when(currentTenant.getJaloConnection()).thenReturn(jaloConnection);
		when(jaloConnection.getPasswordEncoder("md5")).thenReturn(passwordEncoder);
		when(passwordEncoder.check(model.getEmailId(), model.getPassword(), "123456@P2")).thenReturn(false);
		Assert.assertEquals(extWhitelistedLoginService.findUserByEmailAndPassword("testcv@gmail.com", "123456@P2"), null);
	}
}
