package com.simon.core.keygenerators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * MediaKeyGenerator unit test for {@link MediaKeyGenerator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MediaKeyGeneratorTest
{

	@InjectMocks
	private MediaKeyGenerator mediaKeyGenerator;
	@Mock
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Mock
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	/**
	 * Verify exception thrown for generate without argument.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForGenerateWithoutArgument()
	{
		mediaKeyGenerator.generate();
	}

	/**
	 * Verify exception thrown for reset.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForReset()
	{
		mediaKeyGenerator.reset();
	}

	/**
	 * Verify code generated correctly.
	 */
	@Test
	public void verifyCodeGeneratedCorrectly()
	{
		when(variantCategoryKeyGenerator.generateFor("color")).thenReturn("color");
		when(variantValueCategoryKeyGenerator.generateFor("British Tan")).thenReturn("VVC-british-tan");
		assertThat(mediaKeyGenerator.generateFor("color=British Tan"), is("VVC-british-tan-color_thumbnail"));
	}
}
