package com.simon.core.strategies.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.OrderManager;
import de.hybris.platform.jalo.order.price.PriceFactory;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.strategies.calculation.impl.FindPricingWithCurrentPriceFactoryStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.PriceValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.facades.product.OfferFacade;
import com.simon.core.model.AdditionalCartInfoModel;


/*
 * This class tests the methods of Class {@link SimonFindPricingWithCurrentPriceFactoryStrategy}.
 */
@UnitTest
public class SimonFindPricingWithCurrentPriceFactoryStrategyTest
{

	@InjectMocks
	@Spy
	private SimonFindPricingWithCurrentPriceFactoryStrategy simonFindPricingWithCurrentPriceFactoryStrategy;
	@Mock
	FindPricingWithCurrentPriceFactoryStrategy findPricingWithCurrentPriceFactoryStrategy;

	@Mock
	private OfferFacade offerFacade;
	@Mock
	private AbstractOrderEntryModel entry;
	@Mock
	private AbstractOrderModel order;
	@Mock
	private CurrencyModel currency;
	@Mock
	private AdditionalCartInfoModel cart;
	@Mock
	private ModelService modelService;
	@Mock
	private PriceValue priceValue;
	@Mock
	private PriceFactory priceFactory;
	@Mock
	private OrderManager orderManager;
	private AbstractOrderEntry entryItem;
	private final ProductModel product = new ProductModel();
	private final List<OfferData> offers = new ArrayList<OfferData>();
	private final OfferData offer = new OfferData();
	private final PriceData price = new PriceData();
	private final double value = 24;

	/*
	 * setup method.
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	/*
	 * Test find base price when two tap price and offer are null.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceAndOfferAreNull() throws CalculationException
	{
		setEntryOrder(currency);
		assertEquals(priceValue, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry));
	}

	/*
	 * Test find base price when two tap price and offer are null curr null.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceAndOfferAreNullCurrNull() throws CalculationException
	{
		setEntryOrder(null);
		assertEquals(priceValue, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry));
	}

	/*
	 * Test find base price when two tap price and offer are not null.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceAndOfferAreNotNull() throws CalculationException
	{
		setCurrencyAndPrice();
		when(entry.getProduct()).thenReturn(product);
		when(offerFacade.getOffersForProductCode("11001")).thenReturn(offers);
		when(modelService.getSource(entry)).thenReturn(entryItem);
		doReturn(priceValue).when(simonFindPricingWithCurrentPriceFactoryStrategy).getBasePrice(entry);
		assertEquals(value, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry).getValue(), 0.0);
	}

	/*
	 * Test find base price when two tap price is not null.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceIsNotNull() throws CalculationException
	{
		product.setCode("11001");
		when(entry.getOrder()).thenReturn(order);
		when(order.getCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");
		when(order.getAdditionalCartInfo()).thenReturn(cart);
		when(entry.getAdditionalProductBasePrice()).thenReturn("$24");
		assertEquals(24d, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry).getValue(), 0.0);
	}

	/*
	 * Test find base price when two tap price is empty.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceIsEmpty() throws CalculationException
	{
		setCurrencyAndPrice();
		when(entry.getAdditionalProductBasePrice()).thenReturn("");
		when(entry.getProduct()).thenReturn(product);
		when(offerFacade.getOffersForProductCode("11001")).thenReturn(offers);
		when(modelService.getSource(entry)).thenReturn(entryItem);
		doReturn(priceValue).when(simonFindPricingWithCurrentPriceFactoryStrategy).getBasePrice(entry);
		assertEquals(priceValue, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry));
	}

	/*
	 * Test find base price when two tap price is zero.
	 *
	 */
	@Test
	public void testFindBasePriceWhenTwoTapPriceIsZero() throws CalculationException
	{
		setCurrencyAndPrice();
		when(entry.getAdditionalProductBasePrice()).thenReturn("$0");
		when(entry.getProduct()).thenReturn(product);
		when(offerFacade.getOffersForProductCode("11001")).thenReturn(offers);
		when(modelService.getSource(entry)).thenReturn(entryItem);
		doReturn(priceValue).when(simonFindPricingWithCurrentPriceFactoryStrategy).getBasePrice(entry);
		assertEquals(priceValue, simonFindPricingWithCurrentPriceFactoryStrategy.findBasePrice(entry));
	}

	/*
	 * Sets the entry order.
	 */
	private void setEntryOrder(final CurrencyModel currency) throws CalculationException
	{
		product.setCode("11001");
		when(entry.getOrder()).thenReturn(order);
		when(order.getCurrency()).thenReturn(currency);
		if (null != currency)
		{
			when(currency.getIsocode()).thenReturn("USD");
		}
		when(order.getAdditionalCartInfo()).thenReturn(cart);
		when(entry.getProduct()).thenReturn(product);
		when(offerFacade.getOffersForProductCode("11001")).thenReturn(offers);
		when(modelService.getSource(entry)).thenReturn(entryItem);
		doReturn(priceValue).when(simonFindPricingWithCurrentPriceFactoryStrategy).getBasePrice(entry);
	}

	/*
	 * Sets the currency and price.
	 */
	private void setCurrencyAndPrice()
	{
		product.setCode("11001");
		offers.add(offer);
		offer.setPrice(price);
		price.setValue(BigDecimal.valueOf(value));
		when(entry.getOrder()).thenReturn(order);
		when(order.getCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");
		when(order.getAdditionalCartInfo()).thenReturn(cart);
	}
}
