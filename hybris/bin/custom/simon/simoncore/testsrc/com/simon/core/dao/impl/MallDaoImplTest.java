package com.simon.core.dao.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.simon.core.dao.MallDao;
import com.simon.core.model.MallModel;


/**
 * The Class DefaultDesignerDaoTest. Integration test for {@link DefaultDesignerDao}
 */
@IntegrationTest
public class MallDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource
	private MallDao mallDao;


	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testMall.impex", "utf-8");
	}

	/**
	 * Verify mall is returned given code.
	 */
	@Test
	public void verifyMallReturnedCode()
	{
		final MallModel actual = mallDao.findMallByCode("testPremiumMall");
		assertNotNull(actual);
		Assert.assertEquals("testBusinessURL", actual.getBusinessFacingURL());
		Assert.assertEquals("testShopperFacingURL", actual.getShopperFacingURL());
	}

	/**
	 * Verify null is returned for invalid code.
	 */
	@Test
	public void verifyNullDesignerIsReturnedForInvalidCode()
	{
		final MallModel actual = mallDao.findMallByCode("invalidCode");
		assertNull(actual);
	}

	/**
	 * Method to test findAllMalls() in valid list case.
	 */
	@Test
	public void testFindAllMalls()
	{
		final List<MallModel> result = mallDao.findAllMalls();
		Assert.assertTrue(!result.isEmpty());
	}

	/**
	 * Method to test findMallListForCode() in valid list case.
	 */
	@Test
	public void testFindMallListForCode()
	{
		final List<String> mallCodes = new ArrayList<>();
		mallCodes.add("testPremiumMall");
		mallCodes.add("testVipMall");

		final Set<MallModel> result = mallDao.findMallListForCode(mallCodes);
		Assert.assertTrue(!result.isEmpty());
	}

}
