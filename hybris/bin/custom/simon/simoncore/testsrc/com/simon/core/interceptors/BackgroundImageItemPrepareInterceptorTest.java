package com.simon.core.interceptors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.BackgroundImageItemModel;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class BackgroundImageItemPrepareInterceptorTest
{
	@InjectMocks
	private BackgroundImageItemPrepareInterceptor backgroundImageItemPrepareInterceptor;
	@Mock
	private InterceptorContext interceptorContext;


	BackgroundImageItemModel backgroundImageItem;
	MediaModel desktopMedia;

	/**
	 *
	 */
	@Before
	public void setup()
	{
		backgroundImageItem = new BackgroundImageItemModel();
		desktopMedia = new MediaModel();
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * @throws InterceptorException
	 *
	 */
	@Test
	public void testOnPrepareWithMobileMediaNull() throws InterceptorException
	{
		backgroundImageItem.setDesktopImage(desktopMedia);
		backgroundImageItemPrepareInterceptor.onPrepare(backgroundImageItem, interceptorContext);
		assertEquals(desktopMedia, backgroundImageItem.getMobileImage());
	}

	/**
	 * @throws InterceptorException
	 *
	 */
	@Test
	public void testOnPrepareWithMobileMediaNotNull() throws InterceptorException
	{
		backgroundImageItem.setDesktopImage(desktopMedia);

		final MediaModel mobileMedia = new MediaModel();
		backgroundImageItem.setMobileImage(mobileMedia);

		backgroundImageItemPrepareInterceptor.onPrepare(backgroundImageItem, interceptorContext);

		assertNotEquals(desktopMedia, backgroundImageItem.getMobileImage());
	}

	/**
	 * @throws InterceptorException
	 *
	 */
	@Test
	public void testOnPrepareWithDesktopMediaNull() throws InterceptorException
	{
		backgroundImageItemPrepareInterceptor.onPrepare(backgroundImageItem, interceptorContext);
		assertNull(backgroundImageItem.getMobileImage());
	}

}
