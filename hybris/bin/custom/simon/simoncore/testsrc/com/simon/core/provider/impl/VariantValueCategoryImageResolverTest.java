package com.simon.core.provider.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.StubLocaleProvider;
import de.hybris.platform.servicelayer.internal.model.impl.LocaleProvider;
import de.hybris.platform.servicelayer.model.ItemModelContextImpl;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;


@UnitTest
public class VariantValueCategoryImageResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	VariantValueCategoryImageResolver variantValueCategoryImageResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	@Mock
	private MediaModel thumbnail;
	@Mock
	private GenericVariantProductModel model;
	public static final String OPTIONAL_PARAM = "optional";
	public static final String ATTRIBUTE_PARAM = "attribute";

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		indexedProperty.setName("Name");
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		Mockito.when(model.getSwatchColorMedia()).thenReturn(thumbnail);
		Mockito.when(thumbnail.getURL()).thenReturn("URL");
		variantValueCategoryImageResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}


	@Test
	public void testResolveIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = setGenericVariantProductModel();
		Mockito.when(thumbnail.getURL()).thenReturn(null);
		variantValueCategoryImageResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
	}


	@Test
	public void testResolveNullVariantValueCategories() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = setGenericVariantProductModel();
		model.getSupercategories().stream().forEach(categoryModel -> categoryModel.setThumbnail(null));
		variantValueCategoryImageResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}


	@Test
	public void testResolverVariantValueCategoriesWithoutThumbnail() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = setGenericVariantProductModel();
		model.getSupercategories().stream()
				.filter(categoryModel -> categoryModel.getSupercategories().get(0).getCode().equals("color"))
				.forEach(categoryModel -> categoryModel.getSupercategories().get(0).setCode("NotColor"));
		variantValueCategoryImageResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	private GenericVariantProductModel setGenericVariantProductModel()
	{
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final List<CategoryModel> variantValueCategories = new ArrayList<>();
		final CategoryModel cat = new CategoryModel();
		final LocaleProvider localeProvider = new StubLocaleProvider(Locale.ENGLISH);
		final ItemModelContextImpl itemModelContext = (ItemModelContextImpl) cat.getItemModelContext();
		itemModelContext.setLocaleProvider(localeProvider);
		Mockito.when(thumbnail.getURL()).thenReturn("URL");
		cat.setThumbnail(thumbnail);
		final CategoryModel superCat = new CategoryModel();
		final ItemModelContextImpl itemModelContext2 = (ItemModelContextImpl) superCat.getItemModelContext();
		itemModelContext2.setLocaleProvider(localeProvider);
		superCat.setCode("color");
		final List<CategoryModel> supercats = new ArrayList<>();
		supercats.add(superCat);
		cat.setSupercategories(supercats);
		variantValueCategories.add(cat);
		model.setSupercategories(variantValueCategories);
		return model;
	}

}
