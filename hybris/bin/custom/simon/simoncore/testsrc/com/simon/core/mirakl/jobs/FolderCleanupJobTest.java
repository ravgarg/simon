
package com.simon.core.mirakl.jobs;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cronjob.constants.GeneratedCronJobConstants.Enumerations.CronJobResult;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.model.FolderCleanupCronJobModel;


/**
 * This class test FolderCleanupJobTest class.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class FolderCleanupJobTest
{
	@InjectMocks
	private FolderCleanupJob folderCleanupJob;

	@Mock
	private ConfigurationService configurationService;
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private FolderCleanupCronJobModel folderCleanupCronJobModel;
	private File output;


	/**
	 * this method is called to set up basic things.
	 *
	 *
	 * @throws IOException
	 */
	@Before
	public void setup() throws IOException
	{
		folderCleanupCronJobModel = mock(FolderCleanupCronJobModel.class);
		output = temporaryFolder.newFile("output.txt");
		output.setLastModified(1520500019065L);
		when(folderCleanupCronJobModel.getSubBatchSize()).thenReturn(500);
		when(folderCleanupCronJobModel.getMaxFileInOneBatch()).thenReturn(10000);
		when(folderCleanupCronJobModel.getNumberOfWorkers()).thenReturn(1);
		when(folderCleanupCronJobModel.getCleanupAgeInHours()).thenReturn(24);
		when(folderCleanupCronJobModel.getFilesDirectory()).thenReturn(temporaryFolder.getRoot().getPath());
	}

	/**
	 * This method test when we have files available to delete.
	 */
	@Test
	public void verifyDeleteOfAllFile()
	{
		final PerformResult actual = folderCleanupJob.perform(folderCleanupCronJobModel);
		assertThat(output.exists(), is(Boolean.FALSE));
		assertThat(actual.getResult().getCode(), is(CronJobResult.SUCCESS));
	}

	/**
	 * this method test directory does not exist.
	 */
	@Test
	public void verifyIfDirectoryDoesNotExist()
	{
		final PerformResult actual = folderCleanupJob.perform(folderCleanupCronJobModel);
		assertThat(actual.getResult().getCode(), is(CronJobResult.SUCCESS));
	}


}
