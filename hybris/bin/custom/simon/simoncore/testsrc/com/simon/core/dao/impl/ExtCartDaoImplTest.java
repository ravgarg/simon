package com.simon.core.dao.impl;

import static org.mockito.Mockito.doReturn;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonFlexiConstants;


/**
 * The Class ExtCartDaoImplTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCartDaoImplTest
{

	@InjectMocks
	@Spy
	private ExtCartDaoImpl extCartDaoImpl;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private SearchResult searchResult;

	@Mock
	private FlexibleSearchQuery query;

	private final String cartId = "cartId";


	/**
	 *
	 */
	@Before
	public void setup()
	{
		doReturn(query).when(extCartDaoImpl).getFlexibleSearchQuery(SimonFlexiConstants.TWOTAP_CART_BY_CART_ID_QUERY);
	}


	/**
	 *
	 */
	@Test
	public void getCartByCartIdTest()
	{
		extCartDaoImpl.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		extCartDaoImpl.getCartByCartId(cartId);
	}

	/**
	 *
	 */
	@Test
	public void getCartByCartIdTestsearchResult()
	{
		extCartDaoImpl.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		doReturn(new ArrayList<>()).when(searchResult).getResult();
		extCartDaoImpl.getCartByCartId(cartId);
	}
}
