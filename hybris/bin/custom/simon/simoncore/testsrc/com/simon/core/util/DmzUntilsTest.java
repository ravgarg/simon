package com.simon.core.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;

import com.simon.core.exceptions.SystemException;


/**
 * The Class DmzUntilsTest. Unit test for {@link DmzUntils}.
 */
@UnitTest
public class DmzUntilsTest
{

	/** The mock server. */
	private static ClientAndServer mockServer;

	/**
	 * Returns following json response as mock response from webserver. {@code} { "tunnels": [{ "name": "command_line
	 * (http)", "uri": "/api/tunnels/command_line+%28http%29", "public_url": "http://17733131.ngrok.io", "proto": "http",
	 * "config": { "addr": "localhost:9001", "inspect": true } }], "uri": "/api/tunnels" } } }
	 */
	private final String mockValidResponse = "{" + "\"tunnels\": [{" + "\"name\": \"command_line (http)\","
			+ "\"uri\": \"/api/tunnels/command_line+%28http%29\"," + "\"public_url\": \"http://17733131.ngrok.io\","
			+ "\"proto\": \"http\"," + "\"config\": {" + "\"addr\": \"localhost:9001\"," + "\"inspect\": true"
			+ "}}],\"uri\":\"/api/tunnels\"}";

	/**
	 * Returns following json response as mock response from webserver. {@code} { "tunnels": [{ "name": "command_line
	 * (http)", "uri": "/api/tunnels/command_line+%28http%29", "public_url": "http://17733131.ngrok.io", "proto":
	 * "non-http", "config": { "addr": "localhost:9001", "inspect": true } }], "uri": "/api/tunnels" } } }
	 */
	private final String mockValidResponseNonHttpProto = "{" + "\"tunnels\": [{" + "\"name\": \"command_line (http)\","
			+ "\"uri\": \"/api/tunnels/command_line+%28http%29\"," + "\"public_url\": \"http://17733131.ngrok.io\","
			+ "\"proto\": \"non-http\"," + "\"config\": {" + "\"addr\": \"localhost:9001\"," + "\"inspect\": true"
			+ "}}],\"uri\":\"/api/tunnels\"}";

	/**
	 * Returns following json response as mock response from webserver. {@code} { "tunnels": [], "uri": "/api/tunnels" }
	 * } }
	 */
	private final String mockValidResponseEmptyTunnels = "{" + "\"tunnels\": [],\"uri\":\"/api/tunnels\"}";

	/**
	 * Returns following json response as mock response from webserver. {@code} { "tunnels": [{ "name": "command_line
	 * (http)", "uri": "/api/tunnels/command_line+%28http%29", "public_url": "http://17733131.ngrok.io", "proto": "http",
	 * "config": { "addr": "localhost:9001", "inspect": true } }], "uri": "/api/tunnels" } } }
	 */
	private final String mockInvalidResponse = "{" + "\"tunnels\": [{" + "\"name\": \"command_line (http)\","
			+ "\"uri\": \"/api/tunnels/command_line+%28http%29\"," + "\"public_url\": \"http://17733131.ngrok.io\","
			+ "\"proto\": \"http\"," + ": {" + "\"addr\": \"localhost:9001\"," + "\"inspect\": true"
			+ "}}],\"uri\":\"/api/tunnels\"}";

	/**
	 * Start proxy.
	 */
	@Before
	public void startMockServer()
	{
		mockServer = ClientAndServer.startClientAndServer(4040);
	}

	/**
	 * Clear call back urls.
	 */
	@Before
	public void clearCallBackUrls()
	{
		DmzUntils.setPurchaseCallbackUrlConfirmed(null);
		DmzUntils.setPurchaseCallbackUrlUpdated(null);
	}


	/**
	 * Verify when ngrok is not running and returns empty response default url is returned.
	 */
	@Test
	public void verifyWhenNgrokRunningReturnsEmptyResponseDefaultUrlIsReturned()
	{
		mockServer.when(HttpRequest.request().withPath("/api/tunnels")).respond(HttpResponse.notFoundResponse());
		DmzUntils.setupCallbackUrls();
		assertThat(DmzUntils.getPurchaseCallbackUrlConfirmed(), is("localhost/simonwebservices/twotap/confirmed"));
		assertThat(DmzUntils.getPurchaseCallbackUrlUpdated(), is("localhost/simonwebservices/twotap/updated"));
	}

	/**
	 * Verify exception is thrown when ngrok is running and returns invalid response.
	 */
	@Test(expected = SystemException.class)
	public void verifyExceptionThrownWhenNgrokRunningAndReturnsInvalidResponse()
	{
		mockServer.when(HttpRequest.request().withPath("/api/tunnels"))
				.respond(HttpResponse.response().withBody(mockInvalidResponse));
		DmzUntils.setupCallbackUrls();
	}

	/**
	 * Verify when ngrok is running and returns valid response public url is returned.
	 */
	@Test
	public void verifyWhenNgrokRunningAndReturnsValidResponsePublicUrlIsReturned()
	{
		mockServer.when(HttpRequest.request().withPath("/api/tunnels"))
				.respond(HttpResponse.response().withBody(mockValidResponse));
		DmzUntils.setupCallbackUrls();
		assertThat(DmzUntils.getPurchaseCallbackUrlConfirmed(), is("http://17733131.ngrok.io/simonwebservices/twotap/confirmed"));
		assertThat(DmzUntils.getPurchaseCallbackUrlUpdated(), is("http://17733131.ngrok.io/simonwebservices/twotap/updated"));
	}

	/**
	 * Verify when ngrok is running and returns response with no tunnels default url is returned.
	 */
	@Test
	public void verifyWhenNgrokRunningAndReturnsResponseWithNoTunnelsDefaultUrlIsReturned()
	{
		mockServer.when(HttpRequest.request().withPath("/api/tunnels"))
				.respond(HttpResponse.response().withBody(mockValidResponseEmptyTunnels));
		DmzUntils.setupCallbackUrls();
		assertThat(DmzUntils.getPurchaseCallbackUrlConfirmed(), is("localhost/simonwebservices/twotap/confirmed"));
		assertThat(DmzUntils.getPurchaseCallbackUrlUpdated(), is("localhost/simonwebservices/twotap/updated"));
	}

	/**
	 * Verify when ngrok running and returns response with incorrect protocol default url is returned.
	 */
	@Test
	public void verifyWhenNgrokRunningAndReturnsResponseWithIncorrectProtocolDefaultUrlIsReturned()
	{
		mockServer.when(HttpRequest.request().withPath("/api/tunnels"))
				.respond(HttpResponse.response().withBody(mockValidResponseNonHttpProto));
		DmzUntils.setupCallbackUrls();
		assertThat(DmzUntils.getPurchaseCallbackUrlConfirmed(), is("localhost/simonwebservices/twotap/confirmed"));
		assertThat(DmzUntils.getPurchaseCallbackUrlUpdated(), is("localhost/simonwebservices/twotap/updated"));
	}

	/**
	 * Stop mock server.
	 */
	@After
	public void stopMockServer()
	{
		mockServer.stop();
	}

}
