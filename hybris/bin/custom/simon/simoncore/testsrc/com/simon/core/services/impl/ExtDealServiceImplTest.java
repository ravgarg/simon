package com.simon.core.services.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.dao.impl.ExtDealDaoImpl;
import com.simon.core.model.DealModel;


/**
 * The Class DefaultDealServiceTest. Unit test for {@link ExtDealServiceImpl}}
 */
@UnitTest
public class ExtDealServiceImplTest
{

	/** The default Deal service. */
	@InjectMocks
	private ExtDealServiceImpl extDealService;

	/** The Deal dao. */
	@Mock
	private ExtDealDaoImpl extDealDao;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyDealReturnedFromDaoIsReturnedCorrectly()
	{
		final DealModel dealModel = mock(DealModel.class);
		when(extDealDao.findDealByCode("USPOLO")).thenReturn(dealModel);
		assertThat(extDealService.getDealForCode("USPOLO"), is(dealModel));
	}

	/**
	 * Test to find list of all deals.
	 */
	@Test
	public void testFindListOfDeals()
	{
		final List<DealModel> deal = new ArrayList<>();
		final DealModel dealModel = Mockito.mock(DealModel.class);
		deal.add(dealModel);
		Mockito.when(extDealDao.findAllDeals()).thenReturn(deal);
		extDealDao.findAllDeals();
		Mockito.verify(extDealDao).findAllDeals();
	}


	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyDealListReturnedFromDaoIsReturnedCorrectly()
	{
		final List<DealModel> dealList = new ArrayList<>();
		final DealModel designerModel = mock(DealModel.class);
		dealList.add(designerModel);
		final List<String> codes = new ArrayList<>();
		codes.add("testDeal1");
		when(extDealDao.findListOfDealForCodes(codes)).thenReturn(dealList);
		assertEquals(dealList, extDealService.getListOfDealsForCodes(codes));
	}

}
