package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.simon.core.dao.CustomMessageDao;


/**
 * Test Class for SimonMessageServiceImpl
 */
@UnitTest
public class CustomMessageServiceTest
{
	@InjectMocks
	CustomMessageServiceImpl customMessageService;

	@Mock
	private CustomMessageDao messageDao;

	@Mock
	private CatalogVersionService catalogVersionService;

	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(catalogVersionService.getSessionCatalogVersions()).thenReturn(new ArrayList<CatalogVersionModel>());
	}

	/**
	 * Method to test empty string is returned when invalid messagecode is passed
	 */
	@Test
	public void testEmptyStringReturnedWithInvalidMessageCode()
	{
		final String messageCode = "INVALID.CODE";
		when(messageDao.getMessageTextForCode(Matchers.anyString(), Matchers.anyCollection())).thenReturn(StringUtils.EMPTY);
		assertEquals(StringUtils.EMPTY, customMessageService.getMessageForCode(messageCode));
	}

	/**
	 * Method to test MessageString is returned when valid messagecode is passed
	 */
	@Test
	public void testLabelReturnedWithValidMessageCode()
	{

		final String messageCode = "label.text.home";
		final String messageLabel = "Customized Home Label";
		when(messageDao.getMessageTextForCode(Matchers.anyString(), Matchers.anyCollection())).thenReturn(messageLabel);
		assertEquals(messageLabel, customMessageService.getMessageForCode(messageCode));
	}

}
