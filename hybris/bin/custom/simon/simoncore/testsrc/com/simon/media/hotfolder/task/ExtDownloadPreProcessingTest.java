package com.simon.media.hotfolder.task;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;
import de.hybris.platform.acceleratorservices.dataimport.batch.util.SequenceIdParser;
import de.hybris.platform.acceleratorservices.util.RegexParser;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.opencsv.CSVReader;
import com.simon.core.file.wrapper.FileWriterWrapper;


/**
 * Unit test for {@link ExtDownloadPreProcessing}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtDownloadPreProcessingTest
{

	@InjectMocks
	@Spy
	private ExtDownloadPreProcessing extDownloadPreProcessing;

	@Mock
	private File file;

	@Mock
	private CSVReader csvReader;

	@Mock
	private FileWriterWrapper fileWriterWrapper;

	@Mock
	private Writer writer;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private Configuration configuration;

	@Mock
	private BatchHeader header;

	@Mock
	private SequenceIdParser sequenceIdParser;

	@Mock
	private RegexParser languageParser;

	@Mock
	private I18NService i18nService;

	/**
	 * Setup.
	 *
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Before
	public void setup() throws IOException
	{
		when(header.getFile()).thenReturn(file);
		when(file.getName()).thenReturn("simonImageFeedFile_12345");
		extDownloadPreProcessing.setSequenceIdParser(sequenceIdParser);
		extDownloadPreProcessing.setLanguageParser(languageParser);
		extDownloadPreProcessing.setFileName("simonImageFeedFile");
		extDownloadPreProcessing.setImageDir("downloaded/");
		extDownloadPreProcessing.setFileExt("jpg");
		extDownloadPreProcessing.setOutputCsvPath("processed/");
		extDownloadPreProcessing.setFilePrefix("simonmedia");
		when(languageParser.parse(header.getFile().getName(), 1)).thenReturn("en");
		when(sequenceIdParser.getSequenceId(header.getFile())).thenReturn(101011L);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("simon.media.feed.error.log")).thenReturn("error/feed");
		when(configuration.getString("simon.media.feed.error.summary.mapping.text")).thenReturn("Total Failed Records");
		when(configuration.getString("simon.media.feed.error.summary.prefix")).thenReturn("mediaErrorSummary");
		when(i18nService.getCurrentLocale()).thenReturn(Locale.ENGLISH);
		when(fileWriterWrapper.getWriter(any(String.class))).thenReturn(writer);
	}

	/**
	 * Verify no interaction for incorrect file name.
	 */
	@Test
	public void verifyNoInteractionForIncorrectFileName()
	{
		when(file.getName()).thenReturn("incorrectFileName");
		extDownloadPreProcessing.execute(header);
		verifyZeroInteractions(csvReader);
	}

	/**
	 * Verify all product images are sent for downloading.
	 *
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyAllValidProductImagesAreSentForDownloading() throws IOException
	{
		doReturn(mockCsvReader()).when(extDownloadPreProcessing).getCSVReader(file, "UTF-8");
		when(configuration.getInt(eq("simon.media.feed.download.threads"), eq(1))).thenReturn(1);
		when(configuration.getInt(eq("simon.media.retry.counter"), eq(3))).thenReturn(3);
		doReturn(true).when(extDownloadPreProcessing).deleteAndSaveFile(any(String.class), any(String.class), any(Integer.class));
		extDownloadPreProcessing.execute(header);
		verify(extDownloadPreProcessing, times(16)).deleteAndSaveFile(any(String.class), any(String.class), any(Integer.class));
	}

	/**
	 * Verify unsuccessful downloads are logged into error file.
	 *
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	@Test
	public void verifyUnsuccessfulDownloadsAreLoggedIntoErrorFile() throws IOException
	{
		doReturn(mockCsvReader()).when(extDownloadPreProcessing).getCSVReader(file, "UTF-8");
		when(configuration.getInt(eq("simon.media.feed.download.threads"), eq(1))).thenReturn(1);
		when(configuration.getInt(eq("simon.media.retry.counter"), eq(3))).thenReturn(3);
		when(configuration.getString("simon.media.feed.error.summary.prefix")).thenReturn("mediaErrorSummary");
		when(configuration.getString("product.image.filename.seperator", "-")).thenReturn("-");
		doReturn(false).when(extDownloadPreProcessing).deleteAndSaveFile(any(String.class), any(String.class), any(Integer.class));
		final Writer errorWriter = mock(Writer.class);
		when(fileWriterWrapper.getWriter(contains("mediaErrorSummary"))).thenReturn(errorWriter);
		extDownloadPreProcessing.execute(header);
		verify(errorWriter, times(10)).write(any(String.class));
	}


	/**
	 * Mock csv reader.
	 *
	 * @return the CSV reader
	 */
	private CSVReader mockCsvReader()
	{
		final String image1 = "https://mo-upload-image-store.s3.amazonaws.com/0/d2f9099f-7e4f-456c-ab47-8f9d5d377759.jpg";
		final String image2 = "https://mo-upload-image-store.s3.amazonaws.com/0/3d8d1c06-3acf-4f20-8f4b-f72972b8b95f.jpg";
		final String image3 = "https://mo-upload-image-store.s3.amazonaws.com/0/c8a73701-c118-4221-944e-a4932d26b293.jpg";
		final String image4 = "https://mo-upload-image-store.s3.amazonaws.com/0/6d1c3687-cb76-473a-acdd-37981effef13.jpg";
		final String image5 = "https://mo-upload-image-store.s3.amazonaws.com/0/34b94d1c-578f-4e4f-8415-7e3f24b0f86e.jpg";
		final String image6 = "https://mo-upload-image-store.s3.amazonaws.com/0/72427423-8b2c-4a95-b5a9-6aa355e02c80.jpg";
		final String image7 = "https://mo-upload-image-store.s3.amazonaws.com/0/1033d9cd-5e2d-4f55-869c-8a92f19a73ad.jpg";
		final String image8 = "https://mo-upload-image-store.s3.amazonaws.com/0/5acb5cb7-4435-486b-a9fd-b165c986e16a.jpg";
		final String image9 = "https://mo-upload-image-store.s3.amazonaws.com/0/beae823a-ff09-414c-9d92-ea628bb15c3e.jpg";
		final String image10 = "https://mo-upload-image-store.s3.amazonaws.com/0/a4242d6b-0ebe-446d-9c67-9e6d7adad201.jpg";
		final String image11 = "https://mo-upload-image-store.s3.amazonaws.com/0/70576a0f-0106-4b1e-9625-a70679ac63e8.jpg";
		final String image12 = "https://mo-upload-image-store.s3.amazonaws.com/0/2375b154-a67e-4375-b3c7-b3f161bb2f57.jpg";
		final String image13 = "https://mo-upload-image-store.s3.amazonaws.com/0/b44ddc91-1c77-446c-a438-86668dc92106.jpg";
		final String image14 = "https://mo-upload-image-store.s3.amazonaws.com/0/5f8b59c4-8d14-49b9-bb24-7e943a048ef9.jpg";
		final String image15 = "https://mo-upload-image-store.s3.amazonaws.com/0/fd5de7ed-2d53-4e9e-bf4f-721f0a9a7c0a.jpg";
		final String image16 = "https://mo-upload-image-store.s3.amazonaws.com/0/6e74f6b1-05d5-4b16-be44-64554c88178d.jpg";

		final String product1Code = "COLE-13816054013";
		final String product2Code = "COLE-13816054044";
		final String product3Code = "COLE-13816063848";
		final String product4Code = "COLE-13816063855";
		final String product1Images = Arrays.asList(image1, image2, image3, image4).stream().collect(Collectors.joining("|"));
		final String product2Images = Arrays.asList(image5, image6, image7, image8).stream().collect(Collectors.joining("|"));
		final String product3Images = Arrays.asList(image9, image10, image11, image12).stream().collect(Collectors.joining("|"));
		final String product4Images = Arrays.asList(image13, image14, image15, image16).stream().collect(Collectors.joining("|"));

		final String csvData = new StringBuilder().append(product1Code).append(",").append(product1Images).append("\n")
				.append(product2Code).append(",").append(product2Images).append("\n").append(product3Code).append(",")
				.append(product3Images).append("\n").append(product4Code).append(",").append(product4Images).append("\n").toString();
		return new CSVReader(new InputStreamReader(new ByteArrayInputStream(csvData.getBytes(Charset.forName("UTF-8")))));
	}
}
