package com.simon.core.cms.component.caching;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;




@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCMSCacheServiceTest
{
	@InjectMocks
	private ExtCMSCacheService extCMSCacheService;
	@Mock
	private ConfigurationService configurationService;

	@Test
	public void useCacheWhenRequestIsNull()
	{
		assertFalse(extCMSCacheService.useCache(null, mock(AbstractCMSComponentModel.class)));
	}

	@Test
	public void useCacheWhenComponentModelIsNull()
	{
		assertFalse(extCMSCacheService.useCache(mock(HttpServletRequest.class), null));
	}

	@Test
	public void useCacheWhenComponentModelAndRequestISAvailble()
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("cms.cache.enabled.components")).thenReturn("");
		assertFalse(extCMSCacheService.useCache(mock(HttpServletRequest.class), mock(AbstractCMSComponentModel.class)));
	}

	@Test
	public void isComponentCacheRequired_WhenNoComponentConfigured()
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("cms.cache.enabled.components")).thenReturn("");
		assertFalse(extCMSCacheService.isComponentCacheRequired(mock(AbstractCMSComponentModel.class)));
	}

	@Test
	public void isComponentCacheRequired_WhenComponentIsConfigured()
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("cms.cache.enabled.components")).thenReturn("bannerCompoent,headerComp");
		final AbstractCMSComponentModel abstractCMSComponentModel=mock(AbstractCMSComponentModel.class);
		when(abstractCMSComponentModel.getUid()).thenReturn("bannerCompoent");
		assertTrue(extCMSCacheService.isComponentCacheRequired(abstractCMSComponentModel));
	}

	@Test
	public void isComponentCacheRequired_WhenOnlyOneComponentIsConfigured()
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);
		when(configuration.getString("cms.cache.enabled.components")).thenReturn("bannerCompoent");
		final AbstractCMSComponentModel abstractCMSComponentModel = mock(AbstractCMSComponentModel.class);
		when(abstractCMSComponentModel.getUid()).thenReturn("bannerComp");
		assertFalse(extCMSCacheService.isComponentCacheRequired(abstractCMSComponentModel));
	}
}
