package com.simon.core.caches;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.keygenerators.MediaKeyGenerator;
import com.simon.core.keygenerators.VariantCategoryKeyGenerator;
import com.simon.core.keygenerators.VariantValueCategoryKeyGenerator;
import com.simon.core.services.VariantCategoryService;
import com.simon.core.services.VariantValueCategoryService;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ColorSwatchMediaPreProcessorTest
{
	@InjectMocks
	private ColorSwatchMediaPreProcessor colorSwatchMediaPreProcessor;

	@Mock
	private MediaService mediaService;
	@Mock
	private MediaKeyGenerator mediaKeyGenerator;



	@Mock
	private VariantCategoryService variantCategoryService;

	@Mock
	private VariantValueCategoryService variantValueCategoryService;

	@Mock
	private VariantCategoryKeyGenerator variantCategoryKeyGenerator;

	@Mock
	private VariantValueCategoryKeyGenerator variantValueCategoryKeyGenerator;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private UserService userService;

	@Mock
	private ModelService modelService;

	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private ProductImportFileContextData context;

	/**
	 * test create key method
	 */
	@Test
	public void testCreateKey()
	{
		final String attribute = "colorSwatchUrl";
		final String rawValue = "https://dummyimage.com/600x400/f211f2/fff";
		final Map<String, String> productValues = ImmutableMap.of("variantAttributes", "color=British Tan|size=10|width=M");
		when(mediaKeyGenerator.generateFor("color=British Tan|size=10|width=M")).thenReturn("VVC-british-tan-color_thumbnail");
		when(context.getShopId()).thenReturn("2027");
		assertThat(colorSwatchMediaPreProcessor.createKey(attribute, rawValue, productValues, context),
				is("2027_VVC-british-tan-color_thumbnail"));

	}

	/**
	 * test if media is existing with same real file url
	 */
	@Test
	public void verifyThumbnailNotUpdatedForExistingMediaWithSameRealFileName()
	{
		final String attribute = "colorSwatchUrl";
		final String rawValue = "https://dummyimage.com/600x400/f211f2/fff";
		final Map<String, String> productValues = ImmutableMap.of("variantAttributes", "color=British Tan|size=10|width=M");
		final MediaModel media = mock(MediaModel.class);
		when(mediaKeyGenerator.generateFor("color=British Tan|size=10|width=M")).thenReturn("VVC-british-tan-color_thumbnail");
		when(context.getShopId()).thenReturn("2027");
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT))
				.thenReturn(catalogVersion);
		when(mediaService.getMedia(catalogVersion, "2027_VVC-british-tan-color_thumbnail")).thenReturn(media);
		when(media.getRealFileName()).thenReturn(rawValue);
		final MediaModel mediaModel = colorSwatchMediaPreProcessor.createValue(attribute, rawValue, productValues, context);
		Mockito.verify(modelService, Mockito.times(0)).save(media);
	}

	/**
	 * test if media does not exist in DB
	 */
	@Test
	public void testNewSwatchMediaCreatedWhenNotExisted()
	{
		final String attribute = "colorSwatchUrl";
		final String rawValue = "https://dummyimage.com/600x400/f211f2/fff";
		final Map<String, String> productValues = ImmutableMap.of("variantAttributes", "color=British Tan|size=10|width=M");
		final MediaModel media = mock(MediaModel.class);
		when(mediaKeyGenerator.generateFor("color=British Tan|size=10|width=M")).thenReturn("VVC-british-tan-color_thumbnail");
		when(context.getShopId()).thenReturn("2027");
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_IMAGE_CATALOG_CODE, Catalog.DEFAULT))
				.thenReturn(catalogVersion);
		when(modelService.create(MediaModel.class)).thenReturn(media);
		final MediaModel mediaModel = colorSwatchMediaPreProcessor.createValue(attribute, rawValue, productValues, context);
		Mockito.verify(modelService, Mockito.times(2)).save(media);
	}



}
