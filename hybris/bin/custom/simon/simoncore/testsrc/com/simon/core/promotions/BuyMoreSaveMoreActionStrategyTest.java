package com.simon.core.promotions;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderAdjustTotalActionModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderEntryAdjustActionModel;
import de.hybris.platform.promotions.model.PromotionOrderEntryConsumedModel;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.ShipmentRAO;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.promotions.service.impl.ExtDefaultPromotionActionServiceImpl;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class BuyMoreSaveMoreActionStrategyTest
{
	@InjectMocks
	private BuyMoreSaveMoreStrategy actionStrategy;
	@Mock
	private ExtDefaultPromotionActionServiceImpl promotionActionService;
	@Mock
	private DiscountRAO action;
	@Mock
	CartModel cart;
	@Mock
	private PromotionResultModel promotionResult;
	@Mock
	ModelService modelService;
	@Mock
	private RuleBasedOrderEntryAdjustActionModel actionModel;
	@Mock
	private RetailerRAO retailerRao;
	@Mock
	private CartEntryModel cartEntry;
	@Mock
	private PromotionOrderEntryConsumedModel prce1;
	@Mock
	private PromotionOrderEntryConsumedModel prce2;

	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test
	public void applyTest_whenActionRaoIsNotDiscount()
	{
		final ShipmentRAO action = new ShipmentRAO();
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTestWhenOrderEntryIsNull()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(null);
		when(promotionResult.getOrder()).thenReturn(cart);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTestWhenPRsNull()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(null);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(cartEntry);
		when(cartEntry.getOrder()).thenReturn(cart);
		when(cartEntry.getQuantity()).thenReturn(3L);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}


	@Test
	public void applyTestWhenPRhasConsumedEntries()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		final Collection<PromotionOrderEntryConsumedModel> consumedEntries = new ArrayList<>();
		consumedEntries.add(prce1);
		consumedEntries.add(prce2);
		when(prce1.getQuantity()).thenReturn(1l);
		when(prce2.getQuantity()).thenReturn(2l);
		when(promotionResult.getConsumedEntries()).thenReturn(consumedEntries);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(cartEntry);
		when(cartEntry.getOrder()).thenReturn(cart);
		when(cartEntry.getQuantity()).thenReturn(3L);
		when(promotionResult.getOrder()).thenReturn(cart);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		when(action.getCurrencyIsoCode()).thenReturn("USD");
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 1);
	}

	@Test
	public void applyTestWhenOrderIsNotNull()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(cartEntry);
		when(action.getCurrencyIsoCode()).thenReturn("USD");
		when(action.getAppliedToQuantity()).thenReturn(1L);
		when(action.getValue()).thenReturn(new BigDecimal(30));
		when(cartEntry.getOrder()).thenReturn(cart);
		when(cartEntry.getQuantity()).thenReturn(3L);
		when(promotionResult.getOrder()).thenReturn(cart);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 1);
	}

	@Test
	public void applyTestWhenOrderIsNull()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(cartEntry);
		when(cartEntry.getOrder()).thenReturn(null);
		when(cartEntry.getQuantity()).thenReturn(3L);
		when(promotionResult.getOrder()).thenReturn(cart);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void applyTestWhenOrderIsNullPRisNew()
	{
		actionStrategy.setPromotionAction(RuleBasedOrderEntryAdjustActionModel.class);
		when(promotionActionService.createPromotionResult(action)).thenReturn(promotionResult);
		when(promotionActionService.getConsumedOrderEntry(action)).thenReturn(cartEntry);
		when(cartEntry.getOrder()).thenReturn(null);
		when(cartEntry.getQuantity()).thenReturn(3L);
		when(modelService.isNew(promotionResult)).thenReturn(true);
		when(modelService.create(RuleBasedOrderEntryAdjustActionModel.class)).thenReturn(actionModel);
		final Collection<String> metaDataList = new ArrayList<>();
		metaDataList.add("metaData");
		metaDataList.iterator().hasNext();
		when(actionModel.getMetadataHandlers()).thenReturn(metaDataList);
		final List<PromotionResultModel> pr = actionStrategy.apply(action);
		assertTrue("PR should be created only if action is of DiscountRAO", pr.size() == 0);
	}

	@Test
	public void undoTest()
	{
		when(actionModel.getPromotionResult()).thenReturn(promotionResult);
		when(actionModel.getGuid()).thenReturn("guid");

		when(promotionResult.getOrder()).thenReturn(cart);
		when(promotionActionService.removeDiscountValue("guid", cart)).thenReturn(new ArrayList<>());
		actionStrategy.undo(actionModel);
		verify(promotionActionService).removeDiscountValue("guid", cart);
	}

	@Test
	public void undoTestWhenActionIsNotRuleBasedOrderEntryAdjustActionModel()
	{
		actionStrategy.undo(new RuleBasedOrderAdjustTotalActionModel());
		verifyZeroInteractions(promotionActionService);
	}

}
