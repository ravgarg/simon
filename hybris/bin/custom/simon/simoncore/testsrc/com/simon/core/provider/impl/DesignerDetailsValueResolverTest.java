package com.simon.core.provider.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.model.DesignerModel;


@UnitTest
public class DesignerDetailsValueResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	DesignerDetailsValueResolver designerDetailsValueResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private TypeService typeService;
	public static final String OPTIONAL_PARAM = "optional";
	public static final String ATTRIBUTE_PARAM = "attribute";

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		final DesignerModel designer = new DesignerModel();
		baseProduct.setProductDesigner(designer);
		model.setBaseProduct(baseProduct);
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, "attributeName");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(designer.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(designer, attributeName)).thenReturn(value);
		designerDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));
	}




	@Test(expected = FieldValueProviderException.class)
	public void testResolveNullDesignerModelAndIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		designerDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
	}


	@Test
	public void testResolveNullDesignerModel() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = new GenericVariantProductModel();
		final ProductModel baseProduct = new ProductModel();
		model.setBaseProduct(baseProduct);
		designerDetailsValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}


	@Test
	public void testGetAttributeNameWithValidAttributeName()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(ATTRIBUTE_PARAM, "attributeName");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		assertEquals("attributeName", designerDetailsValueResolver.getAttributeName(indexedProperty));
	}

	@Test
	public void testGetAttributeNameWithNullAttributeName()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		indexedProperty.setName("attributeName");
		assertEquals("attributeName", designerDetailsValueResolver.getAttributeName(indexedProperty));
	}

	@Test
	public void testGetAttributeValueWithNullAttributeName()
	{
		final DesignerModel designer = new DesignerModel();
		assertNull(designerDetailsValueResolver.getAttributeValue(designer, null));
	}

	@Test
	public void testGetModelAttributeValueWithValidData()
	{
		final DesignerModel designer = new DesignerModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(designer.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.TRUE);
		final Object value = new Object();
		when(modelService.getAttributeValue(designer, attributeName)).thenReturn(value);
		assertEquals(value, designerDetailsValueResolver.getModelAttributeValue(designer, attributeName));
	}

	@Test
	public void testGetModelAttributeValueWithTypeServiceWithoutAttribute()
	{
		final DesignerModel designer = new DesignerModel();
		final ComposedTypeModel composedValue = new ComposedTypeModel();
		when(typeService.getComposedTypeForClass(designer.getClass())).thenReturn(composedValue);
		final String attributeName = "attributeName";
		when(typeService.hasAttribute(composedValue, attributeName)).thenReturn(Boolean.FALSE);
		assertNull(designerDetailsValueResolver.getModelAttributeValue(designer, attributeName));
	}
}
