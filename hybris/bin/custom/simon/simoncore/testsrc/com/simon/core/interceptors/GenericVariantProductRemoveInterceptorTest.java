package com.simon.core.interceptors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableSet;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class GenericVariantProductRemoveInterceptorTest
{
	@InjectMocks
	private GenericVariantProductRemoveInterceptor genericVariantProductRemoveInterceptor;

	@Mock
	private GenericVariantProductModel variant;

	@Mock
	private PriceRowModel priceRow1;

	@Mock
	private PriceRowModel priceRow2;

	@Mock
	private MediaContainerModel mediaContainer1;

	@Mock
	private MediaContainerModel mediaContainer2;


	@Mock
	private MediaModel thumbail;

	@Mock
	private MediaModel rawImage;

	@Mock
	private InterceptorContext ctx;

	@Mock
	private StockLevelModel stock1;

	@Mock
	private StockLevelModel stock2;

	@Test
	public void testPriceRowsGetsRegistered() throws InterceptorException
	{
		when(variant.getEurope1Prices()).thenReturn(Arrays.asList(priceRow1, priceRow2));
		genericVariantProductRemoveInterceptor.onRemove(variant, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(priceRow1, PersistenceOperation.DELETE);
		verify(ctx, Mockito.times(1)).registerElementFor(priceRow2, PersistenceOperation.DELETE);
	}

	@Test
	public void testGalleryImagesGetsRegistered() throws InterceptorException
	{
		when(variant.getGalleryImages()).thenReturn(Arrays.asList(mediaContainer1, mediaContainer2));
		genericVariantProductRemoveInterceptor.onRemove(variant, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(mediaContainer1, PersistenceOperation.DELETE);
		verify(ctx, Mockito.times(1)).registerElementFor(mediaContainer2, PersistenceOperation.DELETE);
	}

	@Test
	public void testThumbailImageGetsRegistered() throws InterceptorException
	{
		when(variant.getThumbnail()).thenReturn(thumbail);
		genericVariantProductRemoveInterceptor.onRemove(variant, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(thumbail, PersistenceOperation.DELETE);
	}

	@Test
	public void testRawPictureImageGetsRegistered() throws InterceptorException
	{
		when(variant.getRawPicture()).thenReturn(rawImage);
		genericVariantProductRemoveInterceptor.onRemove(variant, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(rawImage, PersistenceOperation.DELETE);
	}

	public void testStockLevelsGetsRegistered() throws InterceptorException
	{
		when(variant.getStockLevels()).thenReturn(ImmutableSet.of(stock1, stock2));
		genericVariantProductRemoveInterceptor.onRemove(variant, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(stock1, PersistenceOperation.DELETE);
		verify(ctx, Mockito.times(1)).registerElementFor(stock2, PersistenceOperation.DELETE);
	}


}
