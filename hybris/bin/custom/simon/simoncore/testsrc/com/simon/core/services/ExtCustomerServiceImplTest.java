package com.simon.core.services;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.customer.CustomerEmailResolutionService;
import de.hybris.platform.commerceservices.order.CommerceCardTypeService;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.strategies.CustomerNameStrategy;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.payment.dto.CardType;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.customer.service.impl.ExtCustomerAccountServiceImpl;
import com.simon.core.strategies.impl.ExtCheckoutCustomerStrategy;


/**
 * JUnit test suite for {@link ExtCustomerServiceImplTest}
 */
@UnitTest
public class ExtCustomerServiceImplTest
{
	@InjectMocks
	private ExtCustomerAccountServiceImpl customerAccountService;
	@Mock
	private SetupImpexService setupImpexService;
	@Mock
	ExtCheckoutCustomerStrategy checkoutCustomerStrategy;
	@Mock
	private ModelService modelService;
	@Mock
	private CommonI18NService commonI18NService;
	@Mock
	private CustomerEmailResolutionService customerEmailResolutionService;
	@Mock
	private FlexibleSearchService flexibleSearchService;

	private AddressData addressData;

	private CCPaymentInfoData extPaymentInfoData;
	@Mock
	private CardType cardType;
	@Mock
	private CommerceCardTypeService commerceCardTypeService;
	@Mock
	private CustomerNameStrategy customerNameStrategy;

	/**
	 * set up
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		extPaymentInfoData = new CCPaymentInfoData();
		extPaymentInfoData.setPaymentToken("4BaK5ytLJyqL92EdCqfp4EY5iNt");
		extPaymentInfoData.setAccountHolderName("Sachin Shrivastava");
		extPaymentInfoData.setCardNumber("************1111");
		extPaymentInfoData.setLastFourDigits("1111");
		extPaymentInfoData.setCardType("visa");
		extPaymentInfoData.setExpiryMonth("09");
		extPaymentInfoData.setExpiryYear("2018");
		extPaymentInfoData.setSaveInAccount(true);

		addressData = new AddressData();
		final CountryData countryData = new CountryData();
		countryData.setIsocode("US");

		final RegionData regionData = new RegionData();
		regionData.setCountryIso("US");
		regionData.setIsocode("US-NY");

		addressData.setFirstName("Sachin");
		addressData.setLastName("Shrivastava");
		addressData.setCountry(countryData);
		addressData.setLine1("Tower C, Plot No. -7");
		addressData.setLine2("Oxygen Business Park, SEZ, Sec-144");
		addressData.setTown("Noida");
		addressData.setRegion(regionData);
		addressData.setPostalCode("201301");
		addressData.setPhone("7042922799");
		addressData.setTitle("Mr");

		extPaymentInfoData.setBillingAddress(addressData);
		extPaymentInfoData.setCardType("VISA");

	}

	/**
	 * test for importing common data
	 */
	@Test
	public void createSpreedlyPaymentSubscription()
	{
		final CustomerModel customerModel = Mockito.mock(CustomerModel.class);
		customerModel.setUid("123");

		final AddressModel addressModel = Mockito.mock(AddressModel.class);

		final TitleModel titleModel = Mockito.mock(TitleModel.class);

		final CreditCardPaymentInfoModel extPaymentInfoModel = Mockito.mock(CreditCardPaymentInfoModel.class);

		final List<PaymentInfoModel> paymentInfoModels = new ArrayList<PaymentInfoModel>();
		final String title = "Mr";
		final String[] names = new String[10];

		names[0] = "firstname";
		names[1] = "lastname";

		Mockito.when(checkoutCustomerStrategy.getCurrentUserForCheckout()).thenReturn(customerModel);
		Mockito.when(modelService.create(AddressModel.class)).thenReturn(addressModel);
		Mockito.when(modelService.create(TitleModel.class)).thenReturn(titleModel);
		Mockito.when(modelService.create(CreditCardPaymentInfoModel.class)).thenReturn(extPaymentInfoModel);
		Mockito.when(flexibleSearchService.getModelByExample(title)).thenReturn(title);
		Mockito.when(customerModel.getPaymentInfos()).thenReturn(paymentInfoModels);
		Mockito.when(customerNameStrategy.splitName(Mockito.anyString())).thenReturn(names);
		Mockito.doReturn(cardType).when(commerceCardTypeService).getCardTypeForCode("VISA");


		Assert.assertNotNull(customerAccountService.createPaymentInfoSubscription(extPaymentInfoData));
	}
}
