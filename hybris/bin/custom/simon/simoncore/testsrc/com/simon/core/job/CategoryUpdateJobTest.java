package com.simon.core.job;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.CategoryUpdateType;
import com.simon.core.model.CategoryUpdateCronJobModel;
import com.simon.core.services.ExtProductService;


/**
 * CategoryUpdateJobTest unit test for {@link CategoryUpdateJob}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CategoryUpdateJobTest
{
	@InjectMocks
	private CategoryUpdateJob categoryUpdateJob;

	@Mock
	private CatalogVersionService catalogVersionService;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private ModelService modelService;

	/**
	 * Verify base products with missing categories are updated and saved if category update type is missing categories.
	 */
	@Test
	public void verifyBaseProductsWithMissingCategoriesAreUpdatedAndSavedIfCategoryUpdateTypeIsMissingCategories()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);

		final CategoryUpdateCronJobModel categoryUpdateCronJob = mock(CategoryUpdateCronJobModel.class);
		when(categoryUpdateCronJob.getCategoryUpdateType()).thenReturn(CategoryUpdateType.MISSING_CATEGORIES);

		final List<ProductModel> baseProducts = new ArrayList<>();
		final ProductModel baseProduct1 = mock(ProductModel.class);
		final ProductModel baseProduct2 = mock(ProductModel.class);
		final ProductModel baseProduct3 = mock(ProductModel.class);
		baseProducts.add(baseProduct1);
		baseProducts.add(baseProduct2);
		baseProducts.add(baseProduct3);
		when(extProductService.getProductsWithoutMerchandizingCategory(catalogVersion)).thenReturn(baseProducts);

		categoryUpdateJob.perform(categoryUpdateCronJob);
		baseProducts.forEach(verifyUpdateCategories.andThen(verifyModelSave));
	}

	/**
	 * Verify all base products are updated and saved if category update type is full import.
	 */
	@Test
	public void verifyAllBaseProductsAreUpdatedAndSavedIfCategoryUpdateTypeIsFullImport()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);

		final CategoryUpdateCronJobModel categoryUpdateCronJob = mock(CategoryUpdateCronJobModel.class);
		when(categoryUpdateCronJob.getCategoryUpdateType()).thenReturn(CategoryUpdateType.FULL_IMPORT);

		final List<ProductModel> baseProducts = new ArrayList<>();
		final ProductModel baseProduct1 = mock(ProductModel.class);
		final ProductModel baseProduct2 = mock(ProductModel.class);
		final ProductModel baseProduct3 = mock(ProductModel.class);
		baseProducts.add(baseProduct1);
		baseProducts.add(baseProduct2);
		baseProducts.add(baseProduct3);
		when(extProductService.getProductsForCatalogVersion(catalogVersion)).thenReturn(baseProducts);

		categoryUpdateJob.perform(categoryUpdateCronJob);
		baseProducts.forEach(verifyUpdateCategories.andThen(verifyModelSave));
	}

	/**
	 * Verify no interaction for null category update type.
	 */
	@Test
	public void verifyNoInteractionForNullCategoryUpdateType()
	{
		final CatalogVersionModel catalogVersion = mock(CatalogVersionModel.class);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED)).thenReturn(catalogVersion);

		final CategoryUpdateCronJobModel categoryUpdateCronJob = mock(CategoryUpdateCronJobModel.class);

		final List<ProductModel> baseProducts = new ArrayList<>();
		final ProductModel baseProduct1 = mock(ProductModel.class);
		final ProductModel baseProduct2 = mock(ProductModel.class);
		final ProductModel baseProduct3 = mock(ProductModel.class);
		baseProducts.add(baseProduct1);
		baseProducts.add(baseProduct2);
		baseProducts.add(baseProduct3);

		categoryUpdateJob.perform(categoryUpdateCronJob);
		verifyZeroInteractions(extProductService);
		verifyZeroInteractions(modelService);
	}

	final Consumer<ProductModel> verifyUpdateCategories = product -> {
		verify(product).setSupercategories(any());
	};

	final Consumer<ProductModel> verifyModelSave = product -> {
		verify(modelService).save(product);
	};
}
