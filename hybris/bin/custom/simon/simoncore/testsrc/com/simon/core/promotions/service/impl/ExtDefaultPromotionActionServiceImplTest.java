package com.simon.core.promotions.service.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.promotionengineservices.model.RuleBasedPromotionModel;
import de.hybris.platform.ruleengine.model.AbstractRuleEngineRuleModel;
import de.hybris.platform.ruleengineservices.model.AbstractRuleModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.DiscountRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryConsumedRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.promotion.rao.RetailerRAO;


/**
 *
 * Junit test for ExtDefaultPromotionActionServiceImpl
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtDefaultPromotionActionServiceImplTest
{
	@InjectMocks
	@Spy
	private ExtDefaultPromotionActionServiceImpl promotionActionServiceImpl;
	@Mock
	private AbstractRuleActionRAO abstractRao;
	@Mock
	private AbstractRuleEngineRuleModel rule;
	@Mock
	private RuleBasedPromotionModel promotionModel;
	@Mock
	private AbstractRuleModel abstractRuleModel;
	@Mock
	private OrderModel order;

	@Mock
	private DiscountRAO discountRao;
	@Mock
	private CurrencyModel currencyModel;
	@Mock
	private AbstractOrderEntryModel mockedOrderEntry;
	@Mock
	private RetailerRAO retailer;


	@Test
	public void getPromotion()
	{

		when(abstractRao.getFiredRuleCode()).thenReturn("firedRuleCode");
		doReturn(rule).when(promotionActionServiceImpl).getRules(abstractRao);
		when(rule.getPromotion()).thenReturn(promotionModel);
		when(rule.getSourceRule()).thenReturn(abstractRuleModel);
		when(abstractRuleModel.getPromoCode()).thenReturn("PromoCode");
		doNothing().when(promotionActionServiceImpl).savePromotionModel(promotionModel);
		promotionActionServiceImpl.getPromotion(abstractRao);
		Assert.assertNotNull(promotionActionServiceImpl.getPromotion(abstractRao));
	}


	@Test
	public void getPromotionAbstractRuleActionRAOIsNull()
	{
		Assert.assertNull(promotionActionServiceImpl.getPromotion(null));
	}

	@Test
	public void getPromotionFiredRuleCodeIsNull()
	{

		when(abstractRao.getFiredRuleCode()).thenReturn(null);
		Assert.assertNull(promotionActionServiceImpl.getPromotion(abstractRao));
	}

	@Test
	public void getPromotionRuleEngineRuleIsNull()
	{

		when(abstractRao.getFiredRuleCode()).thenReturn("firedRuleCode");
		doReturn(null).when(promotionActionServiceImpl).getRules(abstractRao);
		when(rule.getPromotion()).thenReturn(promotionModel);
		when(rule.getSourceRule()).thenReturn(abstractRuleModel);
		when(abstractRuleModel.getPromoCode()).thenReturn("PromoCode");
		promotionActionServiceImpl.getPromotion(abstractRao);
		Assert.assertNull(promotionActionServiceImpl.getPromotion(abstractRao));
	}


	@Test
	public void getPromotionModelIsNull()
	{

		when(abstractRao.getFiredRuleCode()).thenReturn("firedRuleCode");
		doReturn(rule).when(promotionActionServiceImpl).getRules(abstractRao);
		when(rule.getPromotion()).thenReturn(null);
		when(rule.getSourceRule()).thenReturn(abstractRuleModel);
		when(abstractRuleModel.getPromoCode()).thenReturn("PromoCode");
		promotionActionServiceImpl.getPromotion(abstractRao);
		Assert.assertNull(promotionActionServiceImpl.getPromotion(abstractRao));
	}

	@Test
	public void getPromotionAbstractRuleIsNull()
	{

		when(abstractRao.getFiredRuleCode()).thenReturn("firedRuleCode");
		doReturn(rule).when(promotionActionServiceImpl).getRules(abstractRao);
		when(rule.getPromotion()).thenReturn(promotionModel);
		when(rule.getSourceRule()).thenReturn(null);
		when(abstractRuleModel.getPromoCode()).thenReturn("PromoCode");
		promotionActionServiceImpl.getPromotion(abstractRao);
		Assert.assertNotNull(promotionActionServiceImpl.getPromotion(abstractRao));
	}

	@Test
	public void testCreateRetailerDiscountValueBMSM_WhenDiscountCurrencyIsNull()
	{
		final AbstractOrderEntryModel orderEntry = new OrderEntryModel();
		when(discountRao.getCurrencyIsoCode()).thenReturn(null);
		final BigDecimal discountValue = new BigDecimal(100);
		when(discountRao.getValue()).thenReturn(discountValue);
		orderEntry.setOrder(order);
		orderEntry.setQuantity(3L);
		orderEntry.setCalculated(true);
		when(order.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		promotionActionServiceImpl.createDiscountValueForBuyMoreSaveMore(discountRao, "firedRuleCode", orderEntry);
		assertFalse(orderEntry.getCalculated());
	}

	@Test
	public void testCreateRetailerDiscountValueBMSM_WhenDiscountCurrencyIsUSD()
	{
		final AbstractOrderEntryModel orderEntry = new OrderEntryModel();
		when(discountRao.getCurrencyIsoCode()).thenReturn("USD");
		final BigDecimal discountValue = new BigDecimal(100);
		when(discountRao.getValue()).thenReturn(discountValue);
		orderEntry.setOrder(order);
		orderEntry.setQuantity(3L);
		orderEntry.setCalculated(true);
		when(order.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		promotionActionServiceImpl.createDiscountValueForBuyMoreSaveMore(discountRao, "firedRuleCode", orderEntry);
		assertFalse(orderEntry.getCalculated());

	}

	@Test
	public void testCreateRetailerDiscountValue_WhenDiscountCurrencyIsNull()
	{
		when(discountRao.getCurrencyIsoCode()).thenReturn(null);
		when(discountRao.getRetailer()).thenReturn(retailer);
		when(retailer.getRetailerId()).thenReturn("retailer");
		final BigDecimal discountValue = new BigDecimal(100);
		when(discountRao.getValue()).thenReturn(discountValue);
		when(mockedOrderEntry.getOrder()).thenReturn(order);
		when(mockedOrderEntry.getQuantity()).thenReturn(3L);
		when(mockedOrderEntry.getCalculated()).thenReturn(true);
		when(mockedOrderEntry.getDiscountValues()).thenReturn(new ArrayList<>());
		when(order.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		promotionActionServiceImpl.createRetailerDiscountValue(discountRao, "firedRuleCode", order);
		verify(order).setSubbagDiscountValues(Mockito.anyMap());
	}

	@Test
	public void testCreateRetailerDiscountValue_WhenDiscountCurrencyIsUSD()
	{
		when(discountRao.getCurrencyIsoCode()).thenReturn("USD");
		when(discountRao.getRetailer()).thenReturn(retailer);
		when(retailer.getRetailerId()).thenReturn("retailer");
		final BigDecimal discountValue = new BigDecimal(100);
		when(discountRao.getValue()).thenReturn(discountValue);
		when(mockedOrderEntry.getOrder()).thenReturn(order);
		when(mockedOrderEntry.getQuantity()).thenReturn(3L);
		when(mockedOrderEntry.getCalculated()).thenReturn(true);
		when(mockedOrderEntry.getDiscountValues()).thenReturn(new ArrayList<>());
		when(order.getCurrency()).thenReturn(currencyModel);
		when(currencyModel.getIsocode()).thenReturn("USD");
		promotionActionServiceImpl.createRetailerDiscountValue(discountRao, "firedRuleCode", order);
		verify(order).setSubbagDiscountValues(Mockito.anyMap());

	}

	@Test
	public void testGetConsumedEntryModel()
	{
		final Set<OrderEntryConsumedRAO> consumedEntriesSet = new HashSet<>();
		final OrderEntryConsumedRAO consumedEntry = mock(OrderEntryConsumedRAO.class);
		final OrderEntryRAO orderEntry = mock(OrderEntryRAO.class);
		when(consumedEntry.getOrderEntry()).thenReturn(orderEntry);
		consumedEntriesSet.add(consumedEntry);
		when(discountRao.getConsumedEntries()).thenReturn(consumedEntriesSet);
		when(discountRao.getFiredRuleCode()).thenReturn("firerulecode");
		when(consumedEntry.getFiredRuleCode()).thenReturn("firerulecode");

		doReturn(mockedOrderEntry).when(promotionActionServiceImpl).invokeOrderEntry(orderEntry);
		promotionActionServiceImpl.getConsumedOrderEntry(discountRao);
	}

}
