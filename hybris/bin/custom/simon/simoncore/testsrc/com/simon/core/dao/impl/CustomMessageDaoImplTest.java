package com.simon.core.dao.impl;


import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonFlexiConstants;
import com.simon.core.exceptions.SystemException;
import com.simon.core.model.CustomMessageModel;


/**
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CustomMessageDaoImplTest
{

	@InjectMocks
	@Spy
	CustomMessageDaoImpl customMessageDaoImpl;
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private FlexibleSearchQuery query;
	@Mock
	CatalogVersionModel catalogVersionModel;

	String messageCode = "messageCode";

	private Collection<CatalogVersionModel> catalogVersions;
	@Mock
	private CustomMessageModel searchResult;

	/**
	 *
	 */
	@Before
	public void setup()
	{
		catalogVersions = new ArrayList<>();
		catalogVersions.add(catalogVersionModel);
		doReturn(query).when(customMessageDaoImpl).getFlexibleSearchQuery(SimonFlexiConstants.MESSAGE_BY_CODE);
	}

	/**
	 *
	 */
	@Test
	public void getMessageTextNotEmpty()
	{
		doReturn(searchResult).when(flexibleSearchService).searchUnique(query);
		doReturn("messageText").when(searchResult).getMessageText();
		Assert.assertEquals(customMessageDaoImpl.getMessageTextForCode(messageCode, catalogVersions), "messageText");
	}

	/**
	 *
	 */
	@Test
	public void getMessageTextIsEmpty()
	{
		doReturn(searchResult).when(flexibleSearchService).searchUnique(query);
		doReturn(StringUtils.EMPTY).when(searchResult).getMessageText();
		Assert.assertEquals(customMessageDaoImpl.getMessageTextForCode(messageCode, catalogVersions), StringUtils.EMPTY);
	}

	/**
	 *
	 */
	@Test(expected = SystemException.class)
	public void getMessageTextWhenModelNotFoundException()
	{

		doThrow(new ModelNotFoundException("ModelNotFoundException")).when(flexibleSearchService).searchUnique(query);
		customMessageDaoImpl.getMessageTextForCode(messageCode, catalogVersions);
	}

	/**
	 *
	 */
	@Test(expected = SystemException.class)
	public void getMessageTextWhenAmbiguousIdentifierException()
	{

		doThrow(new AmbiguousIdentifierException("AmbiguousIdentifierException")).when(flexibleSearchService).searchUnique(query);
		customMessageDaoImpl.getMessageTextForCode(messageCode, catalogVersions);
	}
}
