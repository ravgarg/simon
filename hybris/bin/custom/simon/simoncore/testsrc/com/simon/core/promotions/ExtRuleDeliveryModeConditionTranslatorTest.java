package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruledefinitions.conditions.builders.IrConditions;
import de.hybris.platform.ruledefinitions.conditions.builders.RuleIrGroupConditionBuilder;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;


@UnitTest
public class ExtRuleDeliveryModeConditionTranslatorTest
{
	@InjectMocks
	ExtRuleDeliveryModeConditionTranslator translator;
	@Mock
	private RuleCompilerContext context;
	@Mock
	private RuleConditionData condition;
	@Mock
	private RuleConditionDefinitionData conditionDefinition;
	protected static final String RET_DELIVERY_MODE = "ret_delivery_mode";

	@Before
	public void setup()
	{
		initMocks(this);
	}

	@Test
	public void testTranslate_whenConditionParameterAreEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		assertEquals(IrConditions.newIrRuleFalseCondition().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenConditionParameterHaveValues_ButValuesAreEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(RET_DELIVERY_MODE, operatorParam);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(IrConditions.newIrRuleFalseCondition().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenVerifyAllPresentValueTrue()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(RET_DELIVERY_MODE, operatorParam);
		final String shop = "cheapest";
		operatorParam.setValue(shop);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND).build().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}

	@Test
	public void testTranslate_whenConditionParameterHaveValues_andValuesAreNotEmpty()
	{
		final Map<String, RuleParameterData> conditionParams = new HashMap<>();
		final RuleParameterData operatorParam = new RuleParameterData();
		conditionParams.put(RET_DELIVERY_MODE, operatorParam);
		final String shop = "cheapest";
		operatorParam.setValue(shop);
		when(condition.getParameters()).thenReturn(conditionParams);
		assertEquals(RuleIrGroupConditionBuilder.newGroupConditionOf(RuleIrGroupOperator.AND).build().getClass(),
				translator.translate(context, condition, conditionDefinition).getClass());
	}
}
