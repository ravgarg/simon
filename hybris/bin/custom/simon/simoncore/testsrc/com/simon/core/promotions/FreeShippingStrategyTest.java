package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.order.daos.DeliveryModeDao;
import de.hybris.platform.promotionengineservices.model.RuleBasedOrderChangeDeliveryModeActionModel;
import de.hybris.platform.promotionengineservices.promotionengine.PromotionActionService;
import de.hybris.platform.promotions.model.PromotionResultModel;
import de.hybris.platform.ruleengineservices.rao.AbstractRuleActionRAO;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.DeliveryModeRAO;
import de.hybris.platform.ruleengineservices.rao.ShipmentRAO;
import de.hybris.platform.servicelayer.model.ModelService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class FreeShippingStrategyTest
{
	@InjectMocks
	@Spy
	private FreeShippingActionStrategy freeShippingActionStrategy;
	@Mock
	private ModelService modelService;
	@Mock
	CartRAO cartRAO;
	@Mock
	private PromotionActionService promotionActionService;
	@Mock
	private DeliveryModeDao deliveryModeDao;

	DeliveryModeModel deliveryModeModel;
	PromotionResultModel promotionResult;
	AbstractOrderModel order;
	AbstractRuleActionRAO abstractRuleActionRAO;
	DeliveryModeModel shipmentModel;
	@Mock
	RuleBasedOrderChangeDeliveryModeActionModel actionModel;


	Map<String, DeliveryModeModel> retailerDeliveryModes;
	Map<String, Double> retailerDeliveryCost;

	@Before
	public void setup()
	{
		initMocks(this);
		abstractRuleActionRAO = new AbstractRuleActionRAO();
		promotionResult = new PromotionResultModel();
		order = new AbstractOrderModel();
		deliveryModeModel = new DeliveryModeModel();
		//actionModel = new RuleBasedOrderChangeDeliveryModeActionModel();

		deliveryModeModel.setCode("freeStandardShipping");

		retailerDeliveryModes = new HashMap<>();
		retailerDeliveryCost = new HashMap<>();

		retailerDeliveryModes.put("retailer1", deliveryModeModel);
		retailerDeliveryCost.put("retailer1", 0.0);

		order.setRetailersDeliveryCost(retailerDeliveryCost);
		order.setRetailersDeliveryModes(retailerDeliveryModes);

		doNothing().when(modelService).save(any());

	}

	@Test
	public void testUndo()
	{
		assertEquals("freeStandardShipping", order.getRetailersDeliveryModes().get("retailer1").getCode());

		final DeliveryModeModel setNewDelMode = new DeliveryModeModel();
		setNewDelMode.setCode("fastest");
		retailerDeliveryModes.put("retailer1", setNewDelMode);
		retailerDeliveryCost.put("retailer1", 14.0);

		order.setRetailersDeliveryCost(retailerDeliveryCost);
		order.setRetailersDeliveryModes(retailerDeliveryModes);

		promotionResult.setOrder(order);

		final RuleBasedOrderChangeDeliveryModeActionModel action = new RuleBasedOrderChangeDeliveryModeActionModel();
		final DeliveryModeModel deliveryMode = new DeliveryModeModel();
		final DeliveryModeModel replacedDeliveryMode = new DeliveryModeModel();
		final BigDecimal replaceDeliveryCost = new BigDecimal(4.5);


		deliveryMode.setCode("freeStandardShipping");
		replacedDeliveryMode.setCode("cheapest");

		action.setRetailer("retailer1");
		action.setReplacedDeliveryCost(replaceDeliveryCost);
		action.setReplacedDeliveryMode(replacedDeliveryMode);
		action.setDeliveryMode(deliveryMode);

		promotionResult.setOrder(order);
		action.setPromotionResult(promotionResult);

		Mockito.doReturn(order).when(freeShippingActionStrategy).performUndoInternal(action);
		freeShippingActionStrategy.undo(action);
	}

	@Test
	public void testApply()
	{
		final AbstractRuleActionRAO abstractRuleActionRAO = new AbstractRuleActionRAO();
		final ShipmentRAO shipmentRAO = new ShipmentRAO();
		final RetailerRAO retailerRAO = new RetailerRAO();
		final DeliveryModeRAO deliveryModeRao = new DeliveryModeRAO();
		deliveryModeRao.setCode("free-standard-shipping");
		retailerRAO.setRetailerId("retailer1");

		shipmentRAO.setAppliedToObject(cartRAO);
		shipmentRAO.setRetailer(retailerRAO);
		shipmentRAO.setMode(deliveryModeRao);

		promotionResult.setOrder(order);
		when(promotionActionService.createPromotionResult(shipmentRAO)).thenReturn(promotionResult);

		shipmentModel = new DeliveryModeModel();
		shipmentModel.setCode("freeStandardShipping");

		final List<DeliveryModeModel> deliveryModes = new ArrayList<DeliveryModeModel>();
		deliveryModes.add(shipmentModel);
		when(deliveryModeDao.findDeliveryModesByCode("free-standard-shipping")).thenReturn(deliveryModes);
		Mockito.doReturn(actionModel).when(freeShippingActionStrategy).getCreatedPromotionAction(promotionResult, shipmentRAO);

		final List result = freeShippingActionStrategy.apply(shipmentRAO);
		assertEquals(1, result.size());
	}


	@Test
	public void testApplyAppliedToObjectNotShipmentRAO()
	{
		final List<PromotionResultModel> result = freeShippingActionStrategy.apply(abstractRuleActionRAO);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testApplyAppliedToObjectNotCartRAO()
	{
		final ShipmentRAO shipmentRAO = new ShipmentRAO();
		shipmentRAO.setAppliedToObject(null);
		final List result = freeShippingActionStrategy.apply(shipmentRAO);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testApplyPromotionResultNull()
	{
		final ShipmentRAO shipmentRAO = new ShipmentRAO();
		when(promotionActionService.createPromotionResult(shipmentRAO)).thenReturn(null);

		final List result = freeShippingActionStrategy.apply(shipmentRAO);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testApplyAddedOrderNull()
	{
		final ShipmentRAO shipmentRAO = new ShipmentRAO();
		shipmentRAO.setAppliedToObject(cartRAO);
		when(promotionActionService.createPromotionResult(shipmentRAO)).thenReturn(promotionResult);

		final List result = freeShippingActionStrategy.apply(shipmentRAO);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testApplyDeliveryModeNull()
	{
		final ShipmentRAO shipmentRAO = new ShipmentRAO();
		promotionResult.setOrder(order);
		when(promotionActionService.createPromotionResult(shipmentRAO)).thenReturn(promotionResult);

		final DeliveryModeModel setNewDelMode = null;

		Mockito.doReturn(setNewDelMode).when(freeShippingActionStrategy).getDeliveryModeForCode("fastest");

		final List result = freeShippingActionStrategy.apply(shipmentRAO);
		assertTrue(result.isEmpty());
	}


}
