package com.simon.core.keygenerators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants;


/**
 * VariantValueCategoryKeyGeneratorTest unit test for {@link VariantValueCategoryKeyGenerator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantValueCategoryKeyGeneratorTest
{

	@InjectMocks
	private VariantValueCategoryKeyGenerator valueCategoryKeyGenerator;

	/**
	 * Verify exception thrown for generate without argument.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForGenerateWithoutArgument()
	{
		valueCategoryKeyGenerator.generate();
	}

	/**
	 * Verify exception thrown for reset.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForReset()
	{
		valueCategoryKeyGenerator.reset();
	}

	/**
	 * Verify code generated correctly.
	 */
	@Test
	public void verifyCodeGeneratedCorrectly()
	{
		assertThat(valueCategoryKeyGenerator.generateFor("10 B"),
				is(SimonCoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + "-" + "10-b"));
	}

	/**
	 * Verify code generated correctly.
	 */
	@Test
	public void verifyCodeGeneratedCorrectlyWithoutSlash()
	{
		assertThat(valueCategoryKeyGenerator.generateFor("///Black / White//    color     "),
				is(SimonCoreConstants.VARIANT_VALUE_CATEGORY_CODE_PREFIX + "-" + "black-white-color-"));
	}
}
