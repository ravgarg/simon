package com.simon.core.sitemap.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.AbstractSiteMapGenerator;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.ContentPageModelSiteMapGenerator;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * Test class for ExtContentPageModelSiteMapGenerator
 */
@UnitTest
public class ExtContentPageModelSiteMapGeneratorTest extends ContentPageModelSiteMapGenerator
{
	@Spy
	@InjectMocks
	ExtContentPageModelSiteMapGenerator extContentPageModelSiteMapGenerator;

	@Mock
	AbstractSiteMapGenerator abstractSiteMapGenerator;

	@Mock
	FlexibleSearchService flexibleSearchService;

	@Mock
	CMSSiteModel siteModel;

	@Mock
	SearchResult result;

	@Mock
	CatalogVersionService catalogVersionService;
	/**
	 * Sets the initial data for each test case.
	 */
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test Method for getDataInternal
	 */
	@Test
	public void testGetDataInternal()
	{
		final CategoryModel categoryModel = Mockito.mock(CategoryModel.class);
		final CatalogVersionModel catalogVersionModel = Mockito.mock(CatalogVersionModel.class);
		final List<CategoryModel> models = new ArrayList<>();
		final Collection<CatalogVersionModel> collection = new HashSet<>();
		final Map<String, Object> params = new HashMap<String, Object>();
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(result);
		Mockito.when(result.getResult()).thenReturn(models);
		Mockito.when(abstractSiteMapGenerator.getCatalogVersionService()).thenReturn(catalogVersionService);
		Mockito.when(catalogVersionService.getSessionCatalogVersions()).thenReturn(collection);
		Mockito.doReturn("").when(extContentPageModelSiteMapGenerator).buildOracleCompatibleStatement(params);
		collection.add(catalogVersionModel);
		models.add(categoryModel);

		extContentPageModelSiteMapGenerator.getDataInternal(siteModel);
		Assert.assertEquals(result.getResult(), models);
	}


}
