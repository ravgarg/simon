package com.simon.core.catalog.strategies.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCategoryCoreAttributeModel;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * The Class SimonCategoryAttributeHandlerTest. Unit test for {@link SimonCategoryAttributeHandler}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class SimonCategoryAttributeHandlerTest
{

	/** The simon category attribute handler. */
	@InjectMocks
	private SimonCategoryAttributeHandler simonCategoryAttributeHandler;

	/** The core attribute owner strategy. */
	@Mock
	private CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	@Mock
	private ModelService modelService;

	@Mock
	private CategoryService categoryService;


	/**
	 * Setup method.
	 */
	@Before
	public void setUp()
	{
		when(coreAttributeOwnerStrategy.determineOwner(any(MiraklCoreAttributeModel.class), any(ProductImportData.class),
				any(ProductImportFileContextData.class))).thenReturn(mock(ProductModel.class));
	}

	/**
	 * Verify null categories are not set on variant and product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNullCategoriesAreNotSetOnVariantAndProduct() throws ProductImportException
	{
		final AttributeValueData attributeValue = mock(AttributeValueData.class);
		when(attributeValue.getCoreAttribute()).thenReturn(mock(MiraklCategoryCoreAttributeModel.class));

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final VariantProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		mockCommonData(data, context, productCatalogVersion, variantProduct, baseProduct);

		simonCategoryAttributeHandler.setValue(attributeValue, data, context);

		verify(variantProduct, times(0)).setMiraklCategory(any(CategoryModel.class));
	}

	/**
	 * Verify valid categories are set correctly on variant and base.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyValidCategoriesAreSetCorrectlyOnVariantAndBase() throws ProductImportException
	{
		final AttributeValueData attributeValue = mock(AttributeValueData.class);
		when(attributeValue.getCoreAttribute()).thenReturn(mock(MiraklCategoryCoreAttributeModel.class));
		when(attributeValue.getValue()).thenReturn("cat01");

		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final VariantProductModel variantProduct = mock(VariantProductModel.class);
		final ProductModel baseProduct = mock(ProductModel.class);
		mockCommonData(data, context, productCatalogVersion, variantProduct, baseProduct);

		final CategoryModel category = mock(CategoryModel.class);
		when(categoryService.getCategoryForCode(productCatalogVersion, "cat01")).thenReturn(category);

		simonCategoryAttributeHandler.setValue(attributeValue, data, context);

		verify(variantProduct).setMiraklCategory(category);
		verify(baseProduct).setMiraklCategory(category);
	}

	/**
	 * Mock common data.
	 *
	 * @param data
	 *           the data
	 * @param context
	 * @param variantProduct
	 *           the variant product
	 * @param baseProduct
	 *           the base product
	 */
	private void mockCommonData(final ProductImportData data, final ProductImportFileContextData context,
			final CatalogVersionModel productCatalogVersion, final VariantProductModel variantProduct,
			final ProductModel baseProduct)
	{
		final ProductImportGlobalContextData globalContext = mock(ProductImportGlobalContextData.class);
		final PK catalogVersionPK = PK.fromLong(1L);

		when(context.getGlobalContext()).thenReturn(globalContext);
		when(globalContext.getProductCatalogVersion()).thenReturn(catalogVersionPK);
		when(modelService.get(catalogVersionPK)).thenReturn(productCatalogVersion);

		when(variantProduct.getItemtype()).thenReturn("VariantProduct");
		when(data.getProductToUpdate()).thenReturn(variantProduct);

		when(data.getRootBaseProductToUpdate()).thenReturn(baseProduct);
		when(baseProduct.getItemtype()).thenReturn("Product");

		when(variantProduct.getBaseProduct()).thenReturn(baseProduct);
	}
}
