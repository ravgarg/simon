package com.simon.core.interceptors;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.PersistenceOperation;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantValueCategoryRemoveInterceptorTest
{
	@InjectMocks
	private VariantValueCategoryRemoveInterceptor variantValueCategoryRemoveInterceptor;

	@Mock
	private VariantValueCategoryModel variantValueCategory;

	@Mock
	private MediaModel thumbail;

	@Mock
	private InterceptorContext ctx;

	@Test
	public void testThumbailGetsRegistered() throws InterceptorException
	{
		when(variantValueCategory.getThumbnail()).thenReturn(thumbail);
		variantValueCategoryRemoveInterceptor.onRemove(variantValueCategory, ctx);
		verify(ctx, Mockito.times(1)).registerElementFor(thumbail, PersistenceOperation.DELETE);
	}

}
