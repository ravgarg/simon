package com.simon.core.services.impl;

import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.internal.dao.GenericDao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ControlType;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.model.VariantAttributeMappingModel;
import com.simon.core.services.ShopGatewayService;


/**
 * VariantAttributeMappingServiceTest unit test for {@link VariantAttributeMappingServiceImpl}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class VariantAttributeMappingServiceTest
{
	@InjectMocks
	private VariantAttributeMappingServiceImpl variantAttributeMappingService;

	@Mock
	private GenericDao<VariantAttributeMappingModel> variantAttributeMappingGenericDao;

	@Mock
	private ShopGatewayService shopGatewayService;

	/**
	 * Test variant attribute mapping for shop when shop gateway exist.
	 */
	@Test
	public void testVariantAttributeMappingForShopWhenShopGatewayExist()
	{
		final ShopModel shop = mock(ShopModel.class);
		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final Map<String, Object> paramMap = ImmutableMap.of("targetSystem", shopGateway, "shop", shop);

		when(variantAttributeMappingGenericDao.find(paramMap)).thenReturn(singletonList(new VariantAttributeMappingModel()));
		assertNotNull(variantAttributeMappingService.getVariantAttributeMappingForShop(shop, shopGateway));
	}

	/**
	 * Test shop gateway not exist exception.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testShopGatewayNotExistException()
	{
		final ShopModel shop = mock(ShopModel.class);
		variantAttributeMappingService.getVariantAttributeMappingForShop(shop, null);
	}

	/**
	 * Test variant attribute mapping when shop gateway exist.
	 */
	@Test
	public void testVariantAttributeMappingWhenShopGatewayExist()
	{
		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final Map<String, Object> paramMap = ImmutableMap.of("targetSystem", shopGateway);

		when(variantAttributeMappingGenericDao.find(paramMap)).thenReturn(singletonList(new VariantAttributeMappingModel()));
		assertNotNull(variantAttributeMappingService.getVariantAttributeMapping(shopGateway));
	}

	/**
	 * Test variant attribute mapping when shop gateway not exist exception.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testVariantAttributeMappingWhenShopGatewayNotExistException()
	{
		variantAttributeMappingService.getVariantAttributeMapping(null);
	}

	/**
	 * Verify correct source attribute label map is returned.
	 */
	@Test
	public void verifyCorrectSourceAttributeLabelMapIsReturned()
	{
		final VariantAttributeMappingModel variantAttributeMappingModel1 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel1.getSourceAttribute()).thenReturn("VC-color");
		when(variantAttributeMappingModel1.getSourceAttributeLabel()).thenReturn("color");

		final VariantAttributeMappingModel variantAttributeMappingModel2 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel2.getSourceAttribute()).thenReturn("VC-size");
		when(variantAttributeMappingModel2.getSourceAttributeLabel()).thenReturn("size");

		final List<VariantAttributeMappingModel> variantAttributeMappings = Arrays.asList(variantAttributeMappingModel1,
				variantAttributeMappingModel2);

		when(variantAttributeMappingGenericDao.find(anyMapOf(String.class, String.class))).thenReturn(variantAttributeMappings);

		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final ShopModel shop = mock(ShopModel.class);
		final Map<String, String> actualVariantAttributeLabelMap = variantAttributeMappingService.getVariantAttributeLabelMap(shop,
				shopGateway);

		final Map<String, String> expectedVariantAttributeLabelMap = new HashMap<>();
		expectedVariantAttributeLabelMap.put("VC-color", "color");
		expectedVariantAttributeLabelMap.put("VC-size", "size");
		assertThat(actualVariantAttributeLabelMap, is(expectedVariantAttributeLabelMap));
	}

	/**
	 * Verify correct control type map is returned.
	 */
	@Test
	public void verifyCorrectControlTypeMapIsReturned()
	{
		final VariantAttributeMappingModel variantAttributeMappingModel1 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel1.getSourceAttribute()).thenReturn("VC-color");
		when(variantAttributeMappingModel1.getControlType()).thenReturn(ControlType.SWATCH);

		final VariantAttributeMappingModel variantAttributeMappingModel2 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel2.getSourceAttribute()).thenReturn("VC-size");
		when(variantAttributeMappingModel2.getControlType()).thenReturn(ControlType.DROPDOWN);

		final List<VariantAttributeMappingModel> variantAttributeMappings = Arrays.asList(variantAttributeMappingModel1,
				variantAttributeMappingModel2);

		when(variantAttributeMappingGenericDao.find(anyMapOf(String.class, String.class))).thenReturn(variantAttributeMappings);

		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final ShopModel shop = mock(ShopModel.class);
		final Map<String, ControlType> actualVariantAttributeControlTypeMap = variantAttributeMappingService
				.getVariantAttributeControlTypeMap(shop, shopGateway);

		final Map<String, ControlType> expectedVariantAttributeControlTypeMap = new HashMap<>();
		expectedVariantAttributeControlTypeMap.put("VC-color", ControlType.SWATCH);
		expectedVariantAttributeControlTypeMap.put("VC-size", ControlType.DROPDOWN);
		assertThat(actualVariantAttributeControlTypeMap, is(expectedVariantAttributeControlTypeMap));
	}

	/**
	 * Verify correct source attribute label map is returned.
	 */
	@Test
	public void verifyVarinatAttributeTwoTapLabelMap()
	{
		final VariantAttributeMappingModel variantAttributeMappingModel1 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel1.getSourceAttribute()).thenReturn("VC-color");
		when(variantAttributeMappingModel1.getTargetAttribute()).thenReturn("color");

		final VariantAttributeMappingModel variantAttributeMappingModel2 = mock(VariantAttributeMappingModel.class);
		when(variantAttributeMappingModel2.getSourceAttribute()).thenReturn("VC-size");
		when(variantAttributeMappingModel2.getTargetAttribute()).thenReturn("size");

		final List<VariantAttributeMappingModel> variantAttributeMappings = Arrays.asList(variantAttributeMappingModel1,
				variantAttributeMappingModel2);

		when(variantAttributeMappingGenericDao.find(anyMapOf(String.class, String.class))).thenReturn(variantAttributeMappings);

		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final ShopModel shop = mock(ShopModel.class);
		final Map<String, String> actualVariantAttributeLabelMap = variantAttributeMappingService
				.getVarinatAttributeTwoTapLabelMap(shop, shopGateway);

		final Map<String, String> expectedVariantAttributeLabelMap = new HashMap<>();
		expectedVariantAttributeLabelMap.put("VC-color", "color");
		expectedVariantAttributeLabelMap.put("VC-size", "size");
		assertThat(actualVariantAttributeLabelMap, is(expectedVariantAttributeLabelMap));
	}
}
