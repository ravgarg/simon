package com.simon.core.sitemap.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.sitemap.generator.impl.AbstractSiteMapGenerator;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;


/**
 * Test class for ExtCategoryPageSiteMapGenerator
 */
@UnitTest
public class ExtCategoryPageSiteMapGeneratorTest
{
	@Spy
	@InjectMocks
	ExtCategoryPageSiteMapGenerator extCategoryPageSiteMapGenerator;

	@Mock
	AbstractSiteMapGenerator abstractSiteMapGenerator;

	@Mock
	FlexibleSearchService flexibleSearchService;

	@Mock
	CMSSiteModel siteModel;

	@Mock
	SearchResult result;
	/**
	 * Sets the initial data for each test case.
	 */
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test Method for getDataInternal
	 */
	@Test
	public void testGetDataInternal()
	{
		final CategoryModel categoryModel = Mockito.mock(CategoryModel.class);
		final List<CategoryModel> models = new ArrayList<>();
		models.add(categoryModel);
		Mockito.when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(result);
		Mockito.when(result.getResult()).thenReturn(models);
		extCategoryPageSiteMapGenerator.getDataInternal(siteModel);
		Assert.assertEquals(result.getResult(), models);
	}

}
