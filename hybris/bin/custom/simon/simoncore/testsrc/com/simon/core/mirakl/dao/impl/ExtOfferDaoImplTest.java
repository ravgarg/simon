package com.simon.core.mirakl.dao.impl;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.ordersplitting.model.StockLevelModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.validation.services.ValidationService;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;

import com.simon.core.enums.PriceType;

import jersey.repackaged.com.google.common.collect.Sets;


/**
 * ExtOfferDaoImplTest integration test for {@link ExtOfferDaoImpl}.
 */
@IntegrationTest
public class ExtOfferDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource
	private ExtOfferDaoImpl extOfferDaoImpl;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private static CatalogVersionService catalogVersionService = (CatalogVersionService) Registry.getApplicationContext()
			.getBean("catalogVersionService");

	@Resource
	private static ValidationService validationService = (ValidationService) Registry.getApplicationContext()
			.getBean("validationService");

	/**
	 * Before.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void before() throws ImpExException
	{
		validationService.unloadValidationEngine();
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testCategories.impex", "utf-8");
		importCsv("/simoncore/test/testProducts.impex", "utf-8");
		importCsv("/simoncore/test/testPrices.impex", "utf-8");
		importCsv("/simoncore/test/testStockLevels.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify exception thrown list prices for product codes when product codes are null.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownFindPricesForProductCodesWhenProductCodesAreNull()
	{
		extOfferDaoImpl.findPricesForProductCodes(null, PriceType.LIST);
	}

	/**
	 * Verify exception thrown find prices for product codes when price type is null.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownFindPricesForProductCodesWhenPriceTypeIsNull()
	{
		extOfferDaoImpl.findPricesForProductCodes(Sets.newHashSet("testProductMVId1", "testProductMVId2"), null);
	}

	/**
	 * Verify list prices for product codes are fetched correctly.
	 */
	@Test
	public void verifyListPricesForProductCodesAreFetchedCorrectly()
	{
		final List<PriceRowModel> listPriceMap = extOfferDaoImpl
				.findPricesForProductCodes(Sets.newHashSet("testProductMVId1", "testProductMVId2"), PriceType.LIST);
		assertThat(listPriceMap.get(0).getProductId(), is("testProductMVId1"));
		assertThat(listPriceMap.get(0).getPriceType(), is(PriceType.LIST));
		assertThat(listPriceMap.get(1).getProductId(), is("testProductMVId2"));
		assertThat(listPriceMap.get(1).getPriceType(), is(PriceType.LIST));
	}

	/**
	 * Verify sale prices for product codes are fetched correctly.
	 */
	@Test
	public void verifySalePricesForProductCodesAreFetchedCorrectly()
	{
		final List<PriceRowModel> listPriceMap = extOfferDaoImpl
				.findPricesForProductCodes(Sets.newHashSet("testProductMVId1", "testProductMVId2"), PriceType.SALE);
		assertThat(listPriceMap.get(0).getProductId(), is("testProductMVId1"));
		assertThat(listPriceMap.get(0).getPriceType(), is(PriceType.SALE));
		assertThat(listPriceMap.get(1).getProductId(), is("testProductMVId2"));
		assertThat(listPriceMap.get(1).getPriceType(), is(PriceType.SALE));
	}

	/**
	 * Verify exception thrown price rows modified before date fetched when modified date is null.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownPriceRowsModifiedBeforeDateFetchedWhenModifiedDateIsNull()
	{
		extOfferDaoImpl.findPricesModifiedBeforeDate(null);
	}

	/**
	 * Verify all price rows are fetched if modified date is in future.
	 */
	@Test
	public void verifyAllPriceRowsAreFetchedIfModifiedDateIsInFuture()
	{
		final Date currentDate = new Date();
		final Date dayAfter = new Date(currentDate.getTime() + TimeUnit.DAYS.toMillis(365));
		final List<PriceRowModel> priceNotModified = extOfferDaoImpl.findPricesModifiedBeforeDate(dayAfter);
		assertThat(priceNotModified, hasSize(greaterThan(0)));
	}

	/**
	 * Verify no price rows are fetched if modified date is in past.
	 */
	@Test
	public void verifyNoPriceRowsAreFetchedIfModifiedDateIsInPast()
	{
		final Date currentDate = new Date();
		final Date dayPast = new Date(currentDate.getTime() - TimeUnit.DAYS.toMillis(365));
		final List<PriceRowModel> priceNotModified = extOfferDaoImpl.findPricesModifiedBeforeDate(dayPast);
		assertThat(priceNotModified, IsEmptyCollection.empty());
	}

	/**
	 * Verify exception thrown Stocks modified before date fetched when modified date is null.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownStockRowsModifiedBeforeDateFetchedWhenModifiedDateIsNull()
	{
		extOfferDaoImpl.findStocksModifiedBeforeDate(null);
	}

	/**
	 * Verify all Stocks are fetched if modified date is in future.
	 */
	@Test
	public void verifyAllStockRowsAreFetchedIfModifiedDateIsInFuture()
	{
		final Date currentDate = new Date();
		final Date dayAfter = new Date(currentDate.getTime() + TimeUnit.DAYS.toMillis(365));
		final List<StockLevelModel> stockNotModified = extOfferDaoImpl.findStocksModifiedBeforeDate(dayAfter);
		assertThat(stockNotModified, hasSize(greaterThan(0)));
	}

	/**
	 * Verify no Stocks are fetched if modified date is in past.
	 */
	@Test
	public void verifyNoStockRowsAreFetchedIfModifiedDateIsInPast()
	{
		final Date currentDate = new Date();
		final Date dayPast = new Date(currentDate.getTime() - TimeUnit.DAYS.toMillis(365));
		final List<StockLevelModel> stockNotModified = extOfferDaoImpl.findStocksModifiedBeforeDate(dayPast);
		assertThat(stockNotModified, IsEmptyCollection.empty());
	}
}
