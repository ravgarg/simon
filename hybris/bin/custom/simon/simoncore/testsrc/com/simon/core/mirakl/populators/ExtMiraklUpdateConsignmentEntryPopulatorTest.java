package com.simon.core.mirakl.populators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.mirakl.client.mmp.domain.additionalfield.MiraklAdditionalFieldType;
import com.mirakl.client.mmp.domain.common.MiraklAdditionalFieldValue.MiraklAbstractAdditionalFieldWithSingleValue;
import com.mirakl.client.mmp.domain.order.MiraklOrderLine;
import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.ConsignmentEntryCustomFields;


@UnitTest
public class ExtMiraklUpdateConsignmentEntryPopulatorTest
{
	@InjectMocks
	private ExtMiraklUpdateConsignmentEntryPopulator extMiraklUpdateConsignmentEntryPopulator;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void populaterCustomLineStatusTest()
	{
		final MiraklOrderLine miraklOrderLine = mock(MiraklOrderLine.class);
		final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("SHIPPING");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ConsignmentEntryCustomFields.ORDER_LINE_STATUS);
		when(miraklOrderLine.getAdditionalFields()).thenReturn(Arrays.asList(additionalFieldValue));
		when(miraklOrderLine.getId()).thenReturn("Test");
		when(miraklOrderLine.getQuantity()).thenReturn(SimonCoreConstants.FIRST_INT);
		when(miraklOrderLine.getCanOpenIncident()).thenReturn(Boolean.FALSE);
		extMiraklUpdateConsignmentEntryPopulator.populate(miraklOrderLine, consignmentEntryModel);
	}

	@Test
	public void populaterCustomTrackingNoTest()
	{
		final MiraklOrderLine miraklOrderLine = mock(MiraklOrderLine.class);
		final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("123");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ConsignmentEntryCustomFields.TRACKING_NUMBER);
		when(miraklOrderLine.getAdditionalFields()).thenReturn(Arrays.asList(additionalFieldValue));
		when(miraklOrderLine.getId()).thenReturn("Test");
		when(miraklOrderLine.getQuantity()).thenReturn(SimonCoreConstants.FIRST_INT);
		when(miraklOrderLine.getCanOpenIncident()).thenReturn(Boolean.FALSE);
		extMiraklUpdateConsignmentEntryPopulator.populate(miraklOrderLine, consignmentEntryModel);
		verify(consignmentEntryModel).setTrackingNo("123");
	}

	@Test
	public void populaterCustomCancellationReasonTest()
	{
		final MiraklOrderLine miraklOrderLine = mock(MiraklOrderLine.class);
		final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("This is cancelled");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ConsignmentEntryCustomFields.CANCELLATION_REASON);
		when(miraklOrderLine.getAdditionalFields()).thenReturn(Arrays.asList(additionalFieldValue));
		when(miraklOrderLine.getId()).thenReturn("Test");
		when(miraklOrderLine.getQuantity()).thenReturn(SimonCoreConstants.FIRST_INT);
		when(miraklOrderLine.getCanOpenIncident()).thenReturn(Boolean.FALSE);
		extMiraklUpdateConsignmentEntryPopulator.populate(miraklOrderLine, consignmentEntryModel);
		verify(consignmentEntryModel).setCancellationReason("This is cancelled");
	}

	@Test
	public void populaterCustomTaxAmountTest()
	{
		final MiraklOrderLine miraklOrderLine = mock(MiraklOrderLine.class);
		final ConsignmentEntryModel consignmentEntryModel = mock(ConsignmentEntryModel.class);
		final MiraklAbstractAdditionalFieldWithSingleValue additionalFieldValue = mock(
				MiraklAbstractAdditionalFieldWithSingleValue.class);
		when(additionalFieldValue.getValue()).thenReturn("2.00");
		when(additionalFieldValue.getFieldType()).thenReturn(MiraklAdditionalFieldType.STRING);
		when(additionalFieldValue.getCode()).thenReturn(ConsignmentEntryCustomFields.TAX_AMOUNT);
		when(miraklOrderLine.getAdditionalFields()).thenReturn(Arrays.asList(additionalFieldValue));
		when(miraklOrderLine.getId()).thenReturn("Test");
		when(miraklOrderLine.getQuantity()).thenReturn(SimonCoreConstants.FIRST_INT);
		when(miraklOrderLine.getCanOpenIncident()).thenReturn(Boolean.FALSE);
		extMiraklUpdateConsignmentEntryPopulator.populate(miraklOrderLine, consignmentEntryModel);
		verify(consignmentEntryModel).setTaxAmount(2.00);
	}
}
