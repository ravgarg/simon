package com.simon.core.mirakl.services.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.common.collect.ImmutableMap;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.enums.MiraklAttributeRole;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.util.services.impl.DefaultCsvService;
import com.mirakl.hybris.core.util.strategies.ChecksumCalculationStrategy;
import com.simon.core.caches.DesignerPreProcessor;
import com.simon.core.caches.ProductFileImportPreProcessor;
import com.simon.core.caches.VariantCategoryPreProcessor;
import com.simon.core.caches.VariantValueCategoryPreProcessor;

import shaded.org.supercsv.io.ICsvMapReader;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtMiraklRawProductImportServiceTest
{
	@InjectMocks
	private ExtMiraklRawProductImportService extMiraklRawProductImportService;

	@Mock
	private ModelService modelService;

	@Mock
	private ChecksumCalculationStrategy checksumCalculationStrategy;

	@Mock
	private MiraklRawProductModel rawProduct;

	@Mock
	private DesignerPreProcessor designerPreProcessor;

	@Mock
	private VariantCategoryPreProcessor variantCategoryPreProcessor;

	@Mock
	private VariantValueCategoryPreProcessor variantValueCategoryPreProcessor;

	@Mock
	private ICsvMapReader csvMapReader;

	@Mock
	private ProductImportFileContextData context;

	@Mock
	private ProductImportGlobalContextData globalContext;

	@Spy
	private DefaultCsvService csvService;

	@Mock
	private Populator<String[], ProductImportFileContextData> headerFileContextPopulator;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	private Map<String, List<ProductFileImportPreProcessor>> preProcessorsMap;

	@Test
	public void verifyAllPreProcessorCalled()
	{
		setPreProcessorMap();
		final Map<String, String> productValues = ImmutableMap.of("colorSwatchUrl", "https://dummyimage.com/600x400/f211f2/fff");
		final Map<MiraklAttributeRole, String> coreAttributePerRole = ImmutableMap.of(MiraklAttributeRole.SHOP_SKU_ATTRIBUTE,
				"shop", MiraklAttributeRole.VARIANT_GROUP_CODE_ATTRIBUTE, "variant");

		when(modelService.create(MiraklRawProductModel.class)).thenReturn(rawProduct);
		when(checksumCalculationStrategy.calculateChecksum(Matchers.anyString())).thenReturn("checksum");
		when(context.getGlobalContext()).thenReturn(globalContext);
		when(globalContext.getCoreAttributePerRole()).thenReturn(coreAttributePerRole);

		extMiraklRawProductImportService.createRawProduct(productValues, csvMapReader, context, "2008");

		Mockito.verify(designerPreProcessor).preProcess("productDesigner", productValues, context);
		Mockito.verify(variantCategoryPreProcessor).preProcess("variantAttributes", productValues, context);
		Mockito.verify(variantValueCategoryPreProcessor).preProcess("variantAttributes", productValues, context);
	}

	@Test
	public void verifyCacheCleanup() throws IOException
	{
		setPreProcessorMap();
		final File inputFile = folder.newFile();
		FileUtils.writeStringToFile(inputFile, "header1,header2,header3");
		extMiraklRawProductImportService.importRawProducts(inputFile, context);
		Mockito.verify(designerPreProcessor).cleanup();
		Mockito.verify(variantCategoryPreProcessor).cleanup();
		Mockito.verify(variantValueCategoryPreProcessor).cleanup();
	}

	public void setPreProcessorMap()
	{
		preProcessorsMap = ImmutableMap.of("productDesigner", Arrays.asList(designerPreProcessor), "variantAttributes",
				Arrays.asList(variantCategoryPreProcessor, variantValueCategoryPreProcessor));
		extMiraklRawProductImportService.setImportCacheMap(preProcessorsMap);
	}
}
