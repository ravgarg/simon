package com.simon.core.mirakl.strategies.impl;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.simon.core.exceptions.ProductFileImportPostProcessingException;
import com.simon.core.mirakl.strategies.ProductFileImportPostProcessor;


/**
 * ExtPostProcessProductFileImportStrategyTest unit test for {@link ExtPostProcessProductFileImportStrategy}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtPostProcessProductFileImportStrategyTest
{
	@InjectMocks
	@Spy
	private ExtPostProcessProductFileImportStrategy extPostProcessProductFileImportStrategy;

	@Mock
	private ConfigurationService configurationService;

	/**
	 * Verify null post processors are not processed.
	 *
	 * @throws ProductFileImportPostProcessingException
	 *            the product file import post processing exception
	 */
	@Test
	public void verifyNullPostProcessorsAreNotProcessed() throws ProductFileImportPostProcessingException
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final List<ProductFileImportPostProcessor> postprocessors = new ArrayList<>();

		final MediaConversionPostProcessor enabledProcessor = mock(MediaConversionPostProcessor.class);
		when(extPostProcessProductFileImportStrategy.isEnabled(enabledProcessor)).thenReturn(true);
		postprocessors.add(enabledProcessor);
		postprocessors.add(null);

		extPostProcessProductFileImportStrategy.setPostprocessors(postprocessors);
		extPostProcessProductFileImportStrategy.postProcess(context, "12345");

		verify(enabledProcessor).postProcess(context, "12345");
	}

	/**
	 * Verify only enabled post processors are processed.
	 *
	 * @throws ProductFileImportPostProcessingException
	 *            the product file import post processing exception
	 */
	@Test
	public void verifyOnlyEnabledPostProcessorsAreProcessed() throws ProductFileImportPostProcessingException
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final List<ProductFileImportPostProcessor> postprocessors = new ArrayList<>();

		final ErrorHandlingPostProcessor enabledProcessor = mock(ErrorHandlingPostProcessor.class);
		when(extPostProcessProductFileImportStrategy.isEnabled(enabledProcessor)).thenReturn(true);
		postprocessors.add(enabledProcessor);

		final MediaConversionPostProcessor disabledProcessor = mock(MediaConversionPostProcessor.class);
		when(extPostProcessProductFileImportStrategy.isEnabled(disabledProcessor)).thenReturn(false);
		postprocessors.add(disabledProcessor);

		extPostProcessProductFileImportStrategy.setPostprocessors(postprocessors);
		extPostProcessProductFileImportStrategy.postProcess(context, "12345");

		verify(enabledProcessor).postProcess(context, "12345");
		verify(disabledProcessor, times(0)).postProcess(context, "12345");
	}

	/**
	 * Verify errored post processor ignored safely.
	 *
	 * @throws ProductFileImportPostProcessingException
	 *            the product file import post processing exception
	 */
	@Test
	public void verifyErroredPostProcessorIgnoredSafely() throws ProductFileImportPostProcessingException
	{
		final Configuration configuration = mock(Configuration.class);
		when(configurationService.getConfiguration()).thenReturn(configuration);

		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final List<ProductFileImportPostProcessor> postprocessors = new ArrayList<>();

		final ErrorHandlingPostProcessor erroredPostProcessor = mock(ErrorHandlingPostProcessor.class);
		when(extPostProcessProductFileImportStrategy.isEnabled(erroredPostProcessor)).thenReturn(true);
		doThrow(new ProductFileImportPostProcessingException("", new ModelSavingException(""))).when(erroredPostProcessor)
				.postProcess(context, "12345");
		postprocessors.add(erroredPostProcessor);

		final MediaConversionPostProcessor nonErroredPostProcessor = mock(MediaConversionPostProcessor.class);
		when(extPostProcessProductFileImportStrategy.isEnabled(nonErroredPostProcessor)).thenReturn(true);
		postprocessors.add(nonErroredPostProcessor);

		extPostProcessProductFileImportStrategy.setPostprocessors(postprocessors);
		extPostProcessProductFileImportStrategy.postProcess(context, "12345");

		verify(erroredPostProcessor).postProcess(context, "12345");
		verify(nonErroredPostProcessor).postProcess(context, "12345");
	}
}
