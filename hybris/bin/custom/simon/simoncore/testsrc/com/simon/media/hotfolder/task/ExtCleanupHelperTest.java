package com.simon.media.hotfolder.task;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * test clean up files
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtCleanupHelperTest
{
	@Mock
	BatchHeader header;
	@Mock
	File file;

	@InjectMocks
	private final ExtCleanupHelper simonCleanupTask = Mockito.spy(new ExtCleanupHelper());

	/**
	 * test clean up files method
	 */
	@Test
	public void cleanup()
	{

		when(header.getFile()).thenReturn(file);
		when(header.getFile().getName()).thenReturn("file");
		simonCleanupTask.setInPutFile("file");
		simonCleanupTask.setOutPutFile("file");
		simonCleanupTask.setInPutDir("file");
		simonCleanupTask.setOutPutDir("file");
		doNothing().when(simonCleanupTask).superCleanup(header, false);
		simonCleanupTask.cleanup(header, false);
		Assert.assertNotNull(header.getSequenceId());

	}

	/**
	 * test clean up files method
	 */
	@Test
	public void cleanupWhenNoError()
	{

		Mockito.when(header.getFile()).thenReturn(file);
		Mockito.when(header.getFile().getName()).thenReturn("file");
		simonCleanupTask.setInPutFile("file");
		simonCleanupTask.setOutPutFile("file");
		simonCleanupTask.setInPutDir("file");
		simonCleanupTask.setOutPutDir("file");
		doNothing().when(simonCleanupTask).superCleanup(header, true);
		simonCleanupTask.cleanup(header, true);
		Assert.assertNotNull(header.getSequenceId());

	}

	/**
	 * test clean up files method
	 */
	@Test
	public void cleanupWhenFileNameNotMatch()
	{

		Mockito.when(header.getFile()).thenReturn(file);
		Mockito.when(header.getFile().getName()).thenReturn("file");
		simonCleanupTask.setInPutFile("other");
		simonCleanupTask.setOutPutFile("file");
		simonCleanupTask.setInPutDir("file");
		simonCleanupTask.setOutPutDir("file");
		doNothing().when(simonCleanupTask).superCleanup(header, false);
		simonCleanupTask.cleanup(header, false);
		Assert.assertNotNull(header.getSequenceId());

	}

}
