package com.simon.core.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.dao.ExtShopDao;
import com.simon.core.services.ExtShopService;

import junit.framework.Assert;


/**
 * The Class ExtStoreServiceImplTest. Unit test for {@link ExtShopService}}
 */
@UnitTest
public class ExtShopServiceImplTest
{
	/** The default store service. */
	@InjectMocks
	private ExtShopServiceImpl shopService;

	/** The shop dao. */
	@Mock
	private ExtShopDao extShopDao;


	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionIsThrownForNullCodes()
	{
		shopService.getListOfShopsForCodes(null);
	}

	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyShopListReturnedFromDaoIsReturnedCorrectly()
	{
		final List<ShopModel> shopList = new ArrayList<ShopModel>();
		final ShopModel shopModel = mock(ShopModel.class);
		shopList.add(shopModel);
		final String[] codes = new String[1];
		codes[0] = "testShop1";
		when(extShopDao.findListOfShopsForCodes(codes)).thenReturn(shopList);
		assertEquals(shopList, shopService.getListOfShopsForCodes(codes));
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionIsThrownForNullObjectCodes()
	{
		shopService.getListOfShopsForCodes(null);
	}

	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyObjectShopListReturnedFromDaoIsReturnedCorrectly()
	{
		final List<ShopModel> shopList = new ArrayList<ShopModel>();
		final ShopModel shopModel = mock(ShopModel.class);
		shopList.add(shopModel);
		final String[] codes = new String[1];
		codes[0] = "testShop1";
		when(extShopDao.findListOfShopsForCodes(codes)).thenReturn(shopList);
		assertEquals(shopList, shopService.getListOfShopsForCodes(codes));
	}

	@Test
	public void testgetRetailerlist()
	{
		final ShopModel shop = new ShopModel();
		shop.setId("2000");
		shop.setName("test");
		final List<ShopModel> shopList = new ArrayList();
		shopList.add(shop);
		Mockito.when(extShopDao.getRetailerList()).thenReturn(shopList);
		shopService.getRetailerlist();
		Assert.assertNotNull(shopService.getRetailerlist());
	}
}
