package com.simon.core.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.simon.core.mirakl.product.dao.MiraklRawProductDAO;


@IntegrationTest
public class MiraklRawProductDAOImplTest extends ServicelayerTransactionalTest
{

	@Resource
	MiraklRawProductDAO miraklRawProductDAOImpl;

	private static final String IMPORT_ID = "import-1";

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		importCsv("/simoncore/test/testMiraklProducts.impex", "utf-8");
		MockitoAnnotations.initMocks(this);
	}

	/**
	 *
	 */
	@Test
	public void testfetchMiraklProductsForImport_NORESULTS()
	{
		final List<MiraklRawProductModel> miraklProductList = miraklRawProductDAOImpl.fetchMiraklProductsForImport("test");
		assertEquals(0, miraklProductList.size());
	}

	/**
	 *
	 */
	@Test
	public void testfetchMiraklProductsForImport()
	{
		final List<MiraklRawProductModel> miraklProductList = miraklRawProductDAOImpl.fetchMiraklProductsForImport(IMPORT_ID);
		assertTrue(CollectionUtils.isNotEmpty(miraklProductList));
	}
}
