package com.simon.core.keygenerators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * DesignerKeyGeneratorTest unit test for {@link DesignerKeyGenerator}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DesignerKeyGeneratorTest
{
	@InjectMocks
	private DesignerKeyGenerator designerKeyGenerator;

	/**
	 * Verify exception thrown for generate without argument.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForGenerateWithoutArgument()
	{
		designerKeyGenerator.generate();
	}

	/**
	 * Verify exception thrown for reset.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void verifyExceptionThrownForReset()
	{
		designerKeyGenerator.reset();
	}

	/**
	 * Verify code generated correctly.
	 */
	@Test
	public void verifyCodeGeneratedCorrectly()
	{
		assertThat(designerKeyGenerator.generateFor("10 TEST"), is("PD" + "-" + "10-test"));
	}

}
