package com.simon.core.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.core.model.DesignerModel;


/**
 * The Class DefaultDesignerDaoTest. Integration test for {@link DefaultDesignerDao}
 */
@IntegrationTest
public class DefaultDesignerDaoTest extends ServicelayerTransactionalTest
{

	@Resource
	private DefaultDesignerDao defaultDesignerDao;

	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testDesigners.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify null designer is returned for invalid code.
	 */
	@Test
	public void verifyNullDesignerIsReturnedForInvalidCode()
	{
		final DesignerModel actual = defaultDesignerDao.findDesignerByCode("InvalidDesignerCode");
		assertNull(actual);
	}

	/**
	 * Verify correct designer is returned for valid code.
	 */
	@Test
	public void verifyCorrectDesignerIsReturnedForValidCode()
	{
		final DesignerModel actual = defaultDesignerDao.findDesignerByCode("USPOLOTEST");
		assertNotNull(actual);
		assertThat(actual.getCode(), is("USPOLOTEST"));
	}

	/**
	 * Verify null designer is returned for invalid code.
	 */
	@Test
	public void verifyDesignerListEmptyIsReturnedForInvalidCodes()
	{
		final List<String> codes = new ArrayList<>();
		codes.add("USPOLOTEST123");
		final List<DesignerModel> actual = defaultDesignerDao.findListOfDesignerForCodes(codes);
		assertEquals(actual.size(), 0);
	}

	/**
	 * Verify correct designer is returned for valid code.
	 */
	@Test
	public void verifyCorrectAllDesignerIsReturnedForValidCodes()
	{
		final List<String> codes = new ArrayList<>();
		codes.add("USPOLOTEST");
		final List<DesignerModel> actual = defaultDesignerDao.findListOfDesignerForCodes(codes);
		assertEquals(actual.size(), 1);
	}
}
