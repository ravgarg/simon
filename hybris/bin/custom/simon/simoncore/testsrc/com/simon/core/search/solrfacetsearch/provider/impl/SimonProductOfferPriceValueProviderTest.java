package com.simon.core.search.solrfacetsearch.provider.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.mirakl.hybris.beans.OfferData;
import com.mirakl.hybris.facades.product.OfferFacade;


/**
 * The Class SimonProductOfferPriceValueProviderTest. Unit test class for {@link SimonProductOfferPriceValueProvider}
 */
@UnitTest
public class SimonProductOfferPriceValueProviderTest
{

	/** The simon product offer price value provider. */
	@InjectMocks
	SimonProductOfferPriceValueProvider simonProductOfferPriceValueProvider;

	/** The index config. */
	@Mock
	IndexConfig indexConfig;

	/** The indexed property. */
	@Mock
	IndexedProperty indexedProperty;

	/** The field name provider. */
	@Mock
	FieldNameProvider fieldNameProvider;

	/** The offer facade. */
	@Mock
	OfferFacade offerFacade;

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		initMocks(this);
		when(fieldNameProvider.getFieldNames(indexedProperty, "USD")).thenReturn(Arrays.asList("USD"));
	}

	/**
	 * Verify exception thrown for non product models.
	 *
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Test(expected = FieldValueProviderException.class)
	public void verifyExceptionThrownForNonProductModels() throws FieldValueProviderException
	{
		simonProductOfferPriceValueProvider.getFieldValues(indexConfig, indexedProperty, mock(CartModel.class));
	}

	/**
	 * Verify if offer is present for given product correct price is returned as field values.
	 *
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Test
	public void verifyIfOfferIsPresentForGivenProductCorrectPriceIsReturnedAsFieldValues() throws FieldValueProviderException
	{
		final ProductModel productModel = mock(ProductModel.class);

		final PriceData priceData = mock(PriceData.class);
		final OfferData offerData = mock(OfferData.class);

		when(productModel.getCode()).thenReturn("12345");
		when(priceData.getValue()).thenReturn(BigDecimal.valueOf(100));
		when(offerFacade.getOffersForProductCode("12345")).thenReturn(Arrays.asList(offerData));
		when(offerData.getProductCode()).thenReturn("12345");
		when(offerData.getPrice()).thenReturn(priceData);

		final Collection<FieldValue> actualFieldValues = simonProductOfferPriceValueProvider.getFieldValues(indexConfig,
				indexedProperty, productModel);
		assertThat(actualFieldValues.size(), is(1));
		assertThat(actualFieldValues.iterator().next().getFieldName(), is("USD"));
		assertThat(actualFieldValues.iterator().next().getValue(), is(100.0));
	}

	/**
	 * Verify if offer is not present for given product zero price is returned as field values.
	 *
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Test
	public void verifyIfOfferIsNotPresentForGivenProductZeroPriceIsReturnedAsFieldValues() throws FieldValueProviderException
	{
		final ProductModel productModel = mock(ProductModel.class);

		when(productModel.getCode()).thenReturn("12345");
		when(offerFacade.getOffersForProductCode("12345")).thenReturn(Collections.emptyList());

		final Collection<FieldValue> actualFieldValues = simonProductOfferPriceValueProvider.getFieldValues(indexConfig,
				indexedProperty, productModel);
		assertThat(actualFieldValues.size(), is(1));
		assertThat(actualFieldValues.iterator().next().getFieldName(), is("USD"));
		assertThat(actualFieldValues.iterator().next().getValue(), is(0.0));
	}

}
