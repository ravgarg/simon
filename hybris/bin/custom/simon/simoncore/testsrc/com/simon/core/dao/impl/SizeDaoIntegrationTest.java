
package com.simon.core.dao.impl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.simon.core.model.ProductSizeSequenceModel;


/**
 * The Class SizeDaoImplTest. Integration test for {@link SizeDaoImpl}
 */
@IntegrationTest
public class SizeDaoIntegrationTest extends ServicelayerTransactionalTest
{

	@Resource
	private SizeDaoImpl sizeDao;


	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testSizeSequences.impex", "utf-8");
	}


	@Test
	public void testGetAllSequencesForSizes()
	{
		final List<ProductSizeSequenceModel> actual = sizeDao.getAllSequencesForSizes();
		Assert.assertTrue(CollectionUtils.isNotEmpty(actual));
	}
}
