package com.simon.core.dao.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonFlexiConstants;


/**
 * The Class ExtOrderDaoTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtOrderDaoTest
{

	private final String ORDER_ID = "123456789";

	@InjectMocks
	@Spy
	private ExtOrderDaoImpl extOrderDao;
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private SearchResult searchResult;
	@Mock
	private FlexibleSearchQuery query;
	@Mock
	private NullPointerException nullPointerException;

	@Mock
	private List<Object> results;
	@Mock
	private SearchResult<OrderModel> newOrderResult;
	@Mock
	private List<OrderModel> resultNewOrders;


	/**
	 *
	 */
	@Before
	public void setup()
	{
		doReturn(query).when(extOrderDao).getFlexibleSearchQuery(SimonFlexiConstants.FIND_ORDER_BY_ORDER_ID_QUERY);
	}


	/**
	 *
	 */
	@Test
	public void getCartByCartIdTest()
	{
		extOrderDao.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		extOrderDao.getOrderByOrderId(ORDER_ID);
	}

	/**
	 *
	 */
	@Test
	public void getCartByCartIdTest1()
	{
		extOrderDao.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		doReturn(new ArrayList<>()).when(searchResult).getResult();
		extOrderDao.getOrderByOrderId(ORDER_ID);
	}

	/**
	 *
	 */
	@Test
	public void getCartByCartIdTest2()
	{
		extOrderDao.setFlexibleSearchService(flexibleSearchService);
		doReturn(searchResult).when(flexibleSearchService).search(query);
		when(searchResult.getResult()).thenReturn(results);
		extOrderDao.getOrderByOrderId(ORDER_ID);
	}

	/**
	 *
	 */
	@Test
	public void getCartByCartIdTest3()
	{
		extOrderDao.setFlexibleSearchService(flexibleSearchService);
		extOrderDao.getOrderByOrderId("");
		Assert.assertNull(null);
	}

	@Test
	public void getFindOrdersByDate()
	{
		extOrderDao.setFlexibleSearchService(flexibleSearchService);
		doReturn(newOrderResult).when(flexibleSearchService).<OrderModel> search(Mockito.any(FlexibleSearchQuery.class));
		when(newOrderResult.getResult()).thenReturn(resultNewOrders);
		final int day = 1;
		final String startHours = "00";
		final String endHours = "23";
		extOrderDao.findOrdersToExport(day, startHours, endHours);
	}

}
