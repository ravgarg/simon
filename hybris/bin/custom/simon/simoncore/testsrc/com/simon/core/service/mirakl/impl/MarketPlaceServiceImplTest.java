package com.simon.core.service.mirakl.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.ordersplitting.model.ConsignmentModel;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.mirakl.client.core.exception.MiraklRequestValidationException;
import com.mirakl.client.mmp.front.core.MiraklMarketplacePlatformFrontApi;
import com.mirakl.client.mmp.front.request.order.accept.MiraklAcceptOrderRequest;
import com.mirakl.hybris.core.model.MarketplaceConsignmentModel;
import com.simon.core.api.SimonMiraklMarketplacePlatformFrontOperatorApi;
import com.simon.core.api.mirakl.dto.SimonMiraklShipOrderRequest;


/**
 * Unit test case for {@link MarketPlaceServiceImplTest}
 */
@UnitTest
public class MarketPlaceServiceImplTest
{
	@InjectMocks
	private MarketPlaceServiceImpl marketPlacenMiraklService;

	@Mock
	private SimonMiraklMarketplacePlatformFrontOperatorApi frontOperatorApi;

	@Mock
	private MiraklMarketplacePlatformFrontApi miraklApi;

	@Mock
	private AbstractOrderModel abstractOrderModel;

	@Mock
	private Set<ConsignmentModel> consignmentModels;

	@Mock
	private ConsignmentModel consignmentModel;

	@Mock
	private ConsignmentEntryModel consignmentEntryModel;

	@Mock
	private Set<ConsignmentEntryModel> consignmentEntryModels;

	@Mock
	private MarketplaceConsignmentModel consignment;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action
	 */
	@Test
	public void testAcceptOrRejectOrder()
	{
		final boolean accepted = true;
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentEntryModels = new HashSet<ConsignmentEntryModel>();
		when(consignmentEntryModel.getMiraklOrderLineId()).thenReturn("1234");
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(consignmentModel.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		consignmentModels.add(consignmentModel);
		consignmentEntryModels.add(consignmentEntryModel);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(frontOperatorApi, times(1)).acceptOrRejectOrder(Mockito.any());

	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action in case order doesn't have any
	 * consignments.
	 */
	@Test
	public void testAcceptOrRejectOrderWithoutConsignments()
	{
		final boolean accepted = true;
		consignmentModels = new HashSet<ConsignmentModel>();
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(frontOperatorApi, times(0)).acceptOrRejectOrder(Mockito.any());
	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action in case order doesn't have any
	 * consignment entry.
	 */
	@Test
	public void testAcceptOrRejectOrderWithoutConsignmentEntries()
	{
		final boolean accepted = true;
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentEntryModels = new HashSet<ConsignmentEntryModel>();
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(consignmentModel.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		consignmentModels.add(consignmentModel);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(consignmentEntryModel, times(0)).getMiraklOrderLineId();
		verify(frontOperatorApi, times(1)).acceptOrRejectOrder(Mockito.any());
	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action
	 */
	@Test
	public void testAcceptOrRejectOrderWithoutMiraklOrderLine()
	{
		final boolean accepted = true;
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentEntryModels = new HashSet<ConsignmentEntryModel>();
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(consignmentModel.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		consignmentModels.add(consignmentModel);
		consignmentEntryModels.add(consignmentEntryModel);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(frontOperatorApi, times(1)).acceptOrRejectOrder(Mockito.any());

	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action consignment without code.
	 */
	@Test(expected = MiraklRequestValidationException.class)
	public void testAcceptOrRejectOrderWithoutConsignmentCode()
	{
		final boolean accepted = false;
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentEntryModels = new HashSet<ConsignmentEntryModel>();
		when(consignmentModel.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		consignmentModels.add(consignmentModel);
		consignmentEntryModels.add(consignmentEntryModel);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(frontOperatorApi, times(0)).acceptOrRejectOrder(Mockito.any());

	}

	/**
	 * Method will test if order doesn't have any consignments
	 */
	@Test(expected = NullPointerException.class)
	public void testAcceptOrRejectOrderWithException()
	{
		final MiraklAcceptOrderRequest request = mock(MiraklAcceptOrderRequest.class);
		final boolean accepted = true;
		when(abstractOrderModel.getConsignments()).thenReturn(null);
		marketPlacenMiraklService.acceptOrRejectOrder(abstractOrderModel, accepted);
		verify(frontOperatorApi, times(0)).acceptOrRejectOrder(request);
	}

	/**
	 * Method will test carrier tracking details within Mirakl
	 */
	@Test
	public void testTrackingOrder()
	{
		consignmentModels = new HashSet<ConsignmentModel>();
		when(consignmentModel.getCarrier()).thenReturn("abc");
		when(consignmentModel.getTrackingID()).thenReturn("12345");
		consignmentModels.add(consignmentModel);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.trackingOrder(abstractOrderModel);
		verify(frontOperatorApi, times(1)).trackOrder(Mockito.any());
	}

	/**
	 * Method will test carrier tracking details within Mirakl when abstractOrderModel is null
	 */
	@Test(expected = NullPointerException.class)
	public void testTrackingOrderWithException()
	{
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.trackingOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).trackOrder(Mockito.any());
	}

	/**
	 * Method will test carrier tracking details within Mirakl
	 */
	@Test
	public void testTrackingOrderWithoutConsignment()
	{
		consignmentModels = new HashSet<ConsignmentModel>();
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.trackingOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).trackOrder(Mockito.any());
	}



	/**
	 * Method will test payment of order in Mirakl.
	 */
	@Test
	public void testProcessPayment()
	{
		final CustomerModel customerModel = mock(CustomerModel.class);
		final CurrencyModel currencyModel = mock(CurrencyModel.class);
		final MarketplaceConsignmentModel marketplaceConsignmentModel = mock(MarketplaceConsignmentModel.class);
		when(marketplaceConsignmentModel.getTotalPrice()).thenReturn(new Double(12.98));
		when(currencyModel.getIsocode()).thenReturn("EUR");
		when(abstractOrderModel.getCurrency()).thenReturn(currencyModel);
		when(customerModel.getContactEmail()).thenReturn("abc@gmail.com");
		when(abstractOrderModel.getUser()).thenReturn(customerModel);
		when(marketplaceConsignmentModel.getOrder()).thenReturn(abstractOrderModel);
		marketPlacenMiraklService.processPayment(marketplaceConsignmentModel);
		verify(miraklApi, times(1)).confirmOrderDebit(Mockito.any());
	}

	/**
	 * Method will test payment of order in Mirakl when ConsignmentModel is not instance of MarketplaceConsignmentModel
	 */
	@Test(expected = ClassCastException.class)
	public void testProcessPaymentWithConsignmentModel()
	{
		final ConsignmentModel consignmentModel = mock(ConsignmentModel.class);
		marketPlacenMiraklService.processPayment(consignmentModel);
		verify(miraklApi, times(0)).confirmOrderDebit(Mockito.any());
	}


	/**
	 * Method will test payment of order in Mirakl.
	 */
	@Test(expected = NullPointerException.class)
	public void testProcessPaymentWithException()
	{
		final MarketplaceConsignmentModel marketplaceConsignmentModel = mock(MarketplaceConsignmentModel.class);
		marketPlacenMiraklService.processPayment(marketplaceConsignmentModel);
		verify(miraklApi, times(0)).confirmOrderDebit(Mockito.any());
	}

	/**
	 * Method will test status of shipping within Mirakl
	 */
	@Test
	public void testShipOrder()
	{
		final SimonMiraklShipOrderRequest simonMiraklShipOrderRequest = mock(SimonMiraklShipOrderRequest.class);
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentModels.add(consignmentModel);
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.shipOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).shipOrder(simonMiraklShipOrderRequest);
	}

	/**
	 * Method will test status of shipping within Mirakl in case abstractOrderModel is null
	 */
	@Test(expected = NullPointerException.class)
	public void testShipOrderWithException()
	{
		final SimonMiraklShipOrderRequest simonMiraklShipOrderRequest = mock(SimonMiraklShipOrderRequest.class);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.shipOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).shipOrder(simonMiraklShipOrderRequest);
	}

	/**
	 * Method will test status of shipping within Mirakl without consignment model
	 */
	@Test
	public void testShipOrderWithoutConsignment()
	{
		final SimonMiraklShipOrderRequest simonMiraklShipOrderRequest = mock(SimonMiraklShipOrderRequest.class);
		consignmentModels = new HashSet<ConsignmentModel>();
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.shipOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).shipOrder(simonMiraklShipOrderRequest);
	}

	/**
	 * Method will test status of shipping within Mirakl without consignment model
	 */
	@Test(expected = MiraklRequestValidationException.class)
	public void testShipOrderWithoutConsignmentCode()
	{
		final SimonMiraklShipOrderRequest simonMiraklShipOrderRequest = mock(SimonMiraklShipOrderRequest.class);
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentModels.add(consignmentModel);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		marketPlacenMiraklService.shipOrder(abstractOrderModel);
		verify(frontOperatorApi, times(0)).shipOrder(simonMiraklShipOrderRequest);
	}

	/**
	 * Method will test status of order acceptance/Rejection depends on Retailer action
	 */
	@Test
	public void testAcceptOrRejectOrderOverride()
	{
		final boolean accepted = true;
		consignmentModels = new HashSet<ConsignmentModel>();
		consignmentEntryModels = new HashSet<ConsignmentEntryModel>();
		when(consignmentEntryModel.getMiraklOrderLineId()).thenReturn("1234");
		when(consignment.getCode()).thenReturn("1234");
		when(consignment.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(consignmentModel.getCode()).thenReturn("M0001");
		when(consignmentModel.getConsignmentEntries()).thenReturn(consignmentEntryModels);
		when(abstractOrderModel.getConsignments()).thenReturn(consignmentModels);
		consignmentModels.add(consignmentModel);
		consignmentEntryModels.add(consignmentEntryModel);
		doNothing().when(frontOperatorApi).acceptOrRejectOrder(Mockito.any());
		marketPlacenMiraklService.acceptOrRejectOrder(consignment, accepted);
		verify(frontOperatorApi, times(1)).acceptOrRejectOrder(Mockito.any());
	}

	/**
	 * This test case for OR25 API,It make OOB, Mirakl Order is Received.
	 */
	@Test
	public void testOrderReceived()
	{
		final String miraklOrder = "Test_A";
		doNothing().when(frontOperatorApi).receiveOrder(Mockito.any());
		marketPlacenMiraklService.receivedMarketplaceOrder(miraklOrder);
		verify(frontOperatorApi, times(1)).receiveOrder(Mockito.any());
	}


}
