package com.simon.core.services.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.dao.DesignerDao;
import com.simon.core.model.DesignerModel;


/**
 * The Class DefaultDesignerServiceTest. Unit test for {@link DefaultDesignerService}}
 */
@UnitTest
public class DefaultDesignerServiceTest
{

	/** The default designer service. */
	@InjectMocks
	private DefaultDesignerService defaultDesignerService;

	/** The designer dao. */
	@Mock
	private DesignerDao designerDao;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp()
	{
		initMocks(this);
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionIsThrownForNullCode()
	{
		defaultDesignerService.getDesignerForCode(null);
	}

	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyDesingeReturnedFromDaoIsReturnedCorrectly()
	{
		final DesignerModel designerModel = mock(DesignerModel.class);
		when(designerDao.findDesignerByCode("USPOLO")).thenReturn(designerModel);
		assertThat(defaultDesignerService.getDesignerForCode("USPOLO"), is(designerModel));
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionIsThrownForNullCodes()
	{
		defaultDesignerService.getListOfDesignerForCodes(null);
	}

	/**
	 * Verify code is passed to dao correctly for search.
	 */
	@Test
	public void verifyDesingeListReturnedFromDaoIsReturnedCorrectly()
	{
		final List<DesignerModel> designerList = new ArrayList<DesignerModel>();
		final DesignerModel designerModel = mock(DesignerModel.class);
		designerList.add(designerModel);
		final List<String> codes = new ArrayList<>();
		codes.add("USPOLO");
		when(designerDao.findListOfDesignerForCodes(codes)).thenReturn(designerList);
		assertEquals(designerList, defaultDesignerService.getListOfDesignerForCodes(codes));
	}
}
