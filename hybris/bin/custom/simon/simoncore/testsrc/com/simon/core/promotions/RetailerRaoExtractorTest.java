package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.ruleengineservices.rao.CartRAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class RetailerRaoExtractorTest
{
	@InjectMocks
	RetailerRaoExtractor retailerRaoExtractor;
	@Mock
	CartRAO cart;
	@Mock
	RetailerRAO retailer1;
	@Mock
	RetailerRAO retailer2;

	@Before
	public void setup()
	{
		initMocks(this);
		final List<RetailerRAO> retailers = new ArrayList();
		retailers.add(retailer1);
		retailers.add(retailer2);
		when(cart.getRetailers()).thenReturn(retailers);
	}



	@Test
	public void expandFact()
	{
		final Set<?> facts = retailerRaoExtractor.expandFact(cart);
		assertEquals(2, facts.size());
	}

	@Test
	public void getTriggeringOption()
	{
		assertEquals("EXPAND_RETAILER_RAO", retailerRaoExtractor.getTriggeringOption());
	}

	@Test
	public void isDefault()
	{
		assertTrue(retailerRaoExtractor.isDefault());
	}

	@Test
	public void isMinOption()
	{
		assertFalse(retailerRaoExtractor.isMinOption());
	}

}
