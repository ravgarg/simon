package com.simon.core.dao.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonFlexiConstants;


/**
 * The Class ErrorLogDaoImplTest.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ErrorLogDaoImplTest
{
	private static final String ERRORMESSAGE = "errorMessage";
	private static final String JOBLASTMODIFIEDTIME = "jobLastModifiedTime";

	@InjectMocks
	private ErrorLogDaoImpl errorLogDao;
	@Mock
	private FlexibleSearchService flexibleSearchService;
	@Mock
	private SearchResult<Object> searchResult;
	/*
	 * @Mock private FlexibleSearchQuery fQuery;
	 */

	@Before
	public void setup()
	{
		initMocks(this);
	}

	/**
	 * Verify exception is thrown for null code.
	 */
	@Test
	public void verifyGetProductCodes()
	{
		final Date lastEndTime = new Date();
		final List<Object> productCode = new ArrayList<>();
		productCode.add("TestProduct");

		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(SimonFlexiConstants.FIND_PRODUCT_CODES_ERRORLOG);
		fQuery.addQueryParameter(ERRORMESSAGE, SimonCoreConstants.UNAPPROVED);
		fQuery.addQueryParameter(JOBLASTMODIFIEDTIME, lastEndTime);
		fQuery.setResultClassList(Arrays.asList(String.class));

		doReturn(searchResult).when(flexibleSearchService).search(fQuery);
		Mockito.when(searchResult.getResult()).thenReturn(productCode);
		errorLogDao.getProductCodes(lastEndTime);
	}
}
