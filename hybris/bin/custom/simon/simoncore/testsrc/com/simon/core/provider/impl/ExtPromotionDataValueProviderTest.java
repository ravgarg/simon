package com.simon.core.provider.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.promotions.PromotionsService;
import de.hybris.platform.promotions.model.AbstractPromotionModel;
import de.hybris.platform.promotions.model.PromotionGroupModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants.IndexedKeys;


/**
 * Test case for {@link ExtPromotionDataValueProvider}
 */
@RunWith(MockitoJUnitRunner.class)
public class ExtPromotionDataValueProviderTest
{
	private static final String SAMPLE_PROMOTION_TYPE = "Rule Based Promotion";

	private static final Integer SAMPLE_PROMOTION_PRIORITY = Integer.valueOf(100);

	private static final String SAMPLE_PROMO_DESCRIPTION = "promoDescription";

	private static final String SAMPLE_PROMO_CODE = "promoCode";

	private static final Date SAMPLE_PROMO_END_DATE = new Date();

	@Mock
	IndexConfig indexConfig;

	@Mock
	IndexedProperty indexedProperty;

	@Mock
	private AbstractPromotionModel promotion;

	@Mock
	private FieldNameProvider fieldNameProvider;

	@Mock
	private PromotionsService promotionsService;

	@InjectMocks
	private ExtPromotionDataValueProvider extPromotionDataValueProvider;

	private ProductModel product;
	private BaseSiteModel baseSite;
	private PromotionGroupModel promotionGroup;

	@Before
	public void setUp() throws Exception
	{
		product = new ProductModel();
		baseSite = new BaseSiteModel();
		promotionGroup = new PromotionGroupModel();
		baseSite.setDefaultPromotionGroup(promotionGroup);
		final List<? extends AbstractPromotionModel> promotionsL = new ArrayList<AbstractPromotionModel>();
		when(indexConfig.getBaseSite()).thenReturn(baseSite);
		doReturn(Collections.singletonList(promotion)).when(promotionsService).getAbstractProductPromotions(Mockito.anyList(),
				Mockito.any(ProductModel.class), Mockito.anyBoolean(), Mockito.any(Date.class));
		when(promotion.getPromotionType()).thenReturn(SAMPLE_PROMOTION_TYPE);
		when(promotion.getCode()).thenReturn(SAMPLE_PROMO_CODE);
		when(promotion.getDescription()).thenReturn(SAMPLE_PROMO_DESCRIPTION);
		when(promotion.getEndDate()).thenReturn(SAMPLE_PROMO_END_DATE);
		when(promotion.getPriority()).thenReturn(SAMPLE_PROMOTION_PRIORITY);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoCode() throws Exception
	{
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_CODE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList(SAMPLE_PROMO_CODE));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoEndDate() throws Exception
	{
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_END_DATE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(String.valueOf(SAMPLE_PROMO_END_DATE)));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoType() throws Exception
	{
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_TYPE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList(SAMPLE_PROMOTION_TYPE));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoDescription() throws Exception
	{
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_DESCRIPTION);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(SAMPLE_PROMO_DESCRIPTION));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoPriority() throws Exception
	{
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_PRIORITY);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(String.valueOf(SAMPLE_PROMOTION_PRIORITY)));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoCodeForMultiValues() throws Exception
	{
		when(indexedProperty.isMultiValue()).thenReturn(Boolean.TRUE);
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_CODE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList(SAMPLE_PROMO_CODE));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoEndDateForMultiValues() throws Exception
	{
		when(indexedProperty.isMultiValue()).thenReturn(Boolean.TRUE);
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_END_DATE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(String.valueOf(SAMPLE_PROMO_END_DATE)));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoTypeForMultiValues() throws Exception
	{
		when(indexedProperty.isMultiValue()).thenReturn(Boolean.TRUE);
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_TYPE);
		when(fieldNameProvider.getFieldNames(indexedProperty, null)).thenReturn(Collections.singletonList(SAMPLE_PROMOTION_TYPE));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoDescriptionForMultiValues() throws Exception
	{
		when(indexedProperty.isMultiValue()).thenReturn(Boolean.TRUE);
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_DESCRIPTION);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(SAMPLE_PROMO_DESCRIPTION));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}

	/**
	 * Test method for
	 * {@link com.simon.core.provider.impl.ExtPromotionDataValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)}.
	 */
	@Test
	public void testGetFieldValuesForPromoPriorityForMultiValues() throws Exception
	{
		when(indexedProperty.isMultiValue()).thenReturn(Boolean.TRUE);
		when(indexedProperty.getName()).thenReturn(IndexedKeys.PROMOTION_PRIORITY);
		when(fieldNameProvider.getFieldNames(indexedProperty, null))
				.thenReturn(Collections.singletonList(String.valueOf(SAMPLE_PROMOTION_PRIORITY)));
		extPromotionDataValueProvider.getFieldValues(indexConfig, indexedProperty, product);
	}
}
