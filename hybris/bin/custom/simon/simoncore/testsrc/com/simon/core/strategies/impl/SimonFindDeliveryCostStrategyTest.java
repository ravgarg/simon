package com.simon.core.strategies.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.delivery.DeliveryModeModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.util.PriceValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.model.AdditionalCartEstimateModel;
import com.simon.core.model.AdditionalCartInfoModel;
import com.simon.core.model.AdditionalPriceModel;
import com.simon.core.model.RetailersInfoModel;
import com.simon.core.model.ShippingDetailsModel;


/*
 * This class tests the methods of class {@link SimonFindDeliveryCostStrategy}.
 */
@UnitTest
public class SimonFindDeliveryCostStrategyTest
{

	private static final double TEN = 10.0d;
	@InjectMocks
	@Spy
	private SimonFindDeliveryCostStrategy simonFindDeliveryCostStrategy;
	@Mock
	private AbstractOrderEntryModel entry;
	@Mock
	private AbstractOrderModel order;
	@Mock
	private AdditionalCartInfoModel cartModel;
	@Mock
	private AdditionalCartEstimateModel estimate;
	@Mock
	private AdditionalPriceModel estimatePrices;
	@Mock
	private CurrencyModel currency;
	private PriceValue priceValue;
	@Mock
	private AddressModel address;
	@Mock
	private DeliveryModeModel delModeForRetA;
	@Mock
	private DeliveryModeModel delModeForRetB;
	@Mock
	private DeliveryModeModel delModeForRetC;
	@Mock
	private AdditionalPriceModel prices;
	@Mock
	private RetailersInfoModel retailerInfoModelA;
	@Mock
	private RetailersInfoModel retailerInfoModelB;
	@Mock
	private RetailersInfoModel retailerInfoModelC;
	@Mock
	private ShippingDetailsModel shippingCheapestForA;
	@Mock
	private ShippingDetailsModel shippingCheapestForB;
	@Mock
	private ShippingDetailsModel shippingCheapestForC;

	private final String shippingPrice = "$20.0";
	private final String RETA = "RetailerA";
	private final Double RETA_SUBTOTAL = 25d;
	private final String RETB = "RetailerB";
	private final Double RETB_SUBTOTAL = 45d;
	private final String RETC = "RetailerC";
	private final Double RETC_SUBTOTAL = 30d;
	private final String CHEAPEST = "cheapest";


	/*
	 * Setup method.
	 *
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		priceValue = new PriceValue("USD", 0, true);
		final Map<String, Double> retailerSubtotalMap = new HashMap<>();
		retailerSubtotalMap.put(RETA, RETA_SUBTOTAL);
		retailerSubtotalMap.put(RETB, RETB_SUBTOTAL);
		retailerSubtotalMap.put(RETC, RETC_SUBTOTAL);
		when(order.getRetailersSubtotal()).thenReturn(retailerSubtotalMap);
		final Map<String, DeliveryModeModel> retailerDeliveryModeMap = new HashMap<>();
		retailerDeliveryModeMap.put(RETA, delModeForRetA);
		retailerDeliveryModeMap.put(RETB, delModeForRetB);
		retailerDeliveryModeMap.put(RETC, delModeForRetC);
		when(order.getRetailersDeliveryModes()).thenReturn(retailerDeliveryModeMap);
		when(order.getRetailersDeliveryCost()).thenReturn(new HashMap<>());

	}

	@Test
	public void testGetDeliveryCostWithEmptyOrder()
	{
		when(order.getSubtotal()).thenReturn(null);
		when(order.getRetailersSubtotal()).thenReturn(null);
		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}

	@Test
	public void testGetDeliveryCostWithNegativeSubTotal()
	{
		when(order.getSubtotal()).thenReturn(-10.0d);
		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}

	@Test
	public void testGetDeliveryCostWithPositiveSubTotal()
	{
		when(order.getSubtotal()).thenReturn(10.0d);
		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}

	@Test
	public void testGetDeliveryCost_WhenAdditinalCartInfoIsPresent()
	{
		when(order.getSubtotal()).thenReturn(10.0d);
		when(order.getAdditionalCartInfo()).thenReturn(cartModel);
		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}

	@Test
	public void testGetDeliveryCost_WhenRetailerInfoIsPresent()
	{
		when(order.getSubtotal()).thenReturn(10.0d);
		when(order.getAdditionalCartInfo()).thenReturn(cartModel);
		final List<RetailersInfoModel> retailerInfo = new ArrayList<>();
		retailerInfo.add(retailerInfoModelA);
		retailerInfo.add(retailerInfoModelB);
		retailerInfo.add(retailerInfoModelC);
		when(retailerInfoModelA.getRetailerId()).thenReturn(RETA);
		when(retailerInfoModelB.getRetailerId()).thenReturn(RETB);
		when(retailerInfoModelC.getRetailerId()).thenReturn(RETC);
		when(cartModel.getRetailersInfo()).thenReturn(retailerInfo);

		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}

	@Test
	public void testGetDeliveryCost_WhenShippingIsPresent()
	{
		when(order.getSubtotal()).thenReturn(10.0d);
		when(order.getAdditionalCartInfo()).thenReturn(cartModel);
		final List<RetailersInfoModel> retailerInfo = new ArrayList<>();
		retailerInfo.add(retailerInfoModelA);
		retailerInfo.add(retailerInfoModelB);
		retailerInfo.add(retailerInfoModelC);
		when(retailerInfoModelA.getRetailerId()).thenReturn(RETA);
		when(retailerInfoModelB.getRetailerId()).thenReturn(RETB);
		when(retailerInfoModelC.getRetailerId()).thenReturn(RETC);
		final List<ShippingDetailsModel> shippingA = new ArrayList<>();
		shippingA.add(shippingCheapestForA);
		when(shippingCheapestForA.getShippingEstimate()).thenReturn(Double.valueOf(1));
		when(shippingCheapestForA.getCode()).thenReturn(CHEAPEST);
		when(delModeForRetA.getCode()).thenReturn(CHEAPEST);
		when(delModeForRetB.getCode()).thenReturn(CHEAPEST);
		when(delModeForRetB.getCode()).thenReturn(CHEAPEST);
		when(retailerInfoModelA.getShippingDetails()).thenReturn(shippingA);
		final List<ShippingDetailsModel> shippingB = new ArrayList<>();
		shippingA.add(shippingCheapestForB);
		when(shippingCheapestForB.getShippingEstimate()).thenReturn(Double.valueOf(2));
		when(shippingCheapestForB.getCode()).thenReturn(CHEAPEST);
		when(retailerInfoModelB.getShippingDetails()).thenReturn(shippingB);

		when(retailerInfoModelA.getShippingDetails()).thenReturn(shippingA);
		final List<ShippingDetailsModel> shippingC = new ArrayList<>();
		shippingA.add(shippingCheapestForC);
		when(shippingCheapestForC.getShippingEstimate()).thenReturn(Double.valueOf(3));
		when(shippingCheapestForC.getCode()).thenReturn(CHEAPEST);
		when(retailerInfoModelB.getShippingDetails()).thenReturn(shippingB);
		when(cartModel.getRetailersInfo()).thenReturn(retailerInfo);

		doReturn(priceValue).when(simonFindDeliveryCostStrategy).getDeliveryCostFromSuper(order);
		assertEquals(priceValue, simonFindDeliveryCostStrategy.getDeliveryCost(order));
	}



}
