package com.simon.core.catalog.strategies.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.catalog.model.KeywordModel;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.beans.ProductImportGlobalContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;
import com.simon.core.enums.KeywordType;
import com.simon.core.services.ExtKeywordService;


/**
 * The Class DelimitedKeywordHandlerTest. Unit test for {@link DelimitedKeywordHandler}
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DelimitedKeywordHandlerTest
{

	@InjectMocks
	private DelimitedKeywordHandler delimitedTextAttributeHandler;

	@Mock
	private CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	@Mock
	private ModelService modelService;

	@Mock
	private ExtKeywordService keywordService;

	@Mock
	private CommonI18NService commonI18NService;

	/**
	 * Verify no interaction for null raw text.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoInteractionForNullRawText() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getTypeParameter()).thenReturn("|");

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		delimitedTextAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify no interaction for null delimiter.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyNoInteractionForNullDelimiter() throws ProductImportException
	{
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("text1|text2|text3");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		delimitedTextAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify existing keyword returned for SEO keywords and set on product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyExistingKeywordsReturnedForSEOKeywordsAndSetOnProduct() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final CatalogVersionModel catalogVersion = mockProductCatalogVersion(context);

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getTypeParameter()).thenReturn("|");
		when(coreAttribute.getCode()).thenReturn("seoKeywords");

		final AttributeValueData attribute = mock(AttributeValueData.class);
		final String rawText = "text1|text2|text3";
		when(attribute.getValue()).thenReturn(rawText);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final KeywordModel mockKeyword1 = mock(KeywordModel.class);
		final KeywordModel mockKeyword2 = mock(KeywordModel.class);
		final KeywordModel mockKeyword3 = mock(KeywordModel.class);
		when(keywordService.getKeyword(catalogVersion, "text1", KeywordType.SEO)).thenReturn(mockKeyword1);
		when(keywordService.getKeyword(catalogVersion, "text2", KeywordType.SEO)).thenReturn(mockKeyword2);
		when(keywordService.getKeyword(catalogVersion, "text3", KeywordType.SEO)).thenReturn(mockKeyword3);

		delimitedTextAttributeHandler.setValue(attribute, data, context);
		verify(product).setKeywords(Arrays.asList(mockKeyword1, mockKeyword2, mockKeyword3));
	}

	/**
	 * Verify existing keyword returned for marketing keywords and set on product.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyExistingKeywordsReturnedForMarketingKeywordsAndSetOnProduct() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		final CatalogVersionModel catalogVersion = mockProductCatalogVersion(context);

		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getTypeParameter()).thenReturn("|");
		when(coreAttribute.getCode()).thenReturn("marketingKeywords");

		final AttributeValueData attribute = mock(AttributeValueData.class);
		final String rawText = "text1|text2|text3";
		when(attribute.getValue()).thenReturn(rawText);
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final KeywordModel mockKeyword1 = mock(KeywordModel.class);
		final KeywordModel mockKeyword2 = mock(KeywordModel.class);
		final KeywordModel mockKeyword3 = mock(KeywordModel.class);
		when(keywordService.getKeyword(catalogVersion, "text1", KeywordType.MARKETING)).thenReturn(mockKeyword1);
		when(keywordService.getKeyword(catalogVersion, "text2", KeywordType.MARKETING)).thenReturn(mockKeyword2);
		when(keywordService.getKeyword(catalogVersion, "text3", KeywordType.MARKETING)).thenReturn(mockKeyword3);

		delimitedTextAttributeHandler.setValue(attribute, data, context);
		verify(product).setKeywords(Arrays.asList(mockKeyword1, mockKeyword2, mockKeyword3));
	}

	/**
	 * Mock product catalog version.
	 *
	 * @param context
	 *           the context
	 * @return the catalog version model
	 */
	private CatalogVersionModel mockProductCatalogVersion(final ProductImportFileContextData context)
	{
		final CatalogVersionModel productCatalogVersion = mock(CatalogVersionModel.class);

		final ProductImportGlobalContextData globalContext = mock(ProductImportGlobalContextData.class);
		final PK catalogVersionPK = PK.fromLong(1L);

		when(context.getGlobalContext()).thenReturn(globalContext);
		when(globalContext.getProductCatalogVersion()).thenReturn(catalogVersionPK);
		when(modelService.get(catalogVersionPK)).thenReturn(productCatalogVersion);

		return productCatalogVersion;
	}

	/**
	 * KeywordListMatcher custom matcher to match keywords list
	 */
	class KeywordListMatcher extends ArgumentMatcher<List<KeywordModel>>
	{

		String rawKeyword;
		CatalogVersionModel catalogVersion;
		KeywordType keywordType;

		/**
		 * Instantiates a new keyword list matcher.
		 *
		 * @param rawKeyword
		 *           the raw keyword
		 * @param catalogVersion
		 *           the catalog version
		 * @param keywordType
		 *           the keyword type
		 */
		public KeywordListMatcher(final String rawKeyword, final CatalogVersionModel catalogVersion,
				final com.simon.core.enums.KeywordType keywordType)
		{
			this.rawKeyword = rawKeyword;
			this.catalogVersion = catalogVersion;
			this.keywordType = keywordType;
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see org.mockito.ArgumentMatcher#matches(java.lang.Object)
		 */
		@Override
		public boolean matches(final Object argument)
		{
			@SuppressWarnings("unchecked")
			final List<KeywordModel> actualKeywords = (List<KeywordModel>) argument;
			final String[] rawKeywords = StringUtils.split(rawKeyword, "|");
			for (final KeywordModel actualKeyword : actualKeywords)
			{
				verify(actualKeyword).setKeyword(rawKeywords[actualKeywords.indexOf(actualKeyword)]);
				verify(actualKeyword, times(3)).setCatalogVersion(catalogVersion);
				verify(actualKeyword, times(3)).setKeywordType(keywordType);
			}
			return true;
		}
	}
}
