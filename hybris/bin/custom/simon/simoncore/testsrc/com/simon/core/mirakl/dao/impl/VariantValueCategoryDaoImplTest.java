package com.simon.core.mirakl.dao.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.dao.impl.VariantValueCategoryDaoImpl;


/**
 * VariantValueCategoryDaoTest integration test for {@link VariantValueCategoryDaoImpl}.
 */
@IntegrationTest
public class VariantValueCategoryDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource
	private VariantValueCategoryDaoImpl variantValueCategoryDao;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private CategoryService categoryService;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testCategories.impex", "utf-8");
		importCsv("/simoncore/test/testProducts.impex", "utf-8");
		importCsv("/simoncore/test/testPrices.impex", "utf-8");
		importCsv("/simoncore/test/testStockLevels.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify top sequence category is returned if found.
	 */
	@Test
	public void verifyTopSequenceCategoryIsReturnedIfFound()
	{
		final CategoryModel colorVariantCategory = categoryService
				.getCategoryForCode(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED), "color");

		final VariantValueCategoryModel topSequenceVariantValueCategory = variantValueCategoryDao
				.findTopSequenceVariantValueCategory((VariantCategoryModel) colorVariantCategory);

		assertThat(topSequenceVariantValueCategory.getCode(), is("assorted"));
	}

}
