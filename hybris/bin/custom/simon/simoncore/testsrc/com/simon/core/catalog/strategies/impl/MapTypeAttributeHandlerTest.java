package com.simon.core.catalog.strategies.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.beans.AttributeValueData;
import com.mirakl.hybris.beans.ProductImportData;
import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.catalog.strategies.CoreAttributeOwnerStrategy;
import com.mirakl.hybris.core.model.MiraklCoreAttributeModel;
import com.mirakl.hybris.core.product.exceptions.ProductImportException;


/**
 * MapTypeAttributeHandlerTest unit test for {@link MapTypeAttributeHandler}.
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class MapTypeAttributeHandlerTest
{

	@InjectMocks
	private MapTypeAttributeHandler mapTypeAttributeHandler;

	@Mock
	private ModelService modelService;

	@Mock
	private CoreAttributeOwnerStrategy coreAttributeOwnerStrategy;

	/**
	 * Verify empty texts are ignored.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyEmptyTextsAreIgnored() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getCode()).thenReturn("additionalAttributes");
		when(coreAttribute.getTypeParameter()).thenReturn("|:");

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		mapTypeAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}


	/**
	 * Verify if delimter is empty invocation is ignored.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyIfDelimterIsEmptyInvocationIsIgnored() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getCode()).thenReturn("additionalAttributes");

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("size-type:Plus|top-type:T-Shirts");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		mapTypeAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify if delimter is improper invocation is ignored.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyIfDelimterIsImproperInvocationIsIgnored() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getCode()).thenReturn("additionalAttributes");
		when(coreAttribute.getTypeParameter()).thenReturn("|");

		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("size-type:Plus|top-type:T-Shirts");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		mapTypeAttributeHandler.setValue(attribute, data, context);
		verifyZeroInteractions(modelService);
	}

	/**
	 * Verify valid text given valid delimiters are converted into map and stored.
	 *
	 * @throws ProductImportException
	 *            the product import exception
	 */
	@Test
	public void verifyValidTextAreConvertedIntoMapAndStored() throws ProductImportException
	{
		final ProductModel product = mock(ProductModel.class);
		final MiraklCoreAttributeModel coreAttribute = mock(MiraklCoreAttributeModel.class);
		when(coreAttribute.getCode()).thenReturn("additionalAttributes");
		when(coreAttribute.getTypeParameter()).thenReturn("|:");
		final ProductImportData data = mock(ProductImportData.class);
		final ProductImportFileContextData context = mock(ProductImportFileContextData.class);
		when(coreAttributeOwnerStrategy.determineOwner(coreAttribute, data, context)).thenReturn(product);

		final AttributeValueData attribute = mock(AttributeValueData.class);
		when(attribute.getValue()).thenReturn("size-type:Plus|top-type:T-Shirts");
		when(attribute.getCoreAttribute()).thenReturn(coreAttribute);

		final Map<String, String> expectedMap = new HashMap<>();
		expectedMap.put("size-type", "Plus");
		expectedMap.put("top-type", "T-Shirts");

		mapTypeAttributeHandler.setValue(attribute, data, context);

		verify(modelService).setAttributeValue(product, "additionalAttributes", expectedMap);
	}
}
