package com.simon.core.provider.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.DealModel;
import com.simon.core.services.ExtDealService;


@UnitTest
public class DealDirectProductResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	private DealDirectProductResolver dealDirectProductResolver;
	@Mock
	private ModelService modelService;
	@Mock
	private ExtDealService extDealService;
	@Mock
	private TypeService typeService;
	@Mock
	private JaloSession jalo;
	@Mock
	private IndexedProperty indexedProperty;
	@Mock
	private GenericVariantProductModel product;
	@Mock
	private DealModel dealModel;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

	}

	/**
	 * This method to test resolve method with valid data.
	 * 
	 * @throws FieldValueProviderException
	 */
	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		Mockito.when(getSessionService().getRawSession(Matchers.any(Session.class))).thenReturn(jalo);
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final List<DealModel> listOfDeal = new ArrayList<>();
		final Set<GenericVariantProductModel> listOfProduct = new HashSet<>();
		listOfDeal.add(dealModel);
		listOfProduct.add(product);
		when(extDealService.findListOfDeals()).thenReturn(listOfDeal);
		when(dealModel.getProducts()).thenReturn(listOfProduct);
		when(dealModel.getCode()).thenReturn("Deal1");
		when(product.getCode()).thenReturn("testproduct1");
		when(product.getProperty(INDEXED_PROPERTY_NAME)).thenReturn("productDealCode");
		dealDirectProductResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, product);
		verify(getInputDocument(), Mockito.times(1)).addField(any(IndexedProperty.class), any(), any(String.class));

	}

}
