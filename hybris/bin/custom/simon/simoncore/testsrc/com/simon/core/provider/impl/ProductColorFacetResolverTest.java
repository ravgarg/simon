package com.simon.core.provider.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.enumeration.EnumerationValueModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.type.TypeService;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.indexer.IndexerBatchContext;
import de.hybris.platform.solrfacetsearch.indexer.spi.InputDocument;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolver;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.enums.SwatchColorEnum;


@UnitTest
public class ProductColorFacetResolverTest extends AbstractValueResolverTest
{
	private ProductColorFacetResolver valueResolver;

	@Rule
	public ExpectedException expectedException = ExpectedException.none(); //NOPMD
	@Mock
	AbstractValueResolver<GenericVariantProductModel, MediaModel, Object> abstractValueResolver;
	@Mock
	private TypeService typeService;
	@Mock
	private InputDocument document;
	@Mock
	private IndexerBatchContext batchContext;
	@Mock
	private IndexedProperty indexedProperty;

	@Mock
	private Collection<IndexedProperty> indexedProperties;
	private static final String mediaModelUrl = "url1";
	private IndexedProperty indexedProperty1;
	private IndexedProperty indexedProperty2;

	private GenericVariantProductModel variantProductModel;

	@Mock
	private MediaModel mediaModel;

	private final Set<SwatchColorEnum> colorSwatches = new HashSet<>();

	@Before
	public void setUp()
	{
		indexedProperty1 = new IndexedProperty();
		indexedProperty1.setName((String) SimonCoreConstants.NORMALIZED_COLOR_CODE);
		indexedProperty1.setValueProviderParameters(new HashMap<String, String>());

		indexedProperty2 = new IndexedProperty();
		indexedProperty2.setName((String) SimonCoreConstants.NORMALIZED_COLOR_URL);
		indexedProperty2.setValueProviderParameters(new HashMap<String, String>());

		variantProductModel = new GenericVariantProductModel();
		when(Boolean.valueOf(getQualifierProvider().canApply(any(IndexedProperty.class)))).thenReturn(Boolean.FALSE);

		when(mediaModel.getURL()).thenReturn(mediaModelUrl);
		when(mediaModel.getCode()).thenReturn("test");

		valueResolver = new ProductColorFacetResolver();
		valueResolver.setSessionService(getSessionService());
		valueResolver.setQualifierProvider(getQualifierProvider());
		valueResolver.setTypeService(typeService);
	}

	@Test
	public void resolveProductWithNoNormalizedColorSwatches() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);


		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any());
		verify(getInputDocument(), Mockito.never()).addField(any(IndexedProperty.class), any(), any(String.class));
	}

	@Test(expected = FieldValueProviderException.class)
	public void resolveVariantProductExceptionScenario() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final Map<String, String> params = new HashMap<String, String>();
		params.put(SimonCoreConstants.OPTIONAL_PARAM, "false");
		indexedProperty1.setValueProviderParameters(params);


		// when
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

	}

	@Test
	public void resolveProductWithNormalizedColorAndNoSwatchImage() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty1);
		final SwatchColorEnum swatch1 = SwatchColorEnum.BLACK;
		colorSwatches.add(swatch1);
		// when
		variantProductModel.setColorFacetSwatches(colorSwatches);
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty1, swatch1.getCode(), null);
	}

	@Test
	public void resolveProductWithNormalizedColorAndSwatchImage() throws FieldValueProviderException
	{
		// given
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty2);
		final SwatchColorEnum swatch1 = SwatchColorEnum.BLACK;
		colorSwatches.add(swatch1);

		// when
		variantProductModel.setColorFacetSwatches(colorSwatches);
		final EnumerationValueModel enumerationValueModel = new EnumerationValueModel();
		enumerationValueModel.setIcon(mediaModel);
		when(typeService.getEnumerationValue(swatch1)).thenReturn(enumerationValueModel);
		valueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, variantProductModel);

		// then
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperty2, mediaModel.getURL(), null);
	}
}
