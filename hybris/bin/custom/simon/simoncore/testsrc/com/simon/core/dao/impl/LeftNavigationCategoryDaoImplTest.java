package com.simon.core.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.model.MerchandizingCategoryModel;


@UnitTest
public class LeftNavigationCategoryDaoImplTest
{
	@InjectMocks
	private LeftNavigationCategoryDaoImpl leftNavigationCategoryDao;

	@Mock
	private FlexibleSearchService flexibleSearchService;

	@Mock
	private MerchandizingCategoryModel level1CategoryModel;
	@Mock
	private MerchandizingCategoryModel level0CategoryModel;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		when(level1CategoryModel.getCode()).thenReturn("Men");
		when(level1CategoryModel.getName()).thenReturn("Men");
		when(level0CategoryModel.getCode()).thenReturn("Gender");
		when(level0CategoryModel.getName()).thenReturn("Gender");
	}

	@Test
	public void testSearchResultWithValidCategory()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(Collections.singletonList(level1CategoryModel));
		assertEquals(leftNavigationCategoryDao.getL1Categories(level0CategoryModel.getCode()).get(0).getCode(),
				level1CategoryModel.getCode());
	}

	@Test
	public void testSearchResultWithInValidCategory()
	{
		final SearchResult<Object> searchResult = Mockito.mock(SearchResult.class);
		when(flexibleSearchService.search(Mockito.any(FlexibleSearchQuery.class))).thenReturn(searchResult);
		when(searchResult.getResult()).thenReturn(null);
		assertEquals(leftNavigationCategoryDao.getL1Categories(level0CategoryModel.getCode()), null);
	}
}
