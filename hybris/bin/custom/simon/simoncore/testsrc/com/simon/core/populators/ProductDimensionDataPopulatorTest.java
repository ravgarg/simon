package com.simon.core.populators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.search.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.constants.SimonCoreConstants;
import com.simon.core.constants.SimonCoreConstants.IndexedKeys;
import com.simon.core.dto.ImagesData;
import com.simon.core.dto.ProductDimensionsData;
import com.simon.core.exceptions.SystemException;


@UnitTest
public class ProductDimensionDataPopulatorTest
{

	@InjectMocks
	@Spy
	ProductDimensionDataPopulator dimensionDataPopulator;

	@Mock
	private VariantCategoryMapPopulator variantCategoryMapPopulator;

	@Spy
	private ProductImageDataPopulator productImageDataPopulator;

	@Mock
	private PriceDataFactory priceDataFactory;

	@Mock
	private Document document;

	@Mock
	CommonI18NService commonI18NService;

	@Mock
	CurrencyModel currency;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		when((Boolean) document.getFieldValue(IndexedKeys.INSTOCKFLAG)).thenReturn(true);
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_NAME)).thenReturn("baseProductName");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_DESCRIPTION)).thenReturn("baseProductDescription");
		when((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_NAME)).thenReturn("variantProductName");
		when((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_DESCRIPTION)).thenReturn("variantProductDescription");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_NAME)).thenReturn("shopName");
		when((String) document.getFieldValue(IndexedKeys.BASE_PRODUCT_RETAILER_LOGO)).thenReturn("logoURL");
		when((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_ID)).thenReturn("DarkGreen");
		when((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_COLOR_NAME)).thenReturn("Dark Green");
		when((String) document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_CODE)).thenReturn("variantProductCode");
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_DETAIL)).thenReturn(new ArrayList<String>());
		Mockito.doReturn("% Off").when(dimensionDataPopulator).getLocalizedString(SimonCoreConstants.PERCENT_SAVING_MESSAGE);
		when(commonI18NService.getCurrentCurrency()).thenReturn(currency);
		when(currency.getIsocode()).thenReturn("USD");
	}

	@Test
	public void testPopulateWhenDocumentHasValidData()
	{
		final ProductDimensionsData productData = new ProductDimensionsData();
		setImageData();
		setVariantCategoryData();
		setPriceData("MSRP|56.0|true");
		dimensionDataPopulator.populate(document, productData);
		assertTrue(productData.getRetailerLogo().equals("logoURL"));
		assertTrue(productData.getMsrpValue().isStrikeOff());
		assertFalse(productData.getImages().isEmpty());
		assertFalse(MapUtils.isNotEmpty(productData.getDimensions()));
	}

	@Test(expected = SystemException.class)
	public void testPopulateWhenDocumentHasNoImageDataAndInvalidPrice()
	{
		final ProductDimensionsData productData = new ProductDimensionsData();
		setVariantCategoryData();
		setPriceData("MSRP|msrp|true");
		dimensionDataPopulator.populate(document, productData);
		assertTrue(productData.getImages().isEmpty());
		assertTrue(productData.getRetailerLogo().equals("logoURL"));
		assertFalse(productData.getDimensions().isEmpty());
	}

	private void setPriceData(final String price)
	{
		final List<String> priceList = new ArrayList<>();
		priceList.add("LIST|19.95|false");
		priceList.add("SALE|1.95|false");
		priceList.add("PERCENT_SAVING|2");
		priceList.add(price);

		final PriceData priceData = new PriceData();
		priceData.setFormattedValue("$1");
		priceData.setValue(BigDecimal.ONE);

		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_PRICE)).thenReturn(priceList);
		when(priceDataFactory.create(Matchers.any(), BigDecimal.valueOf(Matchers.anyDouble()), Matchers.anyString()))
				.thenReturn(priceData);
	}

	private void setVariantCategoryData()
	{
		final List<String> categoryPath = new ArrayList<>();
		categoryPath.add("test/test/cat01");
		categoryPath.add("test/test/cat2");
		when(document.getFieldValue(IndexedKeys.VARIANT_CATEGORY_PATH)).thenReturn(categoryPath);
		when(document.getFieldValue("cat01Id")).thenReturn("variantValueCategoryId");
		when(document.getFieldValue("cat01Name")).thenReturn("CatName");
		doReturn("Title").when(variantCategoryMapPopulator).getLocalizedStringForLabel("cat01");
	}

	private void setImageData()
	{
		final Map<String, ImagesData> images = new HashMap<>();
		final List<String> imagesFromSolr = new ArrayList<String>();
		imagesFromSolr.add("84Wx128H_default|/images/84Wx128H/1-thumb.jpg");
		imagesFromSolr.add("580Wx884H_default|/images/default/1-1x.jpg");
		imagesFromSolr.add("580Wx884H*2_default|/images/zoom/1-zoom.jpg");
		imagesFromSolr.add("325Wx495H_default|/images/mobile/1-mob-thumb.jpg");
		when(document.getFieldValue(IndexedKeys.VARIANT_PRODUCT_IMAGES)).thenReturn(imagesFromSolr);
	}
}
