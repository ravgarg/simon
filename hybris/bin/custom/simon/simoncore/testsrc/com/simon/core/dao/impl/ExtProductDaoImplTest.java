package com.simon.core.dao.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.validation.services.ValidationService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;


/**
 * ExtProductDaoImplTest, integration test for {@link ExtProductDaoImpl}.
 */
@IntegrationTest
public class ExtProductDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource
	private ExtProductDaoImpl extProductDaoImpl;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Resource
	private UserService userService;

	@Resource
	private static ValidationService validationService = (ValidationService) Registry.getApplicationContext()
			.getBean("validationService");

	/**
	 * Setup Method.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		validationService.unloadValidationEngine();
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testCategories.impex", "utf-8");
		importCsv("/simoncore/test/testProducts.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}

	/**
	 * Verify correct products matching with product codes are returned.
	 */
	@Test
	public void verifyCorrectProductsMatchingWithProductCodesAreReturned()
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final HashSet<String> codes = Sets.newHashSet("testProductMVId1", "testProductMVId7");

		final List<ProductModel> fetchedProducts = extProductDaoImpl.findProductsByCodes(catalogVersion, codes);
		final List<String> actualProductCodes = fetchedProducts.stream().map(ProductModel::getCode).collect(Collectors.toList());
		assertThat(actualProductCodes, Matchers.hasItems("testProductMVId1", "testProductMVId1"));
	}

	/**
	 * Verify exception thrown for null catalog version.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownForNullCatalogVersion()
	{
		final HashSet<String> codes = Sets.newHashSet("testProductMVId1", "testProductMVId7");
		extProductDaoImpl.findProductsByCodes(null, codes);
	}

	/**
	 * Verify exception thrown for null product codes.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void verifyExceptionThrownForNullProductCodes()
	{
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		extProductDaoImpl.findProductsByCodes(catalogVersion, null);
	}

	/**
	 * Verify products having merch category and image are returned.
	 */
	@Test
	public void verifyProductsHavingMerchCategoryAndImageAreReturned()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final List<GenericVariantProductModel> fetchedProducts = extProductDaoImpl
				.findUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion);
		final List<String> actualProductCodes = fetchedProducts.stream().map(GenericVariantProductModel::getCode)
				.collect(Collectors.toList());
		assertThat(actualProductCodes, Matchers.hasItems("testProdSku31"));
	}

	/**
	 * Verify Product which is having any of GTIN or MPN.
	 */
	@Test
	public void verifyProductsAnyOneInGtinAndMpnAvailable()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final List<GenericVariantProductModel> fetchedProducts = extProductDaoImpl
				.findUnApprovedProductsHavingImageAndMerchandizingCategory(catalogVersion);
		final List<String> actualProductCodes = fetchedProducts.stream().map(GenericVariantProductModel::getCode)
				.collect(Collectors.toList());
		assertTrue(actualProductCodes.contains("testProdSku43"));

	}

	/**
	 * Verify find products by catalog version returns correct products.
	 */
	@Test
	public void verifyFindProductsByCatalogVersionReturnsCorrectProducts()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final List<ProductModel> fetchedProducts = extProductDaoImpl.findProductsByCatalogVersion(catalogVersion);
		final List<String> actualProductCodes = fetchedProducts.stream().map(ProductModel::getCode).collect(Collectors.toList());
		assertTrue(actualProductCodes.containsAll(Arrays.asList("testProduct1", "testProduct2", "testProduct3", "testProduct4")));
	}

	/**
	 * Verify find products without merchandizing category returns correct products.
	 */
	@Test
	public void verifyFindProductsWithoutMerchandizingCategoryReturnsCorrectProducts()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final List<ProductModel> fetchedProducts = extProductDaoImpl.findProductsByCatalogVersion(catalogVersion);
		final List<String> actualProductCodes = fetchedProducts.stream().map(ProductModel::getCode).collect(Collectors.toList());
		assertTrue(actualProductCodes.containsAll(Arrays.asList("testProduct1", "testProduct2", "testProduct4")));
	}

	/**
	 * Verify find products by catalog version and approval status returns correct products.
	 */
	@Test
	public void verifyFindProductsByCatalogVersionAndApprovalStatusReturnsCorrectProductsWithCorrectStatus()
	{
		userService.setCurrentUser(userService.getAdminUser());
		final CatalogVersionModel catalogVersion = catalogVersionService.getCatalogVersion("simonProductCatalog", "Staged");
		final List<ProductModel> fetchedProducts = extProductDaoImpl.findProductsByCatalogVersion(catalogVersion,
				ArticleApprovalStatus.UNAPPROVED);
		final Set<ArticleApprovalStatus> approvalStatuses = fetchedProducts.stream().map(ProductModel::getApprovalStatus)
				.collect(Collectors.toSet());
		assertThat(approvalStatuses.size(), is(1));
		assertThat(approvalStatuses.iterator().next(), is(ArticleApprovalStatus.UNAPPROVED));
	}
}
