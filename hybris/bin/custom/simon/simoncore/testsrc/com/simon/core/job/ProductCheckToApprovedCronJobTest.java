package com.simon.core.job;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.simon.core.constants.SimonCoreConstants.Catalog;
import com.simon.core.enums.PriceType;
import com.simon.core.services.ExtProductService;


/**
 * Test class for ProductCheckToApprovedCronJob
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductCheckToApprovedCronJobTest
{
	@InjectMocks
	private ProductCheckToApprovedCronJob productCheckToApprovedCronJob;

	@Mock
	private CronJobModel cronjob;

	@Mock
	private ExtProductService extProductService;

	@Mock
	private CatalogVersionService catalogVersionService;
	@Mock
	private ModelService modelService;

	/**
	 * Test for when Variant list is not empty.
	 */
	@Test
	public void testCheckVariantAndBaseProductStatusChange()
	{
		final GenericVariantProductModel varient = Mockito.mock(GenericVariantProductModel.class);
		final ProductModel baseproduct = Mockito.mock(ProductModel.class);
		final CatalogVersionModel catalogversion = Mockito.mock(CatalogVersionModel.class);
		when(varient.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
		when(baseproduct.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
		when(varient.getBaseProduct()).thenReturn(baseproduct);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED))
				.thenReturn(catalogversion);
		final List<GenericVariantProductModel> varients = new ArrayList<>();
		varients.add(varient);
		when(extProductService.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogversion)).thenReturn(varients);
		final PerformResult result = productCheckToApprovedCronJob.perform(cronjob);
		assertEquals(CronJobResult.SUCCESS, result.getResult());
		assertEquals(CronJobStatus.FINISHED, result.getStatus());
		Mockito.verify(modelService).saveAll(varients);

	}

	/**
	 * Test for when Variant list is not empty.
	 */
	@Test
	public void testPerformResultForVariantListAndWithApprovedBaseProduct()
	{
		final GenericVariantProductModel varient = Mockito.mock(GenericVariantProductModel.class);
		final ProductModel baseproduct = Mockito.mock(ProductModel.class);
		final CatalogVersionModel catalogversion = Mockito.mock(CatalogVersionModel.class);
		when(varient.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
		when(baseproduct.getApprovalStatus()).thenReturn(ArticleApprovalStatus.APPROVED);
		when(varient.getBaseProduct()).thenReturn(baseproduct);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED))
				.thenReturn(catalogversion);
		final List<GenericVariantProductModel> varients = new ArrayList<>();
		varients.add(varient);
		when(extProductService.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogversion)).thenReturn(varients);
		final PerformResult result = productCheckToApprovedCronJob.perform(cronjob);
		assertEquals(CronJobResult.SUCCESS, result.getResult());
		assertEquals(CronJobStatus.FINISHED, result.getStatus());
		Mockito.verify(modelService).saveAll(varients);

	}

	/**
	 * Test for Model saving Exception
	 */
	@Test
	public void verifyModelSavingExceptionCausesCronJobFailure()
	{
		final GenericVariantProductModel varient = Mockito.mock(GenericVariantProductModel.class);
		final ProductModel baseproduct = Mockito.mock(ProductModel.class);
		final CatalogVersionModel catalogversion = Mockito.mock(CatalogVersionModel.class);
		when(varient.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
		when(baseproduct.getApprovalStatus()).thenReturn(ArticleApprovalStatus.CHECK);
		when(varient.getBaseProduct()).thenReturn(baseproduct);
		when(catalogVersionService.getCatalogVersion(Catalog.PRODUCT_CATALOG_CODE, Catalog.STAGED))
				.thenReturn(catalogversion);
		final List<GenericVariantProductModel> varients = new ArrayList<>();
		varients.add(varient);
		when(extProductService.getCheckedProductsHavingPriceRow(PriceType.LIST, catalogversion)).thenReturn(varients);
		doThrow(new ModelSavingException("Saving test failed")).when(modelService).saveAll(varients);
		final PerformResult result = productCheckToApprovedCronJob.perform(cronjob);
		assertEquals(CronJobResult.ERROR, result.getResult());
		assertEquals(CronJobStatus.ABORTED, result.getStatus());


	}

}
