package com.simon.core.syetem.initial.data;

import static org.junit.Assert.assertTrue;

import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.SetupImpexService;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.simon.core.syetem.initial.data.setup.SimonSampleDataImportService;


/**
 * Test for SimonSampleDataImportService
 */
public class SimonSampleDataImportServiceTest
{
	@InjectMocks
	@Spy
	private SimonSampleDataImportService simonSampleDataImportService;
	@Mock
	private SetupImpexService setupImpexService;
	@Mock
	private ConfigurationService configurationService;
	@Mock
	private Configuration configuration;
	@Mock
	private AbstractSystemSetup systemSetup;
	@Mock
	private SystemSetupContext context;
	@Mock
	private ImportData importData;
	final String extensionName = "simoninitialdata";

	/**
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		simonSampleDataImportService = Mockito.spy(new SimonSampleDataImportService());
		Mockito.doReturn(setupImpexService).when(simonSampleDataImportService).getSetupImpexService();
		Mockito.doReturn(configurationService).when(simonSampleDataImportService).getConfigurationService();
	}

	/**
	 * Test when design import.designers.retailers is true
	 */
	@Test
	public void importCommonData()
	{
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleCommon(extensionName);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/common/customer-address.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/common/designers.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/common/retailers.impex", extensionName), false);
		simonSampleDataImportService.importCommonData(extensionName);
		Mockito.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/common/customer-address.impex", extensionName), false);

		Mockito.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/sampledata/common/custom-message.impex", extensionName), false);
	}

	/**
	 * Test when importSampleProduct is true
	 */
	@Test
	public void importAllData()
	{
		Mockito.doReturn(true).when(systemSetup).getBooleanSystemSetupParameter(context, "importSampleProduct");
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleImportAll(systemSetup, context, importData, true);
		simonSampleDataImportService.importAllData(systemSetup, context, importData, true);
		assertTrue(simonSampleDataImportService.isImportSampleProduct());
	}

	/**
	 * Test when importSampleProduct is false
	 */
	@Test
	public void importAllDataFalse()
	{
		Mockito.doReturn(false).when(systemSetup).getBooleanSystemSetupParameter(context, "importSampleProduct");
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleImportAll(systemSetup, context, importData, true);
		simonSampleDataImportService.importAllData(systemSetup, context, importData, true);
		assertTrue(!simonSampleDataImportService.isImportSampleProduct());
	}


	/**
	 * Test when importSampleProduct is true
	 */
	@Test
	public void importProductCatalog()
	{
		Mockito.doNothing().when(setupImpexService).importImpexFile(
				String.format("%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample.impex", extensionName),
				false);
		Mockito.doNothing().when(setupImpexService).importImpexFile(
				String.format("%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-media.impex", extensionName),
				false);
		Mockito.doNothing().when(setupImpexService).importImpexFile(String.format(
				"%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-relations.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService).importImpexFile(String.format(
				"%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-stocklevels.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService).importImpexFile(
				String.format("%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample-prices.impex", extensionName),
				false);
		Mockito.doReturn(true).when(systemSetup).getBooleanSystemSetupParameter(context, "importSampleProduct");
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleImportAll(systemSetup, context, importData, true);
		Mockito.doNothing().when(simonSampleDataImportService).parentImportProductCatalog(extensionName, "catalog");
		simonSampleDataImportService.importAllData(systemSetup, context, importData, true);
		simonSampleDataImportService.importProductCatalog(extensionName, "catalog");
		assertTrue(simonSampleDataImportService.isImportSampleProduct());
		Mockito.verify(setupImpexService).importImpexFile(
				String.format("/%s/import/sampledata/productCatalogs/simonProductCatalog/products-sample.impex", extensionName),
				false);
	}

	/**
	 * Test when importSampleProduct is false
	 */
	@Test
	public void importProductCatalogFalse()
	{
		Mockito.doReturn(false).when(systemSetup).getBooleanSystemSetupParameter(context, "importSampleProduct");
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleImportAll(systemSetup, context, importData, true);
		simonSampleDataImportService.importAllData(systemSetup, context, importData, true);
		Mockito.doNothing().when(simonSampleDataImportService).parentImportProductCatalog(extensionName, "catalog");
		simonSampleDataImportService.importAllData(systemSetup, context, importData, true);
		simonSampleDataImportService.importProductCatalog(extensionName, "catalog");
		assertTrue(!simonSampleDataImportService.isImportSampleProduct());
	}

	/**
	 * Test for importing solr configuration
	 */
	@Test
	public void importSolrIndex()
	{
		final String storeName = "test";
		Mockito.doNothing().when(simonSampleDataImportService).parentSampleSolr(extensionName, storeName);
		simonSampleDataImportService.importSolrIndex(extensionName, storeName);
	}





}
