package com.simon.core.syetem.initial.data;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commerceservices.setup.SetupImpexService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.syetem.initial.data.setup.SimonCoreDataImportService;


/**
 * Test case for SimonCoreDataImportService
 */
@UnitTest
public class SimonCoreDataImportServiceTest
{
	@InjectMocks
	private SimonCoreDataImportService simonCoreDataImportService;
	@Mock
	private SetupImpexService setupImpexService;
	final String extensionName = "simoninitialdata";

	/**
	 * set up
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		simonCoreDataImportService = Mockito.spy(new SimonCoreDataImportService());
		Mockito.doReturn(setupImpexService).when(simonCoreDataImportService).getSetupImpexService();
	}

	/**
	 * test for importing common data
	 */
	@Test
	public void importCommonData()
	{
		Mockito.doNothing().when(simonCoreDataImportService).parentCommonData(extensionName);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/mirakl-user-groups.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/mirakl-common-addon-extra.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/user-groups.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/my-centers.impex", extensionName), false);
		simonCoreDataImportService.importCommonData(extensionName);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/searchRestriction.impex", extensionName), false);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/cronJob.impex", extensionName), false);

		Mockito.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/common/mirakl-user-groups.impex", extensionName), false);

	}

	/**
	 * test for importing product data
	 */
	@Test
	public void importProductData()
	{
		final String productCatalogName = "test";
		Mockito.doNothing().when(simonCoreDataImportService).parentProductCatalog(extensionName, productCatalogName);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/mirakl-core-attributes.impex",
						extensionName, productCatalogName), false);
		simonCoreDataImportService.importProductCatalog(extensionName, productCatalogName);
		Mockito.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/productCatalogs/%sProductCatalog/mirakl-core-attributes.impex",
						extensionName, productCatalogName), false);
	}

	/**
	 * test for importing store data
	 */
	@Test
	public void importStore()
	{
		final String productCatalogName = "test";
		final String storeName = "simon";
		Mockito.doNothing().when(simonCoreDataImportService).parentStoreData(extensionName, storeName, productCatalogName);
		Mockito.doNothing().when(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/stores/%s/mirakl-site.impex", extensionName, storeName), false);
		simonCoreDataImportService.importStore(extensionName, storeName, productCatalogName);
		Mockito.verify(setupImpexService)
				.importImpexFile(String.format("/%s/import/coredata/stores/%s/mirakl-site.impex", extensionName, storeName), false);
	}
}
