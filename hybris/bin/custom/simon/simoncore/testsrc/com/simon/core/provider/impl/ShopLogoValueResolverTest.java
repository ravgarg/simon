package com.simon.core.provider.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.mirakl.hybris.core.model.ShopModel;


@UnitTest
public class ShopLogoValueResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	ShopLogoValueResolver shopLogoValueResolver;

	@Mock
	private MediaModel mediaModel;

	@Mock
	private ProductModel baseProduct;

	@Mock
	private ShopModel shop;

	public static final String OPTIONAL_PARAM = "optional";

	@Test
	public void testResolveWithValidData() throws FieldValueProviderException
	{
		final Collection<IndexedProperty> indexedProperties = setindexedProperties();
		GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		model = setGenericVariantProductModel(model, mediaModel, shop, "URL");
		shopLogoValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(1)).addField(indexedProperties.iterator().next(), "URL", null);
	}


	@Test(expected = FieldValueProviderException.class)
	public void testResolveWithNullLogo() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		when(model.getBaseProduct()).thenReturn(baseProduct);
		when(baseProduct.getShop()).thenReturn(shop);
		shopLogoValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(0)).addField(indexedProperties.iterator().next(), "URL", null);
	}


	@Test(expected = FieldValueProviderException.class)
	public void testResolverWithNullShopAndIsNotOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "false");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		when(model.getBaseProduct()).thenReturn(baseProduct);
		shopLogoValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
	}

	@Test
	public void testResolverWithNullShopAndIsOptional() throws FieldValueProviderException
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		final Map<String, String> valueProviderParameters = new HashMap<>();
		valueProviderParameters.put(OPTIONAL_PARAM, "true");
		indexedProperty.setValueProviderParameters(valueProviderParameters);
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		final GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		when(model.getBaseProduct()).thenReturn(baseProduct);
		shopLogoValueResolver.resolve(getInputDocument(), getBatchContext(), indexedProperties, model);
		verify(getInputDocument(), Mockito.times(0)).addField(indexedProperties.iterator().next(), "URL", null);
	}



	private GenericVariantProductModel setGenericVariantProductModel(final GenericVariantProductModel genericVariantProduct,
			final MediaModel logo, final ShopModel retailer, final String url)
	{
		when(genericVariantProduct.getBaseProduct()).thenReturn(baseProduct);
		when(baseProduct.getShop()).thenReturn(retailer);
		when(retailer.getLogo()).thenReturn(logo);
		when(logo.getURL()).thenReturn(url);
		return genericVariantProduct;

	}

	private Collection<IndexedProperty> setindexedProperties()
	{
		final IndexedProperty indexedProperty = getIndexedProperty();
		indexedProperty.setName("Name");
		final Collection<IndexedProperty> indexedProperties = Collections.singletonList(indexedProperty);
		return indexedProperties;
	}
}
