package com.simon.core.mirakl.strategies.impl;

import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.servicelayer.config.ConfigurationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mirakl.hybris.beans.ProductImportFileContextData;
import com.mirakl.hybris.core.model.MiraklRawProductModel;
import com.mirakl.hybris.core.model.ShopModel;
import com.mirakl.hybris.core.shop.services.ShopService;
import com.simon.core.mirakl.product.dao.MiraklRawProductDAO;


/**
 * Unit test class
 */
@UnitTest
public class MediaConversionPostProcessorTest
{

	@InjectMocks
	private MediaConversionPostProcessor mediaConversionPostProcessor;

	@Mock
	private ProductImportFileContextData context;
	@Mock
	private ShopModel shopModel;

	private static final String IMPORT_ID = "importId";

	private List<MiraklRawProductModel> miraklProductList;

	@Mock
	private MiraklRawProductModel rawProductModel;

	@Mock
	private MiraklRawProductDAO miraklRawProductDAOImpl;

	@Mock
	private Configuration config;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private ShopService shopService;

	private static final String PRODUCT_SKU = "sku";

	private static final String PRODUCT_RAW_PICTURE = "rawPicture";

	private static final String CSV_SEPERATOR = "product.image.csv.seperator";

	private static final String FILENAME_SEPERATOR = "product.image.filename.seperator";

	private static final String CSV_URL = "simon.hotfolder.importdata.image";

	private static final String PREFIX_FILENAME_CSV = "image.product.import.prefix.filename";

	private static final String DEFAULT_SEPERATOR = ",";

	private static final String DEFAULT_FILENAME_SEPERATOR = "-";

	/**
	 *
	 */
	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		miraklProductList = new ArrayList<>();
		miraklProductList.add(rawProductModel);
		when(configurationService.getConfiguration()).thenReturn(config);
		when(miraklRawProductDAOImpl.fetchMiraklProductsForImport(IMPORT_ID)).thenReturn(miraklProductList);
		when(configurationService.getConfiguration().getString(FILENAME_SEPERATOR, DEFAULT_FILENAME_SEPERATOR))
				.thenReturn(DEFAULT_FILENAME_SEPERATOR);
		when(configurationService.getConfiguration().getString(CSV_URL)).thenReturn("URL");
		when(configurationService.getConfiguration().getString(PREFIX_FILENAME_CSV)).thenReturn("PREFIX");
		final Map<String, String> values = new HashMap<>();
		values.put(PRODUCT_SKU, "12345");
		values.put(PRODUCT_RAW_PICTURE, "rawpictureurl");
		when(rawProductModel.getValues()).thenReturn(values);

		when(context.getShopId()).thenReturn("shopId");
		when(shopService.getShopForId(context.getShopId())).thenReturn(shopModel);


		when(configurationService.getConfiguration().getString(CSV_SEPERATOR, DEFAULT_SEPERATOR)).thenReturn(DEFAULT_SEPERATOR);

	}

	/**
	 *
	 */
	@Test
	public void testPostProcess()
	{
		mediaConversionPostProcessor.postProcess(context, IMPORT_ID);

	}

}
