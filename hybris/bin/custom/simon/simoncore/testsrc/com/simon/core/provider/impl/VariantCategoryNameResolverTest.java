package com.simon.core.provider.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractValueResolverTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;
import de.hybris.platform.variants.model.VariantCategoryModel;
import de.hybris.platform.variants.model.VariantValueCategoryModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.enums.ControlType;
import com.simon.core.model.ShopGatewayModel;
import com.simon.core.services.ShopGatewayService;
import com.simon.core.services.VariantAttributeMappingService;


/**
 * VariantCategoryNameResolverTest unit test for {@link VariantCategoryNameResolver}.
 */
public class VariantCategoryNameResolverTest extends AbstractValueResolverTest
{
	@InjectMocks
	@Spy
	VariantCategoryNameResolver variantCategoryNameResolver;

	@Mock
	private ShopGatewayService shopGatewayService;

	@Mock
	private VariantAttributeMappingService variantAttributeMappingService;

	@Captor
	private ArgumentCaptor<String> valueString;

	/**
	 * Verify correct variant categories list are indexed.
	 *
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Test
	public void verifyCorrectVariantCategoriesListAreIndexed() throws FieldValueProviderException
	{
		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final ShopModel shop = mock(ShopModel.class);
		final GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		when(model.getShop()).thenReturn(shop);

		when(shopGatewayService.getShopGatewayForCode(anyString())).thenReturn(shopGateway);

		final Map<String, String> variantAttributeLabelMap = new HashMap<>();
		variantAttributeLabelMap.put("VC-color", "color");
		variantAttributeLabelMap.put("VC-size", "size");

		final Map<String, ControlType> variantAttributeControlTypeMap = new HashMap<>();
		variantAttributeControlTypeMap.put("VC-color", ControlType.SWATCH);
		variantAttributeControlTypeMap.put("VC-size", ControlType.DROPDOWN);

		when(variantAttributeMappingService.getVariantAttributeLabelMap(shop, shopGateway)).thenReturn(variantAttributeLabelMap);
		when(variantAttributeMappingService.getVariantAttributeControlTypeMap(shop, shopGateway))
				.thenReturn(variantAttributeControlTypeMap);

		final VariantCategoryModel variantCategoryColor = mock(VariantCategoryModel.class);
		when(variantCategoryColor.getCode()).thenReturn("VC-color");
		final VariantCategoryModel variantCategorySize = mock(VariantCategoryModel.class);
		when(variantCategorySize.getCode()).thenReturn("VC-size");

		final VariantValueCategoryModel variantValueCategoryColor = mock(VariantValueCategoryModel.class);
		when(variantValueCategoryColor.getSupercategories()).thenReturn(Arrays.asList(variantCategoryColor));

		final VariantValueCategoryModel variantValueCategorySize = mock(VariantValueCategoryModel.class);
		when(variantValueCategorySize.getSupercategories()).thenReturn(Arrays.asList(variantCategorySize));

		when(model.getSupercategories()).thenReturn(Arrays.asList(variantValueCategoryColor, variantValueCategorySize));

		variantCategoryNameResolver.resolve(getInputDocument(), getBatchContext(),
				Collections.singletonList(mock(IndexedProperty.class)), model);

		verify(getInputDocument(), times(2)).addField(any(IndexedProperty.class), valueString.capture(), anyString());

		assertTrue(valueString.getAllValues().contains("VC-color|color|swatch"));
		assertTrue(valueString.getAllValues().contains("VC-size|size|dropDown"));
	}

	/**
	 * Verify correct variant categories list are indexed if control type is null.
	 *
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Test
	public void verifyCorrectVariantCategoriesListAreIndexedIfControlTypeIsNull() throws FieldValueProviderException
	{
		final ShopGatewayModel shopGateway = mock(ShopGatewayModel.class);
		final ShopModel shop = mock(ShopModel.class);
		final GenericVariantProductModel model = mock(GenericVariantProductModel.class);
		when(model.getShop()).thenReturn(shop);

		when(shopGatewayService.getShopGatewayForCode(anyString())).thenReturn(shopGateway);

		final Map<String, String> variantAttributeLabelMap = new HashMap<>();
		variantAttributeLabelMap.put("VC-color", "color");
		variantAttributeLabelMap.put("VC-size", "size");

		final Map<String, ControlType> variantAttributeControlTypeMap = new HashMap<>();
		variantAttributeControlTypeMap.put("VC-color", ControlType.SWATCH);
		variantAttributeControlTypeMap.put("VC-size", null);

		when(variantAttributeMappingService.getVariantAttributeLabelMap(shop, shopGateway)).thenReturn(variantAttributeLabelMap);
		when(variantAttributeMappingService.getVariantAttributeControlTypeMap(shop, shopGateway))
				.thenReturn(variantAttributeControlTypeMap);

		final VariantCategoryModel variantCategoryColor = mock(VariantCategoryModel.class);
		when(variantCategoryColor.getCode()).thenReturn("VC-color");
		final VariantCategoryModel variantCategorySize = mock(VariantCategoryModel.class);
		when(variantCategorySize.getCode()).thenReturn("VC-size");

		final VariantValueCategoryModel variantValueCategoryColor = mock(VariantValueCategoryModel.class);
		when(variantValueCategoryColor.getSupercategories()).thenReturn(Arrays.asList(variantCategoryColor));

		final VariantValueCategoryModel variantValueCategorySize = mock(VariantValueCategoryModel.class);
		when(variantValueCategorySize.getSupercategories()).thenReturn(Arrays.asList(variantCategorySize));

		when(model.getSupercategories()).thenReturn(Arrays.asList(variantValueCategoryColor, variantValueCategorySize));

		variantCategoryNameResolver.resolve(getInputDocument(), getBatchContext(),
				Collections.singletonList(mock(IndexedProperty.class)), model);

		verify(getInputDocument(), times(2)).addField(any(IndexedProperty.class), valueString.capture(), anyString());

		assertTrue(valueString.getAllValues().contains("VC-color|color|swatch"));
		assertTrue(valueString.getAllValues().contains("VC-size|size|swatch"));
	}
}
