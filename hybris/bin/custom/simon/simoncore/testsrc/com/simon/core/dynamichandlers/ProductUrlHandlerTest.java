package com.simon.core.dynamichandlers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.variants.model.GenericVariantProductModel;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.mirakl.hybris.core.model.ShopModel;


/**
 * Test class {@link ProductUrlHandler }
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ProductUrlHandlerTest
{

	@InjectMocks
	private ProductUrlHandler productUrlHandler;

	private static final String RAW_URL = "rawurl.com/raw";

	private static final String TEST_DOMAIN_URL = "rawurl.com";

	private static final String TEST_PRODUCT_URL = "/raw";

	@Mock
	private GenericVariantProductModel product;

	@Mock
	private ShopModel shop;

	/**
	 * Verify when testing mode off
	 */
	@Test
	public void testProductStatusWhenTestingModeOff()
	{
		shop = mock(ShopModel.class);
		when(shop.isTestMode()).thenReturn(false);
		when(product.getRawProductUrl()).thenReturn(RAW_URL);
		final String returnUrl = productUrlHandler.get(product);
		assertEquals(RAW_URL, returnUrl);

	}

	/**
	 * Verify when testing mode on
	 */
	@Test
	public void testProductStatusWhenTestingModeOn()
	{
		shop = mock(ShopModel.class);
		when(shop.isTestMode()).thenReturn(true);
		when(product.getShop()).thenReturn(shop);
		when(shop.getTestDomain()).thenReturn(TEST_DOMAIN_URL);
		when(product.getTestProductPath()).thenReturn(TEST_PRODUCT_URL);
		final String returnUrl = productUrlHandler.get(product);
		assertEquals(RAW_URL, returnUrl);
	}


}
