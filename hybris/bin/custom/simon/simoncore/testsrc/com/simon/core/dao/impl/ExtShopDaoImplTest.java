
package com.simon.core.dao.impl;

import static org.junit.Assert.assertEquals;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mirakl.hybris.core.model.ShopModel;
import com.simon.core.dao.ExtShopDao;


/**
 * The Class ExtShopDaoTest. Integration test for {@link ExtShopDao}
 */
@IntegrationTest
public class ExtShopDaoImplTest extends ServicelayerTransactionalTest
{

	@Resource(name = "shopDao")
	private ExtShopDao extShopDao;


	@Resource
	private CatalogVersionService catalogVersionService;

	/**
	 * Sets the up.
	 *
	 * @throws ImpExException
	 *            the imp ex exception
	 */
	@Before
	public void setUp() throws ImpExException
	{
		importCsv("/simoncore/test/testBasics.impex", "utf-8");
		importCsv("/simoncore/test/testStore.impex", "utf-8");
		catalogVersionService.setSessionCatalogVersion("simonProductCatalog", "Staged");
	}


	/**
	 * Verify null store is returned for invalid code.
	 */
	@Test
	public void verifyDesignerListEmptyIsReturnedForInvalidObjectCodes()
	{
		final String[] codes = new String[1];
		codes[0] = "testShop";
		final List<ShopModel> actual = extShopDao.findListOfShopsForCodes(codes);
		assertEquals(actual.size(), 0);
	}

	/**
	 * Verify correct designer is returned for valid code.
	 */
	@Test
	public void verifyCorrectAllStoreIsReturnedForValidCodes()
	{
		final String[] codes = new String[1];
		codes[0] = "testShop1";
		final List<ShopModel> actual = extShopDao.findListOfShopsForCodes(codes);
		assertEquals(actual.size(), 1);
	}

	/**
	 * Verify null store is returned for invalid code.
	 */
	@Test
	public void verifyDesignerListEmptyIsReturnedForInvalidCodes()
	{
		final String[] codes = new String[1];
		codes[0] = "testShop";
		final List<ShopModel> actual = extShopDao.findListOfShopsForCodes(codes);
		assertEquals(actual.size(), 0);
	}

	/**
	 * Verify correct designer is returned for valid code.
	 */
	@Test
	public void verifyCorrectAllStoreIsReturnedForValidObjectCodes()
	{
		final String[] codes = new String[1];
		codes[0] = "testShop1";
		final List<ShopModel> actual = extShopDao.findListOfShopsForCodes(codes);
		assertEquals(actual.size(), 1);
	}

	@Test
	public void getRetailerList()
	{
		final List<ShopModel> actual = extShopDao.getRetailerList();
		Assert.assertTrue(!actual.isEmpty());
	}
}
