package com.simon.core.services.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.simon.core.dao.MallDao;
import com.simon.core.model.MallModel;


/**
 * Test Class for MallServiseImpl
 */
@UnitTest
public class MallServiceImplTest
{
	@InjectMocks
	private MallServiceImpl mallServiceImpl;

	@Mock
	private MallDao mallDao;


	/**
	 * Sets the initial data for each test case.
	 */
	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Method to test getMallForCode by code in valid case.
	 */
	@Test
	public void testGetMallForCode()
	{
		final MallModel mall = new MallModel();
		mall.setLogo("logo");

		Mockito.when(mallDao.findMallByCode("test")).thenReturn(mall);

		final MallModel result = mallServiceImpl.getMallForCode("test");

		Assert.assertEquals("logo", result.getLogo());
	}

	/**
	 * Method to test getAllMalls in valid case.
	 */
	@Test
	public void testGetAllMalls()
	{
		final MallModel mall = new MallModel();
		mall.setLogo("logo");

		final List<MallModel> listOfMalls = new ArrayList<>();
		listOfMalls.add(mall);

		Mockito.when(mallDao.findAllMalls()).thenReturn(listOfMalls);

		final List<MallModel> result = mallServiceImpl.getAllMalls();

		Assert.assertTrue(!result.isEmpty());
	}

	/**
	 * Method to test getMallListForCode by list of codes in valid case.
	 */
	@Test
	public void testGetMallListForCode()
	{
		final List<String> mallCodes = new ArrayList<>();

		final MallModel mall = new MallModel();
		mall.setLogo("logo");

		final Set<MallModel> setOfMalls = new HashSet<>();
		setOfMalls.add(mall);

		Mockito.when(mallDao.findMallListForCode(mallCodes)).thenReturn(setOfMalls);

		final Set<MallModel> result = mallServiceImpl.getMallListForCode(mallCodes);

		Assert.assertTrue(!result.isEmpty());
	}

}
