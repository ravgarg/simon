package com.simon.media.hotfolder.task;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.acceleratorservices.dataimport.batch.BatchHeader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Test Class for Transformer Impex
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class ExtImpexTransformerTaskTest
{
	@Mock
	private BatchHeader header;
	@Mock
	private File file;

	/**
	 * Test Method for Transformer Impex
	 *
	 * @throws UnsupportedEncodingException
	 * @throws FileNotFoundException
	 */
	@Test
	public void executeFileNameNotMatch() throws UnsupportedEncodingException, FileNotFoundException
	{

		final ExtImpexTransformerTask simonImpexTransformerTask = Mockito.spy(new ExtImpexTransformerTask());
		Mockito.when(header.getFile()).thenReturn(file);
		Mockito.when(file.getName()).thenReturn("file_101011");
		Mockito.when(header.getFile().getName()).thenReturn("file_101011");
		simonImpexTransformerTask.setFileName("fileName");
		Assert.assertNotNull(simonImpexTransformerTask.execute(header));
	}

}
