package com.simon.core.promotions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.simon.core.services.RetailerService;
import com.simon.promotion.rao.RetailerRAO;


@UnitTest
public class RetailerRaoPopulatorTest
{
	@InjectMocks
	RetailerRaoPopulator populator;
	@Mock
	RetailerService retailerService;
	@Mock
	CartModel cart;
	CartRAO cartRAO;
	@Mock
	CartEntryModel entry1;
	@Mock
	private Converter<AbstractOrderModel, RetailerRAO> retailerRaoConverter;

	@Before
	public void setup()
	{
		initMocks(this);
		cartRAO = new CartRAO();
		final Map<String, Double> retailerSubTotalMap = new HashMap();
		when(retailerService.getRetailerSubBagForCart(cart)).thenReturn(retailerSubTotalMap);
		retailerSubTotalMap.put("retailer1", 10.0);
		final List<AbstractOrderEntryModel> entries = new ArrayList();
		entries.add(entry1);
		when(cart.getEntries()).thenReturn(entries);
	}

	@Test
	public void populate()
	{
		populator.populate(cart, cartRAO);
		final List<RetailerRAO> retailers = cartRAO.getRetailers();
		assertNotNull(retailers);
		assertEquals(1, retailers.size());
		final RetailerRAO retailer = retailers.get(0);
		assertEquals("retailer1", retailer.getRetailerId());
		assertEquals(10.0, retailer.getSubTotal().doubleValue(), 0.0);
	}

}
