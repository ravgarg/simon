/**
*
 * Created on Jun 13, 2017
*
* Copyright 2014, SapientRazorfish;  All Rights Reserved.
*
* This software is the confidential and proprietary information of
* SapientRazorfish, ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with SapientRazorfish.
*
*/
package com.sape.junit;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/**
 * This annotation is used to mark the JUnits with the list of applicable features.
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface FunctionalTest
{
	String[] features();
}
