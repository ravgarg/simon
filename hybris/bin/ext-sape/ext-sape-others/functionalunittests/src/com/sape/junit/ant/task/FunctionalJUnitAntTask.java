/**
*
 * Created on Jun 13, 2017
*
* Copyright 2014, SapientRazorfish;  All Rights Reserved.
*
* This software is the confidential and proprietary information of
* SapientRazorfish, ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with SapientRazorfish.
*
*/
package com.sape.junit.ant.task;

import de.hybris.ant.taskdefs.PlatformConfigAntUtils;
import de.hybris.ant.taskdefs.yunit.JUnitTask;
import de.hybris.ant.taskdefs.yunit.JUnitTest;
import de.hybris.bootstrap.config.ExtensionInfo;
import de.hybris.bootstrap.config.PlatformConfig;
import de.hybris.bootstrap.loader.PlatformInPlaceClassLoader;
import de.hybris.bootstrap.testclasses.TestClassesUtil;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import org.apache.tools.ant.Project;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;

import com.sape.junit.FunctionalTestRunner;
import com.sape.junit.FunctionalTestUtil;


/**
 *
 * This class will filter out the JUnits as per configured functional area. It will read the FunctionalTest annotation
 * applied to the class and get the list of applicable features for each JUnit. It will then compare those features with
 * the list of features configured for the current execution of JUnit and filter out the JUnit as per configured
 * features.
 *
 */
public class FunctionalJUnitAntTask extends JUnitTask
{
	/**
	 * Comma separated list of functional area that should be used to filter the list of JUnits for execution. This is
	 * configured via runtime argument -Dtestclasses.features
	 */
	private String features;

	public FunctionalJUnitAntTask() throws Exception
	{
		super();
	}

	/**
	 * This method will compare the configured list of functional area with applicable functional area for each JUnit. If
	 * there is any overlap then JUnit will be added to the list of Junits that will be executed.If
	 * functionaltest.class.annotation parameter is specified via command line with value false then this method will not
	 * perform any filtering.
	 */
	@Override
	protected Enumeration getIndividualTests()
	{
		boolean classFilteringEnabled = true;
		boolean methodFilteringEnabled = true;
		final String classAnnotation = getProject().getProperty("functionaltest.class.annotation");
		if (null != classAnnotation && Boolean.FALSE.equals(Boolean.valueOf(classAnnotation)))
		{
			classFilteringEnabled = false;
		}
		final String methodAnnotation = getProject().getProperty("functionaltest.method.annotation");
		if (null != methodAnnotation && Boolean.FALSE.equals(Boolean.valueOf(methodAnnotation)))
		{
			methodFilteringEnabled = false;
		}
		log("Class Filtering Enabled:" + classFilteringEnabled);
		log("Method Filtering Enabled:" + methodFilteringEnabled);

		@SuppressWarnings("unchecked")
		final Enumeration<JUnitTest> enm = super.getIndividualTests();
		final Vector<JUnitTest> tests = new Vector<>();
		/**
		 * if features is euqal to string "${testclasses.features}" then it means no features are configured for the
		 * current execution. Therefore filterting based on functional area is not needed
		 */
		if ("${testclasses.features}".equals(features))
		{
			return enm;
		}
		final String[] featureList = features.split(",");
		final PlatformConfig platformConfig = PlatformConfigAntUtils.getOrLoadPlatformConfig(this);
		final ClassLoader classLoader = getClassLoader(platformConfig);

		while (enm.hasMoreElements())
		{
			final JUnitTest test = enm.nextElement();
			try
			{
				final Class clazz = Class.forName(test.getName(), false, classLoader);
				if (!classFilteringEnabled)
				{
					Annotation annot = null;
					for (final Annotation annotation : clazz.getDeclaredAnnotations())
					{
						if (annotation.annotationType().getName().equals(RunWith.class.getName()))
						{
							annot = annotation;
							break;
						}
					}
					if (null == annot)
					{
						continue;
					}
					final Class<? extends Runner> runnerClass = (Class<? extends Runner>) annot.getClass()
							.getMethod("value", new Class[] {}).invoke(annot);
					/*
					 * If class filtering is disabled but method filtering is enabled and class is annotated with
					 * FunctionalTestRunner then JUnits should be filtered if any of the method matches with the list of
					 * configured features.
					 */
					if (methodFilteringEnabled && runnerClass.getName().equals(FunctionalTestRunner.class.getName())
							&& isFeatureApplicable(clazz.getDeclaredMethods(), featureList))
					{
						tests.add(test);
						log("Adding Class " + test.getName());
					}
				}
				else if (FunctionalTestUtil.isFeatureApplicable(clazz.getAnnotations(), featureList))
				{
					tests.add(test);
					log("Adding Class " + test.getName());
				}
			}
			catch (final ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e1)
			{
				log(e1, Project.MSG_ERR);
			}

		}


		return tests.elements();
	}

	/**
	 * This method will compare the list of features annotated at method with list of configured features. If there is
	 * any feature common between the two list then it will return true
	 *
	 * @param methods
	 *           - List of methods to validate the applicable features
	 * @param configuredFeatures
	 *           - List of configured features
	 * @return - true if any one method is annotated with the feature present in the configuredFeatures list
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	private boolean isFeatureApplicable(final Method[] methods, final String[] configuredFeatures)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		for (final Method method : methods)
		{
			if (FunctionalTestUtil.isFeatureApplicable(method.getDeclaredAnnotations(), configuredFeatures))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will create a class loader with all class paths required to load the classes. This class loader will
	 * be used to load the JUnit classes and analyze the FunctionalTest annotation on those classes.
	 *
	 * @param platformConfig
	 *           - Platform Configuration
	 * @return - ClassLoader needed to load the Classes
	 */
	private ClassLoader getClassLoader(final PlatformConfig platformConfig)
	{
		final List<URL> urls = new ArrayList<>();
		for (final ExtensionInfo extensionInfo : platformConfig.getExtensionInfosInBuildOrder())
		{
			urls.addAll(getWebModuleLibJar(extensionInfo));

			final File extDir = extensionInfo.getExtensionDirectory();
			urls.add(toURL(extDir + "/web/testclasses"));
			urls.add(toURL(extDir + "/web/webroot/WEB-INF/classes"));
		}
		final ClassLoader classLoader = TestClassesUtil.createTestClassLoader(platformConfig,
				new PlatformInPlaceClassLoader(platformConfig, "", null, false));
		return new URLClassLoader(urls.toArray(new URL[] {}), classLoader);
	}

	/**
	 * This method is invoked for each extension to get the list of URL for all jar files within lib folder of the
	 * extension.
	 *
	 * @param extensionInfo
	 *           - Extension Information
	 * @return - List of URL to be used for the class path for the extension
	 */
	private List<URL> getWebModuleLibJar(final ExtensionInfo extensionInfo)
	{
		final List<URL> result = new ArrayList<>();

		final String webModuleLibDir = extensionInfo.getExtensionDirectory() + "/web/webroot/WEB-INF/lib";
		final File libDir = new File(webModuleLibDir);

		if ((libDir.exists()) && (libDir.isDirectory()))
		{
			for (final File file : libDir.listFiles())
			{
				if (!(file.getPath().endsWith("jar")))
				{
					continue;
				}
				try
				{
					result.add(file.toURI().toURL());
				}
				catch (final MalformedURLException e)
				{
					throw new IllegalStateException(e.getMessage(), e);
				}
			}

		}

		return result;
	}

	/**
	 * This method will convert the string parameter to URL instance.
	 *
	 * @param path
	 * @return - URL
	 */
	private URL toURL(final String path)
	{
		try
		{
			return Paths.get(path, new String[0]).toUri().toURL();
		}
		catch (final MalformedURLException e)
		{
			throw new IllegalStateException(e.getMessage(), e);
		}
	}


	public String getFeatures()
	{
		return features;
	}

	public void setFeatures(final String features)
	{
		this.features = features;
	}

}
