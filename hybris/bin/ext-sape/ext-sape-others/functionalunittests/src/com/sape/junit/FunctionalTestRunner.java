/**
*
 * Created on Jun 13, 2017
*
* Copyright 2014, SapientRazorfish;  All Rights Reserved.
*
* This software is the confidential and proprietary information of
* SapientRazorfish, ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with SapientRazorfish.
*
*/
package com.sape.junit;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;


/**
 *
 * This class will filter out the JUnits as per configured functional area. It will read the FunctionalTest annotation
 * applied to the methods and get the list of applicable features for each method. It will then compare those features
 * with the list of features configured for the current execution of JUnit and filter out the methods as per configured
 * features. JUnit class should be annotated with RunWith annotation along with this class.
 * e.g. @RunWith(FunctionalTestRunner.class)
 *
 */
public class FunctionalTestRunner extends BlockJUnit4ClassRunner
{

	public FunctionalTestRunner(final Class<?> klass) throws InitializationError
	{
		super(klass);
	}

	/**
	 * This method will compare the configured list of functional area with applicable functional area for each Method
	 * with JUnit. If there is any overlap then method will be added to the list of methods that will be executed. If
	 * functionaltest.method.annotation parameter is specified via command line with value false then this method will
	 * not perform any filtering.
	 */
	@Override
	protected List<FrameworkMethod> computeTestMethods()
	{
		final String methodAnnotation = System.getProperty("functionaltest.method.annotation");
		System.out.println("methodAnnotation:" + methodAnnotation);
		if (null == System.getProperty("testclasses.features")
				|| (null != methodAnnotation && Boolean.FALSE.equals(Boolean.valueOf(methodAnnotation))))
		{
			return super.computeTestMethods();
		}
		final List<FrameworkMethod> methods = getTestClass().getAnnotatedMethods(FunctionalTest.class);
		final List<FrameworkMethod> filteredMethods = new ArrayList<>();
		for (final FrameworkMethod method : methods)
		{
			try
			{
				if (FunctionalTestUtil.isFeatureApplicable(method.getAnnotations(),
						System.getProperty("testclasses.features").split(",")))
				{
					filteredMethods.add(method);
				}
			}
			catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
					| SecurityException e)
			{
				throw new RuntimeException(e);
			}
		}
		return filteredMethods;
	}

}
