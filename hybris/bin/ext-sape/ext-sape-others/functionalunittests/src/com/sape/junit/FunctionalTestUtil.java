/**
*
 * Created on Jun 13, 2017
*
* Copyright 2014, SapientRazorfish;  All Rights Reserved.
*
* This software is the confidential and proprietary information of
* SapientRazorfish, ("Confidential Information"). You shall not
* disclose such Confidential Information and shall use it only in
* accordance with the terms of the license agreement you entered into
* with SapientRazorfish.
*
*/
package com.sape.junit;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;


/**
 * Utility class that contains the helper methods
 *
 */
public class FunctionalTestUtil
{
	/**
	 * This method will check if there are any common features between the two input arrays.
	 *
	 * @param classFeatures
	 *           - String array of features to be compared against the configured features.
	 * @param configuredFeatures
	 *           - String array of configured features
	 * @return - returns true if there are any common features between the two arrays.
	 */
	public static boolean isAnyFeaturePresent(final String[] classFeatures, final String[] configuredFeatures)
	{
		final Map testClassFeatureMap = convertToMap(classFeatures);
		for (final String key : configuredFeatures)
		{
			if (testClassFeatureMap.containsKey(key))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will check the FunctionalTest annotation to get the list of functional area corresponding to the
	 * JUnits. It will then compare that against the list of functional area configured for the current execution.
	 *
	 * @param clazz
	 *           - Class for which FunctionalTest annotation will be verified to get the list of applicable functional
	 *           area.
	 * @param configuredFeatureList
	 *           - String array of configured features
	 * @return - returns true if there are any common functional area between the configured value and applied value at
	 *         the class.
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static boolean isFeatureApplicable(final Annotation[] annotations, final String[] configuredFeatureList)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException
	{
		for (final Annotation annot : annotations)
		{
			if (annot.annotationType().getName().equals(FunctionalTest.class.getName()))
			{
				final String[] classFeatures = (String[]) annot.getClass().getMethod("features", new Class[] {}).invoke(annot);
				if (isAnyFeaturePresent(classFeatures, configuredFeatureList))
				{
					return true;
				}
			}

		}
		return false;

	}

	/**
	 * This method will convert the string array to map.
	 *
	 * @param list
	 *           - string array
	 * @return - Map with all the elements from string array.
	 */
	private static Map<String, String> convertToMap(final String[] list)
	{
		final Map<String, String> map = new HashMap<>();
		for (final String item : list)
		{
			map.put(item, item);
		}
		return map;
	}
}
