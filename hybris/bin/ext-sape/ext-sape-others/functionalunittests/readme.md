This extension allows the execution of unit/integration tests based on the functional area. Each Junit can be marked with one or more functional area.
It makes it possible to specify one or more functional area during execution of unit/integration tests. Only those tests will be executed which matches with the one of the functional area.
This makes it possible to get a better picture about the coverage and stability of code for each functional area independently of other.

This extension requires three jar files
1. ${platformhome}/bootstrap/bin/yant.jar
2. ${platformhome}/bootstrap/bin/ybootstrap.jar
3. ${platformhome}/../ext-atdd/atddengine/lib/ant.jar

These files are added to classpath automatically during the build process.


Follow the below mentioned steps to setup and use the extension.

	1. Add the extension functionalunittests in localextensions.xml file.
	
	2. Add the dependency for functionalunittests in extensioninfo.xml of your extension.
	
	3. Annotate your JUnit classes or methods with FunctionalTest and specify the list of functional area corresponding to JUnit or method. e.g.
		@FunctionalTest(features={"category","product"})

	4. Annotate your JUnit classes with FunctionalTestRunner.
		@RunWith(FunctionalTestRunner.class)
	
	4. execute command 'ant clean all' to build the extension. This step will create the functionalunittestsserver.jar file and place it in ${platformhome}\bootstrap\bin folder.
	
	5. Update the taskdef with name yunitint in ${platformhome}\resources\ant\testing.xml with following
		<property name="class.path" value="${platformhome}/bootstrap/bin/functionalunittestsserver.jar" />
		<taskdef classpath="${class.path}" classname="com.sape.junit.ant.task.FunctionalJUnitAntTask" name="yunitint">
		
	6. Add attribute features with value ${testclasses.features} to each of yunitint element in ${platformhome}\resources\ant\testing.xml file. e.g.
		 <yunitint features="${testclasses.features}" failureproperty="yuniterr${yrnd}" printsummary="yes" haltonfailure="no" fork="yes" reloading="false"
                              forkmode="@{forkmode}" showoutput="@{showoutput}" todir="${HYBRIS_TEMP_DIR}/junit"
                              platformhome="${platformhome}" additionalclasspath="@{additionalclasspath}">
	
	7. Add below elements to yunitint within syspropertyset in ${platformhome}\resources\ant\testing.xml file.
		<yunitint features="${testclasses.features}" failureproperty="yuniterr${yrnd}" printsummary="yes" haltonfailure="no" fork="yes" reloading="false"
            forkmode="@{forkmode}" showoutput="@{showoutput}" todir="${HYBRIS_TEMP_DIR}/junit"
            platformhome="${platformhome}" additionalclasspath="@{additionalclasspath}">
			<formatter type="xml"/>
			<tests/>
			<jvmarg line="@{jvmargs}"/>
			<syspropertyset>
				<propertyref prefix="HYBRIS_"/>
				<propertyref name="testclasses.features"/>
				<propertyref name="functionaltest.class.annotation"/>
				<propertyref name="functionaltest.method.annotation"/>
			</syspropertyset>

		</yunitint>
				  
	8. execute one of the targets to execute the unit tests. Use optional parameter testclasses.features to specify the list of functional area. e.g. Below command will execute all Unit/Integration tests that have been annotated with FunctionTest and features product,category
		ant alltests -Dtestclasses.features="category,product"
		
	9. By default annotations will be checked at class level and then at method level. So feature has to be annotated at both places otherwise test case will not be executed. Some optional parameters can be used to disable the annotation check at class level or method level. e.g. 
		ant alltests -Dtestclasses.features="category,product" -Dfunctionaltest.method.annotation=false
		This will disable the annotation check at method level. Annotation present at class level will be used to filter the test case.
		
		ant alltests -Dtestclasses.features="category,product" -Dfunctionaltest.class.annotation=false
		This will disable the annotation check at class level. Annotation present at method levels will be used to filter the test case.
		
		ant alltests -Dtestclasses.features="category,product" -Dfunctionaltest.class.annotation=false -Dfunctionaltest.method.annotation=false
		This will disable the annotation check at both places(class and method). So entire functionality provided by this extension will be disabled.